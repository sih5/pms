﻿using System.Collections.Generic;
using System.Linq;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class VirtualUsedPartsInboundPlanDetailAch : DcsSerivceAchieveBase {
        public VirtualUsedPartsInboundPlanDetailAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public IEnumerable<VirtualUsedPartsInboundPlanDetail> 查询虚拟旧件入库清单(int inboundType, int sourceId, bool canInbound) {
            var result = Enumerable.Empty<VirtualUsedPartsInboundPlanDetail>();
            var sourceid = sourceId;
            var warehouseAreaCode = "";
            var warehouseAreaId = 0;
            switch(inboundType) {
                case (int)DcsUsedPartsInboundOrderInboundType.借用归还:
                    var usedPartsLoanDetails = canInbound ? ObjectContext.UsedPartsLoanDetails.Where(r => r.UsedPartsLoanBillId == sourceId && r.InboundAmount < r.OutboundAmount).ToArray() : ObjectContext.UsedPartsLoanDetails.Where(r => r.UsedPartsLoanBillId == sourceId).ToArray();
                    var usedPartsLoanDetailBarCodes = usedPartsLoanDetails.Select(r => r.UsedPartsBarCode);
                    var usedPartsLoanDetailStocks = (from usedPartsStock in ObjectContext.UsedPartsStocks
                                                     from usedPartsWarehouseArea in ObjectContext.UsedPartsWarehouseAreas
                                                     where usedPartsStock.UsedPartsWarehouseAreaId == usedPartsWarehouseArea.Id
                                                     && usedPartsLoanDetailBarCodes.Contains(usedPartsStock.UsedPartsBarCode)
                                                     select new {
                                                         usedPartsStock.UsedPartsBarCode,
                                                         WarehouseAreaId = usedPartsWarehouseArea.Id,
                                                         WarehouseAreaCode = usedPartsWarehouseArea.Code
                                                     }).ToArray();
                    if(usedPartsLoanDetailStocks.Count() != usedPartsLoanDetails.Count()) {
                        var usedPartsLoanDetailWarehouseArea = ObjectContext.UsedPartsWarehouseAreas.FirstOrDefault(r => ObjectContext.UsedPartsLoanBills.Any(bill => bill.Id == sourceId && r.UsedPartsWarehouseId == bill.UsedPartsWarehouseId) && r.IfDefaultStoragePosition == true && r.Status == (int)DcsBaseDataStatus.有效);
                        if(usedPartsLoanDetailWarehouseArea != null) {
                            warehouseAreaCode = usedPartsLoanDetailWarehouseArea.Code;
                            warehouseAreaId = usedPartsLoanDetailWarehouseArea.Id;
                        }
                    }
                    result = from usedPartsLoanDetail in usedPartsLoanDetails
                             join stock in usedPartsLoanDetailStocks on
                             usedPartsLoanDetail.UsedPartsBarCode equals stock.UsedPartsBarCode into resultGroup
                             from r in resultGroup.DefaultIfEmpty(new {
                                 usedPartsLoanDetail.UsedPartsBarCode,
                                 WarehouseAreaId = warehouseAreaId,
                                 WarehouseAreaCode = warehouseAreaCode,
                             })
                             select new VirtualUsedPartsInboundPlanDetail {
                                 UsedPartsBarCode = usedPartsLoanDetail.UsedPartsBarCode,
                                 UsedPartsCode = usedPartsLoanDetail.UsedPartsCode,
                                 UsedPartsName = usedPartsLoanDetail.UsedPartsName,
                                 UsedPartsWarehouseAreaCode = r.WarehouseAreaCode,
                                 PlannedAmount = usedPartsLoanDetail.PlannedAmount,
                                 ConfirmedAmount = usedPartsLoanDetail.OutboundAmount,
                                 InboundAmount = usedPartsLoanDetail.InboundAmount,
                                 UsedPartsSerialNumber = usedPartsLoanDetail.UsedPartsSerialNumber,
                                 UsedPartsBatchNumber = usedPartsLoanDetail.UsedPartsBatchNumber,
                                 Remark = null,
                                 UsedPartsInboundOrderId = null,
                                 UsedPartsId = usedPartsLoanDetail.UsedPartsId,
                                 UsedPartsWarehouseAreaId = r.WarehouseAreaId,
                                 SettlementPrice = usedPartsLoanDetail.Price,
                                 CostPrice = null,
                                 BranchId = usedPartsLoanDetail.BranchId,
                                 ClaimBillId = usedPartsLoanDetail.ClaimBillId,
                                 ClaimBillCode = usedPartsLoanDetail.ClaimBillCode,
                                 ClaimBillType = usedPartsLoanDetail.ClaimBillType,
                                 UsedPartsSupplierId = usedPartsLoanDetail.UsedPartsSupplierId,
                                 UsedPartsSupplierCode = usedPartsLoanDetail.UsedPartsSupplierCode,
                                 UsedPartsSupplierName = usedPartsLoanDetail.UsedPartsSupplierName,
                                 IfFaultyParts = usedPartsLoanDetail.IfFaultyParts,
                                 FaultyPartsCode = usedPartsLoanDetail.FaultyPartsCode,
                                 FaultyPartsName = usedPartsLoanDetail.FaultyPartsName,
                                 FaultyPartsSupplierId = usedPartsLoanDetail.FaultyPartsSupplierId,
                                 FaultyPartsSupplierCode = usedPartsLoanDetail.FaultyPartsSupplierCode,
                                 FaultyPartsSupplierName = usedPartsLoanDetail.FaultyPartsSupplierName,
                                 ResponsibleUnitId = usedPartsLoanDetail.ResponsibleUnitId,
                                 ResponsibleUnitCode = usedPartsLoanDetail.ResponsibleUnitCode,
                                 SourceId = sourceid,
                             };
                    break;
                case (int)DcsUsedPartsInboundOrderInboundType.加工入库:
                    var usedPartsRefitReturnDetails = canInbound ? ObjectContext.UsedPartsRefitReturnDetails.Where(r => r.UsedPartsRefitBillId == sourceId && r.InboundAmount < r.ConfirmedAmount).ToArray() : ObjectContext.UsedPartsRefitReturnDetails.Where(r => r.UsedPartsRefitBillId == sourceId).ToArray();
                    var usedPartsRefitReturnDetailBarCodes = usedPartsRefitReturnDetails.Select(r => r.UsedPartsBarCode);
                    var usedPartsRefitReturnDetaiStocks = (from usedPartsStock in ObjectContext.UsedPartsStocks
                                                           from usedPartsWarehouseArea in ObjectContext.UsedPartsWarehouseAreas
                                                           where usedPartsStock.UsedPartsWarehouseAreaId == usedPartsWarehouseArea.Id
                                                           && usedPartsRefitReturnDetailBarCodes.Contains(usedPartsStock.UsedPartsBarCode)
                                                           select new {
                                                               usedPartsStock.UsedPartsBarCode,
                                                               WarehouseAreaId = usedPartsWarehouseArea.Id,
                                                               WarehouseAreaCode = usedPartsWarehouseArea.Code
                                                           }).ToArray();
                    if(usedPartsRefitReturnDetaiStocks.Count() != usedPartsRefitReturnDetails.Count()) {
                        var usedPartsRefitReturnDetailWarehouseArea = ObjectContext.UsedPartsWarehouseAreas.FirstOrDefault(r => ObjectContext.UsedPartsRefitBills.Any(bill => bill.Id == sourceId && r.UsedPartsWarehouseId == bill.UsedPartsWarehouseId) && r.IfDefaultStoragePosition == true && r.Status == (int)DcsBaseDataStatus.有效);
                        if(usedPartsRefitReturnDetailWarehouseArea != null) {
                            warehouseAreaCode = usedPartsRefitReturnDetailWarehouseArea.Code;
                            warehouseAreaId = usedPartsRefitReturnDetailWarehouseArea.Id;
                        }
                    }
                    result = from usedPartsRefitReturnDetail in usedPartsRefitReturnDetails
                             join stock in usedPartsRefitReturnDetaiStocks on usedPartsRefitReturnDetail.UsedPartsBarCode
                             equals stock.UsedPartsBarCode into resultGroup
                             from r in resultGroup.DefaultIfEmpty(new {
                                 usedPartsRefitReturnDetail.UsedPartsBarCode,
                                 WarehouseAreaId = warehouseAreaId,
                                 WarehouseAreaCode = warehouseAreaCode,
                             })
                             select new VirtualUsedPartsInboundPlanDetail {
                                 UsedPartsBarCode = usedPartsRefitReturnDetail.UsedPartsBarCode,
                                 UsedPartsCode = usedPartsRefitReturnDetail.UsedPartsCode,
                                 UsedPartsName = usedPartsRefitReturnDetail.UsedPartsName,
                                 UsedPartsWarehouseAreaCode = r.WarehouseAreaCode,
                                 PlannedAmount = usedPartsRefitReturnDetail.PlannedAmount,
                                 ConfirmedAmount = usedPartsRefitReturnDetail.ConfirmedAmount,
                                 InboundAmount = usedPartsRefitReturnDetail.InboundAmount,
                                 UsedPartsSerialNumber = usedPartsRefitReturnDetail.UsedPartsSerialNumber,
                                 UsedPartsBatchNumber = usedPartsRefitReturnDetail.UsedPartsBatchNumber,
                                 Remark = null,
                                 UsedPartsInboundOrderId = null,
                                 UsedPartsId = usedPartsRefitReturnDetail.UsedPartsId,
                                 UsedPartsWarehouseAreaId = r.WarehouseAreaId,
                                 SettlementPrice = usedPartsRefitReturnDetail.Price,
                                 CostPrice = null,
                                 BranchId = usedPartsRefitReturnDetail.BranchId,
                                 ClaimBillId = null,
                                 ClaimBillCode = null,
                                 ClaimBillType = null,
                                 UsedPartsSupplierId = null,
                                 UsedPartsSupplierCode = null,
                                 UsedPartsSupplierName = null,
                                 IfFaultyParts = null,
                                 FaultyPartsCode = null,
                                 FaultyPartsName = null,
                                 FaultyPartsSupplierId = null,
                                 FaultyPartsSupplierCode = null,
                                 FaultyPartsSupplierName = null,
                                 ResponsibleUnitId = null,
                                 ResponsibleUnitCode = null,
                                 SourceId = sourceid,
                             };
                    break;
                case (int)DcsUsedPartsInboundOrderInboundType.调拨入库:
                    var usedPartsTransferDetails = canInbound ? ObjectContext.UsedPartsTransferDetails.Where(r => r.UsedPartsTransferOrderId == sourceId && r.InboundAmount < r.ConfirmedAmount).ToArray() : ObjectContext.UsedPartsTransferDetails.Where(r => r.UsedPartsTransferOrderId == sourceId).ToArray();
                    //var usedPartsTransferDetails = canInbound ? ObjectContext.UsedPartsTransferDetails.Where(r => r.UsedPartsTransferOrderId == sourceId && r.InboundAmount < r.ConfirmedAmount && r.InboundAmount < r.OutboundAmount).ToArray() : ObjectContext.UsedPartsTransferDetails.Where(r => r.UsedPartsTransferOrderId == sourceId).ToArray();
          
                    var usedPartsTransferDetailBarCodes = usedPartsTransferDetails.Select(r => r.UsedPartsBarCode);
                    var usedPartsTransferDetailStocks = (from usedPartsStock in ObjectContext.UsedPartsStocks
                                                         from usedPartsWarehouseArea in ObjectContext.UsedPartsWarehouseAreas
                                                         where usedPartsStock.UsedPartsWarehouseAreaId == usedPartsWarehouseArea.Id
                                                         && usedPartsTransferDetailBarCodes.Contains(usedPartsStock.UsedPartsBarCode)
                                                         select new {
                                                             usedPartsStock.UsedPartsBarCode,
                                                             WarehouseAreaId = usedPartsWarehouseArea.Id,
                                                             WarehouseAreaCode = usedPartsWarehouseArea.Code
                                                         }).ToArray();
                    if(usedPartsTransferDetailStocks.Count() != usedPartsTransferDetails.Count()) {
                        var usedPartsTransferDetailWarehouseArea = ObjectContext.UsedPartsWarehouseAreas.FirstOrDefault(r => ObjectContext.UsedPartsTransferOrders.Any(bill => bill.Id == sourceId && r.UsedPartsWarehouseId == bill.DestinationWarehouseId) && r.IfDefaultStoragePosition == true && r.Status == (int)DcsBaseDataStatus.有效);
                        if(usedPartsTransferDetailWarehouseArea != null) {
                            warehouseAreaCode = usedPartsTransferDetailWarehouseArea.Code;
                            warehouseAreaId = usedPartsTransferDetailWarehouseArea.Id;
                        }
                    }
                    //获取默认库位
                    var usedPartsTransfer = ObjectContext.UsedPartsTransferOrders.FirstOrDefault(r => r.Id == sourceId);
                    var warehousearea = ObjectContext.UsedPartsWarehouseAreas.FirstOrDefault(r => r.UsedPartsWarehouseId == usedPartsTransfer.DestinationWarehouseId && r.IfDefaultStoragePosition.HasValue && r.IfDefaultStoragePosition==true);
                   

                    result = from usedPartsTransferDetail in usedPartsTransferDetails
                             //join stock in usedPartsTransferDetailStocks on usedPartsTransferDetail.UsedPartsBarCode
                             //equals stock.UsedPartsBarCode into resultGroup
                             //from r in resultGroup.DefaultIfEmpty(new {
                             //    usedPartsTransferDetail.UsedPartsBarCode,
                             //    WarehouseAreaId = warehouseAreaId,
                             //    WarehouseAreaCode = warehouseAreaCode,
                             //})
                             select new VirtualUsedPartsInboundPlanDetail {
                                 UsedPartsBarCode = usedPartsTransferDetail.UsedPartsBarCode,
                                 UsedPartsCode = usedPartsTransferDetail.UsedPartsCode,
                                 UsedPartsName = usedPartsTransferDetail.UsedPartsName,
                                 UsedPartsWarehouseAreaCode = warehousearea.Code,
                                 PlannedAmount = usedPartsTransferDetail.PlannedAmount,
                                 ConfirmedAmount = usedPartsTransferDetail.ConfirmedAmount,
                                 InboundAmount = usedPartsTransferDetail.InboundAmount,
                                 UsedPartsSerialNumber = usedPartsTransferDetail.UsedPartsSerialNumber,
                                 UsedPartsBatchNumber = usedPartsTransferDetail.UsedPartsBatchNumber,
                                 Remark = null,
                                 UsedPartsInboundOrderId = null,
                                 UsedPartsId = usedPartsTransferDetail.UsedPartsId,
                                 UsedPartsWarehouseAreaId = warehousearea.Id,
                                 SettlementPrice = usedPartsTransferDetail.Price,
                                 CostPrice = null,
                                 BranchId = usedPartsTransferDetail.BranchId,
                                 ClaimBillId = usedPartsTransferDetail.ClaimBillId,
                                 ClaimBillCode = usedPartsTransferDetail.ClaimBillCode,
                                 ClaimBillType = usedPartsTransferDetail.ClaimBillType,
                                 UsedPartsSupplierId = usedPartsTransferDetail.UsedPartsSupplierId,
                                 UsedPartsSupplierCode = usedPartsTransferDetail.UsedPartsSupplierCode,
                                 UsedPartsSupplierName = usedPartsTransferDetail.UsedPartsSupplierName,
                                 IfFaultyParts = usedPartsTransferDetail.IfFaultyParts,
                                 FaultyPartsCode = usedPartsTransferDetail.FaultyPartsCode,
                                 FaultyPartsName = usedPartsTransferDetail.FaultyPartsName,
                                 FaultyPartsSupplierId = usedPartsTransferDetail.FaultyPartsSupplierId,
                                 FaultyPartsSupplierCode = usedPartsTransferDetail.FaultyPartsSupplierCode,
                                 FaultyPartsSupplierName = usedPartsTransferDetail.FaultyPartsSupplierName,
                                 ResponsibleUnitId = usedPartsTransferDetail.ResponsibleUnitId,
                                 ResponsibleUnitCode = usedPartsTransferDetail.ResponsibleUnitCode,
                                 SourceId = sourceid,
                             };
                    break;
                case (int)DcsUsedPartsInboundOrderInboundType.服务站返件入库:
                    var usedPartsShippingDetails = canInbound ? ObjectContext.UsedPartsShippingDetails.Where(r => r.UsedPartsShippingOrderId == sourceId && r.InboundAmount < r.Quantity).ToArray() : ObjectContext.UsedPartsShippingDetails.Where(r => r.UsedPartsShippingOrderId == sourceId).ToArray();
                    var usedPartsShippingDetailBarCodes = usedPartsShippingDetails.Select(r => r.UsedPartsBarCode);
                    var usedPartsShippingDetailStocks = (from usedPartsStock in ObjectContext.UsedPartsStocks
                                                         from usedPartsWarehouseArea in ObjectContext.UsedPartsWarehouseAreas
                                                         where usedPartsStock.UsedPartsWarehouseAreaId == usedPartsWarehouseArea.Id
                                                         && usedPartsShippingDetailBarCodes.Contains(usedPartsStock.UsedPartsBarCode)
                                                         select new {
                                                             usedPartsStock.UsedPartsBarCode,
                                                             WarehouseAreaId = usedPartsWarehouseArea.Id,
                                                             WarehouseAreaCode = usedPartsWarehouseArea.Code
                                                         }).ToArray();
                    if(usedPartsShippingDetailStocks.Count() != usedPartsShippingDetails.Count()) {
                        var usedPartsTransferDetailWarehouseArea = ObjectContext.UsedPartsWarehouseAreas.FirstOrDefault(r => ObjectContext.UsedPartsShippingOrders.Any(bill => bill.Id == sourceId && r.UsedPartsWarehouseId == bill.DestinationWarehouseId) && r.IfDefaultStoragePosition == true && r.Status == (int)DcsBaseDataStatus.有效);
                        if(usedPartsTransferDetailWarehouseArea != null) {
                            warehouseAreaCode = usedPartsTransferDetailWarehouseArea.Code;
                            warehouseAreaId = usedPartsTransferDetailWarehouseArea.Id;
                        }
                    }
                    result = from usedPartsShippingDetail in usedPartsShippingDetails.Where(r => r.ReceptionStatus != (int)DcsUsedPartsShippingDetailReceptionStatus.未接收 && r.ReceptionStatus != (int)DcsUsedPartsShippingDetailReceptionStatus.罚没)
                             join stock in usedPartsShippingDetailStocks on usedPartsShippingDetail.UsedPartsBarCode
                             equals stock.UsedPartsBarCode into resultGroup
                             from r in resultGroup.DefaultIfEmpty(new {
                                 usedPartsShippingDetail.UsedPartsBarCode,
                                 WarehouseAreaId = warehouseAreaId,
                                 WarehouseAreaCode = warehouseAreaCode,
                             })
                             select new VirtualUsedPartsInboundPlanDetail {
                                 UsedPartsBarCode = usedPartsShippingDetail.UsedPartsBarCode,
                                 UsedPartsCode = usedPartsShippingDetail.UsedPartsCode,
                                 UsedPartsName = usedPartsShippingDetail.UsedPartsName,
                                 UsedPartsWarehouseAreaId = usedPartsShippingDetail.UsedPartsWarehouseAreaId ?? r.WarehouseAreaId,
                                 UsedPartsWarehouseAreaCode = usedPartsShippingDetail.UsedPartsWarehouseAreaCode ?? r.WarehouseAreaCode,
                                 PlannedAmount = 1,
                                 ConfirmedAmount = 1,
                                 InboundAmount = 1,
                                 UsedPartsSerialNumber = usedPartsShippingDetail.UsedPartsSerialNumber,
                                 UsedPartsBatchNumber = usedPartsShippingDetail.UsedPartsBatchNumber,
                                 Remark = null,
                                 UsedPartsInboundOrderId = null,
                                 UsedPartsId = usedPartsShippingDetail.UsedPartsId,
                                 SettlementPrice = usedPartsShippingDetail.UnitPrice,//+ (usedPartsShippingDetail.UsedPartsEncourageAmount ?? 0)
                                 CostPrice = null,
                                 BranchId = usedPartsShippingDetail.BranchId,
                                 ClaimBillId = usedPartsShippingDetail.ClaimBillId,
                                 ClaimBillCode = usedPartsShippingDetail.ClaimBillCode,
                                 ClaimBillType = usedPartsShippingDetail.ClaimBillType,
                                 UsedPartsSupplierId = usedPartsShippingDetail.UsedPartsSupplierId,
                                 UsedPartsSupplierCode = usedPartsShippingDetail.UsedPartsSupplierCode,
                                 UsedPartsSupplierName = usedPartsShippingDetail.UsedPartsSupplierName,
                                 IfFaultyParts = usedPartsShippingDetail.IfFaultyParts,
                                 FaultyPartsCode = usedPartsShippingDetail.FaultyPartsCode,
                                 FaultyPartsName = usedPartsShippingDetail.FaultyPartsName,
                                 FaultyPartsSupplierId = usedPartsShippingDetail.FaultyPartsSupplierId,
                                 FaultyPartsSupplierCode = usedPartsShippingDetail.FaultyPartsSupplierCode,
                                 FaultyPartsSupplierName = usedPartsShippingDetail.FaultyPartsSupplierName,
                                 ResponsibleUnitId = usedPartsShippingDetail.ResponsibleUnitId,
                                 ResponsibleUnitCode = usedPartsShippingDetail.ResponsibleUnitCode,
                                 SourceId = sourceid,
                                 ReceptionStatus = usedPartsShippingDetail.ReceptionStatus,
                                 ReceptionRemark = usedPartsShippingDetail.ReceptionRemark,
                                 UsedPartsEncourageAmount = usedPartsShippingDetail.UsedPartsEncourageAmount
                             };
                    break;
            }
            return result.OrderBy(r => r.UsedPartsBarCode);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:30
        //原分布类的函数全部转移到Ach                                                               

        public IEnumerable<VirtualUsedPartsInboundPlanDetail> 查询虚拟旧件入库清单(int inboundType, int sourceId, bool canInbound) {
            return new VirtualUsedPartsInboundPlanDetailAch(this).查询虚拟旧件入库清单(inboundType,sourceId,canInbound);
        }
    }
}
