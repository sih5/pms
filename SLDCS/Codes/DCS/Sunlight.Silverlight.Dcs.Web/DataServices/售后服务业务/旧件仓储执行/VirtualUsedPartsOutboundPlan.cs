﻿using System;
using System.Collections.Generic;
using System.Linq;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class VirtualUsedPartsOutboundPlanAch : DcsSerivceAchieveBase {
        public VirtualUsedPartsOutboundPlanAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public IEnumerable<VirtualUsedPartsOutboundPlan> 查询虚拟旧件出库计划(string sourceCode, DateTime? sourceBillTimeBegin, DateTime? sourceBillTimeEnd, int? outboundType, int? outboundStatus, string usedPartsWarehouseCode, string usedPartsWarehouseName, int personnelId, string relatedCompanyName, string businessCode) {
            var result = Enumerable.Empty<VirtualUsedPartsOutboundPlan>();
            var topLevelUsedPartsWarehouseAreas = ObjectContext.UsedPartsWarehouseAreas.Where(r =>
                ObjectContext.UsedPartsWarehouseManagers.Any(usedPartsWarehouseManager => usedPartsWarehouseManager.PersonnelId == personnelId && usedPartsWarehouseManager.UsedPartsWarehouseAreaId == r.TopLevelUsedPartsWhseAreaId)
                && r.Status == (int)DcsBaseDataStatus.有效);
            var usedPartsWarehouseIds = topLevelUsedPartsWarehouseAreas.Select(r => r.UsedPartsWarehouseId).ToArray();
            if(usedPartsWarehouseIds.Length == 0)
                return result;
            var usedPartsLoanBills = (from a in ObjectContext.UsedPartsLoanBills.Where(r => r.Status == (int)DcsUsedPartsLoanBillStatus.生效
                 && (r.OutboundStatus == (int)DcsUsedPartsOutboundStatus.待出库 || r.OutboundStatus == (int)DcsUsedPartsOutboundStatus.部分出库)
                 && usedPartsWarehouseIds.Contains(r.UsedPartsWarehouseId))
                                      from usedPartsLoanDetail in ObjectContext.UsedPartsLoanDetails.Where(r => r.UsedPartsLoanBillId == a.Id && r.OutboundAmount < r.ConfirmedAmount)
                                      from usedPartsStock in ObjectContext.UsedPartsStocks.Where(r => r.UsedPartsWarehouseId == a.UsedPartsWarehouseId)
                                      from usedPartsWarehouseArea in topLevelUsedPartsWarehouseAreas
                                      where usedPartsLoanDetail.UsedPartsBarCode == usedPartsStock.UsedPartsBarCode
                                      && usedPartsWarehouseArea.Id == usedPartsStock.UsedPartsWarehouseAreaId
                                      select new VirtualUsedPartsOutboundPlan {
                                          SourceId = a.Id,
                                          SourceCode = a.Code,
                                          SourceBillApprovalTime = a.ApproveTime,
                                          OutboundType = (int)DcsUsedPartsOutboundOrderOutboundType.借用出库,
                                          OutboundStatus = a.OutboundStatus,
                                          UsedPartsWarehouseCode = a.UsedPartsWarehouseCode,
                                          UsedPartsWarehouseName = a.UsedPartsWarehouseName,
                                          UsedPartsWarehouseId = a.UsedPartsWarehouseId,
                                          RelatedCompanyId = null,
                                          RelatedCompanyCode = null,
                                          RelatedCompanyName = a.RelatedCompanyName,
                                          BusinessCode = null,
                                          Remark = a.Remark
                                      }).Distinct();
            var usedPartsRefitBills = (from a in ObjectContext.UsedPartsRefitBills.Where(r => r.Status == (int)DcsUsedPartsRefitBillStatus.生效
                 && (r.OutboundStatus == (int)DcsUsedPartsOutboundStatus.待出库 || r.OutboundStatus == (int)DcsUsedPartsOutboundStatus.部分出库)
                 && usedPartsWarehouseIds.Contains(r.UsedPartsWarehouseId))
                                       from usedPartsRefitReqDetail in ObjectContext.UsedPartsRefitReqDetails.Where(r => r.UsedPartsRefitBillId == a.Id && r.OutboundAmount < r.ConfirmedAmount)
                                       from usedPartsStock in ObjectContext.UsedPartsStocks.Where(r => r.UsedPartsWarehouseId == a.UsedPartsWarehouseId)
                                       from usedPartsWarehouseArea in topLevelUsedPartsWarehouseAreas
                                       where usedPartsRefitReqDetail.UsedPartsBarCode == usedPartsStock.UsedPartsBarCode
                                       && usedPartsWarehouseArea.Id == usedPartsStock.UsedPartsWarehouseAreaId
                                       select new VirtualUsedPartsOutboundPlan {
                                           SourceId = a.Id,
                                           SourceCode = a.Code,
                                           SourceBillApprovalTime = a.ApproveTime,
                                           OutboundType = (int)DcsUsedPartsOutboundOrderOutboundType.加工领料出库,
                                           OutboundStatus = a.OutboundStatus,
                                           UsedPartsWarehouseCode = a.UsedPartsWarehouseCode,
                                           UsedPartsWarehouseName = a.UsedPartsWarehouseName,
                                           UsedPartsWarehouseId = a.UsedPartsWarehouseId,
                                           RelatedCompanyId = null,
                                           RelatedCompanyCode = null,
                                           RelatedCompanyName = a.RelatedCompanyName,
                                           BusinessCode = null,
                                           Remark = a.Remark
                                       }).Distinct();
            var usedPartsDisposalBills = (from a in ObjectContext.UsedPartsDisposalBills.Where(r => r.Status == (int)DcsUsedPartsDisposalBillStatus.生效
                 && (r.OutboundStatus == (int)DcsUsedPartsOutboundStatus.待出库 || r.OutboundStatus == (int)DcsUsedPartsOutboundStatus.部分出库)
                 && usedPartsWarehouseIds.Contains(r.UsedPartsWarehouseId))
                                          from usedPartsDisposalDetail in ObjectContext.UsedPartsDisposalDetails.Where(r => r.UsedPartsDisposalBillId == a.Id && r.OutboundAmount < r.ConfirmedAmount)
                                          from usedPartsStock in ObjectContext.UsedPartsStocks.Where(r => r.UsedPartsWarehouseId == a.UsedPartsWarehouseId)
                                          from usedPartsWarehouseArea in topLevelUsedPartsWarehouseAreas
                                          where usedPartsDisposalDetail.UsedPartsBarCode == usedPartsStock.UsedPartsBarCode
                                          && usedPartsWarehouseArea.Id == usedPartsStock.UsedPartsWarehouseAreaId
                                          select new VirtualUsedPartsOutboundPlan {
                                              SourceId = a.Id,
                                              SourceCode = a.Code,
                                              SourceBillApprovalTime = a.ApproveTime,
                                              OutboundType = (int)DcsUsedPartsOutboundOrderOutboundType.旧件处理出库,
                                              OutboundStatus = a.OutboundStatus,
                                              UsedPartsWarehouseCode = a.UsedPartsWarehouseCode,
                                              UsedPartsWarehouseName = a.UsedPartsWarehouseName,
                                              UsedPartsWarehouseId = a.UsedPartsWarehouseId,
                                              RelatedCompanyId = null,
                                              RelatedCompanyCode = null,
                                              RelatedCompanyName = a.RelatedCompanyName,
                                              BusinessCode = null,
                                              Remark = a.Remark
                                          }).Distinct();
            var usedPartsTransferOrders = (from a in ObjectContext.UsedPartsTransferOrders.Where(r => r.Status == (int)DcsUsedPartsTransferOrderStatus.生效
                && (r.OutboundStatus == (int)DcsUsedPartsOutboundStatus.待出库 || r.OutboundStatus == (int)DcsUsedPartsOutboundStatus.部分出库)
                && usedPartsWarehouseIds.Contains(r.OriginWarehouseId))
                                           from usedPartsTransferDetail in ObjectContext.UsedPartsTransferDetails.Where(r => r.UsedPartsTransferOrderId == a.Id && r.OutboundAmount < r.InboundAmount)
                                           //from usedPartsTransferDetail in ObjectContext.UsedPartsTransferDetails.Where(r => r.UsedPartsTransferOrderId == a.Id && r.OutboundAmount < r.ConfirmedAmount)
                                           from usedPartsStock in ObjectContext.UsedPartsStocks.Where(r => r.UsedPartsWarehouseId == a.OriginWarehouseId)
                                           from usedPartsWarehouseArea in topLevelUsedPartsWarehouseAreas
                                           where usedPartsTransferDetail.UsedPartsBarCode == usedPartsStock.UsedPartsBarCode
                                           && usedPartsWarehouseArea.Id == usedPartsStock.UsedPartsWarehouseAreaId
                                           select new VirtualUsedPartsOutboundPlan {
                                               SourceId = a.Id,
                                               SourceCode = a.Code,
                                               SourceBillApprovalTime = a.ApproveTime,
                                               OutboundType = (int)DcsUsedPartsOutboundOrderOutboundType.调拨出库,
                                               OutboundStatus = a.OutboundStatus,
                                               UsedPartsWarehouseCode = a.OriginWarehouseCode,
                                               UsedPartsWarehouseName = a.OriginWarehouseName,
                                               UsedPartsWarehouseId = a.OriginWarehouseId,
                                               RelatedCompanyId = a.DestinationWarehouseId,
                                               RelatedCompanyCode = a.DestinationWarehouseCode,
                                               RelatedCompanyName = a.DestinationWarehouseName,
                                               BusinessCode = null,
                                               Remark = a.Remark
                                           }).Distinct();
            var usedPartsReturnOrders = (from a in ObjectContext.UsedPartsReturnOrders.Where(r => r.Status == (int)DcsUsedPartsReturnOrderStatus.已审批
                && (r.OutboundStatus == (int)DcsUsedPartsOutboundStatus.待出库 || r.OutboundStatus == (int)DcsUsedPartsOutboundStatus.部分出库)
                && usedPartsWarehouseIds.Contains(r.OutboundWarehouseId))
                                         from usedPartsReturnDetail in ObjectContext.UsedPartsReturnDetails.Where(r => r.UsedPartsReturnOrderId == a.Id && r.OutboundAmount < r.ConfirmedAmount)
                                         from usedPartsStock in ObjectContext.UsedPartsStocks.Where(r => r.UsedPartsWarehouseId == a.OutboundWarehouseId)
                                         from usedPartsWarehouseArea in topLevelUsedPartsWarehouseAreas
                                         from c in ObjectContext.UsedPartsWarehouses
                                         join b in ObjectContext.BranchSupplierRelations.Where(r=>r.Status == (int)DcsBaseDataStatus.有效) on new {
                                             SupplierId = a.ReturnOfficeId,
                                             PartsSalesCategoryId = c.PartsSalesCategoryId
                                         }equals new {
                                             b.SupplierId,
                                             b.PartsSalesCategoryId
                                         }into tempTable
                                         from t in tempTable.DefaultIfEmpty()
                                         where usedPartsReturnDetail.UsedPartsBarCode == usedPartsStock.UsedPartsBarCode
                                         && usedPartsWarehouseArea.Id == usedPartsStock.UsedPartsWarehouseAreaId
                                         && c.Id == a.OutboundWarehouseId
                                         select new VirtualUsedPartsOutboundPlan {
                                             SourceId = a.Id,
                                             SourceCode = a.Code,
                                             SourceBillApprovalTime = a.ApproveTime,
                                             OutboundType = (int)DcsUsedPartsOutboundOrderOutboundType.清退供应商出库,
                                             OutboundStatus = a.OutboundStatus,
                                             UsedPartsWarehouseCode = a.OutboundWarehouseCode,
                                             UsedPartsWarehouseName = a.OutboundWarehouseName,
                                             UsedPartsWarehouseId = a.OutboundWarehouseId,
                                             RelatedCompanyId = a.ReturnOfficeId,
                                             RelatedCompanyCode = a.ReturnOfficeCode,
                                             RelatedCompanyName = a.ReturnOfficeName,
                                             BusinessCode = t.BusinessCode,
                                             Remark = a.Remark
                                         }).Distinct();
            if(outboundType.HasValue) {
                switch(outboundType) {
                    case (int)DcsUsedPartsOutboundOrderOutboundType.借用出库:
                        result = usedPartsLoanBills;
                        break;
                    case (int)DcsUsedPartsOutboundOrderOutboundType.加工领料出库:
                        result = usedPartsRefitBills;
                        break;
                    case (int)DcsUsedPartsOutboundOrderOutboundType.旧件处理出库:
                        result = usedPartsDisposalBills;
                        break;
                    case (int)DcsUsedPartsOutboundOrderOutboundType.调拨出库:
                        result = usedPartsTransferOrders;
                        break;
                    case (int)DcsUsedPartsOutboundOrderOutboundType.清退供应商出库:
                        result = usedPartsReturnOrders;
                        break;
                }
            }else
                result = usedPartsLoanBills.Concat(usedPartsRefitBills.Concat(usedPartsDisposalBills.Concat(usedPartsTransferOrders.Concat(usedPartsReturnOrders))));
            if(!string.IsNullOrEmpty(sourceCode))
                result = result.Where(r => r.SourceCode.ToLower().Contains(sourceCode.ToLower()));
            if(!string.IsNullOrEmpty(businessCode))
                result = result.Where(r => r.BusinessCode != null && r.BusinessCode.Contains(businessCode));
            if(!string.IsNullOrEmpty(relatedCompanyName))
                result = result.Where(r => r.RelatedCompanyName != null && r.RelatedCompanyName.Contains(relatedCompanyName));
            if(sourceBillTimeBegin.HasValue)
                result = result.Where(r => r.SourceBillApprovalTime >= sourceBillTimeBegin.Value);
            if(sourceBillTimeEnd.HasValue)
                result = result.Where(r => r.SourceBillApprovalTime <= sourceBillTimeEnd.Value);
            if(outboundStatus.HasValue)
                result = result.Where(r => r.OutboundStatus == outboundStatus);
            if(!string.IsNullOrEmpty(usedPartsWarehouseCode))
                result = result.Where(r => r.UsedPartsWarehouseCode.ToLower().Contains(usedPartsWarehouseCode.ToLower()));
            if(!string.IsNullOrEmpty(usedPartsWarehouseName))
                result = result.Where(r => r.UsedPartsWarehouseName.ToLower().Contains(usedPartsWarehouseName.ToLower()));
            return result.OrderBy(r => r.SourceBillApprovalTime);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:30
        //原分布类的函数全部转移到Ach                                                               

        public IEnumerable<VirtualUsedPartsOutboundPlan> 查询虚拟旧件出库计划(string sourceCode, DateTime? sourceBillTimeBegin, DateTime? sourceBillTimeEnd, int? outboundType, int? outboundStatus, string usedPartsWarehouseCode, string usedPartsWarehouseName, int personnelId, string relatedCompanyName, string businessCode) {
            return new VirtualUsedPartsOutboundPlanAch(this).查询虚拟旧件出库计划(sourceCode,sourceBillTimeBegin,sourceBillTimeEnd,outboundType,outboundStatus,usedPartsWarehouseCode,usedPartsWarehouseName,personnelId,relatedCompanyName,businessCode);
        }
    }
}
