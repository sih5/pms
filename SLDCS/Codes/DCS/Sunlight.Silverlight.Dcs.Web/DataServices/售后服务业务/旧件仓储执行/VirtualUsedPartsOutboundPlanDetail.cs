﻿using System.Collections.Generic;
using System.Linq;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class VirtualUsedPartsOutboundPlanDetailAch : DcsSerivceAchieveBase {
        public VirtualUsedPartsOutboundPlanDetailAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public IEnumerable<VirtualUsedPartsOutboundPlanDetail> 查询虚拟旧件出库清单(int outboundType, int sourceId, bool canOutbound, int personnelId, int usedPartsWarehouseId) {
            var result = Enumerable.Empty<VirtualUsedPartsOutboundPlanDetail>();
            var sourceid = sourceId;
            //使用lower函数，导致sql出现cross join关联
            //考虑实际业务数据旧件条码大小写不匹配的情况相对较小，衡量sql性能，暂时去除lower函数
            switch(outboundType) {
                case (int)DcsUsedPartsOutboundOrderOutboundType.借用出库:
                    var usedPartsLoanDetails = ObjectContext.UsedPartsLoanDetails.Where(r => r.UsedPartsLoanBillId == sourceId);
                    IEnumerable<UsedPartsWarehouseArea> usedPartsLoanDetailWarehouseAreas = ObjectContext.UsedPartsWarehouseAreas;
                    if(canOutbound) {
                        usedPartsLoanDetails = usedPartsLoanDetails.Where(r => r.OutboundAmount < r.ConfirmedAmount);
                        usedPartsLoanDetailWarehouseAreas = ObjectContext.UsedPartsWarehouseAreas.Where(r => ObjectContext.UsedPartsWarehouseManagers.Any(usedPartsWarehouseManager => usedPartsWarehouseManager.PersonnelId == personnelId && usedPartsWarehouseManager.UsedPartsWarehouseAreaId == r.TopLevelUsedPartsWhseAreaId) && r.Status == (int)DcsBaseDataStatus.有效);
                    }
                    result = from usedPartsLoanDetail in usedPartsLoanDetails
                             from usedPartsStock in ObjectContext.UsedPartsStocks.Where(r => r.UsedPartsWarehouseId == usedPartsWarehouseId)
                             from usedPartsWarehouseArea in usedPartsLoanDetailWarehouseAreas
                             where usedPartsLoanDetail.UsedPartsBarCode == usedPartsStock.UsedPartsBarCode
                             && usedPartsWarehouseArea.Id == usedPartsStock.UsedPartsWarehouseAreaId
                             select new VirtualUsedPartsOutboundPlanDetail {
                                 UsedPartsBarCode = usedPartsLoanDetail.UsedPartsBarCode,
                                 UsedPartsCode = usedPartsLoanDetail.UsedPartsCode,
                                 UsedPartsName = usedPartsLoanDetail.UsedPartsName,
                                 UsedPartsWarehouseAreaCode = usedPartsWarehouseArea.Code,
                                 PlannedAmount = usedPartsLoanDetail.PlannedAmount,
                                 ConfirmedAmount = usedPartsLoanDetail.ConfirmedAmount,
                                 OutboundAmount = usedPartsLoanDetail.OutboundAmount,
                                 UsedPartsSerialNumber = usedPartsLoanDetail.UsedPartsSerialNumber,
                                 UsedPartsBatchNumber = usedPartsLoanDetail.UsedPartsBatchNumber,
                                 Remark = null,
                                 UsedPartsOutboundOrderId = null,
                                 UsedPartsId = usedPartsStock.UsedPartsId,
                                 UsedPartsWarehouseAreaId = usedPartsStock.UsedPartsWarehouseAreaId,
                                 SettlementPrice = usedPartsLoanDetail.Price,
                                 CostPrice = usedPartsStock.CostPrice,
                                 BranchId = usedPartsLoanDetail.BranchId,
                                 ClaimBillId = usedPartsLoanDetail.ClaimBillId,
                                 ClaimBillType = usedPartsLoanDetail.ClaimBillType,
                                 ClaimBillCode = usedPartsLoanDetail.ClaimBillCode,
                                 IfFaultyParts = usedPartsLoanDetail.IfFaultyParts,
                                 FaultyPartsSupplierId = usedPartsLoanDetail.FaultyPartsSupplierId,
                                 FaultyPartsSupplierCode = usedPartsLoanDetail.FaultyPartsSupplierCode,
                                 FaultyPartsSupplierName = usedPartsLoanDetail.FaultyPartsSupplierName,
                                 ResponsibleUnitId = usedPartsLoanDetail.ResponsibleUnitId,
                                 ResponsibleUnitCode = usedPartsLoanDetail.ResponsibleUnitCode,
                                 ResponsibleUnitName = usedPartsLoanDetail.ResponsibleUnitName,
                                 FaultyPartsId = usedPartsLoanDetail.FaultyPartsId,
                                 FaultyPartsCode = usedPartsLoanDetail.FaultyPartsCode,
                                 FaultyPartsName = usedPartsLoanDetail.FaultyPartsName,
                                 UsedPartsSupplierId = usedPartsLoanDetail.UsedPartsSupplierId,
                                 UsedPartsSupplierCode = usedPartsLoanDetail.UsedPartsSupplierCode,
                                 UsedPartsSupplierName = usedPartsLoanDetail.UsedPartsSupplierName,
                                 SourceId = sourceid,
                             };
                    break;
                case (int)DcsUsedPartsOutboundOrderOutboundType.加工领料出库:
                    var usedPartsRefitReqDetails = ObjectContext.UsedPartsRefitReqDetails.Where(r => r.UsedPartsRefitBillId == sourceId);
                    IEnumerable<UsedPartsWarehouseArea> usedPartsRefitReqDetailWarehouseAreas = ObjectContext.UsedPartsWarehouseAreas;
                    if(canOutbound) {
                        usedPartsRefitReqDetails = usedPartsRefitReqDetails.Where(r => r.OutboundAmount < r.ConfirmedAmount);
                        usedPartsRefitReqDetailWarehouseAreas = ObjectContext.UsedPartsWarehouseAreas.Where(r => ObjectContext.UsedPartsWarehouseManagers.Any(usedPartsWarehouseManager => usedPartsWarehouseManager.PersonnelId == personnelId && usedPartsWarehouseManager.UsedPartsWarehouseAreaId == r.TopLevelUsedPartsWhseAreaId) && r.Status == (int)DcsBaseDataStatus.有效);
                    }
                    result = from usedPartsRefitReqDetail in usedPartsRefitReqDetails
                             from usedPartsStock in ObjectContext.UsedPartsStocks.Where(r => r.UsedPartsWarehouseId == usedPartsWarehouseId)
                             from usedPartsWarehouseArea in usedPartsRefitReqDetailWarehouseAreas
                             where usedPartsRefitReqDetail.UsedPartsBarCode == usedPartsStock.UsedPartsBarCode
                             && usedPartsWarehouseArea.Id == usedPartsStock.UsedPartsWarehouseAreaId
                             select new VirtualUsedPartsOutboundPlanDetail {
                                 UsedPartsBarCode = usedPartsRefitReqDetail.UsedPartsBarCode,
                                 UsedPartsCode = usedPartsRefitReqDetail.UsedPartsCode,
                                 UsedPartsName = usedPartsRefitReqDetail.UsedPartsName,
                                 UsedPartsWarehouseAreaCode = usedPartsWarehouseArea.Code,
                                 PlannedAmount = usedPartsRefitReqDetail.PlannedAmount,
                                 ConfirmedAmount = usedPartsRefitReqDetail.ConfirmedAmount,
                                 OutboundAmount = usedPartsRefitReqDetail.OutboundAmount,
                                 UsedPartsSerialNumber = usedPartsRefitReqDetail.UsedPartsSerialNumber,
                                 UsedPartsBatchNumber = usedPartsRefitReqDetail.UsedPartsBatchNumber,
                                 Remark = null,
                                 UsedPartsOutboundOrderId = null,
                                 UsedPartsId = usedPartsStock.UsedPartsId,
                                 UsedPartsWarehouseAreaId = usedPartsStock.UsedPartsWarehouseAreaId,
                                 SettlementPrice = usedPartsRefitReqDetail.Price,
                                 CostPrice = usedPartsStock.CostPrice,
                                 BranchId = usedPartsRefitReqDetail.BranchId,
                                 ClaimBillId = 0,
                                 ClaimBillCode = null,
                                 ClaimBillType = 0,
                                 UsedPartsSupplierId = 0,
                                 UsedPartsSupplierCode = null,
                                 UsedPartsSupplierName = null,
                                 IfFaultyParts = null,
                                 FaultyPartsCode = null,
                                 FaultyPartsName = null,
                                 FaultyPartsSupplierId = null,
                                 FaultyPartsSupplierCode = null,
                                 FaultyPartsSupplierName = null,
                                 ResponsibleUnitId = null,
                                 ResponsibleUnitCode = null,
                                 SourceId = sourceid,
                             };
                    break;
                case (int)DcsUsedPartsOutboundOrderOutboundType.旧件处理出库:
                    var usedPartsDisposalDetails = ObjectContext.UsedPartsDisposalDetails.Where(r => r.UsedPartsDisposalBillId == sourceId);
                    IEnumerable<UsedPartsWarehouseArea> usedPartsDisposalDetailWarehouseAreas = ObjectContext.UsedPartsWarehouseAreas;
                    if(canOutbound) {
                        usedPartsDisposalDetails = usedPartsDisposalDetails.Where(r => r.OutboundAmount < r.ConfirmedAmount);
                        usedPartsDisposalDetailWarehouseAreas = ObjectContext.UsedPartsWarehouseAreas.Where(r => ObjectContext.UsedPartsWarehouseManagers.Any(usedPartsWarehouseManager => usedPartsWarehouseManager.PersonnelId == personnelId && usedPartsWarehouseManager.UsedPartsWarehouseAreaId == r.TopLevelUsedPartsWhseAreaId) && r.Status == (int)DcsBaseDataStatus.有效);
                    }
                    result = from usedPartsDisposalDetail in usedPartsDisposalDetails
                             from usedPartsStock in ObjectContext.UsedPartsStocks.Where(r => r.UsedPartsWarehouseId == usedPartsWarehouseId)
                             from usedPartsWarehouseArea in usedPartsDisposalDetailWarehouseAreas
                             where usedPartsDisposalDetail.UsedPartsBarCode == usedPartsStock.UsedPartsBarCode
                             && usedPartsWarehouseArea.Id == usedPartsStock.UsedPartsWarehouseAreaId
                             select new VirtualUsedPartsOutboundPlanDetail {
                                 UsedPartsBarCode = usedPartsDisposalDetail.UsedPartsBarCode,
                                 UsedPartsCode = usedPartsDisposalDetail.UsedPartsCode,
                                 UsedPartsName = usedPartsDisposalDetail.UsedPartsName,
                                 UsedPartsWarehouseAreaCode = usedPartsWarehouseArea.Code,
                                 PlannedAmount = usedPartsDisposalDetail.PlannedAmount,
                                 ConfirmedAmount = usedPartsDisposalDetail.ConfirmedAmount,
                                 OutboundAmount = usedPartsDisposalDetail.OutboundAmount,
                                 UsedPartsSerialNumber = usedPartsDisposalDetail.UsedPartsSerialNumber,
                                 UsedPartsBatchNumber = usedPartsDisposalDetail.UsedPartsBatchNumber,
                                 Remark = null,
                                 UsedPartsOutboundOrderId = null,
                                 UsedPartsId = usedPartsStock.UsedPartsId,
                                 UsedPartsWarehouseAreaId = usedPartsStock.UsedPartsWarehouseAreaId,
                                 SettlementPrice = usedPartsDisposalDetail.Price,
                                 CostPrice = usedPartsStock.CostPrice,
                                 BranchId = usedPartsDisposalDetail.BranchId,
                                 ClaimBillId = usedPartsDisposalDetail.ClaimBillId,
                                 ClaimBillType = usedPartsDisposalDetail.ClaimBillType,
                                 ClaimBillCode = usedPartsDisposalDetail.ClaimBillCode,
                                 IfFaultyParts = usedPartsDisposalDetail.IfFaultyParts,
                                 FaultyPartsSupplierId = usedPartsDisposalDetail.FaultyPartsSupplierId,
                                 FaultyPartsSupplierCode = usedPartsDisposalDetail.FaultyPartsSupplierCode,
                                 FaultyPartsSupplierName = usedPartsDisposalDetail.FaultyPartsSupplierName,
                                 ResponsibleUnitId = usedPartsDisposalDetail.ResponsibleUnitId,
                                 ResponsibleUnitCode = usedPartsDisposalDetail.ResponsibleUnitCode,
                                 ResponsibleUnitName = usedPartsDisposalDetail.ResponsibleUnitName,
                                 FaultyPartsId = usedPartsDisposalDetail.FaultyPartsId,
                                 FaultyPartsCode = usedPartsDisposalDetail.FaultyPartsCode,
                                 FaultyPartsName = usedPartsDisposalDetail.FaultyPartsName,
                                 UsedPartsSupplierId = usedPartsDisposalDetail.UsedPartsSupplierId,
                                 UsedPartsSupplierCode = usedPartsDisposalDetail.UsedPartsSupplierCode,
                                 UsedPartsSupplierName = usedPartsDisposalDetail.UsedPartsSupplierName,
                                 SourceId = sourceid,
                             };
                    break;
                case (int)DcsUsedPartsOutboundOrderOutboundType.调拨出库:
                    var usedPartsTransferDetails = ObjectContext.UsedPartsTransferDetails.Where(r => r.UsedPartsTransferOrderId == sourceId);
                    IEnumerable<UsedPartsWarehouseArea> usedPartsTransferDetailWarehouseAreas = ObjectContext.UsedPartsWarehouseAreas;
                    if(canOutbound) {
                        usedPartsTransferDetails = usedPartsTransferDetails.Where(r => r.OutboundAmount < r.InboundAmount);
                        //usedPartsTransferDetails = usedPartsTransferDetails.Where(r => r.OutboundAmount < r.ConfirmedAmount);
                        usedPartsTransferDetailWarehouseAreas = ObjectContext.UsedPartsWarehouseAreas.Where(r => ObjectContext.UsedPartsWarehouseManagers.Any(usedPartsWarehouseManager => usedPartsWarehouseManager.PersonnelId == personnelId && usedPartsWarehouseManager.UsedPartsWarehouseAreaId == r.TopLevelUsedPartsWhseAreaId) && r.Status == (int)DcsBaseDataStatus.有效);
                    }
                    result = from usedPartsTransferDetail in usedPartsTransferDetails
                             from usedPartsStock in ObjectContext.UsedPartsStocks.Where(r => r.UsedPartsWarehouseId == usedPartsWarehouseId)
                             from usedPartsWarehouseArea in usedPartsTransferDetailWarehouseAreas
                             where usedPartsTransferDetail.UsedPartsBarCode == usedPartsStock.UsedPartsBarCode
                             && usedPartsWarehouseArea.Id == usedPartsStock.UsedPartsWarehouseAreaId
                             select new VirtualUsedPartsOutboundPlanDetail {
                                 UsedPartsBarCode = usedPartsTransferDetail.UsedPartsBarCode,
                                 UsedPartsCode = usedPartsTransferDetail.UsedPartsCode,
                                 UsedPartsName = usedPartsTransferDetail.UsedPartsName,
                                 UsedPartsWarehouseAreaCode = usedPartsWarehouseArea.Code,
                                 PlannedAmount = usedPartsTransferDetail.PlannedAmount,
                                 ConfirmedAmount = usedPartsTransferDetail.ConfirmedAmount,
                                 OutboundAmount = usedPartsTransferDetail.OutboundAmount,
                                 UsedPartsSerialNumber = usedPartsTransferDetail.UsedPartsSerialNumber,
                                 UsedPartsBatchNumber = usedPartsTransferDetail.UsedPartsBatchNumber,
                                 Remark = null,
                                 UsedPartsOutboundOrderId = null,
                                 UsedPartsId = usedPartsStock.UsedPartsId,
                                 UsedPartsWarehouseAreaId = usedPartsStock.UsedPartsWarehouseAreaId,
                                 SettlementPrice = usedPartsTransferDetail.Price,
                                 CostPrice = usedPartsStock.CostPrice,
                                 BranchId = usedPartsTransferDetail.BranchId,
                                 ClaimBillId = usedPartsTransferDetail.ClaimBillId ?? 0,
                                 ClaimBillType = usedPartsTransferDetail.ClaimBillType ?? 0,
                                 ClaimBillCode = usedPartsTransferDetail.ClaimBillCode,
                                 IfFaultyParts = usedPartsTransferDetail.IfFaultyParts,
                                 FaultyPartsSupplierId = usedPartsTransferDetail.FaultyPartsSupplierId,
                                 FaultyPartsSupplierCode = usedPartsTransferDetail.FaultyPartsSupplierCode,
                                 FaultyPartsSupplierName = usedPartsTransferDetail.FaultyPartsSupplierName,
                                 ResponsibleUnitId = usedPartsTransferDetail.ResponsibleUnitId,
                                 ResponsibleUnitCode = usedPartsTransferDetail.ResponsibleUnitCode,
                                 ResponsibleUnitName = usedPartsTransferDetail.ResponsibleUnitName,
                                 FaultyPartsId = usedPartsTransferDetail.FaultyPartsId,
                                 FaultyPartsCode = usedPartsTransferDetail.FaultyPartsCode,
                                 FaultyPartsName = usedPartsTransferDetail.FaultyPartsName,
                                 UsedPartsSupplierId = usedPartsTransferDetail.UsedPartsSupplierId ?? 0,
                                 UsedPartsSupplierCode = usedPartsTransferDetail.UsedPartsSupplierCode,
                                 UsedPartsSupplierName = usedPartsTransferDetail.UsedPartsSupplierName,
                                 SourceId = sourceid,
                             };
                    break;
                case (int)DcsUsedPartsOutboundOrderOutboundType.清退供应商出库:
                    var usedPartsReturnDetails = ObjectContext.UsedPartsReturnDetails.Where(r => r.UsedPartsReturnOrderId == sourceId);
                    IEnumerable<UsedPartsWarehouseArea> usedPartsReturnDetailWarehouseAreas = ObjectContext.UsedPartsWarehouseAreas;
                    if(canOutbound) {
                        usedPartsReturnDetails = usedPartsReturnDetails.Where(r => r.OutboundAmount < r.ConfirmedAmount);
                        usedPartsReturnDetailWarehouseAreas = ObjectContext.UsedPartsWarehouseAreas.Where(r => ObjectContext.UsedPartsWarehouseManagers.Any(usedPartsWarehouseManager => usedPartsWarehouseManager.PersonnelId == personnelId && usedPartsWarehouseManager.UsedPartsWarehouseAreaId == r.TopLevelUsedPartsWhseAreaId) && r.Status == (int)DcsBaseDataStatus.有效);
                    }
                    result = from usedPartsReturnDetail in usedPartsReturnDetails
                             from usedPartsStock in ObjectContext.UsedPartsStocks.Where(r => r.UsedPartsWarehouseId == usedPartsWarehouseId)
                             from usedPartsWarehouseArea in usedPartsReturnDetailWarehouseAreas
                             where usedPartsReturnDetail.UsedPartsBarCode == usedPartsStock.UsedPartsBarCode
                             && usedPartsWarehouseArea.Id == usedPartsStock.UsedPartsWarehouseAreaId
                             select new VirtualUsedPartsOutboundPlanDetail {
                                 UsedPartsBarCode = usedPartsReturnDetail.UsedPartsBarCode,
                                 UsedPartsCode = usedPartsReturnDetail.UsedPartsCode,
                                 UsedPartsName = usedPartsReturnDetail.UsedPartsName,
                                 UsedPartsWarehouseAreaCode = usedPartsWarehouseArea.Code,
                                 PlannedAmount = usedPartsReturnDetail.PlannedAmount,
                                 ConfirmedAmount = usedPartsReturnDetail.ConfirmedAmount,
                                 OutboundAmount = usedPartsReturnDetail.OutboundAmount,
                                 UsedPartsSerialNumber = usedPartsReturnDetail.UsedPartsSerialNumber,
                                 UsedPartsBatchNumber = usedPartsReturnDetail.UsedPartsBatchNumber,
                                 Remark = null,
                                 UsedPartsOutboundOrderId = null,
                                 UsedPartsId = usedPartsStock.UsedPartsId,
                                 UsedPartsWarehouseAreaId = usedPartsStock.UsedPartsWarehouseAreaId,
                                 SettlementPrice = usedPartsReturnDetail.UnitPrice,
                                 CostPrice = usedPartsStock.CostPrice,
                                 BranchId = usedPartsReturnDetail.BranchId,
                                 ClaimBillId = usedPartsReturnDetail.ClaimBillId,
                                 ClaimBillType = usedPartsReturnDetail.ClaimBillType,
                                 ClaimBillCode = usedPartsReturnDetail.ClaimBillCode,
                                 IfFaultyParts = usedPartsReturnDetail.IfFaultyParts,
                                 FaultyPartsSupplierId = usedPartsReturnDetail.FaultyPartsSupplierId,
                                 FaultyPartsSupplierCode = usedPartsReturnDetail.FaultyPartsSupplierCode,
                                 FaultyPartsSupplierName = usedPartsReturnDetail.FaultyPartsSupplierName,
                                 ResponsibleUnitId = usedPartsReturnDetail.ResponsibleUnitId,
                                 ResponsibleUnitCode = usedPartsReturnDetail.ResponsibleUnitCode,
                                 ResponsibleUnitName = usedPartsReturnDetail.ResponsibleUnitName,
                                 FaultyPartsId = usedPartsReturnDetail.FaultyPartsId,
                                 FaultyPartsCode = usedPartsReturnDetail.FaultyPartsCode,
                                 FaultyPartsName = usedPartsReturnDetail.FaultyPartsName,
                                 UsedPartsSupplierId = usedPartsReturnDetail.UsedPartsSupplierId ?? 0,
                                 UsedPartsSupplierCode = usedPartsReturnDetail.UsedPartsSupplierCode,
                                 UsedPartsSupplierName = usedPartsReturnDetail.UsedPartsSupplierName,
                                 SourceId = sourceid,
                             };
                    break;
            }
            return result.OrderBy(r => r.UsedPartsBarCode);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:30
        //原分布类的函数全部转移到Ach                                                               

        public IEnumerable<VirtualUsedPartsOutboundPlanDetail> 查询虚拟旧件出库清单(int outboundType, int sourceId, bool canOutbound, int personnelId, int usedPartsWarehouseId) {
            return new VirtualUsedPartsOutboundPlanDetailAch(this).查询虚拟旧件出库清单(outboundType,sourceId,canOutbound,personnelId,usedPartsWarehouseId);
        }
    }
}
