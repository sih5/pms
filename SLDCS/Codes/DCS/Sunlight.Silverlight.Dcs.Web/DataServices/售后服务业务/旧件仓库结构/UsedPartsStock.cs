﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class UsedPartsStockAch : DcsSerivceAchieveBase {
        public UsedPartsStockAch(DcsDomainService domainService)
            : base(domainService) {
        }

        internal void InsertUsedPartsStockValidate(UsedPartsStock usedPartsStock) {
            var dbusedPartsStock = ObjectContext.UsedPartsStocks.Where(r => r.UsedPartsWarehouseId == usedPartsStock.UsedPartsWarehouseId && r.UsedPartsWarehouseAreaId == usedPartsStock.UsedPartsWarehouseAreaId && r.UsedPartsBarCode == usedPartsStock.UsedPartsBarCode).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbusedPartsStock != null)
                throw new ValidationException(string.Format(ErrorStrings.UsedPartsStock_Validation1, usedPartsStock.UsedPartsBarCode));
            var userInfo = Utils.GetCurrentUserInfo();
            usedPartsStock.CreatorId = userInfo.Id;
            usedPartsStock.CreatorName = userInfo.Name;
            usedPartsStock.CreateTime = DateTime.Now;
        }
        internal void UpdateUsedPartsStockValidate(UsedPartsStock usedPartsStock) {
            var dbusedPartsStock = ObjectContext.UsedPartsStocks.Where(r => r.Id != usedPartsStock.Id && r.UsedPartsWarehouseId == usedPartsStock.UsedPartsWarehouseId && r.UsedPartsWarehouseAreaId == usedPartsStock.UsedPartsWarehouseAreaId && r.UsedPartsBarCode == usedPartsStock.UsedPartsBarCode).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbusedPartsStock != null)
                throw new ValidationException(string.Format(ErrorStrings.UsedPartsStock_Validation1, usedPartsStock.UsedPartsBarCode));
            var userInfo = Utils.GetCurrentUserInfo();
            usedPartsStock.ModifierId = userInfo.Id;
            usedPartsStock.ModifierName = userInfo.Name;
            usedPartsStock.ModifyTime = DateTime.Now;
        }
        public void InsertUsedPartsStock(UsedPartsStock usedPartsStock) {
            InsertToDatabase(usedPartsStock);
            this.InsertUsedPartsStockValidate(usedPartsStock);
        }
        public void UpdateUsedPartsStock(UsedPartsStock usedPartsStock) {
            UpdateToDatabase(usedPartsStock);
            this.UpdateUsedPartsStockValidate(usedPartsStock);
        }
        public IQueryable<UsedPartsStock> GetUsedPartsStocksWithDetails() {
            return ObjectContext.UsedPartsStocks.Include("UsedPartsWarehouseArea").Include("UsedPartsWarehouse").OrderBy(e => e.Id);
        }
        public IQueryable<UsedPartsStock> GetUsedPartsStocksWithDetailsByclaimBillCreateTime(DateTime? claimBillCreateTimeBegin, DateTime? claimBillCreateTimeEnd) {
            //if(claimBillCreateTimeBegin.HasValue && claimBillCreateTimeEnd.HasValue) {
            //    var repairClaimBillCode = this.ObjectContext.RepairClaimBills.Where(t => t.CreateTime >= claimBillCreateTimeBegin && t.CreateTime <= claimBillCreateTimeEnd).Select(t => t.ClaimBillCode);
            //    return ObjectContext.UsedPartsStocks.Where(t => repairClaimBillCode.Contains(t.ClaimBillCode)).Include("UsedPartsWarehouseArea").Include("UsedPartsWarehouse").OrderBy(e => e.Id);
            //}
            //if(claimBillCreateTimeBegin.HasValue) {
            //    var repairClaimBillCode = this.ObjectContext.RepairClaimBills.Where(t => t.CreateTime >= claimBillCreateTimeBegin).Select(t => t.ClaimBillCode);
            //    return ObjectContext.UsedPartsStocks.Where(t => repairClaimBillCode.Contains(t.ClaimBillCode)).Include("UsedPartsWarehouseArea").Include("UsedPartsWarehouse").OrderBy(e => e.Id);
            //}
            //if(claimBillCreateTimeEnd.HasValue) {
            //    var repairClaimBillCode = this.ObjectContext.RepairClaimBills.Where(t => t.CreateTime <= claimBillCreateTimeEnd).Select(t => t.ClaimBillCode);
            //    return ObjectContext.UsedPartsStocks.Where(t => repairClaimBillCode.Contains(t.ClaimBillCode)).Include("UsedPartsWarehouseArea").Include("UsedPartsWarehouse").OrderBy(e => e.Id);
            //}
            return ObjectContext.UsedPartsStocks.Include("UsedPartsWarehouseArea").Include("UsedPartsWarehouse").OrderBy(e => e.Id);
        }
        public IQueryable<UsedPartsStock> GetUsedPartsStocksWithDetailsByStatus() {
            return ObjectContext.UsedPartsStocks.Include("UsedPartsWarehouseArea").Include("UsedPartsWarehouse").OrderBy(e => e.Id);
        }
        public IQueryable<UsedPartsStock> GetUsedPartsStocksByUsedPartsStockId(int id) {
            return ObjectContext.UsedPartsStocks.Where(e => e.Id == id).OrderBy(e => e.Id);
        }

        public IQueryable<VirtualUsedPartsStock> 查询旧件库存修改供应商() {
            //var result = from usedPartsStock in ObjectContext.UsedPartsStocks
            //             from repairClaimBill in ObjectContext.RepairClaimBills
            //             from usedPartsShippingDetail in ObjectContext.UsedPartsShippingDetails
            //             from usedPartsShippingOrder in ObjectContext.UsedPartsShippingOrders
            //             where repairClaimBill.Id == usedPartsStock.ClaimBillId && usedPartsShippingDetail.UsedPartsBarCode == usedPartsStock.UsedPartsBarCode && usedPartsShippingOrder.Id == usedPartsShippingDetail.UsedPartsShippingOrderId
            //             select new VirtualUsedPartsStock {
            //                 UsedPartsId = usedPartsStock.UsedPartsId, //旧件配件Id
            //                 UsedPartsStockId = usedPartsStock.Id,
            //                 FaultyPartsSupplierId = usedPartsStock.FaultyPartsSupplierId, //祸首件供应商Id
            //                 FaultyPartsSupplierCode = usedPartsStock.FaultyPartsSupplierCode, //祸首件供应商编号
            //                 FaultyPartsSupplierName = usedPartsStock.FaultyPartsSupplierName, //祸首件供应商名称
            //                 UsedPartsName = usedPartsStock.UsedPartsName, //旧件配件名称
            //                 UsedPartsCode = usedPartsStock.UsedPartsCode, //旧件配件编号
            //                 UsedPartsBarCode = usedPartsStock.UsedPartsBarCode, //旧件条码
            //                 ShippingOrderCode = usedPartsShippingOrder.Code, //旧件库存.旧件发运单清单.旧件发运单.发运单编号
            //                 StorageQuantity = usedPartsStock.StorageQuantity, //库存数量
            //                 SettlementPrice = usedPartsStock.SettlementPrice, //结算价格
            //                 IfFaultyParts = usedPartsStock.IfFaultyParts, //是否祸首件
            //                 ClaimBillCode = usedPartsStock.ClaimBillCode, //索赔单编号
            //                 ClaimBillCreateTime = repairClaimBill.CreateTime,//索赔单创建时间
            //                 DealerName = repairClaimBill.DealerName, //维修保养索赔单.服务站编号
            //                 ResponsibleUnitCode = usedPartsStock.ResponsibleUnitCode,
            //                 ResponsibleUnitName = usedPartsStock.ResponsibleUnitName,
            //                 CreateTime = usedPartsShippingOrder.CreateTime //旧件库存.旧件发运单清单.旧件发运单.创建时间
            //             };
            //return result.OrderBy(r => r.UsedPartsId);
            return null;
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:30
        //原分布类的函数全部转移到Ach                                                               

        public void InsertUsedPartsStock(UsedPartsStock usedPartsStock) {
            new UsedPartsStockAch(this).InsertUsedPartsStock(usedPartsStock);
        }

        public void UpdateUsedPartsStock(UsedPartsStock usedPartsStock) {
            new UsedPartsStockAch(this).UpdateUsedPartsStock(usedPartsStock);
        }

        public IQueryable<UsedPartsStock> GetUsedPartsStocksWithDetails() {
            return new UsedPartsStockAch(this).GetUsedPartsStocksWithDetails();
        }

        public IQueryable<UsedPartsStock> GetUsedPartsStocksWithDetailsByclaimBillCreateTime(DateTime? claimBillCreateTimeBegin, DateTime? claimBillCreateTimeEnd) {
            return new UsedPartsStockAch(this).GetUsedPartsStocksWithDetailsByclaimBillCreateTime(claimBillCreateTimeBegin,claimBillCreateTimeEnd);
        }

        public IQueryable<UsedPartsStock> GetUsedPartsStocksWithDetailsByStatus() {
            return new UsedPartsStockAch(this).GetUsedPartsStocksWithDetailsByStatus();
        }

        public IQueryable<UsedPartsStock> GetUsedPartsStocksByUsedPartsStockId(int id) {
            return new UsedPartsStockAch(this).GetUsedPartsStocksByUsedPartsStockId(id);
        }

        public IQueryable<VirtualUsedPartsStock> 查询旧件库存修改供应商() {
            return new UsedPartsStockAch(this).查询旧件库存修改供应商();
        }
    }
}
