﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class UsedPartsWarehouseAch : DcsSerivceAchieveBase {
        internal void InsertUsedPartsWarehouseValidate(UsedPartsWarehouse usedPartsWarehouse) {
            var dbusedPartsWarehouse = ObjectContext.UsedPartsWarehouses.Where(r => r.Code.ToLower() == usedPartsWarehouse.Code.ToLower() && r.Status != (int)DcsBaseDataStatus.作废).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbusedPartsWarehouse != null)
                throw new ValidationException(string.Format(ErrorStrings.UsedPartsWarehouse_Validation1, usedPartsWarehouse.Code));
            var userInfo = Utils.GetCurrentUserInfo();
            usedPartsWarehouse.CreatorId = userInfo.Id;
            usedPartsWarehouse.CreatorName = userInfo.Name;
            usedPartsWarehouse.CreateTime = DateTime.Now;
        }
        internal void UpdateUsedPartsWarehouseValidate(UsedPartsWarehouse usedPartsWarehouse) {
            var dbusedPartsWarehouse = ObjectContext.UsedPartsWarehouses.Where(r => r.Id != usedPartsWarehouse.Id && r.Code.ToLower() == usedPartsWarehouse.Code.ToLower() && r.Status != (int)DcsBaseDataStatus.作废).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbusedPartsWarehouse != null)
                throw new ValidationException(string.Format(ErrorStrings.UsedPartsWarehouse_Validation1, usedPartsWarehouse.Code));
            var userInfo = Utils.GetCurrentUserInfo();
            usedPartsWarehouse.ModifierId = userInfo.Id;
            usedPartsWarehouse.ModifierName = userInfo.Name;
            usedPartsWarehouse.ModifyTime = DateTime.Now;
        }
        public void InsertUsedPartsWarehouse(UsedPartsWarehouse usedPartsWarehouse) {
            InsertToDatabase(usedPartsWarehouse);
            this.InsertUsedPartsWarehouseValidate(usedPartsWarehouse);
        }
        public void UpdateUsedPartsWarehouse(UsedPartsWarehouse usedPartsWarehouse) {
            UpdateToDatabase(usedPartsWarehouse);
            this.UpdateUsedPartsWarehouseValidate(usedPartsWarehouse);
        }
        public IQueryable<UsedPartsWarehouse> GetUsedPartsWarehousesWithDetails() {
            return ObjectContext.UsedPartsWarehouses.Include("UsedPartsWarehouseStaffs.Personnel").OrderBy(e => e.Id);
        }
        public IQueryable<UsedPartsWarehouse> GetUsedPartsWarehousesWithPartsSalesCategorys() {
            //return ObjectContext.UsedPartsWarehouses.Include("PartsSalesCategory").OrderBy(e => e.Id);
            var userInfo = Utils.GetCurrentUserInfo();
            return ObjectContext.UsedPartsWarehouses.Where(r => r.BranchId == userInfo.EnterpriseId).Include("PartsSalesCategory").OrderBy(e => e.Id);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:30
        //原分布类的函数全部转移到Ach                                                               

        public void InsertUsedPartsWarehouse(UsedPartsWarehouse usedPartsWarehouse) {
            new UsedPartsWarehouseAch(this).InsertUsedPartsWarehouse(usedPartsWarehouse);
        }

        public void UpdateUsedPartsWarehouse(UsedPartsWarehouse usedPartsWarehouse) {
            new UsedPartsWarehouseAch(this).UpdateUsedPartsWarehouse(usedPartsWarehouse);
        }

        public IQueryable<UsedPartsWarehouse> GetUsedPartsWarehousesWithDetails() {
            return new UsedPartsWarehouseAch(this).GetUsedPartsWarehousesWithDetails();
        }
                public IQueryable<UsedPartsWarehouse> GetUsedPartsWarehousesWithPartsSalesCategorys() {
            return new UsedPartsWarehouseAch(this).GetUsedPartsWarehousesWithPartsSalesCategorys();
        }
    }
}
