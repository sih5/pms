﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class UsedPartsWarehouseAreaAch : DcsSerivceAchieveBase {
        internal void InsertUsedPartsWarehouseAreaValidate(UsedPartsWarehouseArea usedPartsWarehouseArea) {
            var dbusedPartsWarehouseArea = ObjectContext.UsedPartsWarehouseAreas.Where(r => r.Code.ToLower() == usedPartsWarehouseArea.Code.ToLower()&&r.UsedPartsWarehouseId==usedPartsWarehouseArea.UsedPartsWarehouseId && r.Status != (int)DcsBaseDataStatus.作废).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbusedPartsWarehouseArea != null)
                throw new ValidationException(string.Format(ErrorStrings.UsedPartsWarehouseArea_Validation1, usedPartsWarehouseArea.Code));
            var userInfo = Utils.GetCurrentUserInfo();
            usedPartsWarehouseArea.CreatorId = userInfo.Id;
            usedPartsWarehouseArea.CreatorName = userInfo.Name;
            usedPartsWarehouseArea.CreateTime = DateTime.Now;
        }
        internal void UpdateUsedPartsWarehouseAreaValidate(UsedPartsWarehouseArea usedPartsWarehouseArea) {
            var dbusedPartsWarehouseArea = ObjectContext.UsedPartsWarehouseAreas.Where(r => r.Id != usedPartsWarehouseArea.Id && r.Code.ToLower() == usedPartsWarehouseArea.Code.ToLower() && r.UsedPartsWarehouseId == usedPartsWarehouseArea.UsedPartsWarehouseId && r.Status != (int)DcsBaseDataStatus.作废).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbusedPartsWarehouseArea != null)
                throw new ValidationException(string.Format(ErrorStrings.UsedPartsWarehouseArea_Validation1, usedPartsWarehouseArea.Code));
            var userInfo = Utils.GetCurrentUserInfo();
            usedPartsWarehouseArea.ModifierId = userInfo.Id;
            usedPartsWarehouseArea.ModifierName = userInfo.Name;
            usedPartsWarehouseArea.ModifyTime = DateTime.Now;
        }
        public void InsertUsedPartsWarehouseArea(UsedPartsWarehouseArea usedPartsWarehouseArea) {
            InsertToDatabase(usedPartsWarehouseArea);
            this.InsertUsedPartsWarehouseAreaValidate(usedPartsWarehouseArea);
        }
        public void UpdateUsedPartsWarehouseArea(UsedPartsWarehouseArea usedPartsWarehouseArea) {
            UpdateToDatabase(usedPartsWarehouseArea);
            this.UpdateUsedPartsWarehouseAreaValidate(usedPartsWarehouseArea);
        }
        public IQueryable<UsedPartsWarehouseArea> GetUsedPartsWarehouseAreasWithUsedPartsWarehouse(int branchId) {
            var userInfo = Utils.GetCurrentUserInfo();
            var usedPartsWarehouseStaffs = ObjectContext.UsedPartsWarehouseStaffs.Where(r => r.PersonnelId == userInfo.Id).Select(r => r.UsedPartsWarehouseId).ToArray();
            var usedPartsWarehouses = ObjectContext.UsedPartsWarehouses.Where(r => usedPartsWarehouseStaffs.Contains(r.Id) && r.Status == (int)DcsBaseDataStatus.有效 && r.BranchId == branchId).ToArray();
            if(usedPartsWarehouses.Any()) {
                var usedPartsWarehouseIds = usedPartsWarehouses.Select(e => e.Id);
                var usedPartsWarehouseArea = ObjectContext.UsedPartsWarehouseAreas.Where(e => usedPartsWarehouseIds.Contains(e.UsedPartsWarehouseId) && e.Status == (int)DcsBaseDataStatus.有效);
                return usedPartsWarehouseArea;
            }
            return null;
        }
        public IQueryable<UsedPartsWarehouseArea> GetUsedPartsWarehouseAreasWithDetailsAndParentArea() {
            return ObjectContext.UsedPartsWarehouseAreas.Include("UsedPartsWarehouse").Include("ParentUsedPartsWarehouseArea").OrderBy(entity => entity.Id);
        }
        public UsedPartsWarehouseArea GetUsedPartsWarehouseAreasWithAreaManagersAndDetails(int id) {
            var usedPartsWarehouseArea = ObjectContext.UsedPartsWarehouseAreas.SingleOrDefault(r => r.Id == id && r.Status == (int)DcsBaseDataStatus.有效);
            if(usedPartsWarehouseArea != null) {
                var usedPartsWarehouse = ObjectContext.UsedPartsWarehouses.SingleOrDefault(r => r.Id == usedPartsWarehouseArea.UsedPartsWarehouseId && r.Status == (int)DcsBaseDataStatus.有效);
                var personnel = ObjectContext.UsedPartsWarehouseManagers.Where(r => r.UsedPartsWarehouseAreaId == id).ToArray();
                if(personnel.Length > 0) {
                    var pIds = personnel.Select(r => r.PersonnelId).ToArray();
                    if(pIds.Length > 0) {
                        var personnels = ObjectContext.Personnels.Where(r => pIds.Contains(r.Id)).ToArray();
                    }
                }
            }
            return usedPartsWarehouseArea;
        }
        public IEnumerable<UsedPartsWarehouseArea> 根据上层节点获取下层节点旧件库区库位信息(int parentId) {
            var usedPartsWarehouseAreas = ObjectContext.UsedPartsWarehouseAreas.Where(r => r.ParentId == parentId && r.Status == (int)DcsBaseDataStatus.有效).ToArray();
            //IQueryable<UsedPartsWarehouseArea> query = ObjectContext.UsedPartsWarehouseAreas.Where(r => r.ParentId == parentId && r.Status == (int)DcsBaseDataStatus.有效);
            //var groups =DomainService. GetChildrenByparentId(parentId, query);
            //foreach(var usedPartsWarehouseArea in usedPartsWarehouseAreas) {
            //    var tempGroup = groups.FirstOrDefault(r => r.Id == usedPartsWarehouseArea.Id);
            //    if(tempGroup != null && tempGroup.ChildrenNum > 0) {
            //        usedPartsWarehouseArea.HasChildrenNode = true;
            //    }else {
            //        usedPartsWarehouseArea.HasChildrenNode = false;
            //    }
            //}
            return usedPartsWarehouseAreas.OrderBy(r => r.Id);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:30
        //原分布类的函数全部转移到Ach                                                               

        public void InsertUsedPartsWarehouseArea(UsedPartsWarehouseArea usedPartsWarehouseArea) {
            new UsedPartsWarehouseAreaAch(this).InsertUsedPartsWarehouseArea(usedPartsWarehouseArea);
        }

        public void UpdateUsedPartsWarehouseArea(UsedPartsWarehouseArea usedPartsWarehouseArea) {
            new UsedPartsWarehouseAreaAch(this).UpdateUsedPartsWarehouseArea(usedPartsWarehouseArea);
        }

        public IQueryable<UsedPartsWarehouseArea> GetUsedPartsWarehouseAreasWithUsedPartsWarehouse(int branchId) {
            return new UsedPartsWarehouseAreaAch(this).GetUsedPartsWarehouseAreasWithUsedPartsWarehouse(branchId);
        }

        public IQueryable<UsedPartsWarehouseArea> GetUsedPartsWarehouseAreasWithDetailsAndParentArea() {
            return new UsedPartsWarehouseAreaAch(this).GetUsedPartsWarehouseAreasWithDetailsAndParentArea();
        }

        public UsedPartsWarehouseArea GetUsedPartsWarehouseAreasWithAreaManagersAndDetails(int id) {
            return new UsedPartsWarehouseAreaAch(this).GetUsedPartsWarehouseAreasWithAreaManagersAndDetails(id);
        }

        public IEnumerable<UsedPartsWarehouseArea> 根据上层节点获取下层节点旧件库区库位信息(int parentId) {
            return new UsedPartsWarehouseAreaAch(this).根据上层节点获取下层节点旧件库区库位信息(parentId);
        }
    }
}
