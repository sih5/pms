﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class UsedPartsWarehouseManagerAch : DcsSerivceAchieveBase {
        public UsedPartsWarehouseManagerAch(DcsDomainService domainService)
            : base(domainService) {
        }

        internal void InsertUsedPartsWarehouseManagerValidate(UsedPartsWarehouseManager usedPartsWarehouseManager) {
            var dbusedPartsWarehouseManager = ObjectContext.UsedPartsWarehouseManagers.Where(r => r.UsedPartsWarehouseAreaId == usedPartsWarehouseManager.UsedPartsWarehouseAreaId && r.PersonnelId == usedPartsWarehouseManager.PersonnelId).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbusedPartsWarehouseManager != null)
                throw new ValidationException(ErrorStrings.UsedPartsWarehouseManager_Validation1);
            var userInfo = Utils.GetCurrentUserInfo();
            usedPartsWarehouseManager.CreatorId = userInfo.Id;
            usedPartsWarehouseManager.CreatorName = userInfo.Name;
            usedPartsWarehouseManager.CreateTime = DateTime.Now;
        }
        public void InsertUsedPartsWarehouseManager(UsedPartsWarehouseManager usedPartsWarehouseManager) {
            InsertToDatabase(usedPartsWarehouseManager);
            this.InsertUsedPartsWarehouseManagerValidate(usedPartsWarehouseManager);
        }
        public void UpdateUsedPartsWarehouseManager(UsedPartsWarehouseManager usedPartsWarehouseManager) {
            //客户端Grid编辑界面，作为非强绑定清单
            //Remove操作为修改关联字段，必须提供Edit操作
        }
        public void DeleteUsedPartsWarehouseManager(UsedPartsWarehouseManager usedPartsWarehouseManager) {
            DeleteFromDatabase(usedPartsWarehouseManager);
        }
        public IQueryable<UsedPartsWarehouseManager> GetUsedPartsWarehouseManagersWithPersonnel() {
            return ObjectContext.UsedPartsWarehouseManagers.Include("Personnel").OrderBy(entity => entity.Id);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:30
        //原分布类的函数全部转移到Ach                                                               

        public void InsertUsedPartsWarehouseManager(UsedPartsWarehouseManager usedPartsWarehouseManager) {
            new UsedPartsWarehouseManagerAch(this).InsertUsedPartsWarehouseManager(usedPartsWarehouseManager);
        }

        public void UpdateUsedPartsWarehouseManager(UsedPartsWarehouseManager usedPartsWarehouseManager) {
            new UsedPartsWarehouseManagerAch(this).UpdateUsedPartsWarehouseManager(usedPartsWarehouseManager);
        }

        public void DeleteUsedPartsWarehouseManager(UsedPartsWarehouseManager usedPartsWarehouseManager) {
            new UsedPartsWarehouseManagerAch(this).DeleteUsedPartsWarehouseManager(usedPartsWarehouseManager);
        }

        public IQueryable<UsedPartsWarehouseManager> GetUsedPartsWarehouseManagersWithPersonnel() {
            return new UsedPartsWarehouseManagerAch(this).GetUsedPartsWarehouseManagersWithPersonnel();
        }
    }
}
