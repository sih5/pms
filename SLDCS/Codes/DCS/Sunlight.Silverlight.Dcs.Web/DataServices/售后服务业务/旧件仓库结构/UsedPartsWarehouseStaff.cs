﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class UsedPartsWarehouseStaffAch : DcsSerivceAchieveBase {
        public UsedPartsWarehouseStaffAch(DcsDomainService domainService)
            : base(domainService) {
        }

        internal void InsertUsedPartsWarehouseStaffValidate(UsedPartsWarehouseStaff usedPartsWarehouseStaff) {
            var dbusedPartsWarehouseStaff = ObjectContext.UsedPartsWarehouseStaffs.Where(r => r.UsedPartsWarehouseId == usedPartsWarehouseStaff.UsedPartsWarehouseId && r.PersonnelId == usedPartsWarehouseStaff.PersonnelId).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbusedPartsWarehouseStaff != null)
                throw new ValidationException(ErrorStrings.UsedPartsWarehouseStaff_Validation1);
            var userInfo = Utils.GetCurrentUserInfo();
            usedPartsWarehouseStaff.CreatorId = userInfo.Id;
            usedPartsWarehouseStaff.CreatorName = userInfo.Name;
            usedPartsWarehouseStaff.CreateTime = DateTime.Now;
        }
        public void InsertUsedPartsWarehouseStaff(UsedPartsWarehouseStaff usedPartsWarehouseStaff) {
            InsertToDatabase(usedPartsWarehouseStaff);
            this.InsertUsedPartsWarehouseStaffValidate(usedPartsWarehouseStaff);
        }
        public void UpdateUsedPartsWarehouseStaff(UsedPartsWarehouseStaff usedPartsWarehouseStaff) {
            //客户端Grid编辑界面，作为非强绑定清单
            //Remove操作为修改关联字段，必须提供Edit操作
        }
        public void DeleteUsedPartsWarehouseStaff(UsedPartsWarehouseStaff usedPartsWarehouseStaff) {
            DeleteFromDatabase(usedPartsWarehouseStaff);
        }
        public IQueryable<UsedPartsWarehouseStaff> GetUsedPartsWarehouseStaffsWithPersonnel() {
            return ObjectContext.UsedPartsWarehouseStaffs.Include("Personnel").OrderBy(e => e.Id);
        }
        public IQueryable<UsedPartsWarehouseStaff> GetUsedPartsWarehouseStaffByPersonnelId() {
            return ObjectContext.UsedPartsWarehouseStaffs.Include("UsedPartsWarehouse").Where(e => e.UsedPartsWarehouse.Status == (int)DcsBaseDataStatus.有效).OrderBy(e => e.Id);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:30
        //原分布类的函数全部转移到Ach                                                               

        public void InsertUsedPartsWarehouseStaff(UsedPartsWarehouseStaff usedPartsWarehouseStaff) {
            new UsedPartsWarehouseStaffAch(this).InsertUsedPartsWarehouseStaff(usedPartsWarehouseStaff);
        }

        public void UpdateUsedPartsWarehouseStaff(UsedPartsWarehouseStaff usedPartsWarehouseStaff) {
            new UsedPartsWarehouseStaffAch(this).UpdateUsedPartsWarehouseStaff(usedPartsWarehouseStaff);
        }

        public void DeleteUsedPartsWarehouseStaff(UsedPartsWarehouseStaff usedPartsWarehouseStaff) {
            new UsedPartsWarehouseStaffAch(this).DeleteUsedPartsWarehouseStaff(usedPartsWarehouseStaff);
        }

        public IQueryable<UsedPartsWarehouseStaff> GetUsedPartsWarehouseStaffsWithPersonnel() {
            return new UsedPartsWarehouseStaffAch(this).GetUsedPartsWarehouseStaffsWithPersonnel();
        }

        public IQueryable<UsedPartsWarehouseStaff> GetUsedPartsWarehouseStaffByPersonnelId() {
            return new UsedPartsWarehouseStaffAch(this).GetUsedPartsWarehouseStaffByPersonnelId();
        }
    }
}
