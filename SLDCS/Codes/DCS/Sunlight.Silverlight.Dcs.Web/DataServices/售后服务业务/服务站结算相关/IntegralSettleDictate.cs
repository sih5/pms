﻿using System;
using System.Linq;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class IntegralSettleDictateAch : DcsSerivceAchieveBase {
        internal void InsertIntegralSettleDictateValidate(IntegralSettleDictate integralSettleDictate) {
            var userInfo = Utils.GetCurrentUserInfo();
            integralSettleDictate.CreatorId = userInfo.Id;
            integralSettleDictate.CreatorName = userInfo.Name;
            integralSettleDictate.CreateTime = DateTime.Now;
        }

        internal void UpdateIntegralSettleDictateValidate(IntegralSettleDictate integralSettleDictate) {

        }

        public void InsertIntegralSettleDictate(IntegralSettleDictate integralSettleDictate) {
            InsertToDatabase(integralSettleDictate);
            this.InsertIntegralSettleDictateValidate(integralSettleDictate);
        }

        public void UpdateIntegralSettleDictate(IntegralSettleDictate integralSettleDictate) {
            UpdateToDatabase(integralSettleDictate);
            this.UpdateIntegralSettleDictateValidate(integralSettleDictate);
        }

        //public IQueryable<IntegralSettleDictate> GetIntegralSettleDictateWithServiceProductLine() {
        //    return ObjectContext.IntegralSettleDictates.Include("PartsSalesCategory").Include("ServiceProductLine").OrderBy(r => r.Id);
        //}        
    
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:30
        //原分布类的函数全部转移到Ach                                                               

        public void InsertIntegralSettleDictate(IntegralSettleDictate integralSettleDictate) {
            new IntegralSettleDictateAch(this).InsertIntegralSettleDictate(integralSettleDictate);
        }

        public void UpdateIntegralSettleDictate(IntegralSettleDictate integralSettleDictate) {
            new IntegralSettleDictateAch(this).UpdateIntegralSettleDictate(integralSettleDictate);
        }

    }
}
