﻿
namespace Sunlight.Silverlight.Dcs.Web {
    partial class SettleBillInvoiceLinkAch : DcsSerivceAchieveBase {
        public SettleBillInvoiceLinkAch(DcsDomainService domainService)
            : base(domainService) {
        }

        internal void InsertSettleBillInvoiceLinkValidate(SettleBillInvoiceLink settleBillInvoiceLink) {
        }
        internal void UpdateSettleBillInvoiceLinkValidate(SettleBillInvoiceLink settleBillInvoiceLink) {
        }

        public void InsertSettleBillInvoiceLink(SettleBillInvoiceLink settleBillInvoiceLink) {
            InsertToDatabase(settleBillInvoiceLink);
            this.InsertSettleBillInvoiceLinkValidate(settleBillInvoiceLink);
        }
        public void UpdateSettleBillInvoiceLink(SettleBillInvoiceLink settleBillInvoiceLink) {
            UpdateToDatabase(settleBillInvoiceLink);
            this.UpdateSettleBillInvoiceLinkValidate(settleBillInvoiceLink);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:30
        //原分布类的函数全部转移到Ach                                                               

        public void InsertSettleBillInvoiceLink(SettleBillInvoiceLink settleBillInvoiceLink) {
            new SettleBillInvoiceLinkAch(this).InsertSettleBillInvoiceLink(settleBillInvoiceLink);
        }

        public void UpdateSettleBillInvoiceLink(SettleBillInvoiceLink settleBillInvoiceLink) {
            new SettleBillInvoiceLinkAch(this).UpdateSettleBillInvoiceLink(settleBillInvoiceLink);
        }
    }
}
