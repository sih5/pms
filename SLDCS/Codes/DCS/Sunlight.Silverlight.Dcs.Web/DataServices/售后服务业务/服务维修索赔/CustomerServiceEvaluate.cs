﻿
using System;
using System.Linq;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class CustomerServiceEvaluateAch : DcsSerivceAchieveBase {
        public CustomerServiceEvaluateAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public IQueryable<VirtualCustomerServiceEvaluate> GetCustomerServiceEvaluateDetails() {
            var userInfo = Utils.GetCurrentUserInfo();
            var company = this.ObjectContext.Companies.SingleOrDefault(r => r.Id == userInfo.EnterpriseId);
            //IQueryable<RepairClaimBill> repairClaimBills = this.ObjectContext.RepairClaimBills;
            //if(company.Type == (int)DcsCompanyType.分公司) {
            //    repairClaimBills = from a in repairClaimBills
            //                       join b in this.ObjectContext.PersonSalesCenterLinks.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.PersonId == userInfo.Id) on a.PartsSalesCategoryId equals b.PartsSalesCategoryId
            //                       select a;
            //}
            //if(company.Type == (int)DcsCompanyType.服务站 || company.Type == (int)DcsCompanyType.服务站兼代理库) {
            //    repairClaimBills = repairClaimBills.Where(r => r.DealerId == userInfo.EnterpriseId);
            //}
            //var result = from a in this.ObjectContext.CustomerServiceEvaluates
            //             join b in repairClaimBills on a.RepairClaimBillId equals b.Id
            //             join c in this.ObjectContext.MarketingDepartments on b.MarketingDepartmentId equals c.Id
            //             join d in this.ObjectContext.PartsSalesCategories on b.PartsSalesCategoryId equals d.Id
            //             join x in this.ObjectContext.RepairOrders on b.RepairOrderId equals x.Id
            //             select new VirtualCustomerServiceEvaluate {
            //                 Id = a.Id,
            //                 RepairClaimBillId = a.RepairClaimBillId,
            //                 RepairClaimBillCode = a.RepairClaimBillCode,
            //                 EvaluateSource = a.EvaluateSource,
            //                 EvaluateGrade = a.EvaluateGrade,
            //                 DisContentFactor = a.DisContentFactor,
            //                 EvaluateTime = a.EvaluateTime,
            //                 VIN = b.VIN,
            //                 RepairClaimBillCreateTime = b.CreateTime,
            //                 CreatorId = a.CreatorId,
            //                 CreatorName = a.CreatorName,
            //                 CreateTime = a.CreateTime,
            //                 PartsSalesCategoryId = d.Id,
            //                 PartsSalesCategoryName = d.Name,
            //                 MarketingDepartmentId = c.Id,
            //                 MarketingDepartmentName = c.Name,
            //                 DealerName = b.DealerName,
            //                 DealerCode = b.DealerCode,
            //                 RepairType = b.RepairType,
            //                 VehicleContactPerson = b.VehicleContactPerson,
            //                 ContactPhone = b.ContactPhone,
            //                 VehicleLicensePlate = b.VehicleLicensePlate,
            //                 RepairRequestTime = b.RepairRequestTime,
            //                 EvaluationResults = a.EvaluateGrade > 7 ? (int)DcsCustomerServiceEvaluateResult.好评 : a.EvaluateGrade > 5 ? (int)DcsCustomerServiceEvaluateResult.中评 : (int)DcsCustomerServiceEvaluateResult.差评,
            //                 CustomOpinion = x.CustomOpinion
            //             };
            //return result.OrderBy(r => r.EvaluateGrade).ThenByDescending(r => r.RepairClaimBillCreateTime);
            return null;
        }
        public IQueryable<VirtualCustomerServiceEvaluate> GetCustomerServiceEvaluateStatisticsDetails(DateTime? repairRequestTimeBegion, DateTime? repairRequestTimeEnd) {
            var userInfo = Utils.GetCurrentUserInfo();
            var company = this.ObjectContext.Companies.SingleOrDefault(r => r.Id == userInfo.EnterpriseId);
            //IQueryable<RepairClaimBill> repairClaimBills = this.ObjectContext.RepairClaimBills;
            //if(company.Type == (int)DcsCompanyType.分公司) {
            //    repairClaimBills = from a in repairClaimBills
            //                       join b in this.ObjectContext.PersonSalesCenterLinks.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.PersonId == userInfo.Id) on a.PartsSalesCategoryId equals b.PartsSalesCategoryId
            //                       select a;
            //}
            //if(company.Type == (int)DcsCompanyType.服务站 || company.Type == (int)DcsCompanyType.服务站兼代理库) {
            //    repairClaimBills = repairClaimBills.Where(r => r.DealerId == userInfo.EnterpriseId);
            //}
            //if(repairRequestTimeBegion.HasValue) {
            //    repairClaimBills = repairClaimBills.Where(r => r.RepairRequestTime >= repairRequestTimeBegion);
            //}
            //if(repairRequestTimeEnd.HasValue) {
            //    repairClaimBills = repairClaimBills.Where(r => r.RepairRequestTime < repairRequestTimeEnd);
            //}
            //var structure = from a in this.ObjectContext.CustomerServiceEvaluates
            //                join b in repairClaimBills on a.RepairClaimBillId equals b.Id
            //                group new {
            //                    b.DealerId,
            //                    b.PartsSalesCategoryId,
            //                    b.MarketingDepartmentId,
            //                    a.EvaluateGrade,
            //                    a.DisContentFactor
            //                }by new {
            //                    b.DealerId,
            //                    b.PartsSalesCategoryId,
            //                    b.MarketingDepartmentId
            //                }into g
            //                select new {
            //                    g.Key.DealerId,
            //                    g.Key.PartsSalesCategoryId,
            //                    g.Key.MarketingDepartmentId,
            //                    EvaluationQuantity = g.Count(),
            //                    Praise = g.Count(r => r.EvaluateGrade > 7),
            //                    InTheComments = g.Count(r => r.EvaluateGrade < 8 && r.EvaluateGrade > 5),
            //                    Bad = g.Count(r => r.EvaluateGrade < 6),
            //                    RepairTechnology = g.Count(r => r.DisContentFactor == (int)DcsDisContentFactor.维修技术水平),
            //                    ServiceAttitude = g.Count(r => r.DisContentFactor == (int)DcsDisContentFactor.服务态度),
            //                    ServiceTime = g.Count(r => r.DisContentFactor == (int)DcsDisContentFactor.服务及时性),
            //                    ServiceHygiene = g.Count(r => r.DisContentFactor == (int)DcsDisContentFactor.服务站环境),
            //                    NotNullCount = g.Count(r => r.DisContentFactor == (int)DcsDisContentFactor.维修技术水平 || r.DisContentFactor == (int)DcsDisContentFactor.服务态度 || r.DisContentFactor == (int)DcsDisContentFactor.服务及时性 || r.DisContentFactor == (int)DcsDisContentFactor.服务站环境)
            //                };
            //var result = from a in structure
            //             join b in this.ObjectContext.MarketingDepartments on a.MarketingDepartmentId equals b.Id
            //             join c in this.ObjectContext.PartsSalesCategories on a.PartsSalesCategoryId equals c.Id
            //             join d in this.ObjectContext.Dealers on a.DealerId equals d.Id
            //             select new VirtualCustomerServiceEvaluate {
            //                 DealerId = a.DealerId,
            //                 DealerName = d.Name,
            //                 DealerCode = d.Code,
            //                 PartsSalesCategoryId = a.PartsSalesCategoryId,
            //                 PartsSalesCategoryName = c.Name,
            //                 MarketingDepartmentId = a.MarketingDepartmentId,
            //                 MarketingDepartmentName = b.Name,
            //                 EvaluationQuantity = a.EvaluationQuantity,
            //                 Praise = a.Praise / a.EvaluationQuantity * 100,
            //                 InTheComments = a.InTheComments / a.EvaluationQuantity * 100,
            //                 Bad = a.Bad / a.EvaluationQuantity * 100,
            //                 RepairTechnology = a.NotNullCount==0?0:a.RepairTechnology / (a.NotNullCount) * 100,
            //                 ServiceAttitude = a.NotNullCount == 0 ? 0 : a.ServiceAttitude / (a.NotNullCount) * 100,
            //                 ServiceHygiene = a.NotNullCount == 0 ? 0 : a.ServiceHygiene / (a.NotNullCount) * 100,
            //                 ServiceTime = a.NotNullCount == 0 ? 0 : a.ServiceTime / (a.NotNullCount) * 100
            //             };
            //return result.OrderBy(r => r.DealerId);
            return null;
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:30
        //原分布类的函数全部转移到Ach                                                               

        public IQueryable<VirtualCustomerServiceEvaluate> GetCustomerServiceEvaluateDetails() {
            return new CustomerServiceEvaluateAch(this).GetCustomerServiceEvaluateDetails();
        }

        public IQueryable<VirtualCustomerServiceEvaluate> GetCustomerServiceEvaluateStatisticsDetails(DateTime? repairRequestTimeBegion, DateTime? repairRequestTimeEnd) {
            return new CustomerServiceEvaluateAch(this).GetCustomerServiceEvaluateStatisticsDetails(repairRequestTimeBegion,repairRequestTimeEnd);
        }
    }
}
