﻿using System;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class FdWarrantyMessageAch : DcsSerivceAchieveBase {
        public void InsertFDWarrantyMessage(FDWarrantyMessage fdWarrantyMessage) {
            InsertToDatabase(fdWarrantyMessage);
            InsertfdWarrantyMessageValidate(fdWarrantyMessage);
        }

        internal void InsertfdWarrantyMessageValidate(FDWarrantyMessage fdWarrantyMessage) {
            var userInfo = Utils.GetCurrentUserInfo();
            fdWarrantyMessage.CreatorId = userInfo.Id;
            fdWarrantyMessage.CreatorName = userInfo.Name;
            fdWarrantyMessage.CreateTime = DateTime.Now;
        }


        public void UpdateFDWarrantyMessage(FDWarrantyMessage fdWarrantyMessage) {
            UpdateToDatabase(fdWarrantyMessage);
            this.UpdateFDWarrantyMessageValidate(fdWarrantyMessage);
        }

        internal void UpdateFDWarrantyMessageValidate(FDWarrantyMessage fdWarrantyMessage) {
            var userInfo = Utils.GetCurrentUserInfo();
            fdWarrantyMessage.ModifierId = userInfo.Id;
            fdWarrantyMessage.ModifierName = userInfo.Name;
            fdWarrantyMessage.ModifyTime = DateTime.Now;
        }

    }

    partial class DcsDomainService {
        public void InsertFDWarrantyMessage(FDWarrantyMessage fdWarrantyMessage) {
            new FdWarrantyMessageAch(this).InsertFDWarrantyMessage(fdWarrantyMessage);
        }


        public void UpdateFDWarrantyMessage(FDWarrantyMessage fdWarrantyMessage) {
            new FdWarrantyMessageAch(this).UpdateFDWarrantyMessage(fdWarrantyMessage);
        }
    }
}
