﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class FaultyPartsSupplierAssemblyAch : DcsSerivceAchieveBase {
        internal void InsertFaultyPartsSupplierAssemblyValidate(FaultyPartsSupplierAssembly faultyPartsSupplierAssembly) {
            var dbFaultyPartsSupplierAssembly = ObjectContext.FaultyPartsSupplierAssemblies.Where(r => r.Id != faultyPartsSupplierAssembly.Id && r.Code.ToLower() == faultyPartsSupplierAssembly.Code.ToLower() && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbFaultyPartsSupplierAssembly != null)
                throw new ValidationException("已存在祸首件所属总成编号为“{0}”的单据！");
            var userInfo = Utils.GetCurrentUserInfo();
            faultyPartsSupplierAssembly.CreatorId = userInfo.Id;
            faultyPartsSupplierAssembly.CreatorName = userInfo.Name;
            faultyPartsSupplierAssembly.CreateTime = DateTime.Now;
        }
        internal void UpdateFaultyPartsSupplierAssemblyValidate(FaultyPartsSupplierAssembly faultyPartsSupplierAssembly) {
            var dbFaultyPartsSupplierAssembly = ObjectContext.FaultyPartsSupplierAssemblies.Where(r => r.Id != faultyPartsSupplierAssembly.Id && r.Code.ToLower() == faultyPartsSupplierAssembly.Code.ToLower() && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbFaultyPartsSupplierAssembly != null)
                throw new ValidationException("已存在祸首件所属总成编号为“{0}”的单据！");
            var userInfo = Utils.GetCurrentUserInfo();
            faultyPartsSupplierAssembly.ModifierId = userInfo.Id;
            faultyPartsSupplierAssembly.ModifierName = userInfo.Name;
            faultyPartsSupplierAssembly.ModifyTime = DateTime.Now;
        }
        public void InsertFaultyPartsSupplierAssembly(FaultyPartsSupplierAssembly faultyPartsSupplierAssembly) {
            InsertToDatabase(faultyPartsSupplierAssembly);
            this.InsertFaultyPartsSupplierAssemblyValidate(faultyPartsSupplierAssembly);
        }
        public void UpdateFaultyPartsSupplierAssembly(FaultyPartsSupplierAssembly faultyPartsSupplierAssembly) {
            UpdateToDatabase(faultyPartsSupplierAssembly);
            this.UpdateFaultyPartsSupplierAssemblyValidate(faultyPartsSupplierAssembly);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:30
        //原分布类的函数全部转移到Ach                                                               

        public void InsertFaultyPartsSupplierAssembly(FaultyPartsSupplierAssembly faultyPartsSupplierAssembly) {
            new FaultyPartsSupplierAssemblyAch(this).InsertFaultyPartsSupplierAssembly(faultyPartsSupplierAssembly);
        }

        public void UpdateFaultyPartsSupplierAssembly(FaultyPartsSupplierAssembly faultyPartsSupplierAssembly) {
            new FaultyPartsSupplierAssemblyAch(this).UpdateFaultyPartsSupplierAssembly(faultyPartsSupplierAssembly);
        }
    }
}
