﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.Net;
using System.ServiceModel.DomainServices.Server;
using System.Text;
using System.Transactions;
using System.Web.Configuration;
using FTDCSWebService;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class MarketABQualityInformationAch : DcsSerivceAchieveBase {
        public void InsertMarketABQualityInformation(MarketABQualityInformation marketABQualityInformation) {
            InsertToDatabase(marketABQualityInformation);
            this.InsertMarketABQualityInformationValidate(marketABQualityInformation);
            var dealer = ObjectContext.Dealers.Where(r => r.Id == marketABQualityInformation.DealerId && r.Status == (int)DcsMasterDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dealer == null || dealer.Code == null) {
                throw new ValidationException("经销商未找到");
            }

            //var expressVehicleLists = ChangeSet.GetAssociatedChanges(marketABQualityInformation, v => v.ExpressVehicleLists, ChangeOperation.Insert);
            //foreach(ExpressVehicleList theExpressVehicleList in expressVehicleLists) {
            //    InsertToDatabase(theExpressVehicleList);
            //}
            marketABQualityInformation.Code = CodeGenerator.Generate("MarketABQualityInformation", dealer.Code);
        }
        public void UpdateMarketABQualityInformation(MarketABQualityInformation marketABQualityInformation) {
            UpdateToDatabase(marketABQualityInformation);
            //var expressVehicleLists = ChangeSet.GetAssociatedChanges(marketABQualityInformation, v => v.ExpressVehicleLists);

            //foreach(ExpressVehicleList theExpressVehicleList in expressVehicleLists) {
            //    switch(ChangeSet.GetChangeOperation(theExpressVehicleList)) {
            //        case ChangeOperation.Insert:
            //            InsertToDatabase(theExpressVehicleList);
            //            break;
            //        case ChangeOperation.Update:
            //            UpdateToDatabase(theExpressVehicleList);
            //            break;
            //        case ChangeOperation.Delete:
            //            DeleteFromDatabase(theExpressVehicleList);
            //            break;
            //    }
            //}

            this.UpdateMarketABQualityInformationValidate(marketABQualityInformation);
        }

        internal void InsertMarketABQualityInformationValidate(MarketABQualityInformation marketABQualityInformation) {
            var userInfo = Utils.GetCurrentUserInfo();
            marketABQualityInformation.CreatorId = userInfo.Id;
            marketABQualityInformation.CreatorName = userInfo.Name;
            marketABQualityInformation.CreateTime = DateTime.Now;

        }
        internal void UpdateMarketABQualityInformationValidate(MarketABQualityInformation marketABQualityInformation) {
            var userInfo = Utils.GetCurrentUserInfo();
            marketABQualityInformation.ModifierId = userInfo.Id;
            marketABQualityInformation.ModifierName = userInfo.Name;
            marketABQualityInformation.ModifyTime = DateTime.Now;
        }

        public IQueryable<MarketABQualityInformation> GetMarketABQualityInformationDetails() {
            return ObjectContext.MarketABQualityInformations.Include("Branch").Include("RepairObject").Include("PartsSalesCategory").Include("ServiceProductLineView").Include("Dealer").Include("Company").Include("VehicleInformation").Include("Countersignatures.CountersignatureDetails").OrderBy(r => r.Id);
        }
        public MarketABQualityInformation GetMarketABQualityInformationById(int id) {
            var marketAbQualityInformation = ObjectContext.MarketABQualityInformations.Include("RepairObject").Include("Branch").Include("PartsSalesCategory").Include("ServiceProductLineView").Include("Dealer").Include("Company").Include("VehicleInformation").Include("ExpressVehicleLists").SingleOrDefault(r => r.Id == id);
            var countersignatures = ObjectContext.Countersignatures.SingleOrDefault(r => r.SourceId == marketAbQualityInformation.Id && r.SourceType == (int)DcsCountersignatureApplyType.AB质量信息快报);
            if(countersignatures != null) {
                var countersignatureDetails = ObjectContext.CountersignatureDetails.Where(r => r.CountersignatureId == countersignatures.Id).ToArray();
            }
            return marketAbQualityInformation;
        }



        public IQueryable<MarketABQualityInformation> 查询市场AB质量信息快报() {
            var userInfo = Utils.GetCurrentUserInfo();
            var part1 = from a in ObjectContext.MarketABQualityInformations.Where(r => r.Status != (int)DcsABQualityInformationStatus.作废)
                        join b in ObjectContext.Countersignatures.Where(r => r.Tpye == (int)DcsCountersignatureType.并行 && r.SourceType == (int)DcsCountersignatureApplyType.AB质量信息快报) on a.Id equals b.SourceId
                        join c in ObjectContext.CountersignatureDetails.Where(r => r.AppPersonId == userInfo.Id && r.Status == (int)DcsCountersignatureDetailStatus.未会签) on b.Id equals c.CountersignatureId
                        select a;
            var part2 = from a in ObjectContext.MarketABQualityInformations.Where(r => r.Status != (int)DcsABQualityInformationStatus.作废)
                        join b in ObjectContext.Countersignatures.Where(r => r.Tpye == (int)DcsCountersignatureType.串行 && r.SourceType == (int)DcsCountersignatureApplyType.AB质量信息快报) on a.Id equals b.SourceId
                        join c in ObjectContext.CountersignatureDetails.Where(r => r.AppPersonId == userInfo.Id && r.Status == (int)DcsCountersignatureDetailStatus.未会签) on b.Id equals c.CountersignatureId
                        where b.NextNumber == c.SerialNumber
                        select a;

            var result = part1.Union(part2);
            return result.Include("Branch").Include("RepairObject").Include("PartsSalesCategory").Include("ServiceProductLineView").Include("Dealer").Include("Company").Include("VehicleInformation").Include("Countersignatures").OrderBy(r => r.Id);
        }

        public void PMS_SYC_1035_SendGradeABQualityInfoService(MarketABQualityInformation marketABQualityInformation) {
            bool interfaceEnabled;
            Boolean.TryParse(WebConfigurationManager.AppSettings["isInterfaceEnabled1035"], out interfaceEnabled);
            if(!interfaceEnabled)
                return;
            string databaseName = WebConfigurationManager.AppSettings["DataBaseName"];
            if(databaseName != "DCS")
                return;
            if(marketABQualityInformation.BranchId != 2203) {
                return;
            }
            var dealer=ObjectContext.Dealers.Where(r => r.Id == marketABQualityInformation.DealerId && r.Status == (int)DcsMasterDataStatus.有效).FirstOrDefault();
            var company = ObjectContext.Companies.Where(r => r.Id == marketABQualityInformation.DealerId && r.Status == (int)DcsMasterDataStatus.有效).FirstOrDefault();
            //var vehicle = ObjectContext.VehicleInformations.Where(r => r.Id == marketABQualityInformation.VehicleId).FirstOrDefault();
            //var expressVehicleList = ObjectContext.ExpressVehicleLists.Where(r => r.MarketABQualityInfoId == marketABQualityInformation.Id);
            StringBuilder sbDATA = new StringBuilder();
            sbDATA.Append("<DATA>");
            sbDATA.Append("<HEAD>");
            sbDATA.Append("<BIZTRANSACTIONID>PMS_SYC_1035_" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + "</BIZTRANSACTIONID>");
            sbDATA.Append("<COUNT>1</COUNT>");
            sbDATA.Append("<CONSUMER>PMS</CONSUMER>");
            sbDATA.Append("<SRVLEVEL>2</SRVLEVEL>");
            sbDATA.Append("<ACCOUNT>quality</ACCOUNT>");
            sbDATA.Append("<PASSWORD>123456</PASSWORD>");
            sbDATA.Append("</HEAD>");
            sbDATA.Append("<LIST>");
            sbDATA.Append("<ITEM>");
            sbDATA.Append("<MarketABQualityInformation.code>" + marketABQualityInformation.Code + "</MarketABQualityInformation.code>");
            sbDATA.Append("<MarketABQualityInformation.FaultDescription>" + ReplaceChar(marketABQualityInformation.FaultDescription) + "</MarketABQualityInformation.FaultDescription>");
            sbDATA.Append("<MarketABQualityInformation.FaultyDescription>" + ReplaceChar(marketABQualityInformation.FaultyDescription) + "</MarketABQualityInformation.FaultyDescription>");
            sbDATA.Append("<MarketABQualityInformation.FaultyLocation>" + ReplaceChar(marketABQualityInformation.FaultyLocation) + "</MarketABQualityInformation.FaultyLocation>");
            sbDATA.Append("<MarketABQualityInformation.FaultyQuantity>" + marketABQualityInformation.FaultyQuantity + "</MarketABQualityInformation.FaultyQuantity>");
            sbDATA.Append("<MarketABQualityInformation.FaultyNature>" + ReplaceChar(marketABQualityInformation.FaultyNature) + "</MarketABQualityInformation.FaultyNature>");
            sbDATA.Append("<MarketABQualityInformation.MessageLevel>" + ReplaceChar(marketABQualityInformation.MessageLevel) + "</MarketABQualityInformation.MessageLevel>");
            sbDATA.Append("<company.name>" + ReplaceChar(dealer.Name) + "</company.name>");
            sbDATA.Append("<company.code>" + dealer.Code + "</company.code>");
            sbDATA.Append("<company.ContactMail>" + ReplaceChar(company.ContactMail) + "</company.ContactMail>");
            sbDATA.Append("<MarketABQualityInformation.InformationPhone>" + marketABQualityInformation.InformationPhone + "</MarketABQualityInformation.InformationPhone>");
            sbDATA.Append("<MarketABQualityInformation.StationMasterPhone>" + marketABQualityInformation.StationMasterPhone + "</MarketABQualityInformation.StationMasterPhone>");
            sbDATA.Append("<MarketABQualityInformation.ManagePhone>" + marketABQualityInformation.ManagePhone + "</MarketABQualityInformation.ManagePhone>");
            sbDATA.Append("<MarketABQualityInformation.VIN>" + marketABQualityInformation.VIN + "</MarketABQualityInformation.VIN>");
            sbDATA.Append("<MarketABQualityInformation.RoadConditions>" + ReplaceChar(marketABQualityInformation.RoadConditions) + "</MarketABQualityInformation.RoadConditions>");
            sbDATA.Append("<MarketABQualityInformation.CargoTypes>" + marketABQualityInformation.CargoTypes + "</MarketABQualityInformation.CargoTypes>");
            sbDATA.Append("<MarketABQualityInformation.CarryWeight>" + marketABQualityInformation.CarryWeight + "</MarketABQualityInformation.CarryWeight>");
            sbDATA.Append("<MarketABQualityInformation.FaultyMileage>" + marketABQualityInformation.FaultyMileage + "</MarketABQualityInformation.FaultyMileage>");
            sbDATA.Append("<MarketABQualityInformation.FaultDate>" + ((marketABQualityInformation.FaultDate == null) ? "" : Convert.ToDateTime(marketABQualityInformation.FaultDate).ToString("yyyy-MM-dd HH:mm:ss")) + "</MarketABQualityInformation.FaultDate>");
            sbDATA.Append("<MarketABQualityInformation.FaultyPartsSupplierCode>" + ReplaceChar(marketABQualityInformation.FaultyPartsSupplierCode) + "</MarketABQualityInformation.FaultyPartsSupplierCode>");
            sbDATA.Append("<SparePart.code>" + marketABQualityInformation.FaultyPartsCode + "</SparePart.code>");
            sbDATA.Append("<SparePart.name>" + ReplaceChar(marketABQualityInformation.FaultyPartsName) + "</SparePart.name>");
            sbDATA.Append("<MarketABQualityInformation.BatchNumber>" + ReplaceChar(marketABQualityInformation.BatchNumber) + "</MarketABQualityInformation.BatchNumber>");
            sbDATA.Append("<MarketABQualityInformation.FailureMode>" + ReplaceChar(marketABQualityInformation.FailureMode) + "</MarketABQualityInformation.FailureMode>");
            //sbDATA.Append("<VehicleInformation.EngineModel>" + ReplaceChar(vehicle.EngineModel) + "</VehicleInformation.EngineModel>");
            //sbDATA.Append("<VehicleInformation.SalesDate>" + ((vehicle.SalesDate == null) ? "" : Convert.ToDateTime(vehicle.SalesDate).ToString("yyyy-MM-dd HH:mm:ss")) + "</VehicleInformation.SalesDate>");
            //sbDATA.Append("<VehicleInformation.OutOfFactoryDate>" + ((vehicle.OutOfFactoryDate == null) ? "" : Convert.ToDateTime(vehicle.OutOfFactoryDate).ToString("yyyy-MM-dd HH:mm:ss")) + "</VehicleInformation.OutOfFactoryDate>");
            //sbDATA.Append("<Product.ProductCategoryCode>" + ReplaceChar(marketABQualityInformation.ProductCategoryCode) + "</Product.ProductCategoryCode>");
            //sbDATA.Append("<VehicleInformation.DriveModeCode>" + ReplaceChar(vehicle.DriveModeCode) + "</VehicleInformation.DriveModeCode>");
            //if(expressVehicleList.Any()) {
            //    sbDATA.Append("<orderItemList>");
            //    foreach(var detail in expressVehicleList) {
            //        sbDATA.Append("<ITEM>");
            //        sbDATA.Append("<kuaibaobianhaodetail>" + marketABQualityInformation.Code + "</kuaibaobianhaodetail>");
            //        sbDATA.Append("<VIN>" + detail.VIN + "</VIN>");
            //        sbDATA.Append("<guzhanglicheng>" + detail.FaultyMileage + "</guzhanglicheng>");
            //        sbDATA.Append("<guzhangshijian>" + ((detail.FaultTime == null) ? "" : Convert.ToDateTime(detail.FaultTime).ToString("yyyy-MM-dd HH:mm:ss")) +"</guzhangshijian>");
            //        sbDATA.Append("<picihao>" + detail.BatchNumber + "</picihao>");
            //        sbDATA.Append("<chuchangriqi>" + ((detail.OutOfFactoryDate == null) ? "" : Convert.ToDateTime(detail.OutOfFactoryDate).ToString("yyyy-MM-dd HH:mm:ss"))+ "</chuchangriqi>");
            //        sbDATA.Append("<xiaoshouriqi>" + ((detail.SalesDate == null) ? "" : Convert.ToDateTime(detail.SalesDate).ToString("yyyy-MM-dd HH:mm:ss")) + "</xiaoshouriqi>");
            //        sbDATA.Append("</ITEM>");
            //    }
            //    sbDATA.Append("</orderItemList>");
            //}
            List<QisFtpPath> qisFtpPaths = new List<QisFtpPath>();
            qisFtpPaths = ExchangePath(marketABQualityInformation.Path);
            sbDATA.Append("<pathList>");
            foreach(var qisFtpPath in qisFtpPaths) {
                sbDATA.Append("<ITEM>");
                sbDATA.Append("<kuaibaobianhao>" + marketABQualityInformation.Code + "</kuaibaobianhao>");
                sbDATA.Append("<fujianwenjianming>" + ReplaceChar(qisFtpPath.wenjian) + "</fujianwenjianming>");
                sbDATA.Append("<xunifujianwenjianming>" + ReplaceChar(qisFtpPath.xnwenjian) + "</xunifujianwenjianming>");
                sbDATA.Append("<ftpfuwuming>" + ReplaceChar(qisFtpPath.fuwuming) + "</ftpfuwuming>");
                sbDATA.Append("<fujianwanzhegnlujing>" + ReplaceChar(qisFtpPath.wanzhenglujing) + "</fujianwanzhegnlujing>");
                sbDATA.Append("</ITEM>");
            }
            sbDATA.Append("</pathList>");
            sbDATA.Append("</ITEM>");
            sbDATA.Append("</LIST>");
            sbDATA.Append("</DATA>");
            string report = "";
            try {
                 var qisRepairClaimBill = new PMS_SYC_1035_SendGradeABQualityInfoService_pttbindingQSService();
                 qisRepairClaimBill.Credentials = new NetworkCredential("PMS", "pms201603111515");
                 qisRepairClaimBill.PMS_SYC_1035_SendGradeABQualityInfoService(sbDATA.ToString(), out report);
                 using(TransactionScope scope = new TransactionScope(TransactionScopeOption.RequiresNew)) {
                     //var nccesb = new ESBLogNCC();
                     //nccesb.InterfaceName = "QIS市场AB质量信息快报";
                     //nccesb.Message = report + "----" + sbDATA.ToString();
                     //nccesb.SyncNumberBegin = marketABQualityInformation.Id;
                     //nccesb.SyncNumberEnd = 0;
                     //nccesb.TheDate = DateTime.Now;
                     //InsertToDatabase(nccesb);
                     this.ObjectContext.SaveChanges();
                     scope.Complete();
                 }
            } catch(Exception e) {
                using(TransactionScope scope = new TransactionScope(TransactionScopeOption.RequiresNew)) {
                    //var nccesb = new ESBLogNCC();
                    //nccesb.InterfaceName = "QIS市场AB质量信息快报";
                    //nccesb.Message = report + e.Message + "----" + sbDATA.ToString();
                    //nccesb.SyncNumberBegin = marketABQualityInformation.Id;
                    //nccesb.SyncNumberEnd = 0;
                    //nccesb.TheDate = DateTime.Now;
                    //InsertToDatabase(nccesb);
                    string s = e.Message;
                    this.ObjectContext.SaveChanges();
                    scope.Complete();
                }
            }
        }

        public List<QisFtpPath> ExchangePath(string path) {
            List<QisFtpPath> qisFtpPaths1 = new List<QisFtpPath>();
            if(!string.IsNullOrEmpty(path)) {
                string[] paths = path.Split('?');
                foreach(var item in paths) {
                    QisFtpPath qisFtp = new QisFtpPath();
                    string[] tempPath = item.Split(':');
                    if(tempPath.Length == 2) {
                        qisFtp.fuwuming = "FtpServer";
                    } else {
                        qisFtp.fuwuming = tempPath[2];
                    }
                    qisFtp.wenjian = tempPath[0];
                    qisFtp.xnwenjian = tempPath[1].Split('/')[tempPath[1].Split('/').Length - 1];
                    string hash = (Math.Abs(tempPath[1].Split('/')[tempPath[1].Split('/').Length - 1].GetHashCode()) % 1000).ToString();
                    string str = "pmszs/";
                    for(var i = 0; i < tempPath[1].Split('/').Length - 1; i++) {
                        str += tempPath[1].Split('/')[i] + "/";
                        if(i == tempPath[1].Split('/').Length - 2) {
                            str += hash + "/";
                        }
                    }
                    str += tempPath[1].Split('/')[tempPath[1].Split('/').Length - 1];
                    qisFtp.wanzhenglujing = str;
                    qisFtpPaths1.Add(qisFtp);
                }
            }
            return qisFtpPaths1;
        }

        public string ReplaceChar(string str) {
            if(!string.IsNullOrEmpty(str)) {
                if(str.Contains("&")) {
                    str = str.Replace("&", "&amp;");
                }
                if(str.Contains("<")) {
                    str = str.Replace("<", "&lt;");
                }
                if(str.Contains(">")) {
                    str = str.Replace(">", "&gt;");
                }
                if(str.Contains("\"")) {
                    str = str.Replace("\"", "&quot;");
                }
                return str;
            } else {
                return "";
            }
        }
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:30
        //原分布类的函数全部转移到Ach                                                               

        public void InsertMarketABQualityInformation(MarketABQualityInformation marketABQualityInformation) {
            new MarketABQualityInformationAch(this).InsertMarketABQualityInformation(marketABQualityInformation);
        }
        public void UpdateMarketABQualityInformation(MarketABQualityInformation marketABQualityInformation) {
            new MarketABQualityInformationAch(this).UpdateMarketABQualityInformation(marketABQualityInformation);
        }

        public IQueryable<MarketABQualityInformation> GetMarketABQualityInformationDetails() {
            return new MarketABQualityInformationAch(this).GetMarketABQualityInformationDetails();
        }
        public MarketABQualityInformation GetMarketABQualityInformationById(int id) {
            return new MarketABQualityInformationAch(this).GetMarketABQualityInformationById(id);
        }

        public IQueryable<MarketABQualityInformation> 查询市场AB质量信息快报() {
            return new MarketABQualityInformationAch(this).查询市场AB质量信息快报();
        }
    }
}
