﻿using System;
using System.Linq;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class OutofWarrantyPaymentAch : DcsSerivceAchieveBase {
        internal void InsertOutofWarrantyPaymentValidate(OutofWarrantyPayment outofWarrantyPayment) {
            var userInfo = Utils.GetCurrentUserInfo();
            outofWarrantyPayment.CreatorId = userInfo.Id;
            outofWarrantyPayment.CreatorName = userInfo.Name;
            outofWarrantyPayment.CreateTime = DateTime.Now;
            if (string.IsNullOrWhiteSpace(outofWarrantyPayment.Code) || outofWarrantyPayment.Code == GlobalVar.ASSIGNED_BY_SERVER)
                outofWarrantyPayment.Code = CodeGenerator.Generate("OutofWarrantyPayment", outofWarrantyPayment.DealerCode);
        }

        internal void UpdateOutofWarrantyPaymentValidate(OutofWarrantyPayment outofWarrantyPayment) {
            var userInfo = Utils.GetCurrentUserInfo();
            outofWarrantyPayment.ModifierId = userInfo.Id;
            outofWarrantyPayment.ModifierName = userInfo.Name;
            outofWarrantyPayment.ModifyTime = DateTime.Now;
        }

        public void InsertOutofWarrantyPayment(OutofWarrantyPayment outofWarrantyPayment) {
            InsertToDatabase(outofWarrantyPayment);
            this.InsertOutofWarrantyPaymentValidate(outofWarrantyPayment);
        }

        public void UpdateOutofWarrantyPayment(OutofWarrantyPayment outofWarrantyPayment) {
            UpdateToDatabase(outofWarrantyPayment);
            this.UpdateOutofWarrantyPaymentValidate(outofWarrantyPayment);
        }
        public OutofWarrantyPayment GetOutofWarrantyPaymentById(int id) {
            var dboutofWarrantyPayment = ObjectContext.OutofWarrantyPayments.Include("PartsSalesCategory").Include("ServiceProductLine").SingleOrDefault(e => e.Id == id);
            if (dboutofWarrantyPayment != null) {
                var dbBrandh = ObjectContext.Branches.SingleOrDefault(e => e.Id == dboutofWarrantyPayment.BranchId);
                if (dboutofWarrantyPayment.ResponsibleUnitId.HasValue) {
                    var dbResponsibleUnit = ObjectContext.ResponsibleUnits.SingleOrDefault(e => e.Id == dboutofWarrantyPayment.ResponsibleUnitId.Value);
                }
            }
            return dboutofWarrantyPayment;
        }

        public IQueryable<OutofWarrantyPayment> GetOutofWarrantyPaymentsWithBranch() {
            var userInfo = Utils.GetCurrentUserInfo();
            var companyType = ObjectContext.Companies.First(r => r.Id == userInfo.EnterpriseId).Type;
            var outofWarrantyPayments = ObjectContext.OutofWarrantyPayments;
            var branchIdandDealerIds = outofWarrantyPayments.Select(r => new {
                r.BranchId,
                r.DealerId,
                r.PartsSalesCategoryId
            });
            //登陆企业为分公司时需要根据人员与品牌关系过滤数据
            if (companyType == (int)DcsCompanyType.分公司) {
                var marketingDepartmentName = (from a in ObjectContext.MarketingDepartments
                                               join b in ObjectContext.DealerMarketDptRelations on a.Id equals b.MarketId
                                               from c in branchIdandDealerIds
                                               join d in ObjectContext.PersonSalesCenterLinks.Where(r => r.PersonId == userInfo.Id) on c.PartsSalesCategoryId equals d.PartsSalesCategoryId
                                               where c.BranchId == b.BranchId && c.DealerId == b.DealerId && c.PartsSalesCategoryId == a.PartsSalesCategoryId
                                               select new {
                                                   a.Name,
                                                   a.PartsSalesCategoryId,
                                                   b.BranchId,
                                                   b.DealerId
                                               }).ToList();
                foreach (var outofWarrantyPayment in outofWarrantyPayments.ToList()) {
                    outofWarrantyPayment.MarketingDepartmentName = marketingDepartmentName.FirstOrDefault(r => r.BranchId == outofWarrantyPayment.BranchId && r.DealerId == outofWarrantyPayment.DealerId && r.PartsSalesCategoryId == outofWarrantyPayment.PartsSalesCategoryId) != null ? marketingDepartmentName.First(r => r.BranchId == outofWarrantyPayment.BranchId && r.DealerId == outofWarrantyPayment.DealerId && r.PartsSalesCategoryId == outofWarrantyPayment.PartsSalesCategoryId).Name : string.Empty;
                }
                return outofWarrantyPayments.Include("Company").Include("Branch").Include("ResponsibleUnit").Include("PartsSalesCategory").Include("ServiceProductLine").OrderBy(entity => entity.Id);
            }
            var marketingDepartmentNames = (from a in ObjectContext.MarketingDepartments
                                            join b in ObjectContext.DealerMarketDptRelations on a.Id equals b.MarketId
                                            from c in branchIdandDealerIds
                                            where c.BranchId == b.BranchId && c.DealerId == b.DealerId && c.PartsSalesCategoryId == a.PartsSalesCategoryId
                                            select new {
                                                a.Name,
                                                a.PartsSalesCategoryId,
                                                b.BranchId,
                                                b.DealerId
                                            }).ToList();
            foreach (var outofWarrantyPayment in outofWarrantyPayments.ToList()) {
                outofWarrantyPayment.MarketingDepartmentName = marketingDepartmentNames.FirstOrDefault(r => r.BranchId == outofWarrantyPayment.BranchId && r.DealerId == outofWarrantyPayment.DealerId && r.PartsSalesCategoryId == outofWarrantyPayment.PartsSalesCategoryId) != null ? marketingDepartmentNames.First(r => r.BranchId == outofWarrantyPayment.BranchId && r.DealerId == outofWarrantyPayment.DealerId && r.PartsSalesCategoryId == outofWarrantyPayment.PartsSalesCategoryId).Name : string.Empty;
            }
            return outofWarrantyPayments.Include("Company").Include("Branch").Include("ResponsibleUnit").Include("PartsSalesCategory").Include("ServiceProductLine").OrderBy(entity => entity.Id);

        }

        public IQueryable<VirtualOutofWarrantyPayment> GetVirtualOutofWarrantyPaymentsWithBranch() {
            var userInfo = Utils.GetCurrentUserInfo();
            var companyType = ObjectContext.Companies.First(r => r.Id == userInfo.EnterpriseId).Type;
            //var marketingDepartmentNames = (from a in ObjectContext.MarketingDepartments
            //                                join b in ObjectContext.DealerMarketDptRelations on a.Id equals b.MarketId
            //                                select new {
            //                                    a.Name,
            //                                    a.PartsSalesCategoryId,
            //                                    b.BranchId,
            //                                    b.DealerId
            //                                });
            //登陆企业为分公司时需要根据人员与品牌关系过滤数据
            //if (companyType == (int)DcsCompanyType.分公司) {
            //    var VirtualOutofWarrantyPaymentquery = from a in ObjectContext.OutofWarrantyPayments
            //                                            join b in ObjectContext.Companies on a.DealerId equals b.Id
            //                                            join c in ObjectContext.Branches on a.BranchId equals c.Id
            //                                            join d in ObjectContext.PartsSalesCategories on a.PartsSalesCategoryId equals d.Id
            //                                            join g in ObjectContext.PersonSalesCenterLinks.Where(r => r.PersonId == userInfo.Id) on a.PartsSalesCategoryId equals g.PartsSalesCategoryId
            //                                            join e in ObjectContext.DealerServiceInfoes.Where(r => r.Status == (int)DcsMasterDataStatus.有效) on new {
            //                                                a.BranchId,
            //                                                a.DealerId,
            //                                                a.PartsSalesCategoryId
            //                                            }equals new {
            //                                                e.BranchId,
            //                                                e.DealerId,
            //                                                e.PartsSalesCategoryId
            //                                            }into temp1
            //                                            from t1 in temp1.DefaultIfEmpty()
            //                                            join m in ObjectContext.MarketingDepartments on t1.MarketingDepartmentId equals m.Id
            //                                            into temp2
            //                                            from t2 in temp2.DefaultIfEmpty()
            //                                            join h in ObjectContext.ServiceProductLines on a.ServiceProductLineId equals h.Id into temp
            //                                            from tt in temp.DefaultIfEmpty()
            //                                           select new VirtualOutofWarrantyPayment {
            //                                                Id = a.Id,
            //                                                Code = a.Code,
            //                                                Status = a.Status,
            //                                                SettlementStatus = a.SettlementStatus,
            //                                                TransactionCategory = a.TransactionCategory,
            //                                                BranchName = c.Name,
            //                                                PartsSalesCategoryId = a.PartsSalesCategoryId,
            //                                                PartsSalesCategoryName = d.Name,
            //                                                ServiceProductLineName = tt.Name,
            //                                                DealerCode = a.DealerCode,
            //                                                CompanyCustomerCode = t1.BusinessCode,
            //                                                DealerName = a.DealerName,
            //                                                MarketingDepartmentId = t2.Id,
            //                                                MarketingDepartmentName = t2.Name,
            //                                                SourceType = a.SourceType,
            //                                                SourceCode = a.SourceCode,
            //                                                DebitOrReplenish = a.DebitOrReplenish,
            //                                                TransactionAmount = a.TransactionAmount,
            //                                                TransactionReason = a.TransactionReason,
            //                                                DealerContactPerson = a.DealerContactPerson,
            //                                                DealerPhoneNumber = a.DealerPhoneNumber,
            //                                                CreatorName = a.CreatorName,
            //                                                CreateTime = a.CreateTime,
            //                                                ResponsibleUnitId = a.ResponsibleUnitId,
            //                                                DealerId = a.DealerId,
            //                                                BranchId = a.BranchId
            //                                           };
            //    return VirtualOutofWarrantyPaymentquery.OrderBy(r => r.Id);
            //}else {
            //    var VirtualOutofWarrantyPaymentquery = from a in ObjectContext.OutofWarrantyPayments
            //                                            join b in ObjectContext.Companies on a.DealerId equals b.Id
            //                                            join c in ObjectContext.Branches on a.BranchId equals c.Id
            //                                            join d in ObjectContext.PartsSalesCategories on a.PartsSalesCategoryId equals d.Id
            //                                            //join g in ObjectContext.PersonSalesCenterLinks.Where(r => r.PersonId == userInfo.Id) on a.PartsSalesCategoryId equals g.PartsSalesCategoryId
            //                                            join e in ObjectContext.DealerServiceInfoes.Where(r => r.Status == (int)DcsMasterDataStatus.有效) on new {
            //                                                a.BranchId,
            //                                                a.DealerId,
            //                                                a.PartsSalesCategoryId
            //                                            }equals new {
            //                                                e.BranchId,
            //                                                e.DealerId,
            //                                                e.PartsSalesCategoryId
            //                                            }into temp1
            //                                            from t1 in temp1.DefaultIfEmpty()
            //                                            join m in ObjectContext.MarketingDepartments on t1.MarketingDepartmentId equals m.Id
            //                                            into temp2
            //                                            from t2 in temp2.DefaultIfEmpty()
            //                                            join h in ObjectContext.ServiceProductLines on a.ServiceProductLineId equals h.Id into temp
            //                                            from tt in temp.DefaultIfEmpty()
            //                                            select new VirtualOutofWarrantyPayment {
            //                                                Id = a.Id,
            //                                                Code = a.Code,
            //                                                Status = a.Status,
            //                                                SettlementStatus = a.SettlementStatus,
            //                                                MarketingDepartmentId = t2.Id,
            //                                                TransactionCategory = a.TransactionCategory,
            //                                                BranchName = c.Name,
            //                                                PartsSalesCategoryId = a.PartsSalesCategoryId,
            //                                                PartsSalesCategoryName = d.Name,
            //                                                ServiceProductLineName = tt.Name,
            //                                                DealerCode = a.DealerCode,
            //                                                CompanyCustomerCode = t1.BusinessCode,
            //                                                DealerName = a.DealerName,
            //                                                MarketingDepartmentName = t2.Name,//
            //                                                SourceType = a.SourceType,
            //                                                SourceCode = a.SourceCode,
            //                                                DebitOrReplenish = a.DebitOrReplenish,
            //                                                TransactionAmount = a.TransactionAmount,
            //                                                TransactionReason = a.TransactionReason,
            //                                                DealerContactPerson = a.DealerContactPerson,
            //                                                DealerPhoneNumber = a.DealerPhoneNumber,
            //                                                CreatorName = a.CreatorName,
            //                                                CreateTime = a.CreateTime,
            //                                                ResponsibleUnitId = a.ResponsibleUnitId,
            //                                                DealerId = a.DealerId,
            //                                                BranchId = a.BranchId
            //                                            };
            //    return VirtualOutofWarrantyPaymentquery.OrderBy(r => r.Id);
            //}
            return null;
        }
    
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:30
        //原分布类的函数全部转移到Ach                                                               

        public void InsertOutofWarrantyPayment(OutofWarrantyPayment outofWarrantyPayment) {
            new OutofWarrantyPaymentAch(this).InsertOutofWarrantyPayment(outofWarrantyPayment);
        }

        public void UpdateOutofWarrantyPayment(OutofWarrantyPayment outofWarrantyPayment) {
            new OutofWarrantyPaymentAch(this).UpdateOutofWarrantyPayment(outofWarrantyPayment);
        }
                public OutofWarrantyPayment GetOutofWarrantyPaymentById(int id) {
            return new OutofWarrantyPaymentAch(this).GetOutofWarrantyPaymentById(id);
        }

        public IQueryable<OutofWarrantyPayment> GetOutofWarrantyPaymentsWithBranch() {
            return new OutofWarrantyPaymentAch(this).GetOutofWarrantyPaymentsWithBranch();
        }

        public IQueryable<VirtualOutofWarrantyPayment> GetVirtualOutofWarrantyPaymentsWithBranch() {
            return new OutofWarrantyPaymentAch(this).GetVirtualOutofWarrantyPaymentsWithBranch();
        }
    }
}
