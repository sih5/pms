﻿using System;
using System.Linq;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartRequisitionReturnBillAch : DcsSerivceAchieveBase {
        internal void InsertPartRequisitionReturnBillValidate(PartRequisitionReturnBill partRequisitionReturnBill) {
            var company = ObjectContext.Companies.Where(r => r.Id == partRequisitionReturnBill.DealerId).Single();
            if(string.IsNullOrWhiteSpace(partRequisitionReturnBill.PartRequisitionReturnCode) || partRequisitionReturnBill.PartRequisitionReturnCode == GlobalVar.ASSIGNED_BY_SERVER) {
                partRequisitionReturnBill.PartRequisitionReturnCode = CodeGenerator.Generate("ExpenseAdjustmentBill", company.Code);
            }
            var userInfo = Utils.GetCurrentUserInfo();
            partRequisitionReturnBill.CreateTime = DateTime.Now;
            partRequisitionReturnBill.CreatorId = userInfo.Id;
            partRequisitionReturnBill.CreatorName = userInfo.Name;
        }

        internal void UpdatePartRequisitionReturnBillValidate(PartRequisitionReturnBill partRequisitionReturnBill) {

        }

        public void InsertPartRequisitionReturnBill(PartRequisitionReturnBill partRequisitionReturnBill) {
            InsertToDatabase(partRequisitionReturnBill);
            this.InsertPartRequisitionReturnBillValidate(partRequisitionReturnBill);
        }

        public void UpdatePartRequisitionReturnBill(PartRequisitionReturnBill partRequisitionReturnBill) {
            UpdateToDatabase(partRequisitionReturnBill);
            this.UpdatePartRequisitionReturnBillValidate(partRequisitionReturnBill);
        }
    
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:30
        //原分布类的函数全部转移到Ach                                                               

        public void InsertPartRequisitionReturnBill(PartRequisitionReturnBill partRequisitionReturnBill) {
            new PartRequisitionReturnBillAch(this).InsertPartRequisitionReturnBill(partRequisitionReturnBill);
        }

        public void UpdatePartRequisitionReturnBill(PartRequisitionReturnBill partRequisitionReturnBill) {
            new PartRequisitionReturnBillAch(this).UpdatePartRequisitionReturnBill(partRequisitionReturnBill);
        }
    }
}
