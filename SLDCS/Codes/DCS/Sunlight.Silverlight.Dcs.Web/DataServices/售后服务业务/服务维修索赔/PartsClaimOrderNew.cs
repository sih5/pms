﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsClaimOrderNewAch : DcsSerivceAchieveBase {
        internal void InsertPartsClaimOrderNewValidate(PartsClaimOrderNew partsClaimOrderNew) {
            var userInfo = Utils.GetCurrentUserInfo();
            partsClaimOrderNew.CreatorId = userInfo.Id;
            partsClaimOrderNew.CreatorName = userInfo.Name;
            partsClaimOrderNew.CreateTime = DateTime.Now;
            var company = ObjectContext.Companies.Where(r => r.Id == partsClaimOrderNew.ReturnCompanyId && r.Status == (int)DcsMasterDataStatus.有效).SingleOrDefault();
            if(company == null) {
                throw new ValidationException(ErrorStrings.PartsClaimOrderNew_Validation12);
            }
            if(string.IsNullOrWhiteSpace(partsClaimOrderNew.Code) || partsClaimOrderNew.Code == GlobalVar.ASSIGNED_BY_SERVER)
                partsClaimOrderNew.Code = CodeGenerator.Generate("PartsClaimOrder", company.Code);
        }

        internal void UpdatePartsClaimOrderNewValidate(PartsClaimOrderNew partsClaimOrderNew) {
            var userInfo = Utils.GetCurrentUserInfo();
            partsClaimOrderNew.ModifierId = userInfo.Id;
            partsClaimOrderNew.ModifierName = userInfo.Name;
            partsClaimOrderNew.ModifyTime = DateTime.Now;
        }

        public void InsertPartsClaimOrderNew(PartsClaimOrderNew partsClaimOrderNew) {
            InsertToDatabase(partsClaimOrderNew);
            this.InsertPartsClaimOrderNewValidate(partsClaimOrderNew);
        }

        public void UpdatePartsClaimOrderNew(PartsClaimOrderNew partsClaimOrderNew) {
            UpdateToDatabase(partsClaimOrderNew);
            this.UpdatePartsClaimOrderNewValidate(partsClaimOrderNew);
        }

        public IQueryable<PartsClaimOrderNewWithOtherInfo> 服务站查询配件索赔单含业务编码(string bussinessCode, string bussinessName) {

            var queryPart = from a in ObjectContext.PartsClaimOrderNews
                            join b in ObjectContext.Companies.Where(r => r.Status == (int)DcsMasterDataStatus.有效) on a.ReturnCompanyId equals b.Id into tempTable1
                            from t1 in tempTable1.DefaultIfEmpty()
                            join f in ObjectContext.Companies.Where(r => r.Status == (int)DcsMasterDataStatus.有效) on a.RWarehouseCompanyId equals f.Id into tempTable5
                            from t5 in tempTable5.DefaultIfEmpty()
                            join g in ObjectContext.PartsSalesCategories.Where(r => r.Status == (int)DcsMasterDataStatus.有效) on a.PartsSalesCategoryId equals g.Id into tempTable6
                            from t6 in tempTable6.DefaultIfEmpty()
                            where t1.Type == (int)DcsCompanyType.服务站 || t1.Type == (int)DcsCompanyType.服务站兼代理库
                            select new PartsClaimOrderNewWithOtherInfo {
                                Id = a.Id,
                                OverWarrantyDate = a.OverWarrantyDate,
                                IdForGetBussinessInfo = 0,
                                ReturnCompanyType = t1 == null ? 0 : t1.Type,
                                ReturnCompanyCode = t1.Code,
                                ReturnCompanyName = t1.Name,
                                PartsSalesCategoryName = t6.Name,
                                IsOutWarranty = a.IsOutWarranty,
                                AbandonTime = a.AbandonTime,
                                ApproveTime = a.ApproveTime,
                                CancelTime = a.CancelTime,
                                CreateTime = a.CreateTime,
                                ModifyTime = a.ModifyTime,
                                PartsSaleTime = a.PartsSaleTime,
                                RejectTime = a.RejectTime,
                                RepairRequestTime = a.RepairRequestTime,
                                LaborCost = a.LaborCost,
                                MaterialCost = a.MaterialCost,
                                MaterialManagementCost = a.MaterialManagementCost,
                                OtherCost = a.OtherCost,
                                TotalAmount = a.TotalAmount,
                                AbandonerId = a.AbandonerId,
                                AgencyOutWarehouseId = a.AgencyOutWarehouseId,
                                ApproverId = a.ApproverId,
                                CancelId = a.CancelId,
                                CDCReturnWarehouseId = a.CDCReturnWarehouseId,
                                CDCRWCompanyId = a.CDCRWCompanyId,
                                CDCRWCompanyType = a.CDCRWCompanyType,
                                CreatorId = a.CreatorId,
                                FaultyPartsSupplierId = a.FaultyPartsSupplierId,
                                ModifierId = a.ModifierId,
                                PartsRetailOrderId = a.PartsRetailOrderId,
                                PartsSaleLong = a.PartsSaleLong,
                                PartsSalesOrderId = a.PartsSalesOrderId,
                                PartsWarrantyLong = a.PartsWarrantyLong,
                                Quantity = a.Quantity,
                                RejectId = a.RejectId,
                                RejectQty = a.RejectQty,
                                RejectStatus = a.RejectStatus,
                                RepairOrderId = a.RepairOrderId,
                                RWarehouseCompanyId = a.RWarehouseCompanyId,
                                RWarehouseCompanyType = a.RWarehouseCompanyType,
                                SalesWarehouseId = a.SalesWarehouseId,
                                SubmitterId = a.SubmitterId,
                                SubmitterName = a.SubmitterName,
                                SubmitTime = a.SubmitTime,
                                BranchId = a.Branchid,
                                FaultyPartsId = a.FaultyPartsId,
                                PartsSalesCategoryId = a.PartsSalesCategoryId,
                                ReturnCompanyId = a.ReturnCompanyId,
                                ReturnWarehouseId = a.ReturnWarehouseId,
                                SettlementStatus = a.SettlementStatus,
                                Status = a.Status,
                                AbandonerName = a.AbandonerName,
                                ApproveComment = a.ApproveComment,
                                ApproveCommentHistory = a.ApproveCommentHistory,
                                ApproverName = a.ApproverName,
                                CancelName = a.CancelName,
                                Code = a.Code,
                                CreatorName = a.CreatorName,
                                CustomerContactPerson = a.CustomerContactPerson,
                                CustomerContactPhone = a.CustomerContactPhone,
                                FaultyPartsCode = a.FaultyPartsCode,
                                FaultyPartsName = a.FaultyPartsName,
                                FaultyPartsSupplierCode = a.FaultyPartsSupplierCode,
                                FaultyPartsSupplierName = a.FaultyPartsSupplierName,
                                MalfunctionDescription = a.MalfunctionDescription,
                                ModifierName = a.ModifierName,
                                PartsRetailOrderCode = a.PartsRetailOrderCode,
                                PartsSalesOrderCode = a.PartsSalesOrderCode,
                                Path = a.Path,
                                RDCReceiptSituation = a.RDCReceiptSituation,
                                RejectName = a.RejectName,
                                RejectReason = a.RejectReason,
                                Remark = a.Remark,
                                RepairOrderCode = a.RepairOrderCode,
                                ReturnWarehouseCode = a.ReturnWarehouseCode,
                                ReturnWarehouseName = a.ReturnWarehouseName,
                                RWarehouseCompanyCode = a.RWarehouseCompanyCode,
                                SalesWarehouseCode = a.SalesWarehouseCode,
                                SalesWarehouseName = a.SalesWarehouseName,
                                RWarehouseCompanyName = t5.Name,
                                ReturnReason = a.ReturnReason,
                                AbandonerReason = a.AbandonerReason,
                                StopReason = a.StopReason,
                                ShippingCode = a.ShippingCode,
                                CenterPartsSalesOrderCode = a.CenterPartsSalesOrderCode
                            };

            return queryPart.OrderBy(r => r.Id);
        }

        public IQueryable<PartsClaimOrderNewWithOtherInfo> 查询配件索赔单含业务编码(string bussinessCode, string bussinessName) {
            var user = Utils.GetCurrentUserInfo();
            var company = ObjectContext.Companies.Single(r => r.Id == user.EnterpriseId);
            if(company.Type == (int)DcsCompanyType.分公司) {
                #region 分公司查询配件索赔单
                var queryPart = from a in ObjectContext.PartsClaimOrderNews
                                join b in ObjectContext.Companies.Where(r => r.Status == (int)DcsMasterDataStatus.有效) on a.ReturnCompanyId equals b.Id into tempTable1
                                from t1 in tempTable1.DefaultIfEmpty()
                                join f in ObjectContext.Companies.Where(r => r.Status == (int)DcsMasterDataStatus.有效) on a.RWarehouseCompanyId equals f.Id into tempTable5
                                from t5 in tempTable5.DefaultIfEmpty()
                                join g in ObjectContext.PartsSalesCategories.Where(r => r.Status == (int)DcsMasterDataStatus.有效) on a.PartsSalesCategoryId equals g.Id into tempTable6
                                from t6 in tempTable6.DefaultIfEmpty()
                                select new PartsClaimOrderNewWithOtherInfo {
                                    Id = a.Id,
                                    OverWarrantyDate = a.OverWarrantyDate,
                                    IdForGetBussinessInfo = 0,
                                    ReturnCompanyType = t1 == null ? 0 : t1.Type,
                                    ReturnCompanyCode = t1.Code,
                                    ReturnCompanyName = t1.Name,
                                    PartsSalesCategoryName = t6.Name,
                                    IsOutWarranty = a.IsOutWarranty,
                                    AbandonTime = a.AbandonTime,
                                    ApproveTime = a.ApproveTime,
                                    CancelTime = a.CancelTime,
                                    CreateTime = a.CreateTime,
                                    ModifyTime = a.ModifyTime,
                                    PartsSaleTime = a.PartsSaleTime,
                                    RejectTime = a.RejectTime,
                                    RepairRequestTime = a.RepairRequestTime,
                                    LaborCost = a.LaborCost,
                                    MaterialCost = a.MaterialCost,
                                    MaterialManagementCost = a.MaterialManagementCost,
                                    OtherCost = a.OtherCost,
                                    TotalAmount = a.TotalAmount,
                                    AbandonerId = a.AbandonerId,
                                    AgencyOutWarehouseId = a.AgencyOutWarehouseId,
                                    ApproverId = a.ApproverId,
                                    CancelId = a.CancelId,
                                    CDCReturnWarehouseId = a.CDCReturnWarehouseId,
                                    CDCRWCompanyId = a.CDCRWCompanyId,
                                    CDCRWCompanyType = a.CDCRWCompanyType,
                                    CreatorId = a.CreatorId,
                                    FaultyPartsSupplierId = a.FaultyPartsSupplierId,
                                    ModifierId = a.ModifierId,
                                    PartsRetailOrderId = a.PartsRetailOrderId,
                                    PartsSaleLong = a.PartsSaleLong,
                                    PartsSalesOrderId = a.PartsSalesOrderId,
                                    PartsWarrantyLong = a.PartsWarrantyLong,
                                    Quantity = a.Quantity,
                                    RejectId = a.RejectId,
                                    RejectQty = a.RejectQty,
                                    RejectStatus = a.RejectStatus,
                                    RepairOrderId = a.RepairOrderId,
                                    RWarehouseCompanyId = a.RWarehouseCompanyId,
                                    RWarehouseCompanyType = a.RWarehouseCompanyType,
                                    SalesWarehouseId = a.SalesWarehouseId,
                                    SubmitterId = a.SubmitterId,
                                    SubmitterName = a.SubmitterName,
                                    SubmitTime = a.SubmitTime,
                                    BranchId = a.Branchid,
                                    FaultyPartsId = a.FaultyPartsId,
                                    PartsSalesCategoryId = a.PartsSalesCategoryId,
                                    ReturnCompanyId = a.ReturnCompanyId,
                                    ReturnWarehouseId = a.ReturnWarehouseId,
                                    SettlementStatus = a.SettlementStatus,
                                    Status = a.Status,
                                    AbandonerName = a.AbandonerName,
                                    ApproveComment = a.ApproveComment,
                                    ApproveCommentHistory = a.ApproveCommentHistory,
                                    ApproverName = a.ApproverName,
                                    CancelName = a.CancelName,
                                    Code = a.Code,
                                    CreatorName = a.CreatorName,
                                    CustomerContactPerson = a.CustomerContactPerson,
                                    CustomerContactPhone = a.CustomerContactPhone,
                                    FaultyPartsCode = a.FaultyPartsCode,
                                    FaultyPartsName = a.FaultyPartsName,
                                    FaultyPartsSupplierCode = a.FaultyPartsSupplierCode,
                                    FaultyPartsSupplierName = a.FaultyPartsSupplierName,
                                    MalfunctionDescription = a.MalfunctionDescription,
                                    ModifierName = a.ModifierName,
                                    PartsRetailOrderCode = a.PartsRetailOrderCode,
                                    PartsSalesOrderCode = a.PartsSalesOrderCode,
                                    Path = a.Path,
                                    RDCReceiptSituation = a.RDCReceiptSituation,
                                    RejectName = a.RejectName,
                                    RejectReason = a.RejectReason,
                                    Remark = a.Remark,
                                    RepairOrderCode = a.RepairOrderCode,
                                    ReturnWarehouseCode = a.ReturnWarehouseCode,
                                    ReturnWarehouseName = a.ReturnWarehouseName,
                                    RWarehouseCompanyCode = a.RWarehouseCompanyCode,
                                    SalesWarehouseCode = a.SalesWarehouseCode,
                                    SalesWarehouseName = a.SalesWarehouseName,
                                    RWarehouseCompanyName = t5.Name,
                                    ReturnReason = a.ReturnReason,
                                    AbandonerReason = a.AbandonerReason,
                                    StopReason = a.StopReason,
                                    ShippingCode = a.ShippingCode,
                                    CenterPartsSalesOrderCode = a.CenterPartsSalesOrderCode,
                                    IsUplodFile = a.Path != null && a.Path.Length > 0
                                };
                return queryPart.OrderBy(r => r.Id);
                #endregion
            } else {
                #region 中心库查询配件索赔单
                var queryPart = from a in ObjectContext.PartsClaimOrderNews
                                join b in ObjectContext.Companies.Where(r => r.Status == (int)DcsMasterDataStatus.有效) on a.ReturnCompanyId equals b.Id into tempTable1
                                from t1 in tempTable1.DefaultIfEmpty()
                                join f in ObjectContext.Companies.Where(r => r.Status == (int)DcsMasterDataStatus.有效) on a.RWarehouseCompanyId equals f.Id into tempTable5
                                from t5 in tempTable5.DefaultIfEmpty()
                                join g in ObjectContext.PartsSalesCategories.Where(r => r.Status == (int)DcsMasterDataStatus.有效) on a.PartsSalesCategoryId equals g.Id into tempTable6
                                from t6 in tempTable6.DefaultIfEmpty()
                                join h in ObjectContext.AgencyDealerRelations.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.AgencyId == user.EnterpriseId) on a.ReturnCompanyId equals h.DealerId
                                where t1.Type == (int)DcsCompanyType.服务站 || t1.Type == (int)DcsCompanyType.服务站兼代理库
                                select new PartsClaimOrderNewWithOtherInfo {
                                    Id = a.Id,
                                    OverWarrantyDate = a.OverWarrantyDate,
                                    IdForGetBussinessInfo = 0,
                                    ReturnCompanyType = t1 == null ? 0 : t1.Type,
                                    ReturnCompanyCode = t1.Code,
                                    ReturnCompanyName = t1.Name,
                                    PartsSalesCategoryName = t6.Name,
                                    IsOutWarranty = a.IsOutWarranty,
                                    AbandonTime = a.AbandonTime,
                                    ApproveTime = a.ApproveTime,
                                    CancelTime = a.CancelTime,
                                    CreateTime = a.CreateTime,
                                    ModifyTime = a.ModifyTime,
                                    PartsSaleTime = a.PartsSaleTime,
                                    RejectTime = a.RejectTime,
                                    RepairRequestTime = a.RepairRequestTime,
                                    LaborCost = a.LaborCost,
                                    MaterialCost = a.MaterialCost,
                                    MaterialManagementCost = a.MaterialManagementCost,
                                    OtherCost = a.OtherCost,
                                    TotalAmount = a.TotalAmount,
                                    AbandonerId = a.AbandonerId,
                                    AgencyOutWarehouseId = a.AgencyOutWarehouseId,
                                    ApproverId = a.ApproverId,
                                    CancelId = a.CancelId,
                                    CDCReturnWarehouseId = a.CDCReturnWarehouseId,
                                    CDCRWCompanyId = a.CDCRWCompanyId,
                                    CDCRWCompanyType = a.CDCRWCompanyType,
                                    CreatorId = a.CreatorId,
                                    FaultyPartsSupplierId = a.FaultyPartsSupplierId,
                                    ModifierId = a.ModifierId,
                                    PartsRetailOrderId = a.PartsRetailOrderId,
                                    PartsSaleLong = a.PartsSaleLong,
                                    PartsSalesOrderId = a.PartsSalesOrderId,
                                    PartsWarrantyLong = a.PartsWarrantyLong,
                                    Quantity = a.Quantity,
                                    RejectId = a.RejectId,
                                    RejectQty = a.RejectQty,
                                    RejectStatus = a.RejectStatus,
                                    RepairOrderId = a.RepairOrderId,
                                    RWarehouseCompanyId = a.RWarehouseCompanyId,
                                    RWarehouseCompanyType = a.RWarehouseCompanyType,
                                    SalesWarehouseId = a.SalesWarehouseId,
                                    SubmitterId = a.SubmitterId,
                                    SubmitterName = a.SubmitterName,
                                    SubmitTime = a.SubmitTime,
                                    BranchId = a.Branchid,
                                    FaultyPartsId = a.FaultyPartsId,
                                    PartsSalesCategoryId = a.PartsSalesCategoryId,
                                    ReturnCompanyId = a.ReturnCompanyId,
                                    ReturnWarehouseId = a.ReturnWarehouseId,
                                    SettlementStatus = a.SettlementStatus,
                                    Status = a.Status,
                                    AbandonerName = a.AbandonerName,
                                    ApproveComment = a.ApproveComment,
                                    ApproveCommentHistory = a.ApproveCommentHistory,
                                    ApproverName = a.ApproverName,
                                    CancelName = a.CancelName,
                                    Code = a.Code,
                                    CreatorName = a.CreatorName,
                                    CustomerContactPerson = a.CustomerContactPerson,
                                    CustomerContactPhone = a.CustomerContactPhone,
                                    FaultyPartsCode = a.FaultyPartsCode,
                                    FaultyPartsName = a.FaultyPartsName,
                                    FaultyPartsSupplierCode = a.FaultyPartsSupplierCode,
                                    FaultyPartsSupplierName = a.FaultyPartsSupplierName,
                                    MalfunctionDescription = a.MalfunctionDescription,
                                    ModifierName = a.ModifierName,
                                    PartsRetailOrderCode = a.PartsRetailOrderCode,
                                    PartsSalesOrderCode = a.PartsSalesOrderCode,
                                    Path = a.Path,
                                    RDCReceiptSituation = a.RDCReceiptSituation,
                                    RejectName = a.RejectName,
                                    RejectReason = a.RejectReason,
                                    Remark = a.Remark,
                                    RepairOrderCode = a.RepairOrderCode,
                                    ReturnWarehouseCode = a.ReturnWarehouseCode,
                                    ReturnWarehouseName = a.ReturnWarehouseName,
                                    RWarehouseCompanyCode = a.RWarehouseCompanyCode,
                                    SalesWarehouseCode = a.SalesWarehouseCode,
                                    SalesWarehouseName = a.SalesWarehouseName,
                                    RWarehouseCompanyName = t5.Name,
                                    ReturnReason = a.ReturnReason,
                                    AbandonerReason = a.AbandonerReason,
                                    StopReason = a.StopReason,
                                    ShippingCode = a.ShippingCode,
                                    CenterPartsSalesOrderCode = a.CenterPartsSalesOrderCode,
                                    IsUplodFile = a.Path != null && a.Path.Length > 0
                                };

                var queryPart2 = from a in ObjectContext.PartsClaimOrderNews
                                 join b in ObjectContext.Companies.Where(r => r.Status == (int)DcsMasterDataStatus.有效) on a.ReturnCompanyId equals b.Id into tempTable1
                                 from t1 in tempTable1.DefaultIfEmpty()
                                 join f in ObjectContext.Companies.Where(r => r.Status == (int)DcsMasterDataStatus.有效) on a.RWarehouseCompanyId equals f.Id into tempTable5
                                 from t5 in tempTable5.DefaultIfEmpty()
                                 join g in ObjectContext.PartsSalesCategories.Where(r => r.Status == (int)DcsMasterDataStatus.有效) on a.PartsSalesCategoryId equals g.Id into tempTable6
                                 from t6 in tempTable6.DefaultIfEmpty()
                                 where t1.Type == (int)DcsCompanyType.代理库 && t1.Id == user.EnterpriseId
                                 select new PartsClaimOrderNewWithOtherInfo {
                                     Id = a.Id,
                                     OverWarrantyDate = a.OverWarrantyDate,
                                     IdForGetBussinessInfo = 0,
                                     ReturnCompanyType = t1 == null ? 0 : t1.Type,
                                     ReturnCompanyCode = t1.Code,
                                     ReturnCompanyName = t1.Name,
                                     PartsSalesCategoryName = t6.Name,
                                     IsOutWarranty = a.IsOutWarranty,
                                     AbandonTime = a.AbandonTime,
                                     ApproveTime = a.ApproveTime,
                                     CancelTime = a.CancelTime,
                                     CreateTime = a.CreateTime,
                                     ModifyTime = a.ModifyTime,
                                     PartsSaleTime = a.PartsSaleTime,
                                     RejectTime = a.RejectTime,
                                     RepairRequestTime = a.RepairRequestTime,
                                     LaborCost = a.LaborCost,
                                     MaterialCost = a.MaterialCost,
                                     MaterialManagementCost = a.MaterialManagementCost,
                                     OtherCost = a.OtherCost,
                                     TotalAmount = a.TotalAmount,
                                     AbandonerId = a.AbandonerId,
                                     AgencyOutWarehouseId = a.AgencyOutWarehouseId,
                                     ApproverId = a.ApproverId,
                                     CancelId = a.CancelId,
                                     CDCReturnWarehouseId = a.CDCReturnWarehouseId,
                                     CDCRWCompanyId = a.CDCRWCompanyId,
                                     CDCRWCompanyType = a.CDCRWCompanyType,
                                     CreatorId = a.CreatorId,
                                     FaultyPartsSupplierId = a.FaultyPartsSupplierId,
                                     ModifierId = a.ModifierId,
                                     PartsRetailOrderId = a.PartsRetailOrderId,
                                     PartsSaleLong = a.PartsSaleLong,
                                     PartsSalesOrderId = a.PartsSalesOrderId,
                                     PartsWarrantyLong = a.PartsWarrantyLong,
                                     Quantity = a.Quantity,
                                     RejectId = a.RejectId,
                                     RejectQty = a.RejectQty,
                                     RejectStatus = a.RejectStatus,
                                     RepairOrderId = a.RepairOrderId,
                                     RWarehouseCompanyId = a.RWarehouseCompanyId,
                                     RWarehouseCompanyType = a.RWarehouseCompanyType,
                                     SalesWarehouseId = a.SalesWarehouseId,
                                     SubmitterId = a.SubmitterId,
                                     SubmitterName = a.SubmitterName,
                                     SubmitTime = a.SubmitTime,
                                     BranchId = a.Branchid,
                                     FaultyPartsId = a.FaultyPartsId,
                                     PartsSalesCategoryId = a.PartsSalesCategoryId,
                                     ReturnCompanyId = a.ReturnCompanyId,
                                     ReturnWarehouseId = a.ReturnWarehouseId,
                                     SettlementStatus = a.SettlementStatus,
                                     Status = a.Status,
                                     AbandonerName = a.AbandonerName,
                                     ApproveComment = a.ApproveComment,
                                     ApproveCommentHistory = a.ApproveCommentHistory,
                                     ApproverName = a.ApproverName,
                                     CancelName = a.CancelName,
                                     Code = a.Code,
                                     CreatorName = a.CreatorName,
                                     CustomerContactPerson = a.CustomerContactPerson,
                                     CustomerContactPhone = a.CustomerContactPhone,
                                     FaultyPartsCode = a.FaultyPartsCode,
                                     FaultyPartsName = a.FaultyPartsName,
                                     FaultyPartsSupplierCode = a.FaultyPartsSupplierCode,
                                     FaultyPartsSupplierName = a.FaultyPartsSupplierName,
                                     MalfunctionDescription = a.MalfunctionDescription,
                                     ModifierName = a.ModifierName,
                                     PartsRetailOrderCode = a.PartsRetailOrderCode,
                                     PartsSalesOrderCode = a.PartsSalesOrderCode,
                                     Path = a.Path,
                                     RDCReceiptSituation = a.RDCReceiptSituation,
                                     RejectName = a.RejectName,
                                     RejectReason = a.RejectReason,
                                     Remark = a.Remark,
                                     RepairOrderCode = a.RepairOrderCode,
                                     ReturnWarehouseCode = a.ReturnWarehouseCode,
                                     ReturnWarehouseName = a.ReturnWarehouseName,
                                     RWarehouseCompanyCode = a.RWarehouseCompanyCode,
                                     SalesWarehouseCode = a.SalesWarehouseCode,
                                     SalesWarehouseName = a.SalesWarehouseName,
                                     RWarehouseCompanyName = t5.Name,
                                     ReturnReason = a.ReturnReason,
                                     AbandonerReason = a.AbandonerReason,
                                     StopReason = a.StopReason,
                                     ShippingCode = a.ShippingCode,
                                     CenterPartsSalesOrderCode = a.CenterPartsSalesOrderCode,
                                     IsUplodFile = a.Path != null && a.Path.Length > 0
                                 };
                var result = queryPart.Concat(queryPart2);
                return result.OrderBy(r => r.Id);
                #endregion
            }
        }

        public PartsClaimOrderNew GetPartsClaimOrderNewById(int id) {
            var partsClaimOrderNew = this.ObjectContext.PartsClaimOrderNews.Single(r => r.Id == id);

            //配件信息
            int[] faultyPartsId = new int[] { partsClaimOrderNew.FaultyPartsId };
            var parts = new SparePartAch(this.DomainService).GetSparePartsByIds(faultyPartsId);
            partsClaimOrderNew.FaultyPartsReferenceCode = parts.First().ReferenceCode;

            var company = new CompanyAch(this.DomainService).GetCompanyById(partsClaimOrderNew.RWarehouseCompanyId ?? 0);
            partsClaimOrderNew.RWarehouseCompanyName = company.FirstOrDefault().Name;

            var company1 = new CompanyAch(this.DomainService).GetCompanyById(partsClaimOrderNew.ReturnCompanyId);
            partsClaimOrderNew.ReturnCompanyName = company1.SingleOrDefault().Name;

            var partsSalesCategory = this.ObjectContext.PartsSalesCategories.SingleOrDefault(r => r.Id == partsClaimOrderNew.PartsSalesCategoryId);
            partsClaimOrderNew.BranchCode = partsSalesCategory.BranchCode;
            partsClaimOrderNew.PartsSalesCategory = partsSalesCategory;

            return partsClaimOrderNew;
        }

        public PartsClaimOrderNew GetPartsClaimOrderNewByIdForReport(int id) {
            var partsClaimOrderNew = this.ObjectContext.PartsClaimOrderNews.Include("PartsSalesCategory").Single(r => r.Id == id);

            //配件信息
            int[] faultyPartsId = new int[] { partsClaimOrderNew.FaultyPartsId };
            var parts = new SparePartAch(this.DomainService).GetSparePartsByIds(faultyPartsId);
            partsClaimOrderNew.FaultyPartsReferenceCode = parts.First().ReferenceCode;

            var company = new CompanyAch(this.DomainService).GetCompanyById(partsClaimOrderNew.RWarehouseCompanyId ?? 0);
            partsClaimOrderNew.RWarehouseCompanyName = company.FirstOrDefault().Name;

            var company1 = new CompanyAch(this.DomainService).GetCompanyById(partsClaimOrderNew.ReturnCompanyId);
            partsClaimOrderNew.ReturnCompanyName = company1.SingleOrDefault().Name;

            return partsClaimOrderNew;
        }

    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:30
        //原分布类的函数全部转移到Ach                                                               

        public void InsertPartsClaimOrderNew(PartsClaimOrderNew partsClaimOrderNew) {
            new PartsClaimOrderNewAch(this).InsertPartsClaimOrderNew(partsClaimOrderNew);
        }

        public void UpdatePartsClaimOrderNew(PartsClaimOrderNew partsClaimOrderNew) {
            new PartsClaimOrderNewAch(this).UpdatePartsClaimOrderNew(partsClaimOrderNew);
        }

        public IQueryable<PartsClaimOrderNewWithOtherInfo> 服务站查询配件索赔单含业务编码(string bussinessCode, string bussinessName) {
            return new PartsClaimOrderNewAch(this).服务站查询配件索赔单含业务编码(bussinessCode, bussinessName);
        }
        public IQueryable<PartsClaimOrderNewWithOtherInfo> 查询配件索赔单含业务编码(string bussinessCode, string bussinessName) {
            return new PartsClaimOrderNewAch(this).查询配件索赔单含业务编码(bussinessCode, bussinessName);
        }

        public PartsClaimOrderNew GetPartsClaimOrderNewById(int id) {
            return new PartsClaimOrderNewAch(this).GetPartsClaimOrderNewById(id);
        }

        public PartsClaimOrderNew GetPartsClaimOrderNewByIdForReport(int id) {
            return new PartsClaimOrderNewAch(this).GetPartsClaimOrderNewByIdForReport(id);
        }
    }
}
