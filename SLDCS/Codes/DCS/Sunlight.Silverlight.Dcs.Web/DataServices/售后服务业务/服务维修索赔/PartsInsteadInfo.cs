﻿using System.Linq;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsInsteadInfoAch : DcsSerivceAchieveBase {
        public PartsInsteadInfoAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public IQueryable<PartsInsteadInfo> 查询虚拟替互换信息(int sparePartId, int dealerId, int subDealerId, int partsSalesCategoryId) {
            var temp1 = from a in ObjectContext.PartsExchanges
                        from b in ObjectContext.PartsExchanges
                        from c in ObjectContext.PartsSupplierRelations
                        from d in ObjectContext.PartsSuppliers
                        from e in ObjectContext.SpareParts
                        where a.ExchangeCode == b.ExchangeCode && a.PartId == sparePartId && b.PartId == c.PartId && c.SupplierId == d.Id && b.PartId == e.Id && a.Status == (int)DcsBaseDataStatus.有效 && b.Status == (int)DcsBaseDataStatus.有效 && c.Status == (int)DcsBaseDataStatus.有效 && d.Status == (int)DcsMasterDataStatus.有效 && e.Status == (int)DcsMasterDataStatus.有效 && b.PartId != sparePartId
                        select new {
                            Id = e.Id,
                            SparePartCode = e.Code,
                            SparePartName = e.Name,
                            InsteadType = (int)DcsPartsReplacementReplacementType.互换,
                            OriginalSparePartId = sparePartId,
                            SupplierId = d.Id,
                            SupplierCode = d.Code,
                            SupplierName = d.Name
                        };
            var temp2 = from a in ObjectContext.PartsReplacements
                        from b in ObjectContext.PartsSupplierRelations
                        from c in ObjectContext.PartsSuppliers
                        from d in ObjectContext.SpareParts
                        where a.OldPartId == sparePartId && a.Status == (int)DcsBaseDataStatus.有效 && a.NewPartId == b.PartId && b.SupplierId == c.Id && a.NewPartId == d.Id && c.Status == (int)DcsMasterDataStatus.有效 && d.Status == (int)DcsMasterDataStatus.有效
                        select new {
                            Id = d.Id,
                            SparePartCode = d.Code,
                            SparePartName = d.Name,
                            InsteadType = (int)DcsPartsReplacementReplacementType.替换,
                            OriginalSparePartId = sparePartId,
                            SupplierId = c.Id,
                            SupplierCode = c.Code,
                            SupplierName = c.Name
                        };
            var tempResult = temp1.Union(temp2);
            var result = from a in tempResult
                         join b in ObjectContext.DealerPartsStocks.Where(r => r.DealerId == dealerId && r.SubDealerId == subDealerId && r.SalesCategoryId == partsSalesCategoryId) on a.Id equals b.SparePartId into tempTable
                         from t in tempTable.DefaultIfEmpty()
                         select new PartsInsteadInfo {
                             Id = a.Id,
                             SparePartCode = a.SparePartCode,
                             SparePartName = a.SparePartName,
                             InsteadType = a.InsteadType,
                             OriginalSparePartId = a.OriginalSparePartId,
                             SupplierId = a.SupplierId,
                             SupplierCode = a.SupplierCode,
                             SupplierName = a.SupplierName,
                             DealerPartStockQuantity = (t == null ? 0 : t.Quantity)
                         };
            return result.OrderBy(r => r.Id);
        }

    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:30
        //原分布类的函数全部转移到Ach                                                               

        public IQueryable<PartsInsteadInfo> 查询虚拟替互换信息(int sparePartId, int dealerId, int subDealerId, int partsSalesCategoryId) {
            return new PartsInsteadInfoAch(this).查询虚拟替互换信息(sparePartId, dealerId, subDealerId, partsSalesCategoryId);
        }
    }
}
