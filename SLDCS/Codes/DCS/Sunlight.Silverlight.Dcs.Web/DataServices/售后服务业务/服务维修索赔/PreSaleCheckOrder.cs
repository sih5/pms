﻿using System;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PreSaleCheckOrderAch : DcsSerivceAchieveBase {
        internal void InsertPreSaleCheckOrderValidate(PreSaleCheckOrder preSaleCheckOrder) {
            var userInfo = Utils.GetCurrentUserInfo();
            preSaleCheckOrder.CreatorId = userInfo.Id;
            preSaleCheckOrder.CreatorName = userInfo.Name;
            preSaleCheckOrder.CreateTime = DateTime.Now;
            if(string.IsNullOrWhiteSpace(preSaleCheckOrder.Code) || preSaleCheckOrder.Code == GlobalVar.ASSIGNED_BY_SERVER)
                preSaleCheckOrder.Code = CodeGenerator.Generate("PreSaleCheckOrder", preSaleCheckOrder.DealerCode);
        }

        internal void UpdatePreSaleCheckOrderValidate(PreSaleCheckOrder preSaleCheckOrder) {
            var userInfo = Utils.GetCurrentUserInfo();
            preSaleCheckOrder.ModifierId = userInfo.Id;
            preSaleCheckOrder.ModifierName = userInfo.Name;
            preSaleCheckOrder.ModifyTime = DateTime.Now;
        }

        public void InsertPreSaleCheckOrder(PreSaleCheckOrder preSaleCheckOrder) {
            InsertToDatabase(preSaleCheckOrder);
            var preSalesCheckDetails = ChangeSet.GetAssociatedChanges(preSaleCheckOrder, r => r.PreSalesCheckDetails, ChangeOperation.Insert);
            foreach(PreSalesCheckDetail preSaleCheckDetail in preSalesCheckDetails) {
                InsertToDatabase(preSaleCheckDetail);
            }
            this.InsertPreSaleCheckOrderValidate(preSaleCheckOrder);
        }

        public void UpdatePreSaleCheckOrder(PreSaleCheckOrder preSaleCheckOrder) {
            preSaleCheckOrder.PreSalesCheckDetails.Clear();
            UpdateToDatabase(preSaleCheckOrder);
            var dbpreSalesCheckDetail = ChangeSet.GetAssociatedChanges(preSaleCheckOrder, r => r.PreSalesCheckDetails);
            foreach(PreSalesCheckDetail preSalesCheckDetail in dbpreSalesCheckDetail) {
                switch(ChangeSet.GetChangeOperation(preSalesCheckDetail)) {
                    case ChangeOperation.Insert:
                        InsertToDatabase(preSalesCheckDetail);
                        break;
                    case ChangeOperation.Update:
                        UpdateToDatabase(preSalesCheckDetail);
                        break;
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(preSalesCheckDetail);
                        break;
                }
            }
            this.UpdatePreSaleCheckOrderValidate(preSaleCheckOrder);
        }

        public IQueryable<PreSaleCheckOrder> GetPreSalesCheckDetailWithBranchWithServiceProductLine() {
            return ObjectContext.PreSaleCheckOrders.Include("Branch").Include("ServiceProductLine").Include("ResponsibleUnit").Include("PartsSalesCategory").Include("VehicleInformation").OrderBy(r => r.Id);
        }

        public PreSaleCheckOrder GetPreSaleCheckOrderWithDetailsById(int id) {
            var preSaleCheckOrder = ObjectContext.PreSaleCheckOrders.SingleOrDefault(r => r.Id == id);
            if(preSaleCheckOrder != null) {
                var preSalesCheckDetails = ObjectContext.PreSalesCheckDetails.Where(r => r.PreSalesCheckId == preSaleCheckOrder.Id).ToArray();
                var branch = ObjectContext.Branches.SingleOrDefault(r => r.Id == preSaleCheckOrder.BranchId);
                var partsSalesCategories = ObjectContext.PartsSalesCategories.SingleOrDefault(r => r.Id == preSaleCheckOrder.PartsSalesCategoryId);
                var responsibleUnit = ObjectContext.ResponsibleUnits.SingleOrDefault(r => r.Id == preSaleCheckOrder.ResponsibleUnitId);
                //var serviceProductLine = ObjectContext.ServiceProductLines.SingleOrDefault(r => r.Id == preSaleCheckOrder.ServiceProductLineId);
                //var vehicleInformation = ObjectContext.VehicleInformations.SingleOrDefault(r => r.VIN == preSaleCheckOrder.VIN);
                //var checktime = ObjectContext.PreSaleCheckOrders.Count(r => id != preSaleCheckOrder.Id && r.VehicleId == preSaleCheckOrder.VehicleId && (r.Status == (int)DcsPreSaleStatus.提交 || r.Status == (int)DcsPreSaleStatus.新建));
                //preSaleCheckOrder.CheckedCount = checktime + (vehicleInformation == null ? 0 : vehicleInformation.PreSaleTime);
            }
            return preSaleCheckOrder;
        }
    
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:30
        //原分布类的函数全部转移到Ach                                                               

        public void InsertPreSaleCheckOrder(PreSaleCheckOrder preSaleCheckOrder) {
            new PreSaleCheckOrderAch(this).InsertPreSaleCheckOrder(preSaleCheckOrder);
        }

        public void UpdatePreSaleCheckOrder(PreSaleCheckOrder preSaleCheckOrder) {
            new PreSaleCheckOrderAch(this).UpdatePreSaleCheckOrder(preSaleCheckOrder);
        }

        public IQueryable<PreSaleCheckOrder> GetPreSalesCheckDetailWithBranchWithServiceProductLine() {
            return new PreSaleCheckOrderAch(this).GetPreSalesCheckDetailWithBranchWithServiceProductLine();
        }

        public PreSaleCheckOrder GetPreSaleCheckOrderWithDetailsById(int id) {
            return new PreSaleCheckOrderAch(this).GetPreSaleCheckOrderWithDetailsById(id);
        }
    }
}
