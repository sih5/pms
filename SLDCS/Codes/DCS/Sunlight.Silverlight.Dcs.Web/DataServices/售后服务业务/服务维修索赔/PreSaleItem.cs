﻿using System;
using System.Linq;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PreSaleItemAch : DcsSerivceAchieveBase {
        public PreSaleItemAch(DcsDomainService domainService)
            : base(domainService) {
        }

        internal void InsertPreSaleItemValidate(PreSaleItem preSaleItem) {
            var userInfo = Utils.GetCurrentUserInfo();
            preSaleItem.CreatorId = userInfo.Id;
            preSaleItem.CreatorName = userInfo.Name;
            preSaleItem.CreateTime = DateTime.Now;
        }
        internal void UpdatePreSaleItemValidate(PreSaleItem preSaleItem) {
            var userInfo = Utils.GetCurrentUserInfo();
            preSaleItem.ModifierId = userInfo.Id;
            preSaleItem.ModifierName = userInfo.Name;
            preSaleItem.ModifyTime = DateTime.Now;
        }
        public void InsertPreSaleItem(PreSaleItem preSaleItem) {
            InsertToDatabase(preSaleItem);
            this.InsertPreSaleItemValidate(preSaleItem);
        }
        public void UpdatePreSaleItem(PreSaleItem preSaleItem) {
            UpdateToDatabase(preSaleItem);
            this.UpdatePreSaleItemValidate(preSaleItem);
        }
        public IQueryable<PreSaleItem> GetPreSaleItemWithPartsSalesCategory() {
            return ObjectContext.PreSaleItems.Include("Branch").Include("PartsSalesCategory").Include("ServiceProductLine").OrderBy(e => e.Id);
        }
        public IQueryable<PreSaleItem> GetPreSaleItemByPreSaleCheckOrderInfo(int serviceProductLineId, int partsSalesCategoryId) {
            return ObjectContext.PreSaleItems.Where(r => r.ServiceProductLineId == serviceProductLineId && r.PartsSalesCategoryId == partsSalesCategoryId);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:30
        //原分布类的函数全部转移到Ach                                                               

        public void InsertPreSaleItem(PreSaleItem preSaleItem) {
            new PreSaleItemAch(this).InsertPreSaleItem(preSaleItem);
        }

        public void UpdatePreSaleItem(PreSaleItem preSaleItem) {
            new PreSaleItemAch(this).UpdatePreSaleItem(preSaleItem);
        }

        public IQueryable<PreSaleItem> GetPreSaleItemWithPartsSalesCategory() {
            return new PreSaleItemAch(this).GetPreSaleItemWithPartsSalesCategory();
        }

        public IQueryable<PreSaleItem> GetPreSaleItemByPreSaleCheckOrderInfo(int serviceProductLineId, int partsSalesCategoryId) {
            return new PreSaleItemAch(this).GetPreSaleItemByPreSaleCheckOrderInfo(serviceProductLineId,partsSalesCategoryId);
        }
    }
}
