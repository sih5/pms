﻿using System.Linq;
using Microsoft.Data.Extensions;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class ReturnVisitQuestAch : DcsSerivceAchieveBase {
        public ReturnVisitQuestAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public IQueryable<ReturnVisitQuest> GetReturnVisitQuestWithDetail() {
            IQueryable<ReturnVisitQuest> result = ObjectContext.ReturnVisitQuests.Where(r => r.Status == (int)DcsBaseDataStatus.有效);
            return result.Include("MarketingDepartment").Include("PartsSalesCategory").OrderBy(r => r.id);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:31
        //原分布类的函数全部转移到Ach                                                               

        public IQueryable<ReturnVisitQuest> GetReturnVisitQuestWithDetail() {
            return new ReturnVisitQuestAch(this).GetReturnVisitQuestWithDetail();
        }
    }
}
