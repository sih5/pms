﻿using System;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class SupplierExpenseAdjustBillAch : DcsSerivceAchieveBase {
        internal void InsertSupplierExpenseAdjustBillValidation(SupplierExpenseAdjustBill supplierExpenseAdjustBill) {
            if(string.IsNullOrWhiteSpace(supplierExpenseAdjustBill.Code) || supplierExpenseAdjustBill.Code == GlobalVar.ASSIGNED_BY_SERVER) {
                supplierExpenseAdjustBill.Code = CodeGenerator.Generate("SupplierExpenseAdjustBill", supplierExpenseAdjustBill.SupplierCode);
            }
            var userInfo = Utils.GetCurrentUserInfo();
            supplierExpenseAdjustBill.CreatorId = userInfo.Id;
            supplierExpenseAdjustBill.CreatorName = userInfo.Name;
            supplierExpenseAdjustBill.CreateTime = DateTime.Now;
        }
        internal void UpdateSupplierExpenseAdjustBillValidation(SupplierExpenseAdjustBill supplierExpenseAdjustBill) {
            var userInfo = Utils.GetCurrentUserInfo();
            supplierExpenseAdjustBill.ModifierId = userInfo.Id;
            supplierExpenseAdjustBill.ModifierName = userInfo.Name;
            supplierExpenseAdjustBill.ModifyTime = DateTime.Now;
        }
        public void InsertSupplierExpenseAdjustBill(SupplierExpenseAdjustBill supplierExpenseAdjustBill) {
            InsertToDatabase(supplierExpenseAdjustBill);
            InsertSupplierExpenseAdjustBillValidation(supplierExpenseAdjustBill);
        }
        public void UpdateSupplierExpenseAdjustBill(SupplierExpenseAdjustBill supplierExpenseAdjustBill) {
            UpdateToDatabase(supplierExpenseAdjustBill);
            UpdateSupplierExpenseAdjustBillValidation(supplierExpenseAdjustBill);
        }
        public IQueryable<SupplierExpenseAdjustBill> GetSupplierExpenseAdjustBillWithPartsSalesCategory() {
            var userInfo = Utils.GetCurrentUserInfo();
            var companyType = ObjectContext.Companies.First(r => r.Id == userInfo.EnterpriseId).Type;
            if(companyType == (int)DcsCompanyType.分公司) {
                var supplierExpenseAdjustBill = from a in ObjectContext.SupplierExpenseAdjustBills
                                                join b in ObjectContext.PersonSalesCenterLinks.Where(r => r.PersonId == userInfo.Id) on a.PartsSalesCategoryId equals b.PartsSalesCategoryId
                                                select a;
                return supplierExpenseAdjustBill.Include("PartsSalesCategory").Include("ServiceProductLine").OrderBy(r => r.Id);
            }
            return ObjectContext.SupplierExpenseAdjustBills.Include("PartsSalesCategory").Include("ServiceProductLine").OrderBy(r => r.Id);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:31
        //原分布类的函数全部转移到Ach                                                               
                public void InsertSupplierExpenseAdjustBill(SupplierExpenseAdjustBill supplierExpenseAdjustBill) {
            new SupplierExpenseAdjustBillAch(this).InsertSupplierExpenseAdjustBill(supplierExpenseAdjustBill);
        }
                public void UpdateSupplierExpenseAdjustBill(SupplierExpenseAdjustBill supplierExpenseAdjustBill) {
            new SupplierExpenseAdjustBillAch(this).UpdateSupplierExpenseAdjustBill(supplierExpenseAdjustBill);
        }
                public IQueryable<SupplierExpenseAdjustBill> GetSupplierExpenseAdjustBillWithPartsSalesCategory() {
            return new SupplierExpenseAdjustBillAch(this).GetSupplierExpenseAdjustBillWithPartsSalesCategory();
        }
    }

}
