﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class TransactionInterfaceLogAch : DcsSerivceAchieveBase {
        public IQueryable<TransactionInterfaceLog> GetTransactionInterfaceLogWithRepairOrder() {
            return ObjectContext.TransactionInterfaceLogs.Include("RepairOrder").OrderBy(e => e.Id);
        }
        public void InsertTransactionInterfaceLog(TransactionInterfaceLog transactionInterfaceLog) {
            InsertToDatabase(transactionInterfaceLog);
            if(transactionInterfaceLog.TransactionInterfaceLists.Count > 0) {
                foreach(TransactionInterfaceList transactionInterfaceList in transactionInterfaceLog.TransactionInterfaceLists.ToList())
                    InsertToDatabase(transactionInterfaceList);
            }
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:31
        //原分布类的函数全部转移到Ach                                                               

        public IQueryable<TransactionInterfaceLog> GetTransactionInterfaceLogWithRepairOrder() {
            return new TransactionInterfaceLogAch(this).GetTransactionInterfaceLogWithRepairOrder();
        }

        public void InsertTransactionInterfaceLog(TransactionInterfaceLog transactionInterfaceLog) {
            new TransactionInterfaceLogAch(this).InsertTransactionInterfaceLog(transactionInterfaceLog);
        }
    }
}
