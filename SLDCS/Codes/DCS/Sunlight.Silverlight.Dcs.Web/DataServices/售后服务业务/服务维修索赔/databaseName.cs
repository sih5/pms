﻿using System.Linq;
using System.Web.Configuration;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:30
        public IQueryable<VirtualDataBaseName> GetDataBaseName() {
            var name = WebConfigurationManager.AppSettings["DataBaseName"];
            var temp = from c in ObjectContext.Companies.Where(r => true)
                       select new VirtualDataBaseName {
                           Id = c.Id,
                           Name = name
                       };
            return temp.Take(1);
        }
    }
}
