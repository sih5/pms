﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class CustomerAch : DcsSerivceAchieveBase {
        public CustomerAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public IQueryable<Customer> 查询客户信息(bool? ifVip, int? vIpType) {
            var result = from a in ObjectContext.Customers
                         where a.Status == (int)DcsBaseDataStatus.有效
                         select a;
            var customerIds = ((IQueryable<Customer>)DcsDomainService.QueryComposer.Compose(result, ParamQueryable)).Select(v => v.Id).ToArray();
            IQueryable<RetainedCustomer> retainedCustomer = ObjectContext.RetainedCustomers;
            if(ifVip.HasValue) {
                retainedCustomer = retainedCustomer.Where(r => r.IfVIP == ifVip);
            }
            if(vIpType.HasValue) {
                retainedCustomer = retainedCustomer.Where(r => r.MemberType == vIpType);
            }
            var customers = result.ToArray();
            var retainedCustomers = retainedCustomer.Where(r => customerIds.Contains(r.CustomerId) && r.Status == (int)DcsBaseDataStatus.有效).ToArray();
            return result.OrderBy(r => r.Id);
        }
        internal void InsertCustomerValidate(Customer customer) {
            if(string.IsNullOrWhiteSpace(customer.CustomerCode) || customer.CustomerCode == GlobalVar.ASSIGNED_BY_SERVER) {
                customer.CustomerCode = CodeGenerator.Generate("Customer");
            }
            var userInfo = Utils.GetCurrentUserInfo();
            customer.ModifierId = userInfo.Id;
            customer.ModifierName = userInfo.Name;
            customer.ModifyTime = DateTime.Now;
        }
        internal void UpdateCustomerValidate(Customer customer) {
            var dbCustomer = ObjectContext.Customers.Where(r => r.Id == customer.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbCustomer == null) {
                throw new ValidationException(ErrorStrings.Common_Validation2);
            }
            if(dbCustomer.Status != (int)DcsBaseDataStatus.有效) {
                throw new ValidationException(ErrorStrings.Common_Validation3);
            }
            if(dbCustomer.Id == -1) {
                throw new ValidationException("此客户为系统内置客户,如必须修改请联系管理员");
            }
            var userInfo = Utils.GetCurrentUserInfo();
            customer.ModifierId = userInfo.Id;
            customer.ModifierName = userInfo.Name;
            customer.ModifyTime = DateTime.Now;
        }
        public void InsertCustomer(Customer customer) {
            InsertToDatabase(customer);
            this.InsertCustomerValidate(customer);
        }
        public void UpdateCustomer(Customer customer) {
            UpdateToDatabase(customer);
            this.UpdateCustomerValidate(customer);
        }
        public Customer GetCustomerByRetainedCustomerId(int RetainedCustomerId) {
            var retainedCustomers = ObjectContext.RetainedCustomers.SingleOrDefault(e => e.Id == RetainedCustomerId);
            if(retainedCustomers == null)
                return null;
            return ObjectContext.Customers.SingleOrDefault(ex => ex.Id == retainedCustomers.CustomerId);
        }
        public Customer GetCustomerByVehicleInformationId(int vehicleInformationId) {
            //var retainedCustomerVehicleList = ObjectContext.RetainedCustomerVehicleLists.Include("VehicleInformation").Include("RetainedCustomer.Customer").SingleOrDefault(r => r.VehicleInformation.Id == vehicleInformationId && r.Status == (int)DcsBaseDataStatus.有效);
            //return retainedCustomerVehicleList != null ? retainedCustomerVehicleList.RetainedCustomer.Customer : null;
            return null;
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:31
        //原分布类的函数全部转移到Ach                                                               

        public IQueryable<Customer> 查询客户信息(bool? ifVip, int? vIpType) {
            var result = from a in ObjectContext.Customers
                         where a.Status == (int)DcsBaseDataStatus.有效
                         select a;
            var customerIds = ((IQueryable<Customer>)QueryComposer.Compose(result, ParamQueryable)).Select(v => v.Id).ToArray();
            IQueryable<RetainedCustomer> retainedCustomer = ObjectContext.RetainedCustomers;
            if(ifVip.HasValue) {
                retainedCustomer = retainedCustomer.Where(r => r.IfVIP == ifVip);
            }
            if(vIpType.HasValue) {
                retainedCustomer = retainedCustomer.Where(r => r.MemberType == vIpType);
            }
            var customers = result.ToArray();
            var retainedCustomers = retainedCustomer.Where(r => customerIds.Contains(r.CustomerId) && r.Status == (int)DcsBaseDataStatus.有效).ToArray();
            return result.OrderBy(r => r.Id);
        }//保持原有的public方法，NoChangePublic
 

        public void InsertCustomer(Customer customer) {
            new CustomerAch(this).InsertCustomer(customer);
        }

        public void UpdateCustomer(Customer customer) {
            new CustomerAch(this).UpdateCustomer(customer);
        }
                public Customer GetCustomerByRetainedCustomerId(int RetainedCustomerId) {
            return new CustomerAch(this).GetCustomerByRetainedCustomerId(RetainedCustomerId);
        }

        public Customer GetCustomerByVehicleInformationId(int vehicleInformationId) {
            return new CustomerAch(this).GetCustomerByVehicleInformationId(vehicleInformationId);
        }
    }
}
