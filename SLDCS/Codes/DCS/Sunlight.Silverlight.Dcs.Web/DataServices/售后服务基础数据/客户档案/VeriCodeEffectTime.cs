﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using System.Text;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class VeriCodeEffectTimeAch : DcsSerivceAchieveBase {
        public VeriCodeEffectTimeAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public void InsertVeriCodeEffectTime(VeriCodeEffectTime veriCodeEffectTime) {
            InsertToDatabase(veriCodeEffectTime);
            this.InsertVeriCodeEffectTimeValidate(veriCodeEffectTime);
        }
        internal void InsertVeriCodeEffectTimeValidate(VeriCodeEffectTime veriCodeEffectTime) {
            var userInfo = Utils.GetCurrentUserInfo();
            veriCodeEffectTime.creatorid = userInfo.Id;
            veriCodeEffectTime.creatorname = userInfo.Name;
            veriCodeEffectTime.createtime = DateTime.Now;
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:31
        //原分布类的函数全部转移到Ach                                                               

        public void InsertVeriCodeEffectTime(VeriCodeEffectTime veriCodeEffectTime) {
            new VeriCodeEffectTimeAch(this).InsertVeriCodeEffectTime(veriCodeEffectTime);
        }
    }
}
