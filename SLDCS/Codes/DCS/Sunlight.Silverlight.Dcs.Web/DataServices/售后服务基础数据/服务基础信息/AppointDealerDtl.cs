﻿
using System.Linq;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class AppointDealerDtlAch : DcsSerivceAchieveBase {
        public AppointDealerDtlAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public IQueryable<AppointDealerDtl> GetAppointDealerDtlDealers() {
            return ObjectContext.AppointDealerDtls.Include("Dealer").OrderBy((r => r.Id));
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:31
        //原分布类的函数全部转移到Ach                                                               

        public IQueryable<AppointDealerDtl> GetAppointDealerDtlDealers() {
            return new AppointDealerDtlAch(this).GetAppointDealerDtlDealers();
        }
    }
}
