﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class ExtendedSparePartListAch : DcsSerivceAchieveBase {
        public ExtendedSparePartListAch(DcsDomainService domainService)
            : base(domainService) {
        }
        public void InsertExtendedSparePartList(ExtendedSparePartList extendedSparePartList) {
            InsertToDatabase(extendedSparePartList);
        }

        public void UpdateExtendedSparePartList(ExtendedSparePartList extendedSparePartList) {
            UpdateToDatabase(extendedSparePartList);
        }

    }

    partial class DcsDomainService {
        public void InsertExtendedSparePartList(ExtendedSparePartList extendedSparePartList) {
            new ExtendedSparePartListAch(this).InsertExtendedSparePartList(extendedSparePartList);
        }

        public void UpdateExtendedSparePartList(ExtendedSparePartList extendedSparePartList) {
            new ExtendedSparePartListAch(this).UpdateExtendedSparePartList(extendedSparePartList);
        }
    }
}
