﻿using Sunlight.Silverlight.Web;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class ExtendedWarrantyProductAch : DcsSerivceAchieveBase {
        public ExtendedWarrantyProductAch(DcsDomainService domainService)
            : base(domainService) {
        }
        public void InsertExtendedWarrantyProduct(ExtendedWarrantyProduct extendedWarrantyProduct) {
            var userInfo = Utils.GetCurrentUserInfo();
            var prefixAndBrand = new StringBuilder("YBCP");
            switch (extendedWarrantyProduct.PartsSalesCategory) {
                case "奥铃":
                    prefixAndBrand.Append("AL");
                    break;
                case "拓陆者":
                    prefixAndBrand.Append("TL");
                    break;
                case "欧马可":
                    prefixAndBrand.Append("OK");
                    break;
                case "商务汽车":
                    prefixAndBrand.Append("SW");
                    break;
                case "伽途":
                    prefixAndBrand.Append("JT");
                    break;
                case "欧曼":
                    prefixAndBrand.Append("OM");
                    break;
                case "时代":
                    prefixAndBrand.Append("SD");
                    break;
                case "瑞沃":
                    prefixAndBrand.Append("RW");
                    break;
            }
            extendedWarrantyProduct.ExtendedWarrantyProductCode = CodeGenerator.Generate("ExtendedWarrantyProduct").Replace("YBCP", prefixAndBrand.ToString());
            extendedWarrantyProduct.Status = (int)DcsBaseDataStatus.有效;
            extendedWarrantyProduct.CreatorId = userInfo.Id;
            extendedWarrantyProduct.CreatorTime = DateTime.Now;
            extendedWarrantyProduct.CreatorName = userInfo.Name;
            InsertExtendedWarrantyProductValidate(extendedWarrantyProduct);
            InsertToDatabase(extendedWarrantyProduct);
        }

        public void UpdateExtendedWarrantyProduct(ExtendedWarrantyProduct extendedWarrantyProduct) {
            var userInfo = Utils.GetCurrentUserInfo();
            if (extendedWarrantyProduct.Status == (int)DcsBaseDataStatus.作废) {
                extendedWarrantyProduct.AbandonerId = userInfo.Id;
                extendedWarrantyProduct.AbandonerTime = DateTime.Now;
                extendedWarrantyProduct.AbandonerName = userInfo.Name;
            } else {
                extendedWarrantyProduct.ModifierId = userInfo.Id;
                extendedWarrantyProduct.ModifierTime = DateTime.Now;
                extendedWarrantyProduct.ModifierName = userInfo.Name;
            }
            UpdateToDatabase(extendedWarrantyProduct);
        }

        internal void InsertExtendedWarrantyProductValidate(ExtendedWarrantyProduct extendedWarrantyProduct) {
            var dbExtendedWarrantyProductlike = this.ObjectContext.ExtendedWarrantyProducts.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.PartsSalesCategoryId == extendedWarrantyProduct.PartsSalesCategoryId
                && r.ServiceProductLineId == extendedWarrantyProduct.ServiceProductLineId
                && (
                (r.ExtendedWarrantyMileage == extendedWarrantyProduct.ExtendedWarrantyMileage && r.ExtendedWarrantyTerm == extendedWarrantyProduct.ExtendedWarrantyTerm)
                || (r.ExtendedWarrantyTermRat == extendedWarrantyProduct.ExtendedWarrantyTermRat && r.ExtendedWarrantyMileageRat == extendedWarrantyProduct.ExtendedWarrantyMileageRat)));
            if (dbExtendedWarrantyProductlike.Any()) {
                throw new ValidationException("同一品牌，同一产品线，不能有延保里程,延保里程（比例）和延保期限，延保期限（比例）都相同的数据");
            }
        }

        internal void UpdateExtendedWarrantyProductValidate(ExtendedWarrantyProduct extendedWarrantyProduct) {
        }

        public IQueryable<ExtendedWarrantyProduct> GetExtendedWarrantyProductsIncludeList() {
            return ObjectContext.ExtendedWarrantyProducts.Include("ExtendedSparePartLists").OrderBy(e => e.Id);
        }


    }

    partial class DcsDomainService {
        public void InsertExtendedWarrantyProduct(ExtendedWarrantyProduct extendedWarrantyProduct) {
            new ExtendedWarrantyProductAch(this).InsertExtendedWarrantyProduct(extendedWarrantyProduct);
        }

        public void UpdateExtendedWarrantyProduct(ExtendedWarrantyProduct extendedWarrantyProduct) {
            new ExtendedWarrantyProductAch(this).UpdateExtendedWarrantyProduct(extendedWarrantyProduct);
        }

        public IQueryable<ExtendedWarrantyProduct> GetExtendedWarrantyProductsIncludeList() {
            return new ExtendedWarrantyProductAch(this).GetExtendedWarrantyProductsIncludeList();
        }
    }
}
