﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class GradeCoefficientAch : DcsSerivceAchieveBase {
        internal void InsertGradeCoefficientVailidate(GradeCoefficient gradeCoefficient) {
            var dbGradeCoefficient = ObjectContext.GradeCoefficients.Where(r => r.Grade == gradeCoefficient.Grade && r.PartsSalesCategoryId == gradeCoefficient.PartsSalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效 && r.Coefficient.CompareTo(gradeCoefficient.Coefficient) == 0).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbGradeCoefficient != null) {
                throw new ValidationException(ErrorStrings.GradeCoefficient_Validation1);
            }
            var userInfo = Utils.GetCurrentUserInfo();
            gradeCoefficient.CreatorId = userInfo.Id;
            gradeCoefficient.CreatorName = userInfo.Name;
            gradeCoefficient.CreateTime = DateTime.Now;
        }
        internal void UnsertGradeCoefficientVailidate(GradeCoefficient gradeCoefficient) {
            var dbGradeCoefficient = ObjectContext.GradeCoefficients.Where(r => r.Id == gradeCoefficient.Id && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbGradeCoefficient == null) {
                throw new ValidationException(ErrorStrings.GradeCoefficient_Validation2);
            }
            var checkGradeCoefficient = ObjectContext.GradeCoefficients.
                 Where(r => r.Grade == gradeCoefficient.Grade && r.PartsSalesCategoryId == gradeCoefficient.PartsSalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效 && r.Coefficient.CompareTo(gradeCoefficient.Coefficient) == 0 && r.Id != gradeCoefficient.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(checkGradeCoefficient != null) {
                throw new ValidationException(ErrorStrings.GradeCoefficient_Validation3);
            }
            var userInfo = Utils.GetCurrentUserInfo();
            gradeCoefficient.ModifierId = userInfo.Id;
            gradeCoefficient.ModifierName = userInfo.Name;
            gradeCoefficient.ModifyTime = DateTime.Now;
        }
        public void InsertGradeCoefficient(GradeCoefficient gradeCoefficient) {
            InsertToDatabase(gradeCoefficient);
            InsertGradeCoefficientVailidate(gradeCoefficient);
        }
        public void UpdateGradeCoefficient(GradeCoefficient gradeCoefficient) {
            UpdateToDatabase(gradeCoefficient);
            UnsertGradeCoefficientVailidate(gradeCoefficient);
        }
        public IQueryable<GradeCoefficient> GetGradeCoefficientByPersonSalesCenterLinks() {
            var usrinfo = Utils.GetCurrentUserInfo();
            return this.ObjectContext.GradeCoefficients.Where(r => this.ObjectContext.PersonSalesCenterLinks.Any(x => x.PartsSalesCategoryId == r.PartsSalesCategoryId && x.PersonId == usrinfo.Id && x.Status == (int)DcsBaseDataStatus.有效)).OrderBy(r => r.Id);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:31
        //原分布类的函数全部转移到Ach                                                               

        public void InsertGradeCoefficient(GradeCoefficient gradeCoefficient) {
            new GradeCoefficientAch(this).InsertGradeCoefficient(gradeCoefficient);
        }

        public void UpdateGradeCoefficient(GradeCoefficient gradeCoefficient) {
            new GradeCoefficientAch(this).UpdateGradeCoefficient(gradeCoefficient);
        }

        public IQueryable<GradeCoefficient> GetGradeCoefficientByPersonSalesCenterLinks() {
            return new GradeCoefficientAch(this).GetGradeCoefficientByPersonSalesCenterLinks();
        }
    }
}
