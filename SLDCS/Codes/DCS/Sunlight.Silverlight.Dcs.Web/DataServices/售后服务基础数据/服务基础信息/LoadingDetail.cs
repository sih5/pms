﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class LoadingDetailAch : DcsSerivceAchieveBase {
        public LoadingDetailAch(DcsDomainService domainService)
            : base(domainService) {
        }

        internal void InsertLoadingDetailVailidate(LoadingDetail loadingDetail) {
            var dbLoadingDetail = ObjectContext.LoadingDetails.Where(r => r.ResponsibleUnitCode == loadingDetail.ResponsibleUnitCode && r.FaultyPartsAssemblyName == loadingDetail.FaultyPartsAssemblyName && r.SerialNumber == loadingDetail.SerialNumber && r.BranchId == loadingDetail.BranchId && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbLoadingDetail != null) {
                throw new ValidationException("已经存在相同的装车明细");
            }
            var userInfo = Utils.GetCurrentUserInfo();
            loadingDetail.CreatorId = userInfo.Id;
            loadingDetail.CreatorName = userInfo.Name;
            loadingDetail.CreateTime = DateTime.Now;
        }
        public void InsertGradeCoefficient(LoadingDetail loadingDetail) {
            InsertToDatabase(loadingDetail);
            InsertLoadingDetailVailidate(loadingDetail);
        }
        internal void UpdateLoadingDetailVailidate(LoadingDetail loadingDetail) {
            var dbLoadingDetail = ObjectContext.LoadingDetails.Where(r => loadingDetail.Id != r.Id && r.FaultyPartsAssemblyName == loadingDetail.FaultyPartsAssemblyName && r.ResponsibleUnitCode == loadingDetail.ResponsibleUnitCode && r.SerialNumber == loadingDetail.SerialNumber && r.BranchId == loadingDetail.BranchId && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).ToArray();
            if(dbLoadingDetail.Any()) {
                throw new ValidationException("已经存在相同的装车明细");
            }
            var userInfo = Utils.GetCurrentUserInfo();
            loadingDetail.ModifierId = userInfo.Id;
            loadingDetail.ModifierName = userInfo.Name;
            loadingDetail.ModifyTime = DateTime.Now;
        }
        public void UpdateGradeCoefficient(LoadingDetail loadingDetail) {
            UpdateToDatabase(loadingDetail);
            UpdateLoadingDetailVailidate(loadingDetail);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:31
        //原分布类的函数全部转移到Ach                                                               
                public void InsertGradeCoefficient(LoadingDetail loadingDetail) {
            new LoadingDetailAch(this).InsertGradeCoefficient(loadingDetail);
        }
                public void UpdateGradeCoefficient(LoadingDetail loadingDetail) {
            new LoadingDetailAch(this).UpdateGradeCoefficient(loadingDetail);
        }
    }
}
