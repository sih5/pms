﻿using System.Linq;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class MalfunctionViewAch : DcsSerivceAchieveBase {
        public MalfunctionViewAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public IQueryable<MalfunctionView> GetMalfunctionsViewByCategoryAndPartsSalesCategories() {
            return this.ObjectContext.MalfunctionViews.Include("Malfunction").OrderBy(e => e.Code);
        }

        public IQueryable<MalfunctionView> GetMalfunctionsViewByCategoryAndServerProductLineId(int? serviceProductLineId) {
            if(!serviceProductLineId.HasValue)
                return this.ObjectContext.MalfunctionViews.Include("Malfunction").OrderBy(e => e.Code);
            //判断是否为 产品线 = OM-FDJ-ISGFGL  产品线名称= 福康ISG（非公路车） 或者 OM-FDJ-ISGGL   产品线名称=  福康ISG
            var serviceProductLineView = this.ObjectContext.ServiceProductLineViews.Where(r => r.ProductLineType == (int)DcsServiceProductLineViewProductLineType.发动机产品线
                && r.ProductLineId == serviceProductLineId).FirstOrDefault();
            if(serviceProductLineView == null)
                return this.ObjectContext.MalfunctionViews.Include("Malfunction").OrderBy(e => e.Code);
            //if(serviceProductLineView.ProductLineCode == "OM-FDJ-ISGFGL" || serviceProductLineView.ProductLineCode == "OM-FDJ-ISGGL") {
            //    return this.ObjectContext.MalfunctionViews.Include("Malfunction").Where(r => r.Malfunction.ISISG == (int)DcsIsOrNot.是).OrderBy(e => e.Code);
            //}
            return this.ObjectContext.MalfunctionViews.Include("Malfunction").OrderBy(e => e.Code);
        }       


        public IQueryable<MalfunctionView> 查询故障现象视图关联产品线(int serviceProductLineId) {
            //var result = from a in ObjectContext.MalfunctionViews
            //             from b in ObjectContext.RepairItemCategories
            //             from c in ObjectContext.RepairItems
            //             from d in ObjectContext.RepairItemAffiServProdLines
            //             where a.MalfunctionCategoryId == b.Id && b.Id == c.RepairItemCategoryId && c.Id == d.RepairItemId && d.ServiceProductLineId == serviceProductLineId
            //             && b.Status == (int)DcsBaseDataStatus.有效 && c.Status == (int)DcsBaseDataStatus.有效 && d.Status == (int)DcsBaseDataStatus.有效
            //             select a;
            //return result.OrderBy(r => r.Code);
            return null;
        }

    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:31
        //原分布类的函数全部转移到Ach                                                               


        public IQueryable<MalfunctionView> GetMalfunctionsViewByCategoryAndPartsSalesCategories() {
            return new MalfunctionViewAch(this).GetMalfunctionsViewByCategoryAndPartsSalesCategories();
        }

        public IQueryable<MalfunctionView> GetMalfunctionsViewByCategoryAndServerProductLineId(int? serviceProductLineId) {
            return new MalfunctionViewAch(this).GetMalfunctionsViewByCategoryAndServerProductLineId(serviceProductLineId);
        }



        public IQueryable<MalfunctionView> 查询故障现象视图关联产品线(int serviceProductLineId) {
            return new MalfunctionViewAch(this).查询故障现象视图关联产品线(serviceProductLineId);
        }
    }
}
