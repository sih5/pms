﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsManagementCostGradeAch : DcsSerivceAchieveBase {
        internal void InsertPartsManagementCostGradeValidate(PartsManagementCostGrade partsManagementCostGrade) {
            var partsManagementCostGradeName = ObjectContext.PartsManagementCostGrades.Where(r => r.BranchId == partsManagementCostGrade.BranchId && r.Name.ToLower() == partsManagementCostGrade.Name.ToLower() && r.PartsSalesCategoryId == partsManagementCostGrade.PartsSalesCategoryId && r.Status != (int)DcsBaseDataStatus.作废).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(partsManagementCostGradeName != null)
                throw new ValidationException(string.Format(ErrorStrings.PartsManagementCostGrade_Validation1, partsManagementCostGrade.Name));
            var partsManagementCostGradeCode = ObjectContext.PartsManagementCostGrades.Where(r => r.BranchId == partsManagementCostGrade.BranchId && r.Code.ToLower() == partsManagementCostGrade.Code.ToLower() && r.PartsSalesCategoryId == partsManagementCostGrade.PartsSalesCategoryId && r.Status != (int)DcsBaseDataStatus.作废).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(partsManagementCostGradeCode != null)
                throw new ValidationException(string.Format(ErrorStrings.PartsManagementCostGrade_Validation2, partsManagementCostGrade.Code));
            var userInfo = Utils.GetCurrentUserInfo();
            partsManagementCostGrade.CreatorId = userInfo.Id;
            partsManagementCostGrade.CreatorName = userInfo.Name;
            partsManagementCostGrade.CreateTime = DateTime.Now;
        }
        internal void UpdatePartsManagementCostGradeValidate(PartsManagementCostGrade partsManagementCostGrade) {
            var partsManagementCostGradeName = ObjectContext.PartsManagementCostGrades.Where(r => r.Id != partsManagementCostGrade.Id && r.BranchId == partsManagementCostGrade.BranchId && r.Name.ToLower() == partsManagementCostGrade.Name.ToLower() && r.PartsSalesCategoryId == partsManagementCostGrade.PartsSalesCategoryId && r.Status != (int)DcsBaseDataStatus.作废).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(partsManagementCostGradeName != null)
                throw new ValidationException(string.Format(ErrorStrings.PartsManagementCostGrade_Validation1, partsManagementCostGrade.Name));
            var partsManagementCostGradeCode = ObjectContext.PartsManagementCostGrades.Where(r => r.Id != partsManagementCostGrade.Id && r.BranchId == partsManagementCostGrade.BranchId && r.Code.ToLower() == partsManagementCostGrade.Code.ToLower() && r.PartsSalesCategoryId == partsManagementCostGrade.PartsSalesCategoryId && r.Status != (int)DcsBaseDataStatus.作废).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(partsManagementCostGradeCode != null)
                throw new ValidationException(string.Format(ErrorStrings.PartsManagementCostGrade_Validation2, partsManagementCostGrade.Code));
            var userInfo = Utils.GetCurrentUserInfo();
            partsManagementCostGrade.ModifierId = userInfo.Id;
            partsManagementCostGrade.ModifierName = userInfo.Name;
            partsManagementCostGrade.ModifyTime = DateTime.Now;
        }
        public void InsertPartsManagementCostGrade(PartsManagementCostGrade partsManagementCostGrade) {
            InsertToDatabase(partsManagementCostGrade);
            this.InsertPartsManagementCostGradeValidate(partsManagementCostGrade);
        }
        public void UpdatePartsManagementCostGrade(PartsManagementCostGrade partsManagementCostGrade) {
            UpdateToDatabase(partsManagementCostGrade);
            this.UpdatePartsManagementCostGradeValidate(partsManagementCostGrade);
        }
        public PartsManagementCostGrade GetPartsManagementCostGradeWithDetails(int id) {
            var reslut = ObjectContext.PartsManagementCostGrades.SingleOrDefault(r => r.Id == id && r.Status == (int)DcsBaseDataStatus.有效);
            if(reslut != null) {
                var partsManagementCostRates = ObjectContext.PartsManagementCostRates.Where(r => r.PartsManagementCostGradeId == reslut.Id && r.Status == (int)DcsBaseDataStatus.有效).ToArray();
            }
            return reslut;
        }
        public IQueryable<PartsManagementCostGrade> GetPartsManagementCostGradeWithBranch() {
            var usrinfo = Utils.GetCurrentUserInfo();
            return ObjectContext.PartsManagementCostGrades.Where(r => r.Status == (int)DcsMasterDataStatus.有效 && ObjectContext.PersonSalesCenterLinks.Any(x => x.PartsSalesCategoryId == r.PartsSalesCategoryId && x.PersonId == usrinfo.Id && x.Status == (int)DcsBaseDataStatus.有效)).Include("Branch").Include("PartsSalesCategory").OrderBy(entity => entity.Id);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:31
        //原分布类的函数全部转移到Ach                                                               

        public void InsertPartsManagementCostGrade(PartsManagementCostGrade partsManagementCostGrade) {
            new PartsManagementCostGradeAch(this).InsertPartsManagementCostGrade(partsManagementCostGrade);
        }

        public void UpdatePartsManagementCostGrade(PartsManagementCostGrade partsManagementCostGrade) {
            new PartsManagementCostGradeAch(this).UpdatePartsManagementCostGrade(partsManagementCostGrade);
        }

        public PartsManagementCostGrade GetPartsManagementCostGradeWithDetails(int id) {
            return new PartsManagementCostGradeAch(this).GetPartsManagementCostGradeWithDetails(id);
        }

        public IQueryable<PartsManagementCostGrade> GetPartsManagementCostGradeWithBranch() {
            return new PartsManagementCostGradeAch(this).GetPartsManagementCostGradeWithBranch();
        }
    }
}
