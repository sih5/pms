﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsManagementCostRateAch : DcsSerivceAchieveBase {
        internal void InsertPartsManagementCostRateValidate(PartsManagementCostRate partsManagementCostRate) {
            var userInfo = Utils.GetCurrentUserInfo();
            partsManagementCostRate.CreatorId = userInfo.Id;
            partsManagementCostRate.CreatorName = userInfo.Name;
            partsManagementCostRate.CreateTime = DateTime.Now;
        }
        internal void UpdatePartsManagementCostRateValidate(PartsManagementCostRate partsManagementCostRate) {
            var dbPartsManagementCostRate = ObjectContext.PartsManagementCostRates.Where(r => r.Id == partsManagementCostRate.Id && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbPartsManagementCostRate == null) {
                throw new ValidationException(ErrorStrings.PartsManagementCostRate_Validation2);
            }
            var checkPartsManagementCostRate = ObjectContext.PartsManagementCostRates.Where(r => r.PartsManagementCostGradeId == partsManagementCostRate.PartsManagementCostGradeId && r.Id != partsManagementCostRate.Id &&
                !(r.PartsPriceLowerLimit >= partsManagementCostRate.PartsPriceUpperLimit || r.PartsPriceUpperLimit <= partsManagementCostRate.PartsPriceLowerLimit)
                && r.InvoiceType == partsManagementCostRate.InvoiceType && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(checkPartsManagementCostRate != null) {
                throw new ValidationException(ErrorStrings.PartsManagementCostRate_Validation3);
            }
            var userInfo = Utils.GetCurrentUserInfo();
            partsManagementCostRate.ModifierId = userInfo.Id;
            partsManagementCostRate.ModifierName = userInfo.Name;
            partsManagementCostRate.ModifyTime = DateTime.Now;
        }
        public void InsertPartsManagementCostRate(PartsManagementCostRate partsManagementCostRate) {
            InsertToDatabase(partsManagementCostRate);
            this.InsertPartsManagementCostRateValidate(partsManagementCostRate);
        }
        public void UpdatePartsManagementCostRate(PartsManagementCostRate partsManagementCostRate) {
            UpdateToDatabase(partsManagementCostRate);
            this.UpdatePartsManagementCostRateValidate(partsManagementCostRate);
        }
        public IQueryable<PartsManagementCostRate> GetPartsManagementCostRateByDealerIdAndBranchId(int branchId, int dealerId) {
            var partsManagementCostGradeIds = ObjectContext.DealerServiceInfoes.Where(e => e.BranchId == branchId && e.DealerId == dealerId).Select(e => e.PartsManagingFeeGradeId);
            return ObjectContext.PartsManagementCostRates.Where(entity => partsManagementCostGradeIds.Contains(entity.PartsManagementCostGradeId)).OrderBy(r => r.Id);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:31
        //原分布类的函数全部转移到Ach                                                               

        public void InsertPartsManagementCostRate(PartsManagementCostRate partsManagementCostRate) {
            new PartsManagementCostRateAch(this).InsertPartsManagementCostRate(partsManagementCostRate);
        }

        public void UpdatePartsManagementCostRate(PartsManagementCostRate partsManagementCostRate) {
            new PartsManagementCostRateAch(this).UpdatePartsManagementCostRate(partsManagementCostRate);
        }

        public IQueryable<PartsManagementCostRate> GetPartsManagementCostRateByDealerIdAndBranchId(int branchId, int dealerId) {
            return new PartsManagementCostRateAch(this).GetPartsManagementCostRateByDealerIdAndBranchId(branchId,dealerId);
        }
    }
}
