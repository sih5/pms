﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.Odbc;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class QTSDataQueryForLSAch : DcsSerivceAchieveBase {
        public QTSDataQueryForLSAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public IEnumerable<VirtualQTSInfoLS> GetQTSDataQueryForLSByExecuteSql(string sernr, string kdauf, string matnr, DateTime? timeBegin, DateTime? timeEnd) {


            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT SERNR,DATUM,UZEIT,KDAUF,KDPOS,MATNR,CG_MATNR,CHARG,MENGE,BWART,CC_TIMES,MAKTX FROM \"_SYS_BIC\".\"QTS/CAL_LS_QTS\" where SERNR=?");
            OdbcCommand command = new OdbcCommand();  //command  对象
            if(string.IsNullOrWhiteSpace(sernr)) {
                throw new ValidationException("VIN码不能为空");
            }
            command.Parameters.Add(new OdbcParameter("@SERNR", sernr));
            if(!string.IsNullOrWhiteSpace(kdauf)) {
                sql.Append(" and KDAUF=? ");
                command.Parameters.Add(new OdbcParameter("@KDAUF", kdauf));
            }
            if(!string.IsNullOrWhiteSpace(matnr)) {
                sql.Append(" and MATNR=? ");
                command.Parameters.Add(new OdbcParameter("@MATNR", matnr));
            }
            if(timeBegin.HasValue && timeEnd.HasValue) {
                sql.Append(" and CC_TIMES>='" + timeBegin + "' ");
                sql.Append(" and CC_TIMES<='" + timeEnd + "' ");
            } else if(timeBegin.HasValue) {
                sql.Append(" and CC_TIMES>='" + timeBegin + "' ");
            } else if(timeEnd.HasValue) {
                sql.Append(" and CC_TIMES<='" + timeEnd + "' ");
            }
            command.CommandText = sql.ToString();
            DataSet ds = getDataSetBySql(command);

            List<VirtualQTSInfoLS> virtualQtsInfos = new List<VirtualQTSInfoLS>();
            for(int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                VirtualQTSInfoLS virtualQts = new VirtualQTSInfoLS();
                virtualQts.ID = (i + 1).ToString();
                virtualQts.SERNR = ds.Tables[0].Rows[i]["SERNR"].ToString();
                virtualQts.UZEIT = ds.Tables[0].Rows[i]["UZEIT"].ToString();
                virtualQts.KDAUF = ds.Tables[0].Rows[i]["KDAUF"].ToString();
                virtualQts.DATUM = ds.Tables[0].Rows[i]["DATUM"].ToString();
                virtualQts.KDPOS = ds.Tables[0].Rows[i]["KDPOS"].ToString();
                virtualQts.MATNR = ds.Tables[0].Rows[i]["MATNR"].ToString();
                virtualQts.CHARG = ds.Tables[0].Rows[i]["CHARG"].ToString();
                virtualQts.MENGE = ds.Tables[0].Rows[i]["MENGE"].ToString();
                virtualQts.BWART = ds.Tables[0].Rows[i]["BWART"].ToString();
                virtualQts.CC_TIMES = Convert.ToDateTime(ds.Tables[0].Rows[i]["CC_TIMES"]);
                virtualQts.MAKTX = ds.Tables[0].Rows[i]["MAKTX"].ToString();
                virtualQts.CG_MATNR = ds.Tables[0].Rows[i]["CG_MATNR"].ToString();
                virtualQtsInfos.Add(virtualQts);
            }

            return virtualQtsInfos;

        }
       

        public DataSet getDataSetBySql(OdbcCommand command) {

            try {

                DataSet ds = new DataSet();


                String connstring = WebConfigurationManager.AppSettings["QTSODBCAddress"];  //ODBC连接字符串

                using(OdbcConnection connection = new OdbcConnection(connstring))  //创建connection连接对象
                {

                    command.Connection = connection;

                    connection.Open();  //打开链接

                    OdbcDataAdapter adapter = new OdbcDataAdapter(command);  //实例化dataadapter

                    adapter.Fill(ds);  //填充查询结果

                    return ds;

                }

            } catch(Exception ex) {

                throw new Exception(ex.Message);

            }

        }   
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:31
        //原分布类的函数全部转移到Ach                                                               
        public IEnumerable<VirtualQTSInfoLS> GetQTSDataQueryForLSByExecuteSql(string sernr, string kdauf, string matnr, DateTime? timeBegin, DateTime? timeEnd) {
            return new QTSDataQueryForLSAch(this).GetQTSDataQueryForLSByExecuteSql(sernr, kdauf, matnr, timeBegin, timeEnd);
        }
       
    }
}
