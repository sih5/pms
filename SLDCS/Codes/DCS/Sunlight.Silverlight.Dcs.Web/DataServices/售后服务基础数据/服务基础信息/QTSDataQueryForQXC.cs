﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.Objects;
using System.Data.Odbc;
using System.Reflection;
using System.Text;
using System.Linq;
using System.Web.Configuration;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class QTSDataQueryForQXCAch : DcsSerivceAchieveBase {
        public QTSDataQueryForQXCAch(DcsDomainService domainService)
            : base(domainService) {
        }
        
//        public IEnumerable<QTSDataQueryForQXC> GetQtsDataQueryForQxcByExecuteSql(string vin) {
//            var sql = @"select a.Cscode,
//         a.Zcbmcode,
//         a.Vincode,
//         b.Assembly_date,
//         a.Bind_date,
//         b.Factory,
//         b.Mapid,
//         b.Patch,
//         b.Produceline,
//         b.Mapname,
//         a.submittime
//     from qts.o_vinbind @sddgn_145 a
//    left join qts.o_tmepassembly_cs @sddgn_145 b on a.cscode = b.cscode
//    where a.Vincode=:vin";
//            var search = ObjectContext.ExecuteStoreQuery<QTSDataQueryForQXC>(sql, "QTSDataQueryForQXCs", MergeOption.AppendOnly, (new Devart.Data.Oracle.OracleParameter("vin", vin) as object));
//            var alist = new List<QTSDataQueryForQXC>(search);
//            return alist;
//        }
        public IEnumerable<VirtualQTSInfo> GetQtsDataQueryForQxcByExecuteSql(string vin, string mapid, string csCode, string zcbmCode, string mapName, string factory, DateTime? timeBegin, DateTime? timeEnd) {


            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT CSCODE,PRODUCTNO,ZCBMCODE,ASSEMBLY_DATE, BIND_DATE,FACTORY,MAPID,PATCH,VIN,MAPNAME,SOURCE_CODE,GONGYINGSBIANHAO,GONGYINGSNAME,CHUCHANGID,CHANGXIAN FROM \"_SYS_BIC\".\"QTS/CAL_QTS_MERGE\" where SOURCE_CODE in ('QTS_AL','QTS_BJDGN','QTS_SDDGN')and VIN=?");
            OdbcCommand command = new OdbcCommand();  //command  对象
            if(string.IsNullOrWhiteSpace(vin)) {
                throw new ValidationException("VIN码不能为空");
            }
            command.Parameters.Add(new OdbcParameter("@VIN", vin));
            if(!string.IsNullOrWhiteSpace(mapid)) {
                sql.Append(" and MAPID=? ");
                command.Parameters.Add(new OdbcParameter("@MAPID", mapid));
            }
            if(!string.IsNullOrWhiteSpace(csCode)) {
                sql.Append(" and CSCODE=? ");
                command.Parameters.Add(new OdbcParameter("@CSCODE", csCode));
            }
            if(!string.IsNullOrWhiteSpace(zcbmCode)) {
                sql.Append(" and ZCBMCODE=? ");
                command.Parameters.Add(new OdbcParameter("@ZCBMCODE", zcbmCode));
            }
            if(!string.IsNullOrWhiteSpace(mapName)) {
                sql.Append(" and MAPNAME=? ");
                command.Parameters.Add(new OdbcParameter("@MAPNAME", mapName));
            }
            if(!string.IsNullOrWhiteSpace(factory)) {
                sql.Append(" and FACTORY=? ");
                command.Parameters.Add(new OdbcParameter("@FACTORY", factory));
            }
            if(timeBegin.HasValue && timeEnd.HasValue) {
                sql.Append(" and ASSEMBLY_DATE>='" + timeBegin + "' ");
                sql.Append(" and ASSEMBLY_DATE<='" + timeEnd + "' ");
            } else if(timeBegin.HasValue) {
                sql.Append(" and ASSEMBLY_DATE>='" + timeBegin + "' ");
            } else if(timeEnd.HasValue) {
                sql.Append(" and ASSEMBLY_DATE<='" + timeEnd + "' ");
            }
            command.CommandText = sql.ToString();
            DataSet ds = getDataSetBySql(command);

            List<VirtualQTSInfo> virtualQtsInfos = new List<VirtualQTSInfo>();
            for(int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                VirtualQTSInfo virtualQts = new VirtualQTSInfo();
                virtualQts.ID = (i + 1).ToString();
                virtualQts.CSCODE = ds.Tables[0].Rows[i]["CSCODE"].ToString();
                virtualQts.PRODUCTNO = ds.Tables[0].Rows[i]["PRODUCTNO"].ToString();
                virtualQts.ZCBMCODE = ds.Tables[0].Rows[i]["ZCBMCODE"].ToString();
                virtualQts.ASSEMBLY_DATE = Convert.ToDateTime(ds.Tables[0].Rows[i]["ASSEMBLY_DATE"]);
                virtualQts.BIND_DATE = ds.Tables[0].Rows[i]["BIND_DATE"].ToString();
                virtualQts.FACTORY = ds.Tables[0].Rows[i]["FACTORY"].ToString();
                virtualQts.MAPID = ds.Tables[0].Rows[i]["MAPID"].ToString();
                virtualQts.PATCH = ds.Tables[0].Rows[i]["PATCH"].ToString();
                virtualQts.VIN = ds.Tables[0].Rows[i]["VIN"].ToString();
                virtualQts.MAPNAME = ds.Tables[0].Rows[i]["MAPNAME"].ToString();
                virtualQts.SOURCE_CODE = ds.Tables[0].Rows[i]["SOURCE_CODE"].ToString();
                virtualQts.GONGYINGSBIANHAO = ds.Tables[0].Rows[i]["GONGYINGSBIANHAO"].ToString();
                virtualQts.CHUCHANGID = ds.Tables[0].Rows[i]["CHUCHANGID"].ToString();
                virtualQts.CHANGXIAN = ds.Tables[0].Rows[i]["CHANGXIAN"].ToString();
                virtualQtsInfos.Add(virtualQts); 
            }

            return virtualQtsInfos;

        }
        

        public DataSet getDataSetBySql(OdbcCommand command) {

            try {

                DataSet ds = new DataSet();


                String connstring = WebConfigurationManager.AppSettings["QTSODBCAddress"];  //ODBC连接字符串

                using(OdbcConnection connection = new OdbcConnection(connstring))  //创建connection连接对象
                {

                    command.Connection = connection;

                    connection.Open();  //打开链接

                    OdbcDataAdapter adapter = new OdbcDataAdapter(command);  //实例化dataadapter

                    adapter.Fill(ds);  //填充查询结果

                    return ds;

                }

            } catch(Exception ex) {

                throw new Exception(ex.Message);

            }

        }   
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:31
        //原分布类的函数全部转移到Ach                                                               


        public IEnumerable<VirtualQTSInfo> GetQtsDataQueryForQxcByExecuteSql(string vin, string mapid, string csCode, string zcbmCode, string mapName, string factory, DateTime? timeBegin, DateTime? timeEnd) {
            return new QTSDataQueryForQXCAch(this).GetQtsDataQueryForQxcByExecuteSql(vin, mapid, csCode, zcbmCode, mapName, factory, timeBegin, timeEnd);
        }
    }
}
