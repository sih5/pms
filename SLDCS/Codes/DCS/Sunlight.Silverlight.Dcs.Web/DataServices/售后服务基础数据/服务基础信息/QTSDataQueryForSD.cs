﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.Objects;
using System.Data.Odbc;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web.Configuration;
using Devart.Data.Oracle;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class QTSDataQueryForSDAch : DcsSerivceAchieveBase {
        public QTSDataQueryForSDAch(DcsDomainService domainService)
            : base(domainService) {
        }

//        public IEnumerable<QTSDataQueryForSD> GetQTSDataQueryForSDByExecuteSql(string vin, string mapid, string chuchangid, string zcbmCode, string mapName, string factory, DateTime? timeBegin, DateTime? timeEnd) {

//            var sqlBulider = new StringBuilder();
//            sqlBulider.Append(@" select productline as produceline,
//                                vincode,
//                                detailid,
//                                zcbmcode,
//                                assembly_date,
//                                mapid,
//                                mapname,
//                                patch,
//                                chuchangid,
//                                factory
//                            from wms.vw_pms @QTS_115 where ");
//            var paramaterList = new List<Object>();
//            if(string.IsNullOrWhiteSpace(vin) && string.IsNullOrWhiteSpace(chuchangid)) {
//                throw new ValidationException("VIN码或出厂编码不能为空");
//            }
//            if(!string.IsNullOrWhiteSpace(vin)) {
//                sqlBulider.Append(" and vincode=:vin ");
//                paramaterList.Add((object)new OracleParameter("vin", vin));
//            }
//            if(!string.IsNullOrWhiteSpace(chuchangid)) {
//                sqlBulider.Append(" and chuchangid=:chuchangid ");
//                paramaterList.Add((object)new OracleParameter("chuchangid", chuchangid));
//            }
//            //paramaterList.Add(new OracleParameter("vin", vin));
//            if(!string.IsNullOrWhiteSpace(mapid)) {
//                sqlBulider.Append(" and mapid=:mapId ");
//                paramaterList.Add((object)new OracleParameter("mapid", mapid));
//            }
//            if(!string.IsNullOrWhiteSpace(zcbmCode)) {
//                sqlBulider.Append(" and zcbmCode=:zcbmCode ");
//                paramaterList.Add((object)new OracleParameter("zcbmCode", zcbmCode));
//            }
//            if(!string.IsNullOrWhiteSpace(mapName)) {
//                sqlBulider.Append(" and mapName=:mapName ");
//                paramaterList.Add((object)new OracleParameter("mapName", mapName));
//            }
//            if(!string.IsNullOrWhiteSpace(factory)) {
//                sqlBulider.Append(" and factory=:factory ");
//                paramaterList.Add((object)new OracleParameter("factory", factory));
//            }
//            if(timeBegin.HasValue) {
//                sqlBulider.Append(" and assembly_date>=:assembly_dateBegin ");
//                paramaterList.Add((object)new OracleParameter("assembly_dateBegin", OracleDbType.Date, timeBegin.Value, System.Data.ParameterDirection.Input));
//            }
//            if(timeEnd.HasValue) {
//                sqlBulider.Append(" and assembly_date<:assembly_dateEnd ");
//                paramaterList.Add((object)new OracleParameter("assembly_dateEnd", OracleDbType.Date, timeEnd.Value, System.Data.ParameterDirection.Input));
//            }
//            var sql = sqlBulider.ToString();
//            if(sql.IndexOf(" where  and ") > 0)
//                sql = sql.Replace(" where  and ", " where ");
//            else
//                sql = sql.Replace(" where ", "");
//            var search = ObjectContext.ExecuteStoreQuery<QTSDataQueryForSD>(sql, "QTSDataQueryForSDs", MergeOption.AppendOnly, paramaterList.ToArray());
//            var alist = new List<QTSDataQueryForSD>(search);
//            return alist.Distinct();
//        }
        public IEnumerable<VirtualQTSInfo> GetQTSDataQueryForSDByExecuteSql(string vin, string mapid, string csCode, string zcbmCode, string mapName, string factory, DateTime? timeBegin, DateTime? timeEnd) {


            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT CSCODE,PRODUCTNO,ZCBMCODE,ASSEMBLY_DATE, BIND_DATE,FACTORY,MAPID,PATCH,VIN,MAPNAME,SOURCE_CODE,GONGYINGSBIANHAO,GONGYINGSNAME,CHUCHANGID,CHANGXIAN FROM \"_SYS_BIC\".\"QTS/CAL_QTS_MERGE\" where SOURCE_CODE='QTS_AL' and VIN=?");
            OdbcCommand command = new OdbcCommand();  //command  对象
            if(string.IsNullOrWhiteSpace(vin)) {
                throw new ValidationException("VIN码不能为空");
            }
            command.Parameters.Add(new OdbcParameter("@VIN", vin));
            if(!string.IsNullOrWhiteSpace(mapid)) {
                sql.Append(" and MAPID=? ");
                command.Parameters.Add(new OdbcParameter("@MAPID", mapid));
            }
            if(!string.IsNullOrWhiteSpace(csCode)) {
                sql.Append(" and CSCODE=? ");
                command.Parameters.Add(new OdbcParameter("@CSCODE", csCode));
            }
            if(!string.IsNullOrWhiteSpace(zcbmCode)) {
                sql.Append(" and ZCBMCODE=? ");
                command.Parameters.Add(new OdbcParameter("@ZCBMCODE", zcbmCode));
            }
            if(!string.IsNullOrWhiteSpace(mapName)) {
                sql.Append(" and MAPNAME=? ");
                command.Parameters.Add(new OdbcParameter("@MAPNAME", mapName));
            }
            if(!string.IsNullOrWhiteSpace(factory)) {
                sql.Append(" and FACTORY=? ");
                command.Parameters.Add(new OdbcParameter("@FACTORY", factory));
            }
            if(timeBegin.HasValue && timeEnd.HasValue) {
                sql.Append(" and ASSEMBLY_DATE>='" + timeBegin + "' ");
                sql.Append(" and ASSEMBLY_DATE<='" + timeEnd + "' ");
            } else if(timeBegin.HasValue) {
                sql.Append(" and ASSEMBLY_DATE>='" + timeBegin + "' ");
            } else if(timeEnd.HasValue) {
                sql.Append(" and ASSEMBLY_DATE<='" + timeEnd + "' ");
            }
            command.CommandText = sql.ToString();
            DataSet ds = getDataSetBySql(command);

            List<VirtualQTSInfo> virtualQtsInfos = new List<VirtualQTSInfo>();
            for(int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                VirtualQTSInfo virtualQts = new VirtualQTSInfo();
                virtualQts.ID = (i + 1).ToString();
                virtualQts.CSCODE = ds.Tables[0].Rows[i]["CSCODE"].ToString();
                virtualQts.PRODUCTNO = ds.Tables[0].Rows[i]["PRODUCTNO"].ToString();
                virtualQts.ZCBMCODE = ds.Tables[0].Rows[i]["ZCBMCODE"].ToString();
                virtualQts.ASSEMBLY_DATE = Convert.ToDateTime(ds.Tables[0].Rows[i]["ASSEMBLY_DATE"]);
                virtualQts.BIND_DATE = ds.Tables[0].Rows[i]["BIND_DATE"].ToString();
                virtualQts.FACTORY = ds.Tables[0].Rows[i]["FACTORY"].ToString();
                virtualQts.MAPID = ds.Tables[0].Rows[i]["MAPID"].ToString();
                virtualQts.PATCH = ds.Tables[0].Rows[i]["PATCH"].ToString();
                virtualQts.VIN = ds.Tables[0].Rows[i]["VIN"].ToString();
                virtualQts.MAPNAME = ds.Tables[0].Rows[i]["MAPNAME"].ToString();
                virtualQts.SOURCE_CODE = ds.Tables[0].Rows[i]["SOURCE_CODE"].ToString();
                virtualQts.GONGYINGSBIANHAO = ds.Tables[0].Rows[i]["GONGYINGSBIANHAO"].ToString();
                virtualQts.CHUCHANGID = ds.Tables[0].Rows[i]["CHUCHANGID"].ToString();
                virtualQts.CHANGXIAN = ds.Tables[0].Rows[i]["CHANGXIAN"].ToString();
                virtualQtsInfos.Add(virtualQts); 
            }

            return virtualQtsInfos;

        }
        

        public DataSet getDataSetBySql(OdbcCommand command) {

            try {

                DataSet ds = new DataSet();


                String connstring = WebConfigurationManager.AppSettings["QTSODBCAddress"];  //ODBC连接字符串

                using(OdbcConnection connection = new OdbcConnection(connstring))  //创建connection连接对象
                {

                    command.Connection = connection;

                    connection.Open();  //打开链接

                    OdbcDataAdapter adapter = new OdbcDataAdapter(command);  //实例化dataadapter

                    adapter.Fill(ds);  //填充查询结果

                    return ds;

                }

            } catch(Exception ex) {

                throw new Exception(ex.Message);

            }

        }   
    
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:31
        //原分布类的函数全部转移到Ach                                                               

        public IEnumerable<VirtualQTSInfo> GetQTSDataQueryForSDByExecuteSql(string vin, string mapid, string csCode, string zcbmCode, string mapName, string factory, DateTime? timeBegin, DateTime? timeEnd) {
            return new QTSDataQueryForSDAch(this).GetQTSDataQueryForSDByExecuteSql(vin, mapid, csCode, zcbmCode, mapName, factory, timeBegin, timeEnd);
        }
    }
}
