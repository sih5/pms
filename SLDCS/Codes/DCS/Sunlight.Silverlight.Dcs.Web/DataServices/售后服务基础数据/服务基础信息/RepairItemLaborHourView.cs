﻿using System.Linq;
using Microsoft.Data.Extensions;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class RepairItemLaborHourViewAch : DcsSerivceAchieveBase {
        public RepairItemLaborHourViewAch(DcsDomainService domainService)
            : base(domainService) {
        }     
        public IQueryable<RepairItemLaborHourView> 查询维修项目工时定额视图(int? serviceProductLine) {
            var result = from a in ObjectContext.RepairItemLaborHourViews
                         where (serviceProductLine.HasValue || a.ServiceProductLineId == serviceProductLine.Value)
                         select a;
            return result.OrderBy(r => r.Code);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:31
        //原分布类的函数全部转移到Ach                                                               

        public IQueryable<RepairItemLaborHourView> 查询维修项目工时定额视图(int? serviceProductLine) {
            return new RepairItemLaborHourViewAch(this).查询维修项目工时定额视图(serviceProductLine);
        }
    }
}
