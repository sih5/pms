﻿using System.Linq;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class RepairItemViewAch : DcsSerivceAchieveBase {
        public RepairItemViewAch(DcsDomainService domainService)
            : base(domainService) {
        }
        

        public IQueryable<RepairItemView> 查询维修项目() {
            return ObjectContext.RepairItemViews;
        }
        public IQueryable<RepairItemView> GetRepairItemViewWithPartsSalesCategory() {
            return ObjectContext.RepairItemViews.Include("PartsSalesCategory").Include("RepairItem").OrderBy(e => e.RepairItemId);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:31
        //原分布类的函数全部转移到Ach                                                               

        public IQueryable<RepairItemView> 查询维修项目() {
            return new RepairItemViewAch(this).查询维修项目();
        }

        public IQueryable<RepairItemView> GetRepairItemViewWithPartsSalesCategory() {
            return new RepairItemViewAch(this).GetRepairItemViewWithPartsSalesCategory();
        }
    }
}