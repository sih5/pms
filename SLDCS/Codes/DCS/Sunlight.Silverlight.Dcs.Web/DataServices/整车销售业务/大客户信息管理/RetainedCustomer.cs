﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class RetainedCustomerAch : DcsSerivceAchieveBase {
        public RetainedCustomerAch(DcsDomainService domainService)
            : base(domainService) {
        }

        internal void InsertRetainedCustomerValidate(RetainedCustomer retainedCustomer) {
            //var retainedCustomerVehicleLists = retainedCustomer.RetainedCustomerVehicleLists;
            //foreach(RetainedCustomerVehicleList item in retainedCustomerVehicleLists) {
            //    new RetainedCustomerVehicleListAch(this.DomainService).InsertRetainedCustomerVehicleListValidate(item);
            //}
            var userInfo = Utils.GetCurrentUserInfo();
            retainedCustomer.CreatorId = userInfo.Id;
            retainedCustomer.CreatorName = userInfo.Name;
            retainedCustomer.CreateTime = DateTime.Now;
        }
        internal void UpdateRetainedCustomerValidate(RetainedCustomer retainedCustomer) {
            var dbRetainedCustomer = ObjectContext.RetainedCustomers.Where(r => r.Id == retainedCustomer.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbRetainedCustomer == null) {
                throw new ValidationException(ErrorStrings.Common_Validation2);
            }
            if(dbRetainedCustomer.Status != (int)DcsBaseDataStatus.有效) {
                throw new ValidationException(ErrorStrings.Common_Validation3);
            }
            var userInfo = Utils.GetCurrentUserInfo();
            retainedCustomer.ModifierId = userInfo.Id;
            retainedCustomer.ModifierName = userInfo.Name;
            retainedCustomer.ModifyTime = DateTime.Now;
            //var lists = retainedCustomer.RetainedCustomerVehicleLists;
            //foreach(var item in lists) {
            //    switch(item.EntityState) {
            //        case EntityState.Added:
            //            new RetainedCustomerVehicleListAch(this.DomainService).InsertRetainedCustomerVehicleListValidate(item);
            //            break;
            //        case EntityState.Modified:
            //            new RetainedCustomerVehicleListAch(this.DomainService).UpdateRetainedCustomerVehicleListValidate(item);
            //            break;
            //    }
            //}
        }
        public void InsertRetainedCustomer(RetainedCustomer retainedCustomer) {
            InsertToDatabase(retainedCustomer);
            //var retainedCustomerVehicleLists = ChangeSet.GetAssociatedChanges(retainedCustomer, r => r.RetainedCustomerVehicleLists, ChangeOperation.Insert);
            //foreach(RetainedCustomerVehicleList retainedCustomerVehicleList in retainedCustomerVehicleLists)
            //    InsertToDatabase(retainedCustomerVehicleList);
            this.InsertRetainedCustomerValidate(retainedCustomer);
        }
        public void UpdateRetainedCustomer(RetainedCustomer retainedCustomer) {
            //retainedCustomer.RetainedCustomerVehicleLists.Clear();
            //UpdateToDatabase(retainedCustomer);
            //var retainedCustomerVehicleLists = ChangeSet.GetAssociatedChanges(retainedCustomer, r => r.RetainedCustomerVehicleLists);
            //foreach(RetainedCustomerVehicleList retainedCustomerVehicleList in retainedCustomerVehicleLists) {
            //    switch(ChangeSet.GetChangeOperation(retainedCustomerVehicleList)) {
            //        case ChangeOperation.Insert:
            //            InsertToDatabase(retainedCustomerVehicleList);
            //            break;
            //        case ChangeOperation.Update:
            //            UpdateToDatabase(retainedCustomerVehicleList);
            //            break;
            //        case ChangeOperation.Delete:
            //            DeleteFromDatabase(retainedCustomerVehicleList);
            //            break;
            //    }
            //}
            this.UpdateRetainedCustomerValidate(retainedCustomer);
        }
        public IQueryable<RetainedCustomer> GetRetainedCustomerWithCustomerDetailById() {
            return ObjectContext.RetainedCustomers.Include("VehicleLinkmen").Include("Customer").OrderBy(e => e.Id);
        }
        public IQueryable<RetainedCustomer> GetRetainedCustomerWithCustomerDetail() {
            return ObjectContext.RetainedCustomers.Where(r => r.Id != -1).Include("Customer").OrderBy(e => e.Id);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:32
        //原分布类的函数全部转移到Ach                                                               

        public void InsertRetainedCustomer(RetainedCustomer retainedCustomer) {
            new RetainedCustomerAch(this).InsertRetainedCustomer(retainedCustomer);
        }

        public void UpdateRetainedCustomer(RetainedCustomer retainedCustomer) {
            new RetainedCustomerAch(this).UpdateRetainedCustomer(retainedCustomer);
        }

        public IQueryable<RetainedCustomer> GetRetainedCustomerWithCustomerDetailById() {
            return new RetainedCustomerAch(this).GetRetainedCustomerWithCustomerDetailById();
        }

        public IQueryable<RetainedCustomer> GetRetainedCustomerWithCustomerDetail() {
            return new RetainedCustomerAch(this).GetRetainedCustomerWithCustomerDetail();
        }
    }
}
