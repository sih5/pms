﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class AuthenticationTypeAch : DcsSerivceAchieveBase {

        internal void InsertAuthenticationTypeValidate(AuthenticationType authenticationType) {
            var dbAuthenticationType = ObjectContext.AuthenticationTypes.Where(r => r.AuthenticationTypeName.ToLower() == authenticationType.AuthenticationTypeName.ToLower() && r.BranchId == authenticationType.BranchId).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbAuthenticationType != null)
                throw new ValidationException(string.Format(ErrorStrings.AuthenticationType_Validation1, authenticationType.AuthenticationTypeName));
            var userInfo = Utils.GetCurrentUserInfo();
            authenticationType.CreatorId = userInfo.Id;
            authenticationType.CreatorName = userInfo.Name;
            authenticationType.CreateTime = DateTime.Now;
        }

        public void InsertAuthenticationType(AuthenticationType authenticationType) {
            InsertToDatabase(authenticationType);
            this.InsertAuthenticationTypeValidate(authenticationType);
        }
        
        internal void UpdateAuthenticationTypeValidate(AuthenticationType authenticationType) {
            var userInfo = Utils.GetCurrentUserInfo();
            authenticationType.ModifyId = userInfo.Id;
            authenticationType.ModifyName = userInfo.Name;
            authenticationType.ModifyTime = DateTime.Now;
        }

        public void UpdateAuthenticationType(AuthenticationType authenticationType) {
            UpdateToDatabase(authenticationType);
            this.UpdateAuthenticationTypeValidate(authenticationType);
        }
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:32
        //原分布类的函数全部转移到Ach                                                               

        public void InsertAuthenticationType(AuthenticationType authenticationType) {
            new AuthenticationTypeAch(this).InsertAuthenticationType(authenticationType);
        }
        public void UpdateAuthenticationType(AuthenticationType authenticationType) {
            new AuthenticationTypeAch(this).UpdateAuthenticationType(authenticationType);
        }
    }
}
