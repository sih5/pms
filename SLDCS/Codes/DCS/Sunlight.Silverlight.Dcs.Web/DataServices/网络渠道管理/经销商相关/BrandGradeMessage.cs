﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class BrandGradeMessageAch : DcsSerivceAchieveBase {
        internal void InsertBrandGradeMessageVailidate(BrandGradeMessage brandGradeMessage) {
            var dbBrandGradeMessage = ObjectContext.BrandGradeMessages.Where(r => r.Grade == brandGradeMessage.Grade && r.BrandId == brandGradeMessage.BrandId && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbBrandGradeMessage != null) {
                throw new ValidationException(ErrorStrings.GradeCoefficient_Validation1);
            }
            var userInfo = Utils.GetCurrentUserInfo();
            brandGradeMessage.CreatorId = userInfo.Id;
            brandGradeMessage.CreatorName = userInfo.Name;
            brandGradeMessage.CreateTime = DateTime.Now;
        }
        internal void UnsertBrandGradeMessageVailidate(BrandGradeMessage brandGradeMessage) {
            var dbBrandGradeMessage = ObjectContext.BrandGradeMessages.Where(r => r.Id == brandGradeMessage.Id && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbBrandGradeMessage == null) {
                throw new ValidationException(ErrorStrings.GradeCoefficient_Validation2);
            }
            var checkBrandGradeMessage = ObjectContext.BrandGradeMessages.Where(r => r.Grade == brandGradeMessage.Grade && r.BrandId == brandGradeMessage.BrandId && r.Status == (int)DcsBaseDataStatus.有效 && r.Id != brandGradeMessage.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(checkBrandGradeMessage != null) {
                throw new ValidationException(ErrorStrings.GradeCoefficient_Validation3);
            }
            var userInfo = Utils.GetCurrentUserInfo();
            brandGradeMessage.ModifierId = userInfo.Id;
            brandGradeMessage.ModifierName = userInfo.Name;
            brandGradeMessage.ModifyTime = DateTime.Now;
        }
        public void InsertBrandGradeMessage(BrandGradeMessage brandGradeMessage) {
            InsertToDatabase(brandGradeMessage);
            InsertBrandGradeMessageVailidate(brandGradeMessage);
        }
        public void UpdateBrandGradeMessage(BrandGradeMessage brandGradeMessage) {
            UpdateToDatabase(brandGradeMessage);
            UnsertBrandGradeMessageVailidate(brandGradeMessage);
        }

    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:32
        //原分布类的函数全部转移到Ach                                                               

        public void InsertBrandGradeMessage(BrandGradeMessage brandGradeMessage) {
            new BrandGradeMessageAch(this).InsertBrandGradeMessage(brandGradeMessage);
        }

        public void UpdateBrandGradeMessage(BrandGradeMessage brandGradeMessage) {
            new BrandGradeMessageAch(this).UpdateBrandGradeMessage(brandGradeMessage);
        }
    }
}
