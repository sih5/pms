﻿using System;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class ChannelCapabilityAch : DcsSerivceAchieveBase {
        internal void InsertChannelCapabilityValidate(ChannelCapability channelCapability) {
            var userInfo = Utils.GetCurrentUserInfo();
            channelCapability.CreatorId = userInfo.Id;
            channelCapability.CreatorName = userInfo.Name;
            channelCapability.CreateTime = DateTime.Now;
        }

        internal void UpdateChannelCapabilityValidate(ChannelCapability channelCapability) {
            var userInfo = Utils.GetCurrentUserInfo();
            channelCapability.ModifierId = userInfo.Id;
            channelCapability.ModifierName = userInfo.Name;
            channelCapability.ModifyTime = DateTime.Now;
        }

        public void InsertChannelCapability(ChannelCapability channelCapability) {
            InsertToDatabase(channelCapability);
            this.InsertChannelCapabilityValidate(channelCapability);
        }

        public void UpdateChannelCapability(ChannelCapability channelCapability) {
            UpdateToDatabase(channelCapability);
            this.UpdateChannelCapabilityValidate(channelCapability);
        }
    
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:32
        //原分布类的函数全部转移到Ach                                                               

        public void InsertChannelCapability(ChannelCapability channelCapability) {
            new ChannelCapabilityAch(this).InsertChannelCapability(channelCapability);
        }

        public void UpdateChannelCapability(ChannelCapability channelCapability) {
            new ChannelCapabilityAch(this).UpdateChannelCapability(channelCapability);
        }
    }
}
