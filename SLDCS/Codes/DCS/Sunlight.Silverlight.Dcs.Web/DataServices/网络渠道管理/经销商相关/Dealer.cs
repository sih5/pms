﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DealerAch : DcsSerivceAchieveBase {
        internal void InsertDealerValidate(Dealer dealer) {
            var dbdealer = ObjectContext.Dealers.Where(r => r.Code == dealer.Code && r.Status != (int)DcsMasterDataStatus.作废).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbdealer != null)
                throw new ValidationException(string.Format(ErrorStrings.Dealer_Validation1, dealer.Code));

            var userInfo = Utils.GetCurrentUserInfo();
            dealer.CreatorId = userInfo.Id;
            dealer.CreatorName = userInfo.Name;
            dealer.CreateTime = DateTime.Now;
        }

        internal void UpdateDealerValidate(Dealer dealer) {
            var dbdealer = ObjectContext.Dealers.Where(r => r.Id != dealer.Id && r.Code == dealer.Code && r.Status != (int)DcsMasterDataStatus.作废).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbdealer != null)
                throw new ValidationException(string.Format(ErrorStrings.Dealer_Validation1, dealer.Code));

            var userInfo = Utils.GetCurrentUserInfo();
            dealer.ModifierId = userInfo.Id;
            dealer.ModifierName = userInfo.Name;
            dealer.ModifyTime = DateTime.Now;
        }

        public void InsertDealer(Dealer dealer) {
            InsertToDatabase(dealer);
            this.InsertDealerValidate(dealer);
        }

        public void UpdateDealer(Dealer dealer) {
            UpdateToDatabase(dealer);
            this.UpdateDealerValidate(dealer);
        }

        public IQueryable<Dealer> 查询经销商信息带服务扩展信息(int? branchId, int? dealerServiceInfoStatus) {
            IQueryable<DealerServiceInfo> dealerServiceInfoes = ObjectContext.DealerServiceInfoes;
            //拼接查询：DealerServiceInfo对应的条件
            if(branchId.HasValue)
                dealerServiceInfoes = dealerServiceInfoes.Where(r => r.BranchId == branchId);
            if(dealerServiceInfoStatus.HasValue)
                dealerServiceInfoes = dealerServiceInfoes.Where(r => r.Status == dealerServiceInfoStatus);
            //拼接查询：经销商信息
            var result = ObjectContext.Dealers.Where(v => dealerServiceInfoes.Any(info => info.DealerId == v.Id)).OrderBy(r => r.Id);

            //获取数据：拼接客户端条件（分页）后的经销商信息Id
            var dealerIds = ((IQueryable<Dealer>)DcsDomainService.QueryComposer.Compose(result, ParamQueryable)).Select(v => v.Id).ToArray();
            //获取数据：对应的关联单数据（如果关联单对应的表数据量大，考虑不适用Include，根据关联字段值，分多次查询）
            var resultDealerServiceInfoes = dealerServiceInfoes.Where(v => dealerIds.Contains(v.DealerId)).Include("PartsSalesCategory").Include("MarketingDepartment").Include("PartsManagementCostGrade").Include("Branch").Include("GradeCoefficient").Include("ChannelCapability").ToArray();
            var resultDealerServiceExts = ObjectContext.DealerServiceExts.Where(v => dealerIds.Contains(v.Id)).ToArray();

            //返回主单查询IQueryable
            return result;
        }

        public Dealer GetDealerWithCompany(int id) {
            var userInfo = Utils.GetCurrentUserInfo();
            var dealer = ObjectContext.Dealers.FirstOrDefault(r => r.Id == id && r.Status == (int)DcsMasterDataStatus.有效);
            if(dealer != null) {
                var company = ObjectContext.Companies.FirstOrDefault(r => r.Id == id && r.Status == (int)DcsMasterDataStatus.有效);
                var dealerServiceExt = ObjectContext.DealerServiceExts.FirstOrDefault(e => e.Id == id);
                var companyInvoiceInfo = ObjectContext.CompanyInvoiceInfoes.FirstOrDefault(r => r.CompanyId == id && r.Status == (int)DcsMasterDataStatus.有效 && r.InvoiceCompanyId == userInfo.EnterpriseId);
            }
            return dealer;
        }

        public IQueryable<Dealer> GetDealersWithDealerServiceInfoes() {
            return ObjectContext.Dealers.Include("DealerServiceInfoes.Branch").Include("DealerServiceInfoes.PartsManagementCostGrade").Include("DealerKeyEmployees").OrderBy(r => r.Id);
        }

        public IQueryable<Dealer> GetDealersWithDealer() {
            return ObjectContext.Dealers.Include("DealerServiceInfoes").Include("DealerServiceInfoes.PartsManagementCostGrade").Include("DealerServiceExt").OrderBy(entity => entity.Id);
        }

        public Dealer GetDealersWithDealerServiceExt(int id) {
            var dbDealer = ObjectContext.Dealers.SingleOrDefault(e => e.Id == id);
            if(dbDealer != null) {
                var dbDealerServiceExt = ObjectContext.DealerServiceExts.SingleOrDefault(e => e.Id == id);
                var companyInvoiceInfo = ObjectContext.CompanyInvoiceInfoes.FirstOrDefault(r => r.CompanyId == id && r.Status == (int)DcsMasterDataStatus.有效);
            }
            return dbDealer;
        }

        public IQueryable<Dealer> GetDealersWithDealerServiceInfoesAndDetails() {
            return ObjectContext.Dealers.Include("DealerServiceInfoes").Include("DealerServiceInfoes.Branch")
                .Include("DealerServiceInfoes.PartsManagementCostGrade").Include("DealerServiceInfoes.PartsSalesCategory")
                .Include("DealerServiceInfoes.ChannelCapability")
                .Include("DealerServiceInfoes.GradeCoefficient")
                .Include("DealerServiceInfoes.MarketingDepartment")
                .Include("DealerKeyEmployees").Include("DealerKeyEmployees.DealerKeyPosition").OrderBy(r => r.Id);
        }

        public Dealer GetDealersWithDetailsById(int id) {
            var dealer = ObjectContext.Dealers.SingleOrDefault(r => r.Id == id && r.Status == (int)DcsMasterDataStatus.有效);
            if(dealer != null) {
                var company = ObjectContext.Companies.SingleOrDefault(r => r.Id == id && r.Status == (int)DcsMasterDataStatus.有效);
                var dealerServiceInfoes = ObjectContext.DealerServiceInfoes.Where(r => r.DealerId == id && r.Status == (int)DcsMasterDataStatus.有效).ToArray();
                if(dealerServiceInfoes.Length > 0) {
                    var channelCapabilityIds = dealerServiceInfoes.Select(r => r.ChannelCapabilityId);
                    var channelCapabilities = ObjectContext.ChannelCapabilities.Where(r => channelCapabilityIds.Contains(r.Id) && r.Status == (int)DcsBaseDataStatus.有效).ToArray();
                }
            }
            return dealer;
        }

        public Dealer GetDealersWithDetailsAndDealerServiceExtById(int id) {
            var dealer = ObjectContext.Dealers.SingleOrDefault(r => r.Id == id && r.Status == (int)DcsMasterDataStatus.有效);
            if(dealer != null) {
                var dealerServiceExt = ObjectContext.DealerServiceExts.SingleOrDefault(r => r.Id == id && r.Status == (int)DcsMasterDataStatus.有效);
                var companyInvoiceInfo = ObjectContext.CompanyInvoiceInfoes.FirstOrDefault(r => r.CompanyId == id && r.Status == (int)DcsMasterDataStatus.有效);
                var company = ObjectContext.Companies.SingleOrDefault(r => r.Id == id && r.Status == (int)DcsMasterDataStatus.有效);
            }
            return dealer;
        }

        public IQueryable<Dealer> GetDealersWithCompany() {
            return ObjectContext.Dealers.Include("Company").OrderByDescending(v => v.Id);
        }

        public IQueryable<Dealer> GetDealersWithCompanyInvoiceInfoAndDealerServiceExt() {
            return ObjectContext.Dealers.Include("CompanyInvoiceInfo").Include("DealerServiceExt").Include("Company").OrderBy(v => v.Id);

        }

        [Query(HasSideEffects = true)]
        public IQueryable<Dealer> GetDealersByIds(int[] dealerIds) {
            return ObjectContext.Dealers.Where(e => dealerIds.Contains(e.Id) && e.Status == (int)DcsMasterDataStatus.有效).OrderBy(r => r.Id);
        }

        public IQueryable<Dealer> GetDealerByRegionName(string regionName) {
            return ObjectContext.Dealers.Where(v => ObjectContext.Companies.Any(e => ObjectContext.TiledRegions.Any(r => r.RegionName.ToLower() == regionName.ToLower() && r.Id == e.RegionId) && e.Id == v.Id && e.Status == (int)DcsMasterDataStatus.有效) && v.Status == (int)DcsMasterDataStatus.有效).OrderBy(r => r.Id);
        }

        public IQueryable<Dealer> GetDealersWithDealerMarketDptRelation() {
            var dealerIds = ObjectContext.DealerMarketDptRelations.Where(r => r.Status != (int)DcsBaseDataStatus.作废).Select(v => v.DealerId).ToArray();
            return ObjectContext.Dealers.Where(r => r.Status != (int)DcsMasterDataStatus.作废 && !dealerIds.Contains(r.Id)).OrderBy(t => t.Id);
        }

        public IQueryable<Dealer> GetDealersWithBranchAndPartsSalesCategory() {
            return ObjectContext.Dealers.Include("DealerServiceInfoes").Include("DealerServiceInfoes.PartsSalesCategory").Include("DealerServiceInfoes.Branch").OrderBy(r => r.Id);
        }

        public IQueryable<DealerServiceInfo> GetDealerServiceInfoForDealerWithDetails() {
            return ObjectContext.DealerServiceInfoes.Include("Dealer").Include("Warehouse").Include("Dealer.DealerServiceExt").Include("PartsSalesCategory").Include("MarketingDepartment").Include("GradeCoefficient").Include("PartsManagementCostGrade").Include("Dealer.DealerBusinessPermits").Include("Dealer.DealerBusinessPermits.ServiceProductLine").Include("Dealer.DealerBusinessPermits.EngineProductLine").Include("ChannelCapability").Include("Dealer.DealerKeyEmployees").Include("Dealer.DealerKeyEmployees.DealerKeyPosition").OrderBy(r => r.Id);
        }

        public IQueryable<DealerServiceInfo> GetDealerServiceInfoForDealerWithDetailsDealerKeyEmployees() {
            return ObjectContext.DealerServiceInfoes.Include("Dealer").Include("Warehouse").Include("Dealer.DealerServiceExt").Include("PartsSalesCategory").Include("MarketingDepartment").Include("GradeCoefficient").Include("PartsManagementCostGrade").Include("ChannelCapability").Include("Dealer.DealerKeyEmployees").Include("Dealer.DealerKeyEmployees.DealerKeyPosition").OrderBy(r => r.Id);
        }

        public IQueryable<Dealer> GetDealersWithCompanyForService() {
            return ObjectContext.Dealers.Include("Company").Where(r => !ObjectContext.DealerServiceInfoes.Any(t => t.DealerId == r.Id && t.Status != (int)DcsMasterDataStatus.作废)).OrderByDescending(v => v.Id);
        }

    
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:32
        //原分布类的函数全部转移到Ach                                                               

        public void InsertDealer(Dealer dealer) {
            new DealerAch(this).InsertDealer(dealer);
        }

        public void UpdateDealer(Dealer dealer) {
            new DealerAch(this).UpdateDealer(dealer);
        }

        public IQueryable<Dealer> 查询经销商信息带服务扩展信息(int? branchId, int? dealerServiceInfoStatus) {
            IQueryable<DealerServiceInfo> dealerServiceInfoes = ObjectContext.DealerServiceInfoes;
            //拼接查询：DealerServiceInfo对应的条件
            if(branchId.HasValue)
                dealerServiceInfoes = dealerServiceInfoes.Where(r => r.BranchId == branchId);
            if(dealerServiceInfoStatus.HasValue)
                dealerServiceInfoes = dealerServiceInfoes.Where(r => r.Status == dealerServiceInfoStatus);
            //拼接查询：经销商信息
            var result = ObjectContext.Dealers.Where(v => dealerServiceInfoes.Any(info => info.DealerId == v.Id)).OrderBy(r => r.Id);

            //获取数据：拼接客户端条件（分页）后的经销商信息Id
            var dealerIds = ((IQueryable<Dealer>)QueryComposer.Compose(result, ParamQueryable)).Select(v => v.Id).ToArray();
            //获取数据：对应的关联单数据（如果关联单对应的表数据量大，考虑不适用Include，根据关联字段值，分多次查询）
            var resultDealerServiceInfoes = dealerServiceInfoes.Where(v => dealerIds.Contains(v.DealerId)).Include("PartsSalesCategory").Include("MarketingDepartment").Include("PartsManagementCostGrade").Include("Branch").Include("GradeCoefficient").Include("ChannelCapability").ToArray();
            var resultDealerServiceExts = ObjectContext.DealerServiceExts.Where(v => dealerIds.Contains(v.Id)).ToArray();

            //返回主单查询IQueryable
            return result;
        }//保持原有的public方法，NoChangePublic
 
        

        public IQueryable<Dealer> GetDealersWithDealerServiceInfoes() {
            return new DealerAch(this).GetDealersWithDealerServiceInfoes();
        }

        public IQueryable<Dealer> GetDealersWithDealer() {
            return new DealerAch(this).GetDealersWithDealer();
        }
        public Dealer GetDealerWithCompany(int id) {
            return new DealerAch(this).GetDealerWithCompany(id);
        }
        public Dealer GetDealersWithDealerServiceExt(int id) {
            return new DealerAch(this).GetDealersWithDealerServiceExt(id);
        }

        public IQueryable<Dealer> GetDealersWithDealerServiceInfoesAndDetails() {
            return new DealerAch(this).GetDealersWithDealerServiceInfoesAndDetails();
        }

        public Dealer GetDealersWithDetailsById(int id) {
            return new DealerAch(this).GetDealersWithDetailsById(id);
        }

        public Dealer GetDealersWithDetailsAndDealerServiceExtById(int id) {
            return new DealerAch(this).GetDealersWithDetailsAndDealerServiceExtById(id);
        }

        public IQueryable<Dealer> GetDealersWithCompany() {
            return new DealerAch(this).GetDealersWithCompany();
        }

        public IQueryable<Dealer> GetDealersWithCompanyInvoiceInfoAndDealerServiceExt() {
            return new DealerAch(this).GetDealersWithCompanyInvoiceInfoAndDealerServiceExt();
        }
        

        [Query(HasSideEffects = true)]
        public IQueryable<Dealer> GetDealersByIds(int[] dealerIds) {
            return new DealerAch(this).GetDealersByIds(dealerIds);
        }

        public IQueryable<Dealer> GetDealerByRegionName(string regionName) {
            return new DealerAch(this).GetDealerByRegionName(regionName);
        }

        public IQueryable<Dealer> GetDealersWithDealerMarketDptRelation() {
            return new DealerAch(this).GetDealersWithDealerMarketDptRelation();
        }

        public IQueryable<Dealer> GetDealersWithBranchAndPartsSalesCategory() {
            return new DealerAch(this).GetDealersWithBranchAndPartsSalesCategory();
        }

        public IQueryable<DealerServiceInfo> GetDealerServiceInfoForDealerWithDetails() {
            return new DealerAch(this).GetDealerServiceInfoForDealerWithDetails();
        }

        public IQueryable<DealerServiceInfo> GetDealerServiceInfoForDealerWithDetailsDealerKeyEmployees() {
            return new DealerAch(this).GetDealerServiceInfoForDealerWithDetailsDealerKeyEmployees();
        }
        public IQueryable<Dealer> GetDealersWithCompanyForService() {
            return new DealerAch(this).GetDealersWithCompanyForService();
        }
    }
}
