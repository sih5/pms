﻿using System;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DealerBusinessPermitAch : DcsSerivceAchieveBase {
        internal void InsertDealerBusinessPermitValidate(DealerBusinessPermit dealerBusinessPermit) {
            //var dbdealerBusinessPermit = ObjectContext.DealerBusinessPermits.Where(r => r.ProductLineType == dealerBusinessPermit.ProductLineType && r.ServiceProductLineId == dealerBusinessPermit.ServiceProductLineId && r.DealerId == dealerBusinessPermit.DealerId).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            //if(dbdealerBusinessPermit != null)
            //    throw new ValidationException(ErrorStrings.DealerBusinessPermit_Validation1);
            var userInfo = Utils.GetCurrentUserInfo();
            dealerBusinessPermit.CreatorId = userInfo.Id;
            dealerBusinessPermit.CreatorName = userInfo.Name;
            dealerBusinessPermit.CreateTime = DateTime.Now;
            var newDealerBusinessPermitHistory = new DealerBusinessPermitHistory {
                DealerId = dealerBusinessPermit.DealerId,
                ServiceProductLineId = dealerBusinessPermit.ServiceProductLineId,
                BranchId = dealerBusinessPermit.BranchId,
                ServiceFeeGradeId = dealerBusinessPermit.ServiceFeeGradeId,
                ProductLineType = dealerBusinessPermit.ProductLineType,
                EngineRepairPower = dealerBusinessPermit.EngineRepairPower,
                Status = (int)DcsDealerBusinessPermitHistoryStatus.新增,
                CreatorId = userInfo.Id,
                CreatorName = userInfo.Name,
                CreateTime = DateTime.Now
            };
            dealerBusinessPermit.DealerBusinessPermitHistories.Add(newDealerBusinessPermitHistory);
            DomainService.InsertDealerBusinessPermitHistory(newDealerBusinessPermitHistory);
        }

        public void InsertDealerBusinessPermit(DealerBusinessPermit dealerBusinessPermit) {
            InsertToDatabase(dealerBusinessPermit);
            this.InsertDealerBusinessPermitValidate(dealerBusinessPermit);
        }

        internal void UpdateDealerBusinessPermitValidate(DealerBusinessPermit dealerBusinessPermit) {
            //var dbdealerBusinessPermit = ObjectContext.DealerBusinessPermits.Where(r => r.Id != dealerBusinessPermit.Id && r.ProductLineType == dealerBusinessPermit.ProductLineType && r.ServiceProductLineId == dealerBusinessPermit.ServiceProductLineId && r.DealerId == dealerBusinessPermit.DealerId).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            //if(dbdealerBusinessPermit != null)
            //    throw new ValidationException(ErrorStrings.DealerBusinessPermit_Validation1);
            var userInfo = Utils.GetCurrentUserInfo();
            var newDealerBusinessPermitHistory = new DealerBusinessPermitHistory {
                DealerId = dealerBusinessPermit.DealerId,
                ServiceProductLineId = dealerBusinessPermit.ServiceProductLineId,
                BranchId = dealerBusinessPermit.BranchId,
                ServiceFeeGradeId = dealerBusinessPermit.ServiceFeeGradeId,
                ProductLineType = dealerBusinessPermit.ProductLineType,
                EngineRepairPower = dealerBusinessPermit.EngineRepairPower,
                Status = (int)DcsDealerBusinessPermitHistoryStatus.修改,
                CreatorId = userInfo.Id,
                CreatorName = userInfo.Name,
                CreateTime = DateTime.Now
            };
            dealerBusinessPermit.DealerBusinessPermitHistories.Add(newDealerBusinessPermitHistory);
            DomainService.InsertDealerBusinessPermitHistory(newDealerBusinessPermitHistory);
        }


        public void UpdateDealerBusinessPermit(DealerBusinessPermit dealerBusinessPermit) {
            UpdateToDatabase(dealerBusinessPermit);
            this.UpdateDealerBusinessPermitValidate(dealerBusinessPermit);

        }

        internal void DeleteDealerBusinessPermitValidate(DealerBusinessPermit dealerBusinessPermit) {
            var userInfo = Utils.GetCurrentUserInfo();
            int dealerId = dealerBusinessPermit.DealerId;
            int serviceProductLineId = dealerBusinessPermit.ServiceProductLineId;
            int branchId = dealerBusinessPermit.BranchId;
            int serviceFeeGradeId = dealerBusinessPermit.ServiceFeeGradeId;
            int productLineType = dealerBusinessPermit.ProductLineType;
            int? engineRepairPower = dealerBusinessPermit.EngineRepairPower;
            var newDealerBusinessPermitHistory = new DealerBusinessPermitHistory {
                DealerId = dealerId,
                ServiceProductLineId = serviceProductLineId,
                BranchId = branchId,
                ServiceFeeGradeId = serviceFeeGradeId,
                ProductLineType = productLineType,
                Status = (int)DcsDealerBusinessPermitHistoryStatus.删除,
                EngineRepairPower = engineRepairPower,
                CreatorId = userInfo.Id,
                CreatorName = userInfo.Name,
                CreateTime = DateTime.Now,

            };
            DomainService.InsertDealerBusinessPermitHistory(newDealerBusinessPermitHistory);
        }

        public void DeleteDealerBusinessPermit(DealerBusinessPermit dealerBusinessPermit) {
            DeleteFromDatabase(dealerBusinessPermit);
            this.DeleteDealerBusinessPermitValidate(dealerBusinessPermit);
        }


        public IQueryable<DealerBusinessPermit> GetDealerBusinessPermitWithDetail() {
            return ObjectContext.DealerBusinessPermits.Include("Branch")
                .Include("Dealer").Include("ServiceProductLine").Include("ServiceProductLine.PartsSalesCategory").OrderBy(r => r.Id);
        }
        public IQueryable<DealerBusinessPermit> GetDealerBusinessPermitByDealer(string code, int PartsSalesCategoryId) {
            var dealerId = ObjectContext.Dealers.SingleOrDefault(r => r.Status == (int)DcsMasterDataStatus.有效 && r.Code == code);

            var dealerServiceInfo = ObjectContext.DealerServiceInfoes.SingleOrDefault(r => r.Status == (int)DcsMasterDataStatus.有效 && dealerId.Id == r.DealerId && r.PartsSalesCategoryId == PartsSalesCategoryId);
            return null;
        }

        public IQueryable<DealerBusinessPermit> GetDealerBusinessPermitByDealerAndPartsSalesCategoryId(string code, int partsSalesCategoryId) {
            var dealers = ObjectContext.Dealers.SingleOrDefault(r => r.Status == (int)DcsMasterDataStatus.有效 && r.Code == code);
            if(dealers == null)
                return null;
            var result = from dealerBusinessPermit in ObjectContext.DealerBusinessPermits
                         from serviceProductLineView in ObjectContext.ServiceProductLineViews
                         where dealerBusinessPermit.ServiceProductLineId == serviceProductLineView.ProductLineId && serviceProductLineView.PartsSalesCategoryId == partsSalesCategoryId && dealerBusinessPermit.DealerId == dealers.Id
                         select dealerBusinessPermit;
            return result.OrderBy(r => r.Id);
        }

    
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:32
        //原分布类的函数全部转移到Ach                                                               

        public void InsertDealerBusinessPermit(DealerBusinessPermit dealerBusinessPermit) {
            new DealerBusinessPermitAch(this).InsertDealerBusinessPermit(dealerBusinessPermit);
        }
        

        public void UpdateDealerBusinessPermit(DealerBusinessPermit dealerBusinessPermit) {
            new DealerBusinessPermitAch(this).UpdateDealerBusinessPermit(dealerBusinessPermit);
        }

        public void DeleteDealerBusinessPermit(DealerBusinessPermit dealerBusinessPermit) {
            new DealerBusinessPermitAch(this).DeleteDealerBusinessPermit(dealerBusinessPermit);
        }
        

        public IQueryable<DealerBusinessPermit> GetDealerBusinessPermitWithDetail() {
            return new DealerBusinessPermitAch(this).GetDealerBusinessPermitWithDetail();
        }
                public IQueryable<DealerBusinessPermit> GetDealerBusinessPermitByDealer(string code, int PartsSalesCategoryId) {
            return new DealerBusinessPermitAch(this).GetDealerBusinessPermitByDealer(code,PartsSalesCategoryId);
        }

        public IQueryable<DealerBusinessPermit> GetDealerBusinessPermitByDealerAndPartsSalesCategoryId(string code, int partsSalesCategoryId) {
            return new DealerBusinessPermitAch(this).GetDealerBusinessPermitByDealerAndPartsSalesCategoryId(code,partsSalesCategoryId);
        }
    }
}
