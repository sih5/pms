﻿using Sunlight.Silverlight.Web;
using System;
using System.Linq;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class DealerBusinessPermitHistoryAch : DcsSerivceAchieveBase {
        public DealerBusinessPermitHistoryAch(DcsDomainService domainService)
            : base(domainService) {
        }

        internal void InsertDealerBusinessPermitHistoryValidate(DealerBusinessPermitHistory dealerBusinessPermitHistory) {
            var userInfo = Utils.GetCurrentUserInfo();
            dealerBusinessPermitHistory.FilerId = userInfo.Id;
            dealerBusinessPermitHistory.FilerName = userInfo.Name;
            dealerBusinessPermitHistory.FileTime = DateTime.Now;
        }
        public void InsertDealerBusinessPermitHistory(DealerBusinessPermitHistory dealerBusinessPermitHistory) {
            InsertToDatabase(dealerBusinessPermitHistory);
            this.InsertDealerBusinessPermitHistoryValidate(dealerBusinessPermitHistory);
        }
        public IQueryable<DealerBusinessPermitHistory> GetDealerBusinessPermitHistoryQuery() {
            return ObjectContext.DealerBusinessPermitHistories.Include("Dealer").Include("ServiceProductLine").Include("EngineProductLine").OrderBy(e => e.Id);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:32
        //原分布类的函数全部转移到Ach                                                               

        public void InsertDealerBusinessPermitHistory(DealerBusinessPermitHistory dealerBusinessPermitHistory) {
            new DealerBusinessPermitHistoryAch(this).InsertDealerBusinessPermitHistory(dealerBusinessPermitHistory);
        }

        public IQueryable<DealerBusinessPermitHistory> GetDealerBusinessPermitHistoryQuery() {
            return new DealerBusinessPermitHistoryAch(this).GetDealerBusinessPermitHistoryQuery();
        }
    }
}
