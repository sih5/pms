﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class DealerGradeInfoAch : DcsSerivceAchieveBase {
        internal void InsertDealerGradeInfoVailidate(DealerGradeInfo dealerGradeInfo) {
            var dbBrandGradeMessage = ObjectContext.DealerGradeInfoes.Where(r => r.GradeName == dealerGradeInfo.GradeName && r.PartsSalesCategoryId == dealerGradeInfo.PartsSalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbBrandGradeMessage != null) {
                throw new ValidationException(ErrorStrings.GradeCoefficient_Validation1);
            }
            var userInfo = Utils.GetCurrentUserInfo();
            dealerGradeInfo.CreatorId = userInfo.Id;
            dealerGradeInfo.CreatorName = userInfo.Name;
            dealerGradeInfo.CreateTime = DateTime.Now;
        }
        internal void UnsertDealerGradeInfoVailidate(DealerGradeInfo dealerGradeInfo) {
            var dbBrandGradeMessage = ObjectContext.DealerGradeInfoes.Where(r => r.Id == dealerGradeInfo.Id && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbBrandGradeMessage == null) {
                throw new ValidationException(ErrorStrings.GradeCoefficient_Validation2);
            }
            var checkBrandGradeMessage = ObjectContext.DealerGradeInfoes.Where(r => r.GradeName == dealerGradeInfo.GradeName && r.PartsSalesCategoryId == dealerGradeInfo.PartsSalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效 && r.Id != dealerGradeInfo.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(checkBrandGradeMessage != null) {
                throw new ValidationException(ErrorStrings.GradeCoefficient_Validation3);
            }
            var userInfo = Utils.GetCurrentUserInfo();
            dealerGradeInfo.ModifierId = userInfo.Id;
            dealerGradeInfo.ModifierName = userInfo.Name;
            dealerGradeInfo.ModifyTime = DateTime.Now;
        }
        public void InsertDealerGradeInfo(DealerGradeInfo dealerGradeInfo) {
            InsertToDatabase(dealerGradeInfo);
            InsertDealerGradeInfoVailidate(dealerGradeInfo);
        }
        public void UpdateDealerGradeInfo(DealerGradeInfo dealerGradeInfo) {
            UpdateToDatabase(dealerGradeInfo);
            UnsertDealerGradeInfoVailidate(dealerGradeInfo);
        }

    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:32
        //原分布类的函数全部转移到Ach                                                               

        public void InsertDealerGradeInfo(DealerGradeInfo dealerGradeInfo) {
            new DealerGradeInfoAch(this).InsertDealerGradeInfo(dealerGradeInfo);
        }

        public void UpdateDealerGradeInfo(DealerGradeInfo dealerGradeInfo) {
            new DealerGradeInfoAch(this).UpdateDealerGradeInfo(dealerGradeInfo);
        }
    }
}
