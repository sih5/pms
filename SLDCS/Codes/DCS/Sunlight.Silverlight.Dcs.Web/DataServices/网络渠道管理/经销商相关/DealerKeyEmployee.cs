﻿using System;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DealerKeyEmployeeAch : DcsSerivceAchieveBase {
        internal void InsertDealerKeyEmployeeValidate(DealerKeyEmployee dealerKeyEmployee) {
            var userInfo = Utils.GetCurrentUserInfo();
            dealerKeyEmployee.CreatorId = userInfo.Id;
            dealerKeyEmployee.CreatorName = userInfo.Name;
            dealerKeyEmployee.CreateTime = DateTime.Now;
            var dealerKeyEmployeeHistory = new DealerKeyEmployeeHistory();
            dealerKeyEmployeeHistory.DealerId = dealerKeyEmployee.DealerId;
            dealerKeyEmployeeHistory.PartsSalesCategoryId = dealerKeyEmployee.PartsSalesCategoryId;
            dealerKeyEmployeeHistory.KeyPositionId = dealerKeyEmployee.KeyPositionId;
            dealerKeyEmployeeHistory.Name = dealerKeyEmployee.Name;
            dealerKeyEmployeeHistory.Sex = dealerKeyEmployee.Sex;
            dealerKeyEmployeeHistory.PhoneNumber = dealerKeyEmployee.PhoneNumber;
            dealerKeyEmployeeHistory.Address = dealerKeyEmployee.Address;
            dealerKeyEmployeeHistory.PostCode = dealerKeyEmployee.PostCode;
            dealerKeyEmployeeHistory.Mail = dealerKeyEmployee.Mail;
            dealerKeyEmployeeHistory.Remark = dealerKeyEmployee.Remark;
            dealerKeyEmployeeHistory.Status = (int)DcsDealerBusinessPermitHistoryStatus.新增;
            dealerKeyEmployeeHistory.CreateTime = dealerKeyEmployee.CreateTime;
            dealerKeyEmployeeHistory.CreatorId = dealerKeyEmployee.CreatorId;
            dealerKeyEmployeeHistory.CreatorName = dealerKeyEmployee.CreatorName;
            dealerKeyEmployeeHistory.ModifyTime = dealerKeyEmployee.ModifyTime;
            dealerKeyEmployeeHistory.ModifierId = dealerKeyEmployee.ModifierId;
            dealerKeyEmployeeHistory.ModifierName = dealerKeyEmployee.ModifierName;
            dealerKeyEmployeeHistory.AbandonTime = dealerKeyEmployee.AbandonTime;
            dealerKeyEmployeeHistory.AbandonerId = dealerKeyEmployee.AbandonerId;
            dealerKeyEmployeeHistory.AbandonerName = dealerKeyEmployee.AbandonerName;
            dealerKeyEmployeeHistory.MobileNumber = dealerKeyEmployee.MobileNumber;
            dealerKeyEmployeeHistory.FilerId = userInfo.Id;
            dealerKeyEmployeeHistory.FilerName = userInfo.Name;
            dealerKeyEmployeeHistory.FileTime = DateTime.Now;
            dealerKeyEmployee.DealerKeyEmployeeHistories.Add(dealerKeyEmployeeHistory);
            InsertToDatabase(dealerKeyEmployeeHistory);

        }


        internal void UpdateDealerKeyEmployeeValidate(DealerKeyEmployee dealerKeyEmployee) {
            var userInfo = Utils.GetCurrentUserInfo();
            var dealerKeyEmployeeHistory = new DealerKeyEmployeeHistory();
            dealerKeyEmployeeHistory.RecordId = dealerKeyEmployee.Id;
            dealerKeyEmployeeHistory.DealerId = dealerKeyEmployee.DealerId;
            dealerKeyEmployeeHistory.PartsSalesCategoryId = dealerKeyEmployee.PartsSalesCategoryId;
            dealerKeyEmployeeHistory.KeyPositionId = dealerKeyEmployee.KeyPositionId;
            dealerKeyEmployeeHistory.Name = dealerKeyEmployee.Name;
            dealerKeyEmployeeHistory.Sex = dealerKeyEmployee.Sex;
            dealerKeyEmployeeHistory.PhoneNumber = dealerKeyEmployee.PhoneNumber;
            dealerKeyEmployeeHistory.Address = dealerKeyEmployee.Address;
            dealerKeyEmployeeHistory.PostCode = dealerKeyEmployee.PostCode;
            dealerKeyEmployeeHistory.Mail = dealerKeyEmployee.Mail;
            dealerKeyEmployeeHistory.Remark = dealerKeyEmployee.Remark;
            dealerKeyEmployeeHistory.Status = (int)DcsDealerBusinessPermitHistoryStatus.修改;
            dealerKeyEmployeeHistory.CreateTime = dealerKeyEmployee.CreateTime;
            dealerKeyEmployeeHistory.CreatorId = dealerKeyEmployee.CreatorId;
            dealerKeyEmployeeHistory.CreatorName = dealerKeyEmployee.CreatorName;
            dealerKeyEmployeeHistory.ModifyTime = dealerKeyEmployee.ModifyTime;
            dealerKeyEmployeeHistory.ModifierId = dealerKeyEmployee.ModifierId;
            dealerKeyEmployeeHistory.ModifierName = dealerKeyEmployee.ModifierName;
            dealerKeyEmployeeHistory.AbandonTime = dealerKeyEmployee.AbandonTime;
            dealerKeyEmployeeHistory.AbandonerId = dealerKeyEmployee.AbandonerId;
            dealerKeyEmployeeHistory.AbandonerName = dealerKeyEmployee.AbandonerName;
            dealerKeyEmployeeHistory.MobileNumber = dealerKeyEmployee.MobileNumber;
            dealerKeyEmployeeHistory.FilerId = userInfo.Id;
            dealerKeyEmployeeHistory.FilerName = userInfo.Name;
            dealerKeyEmployeeHistory.FileTime = DateTime.Now;
            dealerKeyEmployee.DealerKeyEmployeeHistories.Add(dealerKeyEmployeeHistory);
            InsertToDatabase(dealerKeyEmployeeHistory);
        }



        public void InsertDealerKeyEmployee(DealerKeyEmployee dealerKeyEmployee) {
            InsertToDatabase(dealerKeyEmployee);
            this.InsertDealerKeyEmployeeValidate(dealerKeyEmployee);

        }

        public void UpdateDealerKeyEmployee(DealerKeyEmployee dealerKeyEmployee) {
            UpdateToDatabase(dealerKeyEmployee);
            this.UpdateDealerKeyEmployeeValidate(dealerKeyEmployee);
        }

        public void DeleteDealerKeyEmployee(DealerKeyEmployee dealerKeyEmployee) {
            DeleteFromDatabase(dealerKeyEmployee);


        }

        public IQueryable<DealerKeyEmployee> GetDealerKeyEmployeesWithDetails() {
            return ObjectContext.DealerKeyEmployees.Include("DealerKeyPosition.Branch").Include("Dealer").Include("PartsSalesCategory").OrderBy(v => v.Id);
        }

        //public IQueryable<DealerKeyEmployee> GetDealerKeyEmployeesWithDetailsByMarketDtpName(string dtpName) {
        //    if(string.IsNullOrWhiteSpace(dtpName)) {
        //        return ObjectContext.DealerKeyEmployees.Include("DealerKeyPosition.Branch").Include("Dealer").Include("PartsSalesCategory").OrderBy(v => v.Id);
        //    }
        //    var query = from a in this.ObjectContext.DealerKeyEmployees
        //                join b in this.ObjectContext.DealerMarketDptRelations on a.DealerId equals b.DealerId
        //                join c in this.ObjectContext.MarketingDepartments on b.MarketId equals c.Id
        //                where c.Name.IndexOf(dtpName) != -1
        //                select a;
        //    return query.Include("DealerKeyPosition.Branch").Include("Dealer").Include("PartsSalesCategory").OrderBy(r => r.Id);
        //}


        public IQueryable<DealerKeyEmployee> GetDealerKeyEmployeesWithDetailsByMarketDtpName(string dtpName) {
            var userInfo = Utils.GetCurrentUserInfo();
            var companyType = ObjectContext.Companies.First(r => r.Id == userInfo.EnterpriseId).Type;
            var query = from a in this.ObjectContext.DealerKeyEmployees
                        where a.DealerId != 0
                        select a;
            if(companyType == (int)DcsCompanyType.分公司) {
                query = from a in query
                        join d in ObjectContext.PersonSalesCenterLinks.Where(r => r.PersonId == userInfo.Id) on a.PartsSalesCategoryId equals d.PartsSalesCategoryId
                        select a;
            }
            if(!string.IsNullOrWhiteSpace(dtpName)) {
                query = from a in query
                        join b in this.ObjectContext.DealerMarketDptRelations on a.DealerId equals b.DealerId
                        join c in this.ObjectContext.MarketingDepartments on b.MarketId equals c.Id
                        where c.Name.IndexOf(dtpName) != -1
                        select a;
            }
            return query.Include("DealerKeyPosition.Branch").Include("Dealer").Include("PartsSalesCategory").Include("MarketingDepartment").OrderBy(r => r.Id);
        }

        public IQueryable<DealerKeyEmployee> GetDealerKeyEmployeesForKeyPositionIds(int[] keyPositionIds) {
            return ObjectContext.DealerKeyEmployees.Where(v => keyPositionIds.Contains(v.KeyPositionId)).OrderBy(v => v.Id);
        }
    
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:32
        //原分布类的函数全部转移到Ach                                                               

        public void InsertDealerKeyEmployee(DealerKeyEmployee dealerKeyEmployee) {
            new DealerKeyEmployeeAch(this).InsertDealerKeyEmployee(dealerKeyEmployee);
        }

        public void UpdateDealerKeyEmployee(DealerKeyEmployee dealerKeyEmployee) {
            new DealerKeyEmployeeAch(this).UpdateDealerKeyEmployee(dealerKeyEmployee);
        }

        public void DeleteDealerKeyEmployee(DealerKeyEmployee dealerKeyEmployee) {
            new DealerKeyEmployeeAch(this).DeleteDealerKeyEmployee(dealerKeyEmployee);
        }

        public IQueryable<DealerKeyEmployee> GetDealerKeyEmployeesWithDetails() {
            return new DealerKeyEmployeeAch(this).GetDealerKeyEmployeesWithDetails();
        }
        

        public IQueryable<DealerKeyEmployee> GetDealerKeyEmployeesWithDetailsByMarketDtpName(string dtpName) {
            return new DealerKeyEmployeeAch(this).GetDealerKeyEmployeesWithDetailsByMarketDtpName(dtpName);
        }

        public IQueryable<DealerKeyEmployee> GetDealerKeyEmployeesForKeyPositionIds(int[] keyPositionIds) {
            return new DealerKeyEmployeeAch(this).GetDealerKeyEmployeesForKeyPositionIds(keyPositionIds);
        }
    }
}