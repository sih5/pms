﻿using System;
using System.Linq;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class DealerKeyEmployeeHistoryAch : DcsSerivceAchieveBase {
        public DealerKeyEmployeeHistoryAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public IQueryable<DealerKeyEmployeeHistory> GetDealerKeyEmployeeHistoriesWithDealerKeyPosition() {
            return ObjectContext.DealerKeyEmployeeHistories.Include("DealerKeyPosition").OrderBy(v => v.Id);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:32
        //原分布类的函数全部转移到Ach                                                               

        public IQueryable<DealerKeyEmployeeHistory> GetDealerKeyEmployeeHistoriesWithDealerKeyPosition() {
            return new DealerKeyEmployeeHistoryAch(this).GetDealerKeyEmployeeHistoriesWithDealerKeyPosition();
        }
    }
}
