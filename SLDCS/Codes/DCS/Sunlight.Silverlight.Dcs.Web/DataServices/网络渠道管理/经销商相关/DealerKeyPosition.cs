﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class DealerKeyPositionAch : DcsSerivceAchieveBase {
        public DealerKeyPositionAch(DcsDomainService domainService)
            : base(domainService) {
        }

        internal void InsertDealerKeyPositionValidate(DealerKeyPosition dealerKeyPosition) {
            var dbdealerKeyPosition = ObjectContext.DealerKeyPositions.Where(r => r.BranchId == dealerKeyPosition.BranchId && r.PositionName.ToLower() == dealerKeyPosition.PositionName.ToLower() && r.Status != (int)DcsMasterDataStatus.作废).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbdealerKeyPosition != null)
                throw new ValidationException(string.Format(ErrorStrings.DealerKeyPosition_Validation1, dealerKeyPosition.PositionName));
            var userInfo = Utils.GetCurrentUserInfo();
            dealerKeyPosition.CreatorId = userInfo.Id;
            dealerKeyPosition.CreatorName = userInfo.Name;
            dealerKeyPosition.CreateTime = DateTime.Now;
        }
        internal void UpdateDealerKeyPositionValidate(DealerKeyPosition dealerKeyPosition) {
            var dbdealerKeyPosition = ObjectContext.DealerKeyPositions.Where(r => r.Id != dealerKeyPosition.Id && r.Status == (int)DcsMasterDataStatus.有效).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbdealerKeyPosition == null)
                throw new ValidationException(ErrorStrings.DealerKeyPosition_Validation2);
            var userInfo = Utils.GetCurrentUserInfo();
            dealerKeyPosition.ModifierId = userInfo.Id;
            dealerKeyPosition.ModifierName = userInfo.Name;
            dealerKeyPosition.ModifyTime = DateTime.Now;
        }
        public void InsertDealerKeyPosition(DealerKeyPosition dealerKeyPosition) {
            InsertToDatabase(dealerKeyPosition);
            this.InsertDealerKeyPositionValidate(dealerKeyPosition);
        }
        public void UpdateDealerKeyPosition(DealerKeyPosition dealerKeyPosition) {
            UpdateToDatabase(dealerKeyPosition);
            this.UpdateDealerKeyPositionValidate(dealerKeyPosition);
        }
        public IQueryable<DealerKeyPosition> GetDealerKeyPositionsWithBranch() {
            return ObjectContext.DealerKeyPositions.Include("Branch").OrderBy(v => v.Id);
        }
        public IQueryable<DealerKeyPosition> GetDealerKeyPositionIds(int[] ids) {
            var dbdealerKeyPosition = ObjectContext.DealerKeyPositions.Where(r => ids.Contains(r.Id));
            return dbdealerKeyPosition;
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:32
        //原分布类的函数全部转移到Ach                                                               

        public void InsertDealerKeyPosition(DealerKeyPosition dealerKeyPosition) {
            new DealerKeyPositionAch(this).InsertDealerKeyPosition(dealerKeyPosition);
        }

        public void UpdateDealerKeyPosition(DealerKeyPosition dealerKeyPosition) {
            new DealerKeyPositionAch(this).UpdateDealerKeyPosition(dealerKeyPosition);
        }

        public IQueryable<DealerKeyPosition> GetDealerKeyPositionsWithBranch() {
            return new DealerKeyPositionAch(this).GetDealerKeyPositionsWithBranch();
        }
                public IQueryable<DealerKeyPosition> GetDealerKeyPositionIds(int[] ids) {
            return new DealerKeyPositionAch(this).GetDealerKeyPositionIds(ids);
        }
    }
}
