﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class DealerMobileNumberListAch : DcsSerivceAchieveBase {
        internal void InsertDealerMobileNumberListValidate(DealerMobileNumberList dealerMobileNumberList) {
            var dbdealerMobileNumberList = ObjectContext.DealerMobileNumberLists.Where(r => r.DealerId == dealerMobileNumberList.DealerId && r.VideoMobileNumber == dealerMobileNumberList.VideoMobileNumber).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbdealerMobileNumberList != null)
                throw new ValidationException(string.Format(ErrorStrings.DealerMobileNumberList_Validation1, dealerMobileNumberList.VideoMobileNumber));
            var userInfo = Utils.GetCurrentUserInfo();
            var newDealerMobileNumberHistory = new DealerMobileNumberHistory {
                DealerId = dealerMobileNumberList.DealerId,
                VideoMobileNumber = dealerMobileNumberList.VideoMobileNumber,
                CreatorId = userInfo.Id,
                CreatorName = userInfo.Name,
                CreateTime = DateTime.Now
            };
            InsertToDatabase(newDealerMobileNumberHistory);
        }
        internal void UpdateDealerMobileNumberListValidate(DealerMobileNumberList dealerMobileNumberList) {
            var userInfo = Utils.GetCurrentUserInfo();
            var newDealerMobileNumberHistory = new DealerMobileNumberHistory {
                DealerId = dealerMobileNumberList.DealerId,
                VideoMobileNumber = dealerMobileNumberList.VideoMobileNumber,
                CreatorId = userInfo.Id,
                CreatorName = userInfo.Name,
                CreateTime = DateTime.Now
            };
            InsertToDatabase(newDealerMobileNumberHistory);
        }
        public void InsertDealerMobileNumberList(DealerMobileNumberList dealerMobileNumberList) {
            InsertToDatabase(dealerMobileNumberList);
            this.InsertDealerMobileNumberListValidate(dealerMobileNumberList);
        }
        public void UpdateDealerMobileNumberList(DealerMobileNumberList dealerMobileNumberList) {
            UpdateToDatabase(dealerMobileNumberList);
            this.UpdateDealerMobileNumberListValidate(dealerMobileNumberList);
        }
        public void DeleteDealerMobileNumberList(DealerMobileNumberList dealerMobileNumberList) {
            DeleteFromDatabase(dealerMobileNumberList);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:32
        //原分布类的函数全部转移到Ach                                                               

        public void InsertDealerMobileNumberList(DealerMobileNumberList dealerMobileNumberList) {
            new DealerMobileNumberListAch(this).InsertDealerMobileNumberList(dealerMobileNumberList);
        }

        public void UpdateDealerMobileNumberList(DealerMobileNumberList dealerMobileNumberList) {
            new DealerMobileNumberListAch(this).UpdateDealerMobileNumberList(dealerMobileNumberList);
        }

        public void DeleteDealerMobileNumberList(DealerMobileNumberList dealerMobileNumberList) {
            new DealerMobileNumberListAch(this).DeleteDealerMobileNumberList(dealerMobileNumberList);
        }
    }
}