﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class DealerPerTrainAutAch : DcsSerivceAchieveBase {
        internal void InsertDealerPerTrainAutValidate(DealerPerTrainAut dealerPerTrainAut) {
            var userInfo = Utils.GetCurrentUserInfo();
            dealerPerTrainAut.CreatorId = userInfo.Id;
            dealerPerTrainAut.CreatorName = userInfo.Name;
            dealerPerTrainAut.CreateTime = DateTime.Now;
        }
        public void InsertDealerPerTrainAut(DealerPerTrainAut dealerPerTrainAut) {
            InsertToDatabase(dealerPerTrainAut);
            this.InsertDealerPerTrainAutValidate(dealerPerTrainAut);
        }
        internal void UpdateDealerPerTrainAutValidate(DealerPerTrainAut dealerPerTrainAut) {
            var dbdealerPerTrainAut = ObjectContext.DealerPerTrainAuts.Where(r => r.Id == dealerPerTrainAut.Id && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbdealerPerTrainAut == null) {
                throw new ValidationException("当前服务站人员培训及认证状态无效");
            }
            var userInfo = Utils.GetCurrentUserInfo();
            dealerPerTrainAut.ModifierId = userInfo.Id;
            dealerPerTrainAut.ModifierName = userInfo.Name;
            dealerPerTrainAut.ModifyTime = DateTime.Now;
        }
        public void UpdateDealerPerTrainAut(DealerPerTrainAut dealerPerTrainAut) {
            UpdateToDatabase(dealerPerTrainAut);
            this.UpdateDealerPerTrainAutValidate(dealerPerTrainAut);
        }
        public IQueryable<DealerPerTrainAut> GetDealerPerTrainAutsWithOtherInfo() {
            return ObjectContext.DealerPerTrainAuts.Include("MarketingDepartment").Include("AuthenticationTypeEntity").Include("TrainingTypeEntity").Include("PartsSalesCategory").Include("DealerKeyPosition").OrderBy(r => r.Id);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:32
        //原分布类的函数全部转移到Ach                                                               

        public void InsertDealerPerTrainAut(DealerPerTrainAut dealerPerTrainAut) {
            new DealerPerTrainAutAch(this).InsertDealerPerTrainAut(dealerPerTrainAut);
        }

        public void UpdateDealerPerTrainAut(DealerPerTrainAut dealerPerTrainAut) {
            new DealerPerTrainAutAch(this).UpdateDealerPerTrainAut(dealerPerTrainAut);
        }

        public IQueryable<DealerPerTrainAut> GetDealerPerTrainAutsWithOtherInfo() {
            return new DealerPerTrainAutAch(this).GetDealerPerTrainAutsWithOtherInfo();
        }
    }
}
