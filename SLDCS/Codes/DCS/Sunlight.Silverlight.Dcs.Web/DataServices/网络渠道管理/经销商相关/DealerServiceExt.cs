﻿using Sunlight.Silverlight.Web;
using System;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DealerServiceExtAch : DcsSerivceAchieveBase {
        internal void InsertDealerServiceExtValidate(DealerServiceExt dealerServiceExt) {
            var userInfo = Utils.GetCurrentUserInfo();
            dealerServiceExt.CreatorId = userInfo.Id;
            dealerServiceExt.CreatorName = userInfo.Name;
            dealerServiceExt.CreateTime = DateTime.Now;
        }

        internal void UpdateDealerServiceExtValidate(DealerServiceExt dealerServiceExt) {
            var userInfo = Utils.GetCurrentUserInfo();
            dealerServiceExt.ModifierId = userInfo.Id;
            dealerServiceExt.ModifierName = userInfo.Name;
            dealerServiceExt.ModifyTime = DateTime.Now;
        }

        public void InsertDealerServiceExt(DealerServiceExt dealerServiceExt) {
            InsertToDatabase(dealerServiceExt);
            this.InsertDealerServiceExtValidate(dealerServiceExt);
        }

        public void UpdateDealerServiceExt(DealerServiceExt dealerServiceExt) {
            UpdateToDatabase(dealerServiceExt);
            this.UpdateDealerServiceExtValidate(dealerServiceExt);
            var dealerServiceExtHistory = new DealerServiceExtHistory {
                RecordId = dealerServiceExt.Id,
                RepairQualification = dealerServiceExt.RepairQualification,
                HasBranch = dealerServiceExt.HasBranch,
                DangerousRepairQualification = dealerServiceExt.DangerousRepairQualification,
                BrandScope = dealerServiceExt.BrandScope,
                CompetitiveBrandScope = dealerServiceExt.CompetitiveBrandScope,
                ParkingArea = dealerServiceExt.ParkingArea,
                RepairingArea = dealerServiceExt.RepairingArea,
                EmployeeNumber = dealerServiceExt.EmployeeNumber,
                PartWarehouseArea = dealerServiceExt.PartWarehouseArea,
                ManagerPhoneNumber = dealerServiceExt.ManagerPhoneNumber,
                ManagerMobile = dealerServiceExt.ManagerMobile,
                ManagerMail = dealerServiceExt.ManagerMail,
                ReceptionRoomArea = dealerServiceExt.ReceptionRoomArea,
                Status = dealerServiceExt.Status,
                CreateTime = dealerServiceExt.CreateTime,
                CreatorId = dealerServiceExt.CreatorId,
                CreatorName = dealerServiceExt.CreatorName,
                ModifierId = dealerServiceExt.ModifierId,
                ModifyTime = dealerServiceExt.ModifyTime,
                ModifierName = dealerServiceExt.ModifierName,
                GeographicPosition = dealerServiceExt.GeographicPosition,
                OwnerCompany = dealerServiceExt.OwnerCompany,
                MainBusinessAreas = dealerServiceExt.MainBusinessAreas,
                AndBusinessAreas = dealerServiceExt.AndBusinessAreas,
                BuildTime = dealerServiceExt.BuildTime,
                TrafficRestrictionsdescribe = dealerServiceExt.TrafficRestrictionsdescribe
            };
            InsertToDatabase(dealerServiceExtHistory);
            this.InsertDealerServiceExtHistoryValidate(dealerServiceExtHistory);
        }
    
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:32
        //原分布类的函数全部转移到Ach                                                               

        public void InsertDealerServiceExt(DealerServiceExt dealerServiceExt) {
            new DealerServiceExtAch(this).InsertDealerServiceExt(dealerServiceExt);
        }

        public void UpdateDealerServiceExt(DealerServiceExt dealerServiceExt) {
            new DealerServiceExtAch(this).UpdateDealerServiceExt(dealerServiceExt);
        }
    }
}
