﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DealerServiceInfoAch : DcsSerivceAchieveBase {
        internal void InsertDealerServiceInfoValidate(DealerServiceInfo dealerServiceInfo) {
            var dbdealerServiceInfo = ObjectContext.DealerServiceInfoes.Where(r => r.PartsSalesCategoryId == dealerServiceInfo.PartsSalesCategoryId && r.DealerId == dealerServiceInfo.DealerId && r.Status != (int)DcsMasterDataStatus.作废).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbdealerServiceInfo != null)
                throw new ValidationException(ErrorStrings.DealerServiceInfo_Validation1);

            var userInfo = Utils.GetCurrentUserInfo();
            dealerServiceInfo.CreatorId = userInfo.Id;
            dealerServiceInfo.CreatorName = userInfo.Name;
            dealerServiceInfo.CreateTime = DateTime.Now;
        }

        internal void UpdateDealerServiceInfoValidate(DealerServiceInfo dealerServiceInfo) {
            var dbdealerServiceInfos = ObjectContext.DealerServiceInfoes.Where(r => r.Id != dealerServiceInfo.Id && r.PartsSalesCategoryId == dealerServiceInfo.PartsSalesCategoryId && r.DealerId == dealerServiceInfo.DealerId && r.Status != (int)DcsMasterDataStatus.作废).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbdealerServiceInfos != null)
                throw new ValidationException(ErrorStrings.DealerServiceInfo_Validation1);
            var userInfo = Utils.GetCurrentUserInfo();
            dealerServiceInfo.ModifierId = userInfo.Id;
            dealerServiceInfo.ModifierName = userInfo.Name;
            dealerServiceInfo.ModifyTime = DateTime.Now;
        }

        public void InsertDealerServiceInfo(DealerServiceInfo dealerServiceInfo) {
            InsertToDatabase(dealerServiceInfo);
            this.InsertDealerServiceInfoValidate(dealerServiceInfo);
        }

        public void UpdateDealerServiceInfo(DealerServiceInfo dealerServiceInfo) {
            UpdateToDatabase(dealerServiceInfo);
            this.UpdateDealerServiceInfoValidate(dealerServiceInfo);
        }

        public IQueryable<DealerServiceInfo> GetDealerServiceInfoWithDetails() {
            return ObjectContext.DealerServiceInfoes.Include("PartsSalesCategory").Include("GradeCoefficient").Include("PartsManagementCostGrade").Include("Warehouse").Include("Dealer").Include("Dealer.DealerBusinessPermits").Include("Dealer.DealerBusinessPermits.ServiceProductLine").Include("Dealer.DealerBusinessPermits.EngineProductLine").Include("ChannelCapability").Include("UsedPartsWarehouse").Include("MarketingDepartment").Include("PartsManagementCostGrade").Include("GradeCoefficient").OrderBy(r => r.Id);
        }

        public IQueryable<DealerServiceInfo> GetDealerServiceInfoWithWarehouseByDealerId(int dealerId) {
            return ObjectContext.DealerServiceInfoes.Where(t => t.Status == (int)DcsBaseDataStatus.有效 && t.DealerId == dealerId).Include("Warehouse").OrderBy(r => r.Id);
        }

        public IQueryable<DealerServiceInfo> GetDealerServiceInfoWithDetailsInfo() {
            var userInfo = Utils.GetCurrentUserInfo();
            var companyType = ObjectContext.Companies.First(r => r.Id == userInfo.EnterpriseId).Type;
            if(companyType == (int)DcsCompanyType.分公司) {
                var dealerServiceInfoes = from a in ObjectContext.DealerServiceInfoes
                                          join b in ObjectContext.PersonSalesCenterLinks.Where(r => r.PersonId == userInfo.Id) on a.PartsSalesCategoryId equals b.PartsSalesCategoryId
                                          select a;
                return dealerServiceInfoes.Include("Dealer").Include("Branch").Include("PartsSalesCategory").Include("UsedPartsWarehouse").Include("MarketingDepartment").Include("Warehouse").Include("ChannelCapability").Include("Company").OrderBy(r => r.Id);
            }
            return ObjectContext.DealerServiceInfoes.Include("Dealer").Include("Branch").Include("PartsSalesCategory").Include("UsedPartsWarehouse").Include("MarketingDepartment").Include("Warehouse").Include("ChannelCapability").OrderBy(r => r.Id);
        }

        public IQueryable<DealerServiceInfo> 查询经销商分公司信息主经销商权限() {
            return ObjectContext.DealerServiceInfoes.Include("Dealer.DealerBusinessPermits").Include("Dealer.DealerKeyEmployees").Include("PartsSalesCategory.ServiceProductLineViews").OrderBy(r => r.Id);
        }

        public IQueryable<DealerServiceInfo> 查询经销商分公司管理信息() {
            return ObjectContext.DealerServiceInfoes.Include("Branch").Include("PartsSalesCategory").Include("Dealer").OrderBy(e => e.Id);
        }

        public IQueryable<DealerServiceInfo> GetDealerServiceInfoIncludeSomeTables() {
            var result = ObjectContext.DealerServiceInfoes.Include("Dealer").Include("PartsSalesCategory").Include("UsedPartsWarehouse").Include("MarketingDepartment").Include("PartsManagementCostGrade").Include("GradeCoefficient").Include("Dealer.DealerKeyEmployees").Include("Dealer.DealerBusinessPermits");

            return result;
        }
        public DealerServiceInfo GetDealerServiceInfoForDealerWithDetailsById(int id) {
            var dealerServiceInfo = ObjectContext.DealerServiceInfoes.SingleOrDefault(r => r.Id == id);
            if(dealerServiceInfo == null) {
                return null;
            }
            var dealer = ObjectContext.Dealers.SingleOrDefault(r => r.Id == dealerServiceInfo.DealerId);
            //if(dealer != null) {
            //    var dealerServiceExt = ObjectContext.DealerServiceExts.SingleOrDefault(r => r.Id == dealer.Id);
            //    //这是2个查询
            //    var tempEngineProductLinesQuery = ObjectContext.EngineProductLines.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.BranchId == dealerServiceInfo.BranchId && r.PartsSalesCategoryId == dealerServiceInfo.PartsSalesCategoryId);
            //    var tempServiceProductLinesQuery = ObjectContext.ServiceProductLines.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.BranchId == dealerServiceInfo.BranchId && r.PartsSalesCategoryId == dealerServiceInfo.PartsSalesCategoryId);
            //    var dealerBusinessPermits = ObjectContext.DealerBusinessPermits.Where(r => r.DealerId == dealer.Id && ((r.ProductLineType == (int)DcsServiceProductLineViewProductLineType.发动机产品线 && tempEngineProductLinesQuery.Any(v => v.Id == r.ServiceProductLineId)) || (r.ProductLineType == (int)DcsServiceProductLineViewProductLineType.服务产品线 && tempServiceProductLinesQuery.Any(v => v.Id == r.ServiceProductLineId)))).ToArray();
            //    var arrayOfEngineProductLinesId = dealerBusinessPermits.Where(r => r.ProductLineType == (int)DcsServiceProductLineViewProductLineType.发动机产品线).Select(r => r.ServiceProductLineId).ToArray();
            //    var arrayOfServiceProductLinesId = dealerBusinessPermits.Where(r => r.ProductLineType == (int)DcsServiceProductLineViewProductLineType.服务产品线).Select(r => r.ServiceProductLineId).ToArray();
            //    var engineProductLines = ObjectContext.EngineProductLines.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && arrayOfEngineProductLinesId.Any(v => v == r.Id)).ToArray();
            //    var serviceProductLines = ObjectContext.ServiceProductLines.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && arrayOfServiceProductLinesId.Any(v => v == r.Id)).ToArray();
            //    var laborHourUnitPriceGradeIds = dealerBusinessPermits.Select(r => r.ServiceFeeGradeId);
            //    var dealerKeyEmployees = ObjectContext.DealerKeyEmployees.Where(r => r.PartsSalesCategoryId == dealerServiceInfo.PartsSalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效 && r.DealerId == dealer.Id).ToArray();
            //    var dealerKeyPositionsId = dealerKeyEmployees.Select(r => r.KeyPositionId).ToArray();
            //    var dealerKeyPositions = ObjectContext.DealerKeyPositions.Where(r => r.Status == (int)DcsMasterDataStatus.有效 && dealerKeyPositionsId.Any(v => v == r.Id)).ToArray();
            //}
            var usedPartsWarehouse = ObjectContext.UsedPartsWarehouses.SingleOrDefault(r => r.Id == dealerServiceInfo.UsedPartsWarehouseId && r.Status == (int)DcsBaseDataStatus.有效);
            var warehouses = ObjectContext.Warehouses.SingleOrDefault(r => r.Id == dealerServiceInfo.WarehouseId && r.Status == (int)DcsBaseDataStatus.有效);
            var partsSalesCategory = ObjectContext.PartsSalesCategories.SingleOrDefault(r => r.Status == (int)DcsMasterDataStatus.有效 && r.Id == dealerServiceInfo.PartsSalesCategoryId);
            var marketingDepartment = ObjectContext.MarketingDepartments.SingleOrDefault(r => r.Status == (int)DcsMasterDataStatus.有效 && r.Id == dealerServiceInfo.MarketingDepartmentId);
            var gradeCoefficient = ObjectContext.GradeCoefficients.SingleOrDefault(r => r.Status == (int)DcsMasterDataStatus.有效 && r.Id == dealerServiceInfo.GradeCoefficientId);
            var partsManagementCostGrade = ObjectContext.PartsManagementCostGrades.SingleOrDefault(r => r.Status == (int)DcsMasterDataStatus.有效 && r.Id == dealerServiceInfo.PartsManagingFeeGradeId);
            var channelCapability = ObjectContext.ChannelCapabilities.SingleOrDefault(r => r.Status == (int)DcsMasterDataStatus.有效 && r.Id == dealerServiceInfo.ChannelCapabilityId);
            return dealerServiceInfo;
        }

        public IQueryable<DealerServiceInfo> GetDealerServiceInfoByEnterPriseId(int enterpriseId) {
            return ObjectContext.DealerServiceInfoes.Where(r => r.BranchId == enterpriseId && (r.Status == (int)DcsMasterDataStatus.有效 || r.Status == (int)DcsMasterDataStatus.停用)).Include("Dealer.Company").OrderBy(v => v.Id);
        }

        public IQueryable<DealerServiceInfo> GetDealerServiceWithMarketingDeptNameByDealerKeyEmployeeId(int[] id) {
            return ObjectContext.DealerServiceInfoes.Where(r => ObjectContext.DealerKeyEmployees.Any(k => k.DealerId == r.DealerId && k.PartsSalesCategoryId == r.PartsSalesCategoryId && id.Contains(k.Id))).Include("MarketingDepartment");
        }

        public IQueryable<DealerServiceInfo> GetDealerServiceInfoByDealerIds(int[] dealerIds) {
            return ObjectContext.DealerServiceInfoes.Include("MarketingDepartment").Where(r => dealerIds.Contains(r.DealerId) && r.Status == (int)DcsMasterDataStatus.有效).OrderBy(v => v.Id);
        }


    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:32
        //原分布类的函数全部转移到Ach                                                               

        public void InsertDealerServiceInfo(DealerServiceInfo dealerServiceInfo) {
            new DealerServiceInfoAch(this).InsertDealerServiceInfo(dealerServiceInfo);
        }

        public void UpdateDealerServiceInfo(DealerServiceInfo dealerServiceInfo) {
            new DealerServiceInfoAch(this).UpdateDealerServiceInfo(dealerServiceInfo);
        }

        public IQueryable<DealerServiceInfo> GetDealerServiceInfoWithDetails() {
            return new DealerServiceInfoAch(this).GetDealerServiceInfoWithDetails();
        }
        public IQueryable<DealerServiceInfo> GetDealerServiceInfoWithWarehouseByDealerId(int dealerId) {
            return new DealerServiceInfoAch(this).GetDealerServiceInfoWithWarehouseByDealerId(dealerId);
        }

        public IQueryable<DealerServiceInfo> GetDealerServiceInfoWithDetailsInfo() {
            return new DealerServiceInfoAch(this).GetDealerServiceInfoWithDetailsInfo();
        }

        public IQueryable<DealerServiceInfo> 查询经销商分公司信息主经销商权限() {
            return new DealerServiceInfoAch(this).查询经销商分公司信息主经销商权限();
        }

        public IQueryable<DealerServiceInfo> 查询经销商分公司管理信息() {
            return new DealerServiceInfoAch(this).查询经销商分公司管理信息();
        }

        public IQueryable<DealerServiceInfo> GetDealerServiceInfoIncludeSomeTables() {
            return new DealerServiceInfoAch(this).GetDealerServiceInfoIncludeSomeTables();
        }
        public DealerServiceInfo GetDealerServiceInfoForDealerWithDetailsById(int id) {
            return new DealerServiceInfoAch(this).GetDealerServiceInfoForDealerWithDetailsById(id);
        }

        public IQueryable<DealerServiceInfo> GetDealerServiceInfoByEnterPriseId(int enterpriseId) {
            return new DealerServiceInfoAch(this).GetDealerServiceInfoByEnterPriseId(enterpriseId);
        }

        public IQueryable<DealerServiceInfo> GetDealerServiceWithMarketingDeptNameByDealerKeyEmployeeId(int[] id) {
            return new DealerServiceInfoAch(this).GetDealerServiceWithMarketingDeptNameByDealerKeyEmployeeId(id);
        }
        public IQueryable<DealerServiceInfo> GetDealerServiceInfoByDealerIds(int[] dealerIds) {
            return new DealerServiceInfoAch(this).GetDealerServiceInfoByDealerIds(dealerIds);
        }
    }
}
