﻿using System;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class DealerServiceInfoHistoryAch : DcsSerivceAchieveBase {
        public DealerServiceInfoHistoryAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public IQueryable<DealerServiceInfoHistory> GetDealerServiceInfoHistoryQuery() {
            var userInfo = Utils.GetCurrentUserInfo();
            var companyType = ObjectContext.Companies.First(r => r.Id == userInfo.EnterpriseId).Type;
            var query = from a in ObjectContext.DealerServiceInfoHistories
                        select a;
            if(companyType == (int)DcsCompanyType.分公司) {
                query = from a in query
                        join d in ObjectContext.PersonSalesCenterLinks.Where(r => r.PersonId == userInfo.Id) on a.PartsSalesCategoryId equals d.PartsSalesCategoryId
                        select a;
            }
            return query.Include("Dealer").Include("PartsSalesCategory").Include("UsedPartsWarehouse").Include("AUsedPartsWarehouse").Include("MarketingDepartment").Include("AMarketingDepartment").Include("PartsManagementCostGrade").Include("APartsManagementCostGrade").Include("GradeCoefficient").Include("AGradeCoefficient").Include("Warehouse").Include("AWarehouse").Include("ChannelCapability").Include("AChannelCapability").OrderBy(r => r.Id);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:32
        //原分布类的函数全部转移到Ach                                                               
                public IQueryable<DealerServiceInfoHistory> GetDealerServiceInfoHistoryQuery() {
            return new DealerServiceInfoHistoryAch(this).GetDealerServiceInfoHistoryQuery();
        }
    }

}
