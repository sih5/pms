﻿using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PersonSubDealerAch : DcsSerivceAchieveBase {
        internal void InsertPersonSubDealerValidate(PersonSubDealer personSubDealer) {
            var dbPersonSubDealer = ObjectContext.PersonSubDealers.Where(r => r.PersonId == personSubDealer.PersonId && r.SubDealerId == personSubDealer.SubDealerId).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbPersonSubDealer != null)
                throw new ValidationException(ErrorStrings.PersonSubDealer_Validation1);
        }
        internal void UpdatePersonSubDealerValidate(PersonSubDealer personSubDealer) {
        }
        public void InsertPersonSubDealer(PersonSubDealer personSubDealer) {
            InsertToDatabase(personSubDealer);
            this.InsertPersonSubDealerValidate(personSubDealer);
        }
        public void UpdatePersonSubDealer(PersonSubDealer personSubDealer) {
            UpdateToDatabase(personSubDealer);
            this.UpdatePersonSubDealerValidate(personSubDealer);
        }
        public IQueryable<PersonSubDealer> GetPersonSubDealerWithDetail() {
            return ObjectContext.PersonSubDealers.Include("SubDealer").Include("Personnel").OrderBy(e => e.Id);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:32
        //原分布类的函数全部转移到Ach                                                               

        public void InsertPersonSubDealer(PersonSubDealer personSubDealer) {
            new PersonSubDealerAch(this).InsertPersonSubDealer(personSubDealer);
        }

        public void UpdatePersonSubDealer(PersonSubDealer personSubDealer) {
            new PersonSubDealerAch(this).UpdatePersonSubDealer(personSubDealer);
        }

        public IQueryable<PersonSubDealer> GetPersonSubDealerWithDetail() {
            return new PersonSubDealerAch(this).GetPersonSubDealerWithDetail();
        }
    }
}
