﻿using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class SubChannelRelationAch : DcsSerivceAchieveBase {
        internal void InsertSubChannelRelationValidate(SubChannelRelation subChannelRelation) {
            var dbSubChannelRelation = ObjectContext.SubChannelRelations.Where(r => r.ParentChanelId == subChannelRelation.ParentChanelId && r.SubChanelId == subChannelRelation.SubChanelId && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbSubChannelRelation != null) {
                throw new ValidationException(ErrorStrings.SubChannelRelation_Validation1);
            }
        }

        internal void UpdateSubChannelRelationValidate(SubChannelRelation subChannelRelation) {
            var dbSubChannelRelation = ObjectContext.SubChannelRelations.Where(r => r.Id == subChannelRelation.Id && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbSubChannelRelation == null) {
                throw new ValidationException(ErrorStrings.SubChannelRelation_Validation2);
            }
            var checkSubChannelRelation = ObjectContext.SubChannelRelations.Where(r => r.ParentChanelId == subChannelRelation.ParentChanelId && r.SubChanelId == subChannelRelation.SubChanelId && r.Status == (int)DcsBaseDataStatus.有效 && r.Id!=subChannelRelation.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(checkSubChannelRelation != null) {
                throw new ValidationException(ErrorStrings.SubChannelRelation_Validation4);
            }
        }

        public void InsertSubChannelRelation(SubChannelRelation subChannelRelation) {
            InsertToDatabase(subChannelRelation);
            this.InsertSubChannelRelationValidate(subChannelRelation);
        }

        public void UpdateSubChannelRelation(SubChannelRelation subChannelRelation) {
            UpdateToDatabase(subChannelRelation);
            this.UpdateSubChannelRelationValidate(subChannelRelation);
        }

    
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:32
        //原分布类的函数全部转移到Ach                                                               

        public void InsertSubChannelRelation(SubChannelRelation subChannelRelation) {
            new SubChannelRelationAch(this).InsertSubChannelRelation(subChannelRelation);
        }

        public void UpdateSubChannelRelation(SubChannelRelation subChannelRelation) {
            new SubChannelRelationAch(this).UpdateSubChannelRelation(subChannelRelation);
        }
    }
}
