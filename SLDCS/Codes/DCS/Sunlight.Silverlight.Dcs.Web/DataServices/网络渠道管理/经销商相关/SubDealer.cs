﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class SubDealerAch : DcsSerivceAchieveBase {
        internal void InsertSubDealerValidate(SubDealer subDealer) {
            var dbSubDealer = ObjectContext.SubDealers.Where(r => r.Code.ToLower() == subDealer.Code.ToLower() && r.DealerId == subDealer.DealerId).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbSubDealer != null)
                throw new ValidationException(string.Format(ErrorStrings.SubDealer_Validation2, subDealer.Code));
            var userInfo = Utils.GetCurrentUserInfo();
            subDealer.CreatorId = userInfo.Id;
            subDealer.CreatorName = userInfo.Name;
            subDealer.CreateTime = DateTime.Now;
        }
        internal void UpdateSubDealerValidate(SubDealer subDealer) {
            var userInfo = Utils.GetCurrentUserInfo();
            subDealer.ModifierId = userInfo.Id;
            subDealer.ModifierName = userInfo.Name;
            subDealer.ModifyTime = DateTime.Now;
        }
        public void InsertSubDealer(SubDealer subDealer) {
            InsertToDatabase(subDealer);
            this.InsertSubDealerValidate(subDealer);
        }
        public void UpdateSubDealer(SubDealer subDealer) {
            UpdateToDatabase(subDealer);
            this.UpdateSubDealerValidate(subDealer);
        }
        public SubDealer GetSubDealerByPersonId(int PersonId) {
            var personSubDealers = ObjectContext.PersonSubDealers.FirstOrDefault(e => e.PersonId == PersonId);
            if(personSubDealers == null)
                return null;
            return ObjectContext.SubDealers.SingleOrDefault(r => personSubDealers.SubDealerId == r.Id);
        }
        public SubDealer 查询登录人员与二级服务站关系关联二级站信息(int personId) {
            SubDealer subDealer = null;
            var personSubDealer = ObjectContext.PersonSubDealers.SingleOrDefault(r => r.PersonId == personId);
            if(personSubDealer != null) {
                subDealer = ObjectContext.SubDealers.SingleOrDefault(r => r.Id == personSubDealer.SubDealerId);
            }
            return subDealer;
        }
        public IQueryable<SubDealer> 查询二级站信息分公司() {
            var subDealers = ObjectContext.SubDealers;
            var dealerIds = subDealers.Select(r => r.DealerId);
            var userInfo = Silverlight.Web.Utils.GetCurrentUserInfo();
            var dealerServiceInfoes = ObjectContext.DealerServiceInfoes.Where(r => dealerIds.Contains(r.DealerId) && r.BranchId == userInfo.EnterpriseId).ToArray();
            var dealers = ObjectContext.Dealers.Where(r => dealerIds.Contains(r.Id)).ToArray();
            return subDealers.OrderBy(r => r.Id);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:32
        //原分布类的函数全部转移到Ach                                                               

        public void InsertSubDealer(SubDealer subDealer) {
            new SubDealerAch(this).InsertSubDealer(subDealer);
        }

        public void UpdateSubDealer(SubDealer subDealer) {
            new SubDealerAch(this).UpdateSubDealer(subDealer);
        }
                public SubDealer GetSubDealerByPersonId(int PersonId) {
            return new SubDealerAch(this).GetSubDealerByPersonId(PersonId);
        }

        public SubDealer 查询登录人员与二级服务站关系关联二级站信息(int personId) {
            return new SubDealerAch(this).查询登录人员与二级服务站关系关联二级站信息(personId);
        }

        public IQueryable<SubDealer> 查询二级站信息分公司() {
            return new SubDealerAch(this).查询二级站信息分公司();
        }
    }
}
