﻿using System;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class OverstockPartsAdjustBillAch : DcsSerivceAchieveBase {
        internal void InsertOverstockPartsAdjustBillValidate(OverstockPartsAdjustBill overstockPartsAdjustBill) {
            var userInfo = Utils.GetCurrentUserInfo();
            overstockPartsAdjustBill.CreatorId = userInfo.Id;
            overstockPartsAdjustBill.CreatorName = userInfo.Name;
            overstockPartsAdjustBill.CreateTime = DateTime.Now;
            if(string.IsNullOrWhiteSpace(overstockPartsAdjustBill.Code) || overstockPartsAdjustBill.Code == GlobalVar.ASSIGNED_BY_SERVER)
                overstockPartsAdjustBill.Code = CodeGenerator.Generate("OverstockPartsAdjustBill", overstockPartsAdjustBill.SalesUnitOwnerCompanyCode);
        }

        internal void UpdateOverstockPartsAdjustBillValidate(OverstockPartsAdjustBill overstockPartsAdjustBill) {
            var userInfo = Utils.GetCurrentUserInfo();
            overstockPartsAdjustBill.ModifierId = userInfo.Id;
            overstockPartsAdjustBill.ModifierName = userInfo.Name;
            overstockPartsAdjustBill.ModifyTime = DateTime.Now;
        }

        public void InsertOverstockPartsAdjustBill(OverstockPartsAdjustBill overstockPartsAdjustBill) {
            InsertToDatabase(overstockPartsAdjustBill);
            var overstockPartsAdjustDetails = ChangeSet.GetAssociatedChanges(overstockPartsAdjustBill, r => r.OverstockPartsAdjustDetails, ChangeOperation.Insert);
            foreach(OverstockPartsAdjustDetail overstockPartsAdjustDetail in overstockPartsAdjustDetails)
                InsertToDatabase(overstockPartsAdjustDetail);
            this.InsertOverstockPartsAdjustBillValidate(overstockPartsAdjustBill);
        }

        public void UpdateOverstockPartsAdjustBill(OverstockPartsAdjustBill overstockPartsAdjustBill) {
            overstockPartsAdjustBill.OverstockPartsAdjustDetails.Clear();
            UpdateToDatabase(overstockPartsAdjustBill);
            var overstockPartsAdjustDetails = ChangeSet.GetAssociatedChanges(overstockPartsAdjustBill, r => r.OverstockPartsAdjustDetails);
            foreach(OverstockPartsAdjustDetail overstockPartsAdjustDetail in overstockPartsAdjustDetails) {
                switch(ChangeSet.GetChangeOperation(overstockPartsAdjustDetail)) {
                    case ChangeOperation.Insert:
                        InsertToDatabase(overstockPartsAdjustDetail);
                        break;
                    case ChangeOperation.Update:
                        UpdateToDatabase(overstockPartsAdjustDetail);
                        break;
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(overstockPartsAdjustDetail);
                        break;
                }
            }
            this.UpdateOverstockPartsAdjustBillValidate(overstockPartsAdjustBill);
        }

        public OverstockPartsAdjustBill 查询调剂看板(int id) {
            var overstockPartsAdjustBill = ObjectContext.OverstockPartsAdjustBills.SingleOrDefault(r => r.Id == id && r.Status != (int)DcsOverstockPartsAdjustBillStatus.作废);
            if(overstockPartsAdjustBill != null) {
                var overstockPartsAdjustDetail = ObjectContext.OverstockPartsAdjustDetails.Where(r => r.OverstockPartsAdjustBillId == id).ToArray();
                var partsShippingOrders = ObjectContext.PartsShippingOrders.Where(r => r.OriginalRequirementBillId == id && r.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.积压件调剂单).ToArray();
                if(partsShippingOrders.Length > 0) {
                    var partsShippingOrderIds = partsShippingOrders.Select(r => r.Id);
                    var partsShippingOrderRefs = ObjectContext.PartsShippingOrderRefs.Where(r => partsShippingOrderIds.Contains(r.PartsShippingOrderId)).ToArray();
                    var partsShippingOrderDetails = ObjectContext.PartsShippingOrderDetails.Where(r => partsShippingOrderIds.Contains(r.PartsShippingOrderId)).ToArray();
                }
                var partsOutboundBills = ObjectContext.PartsOutboundBills.Where(r => r.OriginalRequirementBillId == id && r.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.积压件调剂单).ToArray();
                if(partsOutboundBills.Length > 0) {
                    var partsOutboundBillIds = partsOutboundBills.Select(r => r.Id);
                    var partsOutboundBillDetails = ObjectContext.PartsOutboundBillDetails.Where(r => partsOutboundBillIds.Contains(r.PartsOutboundBillId)).ToArray();
                }
                var partsInboundCheckBills = ObjectContext.PartsInboundCheckBills.Where(r => r.OriginalRequirementBillId == id && r.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.积压件调剂单).ToArray();
                if(partsInboundCheckBills.Length > 0) {
                    var partsInboundCheckBillIds = partsInboundCheckBills.Select(r => r.Id);
                    var partsInboundCheckBillDetails = ObjectContext.PartsInboundCheckBillDetails.Where(r => partsInboundCheckBillIds.Contains(r.PartsInboundCheckBillId)).ToArray();
                }
            }
            return overstockPartsAdjustBill;
        }

        public IQueryable<OverstockPartsAdjustBill> GetOverstockPartsAdjustBillWithDetails() {
            return ObjectContext.OverstockPartsAdjustBills.Include("OverstockPartsAdjustDetails").OrderBy(e => e.Id);
        }

        public IQueryable<OverstockPartsAdjustBill> GetOverstockPartsAdjustBillsByDestStorageCompanyId() {
            var userInfo = Utils.GetCurrentUserInfo();
            return ObjectContext.OverstockPartsAdjustBills.Where(e => e.DestStorageCompanyId == userInfo.EnterpriseId).OrderBy(e => e.Id);
        }

        public IQueryable<OverstockPartsAdjustBill> GetOverstockPartsAdjustBillsBySalesUnitId() {
            var userInfo = Utils.GetCurrentUserInfo();
            return ObjectContext.OverstockPartsAdjustBills.Where(e => e.SalesUnitOwnerCompanyId == userInfo.EnterpriseId).OrderBy(e => e.Id);
        }
    
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:32
        //原分布类的函数全部转移到Ach                                                               

        public void InsertOverstockPartsAdjustBill(OverstockPartsAdjustBill overstockPartsAdjustBill) {
            new OverstockPartsAdjustBillAch(this).InsertOverstockPartsAdjustBill(overstockPartsAdjustBill);
        }

        public void UpdateOverstockPartsAdjustBill(OverstockPartsAdjustBill overstockPartsAdjustBill) {
            new OverstockPartsAdjustBillAch(this).UpdateOverstockPartsAdjustBill(overstockPartsAdjustBill);
        }

        public OverstockPartsAdjustBill 查询调剂看板(int id) {
            return new OverstockPartsAdjustBillAch(this).查询调剂看板(id);
        }

        public IQueryable<OverstockPartsAdjustBill> GetOverstockPartsAdjustBillWithDetails() {
            return new OverstockPartsAdjustBillAch(this).GetOverstockPartsAdjustBillWithDetails();
        }

        public IQueryable<OverstockPartsAdjustBill> GetOverstockPartsAdjustBillsByDestStorageCompanyId() {
            return new OverstockPartsAdjustBillAch(this).GetOverstockPartsAdjustBillsByDestStorageCompanyId();
        }

        public IQueryable<OverstockPartsAdjustBill> GetOverstockPartsAdjustBillsBySalesUnitId() {
            return new OverstockPartsAdjustBillAch(this).GetOverstockPartsAdjustBillsBySalesUnitId();
        }
    }
}
