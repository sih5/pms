﻿using Sunlight.Silverlight.Web;
using System;
using System.Linq;
using System.ServiceModel.DomainServices.Server;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class OverstockPartsAppAch : DcsSerivceAchieveBase {
        internal void InsertOverstockPartsAppValidate(OverstockPartsApp overstockPartsApp) {
            var userInfo = Utils.GetCurrentUserInfo();
            overstockPartsApp.CreatorId = userInfo.Id;
            overstockPartsApp.CreatorName = userInfo.Name;
            overstockPartsApp.CreateTime = DateTime.Now;
            if(string.IsNullOrWhiteSpace(overstockPartsApp.Code) || overstockPartsApp.Code == GlobalVar.ASSIGNED_BY_SERVER)
                overstockPartsApp.Code = CodeGenerator.Generate("OverstockPartsApp", overstockPartsApp.StorageCompanyCode);
        }

        internal void UpdateOverstockPartsAppValidate(OverstockPartsApp overstockPartsApp) {
            var userInfo = Utils.GetCurrentUserInfo();
            overstockPartsApp.ModifierId = userInfo.Id;
            overstockPartsApp.ModifierName = userInfo.Name;
            overstockPartsApp.ModifyTime = DateTime.Now;
        }

        public void InsertOverstockPartsApp(OverstockPartsApp overstockPartsApp) {
            InsertToDatabase(overstockPartsApp);
            var overstockPartsAppDetails = ChangeSet.GetAssociatedChanges(overstockPartsApp, r => r.OverstockPartsAppDetails, ChangeOperation.Insert);
            foreach(OverstockPartsAppDetail overstockPartsAppDetail in overstockPartsAppDetails)
                InsertToDatabase(overstockPartsAppDetail);
            this.InsertOverstockPartsAppValidate(overstockPartsApp);
        }

        public void UpdateOverstockPartsApp(OverstockPartsApp overstockPartsApp) {
            overstockPartsApp.OverstockPartsAppDetails.Clear();
            UpdateToDatabase(overstockPartsApp);
            var overstockPartsAppDetails = ChangeSet.GetAssociatedChanges(overstockPartsApp, r => r.OverstockPartsAppDetails);
            foreach(OverstockPartsAppDetail overstockPartsAppDetail in overstockPartsAppDetails) {
                switch(ChangeSet.GetChangeOperation(overstockPartsAppDetail)) {
                    case ChangeOperation.Insert:
                        InsertToDatabase(overstockPartsAppDetail);
                        break;
                    case ChangeOperation.Update:
                        UpdateToDatabase(overstockPartsAppDetail);
                        break;
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(overstockPartsAppDetail);
                        break;
                }
            }
            this.UpdateOverstockPartsAppValidate(overstockPartsApp);
        }

        public OverstockPartsApp GetOverstockPartsAppWithDetailsById(int id) {
            var dbOverstockPartsApp = ObjectContext.OverstockPartsApps.SingleOrDefault(e => e.Id == id);
            if(dbOverstockPartsApp != null) {
                var details = ObjectContext.OverstockPartsAppDetails.Where(e => e.OverstockPartsAppId == dbOverstockPartsApp.Id).ToArray();
            }
            return dbOverstockPartsApp;
        }

        public IQueryable<OverstockPartsApp> GetOverstockPartsAppsWithDetails() {
            return ObjectContext.OverstockPartsApps.Include("OverstockPartsAppDetails").OrderBy(v => v.Id);
        }
    
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:32
        //原分布类的函数全部转移到Ach                                                               

        public void InsertOverstockPartsApp(OverstockPartsApp overstockPartsApp) {
            new OverstockPartsAppAch(this).InsertOverstockPartsApp(overstockPartsApp);
        }

        public void UpdateOverstockPartsApp(OverstockPartsApp overstockPartsApp) {
            new OverstockPartsAppAch(this).UpdateOverstockPartsApp(overstockPartsApp);
        }

        public OverstockPartsApp GetOverstockPartsAppWithDetailsById(int id) {
            return new OverstockPartsAppAch(this).GetOverstockPartsAppWithDetailsById(id);
        }

        public IQueryable<OverstockPartsApp> GetOverstockPartsAppsWithDetails() {
            return new OverstockPartsAppAch(this).GetOverstockPartsAppsWithDetails();
        }
    }
}
