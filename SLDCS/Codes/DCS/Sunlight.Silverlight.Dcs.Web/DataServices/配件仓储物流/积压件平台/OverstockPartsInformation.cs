﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class OverstockPartsInformationAch : DcsSerivceAchieveBase {
        public OverstockPartsInformationAch(DcsDomainService domainService)
            : base(domainService) {
        }

        internal void InsertOverstockPartsInformationValidate(OverstockPartsInformation overstockPartsInformation) {
            var dboverstockPartsInformation = ObjectContext.OverstockPartsInformations.Where(r => r.StorageCompanyId == overstockPartsInformation.StorageCompanyId && r.BranchId == overstockPartsInformation.BranchId && r.SparePartId == overstockPartsInformation.SparePartId).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dboverstockPartsInformation != null)
                throw new ValidationException(ErrorStrings.OverstockPartsInformation_Validation1);
            var userInfo = Utils.GetCurrentUserInfo();
            overstockPartsInformation.CreatorId = userInfo.Id;
            overstockPartsInformation.CreatorName = userInfo.Name;
            overstockPartsInformation.CreateTime = DateTime.Now;
        }
        public void InserOverstockPartsInformation(OverstockPartsInformation overstockPartsInformation) {
            InsertToDatabase(overstockPartsInformation);
            this.InsertOverstockPartsInformationValidate(overstockPartsInformation);
        }
        public void DeleteOverstockPartsInformation(OverstockPartsInformation overstockPartsInformation) {
            DeleteFromDatabase(overstockPartsInformation);
        }
        /// <summary>
        /// 根据ids查询积压件信息
        /// </summary>
        /// <param name="ids">积压件Id数组</param>
        /// <returns></returns>
        [Query(HasSideEffects = true)]
        public IEnumerable<OverstockPartsInformation> GetOverstockPartsInformationByArrayIds(int[] ids) {
            return ObjectContext.OverstockPartsInformations.Where(e => ids.Contains(e.Id)).OrderBy(r => r.Id);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:32
        //原分布类的函数全部转移到Ach                                                               

        public void InserOverstockPartsInformation(OverstockPartsInformation overstockPartsInformation) {
            new OverstockPartsInformationAch(this).InserOverstockPartsInformation(overstockPartsInformation);
        }

        public void DeleteOverstockPartsInformation(OverstockPartsInformation overstockPartsInformation) {
            new OverstockPartsInformationAch(this).DeleteOverstockPartsInformation(overstockPartsInformation);
        }
        /// <summary>
        /// 根据ids查询积压件信息
        /// </summary>
        /// <param name="ids">积压件Id数组</param>
        /// <returns></returns>
        [Query(HasSideEffects = true)]
        public IEnumerable<OverstockPartsInformation> GetOverstockPartsInformationByArrayIds(int[] ids) {
            return new OverstockPartsInformationAch(this).GetOverstockPartsInformationByArrayIds(ids);
        }
    }
}
