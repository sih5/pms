﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class OverstockPartsPlatFormBillAch : DcsSerivceAchieveBase {

        public IQueryable<VirtualOverstockPartsPlatFormBill> 查询积压件平台明细(string sparePartCode) {
            var user = Utils.GetCurrentUserInfo();
            var company = ObjectContext.Companies.Where(r => r.Id == user.EnterpriseId).First();
            var partsStocks = ObjectContext.PartsStocks.Where(r => ObjectContext.WarehouseAreaCategories.Any(v => v.Id == r.WarehouseAreaCategoryId && (v.Category == (int)DcsAreaType.保管区)));
            var returnpartsStocks = partsStocks.GroupBy(r => new {
                r.WarehouseId,
                r.StorageCompanyId,
                r.BranchId,
                r.PartId
            }).Select(v => new {
                v.Key.WarehouseId,
                v.Key.StorageCompanyId,
                v.Key.BranchId,
                v.Key.PartId,
                sumQuantity = v.Sum(r => r.Quantity)
            });
            var retrunpartsStocks = from partsStock in returnpartsStocks
                                    from sparePart in ObjectContext.SpareParts
                                    from branch in ObjectContext.Branches
                                    from wrehouse in ObjectContext.Warehouses
                                    where sparePart.Id == partsStock.PartId && sparePart.Status != (int)DcsMasterDataStatus.作废 && branch.Id == partsStock.BranchId && branch.Status != (int)DcsBaseDataStatus.作废 && wrehouse.Id == partsStock.WarehouseId && wrehouse.Status != (int)DcsBaseDataStatus.作废 && wrehouse.IsQualityWarehouse != true
                                    select new {
                                        partsStock.WarehouseId,
                                        WarehouseCode = wrehouse.Code,
                                        WarehouseName = wrehouse.Name,
                                        SparePartId = partsStock.PartId,
                                        SparePartCode = sparePart.Code,
                                        SparePartName = sparePart.Name,
                                        sparePart.MeasureUnit,
                                        partsStock.StorageCompanyId,
                                        partsStock.BranchId,
                                        BranchCode = branch.Code,
                                        BranchName = branch.Name,
                                        Quantity = partsStock.sumQuantity
                                    };
            var stockrestuls = from partsStock in retrunpartsStocks
                               join partsLockedStock in ObjectContext.PartsLockedStocks on new {
                                   pId = partsStock.SparePartId,
                                   cId = partsStock.StorageCompanyId,
                                   wId = partsStock.WarehouseId
                               } equals new {
                                   pId = partsLockedStock.PartId,
                                   cId = partsLockedStock.StorageCompanyId,
                                   wId = partsLockedStock.WarehouseId
                               } into details
                               from v in details.DefaultIfEmpty()
                               join bottomStock in ObjectContext.BottomStocks.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on new {
                                   pId = partsStock.SparePartId,
                                   cId = partsStock.StorageCompanyId,
                                   wId = partsStock.WarehouseId
                               } equals new {
                                   pId = bottomStock.SparePartId,
                                   cId = bottomStock.CompanyID,
                                   wId = (int)bottomStock.WarehouseID
                               } into bs
                               from m in bs.DefaultIfEmpty()
                               select new {
                                   partsStock.WarehouseId,
                                   partsStock.WarehouseCode,
                                   partsStock.WarehouseName,
                                   partsStock.SparePartId,
                                   partsStock.SparePartCode,
                                   partsStock.SparePartName,
                                   partsStock.MeasureUnit,
                                   partsStock.StorageCompanyId,
                                   partsStock.BranchId,
                                   partsStock.BranchCode,
                                   partsStock.BranchName,
                                   partsStock.Quantity,
                                   UsableQuantity = partsStock.Quantity - (v == null ? 0 : v.LockedQuantity) - (m == null ? 0 : m.StockQty ?? 0),
                               };

            var dealerStock = ObjectContext.DealerPartsStocks.Where(r => r.SubDealerId == -1);

            var overstockPartsPlatFormBills = ObjectContext.OverstockPartsPlatFormBills.Where(r => r.Status == (int)DcsOverstockPartsPlatFormBillStatus.生效);
            if (!string.IsNullOrWhiteSpace(sparePartCode)) {
                var codes = sparePartCode.Split(',').ToList();
                if (codes.Count == 1) {
                    overstockPartsPlatFormBills = overstockPartsPlatFormBills.Where(r => r.SparePartCode.Contains(codes.FirstOrDefault()));
                } else { 
                    overstockPartsPlatFormBills = overstockPartsPlatFormBills.Where(r => codes.Contains(r.SparePartCode));
                }
            }

            var result = from a in overstockPartsPlatFormBills
                         join b in stockrestuls on new {
                             StorageCompanyId = a.CenterId.Value,
                             SparePartId = a.SparePartId.Value,
                             WarehouseId = a.WarehouseId
                         } equals new {
                             StorageCompanyId = b.StorageCompanyId,
                             SparePartId = b.SparePartId,
                             WarehouseId = (int?)b.WarehouseId
                         } into tempTable2
                         from t2 in tempTable2.DefaultIfEmpty()
                         join c in ObjectContext.PartsRetailGuidePrices.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on a.SparePartId equals c.SparePartId into tempTable
                         from t1 in tempTable.DefaultIfEmpty()
                         join d in ObjectContext.DealerPartsStocks.Where(r => r.SubDealerId == -1) on new {
                             SparePartId = a.SparePartId.Value,
                             DealerId = a.DealerId.Value
                         } equals new {
                             d.SparePartId,
                             d.DealerId
                         } into tempTable3
                         from t3 in tempTable3.DefaultIfEmpty()
                         join e in ObjectContext.Companies on a.DealerId equals e.Id into tempTable4
                         from t4 in tempTable4.DefaultIfEmpty()
                         select new VirtualOverstockPartsPlatFormBill {
                             Id = a.Id,
                             CenterId = a.CenterId,
                             CenterName = a.CenterName,
                             DealerId = a.DealerId,
                             DealerCode = a.DealerCode,
                             DealerName = a.DealerName,
                             SparePartId = a.SparePartId.Value,
                             SparePartCode = a.SparePartCode,
                             SparePartName = a.SparePartName,
                             DiscountRate = a.DiscountRate,
                             EntityStatus = a.EntityStatus,
                             Status = a.Status,
                             RetailGuidePrice = t1.RetailGuidePrice,
                             DiscountedPrice = a.DiscountedPrice,
                             Quantity = a.DealerId != null && t4.Type != (int)DcsCompanyType.服务站兼代理库 ? t3.Quantity : t2.UsableQuantity,
                             WarehouseId = a.WarehouseId,
                             WarehouseCode = a.WarehouseCode,
                             WarehouseName = a.WarehouseName
                         };
            return result.Where(r => r.Quantity > 0).OrderBy(r => r.Id);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:32
        //原分布类的函数全部转移到Ach                                                               
         public IQueryable<VirtualOverstockPartsPlatFormBill> 查询积压件平台明细(string sparePartCode) {
            return new OverstockPartsPlatFormBillAch(this).查询积压件平台明细(sparePartCode);
        }
    }
}
