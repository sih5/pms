﻿using Sunlight.Silverlight.Web;
using System;
using System.Linq;
using System.ServiceModel.DomainServices.Server;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class OverstockPartsRecommendBillAch : DcsSerivceAchieveBase {
        [Query(HasSideEffects = true)]
        public IQueryable<OverstockPartsRecommendBill> GetOverstockPartsRecommendBillAmounts() {
            var userInfo = Utils.GetCurrentUserInfo();
            var company = ObjectContext.Companies.Single(o => o.Id == userInfo.EnterpriseId);
            var query = (from oprb in ObjectContext.OverstockPartsRecommendBills
                         join prgp in ObjectContext.PartsRetailGuidePrices on oprb.SparePartId equals prgp.SparePartId
                         join ps in ObjectContext.PartsStocks on
                         new {
                             CenterId = oprb.CenterId.Value,
                             WarehouseId = oprb.WarehouseId ?? 0,
                             PartId = oprb.SparePartId.Value
                         } equals new {
                             CenterId = ps.StorageCompanyId,
                             WarehouseId = ps.WarehouseId,
                             PartId = ps.PartId
                         } into psTable
                         from psResult in psTable.DefaultIfEmpty()
                         join pls in ObjectContext.PartsLockedStocks on
                         new {
                             CenterId = oprb.CenterId.Value,
                             WarehouseId = oprb.WarehouseId ?? 0,
                             PartId = oprb.SparePartId.Value
                         } equals new {
                             CenterId = pls.StorageCompanyId,
                             WarehouseId = pls.WarehouseId,
                             PartId = pls.PartId
                         } into plsTable
                         from plsResult in plsTable.DefaultIfEmpty()
                         join bs in ObjectContext.BottomStocks on
                         new {
                             CenterId = oprb.CenterId.Value,
                             WarehouseId = oprb.WarehouseId ?? 0,
                             PartId = oprb.SparePartId.Value
                         } equals new {
                             CenterId = bs.CompanyID,
                             WarehouseId = bs.WarehouseID ?? 0,
                             PartId = bs.SparePartId
                         } into bsTable
                         from bsResult in bsTable.DefaultIfEmpty()
                         join dps in ObjectContext.DealerPartsStocks on new {
                             DealerId = oprb.DealerId ?? 0,
                             SparePartId = oprb.SparePartId.Value
                         } equals new {
                             DealerId = dps.DealerId,
                             SparePartId = dps.SparePartId
                         } into dpsTable
                         from dpsResult in dpsTable.DefaultIfEmpty()
                         join c in ObjectContext.Companies on new {
                             DealerId = oprb.DealerId ?? 0,
                         } equals new {
                             DealerId = c.Id,
                         } into cTable
                         from cResult in cTable.DefaultIfEmpty()
                         where (dpsResult.Quantity > 0 || (psResult.Quantity - (plsResult == null ?0 : plsResult.LockedQuantity) - (bsResult == null? 0 : bsResult.StockQty ?? 0)) > 0) &&
                         (company.Type == (int)DcsCompanyType.代理库 ? (oprb.CenterId == company.Id && !oprb.DealerId.HasValue) : 1 == 1)
                                 && ((company.Type == (int)DcsCompanyType.服务站 || company.Type == (int)DcsCompanyType.服务站兼代理库) ? oprb.DealerId == company.Id : 1 == 1)
                         select new {
                             Id = oprb.Id,
                             CenterId = oprb.CenterId,
                             CenterName = oprb.CenterName,
                             WarehouseId = oprb.WarehouseId,
                             WarehouseCode = oprb.WarehouseCode,
                             WarehouseName = oprb.WarehouseName,
                             DealerId = oprb.DealerId,
                             DealerCode = oprb.DealerCode,
                             DealerName = oprb.DealerName,
                             SparePartId = oprb.SparePartId.Value,
                             SparePartCode = oprb.SparePartCode,
                             SparePartName = oprb.SparePartName,
                             CreateTime = oprb.CreateTime,
                             OverstockPartsAmount = oprb.DealerId.HasValue && cResult.Type != (int)DcsCompanyType.服务站兼代理库 ? dpsResult.Quantity : (psResult == null? 0 : psResult.Quantity) - (plsResult == null ? 0 : plsResult.LockedQuantity) - (bsResult == null ? 0: bsResult.StockQty ?? 0),
                             Price = prgp.RetailGuidePrice
                         }).ToList();

            return (from result in query
                    group result by new {
                        result.Id,
                        result.CenterId,
                        result.CenterName,
                        result.WarehouseId,
                        result.WarehouseCode,
                        result.WarehouseName,
                        result.DealerId,
                        result.DealerCode,
                        result.DealerName,
                        result.SparePartId,
                        result.SparePartCode,
                        result.SparePartName,
                        result.CreateTime,
                    } into t
                    select new OverstockPartsRecommendBill {
                        Id = t.Key.Id,
                        CenterId = t.Key.CenterId,
                        CenterName = t.Key.CenterName,
                        WarehouseId = t.Key.WarehouseId,
                        WarehouseCode = t.Key.WarehouseCode,
                        WarehouseName = t.Key.WarehouseName,
                        DealerId = t.Key.DealerId,
                        DealerCode = t.Key.DealerCode,
                        DealerName = t.Key.DealerName,
                        SparePartId = t.Key.SparePartId,
                        SparePartCode = t.Key.SparePartCode,
                        SparePartName = t.Key.SparePartName,
                        CreateTime = t.Key.CreateTime,
                        OverstockPartsAmount = t.Sum(o => o.OverstockPartsAmount),
                        Price = t.Max(o => o.Price)
                    }).AsQueryable();
        }

        public void InsertOverstockPartsRecommendBill(OverstockPartsRecommendBill overstockPartsRecommendBill) {
        }

        public void UpdateOverstockPartsRecommendBill(OverstockPartsRecommendBill OoverstockPartsRecommendBill) {
        }

        [Query(HasSideEffects = true)]
        public IQueryable<OverstockPartsRecommendBill> GetOverstockPartsRecommendBillsIds(int[] ids) {
            return ObjectContext.OverstockPartsRecommendBills.Where(o => ids.Contains(o.Id)).AsQueryable();
        }
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:32
        //原分布类的函数全部转移到Ach 
        [Query(HasSideEffects = true)]
        public IQueryable<OverstockPartsRecommendBill> GetOverstockPartsRecommendBillAmounts() {
            return new OverstockPartsRecommendBillAch(this).GetOverstockPartsRecommendBillAmounts();
        }

        [Query(HasSideEffects = true)]
        public IQueryable<OverstockPartsRecommendBill> GetOverstockPartsRecommendBillsIds(int[] ids) {
            return new OverstockPartsRecommendBillAch(this).GetOverstockPartsRecommendBillsIds(ids);
        }

        public void InsertOverstockPartsRecommendBill(OverstockPartsRecommendBill overstockPartsRecommendBill) {
            new OverstockPartsRecommendBillAch(this).InsertOverstockPartsRecommendBill(overstockPartsRecommendBill);
        }

        public void UpdateOverstockPartsRecommendBill(OverstockPartsRecommendBill overstockPartsRecommendBill) {
            new OverstockPartsRecommendBillAch(this).UpdateOverstockPartsRecommendBill(overstockPartsRecommendBill);
        }
    }
}
