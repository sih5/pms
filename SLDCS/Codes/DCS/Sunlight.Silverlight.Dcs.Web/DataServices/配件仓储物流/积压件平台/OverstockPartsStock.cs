﻿using System.Linq;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class OverstockPartsStockAch : DcsSerivceAchieveBase {
        public OverstockPartsStockAch(DcsDomainService domainService)
            : base(domainService) {
        }

        //企业Id,仓储企业Id，仓储企业类型，配件销售类型Id
        public IQueryable<OverstockPartsStock> 查询积压件库存区分企业(int companyId, int? storageCompanyId, int? storageCompanyType, int? partsSalesCategoryId) {
            var dbCompany = ObjectContext.Companies.SingleOrDefault(r => r.Id == companyId);
            if(dbCompany == null)
                return null;
            IQueryable<OverstockPartsInformation> overstockPartsInformations = ObjectContext.OverstockPartsInformations;
            if(partsSalesCategoryId.HasValue)
                overstockPartsInformations = overstockPartsInformations.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId.Value);
            switch(dbCompany.Type) {
                case (int)DcsCompanyType.分公司:
                    overstockPartsInformations = overstockPartsInformations.Where(r => r.BranchId == companyId);
                    break;
                case (int)DcsCompanyType.代理库:
                case (int)DcsCompanyType.服务站兼代理库:
                    overstockPartsInformations = overstockPartsInformations.Where(r => r.StorageCompanyType == (int)DcsCompanyType.服务站 || r.StorageCompanyType == (int)DcsCompanyType.代理库);
                    break;
                case (int)DcsCompanyType.服务站:
                    overstockPartsInformations = overstockPartsInformations.Where(r => r.StorageCompanyType == (int)DcsCompanyType.服务站);
                    break;
                default:
                    return null;
            }
            //查询数据来源
            var resultSource = from overstockPartsInformation in overstockPartsInformations
                               from company in ObjectContext.Companies
                               from branch in ObjectContext.Branches
                               from sparePart in ObjectContext.SpareParts
                               let ds = ObjectContext.DealerPartsStocks.Where(v => v.DealerId == overstockPartsInformation.StorageCompanyId && v.BranchId == overstockPartsInformation.BranchId && v.SparePartId == overstockPartsInformation.SparePartId).Sum(r => r.Quantity)
                               where branch.Id == overstockPartsInformation.BranchId && branch.Status == (int)DcsMasterDataStatus.有效 &&
                                     company.Id == overstockPartsInformation.StorageCompanyId && company.Status == (int)DcsMasterDataStatus.有效 &&
                                     sparePart.Id == overstockPartsInformation.SparePartId && sparePart.Status == (int)DcsMasterDataStatus.有效
                               select new {
                                   OverstockPartsInformationId = overstockPartsInformation.Id,
                                   overstockPartsInformation.StorageCompanyId,
                                   StorageCompanyType = company.Type,
                                   StorageCompanyCode = company.Code,
                                   StorageCompanyName = company.Name,
                                   company.ProvinceName,
                                   company.CityName,
                                   company.CountyName,
                                   overstockPartsInformation.BranchId,
                                   BranchName = branch.Name,
                                   BranchCode = branch.Code,
                                   overstockPartsInformation.SparePartId,
                                   SparePartCode = sparePart.Code,
                                   SparePartName = sparePart.Name,
                                   overstockPartsInformation.CreateTime,
                                   DealStock = ds,
                                   Stock = ObjectContext.PartsStocks.Where(v => v.StorageCompanyId == overstockPartsInformation.StorageCompanyId && v.BranchId == overstockPartsInformation.BranchId && v.PartId == overstockPartsInformation.SparePartId).Sum(v => v.Quantity),
                                   LockedStork = ObjectContext.PartsLockedStocks.Where(v => v.StorageCompanyId == overstockPartsInformation.StorageCompanyId && v.BranchId == overstockPartsInformation.BranchId && v.PartId == overstockPartsInformation.SparePartId).Sum(v => v.LockedQuantity),
                                   //经设计确认，用分公司Id，配件Id,销售类型Id确认WMS仓库配件冻结库存
                                   CongelationStockQty = ObjectContext.WmsCongelationStockViews.Where(v => v.BranchId == overstockPartsInformation.BranchId && v.SparePartId == overstockPartsInformation.SparePartId && v.PartsSalesCategoryId == overstockPartsInformation.PartsSalesCategoryId).Sum(r => r.CongelationStockQty),
                                   DisabledStock = ObjectContext.WmsCongelationStockViews.Where(v => v.BranchId == overstockPartsInformation.BranchId && v.SparePartId == overstockPartsInformation.SparePartId && v.PartsSalesCategoryId == overstockPartsInformation.PartsSalesCategoryId).Sum(r => r.DisabledStock),
                                   Dealprice = overstockPartsInformation.dealprice,
                                   overstockPartsInformation.SalePrice,
                                   DealAmount = overstockPartsInformation.dealprice * ds,
                                   overstockPartsInformation.PartsSalesCategoryId,
                                   overstockPartsInformation.PartsSalesCategoryName,
                                   overstockPartsInformation.ContactPerson,
                                   overstockPartsInformation.ContactPhone
                               };
            //根据存储企业类型调整UsableQuantity赋值，尽可能简化sql语句
            var result = resultSource.Select(v => new OverstockPartsStock {
                OverstockPartsInformationId = v.OverstockPartsInformationId,
                StorageCompanyId = v.StorageCompanyId,
                StorageCompanyType = v.StorageCompanyType,
                StorageCompanyCode = v.StorageCompanyCode,
                StorageCompanyName = v.StorageCompanyName,
                ProvinceName = v.ProvinceName,
                CityName = v.CityName,
                CountyName = v.CountyName,
                BranchId = v.BranchId,
                BranchName = v.BranchName,
                BranchCode = v.BranchCode,
                SparePartId = v.SparePartId,
                SparePartCode = v.SparePartCode,
                SparePartName = v.SparePartName,
                CreateTime = v.CreateTime,
                UsableQuantity = v.StorageCompanyType == (int)DcsCompanyType.服务站 ? v.DealStock : (v.Stock - ((int?)v.LockedStork ?? 0) - ((int?)v.CongelationStockQty ?? 0) - ((int?)v.DisabledStock ?? 0)),
                Dealprice = v.Dealprice,
                //DealAmount = v.DealAmount,
                DealAmount = v.StorageCompanyType == (int)DcsCompanyType.服务站 ? v.DealAmount : (v.Dealprice * (v.Stock - ((int?)v.LockedStork ?? 0) - ((int?)v.CongelationStockQty ?? 0) - ((int?)v.DisabledStock ?? 0))),
                SalePrice = v.SalePrice,
                PartsSalesCategoryId = v.PartsSalesCategoryId,
                PartsSalesCategoryName = v.PartsSalesCategoryName,
                LinkMan = v.ContactPerson,
                LinkPhone = v.ContactPhone
            });
            return result.OrderBy(r => r.OverstockPartsInformationId);
        }
        //企业Id，仓储企业Id，配件销售类型Id
        public IQueryable<OverstockPartsStock> 查询积压件库存(int companyId, int? storageCompanyId, int? storageCompanyType, int? partsSalesCategoryId) {
            var dbCompany = ObjectContext.Companies.SingleOrDefault(r => r.Id == companyId);
            if(dbCompany == null)
                return null;
            IQueryable<OverstockPartsInformation> overstockPartsInformations = ObjectContext.OverstockPartsInformations;
            if(storageCompanyId.HasValue)
                overstockPartsInformations = overstockPartsInformations.Where(r => r.StorageCompanyId == storageCompanyId.Value);
            if(storageCompanyType.HasValue)
                overstockPartsInformations = overstockPartsInformations.Where(r => r.StorageCompanyType == storageCompanyType.Value);
            if(partsSalesCategoryId.HasValue)
                overstockPartsInformations = overstockPartsInformations.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId.Value);
            switch(dbCompany.Type) {
                case (int)DcsCompanyType.分公司:
                    overstockPartsInformations = overstockPartsInformations.Where(r => r.BranchId == companyId);
                    break;
                case (int)DcsCompanyType.服务站:
                    var salesCompanyIdsDealer = ObjectContext.CustomerInformations.Where(r => r.CustomerCompanyId == companyId && r.Status == (int)DcsMasterDataStatus.有效).Select(r => r.SalesCompanyId).ToArray();
                    if(salesCompanyIdsDealer.Length == 0)
                        return null;
                    overstockPartsInformations = overstockPartsInformations.Where(r => salesCompanyIdsDealer.Contains(r.BranchId));
                    break;
                case (int)DcsCompanyType.代理库:
                    var salesCompanyIdsAgency = ObjectContext.CustomerInformations.Where(r => r.CustomerCompanyId == companyId && r.Status == (int)DcsMasterDataStatus.有效).Select(r => r.SalesCompanyId).ToArray();
                    if(salesCompanyIdsAgency.Length == 0)
                        return null;
                    overstockPartsInformations = overstockPartsInformations.Where(r => salesCompanyIdsAgency.Contains(r.BranchId));
                    break;
                default:
                    return null;
            }
            //查询数据来源
            var resultSource = from overstockPartsInformation in overstockPartsInformations
                               from company in ObjectContext.Companies
                               from branch in ObjectContext.Branches
                               from sparePart in ObjectContext.SpareParts
                               where branch.Id == overstockPartsInformation.BranchId && branch.Status == (int)DcsMasterDataStatus.有效 &&
                                     company.Id == overstockPartsInformation.StorageCompanyId && company.Status == (int)DcsMasterDataStatus.有效 &&
                                     sparePart.Id == overstockPartsInformation.SparePartId && sparePart.Status == (int)DcsMasterDataStatus.有效
                               select new {
                                   OverstockPartsInformationId = overstockPartsInformation.Id,
                                   overstockPartsInformation.StorageCompanyId,
                                   StorageCompanyType = company.Type,
                                   StorageCompanyCode = company.Code,
                                   StorageCompanyName = company.Name,
                                   company.ProvinceName,
                                   company.CityName,
                                   company.CountyName,
                                   overstockPartsInformation.BranchId,
                                   BranchName = branch.Name,
                                   BranchCode = branch.Code,
                                   overstockPartsInformation.SparePartId,
                                   SparePartCode = sparePart.Code,
                                   SparePartName = sparePart.Name,
                                   overstockPartsInformation.CreateTime,
                                   DealStock = ObjectContext.DealerPartsStocks.Where(v => v.DealerId == overstockPartsInformation.StorageCompanyId && v.BranchId == overstockPartsInformation.BranchId && v.SparePartId == overstockPartsInformation.SparePartId).Sum(r => r.Quantity),
                                   Stock = ObjectContext.PartsStocks.Where(v => v.StorageCompanyId == overstockPartsInformation.StorageCompanyId && v.BranchId == overstockPartsInformation.BranchId && v.PartId == overstockPartsInformation.SparePartId).Sum(v => v.Quantity),
                                   LockedStork = ObjectContext.PartsLockedStocks.Where(v => v.StorageCompanyId == overstockPartsInformation.StorageCompanyId && v.BranchId == overstockPartsInformation.BranchId && v.PartId == overstockPartsInformation.SparePartId).Sum(v => v.LockedQuantity),
                                   //经设计确认，用分公司Id，配件Id,销售类型Id确认WMS仓库配件冻结库存
                                   CongelationStockQty = ObjectContext.WmsCongelationStockViews.Where(v => v.BranchId == overstockPartsInformation.BranchId && v.SparePartId == overstockPartsInformation.SparePartId && v.PartsSalesCategoryId == overstockPartsInformation.PartsSalesCategoryId).Sum(r => r.CongelationStockQty),
                                   DisabledStock = ObjectContext.WmsCongelationStockViews.Where(v => v.BranchId == overstockPartsInformation.BranchId && v.SparePartId == overstockPartsInformation.SparePartId && v.PartsSalesCategoryId == overstockPartsInformation.PartsSalesCategoryId).Sum(r => r.DisabledStock),
                                   Dealprice = overstockPartsInformation.dealprice,
                                   overstockPartsInformation.PartsSalesCategoryId,
                                   overstockPartsInformation.PartsSalesCategoryName,
                                   overstockPartsInformation.ContactPerson,
                                   overstockPartsInformation.ContactPhone
                               };
            //根据存储企业类型调整UsableQuantity赋值，尽可能简化sql语句
            var result = resultSource.Select(v => new OverstockPartsStock {
                OverstockPartsInformationId = v.OverstockPartsInformationId,
                StorageCompanyId = v.StorageCompanyId,
                StorageCompanyType = v.StorageCompanyType,
                StorageCompanyCode = v.StorageCompanyCode,
                StorageCompanyName = v.StorageCompanyName,
                ProvinceName = v.ProvinceName,
                CityName = v.CityName,
                CountyName = v.CountyName,
                BranchId = v.BranchId,
                BranchName = v.BranchName,
                BranchCode = v.BranchCode,
                SparePartId = v.SparePartId,
                SparePartCode = v.SparePartCode,
                SparePartName = v.SparePartName,
                CreateTime = v.CreateTime,
                UsableQuantity = v.StorageCompanyType == (int)DcsCompanyType.服务站 ? v.DealStock : (v.Stock - ((int?)v.LockedStork ?? 0) - ((int?)v.CongelationStockQty ?? 0) - ((int?)v.DisabledStock ?? 0)),
                Dealprice = v.Dealprice,
                PartsSalesCategoryId = v.PartsSalesCategoryId,
                PartsSalesCategoryName = v.PartsSalesCategoryName,
                LinkMan = v.ContactPerson,
                LinkPhone = v.ContactPhone
            });
            return result.OrderBy(r => r.OverstockPartsInformationId);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:32
        //原分布类的函数全部转移到Ach                                                               
                public IQueryable<OverstockPartsStock> 查询积压件库存区分企业(int companyId, int? storageCompanyId, int? storageCompanyType, int? partsSalesCategoryId) {
            return new OverstockPartsStockAch(this).查询积压件库存区分企业(companyId,storageCompanyId,storageCompanyType,partsSalesCategoryId);
        }
                public IQueryable<OverstockPartsStock> 查询积压件库存(int companyId, int? storageCompanyId, int? storageCompanyType, int? partsSalesCategoryId) {
            return new OverstockPartsStockAch(this).查询积压件库存(companyId,storageCompanyId,storageCompanyType,partsSalesCategoryId);
        }
    }
}
