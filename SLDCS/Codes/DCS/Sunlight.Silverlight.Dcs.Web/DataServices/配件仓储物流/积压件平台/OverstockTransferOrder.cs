﻿using System;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class OverstockTransferOrderAch : DcsSerivceAchieveBase {
        internal void InsertOverstockTransferOrderValidate(OverstockTransferOrder overstockTransferOrder) {
            var userInfo = Utils.GetCurrentUserInfo();
            overstockTransferOrder.CreatorId = userInfo.Id;
            overstockTransferOrder.CreatorName = userInfo.Name;
            overstockTransferOrder.CreateTime = DateTime.Now;
            if(string.IsNullOrWhiteSpace(overstockTransferOrder.Code) || overstockTransferOrder.Code == GlobalVar.ASSIGNED_BY_SERVER)
                overstockTransferOrder.Code = CodeGenerator.Generate("OverstockTransferOrder", overstockTransferOrder.TransferInCorpCode);
        }

        internal void UpdateOverstockTransferOrderValidate(OverstockTransferOrder overstockTransferOrder) {
            var userInfo = Utils.GetCurrentUserInfo();
            overstockTransferOrder.ModifierId = userInfo.Id;
            overstockTransferOrder.ModifierName = userInfo.Name;
            overstockTransferOrder.ModifyTime = DateTime.Now;
        }

        public void InsertOverstockTransferOrder(OverstockTransferOrder overstockTransferOrder) {
            InsertToDatabase(overstockTransferOrder);
            var overstockTransferOrderDetails = ChangeSet.GetAssociatedChanges(overstockTransferOrder, r => r.OverstockTransferOrderDetails, ChangeOperation.Insert);
            foreach(OverstockTransferOrderDetail overstockTransferOrderDetail in overstockTransferOrderDetails)
                InsertToDatabase(overstockTransferOrderDetail);
            this.InsertOverstockTransferOrderValidate(overstockTransferOrder);
        }

        public void UpdateOverstockTransferOrder(OverstockTransferOrder overstockTransferOrder) {
            UpdateToDatabase(overstockTransferOrder);
            this.UpdateOverstockTransferOrderValidate(overstockTransferOrder);
        }

        public IQueryable<OverstockTransferOrder> GetOverstockTransferOrderWithDetails() {
            return ObjectContext.OverstockTransferOrders.Include("OverstockTransferOrderDetails").OrderBy(e => e.Id);
        }

        public IQueryable<OverstockTransferOrder> GetOverstockTransferOrderByCurrentUser() {
            var userInfo = Utils.GetCurrentUserInfo();
            return ObjectContext.OverstockTransferOrders.Include("OverstockTransferOrderDetails").Where(r => r.TransferInCorpId == userInfo.EnterpriseId || (r.TransferOutCorpId == userInfo.EnterpriseId 
                && (r.Status == (int)DCSOverstockPartsTransferOrderStatus.提交 || r.Status == (int)DCSOverstockPartsTransferOrderStatus.生效))).OrderBy(e => e.Id);
        }

        public void InsertOverstockTransferOrderDetail(OverstockTransferOrderDetail overstockTransferOrderDetail) {
           
        }
        
        public void UpdateOverstockTransferOrderDetail(OverstockTransferOrderDetail overstockTransferOrderDetail) {
            
        }
    
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:32
        //原分布类的函数全部转移到Ach                                                               

        public void InsertOverstockTransferOrder(OverstockTransferOrder overstockTransferOrder) {
            new OverstockTransferOrderAch(this).InsertOverstockTransferOrder(overstockTransferOrder);
        }

        public void UpdateOverstockTransferOrder(OverstockTransferOrder overstockTransferOrder) {
            new OverstockTransferOrderAch(this).UpdateOverstockTransferOrder(overstockTransferOrder);
        }

        public void InsertOverstockTransferOrderDetail(OverstockTransferOrderDetail overstockTransferOrderDetail) {
            new OverstockTransferOrderAch(this).InsertOverstockTransferOrderDetail(overstockTransferOrderDetail);
        }

        public void UpdateOverstockTransferOrderDetail(OverstockTransferOrderDetail overstockTransferOrderDetail) {
            new OverstockTransferOrderAch(this).UpdateOverstockTransferOrderDetail(overstockTransferOrderDetail);
        }

        public IQueryable<OverstockTransferOrder> GetOverstockTransferOrderWithDetails() {
            return new OverstockTransferOrderAch(this).GetOverstockTransferOrderWithDetails();
        }

        public IQueryable<OverstockTransferOrder> GetOverstockTransferOrderByCurrentUser() {
            return new OverstockTransferOrderAch(this).GetOverstockTransferOrderByCurrentUser();
        }

    }
}
