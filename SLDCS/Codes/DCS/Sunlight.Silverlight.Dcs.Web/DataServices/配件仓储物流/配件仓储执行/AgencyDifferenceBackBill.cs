﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class AgencyDifferenceBackBillAch : DcsSerivceAchieveBase {
        public AgencyDifferenceBackBillAch(DcsDomainService domainService)
            : base(domainService) {
        }
        internal void InsertAgencyDifferenceBackBillValidate(AgencyDifferenceBackBill agencyDifferenceBackBill) {
            if(string.IsNullOrWhiteSpace(agencyDifferenceBackBill.Code) || agencyDifferenceBackBill.Code == GlobalVar.ASSIGNED_BY_SERVER)
                agencyDifferenceBackBill.Code = CodeGenerator.Generate("AgencyDifferenceBackBill", agencyDifferenceBackBill.StorageCompanyCode);
            var userInfo = Utils.GetCurrentUserInfo();
            agencyDifferenceBackBill.CreatorId = userInfo.Id;
            agencyDifferenceBackBill.CreatorName = userInfo.Name;
            agencyDifferenceBackBill.CreateTime = DateTime.Now;
        }

        internal void UpdateAgencyDifferenceBackBillValidate(AgencyDifferenceBackBill agencyDifferenceBackBill) {
            var userInfo = Utils.GetCurrentUserInfo();
            agencyDifferenceBackBill.ModifierId = userInfo.Id;
            agencyDifferenceBackBill.ModifierName = userInfo.Name;
            agencyDifferenceBackBill.ModifyTime = DateTime.Now;
        }

        public void InsertAgencyDifferenceBackBill(AgencyDifferenceBackBill agencyDifferenceBackBill) {                    
           
        }        
        public void UpdateAgencyDifferenceBackBill(AgencyDifferenceBackBill agencyDifferenceBackBill) {
            
        }
        public IQueryable<AgencyDifferenceBackBill> GetAgencyDifferenceBackBillsByUser() {
            var userInfo = Utils.GetCurrentUserInfo();
            var company = ObjectContext.Companies.Where(t => t.Id == userInfo.EnterpriseId).First();
            if(company.Type == (int)DcsCompanyType.分公司) {
               //查询人员是否维护了责任关系绑定
                var resRelationship = ObjectContext.ResRelationships.Where(t => t.Status == (int)DcsBaseDataStatus.有效 && ObjectContext.ResponsibleMembers.Any(y => y.ResTem == t.ResTem && y.Status == (int)DcsBaseDataStatus.有效 && ObjectContext.ResponsibleDetails.Any(p => p.ResponMemberId == y.Id && p.PersonelId == userInfo.Id))).ToArray();
                if(resRelationship.Count()>0) {
                    return ObjectContext.AgencyDifferenceBackBills.Where(w => ObjectContext.ResRelationships.Any(t => t.Id==w.ResRelationshipId && t.Status == (int)DcsBaseDataStatus.有效 && ObjectContext.ResponsibleMembers.Any(y => y.ResTem == t.ResTem && y.Status == (int)DcsBaseDataStatus.有效 && ObjectContext.ResponsibleDetails.Any(p => p.ResponMemberId == y.Id && p.PersonelId == userInfo.Id)))).OrderBy(e => e.Id);
                }
                return ObjectContext.AgencyDifferenceBackBills.OrderBy(e => e.Id);
            } else {
                return ObjectContext.AgencyDifferenceBackBills.Where(t=>t.StorageCompanyId==userInfo.EnterpriseId).OrderBy(e => e.Id);
            }
            
        }
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:32
        //原分布类的函数全部转移到Ach                                                               

        public void InsertPartsOutboundBill(AgencyDifferenceBackBill partsOutboundBill) {
            new AgencyDifferenceBackBillAch(this).InsertAgencyDifferenceBackBill(partsOutboundBill);
        }

        public void UpdatePartsOutboundBill(AgencyDifferenceBackBill partsOutboundBill) {
            new AgencyDifferenceBackBillAch(this).UpdateAgencyDifferenceBackBill(partsOutboundBill);
        }
        public IQueryable<AgencyDifferenceBackBill> GetAgencyDifferenceBackBillsByUser() {
            return new AgencyDifferenceBackBillAch(this).GetAgencyDifferenceBackBillsByUser();
        }
    }
}
