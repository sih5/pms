﻿using System;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class AgencyPartsOutboundBillAch : DcsSerivceAchieveBase {
        public AgencyPartsOutboundBillAch(DcsDomainService domainService)
            : base(domainService) {
        }

        internal void InsertAgencyPartsOutboundBillValidate(AgencyPartsOutboundBill agencyPartsOutboundBill) {
            if(string.IsNullOrWhiteSpace(agencyPartsOutboundBill.Code) || agencyPartsOutboundBill.Code == GlobalVar.ASSIGNED_BY_SERVER)
                agencyPartsOutboundBill.Code = CodeGenerator.Generate("AgencyPartsOutboundBill", agencyPartsOutboundBill.StorageCompanyCode);
            var userInfo = Utils.GetCurrentUserInfo();
            agencyPartsOutboundBill.CreatorId = userInfo.Id;
            agencyPartsOutboundBill.CreatorName = userInfo.Name;
            agencyPartsOutboundBill.CreateTime = DateTime.Now;
        }

        internal void UpdateAgencyPartsOutboundBillValidate(AgencyPartsOutboundBill agencyPartsOutboundBill) {
            var userInfo = Utils.GetCurrentUserInfo();
            agencyPartsOutboundBill.ModifierId = userInfo.Id;
            agencyPartsOutboundBill.ModifierName = userInfo.Name;
            agencyPartsOutboundBill.ModifyTime = DateTime.Now;
        }

        public void InsertAgencyPartsOutboundBill(AgencyPartsOutboundBill agencyPartsOutboundBill) {
            InsertToDatabase(agencyPartsOutboundBill);
            var partsOutboundBillDetails = ChangeSet.GetAssociatedChanges(agencyPartsOutboundBill, v => v.APartsOutboundBillDetails, ChangeOperation.Insert);
            foreach(PartsOutboundBillDetail partsOutboundBillDetail in partsOutboundBillDetails)
                InsertToDatabase(partsOutboundBillDetail);
            this.InsertAgencyPartsOutboundBillValidate(agencyPartsOutboundBill);
        }

        public void UpdateAgencyPartsOutboundBill(AgencyPartsOutboundBill agencyPartsOutboundBill) {
            agencyPartsOutboundBill.APartsOutboundBillDetails.Clear();
            UpdateToDatabase(agencyPartsOutboundBill);
            var partsOutboundBillDetails = ChangeSet.GetAssociatedChanges(agencyPartsOutboundBill, v => v.APartsOutboundBillDetails);
            foreach(PartsOutboundBillDetail partsOutboundBillDetail in partsOutboundBillDetails) {
                //获取清单实体对象的操作类型
                switch(ChangeSet.GetChangeOperation(partsOutboundBillDetail)) {
                    //新增操作
                    case ChangeOperation.Insert:
                        InsertToDatabase(partsOutboundBillDetail);
                        break;
                    //更新操作
                    case ChangeOperation.Update:
                        UpdateToDatabase(partsOutboundBillDetail);
                        break;
                    //删除操作
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(partsOutboundBillDetail);
                        break;
                }
            }
            this.UpdateAgencyPartsOutboundBillValidate(agencyPartsOutboundBill);
        }


        public IQueryable<AgencyPartsOutboundBillWithOtherInfo> 代理库仓库人员查询配件出库单(int? operatorId) {
            var userInfo = Utils.GetCurrentUserInfo();
            IQueryable<AgencyPartsOutboundBill> partsOutboundBills = ObjectContext.AgencyPartsOutboundBills.Where(v => v.StorageCompanyId == userInfo.EnterpriseId);
            if(operatorId.HasValue)
                partsOutboundBills = partsOutboundBills.Where(r => ObjectContext.WarehouseOperators.Any(v => v.OperatorId == userInfo.Id && v.WarehouseId == r.WarehouseId) || ObjectContext.Warehouses.Any(v => v.Id == r.WarehouseId && v.Type == (int)DcsWarehouseType.虚拟库));

            var SumPartsOutboundBillDetailQuery = from a in ObjectContext.APartsOutboundBillDetails
                                                  group a by a.PartsOutboundBillId
                                                      into tempTable
                                                      select new {
                                                          PartsOutboundBillId = tempTable.Key,
                                                          SumTotalAmout = tempTable.Sum(r => r.OutboundAmount * r.SettlementPrice)
                                                      };
            var query = from a in partsOutboundBills
                        join b in SumPartsOutboundBillDetailQuery on a.Id equals b.PartsOutboundBillId
                        join c in ObjectContext.AgencyPartsOutboundPlans on a.PartsOutboundPlanId equals c.Id
                        join d in ObjectContext.PartsSalesCategories on a.PartsSalesCategoryId equals d.Id
                        //join e in ObjectContext.PartsSalesOrderTypes on a.PartsSalesOrderTypeId equals e.Id
                        join f in ObjectContext.Companies.Where(r => r.Status == (int)DcsMasterDataStatus.有效) on a.CounterpartCompanyId equals f.Id into tempTable
                        from t2 in tempTable.DefaultIfEmpty()
                        join g in ObjectContext.PartsSalesOrders.Where(r => r.Status != (int)DcsMasterDataStatus.作废) on new {
                            Id = a.OriginalRequirementBillId,
                            Code = a.OriginalRequirementBillCode
                        } equals new {
                            g.Id,
                            g.Code
                        } into tempTable1
                        from t3 in tempTable1.DefaultIfEmpty()
                        select new AgencyPartsOutboundBillWithOtherInfo {
                            PartsOutboundBillId = a.Id,
                            PartsOutboundPlanId = c.Id,
                            PartsSalesCategoryId = d.Id,
                            CompanyId = t2.Id,
                            InvoiceType = t3.InvoiceType,
                            StorageCompanyId = a.StorageCompanyId,
                            TotalAmount = b.SumTotalAmout,
                            Code = a.Code,
                            WarehouseCode = a.WarehouseCode,
                            WarehouseName = a.WarehouseName,
                            WarehouseId = a.WarehouseId,
                            OutboundType = a.OutboundType,
                            OriginalRequirementBillCode = a.OriginalRequirementBillCode,
                            PartsSalesOrderTypeName = a.PartsSalesOrderTypeName,
                            PartsOutboundPlanCode = c.Code,
                            PartsSalesCategoryName = d.Name,
                            BranchId = a.BranchId,
                            BranchCode = a.BranchCode,
                            BranchName = a.BranchName,
                            CounterPartCompanyId = a.CounterpartCompanyId,
                            CounterpartCompanyCode = a.CounterpartCompanyCode,
                            CounterpartCompanyName = a.CounterpartCompanyName,
                            CompanyType = t2.Type,
                            ReceivingCompanyId = a.ReceivingCompanyId,
                            ReceivingWarehouseCode = a.ReceivingWarehouseCode,
                            ReceivingWarehouseName = a.ReceivingWarehouseName,
                            InterfaceRecordId = a.InterfaceRecordId,
                            SettlementStatus = a.SettlementStatus,
                            Remark = a.Remark,
                            OrderApproveComment = a.OrderApproveComment,
                            CreatorId = a.CreatorId,
                            CreatorName = a.CreatorName,
                            CreateTime = a.CreateTime,
                            ModifierId = a.ModifierId,
                            ERPSourceOrderCode = a.ERPSourceOrderCode,
                            ModifierName = a.ModifierName,
                            ModifyTime = a.ModifyTime,
                            ProvinceName = d.Name == "随车行" ? t3.Province : t2.ProvinceName
                        };
            return query.OrderBy(r => r.PartsOutboundBillId);
        }

        public IQueryable<PartsOutboundBill> GetPartsOutboundBillsByIds(int[] ids) {
            return ObjectContext.PartsOutboundBills.Where(t => ids.Contains(t.Id)).OrderBy(e => e.Id);
        }

    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:32
        //原分布类的函数全部转移到Ach                                                               

        public void InsertPartsOutboundBill(AgencyPartsOutboundBill partsOutboundBill) {
            new AgencyPartsOutboundBillAch(this).InsertAgencyPartsOutboundBill(partsOutboundBill);
        }

        public void UpdatePartsOutboundBill(AgencyPartsOutboundBill partsOutboundBill) {
            new AgencyPartsOutboundBillAch(this).UpdateAgencyPartsOutboundBill(partsOutboundBill);
        }

        public IQueryable<AgencyPartsOutboundBillWithOtherInfo> 代理库仓库人员查询配件出库单(int? operatorId) {
            return new AgencyPartsOutboundBillAch(this).代理库仓库人员查询配件出库单(operatorId);
        }


        //[Query(HasSideEffects = true)]
        //public IQueryable<AgencyPartsOutboundBill> 查询退货未完成销售出库单(string[] codes) {
        //    return new AgencyPartsOutboundBillAch(this).代理库查询退货未完成销售出库单(codes);
        //}

        //public IQueryable<AgencyPartsOutboundBill> 仓库负责人查询可退库配件出库单(int userId) {
        //    return new AgencyPartsOutboundBillAch(this).代理库仓库负责人查询可退库配件出库单(userId);
        //}

        //public AgencyPartsOutboundBill 查询可退库配件出库单及配件(int Id) {
        //    return new AgencyPartsOutboundBillAch(this).代理库查询可退库配件出库单及配件(Id);
        //}

        //public IQueryable<AgencyPartsOutboundBill> GetAgencyPartsOutboundBillsByIds(int[] ids) {
        //    return new AgencyPartsOutboundBillAch(this).GetAgencyPartsOutboundBillsByIds(ids);
        //}
    }
}
