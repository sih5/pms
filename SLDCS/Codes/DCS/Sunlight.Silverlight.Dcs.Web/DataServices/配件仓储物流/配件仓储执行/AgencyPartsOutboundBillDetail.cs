﻿﻿using System.Collections.Generic;
using System.Linq;
﻿using Microsoft.Data.Extensions;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class APartsOutboundBillDetailAch : DcsSerivceAchieveBase {
        public APartsOutboundBillDetailAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public IQueryable<APartsOutboundBillDetail> GetPartsOutboundBillDetailsWithPartsOutboundBill() {
            return ObjectContext.APartsOutboundBillDetails.Include("PartsOutboundBill").OrderBy(v => v.Id);
        }


        public IQueryable<APartsOutboundBillDetail> GetAgencyPartsOutboundBillDetailsWithSparePart() {
            return ObjectContext.APartsOutboundBillDetails.Include("SparePart").OrderBy(v => v.Id);
        }


    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:32
        //原分布类的函数全部转移到Ach                                                               
        public IQueryable<APartsOutboundBillDetail> GetAgencyPartsOutboundBillDetailsWithSparePart() {
            return new APartsOutboundBillDetailAch(this).GetAgencyPartsOutboundBillDetailsWithSparePart();
        }
    }
}