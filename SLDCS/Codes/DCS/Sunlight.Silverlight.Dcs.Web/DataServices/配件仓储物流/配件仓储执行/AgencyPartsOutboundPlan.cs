﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class AgencyPartsOutboundPlanAch : DcsSerivceAchieveBase {
        public AgencyPartsOutboundPlanAch(DcsDomainService domainService)
            : base(domainService) {
        }

        internal void InsertAgencyPartsOutboundPlanValidate(AgencyPartsOutboundPlan agencyPartsOutboundPlan) {
            if(string.IsNullOrWhiteSpace(agencyPartsOutboundPlan.Code) || agencyPartsOutboundPlan.Code == GlobalVar.ASSIGNED_BY_SERVER)
                agencyPartsOutboundPlan.Code = CodeGenerator.Generate("AgencyPartsOutboundPlan", agencyPartsOutboundPlan.StorageCompanyCode);
            var userInfo = Utils.GetCurrentUserInfo();
            agencyPartsOutboundPlan.CreatorId = userInfo.Id;
            agencyPartsOutboundPlan.CreatorName = userInfo.Name;
            agencyPartsOutboundPlan.CreateTime = DateTime.Now;
        }

        internal void UpdatePartsOutboundPlanValidate(AgencyPartsOutboundPlan agencyPartsOutboundPlan) {
            var userInfo = Utils.GetCurrentUserInfo();
            agencyPartsOutboundPlan.ModifierId = userInfo.Id;
            agencyPartsOutboundPlan.ModifierName = userInfo.Name;
            agencyPartsOutboundPlan.ModifyTime = DateTime.Now;
        }

        public void InsertPartsOutboundPlan(AgencyPartsOutboundPlan agencyPartsOutboundPlan) {
            InsertToDatabase(agencyPartsOutboundPlan);
            var partsOutboundPlanDetails = ChangeSet.GetAssociatedChanges(agencyPartsOutboundPlan, v => v.APartsOutboundPlanDetails, ChangeOperation.Insert);
            foreach(PartsOutboundPlanDetail partsOutboundPlanDetail in partsOutboundPlanDetails)
                InsertToDatabase(partsOutboundPlanDetail);
            this.InsertAgencyPartsOutboundPlanValidate(agencyPartsOutboundPlan);
        }

        public void UpdatePartsOutboundPlan(AgencyPartsOutboundPlan agencyPartsOutboundPlan) {
            agencyPartsOutboundPlan.APartsOutboundPlanDetails.Clear();
            UpdateToDatabase(agencyPartsOutboundPlan);
            var partsOutboundPlanDetails = ChangeSet.GetAssociatedChanges(agencyPartsOutboundPlan, v => v.APartsOutboundPlanDetails);
            foreach(PartsOutboundPlanDetail partsOutboundPlanDetail in partsOutboundPlanDetails) {
                //获取清单实体对象的操作类型
                switch(ChangeSet.GetChangeOperation(partsOutboundPlanDetail)) {
                    //新增操作
                    case ChangeOperation.Insert:
                        InsertToDatabase(partsOutboundPlanDetail);
                        break;
                    //更新操作
                    case ChangeOperation.Update:
                        UpdateToDatabase(partsOutboundPlanDetail);
                        break;
                    //删除操作
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(partsOutboundPlanDetail);
                        break;
                }
            }
            this.UpdatePartsOutboundPlanValidate(agencyPartsOutboundPlan);
        }

        public AgencyPartsOutboundPlan GetAgencyPartsOutboundPlanWithDetails(int id) {
            var agencyPartsOutboundPlan = ObjectContext.AgencyPartsOutboundPlans.Include("PartsSalesCategory").SingleOrDefault(r => r.Id == id);
            if(agencyPartsOutboundPlan != null) {
                var partsOutboundPlanDetails = ObjectContext.APartsOutboundPlanDetails.Where(r => r.PartsOutboundPlanId == agencyPartsOutboundPlan.Id && (r.OutboundFulfillment == null || r.PlannedAmount != r.OutboundFulfillment)).ToArray();
            }
            return agencyPartsOutboundPlan;
        }

        public IQueryable<AgencyPartsOutboundPlan> GetAgencyPartsOutboundPlansWithPartsCategory() {
            var userInfo = Utils.GetCurrentUserInfo();
            IQueryable<AgencyPartsOutboundPlan> partsOutboundPlans = ObjectContext.AgencyPartsOutboundPlans;
            if(!ObjectContext.Companies.Any(r => r.Id == userInfo.EnterpriseId && r.Type == (int)DcsCompanyType.分公司)) {
                partsOutboundPlans = partsOutboundPlans.Where(r => r.StorageCompanyId == userInfo.EnterpriseId && (ObjectContext.WarehouseOperators.Any(v => v.OperatorId == userInfo.Id && v.WarehouseId == r.WarehouseId) || ObjectContext.Warehouses.Any(v => v.Id == r.WarehouseId && v.Type == (int)DcsWarehouseType.虚拟库)));
            }
            return partsOutboundPlans.Include("APartsOutboundPlanDetails").Include("PartsSalesCategory").OrderBy(e => e.Id);
        }

        public IQueryable<AgencyPartsOutboundPlan> 代理库保管区负责人查询配件出库计划(int? userinfoId) {
            var userInfo = Utils.GetCurrentUserInfo();
            var warehouseAreas = ObjectContext.WarehouseAreas.Where(r => r.AreaKind == (int)DcsAreaKind.库位 && r.Status == (int)DcsBaseDataStatus.有效 && ObjectContext.WarehouseAreaCategories.Any(v => v.Category == (int)DcsAreaType.保管区 && v.Id == r.AreaCategoryId));
            if(userinfoId.HasValue)
                warehouseAreas = warehouseAreas.Where(r => ObjectContext.WarehouseAreaManagers.Any(v => v.ManagerId == userInfo.Id && v.WarehouseAreaId == r.TopLevelWarehouseAreaId));
            //
            //var warehouseToDisplay = new[] { "海外中重卡北京CDC仓库", "海外轻卡北京CDC仓库", "海外乘用车北京CDC仓库" };

            //var partsStock =
            var agencyPartsOutboundPlan = ObjectContext.AgencyPartsOutboundPlans.Where(r => (r.Status == (int)DcsPartsOutboundPlanStatus.新建 || r.Status == (int)DcsPartsOutboundPlanStatus.部分出库) && warehouseAreas.Any(v => r.WarehouseId == v.WarehouseId) && !(ObjectContext.Warehouses.Any(o => o.Id == r.WarehouseId && o.WmsInterface) && (r.OutboundType == (int)DcsPartsOutboundType.采购退货 || r.OutboundType == (int)DcsPartsOutboundType.内部领出 || (r.OutboundType == (int)DcsPartsOutboundType.配件销售) || (r.OutboundType == (int)DcsPartsOutboundType.配件调拨 && (from a in ObjectContext.PartsTransferOrders
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    join b in ObjectContext.Warehouses on a.OriginalWarehouseId equals b.Id
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    join c in ObjectContext.Warehouses on a.DestWarehouseId equals c.Id
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    where (b.StorageCenter != c.StorageCenter || !c.WmsInterface) && a.Id == r.OriginalRequirementBillId
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    select 1).Any()))));
            var agencyPartsOutboundPlans = ObjectContext.AgencyPartsOutboundPlans.Where(r => ObjectContext.WarehouseAreaManagers.Any(m => m.ManagerId == userInfo.Id && ObjectContext.WarehouseAreas.Where(c => ObjectContext.PartsStocks.Any(x => r.WarehouseId == x.WarehouseId && c.Id == x.WarehouseAreaId && this.ObjectContext.APartsOutboundPlanDetails.Any(y => (y.PlannedAmount - (y.OutboundFulfillment ?? 0) > 0) && y.PartsOutboundPlanId == r.Id && x.PartId == y.SparePartId)) && this.ObjectContext.WarehouseAreaCategories.Any(d => d.Id == c.AreaCategoryId && d.Category == (int)DcsAreaType.保管区) && c.TopLevelWarehouseAreaId != null).Select(e => e.TopLevelWarehouseAreaId).Contains(m.WarehouseAreaId)));

            return agencyPartsOutboundPlan.Include("APartsOutboundPlanDetails").Include("PartsSalesCategory").Where(r => agencyPartsOutboundPlans.Any(x => x.Id == r.Id)).OrderBy(r => r.Id);
        }

        public IQueryable<AgencyPartsOutboundPlan> 代理库仓库人员查询待发运出库计划(int? operatorId) {
            var query1 = from tmp1 in
                             (from a in ObjectContext.AgencyPartsOutboundBills
                              from b in ObjectContext.APartsOutboundBillDetails
                              where a.Id == b.PartsOutboundBillId
                              select new {
                                  PartsOutboundBillId = a.Id,
                                  b.SparePartId,
                                  b.OutboundAmount
                              })
                         group tmp1 by new {
                             tmp1.PartsOutboundBillId,
                             tmp1.SparePartId
                         }
                             into tempTable1
                             select new {
                                 tempTable1.Key.PartsOutboundBillId,
                                 tempTable1.Key.SparePartId,
                                 totalOutAmount = tempTable1.Sum(r => r.OutboundAmount)
                             };
            var query2 = from tmp2 in
                             (from a in ObjectContext.PartsShippingOrders
                              from b in ObjectContext.PartsShippingOrderDetails
                              from c in ObjectContext.PartsShippingOrderRefs
                              where a.Id == b.PartsShippingOrderId && a.Id == c.PartsShippingOrderId && a.Status != (int)DcsPartsShippingOrderStatus.作废
                              select new {
                                  c.PartsOutboundBillId,
                                  b.SparePartId,
                                  b.ShippingAmount
                              })
                         group tmp2 by new {
                             tmp2.PartsOutboundBillId,
                             tmp2.SparePartId
                         }
                             into tempTable2
                             select new {
                                 tempTable2.Key.PartsOutboundBillId,
                                 tempTable2.Key.SparePartId,
                                 totalShippingAmount = tempTable2.Sum(r => r.ShippingAmount)
                             };
            var partsOutboundBillIdQuery = from t in
                                               (from a in query1
                                                join b in query2 on new {
                                                    a.SparePartId,
                                                    a.PartsOutboundBillId
                                                } equals new {
                                                    b.SparePartId,
                                                    b.PartsOutboundBillId
                                                } into tempStruct
                                                from k in tempStruct.DefaultIfEmpty()
                                                select new {
                                                    a.PartsOutboundBillId,
                                                    diffAmount = a.totalOutAmount - (!tempStruct.Any() ? 0 : k.totalShippingAmount)
                                                })
                                           where t.diffAmount > 0
                                           select t;
            int[] outboundtypes = new int[]{
                (int)DcsPartsOutboundType.配件销售,
                (int)DcsPartsOutboundType.配件调拨,
                (int)DcsPartsOutboundType.积压件调剂,
                (int)DcsPartsOutboundType.质量件索赔
            };
            var partsOutboundPlanQuery = from a in ObjectContext.AgencyPartsOutboundPlans.Where(p => outboundtypes.Contains(p.OutboundType) || (p.OutboundType == (int)DcsPartsOutboundType.采购退货 && p.StorageCompanyType != (int)DcsCompanyType.分公司))
                                         from b in ObjectContext.AgencyPartsOutboundBills.Where(r => partsOutboundBillIdQuery.Any(v => r.Id == v.PartsOutboundBillId))
                                         where a.Id == b.PartsOutboundPlanId
                                         select a;
            var result = partsOutboundPlanQuery; //.Where(r => ObjectContext.PartsLogisticBatches.Any(v => r.Id == v.SourceId && v.SourceType == (int)DcsPartsLogisticBatchSourceType.配件出库计划 && v.ShippingStatus == (int)DcsPartsLogisticBatchShippingStatus.待运));
            if(operatorId.HasValue) {
                result = result.Where(r => ObjectContext.WarehouseOperators.Any(v => v.OperatorId == operatorId.Value && r.WarehouseId == v.WarehouseId));
            }
            result = result.Include("PartsOutboundPlanDetails");
            return result.OrderBy(r => r.Id);
        }

        public IQueryable<AgencyPartsOutboundPlan> 代理库仓库人员查询配件出库计划() {
            var userinfo = Utils.GetCurrentUserInfo();
            var result = ObjectContext.AgencyPartsOutboundPlans.Where(r => ObjectContext.WarehouseOperators.Any(v => v.WarehouseId == r.WarehouseId && v.OperatorId == userinfo.Id)).Include("PartsOutboundPlanDetails");
            return result.OrderBy(r => r.Id);
        }

        public IQueryable<AgencyPartsOutboundPlan> GetAgencyPartsOutboundById(int id) {
            return ObjectContext.AgencyPartsOutboundPlans.Where(r => r.Id == id);
        }

        public IEnumerable<APartsOutboundPlanDetail> GetAgencyPartsOutboundPlanDetailsByWarehouseAreaManager(int orderId, int userId) {
            var partsStocks = from a in ObjectContext.AgencyPartsOutboundPlans.Where(r => r.Id == orderId)
                              join b in ObjectContext.PartsStocks on a.WarehouseId equals b.WarehouseId
                              join c in ObjectContext.WarehouseAreaCategories.Where(r => r.Category == (int)DcsAreaType.保管区) on b.WarehouseAreaCategoryId equals c.Id
                              join d in ObjectContext.WarehouseAreas on new {
                                  WarehouseAreaId = b.WarehouseAreaId,
                                  WarehouseId = b.WarehouseId
                              } equals new {
                                  WarehouseAreaId = d.Id,
                                  WarehouseId = d.WarehouseId
                              }
                              join e in ObjectContext.WarehouseAreaManagers.Where(r => r.ManagerId == userId) on d.TopLevelWarehouseAreaId equals e.WarehouseAreaId
                              select b;
            var detailIds = ObjectContext.APartsOutboundPlanDetails.Where(r => r.PartsOutboundPlanId == orderId && (!r.OutboundFulfillment.HasValue || r.PlannedAmount != r.OutboundFulfillment.Value) && partsStocks.Any(v => v.PartId == r.SparePartId)).Select(r => r.Id).ToArray();
            return 代理库校验出库计划清单(detailIds, userId);
        }

        public IEnumerable<APartsOutboundPlanDetail> 代理库校验出库计划清单(int[] partsOutboundPlanDetailIds, int personId) {
            var result = from a in this.ObjectContext.APartsOutboundPlanDetails.Where(r => partsOutboundPlanDetailIds.Contains(r.Id) && r.PlannedAmount > (r.OutboundFulfillment ?? 0))
                         join b in this.ObjectContext.AgencyPartsOutboundPlans on a.PartsOutboundPlanId equals b.Id
                         join c in ObjectContext.PartsStocks on new {
                             PartId = a.SparePartId,
                             b.WarehouseId
                         } equals new {
                             c.PartId,
                             c.WarehouseId
                         }
                         join d in ObjectContext.WarehouseAreas.Where(r => this.ObjectContext.WarehouseAreaCategories.Any(d => d.Id == r.AreaCategoryId && d.Category == (int)DcsAreaType.保管区)) on c.WarehouseAreaId equals d.Id
                         join f in ObjectContext.WarehouseAreaManagers.Where(r => r.ManagerId == personId) on d.TopLevelWarehouseAreaId equals f.WarehouseAreaId
                         select a;
            return result.ToArray().Distinct();
        }

    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:32
        //原分布类的函数全部转移到Ach                                                               

        public void InsertAgencyPartsOutboundPlan(AgencyPartsOutboundPlan agencypartsOutboundPlan) {
            new AgencyPartsOutboundPlanAch(this).InsertPartsOutboundPlan(agencypartsOutboundPlan);
        }

        public void UpdateAgencyPartsOutboundPlan(AgencyPartsOutboundPlan agencypartsOutboundPlan) {
            new AgencyPartsOutboundPlanAch(this).UpdatePartsOutboundPlan(agencypartsOutboundPlan);
        }

        public AgencyPartsOutboundPlan GetAgencyPartsOutboundPlanWithDetails(int id) {
            return new AgencyPartsOutboundPlanAch(this).GetAgencyPartsOutboundPlanWithDetails(id);
        }

        public IQueryable<AgencyPartsOutboundPlan> GetAgencyPartsOutboundPlansWithPartsCategory() {
            return new AgencyPartsOutboundPlanAch(this).GetAgencyPartsOutboundPlansWithPartsCategory();
        }

        public IQueryable<AgencyPartsOutboundPlan> 代理库保管区负责人查询配件出库计划(int? userinfoId) {
            return new AgencyPartsOutboundPlanAch(this).代理库保管区负责人查询配件出库计划(userinfoId);
        }

        public IQueryable<AgencyPartsOutboundPlan> 代理库仓库人员查询待发运出库计划(int? operatorId) {
            return new AgencyPartsOutboundPlanAch(this).代理库仓库人员查询待发运出库计划(operatorId);
        }

        public IQueryable<AgencyPartsOutboundPlan> 代理库仓库人员查询配件出库计划() {
            return new AgencyPartsOutboundPlanAch(this).代理库仓库人员查询配件出库计划();
        }

        public IQueryable<AgencyPartsOutboundPlan> GetAgencyPartsOutboundById(int id) {
            return new AgencyPartsOutboundPlanAch(this).GetAgencyPartsOutboundById(id);
        }

        public IEnumerable<APartsOutboundPlanDetail> GetAgencyPartsOutboundPlanDetailsByWarehouseAreaManager(int orderId, int userId) {
            return new AgencyPartsOutboundPlanAch(this).GetAgencyPartsOutboundPlanDetailsByWarehouseAreaManager(orderId, userId);
        }

        public IEnumerable<APartsOutboundPlanDetail> 代理库校验出库计划清单(int[] partsOutboundPlanDetailIds, int personId) {
            return new AgencyPartsOutboundPlanAch(this).代理库校验出库计划清单(partsOutboundPlanDetailIds, personId);
        }
    }
}
