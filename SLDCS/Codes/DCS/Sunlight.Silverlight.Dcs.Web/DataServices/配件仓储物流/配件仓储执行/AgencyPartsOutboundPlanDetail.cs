﻿using System.Linq;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class APartsOutboundPlanDetailAch : DcsSerivceAchieveBase {
        public APartsOutboundPlanDetailAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public IQueryable<APartsOutboundPlanDetail> GetAgencyPartsOutboundPlanDetailsWithSparePart() {
            return ObjectContext.APartsOutboundPlanDetails.Include("SparePart").OrderBy(r => r.Id);
        }

    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:30
        //原分布类的函数全部转移到Ach                                                               

        public IQueryable<APartsOutboundPlanDetail> GetAgencyPartsOutboundPlanDetailsWithSparePart() {
            return new APartsOutboundPlanDetailAch(this).GetAgencyPartsOutboundPlanDetailsWithSparePart();
        }
    }
}
