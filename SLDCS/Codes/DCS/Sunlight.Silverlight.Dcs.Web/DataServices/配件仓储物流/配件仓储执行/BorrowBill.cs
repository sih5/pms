﻿using System;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class BorrowBillAch : DcsSerivceAchieveBase {
        public BorrowBillAch(DcsDomainService domainService)
            : base(domainService) {
        }

        internal void InsertBorrowBillValidate(BorrowBill borrowBill) {
            var userInfo = Utils.GetCurrentUserInfo();
            if(string.IsNullOrWhiteSpace(borrowBill.Code) || borrowBill.Code == GlobalVar.ASSIGNED_BY_SERVER) {
                borrowBill.Code = CodeGenerator.Generate("BorrowBill", borrowBill.WarehouseCode);
            }
            //清单不用校验，唯一性校验
            borrowBill.CreatorId = userInfo.Id;
            borrowBill.CreatorName = userInfo.Name;
            borrowBill.CreateTime = DateTime.Now;
        }

        internal void UpdateBorrowBillValidate(BorrowBill borrowBill) {
            var userInfo = Utils.GetCurrentUserInfo();
            borrowBill.ModifierId = userInfo.Id;
            borrowBill.ModifierName = userInfo.Name;
            borrowBill.ModifyTime = DateTime.Now;
        }

        public void InsertBorrowBill(BorrowBill borrowBill) {
            //在ObjectContext中登记，指明主单将要新增至数据库
            InsertToDatabase(borrowBill);
            //获取主单所关联的清单实体对象，并指明将清单新增至数据库
            var borrowBillDetails = ChangeSet.GetAssociatedChanges(borrowBill, v => v.BorrowBillDetails, ChangeOperation.Insert);
            foreach(BorrowBillDetail borrowBillDetail in borrowBillDetails) {
                InsertToDatabase(borrowBillDetail);
            }
            this.InsertBorrowBillValidate(borrowBill);

        }

        public void UpdateBorrowBill(BorrowBill borrowBill) {
            borrowBill.BorrowBillDetails.Clear();
            UpdateToDatabase(borrowBill);
            var borrowBillDetails = ChangeSet.GetAssociatedChanges(borrowBill, v => v.BorrowBillDetails);
            foreach(BorrowBillDetail borrowBillDetail in borrowBillDetails) {
                switch(ChangeSet.GetChangeOperation(borrowBillDetail)) {
                    case ChangeOperation.Insert:
                        InsertToDatabase(borrowBillDetail);
                        break;
                    case ChangeOperation.Update:
                        UpdateToDatabase(borrowBillDetail);
                        break;
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(borrowBillDetail);
                        break;
                }
            }
            this.UpdateBorrowBillValidate(borrowBill);
        }

        public IQueryable<BorrowBill> GetBorrowBillForDetails() {
            var result = this.ObjectContext.BorrowBills.Include("BorrowBillDetails").OrderBy(r => r.Id);
            foreach (var item in result) {
                item.IsUploadFile = item.Path != null && item.Path.Length > 0;
            }
            return this.ObjectContext.BorrowBills.Include("BorrowBillDetails").OrderBy(r => r.Id);
        }
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:32
        //原分布类的函数全部转移到Ach                                                               

        public void InsertBorrowBill(BorrowBill borrowBill) {
            new BorrowBillAch(this).InsertBorrowBill(borrowBill);
        }

        public void UpdateBorrowBill(BorrowBill borrowBill) {
            new BorrowBillAch(this).UpdateBorrowBill(borrowBill);
        }

        public IQueryable<BorrowBill> GetBorrowBillForDetails() {
            return new BorrowBillAch(this).GetBorrowBillForDetails();
        }
    }
}
