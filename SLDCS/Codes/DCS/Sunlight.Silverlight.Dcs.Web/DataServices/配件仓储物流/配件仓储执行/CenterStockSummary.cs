﻿using System;
using Sunlight.Silverlight.Web;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class CenterStockSummaryAch : DcsSerivceAchieveBase {
        public CenterStockSummaryAch(DcsDomainService domainService)
            : base(domainService) {
        }
        public IEnumerable<CenterStockSummary> 中心库库存汇总(string marketingDepartmentNames, string centerNames, DateTime? bCreateTime, DateTime? eCreateTime, string dealerNames, string newType) {
            //--1:只输入结转时间，显示中心库，服务站两条汇总数据,属性为空
            //--2:只输入结转时间，分销中心
            //--3: 只输入结转时间，中心库，	所选中心库每月显示一条配件库存品种汇总的数据（分销中心、属性分类字段为空）；	隶属于该中心库下所有服务站每家每月各显示一条汇总数据（属性分类字段为空）；
            //--4 输入服务站，结转时间 
            //--5：当查询条件输入结转时间、属性分类
            //--6:	当查询条件输入结转时间、分销中心、属性分类
            //--7:	当查询条件输入结转时间、中心库、属性分类
            //--8:	当查询条件输入结转时间、服务站、属性分类：  
            var selectType = 0;
            var stockType = 0;
            if(String.IsNullOrEmpty(marketingDepartmentNames) && String.IsNullOrEmpty(centerNames) && String.IsNullOrEmpty(dealerNames) && String.IsNullOrEmpty(newType)) {
                selectType = 1;
            } else if(!String.IsNullOrEmpty(marketingDepartmentNames) && String.IsNullOrEmpty(centerNames) && String.IsNullOrEmpty(dealerNames) && String.IsNullOrEmpty(newType)) {
                selectType = 2;
            } else if( !String.IsNullOrEmpty(centerNames) && String.IsNullOrEmpty(dealerNames) && String.IsNullOrEmpty(newType)) {
                selectType = 3;
            } else if(!String.IsNullOrEmpty(dealerNames) && String.IsNullOrEmpty(newType)) {
                selectType = 4;
                stockType = 2;
            } else if(String.IsNullOrEmpty(marketingDepartmentNames) && String.IsNullOrEmpty(centerNames) && String.IsNullOrEmpty(dealerNames) && !String.IsNullOrEmpty(newType)) {
                selectType = 5;
            } else if(!String.IsNullOrEmpty(marketingDepartmentNames) && String.IsNullOrEmpty(centerNames) && String.IsNullOrEmpty(dealerNames) && !String.IsNullOrEmpty(newType)) {
                selectType = 6;
                stockType = 2;
            } else if( !String.IsNullOrEmpty(centerNames) && String.IsNullOrEmpty(dealerNames) && !String.IsNullOrEmpty(newType)) {
                selectType = 7;
                stockType = 1;
            } else if(!String.IsNullOrEmpty(dealerNames) && !String.IsNullOrEmpty(newType)) {
                selectType = 8;
                stockType = 2;
            }
            string SQL = @"select rownum as id , ss.* from (select cs.times as YearMonth,
                           sum(cs.entries) as entries,
                           sum(cs.actualstock) as actualstock,
                           sum(cs.salestotal) as salestotal,
                           (case
                             when {0} = 1 or {0} = 2  or {0}=3 or {0}=4then
                              null
                             else
                              cs.newtype
                           end) as newtype,
                           (case
                             when {0} = 1 or {0} =3   or {0}=7  then
                              cast('' as varchar2(50))
                             else
                              md.name
                           end) as MarketingDepartmentName,
                           (case
                             when {0}= 1 and cs.type = 1 then
                              cast('中心库' as varchar2(50))
                             when (({0} = 1 or {0} = 2   or {0}=5 ) and cs.type = 2) or {0}=6  then
                              cast('' as varchar2(50))
                             when ( ( {0} = 2    or {0}=5 or {0}=7 ) and cs.type = 1 )or {0}=8 or {0}=4 or {0}=3  then
                              ag.name      
                           end) as centerName,
                           (case
                             when {0} = 1 and cs.type = 2 then
                              cast('服务站' as varchar2(50))
                             when ({0} = 1 or {0} = 2 or {0}=3 or {0}=4 or {0}=5 or  {0}=7) and cs.type = 1 then
                              cast('' as varchar2(50))
                             when ( {0} = 2 or {0}=3 or {0}=4 or {0}=5 or {0}=6 or {0}=8  ) and cs.type = 2 then
                              dl.name   
                              else null    
                           end) as dealerName
                      from CenterStockSummary cs
                     left join MarketingDepartment md
                        on cs.mdid = md.id
                      join agency ag
                        on cs.centerid = ag.id
                      left join dealer dl
                        on cs.dealerid = dl.id
                       where 1=1 {1}
                     group by  (case
                             when {0} = 1 or {0} = 2  or {0}=3 or {0}=4then
                              null
                             else
                              cs.newtype
                           end) ,
                           (case
                             when {0} = 1 or {0} =3   or {0}=7  then
                              cast('' as varchar2(50))
                             else
                              md.name
                           end) ,
                           (case
                             when {0}= 1 and cs.type = 1 then
                              cast('中心库' as varchar2(50))
                             when (({0} = 1 or {0} = 2  or {0}=5 ) and cs.type = 2) or {0}=6  then
                              cast('' as varchar2(50))
                             when( ( {0} = 2  or {0}=5 or {0}=7 ) and cs.type = 1 )or {0}=8 or {0}=4 or {0}=3  then
                              ag.name      
                           end) ,
                           (case
                             when {0} = 1 and cs.type = 2 then
                              cast('服务站' as varchar2(50))
                             when ({0} = 1 or {0} = 2 or {0}=3 or {0}=4 or {0}=5 or  {0}=7) and cs.type = 1 then
                              cast('' as varchar2(50))
                             when ( {0} = 2 or {0}=3 or {0}=4 or {0}=5 or {0}=6 or {0}=8  ) and cs.type = 2 then
                              dl.name   
                              else null    
                           end)  ,
                              cs.times)ss";
            var serachSQL = new StringBuilder();
            if(!string.IsNullOrEmpty(marketingDepartmentNames)) {
                var mdn = marketingDepartmentNames.Split(',');
                if(mdn.Length == 1) {
                    var mdName = mdn[0];
                    serachSQL.Append(" and md.name = '" + mdName + "'");
                } else {
                    for(int i = 0; i < mdn.Length; i++) {
                        mdn[i] = "'" + mdn[i] + "'";
                    }
                    string codes = string.Join(",", mdn);
                    serachSQL.Append(" and md.name in (" + codes + ")");
                }
            }
            if(!string.IsNullOrEmpty(centerNames)) {
                if(selectType == 3) {
                    var mdn = centerNames.Split(',');
                    if(mdn.Length == 1) {
                        var mdName = mdn[0];
                        serachSQL.Append(" and ((ag.name = '" + mdName + "' and  exists(select 1 from AgencyDealerRelation  ad where ad.agencyid =cs.centerid and cs.type=2 and ad.status=1)) or ag.name = '" + mdName + "')");
                    } else {
                        for(int i = 0; i < mdn.Length; i++) {
                            mdn[i] = "'" + mdn[i] + "'";
                        }
                        string codes = string.Join(",", mdn);
                        serachSQL.Append(" and (( ag.name in (" + codes + ") and  exists(select 1 from AgencyDealerRelation  ad where ad.agencyid =cs.centerid and cs.type=2 and ad.status=1)) or ag.name in (" + codes + "))");
                    }
                } else {
                    var mdn = centerNames.Split(',');
                    if(mdn.Length == 1) {
                        var mdName = mdn[0];
                        serachSQL.Append(" and ag.name = '" + mdName + "'");
                    } else {
                        for(int i = 0; i < mdn.Length; i++) {
                            mdn[i] = "'" + mdn[i] + "'";
                        }
                        string codes = string.Join(",", mdn);
                        serachSQL.Append(" and ag.name in (" + codes + ")");
                    }
                }
            }
            if(!string.IsNullOrEmpty(dealerNames)) {
                var mdn = dealerNames.Split(',');
                if(mdn.Length == 1) {
                    var mdName = mdn[0];
                    serachSQL.Append(" and dl.name = '" + mdName + "'");
                } else {
                    for(int i = 0; i < mdn.Length; i++) {
                        mdn[i] = "'" + mdn[i] + "'";
                    }
                    string codes = string.Join(",", mdn);
                    serachSQL.Append(" and dl.name in (" + codes + ")");
                }
            }
            if(!String.IsNullOrEmpty(newType)) {
                serachSQL.Append(" and cs.newtype in(" + newType+")");
            }
            if(bCreateTime.HasValue) {
                serachSQL.Append(" and cs.times >= to_char(to_date('" + bCreateTime + "','yyyy-MM-dd HH24:mi:ss'),'yyyy-mm')");
            }
            if(eCreateTime.HasValue) {
                serachSQL.Append(" and cs.times <=  to_char(to_date('" + eCreateTime + "','yyyy-MM-dd HH24:mi:ss'),'yyyy-mm')");
            }
            if(stockType!=0){
                serachSQL.Append(" and cs.type ="+stockType);
            }
            var userInfo = Utils.GetCurrentUserInfo();
            var company = ObjectContext.Companies.Where(r => r.Id == userInfo.EnterpriseId).FirstOrDefault();
            //登录人为配件公司的，判断如果维护了人员与分销中心关系的，只能查询分销中心内的中心库订单。
            //登录人为中心库的只能查询自己辖区内服务站数据，登录人为服务站或者大型服务站的，只能查询自己提报的销售订单
            if(company.Type == (int)DcsCompanyType.分公司) {
                //判断是否维护了人员与分销中心关系
                var isHas = ObjectContext.MarketDptPersonnelRelations.Where(r => r.PersonnelId == userInfo.Id && r.Status == (int)DcsBaseDataStatus.有效).Count();
                if(isHas > 0) {
                    serachSQL.Append(" and  exists(select 1 from  MarketDptPersonnelRelation mr join Agency ags on mr.marketdepartmentid = ags.marketingdepartmentid  where mr.PersonnelId = " + userInfo.Id + " and ags.id= cs.centerid and mr.status=1)");
                }
            }
            if(company.Type == (int)DcsCompanyType.代理库) {
                serachSQL.Append(" and (cs.centerid=" + userInfo.EnterpriseId + " or exists(select 1 from AgencyDealerRelation  ad where ad.agencyid =cs.centerid and cs.type=2 and ad.status=1) )");
            }
            if(company.Type == (int)DcsCompanyType.服务站 || company.Type == (int)DcsCompanyType.服务站兼代理库) {
                serachSQL.Append(" and cs.dealerid=" + userInfo.EnterpriseId);
            }
            SQL = string.Format(SQL, selectType, serachSQL);
            var search = ObjectContext.ExecuteStoreQuery<CenterStockSummary>(SQL).ToList();
            return search;
        }
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:32
        //原分布类的函数全部转移到Ach                                                               
        public IEnumerable<CenterStockSummary> 中心库库存汇总(string marketingDepartmentNames, string centerNames, DateTime? bCreateTime, DateTime? eCreateTime, string dealerNames, string newType) {
            return new CenterStockSummaryAch(this).中心库库存汇总(marketingDepartmentNames, centerNames, bCreateTime, eCreateTime, dealerNames, newType);
        }
    }
}

