﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class CompanyTransferOrderAch : DcsSerivceAchieveBase {
        internal void InsertCompanyTransferOrderValidate(CompanyTransferOrder companyTransferOrder) {
            var userInfo = Utils.GetCurrentUserInfo();
            if(string.IsNullOrWhiteSpace(companyTransferOrder.Code) || companyTransferOrder.Code == GlobalVar.ASSIGNED_BY_SERVER) {
                companyTransferOrder.Code = CodeGenerator.Generate("CompanyTransferOrder", companyTransferOrder.DestCompanyCode);
            }
            //清单不用校验，唯一性校验
            companyTransferOrder.CreatorId = userInfo.Id;
            companyTransferOrder.CreatorName = userInfo.Name;
            companyTransferOrder.CreateTime = DateTime.Now;
        }
        internal void UpdateCompanyTransferOrderValidate(CompanyTransferOrder companyTransferOrder) {
            var dbCompanyTransferOrder = ObjectContext.CompanyTransferOrders.Where(r => r.Id == companyTransferOrder.Id && r.Status != (int)DcsPartsTransferOrderStatus.作废).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbCompanyTransferOrder == null) {
                throw new ValidationException(ErrorStrings.CompanyTransferOrder_Validation1);
            }
            if(dbCompanyTransferOrder.Status != (int)DcsPartsTransferOrderStatus.新建) {
                throw new ValidationException(ErrorStrings.CompanyTransferOrder_Validation2);
            }
            var userInfo = Utils.GetCurrentUserInfo();
            companyTransferOrder.ModifierId = userInfo.Id;
            companyTransferOrder.ModifierName = userInfo.Name;
            companyTransferOrder.ModifyTime = DateTime.Now;
        }
        public void InsertCompanyTransferOrder(CompanyTransferOrder companyTransferOrder) {
            //在ObjectContext中登记，指明主单将要新增至数据库    
            InsertToDatabase(companyTransferOrder);
            //获取主单所关联的清单实体对象，并指明将清单新增至数据库
            var companyTransferOrderDetails = ChangeSet.GetAssociatedChanges(companyTransferOrder, v => v.CompanyTransferOrderDetails, ChangeOperation.Insert);
            foreach(CompanyTransferOrderDetail companyTransferOrderDetail in companyTransferOrderDetails) {
                InsertToDatabase(companyTransferOrderDetail);
            }
            this.InsertCompanyTransferOrderValidate(companyTransferOrder);
        }
        public void UpdateCompanyTransferOrder(CompanyTransferOrder companyTransferOrder) {
            companyTransferOrder.CompanyTransferOrderDetails.Clear();
            UpdateToDatabase(companyTransferOrder);
            var companyTransferOrderDetails = ChangeSet.GetAssociatedChanges(companyTransferOrder, v => v.CompanyTransferOrderDetails);
            foreach(CompanyTransferOrderDetail companyTransferOrderDetail in companyTransferOrderDetails) {
                switch(ChangeSet.GetChangeOperation(companyTransferOrderDetail)) {
                    case ChangeOperation.Insert:
                        InsertToDatabase(companyTransferOrderDetail);
                        break;
                    case ChangeOperation.Update:
                        UpdateToDatabase(companyTransferOrderDetail);
                        break;
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(companyTransferOrderDetail);
                        break;
                }
            }
            this.UpdateCompanyTransferOrderValidate(companyTransferOrder);
        }
        public IQueryable<CompanyTransferOrder> 查询企业间配件调拨单() {
            return ObjectContext.CompanyTransferOrders.Include("CompanyTransferOrderDetail").OrderBy(r => r.Id);
        }
        public CompanyTransferOrder GetCompanyTransferOrderWithDetail(int Id) {
            var companyTransferOrder = ObjectContext.CompanyTransferOrders.SingleOrDefault(e => e.Id == Id);
            if(companyTransferOrder != null) {
                var detail = ObjectContext.CompanyTransferOrderDetails.Where(e => e.PartsTransferOrderId == companyTransferOrder.Id).ToArray();
                var partsSalesCategory = ObjectContext.PartsSalesCategories.SingleOrDefault(e => e.Id == companyTransferOrder.PartsSalesCategoryId);
            }
            return companyTransferOrder;
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:32
        //原分布类的函数全部转移到Ach                                                               

        public void InsertCompanyTransferOrder(CompanyTransferOrder companyTransferOrder) {
            new CompanyTransferOrderAch(this).InsertCompanyTransferOrder(companyTransferOrder);
        }

        public void UpdateCompanyTransferOrder(CompanyTransferOrder companyTransferOrder) {
            new CompanyTransferOrderAch(this).UpdateCompanyTransferOrder(companyTransferOrder);
        }
                public IQueryable<CompanyTransferOrder> 查询企业间配件调拨单() {
            return new CompanyTransferOrderAch(this).查询企业间配件调拨单();
        }

        public CompanyTransferOrder GetCompanyTransferOrderWithDetail(int Id) {
            return new CompanyTransferOrderAch(this).GetCompanyTransferOrderWithDetail(Id);
        }
    }
}
