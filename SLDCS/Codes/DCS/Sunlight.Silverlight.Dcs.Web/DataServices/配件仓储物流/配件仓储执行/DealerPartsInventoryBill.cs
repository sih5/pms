﻿using Sunlight.Silverlight.Web;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Microsoft.Data.Extensions;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        private void InsertDealerPartsInventoryBillValidate(DealerPartsInventoryBill dealerPartsInventoryBill) {
            var partsIds = dealerPartsInventoryBill.DealerPartsInventoryDetails.Select(r => r.SparePartId).ToArray();
            var partinfos = ObjectContext.SpareParts.Count(r => partsIds.Contains(r.Id));
            if(partinfos < partsIds.Length)
                throw new ValidationException(ErrorStrings.DealerPartsInventoryBill_Validation1);
            if(string.IsNullOrWhiteSpace(dealerPartsInventoryBill.Code) || dealerPartsInventoryBill.Code == GlobalVar.ASSIGNED_BY_SERVER)
                dealerPartsInventoryBill.Code = CodeGenerator.Generate("DealerPartsInventoryBill", dealerPartsInventoryBill.StorageCompanyCode);
            var dealerPartsInventoryBills = ObjectContext.DealerPartsInventoryBills.Where(r => r.StorageCompanyId == dealerPartsInventoryBill.StorageCompanyId && r.Status == (int)DcsDealerPartsInventoryBillStatus.新建 && r.DealerPartsInventoryDetails.Any(v => partsIds.Contains(v.SparePartId)));
            if(校验盘点单重复盘点(partsIds, dealerPartsInventoryBill.StorageCompanyId, dealerPartsInventoryBill.SalesCategoryId)) {
                throw new ValidationException(ErrorStrings.PartsInventoryBill_Validation1);
            }
            var agency = ObjectContext.AgencyDealerRelations.Include("Agency").Where(t => t.Status == (int)DcsBaseDataStatus.有效 && t.DealerId == dealerPartsInventoryBill.StorageCompanyId).FirstOrDefault();
            if(agency!=null) {
                dealerPartsInventoryBill.AgencyId = agency.Agency.Id;
                dealerPartsInventoryBill.AgencyName = agency.Agency.Name;
                dealerPartsInventoryBill.AgencyCode = agency.Agency.Code;
            }
            var userInfo = Utils.GetCurrentUserInfo();
            dealerPartsInventoryBill.CreatorId = userInfo.Id;
            dealerPartsInventoryBill.CreatorName = userInfo.Name;
            dealerPartsInventoryBill.CreateTime = DateTime.Now;
        }

        private void UpdateDealerPartsInventoryBillValidate(DealerPartsInventoryBill dealerPartsInventoryBill) {

        }

        public void UpdateDealerPartsInventoryBill(DealerPartsInventoryBill dealerPartsInventoryBill) {

        }




        public void InsertPartsInventoryBill(DealerPartsInventoryBill dealerPartsInventoryBill) {
            InsertToDatabase(dealerPartsInventoryBill);
            var dealerPartsInventoryDetails = ChangeSet.GetAssociatedChanges(dealerPartsInventoryBill, v => v.DealerPartsInventoryDetails, ChangeOperation.Insert);
            foreach(DealerPartsInventoryDetail dealerPartsInventoryDetail in dealerPartsInventoryDetails) {
                InsertToDatabase(dealerPartsInventoryDetail);
            }
            InsertDealerPartsInventoryBillValidate(dealerPartsInventoryBill);
        }

        public IQueryable<DealerPartsInventoryBill> 查询盘点单(int? amountDifference) {
            var userInfo = Utils.GetCurrentUserInfo();
            //中心库只能查询有【服务站隶属管理】中维护了中心库对应关系服务站的盘点单。
            //判断当前的登录人的账号是不是中心库
            bool isZXK = false;
            //当前中心库
            var company = ObjectContext.Companies.Where(r => r.Id == userInfo.EnterpriseId).First();
            isZXK = company.Type == (int)DcsCompanyType.代理库;
            //分销中心人员关系
            var marketDptPersonnelRelationsCount = ObjectContext.MarketDptPersonnelRelations.Count(r => r.PersonnelId == userInfo.Id && r.Status == (int)DcsBaseDataStatus.有效);
            if(marketDptPersonnelRelationsCount > 0) {
                var tempResult = ObjectContext.DealerPartsInventoryBills;
                var dealerServiceInfoes = ObjectContext.DealerServiceInfoes;
                var result2 = from a in tempResult
                              join b in dealerServiceInfoes on new {
                                  dealerId = a.StorageCompanyId,
                                  PartsSalesCategoryrId = a.SalesCategoryId
                              } equals new {
                                  dealerId = b.DealerId,
                                  PartsSalesCategoryrId = b.PartsSalesCategoryId
                              }
                              join c in ObjectContext.MarketDptPersonnelRelations on b.MarketingDepartmentId equals c.MarketDepartmentId
                              where c.PersonnelId == userInfo.Id && b.Status == (int)DcsBaseDataStatus.有效 && c.Status == (int)DcsBaseDataStatus.有效
                              select a;
                result2 = result2.OrderBy(r => r.Id);
                if(isZXK) {
                    var AgencyDealerRelation = ObjectContext.AgencyDealerRelations.Where(r => r.AgencyCode == userInfo.EnterpriseCode).Select(a => a.DealerId).ToList();
                    result2 = result2.Where(a => AgencyDealerRelation.Contains(a.StorageCompanyId));
                }
                result2 = result2.Include("DealerPartsInventoryDetails").Include("PARTSSALESCATEGORY").Include("Company").Include("Branch");
                if(amountDifference.HasValue) {
                    var config = ObjectContext.MultiLevelApproveConfigs.Where(t => t.Id == amountDifference.Value).First();
                    result2 = result2.Where(t => t.DealerPartsInventoryDetails.Sum(r => (r.DealerPrice ?? 0) * r.StorageDifference) >= config.MinApproveFee && t.DealerPartsInventoryDetails.Sum(r => (r.DealerPrice ?? 0) * r.StorageDifference) < config.MaxApproveFee);
                }
                return result2;
            } else {
                var tempResult = ObjectContext.DealerPartsInventoryBills;
                var result = from a in tempResult
                             select a;
                result = result.OrderBy(r => r.Id);
                result = result.Include("DealerPartsInventoryDetails").Include("PARTSSALESCATEGORY").Include("Company").Include("Branch");
                if(amountDifference.HasValue) {
                    var config = ObjectContext.MultiLevelApproveConfigs.Where(t => t.Id == amountDifference.Value).First();
                    result = result.Where(t => t.DealerPartsInventoryDetails.Sum(r => (r.DealerPrice ?? 0) * r.StorageDifference) >= config.MinApproveFee && t.DealerPartsInventoryDetails.Sum(r => (r.DealerPrice ?? 0) * r.StorageDifference) < config.MaxApproveFee);
                }
                if(isZXK) {
                    var AgencyDealerRelation = ObjectContext.AgencyDealerRelations.Where(r => r.AgencyCode == userInfo.EnterpriseCode).Select(a => a.DealerId).ToList();
                    result = result.Where(a => AgencyDealerRelation.Contains(a.StorageCompanyId));
                }
                return result;
            }
        }

        public bool 校验盘点单重复盘点(int[] spareIds, int storageCompanyId, int salesCategoryId) {
            return ObjectContext.DealerPartsInventoryBills.Any(r => r.DealerPartsInventoryDetails.Any(x => spareIds.Contains(x.SparePartId)) && r.Status == (int)DcsDealerPartsInventoryBillStatus.新建 && r.SalesCategoryId == salesCategoryId && r.StorageCompanyId == storageCompanyId);
        }

        public DealerPartsInventoryBill GetDealerPartsInventoryBillById(int id) {
            return ObjectContext.DealerPartsInventoryBills.SingleOrDefault(r => r.Id == id);
        }

        public DealerPartsInventoryBill GetDealerPartsInventoryBillWithDetailsById(int id) {
            return ObjectContext.DealerPartsInventoryBills.Include("DealerPartsInventoryDetails").Include("PartsSalesCategory").SingleOrDefault(r => r.Id == id);
        }
    }
}
