﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DealerPartsStockQueryViewAch : DcsSerivceAchieveBase {
        public DealerPartsStockQueryViewAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public IQueryable<VirtualDealerPartsStockQueryView> 查询经销商保内外调拨配件库存(int warrantyBrandId, int noWarrantyBrandId, string sparePartCode, string sparePartName) {
            var userInfo = Utils.GetCurrentUserInfo();
            var dealerPartsStockQueryView = ObjectContext.DealerPartsStockQueryViews.Where(r => r.DealerCode == userInfo.EnterpriseCode && r.PartsSalesCategoryId == noWarrantyBrandId && r.Quantity > 0);
            if(!String.IsNullOrEmpty(sparePartCode)) {
                dealerPartsStockQueryView = dealerPartsStockQueryView.Where(r => sparePartCode.Contains(r.PartCode));
            }
            if(!String.IsNullOrEmpty(sparePartName)) {
                dealerPartsStockQueryView = dealerPartsStockQueryView.Where(r => sparePartName.Contains(r.PartName));
            }

            var result = from a in dealerPartsStockQueryView
                         join b in ObjectContext.SpareParts.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.IsNotWarrantyTransfer == true) on a.PartCode equals b.Code
                         join c in ObjectContext.PartsSalesPrices.Where(r => r.PartsSalesCategoryId == warrantyBrandId && r.Status == (int)DcsBaseDataStatus.有效) on b.Id equals c.SparePartId into temp
                         from partsSalesPrice in temp.DefaultIfEmpty()
                         select new VirtualDealerPartsStockQueryView {
                             SparePartId = b.Id,
                             SparePartCode = b.Code,
                             SparePartName = b.Name,
                             WarrantyPrice = partsSalesPrice.SalesPrice,
                             NoWarrantyPrice = a.SalesPrice,
                             ValidStock = a.Quantity,
                             NoWarrantyBrandName = a.PartsSalesCategoryName,
                             WarrantyBrandName = partsSalesPrice.PartsSalesCategoryName
                         };
            return result.OrderBy(r => r.SparePartId);
        }
    }

    partial class DcsDomainService {
        public IQueryable<VirtualDealerPartsStockQueryView> 查询经销商保内外调拨配件库存(int warrantyBrandId, int noWarrantyBrandId, string sparePartCode, string sparePartName) {
            return new DealerPartsStockQueryViewAch(this).查询经销商保内外调拨配件库存(warrantyBrandId, noWarrantyBrandId, sparePartCode, sparePartName);
        }
    }
}
