﻿using System.Collections.Generic;
using System.Linq;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class GCCPartsInboundCheckBillQueryAch : DcsSerivceAchieveBase {
        public GCCPartsInboundCheckBillQueryAch(DcsDomainService domainService)
            : base(domainService) {
        }
        
        public IEnumerable<WarehousingInspection> 工程车配件入库检验单查询() {
            return ObjectContext.WarehousingInspections.OrderBy(r=>r.Code);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:32
        //原分布类的函数全部转移到Ach                                                               
        
        public IEnumerable<WarehousingInspection> 工程车配件入库检验单查询() {
            return new GCCPartsInboundCheckBillQueryAch(this).工程车配件入库检验单查询();
        }
    }
}
