﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Sunlight.Silverlight.Dcs.Web.Resources;
using System.Collections.Generic;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class InOutSettleSummaryAch : DcsSerivceAchieveBase {
        public InOutSettleSummaryAch(DcsDomainService domainService)
            : base(domainService) {
        }
        public IEnumerable<InOutSettleSummaryQuery> 查询出入库统计(int? warehouseId, DateTime? bRecordTime, DateTime? eRecordTime) {
            string SQL = @"select yy.*, rownum as Id
                          from (select to_char(s.recordtime, 'yyyy-mm-dd') as recordtimes,
                                       sum(s.inpurchaselist) as inpurchaselist,
                                       sum(s.inreturnlist) as inreturnlist,
                                       sum(nvl(to_number(s.inpurchaselist), 0)) +
                                       sum(nvl(to_number(s.inreturnlist), 0)) as inpurchaselistAll,
                                       sum(s.inpurchaseqty) as inpurchaseqty,
                                       sum(s.inreturnqty) as inreturnqty,
                                       sum(nvl(s.inpurchaseqty,0)) + sum(nvl(s.inreturnqty,0)) as inpurchaseqtyAll,
                                       round(sum(s.inpurchasefee) / 10000, 2) as inpurchasefee,
                                       round(sum(s.inreturnfee) / 10000, 2) as inreturnfee,
                                       nvl(round(sum(s.inpurchasefee) / 10000, 2), 0) +
                                       nvl(round(sum(s.inreturnfee) / 10000, 2), 0) as inpurchasefeeAll,
                                       sum(s.waitinpurchaselist) as waitinpurchaselist,
                                       sum(s.waitinlinelist) as waitinlinelist,
                                       sum(nvl(to_number(s.waitinpurchaselist), 0)) +
                                       sum(nvl(to_number(s.waitinlinelist), 0)) as waitinpurchaselistAll,
                                       round(sum(s.waitinarrivefee) / 10000, 2) as waitinarrivefee,
                                       round(sum(s.waitinlinefee) / 10000, 2) as waitinlinefee,
                                       nvl(round(sum(s.waitinarrivefee) / 10000, 2), 0) +
                                       nvl(round(sum(s.waitinlinefee) / 10000, 2), 0) as waitinarrivefeeAll,
                                       sum(s.packingpurchaselist) as packingpurchaselist,
                                       sum(s.packingreturnlist) as packingreturnlist,
                                       sum(nvl(to_number(s.packingpurchaselist),0)) + sum(nvl(to_number(s.packingreturnlist),0)) as packingpurchaselistAll,
                                       sum(s.packingpurchaseqty) as packingpurchaseqty,
                                       sum(s.packingreturnqty) as packingreturnqty,
                                       sum(nvl(s.packingpurchaseqty,0)) + sum(nvl(s.packingreturnqty,0)) as packingpurchaseqtyAll,
                                       round(sum(s.packingpurchasefee) / 10000, 2) as packingpurchasefee,
                                       round(sum(s.packingreturnfee) / 10000, 2) as packingreturnfee,
                                       nvl(round(sum(s.packingpurchasefee) / 10000, 2),0) +
                                       nvl(round(sum(s.packingreturnfee) / 10000, 2),0) as packingpurchasefeeAll,
                                       sum(s.waitpackpurchaselist) as waitpackpurchaselist,
                                       sum(s.waitpackreturnlist) as waitpackreturnlist,
                                       sum(nvl(to_number(s.waitpackpurchaselist), 0)) + sum(nvl(to_number(s.waitpackreturnlist), 0)) as waitpackpurchaselistAll,
                                       round(sum(s.waitpackpurchasefee) / 10000, 2) as waitpackpurchasefee,
                                       round(sum(s.waitpackreturnfee) / 10000, 2) as waitpackreturnfee,
                                       nvl(round(sum(s.waitpackpurchasefee) / 10000, 2),0) +
                                       nvl(round(sum(s.waitpackreturnfee) / 10000, 2),0) as waitpackpurchasefeeAll,
                                       sum(s.shelvespurchaselist) as shelvespurchaselist,
                                       sum(s.shelvesreturnlist) as shelvesreturnlist,
                                       sum(nvl(to_number(s.shelvespurchaselist),0)) + sum(nvl(to_number(s.shelvesreturnlist),0)) as shelvespurchaselistAll,
                                       sum(s.shelvespurchaseqty) as shelvespurchaseqty,
                                       sum(s.shelvesreturnqty) as shelvesreturnqty,
                                       sum(nvl(s.shelvespurchaseqty,0)) + sum(nvl(s.shelvesreturnqty,0)) as shelvespurchaseqtyAll,
                                       round(sum(s.shelvespurchasefee) / 10000, 2) as shelvespurchasefee,
                                       round(sum(s.shelvesreturnfee) / 10000, 2) as shelvesreturnfee,
                                       nvl(round(sum(s.shelvespurchasefee) / 10000, 2),0) +
                                       nvl(round(sum(s.shelvesreturnfee) / 10000, 2),0) as shelvespurchasefeeAll,
                                       sum(s.waitshelvespurchaselis) as waitshelvespurchaselis,
                                       sum(s.waitshelvesreturnlist) as waitshelvesreturnlist,
                                       sum(nvl(to_number(s.waitshelvespurchaselis),0)) + sum(nvl(to_number(s.waitshelvesreturnlist),0)) as waitshelvespurchaselisAll,
                                       round(sum(s.waitshelvespurchasefee) / 10000, 2) as waitshelvespurchasefee,
                                       round(sum(s.waitshelvesreturnfee) / 10000, 2) as waitshelvesreturnfee,
                                       nvl(round(sum(s.waitshelvespurchasefee) / 10000, 2),0) +
                                       nvl(round(sum(s.waitshelvesreturnfee) / 10000, 2),0) as waitshelvespurchasefeeAll,
                                       sum(s.saleapprovelist) as saleapprovelist,
                                       sum(s.saleapproveqty) as saleapproveqty,
                                       round(sum(s.saleapprovefee) / 10000, 2) as saleapprovefee,
                                       sum(s.pickinglist) as pickinglist,
                                       sum(s.pickingqty) as pickingqty,
                                       round(sum(s.pickingfee) / 10000, 2) as pickingfee,
                                       sum(s.outlist) as outlist,
                                       sum(s.outqty) as outqty,
                                       round(sum(s.outfee) / 10000, 2) as outfee,
                                       sum(s.waitoutlist) as waitoutlist,
                                       sum(s.waitoutqty) as waitoutqty,
                                       round(sum(s.waitoutfee) / 10000, 2) as waitoutfee,
                                       round(sum(s.shippingfee) / 10000, 2) as shippingfee,
                                       sum(s.shippingweight) as shippingweight,
                                       sum(nvl(InPurchaseItem,0)+nvl(InReturnItem,0)) as InPurchaseInReturnItem,
                                       sum(nvl(WaitInPurchaseItem,0)+nvl(WaitInLineItem,0)) as WaitInPurchaseInLineItem,
                                       sum(nvl(PackingPurchaseItem,0)+nvl(PackingReturnItem,0))as PackingPurchaseReturnItem,
                                       sum(nvl(WaitPackPurchaseItem,0)+nvl(WaitPackReturnItem,0)) as WaitPackPurchaseReturnItem,
                                       sum(nvl(ShelvesPurchaseItem,0)+nvl(ShelvesReturnItem,0))as ShelvesPurchasesReturnItem,
                                       sum(nvl(WaitShelvesPurchaseItem,0)+nvl(WaitShelvesReturnItem,0)) as WaitShelvesPurchaseReturnItem,
                                       sum(SaleApproveItem)as SaleApproveItem,
                                       sum(PickingItem)as PickingItem,
                                       sum(OutItem)as OutItem,
                                       sum(WaitOutItem)as WaitOutItem
                      from InOutSettleSummary s  where  1=1  ";
            if(warehouseId.HasValue) {
                SQL = SQL + " and s.warehouseId =" + warehouseId;
            }
            if(bRecordTime.HasValue) {
                SQL = SQL + " and s.RecordTime>= to_date('" + bRecordTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if(eRecordTime.HasValue) {
                SQL = SQL + " and s.RecordTime<= to_date('" + eRecordTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            SQL = SQL + " group by s.recordtime order by s.recordtime)yy";
            var search = ObjectContext.ExecuteStoreQuery<InOutSettleSummaryQuery>(SQL).ToList();
            return search;
        }
    }
    partial class DcsDomainService {
        public IEnumerable<InOutSettleSummaryQuery> 查询出入库统计(int? warehouseId, DateTime? bRecordTime, DateTime? eRecordTime) {
            return new InOutSettleSummaryAch(this).查询出入库统计(warehouseId, bRecordTime, eRecordTime);
        }
    }
}
