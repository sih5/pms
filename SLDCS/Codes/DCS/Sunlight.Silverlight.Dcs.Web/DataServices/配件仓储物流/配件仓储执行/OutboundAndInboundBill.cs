﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Sunlight.Silverlight.Dcs.Web.Resources;
using System.Collections.Generic;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class OutboundAndInboundBillAch : DcsSerivceAchieveBase {
        public OutboundAndInboundBillAch(DcsDomainService domainService)
            : base(domainService) {
        }

        /// <summary>
        /// 查询待结算出入库明细
        /// </summary>
        /// <param name="storageCompanyId">企业Id</param>
        /// <param name="counterpartCompanyId">对方单位Id</param>
        /// <param name="warehouseId">仓库Id</param>
        /// <param name="customerAccountId">客户账户Id</param>
        /// <param name="partsSalesCategoryId"></param>
        /// <param name="inboundTypes">数组【入库类型】</param>
        /// <param name="outboundTypes">数组【出库类型】</param>
        /// <param name="endTime">结束时间</param>
        /// <returns></returns>
        public IQueryable<OutboundAndInboundBill> 查询待结算出入库明细(int storageCompanyId, int counterpartCompanyId, int? warehouseId, int? customerAccountId, int? partsSalesCategoryId, int[] inboundTypes, int[] outboundTypes, DateTime? endTime, bool cpPartsInboundCheckCodeNull, int? type) {
            IQueryable<OutboundAndInboundBill> result = null;
            if(outboundTypes != null && outboundTypes.Length > 0) {
                var partsOutboundBills = ObjectContext.PartsOutboundBills.Where(r => r.SettlementStatus == (int)DcsPartsSettlementStatus.待结算 && r.StorageCompanyId == storageCompanyId && r.CounterpartCompanyId == counterpartCompanyId);
                if(warehouseId.HasValue)
                    partsOutboundBills = partsOutboundBills.Where(r => r.WarehouseId == warehouseId.Value);
                if(customerAccountId.HasValue)
                    partsOutboundBills = partsOutboundBills.Where(r => r.CustomerAccountId == customerAccountId.Value);
                if(partsSalesCategoryId.HasValue)
                    partsOutboundBills = partsOutboundBills.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId.Value);
                if(endTime.HasValue)
                    partsOutboundBills = partsOutboundBills.Where(r => r.CreateTime <= endTime.Value);
                partsOutboundBills = partsOutboundBills.Where(r => outboundTypes.Contains(r.OutboundType));
                if(cpPartsInboundCheckCodeNull == true) {
                    partsOutboundBills = partsOutboundBills.Where(r => r.CPPartsInboundCheckCode == null);
                }
                var resultOut = from partsOutboundBill in partsOutboundBills
                                join g in ObjectContext.PartsSalesOrders on partsOutboundBill.OriginalRequirementBillCode equals g.Code into tempTable1
                                join ia in ObjectContext.InternalAllocationBills on partsOutboundBill.OriginalRequirementBillId equals ia.Id
                                from t3 in tempTable1.DefaultIfEmpty()
                                join partsSalesCategory in ObjectContext.PartsSalesCategories on partsOutboundBill.PartsSalesCategoryId equals partsSalesCategory.Id
                                where ia.Type == type
                                select new OutboundAndInboundBill {
                                    BillId = partsOutboundBill.Id,
                                    InvoiceType = t3.InvoiceType,
                                    ERPOrderCode = t3.ERPSourceOrderCode,
                                    InvoiceCustomerName = t3.InvoiceCustomerName,
                                    Taxpayeridentification = t3.Taxpayeridentification,
                                    BillType = (int)DcsPartsLogisticBatchBillDetailBillType.配件出库单,
                                    BillCode = partsOutboundBill.Code,
                                    SettlementAmount = ObjectContext.PartsOutboundBillDetails.Where(r => r.PartsOutboundBillId == partsOutboundBill.Id).Sum(v => v.SettlementPrice * v.OutboundAmount),
                                    CostAmount = ObjectContext.PartsOutboundBillDetails.Where(r => r.PartsOutboundBillId == partsOutboundBill.Id).Sum(v => v.CostPrice * v.OutboundAmount),
                                    BillCreateTime = partsOutboundBill.CreateTime.Value,
                                    CompanyId = partsOutboundBill.StorageCompanyId,
                                    CounterpartCompanyId = partsOutboundBill.CounterpartCompanyId,
                                    CustomerAccountId = partsOutboundBill.CustomerAccountId,
                                    BillBusinessType = partsOutboundBill.OutboundType,
                                    WarehouseId = partsOutboundBill.WarehouseId,
                                    WarehouseName = partsOutboundBill.WarehouseName,
                                    PartsSalesCategoryId = partsOutboundBill.PartsSalesCategoryId,
                                    PartsSalesCategoryName = partsSalesCategory.Name,
                                    ReturnType = 0,
                                    PartsPurchaseSettleCode = partsOutboundBill.PartsPurchaseSettleCode,
                                    CPPartsInboundCheckCode = partsOutboundBill.CPPartsInboundCheckCode
                                };
                result = result != null ? result.Concat(resultOut) : resultOut;
            }
            return result.OrderBy(r => r.BillId);
        }

        public IQueryable<OutboundAndInboundBill> 查询待结算出入库明细采埃孚(int storageCompanyId, int counterpartCompanyId, int? warehouseId, int? customerAccountId, int? partsSalesCategoryId, int[] inboundTypes, int[] outboundTypes, DateTime? endTime) {
            IQueryable<OutboundAndInboundBill> result = null;
            if(inboundTypes != null && inboundTypes.Length > 0) {
                var partsInboundCheckBills = ObjectContext.PartsInboundCheckBills.Where(r => r.SettlementStatus == (int)DcsPartsSettlementStatus.待结算 && r.StorageCompanyId == storageCompanyId && r.CounterpartCompanyId == counterpartCompanyId);
                if(warehouseId.HasValue)
                    partsInboundCheckBills = partsInboundCheckBills.Where(r => r.WarehouseId == warehouseId.Value);
                if(customerAccountId.HasValue)
                    partsInboundCheckBills = partsInboundCheckBills.Where(r => r.CustomerAccountId == customerAccountId.Value);
                if(partsSalesCategoryId.HasValue)
                    partsInboundCheckBills = partsInboundCheckBills.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId.Value);
                if(endTime.HasValue)
                    partsInboundCheckBills = partsInboundCheckBills.Where(r => r.CreateTime <= endTime.Value);
                partsInboundCheckBills = partsInboundCheckBills.Where(r => inboundTypes.Contains(r.InboundType));
                result = from partsInboundCheckBill in partsInboundCheckBills
                         join partsSalesReturnBill in ObjectContext.PartsSalesReturnBills on partsInboundCheckBill.OriginalRequirementBillId equals partsSalesReturnBill.Id into temp
                         from item in temp.DefaultIfEmpty()
                         join partsSalesReturnBillDetail in ObjectContext.PartsSalesReturnBillDetails on item.Id equals partsSalesReturnBillDetail.PartsSalesReturnBillId into temp1
                         from item1 in temp1.DefaultIfEmpty()
                         join g in ObjectContext.PartsSalesOrders on item1.PartsSalesOrderId equals g.Id into tempTable1
                         from t3 in tempTable1.DefaultIfEmpty()
                         join partsSalesCategory in ObjectContext.PartsSalesCategories on partsInboundCheckBill.PartsSalesCategoryId equals partsSalesCategory.Id
                         select new OutboundAndInboundBill {
                             BillId = partsInboundCheckBill.Id,
                             InvoiceType = t3.InvoiceType,
                             ERPOrderCode = t3.ERPSourceOrderCode,
                             InvoiceCustomerName = t3.InvoiceCustomerName,
                             Taxpayeridentification = t3.Taxpayeridentification,
                             BillType = (int)DcsPartsLogisticBatchBillDetailBillType.配件入库检验单,
                             BillCode = partsInboundCheckBill.Code,
                             SettlementAmount = ObjectContext.PartsInboundCheckBillDetails.Where(r => r.PartsInboundCheckBillId == partsInboundCheckBill.Id).Sum(v => v.SettlementPrice * v.InspectedQuantity),
                             CostAmount = ObjectContext.PartsInboundCheckBillDetails.Where(r => r.PartsInboundCheckBillId == partsInboundCheckBill.Id).Sum(v => v.CostPrice * v.InspectedQuantity),
                             BillCreateTime = partsInboundCheckBill.CreateTime.Value,
                             CompanyId = partsInboundCheckBill.StorageCompanyId,
                             CounterpartCompanyId = partsInboundCheckBill.CounterpartCompanyId,
                             CustomerAccountId = partsInboundCheckBill.CustomerAccountId,
                             BillBusinessType = partsInboundCheckBill.InboundType,
                             WarehouseId = partsInboundCheckBill.WarehouseId,
                             WarehouseName = partsInboundCheckBill.WarehouseName,
                             PartsSalesCategoryId = partsInboundCheckBill.PartsSalesCategoryId,
                             PartsSalesCategoryName = partsSalesCategory.Name,
                             ReturnType = item.ReturnType,
                             PartsPurchaseSettleCode = ""
                         };
            }
            if(outboundTypes != null && outboundTypes.Length > 0) {

                var partsOutboundBills = ObjectContext.PartsOutboundBills.Where(r => r.SettlementStatus == (int)DcsPartsSettlementStatus.待结算 && r.StorageCompanyId == storageCompanyId && r.CounterpartCompanyId == counterpartCompanyId && r.CPPartsInboundCheckCode != null);
                if(warehouseId.HasValue)
                    partsOutboundBills = partsOutboundBills.Where(r => r.WarehouseId == warehouseId.Value);
                if(customerAccountId.HasValue)
                    partsOutboundBills = partsOutboundBills.Where(r => r.CustomerAccountId == customerAccountId.Value);
                if(partsSalesCategoryId.HasValue)
                    partsOutboundBills = partsOutboundBills.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId.Value);
                if(endTime.HasValue)
                    partsOutboundBills = partsOutboundBills.Where(r => r.CreateTime <= endTime.Value);
                partsOutboundBills = partsOutboundBills.Where(r => outboundTypes.Contains(r.OutboundType));
                var resultOut = from partsOutboundBill in partsOutboundBills
                                join g in ObjectContext.PartsSalesOrders on partsOutboundBill.OriginalRequirementBillCode equals g.Code into tempTable1
                                from t3 in tempTable1.DefaultIfEmpty()
                                join partsSalesCategory in ObjectContext.PartsSalesCategories on partsOutboundBill.PartsSalesCategoryId equals partsSalesCategory.Id
                                //join PartsPurchaseorder in ObjectContext.PartsPurchaseOrders
                                //on partsOutboundBill.CPPartsPurchaseOrderCode equals PartsPurchaseorder.CPPartsPurchaseOrderCode into JoinedEmpDept
                                //from PartsPurchaseorder in JoinedEmpDept.DefaultIfEmpty()
                                select new OutboundAndInboundBill {
                                    BillId = partsOutboundBill.Id,
                                    InvoiceType = t3.InvoiceType,
                                    ERPOrderCode = t3.ERPSourceOrderCode,
                                    InvoiceCustomerName = t3.InvoiceCustomerName,
                                    Taxpayeridentification = t3.Taxpayeridentification,
                                    BillType = (int)DcsPartsLogisticBatchBillDetailBillType.配件出库单,
                                    BillCode = partsOutboundBill.Code,
                                    //SettlementAmount = (from detail in ObjectContext.PartsOutboundBillDetails join pricing in ObjectContext.PartsPurchasePricings on detail.SparePartId equals pricing.PartId into JoinedEmpD from pricing in JoinedEmpD.DefaultIfEmpty() where detail.PartsOutboundBillId == partsOutboundBill.Id select new { detail.PartsOutboundBillId, detail.OutboundAmount, pricing.PurchasePrice, pricing.PartsSalesCategoryId, pricing.PartsSupplierId, pricing.BranchId, pricing.ValidFrom, pricing.ValidTo }).Where(d => d.PartsOutboundBillId == partsOutboundBill.Id && d.PartsSalesCategoryId == partsOutboundBill.PartsSalesCategoryId && d.PartsSupplierId == PartsPurchaseorder.PartsSupplierId && d.BranchId == partsOutboundBill.BranchId && (d.ValidFrom <= partsOutboundBill.CreateTime && d.ValidTo >= partsOutboundBill.CreateTime)).Sum(v => v.PurchasePrice * v.OutboundAmount) > 0 ? (from detail in ObjectContext.PartsOutboundBillDetails join pricing in ObjectContext.PartsPurchasePricings on detail.SparePartId equals pricing.PartId into JoinedEmpD from pricing in JoinedEmpD.DefaultIfEmpty() where detail.PartsOutboundBillId == partsOutboundBill.Id select new { detail.PartsOutboundBillId, detail.OutboundAmount, pricing.PurchasePrice, pricing.PartsSalesCategoryId, pricing.PartsSupplierId, pricing.BranchId, pricing.ValidFrom, pricing.ValidTo }).Where(d => d.PartsOutboundBillId == partsOutboundBill.Id && d.PartsSalesCategoryId == partsOutboundBill.PartsSalesCategoryId && d.PartsSupplierId == PartsPurchaseorder.PartsSupplierId && d.BranchId == partsOutboundBill.BranchId && (d.ValidFrom <= partsOutboundBill.CreateTime && d.ValidTo >= partsOutboundBill.CreateTime)).Sum(v => v.PurchasePrice * v.OutboundAmount) : ObjectContext.PartsOutboundBillDetails.Where(r => r.PartsOutboundBillId == partsOutboundBill.Id).Sum(v => v.SettlementPrice * v.OutboundAmount),
                                    //SettlementAmount = partsOutboundBill.PartsPurchaseSettleCode == null ? ObjectContext.PartsOutboundBillDetails.Where(r => r.PartsOutboundBillId == partsOutboundBill.Id).Sum(v => v.SettlementPrice * v.OutboundAmount) : (from detail in ObjectContext.PartsOutboundBillDetails join pricing in ObjectContext.PartsPurchasePricings on detail.SparePartId equals pricing.PartId into JoinedEmpDept1 from pricing in JoinedEmpDept1.DefaultIfEmpty() where detail.PartsOutboundBillId == partsOutboundBill.Id select new { detail.PartsOutboundBillId, detail.OutboundAmount, pricing.PurchasePrice, pricing.PartsSalesCategoryId, pricing.PartsSupplierId, pricing.BranchId, pricing.ValidFrom, pricing.ValidTo }).Where(d => d.PartsOutboundBillId == partsOutboundBill.Id && d.PartsSalesCategoryId == partsOutboundBill.PartsSalesCategoryId && d.PartsSupplierId == PartsPurchaseorder.PartsSupplierId && d.BranchId == partsOutboundBill.BranchId && (d.ValidFrom <= DateTime.Now && d.ValidTo >= DateTime.Now)).Sum(v => v.PurchasePrice * v.OutboundAmount),
                                    SettlementAmount = ObjectContext.PartsOutboundBillDetails.Where(r => r.PartsOutboundBillId == partsOutboundBill.Id).Sum(v => v.SettlementPrice * v.OutboundAmount),
                                    CostAmount = ObjectContext.PartsOutboundBillDetails.Where(r => r.PartsOutboundBillId == partsOutboundBill.Id).Sum(v => v.CostPrice * v.OutboundAmount),
                                    BillCreateTime = partsOutboundBill.CreateTime.Value,
                                    CompanyId = partsOutboundBill.StorageCompanyId,
                                    CounterpartCompanyId = partsOutboundBill.CounterpartCompanyId,
                                    CustomerAccountId = partsOutboundBill.CustomerAccountId,
                                    BillBusinessType = partsOutboundBill.OutboundType,
                                    WarehouseId = partsOutboundBill.WarehouseId,
                                    WarehouseName = partsOutboundBill.WarehouseName,
                                    PartsSalesCategoryId = partsOutboundBill.PartsSalesCategoryId,
                                    PartsSalesCategoryName = partsSalesCategory.Name,
                                    ReturnType = 0,
                                    PartsPurchaseSettleCode = partsOutboundBill.PartsPurchaseSettleCode
                                };
                //resultOut=resultOut.Distinct();
                result = result != null ? result.Concat(resultOut) : resultOut;
            }
            return result.OrderBy(r => r.BillId);
        }

        public IQueryable<OutboundAndInboundBill> 查询待销售结算出入库明细(int companyId, int destCompanyId, int? warehouseId, int? customerAccountId, int? partsSaleCategoryId, int[] inBoundType, int[] outBoundType, DateTime? endTime, int? settleType, int? businessType) {
            //1、企业Id，对方单位Id，不能为空2、数组[入库类型]，数组[出库类型] 不能同时为空
            IQueryable<OutboundAndInboundBill> result;
            if(outBoundType.Length == 0 && inBoundType.Length == 0) {
                throw new ValidationException(ErrorStrings.DataOfPartsInAndOut_Validation1);
            }

            var partsOutBoundBills = from r in ObjectContext.PartsOutboundBills
                                     join s in ObjectContext.PartsSalesOrderTypes on r.PartsSalesOrderTypeId equals s.Id
                                     where r.CPPartsInboundCheckCode == null && r.SettlementStatus == (int)DcsPartsSettlementStatus.待结算 && (warehouseId == null || r.WarehouseId == warehouseId) && r.StorageCompanyId == companyId && r.CounterpartCompanyId == destCompanyId && (customerAccountId == null || r.CustomerAccountId == customerAccountId) && outBoundType.Contains(r.OutboundType) && (endTime == null || r.CreateTime <= endTime) && (partsSaleCategoryId == null || r.PartsSalesCategoryId == partsSaleCategoryId) && (settleType == null || s.SettleType == settleType) && (businessType == null || s.BusinessType == businessType)
                                     select r;
            var originalRequirementBillIds = partsOutBoundBills.Select(r => r.OriginalRequirementBillId);
            var partsSalesReturnBills = from a in ObjectContext.PartsSalesReturnBills
                                        join c in ObjectContext.PartsSalesReturnBillDetails on a.Id equals c.PartsSalesReturnBillId
                                        join b in originalRequirementBillIds on c.PartsSalesOrderId equals b
                                        where a.Status != (int)DcsPartsSalesReturnBillStatus.作废
                                        select a;
            var partsSalesReturnBillIds = partsSalesReturnBills.Select(r => r.Id);
            var partsInboundCheckBills = from a in ObjectContext.PartsInboundCheckBills
                                         from b in partsSalesReturnBillIds
                                         join c in ObjectContext.PartsSalesReturnBills on a.OriginalRequirementBillId equals c.Id
                                         join e in ObjectContext.PartsSalesReturnBillDetails on a.Id equals e.PartsSalesReturnBillId
                                         join d in ObjectContext.PartsSalesOrders on new { Id = e.PartsSalesOrderId.Value, CustomerAccountId=a.CustomerAccountId.Value } equals new { d.Id, d.CustomerAccountId }
                                         join s in ObjectContext.PartsSalesOrderTypes on d.PartsSalesOrderTypeId equals s.Id
                                         where a.CPPartsInboundCheckCode == null && a.OriginalRequirementBillId == b && a.SettlementStatus == (int)DcsPartsSettlementStatus.待结算 && (warehouseId == null || a.WarehouseId == warehouseId) && a.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件销售退货单 && (settleType == null || s.SettleType == settleType) && (businessType == null || s.BusinessType == businessType)
                                         select a;
            result = from partsInboundCheckBill in partsInboundCheckBills
                     join g in ObjectContext.PartsSalesReturnBills on partsInboundCheckBill.OriginalRequirementBillCode equals g.Code
                     join e in ObjectContext.PartsSalesReturnBillDetails on g.Id equals e.PartsSalesReturnBillId into temp1
                     from t1 in temp1.DefaultIfEmpty()
                     join z in ObjectContext.PartsSalesOrders on t1.PartsSalesOrderId equals z.Id
                     join partsSalesCategory in ObjectContext.PartsSalesCategories
                     on partsInboundCheckBill.PartsSalesCategoryId equals partsSalesCategory.Id
                     select new OutboundAndInboundBill {
                         BillId = partsInboundCheckBill.Id,
                         InvoiceType = z.InvoiceType,
                         ERPOrderCode = z.ERPSourceOrderCode,
                         InvoiceCustomerName = z.InvoiceCustomerName,
                         Taxpayeridentification = z.Taxpayeridentification,
                         BillType = (int)DcsPartsLogisticBatchBillDetailBillType.配件入库检验单,
                         BillCode = partsInboundCheckBill.Code,
                         OriginalRequirementBillType = partsInboundCheckBill.OriginalRequirementBillType,
                         OriginalRequirementBillId = partsInboundCheckBill.OriginalRequirementBillId,
                         SettlementAmount = ObjectContext.PartsInboundCheckBillDetails.Where(r => r.PartsInboundCheckBillId == partsInboundCheckBill.Id).Sum(v => v.SettlementPrice * v.InspectedQuantity),
                         CostAmount = ObjectContext.PartsInboundCheckBillDetails.Where(r => r.PartsInboundCheckBillId == partsInboundCheckBill.Id).Sum(v => v.CostPrice * v.InspectedQuantity),
                         BillCreateTime = partsInboundCheckBill.CreateTime.Value,
                         CompanyId = partsInboundCheckBill.StorageCompanyId,
                         CounterpartCompanyId = partsInboundCheckBill.CounterpartCompanyId,
                         CounterpartCompanyCode = partsInboundCheckBill.CounterpartCompanyCode,
                         CounterpartCompanyName = partsInboundCheckBill.CounterpartCompanyName,
                         CustomerAccountId = partsInboundCheckBill.CustomerAccountId,
                         BillBusinessType = partsInboundCheckBill.InboundType,
                         WarehouseId = partsInboundCheckBill.WarehouseId,
                         WarehouseName = partsInboundCheckBill.WarehouseName,
                         PartsSalesCategoryName = partsSalesCategory.Name
                     };
            var resultOut = from partsOutboundBill in partsOutBoundBills
                            join g in ObjectContext.PartsSalesOrders on partsOutboundBill.OriginalRequirementBillCode equals g.Code into tempTable1
                            from t3 in tempTable1
                            join partsSalesCategory in ObjectContext.PartsSalesCategories
                            on partsOutboundBill.PartsSalesCategoryId equals partsSalesCategory.Id
                            select new OutboundAndInboundBill {
                                BillId = partsOutboundBill.Id,
                                InvoiceType = t3.InvoiceType,
                                ERPOrderCode = t3.ERPSourceOrderCode,
                                InvoiceCustomerName = t3.InvoiceCustomerName,
                                Taxpayeridentification = t3.Taxpayeridentification,
                                BillType = (int)DcsPartsLogisticBatchBillDetailBillType.配件出库单,
                                BillCode = partsOutboundBill.Code,
                                OriginalRequirementBillType = partsOutboundBill.OriginalRequirementBillType,
                                OriginalRequirementBillId = partsOutboundBill.OriginalRequirementBillId,
                                SettlementAmount = ObjectContext.PartsOutboundBillDetails.Where(r => r.PartsOutboundBillId == partsOutboundBill.Id).Sum(v => v.SettlementPrice * v.OutboundAmount),
                                CostAmount = ObjectContext.PartsOutboundBillDetails.Where(r => r.PartsOutboundBillId == partsOutboundBill.Id).Sum(v => v.CostPrice * v.OutboundAmount),
                                BillCreateTime = partsOutboundBill.CreateTime.Value,
                                CompanyId = partsOutboundBill.StorageCompanyId,
                                CounterpartCompanyId = partsOutboundBill.CounterpartCompanyId,
                                CounterpartCompanyCode = partsOutboundBill.CounterpartCompanyCode,
                                CounterpartCompanyName = partsOutboundBill.CounterpartCompanyName,
                                CustomerAccountId = partsOutboundBill.CustomerAccountId,
                                BillBusinessType = partsOutboundBill.OutboundType,
                                WarehouseId = partsOutboundBill.WarehouseId,
                                WarehouseName = partsOutboundBill.WarehouseName,
                                PartsSalesCategoryName = partsSalesCategory.Name
                            };
            result = result.Concat(resultOut).Distinct();
            return result.OrderBy(r => r.BillId);
        }
        public IEnumerable<OutboundAndInboundBill> 查询待销售结算出入库明细New(int companyId, int destCompanyId, int? warehouseId, int? customerAccountId, int? partsSaleCategoryId, int[] inBoundType, int[] outBoundType, DateTime? endTime, int? settleType, int? businessType,bool? isOil)
        {
            string SQL = @"select distinct a.id as BillId,
                a.code as BillCode,
                5 as BillType,
                a.warehouseid,
                a.warehousecode,
                a.warehousename,
                (select sum(tb.settlementprice * tb.inspectedquantity) as settlementamount
                   from partsinboundcheckbill ta
                  inner join partsinboundcheckbilldetail tb
                     on ta.id = tb.partsinboundcheckbillid
                  where ta.id = a.id) as settlementamount,
                (select sum(tb.costprice * tb.inspectedquantity) as settlementamount
                   from partsinboundcheckbill ta
                  inner join partsinboundcheckbilldetail tb
                     on ta.id = tb.partsinboundcheckbillid
                  where ta.id = a.id) as costamount,
                a.createtime as BillCreateTime,
                b.returntype,
                e.settletype,
                e.businesstype,
                a.customeraccountid,
a.OriginalRequirementBillId as OriginalRequirementBillId,
 a.OriginalRequirementBillType as OriginalRequirementBillType,
a.CounterpartCompanyCode,
a.CounterpartCompanyName,
e.Name as PartsSalesOrderTypeName
  from partsinboundcheckbill a
 inner join partssalesreturnbill b
    on a.originalrequirementbillid = b.id
 inner join partssalesreturnbilldetail c
    on c.partssalesreturnbillid = b.id
 inner join partssalesorder d
    on d.code = c.partssalesordercode
 inner join partssalesordertype e
    on e.id = d.partssalesordertypeid
 where a.SettlementStatus=2 and  exists
 (select 1
          from partsoutboundbill tmp
         where tmp.settlementstatus = 2
           and tmp.counterpartcompanyid = a.counterpartcompanyid
           and tmp.storagecompanyid = a.storagecompanyid
           and tmp.customeraccountid = a.customeraccountid
           and tmp.originalrequirementbillcode = c.partssalesordercode
           and tmp.StorageCompanyId=" + companyId + " and tmp.CounterpartCompanyId=" + destCompanyId;

            if (warehouseId.HasValue)
            {
                SQL += " and tmp.warehouseId=" + warehouseId;
           }
            if (customerAccountId.HasValue)
            {
                SQL += " and tmp.customerAccountId=" + customerAccountId;
            }
            if (partsSaleCategoryId.HasValue)
            {
                SQL += " and tmp.PartsSalesCategoryId=" + partsSaleCategoryId;
            }
            if (endTime.HasValue)
            {
                SQL = SQL + " and tmp.CreateTime<= to_date('" + endTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
                                                                      SQL += ")";
           if (settleType.HasValue)
            {
                SQL += " and e.settleType=" + settleType;
            }
           if (businessType.HasValue)
           {
               SQL += " and e.businessType=" + businessType;
           }
            if(isOil.HasValue){
                if(isOil.Value){
                    SQL += " and  e.Name='油品订单'" ;
                } else {
                    SQL += " and  e.Name<>'油品订单'";
                }
            }
           SQL += " union all select a.id as BillId,a.code as BillCode, 1 as BillType, a.warehouseid,  a.warehousecode,  a.warehousename, (select sum(b.settlementprice * b.outboundamount) "
           +"from partsoutboundbill s inner join partsoutboundbilldetail b  on s.id = b.partsoutboundbillid where s.id=a.id) as settlementamount, (select sum(b.costprice * b.outboundamount)  from partsoutboundbill a  inner join partsoutboundbilldetail b "
           + " on a.id = b.partsoutboundbillid) as costamount,  a.createtime as BillCreateTime,   null as returntype,  c.settletype, c.businesstype,  a.customeraccountid ,a.OriginalRequirementBillId as OriginalRequirementBillId, a.OriginalRequirementBillType as OriginalRequirementBillType,a.CounterpartCompanyCode,a.CounterpartCompanyName,c.Name as PartsSalesOrderTypeName "
            +"from partsoutboundbill a  inner join partssalesordertype c on c.id = a.partssalesordertypeid "
            + "where a.settlementstatus = 2 and a.StorageCompanyId=" + companyId + " and a.CounterpartCompanyId=" + destCompanyId;
            if (warehouseId.HasValue)
            {
                SQL += " and a.warehouseId=" + warehouseId;
            }
            if (customerAccountId.HasValue)
            {
                SQL += " and a.customerAccountId=" + customerAccountId;
            }
            if (partsSaleCategoryId.HasValue)
            {
                SQL += " and a.PartsSalesCategoryId=" + partsSaleCategoryId;
            }
            if (endTime.HasValue)
            {
                SQL = SQL + " and a.CreateTime<= to_date('" + endTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if (settleType.HasValue)
            {
                SQL += " and c.settleType=" + settleType;
            }
            if (businessType.HasValue)
            {
                SQL += " and c.businessType=" + businessType;
            }
            if(isOil.HasValue) {
                if(isOil.Value) {
                    SQL += " and  c.Name='油品订单'";
                } else {
                    SQL += " and  c.Name<>'油品订单'";
                }
            }
            var search = ObjectContext.ExecuteStoreQuery<OutboundAndInboundBill>(SQL).ToList();
            return search;
        }
        public IEnumerable<OutboundAndInboundBill> 查询待销售结算出入库清单明细New(int companyId, int destCompanyId, int? warehouseId, int? customerAccountId, int? partsSaleCategoryId, int[] inBoundType, int[] outBoundType, DateTime? endTime, int? settleType, int? businessType)
        {
            string SQL = @" select   a.Id as BillId,
                          a.Code as BillCode,
                         h.SparePartCode ,
                         h.SparePartName
  from partsinboundcheckbill a
inner join  PartsInboundCheckBillDetail h
 on a.id=h.PartsInboundCheckBillId
 inner join partssalesreturnbill b
    on a.originalrequirementbillid = b.id
 inner join partssalesreturnbilldetail c
    on c.partssalesreturnbillid = b.id
 inner join partssalesorder d
    on d.code = c.partssalesordercode
 inner join partssalesordertype e
    on e.id = d.partssalesordertypeid
 where exists
 (select 1
          from partsoutboundbill tmp
         where tmp.settlementstatus = 2
           and tmp.counterpartcompanyid = a.counterpartcompanyid
           and tmp.storagecompanyid = a.storagecompanyid
           and tmp.customeraccountid = a.customeraccountid
           and tmp.originalrequirementbillcode = c.partssalesordercode
           and tmp.StorageCompanyId=" + companyId + " and tmp.CounterpartCompanyId=" + destCompanyId;

            if (warehouseId.HasValue)
            {
                SQL += " and tmp.warehouseId=" + warehouseId;
            }
            if (customerAccountId.HasValue)
            {
                SQL += " and tmp.customerAccountId=" + customerAccountId;
            }
            if (partsSaleCategoryId.HasValue)
            {
                SQL += " and tmp.PartsSalesCategoryId=" + partsSaleCategoryId;
            }
            if (endTime.HasValue)
            {
                SQL = SQL + " and tmp.CreateTime<= to_date('" + endTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            SQL += ")";
            if (settleType.HasValue)
            {
                SQL += " and e.settleType=" + settleType;
            }
            if (businessType.HasValue)
            {
                SQL += " and e.businessType=" + businessType;
            }
            SQL += " union all select   a.Id as BillId, a.Code as BillCode,  h.SparePartCode , h.SparePartName "
             + "from partsoutboundbill a inner join PartsOutboundBillDetail h on a.id=h.PartsOutboundBillId  inner join partssalesorder b   on a.originalrequirementbillcode = b.code inner join partssalesordertype c on c.id = b.partssalesordertypeid "
             + "where a.settlementstatus = 2 and a.StorageCompanyId=" + companyId + " and a.CounterpartCompanyId=" + destCompanyId;
            if (warehouseId.HasValue)
            {
                SQL += " and a.warehouseId=" + warehouseId;
            }
            if (customerAccountId.HasValue)
            {
                SQL += " and a.customerAccountId=" + customerAccountId;
            }
            if (partsSaleCategoryId.HasValue)
            {
                SQL += " and a.PartsSalesCategoryId=" + partsSaleCategoryId;
            }
            if (endTime.HasValue)
            {
                SQL = SQL + " and a.CreateTime<= to_date('" + endTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if (settleType.HasValue)
            {
                SQL += " and c.settleType=" + settleType;
            }
            if (businessType.HasValue)
            {
                SQL += " and c.businessType=" + businessType;
            }
            var search = ObjectContext.ExecuteStoreQuery<OutboundAndInboundBill>(SQL).ToList();
            return search;
        }
        public IQueryable<OutboundAndInboundBill> 查询待销售结算出入库明细采埃孚(int companyId, int destCompanyId, int? warehouseId, int? customerAccountId, int? partsSaleCategoryId, int[] inBoundType, int[] outBoundType, DateTime? endTime, int? ischeck) {
            //1、企业Id，对方单位Id，不能为空2、数组[入库类型]，数组[出库类型] 不能同时为空
            IQueryable<OutboundAndInboundBill> result;
            if(outBoundType.Length == 0 && inBoundType.Length == 0) {
                throw new ValidationException(ErrorStrings.DataOfPartsInAndOut_Validation1);
            }

            var partsOutBoundBills = from r in ObjectContext.PartsOutboundBills
                                     where r.SettlementStatus == (int)DcsPartsSettlementStatus.待结算 && (warehouseId == null || r.WarehouseId == warehouseId) && r.StorageCompanyId == companyId && r.CounterpartCompanyId == destCompanyId && (customerAccountId == null || r.CustomerAccountId == customerAccountId) && outBoundType.Contains(r.OutboundType) && (endTime == null || r.CreateTime <= endTime) && (partsSaleCategoryId == null || r.PartsSalesCategoryId == partsSaleCategoryId) && r.CPPartsInboundCheckCode != null
                                     select r;
            if(ischeck.HasValue) {
                if(ischeck == 1) {
                    partsOutBoundBills = partsOutBoundBills.Where(r => r.PartsPurchaseSettleCode != null);
                } else {
                    partsOutBoundBills = partsOutBoundBills.Where(r => r.PartsPurchaseSettleCode == null);
                }

            }
            var originalRequirementBillIds = partsOutBoundBills.Select(r => r.OriginalRequirementBillId);
            var partsSalesReturnBills = from a in ObjectContext.PartsSalesReturnBills
                                        join c in ObjectContext.PartsSalesReturnBillDetails on a.Id equals c.PartsSalesReturnBillId
                                        join b in originalRequirementBillIds on c.PartsSalesOrderId equals b
                                        where a.Status != (int)DcsPartsSalesReturnBillStatus.作废
                                        select a;
            var partsSalesReturnBillIds = partsSalesReturnBills.Select(r => r.Id);
            var partsInboundCheckBills = from a in ObjectContext.PartsInboundCheckBills
                                         from b in partsSalesReturnBillIds
                                         where a.OriginalRequirementBillId == b && a.SettlementStatus == (int)DcsPartsSettlementStatus.待结算 && (warehouseId == null || a.WarehouseId == warehouseId) && a.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件销售退货单
                                         select a;
            result = from partsInboundCheckBill in partsInboundCheckBills
                     join g in ObjectContext.PartsSalesReturnBills on partsInboundCheckBill.OriginalRequirementBillCode equals g.Code
                     join e in ObjectContext.PartsSalesReturnBillDetails on g.Id equals e.PartsSalesReturnBillId
                     join z in ObjectContext.PartsSalesOrders on e.PartsSalesOrderId equals z.Id
                     join partsSalesCategory in ObjectContext.PartsSalesCategories
                     on partsInboundCheckBill.PartsSalesCategoryId equals partsSalesCategory.Id
                     select new OutboundAndInboundBill {
                         BillId = partsInboundCheckBill.Id,
                         InvoiceType = z.InvoiceType,
                         ERPOrderCode = z.ERPSourceOrderCode,
                         InvoiceCustomerName = z.InvoiceCustomerName,
                         Taxpayeridentification = z.Taxpayeridentification,
                         BillType = (int)DcsPartsLogisticBatchBillDetailBillType.配件入库检验单,
                         BillCode = partsInboundCheckBill.Code,
                         OriginalRequirementBillType = partsInboundCheckBill.OriginalRequirementBillType,
                         OriginalRequirementBillId = partsInboundCheckBill.OriginalRequirementBillId,
                         SettlementAmount = ObjectContext.PartsInboundCheckBillDetails.Where(r => r.PartsInboundCheckBillId == partsInboundCheckBill.Id).Sum(v => v.SettlementPrice * v.InspectedQuantity),
                         CostAmount = ObjectContext.PartsInboundCheckBillDetails.Where(r => r.PartsInboundCheckBillId == partsInboundCheckBill.Id).Sum(v => v.CostPrice * v.InspectedQuantity),
                         BillCreateTime = partsInboundCheckBill.CreateTime.Value,
                         CompanyId = partsInboundCheckBill.StorageCompanyId,
                         CounterpartCompanyId = partsInboundCheckBill.CounterpartCompanyId,
                         CounterpartCompanyCode = partsInboundCheckBill.CounterpartCompanyCode,
                         CounterpartCompanyName = partsInboundCheckBill.CounterpartCompanyName,
                         CustomerAccountId = partsInboundCheckBill.CustomerAccountId,
                         BillBusinessType = partsInboundCheckBill.InboundType,
                         WarehouseId = partsInboundCheckBill.WarehouseId,
                         WarehouseName = partsInboundCheckBill.WarehouseName,
                         PartsSalesCategoryName = partsSalesCategory.Name,
                         PartsPurchaseSettleCode = ""
                     };
            var resultOut = from partsOutboundBill in partsOutBoundBills
                            join g in ObjectContext.PartsSalesOrders on partsOutboundBill.OriginalRequirementBillCode equals g.Code into tempTable1
                            from t3 in tempTable1
                            join partsSalesCategory in ObjectContext.PartsSalesCategories
                            on partsOutboundBill.PartsSalesCategoryId equals partsSalesCategory.Id
                            //join PartsPurchaseorder in ObjectContext.PartsPurchaseOrders
                            //on partsOutboundBill.CPPartsPurchaseOrderCode equals PartsPurchaseorder.CPPartsPurchaseOrderCode into JoinedEmpDept
                            //from PartsPurchaseorder in JoinedEmpDept.DefaultIfEmpty()
                            select new OutboundAndInboundBill {
                                BillId = partsOutboundBill.Id,
                                InvoiceType = t3.InvoiceType,
                                ERPOrderCode = t3.ERPSourceOrderCode,
                                InvoiceCustomerName = t3.InvoiceCustomerName,
                                Taxpayeridentification = t3.Taxpayeridentification,
                                BillType = (int)DcsPartsLogisticBatchBillDetailBillType.配件出库单,
                                BillCode = partsOutboundBill.Code,
                                OriginalRequirementBillType = partsOutboundBill.OriginalRequirementBillType,
                                OriginalRequirementBillId = partsOutboundBill.OriginalRequirementBillId,
                                //SettlementAmount = (from detail in ObjectContext.PartsOutboundBillDetails join pricing in ObjectContext.PartsPurchasePricings on detail.SparePartId equals pricing.PartId into JoinedEmpDept1 from pricing in JoinedEmpDept1.DefaultIfEmpty() where detail.PartsOutboundBillId == partsOutboundBill.Id select new { detail.PartsOutboundBillId, detail.OutboundAmount, pricing.PurchasePrice, pricing.PartsSalesCategoryId, pricing.PartsSupplierId, pricing.BranchId, pricing.ValidFrom, pricing.ValidTo }).Where(d => d.PartsOutboundBillId == partsOutboundBill.Id && d.PartsSalesCategoryId == partsOutboundBill.PartsSalesCategoryId && d.PartsSupplierId == PartsPurchaseorder.PartsSupplierId && d.BranchId == partsOutboundBill.BranchId && (d.ValidFrom <= partsOutboundBill.CreateTime && d.ValidTo >= partsOutboundBill.CreateTime)).Sum(v => v.PurchasePrice * v.OutboundAmount) > 0 ? (from detail in ObjectContext.PartsOutboundBillDetails join pricing in ObjectContext.PartsPurchasePricings on detail.SparePartId equals pricing.PartId into JoinedEmpDept1 from pricing in JoinedEmpDept1.DefaultIfEmpty() where detail.PartsOutboundBillId == partsOutboundBill.Id select new { detail.PartsOutboundBillId, detail.OutboundAmount, pricing.PurchasePrice, pricing.PartsSalesCategoryId, pricing.PartsSupplierId, pricing.BranchId, pricing.ValidFrom, pricing.ValidTo }).Where(d => d.PartsOutboundBillId == partsOutboundBill.Id && d.PartsSalesCategoryId == partsOutboundBill.PartsSalesCategoryId && d.PartsSupplierId == PartsPurchaseorder.PartsSupplierId && d.BranchId == partsOutboundBill.BranchId && (d.ValidFrom <= partsOutboundBill.CreateTime && d.ValidTo >= partsOutboundBill.CreateTime)).Sum(v => v.PurchasePrice * v.OutboundAmount) : ObjectContext.PartsOutboundBillDetails.Where(r => r.PartsOutboundBillId == partsOutboundBill.Id).Sum(v => v.SettlementPrice * v.OutboundAmount) ,
                                //SettlementAmount = partsOutboundBill.PartsPurchaseSettleCode ==null ? ObjectContext.PartsOutboundBillDetails.Where(r => r.PartsOutboundBillId == partsOutboundBill.Id).Sum(v => v.SettlementPrice * v.OutboundAmount):(from detail in ObjectContext.PartsOutboundBillDetails join pricing in ObjectContext.PartsPurchasePricings on detail.SparePartId equals pricing.PartId into JoinedEmpDept2 from pricing in JoinedEmpDept2.DefaultIfEmpty() where detail.PartsOutboundBillId==partsOutboundBill.Id  select new { detail.PartsOutboundBillId, detail.OutboundAmount, pricing.PurchasePrice, pricing.PartsSalesCategoryId, pricing.PartsSupplierId, pricing.BranchId, pricing.ValidFrom, pricing.ValidTo }).Where(d=>d.PartsOutboundBillId == partsOutboundBill.Id && d.PartsSalesCategoryId== partsOutboundBill.PartsSalesCategoryId && d.PartsSupplierId == PartsPurchaseorder.PartsSupplierId && d.BranchId == partsOutboundBill.BranchId && (d.ValidFrom<=DateTime.Now && d.ValidTo>= DateTime.Now)).Sum(v => v.PurchasePrice * v.OutboundAmount),
                                SettlementAmount = ObjectContext.PartsOutboundBillDetails.Where(r => r.PartsOutboundBillId == partsOutboundBill.Id).Sum(v => v.SettlementPrice * v.OutboundAmount),
                                CostAmount = ObjectContext.PartsOutboundBillDetails.Where(r => r.PartsOutboundBillId == partsOutboundBill.Id).Sum(v => v.CostPrice * v.OutboundAmount),
                                BillCreateTime = partsOutboundBill.CreateTime.Value,
                                CompanyId = partsOutboundBill.StorageCompanyId,
                                CounterpartCompanyId = partsOutboundBill.CounterpartCompanyId,
                                CounterpartCompanyCode = partsOutboundBill.CounterpartCompanyCode,
                                CounterpartCompanyName = partsOutboundBill.CounterpartCompanyName,
                                CustomerAccountId = partsOutboundBill.CustomerAccountId,
                                BillBusinessType = partsOutboundBill.OutboundType,
                                WarehouseId = partsOutboundBill.WarehouseId,
                                WarehouseName = partsOutboundBill.WarehouseName,
                                PartsSalesCategoryName = partsSalesCategory.Name,
                                PartsPurchaseSettleCode = partsOutboundBill.PartsPurchaseSettleCode
                            };
            result = result.Concat(resultOut).Distinct();
            return result.OrderBy(r => r.BillId);
        }


        public IQueryable<OutboundAndInboundBillWithDetail> 查询待销售结算出入库清单明细(int companyId, int destCompanyId, int? warehouseId, int? customerAccountId, int? partsSaleCategoryId, int[] inBoundType, int[] outBoundType, DateTime? endTime) {
            IQueryable<OutboundAndInboundBillWithDetail> result = null;
            if(outBoundType.Length == 0 && inBoundType.Length == 0) {
                throw new ValidationException(ErrorStrings.DataOfPartsInAndOut_Validation1);
            }

            var partsOutBoundBills = from r in ObjectContext.PartsOutboundBills
                                     where r.SettlementStatus == (int)DcsPartsSettlementStatus.待结算 && (warehouseId == null || r.WarehouseId == warehouseId) && r.StorageCompanyId == companyId && r.CounterpartCompanyId == destCompanyId && (customerAccountId == null || r.CustomerAccountId == customerAccountId) && outBoundType.Contains(r.OutboundType) && (endTime == null || r.CreateTime <= endTime) && (partsSaleCategoryId == null || r.PartsSalesCategoryId == partsSaleCategoryId)
                                     select r;
            var originalRequirementBillIds = partsOutBoundBills.Select(r => r.OriginalRequirementBillId);
            var partsSalesReturnBills = from a in ObjectContext.PartsSalesReturnBills
                                        join c in ObjectContext.PartsSalesReturnBillDetails on a.Id equals c.PartsSalesReturnBillId
                                        join b in originalRequirementBillIds on c.PartsSalesOrderId equals b
                                        where a.Status != (int)DcsPartsSalesReturnBillStatus.作废
                                        select a;
            var partsSalesReturnBillIds = partsSalesReturnBills.Select(r => r.Id);
            var partsInboundCheckBills = from a in ObjectContext.PartsInboundCheckBills
                                         from b in partsSalesReturnBillIds
                                         where a.OriginalRequirementBillId == b && a.SettlementStatus == (int)DcsPartsSettlementStatus.待结算 && (warehouseId == null || a.WarehouseId == warehouseId) && a.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件销售退货单

                                         select a;
            result = from partsInboundCheckBill in partsInboundCheckBills
                     join partsSalesReturnBill in ObjectContext.PartsSalesReturnBills
                     on partsInboundCheckBill.OriginalRequirementBillId equals partsSalesReturnBill.Id
                     join partsInboundCheckBillDetail in ObjectContext.PartsInboundCheckBillDetails
                     on partsInboundCheckBill.Id equals partsInboundCheckBillDetail.PartsInboundCheckBillId
                     join sparepart in ObjectContext.SpareParts.Where(r => string.IsNullOrEmpty(r.GoldenTaxClassifyCode))
                     on partsInboundCheckBillDetail.SparePartId equals sparepart.Id
                     join e in ObjectContext.PartsSalesReturnBillDetails on partsSalesReturnBill.Id equals e.PartsSalesReturnBillId into temp1
                     from t1 in temp1.DefaultIfEmpty()
                     join g in ObjectContext.PartsSalesOrders on t1.PartsSalesOrderId equals g.Id into tempTable1
                     from t3 in tempTable1
                     select new OutboundAndInboundBillWithDetail {
                         BillId = partsInboundCheckBill.Id,
                         BillCode = partsInboundCheckBill.Code,
                         SparePartCode = sparepart.Code,
                         SparePartName = sparepart.Name
                     };
            var resultOut = from partsOutboundBill in partsOutBoundBills
                            join partsOutboundBillDetail in ObjectContext.PartsOutboundBillDetails
                            on partsOutboundBill.Id equals partsOutboundBillDetail.PartsOutboundBillId
                            join sparepart in ObjectContext.SpareParts.Where(r => string.IsNullOrEmpty(r.GoldenTaxClassifyCode))
                            on partsOutboundBillDetail.SparePartId equals sparepart.Id
                            join g in ObjectContext.PartsSalesOrders on partsOutboundBill.OriginalRequirementBillCode equals g.Code into tempTable1
                            from t3 in tempTable1
                            select new OutboundAndInboundBillWithDetail {
                                BillId = partsOutboundBill.Id,
                                BillCode = partsOutboundBill.Code,
                                SparePartCode = sparepart.Code,
                                SparePartName = sparepart.Name
                            };
            result = result.Concat(resultOut).Distinct();
            return result.OrderBy(r => r.BillId);
        }

        public IQueryable<OutboundAndInboundBillWithDetail> 查询待销售结算出入库清单明细采埃孚(int companyId, int destCompanyId, int? warehouseId, int? customerAccountId, int? partsSaleCategoryId, int[] inBoundType, int[] outBoundType, DateTime? endTime) {
            IQueryable<OutboundAndInboundBillWithDetail> result = null;
            if(outBoundType.Length == 0 && inBoundType.Length == 0) {
                throw new ValidationException(ErrorStrings.DataOfPartsInAndOut_Validation1);
            }

            var partsOutBoundBills = from r in ObjectContext.PartsOutboundBills
                                     where r.SettlementStatus == (int)DcsPartsSettlementStatus.待结算 && (warehouseId == null || r.WarehouseId == warehouseId) && r.StorageCompanyId == companyId && r.CounterpartCompanyId == destCompanyId && (customerAccountId == null || r.CustomerAccountId == customerAccountId) && outBoundType.Contains(r.OutboundType) && (endTime == null || r.CreateTime <= endTime) && (partsSaleCategoryId == null || r.PartsSalesCategoryId == partsSaleCategoryId) && r.CPPartsInboundCheckCode != null
                                     select r;
            var originalRequirementBillIds = partsOutBoundBills.Select(r => r.OriginalRequirementBillId);
            var partsSalesReturnBills = from a in ObjectContext.PartsSalesReturnBills
                                        join c in ObjectContext.PartsSalesReturnBillDetails on a.Id equals c.PartsSalesReturnBillId
                                        join b in originalRequirementBillIds on c.PartsSalesOrderId equals b
                                        where a.Status != (int)DcsPartsSalesReturnBillStatus.作废
                                        select a;
            var partsSalesReturnBillIds = partsSalesReturnBills.Select(r => r.Id);
            var partsInboundCheckBills = from a in ObjectContext.PartsInboundCheckBills
                                         from b in partsSalesReturnBillIds
                                         where a.OriginalRequirementBillId == b && a.SettlementStatus == (int)DcsPartsSettlementStatus.待结算 && (warehouseId == null || a.WarehouseId == warehouseId) && a.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件销售退货单

                                         select a;
            result = from partsInboundCheckBill in partsInboundCheckBills
                     join partsSalesReturnBill in ObjectContext.PartsSalesReturnBills
                     on partsInboundCheckBill.OriginalRequirementBillId equals partsSalesReturnBill.Id
                     join partsInboundCheckBillDetail in ObjectContext.PartsInboundCheckBillDetails
                     on partsInboundCheckBill.Id equals partsInboundCheckBillDetail.PartsInboundCheckBillId
                     join sparepart in ObjectContext.SpareParts.Where(r => string.IsNullOrEmpty(r.GoldenTaxClassifyCode))
                     on partsInboundCheckBillDetail.SparePartId equals sparepart.Id
                     join e in ObjectContext.PartsSalesReturnBillDetails on partsSalesReturnBill.Id equals e.PartsSalesReturnBillId into temp1
                     from t1 in temp1.DefaultIfEmpty()
                     join g in ObjectContext.PartsSalesOrders on t1.PartsSalesOrderId equals g.Id into tempTable1
                     from t3 in tempTable1
                     select new OutboundAndInboundBillWithDetail {
                         BillId = partsInboundCheckBill.Id,
                         BillCode = partsInboundCheckBill.Code,
                         SparePartCode = sparepart.Code,
                         SparePartName = sparepart.Name
                     };
            var resultOut = from partsOutboundBill in partsOutBoundBills
                            join partsOutboundBillDetail in ObjectContext.PartsOutboundBillDetails
                            on partsOutboundBill.Id equals partsOutboundBillDetail.PartsOutboundBillId
                            join sparepart in ObjectContext.SpareParts.Where(r => string.IsNullOrEmpty(r.GoldenTaxClassifyCode))
                            on partsOutboundBillDetail.SparePartId equals sparepart.Id
                            join g in ObjectContext.PartsSalesOrders on partsOutboundBill.OriginalRequirementBillCode equals g.Code into tempTable1
                            from t3 in tempTable1
                            select new OutboundAndInboundBillWithDetail {
                                BillId = partsOutboundBill.Id,
                                BillCode = partsOutboundBill.Code,
                                SparePartCode = sparepart.Code,
                                SparePartName = sparepart.Name
                            };
            result = result.Concat(resultOut).Distinct();
            return result.OrderBy(r => r.BillId);
        }
        public IQueryable<OutboundAndInboundBill> 查询待销售退货结算入库明细(int storageCompanyId, int counterpartCompanyId, int? warehouseId, int? customerAccountId, int? partsSaleCategoryId, int[] inboundTypes, DateTime? endTime, int? settleType, int? businessType) {
            // 1、企业Id，对方单位Id，不能为空2、数组[入库类型]不能为空
            DateTime? EndDateTime = new DateTime(endTime.Value.Year, endTime.Value.Month, endTime.Value.Day, 23, 59, 59);
            if(inboundTypes.Length == 0 || inboundTypes == null) {
                throw new ValidationException(ErrorStrings.DataOfPartsInAndOut_Validation2);
            }
            IQueryable<OutboundAndInboundBill> result = null;
            if(inboundTypes.Length > 0) {
                var partsInboundCheckBills = ObjectContext.PartsInboundCheckBills.Where(r => r.SettlementStatus == (int)DcsPartsSettlementStatus.待结算 && r.StorageCompanyId == storageCompanyId && r.CounterpartCompanyId == counterpartCompanyId);
                if(warehouseId.HasValue)
                    partsInboundCheckBills = partsInboundCheckBills.Where(r => r.WarehouseId == warehouseId.Value);
                if(customerAccountId.HasValue)
                    partsInboundCheckBills = partsInboundCheckBills.Where(r => r.CustomerAccountId == customerAccountId.Value);
                if(endTime.HasValue)
                    // partsInboundCheckBills = partsInboundCheckBills.Where(r => r.CreateTime <= endTime.Value); new DateTime(endTime.Value.Year, endTime.Value.Month, endTime.Value.Day, 23, 59, 59);
                    partsInboundCheckBills = partsInboundCheckBills.Where(r => r.CreateTime <= EndDateTime.Value);
                if(partsSaleCategoryId.HasValue)
                    partsInboundCheckBills = partsInboundCheckBills.Where(r => r.PartsSalesCategoryId == partsSaleCategoryId);
                partsInboundCheckBills = partsInboundCheckBills.Where(r => inboundTypes.Contains(r.InboundType));

                result = from partsInboundCheckBill in partsInboundCheckBills
                         join c in ObjectContext.PartsSalesReturnBills on partsInboundCheckBill.OriginalRequirementBillId equals c.Id
                         join e in ObjectContext.PartsSalesReturnBillDetails on c.Id equals e.PartsSalesReturnBillId
                         join d in ObjectContext.PartsSalesOrders on e.PartsSalesOrderId equals d.Id
                         join s in ObjectContext.PartsSalesOrderTypes on d.PartsSalesOrderTypeId equals s.Id
                         join partsSalesReturnBill in ObjectContext.PartsSalesReturnBills on partsInboundCheckBill.OriginalRequirementBillId equals partsSalesReturnBill.Id into temp
                         from item in temp.DefaultIfEmpty()
                         where (settleType == null || s.SettleType == settleType) && (businessType == null || s.BusinessType == businessType)
                         join partsClaimOrderNew in ObjectContext.PartsClaimOrderNews on new {
                             partsClaimOrderNewId = partsInboundCheckBill.OriginalRequirementBillId,
                             partsClaimOrderNewType = partsInboundCheckBill.OriginalRequirementBillType
                         } equals new {
                             partsClaimOrderNewId = partsClaimOrderNew.Id,
                             partsClaimOrderNewType = 14
                         } into temp2
                         from item2 in temp2.DefaultIfEmpty()
                         select new OutboundAndInboundBill {
                             BillId = partsInboundCheckBill.Id,
                             BillType = (int)DcsPartsLogisticBatchBillDetailBillType.配件入库检验单,
                             BillCode = partsInboundCheckBill.Code,
                             SettlementAmount = ObjectContext.PartsInboundCheckBillDetails.Where(r => r.PartsInboundCheckBillId == partsInboundCheckBill.Id).Sum(v => v.SettlementPrice * v.InspectedQuantity),
                             CostAmount = ObjectContext.PartsInboundCheckBillDetails.Where(r => r.PartsInboundCheckBillId == partsInboundCheckBill.Id).Sum(v => v.CostPrice * v.InspectedQuantity),
                             BillCreateTime = partsInboundCheckBill.CreateTime.Value,
                             CompanyId = partsInboundCheckBill.StorageCompanyId,
                             CounterpartCompanyId = partsInboundCheckBill.CounterpartCompanyId,
                             CustomerAccountId = partsInboundCheckBill.CustomerAccountId,
                             BillBusinessType = partsInboundCheckBill.InboundType,
                             WarehouseId = partsInboundCheckBill.WarehouseId,
                             WarehouseName = partsInboundCheckBill.WarehouseName,
                             ReturnType = partsInboundCheckBill.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件索赔单 ? 0 : item.ReturnType,
                             ReturnReason = partsInboundCheckBill.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件索赔单 ? item2.ReturnReason : item.ReturnReason,
                             PartsSalesReturnBillRemark = item.Remark
                         };

                //result = result.Union(resultNotWithPartsSalesReturnBill);
            }
            return result.OrderBy(r => r.BillId);
        }

        public IQueryable<VietualPartsSalesRtnSettlementRef> GetPartsSalesRtnSettlementRefDetail(int partsSalesRtnSettlementId) {
            var result = from c in this.ObjectContext.PartsSalesRtnSettlementRefs.Where(r => r.PartsSalesRtnSettlementId == partsSalesRtnSettlementId)
                         join b in this.ObjectContext.PartsInboundCheckBills on c.SourceId equals b.Id
                         join d in ObjectContext.PartsSalesReturnBills on b.OriginalRequirementBillId equals d.Id
                         select new VietualPartsSalesRtnSettlementRef {
                             Id = c.Id,
                             SourceCode = c.SourceCode,
                             SettlementAmount = c.SettlementAmount,
                             SourceBillCreateTime = c.SourceBillCreateTime,
                             WarehouseName = c.WarehouseName,
                             ReturnReason = d.ReturnReason,
                             PartsSalesRtnSettlementId = c.PartsSalesRtnSettlementId,
                             PartsSalesReturnBillRemark = d.Remark
                         };
            return result.OrderBy(r => r.Id);
        }

        public IQueryable<OutboundAndInboundBill> 查询待采购结算出入库明细(int storageCompanyId, int counterpartCompanyId, int? warehouseId, int? customerAccountId, int? partsSaleCategoryId, int[] inboundTypes, int[] outboundTypes, DateTime? endTime) {
            //1、企业Id，对方单位Id，不能为空 2、数组[入库类型]，数组[出库类型] 不能同时为空
            IQueryable<OutboundAndInboundBill> result = null;
            if(inboundTypes.Length == 0 && outboundTypes.Length == 0) {
                throw new ValidationException(ErrorStrings.DataOfPartsInAndOut_Validation1);
            }

            var partsInboundCheckBills = ObjectContext.PartsInboundCheckBills.Where(r => r.SettlementStatus == (int)DcsPartsSettlementStatus.待结算 && r.StorageCompanyId == storageCompanyId && r.CounterpartCompanyId == counterpartCompanyId);
            var partsOverOutboundBills = ObjectContext.PartsOutboundBills.Where(r => r.SettlementStatus == (int)DcsPartsSettlementStatus.待结算 && r.StorageCompanyId == storageCompanyId && r.CounterpartCompanyId == counterpartCompanyId).Distinct();
            if(warehouseId.HasValue) {
                partsInboundCheckBills = partsInboundCheckBills.Where(r => r.WarehouseId == warehouseId.Value);
                partsOverOutboundBills = partsOverOutboundBills.Where(r => r.WarehouseId == warehouseId.Value);
            }
            if(customerAccountId.HasValue) {
                partsInboundCheckBills = partsInboundCheckBills.Where(r => r.CustomerAccountId == customerAccountId.Value);
                partsOverOutboundBills = partsOverOutboundBills.Where(r => r.CustomerAccountId == customerAccountId.Value);
            }
            if(endTime.HasValue) {
                partsInboundCheckBills = partsInboundCheckBills.Where(r => r.CreateTime <= endTime.Value);
                partsOverOutboundBills = partsOverOutboundBills.Where(r => r.CreateTime <= endTime.Value);
            }
            if(partsSaleCategoryId.HasValue) {
                partsInboundCheckBills = partsInboundCheckBills.Where(r => r.PartsSalesCategoryId == partsSaleCategoryId);
                partsOverOutboundBills = partsOverOutboundBills.Where(r => r.PartsSalesCategoryId == partsSaleCategoryId);
            }
            if(inboundTypes.Length > 0) {
                partsInboundCheckBills = partsInboundCheckBills.Where(r => inboundTypes.Contains(r.InboundType));
            }
            var originalRequirementBillIds = partsInboundCheckBills.Select(r => r.OriginalRequirementBillId);
            var partsPurReturnOrders = from a in ObjectContext.PartsPurReturnOrders
                                       from b in originalRequirementBillIds
                                       where b == a.PartsPurchaseOrderId && a.Status != (int)DcsWorkflowOfSimpleApprovalStatus.作废
                                       select a;
            var partsPurReturnOrderIds = partsPurReturnOrders.Select(r => r.Id);
            var partsOutBoundBills = from a in ObjectContext.PartsOutboundBills.Distinct()
                                     from b in partsPurReturnOrderIds
                                     where a.OriginalRequirementBillId == b && a.SettlementStatus == (int)DcsPartsSettlementStatus.待结算 && a.CounterpartCompanyId == counterpartCompanyId && a.StorageCompanyId == storageCompanyId
                                     select a;
            result = from partsInboundCheckBill in partsInboundCheckBills
                     select new OutboundAndInboundBill {
                         BillId = partsInboundCheckBill.Id,
                         BillType = (int)DcsPartsLogisticBatchBillDetailBillType.配件入库检验单,
                         BillCode = partsInboundCheckBill.Code,
                         SettlementAmount = ObjectContext.PartsInboundCheckBillDetails.Where(r => r.PartsInboundCheckBillId == partsInboundCheckBill.Id).Sum(v => v.SettlementPrice * v.InspectedQuantity),
                         CostAmount = ObjectContext.PartsInboundCheckBillDetails.Where(r => r.PartsInboundCheckBillId == partsInboundCheckBill.Id).Sum(v => v.CostPrice * v.InspectedQuantity),
                         BillCreateTime = partsInboundCheckBill.CreateTime.Value,
                         CompanyId = partsInboundCheckBill.StorageCompanyId,
                         CounterpartCompanyId = partsInboundCheckBill.CounterpartCompanyId,
                         CustomerAccountId = partsInboundCheckBill.CustomerAccountId,
                         BillBusinessType = partsInboundCheckBill.InboundType,
                         WarehouseId = partsInboundCheckBill.WarehouseId,
                         WarehouseName = partsInboundCheckBill.WarehouseName,
                         ReturnReason = "",
                         CPPartsPurchaseOrderCode = partsInboundCheckBill.CPPartsPurchaseOrderCode,
                         CPPartsInboundCheckCode = partsInboundCheckBill.CPPartsInboundCheckCode
                     };

            var resultOut = from partsOutboundBill in partsOutBoundBills.Distinct()
                            select new OutboundAndInboundBill {
                                BillId = partsOutboundBill.Id,
                                BillType = (int)DcsPartsLogisticBatchBillDetailBillType.配件出库单,
                                BillCode = partsOutboundBill.Code,
                                SettlementAmount = ObjectContext.PartsOutboundBillDetails.Where(r => r.PartsOutboundBillId == partsOutboundBill.Id).Sum(v => v.SettlementPrice * v.OutboundAmount),
                                CostAmount = ObjectContext.PartsOutboundBillDetails.Where(r => r.PartsOutboundBillId == partsOutboundBill.Id).Sum(v => -(v.CostPrice * v.OutboundAmount)),
                                BillCreateTime = partsOutboundBill.CreateTime.Value,
                                CompanyId = partsOutboundBill.StorageCompanyId,
                                CounterpartCompanyId = partsOutboundBill.CounterpartCompanyId,
                                CustomerAccountId = partsOutboundBill.CustomerAccountId,
                                BillBusinessType = partsOutboundBill.OutboundType,
                                WarehouseId = partsOutboundBill.WarehouseId,
                                WarehouseName = partsOutboundBill.WarehouseName,

                                ReturnReason = "质量件退货",
                                CPPartsPurchaseOrderCode = partsOutboundBill.CPPartsPurchaseOrderCode,
                                CPPartsInboundCheckCode = partsOutboundBill.CPPartsInboundCheckCode
                            };
            //var resultKeepOut = from partsOutboundBill in partsOverOutboundBills
            //                    join purRtn in ObjectContext.PartsPurReturnOrders on
            //                    new {
            //                        oriId = partsOutboundBill.OriginalRequirementBillId,
            //                        compId = partsOutboundBill.CounterpartCompanyId
            //                    } equals new {
            //                        oriId = purRtn.Id,
            //                        compId = purRtn.PartsSupplierId
            //                    }
            //                    where purRtn.ReturnReason == (int)DcsPartsPurReturnOrderReturnReason.积压件退货 && partsOutboundBill.OutboundType == (int)DcsPartsOutboundType.采购退货
            //                    select new OutboundAndInboundBill {
            //                        BillId = partsOutboundBill.Id,
            //                        BillType = (int)DcsPartsLogisticBatchBillDetailBillType.配件出库单,
            //                        BillCode = partsOutboundBill.Code,
            //                        SettlementAmount = ObjectContext.PartsOutboundBillDetails.Where(r => r.PartsOutboundBillId == partsOutboundBill.Id).Sum(v => v.SettlementPrice * v.OutboundAmount),
            //                        CostAmount = ObjectContext.PartsOutboundBillDetails.Where(r => r.PartsOutboundBillId == partsOutboundBill.Id).Sum(v => v.CostPrice * v.OutboundAmount),
            //                        BillCreateTime = partsOutboundBill.CreateTime.Value,
            //                        CompanyId = partsOutboundBill.StorageCompanyId,
            //                        CounterpartCompanyId = partsOutboundBill.CounterpartCompanyId,
            //                        CustomerAccountId = partsOutboundBill.CustomerAccountId,
            //                        BillBusinessType = partsOutboundBill.OutboundType,
            //                        WarehouseId = partsOutboundBill.WarehouseId,
            //                        WarehouseName = partsOutboundBill.WarehouseName,

            //                        ReturnReason = "积压件退货",
            //                        CPPartsPurchaseOrderCode = partsOutboundBill.CPPartsPurchaseOrderCode,
            //                        CPPartsInboundCheckCode = partsOutboundBill.CPPartsInboundCheckCode
            //                    };
            return result.Concat(resultOut)/*.Concat(resultKeepOut)*/.OrderBy(r => r.BillId);
        }

        public IQueryable<OutboundAndInboundBill> 查询待采购结算出入库明细采埃孚(int storageCompanyId, int counterpartCompanyId, int? warehouseId, int? customerAccountId, int? partsSaleCategoryId, int[] inboundTypes, int[] outboundTypes, DateTime? endTime) {
            //1、企业Id，对方单位Id，不能为空 2、数组[入库类型]，数组[出库类型] 不能同时为空
            IQueryable<OutboundAndInboundBill> result = null;
            if(inboundTypes.Length == 0 && outboundTypes.Length == 0) {
                throw new ValidationException(ErrorStrings.DataOfPartsInAndOut_Validation1);
            }

            var partsInboundCheckBills = ObjectContext.PartsInboundCheckBills.Where(r => r.SettlementStatus == (int)DcsPartsSettlementStatus.待结算 && r.StorageCompanyId == storageCompanyId && r.CounterpartCompanyId == counterpartCompanyId && r.CPPartsInboundCheckCode != null);
            var partsOverOutboundBills = ObjectContext.PartsOutboundBills.Where(r => r.SettlementStatus == (int)DcsPartsSettlementStatus.待结算 && r.StorageCompanyId == storageCompanyId && r.CounterpartCompanyId == counterpartCompanyId).Distinct();
            if(warehouseId.HasValue) {
                partsInboundCheckBills = partsInboundCheckBills.Where(r => r.WarehouseId == warehouseId.Value);
                partsOverOutboundBills = partsOverOutboundBills.Where(r => r.WarehouseId == warehouseId.Value);
            }
            if(customerAccountId.HasValue) {
                partsInboundCheckBills = partsInboundCheckBills.Where(r => r.CustomerAccountId == customerAccountId.Value);
                partsOverOutboundBills = partsOverOutboundBills.Where(r => r.CustomerAccountId == customerAccountId.Value);
            }
            if(endTime.HasValue) {
                partsInboundCheckBills = partsInboundCheckBills.Where(r => r.CreateTime <= endTime.Value);
                partsOverOutboundBills = partsOverOutboundBills.Where(r => r.CreateTime <= endTime.Value);
            }
            if(partsSaleCategoryId.HasValue) {
                partsInboundCheckBills = partsInboundCheckBills.Where(r => r.PartsSalesCategoryId == partsSaleCategoryId);
                partsOverOutboundBills = partsOverOutboundBills.Where(r => r.PartsSalesCategoryId == partsSaleCategoryId);
            }
            if(inboundTypes.Length > 0) {
                partsInboundCheckBills = partsInboundCheckBills.Where(r => inboundTypes.Contains(r.InboundType));
            }
            var originalRequirementBillIds = partsInboundCheckBills.Select(r => r.OriginalRequirementBillId);
            var partsPurReturnOrders = from a in ObjectContext.PartsPurReturnOrders
                                       from b in originalRequirementBillIds
                                       where b == a.PartsPurchaseOrderId && a.Status != (int)DcsWorkflowOfSimpleApprovalStatus.作废
                                       select a;
            var partsPurReturnOrderIds = partsPurReturnOrders.Select(r => r.Id);
            var partsOutBoundBills = from a in ObjectContext.PartsOutboundBills.Distinct()
                                     from b in partsPurReturnOrderIds
                                     where a.OriginalRequirementBillId == b && a.SettlementStatus == (int)DcsPartsSettlementStatus.待结算 && a.CounterpartCompanyId == counterpartCompanyId && a.StorageCompanyId == storageCompanyId
                                     select a;
            result = from partsInboundCheckBill in partsInboundCheckBills
                     select new OutboundAndInboundBill {
                         BillId = partsInboundCheckBill.Id,
                         BillType = (int)DcsPartsLogisticBatchBillDetailBillType.配件入库检验单,
                         BillCode = partsInboundCheckBill.Code,
                         SettlementAmount = ObjectContext.PartsInboundCheckBillDetails.Where(r => r.PartsInboundCheckBillId == partsInboundCheckBill.Id).Sum(v => v.SettlementPrice * v.InspectedQuantity),
                         CostAmount = ObjectContext.PartsInboundCheckBillDetails.Where(r => r.PartsInboundCheckBillId == partsInboundCheckBill.Id).Sum(v => v.CostPrice * v.InspectedQuantity),
                         BillCreateTime = partsInboundCheckBill.CreateTime.Value,
                         CompanyId = partsInboundCheckBill.StorageCompanyId,
                         CounterpartCompanyId = partsInboundCheckBill.CounterpartCompanyId,
                         CustomerAccountId = partsInboundCheckBill.CustomerAccountId,
                         BillBusinessType = partsInboundCheckBill.InboundType,
                         WarehouseId = partsInboundCheckBill.WarehouseId,
                         WarehouseName = partsInboundCheckBill.WarehouseName,
                         ReturnReason = "",
                         CPPartsInboundCheckCode = partsInboundCheckBill.CPPartsInboundCheckCode,
                         CPPartsPurchaseOrderCode = partsInboundCheckBill.CPPartsPurchaseOrderCode
                     };

            var resultOut = from partsOutboundBill in partsOutBoundBills.Distinct()
                            select new OutboundAndInboundBill {
                                BillId = partsOutboundBill.Id,
                                BillType = (int)DcsPartsLogisticBatchBillDetailBillType.配件出库单,
                                BillCode = partsOutboundBill.Code,
                                SettlementAmount = ObjectContext.PartsOutboundBillDetails.Where(r => r.PartsOutboundBillId == partsOutboundBill.Id).Sum(v => v.SettlementPrice * v.OutboundAmount),
                                CostAmount = ObjectContext.PartsOutboundBillDetails.Where(r => r.PartsOutboundBillId == partsOutboundBill.Id).Sum(v => -(v.CostPrice * v.OutboundAmount)),
                                BillCreateTime = partsOutboundBill.CreateTime.Value,
                                CompanyId = partsOutboundBill.StorageCompanyId,
                                CounterpartCompanyId = partsOutboundBill.CounterpartCompanyId,
                                CustomerAccountId = partsOutboundBill.CustomerAccountId,
                                BillBusinessType = partsOutboundBill.OutboundType,
                                WarehouseId = partsOutboundBill.WarehouseId,
                                WarehouseName = partsOutboundBill.WarehouseName,
                                ReturnReason = "质量件退货",
                                CPPartsInboundCheckCode = partsOutboundBill.CPPartsInboundCheckCode,
                                CPPartsPurchaseOrderCode = partsOutboundBill.CPPartsPurchaseOrderCode
                            };
            //var resultKeepOut = from partsOutboundBill in partsOverOutboundBills
            //                    join purRtn in ObjectContext.PartsPurReturnOrders on
            //                    new
            //                    {
            //                        oriId = partsOutboundBill.OriginalRequirementBillId,
            //                        compId = partsOutboundBill.CounterpartCompanyId
            //                    } equals new
            //                    {
            //                        oriId = purRtn.Id,
            //                        compId = purRtn.PartsSupplierId
            //                    }
            //                    where purRtn.ReturnReason == (int)DcsPartsPurReturnOrderReturnReason.积压件退货 && partsOutboundBill.OutboundType == (int)DcsPartsOutboundType.采购退货
            //                    select new OutboundAndInboundBill
            //                    {
            //                        BillId = partsOutboundBill.Id,
            //                        BillType = (int)DcsPartsLogisticBatchBillDetailBillType.配件出库单,
            //                        BillCode = partsOutboundBill.Code,
            //                        SettlementAmount = ObjectContext.PartsOutboundBillDetails.Where(r => r.PartsOutboundBillId == partsOutboundBill.Id).Sum(v => v.SettlementPrice * v.OutboundAmount),
            //                        CostAmount = ObjectContext.PartsOutboundBillDetails.Where(r => r.PartsOutboundBillId == partsOutboundBill.Id).Sum(v => v.CostPrice * v.OutboundAmount),
            //                        BillCreateTime = partsOutboundBill.CreateTime.Value,
            //                        CompanyId = partsOutboundBill.StorageCompanyId,
            //                        CounterpartCompanyId = partsOutboundBill.CounterpartCompanyId,
            //                        CustomerAccountId = partsOutboundBill.CustomerAccountId,
            //                        BillBusinessType = partsOutboundBill.OutboundType,
            //                        WarehouseId = partsOutboundBill.WarehouseId,
            //                        WarehouseName = partsOutboundBill.WarehouseName,
            //                        ReturnReason = "积压件退货",
            //                        CPPartsInboundCheckCode = partsOutboundBill.CPPartsInboundCheckCode,
            //                        CPPartsPurchaseOrderCode = partsOutboundBill.CPPartsPurchaseOrderCode
            //                    };
            return result.Concat(resultOut)/*.Concat(resultKeepOut)*/.OrderBy(r => r.BillId);
        }

        //采购结算管理-新增/修改 查询清单（过滤原始入库单编号）
        public IQueryable<OutboundAndInboundBill> 查询待采购结算出入库明细过滤原始入库单编号(int storageCompanyId, int counterpartCompanyId, int? warehouseId, int? customerAccountId, int? partsSaleCategoryId, int[] inboundTypes, int[] outboundTypes, DateTime? endTime, string invoiceNumber) {
            //1、企业Id，对方单位Id，不能为空 2、数组[入库类型]，数组[出库类型] 不能同时为空
            IQueryable<OutboundAndInboundBill> result = null;
            if(inboundTypes.Length == 0 && outboundTypes.Length == 0) {
                throw new ValidationException(ErrorStrings.DataOfPartsInAndOut_Validation1);
            }
            var partsInboundCheckBills = ObjectContext.PartsInboundCheckBills.Where(r => r.CPPartsInboundCheckCode == null && r.Status != (int)DcsPartsInboundCheckBillStatus.取消 && r.SettlementStatus == (int)DcsPartsSettlementStatus.待结算 && r.StorageCompanyId == storageCompanyId && r.CounterpartCompanyId == counterpartCompanyId && ObjectContext.PartsInboundPlans.Any(pi => r.PartsInboundPlanId == pi.Id) && (!ObjectContext.PartsDifferenceBackBills.Any(t => ObjectContext.PackingTasks.Any(y => y.Code == t.SourceCode && y.PartsInboundCheckBillCode == r.Code) && t.Type == (int)DCSPartsDifferenceBackBillType.包装) || !ObjectContext.PartsDifferenceBackBills.Any(t => ObjectContext.PackingTasks.Any(y => y.Code == t.SourceCode && y.PartsInboundCheckBillCode == r.Code) && t.Type == (int)DCSPartsDifferenceBackBillType.包装 && (t.Status == (int)DCSPartsDifferenceBackBillStatus.生效 || t.Status == (int)DCSPartsDifferenceBackBillStatus.作废)))); ;
            if("" != invoiceNumber) {
                partsInboundCheckBills = ObjectContext.PartsInboundCheckBills.Where(r => r.CPPartsInboundCheckCode == null && r.Status != (int)DcsPartsInboundCheckBillStatus.取消 && r.SettlementStatus == (int)DcsPartsSettlementStatus.待结算 && r.StorageCompanyId == storageCompanyId && r.CounterpartCompanyId == counterpartCompanyId && ObjectContext.PartsInboundPlans.Any(pi => r.PartsInboundPlanId == pi.Id && ObjectContext.SAP_YX_InvoiceverAccountInfo.Any(ia => ia.Code == pi.ERPSourceOrderCode && (invoiceNumber == "" || ia.InvoiceNumber == invoiceNumber))) && (!ObjectContext.PartsDifferenceBackBills.Any(t => ObjectContext.PackingTasks.Any(y => y.Code == t.SourceCode && y.PartsInboundCheckBillCode == r.Code) && t.Type == (int)DCSPartsDifferenceBackBillType.包装) || !ObjectContext.PartsDifferenceBackBills.Any(t => ObjectContext.PackingTasks.Any(y => y.Code == t.SourceCode && y.PartsInboundCheckBillCode == r.Code) && t.Type == (int)DCSPartsDifferenceBackBillType.包装 && (t.Status == (int)DCSPartsDifferenceBackBillStatus.生效 || t.Status == (int)DCSPartsDifferenceBackBillStatus.作废))));
            }

            var partsOverOutboundBills = ObjectContext.PartsOutboundBills.Where(r => r.CPPartsInboundCheckCode == null && r.SettlementStatus == (int)DcsPartsSettlementStatus.待结算 && r.StorageCompanyId == storageCompanyId && r.CounterpartCompanyId == counterpartCompanyId).Distinct();
            if(warehouseId.HasValue) {
                partsInboundCheckBills = partsInboundCheckBills.Where(r => r.WarehouseId == warehouseId.Value);
                partsOverOutboundBills = partsOverOutboundBills.Where(r => r.WarehouseId == warehouseId.Value);
            }
            if(customerAccountId.HasValue) {
                partsInboundCheckBills = partsInboundCheckBills.Where(r => r.CustomerAccountId == customerAccountId.Value);
                partsOverOutboundBills = partsOverOutboundBills.Where(r => r.CustomerAccountId == customerAccountId.Value);
            }
            if(endTime.HasValue) {
                partsInboundCheckBills = partsInboundCheckBills.Where(r => r.CreateTime <= endTime.Value);
                partsOverOutboundBills = partsOverOutboundBills.Where(r => r.CreateTime <= endTime.Value);
            }
            if(partsSaleCategoryId.HasValue) {
                partsInboundCheckBills = partsInboundCheckBills.Where(r => r.PartsSalesCategoryId == partsSaleCategoryId);
                partsOverOutboundBills = partsOverOutboundBills.Where(r => r.PartsSalesCategoryId == partsSaleCategoryId);
            }
            if(inboundTypes.Length > 0) {
                partsInboundCheckBills = partsInboundCheckBills.Where(r => inboundTypes.Contains(r.InboundType));
            }
            var originalRequirementBillIds = partsInboundCheckBills.Select(r => r.OriginalRequirementBillId);
            var partsPurReturnOrders = from a in ObjectContext.PartsPurReturnOrders
                                       from b in originalRequirementBillIds
                                       where b == a.PartsPurchaseOrderId && a.Status != (int)DcsWorkflowOfSimpleApprovalStatus.作废
                                       select a;
            var partsPurReturnOrderIds = partsPurReturnOrders.Select(r => r.Id);
            var partsOutBoundBills = from a in ObjectContext.PartsOutboundBills.Distinct()
                                     from b in partsPurReturnOrderIds
                                     where a.OriginalRequirementBillId == b && a.SettlementStatus == (int)DcsPartsSettlementStatus.待结算 && a.CounterpartCompanyId == counterpartCompanyId && a.StorageCompanyId == storageCompanyId
                                     select a;
            result = from partsInboundCheckBill in partsInboundCheckBills
                     select new OutboundAndInboundBill {
                         BillId = partsInboundCheckBill.Id,
                         BillType = (int)DcsPartsLogisticBatchBillDetailBillType.配件入库检验单,
                         BillCode = partsInboundCheckBill.Code,
                         SettlementAmount = ObjectContext.PartsInboundCheckBillDetails.Where(r => r.PartsInboundCheckBillId == partsInboundCheckBill.Id).Sum(v => v.SettlementPrice * v.InspectedQuantity),
                         CostAmount = ObjectContext.PartsInboundCheckBillDetails.Where(r => r.PartsInboundCheckBillId == partsInboundCheckBill.Id).Sum(v => v.CostPrice * v.InspectedQuantity),
                         BillCreateTime = partsInboundCheckBill.CreateTime.Value,
                         CompanyId = partsInboundCheckBill.StorageCompanyId,
                         CounterpartCompanyId = partsInboundCheckBill.CounterpartCompanyId,
                         CustomerAccountId = partsInboundCheckBill.CustomerAccountId,
                         BillBusinessType = partsInboundCheckBill.InboundType,
                         WarehouseId = partsInboundCheckBill.WarehouseId,
                         WarehouseName = partsInboundCheckBill.WarehouseName,
                         ReturnReason = "",
                         CPPartsPurchaseOrderCode = partsInboundCheckBill.CPPartsPurchaseOrderCode,
                         CPPartsInboundCheckCode = partsInboundCheckBill.CPPartsInboundCheckCode,
                         OriginalRequirementBillCode = partsInboundCheckBill.OriginalRequirementBillCode
                     };

            var resultOut = from partsOutboundBill in partsOutBoundBills.Distinct()
                            select new OutboundAndInboundBill {
                                BillId = partsOutboundBill.Id,
                                BillType = (int)DcsPartsLogisticBatchBillDetailBillType.配件出库单,
                                BillCode = partsOutboundBill.Code,
                                SettlementAmount = ObjectContext.PartsOutboundBillDetails.Where(r => r.PartsOutboundBillId == partsOutboundBill.Id).Sum(v => v.SettlementPrice * v.OutboundAmount),
                                CostAmount = ObjectContext.PartsOutboundBillDetails.Where(r => r.PartsOutboundBillId == partsOutboundBill.Id).Sum(v => -(v.CostPrice * v.OutboundAmount)),
                                BillCreateTime = partsOutboundBill.CreateTime.Value,
                                CompanyId = partsOutboundBill.StorageCompanyId,
                                CounterpartCompanyId = partsOutboundBill.CounterpartCompanyId,
                                CustomerAccountId = partsOutboundBill.CustomerAccountId,
                                BillBusinessType = partsOutboundBill.OutboundType,
                                WarehouseId = partsOutboundBill.WarehouseId,
                                WarehouseName = partsOutboundBill.WarehouseName,
                                ReturnReason = "质量件退货",
                                CPPartsPurchaseOrderCode = partsOutboundBill.CPPartsPurchaseOrderCode,
                                CPPartsInboundCheckCode = partsOutboundBill.CPPartsInboundCheckCode,
                                OriginalRequirementBillCode = partsOutboundBill.OriginalRequirementBillCode
                            };
            //var resultKeepOut = from partsOutboundBill in partsOverOutboundBills
            //                    join purRtn in ObjectContext.PartsPurReturnOrders on
            //                    new
            //                    {
            //                        oriId = partsOutboundBill.OriginalRequirementBillId,
            //                        compId = partsOutboundBill.CounterpartCompanyId
            //                    } equals new
            //                    {
            //                        oriId = purRtn.Id,
            //                        compId = purRtn.PartsSupplierId
            //                    }
            //                    where purRtn.ReturnReason == (int)DcsPartsPurReturnOrderReturnReason.积压件退货 && partsOutboundBill.OutboundType == (int)DcsPartsOutboundType.采购退货
            //                    select new OutboundAndInboundBill
            //                    {
            //                        BillId = partsOutboundBill.Id,
            //                        BillType = (int)DcsPartsLogisticBatchBillDetailBillType.配件出库单,
            //                        BillCode = partsOutboundBill.Code,
            //                        SettlementAmount = ObjectContext.PartsOutboundBillDetails.Where(r => r.PartsOutboundBillId == partsOutboundBill.Id).Sum(v => v.SettlementPrice * v.OutboundAmount),
            //                        CostAmount = ObjectContext.PartsOutboundBillDetails.Where(r => r.PartsOutboundBillId == partsOutboundBill.Id).Sum(v => v.CostPrice * v.OutboundAmount),
            //                        BillCreateTime = partsOutboundBill.CreateTime.Value,
            //                        CompanyId = partsOutboundBill.StorageCompanyId,
            //                        CounterpartCompanyId = partsOutboundBill.CounterpartCompanyId,
            //                        CustomerAccountId = partsOutboundBill.CustomerAccountId,
            //                        BillBusinessType = partsOutboundBill.OutboundType,
            //                        WarehouseId = partsOutboundBill.WarehouseId,
            //                        WarehouseName = partsOutboundBill.WarehouseName,
            //                        ReturnReason = "积压件退货",
            //                        CPPartsPurchaseOrderCode = partsOutboundBill.CPPartsPurchaseOrderCode,
            //                        CPPartsInboundCheckCode = partsOutboundBill.CPPartsInboundCheckCode
            //                    };
            return result.Concat(resultOut)/*.Concat(resultKeepOut)*/.OrderBy(r => r.BillId);
        }

        [Query(HasSideEffects = true)]
        public IQueryable<OutAndInDetailWithPurchasePrice> 查询带采购价的待采购结算出入库明细(int storageCompanyId, int counterpartCompanyId, int? warehouseId, int? customerAccountId, int? partsSaleCategoryId, int[] inboundTypes, int[] outboundTypes, DateTime? endTime) {
            //1、企业Id，对方单位Id，不能为空 2、数组[入库类型]，数组[出库类型] 不能同时为空
            IQueryable<OutAndInDetailWithPurchasePrice> result = null;
            if(inboundTypes.Length == 0 && outboundTypes.Length == 0) {
                throw new ValidationException(ErrorStrings.DataOfPartsInAndOut_Validation1);
            }

            var partsInboundCheckBills = ObjectContext.PartsInboundCheckBills.Where(r => r.Status != (int)DcsPartsInboundCheckBillStatus.取消 && r.SettlementStatus == (int)DcsPartsSettlementStatus.待结算 && r.StorageCompanyId == storageCompanyId && r.CounterpartCompanyId == counterpartCompanyId);
            if(warehouseId.HasValue)
                partsInboundCheckBills = partsInboundCheckBills.Where(r => r.WarehouseId == warehouseId.Value);
            if(customerAccountId.HasValue)
                partsInboundCheckBills = partsInboundCheckBills.Where(r => r.CustomerAccountId == customerAccountId.Value);
            if(endTime.HasValue)
                partsInboundCheckBills = partsInboundCheckBills.Where(r => r.CreateTime <= endTime.Value);
            if(partsSaleCategoryId.HasValue)
                partsInboundCheckBills = partsInboundCheckBills.Where(r => r.PartsSalesCategoryId == partsSaleCategoryId);
            if(inboundTypes.Length > 0) {
                partsInboundCheckBills = partsInboundCheckBills.Where(r => inboundTypes.Contains(r.InboundType));
            }
            var originalRequirementBillIds = partsInboundCheckBills.Select(r => r.OriginalRequirementBillId);
            var partsPurReturnOrders = from a in ObjectContext.PartsPurReturnOrders
                                       from b in originalRequirementBillIds
                                       join detail in ObjectContext.PartsPurReturnOrderDetails on a.Id equals detail.PartsPurReturnOrderId
                                       where b == a.PartsPurchaseOrderId && a.Status != (int)DcsWorkflowOfSimpleApprovalStatus.作废
                                       select new {
                                           a.Id,
                                           a.PartsSupplierId,
                                           detail.SparePartId,
                                           detail.SparePartCode,
                                           detail.SparePartName
                                       };

            var outBoundBillDetails = from a in ObjectContext.PartsOutboundBills
                                      join b in partsPurReturnOrders on a.OriginalRequirementBillId equals b.Id
                                      join detail in ObjectContext.PartsOutboundBillDetails on a.Id equals detail.PartsOutboundBillId
                                      where detail.SparePartId == b.SparePartId && a.SettlementStatus == (int)DcsPartsSettlementStatus.待结算 && a.CounterpartCompanyId == counterpartCompanyId && a.StorageCompanyId == storageCompanyId
                                      select new {
                                          detail.Id,
                                          a.CreateTime,
                                          a.Code,
                                          a.WarehouseName,
                                          detail.SparePartId,
                                          detail.SparePartCode,
                                          detail.SparePartName,
                                          b.PartsSupplierId
                                      };

            var partsPurchaseOrders = from a in partsInboundCheckBills
                                      join b in ObjectContext.PartsPurchaseOrders on a.OriginalRequirementBillId equals b.Id
                                      join detail in ObjectContext.PartsPurchaseOrderDetails on b.Id equals detail.PartsPurchaseOrderId
                                      select new {
                                          a.CreateTime,
                                          a.Id,
                                          a.Code,
                                          a.WarehouseName,
                                          detail.SparePartId,
                                          detail.SparePartCode,
                                          detail.SparePartName,
                                          b.PartsSupplierId
                                      };

            var inboundCheckBillDetails = from a in ObjectContext.PartsInboundCheckBillDetails
                                          join b in partsPurchaseOrders on a.PartsInboundCheckBillId equals b.Id
                                          where a.SparePartId == b.SparePartId
                                          select new {
                                              a.Id,
                                              b.CreateTime,
                                              b.Code,
                                              b.WarehouseName,
                                              a.SparePartId,
                                              a.SparePartCode,
                                              a.SparePartName,
                                              b.PartsSupplierId
                                          };

            var hasPriceInboundParts = from inboundCheckBillDetail in inboundCheckBillDetails
                                       join price in ObjectContext.PartsPurchasePricings on inboundCheckBillDetail.PartsSupplierId equals price.PartsSupplierId
                                       where price.PartId == inboundCheckBillDetail.SparePartId
                                       && price.ValidFrom <= inboundCheckBillDetail.CreateTime
                                       && price.ValidTo > inboundCheckBillDetail.CreateTime
                                       && price.PurchasePrice != 0
                                       select new OutAndInDetailWithPurchasePrice {
                                           BillId = inboundCheckBillDetail.Id,
                                           BillCode = inboundCheckBillDetail.Code,
                                           WarehouseName = inboundCheckBillDetail.WarehouseName,
                                           SparePartCode = inboundCheckBillDetail.SparePartCode,
                                           SparePartName = inboundCheckBillDetail.SparePartName,
                                           PurchasePrice = price.PurchasePrice
                                       };
            var hasPriceIds = hasPriceInboundParts.Select(r => r.BillId);
            var hasNoPriceIds = inboundCheckBillDetails.Select(r => r.Id).Except(hasPriceIds);

            result = from a in inboundCheckBillDetails
                     from b in hasNoPriceIds
                     where a.Id == b
                     select new OutAndInDetailWithPurchasePrice {
                         BillId = a.Id,
                         BillCode = a.Code,
                         WarehouseName = a.WarehouseName,
                         SparePartCode = a.SparePartCode,
                         SparePartName = a.SparePartName,
                         PurchasePrice = 0
                     };

            var hasPriceOutbandParts = from outBoundBillDetail in outBoundBillDetails
                                       join price in ObjectContext.PartsPurchasePricings on outBoundBillDetail.PartsSupplierId equals price.PartsSupplierId
                                       where price.PartId == outBoundBillDetail.SparePartId
                                       && price.ValidFrom <= outBoundBillDetail.CreateTime
                                       && price.ValidTo > outBoundBillDetail.CreateTime
                                       && price.PurchasePrice != 0
                                       select new OutAndInDetailWithPurchasePrice {
                                           BillId = outBoundBillDetail.Id,
                                           BillCode = outBoundBillDetail.Code,
                                           WarehouseName = outBoundBillDetail.WarehouseName,
                                           SparePartCode = outBoundBillDetail.SparePartCode,
                                           SparePartName = outBoundBillDetail.SparePartName,
                                           PurchasePrice = price.PurchasePrice
                                       };
            var hasPriceOutIds = hasPriceOutbandParts.Select(r => r.BillId);
            var hasNoPriceOutIds = outBoundBillDetails.Select(r => r.Id).Except(hasPriceOutIds);

            var resultOut = from a in outBoundBillDetails
                            from b in hasNoPriceOutIds
                            where a.Id == b
                            select new OutAndInDetailWithPurchasePrice {
                                BillId = a.Id,
                                BillCode = a.Code,
                                WarehouseName = a.WarehouseName,
                                SparePartCode = a.SparePartCode,
                                SparePartName = a.SparePartName,
                                PurchasePrice = 0
                            };
            return result.Concat(resultOut).OrderBy(r => r.BillCode);
        }

        public IQueryable<PartsSupplier> 查询存在待采购结算出入库的供应商(int storageCompanyId, int? warehouseId, int? customerAccountId, int? partsSaleCategoryId, int[] inboundTypes, int[] outboundTypes, DateTime? endTime) {
            //1、企业Id，不能为空 2、数组[入库类型]，数组[出库类型] 不能同时为空
            if(inboundTypes.Length == 0 && outboundTypes.Length == 0) {
                throw new ValidationException(ErrorStrings.DataOfPartsInAndOut_Validation1);
            }

            var partsInboundCheckBills = ObjectContext.PartsInboundCheckBills.Where(r => r.SettlementStatus == (int)DcsPartsSettlementStatus.待结算 && r.StorageCompanyId == storageCompanyId);
            if(warehouseId.HasValue)
                partsInboundCheckBills = partsInboundCheckBills.Where(r => r.WarehouseId == warehouseId.Value);
            if(customerAccountId.HasValue)
                partsInboundCheckBills = partsInboundCheckBills.Where(r => r.CustomerAccountId == customerAccountId.Value);
            if(endTime.HasValue)
                partsInboundCheckBills = partsInboundCheckBills.Where(r => r.CreateTime <= endTime.Value);
            if(partsSaleCategoryId.HasValue)
                partsInboundCheckBills = partsInboundCheckBills.Where(r => r.PartsSalesCategoryId == partsSaleCategoryId);
            if(inboundTypes.Length > 0) {
                partsInboundCheckBills = partsInboundCheckBills.Where(r => inboundTypes.Contains(r.InboundType));
            }
            var partsInboundCheckBillIds = partsInboundCheckBills.Select(r => r.CounterpartCompanyId);
            var originalRequirementBillIds = partsInboundCheckBills.Select(r => r.OriginalRequirementBillId);
            var partsPurReturnOrders = from a in ObjectContext.PartsPurReturnOrders
                                       from b in originalRequirementBillIds
                                       where b == a.PartsPurchaseOrderId && a.Status != (int)DcsWorkflowOfSimpleApprovalStatus.作废
                                       select a;
            var partsPurReturnOrderIds = partsPurReturnOrders.Select(r => r.Id);
            var partsOutBoundBills = from a in ObjectContext.PartsOutboundBills
                                     from b in partsPurReturnOrderIds
                                     where a.OriginalRequirementBillId == b && a.SettlementStatus == (int)DcsPartsSettlementStatus.待结算 && a.StorageCompanyId == storageCompanyId
                                     select a;
            var partsOutBoundBillIds = partsOutBoundBills.Select(r => r.CounterpartCompanyId);
            var result = ObjectContext.PartsSuppliers.Where(r => partsInboundCheckBillIds.Concat(partsOutBoundBillIds).Contains(r.Id));
            return result;
        }

        public IQueryable<OutboundAndInboundBill> 查询待采购退货结算出库明细(int storageCompanyId, int counterpartCompanyId, int? warehouseId, int? customerAccountId, int? partsSaleCategoryId, int[] outBoundTypes, DateTime? endTime) {
            //1、企业Id，对方单位Id，不能为空 2、数组[出库类型]不能为空
            if(outBoundTypes == null || outBoundTypes.Length == 0) {
                throw new ValidationException(ErrorStrings.DataOfPartsInAndOut_Validation3);
            }
            IQueryable<OutboundAndInboundBill> result = null;
            var partsOutBoundBills = ObjectContext.PartsOutboundBills.Where(r => r.SettlementStatus == (int)DcsPartsSettlementStatus.待结算 && r.StorageCompanyId == storageCompanyId && r.CounterpartCompanyId == counterpartCompanyId && (customerAccountId == null || r.CustomerAccountId == customerAccountId) && (warehouseId == null || r.WarehouseId == warehouseId) && outBoundTypes.Contains(r.OutboundType) && (endTime == null || r.CreateTime <= endTime) && (partsSaleCategoryId == null || r.PartsSalesCategoryId == partsSaleCategoryId));
            result = from partsOutboundBill in partsOutBoundBills

                     select new OutboundAndInboundBill {
                         BillId = partsOutboundBill.Id,
                         BillType = (int)DcsPartsLogisticBatchBillDetailBillType.配件出库单,
                         BillCode = partsOutboundBill.Code,
                         SettlementAmount = ObjectContext.PartsOutboundBillDetails.Where(r => r.PartsOutboundBillId == partsOutboundBill.Id).Sum(v => v.SettlementPrice * v.OutboundAmount),
                         CostAmount = ObjectContext.PartsOutboundBillDetails.Where(r => r.PartsOutboundBillId == partsOutboundBill.Id).Sum(v => v.CostPrice * v.OutboundAmount),
                         BillCreateTime = partsOutboundBill.CreateTime.Value,
                         CompanyId = partsOutboundBill.StorageCompanyId,
                         CounterpartCompanyId = partsOutboundBill.CounterpartCompanyId,
                         CustomerAccountId = partsOutboundBill.CustomerAccountId,
                         BillBusinessType = partsOutboundBill.OutboundType,
                         WarehouseId = partsOutboundBill.WarehouseId,
                         WarehouseName = partsOutboundBill.WarehouseName

                     };
            return result.OrderBy(r => r.BillId);
        }
        public IQueryable<OutboundAndInboundBill> 查询待采购退货结算出库明细2(int storageCompanyId, int counterpartCompanyId, int? warehouseId, int? customerAccountId, int? partsSaleCategoryId, int[] outBoundTypes, DateTime? endTime) {
            //1、企业Id，对方单位Id，不能为空 2、数组[出库类型]不能为空
            if(outBoundTypes == null || outBoundTypes.Length == 0) {
                throw new ValidationException(ErrorStrings.DataOfPartsInAndOut_Validation3);
            }
            IQueryable<OutboundAndInboundBill> result = null;
            var partsOutBoundBills = ObjectContext.PartsOutboundBills.Where(r => r.SettlementStatus == (int)DcsPartsSettlementStatus.待结算 && r.StorageCompanyId == storageCompanyId && r.CounterpartCompanyId == counterpartCompanyId && (customerAccountId == null || r.CustomerAccountId == customerAccountId) && (warehouseId == null || r.WarehouseId == warehouseId) && outBoundTypes.Contains(r.OutboundType) && (endTime == null || r.CreateTime <= endTime) && (partsSaleCategoryId == null || r.PartsSalesCategoryId == partsSaleCategoryId));
            result = from partsOutboundBill in partsOutBoundBills
                     from partsOutboundBillDetail in ObjectContext.PartsOutboundBillDetails
                     where partsOutboundBillDetail.PartsOutboundBillId == partsOutboundBill.Id
                     join sparepart in ObjectContext.SpareParts
                     on partsOutboundBillDetail.SparePartId equals sparepart.Id
                     select new OutboundAndInboundBill {
                         BillId = partsOutboundBill.Id,
                         BillType = (int)DcsPartsLogisticBatchBillDetailBillType.配件出库单,
                         BillCode = partsOutboundBill.Code,
                         SettlementAmount = ObjectContext.PartsOutboundBillDetails.Where(r => r.PartsOutboundBillId == partsOutboundBill.Id).Sum(v => v.SettlementPrice * v.OutboundAmount),
                         CostAmount = ObjectContext.PartsOutboundBillDetails.Where(r => r.PartsOutboundBillId == partsOutboundBill.Id).Sum(v => v.CostPrice * v.OutboundAmount),
                         BillCreateTime = partsOutboundBill.CreateTime.Value,
                         CompanyId = partsOutboundBill.StorageCompanyId,
                         CounterpartCompanyId = partsOutboundBill.CounterpartCompanyId,
                         CustomerAccountId = partsOutboundBill.CustomerAccountId,
                         BillBusinessType = partsOutboundBill.OutboundType,
                         WarehouseId = partsOutboundBill.WarehouseId,
                         WarehouseName = partsOutboundBill.WarehouseName,
                         GoldenTaxClassifyCode = sparepart.GoldenTaxClassifyCode,
                         SparePartCode = sparepart.Code,
                         SparePartName = sparepart.Name
                     };
            return result.OrderBy(r => r.BillId);
        }
        [Query(HasSideEffects = true)]
        public IQueryable<OutboundAndInboundBill> 查询退货完成销售出入库单(string[] outBoundBillcodes, string[] inBoundBillcodes) {
            IQueryable<PartsInboundCheckBill> inBoundBills = null;
            IQueryable<PartsOutboundBill> outBoundBills = null;
            if(inBoundBillcodes.Length > 0) {
                inBoundBills = from a in ObjectContext.PartsInboundCheckBills.Where(r => inBoundBillcodes.Contains(r.Code) && r.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件销售退货单)
                               join c in ObjectContext.PartsSalesReturnBills on a.OriginalRequirementBillId equals c.Id
                               join e in ObjectContext.PartsSalesReturnBillDetails on c.Id equals e.PartsSalesReturnBillId
                               join d in ObjectContext.PartsOutboundBills.Where(r => r.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件销售订单) on e.PartsSalesOrderId equals d.OriginalRequirementBillId
                               select a;
                outBoundBills = from a in ObjectContext.PartsInboundCheckBills.Where(r => inBoundBillcodes.Contains(r.Code) && r.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件销售退货单)
                                join c in ObjectContext.PartsSalesReturnBills on a.OriginalRequirementBillId equals c.Id
                                join e in ObjectContext.PartsSalesReturnBillDetails on c.Id equals e.PartsSalesReturnBillId
                                join d in ObjectContext.PartsOutboundBills.Where(r => r.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件销售订单) on e.PartsSalesOrderId equals d.OriginalRequirementBillId
                                select d;
            }
            if(inBoundBills != null) {
                var resultOut = from partsOutboundBill in outBoundBills
                                select new OutboundAndInboundBill {
                                    BillId = partsOutboundBill.Id,
                                    BillType = (int)DcsPartsLogisticBatchBillDetailBillType.配件出库单,
                                    BillCode = partsOutboundBill.Code
                                };
                var resultIn = from partsInboundBill in inBoundBills
                               select new OutboundAndInboundBill {
                                   BillId = partsInboundBill.Id,
                                   BillType = (int)DcsPartsLogisticBatchBillDetailBillType.配件入库检验单,
                                   BillCode = partsInboundBill.Code
                               };
                return resultOut.Concat(resultIn).Distinct();
            }
            return null;
        }
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:32
        //原分布类的函数全部转移到Ach                                                               
        /// <summary>
        /// 查询待结算出入库明细
        /// </summary>
        /// <param name="storageCompanyId">企业Id</param>
        /// <param name="counterpartCompanyId">对方单位Id</param>
        /// <param name="warehouseId">仓库Id</param>
        /// <param name="customerAccountId">客户账户Id</param>
        /// <param name="partsSalesCategoryId"></param>
        /// <param name="inboundTypes">数组【入库类型】</param>
        /// <param name="outboundTypes">数组【出库类型】</param>
        /// <param name="endTime">结束时间</param>
        /// <returns></returns>
        public IQueryable<OutboundAndInboundBill> 查询待结算出入库明细(int storageCompanyId, int counterpartCompanyId, int? warehouseId, int? customerAccountId, int? partsSalesCategoryId, int[] inboundTypes, int[] outboundTypes, DateTime? endTime, bool cpPartsInboundCheckCodeNull, int? type) {
            return new OutboundAndInboundBillAch(this).查询待结算出入库明细(storageCompanyId, counterpartCompanyId, warehouseId, customerAccountId, partsSalesCategoryId, inboundTypes, outboundTypes, endTime, cpPartsInboundCheckCodeNull, type);
        }

        public IQueryable<OutboundAndInboundBill> 查询待结算出入库明细采埃孚(int storageCompanyId, int counterpartCompanyId, int? warehouseId, int? customerAccountId, int? partsSalesCategoryId, int[] inboundTypes, int[] outboundTypes, DateTime? endTime) {
            return new OutboundAndInboundBillAch(this).查询待结算出入库明细采埃孚(storageCompanyId, counterpartCompanyId, warehouseId, customerAccountId, partsSalesCategoryId, inboundTypes, outboundTypes, endTime);
        }

        public IQueryable<OutboundAndInboundBill> 查询待销售结算出入库明细(int companyId, int destCompanyId, int? warehouseId, int? customerAccountId, int? partsSaleCategoryId, int[] inBoundType, int[] outBoundType, DateTime? endTime, int? settleType, int? businessType) {
            return new OutboundAndInboundBillAch(this).查询待销售结算出入库明细(companyId, destCompanyId, warehouseId, customerAccountId, partsSaleCategoryId, inBoundType, outBoundType, endTime, settleType, businessType);
        }

        public IQueryable<OutboundAndInboundBill> 查询待销售结算出入库明细采埃孚(int companyId, int destCompanyId, int? warehouseId, int? customerAccountId, int? partsSaleCategoryId, int[] inBoundType, int[] outBoundType, DateTime? endTime, int? ischeck) {
            return new OutboundAndInboundBillAch(this).查询待销售结算出入库明细采埃孚(companyId, destCompanyId, warehouseId, customerAccountId, partsSaleCategoryId, inBoundType, outBoundType, endTime, ischeck);
        }

        public IQueryable<OutboundAndInboundBillWithDetail> 查询待销售结算出入库清单明细(int companyId, int destCompanyId, int? warehouseId, int? customerAccountId, int? partsSaleCategoryId, int[] inBoundType, int[] outBoundType, DateTime? endTime) {
            return new OutboundAndInboundBillAch(this).查询待销售结算出入库清单明细(companyId, destCompanyId, warehouseId, customerAccountId, partsSaleCategoryId, inBoundType, outBoundType, endTime);
        }

        public IQueryable<OutboundAndInboundBillWithDetail> 查询待销售结算出入库清单明细采埃孚(int companyId, int destCompanyId, int? warehouseId, int? customerAccountId, int? partsSaleCategoryId, int[] inBoundType, int[] outBoundType, DateTime? endTime) {
            return new OutboundAndInboundBillAch(this).查询待销售结算出入库清单明细采埃孚(companyId, destCompanyId, warehouseId, customerAccountId, partsSaleCategoryId, inBoundType, outBoundType, endTime);
        }

        public IQueryable<OutboundAndInboundBill> 查询待销售退货结算入库明细(int storageCompanyId, int counterpartCompanyId, int? warehouseId, int? customerAccountId, int? partsSaleCategoryId, int[] inboundTypes, DateTime? endTime, int? settleType, int? businessType) {
            return new OutboundAndInboundBillAch(this).查询待销售退货结算入库明细(storageCompanyId, counterpartCompanyId, warehouseId, customerAccountId, partsSaleCategoryId, inboundTypes, endTime, settleType, businessType);
        }

        public IQueryable<VietualPartsSalesRtnSettlementRef> GetPartsSalesRtnSettlementRefDetail(int partsSalesRtnSettlementId) {
            return new OutboundAndInboundBillAch(this).GetPartsSalesRtnSettlementRefDetail(partsSalesRtnSettlementId);
        }

        public IQueryable<OutboundAndInboundBill> 查询待采购结算出入库明细(int storageCompanyId, int counterpartCompanyId, int? warehouseId, int? customerAccountId, int? partsSaleCategoryId, int[] inboundTypes, int[] outboundTypes, DateTime? endTime) {
            return new OutboundAndInboundBillAch(this).查询待采购结算出入库明细(storageCompanyId, counterpartCompanyId, warehouseId, customerAccountId, partsSaleCategoryId, inboundTypes, outboundTypes, endTime);
        }

        public IQueryable<OutboundAndInboundBill> 查询待采购结算出入库明细采埃孚(int storageCompanyId, int counterpartCompanyId, int? warehouseId, int? customerAccountId, int? partsSaleCategoryId, int[] inboundTypes, int[] outboundTypes, DateTime? endTime) {
            return new OutboundAndInboundBillAch(this).查询待采购结算出入库明细采埃孚(storageCompanyId, counterpartCompanyId, warehouseId, customerAccountId, partsSaleCategoryId, inboundTypes, outboundTypes, endTime);
        }


        [Query(HasSideEffects = true)]
        public IQueryable<OutAndInDetailWithPurchasePrice> 查询带采购价的待采购结算出入库明细(int storageCompanyId, int counterpartCompanyId, int? warehouseId, int? customerAccountId, int? partsSaleCategoryId, int[] inboundTypes, int[] outboundTypes, DateTime? endTime) {
            return new OutboundAndInboundBillAch(this).查询带采购价的待采购结算出入库明细(storageCompanyId, counterpartCompanyId, warehouseId, customerAccountId, partsSaleCategoryId, inboundTypes, outboundTypes, endTime);
        }
        public IQueryable<OutboundAndInboundBill> 查询待采购结算出入库明细过滤原始入库单编号(int storageCompanyId, int counterpartCompanyId, int? warehouseId, int? customerAccountId, int? partsSaleCategoryId, int[] inboundTypes, int[] outboundTypes, DateTime? endTime, string invoiceNumber) {
            return new OutboundAndInboundBillAch(this).查询待采购结算出入库明细过滤原始入库单编号(storageCompanyId, counterpartCompanyId, warehouseId, customerAccountId, partsSaleCategoryId, inboundTypes, outboundTypes, endTime, invoiceNumber);
        }
        public IQueryable<PartsSupplier> 查询存在待采购结算出入库的供应商(int storageCompanyId, int? warehouseId, int? customerAccountId, int? partsSaleCategoryId, int[] inboundTypes, int[] outboundTypes, DateTime? endTime) {
            return new OutboundAndInboundBillAch(this).查询存在待采购结算出入库的供应商(storageCompanyId, warehouseId, customerAccountId, partsSaleCategoryId, inboundTypes, outboundTypes, endTime);
        }

        public IQueryable<OutboundAndInboundBill> 查询待采购退货结算出库明细(int storageCompanyId, int counterpartCompanyId, int? warehouseId, int? customerAccountId, int? partsSaleCategoryId, int[] outBoundTypes, DateTime? endTime) {
            return new OutboundAndInboundBillAch(this).查询待采购退货结算出库明细(storageCompanyId, counterpartCompanyId, warehouseId, customerAccountId, partsSaleCategoryId, outBoundTypes, endTime);
        }
        public IQueryable<OutboundAndInboundBill> 查询待采购退货结算出库明细2(int storageCompanyId, int counterpartCompanyId, int? warehouseId, int? customerAccountId, int? partsSaleCategoryId, int[] outBoundTypes, DateTime? endTime) {
            return new OutboundAndInboundBillAch(this).查询待采购退货结算出库明细2(storageCompanyId, counterpartCompanyId, warehouseId, customerAccountId, partsSaleCategoryId, outBoundTypes, endTime);
        }

        [Query(HasSideEffects = true)]
        public IQueryable<OutboundAndInboundBill> 查询退货完成销售出入库单(string[] outBoundBillcodes, string[] inBoundBillcodes) {
            return new OutboundAndInboundBillAch(this).查询退货完成销售出入库单(outBoundBillcodes, inBoundBillcodes);
        }
        public IEnumerable<OutboundAndInboundBill> 查询待销售结算出入库明细New(int companyId, int destCompanyId, int? warehouseId, int? customerAccountId, int? partsSaleCategoryId, int[] inBoundType, int[] outBoundType, DateTime? endTime, int? settleType, int? businessType,bool? isOil)
        {
            return new OutboundAndInboundBillAch(this).查询待销售结算出入库明细New(companyId, destCompanyId, warehouseId, customerAccountId, partsSaleCategoryId, inBoundType, outBoundType, endTime, settleType, businessType, isOil);

        }
        public IEnumerable<OutboundAndInboundBill> 查询待销售结算出入库清单明细New(int companyId, int destCompanyId, int? warehouseId, int? customerAccountId, int? partsSaleCategoryId, int[] inBoundType, int[] outBoundType, DateTime? endTime, int? settleType, int? businessType)
        {
            return new OutboundAndInboundBillAch(this).查询待销售结算出入库清单明细New(companyId, destCompanyId, warehouseId, customerAccountId, partsSaleCategoryId, inBoundType, outBoundType, endTime, settleType, businessType);

        }
    }
}
