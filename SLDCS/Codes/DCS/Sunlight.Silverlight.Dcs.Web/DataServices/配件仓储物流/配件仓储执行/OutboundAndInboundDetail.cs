﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class OutboundAndInboundDetailAch : DcsSerivceAchieveBase {
        public OutboundAndInboundDetailAch(DcsDomainService domainService)
            : base(domainService) {
        }

        /// <summary>
        /// 查询待结算出入库明细清单
        /// </summary>
        /// <param name="inboundIds">数组【配件入库检验单Id】</param>
        /// <param name="outboundIds">数组【配件出库单Id】</param>
        /// <returns></returns>
        [Query(HasSideEffects = true)]
        public IQueryable<OutboundAndInboundDetail> 查询待结算出入库明细清单(int[] inboundIds, int[] outboundIds, int? partsSalesCategoryId) {
            IQueryable<OutboundAndInboundDetail> result = null;
            if(inboundIds != null && inboundIds.Length > 0) {
                var partsInboundCheckBillDetails = ObjectContext.PartsInboundCheckBillDetails.Where(r => inboundIds.Contains(r.PartsInboundCheckBillId));
                if(partsSalesCategoryId != null) {
                    partsInboundCheckBillDetails = partsInboundCheckBillDetails.Where(r => ObjectContext.PartsInboundCheckBills.Any(v => v.Id == r.PartsInboundCheckBillId && v.PartsSalesCategoryId == partsSalesCategoryId) && r.InspectedQuantity != 0);
                }
                result = from partsInboundCheckBillDetail in partsInboundCheckBillDetails
                         from partsInboundCheckBill in ObjectContext.PartsInboundCheckBills
                         where partsInboundCheckBillDetail.PartsInboundCheckBillId == partsInboundCheckBill.Id
                         join sparepart in ObjectContext.SpareParts
                         on partsInboundCheckBillDetail.SparePartId equals sparepart.Id
                         select new OutboundAndInboundDetail {
                             BillId = partsInboundCheckBillDetail.Id,
                             BillType = (int)DcsPartsLogisticBatchBillDetailBillType.配件入库检验单,
                             SparePartId = partsInboundCheckBillDetail.SparePartId,
                             SparePartCode = partsInboundCheckBillDetail.SparePartCode,
                             SparePartName = partsInboundCheckBillDetail.SparePartName,
                             SettlementPrice = partsInboundCheckBillDetail.SettlementPrice,
                             CostPrice = partsInboundCheckBillDetail.CostPrice,
                             Quantity = partsInboundCheckBillDetail.InspectedQuantity,
                             SettlementAmount = partsInboundCheckBillDetail.SettlementPrice * partsInboundCheckBillDetail.InspectedQuantity,
                             PartsSaleCategoryId = partsInboundCheckBill.PartsSalesCategoryId,
                             GoldenTaxClassifyCode = sparepart.GoldenTaxClassifyCode,
                             BillCode = partsInboundCheckBill.Code
                         };
            }
            if(outboundIds != null && outboundIds.Length > 0) {
                var partsOutboundBillDetails = ObjectContext.PartsOutboundBillDetails.Where(r => outboundIds.Contains(r.PartsOutboundBillId));
                if(partsSalesCategoryId.HasValue)
                    partsOutboundBillDetails = partsOutboundBillDetails.Where(r => ObjectContext.PartsOutboundBills.Any(v => v.Id == r.PartsOutboundBillId && v.PartsSalesCategoryId == partsSalesCategoryId) && r.OutboundAmount != 0);
                var resultOut = from partsOutboundBillDetail in partsOutboundBillDetails
                                from partsOutbountdBIll in ObjectContext.PartsOutboundBills
                                where partsOutboundBillDetail.PartsOutboundBillId == partsOutbountdBIll.Id
                                join sparepart in ObjectContext.SpareParts
                                on partsOutboundBillDetail.SparePartId equals sparepart.Id
                                select new OutboundAndInboundDetail {
                                    BillId = partsOutboundBillDetail.Id,
                                    BillType = (int)DcsPartsLogisticBatchBillDetailBillType.配件出库单,
                                    SparePartId = partsOutboundBillDetail.SparePartId,
                                    SparePartCode = partsOutboundBillDetail.SparePartCode,
                                    SparePartName = partsOutboundBillDetail.SparePartName,
                                    SettlementPrice = partsOutboundBillDetail.SettlementPrice,
                                    CostPrice = partsOutboundBillDetail.CostPrice,
                                    Quantity = partsOutboundBillDetail.OutboundAmount,
                                    SettlementAmount = partsOutboundBillDetail.SettlementPrice * partsOutboundBillDetail.OutboundAmount,
                                    PartsSaleCategoryId = partsOutbountdBIll.PartsSalesCategoryId,
                                    GoldenTaxClassifyCode = sparepart.GoldenTaxClassifyCode,
                                    BillCode = partsOutbountdBIll.Code
                                };
                resultOut=resultOut.Distinct();
                result = result != null ? result.Concat(resultOut) : resultOut;
            }
            return result.OrderBy(r => r.BillId);
        }

        /// <summary>
        /// 查询待结算出入库明细清单采埃孚
        /// </summary>
        /// <param name="inboundIds">数组【配件入库检验单Id】</param>
        /// <param name="outboundIds">数组【配件出库单Id】</param>
        /// <returns></returns>
        public IQueryable<OutboundAndInboundDetail> 查询待结算出入库明细清单采埃孚(int[] inboundIds, int[] outboundIds, int? partsSalesCategoryId)
        {
            IQueryable<OutboundAndInboundDetail> result = null;
            if (inboundIds != null && inboundIds.Length > 0)
            {
                var partsInboundCheckBillDetails = ObjectContext.PartsInboundCheckBillDetails.Where(r => inboundIds.Contains(r.PartsInboundCheckBillId));
                if (partsSalesCategoryId != null)
                {
                    partsInboundCheckBillDetails = partsInboundCheckBillDetails.Where(r => ObjectContext.PartsInboundCheckBills.Any(v => v.Id == r.PartsInboundCheckBillId && v.PartsSalesCategoryId == partsSalesCategoryId) && r.InspectedQuantity != 0);
                }
                result = from partsInboundCheckBillDetail in partsInboundCheckBillDetails
                         from partsInboundCheckBill in ObjectContext.PartsInboundCheckBills
                         where partsInboundCheckBillDetail.PartsInboundCheckBillId == partsInboundCheckBill.Id
                         join sparepart in ObjectContext.SpareParts
                         on partsInboundCheckBillDetail.SparePartId equals sparepart.Id
                         select new OutboundAndInboundDetail
                         {
                             BillId = partsInboundCheckBillDetail.Id,
                             BillType = (int)DcsPartsLogisticBatchBillDetailBillType.配件入库检验单,
                             SparePartId = partsInboundCheckBillDetail.SparePartId,
                             SparePartCode = partsInboundCheckBillDetail.SparePartCode,
                             SparePartName = partsInboundCheckBillDetail.SparePartName,
                             SettlementPrice = partsInboundCheckBillDetail.SettlementPrice,
                             CostPrice = partsInboundCheckBillDetail.CostPrice,
                             Quantity = partsInboundCheckBillDetail.InspectedQuantity,
                             SettlementAmount = partsInboundCheckBillDetail.SettlementPrice * partsInboundCheckBillDetail.InspectedQuantity,
                             PartsSaleCategoryId = partsInboundCheckBill.PartsSalesCategoryId,
                             GoldenTaxClassifyCode = sparepart.GoldenTaxClassifyCode
                         };
            }
            if (outboundIds != null && outboundIds.Length > 0)
            {
                var partsOutboundBillDetails = ObjectContext.PartsOutboundBillDetails.Where(r => outboundIds.Contains(r.PartsOutboundBillId));
                if (partsSalesCategoryId.HasValue)
                    partsOutboundBillDetails = partsOutboundBillDetails.Where(r => ObjectContext.PartsOutboundBills.Any(v => v.Id == r.PartsOutboundBillId && v.PartsSalesCategoryId == partsSalesCategoryId) && r.OutboundAmount != 0);
                var resultOut = from partsOutboundBillDetail in partsOutboundBillDetails
                                from partsOutbountdBIll in ObjectContext.PartsOutboundBills
                                where partsOutboundBillDetail.PartsOutboundBillId == partsOutbountdBIll.Id
                                join sparepart in ObjectContext.SpareParts
                                on partsOutboundBillDetail.SparePartId equals sparepart.Id
                                join PartsPurchaseorder in ObjectContext.PartsPurchaseOrders
                                on partsOutbountdBIll.CPPartsPurchaseOrderCode equals PartsPurchaseorder.CPPartsPurchaseOrderCode into JoinedEmpDept
                                from PartsPurchaseorder in JoinedEmpDept.DefaultIfEmpty()
                                select new OutboundAndInboundDetail
                                {
                                    BillId = partsOutboundBillDetail.Id,
                                    BillType = (int)DcsPartsLogisticBatchBillDetailBillType.配件出库单,
                                    SparePartId = partsOutboundBillDetail.SparePartId,
                                    SparePartCode = partsOutboundBillDetail.SparePartCode,
                                    SparePartName = partsOutboundBillDetail.SparePartName,
                                    //SettlementPrice = partsOutboundBillDetail.SettlementPrice,
                                    SettlementPrice= ObjectContext.PartsPurchasePricings.Where(a => partsOutboundBillDetail.SparePartId == a.PartId && a.PartsSalesCategoryId == partsOutbountdBIll.PartsSalesCategoryId && a.PartsSupplierId == PartsPurchaseorder.PartsSupplierId && a.BranchId == partsOutbountdBIll.BranchId && (a.ValidFrom <= partsOutbountdBIll.CreateTime && a.ValidTo >= partsOutbountdBIll.CreateTime)).Sum(r => r.PurchasePrice) > 0 ? ObjectContext.PartsPurchasePricings.Where(a => partsOutboundBillDetail.SparePartId == a.PartId && a.PartsSalesCategoryId == partsOutbountdBIll.PartsSalesCategoryId && a.PartsSupplierId == PartsPurchaseorder.PartsSupplierId && a.BranchId == partsOutbountdBIll.BranchId && (a.ValidFrom <= partsOutbountdBIll.CreateTime && a.ValidTo >= partsOutbountdBIll.CreateTime)).Sum(r => r.PurchasePrice) : partsOutboundBillDetail.SettlementPrice,
                                    CostPrice = partsOutboundBillDetail.CostPrice,
                                    Quantity = partsOutboundBillDetail.OutboundAmount,
                                    SettlementAmount =ObjectContext.PartsPurchasePricings.Where(a=> partsOutboundBillDetail.SparePartId == a.PartId && a.PartsSalesCategoryId == partsOutbountdBIll.PartsSalesCategoryId && a.PartsSupplierId == PartsPurchaseorder.PartsSupplierId && a.BranchId == partsOutbountdBIll.BranchId && (a.ValidFrom <= partsOutbountdBIll.CreateTime && a.ValidTo >= partsOutbountdBIll.CreateTime)).Sum(r=>r.PurchasePrice)* partsOutboundBillDetail.OutboundAmount > 0 ? ObjectContext.PartsPurchasePricings.Where(a => partsOutboundBillDetail.SparePartId == a.PartId && a.PartsSalesCategoryId == partsOutbountdBIll.PartsSalesCategoryId && a.PartsSupplierId == PartsPurchaseorder.PartsSupplierId && a.BranchId == partsOutbountdBIll.BranchId && (a.ValidFrom <= partsOutbountdBIll.CreateTime && a.ValidTo >= partsOutbountdBIll.CreateTime)).Sum(r => r.PurchasePrice) * partsOutboundBillDetail.OutboundAmount : partsOutboundBillDetail.SettlementPrice * partsOutboundBillDetail.OutboundAmount,
                                    //SettlementAmount = partsOutboundBillDetail.SettlementPrice * partsOutboundBillDetail.OutboundAmount,
                                    PartsSaleCategoryId = partsOutbountdBIll.PartsSalesCategoryId,
                                    GoldenTaxClassifyCode = sparepart.GoldenTaxClassifyCode
                                };
                resultOut = resultOut.Distinct();
                result = result != null ? result.Concat(resultOut) : resultOut;
            }
            return result.OrderBy(r => r.BillId);
        }
        public IQueryable<OutboundAndInboundDetail> 查询出入库明细清单(int? inboundId, int? outboundId, int? inboundType, int? outboundType) {
            IQueryable<OutboundAndInboundDetail> result = null;
            if(inboundType.HasValue && inboundType.Value == (int)DcsPartsLogisticBatchBillDetailBillType.配件入库检验单) {
                result = from a in ObjectContext.PartsInboundCheckBillDetails.Where(r => r.PartsInboundCheckBillId == inboundId.Value)
                         join b in ObjectContext.SpareParts on a.SparePartId equals b.Id into t1
                         from c in t1
                         select new OutboundAndInboundDetail {
                             BillId = a.Id,
                             BillType = (int)DcsPartsLogisticBatchBillDetailBillType.配件入库检验单,
                             SparePartName = a.SparePartName,
                             SparePartCode = a.SparePartCode,
                             Quantity = a.InspectedQuantity,
                             SettlementPrice = a.SettlementPrice,
                             MeasureUnit = c.MeasureUnit,
                             CostPrice = a.CostPrice,
                             Remark = a.Remark,
                         };
            }
            if(outboundType.HasValue && outboundType.Value == (int)DcsPartsLogisticBatchBillDetailBillType.配件出库单) {
                var resultOut = from a in ObjectContext.PartsOutboundBillDetails.Where(r => r.PartsOutboundBillId == outboundId.Value)
                                join b in ObjectContext.SpareParts on a.SparePartId equals b.Id into t1
                                from c in t1
                                select new OutboundAndInboundDetail {
                                    BillId = a.Id,
                                    BillType = (int)DcsPartsLogisticBatchBillDetailBillType.配件出库单,
                                    SparePartName = a.SparePartName,
                                    SparePartCode = a.SparePartCode,
                                    Quantity = a.OutboundAmount,
                                    SettlementPrice = a.SettlementPrice,
                                    MeasureUnit = c.MeasureUnit,
                                    CostPrice = a.CostPrice,
                                    Remark = a.Remark,
                                };
                result = result != null ? result.Concat(resultOut) : resultOut;
            }
            return result.OrderBy(r => r.BillId);
        }
        [Query(HasSideEffects = true)]
        public List<OutboundAndInboundDetail> 查询待结算出入库明细清单后定价(int[] inboundIds, int[] outboundIds, int partsSalesCategoryId, int supplierId) {
            if(inboundIds.Length == 0 && outboundIds.Length == 0) {
                throw new ValidationException("入库和出库不能同时为空");
            }
            var result = new List<OutboundAndInboundDetail>();
            if(inboundIds.Length > 0) {
                var inboundPartsDetails = (from a in ObjectContext.PartsInboundCheckBills.Where(r => inboundIds.Contains(r.Id))
                                           from b in ObjectContext.PartsInboundCheckBillDetails.Where(r => r.InspectedQuantity != 0)
                                           from sparepart in ObjectContext.SpareParts
                                           where  a.Id == b.PartsInboundCheckBillId && b.SparePartId == sparepart.Id
                                           select new {
                                               a.Id,
                                               b.SparePartId,
                                               b.SparePartCode,
                                               b.SparePartName,
                                               b.CostPrice,
                                               b.InspectedQuantity,
                                               b.SettlementPrice,
                                               a.CreateTime,
                                               sparepart.GoldenTaxClassifyCode
                                           }).ToList();
                var inboundPartsPurchasePrices = (from a in ObjectContext.PartsInboundCheckBills.Where(r => inboundIds.Contains(r.Id))
                                                  from b in ObjectContext.PartsInboundCheckBillDetails.Where(r => r.InspectedQuantity != 0)
                                                  from c in ObjectContext.PartsPurchasePricings.Where(r => r.PartsSupplierId == supplierId)
                                                  where c.PartsSalesCategoryId == partsSalesCategoryId && c.ValidFrom <= a.CreateTime && c.ValidTo >= a.CreateTime && a.Id == b.PartsInboundCheckBillId && b.SparePartId == c.PartId
                                                  select new {
                                                      BillId = a.Id,
                                                      b.SparePartId,
                                                      c.PurchasePrice
                                                  }).ToList();
                foreach(var item in inboundPartsDetails) {
                    var oai = new OutboundAndInboundDetail {
                        BillId = item.Id,
                        BillType = (int)DcsPartsLogisticBatchBillDetailBillType.配件入库检验单,
                        SparePartId = item.SparePartId,
                        SparePartCode = item.SparePartCode,
                        SparePartName = item.SparePartName,
                        SettlementPrice = item.SettlementPrice,
                        CostPrice = item.CostPrice,
                        Quantity = item.InspectedQuantity,
                        PartsSaleCategoryId = partsSalesCategoryId,
                        GoldenTaxClassifyCode = item.GoldenTaxClassifyCode
                    };
                    //if(oai.SettlementPrice == 0) {
                    var tempItem = inboundPartsPurchasePrices.FirstOrDefault(r => r.BillId == oai.BillId && r.SparePartId == oai.SparePartId);
                    if(tempItem != null) {
                        oai.SettlementPrice = tempItem.PurchasePrice;
                    }
                    //}
                    oai.SettlementAmount = oai.SettlementPrice * oai.Quantity;
                    result.Add(oai);
                }
            }
            if(outboundIds.Length > 0) {
                var outboundPartsDetails = (from a in ObjectContext.PartsOutboundBills.Where(r => outboundIds.Contains(r.Id))
                                            from b in ObjectContext.PartsOutboundBillDetails.Where(r => r.OutboundAmount != 0)
                                            join sparepart in ObjectContext.SpareParts
                                            on b.SparePartId equals sparepart.Id
                                            where a.Id == b.PartsOutboundBillId
                                            select new {
                                                a.Id,
                                                b.SparePartId,
                                                b.SparePartCode,
                                                b.SparePartName,
                                                b.CostPrice,
                                                b.OutboundAmount,
                                                b.SettlementPrice,
                                                sparepart.GoldenTaxClassifyCode
                                            }).ToList();
                var outboundPartsPurchasePrices = (from a in ObjectContext.PartsOutboundBills.Where(r => outboundIds.Contains(r.Id))
                                                   from b in ObjectContext.PartsOutboundBillDetails.Where(r => r.OutboundAmount != 0)
                                                   from c in ObjectContext.PartsPurchasePricings
                                                   where c.PartsSalesCategoryId == partsSalesCategoryId && c.PartsSupplierId == supplierId && c.ValidFrom <= a.CreateTime && c.ValidTo >= a.CreateTime && a.Id == b.PartsOutboundBillId && b.SparePartId == c.PartId
                                                   select new {
                                                       BillId = a.Id,
                                                       b.SparePartId,
                                                       c.PurchasePrice
                                                   }).ToList();
                foreach(var item in outboundPartsDetails) {
                    var oai = new OutboundAndInboundDetail {
                        BillId = item.Id,
                        BillType = (int)DcsPartsLogisticBatchBillDetailBillType.配件出库单,
                        SparePartId = item.SparePartId,
                        SparePartCode = item.SparePartCode,
                        SparePartName = item.SparePartName,
                        SettlementPrice = item.SettlementPrice,
                        CostPrice = item.CostPrice,
                        Quantity = item.OutboundAmount,
                        PartsSaleCategoryId = partsSalesCategoryId,
                        GoldenTaxClassifyCode = item.GoldenTaxClassifyCode
                    };
                    //if(oai.SettlementPrice == 0) {
                    var tempItem = outboundPartsPurchasePrices.FirstOrDefault(r => r.BillId == oai.BillId && r.SparePartId == oai.SparePartId);
                    if(tempItem != null) {
                        oai.SettlementPrice = tempItem.PurchasePrice;
                    }
                    // }
                    oai.SettlementAmount = oai.SettlementPrice * oai.Quantity;
                    result.Add(oai);
                }
            }
            return new List<OutboundAndInboundDetail>(result.OrderBy(r => r.BillId));
        }
        public IQueryable<OutboundAndInboundWithDetail> 查询配件历史入库记录(int? partId, int? warehouseId) {
            var inboundpartOfResult = from a in ObjectContext.PartsInboundCheckBills.Where(r => !warehouseId.HasValue || r.WarehouseId == warehouseId.Value)
                                      from b in a.PartsInboundCheckBillDetails.Where(r => !partId.HasValue || r.SparePartId == partId.Value)
                                      select new OutboundAndInboundWithDetail {
                                          Id = b.Id,
                                          WarehouseId = a.WarehouseId,
                                          WarehouseCode = a.WarehouseCode,
                                          WarehouseName = a.WarehouseName,
                                          BranchId = a.BranchId,
                                          BranchCode = a.BranchCode,
                                          BranchName = a.BranchName,
                                          MainbillCode = a.Code,
                                          InboundType = a.InboundType,
                                          OutboundType = 0,
                                          CounterpartCompanyId = a.CounterpartCompanyId,
                                          CounterpartCompanyCode = a.CounterpartCompanyCode,
                                          CounterpartCompanyName = a.CounterpartCompanyName,
                                          SparePartId = b.SparePartId,
                                          SparePartCode = b.SparePartCode,
                                          SparePartName = b.SparePartName,
                                          Quantity = b.InspectedQuantity,
                                          Price = b.SettlementPrice,
                                          CreateTime = a.CreateTime ?? DateTime.MinValue
                                      };
            return inboundpartOfResult.OrderBy(r => r.Id);
        }
        public IQueryable<OutboundAndInboundWithDetail> 查询配件历史出库记录(int? partId, int? warehouseId) {
            var outboundpartOfResult = from a in ObjectContext.PartsOutboundBills.Where(r => !warehouseId.HasValue || r.WarehouseId == warehouseId.Value)
                                       from b in a.PartsOutboundBillDetails.Where(r => !partId.HasValue || r.SparePartId == partId.Value)
                                       select new OutboundAndInboundWithDetail {
                                           Id = b.Id,
                                           WarehouseId = a.WarehouseId,
                                           WarehouseCode = a.WarehouseCode,
                                           WarehouseName = a.WarehouseName,
                                           BranchId = a.BranchId,
                                           BranchCode = a.BranchCode,
                                           BranchName = a.BranchName,
                                           MainbillCode = a.Code,
                                           InboundType = 0,
                                           OutboundType = a.OutboundType,
                                           CounterpartCompanyId = a.CounterpartCompanyId,
                                           CounterpartCompanyCode = a.CounterpartCompanyCode,
                                           CounterpartCompanyName = a.CounterpartCompanyName,
                                           SparePartId = b.SparePartId,
                                           SparePartCode = b.SparePartCode,
                                           SparePartName = b.SparePartName,
                                           Quantity = b.OutboundAmount,
                                           Price = b.SettlementPrice,
                                           CreateTime = a.CreateTime ?? DateTime.MinValue
                                       };
            return outboundpartOfResult.OrderBy(r => r.Id);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:32
        //原分布类的函数全部转移到Ach                                                               
        /// <summary>
        /// 查询待结算出入库明细清单
        /// </summary>
        /// <param name="inboundIds">数组【配件入库检验单Id】</param>
        /// <param name="outboundIds">数组【配件出库单Id】</param>
        /// <returns></returns>
        [Query(HasSideEffects = true)]
        public IQueryable<OutboundAndInboundDetail> 查询待结算出入库明细清单(int[] inboundIds, int[] outboundIds, int? partsSalesCategoryId) {
            return new OutboundAndInboundDetailAch(this).查询待结算出入库明细清单(inboundIds,outboundIds,partsSalesCategoryId);
        }

        public IQueryable<OutboundAndInboundDetail> 查询待结算出入库明细清单采埃孚(int[] inboundIds, int[] outboundIds, int? partsSalesCategoryId)
        {
            return new OutboundAndInboundDetailAch(this).查询待结算出入库明细清单采埃孚(inboundIds, outboundIds, partsSalesCategoryId);
        }

        public IQueryable<OutboundAndInboundDetail> 查询出入库明细清单(int? inboundId, int? outboundId, int? inboundType, int? outboundType) {
            return new OutboundAndInboundDetailAch(this).查询出入库明细清单(inboundId,outboundId,inboundType,outboundType);
        }
        
        [Query(HasSideEffects = true)]
        public List<OutboundAndInboundDetail> 查询待结算出入库明细清单后定价(int[] inboundIds, int[] outboundIds, int partsSalesCategoryId, int supplierId) {
            return new OutboundAndInboundDetailAch(this).查询待结算出入库明细清单后定价(inboundIds,outboundIds,partsSalesCategoryId,supplierId);
        }

        public IQueryable<OutboundAndInboundWithDetail> 查询配件历史入库记录(int? partId, int? warehouseId) {
            return new OutboundAndInboundDetailAch(this).查询配件历史入库记录(partId,warehouseId);
        }

        public IQueryable<OutboundAndInboundWithDetail> 查询配件历史出库记录(int? partId, int? warehouseId) {
            return new OutboundAndInboundDetailAch(this).查询配件历史出库记录(partId,warehouseId);
        }
    }
}
