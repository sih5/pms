﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Sunlight.Silverlight.Dcs.Web.Entities;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsDifferenceBackBillAch : DcsSerivceAchieveBase {
        internal void InsertPartsDifferenceBackBillValidate(PartsDifferenceBackBill partsDifferenceBackBill) {
            var userInfo = Utils.GetCurrentUserInfo();
            var company = ObjectContext.Companies.Single(r => r.Id == userInfo.EnterpriseId);
            partsDifferenceBackBill.Code = CodeGenerator.Generate("PartsDifferenceBackBill", company.Code);
            partsDifferenceBackBill.CreatorId = userInfo.Id;
            partsDifferenceBackBill.CreatorName = userInfo.Name;
            partsDifferenceBackBill.CreateTime = DateTime.Now;
        }

        internal void UpdatePartsDifferenceBackBillValidate(PartsDifferenceBackBill partsDifferenceBackBill) {
            var userInfo = Utils.GetCurrentUserInfo();
            partsDifferenceBackBill.ModifierId = userInfo.Id;
            partsDifferenceBackBill.ModifierName = userInfo.Name;
            partsDifferenceBackBill.ModifyTime = DateTime.Now;
        }

        private void ValidateTraces(PartsDifferenceBackBill partsDifferenceBackBill) {
            var userInfo = Utils.GetCurrentUserInfo();
            //  判断追溯信息
            var tracecodes = ObjectContext.AccurateTraces.Where(t => t.CompanyId == userInfo.EnterpriseId && t.Status == (int)DCSAccurateTraceStatus.有效 && t.Type == (int)DCSAccurateTraceType.入库 && t.OutQty == null).ToArray();
            foreach(var item in partsDifferenceBackBill.PartsDifferenceBackBillDtls) {
                if(item.TraceProperty.HasValue && !string.IsNullOrEmpty(item.TraceCode)) {
                    var newTraces = item.TraceCode.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries).ToArray();
                    if(item.TraceProperty.Value == (int)DCSTraceProperty.精确追溯) {
                        var oldTrace = tracecodes.Where(t => newTraces.Contains(t.TraceCode)).ToArray();
                        if(oldTrace.Count() == 0) {
                            throw new ValidationException("未找到有效的追溯码:" + item.TraceCode);
                        }
                        foreach(var trace in newTraces) {
                            if(!oldTrace.Any(t => t.TraceCode == trace)) {
                                throw new ValidationException("未找到" + item.SparePartCode + "有效的追溯码:" + trace);
                            }
                        }
                    } else {
                        var oldTrace = tracecodes.Where(t => t.TraceCode == newTraces[0]).FirstOrDefault();
                        if(oldTrace == null) {
                            throw new ValidationException("未找到有效的追溯码:" + item.TraceCode);
                        } else if(partsDifferenceBackBill.Type==(int)DCSPartsDifferenceBackBillType.上架 && oldTrace.InQty.HasValue && oldTrace.InQty.Value < item.DiffQuantity) {
                            throw new ValidationException(item.SparePartCode + "追溯码:" + newTraces[0] + "的入库数量小于差异数量");
                        }
                    }
                }
            }
        }
        public void InsertPartsDifferenceBackBill(PartsDifferenceBackBill partsDifferenceBackBill) {
            this.ValidateTraces(partsDifferenceBackBill);
            InsertToDatabase(partsDifferenceBackBill);
            var PartsDifferenceBackBillDtls = ChangeSet.GetAssociatedChanges(partsDifferenceBackBill, v => v.PartsDifferenceBackBillDtls);
            foreach(PartsDifferenceBackBillDtl partsDifferenceBackBillDtl in PartsDifferenceBackBillDtls) {
                InsertToDatabase(partsDifferenceBackBillDtl);
            }
            this.InsertPartsDifferenceBackBillValidate(partsDifferenceBackBill);
            if(partsDifferenceBackBill.Type != (int)DCSPartsDifferenceBackBillType.入库) {
                var partsInboundCheckBill = ObjectContext.PartsInboundCheckBills.First(o => o.Code == partsDifferenceBackBill.PartsInboundCheckBillCode);
                partsInboundCheckBill.IsDifference = true;
            }
        }

        public void UpdatePartsDifferenceBackBill(PartsDifferenceBackBill partsDifferenceBackBill) {
            partsDifferenceBackBill.PartsDifferenceBackBillDtls.Clear();
            UpdateToDatabase(partsDifferenceBackBill);
            var PartsDifferenceBackBillDtls = ChangeSet.GetAssociatedChanges(partsDifferenceBackBill, v => v.PartsDifferenceBackBillDtls);
            foreach(PartsDifferenceBackBillDtl partsDifferenceBackBillDtl in PartsDifferenceBackBillDtls) {
                //获取清单实体对象的操作类型
                switch(ChangeSet.GetChangeOperation(partsDifferenceBackBillDtl)) {
                    //新增操作
                    case ChangeOperation.Insert:
                        InsertToDatabase(partsDifferenceBackBillDtl);
                        break;
                    //更新操作
                    case ChangeOperation.Update:
                        UpdateToDatabase(partsDifferenceBackBillDtl);
                        break;
                    //删除操作
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(partsDifferenceBackBillDtl);
                        break;
                }
            }
            this.ValidateTraces(partsDifferenceBackBill);
            this.UpdatePartsDifferenceBackBillValidate(partsDifferenceBackBill);
        }

        public IQueryable<PartsDifferenceBackBill> GetPartsDifferenceBackBillConditions(string code, int? status, int? type, string taskCode,
           DateTime? beginCreateTime, DateTime? endCreateTime, DateTime? beginSubmitTime, DateTime? endSubmitTime) {
            var result = ObjectContext.PartsDifferenceBackBills.Include("PartsDifferenceBackBillDtls").Where(o => 1 == 1);
            if(!string.IsNullOrEmpty(code))
                result = result.Where(r => r.Code.ToUpper().Contains(code.ToUpper()));
            if(status.HasValue)
                result = result.Where(o => o.Status == status);
            if(type.HasValue)
                result = result.Where(o => o.Type == type);
            if(!string.IsNullOrEmpty(taskCode))
                result = result.Where(o => ObjectContext.PartsDifferenceBackBillDtls.Any(r => r.TaskCode == taskCode && r.PartsDifferenceBackBillId == o.Id));           
            if(beginCreateTime.HasValue)
                result = result.Where(r => r.CreateTime >= beginCreateTime);
            if(endCreateTime.HasValue)
                result = result.Where(r => r.CreateTime <= endCreateTime);
            if(beginSubmitTime.HasValue)
                result = result.Where(r => r.SubmitTime >= beginSubmitTime);
            if(endSubmitTime.HasValue)
                result = result.Where(r => r.SubmitTime <= endSubmitTime);
            return result.OrderByDescending(o => o.CreateTime);
        }

        public PartsDifferenceBackBill GetPartsDifferenceBackBillById(int id) {
            var partsDifferenceBackBill = ObjectContext.PartsDifferenceBackBills.Include("PartsDifferenceBackBillDtls").SingleOrDefault(e => e.Id == id);
            if(partsDifferenceBackBill.Type == (int)DCSPartsDifferenceBackBillType.包装) {
                var packingTasks = ObjectContext.PackingTasks.Where(o => o.SourceCode == partsDifferenceBackBill.SourceCode && (o.Status == (int)DcsPackingTaskStatus.新增 || o.Status == (int)DcsPackingTaskStatus.部分包装)
                        && o.PlanQty > (o.PackingQty ?? 0)).ToList();
                if(packingTasks.Any()) {
                    foreach(var item in partsDifferenceBackBill.PartsDifferenceBackBillDtls) {
                        var packingTask = packingTasks.FirstOrDefault(o => o.SparePartId == item.SparePartId && o.Id == item.TaskId);
                        item.OnlyDiffQuantity = packingTask.PlanQty - (packingTask.PackingQty ?? 0);
                    }
                }
            }
            if(partsDifferenceBackBill.Type == (int)DCSPartsDifferenceBackBillType.上架) {
                var partsShelvesTasks = ObjectContext.PartsShelvesTasks.Where(o => o.SourceBillCode == partsDifferenceBackBill.SourceCode && (o.Status == (int)DcsPartsShelvesTaskStatus.新增 || o.Status == (int)DcsPartsShelvesTaskStatus.部分上架)
                        && o.PlannedAmount > (o.ShelvesAmount ?? 0)).ToList();
                if(partsShelvesTasks.Any()) {
                    foreach(var item in partsDifferenceBackBill.PartsDifferenceBackBillDtls) {
                        var partsShelvesTask = partsShelvesTasks.FirstOrDefault(o => o.SparePartId == item.SparePartId && o.Id == item.TaskId);
                        item.OnlyDiffQuantity = partsShelvesTask.PlannedAmount.Value - (partsShelvesTask.ShelvesAmount ?? 0);
                    }
                }
            }
            if(partsDifferenceBackBill.Type == (int)DCSPartsDifferenceBackBillType.入库) {
                var partsInboundPlanDetails = ObjectContext.PartsInboundPlanDetails.Where(o => o.PartsInboundPlan.OriginalRequirementBillCode == partsDifferenceBackBill.SourceCode && (o.PartsInboundPlan.Status == (int)DcsPartsInboundPlanStatus.新建 || o.PartsInboundPlan.Status == (int)DcsPartsInboundPlanStatus.部分检验)
                        && o.PlannedAmount > (o.InspectedQuantity ?? 0)).ToList();
                foreach(var item in partsDifferenceBackBill.PartsDifferenceBackBillDtls) {
                    var partsInboundPlanDetail = partsInboundPlanDetails.FirstOrDefault(o => o.SparePartId == item.SparePartId);
                    item.OnlyDiffQuantity = partsInboundPlanDetail.PlannedAmount - (partsInboundPlanDetail.InspectedQuantity ?? 0);
                }
            }

            return partsDifferenceBackBill;
        }

        public IQueryable<VirtualPartsDifferenceBackBillSourceCode> GetPartsDifferenceBackBillSourceCodes(string sourceCode, string code, int type) {
            var createTime = DateTime.Now.AddDays(1 - DateTime.Now.Day).Date;
            var result = from a in ObjectContext.PackingTasks.Where(o => (o.Status == (int)DcsPackingTaskStatus.新增 || o.Status == (int)DcsPackingTaskStatus.部分包装) && o.PlanQty > (o.PackingQty ?? 0) &&
               ObjectContext.PartsInboundCheckBills.Any(r => r.CreateTime > createTime && r.Id == o.PartsInboundCheckBillId && r.SettlementStatus != (int)DcsPartsSettlementStatus.已结算))
                         join b in ObjectContext.PartsInboundPlans on a.PartsInboundPlanId equals b.Id
                         select new VirtualPartsDifferenceBackBillSourceCode {
                             Id = a.Id,
                             SourceCode = a.SourceCode,
                             Code = a.Code,
                             PartsInboundCheckBillCode = a.PartsInboundCheckBillCode,
                             WarehouseCode = a.WarehouseCode,
                             WarehouseName = a.WarehouseName,
                             Status = a.Status == (int)DcsPackingTaskStatus.新增 ? "新增" : "部分包装",
                             CreatorName = a.CreatorName,
                             CreateTime = a.CreateTime,
                             OriginalRequirementBillId = b.OriginalRequirementBillId,
                             OriginalRequirementBillCode = b.OriginalRequirementBillCode,
                             PartsSupplierId = b.CounterpartCompanyId,
                             PartsSupplierCode = b.CounterpartCompanyCode,
                             PartsSupplierName = b.CounterpartCompanyName
                         };
            if(type == (int)DCSPartsDifferenceBackBillType.入库) {
                result = ObjectContext.PartsInboundPlans.Where(o => (o.Status == (int)DcsPartsInboundPlanStatus.新建 || o.Status == (int)DcsPartsInboundPlanStatus.部分检验) &&
                           ObjectContext.PartsInboundPlanDetails.Any(r => r.PartsInboundPlanId == o.Id && r.PlannedAmount > (r.InspectedQuantity ?? 0))).Select(o => new VirtualPartsDifferenceBackBillSourceCode {
                               Id = o.Id,
                               SourceCode = o.OriginalRequirementBillCode,
                               Code = o.Code,
                               WarehouseCode = o.WarehouseCode,
                               WarehouseName = o.WarehouseName,
                               Status = o.Status == (int)DcsPartsInboundPlanStatus.新建 ? "新建" : "部分检验",
                               CreatorName = o.CreatorName,
                               CreateTime = o.CreateTime,
                               OriginalRequirementBillId = o.OriginalRequirementBillId,
                               OriginalRequirementBillCode = o.OriginalRequirementBillCode,
                               PartsSupplierId = o.CounterpartCompanyId,
                               PartsSupplierCode = o.CounterpartCompanyCode,
                               PartsSupplierName = o.CounterpartCompanyName
                           });
            }

            if(type == (int)DCSPartsDifferenceBackBillType.上架) {
                result = from a in ObjectContext.PartsShelvesTasks.Where(o => (o.Status == (int)DcsPartsShelvesTaskStatus.新增 || o.Status == (int)DcsPartsShelvesTaskStatus.部分上架)
                       && o.PlannedAmount > (o.ShelvesAmount ?? 0) && ObjectContext.PartsInboundCheckBills.Any(r => r.CreateTime > createTime && r.Id == o.PartsInboundCheckBillId && r.SettlementStatus != (int)DcsPartsSettlementStatus.已结算))
                         join b in ObjectContext.PartsInboundPlans on a.PartsInboundPlanId equals b.Id
                         select new VirtualPartsDifferenceBackBillSourceCode {
                             Id = a.Id,
                             SourceCode = a.SourceBillCode,
                             Code = a.Code,
                             PartsInboundCheckBillCode = a.PartsInboundCheckBillCode,
                             WarehouseCode = a.WarehouseCode,
                             WarehouseName = a.WarehouseName,
                             Status = a.Status == (int)DcsPartsShelvesTaskStatus.新增 ? "新增" : "部分上架",
                             CreatorName = a.CreatorName,
                             CreateTime = a.CreateTime,
                             OriginalRequirementBillId = b.OriginalRequirementBillId,
                             OriginalRequirementBillCode = b.OriginalRequirementBillCode,
                             PartsSupplierId = b.CounterpartCompanyId,
                             PartsSupplierCode = b.CounterpartCompanyCode,
                             PartsSupplierName = b.CounterpartCompanyName
                         };
            }

            if(!string.IsNullOrWhiteSpace(sourceCode))
                result = result.Where(o => o.SourceCode == sourceCode);
            if(!string.IsNullOrWhiteSpace(code))
                result = result.Where(o => o.Code == code);
            if(type != (int)DCSPartsDifferenceBackBillType.入库) {
                result = result.Where(t => !ObjectContext.PartsInboundCheckBill_Sync.Any(r => r.Objid == t.PartsInboundCheckBillCode && r.IOStatus == 2));
            }
            return result.OrderByDescending(o => o.CreateTime);
        }
    }

    partial class DcsDomainService {
        //原分布类的函数全部转移到Ach                                                               
        public void InsertPartsDifferenceBackBill(PartsDifferenceBackBill partsDifferenceBackBill) {
            new PartsDifferenceBackBillAch(this).InsertPartsDifferenceBackBill(partsDifferenceBackBill);
        }

        public void UpdatePartsDifferenceBackBill(PartsDifferenceBackBill partsDifferenceBackBill) {
            new PartsDifferenceBackBillAch(this).UpdatePartsDifferenceBackBill(partsDifferenceBackBill);
        }

        public IQueryable<PartsDifferenceBackBill> GetPartsDifferenceBackBillConditions(string code, int? status, int? type, string taskCode,
           DateTime? beginCreateTime, DateTime? endCreateTime, DateTime? beginSubmitTime, DateTime? endSubmitTime) {
               return new PartsDifferenceBackBillAch(this).GetPartsDifferenceBackBillConditions(code, status, type, taskCode, beginCreateTime, endCreateTime, beginSubmitTime, endSubmitTime);
        }

        public IQueryable<VirtualPartsDifferenceBackBillSourceCode> GetPartsDifferenceBackBillSourceCodes(string sourceCode, string code, int type) {
            return new PartsDifferenceBackBillAch(this).GetPartsDifferenceBackBillSourceCodes(sourceCode, code, type);
        }

        public PartsDifferenceBackBill GetPartsDifferenceBackBillById(int id) {
            return new PartsDifferenceBackBillAch(this).GetPartsDifferenceBackBillById(id);
        }
    }
}
