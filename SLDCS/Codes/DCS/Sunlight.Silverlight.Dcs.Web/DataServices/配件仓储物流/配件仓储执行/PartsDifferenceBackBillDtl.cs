﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunlight.Silverlight.Dcs.Web.Entities;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsDifferenceBackBillDtlAch : DcsSerivceAchieveBase {
        public PartsDifferenceBackBillDtlAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public void InsertPartsDifferenceBackBillDtl(PartsDifferenceBackBillDtl partsDifferenceBackBillDtl) {
        }

        public void UpdatePartsDifferenceBackBillDtl(PartsDifferenceBackBillDtl partsDifferenceBackBillDtl) {
        }

        public IQueryable<VirtualPartsDifferenceBackBillDtl>  可回传配件差异回退单清单(string partCode, string partName, string code, int type) {
            switch(type) {
                case (int)DCSPartsDifferenceBackBillType.上架:
                    var result = from a in ObjectContext.PartsShelvesTasks.Where(o => o.SourceBillCode == code && (o.Status == (int)DcsPartsShelvesTaskStatus.新增 || o.Status == (int)DcsPartsShelvesTaskStatus.部分上架)
                        && o.PlannedAmount > (o.ShelvesAmount ?? 0))
                                 join b in ObjectContext.SpareParts on a.SparePartId equals b.Id
                                 where b.Status == (int)DcsMasterDataStatus.有效
                                 select new VirtualPartsDifferenceBackBillDtl {
                                     SparePartId = a.SparePartId.Value,
                                     SparePartCode = b.Code,
                                     SparePartName = b.Name,
                                     DiffQuantity = a.PlannedAmount.Value - (a.ShelvesAmount ?? 0),
                                     SourceCode = a.SourceBillCode,
                                     TaskId = a.Id,
                                     TaskCode = a.Code,
                                     TraceProperty=b.TraceProperty
                                 };
                    return result;
                case (int)DCSPartsDifferenceBackBillType.包装:
                    var resultPacking = from o in ObjectContext.PackingTasks.Where(o => o.SourceCode == code && (o.Status == (int)DcsPackingTaskStatus.新增 || o.Status == (int)DcsPackingTaskStatus.部分包装)
                       && o.PlanQty > (o.PackingQty ?? 0))
                                        join b in ObjectContext.SpareParts on o.SparePartId equals b.Id
                                        select new VirtualPartsDifferenceBackBillDtl {
                                            TaskId = o.Id,
                                            TaskCode = o.Code,
                                            SparePartId = o.SparePartId.Value,
                                            SparePartCode = o.SparePartCode,
                                            SparePartName = o.SparePartName,
                                            DiffQuantity = o.PlanQty - (o.PackingQty ?? 0),
                                            SourceCode = o.SourceCode,
                                            TraceProperty = b.TraceProperty
                                        };
                    return resultPacking;
                case (int)DCSPartsDifferenceBackBillType.入库:
                    var resultInPlan = from o in ObjectContext.PartsInboundPlanDetails.Where(o => o.PartsInboundPlan.OriginalRequirementBillCode == code && (o.PartsInboundPlan.Status == (int)DcsPartsInboundPlanStatus.新建 || o.PartsInboundPlan.Status == (int)DcsPartsInboundPlanStatus.部分检验)
                        && o.PlannedAmount > (o.InspectedQuantity ?? 0))
                                       join b in ObjectContext.SpareParts on o.SparePartId equals b.Id
                                       select new VirtualPartsDifferenceBackBillDtl {
                                           TaskId = o.Id,
                                           SparePartId = o.SparePartId,
                                           SparePartCode = o.SparePartCode,
                                           SparePartName = o.SparePartName,
                                           DiffQuantity = o.PlannedAmount - (o.InspectedQuantity ?? 0),
                                           SourceCode = o.PartsInboundPlan.OriginalRequirementBillCode,
                                           TraceProperty = b.TraceProperty
                                       };
                    return resultInPlan;
            }
            return null;
        }
    }

    partial class DcsDomainService {
        public void InsertPartsDifferenceBackBillDtl(PartsDifferenceBackBillDtl partsDifferenceBackBillDtl) {
            new PartsDifferenceBackBillDtlAch(this).InsertPartsDifferenceBackBillDtl(partsDifferenceBackBillDtl);
        }

        public void UpdatePartsDifferenceBackBillDtl(PartsDifferenceBackBillDtl partsDifferenceBackBillDtl) {
            new PartsDifferenceBackBillDtlAch(this).UpdatePartsDifferenceBackBillDtl(partsDifferenceBackBillDtl);
        }

        public IQueryable<VirtualPartsDifferenceBackBillDtl> 可回传配件差异回退单清单(string partCode, string partName, string code, int type) {
            return new PartsDifferenceBackBillDtlAch(this).可回传配件差异回退单清单(partCode, partName, code, type);
        }
    }
}
