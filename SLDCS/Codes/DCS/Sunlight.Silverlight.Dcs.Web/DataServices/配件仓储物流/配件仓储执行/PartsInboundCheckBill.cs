﻿using System;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsInboundCheckBillAch : DcsSerivceAchieveBase {
        internal void InsertPartsInboundCheckBillValidate(PartsInboundCheckBill partsInboundCheckBill) {
            if(string.IsNullOrWhiteSpace(partsInboundCheckBill.Code) || partsInboundCheckBill.Code == GlobalVar.ASSIGNED_BY_SERVER)
                partsInboundCheckBill.Code = CodeGenerator.Generate("PartsInboundCheckBill", partsInboundCheckBill.StorageCompanyCode);
            var userInfo = Utils.GetCurrentUserInfo();
            partsInboundCheckBill.CreatorId = userInfo.Id;
            partsInboundCheckBill.CreatorName = userInfo.Name;
            partsInboundCheckBill.CreateTime = DateTime.Now;
        }

        internal void UpdatePartsInboundCheckBillValidate(PartsInboundCheckBill partsInboundCheckBill) {

            var userInfo = Utils.GetCurrentUserInfo();
            partsInboundCheckBill.ModifierId = userInfo.Id;
            partsInboundCheckBill.ModifierName = userInfo.Name;
            partsInboundCheckBill.ModifyTime = DateTime.Now;
        }
        internal void InsertPartsPurchaseSettleRefValidate(PartsPurchaseSettleRef partsPurchaseSettleRef) {
            var userInfo = Utils.GetCurrentUserInfo();
        }

        internal void UpdatePartsPurchaseSettleRefValidate(PartsPurchaseSettleRef partsPurchaseSettleRef) {
            var userInfo = Utils.GetCurrentUserInfo();
        }

        public void InsertPartsInboundCheckBill(PartsInboundCheckBill partsInboundCheckBill) {
            InsertToDatabase(partsInboundCheckBill);
            var partsInboundCheckBillDetails = ChangeSet.GetAssociatedChanges(partsInboundCheckBill, v => v.PartsInboundCheckBillDetails, ChangeOperation.Insert);
            foreach(PartsInboundCheckBillDetail partsInboundCheckBillDetail in partsInboundCheckBillDetails) {
                InsertToDatabase(partsInboundCheckBillDetail);
            }
            var partsInboundPackingDetails = ChangeSet.GetAssociatedChanges(partsInboundCheckBill, v => v.PartsInboundPackingDetails, ChangeOperation.Insert);
            foreach(PartsInboundPackingDetail partsInboundPackingDetail in partsInboundPackingDetails) {
                InsertToDatabase(partsInboundPackingDetail);
            }
            this.InsertPartsInboundCheckBillValidate(partsInboundCheckBill);
        }

        public void UpdatePartsInboundCheckBill(PartsInboundCheckBill partsInboundCheckBill) {
            partsInboundCheckBill.PartsInboundCheckBillDetails.Clear();
            partsInboundCheckBill.PartsInboundPackingDetails.Clear();
            UpdateToDatabase(partsInboundCheckBill);
            var partsInboundCheckBillDetails = ChangeSet.GetAssociatedChanges(partsInboundCheckBill, v => v.PartsInboundCheckBillDetails);
            foreach(PartsInboundCheckBillDetail partsInboundCheckBillDetail in partsInboundCheckBillDetails) {
                switch(ChangeSet.GetChangeOperation(partsInboundCheckBillDetail)) {
                    case ChangeOperation.Insert:
                        InsertToDatabase(partsInboundCheckBillDetail);
                        break;
                    case ChangeOperation.Update:
                        UpdateToDatabase(partsInboundCheckBillDetail);
                        break;
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(partsInboundCheckBillDetail);
                        break;
                }
            }
            var partsInboundPackingDetails = ChangeSet.GetAssociatedChanges(partsInboundCheckBill, v => v.PartsInboundPackingDetails);
            foreach(PartsInboundPackingDetail partsInboundPackingDetail in partsInboundPackingDetails) {
                switch(ChangeSet.GetChangeOperation(partsInboundPackingDetail)) {
                    case ChangeOperation.Insert:
                        InsertToDatabase(partsInboundPackingDetail);
                        break;
                    case ChangeOperation.Update:
                        UpdateToDatabase(partsInboundPackingDetail);
                        break;
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(partsInboundPackingDetail);
                        break;
                }
            }
            this.UpdatePartsInboundCheckBillValidate(partsInboundCheckBill);
        }

        public PartsInboundCheckBill GetPartsInboundCheckBillWithPackingDetailsById(int id) {
            var dbPartsInboundCheckBill = ObjectContext.PartsInboundCheckBills.SingleOrDefault(e => e.Id == id);
            if(dbPartsInboundCheckBill != null) {
                var details = ObjectContext.PartsInboundPackingDetails.Where(e => e.PartsInboundCheckBillId == dbPartsInboundCheckBill.Id).ToArray();
            }
            return dbPartsInboundCheckBill;
        }

        public PartsInboundCheckBill GetPartsInboundCheckBillById(int id) {
            return ObjectContext.PartsInboundCheckBills.SingleOrDefault(e => e.Id == id);
        }

        public IQueryable<PartsInboundCheckBill> 仓库人员查询配件入库检验单(int? partsPurchaseOrderTypeId) {
            var userInfo = Utils.GetCurrentUserInfo();
            var partsInboundCheckBills1 = ObjectContext.PartsInboundCheckBills.Where(r => r.StorageCompanyId == userInfo.EnterpriseId);
            var partsInboundCheckBills = partsInboundCheckBills1.Where(r => ObjectContext.WarehouseOperators.Any(v => v.OperatorId == userInfo.Id && v.WarehouseId == r.WarehouseId) || ObjectContext.Warehouses.Any(v => v.Id == r.WarehouseId && v.Type == (int)DcsWarehouseType.虚拟库));
            if(partsPurchaseOrderTypeId.HasValue) {
                partsInboundCheckBills = partsInboundCheckBills.Where(x => x.InboundType == (int)DcsPartsInboundType.配件采购 && ObjectContext.PartsPurchaseOrders.Any(y => y.Id == x.OriginalRequirementBillId && partsPurchaseOrderTypeId == y.PartsPurchaseOrderTypeId));
            }
            return partsInboundCheckBills.Include("PartsInboundCheckBillDetails").Include("PartsSalesCategory").Include("PartsInboundPackingDetails").OrderByDescending(r => r.CreateTime);
        }

        public IQueryable<VirtualPartsInboundCheckBill> 仓库人员查询配件入库检验单New(int? partsPurchaseOrderTypeId, string sparePartCode, string sparePartName, string supplierPartCode,string batchNumber,bool? hasDifference) {
            var userInfo = Utils.GetCurrentUserInfo();
            var partsInboundCheckBills1 = ObjectContext.PartsInboundCheckBills.Where(r => r.StorageCompanyId == userInfo.EnterpriseId && ObjectContext.WarehouseOperators.Any(v => v.OperatorId == userInfo.Id && v.WarehouseId == r.WarehouseId));
            if(!string.IsNullOrEmpty(supplierPartCode)) {
                partsInboundCheckBills1 = from picb in ObjectContext.PartsInboundCheckBills.Where(r => r.StorageCompanyId == userInfo.EnterpriseId)
                                          join picbd in ObjectContext.PartsInboundCheckBillDetails on picb.Id equals picbd.PartsInboundCheckBillId
                                          where ObjectContext.PartsSupplierRelations.Any(o => o.PartsSalesCategoryId == picb.PartsSalesCategoryId &&
                                              o.PartId == picbd.SparePartId && (o.SupplierPartCode ?? "").IndexOf(supplierPartCode) >= 0)
                                          select picb;
            }

            if(sparePartCode != null) {
                partsInboundCheckBills1 = partsInboundCheckBills1.Where(r => ObjectContext.PartsInboundCheckBillDetails.Any(p => p.PartsInboundCheckBillId == r.Id && p.SparePartCode.Contains(sparePartCode)));
            }
            if(sparePartName != null) {
                partsInboundCheckBills1 = partsInboundCheckBills1.Where(r => ObjectContext.PartsInboundCheckBillDetails.Any(p => p.PartsInboundCheckBillId == r.Id && p.SparePartName.Contains(sparePartName)));
            }
            if(batchNumber != null) {
                partsInboundCheckBills1 = partsInboundCheckBills1.Where(r => ObjectContext.PartsInboundCheckBillDetails.Any(p => p.PartsInboundCheckBillId == r.Id && p.BatchNumber.Contains(batchNumber)));
            }
            //是否有差异单
            if (hasDifference == true) { 
                partsInboundCheckBills1 = partsInboundCheckBills1.Where(x => ObjectContext.PartsDifferenceBackBills.Any(y => y.Status != (int)DCSPartsDifferenceBackBillStatus.作废 && y.PartsInboundCheckBillCode==x.Code));
            }
            if (hasDifference == false) { 
                partsInboundCheckBills1 = partsInboundCheckBills1.Where(x => !ObjectContext.PartsDifferenceBackBills.Any(y => y.Status != (int)DCSPartsDifferenceBackBillStatus.作废 && y.PartsInboundCheckBillCode==x.Code));
            }
            var partsInboundCheckBills = partsInboundCheckBills1.Where(r => ObjectContext.WarehouseOperators.Any(v => v.OperatorId == userInfo.Id && v.WarehouseId == r.WarehouseId) || ObjectContext.Warehouses.Any(v => v.Id == r.WarehouseId && v.Type == (int)DcsWarehouseType.虚拟库));
            if(partsPurchaseOrderTypeId.HasValue) {
                partsInboundCheckBills = partsInboundCheckBills.Where(x => x.InboundType == (int)DcsPartsInboundType.配件采购 && ObjectContext.PartsPurchaseOrders.Any(y => y.Id == x.OriginalRequirementBillId && partsPurchaseOrderTypeId == y.PartsPurchaseOrderTypeId));
            }
            var detail = from a in ObjectContext.PartsInboundCheckBillDetails
                         join b in ObjectContext.PartsInboundCheckBills on a.PartsInboundCheckBillId equals b.Id
                         join c in ObjectContext.PartsRetailGuidePrices on new {
                             a.SparePartId,
                             b.BranchId,
                             b.PartsSalesCategoryId,
                             Status = (int)DcsBaseDataStatus.有效
                         } equals new {
                             c.SparePartId,
                             c.BranchId,
                             c.PartsSalesCategoryId,
                             c.Status
                         } into cc
                         from cs in cc.DefaultIfEmpty()
                         select new {
                             PartsInboundCheckBillId = b.Id,
                             InspectedQuantity = a.InspectedQuantity,
                             SettlementPrice = a.SettlementPrice,
                             RetailGuidePrice = cs.RetailGuidePrice
                         };
            var SumPartsInboundCheckBillDetailQuery = from a in detail
                                                      group a by a.PartsInboundCheckBillId into tempTable
                                                      select new {
                                                          PartsInboundCheckBillId = tempTable.Key,
                                                          SumTotalAmount = tempTable.Sum(r => r.InspectedQuantity * r.SettlementPrice),
                                                          TotalAmountForWarehouse = tempTable.Sum(r => r.InspectedQuantity * r.RetailGuidePrice)
                                                      };
            var result = from a in partsInboundCheckBills
                         join b in this.ObjectContext.PartsSalesCategories on a.PartsSalesCategoryId equals b.Id into tempTable
                         from t in tempTable.DefaultIfEmpty()
                         join c in this.ObjectContext.PartsInboundPlans on a.PartsInboundPlanId equals c.Id into tempTable1
                         from t1 in tempTable1.DefaultIfEmpty()
                         join d in this.ObjectContext.PartsSalesReturnBills on t1.OriginalRequirementBillId equals d.Id into tempTable2
                         from t2 in tempTable2.DefaultIfEmpty()
                         join f in SumPartsInboundCheckBillDetailQuery on a.Id equals f.PartsInboundCheckBillId into tempTable4
                         from t4 in tempTable4.DefaultIfEmpty()
                         join g in ObjectContext.PartsPurchaseOrders on a.OriginalRequirementBillId equals g.Id into tempTable5
                         from t5 in tempTable5.DefaultIfEmpty()
                         join h in ObjectContext.PartsPurchaseOrderTypes on t5.PartsPurchaseOrderTypeId equals h.Id into tempTable6
                         from t6 in tempTable6.DefaultIfEmpty()
                         select new VirtualPartsInboundCheckBill {
                             Id = a.Id,
                             Code = a.Code,
                             PartsInboundPlanId = a.PartsInboundPlanId,
                             WarehouseId = a.WarehouseId,
                             WarehouseCode = a.WarehouseCode,
                             WarehouseName = a.WarehouseName,
                             StorageCompanyId = a.StorageCompanyId,
                             StorageCompanyCode = a.StorageCompanyCode,
                             StorageCompanyName = a.StorageCompanyName,
                             StorageCompanyType = a.StorageCompanyType,
                             PartsSalesCategoryId = a.PartsSalesCategoryId,
                             PartsSalesCategoryName = t.Name,
                             BranchId = a.BranchId,
                             BranchCode = a.BranchCode,
                             BranchName = a.BranchName,
                             ReturnType = t2.ReturnType,
                             CounterpartCompanyId = a.CounterpartCompanyId,
                             CounterpartCompanyCode = a.CounterpartCompanyCode,
                             CounterpartCompanyName = a.CounterpartCompanyName,
                             OriginalRequirementBillId = a.OriginalRequirementBillId,
                             OriginalRequirementBillCode = a.OriginalRequirementBillCode,
                             OriginalRequirementBillType = a.OriginalRequirementBillType,
                             InboundType = a.InboundType,
                             SettlementStatus = a.SettlementStatus,
                             Status = a.Status,
                             CustomerAccountId = a.CustomerAccountId,
                             GPMSPurOrderCode = a.GPMSPurOrderCode,
                             PurOrderCode = a.PurOrderCode,
                             ReturnContainerNumber = a.ReturnContainerNumber,
                             SAPPurchasePlanCode = a.SAPPurchasePlanCode,
                             Remark = a.Remark,
                             CreateTime = a.CreateTime,
                             CreatorName = a.CreatorName,
                             ModifierId = a.ModifierId,
                             ModifierName = a.ModifierName,
                             PWMSStorageCode = a.PWMSStorageCode,
                             PartsInboundPlanCode = t1.Code,
                             ERPSourceOrderCode = a.ERPSourceOrderCode,
                             //PartsSalesOrderCode = t3.Code,
                             //InvoiceType = t3.InvoiceType,
                             TotalAmount = t4.SumTotalAmount,
                             Objid = a.Objid,
                             PartsPurchaseOrderTypeNameQuery = (a.InboundType == (int)DcsPartsInboundType.配件采购 ? t6.Name : ""),
                             PlanSourceQuery = (a.InboundType == (int)DcsPartsInboundType.配件采购 ? t5.PlanSource : ""),
                             CPPartsPurchaseOrderCode = a.CPPartsPurchaseOrderCode,
                             CPPartsInboundCheckCode = a.CPPartsInboundCheckCode,
                             TotalAmountForWarehouse = t4.TotalAmountForWarehouse,
                             HasDifference = ObjectContext.PartsDifferenceBackBills.Any(y => y.Status != (int)DCSPartsDifferenceBackBillStatus.作废 && y.PartsInboundCheckBillCode == a.Code)
                         };
            return result.OrderByDescending(r => r.CreateTime);
        }

        public IQueryable<PartsInboundCheckBill> 仓库人员查询配件入库检验单无价格(int? partsPurchaseOrderTypeId) {
            var userInfo = Utils.GetCurrentUserInfo();
            var partsInboundCheckBills = ObjectContext.PartsInboundCheckBills.Where(r => r.StorageCompanyId == userInfo.EnterpriseId);
            partsInboundCheckBills = partsInboundCheckBills.Where(r => ObjectContext.WarehouseOperators.Any(v => v.OperatorId == userInfo.Id && v.WarehouseId == r.WarehouseId));
            if(partsPurchaseOrderTypeId.HasValue) {
                partsInboundCheckBills = partsInboundCheckBills.Where(x => x.InboundType == (int)DcsPartsInboundType.配件采购 && ObjectContext.PartsPurchaseOrders.Any(y => y.Id == x.OriginalRequirementBillId && partsPurchaseOrderTypeId == y.PartsPurchaseOrderTypeId));
            }
            return partsInboundCheckBills.Include("PartsInboundCheckBillDetails").Include("PartsSalesCategory").Include("PartsInboundPackingDetails").OrderByDescending(r => r.CreateTime);
        }

        public IQueryable<PartsInboundCheckBill> GetPartsInboundCheckBillWithDetailBySupplierId() {
            var userInfo = Utils.GetCurrentUserInfo();
            var partsInboundCheckBills = ObjectContext.PartsInboundCheckBills.Where(r => r.CounterpartCompanyId == userInfo.EnterpriseId);
            return partsInboundCheckBills.Include("PartsInboundCheckBillDetails").Include("PartsSalesCategory").OrderBy(r => r.Id);
        }

        public IQueryable<Warehouse> GetWarehousesbyPersonnelId() {
            var userInfo = Utils.GetCurrentUserInfo();
            var warehouseOperators = ObjectContext.WarehouseOperators.Where(r => r.OperatorId == userInfo.Id).Select(r => r.WarehouseId);
            var warehouses = ObjectContext.Warehouses.Where(w => warehouseOperators.Contains(w.Id));
            return warehouses;
        }

    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:32
        //原分布类的函数全部转移到Ach                                                               

        public void InsertPartsInboundCheckBill(PartsInboundCheckBill partsInboundCheckBill) {
            new PartsInboundCheckBillAch(this).InsertPartsInboundCheckBill(partsInboundCheckBill);
        }

        public void UpdatePartsInboundCheckBill(PartsInboundCheckBill partsInboundCheckBill) {
            new PartsInboundCheckBillAch(this).UpdatePartsInboundCheckBill(partsInboundCheckBill);
        }

        public PartsInboundCheckBill GetPartsInboundCheckBillWithPackingDetailsById(int id) {
            return new PartsInboundCheckBillAch(this).GetPartsInboundCheckBillWithPackingDetailsById(id);
        }

        public IQueryable<PartsInboundCheckBill> 仓库人员查询配件入库检验单(int? partsPurchaseOrderTypeId) {
            return new PartsInboundCheckBillAch(this).仓库人员查询配件入库检验单(partsPurchaseOrderTypeId);
        }

        public IQueryable<VirtualPartsInboundCheckBill> 仓库人员查询配件入库检验单New(int? partsPurchaseOrderTypeId, string sparePartCode, string sparePartName, string supplierPartCode, string batchNumber, bool? hasDifference) {
            return new PartsInboundCheckBillAch(this).仓库人员查询配件入库检验单New(partsPurchaseOrderTypeId, sparePartCode, sparePartName, supplierPartCode, batchNumber, hasDifference);
        }

        public IQueryable<PartsInboundCheckBill> 仓库人员查询配件入库检验单无价格(int? partsPurchaseOrderTypeId) {
            return new PartsInboundCheckBillAch(this).仓库人员查询配件入库检验单无价格(partsPurchaseOrderTypeId);
        }

        public IQueryable<PartsInboundCheckBill> GetPartsInboundCheckBillWithDetailBySupplierId() {
            return new PartsInboundCheckBillAch(this).GetPartsInboundCheckBillWithDetailBySupplierId();
        }

        public IQueryable<Warehouse> GetWarehousesbyPersonnelId() {
            return new PartsInboundCheckBillAch(this).GetWarehousesbyPersonnelId();
        }

        public PartsInboundCheckBill GetPartsInboundCheckBillById(int id) {
            return new PartsInboundCheckBillAch(this).GetPartsInboundCheckBillById(id);
        }
    }
}
