﻿using System.Collections.Generic;
using System.Linq;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsInboundCheckBillDetailAch : DcsSerivceAchieveBase {
        public PartsInboundCheckBillDetailAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public IQueryable<PartsInboundCheckBillDetail> 按原始需求单据查询入库明细(int originalRequirementBillId, int originalRequirementBillType) {
            var partsInboundCheckBills = ObjectContext.PartsInboundCheckBills.Where(e => e.OriginalRequirementBillId == originalRequirementBillId && e.OriginalRequirementBillType == originalRequirementBillType).ToArray();
            if(partsInboundCheckBills.Length == 0)
                return null;
            var partsInboundCheckBillIds = partsInboundCheckBills.Select(e => e.Id);
            return ObjectContext.PartsInboundCheckBillDetails.Include("SparePart").Where(e => partsInboundCheckBillIds.Contains(e.PartsInboundCheckBillId)).OrderBy(r => r.Id);
        }

        public IEnumerable<VirtualPartsInboundCheckBillDetail> GetVirtualPartsInboundCheckBillDetailsWithSparePart(int id, int warehouseId) {

            var partsInboundCheckBillDetails = (from a in ObjectContext.PartsInboundCheckBillDetails.Where(r => r.PartsInboundCheckBillId == id)
                                                join e in ObjectContext.SpareParts on a.SparePartId equals e.Id
                                                join b in ObjectContext.PartsInboundCheckBills on a.PartsInboundCheckBillId equals b.Id
                                                join c in ObjectContext.PartsSupplierRelations.Where(r => r.Status != (int)DcsBaseDataStatus.作废) on new {
                                                    a.SparePartId,
                                                    b.CounterpartCompanyId
                                                } equals new {
                                                    SparePartId = c.PartId,
                                                    CounterpartCompanyId = c.SupplierId
                                                } into temp
                                                from t in temp.DefaultIfEmpty()
                                                select new VirtualPartsInboundCheckBillDetail {
                                                    Id = a.Id,
                                                    PartsInboundCheckBillId = a.PartsInboundCheckBillId,
                                                    SparePartId = a.SparePartId,
                                                    SparePartCode = a.SparePartCode,
                                                    SparePartName = a.SparePartName,
                                                    WarehouseAreaCode = a.WarehouseAreaCode,
                                                    InspectedQuantity = a.InspectedQuantity,
                                                    SettlementPrice = a.SettlementPrice,
                                                    SpareOrderRemark = a.SpareOrderRemark,
                                                    Remark = a.Remark,
                                                    SupplierPartCode = t.SupplierPartCode,
                                                    OverseasPartsFigure = e.OverseasPartsFigure,
                                                    MeasureUnit = e.MeasureUnit
                                                }).ToArray();
            var sparePartIds = partsInboundCheckBillDetails.Select(r => r.SparePartId).ToArray();
            var partsStockStruct = from a in ObjectContext.PartsStocks.Where(r => r.WarehouseId == warehouseId && sparePartIds.Any(v => v == r.PartId))
                                   join b in ObjectContext.WarehouseAreas.Where(r => r.AreaKind == (int)DcsAreaKind.库位) on a.WarehouseAreaId equals b.Id
                                   join c in ObjectContext.WarehouseAreaCategories.Where(r => r.Category == (int)DcsAreaType.保管区) on b.AreaCategoryId equals c.Id
                                   select new {
                                       a.PartId,
                                       b.Code
                                   };
            var structPart = partsStockStruct.GroupBy(r => new {
                r.PartId
            }).Select(r => new {
                partId = r.Key.PartId,
                areacode = r.Min(v => v.Code)
            }).ToArray();
            foreach(var item in partsInboundCheckBillDetails) {
                var temp = structPart.FirstOrDefault(r => r.partId == item.SparePartId);
                item.DestWarehouseAreaCode = temp == null ? "" : temp.areacode;
            }
            return partsInboundCheckBillDetails.OrderBy(r => r.Id);
        }

        public IEnumerable<PartsInboundCheckBillDetail> GetPartsInboundCheckBillDetailsWithSparePart(int id, int warehouseId) {
            var partsInboundCheckBill = ObjectContext.PartsInboundCheckBills.FirstOrDefault(r => r.Id == id);
            var partsInboundCheckBillDetails = ObjectContext.PartsInboundCheckBillDetails.Where(r => r.PartsInboundCheckBillId == id).OrderBy(r => r.SparePartCode).ToArray();
            var sparePartIds = partsInboundCheckBillDetails.Select(r => r.SparePartId).ToArray();
            var partsStockStruct = from a in ObjectContext.PartsStocks.Where(r => r.WarehouseId == warehouseId && sparePartIds.Any(v => v == r.PartId))
                                   join b in ObjectContext.WarehouseAreas.Where(r => r.AreaKind == (int)DcsAreaKind.库位) on a.WarehouseAreaId equals b.Id
                                   join c in ObjectContext.WarehouseAreaCategories.Where(r => r.Category == (int)DcsAreaType.保管区) on b.AreaCategoryId equals c.Id
                                   select new {
                                       a.PartId,
                                       b.Code
                                   };
            var structPart = partsStockStruct.GroupBy(r => new {
                r.PartId
            }).Select(r => new {
                partId = r.Key.PartId,
                areacode = r.Min(v => v.Code)
            }).ToArray();
            var spareparts = ObjectContext.SpareParts.Where(r => sparePartIds.Any(v => v == r.Id)).ToArray();

            var partsSupplierRelations = this.ObjectContext.PartsSupplierRelations.Where(r => sparePartIds.Contains(r.PartId) && r.Status == (int)DcsBaseDataStatus.有效 && r.PartsSalesCategoryId == partsInboundCheckBill.PartsSalesCategoryId).ToArray();
            foreach(var item in partsInboundCheckBillDetails) {
                var temp = spareparts.FirstOrDefault(r => r.Id == item.SparePartId);
                item.Unit = temp == null ? "" : temp.MeasureUnit;
                var newTemp = structPart.FirstOrDefault(r => r.partId == item.SparePartId);
                item.DestWarehouseAreaCode = newTemp == null ? "" : newTemp.areacode;

                var partsSupplierRelation = partsSupplierRelations.FirstOrDefault(r => r.PartId == item.SparePartId);
                item.SupplierPartCode = partsSupplierRelation == null ? "" : partsSupplierRelation.SupplierPartCode;
            }
            return partsInboundCheckBillDetails.OrderBy(r => r.SparePartCode);
        }
        public IEnumerable<VirtualPartsInboundCheckBillDetail> GetPartsInboundCheckBillDetailsWithSparePart2(int id, int warehouseId) {

            var partsInboundCheckBillDetails = (from a in ObjectContext.PartsInboundCheckBillDetails.Where(r => r.PartsInboundCheckBillId == id)
                                                join e in ObjectContext.SpareParts on a.SparePartId equals e.Id
                                                join b in ObjectContext.PartsInboundCheckBills on a.PartsInboundCheckBillId equals b.Id
                                                join q in ObjectContext.PartsRetailGuidePrices on new {
                                                    a.SparePartId,
                                                    b.BranchId,
                                                    b.PartsSalesCategoryId,
                                                    Status = (int)DcsBaseDataStatus.有效
                                                } equals new {
                                                    q.SparePartId,
                                                    q.BranchId,
                                                    q.PartsSalesCategoryId,
                                                    q.Status
                                                } into qq
                                                from qs in qq.DefaultIfEmpty()
                                                join c in ObjectContext.PartsSupplierRelations.Where(r => r.Status != (int)DcsBaseDataStatus.作废) on new {
                                                    a.SparePartId,
                                                    b.CounterpartCompanyId
                                                } equals new {
                                                    SparePartId = c.PartId,
                                                    CounterpartCompanyId = c.SupplierId
                                                } into temp
                                                from t in temp.DefaultIfEmpty()
                                                select new VirtualPartsInboundCheckBillDetail {
                                                    Id = a.Id,
                                                    PartsInboundCheckBillId = a.PartsInboundCheckBillId,
                                                    SparePartId = a.SparePartId,
                                                    SparePartCode = a.SparePartCode,
                                                    SparePartName = a.SparePartName,
                                                    WarehouseAreaCode = a.WarehouseAreaCode,
                                                    InspectedQuantity = a.InspectedQuantity,
                                                    SettlementPrice = qs.RetailGuidePrice,
                                                    SpareOrderRemark = a.SpareOrderRemark,
                                                    Remark = a.Remark,
                                                    SupplierPartCode = t.SupplierPartCode,
                                                    OverseasPartsFigure = e.OverseasPartsFigure,
                                                    MeasureUnit = e.MeasureUnit,
                                                    BatchNumber=a.BatchNumber
                                                }).OrderBy(r => r.SparePartCode).ToArray();
            var sparePartIds = partsInboundCheckBillDetails.Select(r => r.SparePartId).ToArray();
            var partsStockStruct = from a in ObjectContext.PartsStocks.Where(r => r.WarehouseId == warehouseId && sparePartIds.Any(v => v == r.PartId))
                                   join b in ObjectContext.WarehouseAreas.Where(r => r.AreaKind == (int)DcsAreaKind.库位) on a.WarehouseAreaId equals b.Id
                                   join c in ObjectContext.WarehouseAreaCategories.Where(r => r.Category == (int)DcsAreaType.保管区) on b.AreaCategoryId equals c.Id
                                   select new {
                                       a.PartId,
                                       b.Code
                                   };
            var structPart = partsStockStruct.GroupBy(r => new {
                r.PartId
            }).Select(r => new {
                partId = r.Key.PartId,
                areacode = r.Min(v => v.Code)
            }).ToArray();
            foreach(var item in partsInboundCheckBillDetails) {
                var temp = structPart.FirstOrDefault(r => r.partId == item.SparePartId);
                item.DestWarehouseAreaCode = temp == null ? "" : temp.areacode;
            }
            return partsInboundCheckBillDetails.OrderBy(r => r.SparePartCode);
        }



    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:32
        //原分布类的函数全部转移到Ach                                                               

        public IQueryable<PartsInboundCheckBillDetail> 按原始需求单据查询入库明细(int originalRequirementBillId, int originalRequirementBillType) {
            return new PartsInboundCheckBillDetailAch(this).按原始需求单据查询入库明细(originalRequirementBillId, originalRequirementBillType);
        }
        public IEnumerable<PartsInboundCheckBillDetail> GetPartsInboundCheckBillDetailsWithSparePart(int id, int warehouseId) {
            return new PartsInboundCheckBillDetailAch(this).GetPartsInboundCheckBillDetailsWithSparePart(id, warehouseId);
        }
        public IEnumerable<VirtualPartsInboundCheckBillDetail> GetPartsInboundCheckBillDetailsWithSparePart2(int id, int warehouseId) {
            return new PartsInboundCheckBillDetailAch(this).GetPartsInboundCheckBillDetailsWithSparePart2(id, warehouseId);
        }
        public IEnumerable<VirtualPartsInboundCheckBillDetail> GetVirtualPartsInboundCheckBillDetailsWithSparePart(int id, int warehouseId) {
            return new PartsInboundCheckBillDetailAch(this).GetVirtualPartsInboundCheckBillDetailsWithSparePart(id, warehouseId);
        }
    }
}