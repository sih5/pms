﻿using System;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsInboundPlanAch : DcsSerivceAchieveBase {
        internal void InsertPartsInboundPlanValidate(PartsInboundPlan partsInboundPlan) {
            if(string.IsNullOrWhiteSpace(partsInboundPlan.Code) || partsInboundPlan.Code == GlobalVar.ASSIGNED_BY_SERVER)
                partsInboundPlan.Code = CodeGenerator.Generate("PartsInboundPlan", partsInboundPlan.StorageCompanyCode);
            var userInfo = Utils.GetCurrentUserInfo();
            partsInboundPlan.CreatorId = userInfo.Id;
            partsInboundPlan.CreatorName = userInfo.Name;
            partsInboundPlan.CreateTime = DateTime.Now;
        }

        internal void UpdatePartsInboundPlanValidate(PartsInboundPlan partsInboundPlan) {

            var userInfo = Utils.GetCurrentUserInfo();
            partsInboundPlan.ModifierId = userInfo.Id;
            partsInboundPlan.ModifierName = userInfo.Name;
            partsInboundPlan.ModifyTime = DateTime.Now;
        }


        public void InsertPartsInboundPlan(PartsInboundPlan partsInboundPlan) {
            InsertToDatabase(partsInboundPlan);
            var partsInboundPlanDetails = ChangeSet.GetAssociatedChanges(partsInboundPlan, v => v.PartsInboundPlanDetails, ChangeOperation.Insert);
            foreach(PartsInboundPlanDetail partsInboundPlanDetail in partsInboundPlanDetails) {
                InsertToDatabase(partsInboundPlanDetail);
            }
            this.InsertPartsInboundPlanValidate(partsInboundPlan);
        }

        public void UpdatePartsInboundPlan(PartsInboundPlan partsInboundPlan) {
            partsInboundPlan.PartsInboundPlanDetails.Clear();
            UpdateToDatabase(partsInboundPlan);
            var partsInboundPlanDetails = ChangeSet.GetAssociatedChanges(partsInboundPlan, v => v.PartsInboundPlanDetails);
            foreach(PartsInboundPlanDetail partsInboundPlanDetail in partsInboundPlanDetails) {
                switch(ChangeSet.GetChangeOperation(partsInboundPlanDetail)) {
                    case ChangeOperation.Insert:
                        InsertToDatabase(partsInboundPlanDetail);
                        break;
                    case ChangeOperation.Update:
                        UpdateToDatabase(partsInboundPlanDetail);
                        break;
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(partsInboundPlanDetail);
                        break;
                }
            }
            this.UpdatePartsInboundPlanValidate(partsInboundPlan);
        }

        public IQueryable<PartsInboundPlan> 检验区负责人查询配件入库计划(string sparePartCode, string sparePartName) {
            var userInfo = Utils.GetCurrentUserInfo();

            var result = (from partsInboundPlans in ObjectContext.PartsInboundPlans.Where(r => r.StorageCompanyId == userInfo.EnterpriseId && (r.Status == (int)DcsPartsInboundPlanStatus.新建 || r.Status == (int)DcsPartsInboundPlanStatus.部分检验))
                          join partsInboundPlanDetails in ObjectContext.PartsInboundPlanDetails.Where(e => e.PlannedAmount - (e.InspectedQuantity ?? 0) > 0) on partsInboundPlans.Id equals partsInboundPlanDetails.PartsInboundPlanId
                          join warehouseOperator in ObjectContext.WarehouseOperators on new {
                              WarehouseId = partsInboundPlans.WarehouseId,
                              OperatorId = userInfo.Id
                          } equals new {
                              WarehouseId = warehouseOperator.WarehouseId,
                              OperatorId = warehouseOperator.OperatorId
                          }
                          select
                             partsInboundPlans
                           );
            if(!string.IsNullOrEmpty(sparePartCode)) {
                result = result.Where(x => ObjectContext.PartsInboundPlanDetails.Any(y => y.PartsInboundPlanId == x.Id && y.SparePartCode.Contains(sparePartCode)));
            }
            if(!string.IsNullOrEmpty(sparePartName)) {
                result = result.Where(x => ObjectContext.PartsInboundPlanDetails.Any(y => y.PartsInboundPlanId == x.Id && y.SparePartName.Contains(sparePartName)));
            }
            return result.Distinct().Include("PartsSalesCategory").Include("PartsInboundPlanDetails").OrderBy(v => v.Id);
        }

        public IQueryable<VirtualPartsInboundPlan> 检验入库查询配件入库计划(string sparePartCode, string sparePartName) {
           var userInfo = Utils.GetCurrentUserInfo();

            var result = (from partsInboundPlans in ObjectContext.PartsInboundPlans.Where(r => r.StorageCompanyId == userInfo.EnterpriseId && (r.Status == (int)DcsPartsInboundPlanStatus.新建 || r.Status == (int)DcsPartsInboundPlanStatus.部分检验))
                          join partsInboundPlanDetails in ObjectContext.PartsInboundPlanDetails.Where(e => e.PlannedAmount - (e.InspectedQuantity ?? 0) > 0) on partsInboundPlans.Id equals partsInboundPlanDetails.PartsInboundPlanId
                          join warehouseOperator in ObjectContext.WarehouseOperators on new {
                              WarehouseId = partsInboundPlans.WarehouseId,
                              OperatorId = userInfo.Id
                          } equals new {
                              WarehouseId = warehouseOperator.WarehouseId,
                              OperatorId = warehouseOperator.OperatorId
                          }
                          join partsSalesCategory in ObjectContext.PartsSalesCategories on partsInboundPlans.PartsSalesCategoryId equals partsSalesCategory.Id
                          select new VirtualPartsInboundPlan {
                              Id = partsInboundPlans.Id,
                              Code = partsInboundPlans.Code,
                              WarehouseId = partsInboundPlans.WarehouseId,
                              WarehouseCode = partsInboundPlans.WarehouseCode,
                              WarehouseName = partsInboundPlans.WarehouseName,
                              StorageCompanyId = partsInboundPlans.StorageCompanyId,
                              StorageCompanyCode = partsInboundPlans.StorageCompanyCode,
                              StorageCompanyName = partsInboundPlans.StorageCompanyName,
                              StorageCompanyType = partsInboundPlans.StorageCompanyType,
                              BranchId = partsInboundPlans.BranchId,
                              BranchCode = partsInboundPlans.BranchCode,
                              BranchName = partsInboundPlans.BranchName,
                              PartsSalesCategoryId = partsInboundPlans.PartsSalesCategoryId,
                              CounterpartCompanyId = partsInboundPlans.CounterpartCompanyId,
                              CounterpartCompanyCode = partsInboundPlans.CounterpartCompanyCode,
                              CounterpartCompanyName = partsInboundPlans.CounterpartCompanyName,
                              SourceId = partsInboundPlans.SourceId,
                              SourceCode = partsInboundPlans.SourceCode,
                              InboundType = partsInboundPlans.InboundType,
                              CustomerAccountId = partsInboundPlans.CustomerAccountId,
                              OriginalRequirementBillId = partsInboundPlans.OriginalRequirementBillId,
                              OriginalRequirementBillType = partsInboundPlans.OriginalRequirementBillType,
                              OriginalRequirementBillCode = partsInboundPlans.OriginalRequirementBillCode,
                              ERPSourceOrderCode = partsInboundPlans.ERPSourceOrderCode,
                              Status = partsInboundPlans.Status,
                              IfWmsInterface = partsInboundPlans.IfWmsInterface,
                              ArrivalDate = partsInboundPlans.ArrivalDate,
                              Remark = partsInboundPlans.Remark,
                              CreatorId = partsInboundPlans.CreatorId,
                              CreatorName = partsInboundPlans.CreatorName,
                              CreateTime = partsInboundPlans.CreateTime,
                              ModifierId = partsInboundPlans.ModifierId,
                              ModifierName = partsInboundPlans.ModifierName,
                              ModifyTime = partsInboundPlans.ModifyTime,
                              Priority = ObjectContext.PartsPriorityBills.Any(r => r.Priority == 1 && ObjectContext.PartsInboundPlanDetails.Any(x => x.SparePartId==r.PartId && partsInboundPlans.Id == x.PartsInboundPlanId)) == true ?
                              1 : (!ObjectContext.PartsPriorityBills.Any(r => r.Priority == 1 && ObjectContext.PartsInboundPlanDetails.Any(x => x.SparePartId==r.PartId && partsInboundPlans.Id == x.PartsInboundPlanId)) 
                              && ObjectContext.PartsPriorityBills.Any(r => r.Priority == 2 && ObjectContext.PartsInboundPlanDetails.Any(x => x.SparePartId==r.PartId && partsInboundPlans.Id == x.PartsInboundPlanId))) ? 2 : 99,
                              TotalAmount = ObjectContext.PartsInboundPlanDetails.Where(r => partsInboundPlans.Id == r.PartsInboundPlanId).Sum(r => r.Price * r.PlannedAmount),
                                PartsSalesCategoryName = partsSalesCategory.Name
                          }
                           );

            if(!string.IsNullOrEmpty(sparePartCode)) {
                result = result.Where(x => ObjectContext.PartsInboundPlanDetails.Any(y => y.PartsInboundPlanId == x.Id && y.SparePartCode.Contains(sparePartCode)));
            }
            if(!string.IsNullOrEmpty(sparePartName)) {
                result = result.Where(x => ObjectContext.PartsInboundPlanDetails.Any(y => y.PartsInboundPlanId == x.Id && y.SparePartName.Contains(sparePartName)));
            }
            return result.Distinct().OrderBy(r => r.Priority).ThenBy(v => v.Id);
        }

        public IQueryable<PartsInboundPlan> 检验区负责人查询配件入库计划无价格() {
            var userInfo = Utils.GetCurrentUserInfo();
            var partsInboundPlan = ObjectContext.PartsInboundPlans.Where(r => (r.Status == (int)DcsPartsInboundPlanStatus.新建 || r.Status == (int)DcsPartsInboundPlanStatus.部分检验) && r.IfWmsInterface == false);

            var warehouseIds = ObjectContext.WarehouseAreas.Where(v => v.Status == (int)DcsBaseDataStatus.有效 && ObjectContext.WarehouseAreaManagers.Any(r => r.ManagerId == userInfo.Id && r.WarehouseAreaId == v.TopLevelWarehouseAreaId)
                 && ObjectContext.WarehouseAreaCategories.Any(r => r.Category == (int)DcsAreaType.检验区 && r.Id == v.AreaCategoryId)).Select(r => r.WarehouseId);
            partsInboundPlan = partsInboundPlan.Where(r => warehouseIds.Contains(r.WarehouseId) && (r.Status == (int)DcsPartsInboundPlanStatus.新建 || r.Status == (int)DcsPartsInboundPlanStatus.部分检验) && !(ObjectContext.Warehouses.Any(o => o.Id == r.WarehouseId && o.Type != (int)DcsWarehouseType.虚拟库 && o.WmsInterface) && (r.InboundType == (int)DcsPartsInboundType.配件采购 || r.InboundType == (int)DcsPartsInboundType.销售退货 || r.InboundType == (int)DcsPartsInboundType.内部领入 || (r.InboundType == (int)DcsPartsInboundType.配件调拨 && (from a in ObjectContext.PartsTransferOrders
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           select 1).Any())))).Include("PartsInboundPlanDetails").OrderBy(r => r.Id);
            var result1 = (from partsInboundPlans in partsInboundPlan
                           join partsInboundPlanDetails in ObjectContext.PartsInboundPlanDetails.Where(e => e.PlannedAmount - (e.InspectedQuantity ?? 0) > 0) on partsInboundPlans.Id equals partsInboundPlanDetails.PartsInboundPlanId
                           join partsStock in ObjectContext.PartsStocks on partsInboundPlans.WarehouseId equals partsStock.WarehouseId // and partsInboundPlanDetails.SparePartId equals partsStock.PartId
                           join warehouseAreas in ObjectContext.WarehouseAreas.Where(v => v.Status == (int)DcsBaseDataStatus.有效) on partsStock.WarehouseAreaId equals warehouseAreas.Id
                           join warehouseAreaCategorys in ObjectContext.WarehouseAreaCategories on warehouseAreas.AreaCategoryId equals warehouseAreaCategorys.Id
                           where partsInboundPlanDetails.SparePartId == partsStock.PartId && warehouseAreas.Status == (int)DcsBaseDataStatus.有效 && warehouseAreaCategorys.Category == (int)DcsAreaType.保管区 && ObjectContext.WarehouseAreaManagers.Any(v => v.WarehouseAreaId == warehouseAreas.TopLevelWarehouseAreaId && v.ManagerId == userInfo.Id)
                           select
                               partsInboundPlans
                            );
            var result2 = (from partsInboundPlans in partsInboundPlan
                           join partsInboundPlanDetails in ObjectContext.PartsInboundPlanDetails.Where(e => e.PlannedAmount - (e.InspectedQuantity ?? 0) > 0) on partsInboundPlans.Id equals partsInboundPlanDetails.PartsInboundPlanId
                           where !(from partsStock in ObjectContext.PartsStocks  // and partsInboundPlanDetails.SparePartId equals partsStock.PartId
                                   join warehouseAreas in ObjectContext.WarehouseAreas.Where(v => v.Status == (int)DcsBaseDataStatus.有效) on partsStock.WarehouseAreaId equals warehouseAreas.Id
                                   join warehouseAreaCategorys in ObjectContext.WarehouseAreaCategories on warehouseAreas.AreaCategoryId equals warehouseAreaCategorys.Id
                                   where warehouseAreaCategorys.Category == (int)DcsAreaType.保管区 && partsInboundPlanDetails.SparePartId == partsStock.PartId && partsInboundPlans.WarehouseId == partsStock.WarehouseId
                                   select new {
                                       partsStock
                                   }).Any()
                           select
                               partsInboundPlans
                           );
            partsInboundPlan = result1.Union(result2).Distinct();
            return partsInboundPlan.Include("PartsSalesCategory").Include("PartsInboundPlanDetails").OrderBy(v => v.Id);
        }

        public IQueryable<PartsInboundPlan> 仓库人员查询配件入库计划(int? partsPurchaseOrderTypeId, string sparepartCode, string sparepartName, bool? hasDifference) {
            var userInfo = Utils.GetCurrentUserInfo();
            var partsInboundPlans = ObjectContext.PartsInboundPlans.Where(r => ObjectContext.WarehouseOperators.Any(v => v.OperatorId == userInfo.Id && v.WarehouseId == r.WarehouseId));
            if(partsPurchaseOrderTypeId.HasValue) {
                partsInboundPlans = partsInboundPlans.Where(x => x.InboundType == (int)DcsPartsInboundType.配件采购 && ObjectContext.PartsPurchaseOrders.Any(y => y.Id == x.OriginalRequirementBillId && partsPurchaseOrderTypeId == y.PartsPurchaseOrderTypeId));
            }
            if(!string.IsNullOrEmpty(sparepartCode)) {
                partsInboundPlans = partsInboundPlans.Where(x => ObjectContext.PartsInboundPlanDetails.Any(y => y.PartsInboundPlanId == x.Id && y.SparePartCode.Contains(sparepartCode)));
            }
            if(!string.IsNullOrEmpty(sparepartName)) {
                partsInboundPlans = partsInboundPlans.Where(x => ObjectContext.PartsInboundPlanDetails.Any(y => y.PartsInboundPlanId == x.Id && y.SparePartName.Contains(sparepartName)));
            }
            if (hasDifference == true) { 
                partsInboundPlans = partsInboundPlans.Where(x => ObjectContext.PartsDifferenceBackBills.Any(y => x.Code == y.SourceCode && y.Status != (int)DCSPartsDifferenceBackBillStatus.作废));
            }
            if (hasDifference == false) { 
                partsInboundPlans = partsInboundPlans.Where(x => !ObjectContext.PartsDifferenceBackBills.Any(y => x.Code == y.SourceCode && y.Status != (int)DCSPartsDifferenceBackBillStatus.作废));
            }
            var result = from a in partsInboundPlans
                         join b in this.ObjectContext.PartsSalesReturnBills on a.OriginalRequirementBillCode equals b.Code into tempTable
                         from t1 in tempTable.DefaultIfEmpty()
                         join d in this.ObjectContext.PartsTransferOrders on a.OriginalRequirementBillCode equals d.Code into tempTable2
                         from t3 in tempTable2.DefaultIfEmpty()
                         select new {
                             a,
                             t1.ReturnType,
                             HasDifference = ObjectContext.PartsDifferenceBackBills.Any(d => d.SourceCode == a.Code &&d.Status != (int)DCSPartsDifferenceBackBillStatus.作废)
                         };
            var result1 = result.Select(q => q.a);
            result1 = result1.OrderByDescending(r => r.Id);
            var ids1 = DcsDomainService.QueryComposer.Compose(result1, ParamQueryable).Cast<PartsInboundPlan>().Select(q => q.Id).ToArray();

            var temp = from q in result.Where(p => ids1.Contains(p.a.Id))
                       select new {
                           q.a,
                           ReturnType = q.ReturnType,
                           HasDifference = q.HasDifference
                       };
            foreach(var tc in temp) {
                tc.a.ReturnType = tc.ReturnType;
                tc.a.HasDifference = tc.HasDifference;
            }

            return result1.Include("PartsInboundPlanDetails").Include("PartsSalesCategory").OrderBy(r => r.Id);
        }

        public IQueryable<PartsInboundPlan> 仓库人员查询配件入库计划无价格(int? partsPurchaseOrderTypeId) {
            var userInfo = Utils.GetCurrentUserInfo();
            var partsInboundPlans = ObjectContext.PartsInboundPlans.Where(r => ObjectContext.WarehouseOperators.Any(v => v.OperatorId == userInfo.Id && v.WarehouseId == r.WarehouseId));
            if(partsPurchaseOrderTypeId.HasValue) {
                partsInboundPlans = partsInboundPlans.Where(x => x.InboundType == (int)DcsPartsInboundType.配件采购 && ObjectContext.PartsPurchaseOrders.Any(y => y.Id == x.OriginalRequirementBillId && partsPurchaseOrderTypeId == y.PartsPurchaseOrderTypeId));
            }

            var express = partsInboundPlans.Include("PartsInboundPlanDetails").Include("PartsSalesCategory").Include("PartsSalesReturnBill.PartsSalesOrder").OrderBy(r => r.Id);
            var totalCount = express.ComposeWithoutPaging(t => t, this.ParamQueryable).Count();
            var result = express.ComposeAllFilter(t => t, this.ParamQueryable).ToList();

            var ids = result.Where(r => r.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件销售退货单).Select(r => r.OriginalRequirementBillId).ToArray();
            if(ids.Any()) {
                var partsSalesReturnOrders = this.ObjectContext.PartsSalesReturnBills.Where(r => ids.Contains(r.Id)).ToList();
                foreach(var plan in result.Where(r => r.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件销售退货单).ToList()) {
                    var returnBill = partsSalesReturnOrders.FirstOrDefault(r => r.Id == plan.OriginalRequirementBillId);
                    if(returnBill != null) {
                        plan.ReturnType = returnBill.ReturnType;
                    }
                }
            }

            DomainService.SetQueryResultManually(result, totalCount);
            return null;
        }

        public PartsInboundPlan GetPartsInboundPlanWithDetails(int id) {
            var userInfo = Utils.GetCurrentUserInfo();
            var dbInboundPlan = ObjectContext.PartsInboundPlans.Include("PartsSalesCategory").SingleOrDefault(e => e.Id == id);
            var dbInboundPlans = ObjectContext.PartsInboundPlans.Where(e => e.Id == id);
            if(dbInboundPlan != null) {
                var details = ObjectContext.PartsInboundPlanDetails.Where(e => e.PartsInboundPlanId == id && (e.PlannedAmount - (e.InspectedQuantity ?? 0) > 0));

                var result1 = (from partsInboundPlans in dbInboundPlans
                               join partsInboundPlanDetails in ObjectContext.PartsInboundPlanDetails on partsInboundPlans.Id equals partsInboundPlanDetails.PartsInboundPlanId
                               join partsStock in ObjectContext.PartsStocks on partsInboundPlans.WarehouseId equals partsStock.WarehouseId // and partsInboundPlanDetails.SparePartId equals partsStock.PartId
                               join warehouseAreas in ObjectContext.WarehouseAreas.Where(v => v.Status == (int)DcsBaseDataStatus.有效) on partsStock.WarehouseAreaId equals warehouseAreas.Id
                               join warehouseAreaCategorys in ObjectContext.WarehouseAreaCategories on warehouseAreas.AreaCategoryId equals warehouseAreaCategorys.Id
                               where partsInboundPlanDetails.SparePartId == partsStock.PartId && warehouseAreas.Status == (int)DcsBaseDataStatus.有效 && warehouseAreaCategorys.Category == (int)DcsAreaType.保管区 && ObjectContext.WarehouseAreaManagers.Any(v => v.WarehouseAreaId == warehouseAreas.TopLevelWarehouseAreaId && v.ManagerId == userInfo.Id)
                               select
                                   partsInboundPlanDetails
                            );
                var result2 = (from partsInboundPlans in dbInboundPlans
                               join partsInboundPlanDetails in ObjectContext.PartsInboundPlanDetails on partsInboundPlans.Id equals partsInboundPlanDetails.PartsInboundPlanId
                               where !(from partsStock in ObjectContext.PartsStocks  // and partsInboundPlanDetails.SparePartId equals partsStock.PartId
                                       join warehouseAreas in ObjectContext.WarehouseAreas.Where(v => v.Status == (int)DcsBaseDataStatus.有效) on partsStock.WarehouseAreaId equals warehouseAreas.Id
                                       join warehouseAreaCategorys in ObjectContext.WarehouseAreaCategories on warehouseAreas.AreaCategoryId equals warehouseAreaCategorys.Id
                                       where warehouseAreaCategorys.Category == (int)DcsAreaType.保管区 && partsInboundPlanDetails.SparePartId == partsStock.PartId && partsInboundPlans.WarehouseId == partsStock.WarehouseId
                                       select new {
                                           partsStock
                                       }).Any()
                               select
                                   partsInboundPlanDetails
                               );
                details = result1.Union(result2).Distinct();
                var d = details.ToArray();               
                var trace = from a in details
                            join b in ObjectContext.SpareParts on a.SparePartId equals b.Id
                            select new {
                                a.SparePartId,
                                b.TraceProperty
                            };
                foreach(var detail in d) {
                    detail.PlannedAmount = detail.PlannedAmount - (detail.InspectedQuantity ?? 0);
                    var traceDe = trace.Where(t => t.SparePartId == detail.SparePartId).FirstOrDefault();
                    if(traceDe!=null&&traceDe.TraceProperty.HasValue){
                        detail.TraceProperty = traceDe.TraceProperty;
                    }
                }
                var tr = details.Where(r => r.PlannedAmount > 0).ToArray();
                //var billDetails = ObjectContext.PartsLogisticBatchBillDetails.Where(e => e.BillId == id && e.BillType == (int)DcsPartsLogisticBatchBillDetailBillType.配件入库计划).ToArray();
                //if(billDetails.Any()) {
                //    var batchIds = billDetails.Select(e => e.PartsLogisticBatchId).ToArray();
                //    var logisticBatches = ObjectContext.PartsLogisticBatches.Where(e => batchIds.Contains(e.Id)).ToArray();
                //    if(logisticBatches.Any()) {
                //        var itemDetails = ObjectContext.PartsLogisticBatchItemDetails.Where(e => batchIds.Contains(e.PartsLogisticBatchId)).ToArray();
                //    }
                //}
            }
            return dbInboundPlan;
        }

        public PartsInboundPlan GetPartsInboundPlanReceiptDetails(int id) {
            var userInfo = Utils.GetCurrentUserInfo();
            var dbInboundPlan = ObjectContext.PartsInboundPlans.Include("PartsSalesCategory").SingleOrDefault(e => e.Id == id);
            var dbInboundPlans = ObjectContext.PartsInboundPlans.Where(e => e.Id == id);
            if(dbInboundPlan != null) {
                //var details = ObjectContext.PartsInboundPlanDetails.Where(e => e.PartsInboundPlanId == id && (e.PlannedAmount - (e.InspectedQuantity ?? 0) > 0));

                //var result1 = (from partsInboundPlans in dbInboundPlans
                //               join partsInboundPlanDetails in ObjectContext.PartsInboundPlanDetails on partsInboundPlans.Id equals partsInboundPlanDetails.PartsInboundPlanId
                //               join partsStock in ObjectContext.PartsStocks on partsInboundPlans.WarehouseId equals partsStock.WarehouseId // and partsInboundPlanDetails.SparePartId equals partsStock.PartId
                //               join warehouseAreas in ObjectContext.WarehouseAreas.Where(v => v.Status == (int)DcsBaseDataStatus.有效) on partsStock.WarehouseAreaId equals warehouseAreas.Id
                //               join warehouseAreaCategorys in ObjectContext.WarehouseAreaCategories on warehouseAreas.AreaCategoryId equals warehouseAreaCategorys.Id
                //               where partsInboundPlanDetails.SparePartId == partsStock.PartId && warehouseAreas.Status == (int)DcsBaseDataStatus.有效 && warehouseAreaCategorys.Category == (int)DcsAreaType.检验区 && ObjectContext.WarehouseAreaManagers.Any(v => v.WarehouseAreaId == warehouseAreas.TopLevelWarehouseAreaId && v.ManagerId == userInfo.Id)
                //               select
                //                   partsInboundPlanDetails
                //            );
                //var result2 = (from partsInboundPlans in dbInboundPlans
                //               join partsInboundPlanDetails in ObjectContext.PartsInboundPlanDetails on partsInboundPlans.Id equals partsInboundPlanDetails.PartsInboundPlanId
                //               where !(from partsStock in ObjectContext.PartsStocks  // and partsInboundPlanDetails.SparePartId equals partsStock.PartId
                //                       join warehouseAreas in ObjectContext.WarehouseAreas.Where(v => v.Status == (int)DcsBaseDataStatus.有效) on partsStock.WarehouseAreaId equals warehouseAreas.Id
                //                       join warehouseAreaCategorys in ObjectContext.WarehouseAreaCategories on warehouseAreas.AreaCategoryId equals warehouseAreaCategorys.Id
                //                       where warehouseAreaCategorys.Category == (int)DcsAreaType.保管区 && partsInboundPlanDetails.SparePartId == partsStock.PartId && partsInboundPlans.WarehouseId == partsStock.WarehouseId
                //                       select new {
                //                           partsStock
                //                       }).Any()
                //               select
                //                   partsInboundPlanDetails
                //               );
                //var details = result1.Union(result2).Distinct().ToArray();

                var result = (from partsInboundPlans in dbInboundPlans
                              join partsInboundPlanDetail in ObjectContext.PartsInboundPlanDetails  on partsInboundPlans.Id equals partsInboundPlanDetail.PartsInboundPlanId
                              where (from warehouseAreas in ObjectContext.WarehouseAreas.Where(v => v.Status == (int)DcsBaseDataStatus.有效)
                                     join warehouseAreaCategorys in ObjectContext.WarehouseAreaCategories on warehouseAreas.AreaCategoryId equals warehouseAreaCategorys.Id
                                     where warehouseAreaCategorys.Category == (int)DcsAreaType.检验区 && partsInboundPlans.WarehouseId == warehouseAreas.WarehouseId && ObjectContext.WarehouseAreaManagers.Any(v => v.WarehouseAreaId == warehouseAreas.TopLevelWarehouseAreaId && v.ManagerId == userInfo.Id)
                                     select new {
                                         warehouseAreas
                                     }).Any()
                              select partsInboundPlanDetail);

                var details = result.Distinct().ToArray();
            }

            //关联供应商图号
            var partsSupplierRelations = (from partsInboundPlans in dbInboundPlans
                                          join partsInboundPlanDetail in ObjectContext.PartsInboundPlanDetails on partsInboundPlans.Id equals partsInboundPlanDetail.PartsInboundPlanId
                                          join partsSupplierRelation in ObjectContext.PartsSupplierRelations on new {
                                              SupplierId = partsInboundPlans.CounterpartCompanyId,
                                              SparePartId = partsInboundPlanDetail.SparePartId
                                          } equals new {
                                              SupplierId = partsSupplierRelation.SupplierId,
                                              SparePartId = partsSupplierRelation.PartId,                                             
                                          }
                                          where partsSupplierRelation.Status == (int)DcsBaseDataStatus.有效
                                          select partsSupplierRelation).ToArray();
            var spareParts = (from partsInboundPlans in dbInboundPlans
                              join partsInboundPlanDetail in ObjectContext.PartsInboundPlanDetails on partsInboundPlans.Id equals partsInboundPlanDetail.PartsInboundPlanId
                              join spare in ObjectContext.SpareParts on partsInboundPlanDetail.SparePartId equals spare.Id
                              select spare).ToArray();
            foreach(var item in dbInboundPlan.PartsInboundPlanDetails) {
                var partsSupplierRelation = partsSupplierRelations.FirstOrDefault(r => r.PartId == item.SparePartId);             
                item.SupplierPartCode = partsSupplierRelation == null ? "" : partsSupplierRelation.SupplierPartCode;

            }
            return dbInboundPlan;
        }
        public IQueryable<PartsInboundPlan> 仓库人员查询配件入库计划2(int? partsPurchaseOrderTypeId, string erpSourceOrderCode, string partsSalesOrderCode, string sparepartCode, string sparepartName, bool? hasDifference) {
            var userInfo = Utils.GetCurrentUserInfo();
            var partsInboundPlans = ObjectContext.PartsInboundPlans.Where(r => ObjectContext.WarehouseOperators.Any(v => v.OperatorId == userInfo.Id && v.WarehouseId == r.WarehouseId) );
            if(partsPurchaseOrderTypeId.HasValue) {
                partsInboundPlans = partsInboundPlans.Where(x => x.InboundType == (int)DcsPartsInboundType.配件采购 && ObjectContext.PartsPurchaseOrders.Any(y => y.Id == x.OriginalRequirementBillId && partsPurchaseOrderTypeId == y.PartsPurchaseOrderTypeId));
            }
            if(!string.IsNullOrEmpty(sparepartCode)) {
                partsInboundPlans = partsInboundPlans.Where(x => ObjectContext.PartsInboundPlanDetails.Any(y => y.PartsInboundPlanId == x.Id && y.SparePartCode.Contains(sparepartCode)));
            }
            if(!string.IsNullOrEmpty(sparepartName)) {
                partsInboundPlans = partsInboundPlans.Where(x => ObjectContext.PartsInboundPlanDetails.Any(y => y.PartsInboundPlanId == x.Id && y.SparePartName.Contains(sparepartName)));
            }
            if (hasDifference == true) { 
                partsInboundPlans = partsInboundPlans.Where(x => ObjectContext.PartsDifferenceBackBills.Any(y => x.Code == y.SourceCode && y.Status != (int)DCSPartsDifferenceBackBillStatus.作废));
            }
            if (hasDifference == false) { 
                partsInboundPlans = partsInboundPlans.Where(x => !ObjectContext.PartsDifferenceBackBills.Any(y => x.Code == y.SourceCode && y.Status != (int)DCSPartsDifferenceBackBillStatus.作废));
            }
            var detail = from a in ObjectContext.PartsInboundPlanDetails
                         join b in ObjectContext.PartsInboundPlans on a.PartsInboundPlanId equals b.Id 
                         join c in ObjectContext.PartsRetailGuidePrices on new {
                             a.SparePartId,
                             b.PartsSalesCategoryId,
                             b.BranchId,
                             Status = (int)DcsBaseDataStatus.有效
                         } equals new {
                             c.SparePartId,
                             c.PartsSalesCategoryId,
                             c.BranchId,
                             c.Status
                         }
                         select new {
                             PartsInboundPlanId = a.PartsInboundPlanId,
                             PlannedAmount = a.PlannedAmount,
                             RetailGuidePrice = c.RetailGuidePrice
                         };
            var SumPartsOutboundBillDetailQuery = from a in detail
                                                  group a by a.PartsInboundPlanId
                                                      into tempTable
                                                      select new {
                                                          PartsInboundPlanId = tempTable.Key,
                                                          TotalAmountForWarehouse = tempTable.Sum(r => r.PlannedAmount * r.RetailGuidePrice),
                                                      };
            var result = from a in partsInboundPlans
                         join b in this.ObjectContext.PartsSalesReturnBills on a.OriginalRequirementBillCode equals b.Code into tempTable
                         from t1 in tempTable.DefaultIfEmpty()
                         join e in ObjectContext.PartsSalesReturnBillDetails on t1.Id equals e.PartsSalesReturnBillId into tempTable3
                         from t4 in tempTable3.DefaultIfEmpty()
                         join c in this.ObjectContext.PartsSalesOrders on t4.PartsSalesOrderId equals c.Id into tempTable1
                         from t2 in tempTable1.DefaultIfEmpty()
                         join d in this.ObjectContext.PartsTransferOrders on a.OriginalRequirementBillCode equals d.Code into tempTable2
                         from t3 in tempTable2.DefaultIfEmpty()
                         join s in SumPartsOutboundBillDetailQuery on a.Id equals s.PartsInboundPlanId into ss
                          from y in ss.DefaultIfEmpty()
                         select new {
                             a,
                             ERPSourceOrderCode = a.ERPSourceOrderCode,
                             t2.Code,
                             t1.ReturnType,
                             y.TotalAmountForWarehouse,
                         };

            if(!string.IsNullOrEmpty(erpSourceOrderCode)) {
                result = result.Where(v => v.ERPSourceOrderCode == erpSourceOrderCode);
            }

            if(!string.IsNullOrEmpty(partsSalesOrderCode)) {
                result = result.Where(v => v.Code == partsSalesOrderCode);
            }

            var result1 = result.Select(q => q.a).Distinct();
            result1 = result1.OrderByDescending(r => r.Id);
            var ids1 = DcsDomainService.QueryComposer.Compose(result1, ParamQueryable).Cast<PartsInboundPlan>().Select(q => q.Id).ToArray();

            var temp = from q in result.Where(p => ids1.Contains(p.a.Id))
                       select new {
                           q.a,
                           ERPSourceOrderCode = q.ERPSourceOrderCode,
                           PartsSalesOrderCode = q.Code,
                           ReturnType = q.ReturnType,
                           TotalAmountForWarehouse = q.TotalAmountForWarehouse,
                           HasDifference = ObjectContext.PartsDifferenceBackBills.Any(d => d.SourceCode == q.a.Code &&d.Status != (int)DCSPartsDifferenceBackBillStatus.作废)
                       };
            foreach(var tc in temp) {
                tc.a.ERPSourceOrderCode = tc.ERPSourceOrderCode;
                tc.a.PartsSalesOrderCode = tc.PartsSalesOrderCode;
                tc.a.ReturnType = tc.ReturnType;
                tc.a.TotalAmountForWarehouse = tc.TotalAmountForWarehouse;
                tc.a.HasDifference = tc.HasDifference;
            }

            return result1.Include("PartsSalesCategory").OrderBy(r => r.Id);
        }

    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:32
        //原分布类的函数全部转移到Ach                                                               


        public void InsertPartsInboundPlan(PartsInboundPlan partsInboundPlan) {
            new PartsInboundPlanAch(this).InsertPartsInboundPlan(partsInboundPlan);
        }

        public void UpdatePartsInboundPlan(PartsInboundPlan partsInboundPlan) {
            new PartsInboundPlanAch(this).UpdatePartsInboundPlan(partsInboundPlan);
        }

        public IQueryable<PartsInboundPlan> 检验区负责人查询配件入库计划(string sparePartCode, string sparePartName) {
            return new PartsInboundPlanAch(this).检验区负责人查询配件入库计划(sparePartCode, sparePartName);
        }

        public IQueryable<VirtualPartsInboundPlan> 检验入库查询配件入库计划(string sparePartCode, string sparePartName) {
            return new PartsInboundPlanAch(this).检验入库查询配件入库计划(sparePartCode, sparePartName);
        }

        public IQueryable<PartsInboundPlan> 检验区负责人查询配件入库计划无价格() {
            return new PartsInboundPlanAch(this).检验区负责人查询配件入库计划无价格();
        }

        public IQueryable<PartsInboundPlan> 仓库人员查询配件入库计划(int? partsPurchaseOrderTypeId, string sparepartCode, string sparepartName, bool? hasDifference) {
            return new PartsInboundPlanAch(this).仓库人员查询配件入库计划(partsPurchaseOrderTypeId, sparepartCode, sparepartName, hasDifference);
        }

        public IQueryable<PartsInboundPlan> 仓库人员查询配件入库计划无价格(int? partsPurchaseOrderTypeId) {
            return new PartsInboundPlanAch(this).仓库人员查询配件入库计划无价格(partsPurchaseOrderTypeId);
        }

        public PartsInboundPlan GetPartsInboundPlanWithDetails(int id) {
            return new PartsInboundPlanAch(this).GetPartsInboundPlanWithDetails(id);
        }

        public PartsInboundPlan GetPartsInboundPlanReceiptDetails(int id) {
            return new PartsInboundPlanAch(this).GetPartsInboundPlanReceiptDetails(id);
        }
        public IQueryable<PartsInboundPlan> 仓库人员查询配件入库计划2(int? partsPurchaseOrderTypeId, string erpSourceOrderCode, string partsSalesOrderCode, string sparepartCode, string sparepartName, bool? hasDifference) {
            return new PartsInboundPlanAch(this).仓库人员查询配件入库计划2(partsPurchaseOrderTypeId, erpSourceOrderCode, partsSalesOrderCode, sparepartCode, sparepartName, hasDifference);

        }
    }
}