﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Web;
using System;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsInboundPlanDetailAch : DcsSerivceAchieveBase {
        public PartsInboundPlanDetailAch(DcsDomainService domainService)
            : base(domainService) {
        }

        [Query(HasSideEffects = true)]
        public IQueryable<PartsInboundPlanDetail> GetPartsInboundPlanDetailByPartsInboundPlanId(int[] partsInboundPlanId) {
            return ObjectContext.PartsInboundPlanDetails.Where(r => partsInboundPlanId.Any(v => v == r.PartsInboundPlanId)).Include("PartsInboundPlan").Include("SparePart").OrderBy(r => r.SparePartId);

        }
        public IQueryable<PartsInboundPlanDetail> GetPartsInboundPlanDetailById(int partsInboundPlanId) {
            return ObjectContext.PartsInboundPlanDetails.Where(r => r.PartsInboundPlanId == partsInboundPlanId).Include("SparePart").OrderBy(r => r.SparePartId);
        }
        public IEnumerable<PartsInboundPlanDetail> GetPartsInboundPlanDetailWithSparePart(int id, int warehouseId) {
            var userInfo = Utils.GetCurrentUserInfo();
            var partsInboundPlanDetails = ObjectContext.PartsInboundPlanDetails.Where(r => r.PartsInboundPlanId == id && (r.InspectedQuantity ?? 0) <= r.PlannedAmount);
            var sparePartIds = partsInboundPlanDetails.Select(r => r.SparePartId).ToArray();
            var partsStockStruct = from a in ObjectContext.PartsStocks.Where(r => r.WarehouseId == warehouseId && sparePartIds.Any(v => v == r.PartId))
                                   join b in ObjectContext.WarehouseAreas.Where(r => r.AreaKind == (int)DcsAreaKind.库位 && r.Status == (int)DcsBaseDataStatus.有效) on a.WarehouseAreaId equals b.Id
                                   join c in ObjectContext.WarehouseAreaCategories.Where(r => r.Category == (int)DcsAreaType.保管区) on b.AreaCategoryId equals c.Id
                                   select new {
                                       a.PartId,
                                       b.Code
                                   };
            var structPart = partsStockStruct.GroupBy(r => new {
                r.PartId
            }).Select(r => new {
                partId = r.Key.PartId,
                areacode = r.Min(v => v.Code)
            }).ToArray();

            var partsSupplierRelations = (from partsInboundPlans in ObjectContext.PartsInboundPlans
                                          join partsInboundPlanDetail in partsInboundPlanDetails on partsInboundPlans.Id equals partsInboundPlanDetail.PartsInboundPlanId
                                          join partsSupplierRelation in ObjectContext.PartsSupplierRelations on new {
                                              SupplierId = partsInboundPlans.CounterpartCompanyId,
                                              SparePartId = partsInboundPlanDetail.SparePartId
                                          } equals new {
                                              SupplierId = partsSupplierRelation.SupplierId,
                                              SparePartId = partsSupplierRelation.PartId
                                          }
                                          where partsSupplierRelation.Status == (int)DcsBaseDataStatus.有效
                                          select partsSupplierRelation).ToArray();

            var spareparts = ObjectContext.SpareParts.Where(r => sparePartIds.Any(v => v == r.Id)).ToArray();
            //var result1 = (from partsInboundPlans in ObjectContext.PartsInboundPlans
            //               join partsInboundPlanDetail in partsInboundPlanDetails on partsInboundPlans.Id equals partsInboundPlanDetail.PartsInboundPlanId
            //               join partsStock in ObjectContext.PartsStocks on partsInboundPlans.WarehouseId equals partsStock.WarehouseId // and partsInboundPlanDetails.SparePartId equals partsStock.PartId
            //               join warehouseAreas in ObjectContext.WarehouseAreas on partsStock.WarehouseAreaId equals warehouseAreas.Id
            //               join warehouseAreaCategorys in ObjectContext.WarehouseAreaCategories on warehouseAreas.AreaCategoryId equals warehouseAreaCategorys.Id
            //               where partsInboundPlanDetail.SparePartId == partsStock.PartId && warehouseAreas.Status == (int)DcsBaseDataStatus.有效 && warehouseAreaCategorys.Category == (int)DcsAreaType.检验区 && ObjectContext.WarehouseAreaManagers.Any(v => v.WarehouseAreaId == warehouseAreas.TopLevelWarehouseAreaId && v.ManagerId == userInfo.Id)
            //               select
            //                   partsInboundPlanDetail
            //                );
            //var result2 = (from partsInboundPlans in ObjectContext.PartsInboundPlans
            //               join partsInboundPlanDetail in partsInboundPlanDetails on partsInboundPlans.Id equals partsInboundPlanDetail.PartsInboundPlanId
            //               where !(from partsStock in ObjectContext.PartsStocks  // and partsInboundPlanDetails.SparePartId equals partsStock.PartId
            //                       join warehouseAreas in ObjectContext.WarehouseAreas.Where(v => v.Status == (int)DcsBaseDataStatus.有效) on partsStock.WarehouseAreaId equals warehouseAreas.Id
            //                       join warehouseAreaCategorys in ObjectContext.WarehouseAreaCategories on warehouseAreas.AreaCategoryId equals warehouseAreaCategorys.Id
            //                       where warehouseAreaCategorys.Category == (int)DcsAreaType.检验区 && partsInboundPlanDetail.SparePartId == partsStock.PartId && partsInboundPlans.WarehouseId == partsStock.WarehouseId
            //                       select new {
            //                           partsStock
            //                       }).Any()
            //               select
            //                   partsInboundPlanDetail
            //               );
            //partsInboundPlanDetails = result1.Union(result2).Distinct();
            var result = (from partsInboundPlans in ObjectContext.PartsInboundPlans
                          join partsInboundPlanDetail in partsInboundPlanDetails on partsInboundPlans.Id equals partsInboundPlanDetail.PartsInboundPlanId
                          where (from warehouseAreas in ObjectContext.WarehouseAreas.Where(v => v.Status == (int)DcsBaseDataStatus.有效)
                                 join warehouseAreaCategorys in ObjectContext.WarehouseAreaCategories on warehouseAreas.AreaCategoryId equals warehouseAreaCategorys.Id
                                 where warehouseAreaCategorys.Category == (int)DcsAreaType.检验区 && partsInboundPlans.WarehouseId == warehouseAreas.WarehouseId && ObjectContext.WarehouseAreaManagers.Any(v => v.WarehouseAreaId == warehouseAreas.TopLevelWarehouseAreaId && v.ManagerId == userInfo.Id)
                                 select new {
                                     warehouseAreas
                                 }).Any()
                          select partsInboundPlanDetail);
            partsInboundPlanDetails = result.Distinct();
            foreach(var item in partsInboundPlanDetails) {
                var temp = spareparts.FirstOrDefault(r => r.Id == item.SparePartId);
                item.Unit = temp == null ? "" : temp.MeasureUnit;
            }

            foreach(var item in partsInboundPlanDetails) {
                var temp = structPart.FirstOrDefault(r => r.partId == item.SparePartId);
                var partsSupplierRelation = partsSupplierRelations.FirstOrDefault(r => r.PartId == item.SparePartId);
                item.WarehouseAreaCode = temp == null ? "" : temp.areacode;
                item.SupplierPartCode = partsSupplierRelation == null ? "" : partsSupplierRelation.SupplierPartCode;
            }

            return partsInboundPlanDetails.OrderBy(r => r.Id);
        }

        public IEnumerable<PartsInboundPlanDetail> GetPartsInboundPlanDetailForWithSparePart(int id, int warehouseId) {
            var partsInboundPlanDetails = ObjectContext.PartsInboundPlanDetails.Where(r => r.PartsInboundPlanId == id).ToArray();
            var sparePartIds = partsInboundPlanDetails.Select(r => r.SparePartId).ToArray();
            var partsStockStruct = from a in ObjectContext.PartsStocks.Where(r => r.WarehouseId == warehouseId && sparePartIds.Any(v => v == r.PartId))
                                   join b in ObjectContext.WarehouseAreas.Where(r => r.AreaKind == (int)DcsAreaKind.库位) on a.WarehouseAreaId equals b.Id
                                   join c in ObjectContext.WarehouseAreaCategories.Where(r => r.Category == (int)DcsAreaType.保管区) on b.AreaCategoryId equals c.Id
                                   select new {
                                       a.PartId,
                                       b.Code
                                   };
            var structPart = partsStockStruct.GroupBy(r => new {
                r.PartId
            }).Select(r => new {
                partId = r.Key.PartId,
                areacode = r.Min(v => v.Code)
            }).ToArray();

            var spareparts = ObjectContext.SpareParts.Where(r => sparePartIds.Any(v => v == r.Id)).ToArray();
            foreach(var item in partsInboundPlanDetails) {
                var temp = spareparts.FirstOrDefault(r => r.Id == item.SparePartId);
                item.Unit = temp == null ? "" : temp.MeasureUnit;

            }
            foreach(var item in partsInboundPlanDetails) {
                var temp = structPart.FirstOrDefault(r => r.partId == item.SparePartId);
                item.WarehouseAreaCode = temp == null ? "" : temp.areacode;

            }

            return partsInboundPlanDetails.OrderBy(r => r.Id);
        }

        [Query(HasSideEffects = true)]
        public IQueryable<PartsInboundPlanDetail> GetPartsInboundPlanDetailsById(int[] partsInboundPlanId) {
            return ObjectContext.PartsInboundPlanDetails.Where(r => partsInboundPlanId.Any(v => v == r.PartsInboundPlanId)).Include("PartsInboundPlan").OrderBy(r => r.SparePartId);
        }

        public IEnumerable<VirtualPartsInboundCheckBillForPrint> GetPartsInboundCheckBillForPrint(int[] ids) {
            string SQL = @"SELECT Extent1.Id,
       Extent1.SparePartCode,
       Extent1.SparePartName,
       Extent1.SparePartId,
       Extent1.InspectedQuantity,
       Extent1.PartsInboundCheckBillId,
       Extent1.BatchNumber             as BachMunber,
       Extent3.Code                    as Code,
       Extent2.Measureunit,
       p1.measureunit as FirMeasureUnit,
       p2.measureunit as SecMeasureUnit,
       p3.measureunit as ThidMeasureUnit,
      ( case when p1.isboxstandardprint=1 and p1.packingcoefficient>0  then ceil(Extent1.Inspectedquantity/p1.packingcoefficient) else 0 end )as FirPrintNumber,
      ( case when p2.isboxstandardprint=1 and p2.packingcoefficient>0  then ceil(Extent1.Inspectedquantity/p2.packingcoefficient) else 0 end )as SecPrintNumber,
       ( case when p3.isboxstandardprint=1 and p3.packingcoefficient>0  then ceil(Extent1.Inspectedquantity/p3.packingcoefficient) else 0 end )as ThidPrintNumber
  FROM PartsInboundCheckBillDetail Extent1
  join PartsInboundCheckBill Extent3
    on Extent1.PartsInboundCheckBillId = Extent3.Id
 INNER JOIN SparePart Extent2
    ON Extent1.SparePartId = Extent2.Id
  left join  partsbranch pb
    on Extent1.Sparepartid = pb.partid
   and pb.status = 1
  left join PartsBranchPackingProp p1
    on p1.partsbranchid = pb.id
   and p1.PackingType = 1
  left join PartsBranchPackingProp p2
    on p2.partsbranchid = pb.id
   and p2.PackingType = 2
  left join PartsBranchPackingProp p3
    on p3.partsbranchid = pb.id
   and p3.PackingType = 3
 WHERE Extent1.PartsInboundCheckBillId in (";
            for(var i = 0; i < ids.Length; i++) {
                if(ids.Length == i + 1) {
                    SQL = SQL + ids[i];
                } else {
                    SQL = SQL + ids[i] + ",";
                }
            }
            SQL = SQL + ")";
            SQL = SQL + " order by Extent1.SparePartCode";
            var detail = ObjectContext.ExecuteStoreQuery<VirtualPartsInboundCheckBillForPrint>(SQL).ToList();
            return detail;
        }
        public IEnumerable<VirtualPartsInboundCheckBillForPrint> GetPartsInboundCheckBillForPrintPc(int[] id) {
            string SQL = @"SELECT Extent1.Id,
       Extent1.SparePartCode,
       Extent1.SparePartName,
       Extent1.SparePartId,
       Extent1.InspectedQuantity,
       Extent1.PartsInboundCheckBillId,
       Extent1.BatchNumber             as BachMunber,
       Extent3.Code                    as Code,
       Extent2.Measureunit,
       p1.measureunit as FirMeasureUnit,
       p2.measureunit as SecMeasureUnit,
       p3.measureunit as ThidMeasureUnit,
      ( case when p1.IsPrintPartStandard=1 and p1.packingcoefficient>0  then ceil(Extent1.Inspectedquantity/p1.packingcoefficient) else 0 end )as FirPrintNumber,
      ( case when p2.IsPrintPartStandard=1 and p2.packingcoefficient>0  then ceil(Extent1.Inspectedquantity/p2.packingcoefficient) else 0 end )as SecPrintNumber,
       ( case when p3.IsPrintPartStandard=1 and p3.packingcoefficient>0  then ceil(Extent1.Inspectedquantity/p3.packingcoefficient) else 0 end )as ThidPrintNumber
  FROM PartsInboundCheckBillDetail Extent1
  join PartsInboundCheckBill Extent3
    on Extent1.PartsInboundCheckBillId = Extent3.Id
 INNER JOIN SparePart Extent2
    ON Extent1.SparePartId = Extent2.Id
  left join  partsbranch pb
    on Extent1.Sparepartid = pb.partid
   and pb.status = 1
  left join PartsBranchPackingProp p1
    on p1.partsbranchid = pb.id
   and p1.PackingType = 1
  left join PartsBranchPackingProp p2
    on p2.partsbranchid = pb.id
   and p2.PackingType = 2
  left join PartsBranchPackingProp p3
    on p3.partsbranchid = pb.id
   and p3.PackingType = 3
 WHERE Extent1.PartsInboundCheckBillId in (";
            for(var i = 0; i < id.Length; i++) {
                if(id.Length == i + 1) {
                    SQL = SQL + id[i];
                } else {
                    SQL = SQL + id[i] + ",";
                }
            }
            SQL = SQL + ")";
            SQL = SQL + " order by Extent1.SparePartCode";
            var detail = ObjectContext.ExecuteStoreQuery<VirtualPartsInboundCheckBillForPrint>(SQL).ToList();
            return detail;
        }
        public IEnumerable<VirtualPartsInboundPlanDetail> GetPartsInboundPlanDetailForWithSparePart2(int id, int warehouseId) {

            var partsInboundPlanDetails = (from a in ObjectContext.PartsInboundPlanDetails.Where(r => r.PartsInboundPlanId == id)
                                           join b in ObjectContext.SpareParts on a.SparePartId equals b.Id
                                           join c in ObjectContext.PartsInboundPlans on a.PartsInboundPlanId equals c.Id
                                           join d in ObjectContext.PartsRetailGuidePrices on new {
                                               a.SparePartId,
                                               c.PartsSalesCategoryId,
                                               c.BranchId,
                                               Status = (int)DcsBaseDataStatus.有效
                                           } equals new {
                                               d.SparePartId,
                                               d.PartsSalesCategoryId,
                                               d.BranchId,
                                               d.Status
                                           } into dd 
                                           from ds in dd.DefaultIfEmpty()
                                           select new VirtualPartsInboundPlanDetail {
                                               Id = a.Id,
                                               SparePartCode = a.SparePartCode,
                                               SparePartName = a.SparePartName,
                                               PlannedAmount = a.PlannedAmount,
                                               InspectedQuantity = a.InspectedQuantity,
                                               Price = ds.RetailGuidePrice,
                                               MeasureUnit = b.MeasureUnit,
                                               Remark = a.Remark,
                                               SpareOrderRemark = a.SpareOrderRemark,
                                               PartsInboundPlanId = a.PartsInboundPlanId,
                                               Code = c.Code,
                                               SparePartId = a.SparePartId
                                           }).ToArray();
            var sparePartIds = partsInboundPlanDetails.Select(r => r.SparePartId).ToArray();
            var partsStockStruct = from a in ObjectContext.PartsStocks.Where(r => r.WarehouseId == warehouseId && sparePartIds.Any(v => v == r.PartId))
                                   join b in ObjectContext.WarehouseAreas.Where(r => r.AreaKind == (int)DcsAreaKind.库位) on a.WarehouseAreaId equals b.Id
                                   join c in ObjectContext.WarehouseAreaCategories.Where(r => r.Category == (int)DcsAreaType.保管区) on b.AreaCategoryId equals c.Id
                                   select new {
                                       a.PartId,
                                       b.Code
                                   };
            var structPart = partsStockStruct.GroupBy(r => new {
                r.PartId
            }).Select(r => new {
                partId = r.Key.PartId,
                areacode = r.Min(v => v.Code)
            }).ToArray();
            foreach(var item in partsInboundPlanDetails) {
                var temp = structPart.FirstOrDefault(r => r.partId == item.SparePartId);
                item.WarehouseAreaCode = temp == null ? "" : temp.areacode;
            }
            return partsInboundPlanDetails.OrderBy(r => r.Id);
        }

    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:32
        //原分布类的函数全部转移到Ach                                                               

        [Query(HasSideEffects = true)]
        public IQueryable<PartsInboundPlanDetail> GetPartsInboundPlanDetailByPartsInboundPlanId(int[] partsInboundPlanId) {
            return new PartsInboundPlanDetailAch(this).GetPartsInboundPlanDetailByPartsInboundPlanId(partsInboundPlanId);
        }
        public IQueryable<PartsInboundPlanDetail> GetPartsInboundPlanDetailById(int partsInboundPlanId) {
            return new PartsInboundPlanDetailAch(this).GetPartsInboundPlanDetailById(partsInboundPlanId);
        }
        public IEnumerable<PartsInboundPlanDetail> GetPartsInboundPlanDetailWithSparePart(int id, int warehouseId) {
            return new PartsInboundPlanDetailAch(this).GetPartsInboundPlanDetailWithSparePart(id, warehouseId);
        }

        public IEnumerable<PartsInboundPlanDetail> GetPartsInboundPlanDetailForWithSparePart(int id, int warehouseId) {
            return new PartsInboundPlanDetailAch(this).GetPartsInboundPlanDetailForWithSparePart(id, warehouseId);
        }


        [Query(HasSideEffects = true)]
        public IQueryable<PartsInboundPlanDetail> GetPartsInboundPlanDetailsById(int[] partsInboundPlanId) {
            return new PartsInboundPlanDetailAch(this).GetPartsInboundPlanDetailsById(partsInboundPlanId);
        }

        public IEnumerable<VirtualPartsInboundCheckBillForPrint> GetPartsInboundCheckBillForPrint(int[] id) {
            return new PartsInboundPlanDetailAch(this).GetPartsInboundCheckBillForPrint(id);
        }
        public IEnumerable<VirtualPartsInboundCheckBillForPrint> GetPartsInboundCheckBillForPrintPc(int[] id) {
            return new PartsInboundPlanDetailAch(this).GetPartsInboundCheckBillForPrintPc(id);
        }
        public IEnumerable<VirtualPartsInboundPlanDetail> GetPartsInboundPlanDetailForWithSparePart2(int id, int warehouseId) {
            return new PartsInboundPlanDetailAch(this).GetPartsInboundPlanDetailForWithSparePart2(id, warehouseId);
        }
    }
}
