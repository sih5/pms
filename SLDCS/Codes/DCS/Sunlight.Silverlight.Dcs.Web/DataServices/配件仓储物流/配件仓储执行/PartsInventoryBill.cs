﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        private void InsertPartsInventoryBillValidate(PartsInventoryBill partsInventoryBill) {
            var sparepartsIds = partsInventoryBill.PartsInventoryDetails.Select(r => r.SparePartId);
            var warehouseAreaIds = partsInventoryBill.PartsInventoryDetails.Select(r => r.WarehouseAreaId);
            //判断库位是否有效
            var warehouseAreaStatus = ObjectContext.WarehouseAreas.Where(r => warehouseAreaIds.Contains(r.Id) && r.Status == (int)DcsBaseDataStatus.作废).ToArray();
            if(warehouseAreaStatus.Count() > 0) {
                throw new ValidationException(string.Format(ErrorStrings.PartsInventoryBill_Validation36, warehouseAreaStatus.First().Code));
            }
            var dbPartsInventoryDetails = ObjectContext.PartsInventoryDetails.Where(v => sparepartsIds.Contains(v.SparePartId) && warehouseAreaIds.Contains(v.WarehouseAreaId) && ObjectContext.PartsInventoryBills.Any(r => v.PartsInventoryBillId == r.Id && r.Status != (int)DcsPartsInventoryBillStatus.已库存覆盖 && r.Status != (int)DcsPartsInventoryBillStatus.作废)).ToArray();
            foreach(var partsInventoryDetail in partsInventoryBill.PartsInventoryDetails) {
                var dbPartsInventoryDetail = dbPartsInventoryDetails.FirstOrDefault(r => r.SparePartId == partsInventoryDetail.SparePartId && r.WarehouseAreaId == partsInventoryDetail.WarehouseAreaId);
                if(dbPartsInventoryDetail != null)
                    throw new ValidationException(ErrorStrings.PartsInventoryBill_Validation1);
            }
            if(string.IsNullOrWhiteSpace(partsInventoryBill.Code) || partsInventoryBill.Code == GlobalVar.ASSIGNED_BY_SERVER) {
                var dbBranch = ObjectContext.Branches.First(r => r.Id == partsInventoryBill.BranchId);
                partsInventoryBill.Code = CodeGenerator.Generate("PartsInventoryBill", dbBranch.Code);
            }
            var userInfo = Utils.GetCurrentUserInfo();
            partsInventoryBill.CreatorId = userInfo.Id;
            partsInventoryBill.CreatorName = userInfo.Name;
            partsInventoryBill.CreateTime = DateTime.Now;
        }

        private void UpdatePartsInventoryBillValidate(PartsInventoryBill partsInventoryBill) {
            var userInfo = Utils.GetCurrentUserInfo();
            partsInventoryBill.ModifierId = userInfo.Id;
            partsInventoryBill.ModifierName = userInfo.Name;
            partsInventoryBill.ModifyTime = DateTime.Now;
        }


        public void InsertPartsInventoryBill(PartsInventoryBill partsInventoryBill) {
            InsertToDatabase(partsInventoryBill);
            var partsInventoryDetails = ChangeSet.GetAssociatedChanges(partsInventoryBill, v => v.PartsInventoryDetails, ChangeOperation.Insert);
            foreach(PartsInventoryDetail partsInventoryDetail in partsInventoryDetails) {
                InsertToDatabase(partsInventoryDetail);
            }
            InsertPartsInventoryBillValidate(partsInventoryBill);
        }

        public void UpdatePartsInventoryBill(PartsInventoryBill partsInventoryBill) {
            partsInventoryBill.PartsInventoryDetails.Clear();
            UpdateToDatabase(partsInventoryBill);
            var partsInventoryDetails = ChangeSet.GetAssociatedChanges(partsInventoryBill, v => v.PartsInventoryDetails);
            foreach(PartsInventoryDetail partsInventoryDetail in partsInventoryDetails) {
                switch(ChangeSet.GetChangeOperation(partsInventoryDetail)) {
                    case ChangeOperation.Insert:
                        InsertToDatabase(partsInventoryDetail);
                        break;
                    case ChangeOperation.Update:
                        UpdateToDatabase(partsInventoryDetail);
                        break;
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(partsInventoryDetail);
                        break;
                }
            }
            UpdatePartsInventoryBillValidate(partsInventoryBill);
        }

        public PartsInventoryBill GetPartsInventoryBillsWithDetailsById(int PartsInventoryBillId) {
            var partsInventoryBill = ObjectContext.PartsInventoryBills.SingleOrDefault(e => e.Id == PartsInventoryBillId);
            if(partsInventoryBill != null) {
                var partsInventoryDetails = ObjectContext.PartsInventoryDetails.Where(e => e.PartsInventoryBillId == partsInventoryBill.Id).OrderBy(e => e.WarehouseAreaCode).ToArray();
            }
            return partsInventoryBill;
        }

        public IEnumerable<PartsInventoryBillEx> GetPartsInventoryBillWithSumPrice(int? amountDifference) {
            var userId = Utils.GetCurrentUserInfo().Id;
            var tempgroup = from p in ObjectContext.PartsInventoryDetails
                            group p by new {
                                p.PartsInventoryBillId
                            }
                                into tempTable
                                select new {
                                    tempTable.Key.PartsInventoryBillId,
                                    SumCostBeforeInventory = tempTable.Sum(r => (r.CostPrice ?? 0) * r.CurrentStorage),
                                    SumCostAfterInventory = tempTable.Sum(r => (r.CostPrice ?? 0) * r.StorageAfterInventory),
                                    SumCostDifference = tempTable.Sum(r => (r.CostPrice ?? 0) * r.StorageAfterInventory) - tempTable.Sum(r => (r.CostPrice ?? 0) * r.CurrentStorage)
                                };

            var result = from a in ObjectContext.PartsInventoryBills
                         join w in ObjectContext.Warehouses.Where(x => x.Status == (int)DcsBaseDataStatus.有效 && (x.PwmsInterface == null || x.PwmsInterface == false))
                         on a.WarehouseId equals w.Id
                         join sw in ObjectContext.SalesUnitAffiWarehouses
                         on w.Id equals sw.WarehouseId
                         join s in ObjectContext.SalesUnits.Where(x => x.Status == (int)DcsBaseDataStatus.有效)
                         on sw.SalesUnitId equals s.Id
                         join ps in ObjectContext.PersonSalesCenterLinks.Where(x => x.Status == (int)DcsBaseDataStatus.有效 && x.PersonId == userId)
                         on s.PartsSalesCategoryId equals ps.PartsSalesCategoryId
                         from b in tempgroup
                         where a.Id == b.PartsInventoryBillId
                         select new PartsInventoryBillEx {
                             AdvancedAuditID = a.AdvancedAuditID,
                             AdvancedAuditName = a.AdvancedAuditName,
                             AdvancedAuditTime = a.AdvancedAuditTime,
                             CreatorName = a.CreatorName,
                             Id = a.Id,
                             Code = a.Code,
                             InitiatorName = a.InitiatorName,
                             InventoryReason = a.InventoryReason,
                             InventoryRecordOperatorId = a.InventoryRecordOperatorId,
                             InventoryRecordOperatorName = a.InventoryRecordOperatorName,
                             InventoryRecordTime = a.InventoryRecordTime,
                             ModifierId = a.ModifierId,
                             ModifierName = a.ModifierName,
                             ModifyTime = a.ModifyTime,
                             RejectComment = a.RejectComment,
                             Remark = a.Remark,
                             ResultInputOperatorId = a.ResultInputOperatorId,
                             ResultInputOperatorName = a.ResultInputOperatorName,
                             ResultInputTime = a.ResultInputTime,
                             RowVersion = a.RowVersion,
                             Status = a.Status,
                             StorageCompanyCode = a.StorageCompanyCode,
                             StorageCompanyId = a.StorageCompanyId,
                             StorageCompanyName = a.StorageCompanyName,
                             StorageCompanyType = a.StorageCompanyType,
                             SumCostAfterInventory = b.SumCostAfterInventory,
                             SumCostBeforeInventory = b.SumCostBeforeInventory,
                             SumCostDifference = b.SumCostDifference,
                             WarehouseAreaCategory = a.WarehouseAreaCategory,
                             WarehouseCode = a.WarehouseCode,
                             WarehouseId = a.WarehouseId,
                             WarehouseName = a.WarehouseName,
                             CreateTime = a.CreateTime,
                             BranchId = a.BranchId,
                             ApproverName = a.ApproverName,
                             ApproveTime = a.ApproveTime,
                             ApproverId = a.ApproverId,
                             IsUplodFile = a.Path != null && a.Path != "",
                             RejecterId = a.RejecterId,
                             RejecterName = a.RejecterName,
                             RejectTime = a.RejectTime,
                             InitialApproverName = a.InitialApproverName,
                             InitialApproveTime = a.InitialApproveTime,
                             CheckerName = a.CheckerName,
                             CheckTime = a.CheckTime
                         };
            if(amountDifference.HasValue) {
                var config = ObjectContext.MultiLevelApproveConfigs.Where(t => t.Id == amountDifference.Value).First();
                result = result.Where(r => r.SumCostDifference >= config.MinApproveFee && r.SumCostDifference < config.MaxApproveFee);
            }
            return result.OrderByDescending(r => r.Id);



        }

        public IEnumerable<PartsInventoryBillEx> GetPWMSPartsInventoryBillWithSumPrice() {
            var userId = Utils.GetCurrentUserInfo().Id;
            var tempgroup = from p in ObjectContext.PartsInventoryDetails
                            group p by new {
                                p.PartsInventoryBillId
                            }
                                into tempTable
                                select new {
                                    tempTable.Key.PartsInventoryBillId,
                                    SumCostBeforeInventory = tempTable.Sum(r => (r.CostPrice ?? 0) * r.CurrentStorage),
                                    SumCostAfterInventory = tempTable.Sum(r => (r.CostPrice ?? 0) * r.StorageAfterInventory),
                                    SumCostDifference = tempTable.Sum(r => (r.CostPrice ?? 0) * r.StorageAfterInventory) - tempTable.Sum(r => (r.CostPrice ?? 0) * r.CurrentStorage)
                                };

            var result = from a in ObjectContext.PartsInventoryBills
                         join w in ObjectContext.Warehouses.Where(x => x.Status == (int)DcsBaseDataStatus.有效 && x.PwmsInterface == true)
                         on a.WarehouseId equals w.Id
                         from b in tempgroup
                         where a.Id == b.PartsInventoryBillId
                         select new PartsInventoryBillEx {
                             AdvancedAuditID = a.AdvancedAuditID,
                             AdvancedAuditName = a.AdvancedAuditName,
                             AdvancedAuditTime = a.AdvancedAuditTime,
                             CreatorName = a.CreatorName,
                             Id = a.Id,
                             Code = a.Code,
                             InitiatorName = a.InitiatorName,
                             InventoryReason = a.InventoryReason,
                             InventoryRecordOperatorId = a.InventoryRecordOperatorId,
                             InventoryRecordOperatorName = a.InventoryRecordOperatorName,
                             InventoryRecordTime = a.InventoryRecordTime,
                             ModifierId = a.ModifierId,
                             ModifierName = a.ModifierName,
                             ModifyTime = a.ModifyTime,
                             RejectComment = a.RejectComment,
                             Remark = a.Remark,
                             ResultInputOperatorId = a.ResultInputOperatorId,
                             ResultInputOperatorName = a.ResultInputOperatorName,
                             ResultInputTime = a.ResultInputTime,
                             RowVersion = a.RowVersion,
                             Status = a.Status,
                             StorageCompanyCode = a.StorageCompanyCode,
                             StorageCompanyId = a.StorageCompanyId,
                             StorageCompanyName = a.StorageCompanyName,
                             StorageCompanyType = a.StorageCompanyType,
                             SumCostAfterInventory = b.SumCostAfterInventory,
                             SumCostBeforeInventory = b.SumCostBeforeInventory,
                             SumCostDifference = b.SumCostDifference,
                             WarehouseAreaCategory = a.WarehouseAreaCategory,
                             WarehouseCode = a.WarehouseCode,
                             WarehouseId = a.WarehouseId,
                             WarehouseName = a.WarehouseName,
                             CreateTime = a.CreateTime,
                             BranchId = a.BranchId,
                             ApproverName = a.ApproverName,
                             ApproveTime = a.ApproveTime,
                             ApproverId = a.ApproverId
                         };
            return result.OrderByDescending(r => r.Id);



        }

        //todo:根据人员与品牌关系过滤盘点单
        public IEnumerable<PartsInventoryBillEx> GetPartsInventoryBillWithSumPriceByPersonnelId() {
            var userId = Utils.GetCurrentUserInfo().Id;
            var tempgroup = from p in ObjectContext.PartsInventoryDetails
                            group p by new {
                                p.PartsInventoryBillId
                            }
                                into tempTable
                                select new {
                                    tempTable.Key.PartsInventoryBillId,
                                    SumCostBeforeInventory = tempTable.Sum(r => (r.CostPrice ?? 0) * r.CurrentStorage),
                                    SumCostAfterInventory = tempTable.Sum(r => (r.CostPrice ?? 0) * r.StorageAfterInventory),
                                  //  SumCostDifference = tempTable.Sum(r => (r.CostPrice ?? 0) * r.StorageAfterInventory) - tempTable.Sum(r => (r.CostPrice ?? 0) * r.CurrentStorage)
                                };
            IQueryable<PartsInventoryBill> partsInventoryBill = ObjectContext.PartsInventoryBills.Include("PartsInventoryDetail");
            if(Utils.GetCurrentUserInfo().EnterpriseCode == "2450") {
                partsInventoryBill = partsInventoryBill.Where(r => ObjectContext.Warehouses.Any(v => v.Id == r.WarehouseId && v.WmsInterface == false));
            }
            var result = from a in partsInventoryBill.Where(r => ObjectContext.WarehouseOperators.Any(v => v.OperatorId == userId && v.WarehouseId == r.WarehouseId) || ObjectContext.Warehouses.Any(v => v.Id == r.WarehouseId && v.Type == (int)DcsWarehouseType.虚拟库 && v.Status == (int)DcsBaseDataStatus.有效))
                         join sw in ObjectContext.SalesUnitAffiWarehouses
                         on a.WarehouseId equals sw.WarehouseId
                         join s in ObjectContext.SalesUnits.Where(x => x.Status == (int)DcsBaseDataStatus.有效)
                         on sw.SalesUnitId equals s.Id
                         join ps in ObjectContext.PersonSalesCenterLinks.Where(x => x.Status == (int)DcsBaseDataStatus.有效 && x.PersonId == userId)
                         on s.PartsSalesCategoryId equals ps.PartsSalesCategoryId
                         from b in tempgroup
                         where a.Id == b.PartsInventoryBillId
                         select new PartsInventoryBillEx {
                             AdvancedAuditID = a.AdvancedAuditID,
                             AdvancedAuditName = a.AdvancedAuditName,
                             AdvancedAuditTime = a.AdvancedAuditTime,
                             CreatorName = a.CreatorName,
                             Id = a.Id,
                             Code = a.Code,
                             InitiatorName = a.InitiatorName,
                             InventoryReason = a.InventoryReason,
                             InventoryRecordOperatorId = a.InventoryRecordOperatorId,
                             InventoryRecordOperatorName = a.InventoryRecordOperatorName,
                             InventoryRecordTime = a.InventoryRecordTime,
                             ModifierId = a.ModifierId,
                             ModifierName = a.ModifierName,
                             ModifyTime = a.ModifyTime,
                             RejectComment = a.RejectComment,
                             Remark = a.Remark,
                             ResultInputOperatorId = a.ResultInputOperatorId,
                             ResultInputOperatorName = a.ResultInputOperatorName,
                             ResultInputTime = a.ResultInputTime,
                             RowVersion = a.RowVersion,
                             Status = a.Status,
                             StorageCompanyCode = a.StorageCompanyCode,
                             StorageCompanyId = a.StorageCompanyId,
                             StorageCompanyName = a.StorageCompanyName,
                             StorageCompanyType = a.StorageCompanyType,
                             SumCostAfterInventory = b.SumCostAfterInventory,
                             SumCostBeforeInventory = b.SumCostBeforeInventory,
                             //   SumCostDifference = b.SumCostDifference,
                           //  SumCostDifference = a.Status == (int)DcsPartsInventoryBillStatus.已库存覆盖 ? ObjectContext.PartsInventoryDetails.Where(t => t.PartsInventoryBillId == a.Id && t.Ifcover == true).Sum(p => p.StorageDifference * p.CostPrice).Value : ObjectContext.PartsInventoryDetails.Where(t => t.PartsInventoryBillId == a.Id).Sum(p => p.StorageDifference * p.CostPrice).Value,
                              SumCostDifference =a.PartsInventoryDetails.Where(t=>t.PartsInventoryBillId==a.Id && t.Ifcover==true).Sum(p=>p.StorageDifference*p.CostPrice).Value,
                             WarehouseAreaCategory = a.WarehouseAreaCategory,
                             WarehouseCode = a.WarehouseCode,
                             WarehouseId = a.WarehouseId,
                             WarehouseName = a.WarehouseName,
                             CreateTime = a.CreateTime,
                             BranchId = a.BranchId,
                             ApproverName = a.ApproverName,
                             ApproveTime = a.ApproveTime,
                             ApproverId = a.ApproverId,
                             CheckerName = a.CheckerName,
                             CheckTime = a.CheckTime,
                             IsUplodFile = a.Path != null && a.Path != "",
                             RejecterName = a.RejecterName,
                             RejectTime = a.RejectTime,
                             InitialApproverName = a.InitialApproverName,
                             InitialApproveTime = a.InitialApproveTime
                         };
            return result.Distinct().OrderByDescending(r => r.Id);
        }

        public IEnumerable<PartsInventoryBillEx> GetWMSPartsInventoryBillWithSumPriceByPersonnelId() {
            var userId = Utils.GetCurrentUserInfo().Id;
            var tempgroup = from p in ObjectContext.PartsInventoryDetails
                            group p by new {
                                p.PartsInventoryBillId
                            }
                                into tempTable
                                select new {
                                    tempTable.Key.PartsInventoryBillId,
                                    SumCostBeforeInventory = tempTable.Sum(r => (r.CostPrice ?? 0) * r.CurrentStorage),
                                    SumCostAfterInventory = tempTable.Sum(r => (r.CostPrice ?? 0) * r.StorageAfterInventory),
                                    SumCostDifference = tempTable.Sum(r => (r.CostPrice ?? 0) * r.StorageAfterInventory) - tempTable.Sum(r => (r.CostPrice ?? 0) * r.CurrentStorage)
                                };
            var result = from a in ObjectContext.PartsInventoryBills.Where(r => ObjectContext.Warehouses.Any(v => v.Id == r.WarehouseId && v.WmsInterface == true)).Where(r => ObjectContext.WarehouseOperators.Any(v => v.OperatorId == userId && v.WarehouseId == r.WarehouseId) || ObjectContext.Warehouses.Any(v => v.Id == r.WarehouseId && v.Type == (int)DcsWarehouseType.虚拟库 && v.Status == (int)DcsBaseDataStatus.有效))
                         join sw in ObjectContext.SalesUnitAffiWarehouses
                         on a.WarehouseId equals sw.WarehouseId
                         join s in ObjectContext.SalesUnits.Where(x => x.Status == (int)DcsBaseDataStatus.有效)
                         on sw.SalesUnitId equals s.Id
                         join ps in ObjectContext.PersonSalesCenterLinks.Where(x => x.Status == (int)DcsBaseDataStatus.有效 && x.PersonId == userId)
                         on s.PartsSalesCategoryId equals ps.PartsSalesCategoryId
                         from b in tempgroup
                         where a.Id == b.PartsInventoryBillId
                         select new PartsInventoryBillEx {
                             AdvancedAuditID = a.AdvancedAuditID,
                             AdvancedAuditName = a.AdvancedAuditName,
                             AdvancedAuditTime = a.AdvancedAuditTime,
                             CreatorName = a.CreatorName,
                             Id = a.Id,
                             Code = a.Code,
                             InitiatorName = a.InitiatorName,
                             InventoryReason = a.InventoryReason,
                             InventoryRecordOperatorId = a.InventoryRecordOperatorId,
                             InventoryRecordOperatorName = a.InventoryRecordOperatorName,
                             InventoryRecordTime = a.InventoryRecordTime,
                             ModifierId = a.ModifierId,
                             ModifierName = a.ModifierName,
                             ModifyTime = a.ModifyTime,
                             RejectComment = a.RejectComment,
                             Remark = a.Remark,
                             ResultInputOperatorId = a.ResultInputOperatorId,
                             ResultInputOperatorName = a.ResultInputOperatorName,
                             ResultInputTime = a.ResultInputTime,
                             RowVersion = a.RowVersion,
                             Status = a.Status,
                             StorageCompanyCode = a.StorageCompanyCode,
                             StorageCompanyId = a.StorageCompanyId,
                             StorageCompanyName = a.StorageCompanyName,
                             StorageCompanyType = a.StorageCompanyType,
                             SumCostAfterInventory = b.SumCostAfterInventory,
                             SumCostBeforeInventory = b.SumCostBeforeInventory,
                             SumCostDifference = b.SumCostDifference,
                             WarehouseAreaCategory = a.WarehouseAreaCategory,
                             WarehouseCode = a.WarehouseCode,
                             WarehouseId = a.WarehouseId,
                             WarehouseName = a.WarehouseName,
                             CreateTime = a.CreateTime,
                             BranchId = a.BranchId,
                             ApproverName = a.ApproverName,
                             ApproveTime = a.ApproveTime,
                             ApproverId = a.ApproverId
                         };
            return result.Distinct().OrderByDescending(r => r.Id);
        }

        public IEnumerable<PartsInventoryBillEx> GetAgencyPartsInventoryBill() {
            var tempgroup = from p in ObjectContext.PartsInventoryDetails
                            group p by new {
                                p.PartsInventoryBillId
                            }
                                into tempTable
                                select new {
                                    tempTable.Key.PartsInventoryBillId,
                                    SumCostBeforeInventory = tempTable.Sum(r => (r.CostPrice ?? 0) * r.CurrentStorage),
                                    SumCostAfterInventory = tempTable.Sum(r => (r.CostPrice ?? 0) * r.StorageAfterInventory),
                                    SumCostDifference = tempTable.Sum(r => (r.CostPrice ?? 0) * r.StorageAfterInventory) - tempTable.Sum(r => (r.CostPrice ?? 0) * r.CurrentStorage)
                                };

            var result = from a in ObjectContext.PartsInventoryBills
                         join w in ObjectContext.Warehouses.Where(x => x.Status == (int)DcsBaseDataStatus.有效 && (x.PwmsInterface == null || x.PwmsInterface == false))
                         on a.WarehouseId equals w.Id
                         from b in tempgroup
                         where a.Id == b.PartsInventoryBillId
                         select new PartsInventoryBillEx {
                             AdvancedAuditID = a.AdvancedAuditID,
                             AdvancedAuditName = a.AdvancedAuditName,
                             AdvancedAuditTime = a.AdvancedAuditTime,
                             CreatorName = a.CreatorName,
                             Id = a.Id,
                             Code = a.Code,
                             InitiatorName = a.InitiatorName,
                             InventoryReason = a.InventoryReason,
                             InventoryRecordOperatorId = a.InventoryRecordOperatorId,
                             InventoryRecordOperatorName = a.InventoryRecordOperatorName,
                             InventoryRecordTime = a.InventoryRecordTime,
                             ModifierId = a.ModifierId,
                             ModifierName = a.ModifierName,
                             ModifyTime = a.ModifyTime,
                             RejectComment = a.RejectComment,
                             Remark = a.Remark,
                             ResultInputOperatorId = a.ResultInputOperatorId,
                             ResultInputOperatorName = a.ResultInputOperatorName,
                             ResultInputTime = a.ResultInputTime,
                             RowVersion = a.RowVersion,
                             Status = a.Status,
                             StorageCompanyCode = a.StorageCompanyCode,
                             StorageCompanyId = a.StorageCompanyId,
                             StorageCompanyName = a.StorageCompanyName,
                             StorageCompanyType = a.StorageCompanyType,
                             SumCostAfterInventory = b.SumCostAfterInventory,
                             SumCostBeforeInventory = b.SumCostBeforeInventory,
                             SumCostDifference = b.SumCostDifference,
                             WarehouseAreaCategory = a.WarehouseAreaCategory,
                             WarehouseCode = a.WarehouseCode,
                             WarehouseId = a.WarehouseId,
                             WarehouseName = a.WarehouseName,
                             CreateTime = a.CreateTime,
                             BranchId = a.BranchId,
                             ApproverName = a.ApproverName,
                             ApproveTime = a.ApproveTime,
                             ApproverId = a.ApproverId,
                             IsUplodFile = a.Path != null && a.Path != "",
                             RejecterId = a.RejecterId,
                             RejecterName = a.RejecterName,
                             RejectTime = a.RejectTime
                         };
            return result.OrderByDescending(r => r.Id);
        }
        public IEnumerable<PartsInventoryDetail> GetPartsInventoryDetailsDesc(){
            return ObjectContext.PartsInventoryDetails.OrderBy(r => new {
                r.WarehouseAreaCode,
                r.SparePartCode
            }); 
        }         
    }
}
