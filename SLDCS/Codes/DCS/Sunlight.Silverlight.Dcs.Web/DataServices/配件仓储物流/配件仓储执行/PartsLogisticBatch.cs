﻿using System;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsLogisticBatchAch : DcsSerivceAchieveBase {
        public PartsLogisticBatchAch(DcsDomainService domainService)
            : base(domainService) {
        }

        internal void InsertPartsLogisticBatchValidate(PartsLogisticBatch partsLogisticBatch) {
            var userInfo = Utils.GetCurrentUserInfo();
            partsLogisticBatch.CreatorId = userInfo.Id;
            partsLogisticBatch.CreatorName = userInfo.Name;
            partsLogisticBatch.CreateTime = DateTime.Now;
        }
        internal void UpdatePartsLogisticBatchValidate(PartsLogisticBatch partsLogisticBatch) {
        }
        public void InsertPartsLogisticBatch(PartsLogisticBatch partsLogisticBatch) {
            InsertToDatabase(partsLogisticBatch);
            var partsLogisticBatchBillDetails = ChangeSet.GetAssociatedChanges(partsLogisticBatch, v => v.PartsLogisticBatchBillDetails, ChangeOperation.Insert);
            foreach(PartsLogisticBatchBillDetail partsLogisticBatchBillDetail in partsLogisticBatchBillDetails)
                InsertToDatabase(partsLogisticBatchBillDetail);
            var partsLogisticBatchItemDetails = ChangeSet.GetAssociatedChanges(partsLogisticBatch, v => v.PartsLogisticBatchItemDetails, ChangeOperation.Insert);
            foreach(PartsLogisticBatchItemDetail partsLogisticBatchItemDetail in partsLogisticBatchItemDetails)
                InsertToDatabase(partsLogisticBatchItemDetail);
            this.InsertPartsLogisticBatchValidate(partsLogisticBatch);
        }
        public void UpdatePartsLogisticBatch(PartsLogisticBatch partsLogisticBatch) {
            partsLogisticBatch.PartsLogisticBatchBillDetails.Clear();
            partsLogisticBatch.PartsLogisticBatchItemDetails.Clear();
            UpdateToDatabase(partsLogisticBatch);
            //更新配件物流批次单据清单
            var partsLogisticBatchBillDetails = ChangeSet.GetAssociatedChanges(partsLogisticBatch, v => v.PartsLogisticBatchBillDetails);
            foreach(PartsLogisticBatchBillDetail partsLogisticBatchBillDetail in partsLogisticBatchBillDetails) {
                switch(ChangeSet.GetChangeOperation(partsLogisticBatchBillDetail)) {
                    case ChangeOperation.Insert:
                        InsertToDatabase(partsLogisticBatchBillDetail);
                        break;
                    case ChangeOperation.Update:
                        UpdateToDatabase(partsLogisticBatchBillDetail);
                        break;
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(partsLogisticBatchBillDetail);
                        break;
                }
            }
            //更新配件物流批次配件清单
            var partsLogisticBatchItemDetails = ChangeSet.GetAssociatedChanges(partsLogisticBatch, v => v.PartsLogisticBatchItemDetails);
            foreach(PartsLogisticBatchItemDetail partsLogisticBatchItemDetail in partsLogisticBatchItemDetails) {
                switch(ChangeSet.GetChangeOperation(partsLogisticBatchItemDetail)) {
                    case ChangeOperation.Insert:
                        InsertToDatabase(partsLogisticBatchItemDetail);
                        break;
                    case ChangeOperation.Update:
                        UpdateToDatabase(partsLogisticBatchItemDetail);
                        break;
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(partsLogisticBatchItemDetail);
                        break;
                }
            }
            this.UpdatePartsLogisticBatchValidate(partsLogisticBatch);
        }
        public void DeletePartsLogisticBatch(PartsLogisticBatch partsLogisticBatch) {
            DeleteFromDatabase(partsLogisticBatch);
            var partsLogisticBatchBillDetails = ObjectContext.PartsLogisticBatchBillDetails.Where(r => r.PartsLogisticBatchId == partsLogisticBatch.Id).ToArray();
            foreach(var billDetail in partsLogisticBatchBillDetails)
                DeleteFromDatabase(billDetail);
            var partsLogisticBatchItemDetails = ObjectContext.PartsLogisticBatchItemDetails.Where(r => r.PartsLogisticBatchId == partsLogisticBatch.Id).ToArray();
            foreach(var itemDetail in partsLogisticBatchItemDetails)
                DeleteFromDatabase(itemDetail);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:32
        //原分布类的函数全部转移到Ach                                                               

        public void InsertPartsLogisticBatch(PartsLogisticBatch partsLogisticBatch) {
            new PartsLogisticBatchAch(this).InsertPartsLogisticBatch(partsLogisticBatch);
        }

        public void UpdatePartsLogisticBatch(PartsLogisticBatch partsLogisticBatch) {
            new PartsLogisticBatchAch(this).UpdatePartsLogisticBatch(partsLogisticBatch);
        }

        public void DeletePartsLogisticBatch(PartsLogisticBatch partsLogisticBatch) {
            new PartsLogisticBatchAch(this).DeletePartsLogisticBatch(partsLogisticBatch);
        }
    }
}