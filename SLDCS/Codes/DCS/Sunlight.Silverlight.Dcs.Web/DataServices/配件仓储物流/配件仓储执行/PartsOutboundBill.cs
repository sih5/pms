﻿using System;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsOutboundBillAch : DcsSerivceAchieveBase {
        internal void InsertPartsOutboundBillValidate(PartsOutboundBill partsOutboundBill) {
            if(string.IsNullOrWhiteSpace(partsOutboundBill.Code) || partsOutboundBill.Code == GlobalVar.ASSIGNED_BY_SERVER)
                partsOutboundBill.Code = CodeGenerator.Generate("PartsOutboundBill", partsOutboundBill.StorageCompanyCode);
            var userInfo = Utils.GetCurrentUserInfo();
            partsOutboundBill.PrintTimes = 0;
            partsOutboundBill.CreatorId = userInfo.Id;
            partsOutboundBill.CreatorName = userInfo.Name;
            partsOutboundBill.CreateTime = DateTime.Now;
        }

        internal void UpdatePartsOutboundBillValidate(PartsOutboundBill partsOutboundBill) {
            var userInfo = Utils.GetCurrentUserInfo();
            partsOutboundBill.ModifierId = userInfo.Id;
            partsOutboundBill.ModifierName = userInfo.Name;
            partsOutboundBill.ModifyTime = DateTime.Now;
        }

        public void InsertPartsOutboundBill(PartsOutboundBill partsOutboundBill) {
            InsertToDatabase(partsOutboundBill);
            var partsOutboundBillDetails = ChangeSet.GetAssociatedChanges(partsOutboundBill, v => v.PartsOutboundBillDetails, ChangeOperation.Insert);
            foreach(PartsOutboundBillDetail partsOutboundBillDetail in partsOutboundBillDetails)
                InsertToDatabase(partsOutboundBillDetail);
            this.InsertPartsOutboundBillValidate(partsOutboundBill);
        }

        public void UpdatePartsOutboundBill(PartsOutboundBill partsOutboundBill) {
            partsOutboundBill.PartsOutboundBillDetails.Clear();
            UpdateToDatabase(partsOutboundBill);
            var partsOutboundBillDetails = ChangeSet.GetAssociatedChanges(partsOutboundBill, v => v.PartsOutboundBillDetails);
            foreach(PartsOutboundBillDetail partsOutboundBillDetail in partsOutboundBillDetails) {
                //获取清单实体对象的操作类型
                switch(ChangeSet.GetChangeOperation(partsOutboundBillDetail)) {
                    //新增操作
                    case ChangeOperation.Insert:
                        InsertToDatabase(partsOutboundBillDetail);
                        break;
                    //更新操作
                    case ChangeOperation.Update:
                        UpdateToDatabase(partsOutboundBillDetail);
                        break;
                    //删除操作
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(partsOutboundBillDetail);
                        break;
                }
            }
            this.UpdatePartsOutboundBillValidate(partsOutboundBill);
        }

        public IQueryable<PartsOutboundBill> 查询待运配件出库单(int? partsOutboundPlanId, int? partsShippingOrderId) {
            IQueryable<PartsLogisticBatch> partsLogisticBatches = ObjectContext.PartsLogisticBatches;
            if(partsOutboundPlanId.HasValue)
                partsLogisticBatches = partsLogisticBatches.Where(v => v.SourceId == partsOutboundPlanId.Value && v.SourceType == (int)DcsPartsLogisticBatchSourceType.配件出库计划);
            if(partsShippingOrderId.HasValue) {
                var partsLogisticBatchBillDetails = ObjectContext.PartsLogisticBatchBillDetails.Where(r => r.BillId == partsShippingOrderId.Value && r.BillType == (int)DcsPartsLogisticBatchBillDetailBillType.配件发运单);
                partsLogisticBatches = partsLogisticBatches.Where(r => r.ShippingStatus == (int)DcsPartsLogisticBatchShippingStatus.待运 || (r.ShippingStatus == (int)DcsPartsLogisticBatchShippingStatus.发运 && partsLogisticBatchBillDetails.Any(v => v.PartsLogisticBatchId == r.Id)));
            } else {
                partsLogisticBatches = partsLogisticBatches.Where(r => r.ShippingStatus == (int)DcsPartsLogisticBatchShippingStatus.待运);
            }
            var result = ObjectContext.PartsOutboundBills.Where(k => ObjectContext.PartsLogisticBatchBillDetails.Any(v => v.BillId == k.Id && partsLogisticBatches.Contains(v.PartsLogisticBatch)
                && v.BillType == (int)DcsPartsLogisticBatchBillDetailBillType.配件出库单) && !ObjectContext.PartsShippingOrderRefs.Any(p => p.PartsOutboundBillId == k.Id));
            return result.Include("PartsOutboundBillDetails").OrderBy(r => r.Id);
        }

        public IQueryable<PartsOutboundBillWithOtherInfo> 仓库人员查询配件出库单(int? operatorId, string sparePartCode, string sparePartName) {
            var userInfo = Utils.GetCurrentUserInfo();
            IQueryable<PartsOutboundBill> partsOutboundBills = ObjectContext.PartsOutboundBills.Where(v => v.StorageCompanyId == userInfo.EnterpriseId);
            if(operatorId.HasValue)
                partsOutboundBills = partsOutboundBills.Where(r => ObjectContext.WarehouseOperators.Any(v => v.OperatorId == userInfo.Id && v.WarehouseId == r.WarehouseId) || ObjectContext.Warehouses.Any(v => v.Id == r.WarehouseId && v.Type == (int)DcsWarehouseType.虚拟库));
            if(sparePartCode != null) {
                partsOutboundBills = partsOutboundBills.Where(r => ObjectContext.PartsOutboundBillDetails.Any(d => d.SparePartCode.Contains(sparePartCode) && r.Id == d.PartsOutboundBillId));
            }
            if(sparePartName != null) {
                partsOutboundBills = partsOutboundBills.Where(r => ObjectContext.PartsOutboundBillDetails.Any(d => d.SparePartName.Contains(sparePartName) && r.Id == d.PartsOutboundBillId));
            }
            var detail = from a in ObjectContext.PartsOutboundBillDetails
                         join b in ObjectContext.PartsOutboundBills on a.PartsOutboundBillId equals b.Id
                         join c in ObjectContext.PartsRetailGuidePrices on new {
                             a.SparePartId,
                             b.PartsSalesCategoryId,
                             b.BranchId,
                             Status = (int)DcsBaseDataStatus.有效
                         } equals new {
                             c.SparePartId,
                             c.PartsSalesCategoryId,
                             c.BranchId,
                             c.Status
                         } into tempTable
                         from t2 in tempTable.DefaultIfEmpty()
                         select new {
                             PartsOutboundBillId = a.PartsOutboundBillId,
                             OutboundAmount = a.OutboundAmount,
                             SettlementPrice = a.SettlementPrice,
                             RetailGuidePrice = t2.RetailGuidePrice
                         };
            //var SumPartsOutboundBillDetailQuery = from a in detail
            //                                      group a by a.PartsOutboundBillId
            //                                          into tempTable
            //                                          select new {
            //                                              PartsOutboundBillId = tempTable.Key,
            //                                              SumTotalAmout = tempTable.Sum(r => r.OutboundAmount * r.SettlementPrice),
            //                                              TotalAmountForWarehouse = tempTable.Sum(r => r.OutboundAmount * r.RetailGuidePrice),
            //                                          };
            var query = from a in partsOutboundBills
                        //join b in SumPartsOutboundBillDetailQuery on a.Id equals b.PartsOutboundBillId
                        join c in ObjectContext.PartsOutboundPlans on a.PartsOutboundPlanId equals c.Id
                        join d in ObjectContext.PartsSalesCategories on a.PartsSalesCategoryId equals d.Id
                        //join e in ObjectContext.PartsSalesOrderTypes on a.PartsSalesOrderTypeId equals e.Id
                        join f in ObjectContext.Companies.Where(r => r.Status == (int)DcsMasterDataStatus.有效) on a.CounterpartCompanyId equals f.Id into tempTable
                        from t2 in tempTable.DefaultIfEmpty()
                        join g in ObjectContext.PartsSalesOrders.Where(r => r.Status != (int)DcsMasterDataStatus.作废) on new {
                           Id= a.OriginalRequirementBillId,
                           Code=a.OriginalRequirementBillCode
                        } equals new {
                            g.Id,
                            g.Code
                        } into tempTable1
                        from t3 in tempTable1.DefaultIfEmpty()
                        //join pd in ObjectContext.PartsOutboundBillDetails on a.Id equals pd.PartsOutboundBillId into tempTable2
                        //from t4 in tempTable2.DefaultIfEmpty()
                        select new PartsOutboundBillWithOtherInfo {
                            PartsOutboundBillId = a.Id,
                            PartsOutboundPlanId = c.Id,
                            PartsSalesCategoryId = d.Id,
                            CompanyId = t2.Id,
                            InvoiceType = t3.InvoiceType,
                            StorageCompanyId = a.StorageCompanyId,
                            TotalAmount = detail.Where(x => x.PartsOutboundBillId == a.Id).Sum(y => y.OutboundAmount * y.SettlementPrice),
                            Code = a.Code,
                            WarehouseCode = a.WarehouseCode,
                            WarehouseName = a.WarehouseName,
                            PrintTimes = a.PrintTimes,
                            WarehouseId = a.WarehouseId,
                            OutboundType = a.OutboundType,
                            OriginalRequirementBillCode = a.OriginalRequirementBillCode,
                            PartsSalesOrderTypeName = a.PartsSalesOrderTypeName,
                            PartsOutboundPlanCode = c.Code,
                            PartsSalesCategoryName = d.Name,
                            BranchId = a.BranchId,
                            BranchCode = a.BranchCode,
                            BranchName = a.BranchName,
                            CounterPartCompanyId = a.CounterpartCompanyId,
                            CounterpartCompanyCode = a.CounterpartCompanyCode,
                            CounterpartCompanyName = a.CounterpartCompanyName,
                            CompanyType = t2.Type,
                            ReceivingCompanyId = a.ReceivingCompanyId,
                            ReceivingWarehouseCode = a.ReceivingWarehouseCode,
                            ReceivingWarehouseName = a.ReceivingWarehouseName,
                            OutboundPackPlanCode = a.OutboundPackPlanCode,
                            ContractCode = a.ContractCode,
                            GPMSPurOrderCode = a.GPMSPurOrderCode,
                            InterfaceRecordId = a.InterfaceRecordId,
                            SettlementStatus = a.SettlementStatus,
                            Remark = a.Remark,
                            OrderApproveComment = a.OrderApproveComment,
                            CreatorId = a.CreatorId,
                            CreatorName = a.CreatorName,
                            CreateTime = a.CreateTime,
                            ModifierId = a.ModifierId,
                            ERPSourceOrderCode = a.ERPSourceOrderCode,
                            ModifierName = a.ModifierName,
                            ModifyTime = a.ModifyTime,
                            ProvinceName = (d.Name == "随车行" || d.Name == "欧曼电商") ? t3.Province : t2.ProvinceName,
                            SAPPurchasePlanCode = a.SAPPurchasePlanCode,
                            ZpNumber = c.ZPNUMBER,
                            PWMSOutboundCode = a.PWMSOutboundCode,
                            EcommerceMoney = a.EcommerceMoney,
                            CPPartsPurchaseOrderCode = a.CPPartsPurchaseOrderCode,
                            CPPartsInboundCheckCode = a.CPPartsInboundCheckCode,
                            IsSupplySettlement = (a.PartsPurchaseSettleCode == null) ? false : true,
                            ContainerNumber = null,
                            TotalAmountForWarehouse = detail.Where(x => x.PartsOutboundBillId == a.Id).Sum(y => y.OutboundAmount * y.RetailGuidePrice)
                            //ContainerNumber = ObjectContext.PartsOutboundBillDetails.Where(r => r.PartsOutboundBillId == a.Id).Select(w=>w.ContainerNumber).FirstOrDefault()
                            //ContainerNumber=(from pd in ObjectContext.PartsOutboundBillDetails where pd.PartsOutboundBillId == a.Id select pd).FirstOrDefault().ContainerNumber
                        };
            return query.OrderBy(r => r.PartsOutboundBillId);
        }

        public IQueryable<PartsOutboundBillWithOtherInfo> 工程车配件出库单查询(int? operatorId) {

            var query = from a in ObjectContext.OutboundOrders
                        join b in ObjectContext.WarehouseOperators.Where(v => v.OperatorId == operatorId)
                        on a.WarehouseId equals b.WarehouseId
                        select new PartsOutboundBillWithOtherInfo {
                            PartsOutboundBillId = a.PartsOutboundBillId,
                            PartsOutboundPlanId = a.PartsOutboundPlanId,
                            PartsSalesCategoryId = a.PartsSalesCategoryId,
                            Code = a.Code,
                            WarehouseId = a.WarehouseId,
                            WarehouseCode = a.WarehouseCode,
                            WarehouseName = a.WarehouseName,
                            OutboundType = a.OutboundType,
                            TotalAmount = a.zongjine.HasValue ? a.zongjine.Value : 0,
                            OriginalRequirementBillCode = a.OriginalRequirementBillCode,
                            PartsSalesOrderTypeName = a.PartsSalesOrderTypeName,
                            PartsOutboundPlanCode = a.PartsOutboundPlanCode,
                            PartsSalesCategoryName = a.Name,
                            BranchName = a.BranchName,
                            CounterpartCompanyCode = a.CounterpartCompanyCode,
                            CounterpartCompanyName = a.CounterpartCompanyName,
                            CompanyType = a.Type,
                            ReceivingWarehouseCode = a.ReceivingWarehouseCode,
                            ReceivingWarehouseName = a.ReceivingWarehouseName,
                            ProvinceName = a.ProvinceName,
                            OutboundPackPlanCode = a.OutboundPackPlanCode,
                            ContractCode = a.ContractCode,
                            GPMSPurOrderCode = a.GPMSPurOrderCode,
                            InterfaceRecordId = a.InterfaceRecordId,
                            SettlementStatus = a.SettlementStatus,
                            InvoiceType = a.InvoiceType,
                            ERPSourceOrderCode = a.ERPSourceOrderCode,
                            Remark = a.Remark,
                            OrderApproveComment = a.OrderApproveComment,
                            CreatorName = a.CreatorName,
                            CreateTime = a.CreateTime,
                            ModifierName = a.ModifierName,
                            ModifyTime = a.ModifyTime,
                            StorageCompanyId = a.StorageCompanyId
                        };
            return query.OrderBy(r => r.Code);
        }

        [Query(HasSideEffects = true)]
        public IQueryable<PartsOutboundBill> 查询退货未完成销售出库单(string[] codes) {
            var result = ObjectContext.PartsOutboundBills.Where(r => r.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件销售订单 && ObjectContext.PartsSalesReturnBills.Any(v => ObjectContext.PartsSalesReturnBillDetails.Any(d => d.PartsSalesReturnBillId == v.Id && d.PartsSalesOrderId == r.OriginalRequirementBillId) && ObjectContext.PartsInboundPlans.Any(k => k.OriginalRequirementBillId == v.Id && k.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件销售退货单 && k.Status == (int)DcsPartsInboundPlanStatus.新建)));
            if(codes.Length > 0)
                result = result.Where(r => codes.Contains(r.Code));
            return result.OrderBy(r => r.Id);
        }

        public IQueryable<PartsOutboundBill> 仓库负责人查询可退库配件出库单(int userId) {
            var result = ObjectContext.PartsOutboundBills.Where(r => ObjectContext.WarehouseOperators.Any(v => r.WarehouseId == v.WarehouseId && v.OperatorId == userId));
            return result.Include("PartsOutboundBillDetails").Include("PartsOutboundPlan").OrderBy(r => r.Id);

        }

        public PartsOutboundBill 查询可退库配件出库单及配件(int Id) {
            var partsOutboundBill = (from a in ObjectContext.PartsOutboundBills
                                     where a.Id == Id
                                     select a).Single();
            var partsOutboundBillDetails = ObjectContext.PartsOutboundBillDetails.Where(r => r.PartsOutboundBillId == partsOutboundBill.Id).ToArray();
            var partIds = partsOutboundBillDetails.Select(r => r.SparePartId).ToArray();
            var partsOutboundBillDetailsCanReturnAmount = (from b in ObjectContext.PartsLogisticBatchBillDetails
                                                           from c in ObjectContext.PartsLogisticBatchItemDetails
                                                           where Id == b.BillId && b.Id == c.PartsLogisticBatchId
                                                           select new {
                                                               b.BillId,
                                                               c.SparePartId,
                                                               CanReturnAmount = c.OutboundAmount - (c.InboundAmount ?? 0) - c.OutReturnAmount
                                                           } into tmp
                                                           group tmp by new {
                                                               tmp.BillId,
                                                               tmp.SparePartId
                                                           } into tmpgroup
                                                           select new {
                                                               tmpgroup.Key.BillId,
                                                               tmpgroup.Key.SparePartId,
                                                               SumCanReturnAmount = tmpgroup.Sum(r => r.CanReturnAmount)
                                                           }).ToArray();
            var partBranch = ObjectContext.PartsBranches.Where(r => partIds.Contains(r.PartId) && r.PartsSalesCategoryId == partsOutboundBill.PartsSalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效).ToArray();
            foreach(var item in partsOutboundBillDetails) {
                var temp = partsOutboundBillDetailsCanReturnAmount.SingleOrDefault(r => r.BillId == item.PartsOutboundBillId);
                if(temp != null) {
                    item.CanReturnAmount = temp.SumCanReturnAmount;
                }
            }
            return partsOutboundBill;
        }

        //public PartsOutboundBill 查询可退库配件出库单及配件(int Id) {
        //    //1. 查询配件出库单，连同清单  清单增加计算列：可退库数量
        //    //条件：连接配件物流批次单据清单，单据Id=配件出库单.Id,单据类型=配件出库单，根据配件Id与批次号汇总
        //    //可退库数量=配件物流批次配件清单.出库量-配件物流批次配件清单.入库数量-配件物流批次配件清单.退库数量，通过配件Id与批次号定位。
        //    var partsOutboundBill = (from a in ObjectContext.PartsOutboundBills
        //                             where a.Id == Id
        //                             select a).Single();
        //    var partsOutboundBillDetails = ObjectContext.PartsOutboundBillDetails.Where(r => r.PartsOutboundBillId == partsOutboundBill.Id).ToArray();
        //    var partsOutboundBillDetailsCanReturnAmount = (from b in ObjectContext.PartsLogisticBatchBillDetails
        //                                                   from c in ObjectContext.PartsLogisticBatchItemDetails
        //                                                   where Id == b.BillId && b.Id == c.PartsLogisticBatchId
        //                                                   select new {
        //                                                       b.BillId,
        //                                                       c.SparePartId,
        //                                                       CanReturnAmount = c.OutboundAmount - (c.InboundAmount ?? 0) - c.OutReturnAmount
        //                                                   }into tmp
        //                                                   group tmp by new {
        //                                                       tmp.BillId,
        //                                                       tmp.SparePartId
        //                                                   }into tmpgroup
        //                                                   select new {
        //                                                       tmpgroup.Key.BillId,
        //                                                       tmpgroup.Key.SparePartId,
        //                                                       SumCanReturnAmount = tmpgroup.Sum(r => r.CanReturnAmount)
        //                                                   }).ToArray();
        //    var partBranch = ObjectContext.PartsBranches.Where(r => partsOutboundBillDetails.Any(v => v.SparePartId == r.PartId) && r.PartsSalesCategoryId == partsOutboundBill.PartsSalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效).ToArray();
        //    foreach(var item in partsOutboundBillDetails) {
        //        var tempSumCanReturnAmount = partsOutboundBillDetailsCanReturnAmount.SingleOrDefault(r => r.BillId == item.PartsOutboundBillId);
        //        if(tempSumCanReturnAmount != null) {
        //            item.CanReturnAmount = tempSumCanReturnAmount.SumCanReturnAmount;
        //        }
        //        var tempPartBranch = partBranch.SingleOrDefault(r => r.PartId == item.SparePartId);
        //        if(tempPartBranch != null) {
        //            item.PartsWarhouseManageGranularity = tempPartBranch.PartsWarhouseManageGranularity;
        //        }
        //    }
        //    return partsOutboundBill;
        //}

        public IQueryable<PartsOutboundBill> GetPartsOutboundBillsByIds(int[] ids) {
            return ObjectContext.PartsOutboundBills.Where(t => ids.Contains(t.Id)).OrderBy(e => e.Id);
        }

    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:32
        //原分布类的函数全部转移到Ach                                                               

        public void InsertPartsOutboundBill(PartsOutboundBill partsOutboundBill) {
            new PartsOutboundBillAch(this).InsertPartsOutboundBill(partsOutboundBill);
        }

        public void UpdatePartsOutboundBill(PartsOutboundBill partsOutboundBill) {
            new PartsOutboundBillAch(this).UpdatePartsOutboundBill(partsOutboundBill);
        }

        public IQueryable<PartsOutboundBill> 查询待运配件出库单(int? partsOutboundPlanId, int? partsShippingOrderId) {
            return new PartsOutboundBillAch(this).查询待运配件出库单(partsOutboundPlanId, partsShippingOrderId);
        }

        public IQueryable<PartsOutboundBillWithOtherInfo> 仓库人员查询配件出库单(int? operatorId, string sparePartCode, string sparePartName) {
            return new PartsOutboundBillAch(this).仓库人员查询配件出库单(operatorId, sparePartCode, sparePartName);
        }

        public IQueryable<PartsOutboundBillWithOtherInfo> 工程车配件出库单查询(int? operatorId) {
            return new PartsOutboundBillAch(this).工程车配件出库单查询(operatorId);
        }


        [Query(HasSideEffects = true)]
        public IQueryable<PartsOutboundBill> 查询退货未完成销售出库单(string[] codes) {
            return new PartsOutboundBillAch(this).查询退货未完成销售出库单(codes);
        }

        public IQueryable<PartsOutboundBill> 仓库负责人查询可退库配件出库单(int userId) {
            return new PartsOutboundBillAch(this).仓库负责人查询可退库配件出库单(userId);
        }

        public PartsOutboundBill 查询可退库配件出库单及配件(int Id) {
            return new PartsOutboundBillAch(this).查询可退库配件出库单及配件(Id);
        }

        public IQueryable<PartsOutboundBill> GetPartsOutboundBillsByIds(int[] ids) {
            return new PartsOutboundBillAch(this).GetPartsOutboundBillsByIds(ids);
        }
    }
}
