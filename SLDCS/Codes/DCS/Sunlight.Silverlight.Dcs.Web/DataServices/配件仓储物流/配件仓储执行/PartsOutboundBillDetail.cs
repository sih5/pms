﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Data.Extensions;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsOutboundBillDetailAch : DcsSerivceAchieveBase {
        public PartsOutboundBillDetailAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public IQueryable<PartsOutboundBillDetail> GetPartsOutboundBillDetailsWithPartsOutboundBill() {
            return ObjectContext.PartsOutboundBillDetails.Include("PartsOutboundBill").OrderBy(v => v.Id);
        }

        public IEnumerable<PartsOutboundBillDetail> 按原始需求单据查询出库明细(int originalRequirementBillId, int originalRequirementBillType) {
            var partsOutboundBills = ObjectContext.PartsOutboundBills.Where(r => r.OriginalRequirementBillId == originalRequirementBillId && r.OriginalRequirementBillType == originalRequirementBillType).ToArray();
            if(partsOutboundBills.Length == 0)
                return null;
            var partsOutboundBillIds = partsOutboundBills.Select(r => r.Id).ToArray();
            return ObjectContext.PartsOutboundBillDetails.Where(r => partsOutboundBillIds.Contains(r.PartsOutboundBillId)).Include("SparePart").OrderBy(v => v.Id);
        }

        public IEnumerable<PartsOutboundBillDetail> 按原始需求单据查询出库明细不含调拨出库(int originalRequirementBillId, int originalRequirementBillType) {
            if(originalRequirementBillId == 0)
                return null;
            var partsOutboundBills = ObjectContext.PartsOutboundBills.Where(r => r.OriginalRequirementBillId == originalRequirementBillId && r.OutboundType != (int)DcsPartsOutboundType.配件调拨 && r.OriginalRequirementBillType == originalRequirementBillType).ToArray();
            if(partsOutboundBills.Length == 0)
                return null;
            var partsOutboundBillIds = partsOutboundBills.Select(r => r.Id).ToArray();
            return ObjectContext.PartsOutboundBillDetails.Where(r => partsOutboundBillIds.Contains(r.PartsOutboundBillId)).Include("SparePart").OrderBy(v => v.Id);
        }


        public IEnumerable<PartsOutboundBillDetail> 按原始需求单据查询出库明细加参数(int originalRequirementBillId, int originalRequirementBillType, int receivingCompanyId) {
            var partsOutboundBills = ObjectContext.PartsOutboundBills.Where(r => r.OriginalRequirementBillId == originalRequirementBillId && r.OriginalRequirementBillType == originalRequirementBillType && r.ReceivingCompanyId == receivingCompanyId).ToArray();
            if(partsOutboundBills.Length == 0)
                return null;
            var partsOutboundPlanIds = partsOutboundBills.Select(r => r.PartsOutboundPlanId).ToArray();
            var partsOutboundPlans = ObjectContext.PartsOutboundPlans.Where(r => partsOutboundPlanIds.Contains(r.Id)).ToArray();
            if(partsOutboundPlans.Length == 0)
                return null;
            var partsOutboundBillIds = partsOutboundBills.Select(r => r.Id).ToArray();
            return ObjectContext.PartsOutboundBillDetails.Where(r => partsOutboundBillIds.Contains(r.PartsOutboundBillId)).OrderBy(v => v.Id);
        }

        public IQueryable<PartsOutboundBillDetail> GetPartsOutboundBillDetailsWithPartsBranch() {
            return ObjectContext.PartsOutboundBillDetails.Include("PartsBranchs").OrderBy(v => v.Id);
        }

        public IQueryable<PartsOutboundBillDetail> GetPartsOutboundBillDetailsWithSparePart() {
            return ObjectContext.PartsOutboundBillDetails.Include("SparePart").OrderBy(v => v.Id);
        }

        public IQueryable<PartsOutboundBillDetail> GetPartsOutboundBillDetailsByHandId(int? handId) {
            if(!handId.HasValue)
                return null;
            var partsSalesOrders = this.ObjectContext.PartsSalesOrders.Where(r => r.VehiclePartsHandleOrderId == handId);
            var partsOutboundBills = this.ObjectContext.PartsOutboundBills.Where(r => r.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件销售订单 && r.OutboundType == (int)DcsPartsOutboundType.配件销售 && partsSalesOrders.Any(v => v.Id == r.OriginalRequirementBillId));
            return this.ObjectContext.PartsOutboundBillDetails.Include("PartsOutboundBill.PartsOutboundPlan").Where(r => partsOutboundBills.Any(v => v.Id == r.PartsOutboundBillId)).OrderBy(v => v.Id);
        }
        public IEnumerable<VirtualPartsOutboundBillDetail> GetPartsOutboundBillDetailsWithSparePart2(int partsOutboundBillId) {
            var result = (from a in ObjectContext.PartsOutboundBillDetails
                          join c in ObjectContext.SpareParts on a.SparePartId equals c.Id
                          join d in ObjectContext.PartsRetailGuidePrices on new {
                              a.SparePartId,                             
                              Status = (int)DcsBaseDataStatus.有效
                          } equals new {
                              d.SparePartId,                             
                              d.Status
                          }
                          select new VirtualPartsOutboundBillDetail {
                              Id = a.Id,
                              SparePartCode = a.SparePartCode,
                              SparePartName = a.SparePartName,
                              PartsOutboundBillId = a.PartsOutboundBillId,
                              Remark = a.Remark,
                              SettlementPrice = d.RetailGuidePrice,
                              MeasureUnit = c.MeasureUnit,
                              OutboundAmount = a.OutboundAmount,
                              WarehouseAreaCode = a.WarehouseAreaCode
                          }).ToArray();
            return result.OrderBy(r => r.Id);
        }

        public IEnumerable<VirtualPartsOutboundBillDetail> 退货节点选取配件销售单(int? submitCompanyId, int? salesUnitId,string partsSalesOrderCode,int?partsSalesOrderTypeId,string sparePartCode,string sparePartName,bool? ifDirectProvision,DateTime? bCreateTime,DateTime? eCreateTime) {
            string SQL = @"select rownum as Id,pp.*
							  from (select  
										   a.SparePartId,
										   a.SparePartCode,
										   a.SparePartName,
										   a.Remark,
										   a.SettlementPrice,
										   d.MeasureUnit,
										   a.BatchNumber,
										   a.OriginalPrice,
										   sum(a.OutboundAmount)- nvl(re.ApproveQuantity, 0) as ReturnedQuantity,
										   a.WarehouseAreaCode,
										   b.WarehouseId,
										   b.WarehouseCode,
										   b.WarehouseName,
										   c.Id as PartsSalesOrderId,
										   c.Code as PartsSalesOrderCode,
										   c.SubmitCompanyId,
										   c.SalesUnitId,
										   c.SubmitTime,
										   c.PartsSalesOrderTypeId,
										   c.PartsSalesOrderTypeName,
										   c.IfDirectProvision,
										   c.ERPSourceOrderCode,
										   c.CreateTime,
										   nvl(c.SubmitTime,c.CreateTime) as PartsSalesOrderSubmitTime,
										   d.TraceProperty
									  from PartsOutboundBillDetail a
									  join PartsOutboundBill b
										on a.partsoutboundbillid = b.id
									  join PartsSalesOrder c
										on b.originalrequirementbillid = c.id
									  join sparepart d
										on a.sparepartid = d.id
									   and d.status = 1
									  left join (select d.partssalesorderid,
													   d.sparepartid,
													   sum(decode(j.status,
																  6,
																  (d.approvequantity -
																  nvl(pu.plannedamount, 0)),
																  d.returnedquantity)) as ApproveQuantity
												  from PartsSalesReturnBillDetail d
												  join partssalesreturnbill j
													on d.partssalesreturnbillid = j.id
												  left join (select pl.OriginalRequirementBillId,
																   pld.sparepartid,
																   sum(pld.plannedamount-nvl(pld.OutboundFulfillment,0)) as plannedamount
															  from partsoutboundplan pl
															  join PartsOutboundPlanDetail pld
																on pl.id = pld.partsoutboundplanid
															 where pl.status = 4
															 group by pl.OriginalRequirementBillId,
																	  pld.sparepartid) pu
													on j.id = pu.OriginalRequirementBillId
												   and d.sparepartid = pu.sparepartid
												 where j.status <> 99
												   and j.status <> 4
												 group by d.partssalesorderid, d.sparepartid) re
										on c.id = re.partssalesorderid
									   and a.sparepartid = re.sparepartid
									 where b.OutboundType != 4
									   and b.originalrequirementbilltype = 1
									   and c.status in (4, 5, 6)
									   {0}
									 group by  
											  a.SparePartId,
											  a.SparePartCode,
											  a.SparePartName,
											  a.Remark,
											  a.SettlementPrice,
											  d.MeasureUnit,
											  a.BatchNumber,
											  a.OriginalPrice,
											  a.WarehouseAreaCode,
											  b.WarehouseId,
											  b.WarehouseCode,
											  b.WarehouseName,
											  c.Id,
											  c.Code,
											  c.SubmitCompanyId,
											  c.SalesUnitId,
											  c.SubmitTime,
											  c.PartsSalesOrderTypeId,
											  c.PartsSalesOrderTypeName,
											  c.IfDirectProvision,
											  c.ERPSourceOrderCode,
											  c.CreateTime,
											   nvl(c.SubmitTime,c.CreateTime) ,
											  d.TraceProperty, nvl(re.ApproveQuantity, 0))pp
							 where ReturnedQuantity > 0";
			var serachSQL = new StringBuilder();
			serachSQL.Append(" and c.SubmitCompanyId =  "+submitCompanyId);
			serachSQL.Append(" and c.SalesUnitId =  "+salesUnitId);
            if(ifDirectProvision.HasValue && ifDirectProvision.Value== true) {
				serachSQL.Append(" and c.IfDirectProvision = 1 ");
            }
			if(ifDirectProvision.HasValue && ifDirectProvision.Value== false) {
				serachSQL.Append(" and c.IfDirectProvision = 0 ");
            }
            if(!string.IsNullOrEmpty(partsSalesOrderCode)) {
                serachSQL.Append(" and LOWER(c.code) like '%" + partsSalesOrderCode.ToLower() + "%'");
            }
			if(partsSalesOrderTypeId.HasValue){
				serachSQL.Append(" and c.PartsSalesOrderTypeId =" +partsSalesOrderTypeId);
			}
            if(!string.IsNullOrEmpty(sparePartCode)) {
                serachSQL.Append("and LOWER(a.SparePartCode) like '%" + sparePartCode.ToLower() + "%'");
            }
            if(!string.IsNullOrEmpty(sparePartName)) {
                serachSQL.Append(" and LOWER(a.SparePartName) like '%" + sparePartName.ToLower() + "%'");
            }
            if(bCreateTime.HasValue) {
                serachSQL.Append( " and c.CreateTime>= to_date('" + bCreateTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')");
            }
            if(eCreateTime.HasValue) {
                serachSQL.Append( " and c.CreateTime<= to_date('" + eCreateTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')");
            }

            SQL = string.Format(SQL, serachSQL);
           
            var search = ObjectContext.ExecuteStoreQuery<VirtualPartsOutboundBillDetail>(SQL).ToList();
            return search;
            //var status = new[] {
            //    (int)DcsPartsSalesOrderStatus.部分审批, (int)DcsPartsSalesOrderStatus.审批完成, (int)DcsPartsSalesOrderStatus.终止
            //};
            //var result = (from a in this.ObjectContext.PartsOutboundBillDetails
            //              join b in this.ObjectContext.PartsOutboundBills.Where(r => r.OutboundType != (int)DcsPartsOutboundType.配件调拨 && r.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件销售订单) on a.PartsOutboundBillId equals b.Id
            //              join c in this.ObjectContext.PartsSalesOrders.Where(r => status.Contains(r.Status)&& r.SubmitCompanyId==submitCompanyId&& r.SalesUnitId==salesUnitId) on b.OriginalRequirementBillId equals c.Id
            //              join d in this.ObjectContext.SpareParts.Where(r => r.Status == (int)DcsMasterDataStatus.有效) on a.SparePartId equals d.Id
            //              select new VirtualPartsOutboundBillDetail {
            //                  Id = a.Id,
            //                  SparePartId = a.SparePartId,
            //                  SparePartCode = a.SparePartCode,
            //                  SparePartName = a.SparePartName,
            //                  PartsOutboundBillId = a.PartsOutboundBillId,
            //                  Remark = a.Remark,
            //                  SettlementPrice = a.SettlementPrice,
            //                  MeasureUnit = d.MeasureUnit,
            //                  BatchNumber = a.BatchNumber,
            //                  OriginalPrice = a.OriginalPrice,
            //                  OutboundAmount = a.OutboundAmount,
            //                  WarehouseAreaCode = a.WarehouseAreaCode,
            //                  WarehouseId = b.WarehouseId,
            //                  WarehouseCode = b.WarehouseCode,
            //                  WarehouseName = b.WarehouseName,
            //                  PartsSalesOrderId = c.Id,
            //                  PartsSalesOrderCode = c.Code,
            //                  SubmitCompanyId = c.SubmitCompanyId,
            //                  SalesUnitId = c.SalesUnitId,
            //                  SubmitTime = c.SubmitTime,
            //                  PartsSalesOrderTypeId = c.PartsSalesOrderTypeId,
            //                  PartsSalesOrderTypeName = c.PartsSalesOrderTypeName,
            //                  IfDirectProvision = c.IfDirectProvision,
            //                  ERPSourceOrderCode = c.ERPSourceOrderCode,
            //                  CreateTime = c.CreateTime,
            //                  PartsSalesOrderSubmitTime = c.SubmitTime,
            //                  TraceProperty=d.TraceProperty
            //              }).ToArray();

            //var partsOutboundBillDetails = result.GroupBy(r => new {
            //    r.PartsSalesOrderId,
            //    r.SparePartId
            //}).Select(r => new {
            //    r.Key.SparePartId,
            //    r.Key.PartsSalesOrderId,
            //    OutboundAmount = r.Sum(t => t.OutboundAmount),
            //    r.First().Id,
            //    r.First().SparePartCode,
            //    r.First().SparePartName,
            //    r.First().PartsOutboundBillId,
            //    r.First().Remark,
            //    r.First().SettlementPrice,
            //    r.First().MeasureUnit,
            //    r.First().BatchNumber,
            //    r.First().OriginalPrice,
            //    r.First().WarehouseAreaCode,
            //    r.First().WarehouseId,
            //    r.First().WarehouseCode,
            //    r.First().WarehouseName,
            //    r.First().PartsSalesOrderCode,
            //    r.First().SubmitCompanyId,
            //    r.First().SalesUnitId,
            //    r.First().SubmitTime,
            //    r.First().PartsSalesOrderTypeId,
            //    r.First().PartsSalesOrderTypeName,
            //    r.First().IfDirectProvision,
            //    r.First().ERPSourceOrderCode,
            //    r.First().CreateTime,
            //    r.First().PartsSalesOrderSubmitTime,
            //    r.First().TraceProperty
            //}).ToArray();
            //var originalPartsSalesReturnBillDetails = (from c in partsOutboundBillDetails
            //                                           join a in ObjectContext.PartsSalesReturnBillDetails on new {
            //                                               c.SparePartId,
            //                                               c.PartsSalesOrderId
            //                                           } equals new {
            //                                               a.SparePartId,
            //                                               PartsSalesOrderId = a.PartsSalesOrderId == null? 0: a.PartsSalesOrderId.Value
            //                                           }
            //                                           join b in ObjectContext.PartsSalesReturnBills.Where(v => v.Status != (int)DcsPartsSalesReturnBillStatus.作废 && v.Status != (int)DcsPartsSalesReturnBillStatus.终止) on a.PartsSalesReturnBillId equals b.Id
            //                                           select new {
            //                                               a.PartsSalesOrderId,
            //                                               a.SparePartId,
            //                                               ApproveQuantity=b.Status==(int)DcsPartsSalesReturnBillStatus.审批通过? a.ApproveQuantity.Value:a.ReturnedQuantity
            //                                           }).GroupBy(r => new {
            //                                               r.PartsSalesOrderId,
            //                                               r.SparePartId
            //                                           }).Select(r => new {
            //                                               r.Key.SparePartId,
            //                                               r.Key.PartsSalesOrderId,
            //                                               ApproveQuantity = r.Sum(t => t.ApproveQuantity)
            //                                           });

            //var result1 = from a in partsOutboundBillDetails
            //              join c in originalPartsSalesReturnBillDetails on new {
            //                  orderId = a.PartsSalesOrderId,
            //                  a.SparePartId
            //              } equals new {
            //                  orderId = c.PartsSalesOrderId ?? 0,
            //                  c.SparePartId
            //              } into temp1
            //              from item1 in temp1.DefaultIfEmpty()
            //              select new VirtualPartsOutboundBillDetail {
            //                  Id = a.Id,
            //                  SparePartId = a.SparePartId,
            //                  SparePartCode = a.SparePartCode,
            //                  SparePartName = a.SparePartName,
            //                  PartsOutboundBillId = a.PartsOutboundBillId,
            //                  Remark = a.Remark,
            //                  SettlementPrice = a.SettlementPrice,
            //                  MeasureUnit = a.MeasureUnit,
            //                  BatchNumber = a.BatchNumber,
            //                  OriginalPrice = a.OriginalPrice,
            //                  OutboundAmount = a.OutboundAmount,
            //                  WarehouseAreaCode = a.WarehouseAreaCode,
            //                  WarehouseId = a.WarehouseId,
            //                  WarehouseCode = a.WarehouseCode,
            //                  WarehouseName = a.WarehouseName,
            //                  PartsSalesOrderId = a.PartsSalesOrderId,
            //                  PartsSalesOrderCode = a.PartsSalesOrderCode,
            //                  SubmitCompanyId = a.SubmitCompanyId,
            //                  SalesUnitId = a.SalesUnitId,
            //                  SubmitTime = a.SubmitTime,
            //                  PartsSalesOrderTypeId = a.PartsSalesOrderTypeId,
            //                  PartsSalesOrderTypeName = a.PartsSalesOrderTypeName,
            //                  IfDirectProvision = a.IfDirectProvision,
            //                  ERPSourceOrderCode = a.ERPSourceOrderCode,
            //                  CreateTime = a.CreateTime,
            //                  ReturnedQuantity = a.OutboundAmount - (item1 == null ? 0 : item1.ApproveQuantity),
            //                  PartsSalesOrderSubmitTime = a.PartsSalesOrderSubmitTime,
            //                  TraceProperty=a.TraceProperty
            //              };
            //return result1.Where(r => r.ReturnedQuantity > 0).OrderBy(r => r.Id);
        }
        public IEnumerable<VirtualPartsOutboundBillDetail> 退货节点选取配件销售单校验(int?[] partsSalesOrderIds, int[] sparePartIds) {
            string SQL = @"select rownum as Id,pp.*
							  from (select 
										   a.SparePartId,
										   a.SparePartCode,
										   a.SparePartName,
										   a.Remark,
										   a.SettlementPrice,
										   d.MeasureUnit,
										   a.BatchNumber,
										   a.OriginalPrice,
										   sum(a.OutboundAmount ) - nvl(re.ApproveQuantity, 0) as ReturnedQuantity,
										   a.WarehouseAreaCode,
										   b.WarehouseId,
										   b.WarehouseCode,
										   b.WarehouseName,
										   c.Id as PartsSalesOrderId,
										   c.Code as PartsSalesOrderCode,
										   c.SubmitCompanyId,
										   c.SalesUnitId,
										   c.SubmitTime,
										   c.PartsSalesOrderTypeId,
										   c.PartsSalesOrderTypeName,
										   c.IfDirectProvision,
										   c.ERPSourceOrderCode,
										   c.CreateTime,
										   nvl(c.SubmitTime,c.CreateTime) as PartsSalesOrderSubmitTime,
										   d.TraceProperty
									  from PartsOutboundBillDetail a
									  join PartsOutboundBill b
										on a.partsoutboundbillid = b.id
									  join PartsSalesOrder c
										on b.originalrequirementbillid = c.id
									  join sparepart d
										on a.sparepartid = d.id
									   and d.status = 1
									  left join (select d.partssalesorderid,
													   d.sparepartid,
													   sum(decode(j.status,
																  6,
																  (d.approvequantity -
																  nvl(pu.plannedamount, 0)),
																  d.returnedquantity)) as ApproveQuantity
												  from PartsSalesReturnBillDetail d
												  join partssalesreturnbill j
													on d.partssalesreturnbillid = j.id
												  left join (select pl.OriginalRequirementBillId,
																   pld.sparepartid,
																   sum(pld.plannedamount-nvl(pld.OutboundFulfillment,0)) as plannedamount
															  from partsoutboundplan pl
															  join PartsOutboundPlanDetail pld
																on pl.id = pld.partsoutboundplanid
															 where pl.status = 4
															 group by pl.OriginalRequirementBillId,
																	  pld.sparepartid) pu
													on j.id = pu.OriginalRequirementBillId
												   and d.sparepartid = pu.sparepartid
												 where j.status <> 99
												   and j.status <> 4
												 group by d.partssalesorderid, d.sparepartid) re
										on c.id = re.partssalesorderid
									   and a.sparepartid = re.sparepartid
									 where b.OutboundType != 4
									   and b.originalrequirementbilltype = 1
									   and c.status in (4, 5, 6)
									   {0}
									 group by 
											  a.SparePartId,
											  a.SparePartCode,
											  a.SparePartName,
											  a.Remark,
											  a.SettlementPrice,
											  d.MeasureUnit,
											  a.BatchNumber,
											  a.OriginalPrice,
											  a.WarehouseAreaCode,
											  b.WarehouseId,
											  b.WarehouseCode,
											  b.WarehouseName,
											  c.Id,
											  c.Code,
											  c.SubmitCompanyId,
											  c.SalesUnitId,
											  c.SubmitTime,
											  c.PartsSalesOrderTypeId,
											  c.PartsSalesOrderTypeName,
											  c.IfDirectProvision,
											  c.ERPSourceOrderCode,
											  c.CreateTime,
											   nvl(c.SubmitTime,c.CreateTime) ,
											  d.TraceProperty,nvl(re.ApproveQuantity, 0)) pp
							 where ReturnedQuantity > 0";
            var serachSQL = new StringBuilder();           
            serachSQL.Append(" and c.Id in (");
            for(var i = 0; i < partsSalesOrderIds.Length; i++) {
                if(partsSalesOrderIds.Length == i + 1) {
                    serachSQL.Append(partsSalesOrderIds[i].ToString());
                } else {
                    serachSQL.Append(partsSalesOrderIds[i].ToString() + ",");
                }
            }
            serachSQL.Append(") ");
            serachSQL.Append(" and a.SparePartId in (");
            for(var i = 0; i < sparePartIds.Length; i++) {
                if(sparePartIds.Length == i + 1) {
                    serachSQL.Append(sparePartIds[i].ToString());
                } else {
                    serachSQL.Append(sparePartIds[i].ToString() + ",");
                }
            }
            serachSQL.Append(") ");
            SQL = string.Format(SQL, serachSQL);
            var search = ObjectContext.ExecuteStoreQuery<VirtualPartsOutboundBillDetail>(SQL).ToList();
            return search;
        }
        public IEnumerable<PartsOutboundBillDetail> 跨区域销售出库看板(string originalRequirementBillCode, int originalRequirementBillType) {
            var partsOutboundBills = ObjectContext.PartsOutboundBills.Where(r => r.OriginalRequirementBillCode == originalRequirementBillCode && r.OriginalRequirementBillType == originalRequirementBillType ).ToArray();
            if(partsOutboundBills.Length == 0)
                return null;
            var partsOutboundPlanIds = partsOutboundBills.Select(r => r.PartsOutboundPlanId).ToArray();
            var partsOutboundPlans = ObjectContext.PartsOutboundPlans.Where(r => partsOutboundPlanIds.Contains(r.Id)).ToArray();
            if(partsOutboundPlans.Length == 0)
                return null;
            var partsOutboundBillIds = partsOutboundBills.Select(r => r.Id).ToArray();
            return ObjectContext.PartsOutboundBillDetails.Where(r => partsOutboundBillIds.Contains(r.PartsOutboundBillId)).OrderBy(v => v.Id);
        }
    }
 

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:32
        //原分布类的函数全部转移到Ach                                                               

        public IQueryable<PartsOutboundBillDetail> GetPartsOutboundBillDetailsWithPartsOutboundBill() {
            return new PartsOutboundBillDetailAch(this).GetPartsOutboundBillDetailsWithPartsOutboundBill();
        }

        public IEnumerable<PartsOutboundBillDetail> 按原始需求单据查询出库明细(int originalRequirementBillId, int originalRequirementBillType) {
            return new PartsOutboundBillDetailAch(this).按原始需求单据查询出库明细(originalRequirementBillId, originalRequirementBillType);
        }

        public IEnumerable<PartsOutboundBillDetail> 按原始需求单据查询出库明细不含调拨出库(int originalRequirementBillId, int originalRequirementBillType) {
            return new PartsOutboundBillDetailAch(this).按原始需求单据查询出库明细不含调拨出库(originalRequirementBillId, originalRequirementBillType);
        }

        public IEnumerable<PartsOutboundBillDetail> 按原始需求单据查询出库明细加参数(int originalRequirementBillId, int originalRequirementBillType, int receivingCompanyId) {
            return new PartsOutboundBillDetailAch(this).按原始需求单据查询出库明细加参数(originalRequirementBillId, originalRequirementBillType, receivingCompanyId);
        }

        public IQueryable<PartsOutboundBillDetail> GetPartsOutboundBillDetailsWithPartsBranch() {
            return new PartsOutboundBillDetailAch(this).GetPartsOutboundBillDetailsWithPartsBranch();
        }

        public IQueryable<PartsOutboundBillDetail> GetPartsOutboundBillDetailsWithSparePart() {
            return new PartsOutboundBillDetailAch(this).GetPartsOutboundBillDetailsWithSparePart();
        }

        public IQueryable<PartsOutboundBillDetail> GetPartsOutboundBillDetailsByHandId(int? handId) {
            return new PartsOutboundBillDetailAch(this).GetPartsOutboundBillDetailsByHandId(handId);
        }
        public IEnumerable<VirtualPartsOutboundBillDetail> GetPartsOutboundBillDetailsWithSparePart2(int partsOutboundBillId) {
            return new PartsOutboundBillDetailAch(this).GetPartsOutboundBillDetailsWithSparePart2(partsOutboundBillId);
        }
        public IEnumerable<VirtualPartsOutboundBillDetail> 退货节点选取配件销售单(int? submitCompanyId, int? salesUnitId,string partsSalesOrderCode,int?partsSalesOrderTypeId,string sparePartCode,string sparePartName,bool? ifDirectProvision,DateTime?bCreateTime,DateTime?eCreateTime) {
            return new PartsOutboundBillDetailAch(this).退货节点选取配件销售单(submitCompanyId, salesUnitId,partsSalesOrderCode,partsSalesOrderTypeId,sparePartCode,sparePartName,ifDirectProvision,bCreateTime,eCreateTime);
        }
        public IEnumerable<VirtualPartsOutboundBillDetail> 退货节点选取配件销售单校验(int?[] partsSalesOrderIds, int[] sparePartIds) {
            return new PartsOutboundBillDetailAch(this).退货节点选取配件销售单校验(partsSalesOrderIds, sparePartIds);

        }
        public IEnumerable<PartsOutboundBillDetail> 跨区域销售出库看板(string originalRequirementBillCode, int originalRequirementBillType) {
            return new PartsOutboundBillDetailAch(this).跨区域销售出库看板(originalRequirementBillCode, originalRequirementBillType);
        }
    }
}