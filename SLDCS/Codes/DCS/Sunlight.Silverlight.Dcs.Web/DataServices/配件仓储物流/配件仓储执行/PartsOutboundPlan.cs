﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsOutboundPlanAch : DcsSerivceAchieveBase {
        internal void InsertPartsOutboundPlanValidate(PartsOutboundPlan partsOutboundPlan) {
            if(string.IsNullOrWhiteSpace(partsOutboundPlan.Code) || partsOutboundPlan.Code == GlobalVar.ASSIGNED_BY_SERVER)
                partsOutboundPlan.Code = CodeGenerator.Generate("PartsOutboundPlan", partsOutboundPlan.StorageCompanyCode);
            var userInfo = Utils.GetCurrentUserInfo();
            partsOutboundPlan.CreatorId = userInfo.Id;
            partsOutboundPlan.CreatorName = userInfo.Name;
            partsOutboundPlan.CreateTime = DateTime.Now;
        }

        internal void UpdatePartsOutboundPlanValidate(PartsOutboundPlan partsOutboundPlan) {
            var userInfo = Utils.GetCurrentUserInfo();
            partsOutboundPlan.ModifierId = userInfo.Id;
            partsOutboundPlan.ModifierName = userInfo.Name;
            partsOutboundPlan.ModifyTime = DateTime.Now;
        }

        public void InsertPartsOutboundPlan(PartsOutboundPlan partsOutboundPlan) {
            InsertToDatabase(partsOutboundPlan);
            var partsOutboundPlanDetails = ChangeSet.GetAssociatedChanges(partsOutboundPlan, v => v.PartsOutboundPlanDetails, ChangeOperation.Insert);
            foreach(PartsOutboundPlanDetail partsOutboundPlanDetail in partsOutboundPlanDetails)
                InsertToDatabase(partsOutboundPlanDetail);
            this.InsertPartsOutboundPlanValidate(partsOutboundPlan);
        }

        public void UpdatePartsOutboundPlan(PartsOutboundPlan partsOutboundPlan) {
            partsOutboundPlan.PartsOutboundPlanDetails.Clear();
            UpdateToDatabase(partsOutboundPlan);
            var partsOutboundPlanDetails = ChangeSet.GetAssociatedChanges(partsOutboundPlan, v => v.PartsOutboundPlanDetails);
            foreach(PartsOutboundPlanDetail partsOutboundPlanDetail in partsOutboundPlanDetails) {
                //获取清单实体对象的操作类型
                switch(ChangeSet.GetChangeOperation(partsOutboundPlanDetail)) {
                    //新增操作
                    case ChangeOperation.Insert:
                        InsertToDatabase(partsOutboundPlanDetail);
                        break;
                    //更新操作
                    case ChangeOperation.Update:
                        UpdateToDatabase(partsOutboundPlanDetail);
                        break;
                    //删除操作
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(partsOutboundPlanDetail);
                        break;
                }
            }
            this.UpdatePartsOutboundPlanValidate(partsOutboundPlan);
        }

        public PartsOutboundPlan GetPartsOutboundPlanWithDetails(int id) {
            var partsOutboundPlan = ObjectContext.PartsOutboundPlans.Include("PartsSalesCategory").SingleOrDefault(r => r.Id == id);
            if(partsOutboundPlan != null) {
                var partsOutboundPlanDetails = ObjectContext.PartsOutboundPlanDetails.Where(r => r.PartsOutboundPlanId == partsOutboundPlan.Id && (r.OutboundFulfillment == null || r.PlannedAmount != r.OutboundFulfillment)).ToArray();
            }
            return partsOutboundPlan;
        }

        public IQueryable<PartsOutboundPlan> GetPartsOutboundPlansWithPartsCategory(string sparePartCode, string sparePartName) {
            var userInfo = Utils.GetCurrentUserInfo();
            var partsOutboundPlans = ObjectContext.PartsOutboundPlans.Where(r => r.StorageCompanyId == userInfo.EnterpriseId);
            if(sparePartCode != null) {
                partsOutboundPlans = partsOutboundPlans.Where(s => ObjectContext.PartsOutboundPlanDetails.Any(d => d.SparePartCode.Contains(sparePartCode) && s.Id == d.PartsOutboundPlanId));
            }
            if(sparePartName != null) {
                partsOutboundPlans = partsOutboundPlans.Where(s => ObjectContext.PartsOutboundPlanDetails.Any(d => d.SparePartName.Contains(sparePartName) && s.Id == d.PartsOutboundPlanId));
            }
            partsOutboundPlans = partsOutboundPlans.Where(r => ObjectContext.WarehouseOperators.Any(v => v.OperatorId == userInfo.Id && v.WarehouseId == r.WarehouseId) );

            var result = from a in partsOutboundPlans
                         join b in ObjectContext.PartsShippingOrderDetails on a.Id equals b.PartsOutboundPlanId into tempTable
                         from temp1 in tempTable.DefaultIfEmpty()
                         join c in ObjectContext.PartsShippingOrders.Where(r => r.Status != (int)DcsPartsShippingOrderStatus.作废) on temp1.PartsShippingOrderId equals c.Id into tempTable2
                         from temp2 in tempTable2.DefaultIfEmpty()
                         select new {
                             a,
                             PartsShippingOrderCode = temp2.Code
                         };

            var result1 = result.Select(q => q.a).Distinct();
            result1 = result1.OrderBy(e => e.Id);
            var ids1 = DcsDomainService.QueryComposer.Compose(result1, ParamQueryable).Cast<PartsOutboundPlan>().Select(q => q.Id).ToArray();

            var haveInternal = result.Where(r => ObjectContext.InternalAllocationBills.Any(t => t.SourceCode == r.a.OriginalRequirementBillCode));
            var temp = from q in result.Where(p => ids1.Contains(p.a.Id))
                       join d in haveInternal on q.a.Id equals d.a.Id into dd
                       from dt in dd.DefaultIfEmpty()
                       select new {
                           q.a,
                           PartsShippingOrderCode = q.PartsShippingOrderCode,
                           IsInternalAllocationBill = dt.a.Id == null ? false : true
                       };
            foreach(var tc in temp) {
                if(tc.PartsShippingOrderCode != null)
                    tc.a.PartsShippingOrderCode = tc.PartsShippingOrderCode;
                tc.a.IsInternalAllocationBill = tc.IsInternalAllocationBill;
            }

            return result1.Include("PartsSalesCategory").Include("PartsOutboundPlanDetails");
        }

        public IQueryable<PartsOutboundPlan> 保管区负责人查询配件出库计划(int? userinfoId) {
            var userInfo = Utils.GetCurrentUserInfo();
            var warehouses = ObjectContext.Warehouses.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && (r.PwmsInterface == null || r.PwmsInterface == false)).Select(r => r.Id);
            var warehouseAreas = ObjectContext.WarehouseAreas.Where(r => warehouses.Contains(r.WarehouseId) && r.AreaKind == (int)DcsAreaKind.库位 && r.Status == (int)DcsBaseDataStatus.有效 && ObjectContext.WarehouseAreaCategories.Any(v => v.Category == (int)DcsAreaType.保管区 && v.Id == r.AreaCategoryId));
            if(userinfoId.HasValue)
                warehouseAreas = warehouseAreas.Where(r => ObjectContext.WarehouseAreaManagers.Any(v => v.ManagerId == userInfo.Id && v.WarehouseAreaId == r.TopLevelWarehouseAreaId));
            //
            //var warehouseToDisplay = new[] { "海外中重卡北京CDC仓库", "海外轻卡北京CDC仓库", "海外乘用车北京CDC仓库" };

            //var partsStock =
            var partsOutboundPlans = ObjectContext.PartsOutboundPlans.Where(r => r.IfWmsInterface == false && (r.Status == (int)DcsPartsOutboundPlanStatus.新建 || r.Status == (int)DcsPartsOutboundPlanStatus.部分出库) && warehouseAreas.Any(v => r.WarehouseId == v.WarehouseId) && !(ObjectContext.Warehouses.Any(o => o.Id == r.WarehouseId && o.WmsInterface) && (r.OutboundType == (int)DcsPartsOutboundType.采购退货 || r.OutboundType == (int)DcsPartsOutboundType.内部领出 || (r.OutboundType == (int)DcsPartsOutboundType.配件销售) || (r.OutboundType == (int)DcsPartsOutboundType.配件调拨 && (from a in ObjectContext.PartsTransferOrders
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      join b in ObjectContext.Warehouses on a.OriginalWarehouseId equals b.Id
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      join c in ObjectContext.Warehouses on a.DestWarehouseId equals c.Id
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      where (b.StorageCenter != c.StorageCenter || !c.WmsInterface) && a.Id == r.OriginalRequirementBillId
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      select 1).Any()))));
            var partsOutboundPlanss = ObjectContext.PartsOutboundPlans.Where(r => ObjectContext.WarehouseAreaManagers.Any(m => m.ManagerId == userInfo.Id && ObjectContext.WarehouseAreas.Where(c => ObjectContext.PartsStocks.Any(x => r.WarehouseId == x.WarehouseId && c.Id == x.WarehouseAreaId && this.ObjectContext.PartsOutboundPlanDetails.Any(y => (y.PlannedAmount - (y.OutboundFulfillment ?? 0) > 0) && y.PartsOutboundPlanId == r.Id && x.PartId == y.SparePartId)) && this.ObjectContext.WarehouseAreaCategories.Any(d => d.Id == c.AreaCategoryId && d.Category == (int)DcsAreaType.保管区) && c.TopLevelWarehouseAreaId != null).Select(e => e.TopLevelWarehouseAreaId).Contains(m.WarehouseAreaId)));

            return partsOutboundPlans.Include("PartsOutboundPlanDetails").Include("PartsSalesCategory").Where(r => partsOutboundPlanss.Any(x => x.Id == r.Id)).OrderBy(r => r.Id);
        }

        public IEnumerable<TaskListPartsOutboundPlan> 仓库人员查询待发运出库计划(int? operatorId, DateTime? createTimeForm, DateTime? createTimeTo) {
            var userInfo = Utils.GetCurrentUserInfo();
            var company = ObjectContext.Companies.FirstOrDefault(r => r.Id == userInfo.EnterpriseId);

            if(company.Type == (int)DcsCompanyType.分公司) {
                var result = from pop in ObjectContext.PartsOutboundPlans
                             join butd in this.ObjectContext.BoxUpTaskDetails on pop.Id equals butd.PartsOutboundPlanId
                             join bt in this.ObjectContext.BoxUpTasks on butd.BoxUpTaskId equals bt.Id
                             join d in ObjectContext.PartsSalesOrders on pop.OriginalRequirementBillId equals d.Id into tempTable
                             from pso in tempTable.DefaultIfEmpty()
                             where (bt.IsExistShippingOrder.HasValue ? bt.IsExistShippingOrder.Value : false) == false && bt.Status == (int)DcsBoxUpTaskStatus.装箱完成
                             && (createTimeForm.HasValue ? bt.BoxUpFinishTime >= createTimeForm.Value : true) && (createTimeTo.HasValue ? bt.BoxUpFinishTime <= createTimeTo.Value : true)
                             select new TaskListPartsOutboundPlan {
                                 TaskId = bt.Id,
                                 TaskCode = bt.Code,
                                 //TaskStatus = "装箱完成",
                                 Id = pop.Id,
                                 Code = pop.Code,
                                 WarehouseId = pop.WarehouseId,
                                 WarehouseCode = pop.WarehouseCode,
                                 WarehouseName = pop.WarehouseName,
                                 StorageCompanyId = pop.StorageCompanyId,
                                 StorageCompanyCode = pop.StorageCompanyCode,
                                 StorageCompanyName = pop.StorageCompanyName,
                                 StorageCompanyType = pop.StorageCompanyType,
                                 CompanyAddressId = pop.CompanyAddressId.Value,
                                 BranchId = pop.BranchId,
                                 BranchCode = pop.BranchCode,
                                 BranchName = pop.BranchName,
                                 PartsSalesCategoryId = pop.PartsSalesCategoryId,
                                 PartsSalesOrderTypeId = pop.PartsSalesOrderTypeId.Value,
                                 PartsSalesOrderTypeName = pop.PartsSalesOrderTypeName,
                                 CounterpartCompanyId = pop.CounterpartCompanyId,
                                 CounterpartCompanyCode = pop.CounterpartCompanyCode,
                                 CounterpartCompanyName = pop.CounterpartCompanyName,
                                 ReceivingCompanyId = pop.ReceivingCompanyId,
                                 ReceivingCompanyCode = pop.ReceivingCompanyCode,
                                 ReceivingCompanyName = pop.ReceivingCompanyName,
                                 ReceivingWarehouseId = pop.ReceivingWarehouseId.Value,
                                 ReceivingWarehouseCode = pop.ReceivingWarehouseCode,
                                 ReceivingWarehouseName = pop.ReceivingWarehouseName,
                                 SourceId = pop.SourceId,
                                 SourceCode = pop.SourceCode,
                                 OutboundType = pop.OutboundType,
                                 ShippingMethod = pop.ShippingMethod.Value,
                                 OriginalRequirementBillId = pop.OriginalRequirementBillId,
                                 OriginalRequirementBillType = pop.OriginalRequirementBillType,
                                 OriginalRequirementBillCode = pop.OriginalRequirementBillCode,
                                 OrderApproveComment = pop.OrderApproveComment,
                                 GPMSPurOrderCode = pop.GPMSPurOrderCode,
                                 IsPurDistribution = pop.IsPurDistribution,
                                 OtherPackingRequirements = pop.OtherPackingRequirements,
                                 IsSpecialInspection = pop.IsSpecialInspection,
                                 SAPPurchasePlanCode = pop.SAPPurchasePlanCode,
                                 ERPSourceOrderCode = pop.ERPSourceOrderCode,
                                 OrderTypeId = pop.OrderTypeId.Value,
                                 OrderTypeName = pop.OrderTypeName,
                                 ReceivingAddress = pop.ReceivingAddress,
                                 Remark = pop.Remark,
                                 CreatorName = pop.CreatorName,
                                 CreateTime = pop.CreateTime,
                                 ModifierName = pop.ModifierName,
                                 ModifyTime = pop.ModifyTime,
                                 Status = pop.Status,
                                 ContactPerson = pso.ContactPerson,
                                 ContactPhone = pso.ContactPhone,
                                 ApproverName = pso.ApproverName,
                                 IsTurn=pop.IsTurn
                             };
                if(operatorId.HasValue) {
                    result = result.Where(o => ObjectContext.WarehouseOperators.Any(r => r.OperatorId == operatorId.Value && r.WarehouseId == o.WarehouseId));
                }
                return result.OrderBy(r => r.Id).ForEach(o => o.TaskStatus = "装箱完成").ToList();
            } else if(company.Type == (int)DcsCompanyType.代理库 || company.Type == (int)DcsCompanyType.服务站兼代理库) {
                var result = from pop in ObjectContext.PartsOutboundPlans
                             join butd in this.ObjectContext.PickingTaskDetails on pop.Id equals butd.PartsOutboundPlanId
                             join bt in this.ObjectContext.PickingTasks on butd.PickingTaskId equals bt.Id
                             join d in ObjectContext.PartsSalesOrders on pop.OriginalRequirementBillId equals d.Id into tempTable
                             from pso in tempTable.DefaultIfEmpty()
                             where (bt.IsExistShippingOrder.HasValue ? bt.IsExistShippingOrder.Value : false) == false & bt.Status == (int)DcsPickingTaskStatus.拣货完成
                             && (createTimeForm.HasValue ? bt.PickingFinishTime >= createTimeForm.Value : true) && (createTimeTo.HasValue ? bt.PickingFinishTime <= createTimeTo.Value : true)
                             select new TaskListPartsOutboundPlan {
                                 TaskId = bt.Id,
                                 TaskCode = bt.Code,
                                 //TaskStatus = "拣货完成",
                                 Id = pop.Id,
                                 Code = pop.Code,
                                 WarehouseId = pop.WarehouseId,
                                 WarehouseCode = pop.WarehouseCode,
                                 WarehouseName = pop.WarehouseName,
                                 StorageCompanyId = pop.StorageCompanyId,
                                 StorageCompanyCode = pop.StorageCompanyCode,
                                 StorageCompanyName = pop.StorageCompanyName,
                                 StorageCompanyType = pop.StorageCompanyType,
                                 CompanyAddressId = pop.CompanyAddressId.Value,
                                 BranchId = pop.BranchId,
                                 BranchCode = pop.BranchCode,
                                 BranchName = pop.BranchName,
                                 PartsSalesCategoryId = pop.PartsSalesCategoryId,
                                 PartsSalesOrderTypeId = pop.PartsSalesOrderTypeId.Value,
                                 PartsSalesOrderTypeName = pop.PartsSalesOrderTypeName,
                                 CounterpartCompanyId = pop.CounterpartCompanyId,
                                 CounterpartCompanyCode = pop.CounterpartCompanyCode,
                                 CounterpartCompanyName = pop.CounterpartCompanyName,
                                 ReceivingCompanyId = pop.ReceivingCompanyId,
                                 ReceivingCompanyCode = pop.ReceivingCompanyCode,
                                 ReceivingCompanyName = pop.ReceivingCompanyName,
                                 ReceivingWarehouseId = pop.ReceivingWarehouseId.Value,
                                 ReceivingWarehouseCode = pop.ReceivingWarehouseCode,
                                 ReceivingWarehouseName = pop.ReceivingWarehouseName,
                                 SourceId = pop.SourceId,
                                 SourceCode = pop.SourceCode,
                                 OutboundType = pop.OutboundType,
                                 ShippingMethod = pop.ShippingMethod.Value,
                                 OriginalRequirementBillId = pop.OriginalRequirementBillId,
                                 OriginalRequirementBillType = pop.OriginalRequirementBillType,
                                 OriginalRequirementBillCode = pop.OriginalRequirementBillCode,
                                 OrderApproveComment = pop.OrderApproveComment,
                                 GPMSPurOrderCode = pop.GPMSPurOrderCode,
                                 IsPurDistribution = pop.IsPurDistribution,
                                 OtherPackingRequirements = pop.OtherPackingRequirements,
                                 IsSpecialInspection = pop.IsSpecialInspection,
                                 SAPPurchasePlanCode = pop.SAPPurchasePlanCode,
                                 ERPSourceOrderCode = pop.ERPSourceOrderCode,
                                 OrderTypeId = pop.OrderTypeId.Value,
                                 OrderTypeName = pop.OrderTypeName,
                                 ReceivingAddress = pop.ReceivingAddress,
                                 Remark = pop.Remark,
                                 CreatorName = pop.CreatorName,
                                 CreateTime = pop.CreateTime,
                                 ModifierName = pop.ModifierName,
                                 ModifyTime = pop.ModifyTime,
                                 Status = pop.Status,
                                 ContactPerson = pso.ContactPerson,
                                 ContactPhone = pso.ContactPhone,
                                 ApproverName = pso.ApproverName,
                                 IsTurn=pop.IsTurn
                             };

                if(operatorId.HasValue) {
                    result = result.Where(r => ObjectContext.WarehouseOperators.Any(v => v.OperatorId == operatorId.Value && r.WarehouseId == v.WarehouseId));
                }
                return result.OrderBy(r => r.Id).ForEach(o => o.TaskStatus = "拣货完成").ToList();
            }
            return null;
        }
        public IQueryable<PartsOutboundPlan> 仓库人员查询配件出库计划() {
            var userinfo = Utils.GetCurrentUserInfo();
            var result = ObjectContext.PartsOutboundPlans.Where(r => ObjectContext.WarehouseOperators.Any(v => v.WarehouseId == r.WarehouseId && v.OperatorId == userinfo.Id)).Include("PartsOutboundPlanDetails");
            return result.OrderBy(r => r.Id);
        }

        public IQueryable<PartsOutboundPlan> GetPartsOutboundById(int id) {
            return ObjectContext.PartsOutboundPlans.Where(r => r.Id == id);
        }

        public IEnumerable<PartsOutboundPlanDetail> GetPartsOutboundPlanDetailsByWarehouseAreaManager(int orderId, int userId) {
            var partsStocks = from a in ObjectContext.PartsOutboundPlans.Where(r => r.Id == orderId)
                              join b in ObjectContext.PartsStocks on a.WarehouseId equals b.WarehouseId
                              join c in ObjectContext.WarehouseAreaCategories.Where(r => r.Category == (int)DcsAreaType.保管区) on b.WarehouseAreaCategoryId equals c.Id
                              join d in ObjectContext.WarehouseAreas on new {
                                  WarehouseAreaId = b.WarehouseAreaId,
                                  WarehouseId = b.WarehouseId
                              } equals new {
                                  WarehouseAreaId = d.Id,
                                  WarehouseId = d.WarehouseId
                              }
                              join e in ObjectContext.WarehouseAreaManagers.Where(r => r.ManagerId == userId) on d.TopLevelWarehouseAreaId equals e.WarehouseAreaId
                              select b;
            var detailIds = ObjectContext.PartsOutboundPlanDetails.Where(r => r.PartsOutboundPlanId == orderId && (!r.OutboundFulfillment.HasValue || r.PlannedAmount != r.OutboundFulfillment.Value) && partsStocks.Any(v => v.PartId == r.SparePartId)).Select(r => r.Id).ToArray();
            return 校验出库计划清单(detailIds, userId);
        }

        public IEnumerable<PartsOutboundPlanDetail> 校验出库计划清单(int[] partsOutboundPlanDetailIds, int personId) {
            var result = from a in this.ObjectContext.PartsOutboundPlanDetails.Where(r => partsOutboundPlanDetailIds.Contains(r.Id) && r.PlannedAmount > (r.OutboundFulfillment ?? 0))
                         join b in this.ObjectContext.PartsOutboundPlans on a.PartsOutboundPlanId equals b.Id
                         join c in ObjectContext.PartsStocks on new {
                             PartId = a.SparePartId,
                             b.WarehouseId
                         } equals new {
                             c.PartId,
                             c.WarehouseId
                         }
                         join d in ObjectContext.WarehouseAreas.Where(r => this.ObjectContext.WarehouseAreaCategories.Any(d => d.Id == r.AreaCategoryId && d.Category == (int)DcsAreaType.保管区)) on c.WarehouseAreaId equals d.Id
                         join f in ObjectContext.WarehouseAreaManagers.Where(r => r.ManagerId == personId) on d.TopLevelWarehouseAreaId equals f.WarehouseAreaId
                         select a;
            return result.ToArray().Distinct();
        }

        public IQueryable<BoxUpTaskOrPicking> 查询待运配件任务单(int[] taskIds) {
            var userInfo = Utils.GetCurrentUserInfo();
            var company = ObjectContext.Companies.FirstOrDefault(r => r.Id == userInfo.EnterpriseId);
            if(company.Type == (int)DcsCompanyType.分公司) {
                var result = from but in ObjectContext.BoxUpTasks.Where(k => taskIds.Contains(k.Id))
                             join butd in ObjectContext.BoxUpTaskDetails on but.Id equals butd.BoxUpTaskId
                             join pt in ObjectContext.PickingTasks on butd.PickingTaskId equals pt.Id
                             join pop in ObjectContext.PartsOutboundPlans on butd.PartsOutboundPlanId equals pop.Id
                             join popd in ObjectContext.PartsOutboundPlanDetails on new {
                                 pop.Id,
                                 butd.SparePartId
                             } equals new {
                                 Id = popd.PartsOutboundPlanId,
                                 popd.SparePartId
                             }
                             join  sp in ObjectContext.SpareParts on butd.SparePartId equals sp.Id
                             select new BoxUpTaskOrPicking {
                                 Id = popd.Id,
                                 TaskId = but.Id,
                                 TaskCode = but.Code,
                                 WarehouseCode = but.WarehouseCode,
                                 WarehouseName = but.WarehouseName,
                                 OutboundType = pop.OutboundType,
                                 OrderTypeName = pt.OrderTypeName,
                                 OutboundTime = but.BoxUpFinishTime,
                                 ShippingMethod = pt.ShippingMethod,
                                 Remark = but.Remark,
                                 SparePartId = butd.SparePartId,
                                 SparePartCode = butd.SparePartCode,
                                 SparePartName = butd.SparePartName,
                                 SettlementPrice = popd.Price,
                                 ShippingAmount = (butd.PlanQty ?? 0),
                                 Type = 2,
                                 PartsOutboundPlanId = (butd.PartsOutboundPlanId ?? 0),
                                 PartsOutboundPlanCode = butd.PartsOutboundPlanCode,
                                 ContainerNumber = butd.ContainerNumber,
                                 TraceProperty=sp.TraceProperty
                             };
				var tResult= from a in result
				             group a by new { a.Id ,
                                 a.TaskId ,
                                 a.TaskCode  ,
                                 a.WarehouseCode,
                                 a.WarehouseName ,
                                 a.OutboundType ,
                                 a.OrderTypeName ,
                                 a.OutboundTime,
                                 a.ShippingMethod,
                                 a.Remark ,
                                 a.SparePartId ,
                                 a.SparePartCode,
                                 a.SparePartName ,
                                 a.SettlementPrice,
                                 a.Type ,
                                 a.PartsOutboundPlanId,
                                 a.PartsOutboundPlanCode,
                                 a.ContainerNumber ,
                                 a.TraceProperty
							 } into bb
							 select new BoxUpTaskOrPicking{
								 Id = bb.Key.Id,
                                 TaskId =bb.Key.TaskId,
                                 TaskCode = bb.Key.TaskCode,
                                 WarehouseCode = bb.Key.WarehouseCode,
                                 WarehouseName = bb.Key.WarehouseName,
                                 OutboundType = bb.Key.OutboundType,
                                 OrderTypeName = bb.Key.OrderTypeName,
                                 OutboundTime = bb.Key.OutboundTime,
                                 ShippingMethod =bb.Key.ShippingMethod,
                                 Remark = bb.Key.Remark,
                                 SparePartId = bb.Key.SparePartId,
                                 SparePartCode = bb.Key.SparePartCode,
                                 SparePartName = bb.Key.SparePartName,
                                 SettlementPrice =bb.Key.SettlementPrice,
                                 ShippingAmount = bb.Sum(t=>t.ShippingAmount),
                                 Type = bb.Key.Type,
                                 PartsOutboundPlanId = bb.Key.PartsOutboundPlanId ,
                                 PartsOutboundPlanCode = bb.Key.PartsOutboundPlanCode,
                                 ContainerNumber = bb.Key.ContainerNumber,
                                 TraceProperty=bb.Key.TraceProperty
							 };
                return tResult.OrderBy(o => o.SparePartId).Distinct();
            } else {
                var result = from pt in ObjectContext.PickingTasks.Where(k => taskIds.Contains(k.Id))
                             join ptd in ObjectContext.PickingTaskDetails on pt.Id equals ptd.PickingTaskId
                             join pop in ObjectContext.PartsOutboundPlans on ptd.PartsOutboundPlanId equals pop.Id
                             join popd in ObjectContext.PartsOutboundPlanDetails on new {
                                 pop.Id,
                                 ptd.SparePartId,
                                 Price = (ptd.Price ?? 0)
                             } equals new {
                                 Id = popd.PartsOutboundPlanId,
                                 popd.SparePartId,
                                 Price = popd.Price
                             }
                             join sp in ObjectContext.SpareParts on ptd.SparePartId equals sp.Id
                             where (ptd.PickingQty ?? 0) > 0
                             select new BoxUpTaskOrPicking {
                                 Id = popd.Id,
                                 TaskId = pt.Id,
                                 TaskCode = pt.Code,
                                 WarehouseCode = pt.WarehouseCode,
                                 WarehouseName = pt.WarehouseName,
                                 OutboundType = pop.OutboundType,
                                 OrderTypeName = pt.OrderTypeName,
                                 OutboundTime = pt.PickingFinishTime,
                                 ShippingMethod = pt.ShippingMethod,
                                 Remark = pt.Remark,
                                 SparePartId = ptd.SparePartId,
                                 SparePartCode = ptd.SparePartCode,
                                 SparePartName = ptd.SparePartName,
                                 SettlementPrice = popd.Price,
                                 ShippingAmount = (ptd.PickingQty ?? 0),
                                 Type = 1,
                                 PartsOutboundPlanId = (ptd.PartsOutboundPlanId ?? 0),
                                 PartsOutboundPlanCode = ptd.PartsOutboundPlanCode,
                                 TraceProperty=sp.TraceProperty
                             };
							 var tResult= from a in result
				             group a by new { a.Id ,
                                 a.TaskId ,
                                 a.TaskCode  ,
                                 a.WarehouseCode,
                                 a.WarehouseName ,
                                 a.OutboundType ,
                                 a.OrderTypeName ,
                                 a.OutboundTime,
                                 a.ShippingMethod,
                                 a.Remark ,
                                 a.SparePartId ,
                                 a.SparePartCode,
                                 a.SparePartName ,
                                 a.SettlementPrice,
                                 a.Type ,
                                 a.PartsOutboundPlanId,
                                 a.PartsOutboundPlanCode,
                                 a.TraceProperty
							 } into bb
							 select new BoxUpTaskOrPicking{
								 Id = bb.Key.Id,
                                 TaskId =bb.Key.TaskId,
                                 TaskCode = bb.Key.TaskCode,
                                 WarehouseCode = bb.Key.WarehouseCode,
                                 WarehouseName = bb.Key.WarehouseName,
                                 OutboundType = bb.Key.OutboundType,
                                 OrderTypeName = bb.Key.OrderTypeName,
                                 OutboundTime = bb.Key.OutboundTime,
                                 ShippingMethod =bb.Key.ShippingMethod,
                                 Remark = bb.Key.Remark,
                                 SparePartId = bb.Key.SparePartId,
                                 SparePartCode = bb.Key.SparePartCode,
                                 SparePartName = bb.Key.SparePartName,
                                 SettlementPrice =bb.Key.SettlementPrice,
                                 ShippingAmount = bb.Sum(t=>t.ShippingAmount),
                                 Type = bb.Key.Type,
                                 PartsOutboundPlanId = bb.Key.PartsOutboundPlanId ,
                                 PartsOutboundPlanCode = bb.Key.PartsOutboundPlanCode,
                                 TraceProperty=bb.Key.TraceProperty
							 };
                return tResult.OrderBy(o => o.SparePartId).Distinct();
            }
        }

        public IEnumerable<VirtualPartsOutboundPlan> GetPartsOutboundPlansWithPartsCategory2(string sparePartCode, string sparePartName, string status, int? warehouseId, int? outboundType, string counterpartCompanyName, string sourceCode, DateTime? bCreateTime, DateTime? eCreateTime, string partsSalesOrderTypeName, string code) {
            var userInfo = Utils.GetCurrentUserInfo();
            string SQL = @"select a.*,s.code as PartsShippingOrderCode,
       (select sum(s.plannedamount * nvl(d.retailguideprice,0))
         from partsoutboundplandetail s
                  left join PartsRetailGuidePrice d
                    on s.sparepartid = d.sparepartid and d.status=1
                    where s.partsoutboundplanid = a.id group by s.partsoutboundplanid) 
                    as TotalAmountForWarehouse,
(select count(1)
         from partsoutboundplandetail s where s.partsoutboundplanid =a.id) as TotalCount,
  b.name as PartsSalesCategoryName, (case when (select count(1) from InternalAllocationBill ia where ia.sourcecode=a.originalrequirementbillcode)>0 then 1 else 0 end ) as IsInternalAllocationBill
      from partsoutboundplan a left join PartsSalesCategory b on a.PartsSalesCategoryId=b.id 
 left join (select distinct m.code, n.partsoutboundplanid
               from partsshippingorder m
              inner join partsshippingorderdetail n
                 on n.partsshippingorderid = m.id
              where m.status <> 99) s
    on a.id = s.partsoutboundplanid
where a.StorageCompanyId=" + userInfo.EnterpriseId + " and exists (select 1 from warehouseoperator where OperatorId = " + userInfo.Id + " and WarehouseId = a.warehouseid )";
            if(!string.IsNullOrEmpty(sparePartCode)) {
                SQL = SQL + " and  (select count(1) from PartsOutboundPlanDetail s where s.partsoutboundplanid=a.id and LOWER(s.SparePartCode) like '%" + sparePartCode.ToLower() + "%')>0";
            }
            if(!string.IsNullOrEmpty(sparePartName)) {
                SQL = SQL + " and  (select count(1) from PartsOutboundPlanDetail s where s.partsoutboundplanid=a.id and  LOWER(s.SparePartName) like '%" + sparePartName.ToLower() + "%')>0";
            }
            if(!string.IsNullOrEmpty(status)) {
                SQL = SQL + " and  a.status in (" + status + ")";
            }
            if(warehouseId.HasValue) {
                SQL = SQL + " and  a.warehouseId=" + warehouseId;
            }
            if(outboundType.HasValue) {
                SQL = SQL + " and  a.outboundType=" + outboundType;
            }
            if(!string.IsNullOrEmpty(counterpartCompanyName)) {
                SQL = SQL + " and  a.counterpartCompanyName like '%" + counterpartCompanyName + "%'";
            }
            if(!string.IsNullOrEmpty(partsSalesOrderTypeName)) {
                SQL = SQL + " and  a.partsSalesOrderTypeName like '%" + partsSalesOrderTypeName + "%'";
            }
            if(!string.IsNullOrEmpty(sourceCode)) {
                SQL = SQL + " and  LOWER(a.sourceCode) like '%" + sourceCode.ToLower() + "%'";
            }
            if(bCreateTime.HasValue) {
                SQL = SQL + " and a.CreateTime>= to_date('" + bCreateTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if(eCreateTime.HasValue) {
                SQL = SQL + " and a.CreateTime<= to_date('" + eCreateTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if(!string.IsNullOrEmpty(code)) {
                SQL = SQL + " and  LOWER(a.code) like '%" + code.ToLower() + "%'";
            }
            var search = ObjectContext.ExecuteStoreQuery<VirtualPartsOutboundPlan>(SQL).ToList();
            return search;
        }

    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:32
        //原分布类的函数全部转移到Ach                                                               

        public void InsertPartsOutboundPlan(PartsOutboundPlan partsOutboundPlan) {
            new PartsOutboundPlanAch(this).InsertPartsOutboundPlan(partsOutboundPlan);
        }

        public void UpdatePartsOutboundPlan(PartsOutboundPlan partsOutboundPlan) {
            new PartsOutboundPlanAch(this).UpdatePartsOutboundPlan(partsOutboundPlan);
        }

        public PartsOutboundPlan GetPartsOutboundPlanWithDetails(int id) {
            return new PartsOutboundPlanAch(this).GetPartsOutboundPlanWithDetails(id);
        }

        public IQueryable<PartsOutboundPlan> GetPartsOutboundPlansWithPartsCategory(string sparePartCode, string sparePartName) {
            return new PartsOutboundPlanAch(this).GetPartsOutboundPlansWithPartsCategory(sparePartCode, sparePartName);
        }

        public IQueryable<PartsOutboundPlan> 保管区负责人查询配件出库计划(int? userinfoId) {
            return new PartsOutboundPlanAch(this).保管区负责人查询配件出库计划(userinfoId);
        }

        public IEnumerable<TaskListPartsOutboundPlan> 仓库人员查询待发运出库计划(int? operatorId, DateTime? createTimeForm, DateTime? createTimeTo) {
            return new PartsOutboundPlanAch(this).仓库人员查询待发运出库计划(operatorId, createTimeForm, createTimeTo);
        }

        public IQueryable<PartsOutboundPlan> 仓库人员查询配件出库计划() {
            return new PartsOutboundPlanAch(this).仓库人员查询配件出库计划();
        }

        public IQueryable<PartsOutboundPlan> GetPartsOutboundById(int id) {
            return new PartsOutboundPlanAch(this).GetPartsOutboundById(id);
        }

        public IEnumerable<PartsOutboundPlanDetail> GetPartsOutboundPlanDetailsByWarehouseAreaManager(int orderId, int userId) {
            return new PartsOutboundPlanAch(this).GetPartsOutboundPlanDetailsByWarehouseAreaManager(orderId, userId);
        }

        [Query(HasSideEffects = true)]

        public IEnumerable<PartsOutboundPlanDetail> 校验出库计划清单(int[] partsOutboundPlanDetailIds, int personId) {
            return new PartsOutboundPlanAch(this).校验出库计划清单(partsOutboundPlanDetailIds, personId);
        }

        [Query(HasSideEffects = true)]

        public IQueryable<BoxUpTaskOrPicking> 查询待运配件任务单(int[] ids) {
            return new PartsOutboundPlanAch(this).查询待运配件任务单(ids);
        }
        public IEnumerable<VirtualPartsOutboundPlan> GetPartsOutboundPlansWithPartsCategory2(string sparePartCode, string sparePartName, string status, int? warehouseId, int? outboundType, string counterpartCompanyName, string sourceCode, DateTime? bCreateTime, DateTime? eCreateTime, string partsSalesOrderTypeName, string code) {
            return new PartsOutboundPlanAch(this).GetPartsOutboundPlansWithPartsCategory2(sparePartCode, sparePartName, status, warehouseId, outboundType, counterpartCompanyName, sourceCode, bCreateTime, eCreateTime, partsSalesOrderTypeName, code);
        }
    }
}
