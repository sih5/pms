﻿using System;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsPackingAch : DcsSerivceAchieveBase {
        internal void InsertPartsPackingValidate(PartsPacking partsPacking) {
            var userInfo = Utils.GetCurrentUserInfo();
            partsPacking.CreatorId = userInfo.Id;
            partsPacking.CreatorName = userInfo.Name;
            partsPacking.CreateTime = DateTime.Now;
        }

        public void InsertPartsPacking(PartsPacking partsPacking) {
            InsertToDatabase(partsPacking);
            this.InsertPartsPackingValidate(partsPacking);
        }

        internal void UpdatePartsPackingValidate(PartsPacking partsPacking) {

            var userInfo = Utils.GetCurrentUserInfo();
            partsPacking.ModifierId = userInfo.Id;
            partsPacking.ModifierName = userInfo.Name;
            partsPacking.ModifyTime = DateTime.Now;
        }

        public void UpdatePartsPacking(PartsPacking partsPacking) {
            UpdateToDatabase(partsPacking);
            this.UpdatePartsPackingValidate(partsPacking);
        }
    
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:32
        //原分布类的函数全部转移到Ach                                                               

        public void InsertPartsPacking(PartsPacking partsPacking) {
            new PartsPackingAch(this).InsertPartsPacking(partsPacking);
        }

        public void UpdatePartsPacking(PartsPacking partsPacking) {
            new PartsPackingAch(this).UpdatePartsPacking(partsPacking);
        }
    }
}
