﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web
{
    partial class PartsPackingFinishAch : DcsSerivceAchieveBase
    {
        public PartsPackingFinishAch(DcsDomainService domainService)
            : base(domainService)
        {
        }
        public IEnumerable<PartsPackingFinish> PartsPackingFinishs(DateTime? bCheckBillCreateTime, DateTime? eCheckBillCreateTime, string partsInboundCheckBillCode, int? warehouseId, string sparePartCode, string referenceCode, int? partABC, string partsPurchaseOrderCode, DateTime? bModifyTime, DateTime? eModifyTime)
        {
            string SQL = @"select a.id,
                          a.warehouseid,
                          a.warehousename,
                          a.sparepartcode,
                          a.sparepartname,
                          b.PartABC,
                          c.SupplierPartCode,
                          d.code             as PartsInboundCheckBillCode,
                          f.code             as PartsPurchaseOrderCode,
                          (select sum(ck.inspectedquantity) from partsinboundcheckbilldetail ck where ck.partsinboundcheckbillid=d.id and ck.sparepartid=a.sparepartid) as inspectedquantity,
                          a.PackingQty,
                          d.createtime       as CheckBillCreateTime,
                          a.createtime,
                          h.retailguideprice,
                          p.referencecode,
                          a.ModifyTime
                        from PackingTask a
                        left join partsbranch b
                        on a.sparepartid = b.partid
                        and b.status = 1
                        left join PartsSupplierRelation c
                         on a.sparepartid = c.partid
                         and c.status = 1
                       and c.isprimary = 1
                       left join PartsInboundCheckBill d
                       on a.partsinboundcheckbillid = d.id
                       left join PartsPurchaseOrder f
                       on d.originalrequirementbillid = f.id
                       and d.originalrequirementbillcode = f.code
                      left join PartsRetailGuidePrice h
                       on d.partssalescategoryid = h.partssalescategoryid
                       and h.branchid = d.branchid
                       and h.sparepartid = a.sparepartid
                       and h.status = 1
                      join sparepart p on a.sparepartid=p.id where 1=1";

            if (bCheckBillCreateTime.HasValue && bCheckBillCreateTime.Value == Convert.ToDateTime("9999/12/31 23:59:59"))
            {
                bCheckBillCreateTime = null;
            }
            if (eCheckBillCreateTime.HasValue && eCheckBillCreateTime.Value == Convert.ToDateTime("9999/12/31 23:59:59"))
            {
                eCheckBillCreateTime = null;
            }
            if (bModifyTime.HasValue && bModifyTime.Value == Convert.ToDateTime("9999/12/31 23:59:59"))
            {
                bModifyTime = null;
            }
            if (bCheckBillCreateTime.HasValue)
            {
                SQL = SQL + " and d.CreateTime>= to_date('" + bCheckBillCreateTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if (eCheckBillCreateTime.HasValue)
            {
                SQL = SQL + " and d.CreateTime<= to_date('" + eCheckBillCreateTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if (bModifyTime.HasValue)
            {
                SQL = SQL + " and a.ModifyTime>= to_date('" + bModifyTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if (eModifyTime.HasValue)
            {
                SQL = SQL + " and a.ModifyTime<= to_date('" + eModifyTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if (!string.IsNullOrEmpty(partsInboundCheckBillCode))
            {
                SQL = SQL + " and LOWER(d.code) like '%" + partsInboundCheckBillCode.ToLower() + "%'";
            }
            if (!string.IsNullOrEmpty(sparePartCode))
            {
                
                SQL = SQL + " and LOWER(a.sparePartCode) like '%" + sparePartCode.ToLower() + "%'";
            }
            if (warehouseId.HasValue)
            {
                SQL = SQL + " and a.warehouseId=" + warehouseId.Value;
            }
            if (!string.IsNullOrEmpty(referenceCode))
            {
                SQL = SQL + " and LOWER(p.referenceCode) like '%" + referenceCode.ToLower() + "%'";
            }
            if (!string.IsNullOrEmpty(partsPurchaseOrderCode))
            {
                SQL = SQL + " and LOWER(f.code) like '%" + partsPurchaseOrderCode.ToLower() + "%'";
            }
            if (partABC.HasValue)
            {
                SQL = SQL + " and b.partABC=" + partABC.Value;
            }
            var detail = ObjectContext.ExecuteStoreQuery<PartsPackingFinish>(SQL).ToList();
            return detail;
        }
    }
    partial class DcsDomainService
    {
        public IEnumerable<PartsPackingFinish> PartsPackingFinishs(DateTime? bCheckBillCreateTime, DateTime? eCheckBillCreateTime, string partsInboundCheckBillCode, int? warehouseId, string sparePartCode, string referenceCode, int? partABC, string partsPurchaseOrderCode, DateTime? bModifyTime, DateTime? eModifyTime)
        {
            return new PartsPackingFinishAch(this).PartsPackingFinishs(bCheckBillCreateTime, eCheckBillCreateTime, partsInboundCheckBillCode, warehouseId, sparePartCode, referenceCode, partABC, partsPurchaseOrderCode, bModifyTime, eModifyTime);
        }

    }
}
