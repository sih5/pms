﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsPurchaseOrderFinishReportAch : DcsSerivceAchieveBase {
        public PartsPurchaseOrderFinishReportAch(DcsDomainService domainService)
            : base(domainService) {
        }
        public IEnumerable<PartsPurchaseOrderFinishReport> 获取配件优先级分析信息(DateTime? createTime, string partCode, int? partabc, string supplierName, int? priority, int? oweOrderNum) {

            string SQL = @"select rownum,
                           ps.partid,
                           ps.partcode,
                           ps.partname,
                           ps.supplyonlineqty,
                           ps.overdueonlineqty,
                           ps.waitinqty,
                           ps.waitpackqty,
                           ps.waitshelvesqty,
                           ps.oweordernum,
                           ps.owepartnum, 
                           ps.CreateTime,
                           pb.priority,
                           pb.prioritycreatetime,
                           pb.actualuseableqty,
                           pb.stockqty,
                           pb.totaldurativedaynum,
                           pt.standardqty,
                           pt.daily as DailySaleNum,
                           pt.qtylowerlimit,
                           sp.referencecode,
                           ph.partabc,
                           pr.SupplierPartCode,
                           ps.PartsSupplierName as SupplierName,
                           (case when pb.priority=3 then 0 else trunc( sysdate)- trunc(pb.createtime) end)DurationDay,
                         ps.IsOrderable ,ps.IsSalable 
                      from PartsPurchaseInfoSummary ps
                      join PartsPriorityBill pb
                        on ps.partid = pb.partid
                      left join PartsStandardStockBill pt
                        on ps.partid = pt.partid
                      left join sparepart sp
                        on ps.partid = sp.id
                      left join partsbranch ph
                        on sp.id = ph.partid
                       and ph.status = 1
                      left join PartsSupplierRelation pr
                        on ps.partid = pr.partid
                       and pr.isprimary = 1
                       and pr.status = 1                     
                     where pb.priority is not null {0}
                     order by pb.priority    asc,
                              nvl(ps.OweOrderNum,0) desc,
                              nvl(ps.OwePartNum,0)  desc,
                              nvl(ph.partabc,0)";
            var serachSQL = new StringBuilder();
            serachSQL.Append(" and trunc(ps.CreateTime)=trunc(to_date('" + createTime + "','yyyy-MM-dd HH24:mi:ss'))");
            if(!string.IsNullOrEmpty(partCode)) {
                serachSQL.Append(" and ps.partCode like '%" + partCode + "%'");
            }
            if(partabc.HasValue) {
                serachSQL.Append(" and ph.partabc=" + partabc);
            }
            if(!string.IsNullOrEmpty(supplierName)) {
                serachSQL.Append(" and ps.PartsSupplierName like '%" + supplierName + "%'");
            }
            if(priority.HasValue) {
                serachSQL.Append(" and pb.priority=" + priority);
            }
            if(oweOrderNum.HasValue) {
                serachSQL.Append(" and ps.owepartnum=" + oweOrderNum);
            }
            SQL = string.Format(SQL, serachSQL);
            var search = ObjectContext.ExecuteStoreQuery<PartsPurchaseOrderFinishReport>(SQL).ToList();
            return search;
        }
        public IEnumerable<PartsPurchaseOrderDetailReport> 优先入库明细表(DateTime? createTime, string partCode, string supplierName, int? inboundFinishStatus, DateTime? bPriorityCreateTime, DateTime? ePriorityCreateTime, DateTime? bPlanDeliveryTime, DateTime? ePlanDeliveryTime) {

            if(bPriorityCreateTime.HasValue && bPriorityCreateTime.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
                bPriorityCreateTime = null;
            }
            if(bPlanDeliveryTime.HasValue && bPlanDeliveryTime.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
                bPlanDeliveryTime = null;
            }
            string SQL = "";
            var serachSQL = new StringBuilder();
            if(createTime.Value.Date == DateTime.Now.Date) {
                SQL = @"select PriorityCreateTime,
                           InBoundPlanCode,
                           Priority,
                           code                as partspurchaseordercode,
                           sparepartCode,
                           ReferenceCode,
                           sparepartName,
                           shippingamount,
                           inspectedquantity,
                           ShallInBoundQty,
                           ShallInBoundEntry,
                           ShallInBoundEntries,
                           shippingdate,
                           PlanDeliveryTime,
                           createtime          as inboundfinishtime,
                           InBoundFinishStatus,
                           SupplierName,
                           Rownum
                      from (select ppb.PriorityCreateTime,
                               pip.code as InBoundPlanCode,
                               ppb.Priority,
                               ppo.code,
                               pipd.sparepartCode,
                               s.ReferenceCode,
                               pipd.sparepartName,
                               pipd.PlannedAmount as shippingamount,
                               nvl(ff.InspectedQuantity, 0) as inspectedquantity,
                               nvl(pipd.PlannedAmount, 0),
                               (nvl(pipd.PlannedAmount, 0) - nvl(ff.InspectedQuantity, 0)) as ShallInBoundQty,
                               (nvl(pipd.PlannedAmount, 0) - nvl(ff.InspectedQuantity, 0)) /
                               decode(s.MInPackingAmount, 0, 1, null, 1, s.MInPackingAmount) as ShallInBoundEntry,
                               nvl(pipd.PlannedAmount, 0) /
                               decode(s.MInPackingAmount, 0, 1, null, 1, s.MInPackingAmount) as ShallInBoundEntries,
                               pip.createtime as shippingdate,
                               pip.createtime + bsr.ArrivalCycle as PlanDeliveryTime,
                               (select max(picb.createtime)
                                  from partsinboundcheckbill picb
                                 where picb.partsinboundplanid = pip.id) as createtime,
                               (case
                                 when nvl(pipd.PlannedAmount, 0) = 0 then
                                  3
                                 when nvl(pipd.PlannedAmount, 0) > 0 and
                                      (nvl(pipd.PlannedAmount, 0) -
                                       nvl(ff.InspectedQuantity, 0) -
                                       (case
                                          when pip.status = 3 then
                                           pipd.PlannedAmount - nvl(ff.InspectedQuantity, 0)
                                          else
                                           0
                                        end)) = 0 then
                                  1
                                 else
                                  2
                               end) as InBoundFinishStatus,
                               ppo.partssuppliername as SupplierName
        
                          from partsinboundplan pip
                         inner join partsinboundplandetail pipd
                            on pip.id = pipd.partsinboundplanid
                         inner join sparepart s
                            on s.id = pipd.sparepartid
                           and s.Status = 1
                         inner join PartsPriorityBill ppb
                            on ppb.partid = pipd.sparepartid
                           and ppb.priority is not null
                         inner join PartsPurchaseOrder ppo
                            on pip.originalrequirementbillcode = ppo.code
                         inner join PartsPurchaseOrderdetail ppod
                            on ppo.id = ppod.Partspurchaseorderid
                           and ppod.sparepartid = pipd.sparepartid
                         inner join BranchSupplierRelation bsr
                            on ppo.partssupplierid = bsr.SupplierId
                           and bsr.status = 1
                          left join (select sum(ckd.inspectedquantity) as inspectedquantity,
                                           cd.originalrequirementbillid,
                                           ckd.sparepartid
                                      from partsinboundcheckbill cd
                                      join partsinboundcheckbilldetail ckd
                                        on cd.id = ckd.partsinboundcheckbillid
                                     where InboundType = 1
                                     group by cd.originalrequirementbillid, ckd.sparepartid) ff
                            on ppo.id = ff.originalrequirementbillid
                           and pipd.sparepartid = ff.sparepartid
                                 where pip.warehousename = '双桥总库'
                                   and pip.PlanDeliveryTime <
                                       to_date(to_char(SYSDATE, 'yyyy-mm-dd'), 'yyyy-mm-dd')
                                  and (nvl(pipd.PlannedAmount, 0) - nvl(ff.InspectedQuantity, 0) -
                                   (case
                                     when pip.status = 3 then
                                      pipd.PlannedAmount - nvl(ff.InspectedQuantity, 0)
                                     else
                                      0
                                   end)) > 0
                                   and pip.status != 8) s where 1=1 {0}";
            } else {
                SQL = @"select Rownum ,
                           s.prioritycreatetime,
                           s.inboundplancode,
                           s.inboundpriority as Priority,
                           s.partspurchaseordercode,
                           s.sparepartcode,
                           s.supplierpartcode,
                           s.sparepartname,
                           s.shippingamount,
                           s.inspectedquantity,
                           s.shallinboundqty,
                           s.shallinboundentries,
                           s.actualinboundentries,
                           s.shippingdate,
                           s.plandeliverytime,
                           s.inboundfinishtime,
                           s.inboundfinishstatus,
                           s.suppliername,
                           s.createtime
                      from AddInboundDetail s where {0} ";
                 serachSQL.Append("  trunc(s.CreateTime)=trunc(to_date('" + createTime + "','yyyy-MM-dd HH24:mi:ss'))");
            }
           
            if(!string.IsNullOrEmpty(partCode)) {
                serachSQL.Append(" and s.sparepartcode like '%" + partCode + "%'");
            }
            if(!string.IsNullOrEmpty(supplierName)) {
                serachSQL.Append(" and s.suppliername like '%" + supplierName + "%'");
            }
            if(bPriorityCreateTime.HasValue) {
                serachSQL.Append(" and s.PriorityCreateTime>= to_date('" + bPriorityCreateTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')");
            }
            if(ePriorityCreateTime.HasValue) {
                serachSQL.Append(" and s.PriorityCreateTime<= to_date('" + ePriorityCreateTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')");
            }
            if(bPlanDeliveryTime.HasValue) {
                serachSQL.Append(" and s.PlanDeliveryTime>= to_date('" + bPlanDeliveryTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')");
            }
            if(ePlanDeliveryTime.HasValue) {
                serachSQL.Append(" and s.PlanDeliveryTime<= to_date('" + ePlanDeliveryTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')");
            }
            if(inboundFinishStatus.HasValue){
                serachSQL.Append(" and s.inboundFinishStatus=" + inboundFinishStatus );
            }
            SQL = string.Format(SQL, serachSQL);
            var search = ObjectContext.ExecuteStoreQuery<PartsPurchaseOrderDetailReport>(SQL).ToList();
            return search;
        }
    }
    partial class DcsDomainService {
        public IEnumerable<PartsPurchaseOrderFinishReport> 获取配件优先级分析信息(DateTime? createTime, string partCode, int? partabc, string supplierName, int? priority, int? oweOrderNum) {
            return new PartsPurchaseOrderFinishReportAch(this).获取配件优先级分析信息(createTime, partCode, partabc, supplierName, priority, oweOrderNum);
        }
        public IEnumerable<PartsPurchaseOrderDetailReport> 优先入库明细表(DateTime? createTime, string partCode, string supplierName, int? inboundFinishStatus, DateTime? bPriorityCreateTime, DateTime? ePriorityCreateTime, DateTime? bPlanDeliveryTime, DateTime? ePlanDeliveryTime) {
            return new PartsPurchaseOrderFinishReportAch(this).优先入库明细表(createTime, partCode, supplierName, inboundFinishStatus, bPriorityCreateTime, ePriorityCreateTime, bPlanDeliveryTime, ePlanDeliveryTime);
        }
        
    }
}
