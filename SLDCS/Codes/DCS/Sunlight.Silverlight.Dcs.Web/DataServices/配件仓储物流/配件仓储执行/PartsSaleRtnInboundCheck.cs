﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsSaleRtnInboundCheckAch : DcsSerivceAchieveBase {
        public PartsSaleRtnInboundCheckAch(DcsDomainService domainService)
            : base(domainService) {
        }
        public IEnumerable<PartsSaleRtnInboundCheck> 销售退货入库单统计(DateTime? bCreateTime, DateTime? eCreateTime, string partsSalesReturnBillCode, string partsSalesOrderCode, string sparePartCode, string code, string settlementStatus, string settlementNo, string sAPSysInvoiceNumber, string counterpartCompanyName, string counterpartCompanyCode) {
            string SQL = @"select (case
                                    when cp.type = 3 then
                                      (select mt.Name from MarketingDepartment mt join Agency ag on mt.id = ag.marketingdepartmentid   where ag.id = cp.id)
                                   when cp.type = 2 or cp.type = 7 then (select mt.Name  from MarketingDepartment mt  join DealerServiceInfo ag  on mt.id = ag.marketingdepartmentid
                                     where ag.DealerId = cp.id)   else  null  end) as MarketingDepartmentName,
                                   cp.ProvinceName as province,
                                   ps.WarehouseName,
                                   ck.counterpartcompanyname,
                                   ck.CounterpartCompanyCode,
                                   ps.code as PartsSalesReturnBillCode,
                                   ps.returnwarehousecode,
                                  -- po.code as PartsSalesOrderCode,
                                  ck.code,
                                  cd.sparepartcode,
                                  cd.sparepartname,
                                  ph.partabc,
                                  nvl(pst.code, pss.code) as SettlementNo,
                                  nvl(pst.status, pss.status) as SettlementStatus,
                                  cd.inspectedquantity,
                                  cd.originalprice,
                                  cd.originalprice * cd.inspectedquantity as OriginalPriceAll,
                                   cd.settlementprice,
                                  cd.settlementprice * cd.inspectedquantity as SettlementPriceAll,                                 
                                  (case  when cd.settlementprice < nvl(cd.originalprice, 0) then 1 else  0  end) as IsDiscount,
                                  cd.costprice,
                                  cd.costprice * cd.inspectedquantity as CostPriceAll,
                                  ck.CreateTime,
                                  ck.creatorname,
                                  decode(ps.InvoiceRequirement, 3, 1, 0) as IsRed,
                                 ty.PriceTypeName as PriceType,
                                nvl(sfd.sapsysinvoicenumber,sf.sapsysinvoicenumber)as sapsysinvoicenumber,
                                (case when (pst.id is not null and pss.id is null) then  (select listagg(a.InvoiceNumber, ',') within group(order by a.sourcecode)
                                 from InvoiceInformation a
                                join SalesRtnSettleInvoiceRel sr on a.id=sr.invoiceid                 
                                  where sr.partssalesrtnsettlementid=pst.id
                               and a.sourcetype = 2     and a.status <>99               
                                      ) when (pst.id is  null and pss.id is not  null )
                                     then  (select listagg(a.InvoiceNumber, ',') within group(order by a.sourcecode)
                                  from InvoiceInformation a                            
                              where a.sourceid =pss.id 
                                 and a.SourceCode = pss.code  and a.status <>99    
                                  ) else '' end ) as InvoiceNumber, ps.ReturnReason,
                                    Rownum
                                  from partsinboundcheckbill ck
                                  join partsinboundcheckbilldetail cd
                                      on ck.id = cd.partsinboundcheckbillid
                                  join PartsSalesReturnBill ps
                                      on ck.originalrequirementbillid = ps.id
                                      and ck.originalrequirementbilltype = 2
                                left  join (select id, sparepartid, pricetypename
                                          from (select ck.id,
                                                       cd.sparepartid,
                                                       psd.pricetypename,
                                                       psd.orderedquantity,
                                                       row_number() over(partition by ck.id, cd.sparepartid order by psd.orderedquantity desc) as ydy
                                                  from partsinboundcheckbill ck
                                                  join partsinboundcheckbilldetail cd
                                                    on ck.id = cd.partsinboundcheckbillid
                                                  join PartsSalesReturnBill ps
                                                    on ck.originalrequirementbillid = ps.id
                                                   and ck.originalrequirementbilltype = 2
                                                  join partssalesreturnbilldetail pl
                                                    on ps.id = pl.partssalesreturnbillid
                                                   and pl.sparepartid = cd.sparepartid
                                                   and cd.SettlementPrice = pl.ReturnPrice
                                                  join partssalesorder po
                                                    on pl.partssalesorderid = po.id
                                                  join partssalesorderdetail psd
                                                    on po.id = psd.partssalesorderid
                                                   and psd.sparepartid = cd.sparepartid
                                                 where ck.InboundType = 2
                                                   and ck.StorageCompanyType = 1) pp
                                         where pp.ydy = 1) ty
                                    on ck.id = ty.id
                                   and cd.sparepartid = ty.sparepartid
                                  join company cp
                                       on ps.SubmitCompanyId = cp.id
                                  left join partsbranch ph
                                       on cd.sparepartid = ph.partid
                                       and ph.status = 1
                                       and ck.partssalescategoryid = ph.partssalescategoryid
                                  left join PartsSalesRtnSettlementRef psr
                                       on ck.id = psr.sourceid
                                      and ck.code = psr.sourcecode
                                      and exists (select 1  from PartsSalesRtnSettlement pst
                                               where psr.partssalesrtnsettlementid = pst.id and pst.status not in (4, 99))
                                   left join PartsSalesRtnSettlement pst
                                        on psr.partssalesrtnsettlementid = pst.id
                                        and pst.status not in (4, 99)
                                   left join salesbillinfo sfd
                                          on pst.code = sfd.code
                                   left join PartsSalesSettlementRef psf
                                          on ck.id = psf.sourceid
                                          and ck.code = psf.sourcecode
                                        and exists (select 1 from PartsSalesSettlement pss
                                            where psf.partssalessettlementid = pss.id and pss.status not in (4, 99))
                                   left join PartsSalesSettlement pss
                                           on psf.partssalessettlementid = pss.id
                                           and pss.status not in (4, 99)
                                     left join salesbillinfo sf
                                  on pss.code = sf.code
                                   where ck.InboundType = 2  and ck.StorageCompanyType=1 ";

            if(bCreateTime.HasValue && bCreateTime.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
                bCreateTime = null;
            }
            if(eCreateTime.HasValue && eCreateTime.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
                eCreateTime = null;
            }          
            if(bCreateTime.HasValue) {
                SQL = SQL + " and ck.CreateTime>= to_date('" + bCreateTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if(eCreateTime.HasValue) {
                SQL = SQL + " and ck.CreateTime<= to_date('" + eCreateTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }

            if(!string.IsNullOrEmpty(partsSalesReturnBillCode)) {
                SQL = SQL + " and LOWER(ps.code) like '%" + partsSalesReturnBillCode.ToLower() + "%'";
            }
            if(!string.IsNullOrEmpty(sparePartCode)) {

                SQL = SQL + " and LOWER(cd.sparePartCode) like '%" + sparePartCode.ToLower() + "%'";
            }
            if( !string.IsNullOrEmpty(settlementStatus)) {
                SQL = SQL + " and nvl(pst.status, pss.status) in(" + settlementStatus+")";
            }
            if(!string.IsNullOrEmpty(partsSalesOrderCode)) {
                SQL = SQL + " and LOWER(po.code) like '%" + partsSalesOrderCode.ToLower() + "%'";
            }
            if(!string.IsNullOrEmpty(code)) {
                SQL = SQL + " and LOWER(ck.code) like '%" + code.ToLower() + "%'";
            }
            if(!string.IsNullOrEmpty(settlementNo)) {
                SQL = SQL + " and LOWER(nvl(pst.code, pss.code)) like '%" + settlementNo.ToLower() + "%'";
            }
            if(!string.IsNullOrEmpty(sAPSysInvoiceNumber)) {
                SQL = SQL + " and LOWER(nvl(sfd.sapsysinvoicenumber,sf.sapsysinvoicenumber)) like '%" + sAPSysInvoiceNumber.ToLower() + "%'";
           }
            if(!string.IsNullOrEmpty(counterpartCompanyName)) {
                SQL = SQL + " and ck.counterpartCompanyName like '%" + counterpartCompanyName + "%'";
            }
            if(!string.IsNullOrEmpty(counterpartCompanyCode)) {
                SQL = SQL + " and LOWER(ck.counterpartCompanyCode) like '%" + counterpartCompanyCode.ToLower() + "%'";
            }
            var detail = ObjectContext.ExecuteStoreQuery<PartsSaleRtnInboundCheck>(SQL).ToList();
            return detail;
        }
    }
     partial class DcsDomainService {
         public IEnumerable<PartsSaleRtnInboundCheck> 销售退货入库单统计(DateTime? bCreateTime, DateTime? eCreateTime, string partsSalesReturnBillCode, string partsSalesOrderCode, string sparePartCode, string code, string settlementStatus, string settlementNo, string sAPSysInvoiceNumber, string counterpartCompanyName, string counterpartCompanyCode) {
             return new PartsSaleRtnInboundCheckAch(this).销售退货入库单统计(bCreateTime, eCreateTime, partsSalesReturnBillCode, partsSalesOrderCode, sparePartCode, code, settlementStatus, settlementNo, sAPSysInvoiceNumber, counterpartCompanyName, counterpartCompanyCode);
         }
     }
}
