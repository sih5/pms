﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web
{
    partial class PartsShelvesTaskFinishAch : DcsSerivceAchieveBase
    {
        public PartsShelvesTaskFinishAch(DcsDomainService domainService)
            : base(domainService)
        {
        }
        public IEnumerable<PartsShelvesTaskFinish> GetPartsShelvesTaskFinishs(DateTime? bPackModifyTime, DateTime? ePackModifyTime, string partsInboundCheckBillCode, int? warehouseId, string sparePartCode, string referenceCode, int? partABC, string partsPurchaseOrderCode, DateTime? bShelvesFinishTime, DateTime? eShelvesFinishTime)
        {
            string SQL = @"select a.id,
                          a.warehouseid,
                          a.warehousename,
                          p.code as sparepartcode,
                          p.name as sparepartname,
                          b.PartABC,
                          c.SupplierPartCode,
                          d.code as PartsInboundCheckBillCode,
                          f.code as PartsPurchaseOrderCode,
                          (select sum(ck.inspectedquantity)
                             from partsinboundcheckbilldetail ck
                              where ck.partsinboundcheckbillid = d.id
                              and ck.sparepartid = a.sparepartid) as inspectedquantity,
                         a.ShelvesAmount,
                         d.createtime as CheckBillCreateTime,
                           a.createtime,
                          h.retailguideprice,
                         p.referencecode,
                         a.shelvesfinishtime,
                          a.PlannedAmount as packingqty,
                          pt.modifytime as PackModifyTime
                         from PartsShelvesTask a
                         left join partsbranch b
                         on a.sparepartid = b.partid
                         and b.status = 1
                         left join PartsSupplierRelation c
                         on a.sparepartid = c.partid
                        and c.status = 1
                         and c.isprimary = 1
                         left join PartsInboundCheckBill d
                          on a.partsinboundcheckbillid = d.id
                          left join PartsPurchaseOrder f
                           on d.originalrequirementbillid = f.id
                          and d.originalrequirementbillcode = f.code
                        left join PartsRetailGuidePrice h
                        on d.partssalescategoryid = h.partssalescategoryid
                         and h.branchid = d.branchid
                         and h.sparepartid = a.sparepartid
                         and h.status = 1
                         join sparepart p
                         on a.sparepartid = p.id
                        left join packingtask pt
                          on a.packingtaskid = pt.id
                        where 1 = 1 ";

            if (bPackModifyTime.HasValue && bPackModifyTime.Value == Convert.ToDateTime("9999/12/31 23:59:59"))
            {
                bPackModifyTime = null;
            }

            if (bShelvesFinishTime.HasValue && bShelvesFinishTime.Value == Convert.ToDateTime("9999/12/31 23:59:59"))
            {
                bShelvesFinishTime = null;
            }
            if (bPackModifyTime.HasValue)
            {
                SQL = SQL + " and pt.ModifyTime>= to_date('" + bPackModifyTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if (ePackModifyTime.HasValue)
            {
                SQL = SQL + " and pt.ModifyTime<= to_date('" + ePackModifyTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if (bShelvesFinishTime.HasValue)
            {
                SQL = SQL + " and a.ShelvesFinishTime>= to_date('" + bShelvesFinishTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if (eShelvesFinishTime.HasValue)
            {
                SQL = SQL + " and a.ShelvesFinishTime<= to_date('" + eShelvesFinishTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if (!string.IsNullOrEmpty(partsInboundCheckBillCode))
            {
                SQL = SQL + " and LOWER(d.code) like '%" + partsInboundCheckBillCode.ToLower() + "%'";
            }
            if (!string.IsNullOrEmpty(sparePartCode))
            {

                SQL = SQL + " and LOWER(p.code) like '%" + sparePartCode.ToLower() + "%'";
            }
            if (warehouseId.HasValue)
            {
                SQL = SQL + " and a.warehouseId=" + warehouseId.Value;
            }
            if (!string.IsNullOrEmpty(referenceCode))
            {
                SQL = SQL + " and LOWER(p.referenceCode) like '%" + referenceCode.ToLower() + "%'";
            }
            if (!string.IsNullOrEmpty(partsPurchaseOrderCode))
            {
                SQL = SQL + " and LOWER(f.code) like '%" + partsPurchaseOrderCode.ToLower() + "%'";
            }
            if (partABC.HasValue)
            {
                SQL = SQL + " and b.partABC=" + partABC.Value;
            }
            var detail = ObjectContext.ExecuteStoreQuery<PartsShelvesTaskFinish>(SQL).ToList();
            return detail;
        }
    }
    partial class DcsDomainService
    {
        public IEnumerable<PartsShelvesTaskFinish> GetPartsShelvesTaskFinishs(DateTime? bPackModifyTime, DateTime? ePackModifyTime, string partsInboundCheckBillCode, int? warehouseId, string sparePartCode, string referenceCode, int? partABC, string partsPurchaseOrderCode, DateTime? bShelvesFinishTime, DateTime? eShelvesFinishTime)
        {
            return new PartsShelvesTaskFinishAch(this).GetPartsShelvesTaskFinishs(bPackModifyTime, ePackModifyTime, partsInboundCheckBillCode, warehouseId, sparePartCode, referenceCode, partABC, partsPurchaseOrderCode, bShelvesFinishTime, eShelvesFinishTime);
        }

    }
}