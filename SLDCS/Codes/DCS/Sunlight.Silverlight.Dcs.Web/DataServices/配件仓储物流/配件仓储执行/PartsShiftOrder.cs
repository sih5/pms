﻿using System;
using System.ServiceModel.DomainServices.Server;
using Sunlight.Silverlight.Web;
using System.Linq;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsShiftOrderAch : DcsSerivceAchieveBase {
        internal void InsertPartsShiftOrderValidate(PartsShiftOrder partsShiftOrder) {
            if(string.IsNullOrWhiteSpace(partsShiftOrder.Code) || partsShiftOrder.Code == GlobalVar.ASSIGNED_BY_SERVER)
                partsShiftOrder.Code = CodeGenerator.Generate("PartsShiftOrder", partsShiftOrder.StorageCompanyCode);
            var userInfo = Utils.GetCurrentUserInfo();
            partsShiftOrder.CreatorId = userInfo.Id;
            partsShiftOrder.CreatorName = userInfo.Name;
            partsShiftOrder.CreateTime = DateTime.Now;
        }

        internal void UpdatePartsShiftOrderValidate(PartsShiftOrder partsShiftOrder) {
            var userInfo = Utils.GetCurrentUserInfo();
            partsShiftOrder.ModifierId = userInfo.Id;
            partsShiftOrder.ModifierName = userInfo.Name;
            partsShiftOrder.ModifyTime = DateTime.Now;
        }

        public void InsertPartsShiftOrder(PartsShiftOrder partsShiftOrder) {
            InsertToDatabase(partsShiftOrder);
            var partsShiftOrderDetails = ChangeSet.GetAssociatedChanges(partsShiftOrder, v => v.PartsShiftOrderDetails, ChangeOperation.Insert);
            foreach(PartsShiftOrderDetail partsShiftOrderDetail in partsShiftOrderDetails) {
                InsertToDatabase(partsShiftOrderDetail);
            }
            this.InsertPartsShiftOrderValidate(partsShiftOrder);
        }

        public void UpdatePartsShiftOrder(PartsShiftOrder partsShiftOrder) {
            partsShiftOrder.PartsShiftOrderDetails.Clear();
            UpdateToDatabase(partsShiftOrder);
            var partsShiftOrderDetails = ChangeSet.GetAssociatedChanges(partsShiftOrder, v => v.PartsShiftOrderDetails);
            foreach(PartsShiftOrderDetail partsShiftOrderDetail in partsShiftOrderDetails) {
                switch(ChangeSet.GetChangeOperation(partsShiftOrderDetail)) {
                    case ChangeOperation.Insert:
                        InsertToDatabase(partsShiftOrderDetail);
                        break;
                    case ChangeOperation.Update:
                        UpdateToDatabase(partsShiftOrderDetail);
                        break;
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(partsShiftOrderDetail);
                        break;
                }
            }
            this.UpdatePartsShiftOrderValidate(partsShiftOrder);
        }
    
         public IQueryable<PartsShiftOrder> GetPartsShiftOrderWithDetails() {
             var res = ObjectContext.PartsShiftOrders.Include("PartsShiftOrderDetails");
             //foreach (var item in res) { 
             //    item.IsUplodFile = item.Path != null && item.Path.Length > 0;
             //}
            return res.OrderBy(e => e.Id);
        }
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:32
        //原分布类的函数全部转移到Ach                                                               

        public void InsertPartsShiftOrder(PartsShiftOrder partsShiftOrder) {
            new PartsShiftOrderAch(this).InsertPartsShiftOrder(partsShiftOrder);
        }

        public void UpdatePartsShiftOrder(PartsShiftOrder partsShiftOrder) {
            new PartsShiftOrderAch(this).UpdatePartsShiftOrder(partsShiftOrder);
        }

        public IQueryable<PartsShiftOrder> GetPartsShiftOrderWithDetails() { 
           return  new PartsShiftOrderAch(this).GetPartsShiftOrderWithDetails();
        }
    }
}
