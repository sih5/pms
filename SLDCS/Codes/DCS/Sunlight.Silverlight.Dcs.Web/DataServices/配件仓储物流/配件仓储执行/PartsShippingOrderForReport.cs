﻿using System;
using Sunlight.Silverlight.Web;
using System.Collections.Generic;
using System.Linq;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsShippingOrderForReportAch : DcsSerivceAchieveBase {
        public PartsShippingOrderForReportAch(DcsDomainService domainService)
            : base(domainService) {
        }
        public IEnumerable<PartsShippingOrderForReport> 发运单统计查询(string code, string partsSalesOrderCode, DateTime? bShippingDate, DateTime? eShippingDate, string appraiserName) {
            var userInfo = Utils.GetCurrentUserInfo();
            string SQL = @"select tt.*,
       ps.code as PartsSalesOrderCode,
       ps.PartsSalesOrderTypeName,
       ps.ApproverName as AppraiserName
  from (SELECT Extent1.Id,
               Extent1.ShippingDate,
               Extent1.ShippingMethod,
               Extent1.Code,
               (select sum(A1) from (SELECT  (case when  pd.originalprice is null then  Extent4.SettlementPrice * Extent4.ShippingAmount else  pd.originalprice * Extent4.ShippingAmount  end )AS A1,Extent4.PartsShippingOrderId
                  FROM PartsShippingOrderDetail Extent4
                  left join partsoutboundplan pl on  Extent4.Partsoutboundplanid=pl.id
                  left join partssalesorder ps on ps.id=pl.OriginalRequirementBillId and pl.originalrequirementbilltype=1 
                  left join partssalesorderdetail pd on ps.id=pd.partssalesorderid and Extent4.Sparepartid=pd.sparepartid 
                 )  tt where tt.PartsShippingOrderId=Extent1.Id group by PartsShippingOrderId ) AS SettlementPriceAll,
               Extent1.ReceivingAddress,
               Extent1.ReceivingCompanyName,
               Extent1.LinkName,
                (SELECT Sum(sp.Weight * Extent4.ShippingAmount) AS A1
                  FROM PartsShippingOrderDetail Extent4
                  join sparepart sp
                    on Extent4.Sparepartid = sp.id
                 WHERE Extent4.PartsShippingOrderId = Extent1.Id) AS Weight,
               Extent1.WarehouseName,
                (SELECT Sum(sp.volume * Extent4.ShippingAmount) AS A1
                  FROM PartsShippingOrderDetail Extent4
                  join sparepart sp
                    on Extent4.Sparepartid = sp.id
                 WHERE Extent4.PartsShippingOrderId = Extent1.Id) AS Volume,
               Extent1.Remark,
               (select Extent3.id
                  from PartsSalesOrder Extent3
                  join PartsOutboundPlan Extent2
                    ON (Extent2.OriginalRequirementBillId = Extent3.Id)
                   AND (Extent2.OriginalRequirementBillType = 1)
                  join partsshippingorderdetail d
                    on d.PartsOutboundPlanId = Extent2.id
                   and d.partsoutboundplancode = Extent2.code                
                 where rownum = 1
                   and d.partsshippingorderid = Extent1.id) as PartsSalesOrderId,
                Extent1.LogisticCompanyName,
               Extent1.Weight as OldWeight,
               Extent1.Volume as OldVolume,
               Extent1.DeliveryBillNumber
          FROM PartsShippingOrder Extent1 where Extent1.status<>99 and  Extent1.SettlementCompanyId= " + userInfo.EnterpriseId;

            if(bShippingDate.HasValue && bShippingDate.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
                bShippingDate = null;
            }
            if(eShippingDate.HasValue && eShippingDate.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
                eShippingDate = null;
            }
            if(bShippingDate.HasValue) {
                SQL = SQL + " and Extent1.ShippingDate>= to_date('" + bShippingDate.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if(eShippingDate.HasValue) {
                SQL = SQL + " and Extent1.ShippingDate<= to_date('" + eShippingDate.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if(!string.IsNullOrEmpty(code)) {

                SQL = SQL + " and LOWER(Extent1.code) like '%" + code.ToLower() + "%'";
            }
            SQL = SQL + " ) tt left join PartsSalesOrder ps on ps.id = tt.PartsSalesOrderId where 1=1";

            if(!string.IsNullOrEmpty(partsSalesOrderCode)) {

                SQL = SQL + " and LOWER(ps.code) like '%" + partsSalesOrderCode.ToLower() + "%'";
            } if(!string.IsNullOrEmpty(appraiserName)) {

                SQL = SQL + " and LOWER(ps.ApproverName) like '%" + appraiserName.ToLower() + "%'";
            }
            var detail = ObjectContext.ExecuteStoreQuery<PartsShippingOrderForReport>(SQL).ToList();
            return detail;
        }
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:32
        //原分布类的函数全部转移到Ach                                                               


        public IEnumerable<PartsShippingOrderForReport> 发运单统计查询(string code, string partsSalesOrderCode, DateTime? bShippingDate, DateTime? eShippingDate, string appraiserName) {
            return new PartsShippingOrderForReportAch(this).发运单统计查询(code, partsSalesOrderCode, bShippingDate, eShippingDate, appraiserName);
        }
    }
}

