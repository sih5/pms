﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsTransferOrderAch : DcsSerivceAchieveBase {
        internal void InsertPartsTransferOrderValidate(PartsTransferOrder partsTransferOrder) {
            var dbBranch = ObjectContext.Branches.SingleOrDefault(r => ObjectContext.Warehouses.Any(v => v.Id == partsTransferOrder.OriginalWarehouseId && v.BranchId == r.Id && v.Status != (int)DcsBaseDataStatus.作废));
            if(dbBranch == null) {
                throw new ValidationException(ErrorStrings.Branch_Validation4);
            }
            //var dbSalesUnitAffiWarehouses = ObjectContext.SalesUnitAffiWarehouses.Include("SalesUnit").Where(r => r.WarehouseId == partsTransferOrder.OriginalWarehouseId || r.WarehouseId == partsTransferOrder.DestWarehouseId).ToList();
            //var originalPartsSalesCategoryId = dbSalesUnitAffiWarehouses.First(r => r.WarehouseId == partsTransferOrder.OriginalWarehouseId).SalesUnit.PartsSalesCategoryId;
            //var destPartsSalesCategoryId = dbSalesUnitAffiWarehouses.First(r => r.WarehouseId == partsTransferOrder.DestWarehouseId).SalesUnit.PartsSalesCategoryId;
            //var partIds = partsTransferOrder.PartsTransferOrderDetails.Select(r => r.SparePartId).ToArray();
            //var dbPartsPlannedPrices = ObjectContext.PartsPlannedPrices.Where(r => partIds.Contains(r.SparePartId) && (r.PartsSalesCategoryId == originalPartsSalesCategoryId || r.PartsSalesCategoryId == destPartsSalesCategoryId)).ToList();
            //foreach(var detail in partsTransferOrder.PartsTransferOrderDetails) {
            //    var dbPartsPlannedPrice1 = dbPartsPlannedPrices.First(r => r.SparePartId == detail.SparePartId && r.PartsSalesCategoryId == originalPartsSalesCategoryId);
            //    var dbPartsPlannedPrice2 = dbPartsPlannedPrices.First(r => r.SparePartId == detail.SparePartId && r.PartsSalesCategoryId == destPartsSalesCategoryId);
            //    if(dbPartsPlannedPrice1.PlannedPrice != dbPartsPlannedPrice2.PlannedPrice)
            //        throw new ValidationException("调拨出库入库仓库配件计划价不同无法生成调拨单");
            //}
            ValidateWarehouse(partsTransferOrder);
            ValidateBottomStock(partsTransferOrder);
            //判断Code是否需要生成编号
            if(string.IsNullOrWhiteSpace(partsTransferOrder.Code) || partsTransferOrder.Code == GlobalVar.ASSIGNED_BY_SERVER)
                partsTransferOrder.Code = CodeGenerator.Generate("PartsTransferOrder", dbBranch.Code);

            var userInfo = Utils.GetCurrentUserInfo();
            partsTransferOrder.CreatorId = userInfo.Id;
            partsTransferOrder.CreatorName = userInfo.Name;
            partsTransferOrder.CreateTime = DateTime.Now;

        }

        private void ValidateBottomStock(PartsTransferOrder partsTransferOrder) {
            var user = Utils.GetCurrentUserInfo();
            var company = ObjectContext.Companies.Single(r => r.Id == user.EnterpriseId);
            //只有总部才校验保底库存
            if(company.Type == (int)DcsCompanyType.分公司){
                var partIds = partsTransferOrder.PartsTransferOrderDetails.Select(r => r.SparePartId).ToArray();
                //保底库存
                //var bottomStocks = ObjectContext.BottomStocks.Where(r =>r.Status != (int)DcsBaseDataStatus.作废 && r.CompanyID == user.EnterpriseId && r.WarehouseID == partsTransferOrder.OriginalWarehouseId && partIds.Contains(r.SparePartId)).ToArray();
                //可用库存
                var partsStocks =  new WarehousePartsStockAch(this.DomainService).配件调拨查询仓库库存(user.EnterpriseId,partsTransferOrder.OriginalWarehouseId,partIds).ToArray();

                foreach (var detail in partsTransferOrder.PartsTransferOrderDetails) {
                    //var bottomStock = bottomStocks.FirstOrDefault(r => r.SparePartId == detail.SparePartId);
                    //var stockQty = bottomStock == null ? 0 : bottomStock.StockQty;
                    var partsStock = partsStocks.FirstOrDefault(r => r.SparePartId == detail.SparePartId);
                    if(partsStock == null)
                        throw new ValidationException(string.Format(ErrorStrings.InternalAllocationBill_Validation10,detail.SparePartCode));

                    if (detail.ConfirmedAmount != null) {
                        if (partsStock.UsableQuantity - detail.ConfirmedAmount < 0) {
                            throw new ValidationException(string.Format(ErrorStrings.PartsTransferOrder_Validation16,detail.SparePartCode));
                        }
                    } else {
                        if (partsStock.UsableQuantity - detail.PlannedAmount < 0) {
                            throw new ValidationException(string.Format(ErrorStrings.PartsTransferOrder_Validation16,detail.SparePartCode));
                        }
                    }
                }
            }
        }

        private void ValidateWarehouse(PartsTransferOrder partsTransferOrder) {
            var oWarehouse = this.ObjectContext.Warehouses.Where(r => partsTransferOrder.OriginalWarehouseName == r.Name && partsTransferOrder.OriginalWarehouseId == r.Id && r.Code == partsTransferOrder.OriginalWarehouseCode).ToArray();
            if(!oWarehouse.Any()) {
                throw new ValidationException(ErrorStrings.PartsTransferOrder_Validation17);
            }

            var dWarehouse = this.ObjectContext.Warehouses.Where(r => partsTransferOrder.DestWarehouseName == r.Name && partsTransferOrder.DestWarehouseId == r.Id && r.Code == partsTransferOrder.DestWarehouseCode).ToArray();
            if(!dWarehouse.Any()) {
                throw new ValidationException(ErrorStrings.PartsTransferOrder_Validation18);
            }
        }

        internal void UpdatePartsTransferOrderValidate(PartsTransferOrder partsTransferOrder) {
            ValidateWarehouse(partsTransferOrder);
            if (partsTransferOrder.ApprovalId != 0)
                ValidateBottomStock(partsTransferOrder);
            var userInfo = Utils.GetCurrentUserInfo();
            partsTransferOrder.ModifierId = userInfo.Id;
            partsTransferOrder.ModifierName = userInfo.Name;
            partsTransferOrder.ModifyTime = DateTime.Now;
        }

        public void InsertPartsTransferOrder(PartsTransferOrder partsTransferOrder) {
            InsertToDatabase(partsTransferOrder);
            var partsTransferOrderDetails = ChangeSet.GetAssociatedChanges(partsTransferOrder, v => v.PartsTransferOrderDetails, ChangeOperation.Insert);
            foreach(PartsTransferOrderDetail partsTransferOrderDetail in partsTransferOrderDetails)
                InsertToDatabase(partsTransferOrderDetail);
            this.InsertPartsTransferOrderValidate(partsTransferOrder);
        }

        public void UpdatePartsTransferOrder(PartsTransferOrder partsTransferOrder) {
            partsTransferOrder.PartsTransferOrderDetails.Clear();
            UpdateToDatabase(partsTransferOrder);
            var partsTransferOrderDetails = ChangeSet.GetAssociatedChanges(partsTransferOrder, v => v.PartsTransferOrderDetails);
            foreach(PartsTransferOrderDetail partsTransferOrderDetail in partsTransferOrderDetails) {
                //获取清单实体对象的操作类型
                switch(ChangeSet.GetChangeOperation(partsTransferOrderDetail)) {
                    //新增操作
                    case ChangeOperation.Insert:
                        InsertToDatabase(partsTransferOrderDetail);
                        break;
                    //更新操作
                    case ChangeOperation.Update:
                        UpdateToDatabase(partsTransferOrderDetail);
                        break;
                    //删除操作
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(partsTransferOrderDetail);
                        break;
                }
            }
            this.UpdatePartsTransferOrderValidate(partsTransferOrder);
        }

        public PartsTransferOrder 查询调拨看板(int partsTransferOrderId) {
            var partsTransferOrder = ObjectContext.PartsTransferOrders.SingleOrDefault(r => r.Id == partsTransferOrderId && r.Status != (int)DcsPartsTransferOrderStatus.作废);
            if(partsTransferOrder == null)
                return null;
            var partsTransferOrderDetail = ObjectContext.PartsTransferOrderDetails.Where(r => r.PartsTransferOrderId == partsTransferOrder.Id).ToArray();
            var partsShippingOrder = ObjectContext.PartsShippingOrders.FirstOrDefault(r => r.OriginalRequirementBillId == partsTransferOrder.Id && r.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件调拨单 && r.Status != (int)DcsPartsShippingOrderStatus.作废);
            if(partsShippingOrder != null) {
                var partsShippingOrderRefs = ObjectContext.PartsShippingOrderRefs.Where(r => r.PartsShippingOrderId == partsShippingOrder.Id).ToArray();
                var partsShippingOrderDetails = ObjectContext.PartsShippingOrderDetails.Where(r => r.PartsShippingOrderId == partsShippingOrder.Id).ToArray();
            }
            var partsOutboundBill = ObjectContext.PartsOutboundBills.FirstOrDefault(r => r.OriginalRequirementBillId == partsTransferOrder.Id && r.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件调拨单);
            if(partsOutboundBill != null) {
                var partsOutboundBillDetails = ObjectContext.PartsOutboundBillDetails.Where(r => r.PartsOutboundBillId == partsOutboundBill.Id).ToArray();
            }
            var partsInboundCheckBill = ObjectContext.PartsInboundCheckBills.FirstOrDefault(r => r.OriginalRequirementBillId == partsTransferOrder.Id && r.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件调拨单);
            if(partsInboundCheckBill != null) {
                var partsInboundCheckBillDetail = ObjectContext.PartsInboundCheckBillDetails.Where(r => r.PartsInboundCheckBillId == partsInboundCheckBill.Id).ToArray();
            }
            return partsTransferOrder;
        }

        public PartsTransferOrder GetPartsTransferOrdersWithDetailsById(int partsTransferOrderId) {
            var transferOrder = ObjectContext.PartsTransferOrders.SingleOrDefault(e => e.Id == partsTransferOrderId);
            if(transferOrder != null) {
                var transferOrderDetails = ObjectContext.PartsTransferOrderDetails.Where(e => e.PartsTransferOrderId == transferOrder.Id).ToArray();
                var partIds = transferOrderDetails.Select(r => r.SparePartId);
                var spareParts = ObjectContext.SpareParts.Where(r => partIds.Contains(r.Id));
                foreach (var detail in transferOrderDetails) {
                    var part = spareParts.Where(r => r.Id == detail.SparePartId).SingleOrDefault();
                    detail.MInPackingAmount = part.MInPackingAmount;
                }
            }
            return transferOrder;
        }

        public PartsTransferOrder GetPartsTransferOrdersById(int Id) {
            var transferOrder = ObjectContext.PartsTransferOrders.SingleOrDefault(e => e.Id == Id);
            return transferOrder;
        }

        public IQueryable<PartsTransferOrderDetail> GetPartsTransferOrderDetailsWithPartsTransferOrder(int partsTransferOrderId) {
            var details = ObjectContext.PartsTransferOrderDetails.Where(r => r.PartsTransferOrderId == partsTransferOrderId);
            var partIds = details.Select(r => r.SparePartId);
            var spareParts = ObjectContext.SpareParts.Where(r => partIds.Contains(r.Id));
            foreach (var detail in details) {
                var part = spareParts.Where(r => r.Id == detail.SparePartId).SingleOrDefault();
                detail.MInPackingAmount = part.MInPackingAmount;
            }
            return details.OrderBy(r => r.Id);
        }

        public IQueryable<PartsTransferOrder> GetPartsTransferOrdersByWarehouseOperator() {
            var userInfo = Utils.GetCurrentUserInfo();
            var partsTransferOrders = ObjectContext.PartsTransferOrders.Where(r => ObjectContext.WarehouseOperators.Any(v => v.OperatorId == userInfo.Id && v.WarehouseId == r.OriginalWarehouseId) || ObjectContext.Warehouses.Any(v => v.Id == r.OriginalWarehouseId && v.Type == (int)DcsWarehouseType.虚拟库 && v.Status == (int)DcsBaseDataStatus.有效)).OrderBy(i => i.Id);
            foreach (var item in partsTransferOrders) {
                item.IsUploadFile = item.Path != null && item.Path.Length > 0;
            }
            return partsTransferOrders;
        }

    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:32
        //原分布类的函数全部转移到Ach                                                               

        public void InsertPartsTransferOrder(PartsTransferOrder partsTransferOrder) {
            new PartsTransferOrderAch(this).InsertPartsTransferOrder(partsTransferOrder);
        }

        public void UpdatePartsTransferOrder(PartsTransferOrder partsTransferOrder) {
            new PartsTransferOrderAch(this).UpdatePartsTransferOrder(partsTransferOrder);
        }

        public PartsTransferOrder 查询调拨看板(int partsTransferOrderId) {
            return new PartsTransferOrderAch(this).查询调拨看板(partsTransferOrderId);
        }

        public PartsTransferOrder GetPartsTransferOrdersWithDetailsById(int partsTransferOrderId) {
            return new PartsTransferOrderAch(this).GetPartsTransferOrdersWithDetailsById(partsTransferOrderId);
        }

        public PartsTransferOrder GetPartsTransferOrdersById(int Id) {
            return new PartsTransferOrderAch(this).GetPartsTransferOrdersById(Id);
        }

        public IQueryable<PartsTransferOrderDetail> GetPartsTransferOrderDetailsWithPartsTransferOrder(int partsTransferOrderId) {
            return new PartsTransferOrderAch(this).GetPartsTransferOrderDetailsWithPartsTransferOrder(partsTransferOrderId);
        }

        public IQueryable<PartsTransferOrder> GetPartsTransferOrdersByWarehouseOperator() {
            return new PartsTransferOrderAch(this).GetPartsTransferOrdersByWarehouseOperator();
        }
    }
}
