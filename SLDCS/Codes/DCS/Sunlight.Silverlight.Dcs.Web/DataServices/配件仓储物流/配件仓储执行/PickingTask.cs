﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PickingTaskAch : DcsSerivceAchieveBase {
        internal void InsertPickingTaskValidate(PickingTask pickingTask) {
            var userInfo = Utils.GetCurrentUserInfo();
            if(string.IsNullOrWhiteSpace(pickingTask.Code) || pickingTask.Code == GlobalVar.ASSIGNED_BY_SERVER)
                pickingTask.Code = CodeGenerator.Generate("PickingTask", userInfo.EnterpriseCode);
            pickingTask.CreatorId = userInfo.Id;
            pickingTask.CreatorName = userInfo.Name;
            pickingTask.CreateTime = DateTime.Now;
            pickingTask.BranchId = userInfo.EnterpriseId;
        }

        internal void UpdatePickingTaskValidate(PickingTask pickingTask) {
            var userInfo = Utils.GetCurrentUserInfo();
            pickingTask.ModifierId = userInfo.Id;
            pickingTask.ModifierName = userInfo.Name;
            pickingTask.ModifyTime = DateTime.Now;
        }

        public void InsertPickingTask(PickingTask pickingTask) {
            InsertToDatabase(pickingTask);
            var pickingTaskDetails = ChangeSet.GetAssociatedChanges(pickingTask, v => v.PickingTaskDetails, ChangeOperation.Insert);
            foreach(PickingTaskDetail pickingTaskDetail in pickingTaskDetails)
                InsertToDatabase(pickingTaskDetail);
            this.InsertPickingTaskValidate(pickingTask);
        }

        public void UpdatePickingTask(PickingTask pickingTask) {
            pickingTask.PickingTaskDetails.Clear();
            UpdateToDatabase(pickingTask);
            var pickingTaskDetails = ChangeSet.GetAssociatedChanges(pickingTask, v => v.PickingTaskDetails);
            foreach(PickingTaskDetail pickingTaskDetail in pickingTaskDetails) {
                //获取清单实体对象的操作类型
                switch(ChangeSet.GetChangeOperation(pickingTaskDetail)) {
                    //新增操作
                    case ChangeOperation.Insert:
                        InsertToDatabase(pickingTaskDetail);
                        break;
                    //更新操作
                    case ChangeOperation.Update:
                        UpdateToDatabase(pickingTaskDetail);
                        break;
                    //删除操作
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(pickingTaskDetail);
                        break;
                }
            }
            this.UpdatePickingTaskValidate(pickingTask);
        }
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:30
        //原分布类的函数全部转移到Ach                                                               

        public void InsertPickingTask(PickingTask pickingTask) {
            new PickingTaskAch(this).InsertPickingTask(pickingTask);
        }

        public void UpdatePickingTask(PickingTask pickingTask) {
            new PickingTaskAch(this).UpdatePickingTask(pickingTask);
        }

    }
}
