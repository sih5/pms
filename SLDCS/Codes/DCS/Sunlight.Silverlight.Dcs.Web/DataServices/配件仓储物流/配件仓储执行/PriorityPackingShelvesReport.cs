﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Sunlight.Silverlight.Dcs.Web{
    partial class PriorityPackingShelvesReportAch : DcsSerivceAchieveBase {
       public PriorityPackingShelvesReportAch(DcsDomainService domainService)
            : base(domainService) {
        }
       public IEnumerable<PriorityPackingShelvesReport> 获取优先包装上架明细表(DateTime? createTime, int? priority, int? packingFinishStatus, int? shelvesFinishStatus, DateTime? bPriorityPackingTime, DateTime? ePriorityPackingTime, DateTime? bPriorityShelvesTime, DateTime? ePriorityShelvesTime) {

           if(bPriorityPackingTime.HasValue && bPriorityPackingTime.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
               bPriorityPackingTime = null;
           }
           if(bPriorityShelvesTime.HasValue && bPriorityShelvesTime.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
               bPriorityShelvesTime = null;
           }
           string SQL = "";
           var serachSQL = new StringBuilder();
           if(createTime.Value.Date == DateTime.Now.Date) {
               SQL = @"select rownum, s.priority,
                       s.partspurchaseordercode,
                       s.inboundcode,
                       s.sparepartcode,
                       s.referencecode,
                       s.sparepartname,
                       s.inspectedquantity,
                       s.packingqty,
                       s.shallpackingentries,
                       s.actualpackingentries,
                       s.shelvesamount,
                       s.shallshelvesentries,
                       s.actualshelvesentries,
                       s.inboundfinishtime,
                       s.packingfinishtime,
                       s.packingfinishstatus,
                       s.shelvesfinishstatus,
                       s.PriorityCreateTime,
                       s.ShelvesFinishTime
                  from (select distinct ppb.priority,
                                        picb.originalrequirementbillcode as PartsPurchaseOrderCode,
                                        picb.code as InboundCode,
                                        picbd.sparepartcode,
                                        s.referencecode,
                                        picbd.sparepartname,
                                        picbd.inspectedquantity,
                                        pt.PackingQty,
                                        nvl(picbd.inspectedquantity, 0) /
                                        decode(s.MInPackingAmount,
                                               0,
                                               1,
                                               null,
                                               1,
                                               s.MInPackingAmount) as ShallPackingEntries,
                                        nvl(pt.PackingQty, 0) /
                                        decode(s.MInPackingAmount,
                                               0,
                                               1,
                                               null,
                                               1,
                                               s.MInPackingAmount) as ActualPackingEntries,
                                        sum(pst.ShelvesAmount) as ShelvesAmount,
                                        nvl(pt.PackingQty, 0) /
                                        decode(s.MInPackingAmount,
                                               0,
                                               1,
                                               null,
                                               1,
                                               s.MInPackingAmount) as ShallShelvesEntries,
                                        nvl(sum(pst.ShelvesAmount), 0) /
                                        decode(s.MInPackingAmount,
                                               0,
                                               1,
                                               null,
                                               1,
                                               s.MInPackingAmount) as ActualShelvesEntries,
                                        picb.createtime as inboundfinishtime,
                                        pt.ModifyTime as packingfinishtime,
                                       
                                        (case
                                          when nvl(pt.PackingQty, 0) = 0 then
                                           3
                                          when nvl(picbd.inspectedquantity, 0) >
                                               nvl(pt.PackingQty, 0) then
                                           2
                                          else
                                           1
                                        end) as PackingFinishStatus,
                                        max(pst.ShelvesFinishTime) as ShelvesFinishTime,
                                        (case
                                          when nvl(sum(pst.ShelvesAmount), 0) = 0 then
                                           3
                                          when nvl(pt.PackingQty, 0) >
                                               nvl(sum(pst.ShelvesAmount), 0) then
                                           2
                                          else
                                           1
                                        end) as ShelvesFinishStatus,
                                        ppb.PriorityCreateTime
                          from partsinboundcheckbill picb
                         inner join partsinboundcheckbilldetail picbd
                            on picb.id = picbd.partsinboundcheckbillid
                         inner join PartsPriorityBill ppb
                            on ppb.partid = picbd.sparepartid
                           and ppb.Priority is not null
                         inner join sparepart s
                            on s.id = picbd.sparepartid
                           and s.Status = 1
                         inner join packingtask pt
                            on pt.partsinboundcheckbillid = picb.id
                           and pt.sparepartid = picbd.sparepartid
                          left join PartsShelvesTask pst
                            on pst.partsinboundcheckbillid = picb.id
                           and pst.sparepartid = picbd.sparepartid
                         where picb.status<>3
                         group by ppb.priority,
                                  picb.originalrequirementbillcode,
                                  picb.code,
                                  picbd.sparepartcode,
                                  s.referencecode,
                                  picbd.sparepartname,
                                  picbd.inspectedquantity,
                                  pt.PackingQty,
                                  picbd.inspectedquantity,
                                  s.MInPackingAmount,
                                  pt.PackingQty,
                                  picb.createtime,
                                  pt.ModifyTime,
                                  ppb.PriorityCreateTime,
                                  pt.ModifyTime) s  where (s.ShelvesFinishTime > s.PriorityCreateTime  or s.ShelvesFinishTime is null) {0}";
           } else {
               SQL = @"select rownum,
                       s.priority,
                       s.partspurchaseordercode,
                       s.inboundcode,
                       s.sparepartcode,
                       s.referencecode,
                       s.sparepartname,
                       s.inspectedquantity,
                       s.packingqty,
                       s.shallpackingentries,
                       s.actualpackingentries,
                       s.shelvesamount,
                       s.shallshelvesentries,
                       s.actualshelvesentries,
                       s.inboundfinishtime,
                       s.packingfinishtime,
                       s.packingfinishstatus,
                       s.prioritypackingtime,
                       s.shelvesfinishtime,
                       s.shelvesfinishstatus,
                       s.priorityshelvestime,
                       s.createtime,
                       s.PriorityCreateTime
                  from AddPriorityPackingAndShelves s
                 where {0} ";
               serachSQL.Append("  trunc(s.CreateTime)=trunc(to_date('" + createTime + "','yyyy-MM-dd HH24:mi:ss'))");
           }

           if(packingFinishStatus.HasValue) {
               serachSQL.Append(" and s.packingFinishStatus =" + packingFinishStatus );
           }
           if(priority.HasValue) {
               serachSQL.Append(" and s.priority =" + priority );
           }
           if(shelvesFinishStatus.HasValue) {
               serachSQL.Append(" and s.shelvesFinishStatus =" + shelvesFinishStatus);
           }
           if(bPriorityPackingTime.HasValue) {
               serachSQL.Append(" and s.PackingFinishTime>= to_date('" + bPriorityPackingTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')");
           }
           if(ePriorityPackingTime.HasValue) {
               serachSQL.Append(" and s.PackingFinishTime<= to_date('" + ePriorityPackingTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')");
           }
           if(bPriorityShelvesTime.HasValue) {
               serachSQL.Append(" and s.ShelvesFinishTime>= to_date('" + bPriorityShelvesTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')");
           }
           if(ePriorityShelvesTime.HasValue) {
               serachSQL.Append(" and s.ShelvesFinishTime<= to_date('" + ePriorityShelvesTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')");
           }         
           SQL = string.Format(SQL, serachSQL);
           var search = ObjectContext.ExecuteStoreQuery<PriorityPackingShelvesReport>(SQL).ToList();
           return search;
       }
       public IEnumerable<PriorityPackingShelvesReport> 获取优先包装上架明细汇总表(DateTime? bCreateTime,DateTime? eCreateTime, int? priority, int?type) {
           string SQL = @"select rownum,pp.* from (select tt.month,
                           tt.dates,
                           tt.priority,
                           sum(tt.StorageNumber) as StorageNumber,
                           sum(tt.ActStorageNumber) as ActStorageNumber,
                            round(sum(tt.ActStorageNumber)*100/ sum(tt.StorageNumber),2) as CompletionRate,
                           sum(tt.StorageNumberItem) as StorageNumberItem,
                           sum(tt.ActStorageNumberItem) as ActStorageNumberItem,
                           round(sum(tt.ActStorageNumberItem)*100/sum(tt.StorageNumberItem),2) as CompletionEntryRate,
                           sum(tt.PackingNumber) as PackingNumber,
                           sum(tt.PackedNumber) as PackedNumber,
                           sum(tt.PackingNumberItem) as PackingNumberItem,
                           sum(tt.PackedNumberItem) as PackedNumberItem,
                            round(sum(tt.PackedNumber)*100/sum(tt.PackingNumber),2) as PackRate,
                            round( sum(tt.PackedNumberItem)*100/sum(tt.PackingNumberItem),2) as PackItemRate,
                           sum(tt.ShelveNumber) as ShelveNumber,
                           sum(tt.ShelvedNumber) as ShelvedNumber,
                           sum(tt.ShelveNumberItem) as ShelveNumberItem,
                           sum(tt.ShelvedNumberItem) as ShelvedNumberItem,
                           decode(sum(tt.ShelveNumber),0,0,null,0,round(sum(tt.ShelvedNumber)*100/sum(tt.ShelveNumber),2))as ShelveRate,
                           decode(sum(tt.ShelveNumberItem),0,0,null,0 ,round(sum(tt.ShelvedNumberItem)*100/ sum(tt.ShelveNumberItem),2))as ShelveItemRate
                      from (select to_char(s.createtime, 'yyyy-mm') as month,
                                    (case when {1}=2 then to_char(s.createtime, 'dd') else cast('' as varchar2(25)) end)as dates,
                                   s.priority,
                                   1 as StorageNumber,
                                   (case
                                     when s.inspectedquantity = s.shelvesamount then
                                      1
                                     else
                                      0
                                   end) as ActStorageNumber,
                                   ceil(s.inspectedquantity /
                                        (decode(p.minpackingamount,
                                                0,
                                                1,
                                                null,
                                                1,
                                                p.minpackingamount))) as StorageNumberItem,
                                   s.ActualShelvesEntries as ActStorageNumberItem,
                                   1 as PackingNumber,
                                   (case
                                     when s.PackingFinishStatus = 1 then
                                      1
                                     else
                                      0
                                   end) as PackedNumber,
                                   s.ShallPackingEntries as PackingNumberItem,
                                   s.ActualPackingEntries as PackedNumberItem,
                                   (case
                                     when s.ShallShelvesEntries > 0 then
                                      1
                                     else
                                      0
                                   end) as ShelveNumber,
                                   (case
                                     when s.ShallShelvesEntries = s.actualshelvesentries and  s.ShallShelvesEntries > 0 then
                                      1
                                     else
                                      0
                                   end) as ShelvedNumber,
                                   s.ShallShelvesEntries as ShelveNumberItem,
                                   s.actualshelvesentries as ShelvedNumberItem
                              from AddPriorityPackingAndShelves s
                              join sparepart p
                                on s.sparepartcode = p.code
                              where 1=1 {0} ) tt
                     group by tt.month, tt.dates, tt.priority) pp";
           var serachSQL = new StringBuilder();
           if(bCreateTime.HasValue) {
               serachSQL.Append(" and s.CreateTime>= to_date('" + bCreateTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')");
           }
           if(eCreateTime.HasValue) {
               serachSQL.Append(" and s.CreateTime<= to_date('" + eCreateTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')");
           }
           if(priority.HasValue) {
               serachSQL.Append(" and s.priority = " + priority );
           }
           if(type==null){
               type = 2;
           }
           SQL = string.Format(SQL, serachSQL, type);
           var search = ObjectContext.ExecuteStoreQuery<PriorityPackingShelvesReport>(SQL).ToList();
           return search;
       }
    }
   partial class DcsDomainService {
       public IEnumerable<PriorityPackingShelvesReport> 获取优先包装上架明细表(DateTime? createTime, int? priority, int? packingFinishStatus, int? shelvesFinishStatus, DateTime? bPriorityPackingTime, DateTime? ePriorityPackingTime, DateTime? bPriorityShelvesTime, DateTime? ePriorityShelvesTime) {
           return new PriorityPackingShelvesReportAch(this).获取优先包装上架明细表(createTime, priority, packingFinishStatus, shelvesFinishStatus, bPriorityPackingTime, ePriorityPackingTime, bPriorityShelvesTime, ePriorityShelvesTime);
       }
       public IEnumerable<PriorityPackingShelvesReport> 获取优先包装上架明细汇总表(DateTime? bCreateTime, DateTime? eCreateTime, int? priority, int? type) {
           return new PriorityPackingShelvesReportAch(this).获取优先包装上架明细汇总表(bCreateTime, eCreateTime, priority,type);
       }
   }
}
