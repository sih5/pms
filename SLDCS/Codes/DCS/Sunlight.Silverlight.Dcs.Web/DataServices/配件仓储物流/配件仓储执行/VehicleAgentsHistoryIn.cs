﻿using System.Collections.Generic;
using System.Linq;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class VehicleAgentsHistoryInAch : DcsSerivceAchieveBase {
        public VehicleAgentsHistoryInAch(DcsDomainService domainService)
            : base(domainService) {
        }
        
        public IEnumerable<VehicleAgentsHistoryIn> 代理库入库记录查询() {
            const string SQL = @" select a.branchid as BranchId,--营销分公司id
                                       a.branchcode as BranchCode,--分公司编号
                                       a.BranchName as BranchName,--分公司名称
                                       a.code as PartsInboundPlanCode,--入库单编号
									   a.CreateTime as PartsInboundPlanTime,--入库单时间
                                       a.warehouseid as WarehouseId,--仓库id
                                       a.warehousecode as  WarehouseCode, --仓库编号
                                       a.warehousename as WarehouseName,--仓库名称
                                       a.StorageCompanyCode as StorageCompanyCode, --仓储企业编号
                                       a.storagecompanyname as StorageCompanyName,--仓储企业名称
                                       a.CounterpartCompanyCode  as CounterPartCompanyCode, --对方单位编号
                                       a.CounterpartCompanyName  as CounterPartCompanyName , --对方单位名称
                                       a.InboundType as InboundType,                                        
                                       pso.OriginalRequirementBillType as  OriginalRequirementBillType,--原始需求单据类型
                                       a.OriginalRequirementBillCode as  OriginalRequirementBillCode, --原始需求单据编号
                                       a.SettlementStatus as SettlementStatus,
                                       b.warehouseareacode as WarehouseAreaCode,--库位
                                       b.SparePartId as PartId,
                                       b.SparePartCode as PartCode,--配件编号
                                       b.sparepartname as PartName,--配件名称
                                       b.InspectedQuantity as InspectedQuantity, --入库数量
                                       b.SettlementPrice as SettlementPrice, --代理库批发价
                                       (b.InspectedQuantity*b.SettlementPrice) as Price, --入库金额
                                       a.PartsSalesCategoryId as PartsSalesCategoryId,--配件销售类型id
                                       psc.name as PartsSalesCategoryName,
                                       c.provincename as ProvinceName
                                  from partsinboundcheckbill a
                                  left join partssalesorder pso on pso.code = a.originalrequirementbillcode
                                /*  left join SalesUnitAffiWarehouse suaw on suaw.warehouseid = a.warehouseid --销售组织与仓库
                                  left join SalesUnit su on suaw.salesunitid = su.id --销售组织*/
                                  left join partssalescategory psc on a.partssalescategoryid = psc.id
                                  left join company c on a.StorageCompanyCode = c.code
                                 inner join partsinboundcheckbilldetail b on a.id = b.partsinboundcheckbillid
                                 where a.StorageCompanytype in (3,7) ";
            var search = ObjectContext.ExecuteStoreQuery<VehicleAgentsHistoryIn>(SQL).ToList();
            return search.OrderBy(r => r.PartId);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:32
        //原分布类的函数全部转移到Ach                                                               
        
        public IEnumerable<VehicleAgentsHistoryIn> 代理库入库记录查询() {
            return new VehicleAgentsHistoryInAch(this).代理库入库记录查询();
        }
    }
}
