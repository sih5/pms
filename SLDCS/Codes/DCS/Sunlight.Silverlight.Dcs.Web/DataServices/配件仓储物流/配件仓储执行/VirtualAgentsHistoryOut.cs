﻿using System.Collections.Generic;
using System.Linq;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class VirtualAgentsHistoryOutAch : DcsSerivceAchieveBase {
        public VirtualAgentsHistoryOutAch(DcsDomainService domainService)
            : base(domainService) {
        }
        
        public IEnumerable<VirtualAgentsHistoryOut> 代理库出库记录查询() {
            const string SQL = @" select a.branchid as BranchId,
                                   a.branchcode as BranchCode,
                                   a.branchname as BranchName,
                                   a.code as OutboundCode,
                                   a.warehouseid as WarehouseId,
                                   a.warehousecode as WarehouseCode,
                                   a.warehousename as WarehouseName,
                                   a.StorageCompanyCode as StorageCompanyCode, --仓储企业编号
                                   a.storagecompanyname as StorageCompanyName,
                                   a.ReceivingCompanyCode as ReceivingCompanyCode, --收货单位编号
                                   a.receivingcompanyname as ReceivingCompanyName, --收货单位名称
                                   a.ReceivingWarehouseCode as ReceivingWarehouseCode, --收货仓库编号
                                   a.Receivingwarehousename as ReceivingWarehouseName, --收货仓库名称
                                   a.Outboundtype  as OutboundType,--出库类型
                                   pso.OriginalRequirementBillType as OriginalRequirementBillType, --原始需求单据类型
                                   a.OriginalRequirementBillCode as OriginalRequirementBillCode, --原始需求单据编号
                                   a.SettlementStatus as SettlementStatus, --结算状态
                                   b.warehouseareacode as WarehouseAreaCode,
                                   a.createtime as OutboundTime,
                                   b.SparePartId   as PartId,
                                   b.SparePartCode as PartCode,
                                   b.sparepartname as PartName,
                                   b.OutboundAmount as OutboundAmount, --出库数量,
                                   b.SettlementPrice as SettlementPrice,--代理库批发价
                                  (b.OutboundAmount*b.SettlementPrice) as Price,
                                   a.partssalescategoryid as BrandId,
                                   psc.name as BrandName,
                                   c.provincename as ProvinceName,
                                   su.businesscode as Businessode
                              from partsoutboundbill a
                              left join partssalesorder pso on pso.code = a.originalrequirementbillcode
                              left join SalesUnitAffiWarehouse suaw on suaw.warehouseid = a.warehouseid --销售组织与仓库
                              left join SalesUnit su on suaw.salesunitid = su.id --销售组织
                              left join partssalescategory psc on a.partssalescategoryid = psc.id
                              left join company c on a.StorageCompanyCode = c.code
                             inner join partsoutboundbilldetail b
                                on a.id = b.partsoutboundbillid
                             where a.StorageCompanytype in( 3,7)";
            var search = ObjectContext.ExecuteStoreQuery<VirtualAgentsHistoryOut>(SQL);
            return search;
        }
        public IQueryable<VirtualAgentsHistoryOut> 代理库出库记录查询Linq() {
            return from a in ObjectContext.PartsOutboundBills.Where(r => r.StorageCompanyType == 3 || r.StorageCompanyType == 7)
                   join partssalesorder in ObjectContext.PartsSalesOrders on a.OriginalRequirementBillCode equals partssalesorder.Code into tempTable5
                   from pso in tempTable5.DefaultIfEmpty()
                   join sw in ObjectContext.SalesUnitAffiWarehouses on a.WarehouseId equals sw.WarehouseId into tempTable1
                   from suaw in tempTable1.DefaultIfEmpty()
                   join salesUnit in ObjectContext.SalesUnits on suaw.SalesUnitId equals salesUnit.Id into tempTable2
                   from su in tempTable2.DefaultIfEmpty()
                   join partssalescategory in ObjectContext.PartsSalesCategories on a.PartsSalesCategoryId equals partssalescategory.Id into tempTable3
                   from psc in tempTable3.DefaultIfEmpty()
                   join company in ObjectContext.Companies on a.StorageCompanyCode equals company.Code into tempTable4
                   from c in tempTable4.DefaultIfEmpty()
                   join b in ObjectContext.PartsOutboundBillDetails on a.Id equals b.PartsOutboundBillId
                   orderby b.SparePartId
                   select new VirtualAgentsHistoryOut {
                       BranchId = a.BranchId,
                       BranchCode = a.BranchCode,
                       BranchName = a.BranchName,
                       OutboundCode = a.Code,
                       WarehouseId = a.WarehouseId,
                       WarehouseCode = a.WarehouseCode,
                       WarehouseName = a.WarehouseName,
                       StorageCompanyCode = a.StorageCompanyCode,
                       StorageCompanyName = a.StorageCompanyName,
                       ReceivingCompanyCode = a.ReceivingCompanyCode,
                       ReceivingCompanyName = a.ReceivingCompanyName,
                       ReceivingWarehouseCode = a.ReceivingWarehouseCode,
                       ReceivingWarehouseName = a.ReceivingWarehouseName,
                       OutboundType = a.OutboundType,
                       OriginalRequirementBillType = pso.OriginalRequirementBillType,
                       OriginalRequirementBillCode = a.OriginalRequirementBillCode,
                       SettlementStatus = a.SettlementStatus,
                       WarehouseAreaCode = b.WarehouseAreaCode,
                       OutboundTime = a.CreateTime,
                       PartId = b.SparePartId,
                       PartCode = b.SparePartCode,
                       PartName = b.SparePartName,
                       OutboundAmount = b.OutboundAmount,
                       SettlementPrice = b.SettlementPrice,
                       Price = (b.OutboundAmount * b.SettlementPrice),
                       BrandId = a.PartsSalesCategoryId,
                       BrandName = psc.Name,
                       ProvinceName = c.ProvinceName,
                       Businessode = su.BusinessCode
                   };
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:32
        //原分布类的函数全部转移到Ach                                                               
        
        public IEnumerable<VirtualAgentsHistoryOut> 代理库出库记录查询() {
            return new VirtualAgentsHistoryOutAch(this).代理库出库记录查询();
        }

        public IQueryable<VirtualAgentsHistoryOut> 代理库出库记录查询Linq() {
            return new VirtualAgentsHistoryOutAch(this).代理库出库记录查询Linq();
        }
    }
}
