﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class VirtualInboundHistoryQueryAch : DcsSerivceAchieveBase {
        public IEnumerable<VirtualInboundHistoryQuery> 历史入库记录查询() {
            

                #region
                //                const string SQL = @" select a.branchid as BranchId, --分公司ID
                //a.branchcode as BranchCode,--分公司编号
                //ppot.name as PartsPurchaseOrderTypeName,--采购订单类型
                //a.code as PartsInboundOrderCode, --入库单编号
                //a.inboundtype as InboundType,--入库类型
                //a.OriginalRequirementBillCode as OriginalRequirementBillCode,--源单据编号
                //c.Code as WarehouseCode, --仓库编号
                //c.Name as WarehouseName, --仓库名称
                //c.id as WarehouseId, --仓库ID
                //nvl(e.code, g.code) as Code,
                //1 as kind,'采购入库' as KINDNAME,
                //a.CounterpartCompanyCode as CompanyCode,--对方单位编号
                //a.CounterpartCompanyName as CompanyName,--对方单位名称
                //a.CreateTime as Createtime,--创建时间
                //f.code as PartCode,--配件编号
                //f.name as PartName,--配件名称
                // sum(b.InspectedQuantity) as InQty,--入库数量
                //  b.SettlementPrice as Price,
                //  sum(b.InspectedQuantity * b.Settlementprice) as Total,
                //  sum(b.inspectedquantity * b.CostPrice) as TotalJH,
                //case when a.settlementstatus = 1 then 1 when a.settlementstatus = 2 then 2 when jsd.status = 1 then 3 when jsd.status = 2 then 4 
                //          when jsd.status = 3 then 5 when jsd.status = 5 then 6 when jsd.status = 6 then 7 end as settlementstatus, --结算状态
                //       case when a.settlementstatus = 1 then '不结算' when a.settlementstatus = 2 then '待结算' when jsd.status = 1 then '新建'
                //            when jsd.status = 2 then '已审批' when jsd.status = 3 then '发票登记' when jsd.status = 5 then '发票驳回' 
                //            when jsd.status = 6 then '发票已审核' end as settlementstatusValue, a.PartsSalesCategoryId, psc.name as PartsSalesCategoryName,e.PartsPurchaseOrderTypeId,b.CostPrice,
                //              jsd.code as JsdNum
                //  from PartsInboundCheckBill a
                //  left join (
                //       select ppsb.status,ppsr.sourcecode,ppsb.code from PartsPurchaseSettleRef ppsr 
                //              left join PartsPurchaseSettleBill ppsb on ppsr.PartsPurchaseSettleBillId = ppsb.id
                //              where ppsb.status in (1,2,3,5,6)
                //  ) jsd on jsd.sourcecode = a.code
                // inner join partsinboundcheckbilldetail b on a.id =
                //                                                 b.partsinboundcheckbillid
                // inner join warehouse c on c.id = a.warehouseid
                // inner join partsinboundplan d on d.id = a.partsinboundplanid
                // inner join sparepart f on f.id = b.sparepartid
                //  left join PartsPurchaseOrder e on e.code =
                //                                        d.OriginalRequirementBillCode
                //  left join PartsPurchaseOrderType ppot on e.PartsPurchaseOrderTypeId = ppot.id
                //  left join partssalesorder g on g.code = d.OriginalRequirementBillCode
                //  left join PartsSalesCategory psc on a.PartsSalesCategoryId = psc.id
                // where a.inboundtype = 1
                //   and a.storagecompanyid in (2,2203,8481, 11181,12261)
                // group by a.branchid,a.branchcode,a.code,a.inboundtype,a.OriginalRequirementBillCode,c.code, c.name, c.id, nvl(e.code, g.code), a.counterpartcompanycode, a.counterpartcompanyname, a.createtime,
                // f.code, f.name,b.settlementprice,ppot.name,a.settlementstatus,a.PartsSalesCategoryId,psc.name,e.PartsPurchaseOrderTypeId,b.CostPrice,jsd.status,jsd.code
                // 
                //union all
                //select a.branchid,a.branchcode,null as PartsPurchaseOrderTypeName, a.code as InCode,a.inboundtype,a.OriginalRequirementBillCode,c.Code WarehouseCode, c.Name WarehouseName, c.id as WarehouseId, e.code as Code,
                //2 as kind,'销售退货入库' as KINDNAME,a.CounterpartCompanyCode as CompanyCode,a.CounterpartCompanyName as CompanyName, a.CreateTime,f.code as PartCode,f.name as PartName,
                //sum(b.InspectedQuantity) as InQty, b.SettlementPrice as Price, sum(b.InspectedQuantity * b.Settlementprice) as Total, sum(b.inspectedquantity * b.CostPrice) as TotalJH,
                //        case when a.settlementstatus = 1 then 1 when a.settlementstatus = 2 then 2 when jsd.status = 1 then 3 when jsd.status = 2 then 4 
                //          when jsd.status = 3 then 5 when jsd.status = 5 then 6 when jsd.status = 6 then 7 end as settlementstatus, 
                //       case when a.settlementstatus = 1 then '不结算' when a.settlementstatus = 2 then '待结算' when jsd.status = 1 then '新建'
                //            when jsd.status = 2 then '已审批' when jsd.status = 3 then '发票登记' when jsd.status = 5 then '发票驳回' 
                //            when jsd.status = 6 then '发票已审核' end as settlementstatusValue,  a.PartsSalesCategoryId,psc.name, null as PartsPurchaseOrderTypeId,b.CostPrice,jsd.code as 结算单号
                //  from PartsInboundCheckBill a
                //  left join (
                //       select psrs.status,psrsr.sourcecode,psrs.code from PartsSalesRtnSettlement psrs 
                //              left join PartsSalesRtnSettlementRef psrsr on psrs.id = psrsr.partssalesrtnsettlementid
                //              where psrs.status in (1,2,3,5,6)
                //       union all
                //       select pss.status,pssr.sourcecode,pss.code from PartsSalesSettlement pss
                //              left join PartsSalesSettlementRef pssr on pss.id = pssr.partssalessettlementid
                //              where pss.status in (1,2,3,5,6)
                //  ) jsd on jsd.sourcecode = a.code
                // inner join partsinboundcheckbilldetail b on a.id =
                //                                                 b.partsinboundcheckbillid
                // inner join warehouse c on c.id = a.warehouseid
                // inner join partsinboundplan d on d.id = a.partsinboundplanid
                // inner join sparepart f on f.id = b.sparepartid
                // inner join PartsSalesReturnBill e on e.code =
                //                                          d.originalrequirementbillcode
                // left join PartsSalesCategory psc on a.PartsSalesCategoryId = psc.id
                // where a.inboundtype = 2
                //    and a.storagecompanyid in (2,2203,8481, 11181,12261)
                // group by a.branchid, a.branchcode,a.code,a.inboundtype,a.OriginalRequirementBillCode, c.code, c.name, c.id,e.code,a.counterpartcompanycode,a.counterpartcompanyname, a.createtime, f.code,f.name,
                //b.settlementprice,a.settlementstatus,a.PartsSalesCategoryId,psc.name,b.CostPrice,jsd.status,jsd.code  
                //union all
                //select a.branchid,a.branchcode,null as PartsPurchaseOrderTypeName, a.code as InCode,a.inboundtype,a.OriginalRequirementBillCode,c.Code WarehouseCode,c.Name WarehouseName, c.id as WarehouseId, e.code as Code,
                //3 as kind,'配件调拨' as KINDNAME,a.CounterpartCompanyCode as CompanyCode,a.CounterpartCompanyName as CompanyName,a.CreateTime, f.code as PartCode,f.name as PartName,
                // sum(b.InspectedQuantity) as InQty, b.SettlementPrice as Price,sum(b.InspectedQuantity * b.Settlementprice) as Total, sum(b.inspectedquantity * b.CostPrice) as TotalJH,
                //       case when a.settlementstatus = 1 then 1 when a.settlementstatus = 2 then 2  end as settlementstatus, 
                //       case when a.settlementstatus = 1 then '不结算' when a.settlementstatus = 2 then '待结算'  end as settlementstatusValue,  a.PartsSalesCategoryId, psc.name, null as PartsPurchaseOrderTypeId, b.CostPrice
                //          ,null as 结算单号
                //  from PartsInboundCheckBill a
                //  inner join partsinboundcheckbilldetail b on a.id =
                //                                                 b.partsinboundcheckbillid
                // inner join warehouse c on c.id = a.warehouseid
                // inner join partsinboundplan d on d.id = a.partsinboundplanid
                // inner join sparepart f on f.id = b.sparepartid
                // inner join PartsTransferOrder e on e.id = d.OriginalRequirementBillId
                // left join PartsSalesCategory psc on a.PartsSalesCategoryId = psc.id
                // where a.inboundtype = 3
                //   and a.storagecompanyid in (2,2203,8481, 11181,12261)
                // group by a.branchid,a.branchcode, a.code,a.inboundtype,a.OriginalRequirementBillCode,  c.code, c.name,c.id,e.code, a.counterpartcompanycode,a.counterpartcompanyname, a.createtime, f.code,f.name,
                // b.settlementprice,a.settlementstatus,a.PartsSalesCategoryId,psc.name,b.CostPrice
                // 
                //union all
                //select a.branchid,a.branchcode,null as 采购订单类型,a.code as InCode,a.inboundtype,a.OriginalRequirementBillCode, c.Code WarehouseCode,c.Name WarehouseName, c.id as WarehouseId,e.code as Code,
                //4 as kind,'内部领入' as KINDNAME,a.CounterpartCompanyCode as CompanyCode, a.CounterpartCompanyName as CompanyName,a.CreateTime,f.code as PartCode,f.name as PartName,
                // sum(b.InspectedQuantity) as InQty, b.SettlementPrice as Price,sum(b.InspectedQuantity * b.Settlementprice) as Total,sum(b.inspectedquantity * b.CostPrice) as TotalJH,
                //        case when a.settlementstatus = 1 then 1 when a.settlementstatus = 2 then 2 when jsd.status = 1 then 3 when jsd.status = 2 then 4 
                //          when jsd.status = 3 then 5 when jsd.status = 5 then 6 when jsd.status = 6 then 7 end as settlementstatus, 
                //       case when a.settlementstatus = 1 then '不结算' when a.settlementstatus = 2 then '待结算' when jsd.status = 1 then '新建'
                //            when jsd.status = 2 then '已审批' when jsd.status = 3 then '发票登记' when jsd.status = 5 then '发票驳回' 
                //            when jsd.status = 6 then '发票已审核' end as settlementstatusValue,  a.PartsSalesCategoryId,psc.name,null as PartsPurchaseOrderTypeId,b.CostPrice,jsd.code as 结算单号
                //  from PartsInboundCheckBill a
                //     left join (
                //       select prsb.status,prsr.sourcecode,prsb.code from PartsRequisitionSettleBill prsb 
                //              left join PartsRequisitionSettleRef prsr on prsb.id = prsr.partsrequisitionsettlebillid
                //              where prsb.status in (1,2,3,5,6)
                //  ) jsd on jsd.sourcecode = a.code
                // inner join partsinboundcheckbilldetail b on a.id =
                //                                                 b.partsinboundcheckbillid
                // inner join warehouse c on c.id = a.warehouseid
                // inner join partsinboundplan d on d.id = a.partsinboundplanid
                // inner join sparepart f on f.id = b.sparepartid
                // inner join InternalAcquisitionBill e on e.id =
                //                                             d.OriginalRequirementBillId
                // left join PartsSalesCategory psc on a.PartsSalesCategoryId = psc.id
                // where a.inboundtype = 4
                //    and a.storagecompanyid in (2,2203,8481, 11181,12261)
                // group by a.branchid,a.branchcode, a.code,a.inboundtype,a.OriginalRequirementBillCode,c.code, c.name, c.id, e.code, a.counterpartcompanycode,a.counterpartcompanyname,a.createtime,f.code,f.name,
                //b.settlementprice,a.settlementstatus,a.PartsSalesCategoryId,psc.name,b.CostPrice,  jsd.status,jsd.code
                //union all
                //select a.branchid,a.branchcode,null as PartsPurchaseOrderTypeName,a.code as InCode,a.inboundtype,a.OriginalRequirementBillCode,c.Code WarehouseCode, c.Name WarehouseName, c.id as WarehouseId, e.code as Code,
                //5 as kind, '配件零售退货入库' as KINDNAME,a.CounterpartCompanyCode as CompanyCode, a.CounterpartCompanyName as CompanyName,a.CreateTime, f.code as PartCode, f.name as PartName,
                //sum(b.InspectedQuantity) as InQty,b.SettlementPrice as Price, sum(b.InspectedQuantity * b.Settlementprice) as Total,sum(b.inspectedquantity * b.CostPrice) as TotalJH,
                //        case when a.settlementstatus = 1 then 1 when a.settlementstatus = 2 then 2 when jsd.status = 1 then 3 when jsd.status = 2 then 4 
                //          when jsd.status = 3 then 5 when jsd.status = 5 then 6 when jsd.status = 6 then 7 end as settlementstatus, 
                //       case when a.settlementstatus = 1 then '不结算' when a.settlementstatus = 2 then '待结算' when jsd.status = 1 then '新建'
                //            when jsd.status = 2 then '已审批' when jsd.status = 3 then '发票登记' when jsd.status = 5 then '发票驳回' 
                //            when jsd.status = 6 then '发票已审核' end as settlementstatusValue, a.PartsSalesCategoryId,psc.name, null as PartsPurchaseOrderTypeId, b.CostPrice,jsd.code as 结算单号
                //  from PartsInboundCheckBill a
                //      left join (
                //       select psrs.status,psrsr.sourcecode,psrs.code from PartsSalesRtnSettlement psrs 
                //              left join PartsSalesRtnSettlementRef psrsr on psrs.id = psrsr.partssalesrtnsettlementid
                //              where psrs.status in (1,2,3,5,6)
                //       union all
                //       select pss.status,pssr.sourcecode,pss.code from PartsSalesSettlement pss
                //              left join PartsSalesSettlementRef pssr on pss.id = pssr.partssalessettlementid
                //              where pss.status in (1,2,3,5,6)
                //  ) jsd on jsd.sourcecode = a.code
                // inner join partsinboundcheckbilldetail b on a.id =
                //                                                 b.partsinboundcheckbillid
                // inner join warehouse c on c.id = a.warehouseid
                // inner join partsinboundplan d on d.id = a.partsinboundplanid
                // inner join sparepart f on f.id = b.sparepartid
                // inner join PartsRetailReturnBill e on e.id =
                //                                           d.OriginalRequirementBillId
                // left join PartsSalesCategory psc on a.PartsSalesCategoryId = psc.id
                // where a.inboundtype = 5
                //   and a.storagecompanyid in (2,2203,8481, 11181,12261)
                // group by a.branchid,a.branchcode,a.code,a.inboundtype,a.OriginalRequirementBillCode,c.code, c.name,c.id, e.code, a.counterpartcompanycode, a.counterpartcompanyname,a.createtime, f.code, f.name,
                // b.settlementprice, a.settlementstatus, a.PartsSalesCategoryId, psc.name, b.CostPrice ,jsd.status,jsd.code
                //union all
                //select a.branchid,a.branchcode,null as PartsPurchaseOrderTypeName,a.code as InCode,a.inboundtype,a.OriginalRequirementBillCode, c.Code WarehouseCode, c.Name WarehouseName, c.id as WarehouseId, e.code as Code, 6 as kind,
                //'积压件调剂入库' as KINDNAME,a.CounterpartCompanyCode as CompanyCode,a.CounterpartCompanyName as CompanyName,a.CreateTime, f.code as PartCode, f.name as PartName,
                // sum(b.InspectedQuantity) as InQty, b.SettlementPrice as Price,sum(b.InspectedQuantity * b.Settlementprice) as Total, sum(b.inspectedquantity * b.CostPrice) as TotalJH,
                //      case when a.settlementstatus = 1 then 1 when a.settlementstatus = 2 then 2 when jsd.status = 1 then 3 when jsd.status = 2 then 4 
                //          when jsd.status = 3 then 5 when jsd.status = 5 then 6 when jsd.status = 6 then 7 end as settlementstatus, 
                //       case when a.settlementstatus = 1 then '不结算' when a.settlementstatus = 2 then '待结算' when jsd.status = 1 then '新建'
                //            when jsd.status = 2 then '已审批' when jsd.status = 3 then '发票登记' when jsd.status = 5 then '发票驳回' 
                //            when jsd.status = 6 then '发票已审核' end as settlementstatusValue,  a.PartsSalesCategoryId, psc.name, null as PartsPurchaseOrderTypeId, b.CostPrice,jsd.code as 结算单号
                //  from PartsInboundCheckBill a
                //      left join (
                //      select psrs.status,psrsr.sourcecode,psrs.code from PartsSalesRtnSettlement psrs 
                //              left join PartsSalesRtnSettlementRef psrsr on psrs.id = psrsr.partssalesrtnsettlementid
                //              where psrs.status in (1,2,3,5,6)
                //       union all
                //       select pss.status,pssr.sourcecode,pss.code from PartsSalesSettlement pss
                //              left join PartsSalesSettlementRef pssr on pss.id = pssr.partssalessettlementid
                //              where pss.status in (1,2,3,5,6)
                //  ) jsd on jsd.sourcecode = a.code
                // inner join partsinboundcheckbilldetail b on a.id =
                //                                                 b.partsinboundcheckbillid
                // inner join warehouse c on c.id = a.warehouseid
                // inner join partsinboundplan d on d.id = a.partsinboundplanid
                // inner join sparepart f on f.id = b.sparepartid
                // inner join OverstockPartsAdjustBill e on e.id =
                //                                              d.OriginalRequirementBillId
                // left join PartsSalesCategory psc on a.PartsSalesCategoryId = psc.id
                // where a.inboundtype = 6
                //     and a.storagecompanyid in (2,2203,8481, 11181,12261)
                // group by a.branchid,a.branchcode,a.code,a.inboundtype,a.OriginalRequirementBillCode,c.code,c.name,c.id,e.code,a.counterpartcompanycode,a.counterpartcompanyname, a.createtime, f.code,f.name,
                //b.settlementprice,a.settlementstatus,a.PartsSalesCategoryId,psc.name,b.CostPrice  ,jsd.status,jsd.code
                //union all
                //select a.branchid,a.branchcode,null as PartsPurchaseOrderTypeName, a.code as InCode,a.inboundtype,a.OriginalRequirementBillCode,c.Code WarehouseCode, c.Name WarehouseName, c.id as WarehouseId,e.code as Code,
                // 7 as kind,'配件外采入库' as KINDNAME,a.CounterpartCompanyCode as CompanyCode, a.CounterpartCompanyName as CompanyName, a.CreateTime, f.code as PartCode, f.name as PartName,
                // sum(b.InspectedQuantity) as InQty, b.SettlementPrice as Price, sum(b.InspectedQuantity * b.Settlementprice) as Total,sum(b.inspectedquantity * b.CostPrice) as TotalJH,
                //        case when a.settlementstatus = 1 then 1 when a.settlementstatus = 2 then 2  end as settlementstatus, 
                //       case when a.settlementstatus = 1 then '不结算' when a.settlementstatus = 2 then '待结算'  end as settlementstatusValue, a.PartsSalesCategoryId,psc.name, null as PartsPurchaseOrderTypeId, b.CostPrice
                //       ,null as 结算单号
                //        from PartsInboundCheckBill a
                //  inner join partsinboundcheckbilldetail b on a.id =
                //                                                 b.partsinboundcheckbillid
                // inner join warehouse c on c.id = a.warehouseid
                // inner join partsinboundplan d on d.id = a.partsinboundplanid
                // inner join sparepart f on f.id = b.sparepartid
                // inner join PartsOuterPurchaseChange e on e.id =
                //                                              d.OriginalRequirementBillId
                // left join PartsSalesCategory psc on a.PartsSalesCategoryId = psc.id
                // where a.inboundtype = 7
                //   and a.storagecompanyid in (2,2203,8481, 11181,12261)
                // group by a.branchid,a.branchcode, a.code,a.inboundtype,a.OriginalRequirementBillCode, c.code,c.name, c.id, e.code,a.counterpartcompanycode,a.counterpartcompanyname,a.createtime,f.code, f.name,
                // b.settlementprice, a.settlementstatus,a.PartsSalesCategoryId,psc.name,b.CostPrice
                // ";
                #endregion

                const string SQL = @"select 
                                     a.branchid as BranchId, --分公司ID
                                     a.branchcode as BranchCode,--分公司编号
                                     ppot.name as PartsPurchaseOrderTypeName,--采购订单类型
                                     a.code as PartsInboundOrderCode, --入库单编号
                                     a.inboundtype as InboundType,--入库类型
                                     a.OriginalRequirementBillCode as OriginalRequirementBillCode,--源单据编号
                                     c.Code as WarehouseCode, --仓库编号
                                     c.Name as WarehouseName, --仓库名称
                                     c.id as WarehouseId, --仓库ID
                                     nvl(e.code, g.code) as Code,
                                     a.inboundtype as KIND,
                                      keyvalue.value as KINDNAME,
                                     a.CounterpartCompanyCode as CompanyCode,--对方单位编号
                                     a.CounterpartCompanyName as CompanyName,--对方单位名称
                                     a.CreateTime as Createtime,--创建时间
                                     f.code as PartCode,--配件编号
                                     f.name as PartName,--配件名称
                                     sum(b.InspectedQuantity) as InQty,--入库数量
                                     b.SettlementPrice as Price,
                                     sum(b.InspectedQuantity * b.Settlementprice) as Total,
                                     sum(b.inspectedquantity * b.CostPrice) as TotalJH,
                                     case when a.settlementstatus = 1 then 1 when a.settlementstatus = 2 then 2 when jsd.status = 1 then 3 when jsd.status = 2 then 4 
                                          when jsd.status = 3 then 5 when jsd.status = 5 then 6 when jsd.status = 6 then 7 end as settlementstatus, --结算状态
                                     case when a.settlementstatus = 1 then '不结算' when a.settlementstatus = 2 then '待结算' when jsd.status = 1 then '新建'
                                          when jsd.status = 2 then '已审批' when jsd.status = 3 then '发票登记' when jsd.status = 5 then '发票驳回' 
                                          when jsd.status = 6 then '发票已审核' end as SETTLEMENTSTATUSVALUE, a.PartsSalesCategoryId, psc.name as PartsSalesCategoryName,e.PartsPurchaseOrderTypeId,b.CostPrice,
                                     jsd.code as JsdNum
                                     from PartsInboundCheckBill a
                                     left join (
                                            select ppsb.status,ppsr.sourcecode,ppsb.code from PartsPurchaseSettleRef ppsr 
                                                   left join PartsPurchaseSettleBill ppsb on ppsr.PartsPurchaseSettleBillId = ppsb.id
                                                   where ppsb.status in (1,2,3,5,6)
                                       ) jsd on jsd.sourcecode = a.code
                                      inner join partsinboundcheckbilldetail b on a.id = b.partsinboundcheckbillid
                                      inner join warehouse c on c.id = a.warehouseid
                                      inner join partsinboundplan d on d.id = a.partsinboundplanid
                                      inner join sparepart f on f.id = b.sparepartid
                                      inner join keyvalueitem keyvalue on keyvalue.name='Parts_InboundType' and keyvalue.key = a.inboundtype
                                       left join PartsPurchaseOrder e on e.code =d.OriginalRequirementBillCode
                                       left join PartsPurchaseOrderType ppot on e.PartsPurchaseOrderTypeId = ppot.id
                                       left join partssalesorder g on g.code = d.OriginalRequirementBillCode
                                       left join PartsSalesCategory psc on a.PartsSalesCategoryId = psc.id
                                      where  a.storagecompanyid in (2,2203,8481, 11181,12261)
                                      group by a.branchid,a.branchcode,a.code,a.inboundtype,a.OriginalRequirementBillCode,c.code, c.name, c.id, nvl(e.code, g.code),  keyvalue.value, a.counterpartcompanycode, a.counterpartcompanyname, a.createtime,
                                      f.code, f.name,b.settlementprice,ppot.name,a.settlementstatus,a.PartsSalesCategoryId,psc.name,e.PartsPurchaseOrderTypeId,b.CostPrice,jsd.status,jsd.code";
                var search = ObjectContext.ExecuteStoreQuery<VirtualInboundHistoryQuery>(SQL);
                return search;
 
        }
    
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:32
        //原分布类的函数全部转移到Ach                                                               
        

        public IEnumerable<VirtualInboundHistoryQuery> 历史入库记录查询() {
            return new VirtualInboundHistoryQueryAch(this).历史入库记录查询();
        }
    }
}
