﻿using System.Collections.Generic;
using System.Linq;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class AgencyStockQueryAch : DcsSerivceAchieveBase {
        public AgencyStockQueryAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public IEnumerable<AgencyStockQuery> 代理库库存查询() {
            const string SQL = @"select a.BranchId as BranchId,  --分公司id
                        bch.code as BranchCode,  --分公司名称
                        a.Code as AgencyCode,  --代理库编号
                        a.Name as AgencyName,  --代理库名称
                        a.Id as WarehouseId,   --仓库id
                        a.WarehouseName as WarehouseName, --仓库名称
                        a.SpareId as PartId,    --配件id
                        a.SpareCode as PartCode,  --配件编号
                        a.SpareName as PartName,  --配件名称
                        a.Qty as Qty,--库存
                        a.qty - nvl(c.lockedqty, 0) as UsableQty, --可用库存
                        nvl(b.salesprice, 0) as Saleprice, --批发价
                        a.qty * nvl(b.salesprice, 0) as Totalprice, --批发金额
                        a.partssalescategoryid as PartsSalesCategoryId,--配件销售类型id
                        a.partssalescategoryname as PartsSalesCategoryName,--配件销售类型i名称
                        a.value as PartsAttribution --配件所属分类
                   from (select f.branchid,
                                d.code,
                                d.name,
                                b.id,
                                b.name warehousename,
                                c.id spareid,
                                c.code sparecode,
                                c.name sparename,
                                sum(a.quantity) qty,
                                f.partssalescategoryid,
                                psc.name as partssalescategoryname,kvi.value
                           from partsstock a
                          inner join warehouse b
                             on a.warehouseid = b.id
                          inner join sparepart c
                             on c.id = a.partid
                          inner join company d
                             on d.id = a.storagecompanyid
                          inner join salesunitaffiwarehouse e
                             on e.warehouseid = b.id
                          inner join salesunit f
                             on f.id = e.salesunitid
                          left join partssalescategory psc on f.partssalescategoryid = psc.id
                          left join partsbranch psb on psb.partssalescategoryid = f.partssalescategoryid and psb.partid = c.id and psb.status=1
                           left join keyvalueitem kvi on kvi.status = 1 and kvi.name = 'PartsAttribution' and kvi.key = psb.PartsAttribution
                          where a.storagecompanytype in (3, 7)
                          group by f.branchid,
                                   d.code,
                                   d.name,
                                   b.id,
                                   b.name,
                                   c.id,
                                   c.code,
                                   c.name,
                                   f.partssalescategoryid,
                                   psc.name,kvi.value) a
                   inner join branch bch
                     on bch.id=a.branchid
                   left join partssalesprice b
                     on a.spareid = b.sparepartid
                    and a.partssalescategoryid = b.partssalescategoryid
                   left join (select WarehouseId, PartId, sum(LockedQuantity) as lockedqty
                                from PartsLockedStock
                               group by WarehouseId, PartId) c
                     on c.warehouseid = a.id
                    and c.PartId = a.spareid
                     where a.qty <> 0";
            var search = ObjectContext.ExecuteStoreQuery<AgencyStockQuery>(SQL).ToList();
            return search.OrderBy(r => r.PartId);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:32
        //原分布类的函数全部转移到Ach                                                               

        public IEnumerable<AgencyStockQuery> 代理库库存查询() {
            return new AgencyStockQueryAch(this).代理库库存查询();
        }
    }
}
