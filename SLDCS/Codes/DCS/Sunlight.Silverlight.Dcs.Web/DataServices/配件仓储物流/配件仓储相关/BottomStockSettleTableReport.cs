﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class BottomStockSettleTableReportAch : DcsSerivceAchieveBase {
        public BottomStockSettleTableReportAch(DcsDomainService domainService)
            : base(domainService) {
        }
        public IEnumerable<BottomStockSettleTableReport> 保底库存需提报计划明细表(int? type, string distributionCenterName, string centerName, string dealerName, bool? overZero) {
            string SQL = @"select Rownum,qq.* from (select tt.sparepartcode,
                           tt.sparepartid,
                           tt.sparepartname,
                           tt.DistributionCenterName,
                           tt.centerName,
                           tt.dealerName,
                           tt.companyid,
                           tt.dealerCode,
                           sum(tt.StockQty) as StockQty,
                           sum(tt.ActualAvailableStock) as ActualAvailableStock,
                           sum(ss.approvequantity) as onwayQty,
                          (case when ( sum(tt.StockQty) - nvl(sum(tt.ActualAvailableStock), 0) -
                           nvl(sum(ss.approvequantity), 0))>0 then ( sum(tt.StockQty) - nvl(sum(tt.ActualAvailableStock), 0) -
                           nvl(sum(ss.approvequantity), 0)) else 0 end) as DifferenceQty,
                           (case when ( sum(tt.StockQty) - nvl(sum(tt.ActualAvailableStock), 0) -
                           nvl(sum(ss.approvequantity), 0))>0 then ( sum(tt.StockQty) - nvl(sum(tt.ActualAvailableStock), 0) -
                           nvl(sum(ss.approvequantity), 0)) else 0 end) *sum(price) as DifferenceMoney
                      from (select bs.sparepartcode,
                                   bs.sparepartid,
                                   bs.sparepartname,
                                   bs.companyid,
                                   bs.ReserveQty as StockQty,
                                     (case
                                     when cp.type = 3 then
                                      psp.centerprice
                                     else
                                      psp.SalesPrice
                                   end) as price,
                                   (case
                                     when cp.type = 2 then
                                      dps.Quantity
                                     else
                                      tt.ActualAvailableStock
                                   end) as ActualAvailableStock, --实际
                                   (case
                                     when cp.type = 3 then
                                      (select mt.Name
                                         from MarketingDepartment mt
                                         join Agency ag
                                           on mt.id = ag.marketingdepartmentid
                                        where ag.id = bs.companyid)
                                     else
                                      (select mt.Name
                                         from MarketingDepartment mt
                                         join DealerServiceInfo ag
                                           on mt.id = ag.marketingdepartmentid
                                        where ag.DealerId = bs.companyid)
                                   end) as DistributionCenterName,
                                   (case
                                     when cp.type = 3 then
                                       cp.name
                                     else
                                     (select ag.name
                                         from AgencyDealerRelation ar join company ag on ar.AgencyId=ag.id
                                        where ar.dealerid = bs.companyid
                                          and ar.status = 1
                                          and rownum = 1) 
                                   end) as centerName,
                                   (case
                                     when cp.type = 3 then
                                        cast('' as varchar2(20))
                                     else
                                     cp.name
                                   end) as dealerName,
                                   (case
                                     when cp.type = 3 then
                                       cast('' as varchar2(20))
                                     else
                                      bs.companyCode
                                   end) as dealerCode
        
                              from BottomStockColVersion bs join company cp
                             on bs.companyid = cp.id
                              left join PartsSalesPrice psp
                                on bs.sparepartid = psp.sparepartid
                               and psp.status = 1
                                 left join PartsSalesPrice psp
                                on bs.sparepartid = psp.sparepartid
                               and psp.status = 1
                              left join (select aa.partid,
                                               aa.StorageCompanyId,
                                               (aa.actualqty - nvl(bb.orderlockqty, 0)) as ActualAvailableStock
                                          from (select sum(quantity) as actualqty,
                                                       tmp.partid,
                                                       tmp.StorageCompanyId
                                                  from partsstock tmp
                                                 inner join warehouseareacategory b
                                                    on tmp.warehouseareacategoryid = b.id
                                                 where b.category = 1
                                                 group by tmp.partid, tmp.StorageCompanyId) aa
                                          left join (select sum(lockedquantity) as orderlockqty,
                                                           tmp.StorageCompanyId,
                                                           tmp.partid
                                                      from partslockedstock tmp
                                                     group by tmp.StorageCompanyId, tmp.partid) bb
                                            on aa.partid = bb.partid
                                           and aa.StorageCompanyId = bb.StorageCompanyId) tt
                                on bs.sparepartid = tt.partid
                               and bs.CompanyId = tt.StorageCompanyId
                              left join DealerPartsStock dps
                                on bs.sparepartid = dps.sparepartid
                               and bs.companyid = dps.dealerid
                               and bs.companytype = 2
                             where  bs.status = 1
                              {0} ) tt  
                      left join ((select p.sparepartid,
                                        po.submitcompanyid,
                                        sum((case
                                              when po.status in (2,4) then
                                               p.orderedquantity
                                              when po.status  =5 then
                                               p.approvequantity
                                              else
                                               detail.OutboundFulfillment
                                            end) - nvl(tt.inspectedquantity, 0)) as approvequantity
                                   from partssalesorder po
                                   join partssalesorderdetail p
                                     on p.partssalesorderid = po.id
                                   left join (select plans.originalrequirementbillid,
                                                    detail.sparepartid,
                                                    sum(detail.OutboundFulfillment) as OutboundFulfillment
                                               from PartsOutboundPlan plans
                                               join PartsOutboundPlanDetail detail
                                                 on plans.id = detail.partsoutboundplanid
                                              where plans.originalrequirementbilltype = 1
                                              group by plans.originalrequirementbillid,
                                                       detail.sparepartid) detail
                                     on po.id = detail.originalrequirementbillid
                                    and p.sparepartid = detail.sparepartid
                                   join company cp
                                     on po.SubmitCompanyId = cp.id
                                   left join (select pc.originalrequirementbillid,
                                                    pcd.sparepartid,
                                                    sum(pcd.inspectedquantity) as inspectedquantity
                                               from partsinboundcheckbill pc
                                               join partsinboundcheckbilldetail pcd
                                                 on pc.id = pcd.partsinboundcheckbillid
                                              where pc.originalrequirementbilltype = 1
                                              group by pc.originalrequirementbillid,
                                                       pcd.sparepartid) tt
                                     on po.id = tt.originalrequirementbillid
                                    and p.sparepartid = tt.sparepartid
                                  where po.status in (2, 4, 5)
                                    and cp.type = 3
                                    and po.createtime >= to_date('2018-10-08', 'yyyy-mm-dd')
                                and not exists (select 1
                                          from partspurchaseorder pu
                                         where pu.originalrequirementbillid = po.id
                                           and pu.originalrequirementbillcode = po.code
                                           and pu.status = 7)
                                   and not exists (select 1
                                          from partspurchaseorder pu
                                          join partspurchaseorderdetail pud
                                            on pu.id = pud.partspurchaseorderid
                                         where pu.originalrequirementbillid = po.id
                                           and pu.originalrequirementbillcode = po.code
                                           and pud.sparepartid = p.sparepartid
                                           and pud.ConfirmedAmount = 0
                                           and pu.status in (4, 5, 6))
                                  group by p.sparepartid,
                                           po.submitcompanyid
                                           ) ) ss
                        on tt.companyid = ss.submitcompanyid
                       and tt.sparepartid = ss.sparepartid
                     group by tt.sparepartcode,
                              tt.sparepartid,
                              tt.sparepartname,
                              tt.DistributionCenterName,
                              tt.centerName,
                              tt.dealerName,
                              tt.dealerCode,
                              tt.companyid) qq where 1=1 {1}";
            var serachSQL = new StringBuilder();
            if(type.HasValue) {
                if(type == 3) {
                    serachSQL.Append(" and bs.companytype =7 ");
                }else if(type==1){
                    serachSQL.Append(" and bs.companytype =3 ");
                } else {
                    serachSQL.Append(" and bs.companytype =2 ");
                    if(!string.IsNullOrEmpty(centerName)&&!string.IsNullOrEmpty(distributionCenterName)) {
                        distributionCenterName = null;
                    }
                }
            }
           
            if(!string.IsNullOrEmpty(centerName)){
                serachSQL.Append(" and (case when cp.type = 3 then  cp.name  else  (select ag.name from AgencyDealerRelation ar join company ag on ar.AgencyId=ag.id  where ar.dealerid = bs.companyid and ar.status = 1 and rownum = 1)  end)  like '%" + centerName + "%'");
            }
            if(!string.IsNullOrEmpty(distributionCenterName)){
                serachSQL.Append(" and  (case  when cp.type = 3 then  (select mt.Name from MarketingDepartment mt join Agency ag on mt.id = ag.marketingdepartmentid  where ag.id = bs.companyid)  else  (select mt.Name from MarketingDepartment mt join DealerServiceInfo ag  on mt.id = ag.marketingdepartmentid   where ag.DealerId = bs.companyid) end) like'%" + distributionCenterName + "%'");
            }
            if(!string.IsNullOrEmpty(dealerName)) {
                serachSQL.Append(" and (case when cp.type = 3 then  cast('' as varchar2(20)) else  cp.name     end) like '%" + dealerName + "%'");
            }
            var userInfo = Utils.GetCurrentUserInfo();

            var company = ObjectContext.Companies.Where(r => r.Id == userInfo.EnterpriseId).FirstOrDefault();
            if(company.Type == (int)DcsCompanyType.分公司) {
                //判断是否维护了人员与分销中心关系
                var isHas = ObjectContext.MarketDptPersonnelRelations.Where(r => r.PersonnelId == userInfo.Id && r.Status == (int)DcsBaseDataStatus.有效).Count();
                if(isHas > 0) {
                    serachSQL.Append(" and  (exists(select 1 from  MarketDptPersonnelRelation mr join Agency ag on mr.marketdepartmentid = ag.marketingdepartmentid  where mr.PersonnelId = " + userInfo.Id + " and ag.id= bs.companyid and mr.status=1) or exists  (select 1 from MarketDptPersonnelRelation mr join Agency ag on mr.marketdepartmentid = ag.marketingdepartmentid join AgencyDealerRelation ar on ag.id = ar.agencyid   where mr.PersonnelId = "+userInfo.Id+"  and ar.dealerid = bs.companyid  and bs.companytype = 2 and mr.status = 1))");
                }
            }  
            if(company.Type == (int)DcsCompanyType.代理库) {
                serachSQL.Append(" and (case when bs.companytype = 2 then  (select AgencyId from AgencyDealerRelation ar  where ar.dealerid = bs.companyid and ar.status = 1 and rownum = 1) else  bs.CompanyID  end)  = " + userInfo.EnterpriseId);
            }
            if(company.Type == (int)DcsCompanyType.服务站 || company.Type == (int)DcsCompanyType.服务站兼代理库) {
                serachSQL.Append(" and bs.companyid=" + userInfo.EnterpriseId);
            }
            var serachSQL2 = new StringBuilder();
            if(overZero.HasValue && overZero.Value) {
                serachSQL2.Append(" and  qq.DifferenceQty>0 ");
            }
            SQL = string.Format(SQL, serachSQL, serachSQL2);
            var search = ObjectContext.ExecuteStoreQuery<BottomStockSettleTableReport>(SQL).ToArray();
            return search;
        }
        public IEnumerable<BottomStockSettleTableReport> 查询保底覆盖率报表(int? type, string distributionCenterName, string centerName, string dealerName, DateTime? bCreateTime, DateTime? eCreateTime) {
            int groupType = 3;
            if(type.HasValue) {
                if(type == 1) {
                    groupType = 1;
                } else if(type == 2) {
                    groupType = 2;
                } else groupType = 3;
            }                       
            if(eCreateTime.Value.Date > DateTime.Now.Date) {
                eCreateTime = DateTime.Now;
            }
            var countDay = 0;
            var day = eCreateTime - bCreateTime;
            if(eCreateTime.Value.Date == DateTime.Now.Date) {
                countDay = day.Value.Days;
            } else {
                countDay = day.Value.Days+1;
            }
             string SQL = @" select Rownum,tt.*,to_char({2},'yyyy') as year,to_char({2},'mm') as month, to_char({2},'iw')as week  from (select 
                            (case when {0}=2 then cast('' as varchar2(50))else s.DistributionCenterName end) as DistributionCenterName,
                            (case when {0}=1 then cast('' as varchar2(50))else center.Name end) as centername,
                            (case when {0}=3 then   dealer.Name else cast('' as varchar2(50)) end) as dealername,
                              round(sum(ActualSpeciesNum)/" + countDay + @") as ActualSpeciesNum,
                              round(sum(nvl(ACTUALFEE,0))/" + countDay + @")as ActualFee,
                                  round(sum(s.AccountSpeciesNum)/" + countDay + @") as AccountSpeciesNum,
                                  round((sum(AccountFee)/" + countDay + @"),2) as AccountFee,
                                  round(sum(DifferSpeciesNum)/" + countDay + @") as DifferSpeciesNum,
                                round((sum(ACCOUNTFEE)-sum(nvl(ACTUALFEE,0)))/" + countDay + @") as DifferenceMoney,
                                  round( (case
                                     when sum(AccountSpeciesNum) = 0 then
                                      0
                                     else
                                      (sum(ActualSpeciesNum)/" + countDay + @") / (sum(AccountSpeciesNum)/" + countDay + @")
                                   end)*100,2) as BottomCoverage
                              from BottomStockSettleTable s
                                 left join company center
                                on s.centercode = center.code
                               and center.status = 1
                              left join company dealer
                                on s.dealercode = dealer.code
                               and dealer.status = 1
                               where 1=1   {1}
                             group by   (case when {0}=2 then cast('' as varchar2(50))else s.DistributionCenterName end),
                            (case when {0}=1 then cast('' as varchar2(50))else center.Name end) ,
                            (case when {0}=3 then   dealer.Name else cast('' as varchar2(50)) end)  ) tt ";           
            var serachSQL = new StringBuilder();
            if(groupType == 3 || groupType == 1) {
                serachSQL.Append(" and s.DealerCode is not  null ");
            }
            if(groupType==2) {
                serachSQL.Append(" and s.DealerCode is   null ");
            }
            if(!string.IsNullOrEmpty(centerName)) {
                serachSQL.Append(" and center.Name  like '%" + centerName + "%'");
            }
            if(!string.IsNullOrEmpty(distributionCenterName)) {
                serachSQL.Append(" and s.DistributionCenterName like'%" + distributionCenterName + "%'");
            }
            if(!string.IsNullOrEmpty(dealerName)) {
                serachSQL.Append(" and dealer.Name like '%" + dealerName + "%'");
            }
            if(bCreateTime.HasValue){
                serachSQL.Append(" and s.CreateTime>= to_date('" + bCreateTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')");
            }
            if(eCreateTime.HasValue) {
                serachSQL.Append(" and s.CreateTime<= to_date('" + eCreateTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')");
            }
            var userInfo = Utils.GetCurrentUserInfo();

            var company = ObjectContext.Companies.Where(r => r.Id == userInfo.EnterpriseId).FirstOrDefault();
            if(company.Type == (int)DcsCompanyType.分公司) {
                //判断是否维护了人员与分销中心关系
                var isHas = ObjectContext.MarketDptPersonnelRelations.Where(r => r.PersonnelId == userInfo.Id && r.Status == (int)DcsBaseDataStatus.有效).Count();
                if(isHas > 0) {
                    serachSQL.Append(" and  exists(select 1 from  MarketDptPersonnelRelation mr join Agency ag on mr.marketdepartmentid = ag.marketingdepartmentid  where mr.PersonnelId = " + userInfo.Id + " and ag.Code= s.CenterCode and mr.status=1)");
                }
            }
            if(company.Type == (int)DcsCompanyType.代理库) {
                serachSQL.Append(" and ((select AgencyCode from AgencyDealerRelation ar  where ar.DealerCode = s.DealerCode and ar.status = 1 and rownum = 1)  = '" + company.Code + "' or s.centercode='" + company.Code+"')");
            }
            if(company.Type == (int)DcsCompanyType.服务站 || company.Type == (int)DcsCompanyType.服务站兼代理库) {
                serachSQL.Append(" and s.DealerCode='" + company.Code+"'");
            }
            var date="to_date('"+bCreateTime+"','yyyy-mm-dd hh24:mi:ss')";
            SQL = string.Format(SQL, groupType, serachSQL, date);
            var search = ObjectContext.ExecuteStoreQuery<BottomStockSettleTableReport>(SQL).ToArray();
            //汇总

            if(search.Count() > 0) {
                BottomStockSettleTableReport summary = new BottomStockSettleTableReport();
                summary.Rownum = 0;
                summary.DistributionCenterName = "汇总";
                summary.Year =bCreateTime.Value.Year.ToString();
                summary.Month=bCreateTime.Value.Month.ToString();
                summary.Week = null;
                summary.CenterName = "";
                summary.DealerName = "";
                summary.AccountSpeciesNum = search.Sum(r => r.AccountSpeciesNum);
                summary.AccountFee = search.Sum(r => r.AccountFee);
                summary.DifferSpeciesNum = search.Sum(r => r.DifferSpeciesNum);
                summary.ActualSpeciesNum = search.Sum(r => r.ActualSpeciesNum);
                summary.DifferenceMoney = search.Sum(r => r.DifferenceMoney);
                summary.ActualFee = search.Sum(r => r.ActualFee);
                summary.BottomCoverage = Math.Round(Decimal.Parse(((summary.AccountSpeciesNum - (summary.DifferSpeciesNum)) * 100 / summary.AccountSpeciesNum).ToString()), 2);
                List<BottomStockSettleTableReport> returnList = new List<BottomStockSettleTableReport>();
                returnList.Add(summary);
                foreach(var item in search) {
                    returnList.Add(item);
                }               
                return returnList;
            }
            return null;
        }
        public IEnumerable<BottomStockSettleTableReport> 储备类别保底覆盖率报表(int? type, string distributionCenterName, string centerName, string dealerName, int? reserveTypeId, string reserveTypeSubItem) {
            int groupType = 3;
            if(type == 1 && reserveTypeId.HasValue && string.IsNullOrEmpty(reserveTypeSubItem)) {
                    groupType = 3;
                } else if(type == 2 && reserveTypeId.HasValue && (string.IsNullOrEmpty(reserveTypeSubItem)|| !string.IsNullOrEmpty(centerName))) {
                    groupType = 2;
                } else if(type == 3 && reserveTypeId.HasValue && (string.IsNullOrEmpty(reserveTypeSubItem)||!string.IsNullOrEmpty(dealerName))) {
                    groupType = 3;
                } if(type == 1 && reserveTypeId.HasValue && !string.IsNullOrEmpty(reserveTypeSubItem)) {
                    groupType = 4;
                } else if(type == 2 && reserveTypeId.HasValue && !string.IsNullOrEmpty(reserveTypeSubItem)) {
                    groupType = 5;
                } else if(type == 3 && reserveTypeId.HasValue && !string.IsNullOrEmpty(reserveTypeSubItem)) {
                    groupType = 6;
                }
            string SQL = @" select rownum,pp.* from (select 
                            (case when {0}=1 or {0}=2 or {0}=3 or {0}=4 or {0}=5 or {0}=6   then MarketingDepartmentName else cast('' as varchar2(50)) end ) as DistributionCenterName,
                               (case when {0}=1 or {0}=4  then  cast('' as varchar2(50))  else centerName   end )    centerName,
                                (case when {0}=1 or {0}=2  or {0}=4  or {0}=5 then  cast('' as varchar2(50))  else dealerName   end )    dealerName,
                                 (case when {0}=1 or {0}=2 or {0}=3  then  cast('' as varchar2(50))  else ReserveTypeSubItem   end )    ReserveTypeSubItem,
                                  (case when {0}=1 or {0}=2 or {0}=3 or  {0}=4 or {0}=5 or {0}=6 then ReserveType else cast('' as varchar2(50)) end ) as ReserveType,
                                   sum(Varieties) as AccountSpeciesNum, --保底应储备品种数
                                   sum(ReserveQty * SalePrice) as AccountFee, --保底应储备金额
                                   sum(Varieties) - sum(actual) as DifferSpeciesNum, -- 差异品种数
                                   sum((Varieties - nvl(actual, 0)) * SalePrice) as DifferenceMoney, --差异金额
                                   --  sum(actual) as actual, --实际保底品种数
                                   (case when sum(Varieties)=0 then 0 else round(sum(actual) * 100 / sum(Varieties), 2) end) as BottomCoverage
                              from (select 1 as Varieties,
                                           bs.SalePrice,
                                           bs.ReserveQty,
                                           (case
                                             when bs.companytype = 2 then
                                              (case
                                                when dps.Quantity > 0 then
                                                 1
                                                else
                                                 0
                                              end)
                                             else
                                              (case
                                                when tt.ActualAvailableStock > 0 then
                                                 1
                                                else
                                                 0
                                              end)
                                           end) as actual,
                                           (case
                                             when bs.companytype = 2 then
                                              dps.Quantity
                                             else
                                              tt.ActualAvailableStock
                                           end) as ActualAvailableStock, --实际
                                            (case
                                             when bs.companytype = 2 then
                                              (select cp.name
                                                 from AgencyDealerRelation ar
                                                 join company cp on ar.agencyid =cp.id
                                                where ar.dealerid = bs.companyid
                                                  and ar.status = 1
                                                  and rownum = 1)
                                             else
                                              center.Name
                                           end) as centerName,
                                           (case
                                             when bs.companytype = 2 then
                                              dealer.Name
                                             else
                                              cast('' as varchar2(20))
                                           end) as dealerName,
                                           (case
                                             when bs.companytype = 3 then
                                              (select mt.Name
                                                 from MarketingDepartment mt
                                                 join Agency ag
                                                   on mt.id = ag.marketingdepartmentid
                                                where ag.id = bs.companyid)
                                             else
                                              (select mt.Name
                                                 from MarketingDepartment mt
                                                 join DealerServiceInfo ag
                                                   on mt.id = ag.marketingdepartmentid
                                                where ag.DealerId = bs.companyid)
                                           end) as MarketingDepartmentName,
                                           row_number() over(partition by bs.sparepartid, bs.companyid order by bs.ReserveQty desc) as ydy,
                                           bs.ReserveType,
                                           bs.ReserveTypeSubItem
                                      from BottomStockSubVersion bs
                                         left join company center
                                        on bs.companyid = center.id
                                       and center.status = 1
                                       and center.type=3
                                      left join company dealer
                                        on bs.companyid = dealer.id
                                       and dealer.status = 1
                                       and dealer.type in(2,7)
                                      left join (select aa.partid,
                                                       aa.StorageCompanyId,
                                                       (aa.actualqty - nvl(bb.orderlockqty, 0)) as ActualAvailableStock
                                                  from (select sum(quantity) as actualqty,
                                                               tmp.partid,
                                                               tmp.StorageCompanyId
                                                          from partsstock tmp
                                                         inner join warehouseareacategory b
                                                            on tmp.warehouseareacategoryid = b.id
                                                         where b.category = 1
                                                         group by tmp.partid, tmp.StorageCompanyId) aa
                                                  left join (select sum(lockedquantity) as orderlockqty,
                                                                   tmp.StorageCompanyId,
                                                                   tmp.partid
                                                              from partslockedstock tmp
                                                             group by tmp.StorageCompanyId, tmp.partid) bb
                                                    on aa.partid = bb.partid
                                                   and aa.StorageCompanyId = bb.StorageCompanyId) tt
                                        on bs.sparepartid = tt.partid
                                       and bs.companyid = tt.StorageCompanyId
                                      left join DealerPartsStock dps
                                        on bs.sparepartid = dps.sparepartid
                                       and bs.companyid = dps.dealerid
                                       and bs.companytype = 2 where  bs.status=1 {1})tt
                             where ydy = 1 {2}
                             group by (case when {0}=1 or {0}=2or {0}=3 or {0}=4 or {0}=5  or {0}=6 then MarketingDepartmentName else cast('' as varchar2(50)) end )  ,
                               (case when {0}=1  or {0}=4  then  cast('' as varchar2(50))  else centerName   end )    ,
                                (case when {0}=1 or {0}=2 or {0}=4  or {0}=5  then  cast('' as varchar2(50))  else dealerName   end )    ,
                                 (case when {0}=1 or {0}=2 or {0}=3  then  cast('' as varchar2(50))  else ReserveTypeSubItem   end )    ,
                                  (case when {0}=1  or {0}=2 or {0}=3 or {0}=4 or {0}=5 or {0}=6 then ReserveType else cast('' as varchar2(50)) end )  ) pp  ";
            var serachSQL = new StringBuilder();
            var serachSQL2 = new StringBuilder();
            if(groupType == 2 || groupType == 4) {
                serachSQL.Append(" and bs.companytype=3 ");
            }
            if(groupType == 3 || groupType == 6) {
                serachSQL.Append(" and (bs.companytype=2 or bs.companytype=7)");
            }
            if(!string.IsNullOrEmpty(centerName)) {
                serachSQL.Append(" and center.Name  like '%" + centerName + "%'");
            }
            if(!string.IsNullOrEmpty(distributionCenterName)) {
                serachSQL2.Append(" and tt.DistributionCenterName like'%" + distributionCenterName + "%'");
            }
            if(!string.IsNullOrEmpty(dealerName)) {
                serachSQL.Append(" and dealer.Name like '%" + dealerName + "%'");
            }   
            if(reserveTypeId.HasValue){
                serachSQL.Append(" and bs.reserveTypeId= "+reserveTypeId);
            }
            if(!string.IsNullOrEmpty(reserveTypeSubItem)){
                serachSQL.Append(" and Lower(bs.reserveTypeSubItem)  = '" + reserveTypeSubItem.ToLower() + "'");
            }
            var userInfo = Utils.GetCurrentUserInfo();

            var company = ObjectContext.Companies.Where(r => r.Id == userInfo.EnterpriseId).FirstOrDefault();
            if(company.Type == (int)DcsCompanyType.分公司) {
                //判断是否维护了人员与分销中心关系
                var isHas = ObjectContext.MarketDptPersonnelRelations.Where(r => r.PersonnelId == userInfo.Id && r.Status == (int)DcsBaseDataStatus.有效).Count();
                if(isHas > 0) {
                    serachSQL2.Append(" and  exists(select 1 from  MarketDptPersonnelRelation mr join Agency ag on mr.marketdepartmentid = ag.marketingdepartmentid  where mr.PersonnelId = " + userInfo.Id + " and ag.code= tt.centerCode and mr.status=1)");
                }
            }
            if(company.Type == (int)DcsCompanyType.服务站 || company.Type == (int)DcsCompanyType.服务站兼代理库||company.Type == (int)DcsCompanyType.代理库) {
                serachSQL.Append(" and bs.companyId='" + company.Id + "'");
            }
            SQL = string.Format(SQL, groupType, serachSQL, serachSQL2);
            var search = ObjectContext.ExecuteStoreQuery<BottomStockSettleTableReport>(SQL).ToArray();
            //汇总

            if(search.Count() > 0) {
                BottomStockSettleTableReport summary = new BottomStockSettleTableReport();
                summary.Rownum = 0;
                summary.DistributionCenterName = "汇总";
                summary.CenterName = "";
                summary.DealerName = "";
                summary.AccountSpeciesNum = search.Sum(r => r.AccountSpeciesNum);
                summary.AccountFee = search.Sum(r => r.AccountFee);
                summary.DifferSpeciesNum = search.Sum(r => r.DifferSpeciesNum);
                summary.ActualSpeciesNum = search.Sum(r => r.ActualSpeciesNum);
                summary.DifferenceMoney = search.Sum(r => r.DifferenceMoney);
               
                summary.BottomCoverage = Math.Round(Decimal.Parse(((summary.AccountSpeciesNum - (summary.DifferSpeciesNum)) * 100 / summary.AccountSpeciesNum).ToString()), 2);
                List<BottomStockSettleTableReport> returnList = new List<BottomStockSettleTableReport>();
                returnList.Add(summary);
                foreach(var item in search) {
                    returnList.Add(item);
                }
                return returnList;
            }
            return null;
        }
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:33
        //原分布类的函数全部转移到Ach                                                               
        public IEnumerable<BottomStockSettleTableReport> 保底库存需提报计划明细表(int? type, string distributionCenterName, string centerName, string dealerName, bool? overZero) {
            return new BottomStockSettleTableReportAch(this).保底库存需提报计划明细表(type, distributionCenterName, centerName, dealerName, overZero);
        }
        public IEnumerable<BottomStockSettleTableReport> 查询保底覆盖率报表(int? type, string distributionCenterName, string centerName, string dealerName, DateTime? bCreateTime, DateTime? eCreateTime) {
            return new BottomStockSettleTableReportAch(this).查询保底覆盖率报表(type, distributionCenterName, centerName, dealerName, bCreateTime, eCreateTime);
        }
        public IEnumerable<BottomStockSettleTableReport> 储备类别保底覆盖率报表(int? type, string distributionCenterName, string centerName, string dealerName, int? reserveTypeId, string reserveTypeSubItem) {
            return new BottomStockSettleTableReportAch(this).储备类别保底覆盖率报表(type, distributionCenterName, centerName, dealerName, reserveTypeId, reserveTypeSubItem);

        }
    }
}
