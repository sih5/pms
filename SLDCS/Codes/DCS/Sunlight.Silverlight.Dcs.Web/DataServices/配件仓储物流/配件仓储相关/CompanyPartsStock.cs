﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class CompanyPartsStockAch : DcsSerivceAchieveBase {
        public CompanyPartsStockAch(DcsDomainService domainService)
            : base(domainService) {
        }

        [Query(HasSideEffects = true)]
        public IEnumerable<CompanyPartsStock> 查询企业库存(int companyId, int[] partIds) {
            var company = ObjectContext.Companies.FirstOrDefault(r => r.Id == companyId);
            if(company == null)
                return Enumerable.Empty<CompanyPartsStock>();
            switch(company.Type) {
                case (int)DcsCompanyType.服务站:
                    var companyPartsStocks = from dealerPartsStock in ObjectContext.DealerPartsStocks
                                             from branch in ObjectContext.Branches
                                             from sparePart in ObjectContext.SpareParts
                                             where branch.Id == dealerPartsStock.BranchId && sparePart.Id == dealerPartsStock.SparePartId && dealerPartsStock.DealerId == companyId
                                             group dealerPartsStock by new {
                                                 dealerPartsStock.BranchId,
                                                 dealerPartsStock.SparePartId,
                                                 dealerPartsStock.SalesCategoryId,
                                                 SparePartCode = sparePart.Code,
                                                 SparePartName = sparePart.Name,
                                                 BrancheCode = branch.Code,
                                                 BrancheName = branch.Name
                                             } into temp
                                             select new {
                                                 temp.Key.SalesCategoryId,
                                                 CompanyId = companyId,
                                                 CompanyCode = company.Code,
                                                 CompanyName = company.Name,
                                                 CompanyType = company.Type,
                                                 temp.Key.BranchId,
                                                 temp.Key.BrancheCode,
                                                 temp.Key.BrancheName,
                                                 temp.Key.SparePartId,
                                                 temp.Key.SparePartCode,
                                                 temp.Key.SparePartName,
                                                 Quantity = temp.Sum(r => r.Quantity),
                                                 UsableQuantity = temp.Sum(r => r.Quantity),
                                                 company.ProvinceName,
                                                 company.CityName,
                                                 company.CountyName
                                             };
                    if(partIds != null && partIds.Count() > 0)
                        companyPartsStocks = companyPartsStocks.Where(r => partIds.Contains(r.SparePartId));
                    return (from cpt in companyPartsStocks
                            join partsSalesPrice in ObjectContext.PartsSalesPrices on cpt.SparePartId equals partsSalesPrice.SparePartId into details
                            join sparePart in ObjectContext.SpareParts on cpt.SparePartId equals sparePart.Id into sparePartdetails
                            from v in details.DefaultIfEmpty()
                            where v.Status == (int)DcsBaseDataStatus.有效
                            from s in sparePartdetails.DefaultIfEmpty()
                            where s.Status == (int)DcsMasterDataStatus.有效
                            where cpt.SalesCategoryId == v.PartsSalesCategoryId
                            select new CompanyPartsStock {
                                CompanyId = cpt.CompanyId,
                                CompanyCode = cpt.CompanyCode,
                                CompanyName = cpt.CompanyName,
                                CompanyType = cpt.CompanyType,
                                PartsSalesCategoryId = v.PartsSalesCategoryId,
                                PartsSalesCategoryCode = v.PartsSalesCategoryCode,
                                PartsSalesCategoryName = v.PartsSalesCategoryName,
                                BranchId = cpt.BranchId,
                                BranchCode = cpt.BrancheCode,
                                BranchName = cpt.BrancheName,
                                SparePartId = cpt.SparePartId,
                                SparePartCode = cpt.SparePartCode,
                                SparePartName = cpt.SparePartName,
                                Quantity = cpt.Quantity,
                                UsableQuantity = cpt.UsableQuantity,
                                ProvinceName = cpt.ProvinceName,
                                CityName = cpt.CityName,
                                CountyName = cpt.CountyName,
                                PartsSalesPrice = v.SalesPrice,
                                MeasureUnit = s.MeasureUnit
                            }).OrderBy(r => r.CompanyId);
                case (int)DcsCompanyType.分公司:
                case (int)DcsCompanyType.代理库:
                    var partsStocks = ObjectContext.PartsStocks.Where(r => r.StorageCompanyId == companyId && ObjectContext.WarehouseAreaCategories.Any(category => category.Id == r.WarehouseAreaCategoryId && (category.Category == (int)DcsAreaType.保管区 || category.Category == (int)DcsAreaType.检验区))).GroupBy(r => new {
                        r.BranchId,
                        r.PartId
                    }).Select(r => new {
                        r.Key.BranchId,
                        r.Key.PartId,
                        Quantity = r.Sum(v => v.Quantity)
                    });
                    var dlcompanyPartsStocks = (from ps in partsStocks
                                                from branch in ObjectContext.Branches
                                                from salesUnit in ObjectContext.SalesUnits.Where(r => r.Status == (int)DcsMasterDataStatus.有效 && r.OwnerCompanyId == companyId)
                                                from sparePart in ObjectContext.SpareParts
                                                where branch.Id == ps.BranchId && sparePart.Id == ps.PartId && ps.BranchId == salesUnit.BranchId
                                                select new {
                                                    CompanyId = companyId,
                                                    CompanyCode = company.Code,
                                                    CompanyName = company.Name,
                                                    CompanyType = company.Type,
                                                    ps.BranchId,
                                                    BranchCode = branch.Code,
                                                    BranchName = branch.Name,
                                                    salesUnit.PartsSalesCategoryId,
                                                    SparePartId = ps.PartId,
                                                    SparePartCode = sparePart.Code,
                                                    SparePartName = sparePart.Name,
                                                    ps.Quantity,
                                                    LockedQuantity = ObjectContext.PartsLockedStocks.Where(v => v.StorageCompanyId == companyId && v.BranchId == branch.Id && v.PartId == ps.PartId).Sum(v => v.LockedQuantity)
                                                }).Select(v => new {
                                                    v.CompanyId,
                                                    v.CompanyCode,
                                                    v.CompanyName,
                                                    v.CompanyType,
                                                    v.BranchId,
                                                    v.BranchCode,
                                                    v.BranchName,
                                                    v.PartsSalesCategoryId,
                                                    v.SparePartId,
                                                    v.SparePartCode,
                                                    v.SparePartName,
                                                    v.Quantity,
                                                    UsableQuantity = v.Quantity - ((int?)v.LockedQuantity ?? 0),
                                                    company.ProvinceName,
                                                    company.CityName,
                                                    company.CountyName
                                                });
                    if(partIds != null && partIds.Count() > 0)
                        dlcompanyPartsStocks = dlcompanyPartsStocks.Where(r => partIds.Contains(r.SparePartId));
                    return (from cpt in dlcompanyPartsStocks
                            join partsSalesPrice in ObjectContext.PartsSalesPrices on cpt.SparePartId equals partsSalesPrice.SparePartId into details
                            join sparePart in ObjectContext.SpareParts on cpt.SparePartId equals sparePart.Id into sparePartdetails
                            from v in details.DefaultIfEmpty()
                            where v.Status == (int)DcsBaseDataStatus.有效
                            from s in sparePartdetails.DefaultIfEmpty()
                            where s.Status == (int)DcsMasterDataStatus.有效
                            where cpt.PartsSalesCategoryId == v.PartsSalesCategoryId
                            select new CompanyPartsStock {
                                CompanyId = cpt.CompanyId,
                                CompanyCode = cpt.CompanyCode,
                                CompanyName = cpt.CompanyName,
                                CompanyType = cpt.CompanyType,
                                PartsSalesCategoryId = v.PartsSalesCategoryId,
                                PartsSalesCategoryCode = v.PartsSalesCategoryCode,
                                PartsSalesCategoryName = v.PartsSalesCategoryName,
                                BranchId = cpt.BranchId,
                                BranchCode = cpt.BranchCode,
                                BranchName = cpt.BranchName,
                                SparePartId = cpt.SparePartId,
                                SparePartCode = cpt.SparePartCode,
                                SparePartName = cpt.SparePartName,
                                Quantity = cpt.Quantity,
                                UsableQuantity = cpt.UsableQuantity,
                                ProvinceName = cpt.ProvinceName,
                                CityName = cpt.CityName,
                                CountyName = cpt.CountyName,
                                PartsSalesPrice = v.SalesPrice,
                                MeasureUnit = s.MeasureUnit
                            }).OrderBy(r => r.CompanyId);
                default:
                    return Enumerable.Empty<CompanyPartsStock>();
            }
        }
        public IEnumerable<CompanyPartsStock> 查询替互换件企业库存(int companyId, int sparePartId) {
            var partsReplacementIds = ObjectContext.PartsReplacements.Where(r => (r.OldPartId == sparePartId || (r.NewPartId == sparePartId))
                && r.Status == (int)DcsBaseDataStatus.有效).Select(v => v.NewPartId == sparePartId ? v.OldPartId : v.NewPartId).ToArray();
            if(!partsReplacementIds.Any())
                return null;
            return 查询企业库存(companyId, partsReplacementIds);
        }
        [Query(HasSideEffects = true)]
        public IEnumerable<CompanyPartsStock> Get查询企业库存BySparePartIds(int companyId, int[] sparePartIds) {
            return 查询企业库存(companyId, sparePartIds);
        }
        public IQueryable<CompanyPartsStock> 销售订单查询仓库库存(int salesUnitOwnerCompanyId, int warehouseId, int[] partIds) {
            var query = from a in ObjectContext.PartsStocks.Where(r => r.StorageCompanyId == salesUnitOwnerCompanyId && r.WarehouseId == warehouseId && partIds.Contains(r.PartId))
                        join b in ObjectContext.WarehouseAreas.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on a.WarehouseAreaId equals b.Id
                        join c in ObjectContext.WarehouseAreaCategories on b.AreaCategoryId equals c.Id
                        select new {
                            a.WarehouseId,
                            a.PartId,
                            a.Quantity
                        };
            var lockQuery = from a in ObjectContext.PartsLockedStocks.Where(r => r.StorageCompanyId == salesUnitOwnerCompanyId && r.WarehouseId == warehouseId && partIds.Contains(r.PartId))
                            select new {
                                a.WarehouseId,
                                a.PartId,
                                LockedQuantity = (int?)a.LockedQuantity
                            };
            var groupQuery = from a in query
                             group a by new {
                                 a.WarehouseId,
                                 a.PartId
                             }
                                 into tempQery
                             select new {
                                 tempQery.Key.WarehouseId,
                                 tempQery.Key.PartId,
                                 Quantity = tempQery.Sum(r => r.Quantity)
                             };
            var quantityQuery = from a in groupQuery
                                join b in lockQuery on new {
                                    a.PartId,
                                    a.WarehouseId
                                } equals new {
                                    b.PartId,
                                    b.WarehouseId
                                } into tempTable
                                from t in tempTable.DefaultIfEmpty()
                                select new {
                                    a.WarehouseId,
                                    a.PartId,
                                    Quantity = a.Quantity,
                                    UsableQuantity = a.Quantity - (t.LockedQuantity ?? 0)
                                };
            var result = from a in quantityQuery
                         join b in ObjectContext.SpareParts.Where(r => r.Status == (int)DcsMasterDataStatus.有效) on a.PartId equals b.Id
                         join c in ObjectContext.Warehouses.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on a.WarehouseId equals c.Id
                         select new CompanyPartsStock {
                             SparePartId = a.PartId,
                             SparePartCode = b.Code,
                             SparePartName = b.Name,
                             Quantity = a.Quantity,
                             UsableQuantity = a.UsableQuantity,
                             MeasureUnit = b.MeasureUnit,
                             WarehouseId = c.Id,
                             WarehouseCode = c.Code,
                             WarehouseName = c.Name
                         };
            return result;
        }

        [Query(HasSideEffects = true)]
        public IEnumerable<CompanyPartsStock> 查询服务站代理库库存(int companyId, int[] partIds, int branchId, int partsSalesCategoryId) {
            var company = ObjectContext.Companies.FirstOrDefault(r => r.Id == companyId && r.Status == (int)DcsBaseDataStatus.有效);
            if(company == null)
                return Enumerable.Empty<CompanyPartsStock>();
            switch(company.Type) {
                case (int)DcsCompanyType.服务站:
                    return (from dealerPartsStock in ObjectContext.DealerPartsStocks.Where(r => r.SubDealerId == -1 && r.DealerId == companyId && r.SalesCategoryId == partsSalesCategoryId && partIds.Contains(r.SparePartId))
                            join s in ObjectContext.SpareParts.Where(r => r.Status == (int)DcsBaseDataStatus.有效)
                            on dealerPartsStock.SparePartId equals s.Id
                            select new CompanyPartsStock {
                                CompanyCode = company.Code,
                                CompanyName = company.Name,
                                SparePartId = dealerPartsStock.SparePartId,
                                SparePartCode = s.Code,
                                SparePartName = s.Name,
                                Quantity = dealerPartsStock.Quantity
                            }).OrderBy(r => r.SparePartId);
                case (int)DcsCompanyType.服务站兼代理库:
                case (int)DcsCompanyType.代理库:
                    var partsStocks = ObjectContext.PartsStocks.Where(r => r.BranchId == branchId && r.StorageCompanyId == companyId && partIds.Contains(r.PartId) && ObjectContext.WarehouseAreaCategories.Any(category => category.Id == r.WarehouseAreaCategoryId && (category.Category == (int)DcsAreaType.保管区 || category.Category == (int)DcsAreaType.检验区))).GroupBy(r => new {
                        r.BranchId,
                        r.StorageCompanyId,
                        r.PartId
                    }).Select(r => new {
                        r.Key.BranchId,
                        r.Key.StorageCompanyId,
                        r.Key.PartId,
                        Quantity = r.Sum(v => v.Quantity)
                    });
                    return (from ps in partsStocks
                            join sparePart in ObjectContext.SpareParts.Where(r => r.Status == (int)DcsBaseDataStatus.有效)
                            on ps.PartId equals sparePart.Id
                            select new CompanyPartsStock {
                                CompanyCode = company.Code,
                                CompanyName = company.Name,
                                SparePartId = ps.PartId,
                                SparePartCode = sparePart.Code,
                                SparePartName = sparePart.Name,
                                Quantity = ps.Quantity
                            }).OrderBy(r => r.SparePartId);
                default:
                    return Enumerable.Empty<CompanyPartsStock>();
            }
        }





    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:32
        //原分布类的函数全部转移到Ach                                                               

        [Query(HasSideEffects = true)]
        public IEnumerable<CompanyPartsStock> 查询企业库存(int companyId, int[] partIds) {
            return new CompanyPartsStockAch(this).查询企业库存(companyId, partIds);
        }

        public IEnumerable<CompanyPartsStock> 查询替互换件企业库存(int companyId, int sparePartId) {
            return new CompanyPartsStockAch(this).查询替互换件企业库存(companyId, sparePartId);
        }


        [Query(HasSideEffects = true)]
        public IEnumerable<CompanyPartsStock> Get查询企业库存BySparePartIds(int companyId, int[] sparePartIds) {
            return new CompanyPartsStockAch(this).Get查询企业库存BySparePartIds(companyId, sparePartIds);
        }

        public IQueryable<CompanyPartsStock> 销售订单查询仓库库存(int salesUnitOwnerCompanyId, int warehouseId, int[] partIds) {
            return new CompanyPartsStockAch(this).销售订单查询仓库库存(salesUnitOwnerCompanyId, warehouseId, partIds);
        }

        [Query(HasSideEffects = true)]
        public IEnumerable<CompanyPartsStock> 查询服务站代理库库存(int companyId, int[] partIds, int branchId, int partsSalesCategoryId) {
            return new CompanyPartsStockAch(this).查询服务站代理库库存(companyId, partIds, branchId, partsSalesCategoryId);
        }
    }
}
