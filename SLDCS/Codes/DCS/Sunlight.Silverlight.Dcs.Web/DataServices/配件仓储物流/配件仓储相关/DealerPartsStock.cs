﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class DealerPartsStockAch : DcsSerivceAchieveBase {
        internal void InsertDealerPartsStockValidate(DealerPartsStock dealerPartsStock) {
            var dbDealerPartsStock = ObjectContext.DealerPartsStocks.Where(r => r.SubDealerId == dealerPartsStock.SubDealerId && r.DealerId == dealerPartsStock.DealerId && r.SalesCategoryId == dealerPartsStock.SalesCategoryId && r.SparePartId == dealerPartsStock.SparePartId).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbDealerPartsStock != null)
                throw new ValidationException(string.Format(ErrorStrings.DealerPartsStock_Validation1, dealerPartsStock.SparePartCode));
            var userInfo = Utils.GetCurrentUserInfo();
            dealerPartsStock.CreatorId = userInfo.Id;
            dealerPartsStock.CreatorName = userInfo.Name;
            dealerPartsStock.CreateTime = DateTime.Now;
        }
        internal void UpdateDealerPartsStockValidate(DealerPartsStock dealerPartsStock) {
            var dbDealerPartsStock = ObjectContext.DealerPartsStocks.Where(r => r.Id != dealerPartsStock.Id && r.SubDealerId == dealerPartsStock.SubDealerId && r.DealerId == dealerPartsStock.DealerId && r.SalesCategoryId == dealerPartsStock.SalesCategoryId && r.SparePartId == dealerPartsStock.SparePartId).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbDealerPartsStock != null)
                throw new ValidationException(string.Format(ErrorStrings.DealerPartsStock_Validation1, dealerPartsStock.SparePartCode));
            var userInfo = Utils.GetCurrentUserInfo();
            dealerPartsStock.ModifierId = userInfo.Id;
            dealerPartsStock.ModifierName = userInfo.Name;
            dealerPartsStock.ModifyTime = DateTime.Now;
        }
        public void InsertDealerPartsStock(DealerPartsStock dealerPartsStock) {
            InsertToDatabase(dealerPartsStock);
            this.InsertDealerPartsStockValidate(dealerPartsStock);
        }
        public void UpdateDealerPartsStock(DealerPartsStock dealerPartsStock) {
            UpdateToDatabase(dealerPartsStock);
            this.UpdateDealerPartsStockValidate(dealerPartsStock);
        }
        [Query(HasSideEffects = true)]
        public IQueryable<DealerPartsStockForBussiness> 查询经销商配件零售价(int dealerId, int salesCategoryId, int[] sparePartIds, int subDealerId) {
            var result = from a in ObjectContext.DealerPartsStocks.Where(r => r.DealerId == dealerId && r.SalesCategoryId == salesCategoryId && r.SubDealerId == subDealerId)
                         join b in ObjectContext.PartsRetailGuidePrices.Where(r => r.PartsSalesCategoryId == salesCategoryId && r.Status != (int)DcsBaseDataStatus.作废) on new {
                             sparePartId = a.SparePartId,
                             partsSalesCategoryId = a.SalesCategoryId
                         } equals new {
                             sparePartId = b.SparePartId,
                             partsSalesCategoryId = b.PartsSalesCategoryId
                         } into tempTable
                         from t in tempTable.DefaultIfEmpty()
                         join c in ObjectContext.PartsSalesCategories.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on a.SalesCategoryId equals c.Id
                         join d in ObjectContext.SpareParts on t.SparePartId equals d.Id
                         select new DealerPartsStockForBussiness {
                             Id = a.Id,
                             DealerId = a.DealerId,
                             DealerCode = a.DealerCode,
                             DealerName = a.DealerName,
                             SubDealerId = a.SubDealerId,
                             BranchId = a.BranchId,
                             SalesCategoryId = a.SalesCategoryId,
                             SalesCategoryName = c.Name,
                             SparePartId = a.SparePartId,
                             SparePartCode = a.SparePartCode,
                             SparePartName = d.Name,
                             Quantity = a.Quantity,
                             CreatorId = a.CreatorId,
                             CreatorName = a.CreatorName,
                             CreateTime = a.CreateTime,
                             ModifierId = a.ModifierId,
                             ModifierName = a.ModifierName,
                             ModifyTime = a.ModifyTime,
                             RetailGuidePrice = t.RetailGuidePrice
                         };
            if(sparePartIds != null && sparePartIds.Length > 0) {
                result = result.Where(r => sparePartIds.Any(v => v == r.SparePartId));
            }
            return result.OrderBy(r => r.Id);
        }
        [Query(HasSideEffects = true)]
        public IEnumerable<DealerPartsStock> 查询经销商配件批发价(int dealerId, int salesCategoryId, int[] sparePartId) {
            var dealerPartsStockSql = ObjectContext.DealerPartsStocks.Where(r => r.DealerId == dealerId && r.SalesCategoryId == salesCategoryId).Include("SparePart");
            if(sparePartId != null && sparePartId.Length > 0)
                dealerPartsStockSql = dealerPartsStockSql.Where(r => sparePartId.Contains(r.SparePartId));
            var partsSalesPrices = ObjectContext.PartsSalesPrices.Where(r => r.PartsSalesCategoryId == salesCategoryId && r.PriceType == (int)DcsPartsSalesPricePriceType.基准销售价 && r.Status != (int)DcsBaseDataStatus.作废).ToArray();
            var dealerPartsStocks = dealerPartsStockSql.ToArray();
            for(var i = 0; i < dealerPartsStocks.Count(); i++) {
                var partsSalesPrice = partsSalesPrices.FirstOrDefault(r => r.SparePartId == dealerPartsStocks[i].SparePartId);
                dealerPartsStocks[i].BasicSalePrice = partsSalesPrice == null ? 0 : partsSalesPrice.SalesPrice;
            }
            return dealerPartsStocks.OrderBy(r => r.Id);
        }
        public IQueryable<DealerPartsStock> 查询经销商配件库存() {
            return ObjectContext.DealerPartsStocks.OrderBy(c => c.Id);
        }

        public IQueryable<VirtualDealerPartsStock> GetDealerPartsStocksWithSubDealer(string sparePartCode, bool? isDealerQuery, bool? isZero) {
            if(!isDealerQuery.HasValue)
                isDealerQuery = false;
            var enterpriseId = Utils.GetCurrentUserInfo().EnterpriseId;
            var company = ObjectContext.Companies.Single(r => r.Id == enterpriseId);
            IQueryable<Company> enterprise = ObjectContext.Companies;
            if (company.Type == (int)DcsCompanyType.服务站 || company.Type == (int)DcsCompanyType.服务站兼代理库) {
                var agency = ObjectContext.AgencyDealerRelations.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.DealerId == enterpriseId).ToArray();
                var agencyIds = agency.Select(r => r.AgencyId).ToArray();
                var dealerPartsStocks = from a in ObjectContext.DealerPartsStocks
                                    join c in ObjectContext.SubDealers on a.SubDealerId equals c.Id into temp1
                                    from t1 in temp1.DefaultIfEmpty()
                                    join e in ObjectContext.PartsSalesPrices.Where(v => v.Status != (int)DcsBaseDataStatus.作废) on new {
                                        salesCategoryId = a.SalesCategoryId,
                                        sparePartId = a.SparePartId
                                    } equals new {
                                        salesCategoryId = e.PartsSalesCategoryId,
                                        sparePartId = e.SparePartId
                                    } into temp
                                    from t in temp.DefaultIfEmpty()
                                    join d in ObjectContext.SpareParts on a.SparePartId equals d.Id
                                    join e in enterprise on a.DealerId equals e.Id
                                    join f in ObjectContext.AgencyDealerRelations.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && agencyIds.Contains(r.AgencyId)) on a.DealerId equals f.DealerId
                                        join g in ObjectContext.DealerServiceInfoes.Where(v => v.Status == (int)DcsMasterDataStatus.有效) on a.DealerId equals g.DealerId
                                        join h in ObjectContext.ChannelCapabilities.Where(v => v.Status == (int)DcsBaseDataStatus.有效) on g.ChannelCapabilityId equals h.Id into hh
                                        from p in hh.DefaultIfEmpty()
                                        join q in ObjectContext.MarketingDepartments.Where(v => v.Status == (int)DcsBaseDataStatus.有效) on g.MarketingDepartmentId equals q.Id into qq
                                        from r in qq.DefaultIfEmpty()
                                        join gh in ObjectContext.Companies on a.DealerId equals gh.Id
                                        select new VirtualDealerPartsStock {
                                        Id = a.Id,
                                        DealerId = a.DealerId,
                                        DealerCode = a.DealerCode,
                                        DealerName = gh.Name,
                                        SubDealerId = a.SubDealerId,
                                        SubDealerCode = t1.Code,
                                        SubDealerName = t1.Name,
                                        BranchId = a.BranchId,
                                        SalesCategoryId = a.SalesCategoryId,
                                        SalesCategoryName = a.SalesCategoryName,
                                        SparePartId = a.SparePartId,
                                        SparePartCode = a.SparePartCode,
                                        SparePartName = d.Name,
                                        Quantity = a.Quantity,
                                        CreatorName = a.CreatorName,
                                        CreateTime = a.CreateTime,
                                        ModifierName = a.ModifierName,
                                        ModifyTime = a.ModifyTime,
                                        PriceType = t.PriceType,
                                        BasicSalePrice = t == null ? 0 : t.SalesPrice,
                                        DealerType=p.Name,
                                        BasicSalePriceAll = t == null ? 0 : t.SalesPrice * a.Quantity,
                                        CompanyType=e.Type,
                                        MarketingDepartmentName=r.Name
                                    };
                if (!isDealerQuery.Value) { 
                    dealerPartsStocks = dealerPartsStocks.Where(r => r.DealerId == enterpriseId);
                }
                if (!string.IsNullOrEmpty(sparePartCode)) {
                    var spareparts = sparePartCode.Split(',');
                    if (spareparts.Length == 1) {
                        var sparepartcode = spareparts[0];
                        if (isDealerQuery.Value) {
                            dealerPartsStocks = dealerPartsStocks.Where(r => r.SparePartCode.Equals(sparepartcode));
                        } else {
                            dealerPartsStocks = dealerPartsStocks.Where(r => r.SparePartCode.Contains(sparepartcode));
                        }
                    } else {
                        dealerPartsStocks = dealerPartsStocks.Where(r => spareparts.Contains(r.SparePartCode));
                    }
                }
                return dealerPartsStocks.Distinct().OrderBy(r => r.Id);
            }else if(company.Type == (int)DcsCompanyType.代理库){
                var dealerPartsStocks = from a in ObjectContext.DealerPartsStocks
                                    join c in ObjectContext.SubDealers on a.SubDealerId equals c.Id into temp1
                                    from t1 in temp1.DefaultIfEmpty()
                                    join e in ObjectContext.PartsSalesPrices.Where(v => v.Status != (int)DcsBaseDataStatus.作废) on new {
                                        salesCategoryId = a.SalesCategoryId,
                                        sparePartId = a.SparePartId
                                    } equals new {
                                        salesCategoryId = e.PartsSalesCategoryId,
                                        sparePartId = e.SparePartId
                                    } into temp
                                    from t in temp.DefaultIfEmpty()
                                    join d in ObjectContext.SpareParts on a.SparePartId equals d.Id
                                        join f in enterprise on a.DealerId equals f.Id
                                    join e in ObjectContext.AgencyDealerRelations.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.AgencyId == enterpriseId) on a.DealerId equals e.DealerId
                                        join g in ObjectContext.DealerServiceInfoes.Where(v => v.Status == (int)DcsMasterDataStatus.有效) on a.DealerId equals g.DealerId
                                        join h in ObjectContext.ChannelCapabilities.Where(v => v.Status == (int)DcsBaseDataStatus.有效) on g.ChannelCapabilityId equals h.Id into hh
                                        from p in hh.DefaultIfEmpty()
                                        join q in ObjectContext.MarketingDepartments.Where(v => v.Status == (int)DcsBaseDataStatus.有效) on g.MarketingDepartmentId equals q.Id into qq
                                        from r in qq.DefaultIfEmpty()
                                        join gh in ObjectContext.Companies on a.DealerId equals gh.Id
                                        select new VirtualDealerPartsStock {
                                        Id = a.Id,
                                        DealerId = a.DealerId,
                                        DealerCode = a.DealerCode,
                                        DealerName = gh.Name,
                                        SubDealerId = a.SubDealerId,
                                        SubDealerCode = t1.Code,
                                        SubDealerName = t1.Name,
                                        BranchId = a.BranchId,
                                        SalesCategoryId = a.SalesCategoryId,
                                        SalesCategoryName = a.SalesCategoryName,
                                        SparePartId = a.SparePartId,
                                        SparePartCode = a.SparePartCode,
                                        SparePartName = d.Name,
                                        Quantity = a.Quantity,
                                        CreatorName = a.CreatorName,
                                        CreateTime = a.CreateTime,
                                        ModifierName = a.ModifierName,
                                        ModifyTime = a.ModifyTime,
                                        PriceType = t.PriceType,
                                        BasicSalePrice = t == null ? 0 : t.SalesPrice,
                                        BasicSalePriceAll = t == null ? 0 : t.SalesPrice * a.Quantity,
                                        DealerType=p.Name,
                                        CompanyType = f.Type,
                                        MarketingDepartmentName = r.Name
                                    };
                if (!string.IsNullOrEmpty(sparePartCode)) {
                    var spareparts = sparePartCode.Split(',');
                    if (spareparts.Length == 1) {
                        var sparepartcode = spareparts[0];
                        dealerPartsStocks = dealerPartsStocks.Where(r => r.SparePartCode.Contains(sparepartcode));
                    } else {
                        dealerPartsStocks = dealerPartsStocks.Where(r => spareparts.Contains(r.SparePartCode));
                    }
                }
                 return dealerPartsStocks.OrderBy(r => r.Id);
            } else if (company.Type == (int)DcsCompanyType.分公司) { 
                var dealerPartsStocks = from a in ObjectContext.DealerPartsStocks
                                    join c in ObjectContext.SubDealers on a.SubDealerId equals c.Id into temp1
                                    from t1 in temp1.DefaultIfEmpty()
                                    join e in ObjectContext.PartsSalesPrices.Where(v => v.Status != (int)DcsBaseDataStatus.作废) on new {
                                        salesCategoryId = a.SalesCategoryId,
                                        sparePartId = a.SparePartId
                                    } equals new {
                                        salesCategoryId = e.PartsSalesCategoryId,
                                        sparePartId = e.SparePartId
                                    } into temp
                                    from t in temp.DefaultIfEmpty()
                                    join d in ObjectContext.SpareParts on a.SparePartId equals d.Id
                                    join f in enterprise on a.DealerId equals f.Id
                                        join g in ObjectContext.DealerServiceInfoes.Where(v => v.Status == (int)DcsMasterDataStatus.有效) on a.DealerId equals g.DealerId
                                        join h in ObjectContext.ChannelCapabilities.Where(v => v.Status == (int)DcsBaseDataStatus.有效) on g.ChannelCapabilityId equals h.Id into hh
                                        from p in hh.DefaultIfEmpty()
                                         join q in ObjectContext.MarketingDepartments.Where(v => v.Status == (int)DcsBaseDataStatus.有效) on g.MarketingDepartmentId equals q.Id into qq
                                        from r in qq.DefaultIfEmpty()
                                        join gh in ObjectContext.Companies on a.DealerId equals gh.Id
                                    select new VirtualDealerPartsStock {
                                        Id = a.Id,
                                        DealerId = a.DealerId,
                                        DealerCode = a.DealerCode,
                                        DealerName = gh.Name,
                                        SubDealerId = a.SubDealerId,
                                        SubDealerCode = t1.Code,
                                        SubDealerName = t1.Name,
                                        BranchId = a.BranchId,
                                        SalesCategoryId = a.SalesCategoryId,
                                        SalesCategoryName = a.SalesCategoryName,
                                        SparePartId = a.SparePartId,
                                        SparePartCode = a.SparePartCode,
                                        SparePartName = d.Name,
                                        Quantity = a.Quantity,
                                        CreatorName = a.CreatorName,
                                        CreateTime = a.CreateTime,
                                        ModifierName = a.ModifierName,
                                        ModifyTime = a.ModifyTime,
                                        PriceType = t.PriceType,
                                        BasicSalePrice = t == null ? 0 : t.SalesPrice,
                                        BasicSalePriceAll = t == null ? 0 : t.SalesPrice * a.Quantity,
                                        CompanyType = f.Type,
                                        DealerType=p.Name,
                                        MarketingDepartmentName=r.Name
                                    };
                if (!string.IsNullOrEmpty(sparePartCode)) {
                    var spareparts = sparePartCode.Split(',');
                    if (spareparts.Length == 1) {
                        var sparepartcode = spareparts[0];
                        dealerPartsStocks = dealerPartsStocks.Where(r => r.SparePartCode.Contains(sparepartcode));
                    } else {
                        dealerPartsStocks = dealerPartsStocks.Where(r => spareparts.Contains(r.SparePartCode));
                    }
                }
                if(isZero.HasValue&& isZero.Value){
                    dealerPartsStocks=dealerPartsStocks.Where(t=>t.Quantity==0 ||t.Quantity==null);
                }if(isZero.HasValue&& !isZero.Value){
                    dealerPartsStocks = dealerPartsStocks.Where(t => t.Quantity >0);
                }
                return dealerPartsStocks.OrderBy(r => r.Id);
            }
            return null;
        }
        [Query(HasSideEffects = true)]
        public IQueryable<DealerPartsStock> GetDealerPartsStocksBySecondClassStationPlanAndSparePartIds(int firstClassStationId, int partsSalesCategoryId, int[] ids) {
            return ObjectContext.DealerPartsStocks.Where(r => r.DealerId == firstClassStationId && r.SalesCategoryId == partsSalesCategoryId && r.SubDealerId == -1 && ids.Contains(r.SparePartId)).OrderBy(r => r.Id);
        }
        [Query(HasSideEffects = true)]
        public IQueryable<DealerPartsStockForBussiness> 查询经销商配件零售价2(int dealerId, int salesCategoryId, int[] sparePartIds, int subDealerId) {
            var result = from a in ObjectContext.DealerPartsStocks.Where(r => r.DealerId == dealerId && r.SalesCategoryId == salesCategoryId && r.SubDealerId == subDealerId)
                         join b in ObjectContext.PartsRetailGuidePrices.Where(r => r.PartsSalesCategoryId == salesCategoryId && r.Status != (int)DcsBaseDataStatus.作废) on new {
                             sparePartId = a.SparePartId,
                             partsSalesCategoryId = a.SalesCategoryId
                         } equals new {
                             sparePartId = b.SparePartId,
                             partsSalesCategoryId = b.PartsSalesCategoryId
                         } into tempTable
                         from t in tempTable.DefaultIfEmpty()
                         join c in ObjectContext.PartsSalesCategories.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on a.SalesCategoryId equals c.Id
                         join d in ObjectContext.SpareParts on t.SparePartId equals d.Id
                         join e in ObjectContext.PartDeleaveInformations.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on new {
                             sparePartId = a.SparePartId,
                             partsSalesCategoryId = a.SalesCategoryId
                         }
                         equals new {
                             sparePartId = e.OldPartId,
                             partsSalesCategoryId = e.PartsSalesCategoryId
                         } into tempTable1
                         from t1 in tempTable1.DefaultIfEmpty()
                         join ps in ObjectContext.PartsSalesPrices on new {
                             a.SparePartId,
                             Status = (int)DcsBaseDataStatus.有效
                         } equals new {
                             ps.SparePartId,
                             ps.Status
                         } into ps1
                         from psp in ps1.DefaultIfEmpty()
                         join pb in ObjectContext.PartsBranches.Where(t=>t.Status==(int)DcsMasterDataStatus.有效) on a.SparePartId equals pb.PartId
                         join k in ObjectContext.PartsSalePriceIncreaseRates on pb.IncreaseRateGroupId equals k.Id into ks 
                         from kv in ks.DefaultIfEmpty()
                         select new DealerPartsStockForBussiness {
                             Id = a.Id,
                             DealerId = a.DealerId,
                             DealerCode = a.DealerCode,
                             DealerName = a.DealerName,
                             SubDealerId = a.SubDealerId,
                             BranchId = a.BranchId,
                             SalesCategoryId = a.SalesCategoryId,
                             SalesCategoryName = c.Name,
                             SparePartId = a.SparePartId,
                             SparePartCode = a.SparePartCode,
                             SparePartName = d.Name,
                             Quantity = a.Quantity,
                             CreatorId = a.CreatorId,
                             CreatorName = a.CreatorName,
                             CreateTime = a.CreateTime,
                             ModifierId = a.ModifierId,
                             ModifierName = a.ModifierName,
                             ModifyTime = a.ModifyTime,
                             RetailGuidePrice = t.RetailGuidePrice / (t1 == null ? 1 : t1.DeleaveAmount),
                             TraceProperty = d.TraceProperty,
                             SalesPrice =psp.SalesPrice,
                             GroupCode =kv.GroupCode
                         };
            if(sparePartIds != null && sparePartIds.Length > 0) {
                result = result.Where(r => sparePartIds.Any(v => v == r.SparePartId));
            }
            return result.OrderBy(r => r.Id);
        }

        public IQueryable<DealerPartsStockOutInRecord> 出入库记录查询(string billCode, int[] sparepartIds, DateTime? dateTimeBegin, DateTime? dateTimeEnd) {
            var userInfo = Utils.GetCurrentUserInfo();
            var query = (IQueryable<DealerPartsStockOutInRecord>)ObjectContext.DealerPartsStockOutInRecords.Where(r => r.DealerId == userInfo.EnterpriseId);
            if(!string.IsNullOrWhiteSpace(billCode)) {
                query = query.Where(r => r.code == billCode);
            }
            if(sparepartIds != null && sparepartIds.Length > 0) {
                query = query.Where(r => sparepartIds.Contains(r.PartsId));
            }
            if(dateTimeBegin.HasValue) {
                query = query.Where(r => r.CreateTime >= dateTimeBegin);
            }
            if(dateTimeEnd.HasValue) {
                query = query.Where(r => r.CreateTime < dateTimeEnd);
            }
            return query.OrderBy(r => r.CreateTime);
        }

    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:32
        //原分布类的函数全部转移到Ach                                                               

        public void InsertDealerPartsStock(DealerPartsStock dealerPartsStock) {
            new DealerPartsStockAch(this).InsertDealerPartsStock(dealerPartsStock);
        }

        public void UpdateDealerPartsStock(DealerPartsStock dealerPartsStock) {
            new DealerPartsStockAch(this).UpdateDealerPartsStock(dealerPartsStock);
        }


        [Query(HasSideEffects = true)]
        public IQueryable<DealerPartsStockForBussiness> 查询经销商配件零售价(int dealerId, int salesCategoryId, int[] sparePartIds, int subDealerId) {
            return new DealerPartsStockAch(this).查询经销商配件零售价(dealerId, salesCategoryId, sparePartIds, subDealerId);
        }


        [Query(HasSideEffects = true)]
        public IEnumerable<DealerPartsStock> 查询经销商配件批发价(int dealerId, int salesCategoryId, int[] sparePartId) {
            return new DealerPartsStockAch(this).查询经销商配件批发价(dealerId, salesCategoryId, sparePartId);
        }
        public IQueryable<DealerPartsStock> 查询经销商配件库存() {
            return new DealerPartsStockAch(this).查询经销商配件库存();
        }

        public IQueryable<VirtualDealerPartsStock> GetDealerPartsStocksWithSubDealer(string sparePartCode, bool? isDealerQuery, bool? isZero) {
            return new DealerPartsStockAch(this).GetDealerPartsStocksWithSubDealer(sparePartCode, isDealerQuery, isZero);
        }


        [Query(HasSideEffects = true)]
        public IQueryable<DealerPartsStock> GetDealerPartsStocksBySecondClassStationPlanAndSparePartIds(int firstClassStationId, int partsSalesCategoryId, int[] ids) {
            return new DealerPartsStockAch(this).GetDealerPartsStocksBySecondClassStationPlanAndSparePartIds(firstClassStationId, partsSalesCategoryId, ids);
        }


        [Query(HasSideEffects = true)]
        public IQueryable<DealerPartsStockForBussiness> 查询经销商配件零售价2(int dealerId, int salesCategoryId, int[] sparePartIds, int subDealerId) {
            return new DealerPartsStockAch(this).查询经销商配件零售价2(dealerId, salesCategoryId, sparePartIds, subDealerId);
        }


        public IQueryable<DealerPartsStockOutInRecord> 出入库记录查询(string billCode, int[] sparepartIds, DateTime? dateTimeBegin, DateTime? dateTimeEnd) {
            return new DealerPartsStockAch(this).出入库记录查询(billCode, sparepartIds, dateTimeBegin, dateTimeEnd);
        }
    }
}
