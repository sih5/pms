﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class EnterprisePartsCostAch : DcsSerivceAchieveBase {
        public EnterprisePartsCostAch(DcsDomainService domainService)
            : base(domainService) {
        }
        /// <summary>
        /// 获取指定的企业配件成本，并加数据库行级锁（待当前会话结束后释放）
        /// </summary>
        /// <param name="filter">PartsLockedStock的查询语句，或查询结果集</param>
        /// <returns></returns>
        internal IEnumerable<EnterprisePartsCost> GetEnterprisePartsCostsWithLock(IEnumerable<EnterprisePartsCost> filter) {
            if(filter == null)
                return Enumerable.Empty<EnterprisePartsCost>();

            //考虑到filter是本地过滤数据的可能性，使用Id关联代替对象关联
            var query = filter as IQueryable<EnterprisePartsCost>;
            var enterprisePartsCostIds = query == null ? filter.Select(v => v.Id) : query.Select(v => v.Id);

            var objectQuery = ObjectContext.EnterprisePartsCosts.Where(v => enterprisePartsCostIds.Contains(v.Id)) as ObjectQuery<EnterprisePartsCost>;
            if(objectQuery == null)
                return Enumerable.Empty<EnterprisePartsCost>();

            var sql = string.Format("select * from ({0}) for update", objectQuery.ToTraceString());
#if SqlServer
            var paramList = objectQuery.Parameters;
#else
            var paramList = objectQuery.Parameters.Select(v => new Devart.Data.Oracle.OracleParameter(v.Name, v.Value) as object).ToArray();
#endif
            return ObjectContext.ExecuteStoreQuery<EnterprisePartsCost>(sql, "EnterprisePartsCosts", MergeOption.AppendOnly, paramList);
        }

        internal void InsertEnterprisePartsCostValidate(EnterprisePartsCost enterprisePartsCost) {
            var dbenterprisePartsCost = ObjectContext.EnterprisePartsCosts.Where(r => r.OwnerCompanyId == enterprisePartsCost.OwnerCompanyId && r.SparePartId == enterprisePartsCost.SparePartId).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbenterprisePartsCost != null)
                throw new ValidationException(ErrorStrings.EnterprisePartsCost_Validation1);
            var userInfo = Utils.GetCurrentUserInfo();
            enterprisePartsCost.CreatorId = userInfo.Id;
            enterprisePartsCost.CreatorName = userInfo.Name;
            enterprisePartsCost.CreateTime = DateTime.Now;
        }

        internal void UpdateEnterprisePartsCostValidate(EnterprisePartsCost enterprisePartsCost) {
            if(enterprisePartsCost.Quantity < 0)
                throw new ValidationException(ErrorStrings.EnterprisePartsCost_Validation2);
            var dbenterprisePartsCost = ObjectContext.EnterprisePartsCosts.Where(r => r.Id != enterprisePartsCost.Id && r.OwnerCompanyId == enterprisePartsCost.OwnerCompanyId && r.SparePartId == enterprisePartsCost.SparePartId).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbenterprisePartsCost != null)
                throw new ValidationException(ErrorStrings.EnterprisePartsCost_Validation1);
            var userInfo = Utils.GetCurrentUserInfo();
            enterprisePartsCost.ModifierId = userInfo.Id;
            enterprisePartsCost.ModifierName = userInfo.Name;
            enterprisePartsCost.ModifyTime = DateTime.Now;
        }

        public void UpdateEnterprisePartsCost(EnterprisePartsCost enterprisePartsCost) {
            UpdateToDatabase(enterprisePartsCost);
            this.UpdateEnterprisePartsCostValidate(enterprisePartsCost);
        }

        public void InsertEnterprisePartsCost(EnterprisePartsCost enterprisePartsCost) {
            InsertToDatabase(enterprisePartsCost);
            this.InsertEnterprisePartsCostValidate(enterprisePartsCost);
        }

        public IEnumerable<VirtualEnterprisePartsCost> 查询企业配件成本( string sparePartCode, string sparePartName, bool? quantity, string costPrice, string costAmount,int? partABC, DateTime? bCreateTime, DateTime? eCreateTime, DateTime? bModifyTime, DateTime? eModifyTime)
        {
            string SQL = @"select e.Id,e.OwnerCompanyId,e.PartsSalesCategoryId,psc.name as PartsSalesCategoryName,e.SparePartId,sp.PartName as SparePartName,sp.PartCode as SparePartCode,sp.ReferenceCode,e.Quantity,e.CostAmount,e.CostPrice,e.CreatorId,e.CreatorName,e.CreateTime,e.ModifierId,e.ModifierName,e.ModifyTime,sp.partabc
                            from EnterprisePartsCost e inner join partsbranch sp on sp.partid=e.sparepartid and sp.status =1 inner join PartsSalesCategory psc on psc.id=e.partssalescategoryid 
                            where 1=1 ";
            SQL = SQL + " and e.OwnerCompanyId =" + Utils.GetCurrentUserInfo().EnterpriseId;

          
            if (!string.IsNullOrEmpty(sparePartCode))
            {
                SQL = SQL + " and sp.partcode like '%" + sparePartCode + "%'";
            }
            if (partABC.HasValue)
            {
                SQL = SQL + " and sp.partabc = " + partABC + "";
            }
            if (!string.IsNullOrEmpty(sparePartName))
            {
                SQL = SQL + " and sp.partname like '%" + sparePartName + "%'";
            }

            if (quantity != null)
            {
                if ((bool)quantity)
                {
                    SQL = SQL + " and e.quantity > 0";
                }
                else {
                    SQL = SQL + " and e.quantity <= 0";
                }
            }

            if (!string.IsNullOrEmpty(costPrice))
            {
                SQL = SQL + " and to_char(e.costPrice,'fm99990.0099') like '%" + costPrice + "%'";
            }
            if (!string.IsNullOrEmpty(costAmount))
            {
                SQL = SQL + " and to_char(e.costAmount,'fm99990.0099') like '%" + costAmount + "%'";
            }
            if (bCreateTime.HasValue)
            {
                SQL = SQL + " and e.CreateTime>= to_date('" + bCreateTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if (eCreateTime.HasValue)
            {
                SQL = SQL + " and e.CreateTime<= to_date('" + eCreateTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if (bModifyTime.HasValue)
            {
                SQL = SQL + " and e.CreateTime>= to_date('" + bModifyTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if (eModifyTime.HasValue)
            {
                SQL = SQL + " and e.CreateTime<= to_date('" + eModifyTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            var search = ObjectContext.ExecuteStoreQuery<VirtualEnterprisePartsCost>(SQL).ToList();
            return search;
        }
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:32
        //原分布类的函数全部转移到Ach                                                               
        public void UpdateEnterprisePartsCost(EnterprisePartsCost enterprisePartsCost) {
            new EnterprisePartsCostAch(this).UpdateEnterprisePartsCost(enterprisePartsCost);
        }

        public void InsertEnterprisePartsCost(EnterprisePartsCost enterprisePartsCost) {
            new EnterprisePartsCostAch(this).InsertEnterprisePartsCost(enterprisePartsCost);
        }
        public IEnumerable<VirtualEnterprisePartsCost> 查询企业配件成本( string sparePartCode, string sparePartName, bool? quantity, string costPrice, string costAmount,int? partABC, DateTime? bCreateTime, DateTime? eCreateTime, DateTime? bModifyTime, DateTime? eModifyTime)
        {
            return new EnterprisePartsCostAch(this).查询企业配件成本( sparePartCode, sparePartName, quantity, costPrice, costAmount,partABC, bCreateTime, eCreateTime, bModifyTime, eModifyTime);
        }
    }
}
