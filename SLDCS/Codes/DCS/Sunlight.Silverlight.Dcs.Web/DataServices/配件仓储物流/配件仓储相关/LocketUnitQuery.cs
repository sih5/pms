﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web
{
    partial class DcsDomainService
    {
        public IQueryable<LocketUnitQuery> 查询锁定单位() {
            return ObjectContext.LocketUnitQueries.OrderBy(r => r.Code);
        }
    }
}