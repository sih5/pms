﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsBatchRecordAch : DcsSerivceAchieveBase {
        public PartsBatchRecordAch(DcsDomainService domainService)
            : base(domainService) {
        }

        internal void InsertPartsBatchRecordValidate(PartsBatchRecord partsBatchRecord) {
            if(string.IsNullOrWhiteSpace(partsBatchRecord.BatchNumber) || partsBatchRecord.BatchNumber == GlobalVar.ASSIGNED_BY_SERVER)
                partsBatchRecord.BatchNumber = CodeGenerator.Generate("PartsBatchRecord", partsBatchRecord.BatchNumberDate);
            var dbPartsBatchRecord = ObjectContext.PartsBatchRecords.Where(r => r.BatchNumber.ToLower() == partsBatchRecord.BatchNumber.ToLower() && r.Status != (int)DcsBaseDataStatus.作废).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbPartsBatchRecord != null)
                throw new ValidationException(string.Format(ErrorStrings.PartsBatchRecord_Validation1));
            var userInfo = Utils.GetCurrentUserInfo();
            partsBatchRecord.CreatorId = userInfo.Id;
            partsBatchRecord.CreatorName = userInfo.Name;
            partsBatchRecord.CreateTime = DateTime.Now;
            partsBatchRecord.CreatorOwnerCompanyCode = userInfo.EnterpriseCode;
            partsBatchRecord.CreatorOwnerCompanyId = userInfo.EnterpriseId;
        }
        internal void UpdatePartsBatchRecordValidate(PartsBatchRecord partsBatchRecord) {
            var dbPartsBatchRecord = ObjectContext.PartsBatchRecords.Where(r => r.BatchNumber.ToLower() == partsBatchRecord.BatchNumber.ToLower() && r.Id != partsBatchRecord.Id && r.Status != (int)DcsBaseDataStatus.作废).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbPartsBatchRecord != null)
                throw new ValidationException(string.Format(ErrorStrings.PartsBatchRecord_Validation1));
        }
        public void UpdatePartsBatchRecord(PartsBatchRecord partsBatchRecord) {
            UpdateToDatabase(partsBatchRecord);
            this.UpdatePartsBatchRecordValidate(partsBatchRecord);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:32
        //原分布类的函数全部转移到Ach                                                               

        public void UpdatePartsBatchRecord(PartsBatchRecord partsBatchRecord) {
            new PartsBatchRecordAch(this).UpdatePartsBatchRecord(partsBatchRecord);
        }
    }
}
