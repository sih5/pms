﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsLockedStockAch : DcsSerivceAchieveBase {
        public PartsLockedStockAch(DcsDomainService domainService)
            : base(domainService) {
        }

        internal void InsertPartsLockedStockValidate(PartsLockedStock partsLockedStock) {
            var dbpartsLockedStock = ObjectContext.PartsLockedStocks.Where(r => r.WarehouseId == partsLockedStock.WarehouseId && r.PartId == partsLockedStock.PartId).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbpartsLockedStock != null)
                throw new ValidationException(ErrorStrings.PartsLockedStock_Validation1);
        }
        internal void UpdatePartsLockedStockValidate(PartsLockedStock partsLockedStock) {
            var dbpartsLockedStock = ObjectContext.PartsLockedStocks.Where(r => r.Id != partsLockedStock.Id && r.WarehouseId == partsLockedStock.WarehouseId && r.PartId == partsLockedStock.PartId).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbpartsLockedStock != null)
                throw new ValidationException(ErrorStrings.PartsLockedStock_Validation1);
        }
        /// <summary>
        /// 获取指定的配件锁定库存，并加数据库行级锁（待当前会话结束后释放）
        /// </summary>
        /// <param name="filter">PartsLockedStock的查询语句，或查询结果集</param>
        /// <returns></returns>
        internal IEnumerable<PartsLockedStock> GetPartsLockedStocksWithLock(IEnumerable<PartsLockedStock> filter) {
            if(filter == null)
                return Enumerable.Empty<PartsLockedStock>();
            //考虑到filter是本地过滤数据的可能性，使用Id关联代替对象关联
            var query = filter as IQueryable<PartsLockedStock>;
            var partsLockedStockIds = query == null ? filter.Select(v => v.Id) : query.Select(v => v.Id);
            var objectQuery = ObjectContext.PartsLockedStocks.Where(v => partsLockedStockIds.Contains(v.Id)) as ObjectQuery<PartsLockedStock>;
            if(objectQuery == null)
                return Enumerable.Empty<PartsLockedStock>();
            var sql = string.Format("select * from ({0}) for update", objectQuery.ToTraceString());
#if SqlServer
            var paramList = objectQuery.Parameters;
#else
            var paramList = objectQuery.Parameters.Select(v => new Devart.Data.Oracle.OracleParameter(v.Name, v.Value) as object).ToArray();
#endif
            return ObjectContext.ExecuteStoreQuery<PartsLockedStock>(sql, "PartsLockedStocks", MergeOption.AppendOnly, paramList);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:32
        //原分布类的函数全部转移到Ach                                                               
    //使用工具生成后没有任何共有方法 NoPublic
    }
}
