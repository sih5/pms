﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsStockAch : DcsSerivceAchieveBase {
        internal void InsertPartsStockValidate(PartsStock partsStock) {
            var dbpartsStock = ObjectContext.PartsStocks.Where(r => r.WarehouseAreaId == partsStock.WarehouseAreaId && r.PartId == partsStock.PartId).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbpartsStock != null)
                throw new ValidationException(ErrorStrings.PartsStock_Validation1);
            var userInfo = Utils.GetCurrentUserInfo();
            partsStock.CreatorId = userInfo.Id;
            partsStock.CreatorName = userInfo.Name;
            partsStock.CreateTime = DateTime.Now;
        }
        internal void UpdatePartsStockValidate(PartsStock partsStock) {
            if(partsStock.Quantity < 0)
                throw new ValidationException(ErrorStrings.PartsStock_Validation2);
            var dbpartsStock = ObjectContext.PartsStocks.Where(r => r.Id != partsStock.Id && r.WarehouseAreaId == partsStock.WarehouseAreaId && r.PartId == partsStock.PartId).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsStock != null)
                throw new ValidationException(ErrorStrings.PartsStock_Validation1);
            var userInfo = Utils.GetCurrentUserInfo();
            partsStock.ModifierId = userInfo.Id;
            partsStock.ModifierName = userInfo.Name;
            partsStock.ModifyTime = DateTime.Now;
        }
        public void UpdatePartsStock(PartsStock partsStock) {
            UpdateToDatabase(partsStock);
            this.UpdatePartsStockValidate(partsStock);
        }
        public void InsertPartsStock(PartsStock partsStock) {
            InsertToDatabase(partsStock);
            this.InsertPartsStockValidate(partsStock);
        }


        [Query(HasSideEffects = true)]
        public IEnumerable<PartsStock> 上架查询配件定位存储(int warehouseId,int[] sparePartIds) {
            if(sparePartIds == null || sparePartIds.Length == 0)
                return Enumerable.Empty<PartsStock>();
            var user = Utils.GetCurrentUserInfo();
            var result = ObjectContext.PartsStocks.Where(v => sparePartIds.Contains(v.PartId)
                && v.WarehouseId == warehouseId && ObjectContext.WarehouseAreas.Any(area => area.Id == v.WarehouseAreaId && area.Status == (int)DcsBaseDataStatus.有效
                    && ObjectContext.WarehouseAreaManagers.Any(manager => manager.WarehouseAreaId == area.ParentId && manager.ManagerId == user.Id))).ToArray();
            if(result.Length > 0) {
                var warehouseAreaCategoryIds = result.Where(v => v.WarehouseAreaCategoryId.HasValue).Select(v => v.WarehouseAreaCategoryId.Value).ToArray();
                if(warehouseAreaCategoryIds.Length > 0) {
                    var warehouseAreaCategories = ObjectContext.WarehouseAreaCategories.Where(v => warehouseAreaCategoryIds.Contains(v.Id)).ToArray();
                }
                var warehouseAreaIds = result.Select(v => v.WarehouseAreaId).Distinct().ToArray();
                var warehouseAreas = ObjectContext.WarehouseAreas.Where(v => warehouseAreaIds.Contains(v.Id) && v.Status == (int)DcsBaseDataStatus.有效).ToArray();
            }
            return result.OrderBy(r => r.Id);
        }


        [Query(HasSideEffects = true)]
        public IEnumerable<PartsStock> 查询配件定位存储(int[] sparePartIds) {
            if(sparePartIds == null || sparePartIds.Length == 0)
                return Enumerable.Empty<PartsStock>();
            var user = Utils.GetCurrentUserInfo();
            var result = ObjectContext.PartsStocks.Where(v => sparePartIds.Contains(v.PartId)
                && ObjectContext.WarehouseAreas.Any(area => area.Id == v.WarehouseAreaId && area.Status == (int)DcsBaseDataStatus.有效
                    && ObjectContext.WarehouseAreaManagers.Any(manager => manager.WarehouseAreaId == area.ParentId && manager.ManagerId == user.Id))).ToArray();
            if(result.Length > 0) {
                var warehouseAreaCategoryIds = result.Where(v => v.WarehouseAreaCategoryId.HasValue).Select(v => v.WarehouseAreaCategoryId.Value).ToArray();
                if(warehouseAreaCategoryIds.Length > 0) {
                    var warehouseAreaCategories = ObjectContext.WarehouseAreaCategories.Where(v => warehouseAreaCategoryIds.Contains(v.Id)).ToArray();
                }
                var warehouseAreaIds = result.Select(v => v.WarehouseAreaId).Distinct().ToArray();
                var warehouseAreas = ObjectContext.WarehouseAreas.Where(v => warehouseAreaIds.Contains(v.Id) && v.Status == (int)DcsBaseDataStatus.有效).ToArray();
            }
            return result.OrderBy(r => r.Id);
        }
        public IQueryable<PartsStock> 查询仓库库位库存(int storageCompanyId, int? warehouseId) {
            var result = ObjectContext.PartsStocks.Where(r => r.StorageCompanyId == storageCompanyId);
            if(warehouseId.HasValue)
                result = result.Where(r => r.WarehouseId == warehouseId);
            return result.Include("Warehouse").Include("WarehouseArea").Include("WarehouseAreaCategory").Include("SparePart").OrderBy(r => r.Id);
        }
        /// <summary>
        /// 获取指定的配件库存，并加数据库行级锁（待当前会话结束后释放）
        /// </summary>
        /// <param name="filter">PartsLockedStock的查询语句，或查询结果集</param>
        /// <returns></returns>
        internal IEnumerable<PartsStock> GetPartsStocksWithLock(IEnumerable<PartsStock> filter) {
            if(filter == null)
                return Enumerable.Empty<PartsStock>();
            //考虑到filter是本地过滤数据的可能性，使用Id关联代替对象关联
            var query = filter as IQueryable<PartsStock>;
            var partsStockIds = query == null ? filter.Select(v => v.Id) : query.Select(v => v.Id);
            var objectQuery = ObjectContext.PartsStocks.Where(v => partsStockIds.Contains(v.Id)) as ObjectQuery<PartsStock>;
            if(objectQuery == null)
                return Enumerable.Empty<PartsStock>();
            var sql = string.Format("select * from ({0}) for update", objectQuery.ToTraceString());
#if SqlServer
            var paramList = objectQuery.Parameters;
#else
            var paramList = objectQuery.Parameters.Select(v => new Devart.Data.Oracle.OracleParameter(v.Name, v.Value) as object).ToArray();
#endif
            return ObjectContext.ExecuteStoreQuery<PartsStock>(sql, "PartsStocks", MergeOption.AppendOnly, paramList);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:32
        //原分布类的函数全部转移到Ach                                                               

        public void UpdatePartsStock(PartsStock partsStock) {
            new PartsStockAch(this).UpdatePartsStock(partsStock);
        }

        public void InsertPartsStock(PartsStock partsStock) {
            new PartsStockAch(this).InsertPartsStock(partsStock);
        }

        [Query(HasSideEffects = true)]
        public IEnumerable<PartsStock> 上架查询配件定位存储(int warehouseId,int[] sparePartIds) {
            return new PartsStockAch(this).上架查询配件定位存储(warehouseId,sparePartIds);
        }
        [Query(HasSideEffects = true)]
        public IEnumerable<PartsStock> 查询配件定位存储(int[] sparePartIds) {
            return new PartsStockAch(this).查询配件定位存储(sparePartIds);
        }

        public IQueryable<PartsStock> 查询仓库库位库存(int storageCompanyId, int? warehouseId) {
            return new PartsStockAch(this).查询仓库库位库存(storageCompanyId,warehouseId);
        }
    }
}
