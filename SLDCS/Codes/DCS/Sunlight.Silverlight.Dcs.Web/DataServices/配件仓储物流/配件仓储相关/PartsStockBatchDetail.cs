﻿using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsStockBatchDetailAch : DcsSerivceAchieveBase {
        public PartsStockBatchDetailAch(DcsDomainService domainService)
            : base(domainService) {
        }

        internal void InsertPartsStockBatchDetailValidate(PartsStockBatchDetail partsStockBatchDetail) {
            var dbpartsStockBatchDetail = ObjectContext.PartsStockBatchDetails.Where(r => r.BatchNumber.ToLower() == partsStockBatchDetail.BatchNumber.ToLower()).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbpartsStockBatchDetail != null)
                throw new ValidationException(string.Format(ErrorStrings.PartsStockBatchDetail_Validation1, partsStockBatchDetail.BatchNumber));
        }
        internal void UpdatePartsStockBatchDetailValidate(PartsStockBatchDetail partsStockBatchDetail) {
            var dbpartsStockBatchDetail = ObjectContext.PartsStockBatchDetails.Where(r => r.Id != partsStockBatchDetail.Id && r.BatchNumber.ToLower() == partsStockBatchDetail.BatchNumber.ToLower()).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsStockBatchDetail != null)
                throw new ValidationException(string.Format(ErrorStrings.PartsStockBatchDetail_Validation1, partsStockBatchDetail.BatchNumber));
        }
        public IQueryable<PartsStockBatchDetail> 查询配件库存及批次明细(int? partId) {
            IQueryable<PartsStockBatchDetail> result = ObjectContext.PartsStockBatchDetails;
            if(partId.HasValue)
                result = result.Where(r => r.PartId == partId);
            return result.Include("PartsStock").Include("PartsStock.Warehouse").Include("PartsStock.WarehouseArea").OrderBy(r => r.Id);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:32
        //原分布类的函数全部转移到Ach                                                               

        public IQueryable<PartsStockBatchDetail> 查询配件库存及批次明细(int? partId) {
            return new PartsStockBatchDetailAch(this).查询配件库存及批次明细(partId);
        }
    }
}
