﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel.DomainServices.Server;
using System.ComponentModel.DataAnnotations;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web
{
    partial class PartsStockHistoryAch : DcsSerivceAchieveBase
    {
        public PartsStockHistoryAch(DcsDomainService domainService): base(domainService){
        }

        public IEnumerable<VirtualPartsStockHistory> 查询库存变更日志( int? warehouseId, string sparePartCode, string sparePartName, string warehouseAreaCode,int? warehouseAreaCategory,
            DateTime? bCreateTime, DateTime? eCreateTime)
        {
            string SQL = @"select a.Id,a.PartsStockId,a.WarehouseId,b.name as WarehouseName,b.code as WarehouseCode,a.StorageCompanyId,a.StorageCompanyType,a.BranchId,a.WarehouseAreaId,d.code as WarehouseAreaCode,
                            a.WarehouseAreaCategoryId,e.category as WarehouseAreaCategory,a.PartId,c.code as SparePartCode,c.name as SparePartName,c.ReferenceCode,a.Quantity,a.Remark,a.CreatorId,a.CreatorName,a.CreateTime,
                            a.ModifierId,a.ModifierName,a.ModifyTime,a.LockedQty,a.OutingQty,a.OldQuantity,(a.quantity - a.oldquantity) as ChangedQuantity
                              from PartsStockHistory a
                             inner join warehouse b
                                on a.warehouseid = b.id
                             inner join sparepart c
                                on c.id = a.partid
                             inner join warehousearea d
                                on a.WarehouseAreaId = d.id
                             inner join WarehouseAreaCategory e
                                on e.id = a.WarehouseAreaCategoryId where a.storagecompanyid = "+ Utils.GetCurrentUserInfo().EnterpriseId;

            if (!string.IsNullOrEmpty(sparePartCode))
            {
                SQL = SQL + " and c.Code like '%" + sparePartCode + "%'";
            }
            if (!string.IsNullOrEmpty(sparePartName))
            {
                SQL = SQL + " and c.Name like '%" + sparePartName + "%'";
            }
            if (!string.IsNullOrEmpty(warehouseAreaCode))
            {
                SQL = SQL + " and d.Code like '%" + warehouseAreaCode + "%'";
            }
            if (warehouseId.HasValue)
            {
                SQL = SQL + " and a.warehouseId =" + warehouseId;
            }
            if (warehouseAreaCategory.HasValue)
            {
                SQL = SQL + " and e.Category =" + warehouseAreaCategory + "";
            }
            if (bCreateTime.HasValue)
            {
                SQL = SQL + " and a.CreateTime>= to_date('" + bCreateTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if (eCreateTime.HasValue)
            {
                SQL = SQL + " and a.CreateTime<= to_date('" + eCreateTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            SQL = SQL + " order by a.createtime desc";

            var search = ObjectContext.ExecuteStoreQuery<VirtualPartsStockHistory>(SQL).ToList();
            return search;
        }

    }

    partial class DcsDomainService
    {// 已经使用工具进行处理 2017/4/29 12:12:33
        //原分布类的函数全部转移到Ach                                                               
        public IEnumerable<VirtualPartsStockHistory> 查询库存变更日志( int? warehouseId, string sparePartCode, string sparePartName, string warehouseAreaCode,int? warehouseAreaCategory,
            DateTime? bCreateTime, DateTime? eCreateTime)
        {
            return new PartsStockHistoryAch(this).查询库存变更日志(warehouseId, sparePartCode, sparePartName, warehouseAreaCode, warehouseAreaCategory,bCreateTime, eCreateTime);
        }
    }
}
