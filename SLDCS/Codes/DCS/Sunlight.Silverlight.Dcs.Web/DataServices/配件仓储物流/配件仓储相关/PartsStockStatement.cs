﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel.DomainServices.Server;
using System.ComponentModel.DataAnnotations;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web
{
    partial class PartsStockStatementAch : DcsSerivceAchieveBase
    {
        public PartsStockStatementAch(DcsDomainService domainService): base(domainService){
        }

        public IEnumerable<PartsStockStatement> 查询库存对账报表 (int? warehouseId, string sparePartCode, string sparePartName, string referenceCode,DateTime? beginTime, DateTime? endTime)
        {
            var enterpriseId = Utils.GetCurrentUserInfo().EnterpriseId;
            if (beginTime.HasValue && endTime.HasValue) {
                if (beginTime.Value.Date > DateTime.Now.Date || endTime.Value.Date > DateTime.Now.Date) {
                    return null;
                }
            }
            string SQL = @"select rownum as Id,warehouse.Id as warehouseId ,warehouse.code as warehousecode ,warehouse.name as warehousename ,sp.code as sparepartcode,sp.name as sparepartname,
                            sp.referencecode ,d.qtycs as beginquantity,-b.oqty as outquantity,c.inqty as inquantity,pd.qtypd as InventoryQuantity,a.qtynow as EndQuantity  from 
                            ({0})a 
                            left join (
                            select b.warehouseid,a.sparepartid,sum(a.outboundamount)oqty from partsoutboundbilldetail a 
                            inner join partsoutboundbill b on a.partsoutboundbillid = b.id
                            where 1=1 {1} 
                            group by b.warehouseid,a.sparepartid)b on a.partid = b.sparepartid and a.warehouseid = b.warehouseid 
                            left join (select b.warehouseid,a.sparepartid,sum(a.inspectedquantity) inqty from partsinboundcheckbilldetail a 
                            inner join partsinboundcheckbill b on a.partsinboundcheckbillid = b.id
                            where 1=1 {2}
                            group by b.warehouseid,a.sparepartid)c on a.warehouseid = c.warehouseid and a.partid = c.sparepartid
                            left join(select a.warehouseid,a.partid,sum(a.quantity)qtycs from partsstock_bak  a 
                            inner join warehouse b on a.warehouseid = b.id
                            where 1=1 {3}
                            group by a.warehouseid,a.partid
                            )d on d.partid = a.partid and d.warehouseid = a.warehouseid 
                            left join (select a.warehouseid,b.sparepartid,sum(b.storagedifference)qtypd from  partsinventorybill a 
                            inner join partsinventorydetail b on a.id = b.partsinventorybillid
                            where 1=1 {4}
                            and a.status = 3 group by a.warehouseid,b.sparepartid) pd on pd.warehouseid = a.warehouseid and pd.sparepartid = a.partid
                            inner join warehouse on a.warehouseid = warehouse.id
                            inner join sparepart sp on sp.id = a.partid
                            where 1=1 " ;

            string param0 = "select warehouseid,partid,sum(quantity)qtynow from partsstock where 1=1 and StorageCompanyId = " + enterpriseId;
            string param1 = string.Empty;
            string param2 = string.Empty;
            string param3 = string.Empty;
            string param4 = string.Empty;
            string part1 = "select warehouseid,partid,sum(quantity)qtynow from partsstock_bak where 1=1 and StorageCompanyId = " + enterpriseId +"{0} ";
            var dateTemp = new DateTime(2018, 10, 20, 0, 0, 0);
            if (beginTime.HasValue)
            {
                if (beginTime.Value.Date < dateTemp) {
                    beginTime = new DateTime(2018, 10, 8, 0, 0, 0);
                } else {
                    beginTime = new DateTime(beginTime.Value.Year, beginTime.Value.Month, beginTime.Value.Day, 0, 0, 0);
                }
                var begin = beginTime.Value.AddDays(-1);
                param1 += " and b.StorageCompanyId = "+enterpriseId+" and b.createtime>to_date('"+beginTime.Value.ToString()+"','yyyy-mm-dd hh24:mi:ss')";
                param3 += " and a.StorageCompanyId = "+enterpriseId+" and to_date(to_char(a.thedate,'yyyy-mm-dd'),'yyyy-mm-dd') = to_date('"+begin.Date.ToString()+"','yyyy-mm-dd hh24:mi:ss')";
                param4 += " and a.StorageCompanyId = "+enterpriseId+" and a.InventoryRecordTime>to_date('"+beginTime.Value.ToString()+"','yyyy-mm-dd hh24:mi:ss')"; 
            }
            if (endTime.HasValue)
            {
                endTime = new DateTime(endTime.Value.Year, endTime.Value.Month,endTime.Value.Day, 23, 59, 59);
                param1 += " and b.createtime<=to_date('"+endTime.Value.ToString()+"','yyyy-mm-dd hh24:mi:ss')";
                param4 += " and a.InventoryRecordTime<=to_date('"+endTime.Value.ToString()+"','yyyy-mm-dd hh24:mi:ss')"; 

                if (endTime.Value.Date < DateTime.Now.Date) {
                    param0 = string.Format(part1, " and to_date(to_char(thedate,'yyyy-mm-dd'),'yyyy-mm-dd') = to_date('"+endTime.Value.Date.ToString()+"','yyyy-mm-dd hh24:mi:ss')");
                }
            }
            param2 = param1;
            //if (warehouseId.HasValue)
            //{
            //    param0 += " and warehouseid =" + warehouseId;
            //    param1 += " and b.warehouseid =" + warehouseId;
            //    param2 += " and b.warehouseid =" + warehouseId;
            //    param3 += " and a.warehouseid =" + warehouseId;
            //    param4 += " and a.warehouseid =" + warehouseId;
            //}
            
            if (!string.IsNullOrEmpty(sparePartCode) || !string.IsNullOrEmpty(sparePartName) || !string.IsNullOrEmpty(referenceCode)) { 
                param0 += " and exists(select 1 from sparepart where 1=1 {0} and id = partid)";
                param1 += " and exists(select 1 from sparepart where 1=1 {0} and id = a.sparepartid)";
                param2 += " and exists(select 1 from sparepart where 1=1 {0} and id = a.sparepartid)";
                param3 += " and exists(select 1 from sparepart where 1=1 {0} and id = a.partid)";
                param4 += " and exists(select 1 from sparepart where 1=1 {0} and id = b.sparepartid)";
                string partSearchParam0 = string.Empty;
                string partSearchParam1 = string.Empty;
                string partSearchParam2 = string.Empty;
                string partSearchParam3 = string.Empty;
                string partSearchParam4 = string.Empty;

                if (!string.IsNullOrEmpty(sparePartCode)) {
                    partSearchParam0 += " and code = '" + sparePartCode + "'";
                    partSearchParam1 += " and code = '" + sparePartCode + "'";
                    partSearchParam2 += " and code = '" + sparePartCode + "'";
                    partSearchParam3 += " and code = '" + sparePartCode + "'";
                    partSearchParam4 += " and code = '" + sparePartCode + "'";
                }
                if (!string.IsNullOrEmpty(sparePartName)) {
                    partSearchParam0 += " and name like '%" + sparePartName + "%'";
                    partSearchParam1 += " and name like '%" + sparePartName + "%'";
                    partSearchParam2 += " and name like '%" + sparePartName + "%'";
                    partSearchParam3 += " and name like '%" + sparePartName + "%'";
                    partSearchParam4 += " and name like '%" + sparePartName + "%'";
                }
                if (!string.IsNullOrEmpty(referenceCode)) {
                    partSearchParam0 += " and referenceCode = '" + referenceCode + "'";
                    partSearchParam1 += " and referenceCode = '" + referenceCode + "'";
                    partSearchParam2 += " and referenceCode = '" + referenceCode + "'";
                    partSearchParam3 += " and referenceCode = '" + referenceCode + "'";
                    partSearchParam4 += " and referenceCode = '" + referenceCode + "'";
                }
                param0 = string.Format(param0, partSearchParam0);
                param1 = string.Format(param1, partSearchParam1);
                param2 = string.Format(param2, partSearchParam2);
                param3 = string.Format(param3, partSearchParam3);
                param4 = string.Format(param4, partSearchParam4);
            }

            param0 += " group by warehouseid,partid";

            SQL = string.Format(SQL, param0,param1,param2,param3,param4);
            var search = ObjectContext.ExecuteStoreQuery<PartsStockStatement>(SQL).ToList();

            if (warehouseId.HasValue) {
               return search.Where(r => r.WarehouseId == warehouseId).ToList();
            }
            return search;
        }

    }

    partial class DcsDomainService
    {// 已经使用工具进行处理 2017/4/29 12:12:33
        //原分布类的函数全部转移到Ach                                                               
        public IEnumerable<PartsStockStatement> 查询库存对账报表 (int? warehouseId, string sparePartCode, string sparePartName, string referenceCode,DateTime? beginTime, DateTime? endTime)
        {
            return new PartsStockStatementAch(this).查询库存对账报表(warehouseId, sparePartCode, sparePartName, referenceCode,beginTime, endTime);
        }
    }
}
