﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Web;
using System.ServiceModel.DomainServices.Server;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class TraceWarehouseAgeAch : DcsSerivceAchieveBase {
        public TraceWarehouseAgeAch(DcsDomainService domainService)
            : base(domainService) {
        }
        public IEnumerable<TraceWarehouseAge> 查询配件追溯码库位库龄(string warehouseName, string warehouseAreaCode, string storehouseAreaCode, string sparePartCode, int? traceProperty, string traceCode, bool? greaterThanZero, int? kvAge, int? id, int? companyType) {
            var sql = @"select ff.*, trunc(sysdate) - trunc(ff.InBoundDate) as Age
                              from (select sp.code as SparePartCode,
                                           sp.name as SparePartName,
                                           pip.warehousename,
                                           pip.warehouseid,
                                           wa.code as  WarehouseAreaCode,
                                           watop.code as StorehouseAreaCode,
                                           act.traceproperty,
                                           kv.value as TracePropertyString,
                                           (act.InQty - nvl(act.OutQty, 0)) as InQty,
                                           psp.salesprice,
                                           act.tracecode,
                                           (select max(st.sihlabelcode)
                                              from accuratetrace st
                                             where st.tracecode = act.tracecode
                                               and st.companyid = act.companyid
                                               and st.status = 1) as sihlabelcode,
                                           pck.createtime as InBoundDate,
                                           act.Id,
                                           act.BoxCode,
                                           row_number() over(partition by act.partid, act.tracecode, act.companyid, act.warehouseareaid order by act.status desc) as day
                                      from accuratetrace act
                                      join sparepart sp
                                        on act.partid = sp.Id
                                      join partsinboundplan pip
                                        on act.sourcebillid = pip.id
                                      left join WarehouseArea wa
                                        on act.warehouseareaid = wa.id
                                      left join WarehouseArea watop
                                        on wa.toplevelwarehouseareaid = watop.id
                                      left join PartsSalesPrice psp
                                        on act.partid = psp.sparepartid
                                       and psp.status = 1
                                      join partsinboundcheckbill pck
                                        on pip.id = pck.partsinboundplanid
                                      left join keyvalueItem kv
                                        on act.traceproperty = kv.Key
                                       and kv.name = 'TraceProperty'
                                     where act.type = 2
                                       and act.companyType <> 3 and act.InQty>0 
                                       and act.TraceCode is not null
                                       and act.status <> 2 {0}) ff
                             where ff.day = 1 {1}";
            var searchSql = string.Empty;
            var searchSql1 = string.Empty;
            if(id.HasValue){
                searchSql += " and act.id=" + id ;
            }
            if(!string.IsNullOrEmpty(warehouseName)){
                searchSql += " and pip.warehouseName like'%"+warehouseName+"%'";
            }
            if(!string.IsNullOrEmpty(warehouseAreaCode)) {
                searchSql += " and wa.code like '%" + warehouseAreaCode + "%'";
            }
            if(!string.IsNullOrEmpty(storehouseAreaCode)) {
                searchSql += " and watop.code like '%" + storehouseAreaCode + "%'";
            }
            if(!string.IsNullOrEmpty(sparePartCode)) {
                searchSql += " and sp.code like '%" + sparePartCode + "%'";
            }
            if(traceProperty.HasValue) {
                searchSql += " and act.traceProperty=" + traceProperty;
            }
            if(!string.IsNullOrEmpty(traceCode)) {
                searchSql += " and act.traceCode like '%" + traceCode+"%'";
            }
            if(greaterThanZero.HasValue && greaterThanZero.Value) {
                searchSql += " and (act.InQty-nvl(act.OutQty,0))>0";
            }
            if(greaterThanZero.HasValue && !greaterThanZero.Value) {
                searchSql += " and (act.InQty-nvl(act.OutQty,0))<=0";
            }
            if(companyType.HasValue) {
                searchSql += " and act.CompanyType=" + companyType;
            }
            if(kvAge.HasValue) {
                switch(kvAge.Value) {
                    case 1:
                        searchSql1 = " and (trunc(sysdate) - trunc(ff.InBoundDate)) <=90";
                        break;
                    case 2:
                        searchSql1 = " and (trunc(sysdate) - trunc(ff.InBoundDate)) >90 and (trunc(sysdate) - trunc(ff.InBoundDate)) <=180";
                        break;
                    case 3:
                        searchSql1 = " and (trunc(sysdate) - trunc(ff.InBoundDate)) >=181 and (trunc(sysdate) - trunc(ff.InBoundDate)) <=360";
                        break;
                    case 4:
                        searchSql1 = " and (trunc(sysdate) - trunc(ff.InBoundDate)) >=361 and (trunc(sysdate) - trunc(ff.InBoundDate)) <=720";
                        break;
                    case 5:
                        searchSql1 = " and (trunc(sysdate) - trunc(ff.InBoundDate)) >=721 and (trunc(sysdate) - trunc(ff.InBoundDate)) <=1080";
                        break;
                    case 6:
                        searchSql1 = " and (trunc(sysdate) - trunc(ff.InBoundDate)) >=1081 ";
                        break;
                }
              
            }
            var companyId = Utils.GetCurrentUserInfo().EnterpriseId;           
            var company = ObjectContext.Companies.Where(r => r.Id == companyId).ToArray().FirstOrDefault();
            if(company.Type == (int)DcsCompanyType.服务站兼代理库) {
                searchSql = " and act.companyId =" + Utils.GetCurrentUserInfo().EnterpriseId;
            } else if(company.Type == (int)DcsCompanyType.代理库) {
                searchSql = " and ((exists(select 1 from AgencyDealerRelation ag where ag.DealerId = act.companyid and ag.AgencyId= " + Utils.GetCurrentUserInfo().EnterpriseId + ")) or  act.companyId =" + Utils.GetCurrentUserInfo().EnterpriseId+")";
            }
            sql = string.Format(sql, searchSql, searchSql1);
            var search = ObjectContext.ExecuteStoreQuery<TraceWarehouseAge>(sql).ToList();
            return search;
        }
        public IEnumerable<TraceWarehouseAge> 查询服务站配件追溯码库位库龄(string sparePartCode, int? traceProperty, string traceCode, bool? greaterThanZero, int? kvAge, string companyCode, string companyName) {
            var sql = @"select ff.*, trunc(sysdate) - trunc(ff.InBoundDate) as Age
					  from (select sp.code as SparePartCode,
								   sp.name as SparePartName,
								   act.traceproperty,
								    (act.InQty-nvl(act.OutQty,0)) as InQty,
								   psp.salesprice,
								   act.tracecode,
								   act.sihlabelcode,
								   psp.createtime as InBoundDate,
								   act.Id,cp.code as CompanyCode,cp.name as CompanyName,
								   row_number() over(partition by act.partid, act.tracecode,act.companyid order by act.status desc) as day
							  from accuratetrace act
                              join Company cp on act.CompanyId=cp.id
							  join sparepart sp
								on act.partid = sp.id
							  join partsshippingorder psp
								on act.sourcebillid = psp.id
							  join DealerPartsStock psk
								on act.partid = psk.SparePartId
							   and act.companyid = psk.dealerid
							  join PartsSalesPrice psp
								on act.partid = psp.sparepartid
							   and psp.status = 1
							 where act.type = 2
							   and act.companyType = 3
							   and act.status in( 1,99) {0}) ff
					 where ff.day = 1 {1}";
            var searchSql = string.Empty;
            var searchSql1 = string.Empty;
            if(!string.IsNullOrEmpty(sparePartCode)) {
                searchSql += " and sp.code like '%" + sparePartCode + "%'";
            }
            if(traceProperty.HasValue) {
                searchSql += " and act.traceProperty=" + traceProperty;
            }
            if(!string.IsNullOrEmpty(traceCode)) {
                searchSql += " and act.traceCode like '%" + traceCode+"%'";
            }
            if(greaterThanZero.HasValue && greaterThanZero.Value) {
                searchSql += " and (act.InQty-nvl(act.OutQty,0))>0";
            }
            if(greaterThanZero.HasValue && !greaterThanZero.Value) {
                searchSql += " and (act.InQty-nvl(act.OutQty,0))<=0";
            }
            if(!string.IsNullOrEmpty(companyCode)) {
                searchSql += " and cp.code like '%" + companyCode + "%'";
            }
            if(!string.IsNullOrEmpty(companyName)) {
                searchSql += " and cp.Name like '%" + companyName + "%'";
            }
            if(kvAge.HasValue) {
                switch(kvAge.Value) {
                    case 1:
                        searchSql1 = " and (trunc(sysdate) - trunc(ff.InBoundDate)) <=90";
                        break;
                    case 2:
                        searchSql1 = " and (trunc(sysdate) - trunc(ff.InBoundDate)) >90 and (trunc(sysdate) - trunc(ff.InBoundDate)) <=180";
                        break;
                    case 3:
                        searchSql1 = " and (trunc(sysdate) - trunc(ff.InBoundDate)) >=181 and (trunc(sysdate) - trunc(ff.InBoundDate)) <=360";
                        break;
                    case 4:
                        searchSql1 = " and (trunc(sysdate) - trunc(ff.InBoundDate)) >=361 and (trunc(sysdate) - trunc(ff.InBoundDate)) <=720";
                        break;
                    case 5:
                        searchSql1 = " and (trunc(sysdate) - trunc(ff.InBoundDate)) >=721 and (trunc(sysdate) - trunc(ff.InBoundDate)) <=1080";
                        break;
                    case 6:
                        searchSql1 = " and (trunc(sysdate) - trunc(ff.InBoundDate)) >=1081 ";
                        break;
                }
              
            }
			var userInfo=Utils.GetCurrentUserInfo();
            var company = ObjectContext.Companies.Where(r => r.Id == userInfo.EnterpriseId).ToArray().FirstOrDefault();
            if(company.Type == (int)DcsCompanyType.服务站) {
                searchSql = " and act.companyId =" + userInfo.EnterpriseId;
            } else if(company.Type == (int)DcsCompanyType.代理库) {
                searchSql = " and (exists(select 1 from AgencyDealerRelation ag where ag.DealerId=cp.id and act.companyId= " + userInfo.EnterpriseId + "))";
            }else if(company.Type == (int)DcsCompanyType.分公司){
				 var isHas = ObjectContext.MarketDptPersonnelRelations.Where(r => r.PersonnelId == userInfo.Id && r.Status == (int)DcsBaseDataStatus.有效).Count();
                if(isHas > 0) {
                  searchSql +=" and  ( exists  (select 1 from MarketDptPersonnelRelation mr join Agency ag on mr.marketdepartmentid = ag.marketingdepartmentid join AgencyDealerRelation ar on ag.id = ar.agencyid   where mr.PersonnelId = "+userInfo.Id+"  and ar.dealerid = act.companyId  and bs.companytype = 2 and mr.status = 1))";
                }
			}
            sql = string.Format(sql, searchSql, searchSql1);
            var search = ObjectContext.ExecuteStoreQuery<TraceWarehouseAge>(sql).ToList();
            return search;
        }
		public IEnumerable<TraceWarehouseAge> 配件入库精确码物流跟踪( string sparePartCode, string counterpartCompanyCode, string traceCode, string counterpartCompanyName, string sIHLabelCode,DateTime? bInBoundDate,DateTime? eInBoundDate,string packingTaskCode,int? warehouseId,int? status ,string originalRequirementBillCode) {
            var sql = @"select sp.code as SparePartCode,
									   sp.name as SparePartName,
									   pip.CounterpartCompanyCode as SuplierCode,
									   pip.counterpartcompanyname as SuplierName,									 
									   pip.warehousecode,
									   pip.warehousename,
									   act.tracecode,
									   act.sihlabelcode,
									   pip.originalrequirementbillcode,
									   pip.sourcecode,
									   pt.code as PackingTaskCode,
									   pt.createtime as InBoundDate,
									   act.Id,
									   pip.inboundtype,
									   act.status,act.Inqty,act.OutQty
								  from accuratetrace act
								  join sparepart sp
									on act.partid = sp.id
								 left join partsinboundplan pip
									on act.sourcebillid = pip.id
								 left join PackingTask pt
									on act.partid = pt.sparepartid
								   and pt.partsinboundplanid = pip.id and act.packingtaskId=pt.id
								left  join PartsPurchasePlan pco
									on pip.originalrequirementbillid = pco.id								
								 where act.type = 2
								   and act.companyType = 1
								    {0}  ";
            var searchSql = string.Empty;
            if(!string.IsNullOrEmpty(sparePartCode)) {
                searchSql += " and sp.code like '%" + sparePartCode + "%'";
            }
			if(!string.IsNullOrEmpty(counterpartCompanyCode)) {
                searchSql += " and pip.CounterpartCompanyCode like '%" + counterpartCompanyCode + "%'";
            }
			if(!string.IsNullOrEmpty(counterpartCompanyName)) {
                searchSql += " and pip.counterpartCompanyName like '%" + counterpartCompanyName + "%'";
            }
			if(!string.IsNullOrEmpty(sIHLabelCode)){
				searchSql += " and act.sIHLabelCode like '%" + sIHLabelCode + "%'";
			}
			if(!string.IsNullOrEmpty(packingTaskCode)){
				searchSql += " and pt.Code like '%" + packingTaskCode + "%'";
			}           
            if(!string.IsNullOrEmpty(traceCode)) {
                searchSql += " and act.traceCode like '%" + traceCode+"%'";
            }
			
            if(warehouseId.HasValue ) {
                searchSql += " and pip.warehouseId=" +warehouseId;
            }
            if(status.HasValue ) {
                searchSql += " and act.status="+status;
            }            
			if (bInBoundDate.HasValue)
            {
                searchSql = searchSql + " and pt.CreateTime>= to_date('" + bInBoundDate.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if(eInBoundDate.HasValue)
            {
                searchSql = searchSql + " and pt.CreateTime<= to_date('" + eInBoundDate.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
			if(!string.IsNullOrEmpty(originalRequirementBillCode)){
				searchSql += " and pip.originalRequirementBillCode like '%" + originalRequirementBillCode + "%'";
			}
            sql = string.Format(sql, searchSql);
            var search = ObjectContext.ExecuteStoreQuery<TraceWarehouseAge>(sql).ToList();
            return search;
        }
		public IEnumerable<TraceWarehouseAge> 中心库配件入库精确码物流跟踪( string sparePartCode, string inBoundCode, string traceCode, string originalRequirementBillCode, string sIHLabelCode,DateTime? bInBoundDate,DateTime? eInBoundDate,string warehouseName,int? status,string sourceCode ) {
            var sql = @"select sp.code as SparePartCode,
								   sp.name as SparePartName,
								   pip.warehousecode,
								   pip.warehousename,
								   act.tracecode,
								   act.sihlabelcode,
								   pip.originalrequirementbillcode,
								   pip.sourcecode,
								   pck.createtime as InBoundDate,
								   act.Id,
								   act.status,
								   pck.code as InBoundCode,
                                   act.Inqty,act.OutQty
							  from accuratetrace act
							  join sparepart sp
								on act.partid = sp.id
							 left join partsinboundplan pip
								on act.sourcebillid = pip.id						
							 left join partsinboundcheckbill pck
								on pip.id = pck.partsinboundplanid
							 where act.type = 2
							   and act.companyType = 2 {0} ";
            var searchSql = string.Empty;
            if(!string.IsNullOrEmpty(sparePartCode)) {
                searchSql += " and sp.code like '%" + sparePartCode + "%'";
            }
			if(!string.IsNullOrEmpty(inBoundCode)) {
                searchSql += " and pck.code like '%" + inBoundCode + "%'";
            }
			if(!string.IsNullOrEmpty(warehouseName)) {
                searchSql += " and pip.warehouseName like '%" + warehouseName + "%'";
            }
			if(!string.IsNullOrEmpty(sIHLabelCode)){
				searchSql += " and act.sIHLabelCode like '%" + sIHLabelCode + "%'";
			}
			if(!string.IsNullOrEmpty(sourceCode)){
				searchSql += " and pip.sourceCode like '%" + sourceCode + "%'";
			}           
            if(!string.IsNullOrEmpty(traceCode)) {
                searchSql += " and act.traceCode like '%" + traceCode+"%'";
            }			         
            if(status.HasValue ) {
                searchSql += " and act.status="+status;
            }            
			if (bInBoundDate.HasValue)
            {
                searchSql = searchSql + " and pck.CreateTime>= to_date('" + bInBoundDate.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if(eInBoundDate.HasValue)
            {
                searchSql = searchSql + " and pck.CreateTime<= to_date('" + eInBoundDate.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
			if(!string.IsNullOrEmpty(originalRequirementBillCode)){
				searchSql += " and pip.originalRequirementBillCode like '%" + originalRequirementBillCode + "%'";
			}
			var userInfo=Utils.GetCurrentUserInfo();
            var company = ObjectContext.Companies.Where(r => r.Id == userInfo.EnterpriseId).ToArray().FirstOrDefault();
            if(company.Type == (int)DcsCompanyType.代理库) {
                searchSql = " and act.companyId =" + userInfo.EnterpriseId;
            }
            sql = string.Format(sql, searchSql);
            var search = ObjectContext.ExecuteStoreQuery<TraceWarehouseAge>(sql).ToList();
            return search;
        }
        public IEnumerable<TraceWarehouseAge> 查询SIH标签码库位库龄(string warehouseName, string warehouseAreaCode, string storehouseAreaCode, string sparePartCode, int? traceProperty, string sIHLabelCode, bool? greaterThanZero, int? kvAge, int? id) {
            var sql = @"select ff.*, trunc(sysdate) - trunc(ff.InBoundDate) as Age
                          from (select sp.code as SparePartCode,
                                       sp.name as SparePartName,
                                        nvl(pip.warehousename,wh.name) as warehousename,
                                        nvl(pip.warehouseid,wh.id) as warehouseid,
                                       wa.code as WarehouseAreaCode,
                                       watop.code as StorehouseAreaCode,
                                       act.traceproperty,
                                       kv.value as TracePropertyString,
                                       (act.InQty - nvl(act.OutQty, 0)) as InQty,
                                       psp.salesprice,
                                       act.tracecode,
                                       act.sihlabelcode,
                                       nvl(pck.createtime,act.createtime) as InBoundDate,
                                       act.Id,
                                       act.BoxCode,
                                       row_number() over(partition by act.partid, act.sihlabelcode, act.companyid, act.warehouseareaid order by act.status desc) as day
                                  from accuratetrace act
                                  join sparepart sp
                                    on act.partid = sp.Id
                                  left join partsinboundplan pip
                                    on act.sourcebillid = pip.id
                                  left join WarehouseArea wa
                                    on act.warehouseareaid = wa.id
                                  left join WarehouseArea watop
                                    on wa.toplevelwarehouseareaid = watop.id
                                  left join PartsSalesPrice psp
                                    on act.partid = psp.sparepartid
                                   and psp.status = 1
                                  left join partsinboundcheckbill pck
                                    on pip.id = pck.partsinboundplanid
                                  left join keyvalueItem kv
                                    on act.traceproperty = kv.Key
                                   and kv.name = 'TraceProperty'
                                 left join warehouse wh
                                    on wa.warehouseid = wh.id
                                 where act.type = 2
                                   and act.companyType = 1
                                   and act.status in( 1,99 ) and act.InQty>0
                                   and (act.tracecode is null or act.traceproperty is null) {0}) ff
                         where ff.day = 1 {1} ";
            var searchSql = string.Empty;
            var searchSql1 = string.Empty;
            if(id.HasValue) {
                searchSql += " and act.id=" + id;
            }
            if(!string.IsNullOrEmpty(warehouseName)) {
                searchSql += " and pip.warehouseName like'%" + warehouseName + "%'";
            }
            if(!string.IsNullOrEmpty(warehouseAreaCode)) {
                searchSql += " and wa.code like '%" + warehouseAreaCode + "%'";
            }
            if(!string.IsNullOrEmpty(storehouseAreaCode)) {
                searchSql += " and watop.code like '%" + storehouseAreaCode + "%'";
            }
            if(!string.IsNullOrEmpty(sparePartCode)) {
                searchSql += " and sp.code like '%" + sparePartCode + "%'";
            }
            if(traceProperty.HasValue) {
                searchSql += " and act.traceProperty=" + traceProperty;
            }
            if(!string.IsNullOrEmpty(sIHLabelCode)) {
                searchSql += " and act.sIHLabelCode like '%" + sIHLabelCode + "%'";
            }
            if(greaterThanZero.HasValue && greaterThanZero.Value) {
                searchSql += " and (act.InQty-nvl(act.OutQty,0))>0";
            }
            if(greaterThanZero.HasValue && !greaterThanZero.Value) {
                searchSql += " and (act.InQty-nvl(act.OutQty,0))<=0";
            }
            if(kvAge.HasValue) {
                switch(kvAge.Value) {
                    case 1:
                        searchSql1 = " and (trunc(sysdate) - trunc(ff.InBoundDate)) <=90";
                        break;
                    case 2:
                        searchSql1 = " and (trunc(sysdate) - trunc(ff.InBoundDate)) >90 and (trunc(sysdate) - trunc(ff.InBoundDate)) <=180";
                        break;
                    case 3:
                        searchSql1 = " and (trunc(sysdate) - trunc(ff.InBoundDate)) >=181 and (trunc(sysdate) - trunc(ff.InBoundDate)) <=360";
                        break;
                    case 4:
                        searchSql1 = " and (trunc(sysdate) - trunc(ff.InBoundDate)) >=361 and (trunc(sysdate) - trunc(ff.InBoundDate)) <=720";
                        break;
                    case 5:
                        searchSql1 = " and (trunc(sysdate) - trunc(ff.InBoundDate)) >=721 and (trunc(sysdate) - trunc(ff.InBoundDate)) <=1080";
                        break;
                    case 6:
                        searchSql1 = " and (trunc(sysdate) - trunc(ff.InBoundDate)) >=1081 ";
                        break;
                }

            }
            //var companyId = Utils.GetCurrentUserInfo().EnterpriseId;
            //var company = ObjectContext.Companies.Where(r => r.Id == companyId).ToArray().FirstOrDefault();
            //if(company.Type == (int)DcsCompanyType.服务站兼代理库) {
            //    searchSql = " and act.companyId =" + Utils.GetCurrentUserInfo().EnterpriseId;
            //} else if(company.Type == (int)DcsCompanyType.代理库) {
            //    searchSql = " and ((exists(select 1 from AgencyDealerRelation ag where ag.DealerId = act.companyid and ag.AgencyId= " + Utils.GetCurrentUserInfo().EnterpriseId + ")) or  act.companyId =" + Utils.GetCurrentUserInfo().EnterpriseId + ")";
            //}
            sql = string.Format(sql, searchSql, searchSql1);
            var search = ObjectContext.ExecuteStoreQuery<TraceWarehouseAge>(sql).ToList();
            return search;
        }
        public IEnumerable<TraceWarehouseAge> 查询可上架标签(int? partId,  string sIHLabelCode, string boxCode) {
            var sql = @"select sp.code as SparePartCode,
                               sp.name as SparePartName,
                               act.traceproperty,
                               (act.InQty - nvl(act.OutQty, 0)) as DownQty,
                               act.tracecode,
                               act.sihlabelcode,
                               act.Id,
                               act.BoxCode,act.partId
                          from accuratetrace act
                          join sparepart sp
                            on act.partid = sp.Id
                         where act.type = 2
                           and act.companyType = 1
                           and act.status = 1
                           and( (nvl(act.InQty,0) = 0 and act.traceproperty=1) or nvl(act.traceproperty,2)=2 ){0}";
            var searchSql = string.Empty;
            var searchSql1 = string.Empty;

            if(!string.IsNullOrEmpty(sIHLabelCode)) {
                searchSql += " and act.sIHLabelCode like'%" + sIHLabelCode + "%'";
            }
            if(!string.IsNullOrEmpty(boxCode)) {
                searchSql += " and act.BoxCode like '%" + boxCode + "%'";
            }                      
            if(partId.HasValue) {
                searchSql += " and act.partId=" + partId;
            }           
            sql = string.Format(sql, searchSql, searchSql1);
            var search = ObjectContext.ExecuteStoreQuery<TraceWarehouseAge>(sql).ToList();
            return search;
        }
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:33
        //原分布类的函数全部转移到Ach                                                               

        public IEnumerable<TraceWarehouseAge> 查询配件追溯码库位库龄(string warehouseName, string warehouseAreaCode, string storehouseAreaCode, string sparePartCode, int? traceProperty, string traceCode, bool? greaterThanZero, int? kvAge, int? id, int? companyType) {

            return new TraceWarehouseAgeAch(this).查询配件追溯码库位库龄(warehouseName, warehouseAreaCode, storehouseAreaCode, sparePartCode, traceProperty, traceCode, greaterThanZero, kvAge, id, companyType);
        }
        public IEnumerable<TraceWarehouseAge> 查询服务站配件追溯码库位库龄(string sparePartCode, int? traceProperty, string traceCode, bool? greaterThanZero, int? kvAge, string companyCode, string companyName) {
            return new TraceWarehouseAgeAch(this).查询服务站配件追溯码库位库龄(sparePartCode, traceProperty, traceCode, greaterThanZero, kvAge, companyCode, companyName);

		}
		public IEnumerable<TraceWarehouseAge> 配件入库精确码物流跟踪( string sparePartCode, string counterpartCompanyCode, string traceCode, string counterpartCompanyName, string sIHLabelCode,DateTime? bInBoundDate,DateTime? eInBoundDate,string packingTaskCode,int? warehouseId,int? status ,string originalRequirementBillCode) {
	        return new TraceWarehouseAgeAch(this).配件入库精确码物流跟踪(sparePartCode,counterpartCompanyCode,traceCode,counterpartCompanyName,sIHLabelCode,bInBoundDate,eInBoundDate,packingTaskCode,warehouseId,status,originalRequirementBillCode);
		}
		public IEnumerable<TraceWarehouseAge> 中心库配件入库精确码物流跟踪( string sparePartCode, string inBoundCode, string traceCode, string originalRequirementBillCode, string sIHLabelCode,DateTime? bInBoundDate,DateTime? eInBoundDate,string warehouseName,int? status,string sourceCode ) {
	        return new TraceWarehouseAgeAch(this).中心库配件入库精确码物流跟踪(sparePartCode,inBoundCode,traceCode,originalRequirementBillCode,sIHLabelCode,bInBoundDate,eInBoundDate,warehouseName,status,sourceCode);
        }
	    public IEnumerable<TraceWarehouseAge> 查询SIH标签码库位库龄(string warehouseName, string warehouseAreaCode, string storehouseAreaCode, string sparePartCode, int? traceProperty, string sIHLabelCode, bool? greaterThanZero, int? kvAge, int? id) {
            return new TraceWarehouseAgeAch(this).查询SIH标签码库位库龄(warehouseName, warehouseAreaCode, storehouseAreaCode, sparePartCode, traceProperty, sIHLabelCode, greaterThanZero, kvAge, id);
        }
        public IEnumerable<TraceWarehouseAge> 查询可上架标签(int? partId, string sIHLabelCode, string boxCode) {
            return new TraceWarehouseAgeAch(this).查询可上架标签(partId, sIHLabelCode, boxCode);

        }
    }
}
