﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Web;
using System.ServiceModel.DomainServices.Server;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class TraceWarehouseOutAch : DcsSerivceAchieveBase {
        public TraceWarehouseOutAch(DcsDomainService domainService)
            : base(domainService) {
        }
        public IEnumerable<TraceWarehouseOut> 配件出库精确码物流跟踪(int? warehouseId, string sparePartCode, string traceCode, string originalRequirementBillCode, string outCode, string counterpartCompanyName, int? outboundType, int? status, DateTime? bCreatetime, DateTime? eCreatetime) {
            var sql = @"select sp.code as SparePartCode,
                                       sp.name as SparePartName,
                                       pop.WarehouseId,
                                       pop.warehousecode,
                                       pop.warehousename,
                                       act.tracecode,
                                       act.sihlabelcode,
                                       pop.originalrequirementbillcode,
                                       pob.code as OutCode,
                                       act.status,
                                       pob.createtime as outTime,
                                       pop.CounterpartCompanyCode,
                                       pop.CounterpartCompanyName,
                                       act.id,
                                       pop.OutboundType,
                                       (select max(po.code)
                                          from PartsShippingOrderDetail pod
                                          join partsshippingorder po
                                            on pod.partsshippingorderid = po.id
                                         where pod.PartsOutboundPlanId = pop.id
                                           and pod.sparepartid = act.partid) as shippingCode
                                  from accuratetrace act
                                  join sparepart sp
                                    on act.partid = sp.id
                                  join PartsOutboundPlan pop
                                    on act.sourcebillid = pop.id
                                left  join PartsOutboundBill pob
                                    on pop.id = pob.partsoutboundplanid        
                                 where act.type = 1
                                   and act.companyType = 1
                                    {0}  ";
            var searchSql = string.Empty;
            if(bCreatetime.HasValue && bCreatetime.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
                bCreatetime = null;
            }
            if(bCreatetime.HasValue) {
                searchSql = searchSql + " and pop.CreateTime>= to_date('" + bCreatetime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if(eCreatetime.HasValue) {
                searchSql = searchSql + " and pop.CreateTime<= to_date('" + eCreatetime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if(warehouseId.HasValue) {
                searchSql += " and pop.WarehouseId =" + warehouseId ;
            }
            if(!string.IsNullOrEmpty(originalRequirementBillCode)) {
                searchSql += " and pop.originalrequirementbillcode like '%" + originalRequirementBillCode + "%'";
            }
            if(!string.IsNullOrEmpty(outCode)) {
                searchSql += " and pob.code like '%" + outCode + "%'";
            }
            if(!string.IsNullOrEmpty(sparePartCode)) {
                searchSql += " and sp.code like '%" + sparePartCode + "%'";
            }
            if(!string.IsNullOrEmpty(counterpartCompanyName)) {
                searchSql += " and pop.counterpartCompanyName like '%" + counterpartCompanyName + "%'";
            }
            if(!string.IsNullOrEmpty(traceCode)) {
                searchSql += " and act.traceCode like '%" + traceCode + "%'";
            }
            if(outboundType.HasValue) {
                searchSql += " and pop.outboundType =" + outboundType;
            }
            if(status.HasValue) {
                searchSql += " and  act.Status =" + status;
            }
            sql = string.Format(sql, searchSql);
            var search = ObjectContext.ExecuteStoreQuery<TraceWarehouseOut>(sql).ToList();
            return search;
        }
		public IEnumerable<TraceWarehouseOut> 中心库配件出库精确码物流跟踪(string warehouseName, string sparePartCode, string traceCode, string originalRequirementBillCode, string outCode, string counterpartCompanyCode, DateTime? bOutTime,DateTime? eOutTime) {
            var sql = @" select * from (select sp.code as SparePartCode,
						   sp.name as SparePartName,
						   pop.WarehouseId,
						   pop.warehousecode,
						   pop.warehousename,
						   act.tracecode,
						   act.sihlabelcode,
						   pop.originalrequirementbillcode,
						   pob.code as OutCode,
						   act.status,
						   pob.createtime as outTime,
						   pop.ReceivingCompanyCode as CounterpartCompanyCode,
						   pop.ReceivingCompanyName as CounterpartCompanyName,
						   act.id,
						   pop.OutboundType,
						   (select max(po.code)
							  from PartsShippingOrderDetail pod
							  join partsshippingorder po
								on pod.partsshippingorderid = po.id
							 where pod.PartsOutboundPlanId = pop.id
							   and pod.sparepartid = act.partid) as shippingCode
					  from accuratetrace act
					  join sparepart sp
						on act.partid = sp.id
					left  join PartsOutboundPlan pop
						on act.sourcebillid = pop.id
					left  join PartsOutboundBill pob
						on pop.id = pob.partsoutboundplanid
					 where act.type = 1
					   and act.companyType = 2
                                    {0} ) ff where 1=1 {1} ";
            var searchSql = string.Empty;
			var searchSql1 = string.Empty;
            if(!string.IsNullOrEmpty(warehouseName)) {
                searchSql += " and pop.warehouseName like '%" + warehouseName + "%'";
            }if(!string.IsNullOrEmpty(originalRequirementBillCode)) {
                searchSql += " and pop.originalrequirementbillcode like '%" + originalRequirementBillCode + "%'";
            }
            if(!string.IsNullOrEmpty(outCode)) {
                searchSql += " and pob.code like '%" + outCode + "%'";
            }
            if(!string.IsNullOrEmpty(sparePartCode)) {
                searchSql += " and sp.code like '%" + sparePartCode + "%'";
            }
            if(!string.IsNullOrEmpty(counterpartCompanyCode)) {
                searchSql += " and pop.counterpartCompanyCode like '%" + counterpartCompanyCode + "%'";
            }
            if(!string.IsNullOrEmpty(traceCode)) {
                searchSql += " and act.traceCode like '%" + traceCode + "%'";
            }
            if (bOutTime.HasValue)
            {
                searchSql = searchSql + " and  pob.createtime>= to_date('" + bOutTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if(eOutTime.HasValue)
            {
                searchSql = searchSql + " and  pob.createtime<= to_date('" + eOutTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
           var userInfo=Utils.GetCurrentUserInfo();
            var company = ObjectContext.Companies.Where(r => r.Id == userInfo.EnterpriseId).ToArray().FirstOrDefault();
            if(company.Type == (int)DcsCompanyType.服务站) {
                searchSql = " and pop.ReceivingCompanyId =" + userInfo.EnterpriseId;
            } else if(company.Type == (int)DcsCompanyType.代理库) {
                searchSql = "  and act.companyId= " + userInfo.EnterpriseId ;
            }else if(company.Type == (int)DcsCompanyType.分公司){
				 var isHas = ObjectContext.MarketDptPersonnelRelations.Where(r => r.PersonnelId == userInfo.Id && r.Status == (int)DcsBaseDataStatus.有效).Count();
                if(isHas > 0) {
                  searchSql +=" and  ( exists  (select 1 from MarketDptPersonnelRelation mr join Agency ag on mr.marketdepartmentid = ag.marketingdepartmentid join AgencyDealerRelation ar on ag.id = ar.agencyid   where mr.PersonnelId = "+userInfo.Id+"  and ar.dealerid = pop.ReceivingCompanyId  and bs.companytype = 2 and mr.status = 1))";
                }
			}
            sql = string.Format(sql, searchSql,searchSql1);
            var search = ObjectContext.ExecuteStoreQuery<TraceWarehouseOut>(sql).ToList();
            return search;
        }
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:33
        //原分布类的函数全部转移到Ach                                                               

        public IEnumerable<TraceWarehouseOut> 配件出库精确码物流跟踪(int? warehouseId, string sparePartCode, string traceCode, string originalRequirementBillCode, string outCode, string counterpartCompanyName, int? outboundType, int? status, DateTime? bCreatetime, DateTime? eCreatetime) {

            return new TraceWarehouseOutAch(this).配件出库精确码物流跟踪(warehouseId, sparePartCode, traceCode, originalRequirementBillCode, outCode, counterpartCompanyName, outboundType, status, bCreatetime, eCreatetime);
        }
		public IEnumerable<TraceWarehouseOut> 中心库配件出库精确码物流跟踪(string warehouseName, string sparePartCode, string traceCode, string originalRequirementBillCode, string outCode, string counterpartCompanyCode, DateTime? bOutTime,DateTime? eOutTime) {
            return new TraceWarehouseOutAch(this).中心库配件出库精确码物流跟踪(warehouseName, sparePartCode, traceCode, originalRequirementBillCode, outCode, counterpartCompanyCode, bOutTime, eOutTime);

		}
    }
}
