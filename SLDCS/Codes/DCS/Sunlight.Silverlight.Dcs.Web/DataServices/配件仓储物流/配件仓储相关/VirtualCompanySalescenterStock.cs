﻿using System.Linq;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class VirtualCompanySalescenterStockAch : DcsSerivceAchieveBase {
        public VirtualCompanySalescenterStockAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public IQueryable<VirtualCompanySalescenterStock> 查询企业销售中心库存(int companyId, int partsSalesCategoryId, int companyType) {
            var company = ObjectContext.Companies.SingleOrDefault(r => r.Id == companyId);
            if(company == null)
                return null;
            //去除报错，暂时把经销商类型改为服务站，待设计确认。
            if(companyType == (int)DcsCompanyType.服务站) {
                var dealerPartsStocks = ObjectContext.DealerPartsStocks.Where(r => r.DealerId == companyId && r.SalesCategoryId == partsSalesCategoryId).
                    GroupBy(v => new {
                        v.SparePartId,
                        v.BranchId,
                        v.SalesCategoryId,
                        v.SalesCategoryName,
                        v.SparePartCode
                    }).Select(r => new {
                        r.Key.SparePartId,
                        r.Key.BranchId,
                        r.Key.SalesCategoryId,
                        r.Key.SalesCategoryName,
                        r.Key.SparePartCode,
                        sumAmount = r.Sum(v => v.Quantity)
                    });
                var tempdealerPartsStocks = from a in dealerPartsStocks
                                            from b in ObjectContext.SpareParts
                                            where a.SparePartId == b.Id
                                            select new {
                                                a.SparePartId,
                                                a.BranchId,
                                                a.SalesCategoryId,
                                                a.SalesCategoryName,
                                                a.SparePartCode,
                                                a.sumAmount,
                                                SparePartName = b.Name
                                            };
                return (from dealerPartsStock in tempdealerPartsStocks
                        join branch in ObjectContext.Branches on dealerPartsStock.BranchId equals branch.Id
                        join partsSalesPrice in ObjectContext.PartsSalesPrices.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId) on dealerPartsStock.SparePartId equals partsSalesPrice.SparePartId
                        select new VirtualCompanySalescenterStock {
                            CompanyId = company.Id,
                            CompanyType = companyType,
                            CompanyCode = company.Code,
                            CompanyName = company.Name,
                            BranchId = branch.Id,
                            BranchCode = branch.Code,
                            BranchName = branch.Name,
                            PartsSalesCategoryId = partsSalesCategoryId,
                            PartsSalesCategoryName = dealerPartsStock.SalesCategoryName,
                            SparePartsId = dealerPartsStock.SparePartId,
                            SparePartsCode = dealerPartsStock.SparePartCode,
                            SparePartsName = dealerPartsStock.SparePartName,
                            FactAmount = dealerPartsStock.sumAmount,
                            UsableAmount = dealerPartsStock.sumAmount,
                            ProvinceName = company.ProvinceName,
                            CityName = company.CityName,
                            CountyName = company.CountyName,
                            PartsSalesPrice = partsSalesPrice.SalesPrice,
                            PartsSalesPriceId = partsSalesPrice.Id
                        }).OrderBy(r => r.CompanyId);
            }
            if(companyType == (int)DcsCompanyType.代理库 || companyType == (int)DcsCompanyType.分公司) {
                var partsStocks = ObjectContext.PartsStocks.Where(r => r.StorageCompanyId == company.Id && ObjectContext.WarehouseAreaCategories.Any(v => v.Id == r.WarehouseAreaCategoryId && (v.Category == (int)DcsAreaType.保管区 || v.Category == (int)DcsAreaType.检验区))).GroupBy(r => new {
                    r.StorageCompanyId,
                    r.BranchId,
                    r.WarehouseId,
                    r.PartId
                }).Select(r => new {
                    r.Key.PartId,
                    r.Key.BranchId,
                    r.Key.WarehouseId,
                    r.Key.StorageCompanyId,
                    StockAmount = r.Sum(v => v.Quantity)
                });
                var partsLockedStocks = ObjectContext.PartsLockedStocks.Where(r => r.StorageCompanyId == company.Id).GroupBy(r => new {
                    r.StorageCompanyId,
                    r.BranchId,
                    r.WarehouseId,
                    r.PartId
                }).Select(r => new {
                    r.Key.PartId,
                    r.Key.BranchId,
                    r.Key.WarehouseId,
                    r.Key.StorageCompanyId,
                    LockedAmount = (int?)r.Sum(v => v.LockedQuantity)
                });
                var wmsCongelationStockViews = ObjectContext.WmsCongelationStockViews.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId).GroupBy(r => new {
                    r.BranchId,
                    r.SparePartId,
                    r.SparePartCode,
                    r.SparePartName,
                    r.PartsSalesCategoryId,
                    r.PartsSalesCategoryName,
                    r.WarehouseId
                }).Select(r => new {
                    r.Key.SparePartId,
                    r.Key.BranchId,
                    r.Key.SparePartCode,
                    r.Key.SparePartName,
                    r.Key.PartsSalesCategoryId,
                    r.Key.PartsSalesCategoryName,
                    r.Key.WarehouseId,
                    SumCongelationStockQty = (int?)r.Sum(v => v.CongelationStockQty),
                    SumDisabledStock = (int?)r.Sum(v => v.DisabledStock)
                });
                var result = from partsStock in partsStocks
                             join partsLockedStock in partsLockedStocks on new {
                                 partsStock.PartId,
                                 partsStock.BranchId,
                                 partsStock.StorageCompanyId,
                                 partsStock.WarehouseId
                             } equals new {
                                 partsLockedStock.PartId,
                                 partsLockedStock.BranchId,
                                 partsLockedStock.StorageCompanyId,
                                 partsLockedStock.WarehouseId
                             } into tempTable1
                             from t1 in tempTable1.DefaultIfEmpty()
                             join wmsCongelationStockView in wmsCongelationStockViews on new {
                                 partsStock.PartId,
                                 partsStock.WarehouseId
                             } equals new {
                                 PartId = wmsCongelationStockView.SparePartId,
                                 wmsCongelationStockView.WarehouseId
                             } into tempTable2
                             from t2 in tempTable2.DefaultIfEmpty()
                             join branch in ObjectContext.Branches.Where(r => r.Status == (int)DcsMasterDataStatus.有效) on partsStock.BranchId equals branch.Id
                             join salesUnitAffiWarehouse in ObjectContext.SalesUnitAffiWarehouses on partsStock.WarehouseId equals salesUnitAffiWarehouse.WarehouseId
                             join salesUnit in ObjectContext.SalesUnits.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on salesUnitAffiWarehouse.SalesUnitId equals salesUnit.Id
                             join partsSalesPrice in ObjectContext.PartsSalesPrices on new {
                                 salesUnit.PartsSalesCategoryId,
                                 SparePartId = partsStock.PartId
                             } equals new {
                                 partsSalesPrice.PartsSalesCategoryId,
                                 partsSalesPrice.SparePartId
                             }
                             join sparePart in ObjectContext.SpareParts.Where(r => r.Status == (int)DcsMasterDataStatus.有效) on partsStock.PartId equals sparePart.Id
                             select new VirtualCompanySalescenterStock {
                                 CompanyId = company.Id,
                                 CompanyType = companyType,
                                 CompanyCode = company.Code,
                                 CompanyName = company.Name,
                                 BranchId = branch.Id,
                                 BranchCode = branch.Code,
                                 BranchName = branch.Name,
                                 PartsSalesCategoryId = t2.PartsSalesCategoryId,
                                 PartsSalesCategoryName = t2.PartsSalesCategoryName,
                                 SparePartsId = sparePart.Id,
                                 SparePartsCode = sparePart.Code,
                                 SparePartsName = sparePart.Name,
                                 FactAmount = partsStock.StockAmount,
                                 UsableAmount = partsStock.StockAmount - (t1.LockedAmount ?? 0) - (t2.SumCongelationStockQty ?? 0) - (t2.SumDisabledStock ?? 0),
                                 ProvinceName = company.ProvinceName,
                                 CityName = company.CityName,
                                 CountyName = company.CountyName,
                                 PartsSalesPrice = partsSalesPrice.SalesPrice,
                                 PartsSalesPriceId = partsSalesPrice.Id
                             };
                return result.OrderBy(r => r.CompanyId);
            }
            return null;
        }

        public IQueryable<VirtualCompanySalescenterStock> 查询企业销售中心库存累加(int companyId, int partsSalesCategoryId, int companyType) {
            var company = ObjectContext.Companies.SingleOrDefault(r => r.Id == companyId);
            if(company == null)
                return null;
            //去除报错，暂时把经销商类型改为服务站，待设计确认。
            if(companyType == (int)DcsCompanyType.服务站) {
                var dealerPartsStocks = ObjectContext.DealerPartsStocks.Where(r => r.DealerId == companyId && r.SalesCategoryId == partsSalesCategoryId).
                    GroupBy(v => new {
                        v.SparePartId,
                        v.BranchId,
                        v.SalesCategoryId,
                        v.SalesCategoryName,
                        v.SparePartCode
                    }).Select(r => new {
                        r.Key.SparePartId,
                        r.Key.BranchId,
                        r.Key.SalesCategoryId,
                        r.Key.SalesCategoryName,
                        r.Key.SparePartCode,
                        sumAmount = r.Sum(v => v.Quantity)
                    });
                var tempdealerPartsStocks = from a in dealerPartsStocks
                                            from b in ObjectContext.SpareParts
                                            where a.SparePartId == b.Id
                                            select new {
                                                a.SparePartId,
                                                a.BranchId,
                                                a.SalesCategoryId,
                                                a.SalesCategoryName,
                                                a.SparePartCode,
                                                a.sumAmount,
                                                SparePartName = b.Name
                                            };
                return (from dealerPartsStock in tempdealerPartsStocks
                        join branch in ObjectContext.Branches on dealerPartsStock.BranchId equals branch.Id
                        join partsSalesPrice in ObjectContext.PartsSalesPrices.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId) on dealerPartsStock.SparePartId equals partsSalesPrice.SparePartId
                        select new VirtualCompanySalescenterStock {
                            CompanyId = company.Id,
                            CompanyType = companyType,
                            CompanyCode = company.Code,
                            CompanyName = company.Name,
                            BranchId = branch.Id,
                            BranchCode = branch.Code,
                            BranchName = branch.Name,
                            PartsSalesCategoryId = partsSalesCategoryId,
                            PartsSalesCategoryName = dealerPartsStock.SalesCategoryName,
                            SparePartsId = dealerPartsStock.SparePartId,
                            SparePartsCode = dealerPartsStock.SparePartCode,
                            SparePartsName = dealerPartsStock.SparePartName,
                            FactAmount = dealerPartsStock.sumAmount,
                            UsableAmount = dealerPartsStock.sumAmount,
                            ProvinceName = company.ProvinceName,
                            CityName = company.CityName,
                            CountyName = company.CountyName,
                            PartsSalesPrice = partsSalesPrice.SalesPrice,
                            PartsSalesPriceId = partsSalesPrice.Id
                        }).OrderBy(r => r.CompanyId);
            }
            if(companyType == (int)DcsCompanyType.代理库 || companyType == (int)DcsCompanyType.分公司) {
                var partsStocks = ObjectContext.PartsStocks.Where(r => r.StorageCompanyId == company.Id && ObjectContext.WarehouseAreaCategories.Any(v => v.Id == r.WarehouseAreaCategoryId && (v.Category == (int)DcsAreaType.保管区 || v.Category == (int)DcsAreaType.检验区))).GroupBy(r => new {
                    r.BranchId,
                    r.PartId
                }).Select(r => new {
                    r.Key.PartId,
                    r.Key.BranchId,
                    StockAmount = r.Sum(v => v.Quantity)
                });
                var partsLockedStocks = ObjectContext.PartsLockedStocks.Where(r => r.StorageCompanyId == company.Id).GroupBy(r => new {
                    r.BranchId,
                    r.PartId
                }).Select(r => new {
                    r.Key.PartId,
                    r.Key.BranchId,
                    LockedAmount = (int?)r.Sum(v => v.LockedQuantity)
                });
                var wmsCongelationStockViews = ObjectContext.WmsCongelationStockViews.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId).GroupBy(r => new {
                    r.BranchId,
                    r.SparePartId,
                    r.SparePartCode,
                    r.SparePartName,
                    r.PartsSalesCategoryId,
                    r.PartsSalesCategoryName,
                }).Select(r => new {
                    r.Key.SparePartId,
                    r.Key.BranchId,
                    r.Key.SparePartCode,
                    r.Key.SparePartName,
                    r.Key.PartsSalesCategoryId,
                    r.Key.PartsSalesCategoryName,
                    SumCongelationStockQty = (int?)r.Sum(v => v.CongelationStockQty),
                    SumDisabledStock = (int?)r.Sum(v => v.DisabledStock)
                });
                var result = from partsStock in partsStocks
                             join partsLockedStock in partsLockedStocks on new {
                                 partsStock.PartId,
                                 partsStock.BranchId
                             } equals new {
                                 partsLockedStock.PartId,
                                 partsLockedStock.BranchId
                             } into tempTable1
                             from t1 in tempTable1.DefaultIfEmpty()
                             join wmsCongelationStockView in wmsCongelationStockViews on new {
                                 partsStock.PartId,
                             } equals new {
                                 PartId = wmsCongelationStockView.SparePartId
                             } into tempTable2
                             from t2 in tempTable2.DefaultIfEmpty()
                             join branch in ObjectContext.Branches.Where(r => r.Status == (int)DcsMasterDataStatus.有效) on partsStock.BranchId equals branch.Id
                             join partsSalesPrice in ObjectContext.PartsSalesPrices.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId) on partsStock.PartId equals partsSalesPrice.SparePartId
                             join partsSalesCategory in ObjectContext.PartsSalesCategories on partsSalesPrice.PartsSalesCategoryId equals partsSalesCategory.Id
                             join sparePart in ObjectContext.SpareParts.Where(r => r.Status == (int)DcsMasterDataStatus.有效) on partsStock.PartId equals sparePart.Id
                             select new VirtualCompanySalescenterStock {
                                 CompanyId = company.Id,
                                 CompanyType = companyType,
                                 CompanyCode = company.Code,
                                 CompanyName = company.Name,
                                 BranchId = branch.Id,
                                 BranchCode = branch.Code,
                                 BranchName = branch.Name,
                                 PartsSalesCategoryId = partsSalesCategory.Id,
                                 PartsSalesCategoryName = partsSalesCategory.Name,
                                 SparePartsId = sparePart.Id,
                                 SparePartsCode = sparePart.Code,
                                 SparePartsName = sparePart.Name,
                                 FactAmount = partsStock.StockAmount,
                                 UsableAmount = partsStock.StockAmount - (t1.LockedAmount ?? 0) - (t2.SumCongelationStockQty ?? 0) - (t2.SumDisabledStock ?? 0),
                                 ProvinceName = company.ProvinceName,
                                 CityName = company.CityName,
                                 CountyName = company.CountyName,
                                 PartsSalesPrice = partsSalesPrice.SalesPrice,
                                 PartsSalesPriceId = partsSalesPrice.Id
                             };
                return result.OrderBy(r => r.CompanyId);
            }
            return null;
        }
        public IQueryable<VirtualCompanySalescenterStock> 查询替互换件企业销售中心库存(int companyId, int partsSalesCategoryId, int sparePartsId) {
            var partsReplacements = ObjectContext.PartsReplacements.Where(r => r.OldPartId == sparePartsId || (r.NewPartId == sparePartsId));
            var sparePartsIds = partsReplacements.Select(r => r.OldPartId).Concat(partsReplacements.Select(r => r.NewPartId)).Distinct().ToArray();
            if(!partsReplacements.Any())
                return null;
            var company = ObjectContext.Companies.SingleOrDefault(r => r.Id == companyId);
            if(company == null)
                return null;
            var result = DomainService.查询企业销售中心库存(companyId, partsSalesCategoryId, company.Type);
            return result.Where(r => sparePartsIds.Contains(r.SparePartsId));
        }

    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:33
                                    //原分布类的函数全部转移到Ach                                                               


        public IQueryable<VirtualCompanySalescenterStock> 查询企业销售中心库存(int companyId, int partsSalesCategoryId, int companyType) {
            return new VirtualCompanySalescenterStockAch(this).查询企业销售中心库存(companyId, partsSalesCategoryId, companyType);
        }


        public IQueryable<VirtualCompanySalescenterStock> 查询企业销售中心库存累加(int companyId, int partsSalesCategoryId, int companyType) {
            return new VirtualCompanySalescenterStockAch(this).查询企业销售中心库存累加(companyId, partsSalesCategoryId, companyType);
        }

        public IQueryable<VirtualCompanySalescenterStock> 查询替互换件企业销售中心库存(int companyId, int partsSalesCategoryId, int sparePartsId) {
            return new VirtualCompanySalescenterStockAch(this).查询替互换件企业销售中心库存(companyId, partsSalesCategoryId, sparePartsId);
        }
    }
}
