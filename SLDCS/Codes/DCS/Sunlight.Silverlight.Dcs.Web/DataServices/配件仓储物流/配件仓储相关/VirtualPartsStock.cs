﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Web;
using System.ServiceModel.DomainServices.Server;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class VirtualPartsStockAch : DcsSerivceAchieveBase {
        public VirtualPartsStockAch(DcsDomainService domainService)
            : base(domainService) {
        }
        public IEnumerable<VirtualAgencyPartsStock> 查询中心库配件库存(string companyCode,string companyName,string sparePartCode,string sparePartName,bool? greaterThanZero,bool? isAgencyQuery) {
            //var result = from a in ObjectContext.PartsStocks
            //             join b in ObjectContext.Warehouses on a.WarehouseId equals b.Id
            //             join c in ObjectContext.Companies.Where(r => r.Type == (int)DcsCompanyType.代理库) on a.StorageCompanyId equals c.Id
            //             join d in ObjectContext.WarehouseAreas.Where(r => r.AreaKind == (int)DcsAreaKind.库位 && r.Status == (int)DcsBaseDataStatus.有效) on a.WarehouseAreaId equals d.Id
            //             join e in ObjectContext.WarehouseAreas.Where(r => r.AreaKind == (int)DcsAreaKind.库区 && r.Status == (int)DcsBaseDataStatus.有效) on d.ParentId equals e.Id
            //             join f in ObjectContext.SpareParts on a.PartId equals f.Id
            //             join g in ObjectContext.WarehouseAreaCategories on d.AreaCategoryId equals g.Id
            //             select new VirtualAgencyPartsStock {
            //                 Id = a.Id,
            //                 CompanyCode = c.Code,
            //                 CompanyName = c.Name,
            //                 WarehouseCode = b.Code,
            //                 WarehouseName = b.Name,
            //                 WarehouseAreaCategory = g.Category,
            //                 WarehouseAreaCode = d.Code,
            //                 WarehouseAreaPositionCode = e.Code,
            //                 SparePartCode = f.Code,
            //                 SparePartName = f.Name,
            //                 Quantity = a.Quantity
            //             };
            //if (!string.IsNullOrEmpty(companyCode))
            //    result = result.Where(r => r.CompanyCode.Contains(companyCode));
            //if (!string.IsNullOrEmpty(companyName))
            //    result = result.Where(r => r.CompanyName.Contains(companyName));
            //if (!string.IsNullOrEmpty(sparePartCode))
            //    result = result.Where(r => r.SparePartCode.Contains(sparePartCode));
            //if (!string.IsNullOrEmpty(sparePartName))
            //    result = result.Where(r => r.SparePartName.Contains(sparePartName));
            //if (greaterThanZero != null) {
            //    if ((bool)greaterThanZero) {
            //        result = result.Where(r => r.Quantity > 0);
            //    } else {
            //        result = result.Where(r => r.Quantity <= 0);

            //    }
            //}
            //return result.OrderBy(r => r.Id);
            var companyId = Utils.GetCurrentUserInfo().EnterpriseId;
            var company = ObjectContext.Companies.Where(r => r.Id == companyId).ToArray().FirstOrDefault();

            var sql = @"select id,companyCode,companyName, warehousecode ,warehousename ,SparePartCode,SparePartName,CenterPrice ,partabc,
                  totalqty as quantity, (nvl(actualqty,0) - nvl(orderlockqty,0)) as   ActualAvailableStock, totalqty * CenterPrice as QuantityAll,(nvl(actualqty, 0) - nvl(orderlockqty, 0)) * CenterPrice as ActualAll    
                   from(
                   select rownum as id, companyCode,companyName,warehouseid,warehousecode,warehousename,SparePartId,SparePartCode, SparePartName, referencecode, CenterPrice,zmqty as totalqty,partabc,
                   (select sum(quantity) from partsstock tmp inner join warehouseareacategory b on tmp.warehouseareacategoryid = b.id  where tmp.partid = a.SparePartId and tmp.warehouseid = a.warehouseid and b.category = 1) actualqty,
                   (select sum(lockedquantity) from partslockedstock tmp where tmp.partid = a.SparePartId and tmp.warehouseid = a.warehouseid) as orderlockqty
              from (select cp.code as companyCode,cp.name as companyName, b.id  as warehouseid,  b.code as  warehousecode, b.name as warehousename,
                           sp.id as SparePartId,  sp.code  SparePartCode, sp.name  SparePartName, sp.referencecode,  (case when cp.type=3 then f.CenterPrice else f.salesprice end) as  CenterPrice,sum(a.quantity) as zmqty,  cf.newtype  as PartABC
                      from partsstock a  inner join warehouse b on a.warehouseid = b.id inner join warehouseareacategory c  on c.id = a.warehouseareacategoryid
                      inner join company cp on cp.id = a.storagecompanyid and cp.type in (3,7) {1}
                     inner join sparepart sp on sp.id = a.partid  left join salesunitaffiwarehouse d  on d.warehouseid = b.id left join salesunit e  on e.id = d.salesunitid  left join partssalescategory pcg on pcg.id = e.partssalescategoryid
                     inner join partsbranch pb on pb.partid = sp.id and pb.status = 1
                    left join PartsSalesPrice f  on f.partssalescategoryid = pcg.id and f.sparepartid = a.partid   and f.status = 1   left join CentralABCClassification cf on a.storagecompanyid=cf.centerid and a.partid=cf.sparepartid              
                     where 1=1 {0} and c.category <> 2 group by cp.code,cp.name,b.id,  b.code, b.name,  sp.id,sp.code,  sp.name,  sp.referencecode,   (case when cp.type=3 then f.CenterPrice else f.salesprice end), cf.newtype  order by sp.id) a   
                )b where 1=1
                ";
            var searchSql = string.Empty;
            string param1 = string.Empty;
            if (!isAgencyQuery.HasValue) {
                isAgencyQuery = false;
            }
            if (company.Type != (int)DcsCompanyType.分公司 && !isAgencyQuery.Value) {
                param1 = " and cp.id =" + Utils.GetCurrentUserInfo().EnterpriseId;
            }

            if (!string.IsNullOrEmpty(companyCode))
                searchSql += " and cp.code like '%" + companyCode + "%'"; 
            if (!string.IsNullOrEmpty(companyName))
                searchSql += " and cp.name like '%" + companyName + "%'";

            
            if (!string.IsNullOrEmpty(sparePartCode)) { 
                var spareparts = sparePartCode.Split(',');
                if (spareparts.Length == 1) { 
                    var sparepartcode = spareparts[0]; 
                    if (isAgencyQuery.Value && company.Type != (int)DcsCompanyType.分公司) {
                        searchSql += " and sp.code = '" + sparepartcode + "'"; 
                    } else {
                        searchSql += " and sp.code like '%" + sparepartcode + "%'";
                    }
                } else {
                    for (var i = 0; i < spareparts.Length; i++) {
                        spareparts[i] = "'" + spareparts[i] + "'";
                    }
                    string codes = string.Join(",", spareparts); 
                    searchSql += " and sp.code in (" + codes + ")"; ;
                } 
            }
            if (!string.IsNullOrEmpty(sparePartName))
                searchSql += " and sp.name like '%" + sparePartName + "%'";
            if (isAgencyQuery.HasValue && isAgencyQuery.Value) {
                sql = string.Format(sql, searchSql,param1);
            } else {
                sql = string.Format(sql, searchSql,param1);
            }
            if (greaterThanZero != null) {
                if ((bool)greaterThanZero) {
                    sql += " and totalqty > 0";
                } else {
                    sql += " and totalqty <= 0";
                }
            }
            var search = ObjectContext.ExecuteStoreQuery<VirtualAgencyPartsStock>(sql).ToList();
            return search;
        }
        public IEnumerable<VirtualAgencyPartsStock> 大服务站库存查询(string companyCode, string companyName, string sparePartCode, string sparePartName, bool? greaterThanZero) {
           
            var companyId = Utils.GetCurrentUserInfo().EnterpriseId;
            var company = ObjectContext.Companies.Where(r => r.Id == companyId).ToArray().FirstOrDefault();

            var sql = @"select id,companyCode,companyName, warehousecode ,warehousename ,SparePartCode,SparePartName,CenterPrice ,partabc,
                  totalqty as quantity, (nvl(actualqty,0) - nvl(orderlockqty,0)) as   ActualAvailableStock, totalqty * CenterPrice as QuantityAll,(nvl(actualqty, 0) - nvl(orderlockqty, 0)) * CenterPrice as ActualAll    
                   from(
                   select rownum as id, companyCode,companyName,warehouseid,warehousecode,warehousename,SparePartId,SparePartCode, SparePartName, referencecode, CenterPrice,zmqty as totalqty,partabc,
                   (select sum(quantity) from partsstock tmp inner join warehouseareacategory b on tmp.warehouseareacategoryid = b.id  where tmp.partid = a.SparePartId and tmp.warehouseid = a.warehouseid and b.category = 1) actualqty,
                   (select sum(lockedquantity) from partslockedstock tmp where tmp.partid = a.SparePartId and tmp.warehouseid = a.warehouseid) as orderlockqty
              from (select cp.code as companyCode,cp.name as companyName, b.id  as warehouseid,  b.code as  warehousecode, b.name as warehousename,
                           sp.id as SparePartId,  sp.code  SparePartCode, sp.name  SparePartName, sp.referencecode,  (case when cp.type=3 then f.CenterPrice else f.salesprice end) as  CenterPrice,sum(a.quantity) as zmqty,  cf.newtype  as PartABC
                      from partsstock a  inner join warehouse b on a.warehouseid = b.id inner join warehouseareacategory c  on c.id = a.warehouseareacategoryid
                      inner join company cp on cp.id = a.storagecompanyid and cp.type in (7) 
                     inner join sparepart sp on sp.id = a.partid  left join salesunitaffiwarehouse d  on d.warehouseid = b.id left join salesunit e  on e.id = d.salesunitid  left join partssalescategory pcg on pcg.id = e.partssalescategoryid
                     inner join partsbranch pb on pb.partid = sp.id and pb.status = 1
                    left join PartsSalesPrice f  on f.partssalescategoryid = pcg.id and f.sparepartid = a.partid   and f.status = 1   left join CentralABCClassification cf on a.storagecompanyid=cf.centerid and a.partid=cf.sparepartid              
                     where 1=1 {0} and c.category <> 2 group by cp.code,cp.name,b.id,  b.code, b.name,  sp.id,sp.code,  sp.name,  sp.referencecode,   (case when cp.type=3 then f.CenterPrice else f.salesprice end), cf.newtype  order by sp.id) a   
                )b where 1=1
                ";
            var searchSql = string.Empty;
            if(company.Type == (int)DcsCompanyType.服务站兼代理库 ) {
                searchSql = " and cp.id =" + Utils.GetCurrentUserInfo().EnterpriseId;
            } else if(company.Type == (int)DcsCompanyType.代理库) {
                searchSql = " and (exists(select 1 from AgencyDealerRelation ag where ag.DealerId=cp.id and ag.AgencyId= " + Utils.GetCurrentUserInfo().EnterpriseId + "))";                
            }

            if(!string.IsNullOrEmpty(companyCode))
                searchSql += " and cp.code like '%" + companyCode + "%'";
            if(!string.IsNullOrEmpty(companyName))
                searchSql += " and cp.name like '%" + companyName + "%'";


            if(!string.IsNullOrEmpty(sparePartCode)) {
                var spareparts = sparePartCode.Split(',');
                if(spareparts.Length == 1) {
                    var sparepartcode = spareparts[0];
                        searchSql += " and sp.code like '%" + sparepartcode + "%'";
                } else {
                    for(var i = 0; i < spareparts.Length; i++) {
                        spareparts[i] = "'" + spareparts[i] + "'";
                    }
                    string codes = string.Join(",", spareparts);
                    searchSql += " and sp.code in (" + codes + ")"; ;
                }
            }
            if(!string.IsNullOrEmpty(sparePartName))
                searchSql += " and sp.name like '%" + sparePartName + "%'";
             sql = string.Format(sql, searchSql);
            if(greaterThanZero != null) {
                if((bool)greaterThanZero) {
                    sql += " and totalqty > 0";
                } else {
                    sql += " and totalqty <= 0";
                }
            }
            var search = ObjectContext.ExecuteStoreQuery<VirtualAgencyPartsStock>(sql).ToList();
            return search;
        }

        public IEnumerable<VirtualPartsStock> 查询待包装配件库存() {
            var userinfo = Utils.GetCurrentUserInfo();
            var warehouseAreaManagerAreaIds = ObjectContext.WarehouseAreaManagers.Where(r => r.ManagerId == userinfo.Id).Select(v => v.WarehouseAreaId).ToArray();
            if(warehouseAreaManagerAreaIds.Length == 0)
                return null;
            var warehouseAreaCategoryIds = ObjectContext.WarehouseAreaCategories.Where(r => r.Category == (int)DcsAreaType.检验区).Select(v => v.Id).ToArray();
            if(warehouseAreaCategoryIds.Length == 0)
                return null;

            var result = from partsStock in ObjectContext.PartsStocks
                         from partsBranch in ObjectContext.PartsBranches
                         from warehouseArea in ObjectContext.WarehouseAreas
                         from warehouse in ObjectContext.Warehouses
                         from sparePart in ObjectContext.SpareParts
                         where partsStock.PartId == partsBranch.PartId && partsStock.BranchId == partsBranch.BranchId && partsBranch.Status == (int)DcsBaseDataStatus.有效 && partsBranch.PartsWarhouseManageGranularity == (int)DcsPartsBranchPartsWarhouseManageGranularity.序列号管理
                         && warehouseArea.Id == partsStock.WarehouseAreaId && warehouseArea.AreaKind == (int)DcsAreaKind.库位 && warehouseAreaManagerAreaIds.Contains(warehouseArea.TopLevelWarehouseAreaId.Value) && warehouseAreaCategoryIds.Contains(warehouseArea.AreaCategoryId.Value) && warehouseArea.Status == (int)DcsBaseDataStatus.有效
                         && warehouse.Id == partsStock.WarehouseId
                         && sparePart.Id == partsStock.PartId && sparePart.Status == (int)DcsMasterDataStatus.有效
                         && partsStock.Quantity > 0
                         select new {
                             PartsStockId = partsStock.Id,
                             partsStock.WarehouseId,
                             WarehouseCode = warehouse.Code,
                             WarehouseName = warehouse.Name,
                             partsStock.StorageCompanyId,
                             partsStock.StorageCompanyType,
                             partsStock.BranchId,
                             SparePartId = sparePart.Id,
                             SparePartCode = sparePart.Code,
                             SparePartName = sparePart.Name,
                             sparePart.MeasureUnit,
                             partsStock.WarehouseAreaId,
                             WarehouseAreaCode = warehouseArea.Code,
                             partsStock.Quantity,
                             UnusableQuantity = (int?)ObjectContext.PartsStockBatchDetails.Where(v => v.PartsStockId == partsStock.Id).Sum(r => r.Quantity),
                             sparePart.PackingAmount,
                             sparePart.PackingSpecification
                         };
            return result.Where(r => r.Quantity > r.UnusableQuantity || !r.UnusableQuantity.HasValue).Select(v => new VirtualPartsStock {
                PartsStockId = v.PartsStockId,
                WarehouseId = v.WarehouseId,
                WarehouseCode = v.WarehouseCode,
                WarehouseName = v.WarehouseName,
                StorageCompanyId = v.StorageCompanyId,
                StorageCompanyType = v.StorageCompanyType,
                BranchId = v.BranchId,
                SparePartId = v.SparePartId,
                SparePartCode = v.SparePartCode,
                SparePartName = v.SparePartName,
                MeasureUnit = v.MeasureUnit,
                WarehouseAreaCategory = (int)DcsAreaType.检验区,
                WarehouseAreaId = v.WarehouseAreaId,
                WarehouseAreaCode = v.WarehouseAreaCode,
                Quantity = v.Quantity,
                UsableQuantity = v.Quantity - (v.UnusableQuantity ?? 0),
                PackingAmount = v.PackingAmount,
                PackingSpecification = v.PackingSpecification
            }).OrderBy(r => r.PartsStockId);
        }

        public IQueryable<VirtualPartsStock> 查询待上架配件库存(int warehouseId) {
            var userinfo = Utils.GetCurrentUserInfo();
            //查询出指定库区库位
            var warehouseAreaManagerAreaIds = ObjectContext.WarehouseAreaManagers.Where(r => r.ManagerId == userinfo.Id).Select(v => v.WarehouseAreaId).ToArray();
            if(warehouseAreaManagerAreaIds.Length == 0)
                return null;
            var saleUnit = ObjectContext.SalesUnits.FirstOrDefault(r => ObjectContext.SalesUnitAffiWarehouses.Any(v => v.SalesUnitId == r.Id && v.WarehouseId == warehouseId && ObjectContext.WarehouseAreas.Any(k => warehouseAreaManagerAreaIds.Contains(k.Id) && k.WarehouseId == v.WarehouseId)));
            if(saleUnit == null)
                return null;
            //var warehouseAreaCategoryIds = ObjectContext.WarehouseAreaCategories.Where(r => r.Category == (int)DcsAreaType.检验区).Select(v => v.Id).ToArray();
            //if(warehouseAreaCategoryIds.Length == 0)
            //    return null;

            var resultPartsStock = from partsStock in ObjectContext.PartsStocks.Where(r => r.Quantity > 0)
                                   from warehouseArea in ObjectContext.WarehouseAreas.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.AreaKind == (int)DcsAreaKind.库位)
                                   from warehouse in ObjectContext.Warehouses.Where(r => r.Status == (int)DcsBaseDataStatus.有效)
                                   from sparePart in ObjectContext.SpareParts.Where(r => r.Status == (int)DcsMasterDataStatus.有效)
                                   where warehouseArea.Id == partsStock.WarehouseAreaId
                                   && warehouseAreaManagerAreaIds.Contains(warehouseArea.TopLevelWarehouseAreaId.Value)
                                   && ObjectContext.WarehouseAreaCategories.Any(r => r.Category == (int)DcsAreaType.检验区 && r.Id == warehouseArea.AreaCategoryId)
                                   && warehouse.Id == partsStock.WarehouseId
                                   && sparePart.Id == partsStock.PartId
                                   && warehouse.Id == warehouseId
                                   select new {
                                       PartsStockId = partsStock.Id,
                                       partsStock.WarehouseId,
                                       WarehouseCode = warehouse.Code,
                                       WarehouseName = warehouse.Name,
                                       partsStock.StorageCompanyId,
                                       partsStock.StorageCompanyType,
                                       partsStock.BranchId,
                                       SparePartId = sparePart.Id,
                                       SparePartCode = sparePart.Code,
                                       SparePartName = sparePart.Name,
                                       sparePart.MeasureUnit,
                                       partsStock.WarehouseAreaId,
                                       WarehouseAreaCode = warehouseArea.Code,
                                       partsStock.Quantity
                                   };

            var result = from rt in resultPartsStock
                         join partsStockBatchDetail in ObjectContext.PartsStockBatchDetails on rt.PartsStockId equals partsStockBatchDetail.PartsStockId into details
                         from v in details.DefaultIfEmpty()
                         where ((int?)v.PartsStockId).HasValue || ObjectContext.PartsBranches.Any(r => r.PartId == rt.SparePartId && r.BranchId == rt.BranchId && r.Status == (int)DcsBaseDataStatus.有效
                            && r.PartsSalesCategoryId == saleUnit.PartsSalesCategoryId && (r.PartsWarhouseManageGranularity == null || r.PartsWarhouseManageGranularity == (int)DcsPartsBranchPartsWarhouseManageGranularity.无批次管理))
                         select new VirtualPartsStock {
                             PartsStockId = rt.PartsStockId,
                             WarehouseId = rt.WarehouseId,
                             WarehouseCode = rt.WarehouseCode,
                             WarehouseName = rt.WarehouseName,
                             StorageCompanyId = rt.StorageCompanyId,
                             StorageCompanyType = rt.StorageCompanyType,
                             BranchId = rt.BranchId,
                             SparePartId = rt.SparePartId,
                             SparePartCode = rt.SparePartCode,
                             SparePartName = rt.SparePartName,
                             MeasureUnit = rt.MeasureUnit,
                             WarehouseAreaId = rt.WarehouseAreaId,
                             WarehouseAreaCode = rt.WarehouseAreaCode,
                             Quantity = rt.Quantity,
                             UsableQuantity = (int?)v.Quantity ?? rt.Quantity,//此处不能改为v.Quantity 因join 可能导致v.Quantity为Null 所以强制转换有效
                             BatchNumber = v.BatchNumber ?? "",
                             WarehouseAreaCategory = (int)DcsAreaType.检验区
                         };
            return result.OrderBy(r => r.PartsStockId);
        }

        public IQueryable<VirtualPartsStock> 查询配件库位库存(int? partsOutPlanId, int? warehouseAreaCategoryValue, int? operatorId, int? managerId, string warehouseRegionCode, int? pricetype, bool? greaterThanZero) {
            //出库计划Id（非必填）、 库区用途（非必填）、仓库人员Id（非必填）、库区负责人Id（非必填）、库区编号（非必填）
            var partsStockFilterByQuantity = DomainService.GetVirtualPartsStocks(greaterThanZero);
            var userInfo = Utils.GetCurrentUserInfo();
            var resultPartsStock = from partsStock in partsStockFilterByQuantity
                                   join t in ObjectContext.SalesUnitAffiWarehouses on partsStock.WarehouseId equals t.WarehouseId
                                   join salesUnit in ObjectContext.SalesUnits.Where(r => r.Status == (int)DcsMasterDataStatus.有效) on t.SalesUnitId equals salesUnit.Id
                                   from warehouseArea in ObjectContext.WarehouseAreas.Where(r => r.AreaKind == (int)DcsAreaKind.库位 && r.Status == (int)DcsBaseDataStatus.有效)
                                   from warehouseRegion in ObjectContext.WarehouseAreas.Where(r => r.Status == (int)DcsBaseDataStatus.有效)
                                   from warehouse in ObjectContext.Warehouses.Where(r => r.Status == (int)DcsBaseDataStatus.有效)
                                   from sparePart in ObjectContext.SpareParts.Where(r => r.Status == (int)DcsMasterDataStatus.有效)
                                   from warehouseAreaCategory in ObjectContext.WarehouseAreaCategories
                                   from partsSalesCategory in ObjectContext.PartsSalesCategories.Where(r => r.Status == (int)DcsBaseDataStatus.有效)
                                   where partsStock.WarehouseAreaId == warehouseArea.Id
                                   && partsStock.WarehouseId == warehouse.Id
                                   && partsStock.PartId == sparePart.Id
                                   && partsStock.WarehouseAreaCategoryId == warehouseAreaCategory.Id
                                   && warehouseArea.TopLevelWarehouseAreaId == warehouseRegion.Id
                                   && salesUnit.PartsSalesCategoryId == partsSalesCategory.Id
                                   select new {
                                       PartsStockId = partsStock.Id,
                                       partsStock.WarehouseId,
                                       WarehouseCode = warehouse.Code,
                                       WarehouseName = warehouse.Name,
                                       partsStock.StorageCompanyId,
                                       partsStock.StorageCompanyType,
                                       partsStock.BranchId,
                                       SparePartId = sparePart.Id,
                                       SparePartCode = sparePart.Code,
                                       SparePartName = sparePart.Name,
                                       sparePart.MeasureUnit,
                                       salesUnit.PartsSalesCategoryId,//销售类型id,用于关联配件销售价格
                                       partsStock.WarehouseAreaId,//库位Id
                                       WarehouseAreaCode = warehouseArea.Code,//库位编号
                                       partsStock.Quantity,
                                       partsStock.LockedQty,
                                       warehouseArea.AreaCategoryId,
                                       warehouseAreaCategory.Category,//库区用途
                                       warehouseArea.TopLevelWarehouseAreaId,//顶层库区Id
                                       WarehouseRegionCode = warehouseRegion.Code,//库区编号
                                       Pid = partsSalesCategory.Id,
                                       Pname = partsSalesCategory.Name
                                   };
            if(operatorId.HasValue)
                resultPartsStock = resultPartsStock.Where(r => ObjectContext.WarehouseOperators.Any(v => v.WarehouseId == r.WarehouseId && v.OperatorId == operatorId.Value));

            if(managerId.HasValue)
                resultPartsStock = resultPartsStock.Where(r => ObjectContext.WarehouseAreaManagers.Any(v => v.ManagerId == userInfo.Id && v.WarehouseAreaId == r.TopLevelWarehouseAreaId.Value));
            if(!string.IsNullOrWhiteSpace(warehouseRegionCode)) {
                resultPartsStock = resultPartsStock.Where(r => r.WarehouseRegionCode == warehouseRegionCode);
            }
            //查询出库计划
            if(partsOutPlanId.HasValue) {
                var partsOutboundPlan = ObjectContext.PartsOutboundPlans.SingleOrDefault(r => r.Id == partsOutPlanId.Value);
                if(partsOutboundPlan == null)
                    return null;
                var partsOutboundPlanDetailPartIds = ObjectContext.PartsOutboundPlanDetails.Where(r => r.PartsOutboundPlanId == partsOutboundPlan.Id && (r.OutboundFulfillment == null || r.PlannedAmount != r.OutboundFulfillment)).Select(r => r.SparePartId).ToArray();
                if(partsOutboundPlanDetailPartIds.Length == 0)
                    return null;
                resultPartsStock = resultPartsStock.Where(r => r.WarehouseId == partsOutboundPlan.WarehouseId && partsOutboundPlanDetailPartIds.Contains(r.SparePartId));
            }
            if(warehouseAreaCategoryValue.HasValue)
                resultPartsStock = resultPartsStock.Where(r => r.Category == warehouseAreaCategoryValue.Value);
            //左外联库存批次
            var result = from r in resultPartsStock
                         //join partsStockBatchDetail in ObjectContext.PartsStockBatchDetails on r.PartsStockId equals partsStockBatchDetail.PartsStockId into details
                         //from v in details.DefaultIfEmpty()
                         join s in ObjectContext.PartsSalesPrices on new {
                             t1 = r.PartsSalesCategoryId,
                             t2 = r.SparePartId,
                             Status = (int)DcsBaseDataStatus.有效,
                             PriceType = pricetype.Value
                         }
                        equals new {
                            t1 = s.PartsSalesCategoryId,
                            t2 = s.SparePartId,
                            s.Status,
                            s.PriceType
                        } into a
                         from b in a.DefaultIfEmpty()
                         select new VirtualPartsStock {
                             PartsStockId = r.PartsStockId,
                             WarehouseId = r.WarehouseId,
                             WarehouseCode = r.WarehouseCode,
                             WarehouseName = r.WarehouseName,
                             StorageCompanyId = r.StorageCompanyId,
                             StorageCompanyType = r.StorageCompanyType,
                             BranchId = r.BranchId,
                             SparePartId = r.SparePartId,
                             SparePartCode = r.SparePartCode,
                             SparePartName = r.SparePartName,
                             MeasureUnit = r.MeasureUnit,
                             WarehouseAreaId = r.WarehouseAreaId,
                             WarehouseAreaCode = r.WarehouseAreaCode,
                             Quantity = r.Quantity,
                             LockedQty = r.LockedQty ?? 0,
                           //  UsableQuantity = (int?)v.Quantity ?? r.Quantity,//此处不能改为v.Quantity 因join 可能导致v.Quantity为Null 所以强制转换有效
                             UsableQuantity =  r.Quantity,
                          //   BatchNumber = v.BatchNumber ?? "",
                               BatchNumber = "",
                             WarehouseAreaCategory = r.Category,
                       //      InboundTime = v.InboundTime,
                             InboundTime = null,
                             WarehouseRegionCode = r.WarehouseRegionCode,
                             PriceType = b.PriceType,
                             SalesPrice = b == null ? 0 : b.SalesPrice,
                             PartsSalesCategoryId = r.Pid,
                             PartsSalesCategoryName = r.Pname

                         };

            return result.OrderBy(r => r.PartsStockId);
        }
        public IQueryable<VirtualPartsStock> 查询配件库位库存1(int? partsOutPlanId, int? warehouseAreaCategoryValue, int? operatorId, int? managerId, string warehouseRegionCode, int? pricetype, bool? greaterThanZero,string sparePartCodes) {

            //出库计划Id（非必填）、 库区用途（非必填）、仓库人员Id（非必填）、库区负责人Id（非必填）、库区编号（非必填）
            var partsStockFilterByQuantity = DomainService.GetVirtualPartsStocks1(greaterThanZero);

            var resultPartsStock = from partsStock in partsStockFilterByQuantity
                                   join t in ObjectContext.SalesUnitAffiWarehouses on partsStock.WarehouseId equals t.WarehouseId
                                   join salesUnit in ObjectContext.SalesUnits.Where(r => r.Status == (int)DcsMasterDataStatus.有效) on t.SalesUnitId equals salesUnit.Id
                                   from warehouseArea in ObjectContext.WarehouseAreas.Where(r => r.AreaKind == (int)DcsAreaKind.库位 && r.Status == (int)DcsBaseDataStatus.有效)
                                   from warehouseRegion in ObjectContext.WarehouseAreas.Where(r => r.Status == (int)DcsBaseDataStatus.有效)
                                   from warehouse in ObjectContext.Warehouses.Where(r => r.Status == (int)DcsBaseDataStatus.有效)
                                   from sparePart in ObjectContext.SpareParts.Where(r => r.Status == (int)DcsMasterDataStatus.有效)
                                   from warehouseAreaCategory in ObjectContext.WarehouseAreaCategories
                                   from partsSalesCategory in ObjectContext.PartsSalesCategories.Where(r => r.Status == (int)DcsBaseDataStatus.有效)
                                   where partsStock.WarehouseAreaId == warehouseArea.Id
                                   && partsStock.WarehouseId == warehouse.Id
                                   && partsStock.PartId == sparePart.Id
                                   && partsStock.WarehouseAreaCategoryId == warehouseAreaCategory.Id
                                   && warehouseArea.TopLevelWarehouseAreaId == warehouseRegion.Id
                                   && salesUnit.PartsSalesCategoryId == partsSalesCategory.Id
                                   select new {
                                       PartsStockId = partsStock.Id,
                                       partsStock.WarehouseId,
                                       WarehouseCode = warehouse.Code,
                                       WarehouseName = warehouse.Name,
                                       partsStock.StorageCompanyId,
                                       partsStock.StorageCompanyType,
                                       partsStock.BranchId,
                                       SparePartId = sparePart.Id,
                                       SparePartCode = sparePart.Code,
                                       SparePartName = sparePart.Name,
                                       sparePart.MeasureUnit,
                                       salesUnit.PartsSalesCategoryId,//销售类型id,用于关联配件销售价格
                                       partsStock.WarehouseAreaId,//库位Id
                                       WarehouseAreaCode = warehouseArea.Code,//库位编号
                                       partsStock.Quantity,
                                       warehouseArea.AreaCategoryId,
                                       warehouseAreaCategory.Category,//库区用途
                                       warehouseArea.TopLevelWarehouseAreaId,//顶层库区Id
                                       WarehouseRegionCode = warehouseRegion.Code,//库区编号
                                       Pid = partsSalesCategory.Id,
                                       Pname = partsSalesCategory.Name,
                                       LockedQty = partsStock.LockedQty,
                                       OutingQty = partsStock.OutingQty
                                   };
            if (!string.IsNullOrEmpty(sparePartCodes)) {
                var spareparts = sparePartCodes.Split(',');

                if (spareparts.Length == 1) {
                    var sparepartcode = spareparts[0];
                    resultPartsStock = resultPartsStock.Where(r => r.SparePartCode.Contains(sparepartcode));
                } else { 
                    resultPartsStock = resultPartsStock.Where(r => spareparts.Contains(r.SparePartCode));
                }
            }
            if(operatorId.HasValue)
                resultPartsStock = resultPartsStock.Where(r => ObjectContext.WarehouseOperators.Any(v => v.WarehouseId == r.WarehouseId && v.OperatorId == operatorId.Value));

            if(managerId.HasValue)
                resultPartsStock = resultPartsStock.Where(r => ObjectContext.WarehouseAreaManagers.Any(v => v.ManagerId == managerId.Value && v.WarehouseAreaId == r.TopLevelWarehouseAreaId.Value));
            if(!string.IsNullOrWhiteSpace(warehouseRegionCode)) {
                resultPartsStock = resultPartsStock.Where(r => r.WarehouseRegionCode.Contains(warehouseRegionCode));
            }
            //查询出库计划
            if(partsOutPlanId.HasValue) {
                var partsOutboundPlan = ObjectContext.PartsOutboundPlans.SingleOrDefault(r => r.Id == partsOutPlanId.Value);
                if(partsOutboundPlan == null)
                    return null;
                var partsOutboundPlanDetailPartIds = ObjectContext.PartsOutboundPlanDetails.Where(r => r.PartsOutboundPlanId == partsOutboundPlan.Id && (r.OutboundFulfillment == null || r.PlannedAmount != r.OutboundFulfillment)).Select(r => r.SparePartId).ToArray();
                if(partsOutboundPlanDetailPartIds.Length == 0)
                    return null;
                resultPartsStock = resultPartsStock.Where(r => r.WarehouseId == partsOutboundPlan.WarehouseId && partsOutboundPlanDetailPartIds.Contains(r.SparePartId));
            }
            if(warehouseAreaCategoryValue.HasValue)
                resultPartsStock = resultPartsStock.Where(r => r.Category == warehouseAreaCategoryValue.Value);
            //左外联库存批次
            var result = from r in resultPartsStock
                       //  join partsStockBatchDetail in ObjectContext.PartsStockBatchDetails on r.PartsStockId equals partsStockBatchDetail.PartsStockId into details
                        // from v in details.DefaultIfEmpty()
                         join s in ObjectContext.PartsSalesPrices on new {
                             t1 = r.PartsSalesCategoryId,
                             t2 = r.SparePartId,
                             Status = (int)DcsBaseDataStatus.有效
                         }
                         equals new {
                             t1 = s.PartsSalesCategoryId,
                             t2 = s.SparePartId,
                             s.Status,
                         }
                         into a
                         from b in a.DefaultIfEmpty()
                         select new VirtualPartsStock {
                             PartsStockId = r.PartsStockId,
                             WarehouseId = r.WarehouseId,
                             WarehouseCode = r.WarehouseCode,
                             WarehouseName = r.WarehouseName,
                             StorageCompanyId = r.StorageCompanyId,
                             StorageCompanyType = r.StorageCompanyType,
                             BranchId = r.BranchId,
                             SparePartId = r.SparePartId,
                             SparePartCode = r.SparePartCode,
                             SparePartName = r.SparePartName,
                             MeasureUnit = r.MeasureUnit,
                             WarehouseAreaId = r.WarehouseAreaId,
                             WarehouseAreaCode = r.WarehouseAreaCode,
                             Quantity = r.Quantity,
                           //  UsableQuantity = (int?)v.Quantity ?? r.Quantity,//此处不能改为v.Quantity 因join 可能导致v.Quantity为Null 所以强制转换有效
                            // BatchNumber = v.BatchNumber ?? "",
                             WarehouseAreaCategory = r.Category,
                            // InboundTime = v.InboundTime,
                             WarehouseRegionCode = r.WarehouseRegionCode,
                             PriceType = b.PriceType,
                             SalesPrice = b == null ? 0 : b.SalesPrice,
                             PartsSalesCategoryId = r.Pid,
                             PartsSalesCategoryName = r.Pname,
                             LockedQty = r.LockedQty,
                             OutingQty = r.OutingQty

                         };
            if(pricetype.HasValue) {
                return result.OrderBy(r => r.PartsStockId);
            }
            return result.OrderBy(r => r.PartsStockId);
        }
        public IQueryable<PartsStock> GetVirtualPartsStocks(bool? greaterThanZero) {
            var partsStocks = ObjectContext.PartsStocks.Include("Branch").Include("Warehouse").Include("Company").Include("WarehouseArea").Include("WarehouseAreaCategory").Include("SparePart").Include("PartsSalesCategory");
            if(greaterThanZero.HasValue && greaterThanZero.Value)
                return partsStocks.Where(r => r.Quantity > 0).OrderBy(e => e.Id);
            if(greaterThanZero.HasValue && !greaterThanZero.Value)
                return partsStocks.Where(r => r.Quantity <= 0).OrderBy(e => e.Id);
            return partsStocks.OrderBy(e => e.Id);
        }
        public IQueryable<PartsStock> GetVirtualPartsStocks1(bool? greaterThanZero) {
            var userInfo = Utils.GetCurrentUserInfo();
            var partsStocks = ObjectContext.PartsStocks.Where(r => r.StorageCompanyId == userInfo.EnterpriseId).Include("Branch").Include("Warehouse").Include("Company").Include("WarehouseArea").Include("WarehouseAreaCategory").Include("SparePart").Include("PartsSalesCategory");
            if(greaterThanZero.HasValue && greaterThanZero.Value)
                return partsStocks.Where(r => r.Quantity > 0).OrderBy(e => e.Id);
            if(greaterThanZero.HasValue && !greaterThanZero.Value)
                return partsStocks.Where(r => r.Quantity <= 0).OrderBy(e => e.Id);
            return partsStocks.OrderBy(e => e.Id);
        }

        public IQueryable<VirtualPartsStock> 查询配件库位库存For代理库电商订单(int warehouseId, int operatorId) {
            var partsStocks = ObjectContext.PartsStocks.Where(r => r.WarehouseId == warehouseId);

            var resultPartsStock = from partsStock in partsStocks
                                   join t in ObjectContext.SalesUnitAffiWarehouses on partsStock.WarehouseId equals t.WarehouseId
                                   join salesUnit in ObjectContext.SalesUnits.Where(r => r.Status == (int)DcsMasterDataStatus.有效) on t.SalesUnitId equals salesUnit.Id
                                   from warehouseArea in ObjectContext.WarehouseAreas.Where(r => r.AreaKind == (int)DcsAreaKind.库位 && r.Status == (int)DcsBaseDataStatus.有效)
                                   from warehouseRegion in ObjectContext.WarehouseAreas.Where(r => r.Status == (int)DcsBaseDataStatus.有效)
                                   from warehouse in ObjectContext.Warehouses.Where(r => r.Status == (int)DcsBaseDataStatus.有效)
                                   from sparePart in ObjectContext.SpareParts.Where(r => r.Status == (int)DcsMasterDataStatus.有效)
                                   from warehouseAreaCategory in ObjectContext.WarehouseAreaCategories.Where(r => r.Category == (int)DcsAreaType.保管区)
                                   from partsSalesCategory in ObjectContext.PartsSalesCategories.Where(r => r.Status == (int)DcsBaseDataStatus.有效)
                                   where partsStock.WarehouseAreaId == warehouseArea.Id
                                   && partsStock.WarehouseId == warehouse.Id
                                   && partsStock.PartId == sparePart.Id
                                   && partsStock.WarehouseAreaCategoryId == warehouseAreaCategory.Id
                                   && warehouseArea.TopLevelWarehouseAreaId == warehouseRegion.Id
                                   && salesUnit.PartsSalesCategoryId == partsSalesCategory.Id
                                   select new {
                                       PartsStockId = partsStock.Id,
                                       partsStock.WarehouseId,
                                       WarehouseCode = warehouse.Code,
                                       WarehouseName = warehouse.Name,
                                       partsStock.StorageCompanyId,
                                       partsStock.StorageCompanyType,
                                       partsStock.BranchId,
                                       SparePartId = sparePart.Id,
                                       SparePartCode = sparePart.Code,
                                       SparePartName = sparePart.Name,
                                       sparePart.MeasureUnit,
                                       salesUnit.PartsSalesCategoryId,//销售类型id,用于关联配件销售价格
                                       partsStock.WarehouseAreaId,//库位Id
                                       WarehouseAreaCode = warehouseArea.Code,//库位编号
                                       partsStock.Quantity,
                                       warehouseArea.AreaCategoryId,
                                       warehouseAreaCategory.Category,//库区用途
                                       warehouseArea.TopLevelWarehouseAreaId,//顶层库区Id
                                       WarehouseRegionCode = warehouseRegion.Code,//库区编号
                                       Pid = partsSalesCategory.Id,
                                       Pname = partsSalesCategory.Name
                                   };

            resultPartsStock = resultPartsStock.Where(r => ObjectContext.WarehouseOperators.Any(v => v.WarehouseId == r.WarehouseId && v.OperatorId == operatorId));

            //左外联库存批次
            var result = from r in resultPartsStock
                         join partsStockBatchDetail in ObjectContext.PartsStockBatchDetails on r.PartsStockId equals partsStockBatchDetail.PartsStockId into details
                         from v in details.DefaultIfEmpty()
                         join s in ObjectContext.PartsSalesPrices.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on new {
                             t1 = r.PartsSalesCategoryId,
                             t2 = r.SparePartId
                         }
                         equals new {
                             t1 = s.PartsSalesCategoryId,
                             t2 = s.SparePartId
                         }
                         into a
                         from b in a.DefaultIfEmpty()
                         select new VirtualPartsStock {
                             PartsStockId = r.PartsStockId,
                             WarehouseId = r.WarehouseId,
                             WarehouseCode = r.WarehouseCode,
                             WarehouseName = r.WarehouseName,
                             StorageCompanyId = r.StorageCompanyId,
                             StorageCompanyType = r.StorageCompanyType,
                             BranchId = r.BranchId,
                             SparePartId = r.SparePartId,
                             SparePartCode = r.SparePartCode,
                             SparePartName = r.SparePartName,
                             MeasureUnit = r.MeasureUnit,
                             WarehouseAreaId = r.WarehouseAreaId,
                             WarehouseAreaCode = r.WarehouseAreaCode,
                             Quantity = r.Quantity,
                             UsableQuantity = (int?)v.Quantity ?? r.Quantity,//此处不能改为v.Quantity 因join 可能导致v.Quantity为Null 所以强制转换有效
                             BatchNumber = v.BatchNumber ?? "",
                             WarehouseAreaCategory = r.Category,
                             InboundTime = v.InboundTime,
                             WarehouseRegionCode = r.WarehouseRegionCode,
                             PriceType = b.PriceType,
                             SalesPrice = b == null ? 0 : b.SalesPrice,
                             PartsSalesCategoryId = r.Pid,
                             PartsSalesCategoryName = r.Pname

                         };
            return result.OrderBy(r => r.PartsStockId);
        }
        public IQueryable<VirtualPartsStock> 查询配件库位库存带营销信息(int? partsOutPlanId, int? warehouseAreaCategoryValue, int? operatorId, int? managerId, string warehouseRegionCode, int? pricetype) {
            //出库计划Id（非必填）、 库区用途（非必填）、仓库人员Id（非必填）、库区负责人Id（非必填）、库区编号（非必填）

            var resultPartsStock = from partsStock in ObjectContext.PartsStocks
                                   from t in ObjectContext.SalesUnitAffiWarehouses
                                   from salesUnit in ObjectContext.SalesUnits.Where(r => r.Status == (int)DcsMasterDataStatus.有效)
                                   from pb in ObjectContext.PartsBranches.Where(r => r.Status == (int)DcsMasterDataStatus.有效)
                                   from warehouseArea in ObjectContext.WarehouseAreas.Where(r => r.AreaKind == (int)DcsAreaKind.库位 && r.Status == (int)DcsBaseDataStatus.有效)
                                   from warehouseRegion in ObjectContext.WarehouseAreas.Where(r => r.Status == (int)DcsBaseDataStatus.有效)
                                   from warehouse in ObjectContext.Warehouses.Where(r => r.Status == (int)DcsBaseDataStatus.有效)
                                   from sparePart in ObjectContext.SpareParts.Where(r => r.Status == (int)DcsMasterDataStatus.有效)
                                   from warehouseAreaCategory in ObjectContext.WarehouseAreaCategories
                                   where partsStock.WarehouseId == t.WarehouseId
                                   && t.SalesUnitId == salesUnit.Id
                                   && partsStock.PartId == pb.PartId
                                   && pb.PartsSalesCategoryId == salesUnit.PartsSalesCategoryId
                                   && partsStock.WarehouseAreaId == warehouseArea.Id
                                   && partsStock.WarehouseId == warehouse.Id
                                   && partsStock.PartId == sparePart.Id
                                   && partsStock.WarehouseAreaCategoryId == warehouseAreaCategory.Id
                                   && warehouseArea.TopLevelWarehouseAreaId == warehouseRegion.Id
                                   select new {
                                       PartsStockId = partsStock.Id,
                                       partsStock.WarehouseId,
                                       WarehouseCode = warehouse.Code,
                                       WarehouseName = warehouse.Name,
                                       partsStock.StorageCompanyId,
                                       partsStock.StorageCompanyType,
                                       partsStock.BranchId,
                                       SparePartId = sparePart.Id,
                                       SparePartCode = sparePart.Code,
                                       SparePartName = sparePart.Name,
                                       sparePart.MeasureUnit,
                                       salesUnit.PartsSalesCategoryId,//销售类型id,用于关联配件销售价格
                                       partsStock.WarehouseAreaId,//库位Id
                                       WarehouseAreaCode = warehouseArea.Code,//库位编号
                                       Quantity = partsStock.Quantity - (partsStock.LockedQty??0),
                                       warehouseArea.AreaCategoryId,
                                       warehouseAreaCategory.Category,//库区用途
                                       warehouseArea.TopLevelWarehouseAreaId,//顶层库区Id
                                       WarehouseRegionCode = warehouseRegion.Code,//库区编号
                                       pb.ReferenceCode //红岩号
                                   };
            if(operatorId.HasValue)
                resultPartsStock = resultPartsStock.Where(r => ObjectContext.WarehouseOperators.Any(v => v.WarehouseId == r.WarehouseId && v.OperatorId == operatorId.Value));

            if(managerId.HasValue)
                resultPartsStock = resultPartsStock.Where(r => ObjectContext.WarehouseAreaManagers.Any(v => v.ManagerId == managerId.Value && v.WarehouseAreaId == r.TopLevelWarehouseAreaId.Value));
            if(!string.IsNullOrWhiteSpace(warehouseRegionCode)) {
                resultPartsStock = resultPartsStock.Where(r => r.WarehouseRegionCode == warehouseRegionCode);
            }
            //查询出库计划
            if(partsOutPlanId.HasValue) {
                var partsOutboundPlan = ObjectContext.PartsOutboundPlans.SingleOrDefault(r => r.Id == partsOutPlanId.Value);
                if(partsOutboundPlan == null)
                    return null;
                var partsOutboundPlanDetailPartIds = ObjectContext.PartsOutboundPlanDetails.Where(r => r.PartsOutboundPlanId == partsOutboundPlan.Id && (r.OutboundFulfillment == null || r.PlannedAmount != r.OutboundFulfillment)).Select(r => r.SparePartId).ToArray();
                if(partsOutboundPlanDetailPartIds.Length == 0)
                    return null;
                resultPartsStock = resultPartsStock.Where(r => r.WarehouseId == partsOutboundPlan.WarehouseId && partsOutboundPlanDetailPartIds.Contains(r.SparePartId));
            }
            if(warehouseAreaCategoryValue.HasValue)
                resultPartsStock = resultPartsStock.Where(r => r.Category == warehouseAreaCategoryValue.Value);
            //左外联库存批次
            var result = from r in resultPartsStock
                     //    join partsStockBatchDetail in ObjectContext.PartsStockBatchDetails on r.PartsStockId equals partsStockBatchDetail.PartsStockId into details
                      //   from v in details.DefaultIfEmpty()
                         join s in ObjectContext.PartsSalesPrices.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on new {
                             t1 = r.PartsSalesCategoryId,
                             t2 = r.SparePartId
                         }
                         equals new {
                             t1 = s.PartsSalesCategoryId,
                             t2 = s.SparePartId
                         }
                         into a
                         from b in a.DefaultIfEmpty()
                         select new VirtualPartsStock {
                             PartsStockId = r.PartsStockId,
                             WarehouseId = r.WarehouseId,
                             WarehouseCode = r.WarehouseCode,
                             WarehouseName = r.WarehouseName,
                             StorageCompanyId = r.StorageCompanyId,
                             StorageCompanyType = r.StorageCompanyType,
                             BranchId = r.BranchId,
                             SparePartId = r.SparePartId,
                             SparePartCode = r.SparePartCode,
                             SparePartName = r.SparePartName,
                             MeasureUnit = r.MeasureUnit,
                             WarehouseAreaId = r.WarehouseAreaId,
                             WarehouseAreaCode = r.WarehouseAreaCode,
                             Quantity = r.Quantity,
                            // UsableQuantity = (int?)v.Quantity ?? r.Quantity,//此处不能改为v.Quantity 因join 可能导致v.Quantity为Null 所以强制转换有效
                           //  BatchNumber = v.BatchNumber ?? "",
                             WarehouseAreaCategory = r.Category,
                           //  InboundTime = v.InboundTime,
                             WarehouseRegionCode = r.WarehouseRegionCode,
                             PriceType = b.PriceType,
                             SalesPrice = b == null ? 0 : b.SalesPrice,
                             ReferenceCode = r.ReferenceCode

                         };
            if(pricetype.HasValue) {
                return result.Where(s => s.PriceType == pricetype).OrderBy(r => r.PartsStockId);
            }
            return result.OrderBy(r => r.PartsStockId);
        }



        public IQueryable<VirtualPartsStock> 查询配件库位库存打印(int? warehouseAreaCategoryValue, int? warehouseId, string sparePartCode, string sparePartName, string warehouseAreaCode) {
            //库位id（非必填）、 配件编号（非必填）、仓库Id（非必填）、配件名称（非必填）、库区编号（非必填）
            var userInfo = Utils.GetCurrentUserInfo();
            var resultPartsStock = from partsStock in ObjectContext.PartsStocks.Where(r => r.StorageCompanyId == userInfo.EnterpriseId && (!warehouseId.HasValue || r.WarehouseId == warehouseId))
                                   from warehouseArea in ObjectContext.WarehouseAreas.Where(r => r.AreaKind == (int)DcsAreaKind.库位 && r.Status == (int)DcsBaseDataStatus.有效 && (warehouseAreaCode == null || warehouseAreaCode.Trim() == string.Empty || r.Code == warehouseAreaCode))
                                   from warehouseRegion in ObjectContext.WarehouseAreas.Where(r => r.Status == (int)DcsBaseDataStatus.有效)
                                   from warehouse in ObjectContext.Warehouses.Where(r => r.Status == (int)DcsBaseDataStatus.有效)
                                   from sparePart in ObjectContext.SpareParts.Where(r => r.Status == (int)DcsMasterDataStatus.有效 && (sparePartCode == null || sparePartCode.Trim() == string.Empty || r.Code == sparePartCode) && (sparePartName == null || sparePartName.Trim() == string.Empty || r.Name == sparePartName))
                                   from warehouseAreaCategory in ObjectContext.WarehouseAreaCategories.Where(r => (!warehouseAreaCategoryValue.HasValue || r.Category == warehouseAreaCategoryValue.Value))
                                   where partsStock.WarehouseAreaId == warehouseArea.Id
                                   && partsStock.WarehouseId == warehouse.Id
                                   && partsStock.PartId == sparePart.Id
                                   && partsStock.WarehouseAreaCategoryId == warehouseAreaCategory.Id
                                   && warehouseArea.TopLevelWarehouseAreaId == warehouseRegion.Id
                                   select new {
                                       PartsStockId = partsStock.Id,
                                       partsStock.WarehouseId,
                                       WarehouseCode = warehouse.Code,
                                       WarehouseName = warehouse.Name,
                                       partsStock.StorageCompanyId,
                                       partsStock.StorageCompanyType,
                                       partsStock.BranchId,
                                       SparePartId = sparePart.Id,
                                       SparePartCode = sparePart.Code,
                                       SparePartName = sparePart.Name,
                                       sparePart.MeasureUnit,
                                       partsStock.WarehouseAreaId,//库位Id
                                       WarehouseAreaCode = warehouseArea.Code,//库位编号
                                       partsStock.Quantity,
                                       warehouseArea.AreaCategoryId,
                                       warehouseAreaCategory.Category,//库区用途
                                       warehouseArea.TopLevelWarehouseAreaId,//顶层库区Id
                                       WarehouseRegionCode = warehouseRegion.Code//库区编号
                                   };
            //左外联库存批次
            var result = from r in resultPartsStock
                         join partsStockBatchDetail in ObjectContext.PartsStockBatchDetails on r.PartsStockId equals partsStockBatchDetail.PartsStockId into details
                         from v in details.DefaultIfEmpty()
                         select new VirtualPartsStock {
                             PartsStockId = r.PartsStockId,
                             WarehouseId = r.WarehouseId,
                             WarehouseCode = r.WarehouseCode,
                             WarehouseName = r.WarehouseName,
                             StorageCompanyId = r.StorageCompanyId,
                             StorageCompanyType = r.StorageCompanyType,
                             BranchId = r.BranchId,
                             SparePartId = r.SparePartId,
                             SparePartCode = r.SparePartCode,
                             SparePartName = r.SparePartName,
                             MeasureUnit = r.MeasureUnit,
                             WarehouseAreaId = r.WarehouseAreaId,
                             WarehouseAreaCode = r.WarehouseAreaCode,
                             Quantity = r.Quantity,
                             UsableQuantity = (int?)v.Quantity ?? r.Quantity,//此处不能改为v.Quantity 因join 可能导致v.Quantity为Null 所以强制转换有效
                             BatchNumber = v.BatchNumber ?? "",
                             WarehouseAreaCategory = r.Category,
                             InboundTime = v.InboundTime,
                             WarehouseRegionCode = r.WarehouseRegionCode,
                         };
            return result.OrderBy(r => r.PartsStockId);
        }


        public IEnumerable<VirtualOutboundAndInboundBill> 查询历史出入库记录(int sparePartId, int warehouseId, string billBusinessType, int? partsSalesCategoryId, int? partsSalesOrderTypeId, string warehouseAreaCode) {
            if(billBusinessType.Equals("出库")) {
                if(partsSalesCategoryId != null || partsSalesOrderTypeId != null) {
                    if(!string.IsNullOrEmpty(warehouseAreaCode)) {
                        var partsOutboundBills = from partsOutboundBill in ObjectContext.PartsOutboundBills
                                                 from partsOutboundBillDetail in ObjectContext.PartsOutboundBillDetails
                                                 join partsSalesCategory in ObjectContext.PartsSalesCategories on partsOutboundBill.PartsSalesCategoryId equals partsSalesCategory.Id
                                                 join a in ObjectContext.PartsSalesOrderTypes on partsOutboundBill.PartsSalesOrderTypeId equals a.Id into tempTable
                                                 from partsSalesOrderType in tempTable.DefaultIfEmpty()
                                                 where partsOutboundBill.WarehouseId == warehouseId && partsOutboundBillDetail.SparePartId == sparePartId && (partsSalesCategoryId == null || partsOutboundBill.PartsSalesCategoryId == partsSalesCategoryId) && (partsSalesOrderTypeId == null || partsOutboundBill.PartsSalesOrderTypeId == partsSalesOrderTypeId) && partsOutboundBill.Id == partsOutboundBillDetail.PartsOutboundBillId && partsOutboundBillDetail.WarehouseAreaCode.Contains(warehouseAreaCode)
                                                 select new VirtualOutboundAndInboundBill {
                                                     BillId = partsOutboundBillDetail.Id,
                                                     WarehouseCode = partsOutboundBill.WarehouseCode,
                                                     WarehouseName = partsOutboundBill.WarehouseName,
                                                     ReceivingWarehouseCode = partsOutboundBill.ReceivingWarehouseCode,
                                                     ReceivingWarehouseName = partsOutboundBill.ReceivingWarehouseName,
                                                     BranchName = partsOutboundBill.BranchName,
                                                     BillCode = partsOutboundBill.Code,
                                                     BillBusinessType = partsOutboundBill.OutboundType,
                                                     CounterpartCompanyCode = partsOutboundBill.CounterpartCompanyCode,
                                                     CounterpartCompanyName = partsOutboundBill.CounterpartCompanyName,
                                                     SparePartCode = partsOutboundBillDetail.SparePartCode,
                                                     SparePartName = partsOutboundBillDetail.SparePartName,
                                                     Quantity = partsOutboundBillDetail.OutboundAmount,
                                                     CreateTime = partsOutboundBill.CreateTime ?? DateTime.MinValue,
                                                     WholesalePrice = partsOutboundBillDetail.SettlementPrice,
                                                     PartsSalesCategoryName = partsSalesCategory.Name,
                                                     PartsSalesOrderTypeName = partsSalesOrderType.Name,
                                                     WarehouseAreaCode = partsOutboundBillDetail.WarehouseAreaCode
                                                 };
                        var agencyPartsOutboundBills = from partsOutboundBill in ObjectContext.AgencyPartsOutboundBills
                                                       from partsOutboundBillDetail in ObjectContext.APartsOutboundBillDetails
                                                       join partsSalesCategory in ObjectContext.PartsSalesCategories on partsOutboundBill.PartsSalesCategoryId equals partsSalesCategory.Id
                                                       join a in ObjectContext.PartsSalesOrderTypes on partsOutboundBill.PartsSalesOrderTypeId equals a.Id into tempTable
                                                       from partsSalesOrderType in tempTable.DefaultIfEmpty()
                                                       where partsOutboundBill.WarehouseId == warehouseId && partsOutboundBillDetail.SparePartId == sparePartId && (partsSalesCategoryId == null || partsOutboundBill.PartsSalesCategoryId == partsSalesCategoryId) && (partsSalesOrderTypeId == null || partsOutboundBill.PartsSalesOrderTypeId == partsSalesOrderTypeId) && partsOutboundBill.Id == partsOutboundBillDetail.PartsOutboundBillId && partsOutboundBillDetail.WarehouseAreaCode.Contains(warehouseAreaCode)
                                                       select new VirtualOutboundAndInboundBill {
                                                           BillId = partsOutboundBillDetail.Id,
                                                           WarehouseCode = partsOutboundBill.WarehouseCode,
                                                           WarehouseName = partsOutboundBill.WarehouseName,
                                                           ReceivingWarehouseCode = partsOutboundBill.ReceivingWarehouseCode,
                                                           ReceivingWarehouseName = partsOutboundBill.ReceivingWarehouseName,
                                                           BranchName = partsOutboundBill.BranchName,
                                                           BillCode = partsOutboundBill.Code,
                                                           BillBusinessType = partsOutboundBill.OutboundType,
                                                           CounterpartCompanyCode = partsOutboundBill.CounterpartCompanyCode,
                                                           CounterpartCompanyName = partsOutboundBill.CounterpartCompanyName,
                                                           SparePartCode = partsOutboundBillDetail.SparePartCode,
                                                           SparePartName = partsOutboundBillDetail.SparePartName,
                                                           Quantity = partsOutboundBillDetail.OutboundAmount,
                                                           CreateTime = partsOutboundBill.CreateTime ?? DateTime.MinValue,
                                                           WholesalePrice = partsOutboundBillDetail.SettlementPrice,
                                                           PartsSalesCategoryName = partsSalesCategory.Name,
                                                           PartsSalesOrderTypeName = partsSalesOrderType.Name,
                                                           WarehouseAreaCode = partsOutboundBillDetail.WarehouseAreaCode
                                                       };
                        return partsOutboundBills.Union(agencyPartsOutboundBills).OrderBy(r => r.BillId);
                    } else {
                        var partsOutboundBills = from partsOutboundBill in ObjectContext.PartsOutboundBills
                                                 from partsOutboundBillDetail in ObjectContext.PartsOutboundBillDetails
                                                 join partsSalesCategory in ObjectContext.PartsSalesCategories on partsOutboundBill.PartsSalesCategoryId equals partsSalesCategory.Id
                                                 join a in ObjectContext.PartsSalesOrderTypes on partsOutboundBill.PartsSalesOrderTypeId equals a.Id into tempTable
                                                 from partsSalesOrderType in tempTable.DefaultIfEmpty()
                                                 where partsOutboundBill.WarehouseId == warehouseId && partsOutboundBillDetail.SparePartId == sparePartId && (partsSalesCategoryId == null || partsOutboundBill.PartsSalesCategoryId == partsSalesCategoryId) && (partsSalesOrderTypeId == null || partsOutboundBill.PartsSalesOrderTypeId == partsSalesOrderTypeId) && partsOutboundBill.Id == partsOutboundBillDetail.PartsOutboundBillId
                                                 select new VirtualOutboundAndInboundBill {
                                                     BillId = partsOutboundBillDetail.Id,
                                                     WarehouseCode = partsOutboundBill.WarehouseCode,
                                                     WarehouseName = partsOutboundBill.WarehouseName,
                                                     ReceivingWarehouseCode = partsOutboundBill.ReceivingWarehouseCode,
                                                     ReceivingWarehouseName = partsOutboundBill.ReceivingWarehouseName,
                                                     BranchName = partsOutboundBill.BranchName,
                                                     BillCode = partsOutboundBill.Code,
                                                     BillBusinessType = partsOutboundBill.OutboundType,
                                                     CounterpartCompanyCode = partsOutboundBill.CounterpartCompanyCode,
                                                     CounterpartCompanyName = partsOutboundBill.CounterpartCompanyName,
                                                     SparePartCode = partsOutboundBillDetail.SparePartCode,
                                                     SparePartName = partsOutboundBillDetail.SparePartName,
                                                     Quantity = partsOutboundBillDetail.OutboundAmount,
                                                     CreateTime = partsOutboundBill.CreateTime ?? DateTime.MinValue,
                                                     WholesalePrice = partsOutboundBillDetail.SettlementPrice,
                                                     PartsSalesCategoryName = partsSalesCategory.Name,
                                                     PartsSalesOrderTypeName = partsSalesOrderType.Name,
                                                     WarehouseAreaCode = partsOutboundBillDetail.WarehouseAreaCode
                                                 };
                        var agencyPartsOutboundBills = from partsOutboundBill in ObjectContext.AgencyPartsOutboundBills
                                                       from partsOutboundBillDetail in ObjectContext.APartsOutboundBillDetails
                                                       join partsSalesCategory in ObjectContext.PartsSalesCategories on partsOutboundBill.PartsSalesCategoryId equals partsSalesCategory.Id
                                                       join a in ObjectContext.PartsSalesOrderTypes on partsOutboundBill.PartsSalesOrderTypeId equals a.Id into tempTable
                                                       from partsSalesOrderType in tempTable.DefaultIfEmpty()
                                                       where partsOutboundBill.WarehouseId == warehouseId && partsOutboundBillDetail.SparePartId == sparePartId && (partsSalesCategoryId == null || partsOutboundBill.PartsSalesCategoryId == partsSalesCategoryId) && (partsSalesOrderTypeId == null || partsOutboundBill.PartsSalesOrderTypeId == partsSalesOrderTypeId) && partsOutboundBill.Id == partsOutboundBillDetail.PartsOutboundBillId
                                                       select new VirtualOutboundAndInboundBill {
                                                           BillId = partsOutboundBillDetail.Id,
                                                           WarehouseCode = partsOutboundBill.WarehouseCode,
                                                           WarehouseName = partsOutboundBill.WarehouseName,
                                                           ReceivingWarehouseCode = partsOutboundBill.ReceivingWarehouseCode,
                                                           ReceivingWarehouseName = partsOutboundBill.ReceivingWarehouseName,
                                                           BranchName = partsOutboundBill.BranchName,
                                                           BillCode = partsOutboundBill.Code,
                                                           BillBusinessType = partsOutboundBill.OutboundType,
                                                           CounterpartCompanyCode = partsOutboundBill.CounterpartCompanyCode,
                                                           CounterpartCompanyName = partsOutboundBill.CounterpartCompanyName,
                                                           SparePartCode = partsOutboundBillDetail.SparePartCode,
                                                           SparePartName = partsOutboundBillDetail.SparePartName,
                                                           Quantity = partsOutboundBillDetail.OutboundAmount,
                                                           CreateTime = partsOutboundBill.CreateTime ?? DateTime.MinValue,
                                                           WholesalePrice = partsOutboundBillDetail.SettlementPrice,
                                                           PartsSalesCategoryName = partsSalesCategory.Name,
                                                           PartsSalesOrderTypeName = partsSalesOrderType.Name,
                                                           WarehouseAreaCode = partsOutboundBillDetail.WarehouseAreaCode
                                                       };
                        return partsOutboundBills.Union(agencyPartsOutboundBills).OrderBy(r => r.BillId);
                    }
                } else {
                    if(!string.IsNullOrEmpty(warehouseAreaCode)) {
                        var partsOutboundBills = from partsOutboundBill in ObjectContext.PartsOutboundBills
                                                 from partsOutboundBillDetail in ObjectContext.PartsOutboundBillDetails
                                                 join partsSalesCategory in ObjectContext.PartsSalesCategories on partsOutboundBill.PartsSalesCategoryId equals partsSalesCategory.Id
                                                 join a in ObjectContext.PartsSalesOrderTypes on partsOutboundBill.PartsSalesOrderTypeId equals a.Id into tempTable
                                                 from partsSalesOrderType in tempTable.DefaultIfEmpty()
                                                 where partsOutboundBill.WarehouseId == warehouseId && partsOutboundBillDetail.SparePartId == sparePartId && partsOutboundBill.Id == partsOutboundBillDetail.PartsOutboundBillId && partsOutboundBillDetail.WarehouseAreaCode.Contains(warehouseAreaCode)
                                                 select new VirtualOutboundAndInboundBill {
                                                     BillId = partsOutboundBillDetail.Id,
                                                     WarehouseCode = partsOutboundBill.WarehouseCode,
                                                     WarehouseName = partsOutboundBill.WarehouseName,
                                                     ReceivingWarehouseCode = partsOutboundBill.ReceivingWarehouseCode,
                                                     ReceivingWarehouseName = partsOutboundBill.ReceivingWarehouseName,
                                                     BranchName = partsOutboundBill.BranchName,
                                                     BillCode = partsOutboundBill.Code,
                                                     BillBusinessType = partsOutboundBill.OutboundType,
                                                     CounterpartCompanyCode = partsOutboundBill.CounterpartCompanyCode,
                                                     CounterpartCompanyName = partsOutboundBill.CounterpartCompanyName,
                                                     SparePartCode = partsOutboundBillDetail.SparePartCode,
                                                     SparePartName = partsOutboundBillDetail.SparePartName,
                                                     Quantity = partsOutboundBillDetail.OutboundAmount,
                                                     CreateTime = partsOutboundBill.CreateTime ?? DateTime.MinValue,
                                                     WholesalePrice = partsOutboundBillDetail.SettlementPrice,
                                                     PartsSalesCategoryName = partsSalesCategory.Name,
                                                     PartsSalesOrderTypeName = partsSalesOrderType.Name,
                                                     WarehouseAreaCode = partsOutboundBillDetail.WarehouseAreaCode
                                                 };
                        var agencyPartsOutboundBills = from partsOutboundBill in ObjectContext.AgencyPartsOutboundBills
                                                       from partsOutboundBillDetail in ObjectContext.APartsOutboundBillDetails
                                                       join partsSalesCategory in ObjectContext.PartsSalesCategories on partsOutboundBill.PartsSalesCategoryId equals partsSalesCategory.Id
                                                       join a in ObjectContext.PartsSalesOrderTypes on partsOutboundBill.PartsSalesOrderTypeId equals a.Id into tempTable
                                                       from partsSalesOrderType in tempTable.DefaultIfEmpty()
                                                       where partsOutboundBill.WarehouseId == warehouseId && partsOutboundBillDetail.SparePartId == sparePartId && partsOutboundBill.Id == partsOutboundBillDetail.PartsOutboundBillId && partsOutboundBillDetail.WarehouseAreaCode.Contains(warehouseAreaCode)
                                                       select new VirtualOutboundAndInboundBill {
                                                           BillId = partsOutboundBillDetail.Id,
                                                           WarehouseCode = partsOutboundBill.WarehouseCode,
                                                           WarehouseName = partsOutboundBill.WarehouseName,
                                                           ReceivingWarehouseCode = partsOutboundBill.ReceivingWarehouseCode,
                                                           ReceivingWarehouseName = partsOutboundBill.ReceivingWarehouseName,
                                                           BranchName = partsOutboundBill.BranchName,
                                                           BillCode = partsOutboundBill.Code,
                                                           BillBusinessType = partsOutboundBill.OutboundType,
                                                           CounterpartCompanyCode = partsOutboundBill.CounterpartCompanyCode,
                                                           CounterpartCompanyName = partsOutboundBill.CounterpartCompanyName,
                                                           SparePartCode = partsOutboundBillDetail.SparePartCode,
                                                           SparePartName = partsOutboundBillDetail.SparePartName,
                                                           Quantity = partsOutboundBillDetail.OutboundAmount,
                                                           CreateTime = partsOutboundBill.CreateTime ?? DateTime.MinValue,
                                                           WholesalePrice = partsOutboundBillDetail.SettlementPrice,
                                                           PartsSalesCategoryName = partsSalesCategory.Name,
                                                           PartsSalesOrderTypeName = partsSalesOrderType.Name,
                                                           WarehouseAreaCode = partsOutboundBillDetail.WarehouseAreaCode
                                                       };
                        return partsOutboundBills.Union(agencyPartsOutboundBills).OrderBy(r => r.BillId);
                    } else {
                        var partsOutboundBills = from partsOutboundBill in ObjectContext.PartsOutboundBills
                                                 from partsOutboundBillDetail in ObjectContext.PartsOutboundBillDetails
                                                 join partsSalesCategory in ObjectContext.PartsSalesCategories on partsOutboundBill.PartsSalesCategoryId equals partsSalesCategory.Id
                                                 join a in ObjectContext.PartsSalesOrderTypes on partsOutboundBill.PartsSalesOrderTypeId equals a.Id into tempTable
                                                 from partsSalesOrderType in tempTable.DefaultIfEmpty()
                                                 where partsOutboundBill.WarehouseId == warehouseId && partsOutboundBillDetail.SparePartId == sparePartId && partsOutboundBill.Id == partsOutboundBillDetail.PartsOutboundBillId
                                                 select new VirtualOutboundAndInboundBill {
                                                     BillId = partsOutboundBillDetail.Id,
                                                     WarehouseCode = partsOutboundBill.WarehouseCode,
                                                     WarehouseName = partsOutboundBill.WarehouseName,
                                                     ReceivingWarehouseCode = partsOutboundBill.ReceivingWarehouseCode,
                                                     ReceivingWarehouseName = partsOutboundBill.ReceivingWarehouseName,
                                                     BranchName = partsOutboundBill.BranchName,
                                                     BillCode = partsOutboundBill.Code,
                                                     BillBusinessType = partsOutboundBill.OutboundType,
                                                     CounterpartCompanyCode = partsOutboundBill.CounterpartCompanyCode,
                                                     CounterpartCompanyName = partsOutboundBill.CounterpartCompanyName,
                                                     SparePartCode = partsOutboundBillDetail.SparePartCode,
                                                     SparePartName = partsOutboundBillDetail.SparePartName,
                                                     Quantity = partsOutboundBillDetail.OutboundAmount,
                                                     CreateTime = partsOutboundBill.CreateTime ?? DateTime.MinValue,
                                                     WholesalePrice = partsOutboundBillDetail.SettlementPrice,
                                                     PartsSalesCategoryName = partsSalesCategory.Name,
                                                     PartsSalesOrderTypeName = partsSalesOrderType.Name,
                                                     WarehouseAreaCode = partsOutboundBillDetail.WarehouseAreaCode
                                                 };
                        var agencyPartsOutboundBills = from partsOutboundBill in ObjectContext.AgencyPartsOutboundBills
                                                       from partsOutboundBillDetail in ObjectContext.APartsOutboundBillDetails
                                                       join partsSalesCategory in ObjectContext.PartsSalesCategories on partsOutboundBill.PartsSalesCategoryId equals partsSalesCategory.Id
                                                       join a in ObjectContext.PartsSalesOrderTypes on partsOutboundBill.PartsSalesOrderTypeId equals a.Id into tempTable
                                                       from partsSalesOrderType in tempTable.DefaultIfEmpty()
                                                       where partsOutboundBill.WarehouseId == warehouseId && partsOutboundBillDetail.SparePartId == sparePartId && partsOutboundBill.Id == partsOutboundBillDetail.PartsOutboundBillId
                                                       select new VirtualOutboundAndInboundBill {
                                                           BillId = partsOutboundBillDetail.Id,
                                                           WarehouseCode = partsOutboundBill.WarehouseCode,
                                                           WarehouseName = partsOutboundBill.WarehouseName,
                                                           ReceivingWarehouseCode = partsOutboundBill.ReceivingWarehouseCode,
                                                           ReceivingWarehouseName = partsOutboundBill.ReceivingWarehouseName,
                                                           BranchName = partsOutboundBill.BranchName,
                                                           BillCode = partsOutboundBill.Code,
                                                           BillBusinessType = partsOutboundBill.OutboundType,
                                                           CounterpartCompanyCode = partsOutboundBill.CounterpartCompanyCode,
                                                           CounterpartCompanyName = partsOutboundBill.CounterpartCompanyName,
                                                           SparePartCode = partsOutboundBillDetail.SparePartCode,
                                                           SparePartName = partsOutboundBillDetail.SparePartName,
                                                           Quantity = partsOutboundBillDetail.OutboundAmount,
                                                           CreateTime = partsOutboundBill.CreateTime ?? DateTime.MinValue,
                                                           WholesalePrice = partsOutboundBillDetail.SettlementPrice,
                                                           PartsSalesCategoryName = partsSalesCategory.Name,
                                                           PartsSalesOrderTypeName = partsSalesOrderType.Name,
                                                           WarehouseAreaCode = partsOutboundBillDetail.WarehouseAreaCode
                                                       };
                        return partsOutboundBills.Union(agencyPartsOutboundBills).OrderBy(r => r.BillId);
                    }
                }
            }
            if(billBusinessType.Equals("入库")) {
                var warehouseAreaIds = ObjectContext.WarehouseAreas.Where(r => r.Status == (int)DcsBaseDataStatus.有效 &&
                    ObjectContext.WarehouseAreaCategories.Any(w => w.Category == (int)DcsAreaType.保管区 && w.Id == r.AreaCategoryId)).Select(p => p.Id);
                var partsStocks = ObjectContext.PartsStocks.Where(p => p.WarehouseId == warehouseId && p.PartId == sparePartId && warehouseAreaIds.Contains(p.WarehouseAreaId));
                var items = from partsStock in partsStocks
                            join warehouseArea in ObjectContext.WarehouseAreas on partsStock.WarehouseAreaId equals warehouseArea.Id
                            select new {
                                partsStock.WarehouseId,
                                warehouseArea.Code,
                                partsStock.PartId,
                                partsStock.WarehouseAreaId
                            };
                var partsInboundCheckBills = from partsInboundCheckBill in ObjectContext.PartsInboundCheckBills
                                             from partsInboundCheckBillDetail in ObjectContext.PartsInboundCheckBillDetails
                                             join partsStock in items on new {
                                                 warehouseIds = partsInboundCheckBill.WarehouseId,
                                                 sparePartIds = partsInboundCheckBillDetail.SparePartId,
                                                 warehouseareId = partsInboundCheckBillDetail.WarehouseAreaId
                                             } equals new {
                                                 warehouseIds = partsStock.WarehouseId,
                                                 sparePartIds = partsStock.PartId,
                                                 warehouseareId = partsStock.WarehouseAreaId
                                             } into tempTable
                                             from partsStock in tempTable.DefaultIfEmpty()
                                             where partsInboundCheckBill.WarehouseId == warehouseId && partsInboundCheckBillDetail.SparePartId == sparePartId
                                             && partsInboundCheckBill.Id == partsInboundCheckBillDetail.PartsInboundCheckBillId
                                             select new VirtualOutboundAndInboundBill {
                                                 BillId = partsInboundCheckBillDetail.Id,
                                                 WarehouseCode = partsInboundCheckBill.WarehouseCode,
                                                 WarehouseName = partsInboundCheckBill.WarehouseName,
                                                 BranchName = partsInboundCheckBill.BranchName,
                                                 BillCode = partsInboundCheckBill.Code,
                                                 BillBusinessType = partsInboundCheckBill.InboundType,
                                                 CounterpartCompanyCode = partsInboundCheckBill.CounterpartCompanyCode,
                                                 CounterpartCompanyName = partsInboundCheckBill.CounterpartCompanyName,
                                                 SparePartCode = partsInboundCheckBillDetail.SparePartCode,
                                                 SparePartName = partsInboundCheckBillDetail.SparePartName,
                                                 Quantity = partsInboundCheckBillDetail.InspectedQuantity,
                                                 CreateTime = partsInboundCheckBill.CreateTime ?? DateTime.MinValue,
                                                 WholesalePrice = partsInboundCheckBillDetail.CostPrice,
                                                 WarehouseAreaCode = partsStock.Code
                                             };
                return partsInboundCheckBills;
            }
            return null;
        }



        public IEnumerable<VirtualPartsStock> 查询企业间调拨库位库存(int? companyTransferOrderId, int? warehouseAreaCategoryValue) {
            var resultPartsStock = from partsStock in ObjectContext.PartsStocks
                                   from warehouseArea in ObjectContext.WarehouseAreas.Where(r => r.AreaKind == (int)DcsAreaKind.库位 && r.Status == (int)DcsBaseDataStatus.有效)
                                   from warehouseRegion in ObjectContext.WarehouseAreas.Where(r => r.Status == (int)DcsBaseDataStatus.有效)
                                   from warehouse in ObjectContext.Warehouses.Where(r => r.Status == (int)DcsBaseDataStatus.有效)
                                   from sparePart in ObjectContext.SpareParts.Where(r => r.Status == (int)DcsMasterDataStatus.有效)
                                   from warehouseAreaCategory in ObjectContext.WarehouseAreaCategories
                                   where partsStock.WarehouseAreaId == warehouseArea.Id
                                   && partsStock.WarehouseId == warehouse.Id
                                   && partsStock.PartId == sparePart.Id
                                   && partsStock.WarehouseAreaCategoryId == warehouseAreaCategory.Id
                                   && warehouseArea.TopLevelWarehouseAreaId == warehouseRegion.Id
                                   select new {
                                       PartsStockId = partsStock.Id,
                                       partsStock.WarehouseId,
                                       WarehouseCode = warehouse.Code,
                                       WarehouseName = warehouse.Name,
                                       partsStock.StorageCompanyId,
                                       partsStock.StorageCompanyType,
                                       partsStock.BranchId,
                                       SparePartId = sparePart.Id,
                                       SparePartCode = sparePart.Code,
                                       SparePartName = sparePart.Name,
                                       sparePart.MeasureUnit,
                                       partsStock.WarehouseAreaId,//库位Id
                                       WarehouseAreaCode = warehouseArea.Code,//库位编号
                                       partsStock.Quantity,
                                       warehouseArea.AreaCategoryId,
                                       warehouseAreaCategory.Category,//库区用途
                                       warehouseArea.TopLevelWarehouseAreaId,//顶层库区Id
                                       WarehouseRegionCode = warehouseRegion.Code//库区编号
                                   };
            //查询出库计划
            if(companyTransferOrderId.HasValue) {
                var companyTransferOrder = ObjectContext.CompanyTransferOrders.SingleOrDefault(r => r.Id == companyTransferOrderId.Value);
                if(companyTransferOrder == null)
                    return Enumerable.Empty<VirtualPartsStock>();
                var companyTransferOrderDetailPartIds = ObjectContext.CompanyTransferOrderDetails.Where(r => r.PartsTransferOrderId == companyTransferOrderId).Select(r => r.PartsId).ToArray();
                if(companyTransferOrderDetailPartIds.Length == 0)
                    return Enumerable.Empty<VirtualPartsStock>();
                resultPartsStock = resultPartsStock.Where(r => r.WarehouseId == companyTransferOrder.OriginalWarehouseId && companyTransferOrderDetailPartIds.Contains(r.SparePartId));
            }
            if(warehouseAreaCategoryValue.HasValue)
                resultPartsStock = resultPartsStock.Where(r => r.Category == warehouseAreaCategoryValue.Value);
            //左外联库存批次
            var result = from r in resultPartsStock
                         join partsStockBatchDetail in ObjectContext.PartsStockBatchDetails on r.PartsStockId equals partsStockBatchDetail.PartsStockId into details
                         from v in details.DefaultIfEmpty()
                         select new VirtualPartsStock {
                             PartsStockId = r.PartsStockId,
                             WarehouseId = r.WarehouseId,
                             WarehouseCode = r.WarehouseCode,
                             WarehouseName = r.WarehouseName,
                             StorageCompanyId = r.StorageCompanyId,
                             StorageCompanyType = r.StorageCompanyType,
                             BranchId = r.BranchId,
                             SparePartId = r.SparePartId,
                             SparePartCode = r.SparePartCode,
                             SparePartName = r.SparePartName,
                             MeasureUnit = r.MeasureUnit,
                             WarehouseAreaId = r.WarehouseAreaId,
                             WarehouseAreaCode = r.WarehouseAreaCode,
                             Quantity = r.Quantity,
                             UsableQuantity = (int?)v.Quantity ?? r.Quantity,//此处不能改为v.Quantity 因join 可能导致v.Quantity为Null 所以强制转换有效
                             BatchNumber = v.BatchNumber ?? "",
                             WarehouseAreaCategory = r.Category,
                             InboundTime = v.InboundTime,
                             WarehouseRegionCode = r.WarehouseRegionCode,
                         };
            return result.OrderBy(r => r.PartsStockId);
        }

    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:33
        //原分布类的函数全部转移到Ach                                                               

        public IEnumerable<VirtualAgencyPartsStock> 查询中心库配件库存(string companyCode, string companyName, string sparePartCode, string sparePartName, bool? greaterThanZero,bool? isAgencyQuery) {

            return new VirtualPartsStockAch(this).查询中心库配件库存(companyCode,companyName,sparePartCode,sparePartName,greaterThanZero,isAgencyQuery);
        }
        public IEnumerable<VirtualPartsStock> 查询待包装配件库存() {
            return new VirtualPartsStockAch(this).查询待包装配件库存();
        }

        public IQueryable<VirtualPartsStock> 查询待上架配件库存(int warehouseId) {
            return new VirtualPartsStockAch(this).查询待上架配件库存(warehouseId);
        }

        public IQueryable<VirtualPartsStock> 查询配件库位库存(int? partsOutPlanId, int? warehouseAreaCategoryValue, int? operatorId, int? managerId, string warehouseRegionCode, int? pricetype, bool? greaterThanZero) {
            return new VirtualPartsStockAch(this).查询配件库位库存(partsOutPlanId, warehouseAreaCategoryValue, operatorId, managerId, warehouseRegionCode, pricetype, greaterThanZero);
        }

        public IQueryable<PartsStock> GetVirtualPartsStocks(bool? greaterThanZero) {
            return new VirtualPartsStockAch(this).GetVirtualPartsStocks(greaterThanZero);
        }

        [Query(HasSideEffects = true)]
        public IQueryable<VirtualPartsStock> 查询配件库位库存1(int? partsOutPlanId, int? warehouseAreaCategoryValue, int? operatorId, int? managerId, string warehouseRegionCode, int? pricetype, bool? greaterThanZero,string sparePartCodes) {
            return new VirtualPartsStockAch(this).查询配件库位库存1(partsOutPlanId, warehouseAreaCategoryValue, operatorId, managerId, warehouseRegionCode, pricetype, greaterThanZero,sparePartCodes);
        }

        public IQueryable<PartsStock> GetVirtualPartsStocks1(bool? greaterThanZero) {
            return new VirtualPartsStockAch(this).GetVirtualPartsStocks1(greaterThanZero);
        }


        public IQueryable<VirtualPartsStock> 查询配件库位库存带营销信息(int? partsOutPlanId, int? warehouseAreaCategoryValue, int? operatorId, int? managerId, string warehouseRegionCode, int? pricetype) {
            return new VirtualPartsStockAch(this).查询配件库位库存带营销信息(partsOutPlanId, warehouseAreaCategoryValue, operatorId, managerId, warehouseRegionCode, pricetype);
        }

        public IQueryable<VirtualPartsStock> 查询配件库位库存打印(int? warehouseAreaCategoryValue, int? warehouseId, string sparePartCode, string sparePartName, string warehouseAreaCode) {
            return new VirtualPartsStockAch(this).查询配件库位库存打印(warehouseAreaCategoryValue, warehouseId, sparePartCode, sparePartName, warehouseAreaCode);
        }


        public IEnumerable<VirtualOutboundAndInboundBill> 查询历史出入库记录(int sparePartId, int warehouseId, string billBusinessType, int? partsSalesCategoryId, int? partsSalesOrderTypeId, string warehouseAreaCode) {
            return new VirtualPartsStockAch(this).查询历史出入库记录(sparePartId, warehouseId, billBusinessType, partsSalesCategoryId, partsSalesOrderTypeId, warehouseAreaCode);
        }

        public IEnumerable<VirtualPartsStock> 查询企业间调拨库位库存(int? companyTransferOrderId, int? warehouseAreaCategoryValue) {
            return new VirtualPartsStockAch(this).查询企业间调拨库位库存(companyTransferOrderId, warehouseAreaCategoryValue);
        }

        public IQueryable<VirtualPartsStock> 查询配件库位库存For代理库电商订单(int warehouseId, int operatorId) {
            return new VirtualPartsStockAch(this).查询配件库位库存For代理库电商订单(warehouseId, operatorId);
        }
        public IEnumerable<VirtualAgencyPartsStock> 大服务站库存查询(string companyCode, string companyName, string sparePartCode, string sparePartName, bool? greaterThanZero) {
            return new VirtualPartsStockAch(this).大服务站库存查询(companyCode, companyName, sparePartCode, sparePartName, greaterThanZero);

        }
    }
}
