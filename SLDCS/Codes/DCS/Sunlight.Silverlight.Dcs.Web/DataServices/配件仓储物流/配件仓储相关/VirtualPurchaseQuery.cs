﻿using System.Collections.Generic;
using System.Linq;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class VirtualPurchaseQueryAch : DcsSerivceAchieveBase {
        public VirtualPurchaseQueryAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public IEnumerable<VirtualPurchaseQuery> 查询配件采购需求() {
            const string SQL = @"select 
                            d.branchid as BranchId ,--分公司id
                            d.partssalescategoryid as PartsSalesCategoryId,
                            b.id as PartId,
                            b.code as PartCode ,--配件图号
                            b.name as PartName ,--配件名称
                            b.CreateTime as CreateTime,
                            sum(a.quantity) as stock ,--库存
                            sum(nvl(e.CongelationStockQty,0)) as WmsLockQty,
                            sum(nvl(f.lockedquantity,0)) as LockedQty, --锁定库存
                            nvl(onway.OnwayQty, 0) as OnwayQty, --调拨在途数量
                            nvl(gg.WMZQty, 0) as WMZQty,--订单未满足数量
                            nvl(out1.OutQty,0) as OutQty, --出库数量
                            kvi.value as PartsAttribution --配件所属分类
                            from partsstock a
                            inner join sparepart b on a.partid = b.id
                            inner join salesunitaffiwarehouse c on c.warehouseid = a.warehouseid
                            inner join salesunit d on d.id = c.salesunitid
                            left join wmscongelationstockview e on e.SparePartId = a.partid and e.WarehouseId = a.warehouseid
                            left join partslockedstock f on f.partid = a.partid and f.warehouseid = a.warehouseid
                            left join partsbranch psb on psb.partssalescategoryid = d.partssalescategoryid and psb.partid = b.id and psb.status=1
                            left join keyvalueitem kvi on kvi.status = 1 and kvi.name = 'PartsAttribution' and kvi.key = psb.PartsAttribution 
                            left join (select a.branchid,
                            b.SparePartId,
                            sum(b.Quantity) as OnwayQty
                            from SupplierShippingOrder a
                            inner join Suppliershippingdetail b
                            on a.id = b.SupplierShippingOrderId
                            where a.status = 1
                            group by a.branchid, b.sparepartid) onway
                            on onway.branchid = d.branchid
                            and onway.sparepartid = a.partid
                            left join (select a.branchid,
                            b.sparepartid,
                            sum(b.OrderedQuantity - b.ApproveQuantity) as WMZQty
                            from partssalesorder a
                            inner join partssalesorderdetail b
                            on a.id = b.partssalesorderid
                            where a.status in (2, 4)
                            group by a.branchid, b.sparepartid) gg
                            on gg.branchid = d.branchid
                            and gg.sparepartid = a.partid
                            left join (select a.partssalescategoryid,
                            b.sparepartid,
                            sum(b.outboundamount) as OutQty
                            from partsoutboundbill a
                            inner join partsoutboundbilldetail b
                            on a.id = b.partsoutboundbillid
                            where a.OutboundType <> 2 /* and a.createtime */
                            -- between to_date('2012-01-01','yyyy-MM-dd') and to_date('2015-5-29','yyyy-MM-dd')
                            --between  @Prompt('time1','D',,Mono,Free,Persistent) and @Prompt('time2','D',,Mono,Free,Persistent)
                            group by a.partssalescategoryid,b.sparepartid) out1
                            on out1.partssalescategoryid = d.partssalescategoryid
                            and out1.sparepartid = a.partid
                            where a.storagecompanytype = 1
                            group by d.branchid,
                            d.partssalescategoryid,
                            b.id,
                            b.code,
                            b.name,
                            b.CreateTime,
                            onway.OnwayQty,
                            gg.WMZQty,
                            out1.OutQty,
                            kvi.value";
            var search = ObjectContext.ExecuteStoreQuery<VirtualPurchaseQuery>(SQL).ToList();
            return search.OrderBy(r => r.PartId);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:33
        //原分布类的函数全部转移到Ach                                                               

        public IEnumerable<VirtualPurchaseQuery> 查询配件采购需求() {
            return new VirtualPurchaseQueryAch(this).查询配件采购需求();
        }
    }
}
