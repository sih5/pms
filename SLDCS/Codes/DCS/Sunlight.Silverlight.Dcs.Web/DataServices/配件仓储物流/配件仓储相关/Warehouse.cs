﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class WarehouseAch : DcsSerivceAchieveBase {
        internal void InsertWarehouseValidate(Warehouse warehouse) {
            var dbwarehouse = ObjectContext.Warehouses.Where(r => r.Code.ToLower() == warehouse.Code.ToLower() && r.Status != (int)DcsBaseDataStatus.作废).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbwarehouse != null)
                throw new ValidationException(string.Format(ErrorStrings.Warehouse_Validation1, warehouse.Code));
            if(warehouse.Type == (int)DcsWarehouseType.虚拟库 && (string.IsNullOrWhiteSpace(warehouse.Code) || warehouse.Code == GlobalVar.ASSIGNED_BY_SERVER)) {
                var branch = ObjectContext.Branches.SingleOrDefault(r => r.Id == warehouse.BranchId);
                if(branch == null)
                    throw new ValidationException(ErrorStrings.Warehouse_Validation4);
                warehouse.Code = CodeGenerator.Generate("Warehouse", branch.Code);
                warehouse.Name = GlobalVar.VIRTUAL_WAREHOUSE_NAME_PREFIX + warehouse.Code;
            }
            var userInfo = Utils.GetCurrentUserInfo();
            warehouse.CreatorId = userInfo.Id;
            warehouse.CreatorName = userInfo.Name;
            warehouse.CreateTime = DateTime.Now;
        }

        internal void UpdateWarehouseValidate(Warehouse warehouse) {
            var dbwarehouse = ObjectContext.Warehouses.Where(r => r.Id != warehouse.Id && r.Code.ToLower() == warehouse.Code.ToLower() && r.Status != (int)DcsBaseDataStatus.作废 && r.StorageCompanyId == warehouse.StorageCompanyId).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbwarehouse != null)
                throw new ValidationException(string.Format(ErrorStrings.Warehouse_Validation1, warehouse.Code));
            var userInfo = Utils.GetCurrentUserInfo();
            warehouse.ModifierId = userInfo.Id;
            warehouse.ModifierName = userInfo.Name;
            warehouse.ModifyTime = DateTime.Now;
        }

        public void InsertWarehouse(Warehouse warehouse) {
            
            this.InsertWarehouseValidate(warehouse);
        }

        public void UpdateWarehouse(Warehouse warehouse) {
            UpdateToDatabase(warehouse);
            this.UpdateWarehouseValidate(warehouse);
        }

        public IQueryable<Warehouse> GetWarehousesWithBranch() {
            int CompanyId = Utils.GetCurrentUserInfo().EnterpriseId;
            return ObjectContext.Warehouses.Where(r => r.StorageCompanyId == CompanyId && r.Type != (int)DcsWarehouseType.虚拟库).Include("Branch").OrderBy(e => e.Id);
        }
        public IQueryable<Warehouse> GetWarehousesWithBranchForDropDown() {
            int CompanyId = Utils.GetCurrentUserInfo().EnterpriseId;
            return ObjectContext.Warehouses.Include("Branch").OrderBy(e => e.Id);
        }

        public IQueryable<Warehouse> GetWarehousesWithDetails() {
            return ObjectContext.Warehouses.Include("Branch").Include("WarehouseOperators.Personnel").OrderBy(e => e.Id);
        }

        public IQueryable<Warehouse> GetWarehousesByOperatorName(string name) {
            if(!string.IsNullOrEmpty(name)) {
                var personnels = this.ObjectContext.Personnels.Where(r => r.Name == name);
                var warehouseOperators = this.ObjectContext.WarehouseOperators.Where(r => personnels.Select(p => p.Id).Contains(r.OperatorId));
                return ObjectContext.Warehouses.Include("Branch").Include("WarehouseOperators.Personnel").Where(w => warehouseOperators.Select(r => r.WarehouseId).Contains(w.Id)).OrderBy(e => e.Id);
            } else
                return ObjectContext.Warehouses.Include("Branch").Include("WarehouseOperators.Personnel").OrderBy(e => e.Id);
        }

        public IQueryable<Warehouse> GetWarehousesWithStorageCompany() {
            var userinfo = Utils.GetCurrentUserInfo();
            var personSalesCenterLinkQuery = ObjectContext.PersonSalesCenterLinks.Where(r => r.PersonId == userinfo.Id && r.Status == (int)DcsBaseDataStatus.有效);
            var dbSalesUnitQuery = ObjectContext.SalesUnits.Where(t => personSalesCenterLinkQuery.Any(v => v.PartsSalesCategoryId == t.PartsSalesCategoryId) && t.Status == (int)DcsBaseDataStatus.有效);
            return ObjectContext.Warehouses.Where(g => this.ObjectContext.SalesUnitAffiWarehouses.Any(houese => dbSalesUnitQuery.Any(dbSalesUnit => houese.SalesUnitId == dbSalesUnit.Id) && houese.WarehouseId == g.Id) && g.Status == (int)DcsBaseDataStatus.有效).Include("Company").OrderBy(e => e.Id);
        }

        public IQueryable<Warehouse> GetWarehousesByManagerId(int personnelId) {
            return ObjectContext.Warehouses.Where(v => v.Status == (int)DcsBaseDataStatus.有效
                && ObjectContext.WarehouseAreas.Any(area => area.WarehouseId == v.Id && area.Status == (int)DcsBaseDataStatus.有效
                    && ObjectContext.WarehouseAreaManagers.Any(manager => manager.ManagerId == personnelId && manager.WarehouseAreaId == area.Id))).OrderBy(r => r.Id);
        }

        public IQueryable<Warehouse> GetWarehousesBySalesUnitId(int salesUnitId) {
            return ObjectContext.Warehouses.Where(v => v.Status == (int)DcsBaseDataStatus.有效
                && ObjectContext.SalesUnitAffiWarehouses.Any(houese => houese.SalesUnitId == salesUnitId && houese.WarehouseId == v.Id)).OrderBy(r => r.Id);
        }

        public IQueryable<Warehouse> GetWarehousesBySalesUnitId2(int salesUnitId, int ownerCompanyId) {
            var partsSalesCategoryQuery = ObjectContext.SalesUnits.Where(r => r.Id == salesUnitId && r.Status == (int)DcsBaseDataStatus.有效);
            var dbSalesUnitQuery = ObjectContext.SalesUnits.Where(t => partsSalesCategoryQuery.Any(v => v.PartsSalesCategoryId == t.PartsSalesCategoryId) && t.OwnerCompanyId == ownerCompanyId && t.Status == (int)DcsBaseDataStatus.有效);
            return this.ObjectContext.Warehouses.Where(v => v.Status == (int)DcsBaseDataStatus.有效 && this.ObjectContext.SalesUnitAffiWarehouses.Any(houese => dbSalesUnitQuery.Any(dbSalesUnit => houese.SalesUnitId == dbSalesUnit.Id) && houese.WarehouseId == v.Id)).OrderBy(r => r.Id);
        }

        public IQueryable<Warehouse> 根据销售组织查询仓库(int salesUnitId) {
            return ObjectContext.Warehouses.Where(v => ObjectContext.SalesUnitAffiWarehouses.Any(r => r.SalesUnitId == salesUnitId && r.WarehouseId == v.Id) && v.Status == (int)DcsBaseDataStatus.有效).OrderBy(r => r.Id);
        }

        [Query(HasSideEffects = true)]
        public IQueryable<Warehouse> GetWarehousesByWarehouseId(int[] warehouseIds) {
            if(warehouseIds.Length <= 0)
                return null;
            return ObjectContext.Warehouses.Where(v => warehouseIds.Contains(v.Id)).OrderBy(r => r.Id);
        }

        //public IQueryable<Warehouse> GetWarehousesOrdered(int partsSalesCategoryId, int dealerId, int enterpriseId) {
        //    var result = ObjectContext.Warehouses.Where(m => m.Status == (int)DcsBaseDataStatus.有效 && ObjectContext.SalesUnitAffiWarehouses.Where(k => ObjectContext.SalesUnits.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId && r.Status == (int)DcsMasterDataStatus.有效).Any(v => v.Id == k.SalesUnitId)).Any(n => m.Id == n.WarehouseId) && (m.Id == enterpriseId || ObjectContext.AgencyDealerRelations.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.PartsSalesOrderTypeId == partsSalesCategoryId && r.DealerId == dealerId).Any(a => a.DealerId == m.Id)));
        //    return result;
        //}

        public IQueryable<Warehouse> GetWarehousesByPartsSalesCategoryIdAndOwnerCompanyId(int partsSalesCategoryId, int companyId) {
            return ObjectContext.Warehouses.Where(g => ObjectContext.SalesUnitAffiWarehouses.Any(v => ObjectContext.SalesUnits.Any(r => r.PartsSalesCategoryId == partsSalesCategoryId && r.OwnerCompanyId == companyId && r.Status == (int)DcsMasterDataStatus.有效 && r.Id == v.SalesUnitId) && v.WarehouseId == g.Id) && g.Status == (int)DcsBaseDataStatus.有效).OrderBy(r => r.Id);
        }

        public IQueryable<Warehouse> GetWarehousesByPartsSalesCategoryIdAndWarehouseId(int partsSalesCategoryId, int warehouseId, int companyId) {
            return ObjectContext.Warehouses.Where(t => ObjectContext.SalesUnitAffiWarehouses.Any(r => ObjectContext.SalesUnits.Any(v => v.PartsSalesCategoryId == partsSalesCategoryId && v.OwnerCompanyId == companyId && v.Status == (int)DcsMasterDataStatus.有效 && v.Id == r.SalesUnitId && r.WarehouseId == t.Id) && t.Status == (int)DcsBaseDataStatus.有效 && ObjectContext.Warehouses.Any(g => g.Id == warehouseId && g.StorageCenter == t.StorageCenter))).OrderBy(r => r.Id);
        }

        public IQueryable<Warehouse> GetWarehousesByPartsSalesOrder(int submitCompanyId, int branchId, int salesCategoryId) {
            return ObjectContext.Warehouses.Where(warehouse => warehouse.StorageCompanyId == submitCompanyId && warehouse.BranchId == branchId && warehouse.StorageCompanyType == (int)DcsCompanyType.代理库 && ObjectContext.SalesUnitAffiWarehouses.Any(e => e.WarehouseId == warehouse.Id && ObjectContext.SalesUnits.Any(r => r.OwnerCompanyId == warehouse.StorageCompanyId && r.BranchId == warehouse.BranchId && r.PartsSalesCategoryId == salesCategoryId && r.Id == e.SalesUnitId)));
        }

        public IQueryable<Warehouse> GetWarehousesBySalesUnit(int partsSalesCategoryId) {
            int companyId = Utils.GetCurrentUserInfo().EnterpriseId;
            return ObjectContext.Warehouses.Where(v => ObjectContext.SalesUnitAffiWarehouses.Any(r => ObjectContext.SalesUnits.Any(x => x.PartsSalesCategoryId == partsSalesCategoryId && x.OwnerCompanyId == companyId && r.SalesUnitId == x.Id) && r.WarehouseId == v.Id));
        }

        //销售订单提报节点获取收货仓库
        public IQueryable<Warehouse> GetWarehousesForSalesOrderReport(int partsSalesCategoryId, int submitCompanyId) {
            return ObjectContext.Warehouses.Where(v => ObjectContext.SalesUnitAffiWarehouses.Any(r => ObjectContext.SalesUnits.Any(x => x.PartsSalesCategoryId == partsSalesCategoryId && x.OwnerCompanyId == submitCompanyId && r.SalesUnitId == x.Id) && r.WarehouseId == v.Id) && v.Status == (int)DcsBaseDataStatus.有效);
        }

        public Warehouse GetWarehousesByIdWithStorageCompanyId(int warehouseId) {
            return ObjectContext.Warehouses.FirstOrDefault(v => ObjectContext.Warehouses.Any(r => r.Id == warehouseId && r.StorageCompanyId == v.StorageCompanyId));
        }

        public IQueryable<Warehouse> GetWarehousesOrderByName() {
            return ObjectContext.Warehouses.OrderBy(r => r.Name);
        }
        public IQueryable<Warehouse> GetWarehousesOrderByNameWithBranchId() {
            var userInfo = Utils.GetCurrentUserInfo();
            return ObjectContext.Warehouses.Where(r => r.StorageCompanyId == userInfo.EnterpriseId).OrderBy(r => r.Name);
        }
        public IQueryable<Warehouse> 获取CDC退货仓库(int partsSalesCategoryId, int branchid) {
            //return ObjectContext.Warehouses.Where(t => ObjectContext.SalesUnitAffiWarehouses.Any(r => ObjectContext.SalesUnits.Any(v => v.OwnerCompanyId == branchid && v.PartsSalesCategoryId == partsSalesCategoryId && v.Status == (int)DcsMasterDataStatus.有效 && v.Id == r.SalesUnitId && r.WarehouseId == t.Id)) && t.Status == (int)DcsBaseDataStatus.有效 && !t.WmsInterface).OrderBy(r => r.Id);
            return ObjectContext.Warehouses.Where(t => ObjectContext.SalesUnitAffiWarehouses.Any(r => ObjectContext.SalesUnits.Any(v => v.OwnerCompanyId == branchid && v.PartsSalesCategoryId == partsSalesCategoryId && v.Status == (int)DcsMasterDataStatus.有效 && v.Id == r.SalesUnitId && r.WarehouseId == t.Id)) && t.Status == (int)DcsBaseDataStatus.有效 && !t.WmsInterface && t.IsQualityWarehouse == true).OrderBy(r => r.Id);

        }

        //配件索赔提报查询面板根据品牌过滤仓库
        public IQueryable<VirtualWarehouse> GetWarehousesForQuery() {
            var t = from a in ObjectContext.Warehouses.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && !r.WmsInterface)
                    join b in ObjectContext.SalesUnitAffiWarehouses on a.Id equals b.WarehouseId
                    join c in ObjectContext.SalesUnits.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on b.SalesUnitId equals c.Id
                    select new VirtualWarehouse {
                        Id = a.Id,
                        Code = a.Code,
                        Name = a.Name,
                        PartsSalesCategoryId = c.PartsSalesCategoryId

                    };
            return t.OrderBy(r => r.Name);
        }

        public IQueryable<Warehouse> GetWarehousesForAgencyOutWarehouse(int enterPriseId, int partsSalesCategoryId) {
            var t = from a in ObjectContext.Warehouses.Where(r => r.StorageCompanyId == enterPriseId && r.Status == (int)DcsBaseDataStatus.有效 && r.Type != (int)DcsWarehouseType.虚拟库)
                    join b in ObjectContext.SalesUnitAffiWarehouses on a.Id equals b.WarehouseId
                    join c in ObjectContext.SalesUnits.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.PartsSalesCategoryId == partsSalesCategoryId) on b.SalesUnitId equals c.Id
                    select a;
            return t.OrderBy(r => r.Name);
        }

        public IQueryable<Warehouse> 获取仓库数据源(int companyType, int companyId, int partsSalesCategoryId) {
            var branchIds = ObjectContext.Branches.Where(r => r.Status == (int)DcsMasterDataStatus.有效).Select(r => r.Id).ToArray();
            if(companyType == (int)DcsCompanyType.服务站) {
                var AgencyIds = ObjectContext.AgencyDealerRelations.Where(r => r.DealerId == companyId && r.Status == (int)DcsBaseDataStatus.有效).Select(r => r.AgencyId).ToArray();
                var warehouseQuery = from a in ObjectContext.Warehouses.Where(r => !r.WmsInterface && r.Status == (int)DcsBaseDataStatus.有效 && r.IsQualityWarehouse == true)
                                     join b in ObjectContext.SalesUnitAffiWarehouses on a.Id equals b.WarehouseId
                                     join c in ObjectContext.SalesUnits.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId && (branchIds.Contains(r.OwnerCompanyId) || AgencyIds.Contains(r.OwnerCompanyId))) on b.SalesUnitId equals c.Id
                                     select a;
                return warehouseQuery.OrderBy(r => r.Id);
            } else if(companyType == (int)DcsCompanyType.服务站兼代理库) {
                var SalesCompanyIds = ObjectContext.CustomerInformations.Where(r => r.CustomerCompanyId == companyId && r.Status == (int)DcsMasterDataStatus.有效).Select(r => r.SalesCompanyId).ToArray();
                var warehouseQuery = from a in ObjectContext.Warehouses.Where(r => !r.WmsInterface && r.Status == (int)DcsBaseDataStatus.有效 && r.IsQualityWarehouse == true)
                                     join b in ObjectContext.SalesUnitAffiWarehouses on a.Id equals b.WarehouseId
                                     join c in ObjectContext.SalesUnits.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId && (branchIds.Contains(r.OwnerCompanyId) || SalesCompanyIds.Contains(r.OwnerCompanyId))) on b.SalesUnitId equals c.Id
                                     select a;
                return warehouseQuery.OrderBy(r => r.Id);
            } else if(companyType == (int)DcsCompanyType.代理库) {
                var warehouseQuery = from a in ObjectContext.Warehouses.Where(r => !r.WmsInterface && r.Status == (int)DcsBaseDataStatus.有效 && r.StorageCompanyType == (int)DcsCompanyType.分公司 && r.IsQualityWarehouse == true)
                                     join b in ObjectContext.SalesUnitAffiWarehouses on a.Id equals b.WarehouseId
                                     join c in ObjectContext.SalesUnits.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId && branchIds.Contains(r.OwnerCompanyId)) on b.SalesUnitId equals c.Id
                                     select a;
                return warehouseQuery.OrderBy(r => r.Id);
            } else {
                return null;
            }
        }

        public IQueryable<Warehouse> 维修索赔获取退货仓库(int companyId)
        {
            return ObjectContext.Warehouses.Where(r => r.StorageCompanyId == companyId && r.IsQualityWarehouse == true && r.Status == (int)DcsBaseDataStatus.有效);
        }

        public IQueryable<VirtualWarehouse> GetWarehousesWithPartsSalesCategory() {
            var t = from a in ObjectContext.Warehouses.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.StorageCompanyType == (int)DcsCompanyType.分公司)
                    join b in ObjectContext.SalesUnitAffiWarehouses on a.Id equals b.WarehouseId
                    join c in ObjectContext.SalesUnits.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on b.SalesUnitId equals c.Id
                    select new VirtualWarehouse {
                        Id = a.Id,
                        Code = a.Code,
                        Name = a.Name,
                        PartsSalesCategoryId = c.PartsSalesCategoryId

                    };
            return t.OrderBy(r => r.Name);
        }
        public IQueryable<Warehouse> GetWarehousesWithBrandId(int brandId, int partsSalesCategoryId) {
            //var salesUnitIds = ObjectContext.SalesUnits.Where(r => r.BranchId == brandId && r.PartsSalesCategoryId == partsSalesCategoryId).Select(r =>r.Id);
            var salesUnitIds = ObjectContext.SalesUnits.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId).Select(r => r.Id);
            var salesUnitAffiWarehousesIds = ObjectContext.SalesUnitAffiWarehouses.Where(r => salesUnitIds.Contains(r.SalesUnitId)).Select(r => r.WarehouseId);
            return ObjectContext.Warehouses.Where(r => salesUnitAffiWarehousesIds.Contains(r.Id)).OrderBy(v => v.Id);

        }
        public IQueryable<Warehouse> GetWarehousesWithBranchIds() {
            int CompanyId = Utils.GetCurrentUserInfo().EnterpriseId;
            return ObjectContext.Warehouses.Where(r => r.StorageCompanyId == CompanyId).OrderBy(e => e.Id);
        }

        public IQueryable<Warehouse> GetWarehousesWithStorageCompanyIds(int storageCompanyId) {
            return ObjectContext.Warehouses.Where(r => r.StorageCompanyId == storageCompanyId && r.Type != (int)DcsWarehouseType.虚拟库 && r.IsQualityWarehouse == false && r.Status == (int)DcsBaseDataStatus.有效).OrderBy(e => e.Id);
        }
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:33
        //原分布类的函数全部转移到Ach                                                               

        public void InsertWarehouse(Warehouse warehouse) {
            new WarehouseAch(this).InsertWarehouse(warehouse);
        }

        public void UpdateWarehouse(Warehouse warehouse) {
            new WarehouseAch(this).UpdateWarehouse(warehouse);
        }

        public IQueryable<Warehouse> GetWarehousesWithBranch() {
            return new WarehouseAch(this).GetWarehousesWithBranch();
        }
        public IQueryable<Warehouse> GetWarehousesWithBranchForDropDown() {
            return new WarehouseAch(this).GetWarehousesWithBranchForDropDown();
        }

        public IQueryable<Warehouse> GetWarehousesWithDetails() {
            return new WarehouseAch(this).GetWarehousesWithDetails();
        }

        public IQueryable<Warehouse> GetWarehousesWithStorageCompany() {
            return new WarehouseAch(this).GetWarehousesWithStorageCompany();
        }

        public IQueryable<Warehouse> GetWarehousesByManagerId(int personnelId) {
            return new WarehouseAch(this).GetWarehousesByManagerId(personnelId);
        }

        public IQueryable<Warehouse> GetWarehousesBySalesUnitId(int salesUnitId) {
            return new WarehouseAch(this).GetWarehousesBySalesUnitId(salesUnitId);
        }

        public IQueryable<Warehouse> GetWarehousesBySalesUnitId2(int salesUnitId, int ownerCompanyId) {
            return new WarehouseAch(this).GetWarehousesBySalesUnitId2(salesUnitId, ownerCompanyId);
        }

        public IQueryable<Warehouse> 根据销售组织查询仓库(int salesUnitId) {
            return new WarehouseAch(this).根据销售组织查询仓库(salesUnitId);
        }


        [Query(HasSideEffects = true)]
        public IQueryable<Warehouse> GetWarehousesByWarehouseId(int[] warehouseIds) {
            return new WarehouseAch(this).GetWarehousesByWarehouseId(warehouseIds);
        }

        public IQueryable<Warehouse> GetWarehousesByPartsSalesCategoryIdAndOwnerCompanyId(int partsSalesCategoryId, int companyId) {
            return new WarehouseAch(this).GetWarehousesByPartsSalesCategoryIdAndOwnerCompanyId(partsSalesCategoryId, companyId);
        }

        public IQueryable<Warehouse> GetWarehousesByPartsSalesCategoryIdAndWarehouseId(int partsSalesCategoryId, int warehouseId, int companyId) {
            return new WarehouseAch(this).GetWarehousesByPartsSalesCategoryIdAndWarehouseId(partsSalesCategoryId, warehouseId, companyId);
        }

        public IQueryable<Warehouse> GetWarehousesByPartsSalesOrder(int submitCompanyId, int branchId, int salesCategoryId) {
            return new WarehouseAch(this).GetWarehousesByPartsSalesOrder(submitCompanyId, branchId, salesCategoryId);
        }

        public IQueryable<Warehouse> GetWarehousesBySalesUnit(int partsSalesCategoryId) {
            return new WarehouseAch(this).GetWarehousesBySalesUnit(partsSalesCategoryId);
        }
        public IQueryable<Warehouse> GetWarehousesForSalesOrderReport(int partsSalesCategoryId, int submitCompanyId) {
            return new WarehouseAch(this).GetWarehousesForSalesOrderReport(partsSalesCategoryId, submitCompanyId);
        }

        public Warehouse GetWarehousesByIdWithStorageCompanyId(int warehouseId) {
            return new WarehouseAch(this).GetWarehousesByIdWithStorageCompanyId(warehouseId);
        }

        public IQueryable<Warehouse> GetWarehousesOrderByName() {
            return new WarehouseAch(this).GetWarehousesOrderByName();
        }

        public IQueryable<Warehouse> GetWarehousesByOperatorName(string name) {
            return new WarehouseAch(this).GetWarehousesByOperatorName(name);
        }

        public IQueryable<Warehouse> GetWarehousesOrderByNameWithBranchId() {
            return new WarehouseAch(this).GetWarehousesOrderByNameWithBranchId();
        }

        public IQueryable<Warehouse> 获取CDC退货仓库(int partsSalesCategoryId, int branchid) {
            return new WarehouseAch(this).获取CDC退货仓库(partsSalesCategoryId, branchid);
        }
        public IQueryable<VirtualWarehouse> GetWarehousesForQuery() {
            return new WarehouseAch(this).GetWarehousesForQuery();
        }

        public IQueryable<Warehouse> GetWarehousesForAgencyOutWarehouse(int enterPriseId, int partsSalesCategoryId) {
            return new WarehouseAch(this).GetWarehousesForAgencyOutWarehouse(enterPriseId, partsSalesCategoryId);
        }

        public IQueryable<Warehouse> 获取仓库数据源(int companyType, int companyId, int partsSalesCategoryId) {
            return new WarehouseAch(this).获取仓库数据源(companyType, companyId, partsSalesCategoryId);
        }

        public IQueryable<VirtualWarehouse> GetWarehousesWithPartsSalesCategory() {
            return new WarehouseAch(this).GetWarehousesWithPartsSalesCategory();
        }
        public IQueryable<Warehouse> GetWarehousesWithBrandId(int brandId, int partsSalesCategoryId) {
            return new WarehouseAch(this).GetWarehousesWithBrandId(brandId, partsSalesCategoryId);
        }
        public IQueryable<Warehouse> GetWarehousesWithBranchIds() {
            return new WarehouseAch(this).GetWarehousesWithBranchIds();
        }
        public IQueryable<Warehouse> 维修索赔获取退货仓库(int companyId) {
            return new WarehouseAch(this).维修索赔获取退货仓库(companyId);
        }
        public IQueryable<Warehouse> GetWarehousesWithStorageCompanyIds(int storageCompanyId) {
            return new WarehouseAch(this).GetWarehousesWithStorageCompanyIds(storageCompanyId);
        }
    }
}
