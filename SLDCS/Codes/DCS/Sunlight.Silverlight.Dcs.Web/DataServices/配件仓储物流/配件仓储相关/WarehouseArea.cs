﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class WarehouseAreaAch : DcsSerivceAchieveBase {
        internal void InsertWarehouseAreaValidate(WarehouseArea warehouseArea) {
            var dbwarehouseArea = ObjectContext.WarehouseAreas.Where(r => r.WarehouseId == warehouseArea.WarehouseId && r.Code.ToLower() == warehouseArea.Code.ToLower()).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbwarehouseArea != null && dbwarehouseArea.Status == (int)DcsBaseDataStatus.有效)
                throw new ValidationException(string.Format(ErrorStrings.WarehouseArea_Validation1, warehouseArea.Code));
            var userInfo = Utils.GetCurrentUserInfo();
            warehouseArea.CreatorId = userInfo.Id;
            warehouseArea.CreatorName = userInfo.Name;
            warehouseArea.CreateTime = DateTime.Now;
        }
        internal void UpdateWarehouseAreaValidate(WarehouseArea warehouseArea) {
            var dbwarehouseArea = ObjectContext.WarehouseAreas.Where(r => r.Id != warehouseArea.Id && r.WarehouseId == warehouseArea.WarehouseId && r.Code.ToLower() == warehouseArea.Code.ToLower()).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbwarehouseArea != null)
                throw new ValidationException(string.Format(ErrorStrings.WarehouseArea_Validation1, warehouseArea.Code));
            var userInfo = Utils.GetCurrentUserInfo();
            warehouseArea.ModifierId = userInfo.Id;
            warehouseArea.ModifierName = userInfo.Name;
            warehouseArea.ModifyTime = DateTime.Now;
        }
        public void InsertWarehouseArea(WarehouseArea warehouseArea) {
            InsertToDatabase(warehouseArea);
            this.InsertWarehouseAreaValidate(warehouseArea);
        }
        public void UpdateWarehouseArea(WarehouseArea warehouseArea) {
            UpdateToDatabase(warehouseArea);
            this.UpdateWarehouseAreaValidate(warehouseArea);
        }
        public IQueryable<WarehouseArea> GetWarehouseAreasWithDetails() {
            int CompanyId = Utils.GetCurrentUserInfo().EnterpriseId;
            return ObjectContext.WarehouseAreas.Where(e => e.Status == (int)DcsBaseDataStatus.有效 && e.Warehouse.StorageCompanyId == CompanyId).Include("Warehouse").Include("WarehouseAreaCategory").Include("ParentWarehouseArea").OrderBy(entity => entity.Id);
        }
        public IQueryable<WarehouseArea> GetWarehouseAreasWithRoot() {
            int CompanyId = Utils.GetCurrentUserInfo().EnterpriseId;
            return ObjectContext.WarehouseAreas.Where(e => e.ParentId == null && e.Status == (int)DcsBaseDataStatus.有效 && e.Warehouse.StorageCompanyId == CompanyId && e.AreaKind != (int)DcsAreaKind.库位).Include("Warehouse").Include("WarehouseAreaCategory").Include("ParentWarehouseArea").OrderBy(entity => entity.Id);
        }

        [Query(HasSideEffects = true)]
        public IEnumerable<WarehouseArea> GetWarehouseAreasWithCategoryByIds(int[] ids) {
            var dbWarehouseAreas = ObjectContext.WarehouseAreas.Where(e => ids.Contains(e.Id) && e.Status == (int)DcsBaseDataStatus.有效).ToArray();
            if(dbWarehouseAreas.Any()) {
                var categoryIds = dbWarehouseAreas.Select(e => e.AreaCategoryId).ToArray();
                var warehouseIds = dbWarehouseAreas.Select(e => e.WarehouseId).ToArray();
                var warehouseCategories = ObjectContext.WarehouseAreaCategories.Where(e => categoryIds.Contains(e.Id)).ToArray();
                var warehouse = ObjectContext.Warehouses.Where(e => warehouseIds.Contains(e.Id)).ToArray();
            }
            return dbWarehouseAreas.OrderBy(r => r.Id);
        }
        public IEnumerable<WarehouseArea> GetWarehouseAreasWithRootWithoutPositionByOperatorId(int personnelId) {
            var warehouseOperators = ObjectContext.WarehouseOperators.Where(e => e.OperatorId == personnelId).ToArray();
            if(!warehouseOperators.Any())
                return null;
            var warehouseIds = warehouseOperators.Select(e => e.WarehouseId);
            return ObjectContext.WarehouseAreas.Where(e => warehouseIds.Contains(e.WarehouseId) && e.ParentId == null && e.Status == (int)DcsBaseDataStatus.有效).OrderBy(r => r.Code);
        }
        public IQueryable<WarehouseArea> GetWarehouseAreasWithRootWithoutPositionByWarehouseId(int warehouseId) {
            return ObjectContext.WarehouseAreas.Where(e => e.WarehouseId == warehouseId && e.ParentId == null && e.Status == (int)DcsBaseDataStatus.有效);
        }
        public IEnumerable<WarehouseArea> GetWarehouseAreasWithDetailsWithoutPositionByOperatorId(int personnelId) {
            var warehouseOperators = ObjectContext.WarehouseOperators.Where(e => e.OperatorId == personnelId).ToArray();
            if(!warehouseOperators.Any())
                return null;
            var warehouseIds = warehouseOperators.Select(e => e.WarehouseId);
            var warehouseAreas = ObjectContext.WarehouseAreas.Where(e => warehouseIds.Contains(e.WarehouseId) && (e.AreaKind == (int)DcsAreaKind.仓库 || (e.AreaKind == (int)DcsAreaKind.库区 &&
                ObjectContext.WarehouseAreaCategories.Any(r => r.Category == (int)DcsAreaType.保管区 && r.Id == e.AreaCategoryId))) && e.Status == (int)DcsBaseDataStatus.有效).ToArray();
            var warehouses = ObjectContext.Warehouses.Where(e => warehouseIds.Contains(e.Id) && e.Status == (int)DcsBaseDataStatus.有效).ToArray();
            if(warehouseAreas.Any()) {
                var warehouseAreaIds = warehouseAreas.Select(e => e.Id);
                var categories = ObjectContext.WarehouseAreaCategories.Where(e => warehouseAreaIds.Contains(e.Id)).ToArray();
            }
            return warehouseAreas.OrderBy(r => r.Id);
        }
        public WarehouseArea GetWarehouseAreasWithAreaManagersAndDetails(int id) {
            var result = ObjectContext.WarehouseAreas.SingleOrDefault(e => e.Id == id && e.Status == (int)DcsBaseDataStatus.有效);
            if(result != null) {
                var warehouse = ObjectContext.Warehouses.Where(r => r.Id == result.WarehouseId && r.Status == (int)DcsBaseDataStatus.有效).ToArray();
                var warehouseAreaManagers = ObjectContext.WarehouseAreaManagers.Where(e => e.WarehouseAreaId == id && ObjectContext.Personnels.Any(x => x.Id == e.ManagerId && x.Status == (int)DcsMasterDataStatus.有效)).ToArray();
                if(warehouseAreaManagers.Any()) {
                    var warehouseAreaManagerIds = warehouseAreaManagers.Select(r => r.ManagerId);
                    var personnel = ObjectContext.Personnels.Where(r => warehouseAreaManagerIds.Contains(r.Id) && r.Status == (int)DcsMasterDataStatus.有效).ToArray();
                }
                var warehouseAreaCategory = ObjectContext.WarehouseAreaCategories.SingleOrDefault(e => e.Id == result.AreaCategoryId);
            }
            return result;
        }
        public IQueryable<WarehouseArea> GetWarehouseAreasWithAreaManagers(int? personnelId) {
            IQueryable<WarehouseArea> warehouseAreas = ObjectContext.WarehouseAreas.Where(r => r.Status == (int)DcsBaseDataStatus.有效);
            if(personnelId.HasValue)
                warehouseAreas = warehouseAreas.Where(r => ObjectContext.WarehouseAreaManagers.Any(v => v.ManagerId == personnelId && v.WarehouseAreaId == r.TopLevelWarehouseAreaId.Value));
            return warehouseAreas.Include("WarehouseAreaCategory").Include("Warehouse").OrderBy(e => e.Id);
        }
        public WarehouseArea GetWarehouseAreaWithWarehouseAreaCategory(int id) {
            var dbWarehouseArea = ObjectContext.WarehouseAreas.SingleOrDefault(e => e.Id == id && e.Status == (int)DcsBaseDataStatus.有效);
            if(dbWarehouseArea != null) {
                var dbWarehouseAreaCategory = ObjectContext.WarehouseAreaCategories.SingleOrDefault(e => e.Id == dbWarehouseArea.AreaCategoryId);
            }
            return dbWarehouseArea;
        }
        public IEnumerable<WarehouseArea> GetWarehouseAreasByParentId(int parentId) {
            var warehouseAreas = ObjectContext.WarehouseAreas.Where(r => r.ParentId == parentId && r.Status == (int)DcsBaseDataStatus.有效 && r.AreaKind != (int)DcsAreaKind.库位).ToArray();
            var warehouseAreaCategoryIds = warehouseAreas.Select(r => r.AreaCategoryId).ToArray();
            var warehouseAreaCategory = ObjectContext.WarehouseAreaCategories.Where(e => warehouseAreaCategoryIds.Contains(e.Id)).ToArray();
            return warehouseAreas.OrderBy(r => r.Code);
        }
        public IEnumerable<VirtualWarehouseArea> GetWarehouseAreasByParentIdIsPosition(int parentId) {
            var t = from a in ObjectContext.WarehouseAreas.Where(r => r.ParentId == parentId && r.Status == (int)DcsBaseDataStatus.有效 && r.AreaKind == (int)DcsAreaKind.库位)
                    from b in ObjectContext.WarehouseAreaCategories
                    where a.AreaCategoryId == b.Id
                    select new VirtualWarehouseArea {
                        Id = a.Id,
                        Code = a.Code,
                        WarehouseName = a.Warehouse == null ? "" : a.Warehouse.Name,
                        AreaKind = a.AreaKind,
                        AreaCategory = b == null ? 0 : b.Category,
                        Remark = a.Remark
                    };
            return t.OrderBy(r => r.Code);
        }
        public IEnumerable<WarehouseArea> 根据上层节点获取下层节点库区库位信息(int parentId) {
            //IQueryable<WarehouseArea> query = ObjectContext.WarehouseAreas.Where(r => r.ParentId == parentId && r.Status == (int)DcsBaseDataStatus.有效);
            //var groups =DomainService. GetChildrenByparentId(parentId, query);
            var warehouseAreas = ObjectContext.WarehouseAreas.Where(r => r.ParentId == parentId && r.Status == (int)DcsBaseDataStatus.有效).ToArray();
            //foreach(var warehouseArea in warehouseAreas) {
            //    var tempGroup = groups.FirstOrDefault(r => r.Id == warehouseArea.Id);
            //    if(tempGroup != null && tempGroup.ChildrenNum > 0) {
            //        warehouseArea.HasChildrenNode = true;
            //    }else {
            //        warehouseArea.HasChildrenNode = false;
            //    }
            //}
            return warehouseAreas.OrderBy(r => r.Id);
        }

        public List<ParentIdWithChildrenNum> GetChildrenByparentId<T>(int parentId, IQueryable<T> query) {
            var tmp = new List<ParentIdWithChildrenNum>();
            if(query is IQueryable<WarehouseArea>) {
                var tempQuery = query as IQueryable<WarehouseArea>;
                tmp = (from a in tempQuery.Where(r => r.ParentId == parentId)
                       join b in tempQuery on a.Id equals b.ParentId into tempTable1
                       from t in tempTable1.DefaultIfEmpty()
                       select new {
                           a.Id,
                           Children = t == null ? 0 : 1
                       }).GroupBy(r => new {
                           r.Id
                       }).Select(r => new ParentIdWithChildrenNum {
                           Id = r.Key.Id,
                           ChildrenNum = r.Sum(v => v.Children)
                       }).ToList();
            }
            if(query is IQueryable<UsedPartsWarehouseArea>) {
                var tempQuery = query as IQueryable<UsedPartsWarehouseArea>;
                tmp = (from a in tempQuery.Where(r => r.ParentId == parentId)
                       join b in tempQuery on a.Id equals b.ParentId into tempTable1
                       from t in tempTable1.DefaultIfEmpty()
                       select new {
                           a.Id,
                           Children = t == null ? 0 : 1
                       }).GroupBy(r => new {
                           r.Id
                       }).Select(r => new ParentIdWithChildrenNum {
                           Id = r.Key.Id,
                           ChildrenNum = r.Sum(v => v.Children)
                       }).ToList();
            }
            if(query is IQueryable<RepairItemCategory>) {
                var tempQuery = query as IQueryable<RepairItemCategory>;
                //tmp = (from a in tempQuery.Where(r => r.ParentId == parentId)
                //       join b in tempQuery on a.Id equals b.ParentId into tempTable1
                //       from t in tempTable1.DefaultIfEmpty()
                //       select new {
                //           a.Id,
                //           Children = t == null ? 0 : 1
                //       }).GroupBy(r => new {
                //           r.Id
                //       }).Select(r => new ParentIdWithChildrenNum {
                //           Id = r.Key.Id,
                //           ChildrenNum = r.Sum(v => v.Children)
                //       }).ToList();
            }
            //if(query is IQueryable<MalfunctionCategory>) {
            //    var tempQuery = query as IQueryable<MalfunctionCategory>;
            //    tmp = (from a in tempQuery.Where(r => r.ParentId == parentId)
            //           join b in tempQuery on a.Id equals b.ParentId into tempTable1
            //           from t in tempTable1.DefaultIfEmpty()
            //           select new {
            //               a.Id,
            //               Children = t == null ? 0 : 1
            //           }).GroupBy(r => new {
            //               r.Id
            //           }).Select(r => new ParentIdWithChildrenNum {
            //               Id = r.Key.Id,
            //               ChildrenNum = r.Sum(v => v.Children)
            //           }).ToList();
            //}
            return tmp;
        }
        public IEnumerable<WarehouseAreaManager> 查询库区负责人库区库位(int? managerId) {
            //1、如果参数 人员Id 不为空，根据人员Id 过滤库区库位
            //库区负责人.库区负责人Id=人员Id
            //库区负责人.库区库位Id=库区库位.顶层库区Id
            //2、同时返回库区库位、库区用途
            IQueryable<WarehouseAreaManager> warehouseAreaManagerQuery = ObjectContext.WarehouseAreaManagers;
            if(managerId != default(int)) {
                warehouseAreaManagerQuery = warehouseAreaManagerQuery.Where(r => r.ManagerId == managerId);
            }
            var warehouseAreaManagers = warehouseAreaManagerQuery.ToArray();
            var warehouseAreas = ObjectContext.WarehouseAreas.Where(r => warehouseAreaManagers.Any(v => v.WarehouseAreaId == r.TopLevelWarehouseAreaId)).ToArray();
            var warehouseAreaIds = warehouseAreas.Select(r => r.AreaCategoryId).ToArray();
            var warehouseAreaCategories = ObjectContext.WarehouseAreaCategories.Where(r => warehouseAreaIds.Contains(r.Id)).ToArray();
            return warehouseAreaManagers.OrderBy(r => r.Id);
        }
        public IEnumerable<WarehouseArea> 查询库区库位根据父节点Id(int parentId) {
            var userInfo = Utils.GetCurrentUserInfo();
            var warehouseAreas = ObjectContext.WarehouseAreas.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.ParentId == parentId && ObjectContext.WarehouseAreaManagers.Any(v => v.ManagerId == userInfo.Id)).ToArray();
            var warehouseAreaIds = warehouseAreas.Select(r => r.Id).ToArray();
            var warehouseAreaCategories = ObjectContext.WarehouseAreaCategories.Where(r => warehouseAreaIds.Contains(r.Id)).ToArray();
            return warehouseAreas.OrderBy(r => r.Code);
        }

        public IQueryable<VirtualWarehouseAreaToQuery> 查询库区负责人库区库位带配件信息(int? personId) {
            var warehouseAreas = ObjectContext.WarehouseAreas.AsQueryable();
            if(personId.HasValue) {
                warehouseAreas = warehouseAreas.Where(r => ObjectContext.WarehouseAreaManagers.Any(v => v.ManagerId == personId));
            }
            var result = from a in warehouseAreas
                         join b in ObjectContext.PartsStocks on a.Id equals b.WarehouseAreaId into t1
                         from c in t1.DefaultIfEmpty()
                         select new VirtualWarehouseAreaToQuery {
                             Id = a.Id,
                             WarehouseId = a.WarehouseId,
                             PartStockId = c.Id,
                             Code = a.Code,
                             AreaKind = a.AreaKind,
                             CreatorName = a.CreatorName,
                             CreateTime = a.CreateTime,
                             ModifierName = a.ModifierName,
                             ModifyTime = a.ModifyTime,
                             Remark = a.Remark,
                             WarehouseName = a.Warehouse.Name,
                             ParentWarehouseAreaCode = a.ParentWarehouseArea.Code,
                             ParentWarehouseAreaAreaKind = a.ParentWarehouseArea.AreaKind,
                             AreaCategory = a.WarehouseAreaCategory.Category,
                             PartCode = c.SparePart.Code,
                             PartName = c.SparePart.Name,
                             Status = a.Status
                         };
            return result.OrderBy(r => r.Id).ThenBy(r => r.PartStockId);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:33
        //原分布类的函数全部转移到Ach                                                               

        public void InsertWarehouseArea(WarehouseArea warehouseArea) {
            new WarehouseAreaAch(this).InsertWarehouseArea(warehouseArea);
        }

        public void UpdateWarehouseArea(WarehouseArea warehouseArea) {
            new WarehouseAreaAch(this).UpdateWarehouseArea(warehouseArea);
        }

        public IQueryable<WarehouseArea> GetWarehouseAreasWithDetails() {
            return new WarehouseAreaAch(this).GetWarehouseAreasWithDetails();
        }

        public IQueryable<WarehouseArea> GetWarehouseAreasWithRoot() {
            return new WarehouseAreaAch(this).GetWarehouseAreasWithRoot();
        }

        [Query(HasSideEffects = true)]
        public IEnumerable<WarehouseArea> GetWarehouseAreasWithCategoryByIds(int[] ids) {
            return new WarehouseAreaAch(this).GetWarehouseAreasWithCategoryByIds(ids);
        }

        public IEnumerable<WarehouseArea> GetWarehouseAreasWithRootWithoutPositionByOperatorId(int personnelId) {
            return new WarehouseAreaAch(this).GetWarehouseAreasWithRootWithoutPositionByOperatorId(personnelId);
        }

        public IQueryable<WarehouseArea> GetWarehouseAreasWithRootWithoutPositionByWarehouseId(int warehouseId) {
            return new WarehouseAreaAch(this).GetWarehouseAreasWithRootWithoutPositionByWarehouseId(warehouseId);
        }

        public IEnumerable<WarehouseArea> GetWarehouseAreasWithDetailsWithoutPositionByOperatorId(int personnelId) {
            return new WarehouseAreaAch(this).GetWarehouseAreasWithDetailsWithoutPositionByOperatorId(personnelId);
        }

        public WarehouseArea GetWarehouseAreasWithAreaManagersAndDetails(int id) {
            return new WarehouseAreaAch(this).GetWarehouseAreasWithAreaManagersAndDetails(id);
        }

        public IQueryable<WarehouseArea> GetWarehouseAreasWithAreaManagers(int? personnelId) {
            return new WarehouseAreaAch(this).GetWarehouseAreasWithAreaManagers(personnelId);
        }

        public WarehouseArea GetWarehouseAreaWithWarehouseAreaCategory(int id) {
            return new WarehouseAreaAch(this).GetWarehouseAreaWithWarehouseAreaCategory(id);
        }

        public IEnumerable<WarehouseArea> GetWarehouseAreasByParentId(int parentId) {
            return new WarehouseAreaAch(this).GetWarehouseAreasByParentId(parentId);
        }

        public IEnumerable<VirtualWarehouseArea> GetWarehouseAreasByParentIdIsPosition(int parentId) {
            return new WarehouseAreaAch(this).GetWarehouseAreasByParentIdIsPosition(parentId);
        }

        public IEnumerable<WarehouseArea> 根据上层节点获取下层节点库区库位信息(int parentId) {
            return new WarehouseAreaAch(this).根据上层节点获取下层节点库区库位信息(parentId);
        }
        
        public List<ParentIdWithChildrenNum> GetChildrenByparentId<T>(int parentId, IQueryable<T> query) {
            return new WarehouseAreaAch(this).GetChildrenByparentId<T>(parentId,query);
        }

        public IEnumerable<WarehouseAreaManager> 查询库区负责人库区库位(int? managerId) {
            return new WarehouseAreaAch(this).查询库区负责人库区库位(managerId);
        }

        public IEnumerable<WarehouseArea> 查询库区库位根据父节点Id(int parentId) {
            return new WarehouseAreaAch(this).查询库区库位根据父节点Id(parentId);
        }
        
        public IQueryable<VirtualWarehouseAreaToQuery> 查询库区负责人库区库位带配件信息(int? personId) {
            return new WarehouseAreaAch(this).查询库区负责人库区库位带配件信息(personId);
        }
    }
}