﻿
namespace Sunlight.Silverlight.Dcs.Web {
    partial class WarehouseAreaCategoryAch : DcsSerivceAchieveBase {
        public WarehouseAreaCategoryAch(DcsDomainService domainService)
            : base(domainService) {
        }

        internal void InsertWarehouseAreaCategoryValidate(WarehouseAreaCategory warehouseAreaCategory) {
        }
        internal void UpdateWarehouseAreaCategoryValidate(WarehouseAreaCategory warehouseAreaCategory) {
        }
        public void InsertWarehouseAreaCategory(WarehouseAreaCategory warehouseAreaCategory) {
            InsertToDatabase(warehouseAreaCategory);
            this.InsertWarehouseAreaCategoryValidate(warehouseAreaCategory);
        }
        public void UpdateWarehouseAreaCategory(WarehouseAreaCategory warehouseAreaCategory) {
            UpdateToDatabase(warehouseAreaCategory);
            this.UpdateWarehouseAreaCategoryValidate(warehouseAreaCategory);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:33
        //原分布类的函数全部转移到Ach                                                               

        public void InsertWarehouseAreaCategory(WarehouseAreaCategory warehouseAreaCategory) {
            new WarehouseAreaCategoryAch(this).InsertWarehouseAreaCategory(warehouseAreaCategory);
        }

        public void UpdateWarehouseAreaCategory(WarehouseAreaCategory warehouseAreaCategory) {
            new WarehouseAreaCategoryAch(this).UpdateWarehouseAreaCategory(warehouseAreaCategory);
        }
    }
}