﻿using System;
using System.Linq;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class WarehouseAreaHistoryAch : DcsSerivceAchieveBase {
        public WarehouseAreaHistoryAch(DcsDomainService domainService)
            : base(domainService) {
        }

        internal void InsertWarehouseAreaHistoryValidate(WarehouseAreaHistory warehouseAreaHistory) {
            var userInfo = Utils.GetCurrentUserInfo();
            warehouseAreaHistory.CreatorId = userInfo.Id;
            warehouseAreaHistory.CreatorName = userInfo.Name;
            warehouseAreaHistory.CreateTime = DateTime.Now;
        }
        internal void UpdateWarehouseAreaHistoryValidate(WarehouseAreaHistory warehouseAreaHistory) {
            var userInfo = Utils.GetCurrentUserInfo();
            warehouseAreaHistory.ModifierId = userInfo.Id;
            warehouseAreaHistory.ModifierName = userInfo.Name;
            warehouseAreaHistory.ModifyTime = DateTime.Now;
        }
        public void InsertWarehouseAreaHistory(WarehouseAreaHistory warehouseAreaHistory) {
            InsertToDatabase(warehouseAreaHistory);
            this.InsertWarehouseAreaHistoryValidate(warehouseAreaHistory);
        }
        public void UpdateWarehouseAreaHistory(WarehouseAreaHistory warehouseAreaHistory) {
            UpdateToDatabase(warehouseAreaHistory);
            this.UpdateWarehouseAreaHistoryValidate(warehouseAreaHistory);
        }
        public IQueryable<WarehouseAreaHistory> GetWarehouseAreaHistoryWithWarehouseDetailWarehouseAreaCategory() {
            return ObjectContext.WarehouseAreaHistories.Include("SparePart").Include("Warehouse").Include("WarehouseArea").Include("WarehouseArea1").Include("WarehouseAreaCategory").OrderBy(c => c.Id);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:33
        //原分布类的函数全部转移到Ach                                                               

        public void InsertWarehouseAreaHistory(WarehouseAreaHistory warehouseAreaHistory) {
            new WarehouseAreaHistoryAch(this).InsertWarehouseAreaHistory(warehouseAreaHistory);
        }

        public void UpdateWarehouseAreaHistory(WarehouseAreaHistory warehouseAreaHistory) {
            new WarehouseAreaHistoryAch(this).UpdateWarehouseAreaHistory(warehouseAreaHistory);
        }

        public IQueryable<WarehouseAreaHistory> GetWarehouseAreaHistoryWithWarehouseDetailWarehouseAreaCategory() {
            return new WarehouseAreaHistoryAch(this).GetWarehouseAreaHistoryWithWarehouseDetailWarehouseAreaCategory();
        }
    }
}
