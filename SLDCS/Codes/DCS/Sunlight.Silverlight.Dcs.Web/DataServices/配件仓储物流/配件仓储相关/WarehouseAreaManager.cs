﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class WarehouseAreaManagerAch : DcsSerivceAchieveBase {
        internal void InsertWarehouseAreaManagerValidate(WarehouseAreaManager warehouseAreaManager) {
            var dbwarehouseAreaManager = ObjectContext.WarehouseAreaManagers.Where(r => r.WarehouseAreaId == warehouseAreaManager.WarehouseAreaId && r.ManagerId == warehouseAreaManager.ManagerId).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbwarehouseAreaManager != null)
                throw new ValidationException(ErrorStrings.WarehouseAreaManager_Validation1);
            var userInfo = Utils.GetCurrentUserInfo();
            warehouseAreaManager.CreatorId = userInfo.Id;
            warehouseAreaManager.CreatorName = userInfo.Name;
            warehouseAreaManager.CreateTime = DateTime.Now;
        }
        public void InsertWarehouseAreaManager(WarehouseAreaManager warehouseAreaManager) {
            InsertToDatabase(warehouseAreaManager);
            this.InsertWarehouseAreaManagerValidate(warehouseAreaManager);
        }
        public void UpdateWarehouseAreaManager(WarehouseAreaManager warehouseAreaManager) {
            //客户端Grid编辑界面，作为非强绑定清单
            //Remove操作为修改关联字段，必须提供Edit操作
        }
        public void DeleteWarehouseAreaManager(WarehouseAreaManager warehouseAreaManager) {
            DeleteFromDatabase(warehouseAreaManager);
        }
        public IQueryable<WarehouseAreaManager> GetWarehouseAreaManagerWithPersonnel() {
            return ObjectContext.WarehouseAreaManagers.Include("Personnel").Where(r => r.Personnel.Status == (int)DcsBaseDataStatus.有效).OrderBy(entity => entity.Id);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:33
        //原分布类的函数全部转移到Ach                                                               

        public void InsertWarehouseAreaManager(WarehouseAreaManager warehouseAreaManager) {
            new WarehouseAreaManagerAch(this).InsertWarehouseAreaManager(warehouseAreaManager);
        }

        public void UpdateWarehouseAreaManager(WarehouseAreaManager warehouseAreaManager) {
            new WarehouseAreaManagerAch(this).UpdateWarehouseAreaManager(warehouseAreaManager);
        }

        public void DeleteWarehouseAreaManager(WarehouseAreaManager warehouseAreaManager) {
            new WarehouseAreaManagerAch(this).DeleteWarehouseAreaManager(warehouseAreaManager);
        }

        public IQueryable<WarehouseAreaManager> GetWarehouseAreaManagerWithPersonnel() {
            return new WarehouseAreaManagerAch(this).GetWarehouseAreaManagerWithPersonnel();
        }
    }
}
