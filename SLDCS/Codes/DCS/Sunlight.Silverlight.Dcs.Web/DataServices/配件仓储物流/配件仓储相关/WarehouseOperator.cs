﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class WarehouseOperatorAch : DcsSerivceAchieveBase {
        public WarehouseOperatorAch(DcsDomainService domainService)
            : base(domainService) {
        }

        internal void InsertWarehouseOperatorValidate(WarehouseOperator warehouseOperator) {
            var dbwarehouseOperator = ObjectContext.WarehouseOperators.Where(r => r.WarehouseId == warehouseOperator.WarehouseId && r.OperatorId == warehouseOperator.OperatorId).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbwarehouseOperator != null)
                throw new ValidationException(ErrorStrings.WarehouseOperator_Validation1);
            var userInfo = Utils.GetCurrentUserInfo();
            warehouseOperator.CreatorId = userInfo.Id;
            warehouseOperator.CreatorName = userInfo.Name;
            warehouseOperator.CreateTime = DateTime.Now;
        }
        public void InsertWarehouseOperator(WarehouseOperator warehouseOperator) {
            InsertToDatabase(warehouseOperator);
            this.InsertWarehouseOperatorValidate(warehouseOperator);
        }
        public void UpdateWarehouseOperator(WarehouseOperator warehouseOperator) {
            //客户端Grid编辑界面，作为非强绑定清单
            //Remove操作为修改关联字段，必须提供Edit操作
        }
        public void DeleteWarehouseOperator(WarehouseOperator warehouseOperator) {
            DeleteFromDatabase(warehouseOperator);
        }
        public IQueryable<WarehouseOperator> GetWarehouseOperatorsWithPersonnel() {
            return ObjectContext.WarehouseOperators.Include("Personnel").OrderBy(e => e.Id);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:33
        //原分布类的函数全部转移到Ach                                                               

        public void InsertWarehouseOperator(WarehouseOperator warehouseOperator) {
            new WarehouseOperatorAch(this).InsertWarehouseOperator(warehouseOperator);
        }

        public void UpdateWarehouseOperator(WarehouseOperator warehouseOperator) {
            new WarehouseOperatorAch(this).UpdateWarehouseOperator(warehouseOperator);
        }

        public void DeleteWarehouseOperator(WarehouseOperator warehouseOperator) {
            new WarehouseOperatorAch(this).DeleteWarehouseOperator(warehouseOperator);
        }

        public IQueryable<WarehouseOperator> GetWarehouseOperatorsWithPersonnel() {
            return new WarehouseOperatorAch(this).GetWarehouseOperatorsWithPersonnel();
        }
    }
}
