﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class WarehousePartsStockAch : DcsSerivceAchieveBase {
        public WarehousePartsStockAch(DcsDomainService domainService)
            : base(domainService) {
        }

        /// <summary>
        /// 仓库库存
        /// </summary>
        internal struct WarehousePartsStockSimple {
            public int WarehouseId;
            public int SparePartId;
            public int Quantity;
            public int LockedQuantity;
            public int UsableQuantity;
        }

        /// <summary>
        /// 存在可用库存的库区用途类型
        /// </summary>
        internal static readonly int[] UsableStockAreaCategory = new int[] {
            (int)DcsAreaType.保管区, (int)DcsAreaType.检验区
        };

        /// <summary>
        /// 获取指定仓库的配件库存记录
        /// </summary>
        /// <param name="warehouseId">仓库Id</param>
        /// <param name="partIds">配件Id</param>
        /// <returns>指定配件的库存数据，如果配件不存在库存记录，则对应的库存数量为零</returns>
        internal IEnumerable<WarehousePartsStockSimple> GetWarehousePartsStockSimple(int warehouseId, int[] partIds) {
            if(partIds == null || partIds.Length == 0)
                return new WarehousePartsStockSimple[0];
            partIds = partIds.Distinct().ToArray();
            var stocks = ObjectContext.PartsStocks.Where(v => v.WarehouseId == warehouseId && partIds.Contains(v.PartId) && ObjectContext.WarehouseAreaCategories.Any(category => category.Id == v.WarehouseAreaCategoryId && UsableStockAreaCategory.Contains(category.Category))).GroupBy(v => v.PartId).Select(v => new {
                SparePartId = v.Key,
                Quantity = v.Sum(v1 => v1.Quantity)
            }).ToArray();
            var lockedStocks = ObjectContext.PartsLockedStocks.Where(v => v.WarehouseId == warehouseId && partIds.Contains(v.PartId)).Select(v => new {
                v.PartId,
                v.LockedQuantity
            }).ToArray();
            return (from partId in partIds
                    join stock in stocks on partId equals stock.SparePartId into stocksJoin
                    from stock in stocksJoin.DefaultIfEmpty(new {
                        SparePartId = 0,
                        Quantity = 0
                    })
                    join lockedStock in lockedStocks on partId equals lockedStock.PartId into lockedStocksJoin
                    from lockedStock in lockedStocksJoin.DefaultIfEmpty(new {
                        PartId = 0,
                        LockedQuantity = 0
                    })
                    select new WarehousePartsStockSimple {
                        WarehouseId = warehouseId,
                        SparePartId = partId,
                        Quantity = stock.Quantity,
                        LockedQuantity = lockedStock.LockedQuantity,
                        UsableQuantity = stock.Quantity - lockedStock.LockedQuantity
                    }).ToArray().OrderBy(r => r.WarehouseId);
        }

                [Query(HasSideEffects = true)]
        public IEnumerable<WarehousePartsStock> 配件调拨查询仓库库存(int storageCompanyId, int? warehouseId, int[] partIds) {
            var partsStocks = ObjectContext.PartsStocks.Where(r => r.StorageCompanyId == storageCompanyId && ObjectContext.WarehouseAreaCategories.Any(v => v.Id == r.WarehouseAreaCategoryId && (v.Category == (int)DcsAreaType.保管区)));
            if(warehouseId.HasValue)
                partsStocks = partsStocks.Where(r => r.WarehouseId == warehouseId.Value);
            if(partIds != null && partIds.Length > 0)
                partsStocks = partsStocks.Where(r => partIds.Contains(r.PartId));
            var returnpartsStocks = partsStocks.GroupBy(r => new {
                r.WarehouseId,
                r.StorageCompanyId,
                //r.StorageCompanyType,
                r.BranchId,
                r.PartId
            }).Select(v => new {
                v.Key.WarehouseId,
                v.Key.StorageCompanyId,
                //v.Key.StorageCompanyType,
                v.Key.BranchId,
                v.Key.PartId,
                sumQuantity = v.Sum(r => r.Quantity)
            });
            var retrunpartsStocks = from partsStock in returnpartsStocks
                                    from sparePart in ObjectContext.SpareParts
                                    from branch in ObjectContext.Branches
                                    from wrehouse in ObjectContext.Warehouses
                                    where sparePart.Id == partsStock.PartId && sparePart.Status != (int)DcsMasterDataStatus.作废 && branch.Id == partsStock.BranchId && branch.Status != (int)DcsBaseDataStatus.作废 && wrehouse.Id == partsStock.WarehouseId && wrehouse.Status != (int)DcsBaseDataStatus.作废
                                    select new {
                                        partsStock.WarehouseId,
                                        WarehouseCode = wrehouse.Code,
                                        WarehouseName = wrehouse.Name,
                                        SparePartId = partsStock.PartId,
                                        SparePartCode = sparePart.Code,
                                        SparePartName = sparePart.Name,
                                        sparePart.MeasureUnit,
                                        partsStock.StorageCompanyId,
                                        partsStock.BranchId,
                                        BranchCode = branch.Code,
                                        BranchName = branch.Name,
                                        Quantity = partsStock.sumQuantity
                                    };
            var stockrestuls = from partsStock in retrunpartsStocks
                               join partsLockedStock in ObjectContext.PartsLockedStocks on new {
                                   pId = partsStock.SparePartId,
                                   cId = partsStock.StorageCompanyId,
                                   wId = partsStock.WarehouseId
                               } equals new {
                                   pId = partsLockedStock.PartId,
                                   cId = partsLockedStock.StorageCompanyId,
                                   wId = partsLockedStock.WarehouseId
                               } into details
                               from v in details.DefaultIfEmpty()
                               join bottomStock in ObjectContext.BottomStocks.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on new {
                                   pId = partsStock.SparePartId,
                                   cId = partsStock.StorageCompanyId,
                                   wId = partsStock.WarehouseId
                               } equals new {
                                   pId = bottomStock.SparePartId,
                                   cId = bottomStock.CompanyID,
                                   wId = (int)bottomStock.WarehouseID
                               } into bs
                               from m in bs.DefaultIfEmpty()
                               select new {
                                   partsStock.WarehouseId,
                                   partsStock.WarehouseCode,
                                   partsStock.WarehouseName,
                                   partsStock.SparePartId,
                                   partsStock.SparePartCode,
                                   partsStock.SparePartName,
                                   partsStock.MeasureUnit,
                                   partsStock.StorageCompanyId,
                                   partsStock.BranchId,
                                   partsStock.BranchCode,
                                   partsStock.BranchName,
                                   partsStock.Quantity,
                                   UsableQuantity = partsStock.Quantity - (v == null ? 0 : v.LockedQuantity) - (m == null ? 0 : m.StockQty ?? 0),
                               };

            var tempresult = from wmsresult in stockrestuls
                             from enterprisePartsCost in ObjectContext.EnterprisePartsCosts
                             where wmsresult.SparePartId == enterprisePartsCost.SparePartId && enterprisePartsCost.OwnerCompanyId == wmsresult.StorageCompanyId 
                             select new {
                                 wmsresult.WarehouseId,
                                 wmsresult.WarehouseCode,
                                 wmsresult.WarehouseName,
                                 wmsresult.SparePartId,
                                 wmsresult.SparePartCode,
                                 wmsresult.SparePartName,
                                 wmsresult.MeasureUnit,
                                 wmsresult.StorageCompanyId,
                                 wmsresult.BranchId,
                                 wmsresult.BranchCode,
                                 wmsresult.BranchName,
                                 wmsresult.Quantity,
                                 wmsresult.UsableQuantity,
                                 CostPrice = enterprisePartsCost.CostPrice,
                                 enterprisePartsCost.PartsSalesCategoryId
                             };
            var result = from t in tempresult
                         join a in ObjectContext.PartsRetailGuidePrices on new {
                             t.SparePartId,
                             t.PartsSalesCategoryId
                         } equals new {
                             a.SparePartId,
                             a.PartsSalesCategoryId
                         } into tempTable
                         from t1 in tempTable.DefaultIfEmpty()
                         select new WarehousePartsStock {
                             WarehouseId = t.WarehouseId,
                             WarehouseCode = t.WarehouseCode,
                             WarehouseName = t.WarehouseName,
                             SparePartId = t.SparePartId,
                             SparePartCode = t.SparePartCode,
                             SparePartName = t.SparePartName,
                             MeasureUnit = t.MeasureUnit,
                             StorageCompanyId = t.StorageCompanyId,
                             BranchId = t.BranchId,
                             BranchCode = t.BranchCode,
                             BranchName = t.BranchName,
                             Quantity = t.Quantity,
                             UsableQuantity = t.UsableQuantity,
                             PartsSalesPrice = t.CostPrice,//销售价改为成本价
                             PartsSalesCategoryId = t.PartsSalesCategoryId,
                             PartsSalesCategoryName = null,
                             PartsRetailGuidePrice = t1.RetailGuidePrice
                         };
            return result.Distinct().OrderBy(r => r.WarehouseId);
        }

        [Query(HasSideEffects = true)]
        public IEnumerable<WarehousePartsStock> 查询仓库库存(int storageCompanyId, int? warehouseId, int[] partIds) {
            var partsStocks = ObjectContext.PartsStocks.Where(r => r.StorageCompanyId == storageCompanyId && ObjectContext.WarehouseAreaCategories.Any(v => v.Id == r.WarehouseAreaCategoryId && (v.Category == (int)DcsAreaType.保管区 || v.Category == (int)DcsAreaType.检验区)));
            if(warehouseId.HasValue)
                partsStocks = partsStocks.Where(r => r.WarehouseId == warehouseId.Value);
            if(partIds != null && partIds.Length > 0)
                partsStocks = partsStocks.Where(r => partIds.Contains(r.PartId));
            var returnpartsStocks = partsStocks.GroupBy(r => new {
                r.WarehouseId,
                r.StorageCompanyId,
                //r.StorageCompanyType,
                r.BranchId,
                r.PartId
            }).Select(v => new {
                v.Key.WarehouseId,
                v.Key.StorageCompanyId,
                //v.Key.StorageCompanyType,
                v.Key.BranchId,
                v.Key.PartId,
                sumQuantity = v.Sum(r => r.Quantity)
            });
            var retrunpartsStocks = from partsStock in returnpartsStocks
                                    from sparePart in ObjectContext.SpareParts
                                    from branch in ObjectContext.Branches
                                    from wrehouse in ObjectContext.Warehouses
                                    where sparePart.Id == partsStock.PartId && sparePart.Status != (int)DcsMasterDataStatus.作废 && branch.Id == partsStock.BranchId && branch.Status != (int)DcsBaseDataStatus.作废 && wrehouse.Id == partsStock.WarehouseId && wrehouse.Status != (int)DcsBaseDataStatus.作废
                                    select new {
                                        partsStock.WarehouseId,
                                        WarehouseCode = wrehouse.Code,
                                        WarehouseName = wrehouse.Name,
                                        SparePartId = partsStock.PartId,
                                        SparePartCode = sparePart.Code,
                                        SparePartName = sparePart.Name,
                                        sparePart.MeasureUnit,
                                        partsStock.StorageCompanyId,
                                        // partsStock.StorageCompanyType,
                                        partsStock.BranchId,
                                        BranchCode = branch.Code,
                                        BranchName = branch.Name,
                                        Quantity = partsStock.sumQuantity
                                    };
            var stockrestuls = from partsStock in retrunpartsStocks
                               join partsLockedStock in ObjectContext.PartsLockedStocks on new {
                                   pId = partsStock.SparePartId,
                                   cId = partsStock.StorageCompanyId,
                                   wId = partsStock.WarehouseId
                               } equals new {
                                   pId = partsLockedStock.PartId,
                                   cId = partsLockedStock.StorageCompanyId,
                                   wId = partsLockedStock.WarehouseId
                               } into details
                               from v in details.DefaultIfEmpty()
                               select new {
                                   partsStock.WarehouseId,
                                   partsStock.WarehouseCode,
                                   partsStock.WarehouseName,
                                   partsStock.SparePartId,
                                   partsStock.SparePartCode,
                                   partsStock.SparePartName,
                                   partsStock.MeasureUnit,
                                   partsStock.StorageCompanyId,
                                   //partsStock.StorageCompanyType,
                                   partsStock.BranchId,
                                   partsStock.BranchCode,
                                   partsStock.BranchName,
                                   partsStock.Quantity,
                                   UsableQuantity = partsStock.Quantity - (v == null ? 0 : v.LockedQuantity),
                               };

            //var wmsresults = from stockrestul in stockrestuls
            //                 join wmsCongelationStockView in ObjectContext.WmsCongelationStockViews on new {
            //                     pId = stockrestul.SparePartId,
            //                     wId = stockrestul.WarehouseId
            //                 } equals new {
            //                     pId = wmsCongelationStockView.SparePartId,
            //                     wId = wmsCongelationStockView.WarehouseId
            //                 } into details
            //                 from v in details.DefaultIfEmpty()
            //                 select new {
            //                     stockrestul.WarehouseId,
            //                     stockrestul.WarehouseCode,
            //                     stockrestul.WarehouseName,
            //                     stockrestul.SparePartId,
            //                     stockrestul.SparePartCode,
            //                     stockrestul.SparePartName,
            //                     stockrestul.MeasureUnit,
            //                     stockrestul.StorageCompanyId,
            //                     //stockrestul.StorageCompanyType,
            //                     stockrestul.BranchId,
            //                     stockrestul.BranchCode,
            //                     stockrestul.BranchName,
            //                     stockrestul.Quantity,
            //                     UsableQuantity = stockrestul.UsableQuantity - (v == null ? 0 : v.CongelationStockQty) - (v == null ? 0 : v.DisabledStock)
            //                 };

            var tempresult = from partsSalesPrice in ObjectContext.PartsSalesPrices
                             from wmsresult in stockrestuls
                             from salesUnit in ObjectContext.SalesUnits
                             from salesUnitAffiWarehouse in ObjectContext.SalesUnitAffiWarehouses
                             //from partsPlannedPrice in ObjectContext.PartsPlannedPrices
                             where wmsresult.SparePartId == partsSalesPrice.SparePartId && salesUnit.PartsSalesCategoryId == partsSalesPrice.PartsSalesCategoryId &&
                                 //wmsresult.SparePartId == partsPlannedPrice.SparePartId && partsPlannedPrice.PartsSalesCategoryId == salesUnit.PartsSalesCategoryId &&
                             salesUnitAffiWarehouse.WarehouseId == wmsresult.WarehouseId && salesUnitAffiWarehouse.SalesUnitId == salesUnit.Id
                             select new {
                                 wmsresult.WarehouseId,
                                 wmsresult.WarehouseCode,
                                 wmsresult.WarehouseName,
                                 wmsresult.SparePartId,
                                 wmsresult.SparePartCode,
                                 wmsresult.SparePartName,
                                 wmsresult.MeasureUnit,
                                 wmsresult.StorageCompanyId,
                                 //wmsresult.StorageCompanyType,
                                 wmsresult.BranchId,
                                 wmsresult.BranchCode,
                                 wmsresult.BranchName,
                                 wmsresult.Quantity,
                                 wmsresult.UsableQuantity,
                                 PartsSalesPrice = partsSalesPrice.SalesPrice,
                                 //PartsPlannedPrice = partsPlannedPrice.PlannedPrice,
                                 salesUnit.PartsSalesCategoryId
                             };
            var result = from t in tempresult
                         join a in ObjectContext.PartsRetailGuidePrices on new {
                             t.SparePartId,
                             t.PartsSalesCategoryId
                         } equals new {
                             a.SparePartId,
                             a.PartsSalesCategoryId
                         } into tempTable
                         from t1 in tempTable.DefaultIfEmpty()
                         select new WarehousePartsStock {
                             WarehouseId = t.WarehouseId,
                             WarehouseCode = t.WarehouseCode,
                             WarehouseName = t.WarehouseName,
                             SparePartId = t.SparePartId,
                             SparePartCode = t.SparePartCode,
                             SparePartName = t.SparePartName,
                             MeasureUnit = t.MeasureUnit,
                             StorageCompanyId = t.StorageCompanyId,
                             //StorageCompanyType = t.StorageCompanyType,
                             BranchId = t.BranchId,
                             BranchCode = t.BranchCode,
                             BranchName = t.BranchName,
                             Quantity = t.Quantity,
                             UsableQuantity = t.UsableQuantity,
                             PartsSalesPrice = t.PartsSalesPrice,
                             //PartsPlannedPrice = t.PartsPlannedPrice,
                             PartsSalesCategoryId = t.PartsSalesCategoryId,
                             PartsSalesCategoryName = null,
                             PartsRetailGuidePrice = t1.RetailGuidePrice
                         };
            return result.OrderBy(r => r.WarehouseId);
        }

        [Query(HasSideEffects = true)]
        public IEnumerable<WarehousePartsStock> 查询仓库库存销售审核(int storageCompanyId, int? warehouseId, int[] partIds) {
            var partsStocks = ObjectContext.PartsStocks.Where(r => r.StorageCompanyId == storageCompanyId && ObjectContext.WarehouseAreaCategories.Any(v => v.Id == r.WarehouseAreaCategoryId && (v.Category == (int)DcsAreaType.保管区)));
            if(warehouseId.HasValue)
                partsStocks = partsStocks.Where(r => r.WarehouseId == warehouseId.Value);
            if(partIds != null && partIds.Length > 0)
                partsStocks = partsStocks.Where(r => partIds.Contains(r.PartId));
            var returnpartsStocks = partsStocks.GroupBy(r => new {
                r.WarehouseId,
                r.StorageCompanyId,
                //r.StorageCompanyType,
                r.BranchId,
                r.PartId
            }).Select(v => new {
                v.Key.WarehouseId,
                v.Key.StorageCompanyId,
                //v.Key.StorageCompanyType,
                v.Key.BranchId,
                v.Key.PartId,
                sumQuantity = v.Sum(r => r.Quantity)
            });
            var retrunpartsStocks = from partsStock in returnpartsStocks
                                    from sparePart in ObjectContext.SpareParts
                                    from branch in ObjectContext.Branches
                                    from wrehouse in ObjectContext.Warehouses
                                    where sparePart.Id == partsStock.PartId && sparePart.Status != (int)DcsMasterDataStatus.作废 && branch.Id == partsStock.BranchId && branch.Status != (int)DcsBaseDataStatus.作废 && wrehouse.Id == partsStock.WarehouseId && wrehouse.Status != (int)DcsBaseDataStatus.作废
                                    select new {
                                        partsStock.WarehouseId,
                                        WarehouseCode = wrehouse.Code,
                                        WarehouseName = wrehouse.Name,
                                        SparePartId = partsStock.PartId,
                                        SparePartCode = sparePart.Code,
                                        SparePartName = sparePart.Name,
                                        sparePart.MeasureUnit,
                                        partsStock.StorageCompanyId,
                                        // partsStock.StorageCompanyType,
                                        partsStock.BranchId,
                                        BranchCode = branch.Code,
                                        BranchName = branch.Name,
                                        Quantity = partsStock.sumQuantity
                                    };
            var stockrestuls = from partsStock in retrunpartsStocks
                               join partsLockedStock in ObjectContext.PartsLockedStocks on new {
                                   pId = partsStock.SparePartId,
                                   cId = partsStock.StorageCompanyId,
                                   wId = partsStock.WarehouseId
                               } equals new {
                                   pId = partsLockedStock.PartId,
                                   cId = partsLockedStock.StorageCompanyId,
                                   wId = partsLockedStock.WarehouseId
                               } into details
                               from v in details.DefaultIfEmpty()
                               select new {
                                   partsStock.WarehouseId,
                                   partsStock.WarehouseCode,
                                   partsStock.WarehouseName,
                                   partsStock.SparePartId,
                                   partsStock.SparePartCode,
                                   partsStock.SparePartName,
                                   partsStock.MeasureUnit,
                                   partsStock.StorageCompanyId,
                                   //partsStock.StorageCompanyType,
                                   partsStock.BranchId,
                                   partsStock.BranchCode,
                                   partsStock.BranchName,
                                   partsStock.Quantity,
                                   UsableQuantity = partsStock.Quantity - (v == null ? 0 : v.LockedQuantity),
                               };

            //from wmsresult in wmsresults
            var tempresult = from partsSalesPrice in ObjectContext.PartsSalesPrices.Where(r => partIds.Contains(r.SparePartId))
                             from salesUnit in ObjectContext.SalesUnits
                             from salesUnitAffiWarehouse in ObjectContext.SalesUnitAffiWarehouses.Where(r => r.WarehouseId == warehouseId)
                             where salesUnit.PartsSalesCategoryId == partsSalesPrice.PartsSalesCategoryId &&
                            salesUnitAffiWarehouse.SalesUnitId == salesUnit.Id
                             select new {
                                 WarehouseId = salesUnitAffiWarehouse.WarehouseId,
                                 SparePartId = partsSalesPrice.SparePartId,
                                 PartsSalesPrice = partsSalesPrice.SalesPrice,
                                 salesUnit.PartsSalesCategoryId
                             };
            var result = from t in stockrestuls
                         join s in tempresult
                        on new {
                            t.SparePartId,
                            t.WarehouseId
                        } equals new {
                            s.SparePartId,
                            s.WarehouseId
                        } into tempTable1
                         from t2 in tempTable1.DefaultIfEmpty()
                         join a in ObjectContext.PartsRetailGuidePrices on new {
                             t2.SparePartId,
                             t2.PartsSalesCategoryId
                         } equals new {
                             a.SparePartId,
                             a.PartsSalesCategoryId
                         } into tempTable
                         from t1 in tempTable.DefaultIfEmpty()
                         select new WarehousePartsStock {
                             WarehouseId = t.WarehouseId,
                             WarehouseCode = t.WarehouseCode,
                             WarehouseName = t.WarehouseName,
                             SparePartId = t.SparePartId,
                             SparePartCode = t.SparePartCode,
                             SparePartName = t.SparePartName,
                             MeasureUnit = t.MeasureUnit,
                             StorageCompanyId = t.StorageCompanyId,
                             //StorageCompanyType = t.StorageCompanyType,
                             BranchId = t.BranchId,
                             BranchCode = t.BranchCode,
                             BranchName = t.BranchName,
                             Quantity = t.Quantity,
                             UsableQuantity = t.UsableQuantity,
                             PartsSalesPrice = t2.PartsSalesPrice,
                             //PartsPlannedPrice = t2.PartsPlannedPrice,
                             PartsSalesCategoryId = t2.PartsSalesCategoryId,
                             PartsSalesCategoryName = null,
                             PartsRetailGuidePrice = t1.RetailGuidePrice
                         };
            return result.OrderBy(r => r.WarehouseId);
        }


        [Query(HasSideEffects = true)]
        public IEnumerable<WarehousePartsStock> 只查询仓库库存(int storageCompanyId, int? warehouseId, int[] partIds) {

            var partsStocks = ObjectContext.PartsStocks.Where(r => r.StorageCompanyId == storageCompanyId && ObjectContext.WarehouseAreaCategories.Any(v => v.Id == r.WarehouseAreaCategoryId && (v.Category == (int)DcsAreaType.保管区 || v.Category == (int)DcsAreaType.检验区)));
            if(warehouseId.HasValue) {
                partsStocks = partsStocks.Where(r => r.WarehouseId == warehouseId.Value);
            }
            if(partIds != null && partIds.Length > 0) {
                partsStocks = partsStocks.Where(r => partIds.Contains(r.PartId));
            }
            var partsStocksGroup = partsStocks.GroupBy(r => new {
                r.WarehouseId,
                r.StorageCompanyId,
                r.BranchId,
                r.PartId,
            }).Select(v => new {
                v.Key.WarehouseId,
                v.Key.StorageCompanyId,
                v.Key.BranchId,
                v.Key.PartId,
                sumQuantity = v.Sum(r => r.Quantity)
            });
            var partsLockedStockGroup = ObjectContext.PartsLockedStocks.GroupBy(r => new {
                r.BranchId,
                r.StorageCompanyId,
                r.WarehouseId,
                r.PartId
            }).Select(v => new {
                v.Key.WarehouseId,
                v.Key.StorageCompanyId,
                v.Key.BranchId,
                v.Key.PartId,
                sumLockQuantity = v.Sum(r => r.LockedQuantity)
            });

            var wmsCongelationStockViewGroup = ObjectContext.WmsCongelationStockViews.Where(r => r.StorageCompanyId == storageCompanyId).GroupBy(r => new {
                r.StorageCompanyId,
                r.BranchId,
                r.WarehouseId,
                r.SparePartId
            }).Select(v => new {
                v.Key.WarehouseId,
                v.Key.StorageCompanyId,
                v.Key.BranchId,
                PartId = v.Key.SparePartId,
                sumCongelationStockQty = v.Sum(r => r.CongelationStockQty),
                sumDisabledStock = v.Sum(r => r.DisabledStock)
            });
            var tempStruct = from a in partsStocksGroup
                             join t in ObjectContext.SalesUnitAffiWarehouses on a.WarehouseId equals t.WarehouseId
                             join salesUnit in ObjectContext.SalesUnits on t.SalesUnitId equals salesUnit.Id
                             join s in ObjectContext.PartsSalesPrices.Where(p => p.Status == (int)DcsBaseDataStatus.有效) on new {
                                 t1 = salesUnit.PartsSalesCategoryId,
                                 t2 = a.PartId
                             }
                            equals new {
                                t1 = s.PartsSalesCategoryId,
                                t2 = s.SparePartId
                            } into tempTable3
                             from t3 in tempTable3.DefaultIfEmpty()
                             join b in wmsCongelationStockViewGroup on new {
                                 a.WarehouseId,
                                 a.StorageCompanyId,
                                 a.BranchId,
                                 a.PartId
                             } equals new {
                                 b.WarehouseId,
                                 b.StorageCompanyId,
                                 b.BranchId,
                                 b.PartId
                             } into tempTable1
                             from t1 in tempTable1.DefaultIfEmpty()
                             join c in partsLockedStockGroup on new {
                                 a.WarehouseId,
                                 a.StorageCompanyId,
                                 a.BranchId,
                                 a.PartId
                             } equals new {
                                 c.WarehouseId,
                                 c.StorageCompanyId,
                                 c.BranchId,
                                 c.PartId
                             } into tempTable2
                             from t2 in tempTable2.DefaultIfEmpty()
                             select new {
                                 salesUnit.PartsSalesCategoryId,
                                 a.WarehouseId,
                                 a.StorageCompanyId,
                                 a.BranchId,
                                 a.PartId,
                                 t3.PriceType,
                                 t3.SalesPrice,
                                 Quantity = a.sumQuantity,
                                 CongelationStockQty = (int?)t1.sumCongelationStockQty,
                                 DisabledStock = (int?)t1.sumDisabledStock,
                                 LockQuantity = (int?)t2.sumLockQuantity
                             };


            var result = from a in tempStruct
                         join b in ObjectContext.Warehouses.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on a.WarehouseId equals b.Id
                         join c in ObjectContext.Branches.Where(r => r.Status == (int)DcsMasterDataStatus.有效) on a.BranchId equals c.Id
                         join d in ObjectContext.SpareParts.Where(r => r.Status == (int)DcsMasterDataStatus.有效) on a.PartId equals d.Id
                         select new WarehousePartsStock {
                             Id = d.Id,
                             PartsSalesCategoryId = a.PartsSalesCategoryId,
                             WarehouseId = a.WarehouseId,
                             WarehouseCode = b.Code,
                             WarehouseName = b.Name,
                             SparePartId = a.PartId,
                             SparePartCode = d.Code,
                             SparePartName = d.Name,
                             StorageCompanyId = a.StorageCompanyId,
                             BranchId = a.BranchId,
                             BranchCode = c.Code,
                             BranchName = c.Name,
                             Quantity = a.Quantity,
                             UsableQuantity = a.Quantity - (a.CongelationStockQty ?? 0) - (a.DisabledStock ?? 0) - (a.LockQuantity ?? 0),
                             PartsSalesPrice = a.SalesPrice,
                             PriceType = a.PriceType,
                             StorageCenter = b.StorageCenter,
                             StockMaximum = 0,
                             StockMinimum = 0,
                             SafeStock = 0
                         };
            return result.OrderBy(r => r.WarehouseId);
        }
        [Query(HasSideEffects = true)]
        public IEnumerable<WarehousePartsStock> 只查询仓库库存1(int storageCompanyId, int? warehouseId, int[] partIds) {
            var currentEnterPriseId = Utils.GetCurrentUserInfo().EnterpriseId;
            var partsStocks = ObjectContext.PartsStocks.Where(r => r.StorageCompanyId == currentEnterPriseId);
            partsStocks = partsStocks.Where(r => ObjectContext.WarehouseAreaCategories.Any(v => v.Id == r.WarehouseAreaCategoryId && (v.Category == (int)DcsAreaType.保管区 || v.Category == (int)DcsAreaType.检验区)));
            if(warehouseId.HasValue) {
                partsStocks = partsStocks.Where(r => r.WarehouseId == warehouseId.Value);
            }
            if(partIds != null && partIds.Length > 0) {
                partsStocks = partsStocks.Where(r => partIds.Contains(r.PartId));
            }
            var partsStocksGroup = partsStocks.GroupBy(r => new {
                r.WarehouseId,
                r.StorageCompanyId,
                r.BranchId,
                r.PartId,
            }).Select(v => new {
                v.Key.WarehouseId,
                v.Key.StorageCompanyId,
                v.Key.BranchId,
                v.Key.PartId,
                sumQuantity = v.Sum(r => r.Quantity)
            });
            var partsLockedStockGroup = ObjectContext.PartsLockedStocks.GroupBy(r => new {
                r.BranchId,
                r.StorageCompanyId,
                r.WarehouseId,
                r.PartId
            }).Select(v => new {
                v.Key.WarehouseId,
                v.Key.StorageCompanyId,
                v.Key.BranchId,
                v.Key.PartId,
                sumLockQuantity = v.Sum(r => r.LockedQuantity)
            });

            var wmsCongelationStockViewGroup = ObjectContext.WmsCongelationStockViews.Where(r => r.StorageCompanyId == storageCompanyId).GroupBy(r => new {
                r.StorageCompanyId,
                r.BranchId,
                r.WarehouseId,
                r.SparePartId
            }).Select(v => new {
                v.Key.WarehouseId,
                v.Key.StorageCompanyId,
                v.Key.BranchId,
                PartId = v.Key.SparePartId,
                sumCongelationStockQty = v.Sum(r => r.CongelationStockQty),
                sumDisabledStock = v.Sum(r => r.DisabledStock)
            });
            var tempStruct = from a in partsStocksGroup
                             join t in ObjectContext.SalesUnitAffiWarehouses on a.WarehouseId equals t.WarehouseId
                             join salesUnit in ObjectContext.SalesUnits on t.SalesUnitId equals salesUnit.Id
                             join s in ObjectContext.PartsSalesPrices.Where(p => p.Status == (int)DcsBaseDataStatus.有效) on new {
                                 t1 = salesUnit.PartsSalesCategoryId,
                                 t2 = a.PartId
                             }
                            equals new {
                                t1 = s.PartsSalesCategoryId,
                                t2 = s.SparePartId
                            } into tempTable3
                             from t3 in tempTable3.DefaultIfEmpty()
                             join b in wmsCongelationStockViewGroup on new {
                                 a.WarehouseId,
                                 a.StorageCompanyId,
                                 a.BranchId,
                                 a.PartId
                             } equals new {
                                 b.WarehouseId,
                                 b.StorageCompanyId,
                                 b.BranchId,
                                 b.PartId
                             } into tempTable1
                             from t1 in tempTable1.DefaultIfEmpty()
                             join c in partsLockedStockGroup on new {
                                 a.WarehouseId,
                                 a.StorageCompanyId,
                                 a.BranchId,
                                 a.PartId
                             } equals new {
                                 c.WarehouseId,
                                 c.StorageCompanyId,
                                 c.BranchId,
                                 c.PartId
                             } into tempTable2
                             from t2 in tempTable2.DefaultIfEmpty()
                             select new {
                                 salesUnit.PartsSalesCategoryId,
                                 a.WarehouseId,
                                 a.StorageCompanyId,
                                 a.BranchId,
                                 a.PartId,
                                 t3.PriceType,
                                 t3.SalesPrice,
                                 Quantity = a.sumQuantity,
                                 CongelationStockQty = (int?)t1.sumCongelationStockQty,
                                 DisabledStock = (int?)t1.sumDisabledStock,
                                 LockQuantity = (int?)t2.sumLockQuantity
                             };


            var result = from a in tempStruct
                         join b in ObjectContext.Warehouses.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on a.WarehouseId equals b.Id
                         join c in ObjectContext.Branches.Where(r => r.Status == (int)DcsMasterDataStatus.有效) on a.BranchId equals c.Id
                         join d in ObjectContext.SpareParts.Where(r => r.Status == (int)DcsMasterDataStatus.有效) on a.PartId equals d.Id
                         select new WarehousePartsStock {
                             Id = d.Id,
                             PartsSalesCategoryId = a.PartsSalesCategoryId,
                             WarehouseId = a.WarehouseId,
                             WarehouseCode = b.Code,
                             WarehouseName = b.Name,
                             SparePartId = a.PartId,
                             SparePartCode = d.Code,
                             SparePartName = d.Name,
                             StorageCompanyId = a.StorageCompanyId,
                             BranchId = a.BranchId,
                             BranchCode = c.Code,
                             BranchName = c.Name,
                             Quantity = a.Quantity,
                             UsableQuantity = a.Quantity - (a.CongelationStockQty ?? 0) - (a.DisabledStock ?? 0) - (a.LockQuantity ?? 0),
                             PartsSalesPrice = a.SalesPrice,
                             PriceType = a.PriceType,
                             StorageCenter = b.StorageCenter,
                             StockMaximum = 0,
                             StockMinimum = 0,
                             SafeStock = 0
                         };
            return result.OrderBy(r => r.WarehouseId);
        }
        [Query(HasSideEffects = true)]
        public IEnumerable<WarehousePartsStock> 只查询仓库库存保外(int storageCompanyId, int? warehouseId, int[] partIds) {
            var currentEnterPriseId = Utils.GetCurrentUserInfo().EnterpriseId;
            var company = ObjectContext.Companies.SingleOrDefault(r => r.Status == (int)DcsBaseDataStatus.有效 && r.Id == currentEnterPriseId);
            if(company == null)
                return null;
            var branchIds = ObjectContext.DealerServiceInfoes.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.DealerId == currentEnterPriseId).Select(t => t.BranchId).Distinct().ToArray();
            if(company.Type == (int)DcsCompanyType.服务站 && !branchIds.Any())
                return null;
            var agencyBranchIds = ObjectContext.AgencyAffiBranches.Where(r => r.AgencyId == currentEnterPriseId).Select(t => t.BranchId).Distinct().ToArray();
            if(((company.Type == (int)DcsCompanyType.服务站兼代理库 || company.Type == (int)DcsCompanyType.代理库)) && !agencyBranchIds.Any())
                return null;
            var partsStocks = ObjectContext.PartsStocks.Where(r => ObjectContext.WarehouseAreaCategories.Any(v => v.Id == r.WarehouseAreaCategoryId && (v.Category == (int)DcsAreaType.保管区 || v.Category == (int)DcsAreaType.检验区)));
            var wmsCongelationStockViewGroup = ObjectContext.WmsCongelationStockViews.GroupBy(r => new {
                r.StorageCompanyId,
                r.BranchId,
                r.WarehouseId,
                r.SparePartId
            }).Select(v => new {
                v.Key.WarehouseId,
                v.Key.StorageCompanyId,
                v.Key.BranchId,
                PartId = v.Key.SparePartId,
                sumCongelationStockQty = (int?)v.Sum(r => r.CongelationStockQty),
                sumDisabledStock = (int?)v.Sum(r => r.DisabledStock)
            });
            if(company.Type == (int)DcsCompanyType.服务站兼代理库 || company.Type == (int)DcsCompanyType.代理库) {
                partsStocks = partsStocks.Where(r => agencyBranchIds.Contains(r.StorageCompanyId));
                wmsCongelationStockViewGroup = wmsCongelationStockViewGroup.Where(r => agencyBranchIds.Contains(r.StorageCompanyId));
            } else {
                partsStocks = partsStocks.Where(r => branchIds.Contains(r.StorageCompanyId));
                wmsCongelationStockViewGroup = wmsCongelationStockViewGroup.Where(r => branchIds.Contains(r.StorageCompanyId));
            }
            if(warehouseId.HasValue) {
                partsStocks = partsStocks.Where(r => r.WarehouseId == warehouseId.Value);
            }
            if(partIds != null && partIds.Length > 0) {
                partsStocks = partsStocks.Where(r => partIds.Contains(r.PartId));
            }
            var partsStocksGroup = partsStocks.GroupBy(r => new {
                r.WarehouseId,
                r.StorageCompanyId,
                r.BranchId,
                r.PartId,
            }).Select(v => new {
                v.Key.WarehouseId,
                v.Key.StorageCompanyId,
                v.Key.BranchId,
                v.Key.PartId,
                sumQuantity = v.Sum(r => r.Quantity)
            });
            var partsLockedStockGroup = ObjectContext.PartsLockedStocks.GroupBy(r => new {
                r.BranchId,
                r.StorageCompanyId,
                r.WarehouseId,
                r.PartId
            }).Select(v => new {
                v.Key.WarehouseId,
                v.Key.StorageCompanyId,
                v.Key.BranchId,
                v.Key.PartId,
                sumLockQuantity = (int?)v.Sum(r => r.LockedQuantity)
            });
            var tempStruct = from a in partsStocksGroup
                             join t in ObjectContext.SalesUnitAffiWarehouses on a.WarehouseId equals t.WarehouseId
                             join salesUnit in ObjectContext.SalesUnits on t.SalesUnitId equals salesUnit.Id
                             join ps in ObjectContext.PartsSalesCategories.Where(r => r.IsNotWarranty && r.Status == (int)DcsBaseDataStatus.有效) on salesUnit.PartsSalesCategoryId equals ps.Id
                             join s in ObjectContext.PartsSalesPrices.Where(p => p.Status == (int)DcsBaseDataStatus.有效) on new {
                                 t1 = salesUnit.PartsSalesCategoryId,
                                 t2 = a.PartId
                             }
                            equals new {
                                t1 = s.PartsSalesCategoryId,
                                t2 = s.SparePartId
                            }
                             join b in wmsCongelationStockViewGroup on new {
                                 a.WarehouseId,
                                 a.StorageCompanyId,
                                 a.BranchId,
                                 a.PartId
                             } equals new {
                                 b.WarehouseId,
                                 b.StorageCompanyId,
                                 b.BranchId,
                                 b.PartId
                             } into tempTable1
                             from t1 in tempTable1.DefaultIfEmpty()
                             join c in partsLockedStockGroup on new {
                                 a.WarehouseId,
                                 a.StorageCompanyId,
                                 a.BranchId,
                                 a.PartId
                             } equals new {
                                 c.WarehouseId,
                                 c.StorageCompanyId,
                                 c.BranchId,
                                 c.PartId
                             } into tempTable2
                             from t2 in tempTable2.DefaultIfEmpty()
                             select new {
                                 salesUnit.PartsSalesCategoryId,
                                 a.WarehouseId,
                                 a.StorageCompanyId,
                                 a.BranchId,
                                 a.PartId,
                                 s.PriceType,
                                 s.SalesPrice,
                                 Quantity = a.sumQuantity,
                                 UsableQuantity = a.sumQuantity - (t1.sumCongelationStockQty ?? 0) - (t1.sumDisabledStock ?? 0) - (t2.sumLockQuantity ?? 0)
                             };
            var result = from a in tempStruct
                         join b in ObjectContext.Warehouses.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on a.WarehouseId equals b.Id
                         join c in ObjectContext.Branches.Where(r => r.Status == (int)DcsMasterDataStatus.有效) on a.BranchId equals c.Id
                         join d in ObjectContext.SpareParts.Where(r => r.Status == (int)DcsMasterDataStatus.有效) on a.PartId equals d.Id

                         select new WarehousePartsStock {
                             PartsSalesCategoryId = a.PartsSalesCategoryId,
                             WarehouseId = a.WarehouseId,
                             WarehouseCode = b.Code,
                             WarehouseName = b.Name,
                             SparePartId = a.PartId,
                             SparePartCode = d.Code,
                             SparePartName = d.Name,
                             StorageCompanyId = a.StorageCompanyId,
                             BranchId = a.BranchId,
                             BranchCode = c.Code,
                             BranchName = c.Name,
                             Quantity = a.Quantity,
                             UsableQuantity = a.UsableQuantity,
                             PartsSalesPrice = a.SalesPrice,
                             PriceType = a.PriceType
                         };
            return result.OrderBy(r => r.WarehouseId);
        }

        public IQueryable<WarehousePartsStock> 查询替互换件库存(int partsSalesCategoryId, int warehouseId, int partId) {

            var partsStockGroupQuery = from g in
                                           (from a in ObjectContext.PartsStocks
                                            join b in ObjectContext.WarehouseAreaCategories on a.WarehouseAreaCategoryId equals b.Id
                                            select new {
                                                a.PartId,
                                                a.BranchId,
                                                a.StorageCompanyId,
                                                a.StorageCompanyType,
                                                b.Category,
                                                a.WarehouseId,
                                                a.Quantity

                                            })
                                       group g by new {
                                           g.PartId,
                                           g.BranchId,
                                           g.StorageCompanyId,
                                           g.StorageCompanyType,
                                           g.WarehouseId,
                                           g.Category

                                       }
                                           into tmp
                                           select new {
                                               tmp.Key.PartId,

                                               tmp.Key.BranchId,
                                               tmp.Key.StorageCompanyId,
                                               tmp.Key.StorageCompanyType,
                                               tmp.Key.WarehouseId,
                                               tmp.Key.Category,
                                               Quantity = (int?)tmp.Sum(r => r.Quantity)
                                           };







            var warehousePartsStock = (from b in ObjectContext.PartsReplacements.Where(r => r.OldPartId == partId && r.Status == (int)DcsBaseDataStatus.有效)
                                       join c in ObjectContext.SpareParts on b.NewPartId equals c.Id
                                       join d in partsStockGroupQuery.Where(r => r.WarehouseId == warehouseId && r.Category == (int)DcsAreaType.保管区) on c.Id equals d.PartId into tempTable1
                                       from t1 in tempTable1.DefaultIfEmpty()
                                       join e in ObjectContext.PartsLockedStocks.Where(r => r.WarehouseId == warehouseId) on c.Id equals e.PartId into tempTable2
                                       from t2 in tempTable2.DefaultIfEmpty()
                                       join g in ObjectContext.PartsBranches.Where(r => r.IsSalable.HasValue && r.IsSalable.Value && r.PartsSalesCategoryId == partsSalesCategoryId) on b.NewPartId equals g.PartId
                                       join h in ObjectContext.PartsSalesPrices.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId) on b.NewPartId equals h.SparePartId
                                       where h.SalesPrice != 0
                                       select new {
                                           WarehouseId = warehouseId,
                                           ReplacementType = (int)DcsPartsReplacementReplacementType.替换,
                                           Quantity = (t1.Quantity ?? 0),
                                           b.NewPartId,
                                           b.NewPartCode,
                                           b.NewPartName,
                                           UsableQuantity = (t1.Quantity ?? 0) - (t2 == null ? 0 : t2.LockedQuantity)
                                       }).Distinct();
            var sparePart = ObjectContext.SpareParts.Where(r => r.Id == partId).FirstOrDefault();
            if(sparePart == null) {
                return null;
            }
            var partsExchangeExchangeCodes = ObjectContext.PartsExchanges.FirstOrDefault(v => v.PartId == sparePart.Id && v.Status == (int)DcsBaseDataStatus.有效);
            if(partsExchangeExchangeCodes == null) {
                var resultPart3 = warehousePartsStock.GroupBy(r => new {
                    r.WarehouseId,
                    r.ReplacementType,
                    r.NewPartId,
                    r.NewPartCode,
                    r.NewPartName
                }).Select(r => new WarehousePartsStock {
                    WarehouseId = r.Key.WarehouseId,
                    ReplacementType = r.Key.ReplacementType,
                    NewPartId = r.Key.NewPartId,
                    NewPartCode = r.Key.NewPartCode,
                    NewPartName = r.Key.NewPartName,
                    Quantity = r.Sum(v => v.Quantity),
                    UsableQuantity = r.Sum(v => v.UsableQuantity)
                });
                return resultPart3.OrderBy(r => r.NewPartId);
            }
            var partsExchangeGroup = ObjectContext.PartsExchangeGroups.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && partsExchangeExchangeCodes.ExchangeCode == r.ExchangeCode);
            //if(!partsExchangeGroup.Any()) {
            //    return null;
            //}
            var partsExchangeGroupExchangeCode = ObjectContext.PartsExchangeGroups.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && partsExchangeGroup.Select(v => v.ExGroupCode).Contains(r.ExGroupCode));
            var partsstock = (from c in ObjectContext.PartsExchanges.Where(r => r.PartId != partId && r.Status == (int)DcsBaseDataStatus.有效 && (partsExchangeGroupExchangeCode.Select(v => v.ExchangeCode).Contains(r.ExchangeCode) || partsExchangeExchangeCodes.ExchangeCode == r.ExchangeCode))// on b.ExchangeCode equals c.ExchangeCode
                              join d in ObjectContext.SpareParts on c.PartId equals d.Id
                              join b in ObjectContext.PartsExchangeGroups.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on c.ExchangeCode equals b.ExchangeCode into tempTable3
                              from t3 in tempTable3.DefaultIfEmpty()
                              join e in partsStockGroupQuery.Where(r => r.WarehouseId == warehouseId && r.Category == (int)DcsAreaType.保管区) on d.Id equals e.PartId into tempTable1
                              from t1 in tempTable1.DefaultIfEmpty()
                              join f in ObjectContext.PartsLockedStocks.Where(r => r.WarehouseId == warehouseId) on d.Id equals f.PartId into tempTable2
                              from t2 in tempTable2.DefaultIfEmpty()
                              join g in ObjectContext.PartsBranches.Where(r => r.IsSalable.HasValue && r.IsSalable.Value && r.PartsSalesCategoryId == partsSalesCategoryId) on c.PartId equals g.PartId
                              join h in ObjectContext.PartsSalesPrices.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId) on c.PartId equals h.SparePartId
                              where h.SalesPrice != 0
                              select new {
                                  WarehouseId = warehouseId,
                                  ReplacementType = (int)DcsPartsReplacementReplacementType.互换,
                                  UsableQuantity = (t1.Quantity ?? 0) - (t2 == null ? 0 : t2.LockedQuantity),
                                  NewPartId = c.PartId,
                                  NewPartCode = d.Code,
                                  NewPartName = d.Name,
                                  Quantity = (t1.Quantity ?? 0)
                              }).Distinct();
            var resultPart1 = warehousePartsStock.GroupBy(r => new {
                r.WarehouseId,
                r.ReplacementType,
                r.NewPartId,
                r.NewPartCode,
                r.NewPartName
            }).Select(r => new WarehousePartsStock {
                WarehouseId = r.Key.WarehouseId,
                ReplacementType = r.Key.ReplacementType,
                NewPartId = r.Key.NewPartId,
                NewPartCode = r.Key.NewPartCode,
                NewPartName = r.Key.NewPartName,
                Quantity = r.Sum(v => v.Quantity),
                UsableQuantity = r.Sum(v => v.UsableQuantity)
            });
            var resultPart2 = partsstock.GroupBy(r => new {
                r.WarehouseId,
                r.ReplacementType,
                r.NewPartId,
                r.NewPartCode,
                r.NewPartName
            }).Select(r => new WarehousePartsStock {
                WarehouseId = r.Key.WarehouseId,
                ReplacementType = r.Key.ReplacementType,
                NewPartId = r.Key.NewPartId,
                NewPartCode = r.Key.NewPartCode,
                NewPartName = r.Key.NewPartName,
                Quantity = r.Sum(v => v.Quantity),
                UsableQuantity = r.Sum(v => v.UsableQuantity)
            });

            return resultPart1.Union(resultPart2).OrderBy(r => r.NewPartId);
        }

        public IQueryable<WarehousePartsStock> 查询替互换件库存销售审核(int partsSalesCategoryId, int warehouseId, int partId) {

            var partsStockGroupQuery = from g in
                                           (from a in ObjectContext.PartsStocks
                                            join b in ObjectContext.WarehouseAreaCategories.Where(r => r.Category == (int)DcsAreaType.保管区) on a.WarehouseAreaCategoryId equals b.Id
                                            select new {
                                                a.PartId,
                                                a.BranchId,
                                                a.StorageCompanyId,
                                                a.StorageCompanyType,
                                                b.Category,
                                                a.WarehouseId,
                                                a.Quantity

                                            })
                                       group g by new {
                                           g.PartId,
                                           g.BranchId,
                                           g.StorageCompanyId,
                                           g.StorageCompanyType,
                                           g.WarehouseId,
                                           g.Category

                                       }
                                           into tmp
                                           select new {
                                               tmp.Key.PartId,

                                               tmp.Key.BranchId,
                                               tmp.Key.StorageCompanyId,
                                               tmp.Key.StorageCompanyType,
                                               tmp.Key.WarehouseId,
                                               tmp.Key.Category,
                                               Quantity = (int?)tmp.Sum(r => r.Quantity)
                                           };
            var warehousePartsStock = (from b in ObjectContext.PartsReplacements.Where(r => r.OldPartId == partId && r.Status == (int)DcsBaseDataStatus.有效 && r.IsSPM==true)
                                       join c in ObjectContext.SpareParts on b.NewPartId equals c.Id
                                       join d in partsStockGroupQuery.Where(r => r.WarehouseId == warehouseId && r.Category == (int)DcsAreaType.保管区) on c.Id equals d.PartId into tempTable1
                                       from t1 in tempTable1.DefaultIfEmpty()
                                       join e in ObjectContext.PartsLockedStocks.Where(r => r.WarehouseId == warehouseId) on c.Id equals e.PartId into tempTable2
                                       from t2 in tempTable2.DefaultIfEmpty()
                                       join g in ObjectContext.PartsBranches.Where(r => r.IsSalable.HasValue && r.IsSalable.Value && r.PartsSalesCategoryId == partsSalesCategoryId) on b.NewPartId equals g.PartId
                                       join h in ObjectContext.PartsSalesPrices.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId) on b.NewPartId equals h.SparePartId
                                       where h.SalesPrice != 0 && c.Status == (int)DcsBaseDataStatus.有效
                                       select new {
                                           WarehouseId = warehouseId,
                                           ReplacementType = (int)DcsPartsReplacementReplacementType.替换,
                                           Quantity = (t1.Quantity ?? 0),
                                           b.NewPartId,
                                           b.NewPartCode,
                                           b.NewPartName,
                                           UsableQuantity = (t1.Quantity ?? 0) - (t2 == null ? 0 : t2.LockedQuantity)
                                       }).Distinct();
            var sparePart = ObjectContext.SpareParts.Where(r => r.Id == partId).FirstOrDefault();
            if(sparePart == null) {
                return null;
            }
            var partsExchangeExchangeCodes = ObjectContext.PartsExchanges.FirstOrDefault(v => v.PartId == sparePart.Id && v.Status == (int)DcsBaseDataStatus.有效);
            if(partsExchangeExchangeCodes == null) {
                var resultPart3 = warehousePartsStock.GroupBy(r => new {
                    r.WarehouseId,
                    r.ReplacementType,
                    r.NewPartId,
                    r.NewPartCode,
                    r.NewPartName
                }).Select(r => new WarehousePartsStock {
                    WarehouseId = r.Key.WarehouseId,
                    ReplacementType = r.Key.ReplacementType,
                    NewPartId = r.Key.NewPartId,
                    NewPartCode = r.Key.NewPartCode,
                    NewPartName = r.Key.NewPartName,
                    Quantity = r.Sum(v => v.Quantity),
                    UsableQuantity = r.Sum(v => v.UsableQuantity)
                });
                return resultPart3.OrderBy(r => r.NewPartId);
            }
            var partsExchangeGroup = ObjectContext.PartsExchangeGroups.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && partsExchangeExchangeCodes.ExchangeCode == r.ExchangeCode);
            //if(!partsExchangeGroup.Any()) {
            //    return null;
            //}
            var partsExchangeGroupExchangeCode = ObjectContext.PartsExchangeGroups.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && partsExchangeGroup.Select(v => v.ExGroupCode).Contains(r.ExGroupCode));
            var partsstock = (//from b in ObjectContext.PartsExchangeGroups.Where(r => r.ExGroupCode == partsExchangeGroup.ExGroupCode && r.Status == (int)DcsBaseDataStatus.有效)
                              from c in ObjectContext.PartsExchanges.Where(r => r.PartId != partId && r.Status == (int)DcsBaseDataStatus.有效 && (partsExchangeGroupExchangeCode.Select(v => v.ExchangeCode).Contains(r.ExchangeCode) || partsExchangeExchangeCodes.ExchangeCode == r.ExchangeCode))// on b.ExchangeCode equals c.ExchangeCode
                              join d in ObjectContext.SpareParts on c.PartId equals d.Id
                              join b in ObjectContext.PartsExchangeGroups.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on c.ExchangeCode equals b.ExchangeCode into tempTable3
                              from t3 in tempTable3.DefaultIfEmpty()
                              join e in partsStockGroupQuery.Where(r => r.WarehouseId == warehouseId && r.Category == (int)DcsAreaType.保管区) on d.Id equals e.PartId into tempTable1
                              from t1 in tempTable1.DefaultIfEmpty()
                              join f in ObjectContext.PartsLockedStocks.Where(r => r.WarehouseId == warehouseId) on d.Id equals f.PartId into tempTable2
                              from t2 in tempTable2.DefaultIfEmpty()
                              join g in ObjectContext.PartsBranches.Where(r => r.IsSalable.HasValue && r.IsSalable.Value && r.PartsSalesCategoryId == partsSalesCategoryId) on c.PartId equals g.PartId
                              join h in ObjectContext.PartsSalesPrices.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId) on c.PartId equals h.SparePartId
                              where h.SalesPrice != 0 && c.Status == (int)DcsBaseDataStatus.有效
                              select new {
                                  WarehouseId = warehouseId,
                                  ReplacementType = (int)DcsPartsReplacementReplacementType.互换,
                                  UsableQuantity = (t1.Quantity ?? 0) - (t2 == null ? 0 : t2.LockedQuantity),
                                  NewPartId = c.PartId,
                                  NewPartCode = d.Code,
                                  NewPartName = d.Name,
                                  Quantity = (t1.Quantity ?? 0)
                              }).Distinct();
            var resultPart1 = warehousePartsStock.GroupBy(r => new {
                r.WarehouseId,
                r.ReplacementType,
                r.NewPartId,
                r.NewPartCode,
                r.NewPartName
            }).Select(r => new WarehousePartsStock {
                WarehouseId = r.Key.WarehouseId,
                ReplacementType = r.Key.ReplacementType,
                NewPartId = r.Key.NewPartId,
                NewPartCode = r.Key.NewPartCode,
                NewPartName = r.Key.NewPartName,
                Quantity = r.Sum(v => v.Quantity),
                UsableQuantity = r.Sum(v => v.UsableQuantity)
            });
            var resultPart2 = partsstock.GroupBy(r => new {
                r.WarehouseId,
                r.ReplacementType,
                r.NewPartId,
                r.NewPartCode,
                r.NewPartName
            }).Select(r => new WarehousePartsStock {
                WarehouseId = r.Key.WarehouseId,
                ReplacementType = r.Key.ReplacementType,
                NewPartId = r.Key.NewPartId,
                NewPartCode = r.Key.NewPartCode,
                NewPartName = r.Key.NewPartName,
                Quantity = r.Sum(v => v.Quantity),
                UsableQuantity = r.Sum(v => v.UsableQuantity)
            });

            return resultPart1.Union(resultPart2).OrderBy(r => r.NewPartId);
        }

        public IQueryable<WarehousePartsStock> 查询经销商替互换件库存(int partsSalesCategoryId, int dealerId, int partId) {
            var warehousePartsStock = from a in ObjectContext.DealerPartsStocks.Where(r => r.DealerId == dealerId && r.SalesCategoryId == partsSalesCategoryId)
                                      from b in ObjectContext.PartsReplacements.Where(t=>t.Status==(int)DcsBaseDataStatus.有效 && t.IsSPM==true)
                                      join c in ObjectContext.PartsBranches.Where(r => r.IsSalable.HasValue && r.IsSalable.Value && r.PartsSalesCategoryId == partsSalesCategoryId) on b.NewPartId equals c.PartId
                                      join d in ObjectContext.PartsSalesPrices.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId) on b.NewPartId equals d.SparePartId
                                      where a.SparePartId == b.NewPartId && b.OldPartId == partId
                                      select new WarehousePartsStock {
                                          ReplacementType = (int)DcsPartsReplacementReplacementType.替换,
                                          Quantity = a.Quantity,
                                          NewPartId = b.NewPartId,
                                          NewPartCode = b.NewPartCode,
                                          NewPartName = b.NewPartName
                                      };
            var sparePart = ObjectContext.SpareParts.Where(r => r.Id == partId).FirstOrDefault();
            if(sparePart == null) {
                return null;
            }
            var partsExchangeExchangeCodes = ObjectContext.PartsExchanges.FirstOrDefault(v => v.PartId == sparePart.Id && v.Status == (int)DcsBaseDataStatus.有效);
            if(partsExchangeExchangeCodes == null) {
                var resultPart3 = warehousePartsStock.GroupBy(r => new {
                    r.WarehouseId,
                    r.ReplacementType,
                    r.NewPartId,
                    r.NewPartCode,
                    r.NewPartName
                }).Select(r => new WarehousePartsStock {
                    WarehouseId = r.Key.WarehouseId,
                    ReplacementType = r.Key.ReplacementType,
                    NewPartId = r.Key.NewPartId,
                    NewPartCode = r.Key.NewPartCode,
                    NewPartName = r.Key.NewPartName,
                    Quantity = r.Sum(v => v.Quantity),
                    UsableQuantity = r.Sum(v => v.UsableQuantity)
                });
                return resultPart3.OrderBy(r => r.NewPartId);
            }
            var partsExchangeGroup = ObjectContext.PartsExchangeGroups.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && partsExchangeExchangeCodes.ExchangeCode == r.ExchangeCode);
            //if(!partsExchangeGroup.Any()) {
            //    return null;
            //}
            var partsExchangeGroupExchangeCode = ObjectContext.PartsExchangeGroups.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && partsExchangeGroup.Select(v => v.ExGroupCode).Contains(r.ExGroupCode));
            var partstock = from c in ObjectContext.PartsExchanges.Where(r => r.PartId != partId && r.Status == (int)DcsBaseDataStatus.有效 && (partsExchangeGroupExchangeCode.Select(v => v.ExchangeCode).Contains(r.ExchangeCode) || partsExchangeExchangeCodes.ExchangeCode == r.ExchangeCode))
                            from d in ObjectContext.SpareParts
                            from b in ObjectContext.PartsExchangeGroups//.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.ExchangeCode == sparePart.ExchangeIdentification)
                            from a in ObjectContext.DealerPartsStocks
                            join e in ObjectContext.PartsBranches.Where(r => r.IsSalable.HasValue && r.IsSalable.Value && r.PartsSalesCategoryId == partsSalesCategoryId) on c.PartId equals e.PartId
                            join f in ObjectContext.PartsSalesPrices.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId) on c.PartId equals f.SparePartId
                            where a.SparePartId == c.PartId && a.SalesCategoryId == partsSalesCategoryId && a.DealerId == dealerId && c.ExchangeCode == b.ExchangeCode && c.PartId != partId && d.Id == c.PartId && b.Status == (int)DcsBaseDataStatus.有效 && c.Status == (int)DcsBaseDataStatus.有效
                            select new WarehousePartsStock {
                                ReplacementType = (int)DcsPartsReplacementReplacementType.互换,
                                Quantity = a.Quantity,
                                NewPartId = c.PartId,
                                NewPartCode = d.Code,
                                NewPartName = d.Name
                            };
            return warehousePartsStock.Union(partstock).OrderBy(r => r.NewPartId);

        }

        public IEnumerable<WarehousePartsStock> 采购退货查询仓库库存(int storagecompanyId, int? warehouseId, int? partsSaleCategoryId, string partCode, string partName, int supplierId) {
            //仓储企业Id，仓库Id，配件销售类型Id, 数组[配件Id] ,时间 ,供应商Id
            var nowTime = DateTime.Now;
            IQueryable<SparePart> tempSparePart = ObjectContext.SpareParts;
            if(partCode != null) {
                tempSparePart = tempSparePart.Where(r => r.Code.Contains(partCode));
            }
            if(partName != null) {
                tempSparePart = tempSparePart.Where(r => r.Name.Contains(partName));
            }
            var partsStockGroups = from partstock in ObjectContext.PartsStocks
                                   from warehouseAreaCategory in ObjectContext.WarehouseAreaCategories
                                   from sparePart in tempSparePart
                                   where (warehouseAreaCategory.Category == (int)DcsAreaType.检验区 || warehouseAreaCategory.Category == (int)DcsAreaType.保管区) && partstock.StorageCompanyId == storagecompanyId && (!warehouseId.HasValue || partstock.WarehouseId == warehouseId) && partstock.PartId == sparePart.Id
                                   && partstock.WarehouseAreaCategoryId == warehouseAreaCategory.Id
                                   group partstock by new {
                                       partstock.StorageCompanyId,
                                       partstock.WarehouseId,
                                       partstock.StorageCompanyType,
                                       partstock.BranchId,
                                       partstock.PartId
                                   }
                                       into tempTablePartStrock
                                       select new {
                                           tempTablePartStrock.Key.StorageCompanyId,
                                           tempTablePartStrock.Key.WarehouseId,
                                           tempTablePartStrock.Key.StorageCompanyType,
                                           tempTablePartStrock.Key.BranchId,
                                           tempTablePartStrock.Key.PartId,
                                           allQuantity = tempTablePartStrock.Sum(r => r.Quantity)
                                       };
            var wmsCongelationStockViewGroups = from wmsCongelationStockView in ObjectContext.WmsCongelationStockViews //WMS仓库配件冻结库存（视图）
                                                where wmsCongelationStockView.StorageCompanyId == storagecompanyId && (!warehouseId.HasValue || wmsCongelationStockView.WarehouseId == warehouseId)
                                                group wmsCongelationStockView by new {
                                                    wmsCongelationStockView.StorageCompanyId,
                                                    wmsCongelationStockView.WarehouseId,
                                                    wmsCongelationStockView.BranchId,
                                                    wmsCongelationStockView.SparePartId
                                                }
                                                    into tempWmsCongelationStockViews
                                                    select new {
                                                        tempWmsCongelationStockViews.Key.StorageCompanyId,
                                                        tempWmsCongelationStockViews.Key.WarehouseId,
                                                        tempWmsCongelationStockViews.Key.BranchId,
                                                        tempWmsCongelationStockViews.Key.SparePartId,
                                                        allDisabledStock = tempWmsCongelationStockViews.Sum(r => r.DisabledStock),
                                                        allCongelationStockQty = tempWmsCongelationStockViews.Sum(r => r.CongelationStockQty)
                                                    };
            var partsLockedStockGroups = from partsLockedStock in ObjectContext.PartsLockedStocks
                                         where partsLockedStock.StorageCompanyId == storagecompanyId && (!warehouseId.HasValue || partsLockedStock.WarehouseId == warehouseId)
                                         group partsLockedStock by new {
                                             partsLockedStock.StorageCompanyId,
                                             partsLockedStock.WarehouseId,
                                             partsLockedStock.BranchId,
                                             partsLockedStock.PartId
                                         }
                                             into tempPartsLockedStocks
                                             select new {
                                                 tempPartsLockedStocks.Key.StorageCompanyId,
                                                 tempPartsLockedStocks.Key.WarehouseId,
                                                 tempPartsLockedStocks.Key.BranchId,
                                                 SparePartId = tempPartsLockedStocks.Key.PartId,
                                                 allLockedQuantity = tempPartsLockedStocks.Sum(r => r.LockedQuantity),
                                             };
            var tempResultQuery = from partsStockGroup in partsStockGroups
                                  join wmsCongelationStockViewGroup in wmsCongelationStockViewGroups on new {
                                      partId = partsStockGroup.PartId,
                                      warehouseId = partsStockGroup.WarehouseId
                                  } equals new {
                                      partId = wmsCongelationStockViewGroup.SparePartId,
                                      warehouseId = wmsCongelationStockViewGroup.WarehouseId
                                  } into tempTableWmsCongelationStockViewGroups
                                  from a in tempTableWmsCongelationStockViewGroups.DefaultIfEmpty()
                                  join partsLockedStockGroup in partsLockedStockGroups // 配件锁定库存 
                                      on new {
                                          partId = partsStockGroup.PartId,
                                          warehouseId = partsStockGroup.WarehouseId
                                      } equals new {
                                          partId = partsLockedStockGroup.SparePartId,
                                          warehouseId = partsLockedStockGroup.WarehouseId
                                      } into tempTablePartsLockedStockGroups
                                  from b in tempTablePartsLockedStockGroups.DefaultIfEmpty()
                                  join sparePart in ObjectContext.SpareParts.Where(r => r.Status == (int)DcsMasterDataStatus.有效) on partsStockGroup.PartId equals sparePart.Id into tempSpareParts
                                  from c in tempSpareParts.DefaultIfEmpty()
                                  join branch in ObjectContext.Branches.Where(r => r.Status == (int)DcsMasterDataStatus.有效) on partsStockGroup.BranchId equals branch.Id into tempBranches
                                  from d in tempBranches.DefaultIfEmpty()
                                  join partsPurchasePricing in ObjectContext.PartsPurchasePricings.Where(r => r.Status == (int)DcsPartsPurchasePricingStatus.生效 && r.ValidFrom <= nowTime && r.ValidTo >= nowTime && r.PartsSupplierId == supplierId && (partsSaleCategoryId == null || r.PartsSalesCategoryId == partsSaleCategoryId)) on partsStockGroup.PartId equals partsPurchasePricing.PartId into tempPartsPurchasePricings
                                  from e in tempPartsPurchasePricings.DefaultIfEmpty()
                                  join warehouse in ObjectContext.Warehouses.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on partsStockGroup.WarehouseId equals warehouse.Id into tempWarehouses
                                  from f in tempWarehouses
                                  join company in ObjectContext.Companies.Where(r => r.Status == (int)DcsMasterDataStatus.有效) on partsStockGroup.StorageCompanyId equals company.Id into tempCompanies
                                  from g in tempCompanies.DefaultIfEmpty()
                                  select new {
                                      partsStockGroup.WarehouseId,
                                      WarehouseCode = f.Code,
                                      WarehouseName = f.Name,
                                      partsStockGroup.StorageCompanyId,
                                      StorageCompanyCode = g.Code,
                                      StorageCompanyName = g.Name,
                                      partsStockGroup.BranchId,
                                      BranchCode = d.Code,
                                      BranchName = d.Name,
                                      SparePartId = partsStockGroup.PartId,
                                      SparePartCode = c.Code,
                                      SparePartName = c.Name,
                                      SupplierId = e.PartsSupplierId,
                                      partsStockGroup.StorageCompanyType,
                                      Quantity = (int?)partsStockGroup.allQuantity,
                                      UsableQuantity = (int?)partsStockGroup.allQuantity,
                                      AllDisabledStock = (int?)a.allDisabledStock,
                                      AllCongelationStockQty = (int?)a.allCongelationStockQty,
                                      AllLockedQuantity = (int?)b.allLockedQuantity,
                                      PartsPurchasePricing = (decimal?)e.PurchasePrice
                                  };
            var result = tempResultQuery.Select(r => new WarehousePartsStock {
                WarehouseId = r.WarehouseId,
                WarehouseCode = r.WarehouseCode,
                WarehouseName = r.WarehouseName,
                StorageCompanyId = r.StorageCompanyId,
                StorageCompanyCode = r.StorageCompanyCode,
                StorageCompanyName = r.StorageCompanyName,
                SupplierId = r.SupplierId,
                BranchId = r.BranchId,
                BranchCode = r.BranchCode,
                BranchName = r.BranchName,
                SparePartId = r.SparePartId,
                SparePartCode = r.SparePartCode,
                SparePartName = r.SparePartName,
                StorageCompanyType = r.StorageCompanyType,
                Quantity = r.Quantity ?? 0,
                UsableQuantity = (r.Quantity ?? 0) - (r.AllDisabledStock ?? 0) - (r.AllCongelationStockQty ?? 0) - (r.AllLockedQuantity ?? 0),
                PartsPurchasePricing = r.PartsPurchasePricing ?? 0
            });


            return result.ToArray().OrderBy(r => r.WarehouseId);



        }

        [Query(HasSideEffects = true)]
        public IEnumerable<WarehousePartsStock> 调拨查询仓库库存(int warehouseId) {
            var userId = Utils.GetCurrentUserInfo();
            var spareParts = ObjectContext.PartsBranches.Where(r => ObjectContext.SalesUnits.Where(v => ObjectContext.SalesUnitAffiWarehouses.Any(p => p.WarehouseId == warehouseId && p.SalesUnitId == v.Id)).Any(t => t.PartsSalesCategoryId == r.PartsSalesCategoryId));
            #region  查询仓库库存
            var partsStocks = ObjectContext.PartsStocks.Where(r => r.StorageCompanyId == userId.EnterpriseId && ObjectContext.WarehouseAreaCategories.Any(v => v.Id == r.WarehouseAreaCategoryId && (v.Category == (int)DcsAreaType.保管区)) && r.WarehouseId == warehouseId && spareParts.Any(v => v.PartId == r.PartId));
            var returnpartsStocks = partsStocks.GroupBy(r => new {
                r.WarehouseId,
                r.StorageCompanyId,
                r.StorageCompanyType,
                r.BranchId,
                r.PartId
            }).Select(v => new {
                v.Key.WarehouseId,
                v.Key.StorageCompanyId,
                v.Key.StorageCompanyType,
                v.Key.BranchId,
                v.Key.PartId,
                sumQuantity = v.Sum(r => r.Quantity)
            });
            var retrunpartsStocks = from partsStock in returnpartsStocks
                                    from sparePart in ObjectContext.SpareParts
                                    from branch in ObjectContext.Branches
                                    from wrehouse in ObjectContext.Warehouses
                                    where sparePart.Id == partsStock.PartId && sparePart.Status != (int)DcsMasterDataStatus.作废 && branch.Id == partsStock.BranchId && branch.Status != (int)DcsBaseDataStatus.作废 && wrehouse.Id == partsStock.WarehouseId && wrehouse.Status != (int)DcsBaseDataStatus.作废
                                    select new {
                                        partsStock.WarehouseId,
                                        WarehouseCode = wrehouse.Code,
                                        WarehouseName = wrehouse.Name,
                                        SparePartId = partsStock.PartId,
                                        SparePartCode = sparePart.Code,
                                        SparePartName = sparePart.Name,
                                        sparePart.MeasureUnit,
                                        partsStock.StorageCompanyId,
                                        partsStock.StorageCompanyType,
                                        partsStock.BranchId,
                                        BranchCode = branch.Code,
                                        BranchName = branch.Name,
                                        Quantity = partsStock.sumQuantity
                                    };
            var stockrestuls = from partsStock in retrunpartsStocks
                               join partsLockedStock in ObjectContext.PartsLockedStocks on new {
                                   pId = partsStock.SparePartId,
                                   cId = partsStock.StorageCompanyId,
                                   wId = partsStock.WarehouseId
                               } equals new {
                                   pId = partsLockedStock.PartId,
                                   cId = partsLockedStock.StorageCompanyId,
                                   wId = partsLockedStock.WarehouseId
                               } into details
                               from v in details.DefaultIfEmpty()
                               join bottomStock in ObjectContext.BottomStocks.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on new {
                                   pId = partsStock.SparePartId,
                                   cId = partsStock.StorageCompanyId,
                                   wId = partsStock.WarehouseId
                               } equals new {
                                   pId = bottomStock.SparePartId,
                                   cId = bottomStock.CompanyID,
                                   wId = (int)bottomStock.WarehouseID
                               } into bs
                               from m in bs.DefaultIfEmpty()
                               select new {
                                   partsStock.WarehouseId,
                                   partsStock.WarehouseCode,
                                   partsStock.WarehouseName,
                                   partsStock.SparePartId,
                                   partsStock.SparePartCode,
                                   partsStock.SparePartName,
                                   partsStock.MeasureUnit,
                                   partsStock.StorageCompanyId,
                                   partsStock.StorageCompanyType,
                                   partsStock.BranchId,
                                   partsStock.BranchCode,
                                   partsStock.BranchName,
                                   partsStock.Quantity,
                                   UsableQuantity = partsStock.Quantity - (v == null ? 0 : v.LockedQuantity) - (m == null ? 0 : m.StockQty ?? 0),
                               };

            var wmsresults = from stockrestul in stockrestuls
                             join wmsCongelationStockView in ObjectContext.WmsCongelationStockViews on new {
                                 pId = stockrestul.SparePartId,
                                 wId = stockrestul.WarehouseId
                             } equals new {
                                 pId = wmsCongelationStockView.SparePartId,
                                 wId = wmsCongelationStockView.WarehouseId
                             } into details
                             from v in details.DefaultIfEmpty()
                             select new {
                                 stockrestul.WarehouseId,
                                 stockrestul.WarehouseCode,
                                 stockrestul.WarehouseName,
                                 stockrestul.SparePartId,
                                 stockrestul.SparePartCode,
                                 stockrestul.SparePartName,
                                 stockrestul.MeasureUnit,
                                 stockrestul.StorageCompanyId,
                                 stockrestul.StorageCompanyType,
                                 stockrestul.BranchId,
                                 stockrestul.BranchCode,
                                 stockrestul.BranchName,
                                 stockrestul.Quantity,
                                 UsableQuantity = stockrestul.UsableQuantity - (v == null ? 0 : v.CongelationStockQty) - (v == null ? 0 : v.DisabledStock)
                             };

            //调拨取消销售价，改为配件成本价
            var tempresult = from wmsresult in wmsresults
                             from enterprisePartsCost in ObjectContext.EnterprisePartsCosts
                             where wmsresult.SparePartId == enterprisePartsCost.SparePartId && enterprisePartsCost.OwnerCompanyId == wmsresult.StorageCompanyId 
                             select new {
                                 wmsresult.WarehouseId,
                                 wmsresult.WarehouseCode,
                                 wmsresult.WarehouseName,
                                 wmsresult.SparePartId,
                                 wmsresult.SparePartCode,
                                 wmsresult.SparePartName,
                                 wmsresult.MeasureUnit,
                                 wmsresult.StorageCompanyId,
                                 wmsresult.StorageCompanyType,
                                 wmsresult.BranchId,
                                 wmsresult.BranchCode,
                                 wmsresult.BranchName,
                                 wmsresult.Quantity,
                                 wmsresult.UsableQuantity,
                                 PartsSalesPrice = enterprisePartsCost.CostPrice,
                                 PartsPlannedPrice = enterprisePartsCost.CostPrice,
                                 enterprisePartsCost.PartsSalesCategoryId
                             };
            var result = from t in tempresult
                         join a in ObjectContext.PartsRetailGuidePrices on new {
                             t.SparePartId,
                             t.PartsSalesCategoryId
                         } equals new {
                             a.SparePartId,
                             a.PartsSalesCategoryId
                         } into tempTable
                         from t1 in tempTable.DefaultIfEmpty()
                         select new {
                             t.WarehouseId,
                             t.WarehouseCode,
                             t.WarehouseName,
                             t.SparePartId,
                             t.SparePartCode,
                             t.SparePartName,
                             t.MeasureUnit,
                             t.StorageCompanyId,
                             t.StorageCompanyType,
                             t.BranchId,
                             t.BranchCode,
                             t.BranchName,
                             t.Quantity,
                             t.UsableQuantity,
                             t.PartsSalesPrice,//配件成本价
                             t.PartsPlannedPrice,//配件成本价
                             t.PartsSalesCategoryId,
                             t1.RetailGuidePrice
                         };
            #endregion
            var results = from a in spareParts
                          from c in ObjectContext.Warehouses.Where(r => r.Id == warehouseId)
                          from e in ObjectContext.SpareParts.Where(r => r.Id == a.PartId)
                          join t in result on a.PartId equals t.SparePartId
                          join d in ObjectContext.EnterprisePartsCosts on new {
                              OwnerCompanyId = c.StorageCompanyId,
                              SparePartId = a.PartId,
                              a.PartsSalesCategoryId
                          } equals new {
                              d.OwnerCompanyId,
                              d.SparePartId,
                              d.PartsSalesCategoryId
                          } into tmp3
                          from t2 in tmp3.DefaultIfEmpty()
                          select new WarehousePartsStock {
                              WarehouseId = c.Id,
                              WarehouseCode = c.Code,
                              WarehouseName = c.Name,
                              SparePartId = a.PartId,
                              SparePartCode = a.PartCode,
                              SparePartName = a.PartName,
                              UsableQuantity = t.SparePartId != 0 ? t.UsableQuantity : 0,
                              PartsSalesPrice = t.PartsSalesPrice,//配件成本价
                              PartsPlannedPrice = t2.CostPrice,//20180727 计划价改为取配件成本价
                              PartsSalesCategoryId = a.PartsSalesCategoryId,
                              PartsSalesCategoryName = a.PartsSalesCategoryName,
                              MInPackingAmount = e.MInPackingAmount
                          };
            return results.Distinct().OrderBy(r => r.WarehouseId);
        }
        [Query(HasSideEffects = true)]
        public IEnumerable<WarehousePartsStock> 查询仓库库存领出(int storageCompanyId, int? warehouseId, int[] partIds)
        {
            var partsStocks = ObjectContext.PartsStocks.Where(r => r.StorageCompanyId == storageCompanyId && ObjectContext.WarehouseAreaCategories.Any(v => v.Id == r.WarehouseAreaCategoryId && (v.Category == (int)DcsAreaType.保管区)));
            if (warehouseId.HasValue)
                partsStocks = partsStocks.Where(r => r.WarehouseId == warehouseId.Value);
            if (partIds != null && partIds.Length > 0)
                partsStocks = partsStocks.Where(r => partIds.Contains(r.PartId));
            var returnpartsStocks = partsStocks.GroupBy(r => new
            {
                r.WarehouseId,
                r.StorageCompanyId,
                r.BranchId,
                r.PartId
            }).Select(v => new
            {
                v.Key.WarehouseId,
                v.Key.StorageCompanyId,
                v.Key.BranchId,
                v.Key.PartId,
                sumQuantity = v.Sum(r => r.Quantity)
            });
            var retrunpartsStocks = from partsStock in returnpartsStocks
                                    from sparePart in ObjectContext.SpareParts
                                    from branch in ObjectContext.Branches
                                    from wrehouse in ObjectContext.Warehouses
                                    where sparePart.Id == partsStock.PartId && sparePart.Status != (int)DcsMasterDataStatus.作废 && branch.Id == partsStock.BranchId && branch.Status != (int)DcsBaseDataStatus.作废 && wrehouse.Id == partsStock.WarehouseId && wrehouse.Status != (int)DcsBaseDataStatus.作废
                                    select new
                                    {
                                        partsStock.WarehouseId,
                                        WarehouseCode = wrehouse.Code,
                                        WarehouseName = wrehouse.Name,
                                        SparePartId = partsStock.PartId,
                                        SparePartCode = sparePart.Code,
                                        SparePartName = sparePart.Name,
                                        sparePart.MeasureUnit,
                                        partsStock.StorageCompanyId,
                                        partsStock.BranchId,
                                        BranchCode = branch.Code,
                                        BranchName = branch.Name,
                                        Quantity = partsStock.sumQuantity
                                    };
            var stockrestuls = from partsStock in retrunpartsStocks
                               join partsLockedStock in ObjectContext.PartsLockedStocks on new
                               {
                                   pId = partsStock.SparePartId,
                                   cId = partsStock.StorageCompanyId,
                                   wId = partsStock.WarehouseId
                               } equals new
                               {
                                   pId = partsLockedStock.PartId,
                                   cId = partsLockedStock.StorageCompanyId,
                                   wId = partsLockedStock.WarehouseId
                               } into details
                               from v in details.DefaultIfEmpty()
                               select new
                               {
                                   partsStock.WarehouseId,
                                   partsStock.WarehouseCode,
                                   partsStock.WarehouseName,
                                   partsStock.SparePartId,
                                   partsStock.SparePartCode,
                                   partsStock.SparePartName,
                                   partsStock.MeasureUnit,
                                   partsStock.StorageCompanyId,
                                   partsStock.BranchId,
                                   partsStock.BranchCode,
                                   partsStock.BranchName,
                                   partsStock.Quantity,
                                   UsableQuantity = partsStock.Quantity - (v == null ? 0 : v.LockedQuantity),
                               };

            var tempresult = from  wmsresult in stockrestuls
                             from salesUnit in ObjectContext.SalesUnits
                             from salesUnitAffiWarehouse in ObjectContext.SalesUnitAffiWarehouses
                             from partsCost in ObjectContext.EnterprisePartsCosts
                             where salesUnitAffiWarehouse.WarehouseId == wmsresult.WarehouseId && salesUnitAffiWarehouse.SalesUnitId == salesUnit.Id
                              && wmsresult.SparePartId == partsCost.SparePartId && salesUnit.PartsSalesCategoryId == partsCost.PartsSalesCategoryId
                              && partsCost.OwnerCompanyId == storageCompanyId
                             select new
                             {
                                 wmsresult.WarehouseId,
                                 wmsresult.WarehouseCode,
                                 wmsresult.WarehouseName,
                                 wmsresult.SparePartId,
                                 wmsresult.SparePartCode,
                                 wmsresult.SparePartName,
                                 wmsresult.MeasureUnit,
                                 wmsresult.StorageCompanyId,
                                 wmsresult.BranchId,
                                 wmsresult.BranchCode,
                                 wmsresult.BranchName,
                                 wmsresult.Quantity,
                                 wmsresult.UsableQuantity,
                                 salesUnit.PartsSalesCategoryId,
                                 partsCost.CostPrice
                             };
            var result = from t in tempresult
                         join a in ObjectContext.PartsRetailGuidePrices on new
                         {
                             t.SparePartId,
                             t.PartsSalesCategoryId
                         } equals new
                         {
                             a.SparePartId,
                             a.PartsSalesCategoryId
                         } into tempTable
                         from t1 in tempTable.DefaultIfEmpty()
                         select new WarehousePartsStock
                         {
                             WarehouseId = t.WarehouseId,
                             WarehouseCode = t.WarehouseCode,
                             WarehouseName = t.WarehouseName,
                             SparePartId = t.SparePartId,
                             SparePartCode = t.SparePartCode,
                             SparePartName = t.SparePartName,
                             MeasureUnit = t.MeasureUnit,
                             StorageCompanyId = t.StorageCompanyId,
                             BranchId = t.BranchId,
                             BranchCode = t.BranchCode,
                             BranchName = t.BranchName,
                             Quantity = t.Quantity,
                             UsableQuantity = t.UsableQuantity,
                             PartsSalesCategoryId = t.PartsSalesCategoryId,
                             PartsSalesCategoryName = null,
                             PartsRetailGuidePrice = t1.RetailGuidePrice,
                             CostPrice=t.CostPrice
                         };
            return result.OrderBy(r => r.WarehouseId);
        }
        public IEnumerable<WarehousePartsStock> 配件仓库库存查询(int storageCompanyId, string[] sparePartCodes, string sparePartName, int? warehouseId, int? storageCenter, string referenceCode)
        {
            string SQL = @"
select warehouseid, warehousecode ,warehousename ,categoryid,Categoryname , storagecenter , SparePartId , SparePartCode,SparePartName ,referencecode ,retailguideprice ,
       storagecompanyid, branchid, branchcode ,branchname ,totalqty ,dfqty,actualqty , (nvl(actualqty,0) - nvl(orderlockqty,0)) as   ActualAvailableStock ,orderlockqty ,
       (nvl(cgqty,0) - nvl(orderlockqty,0)) as cgqty ,inlockqty ,(nvl(cgqty,0) - nvl(inlockqty,0) - nvl(actualqty,0)) as ShelfQty,     (case when StockQty is null then  0 when (nvl(actualqty, 0) - nvl(orderlockqty, 0)) > nvl(StockQty, 0) then  nvl(StockQty, 0)  else  (nvl(actualqty, 0) - nvl(orderlockqty, 0)) end) as StockQty
       from(select warehouseid,warehousecode,warehousename, categoryid, Categoryname,storagecenter,SparePartId,SparePartCode, SparePartName, referencecode, retailguideprice,
       storagecompanyid, branchid, branchcode, branchname,zmqty as totalqty,
       (select sum(tmp.quantity)from partsstock tmp inner join warehouseareacategory b on tmp.warehouseareacategoryid = b.id where tmp.partid =a.SparePartId and tmp.warehouseid = a.warehouseid and b.category = 4) as dfqty,
       (select sum(quantity) from partsstock tmp inner join warehouseareacategory b on tmp.warehouseareacategoryid = b.id  where tmp.partid = a.SparePartId and tmp.warehouseid = a.warehouseid and b.category = 1) actualqty,
       (select sum(lockedquantity) from partslockedstock tmp where tmp.partid = a.SparePartId and tmp.warehouseid = a.warehouseid) as orderlockqty,
       (select sum(quantity) from partsstock tmp inner join warehouseareacategory b   on tmp.warehouseareacategoryid = b.id where tmp.partid = a.SparePartId and tmp.warehouseid = a.warehouseid and (b.category = 1 or b.category = 3)) cgqty,
       (select sum(tmp.lockedqty)   from partsstock tmp  inner join warehouseareacategory b   on tmp.warehouseareacategoryid = b.id  where tmp.partid = a.SparePartId  and tmp.warehouseid = a.warehouseid and b.category = 3) as inlockqty,StockQty
  from (select b.id    as warehouseid,  b.code   warehousecode, b.name  warehousename, pcg.id  as categoryid, pcg.name  Categoryname,
               b.storagecenter,  sp.id as SparePartId,  sp.code  SparePartCode, sp.name  SparePartName, nvl(sp.referencecode,sp.code) as referencecode,  f.retailguideprice,
               b.storagecompanyid, b.branchid,  pcg.branchcode,  pcg.branchname, sum(a.quantity) as zmqty,bs.StockQty
          from partsstock a  inner join warehouse b on a.warehouseid = b.id inner join warehouseareacategory c  on c.id = a.warehouseareacategoryid
         inner join sparepart sp on sp.id = a.partid  left join salesunitaffiwarehouse d  on d.warehouseid = b.id left join salesunit e  on e.id = d.salesunitid  left join partssalescategory pcg
        on pcg.id = e.partssalescategoryid left join partsretailguideprice f  on f.partssalescategoryid = pcg.id and f.sparepartid = a.partid   and f.status = 1
                         left join BottomStock bs on a.PartId = bs.SparePartId and pcg.id = bs.PartsSalesCategoryId and a.WarehouseId=bs.WarehouseID  and b.branchid=bs.CompanyID  and bs.status=1
    where c.category <> 2 and a.StorageCompanyId=" + storageCompanyId;
            if (sparePartCodes != null && sparePartCodes.Length > 0)
            {
                if (sparePartCodes.Length == 1) {
                    SQL = SQL + " and sp.code like '%" + sparePartCodes[0] + "%'";
                } else {
                    for (var i = 0; i < sparePartCodes.Length; i++) {
                        sparePartCodes[i] = "'" + sparePartCodes[i] + "'";
                    }
                    string codes = string.Join(",", sparePartCodes);
                    SQL = SQL + " and sp.code in (" + codes + ")";
                }
            }
            if (!string.IsNullOrEmpty(sparePartName))
            {
                SQL = SQL + " and sp.name like '%" + sparePartName + "%'";
            }
            if (warehouseId.HasValue)
            {
                SQL = SQL + " and a.warehouseId=" + warehouseId.Value;
            }
            if (storageCenter.HasValue)
            {
                SQL = SQL + " and b.StorageCenter=" + storageCenter.Value;
            }
            if(!string.IsNullOrEmpty(referenceCode)) {
                SQL = SQL + " and sp.referenceCode like '%" + referenceCode + "%'";
            }
            SQL = SQL + "group by b.id,  b.code, b.name, pcg.id, pcg.name,   b.storagecenter,  sp.id,   sp.code,  sp.name,  sp.referencecode,   f.retailguideprice,  b.storagecompanyid,  b.branchid, pcg.branchcode,pcg.branchname,bs.StockQty) a)b";
            var search = ObjectContext.ExecuteStoreQuery<WarehousePartsStock>(SQL).ToList();
            return search;
        
        }

    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/5/22 15:23:24
        //原分布类的函数全部转移到Ach                                                               


        [Query(HasSideEffects = true)]
        public IEnumerable<WarehousePartsStock> 查询仓库库存(int storageCompanyId, int? warehouseId, int[] partIds) {
            return new WarehousePartsStockAch(this).查询仓库库存(storageCompanyId, warehouseId, partIds);
        }
        [Query(HasSideEffects = true)]
        public IEnumerable<WarehousePartsStock> 配件调拨查询仓库库存(int storageCompanyId, int? warehouseId, int[] partIds) {
            return new WarehousePartsStockAch(this).配件调拨查询仓库库存(storageCompanyId, warehouseId, partIds);
        }

        [Query(HasSideEffects = true)]
        public IEnumerable<WarehousePartsStock> 查询仓库库存销售审核(int storageCompanyId, int? warehouseId, int[] partIds) {
            return new WarehousePartsStockAch(this).查询仓库库存销售审核(storageCompanyId, warehouseId, partIds);
        }

        [Query(HasSideEffects = true)]
        public IEnumerable<WarehousePartsStock> 只查询仓库库存(int storageCompanyId, int? warehouseId, int[] partIds) {
            return new WarehousePartsStockAch(this).只查询仓库库存(storageCompanyId, warehouseId, partIds);
        }
        [Query(HasSideEffects = true)]
        public IEnumerable<WarehousePartsStock> 只查询仓库库存1(int storageCompanyId, int? warehouseId, int[] partIds) {
            return new WarehousePartsStockAch(this).只查询仓库库存1(storageCompanyId, warehouseId, partIds);
        }

        [Query(HasSideEffects = true)]
        public IEnumerable<WarehousePartsStock> 只查询仓库库存保外(int storageCompanyId, int? warehouseId, int[] partIds) {
            return new WarehousePartsStockAch(this).只查询仓库库存保外(storageCompanyId, warehouseId, partIds);
        }

        public IQueryable<WarehousePartsStock> 查询替互换件库存(int partsSalesCategoryId, int warehouseId, int partId) {
            return new WarehousePartsStockAch(this).查询替互换件库存(partsSalesCategoryId, warehouseId, partId);
        }

        public IQueryable<WarehousePartsStock> 查询替互换件库存销售审核(int partsSalesCategoryId, int warehouseId, int partId) {
            return new WarehousePartsStockAch(this).查询替互换件库存销售审核(partsSalesCategoryId, warehouseId, partId);
        }

        public IQueryable<WarehousePartsStock> 查询经销商替互换件库存(int partsSalesCategoryId, int dealerId, int partId) {
            return new WarehousePartsStockAch(this).查询经销商替互换件库存(partsSalesCategoryId, dealerId, partId);
        }

        public IEnumerable<WarehousePartsStock> 采购退货查询仓库库存(int storagecompanyId, int? warehouseId, int? partsSaleCategoryId, string partCode, string partName, int supplierId) {
            return new WarehousePartsStockAch(this).采购退货查询仓库库存(storagecompanyId, warehouseId, partsSaleCategoryId, partCode, partName, supplierId);
        }


        [Query(HasSideEffects = true)]
        public IEnumerable<WarehousePartsStock> 调拨查询仓库库存(int warehouseId) {
            return new WarehousePartsStockAch(this).调拨查询仓库库存(warehouseId);
        }

        [Query(HasSideEffects = true)]
        public IEnumerable<WarehousePartsStock> 查询仓库库存领出(int storageCompanyId, int? warehouseId, int[] partIds)
        {
            return new WarehousePartsStockAch(this).查询仓库库存领出(storageCompanyId, warehouseId, partIds);
        }
        [Query(HasSideEffects = true)]
        public IEnumerable<WarehousePartsStock> 配件仓库库存查询(int storageCompanyId, string[] sparePartCodes, string sparePartName, int? warehouseId, int? storageCenter, string referenceCode)
        {
            return new WarehousePartsStockAch(this).配件仓库库存查询(storageCompanyId, sparePartCodes, sparePartName,warehouseId,storageCenter, referenceCode);
        }

    }
}
