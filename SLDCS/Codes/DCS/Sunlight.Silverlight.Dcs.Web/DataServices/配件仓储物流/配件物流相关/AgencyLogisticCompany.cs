﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class AgencyLogisticCompanyAch : DcsSerivceAchieveBase {
        public AgencyLogisticCompanyAch(DcsDomainService domainService)
            : base(domainService) {
        }
        internal void InsertLogisticCompanyValidate(AgencyLogisticCompany agencyLogisticCompany) {
            var dbAgencyLogisticCompany = ObjectContext.AgencyLogisticCompanies.Where(r => r.Code == agencyLogisticCompany.Code && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbAgencyLogisticCompany != null)
                throw new ValidationException(string.Format(ErrorStrings.AgencyLogisticCompany_Validation_ExistCode, agencyLogisticCompany.Code));
            var userInfo = Utils.GetCurrentUserInfo();
            agencyLogisticCompany.CreatorId = userInfo.Id;
            agencyLogisticCompany.CreatorName = userInfo.Name;
            agencyLogisticCompany.CreateTime = DateTime.Now;
        }

        internal void UpdateAgencyLogisticCompanyValidate(AgencyLogisticCompany agencyLogisticCompany) {
            var userInfo = Utils.GetCurrentUserInfo();
            agencyLogisticCompany.ModifierId = userInfo.Id;
            agencyLogisticCompany.ModifierName = userInfo.Name;
            agencyLogisticCompany.ModifyTime = DateTime.Now;
        }

        public void InsertAgencyLogisticCompany(AgencyLogisticCompany agencyLogisticCompany) {
            this.InsertToDatabase(agencyLogisticCompany);
            this.InsertLogisticCompanyValidate(agencyLogisticCompany);
        }

        public void UpdateAgencyLogisticCompany(AgencyLogisticCompany agencyLogisticCompany) {
            UpdateToDatabase(agencyLogisticCompany);
            this.UpdateAgencyLogisticCompanyValidate(agencyLogisticCompany);
        }

        public AgencyLogisticCompany GetAgencyLogisticCompanyById(int id) {
            var dbAgencyLogisticCompany = ObjectContext.AgencyLogisticCompanies.SingleOrDefault(e => e.Id == id && e.Status == (int)DcsMasterDataStatus.有效);
            return dbAgencyLogisticCompany;
        }

        public void 作废代理库物流公司信息(AgencyLogisticCompany agencyLogisticCompany) {
            var dbAgencyLogisticCompany = ObjectContext.AgencyLogisticCompanies.Where(r => r.Id == agencyLogisticCompany.Id && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbAgencyLogisticCompany == null)
                throw new ValidationException(ErrorStrings.AgencyLogisticCompany_Validation_OnlyEffectCanAbandone);
            var userinfo = Utils.GetCurrentUserInfo();
            UpdateToDatabase(agencyLogisticCompany);
            agencyLogisticCompany.Status = (int)DcsBaseDataStatus.作废;
            agencyLogisticCompany.AbandonerId = userinfo.Id;
            agencyLogisticCompany.AbandonerName = userinfo.Name;
            agencyLogisticCompany.AbandonerTime = DateTime.Now;
        }
    }

    partial class DcsDomainService {
        public void InsertAgencyLogisticCompany(AgencyLogisticCompany agencyLogisticCompany) {
            new AgencyLogisticCompanyAch(this).InsertAgencyLogisticCompany(agencyLogisticCompany);
        }

        public void UpdateAgencyLogisticCompany(AgencyLogisticCompany agencyLogisticCompany) {
            new AgencyLogisticCompanyAch(this).UpdateAgencyLogisticCompany(agencyLogisticCompany);
        }

        public AgencyLogisticCompany GetAgencyLogisticCompanyById(int id) {
            return new AgencyLogisticCompanyAch(this).GetAgencyLogisticCompanyById(id);
        }

        [Update(UsingCustomMethod = true)]
        public void 作废代理库物流公司信息(AgencyLogisticCompany agencyLogisticCompany) {
            new AgencyLogisticCompanyAch(this).作废代理库物流公司信息(agencyLogisticCompany);
        }
    }
}
