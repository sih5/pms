﻿using System;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Quartz.Xml;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class AgencyPartsShippingOrderAch : DcsSerivceAchieveBase {
        public AgencyPartsShippingOrderAch(DcsDomainService domainService)
            : base(domainService) {
        }

        internal void InsertAgencyPartsShippingOrderValidate(AgencyPartsShippingOrder agencyPartsShippingOrder) {
            var userInfo = Utils.GetCurrentUserInfo();
            agencyPartsShippingOrder.CreatorId = userInfo.Id;
            agencyPartsShippingOrder.CreatorName = userInfo.Name;
            agencyPartsShippingOrder.CreateTime = DateTime.Now;
            if(string.IsNullOrWhiteSpace(agencyPartsShippingOrder.Code) || agencyPartsShippingOrder.Code == GlobalVar.ASSIGNED_BY_SERVER)
                agencyPartsShippingOrder.Code = CodeGenerator.Generate("AgencyPartsShippingOrder", agencyPartsShippingOrder.ShippingCompanyCode);
        }

        private void UpdateAgencyPartsShippingOrderValidate(AgencyPartsShippingOrder agencyPartsShippingOrder) {
            var userInfo = Utils.GetCurrentUserInfo();
            agencyPartsShippingOrder.ModifierId = userInfo.Id;
            agencyPartsShippingOrder.ModifierName = userInfo.Name;
            agencyPartsShippingOrder.ModifyTime = DateTime.Now;
        }

        public void InsertAgencyPartsShippingOrder(AgencyPartsShippingOrder agencyPartsShippingOrder) {
            InsertToDatabase(agencyPartsShippingOrder);
            var agencyPartsShippingOrderDetails = ChangeSet.GetAssociatedChanges(agencyPartsShippingOrder, r => r.APartsShippingOrderDetails, ChangeOperation.Insert);
            foreach(APartsShippingOrderDetail agencyPartsShippingOrderDetail in agencyPartsShippingOrderDetails) {
                InsertToDatabase(agencyPartsShippingOrderDetail);
            }
            InsertAgencyPartsShippingOrderValidate(agencyPartsShippingOrder);
        }

        public void UpdateAgencyPartsShippingOrder(AgencyPartsShippingOrder agencyPartsShippingOrder) {
            agencyPartsShippingOrder.APartsShippingOrderDetails.Clear();
            UpdateToDatabase(agencyPartsShippingOrder);
            var AgencyPartsShippingOrderDetails = ChangeSet.GetAssociatedChanges(agencyPartsShippingOrder, r => r.APartsShippingOrderDetails);
            foreach(APartsShippingOrderDetail AgencyPartsShippingOrderDetail in AgencyPartsShippingOrderDetails) {
                switch(ChangeSet.GetChangeOperation(AgencyPartsShippingOrderDetail)) {
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(AgencyPartsShippingOrderDetail);
                        break;
                    case ChangeOperation.Insert:
                        InsertToDatabase(AgencyPartsShippingOrderDetail);
                        break;
                    case ChangeOperation.Update:
                        UpdateToDatabase(AgencyPartsShippingOrderDetail);
                        break;
                }
                UpdateAgencyPartsShippingOrderValidate(agencyPartsShippingOrder);
            }

        }
        public IQueryable<AgencyPartsShippingOrder> 查询配件发运单代理库(int? closedLoopStatus, string ERPSourceOrderCodeQuery) {
            var userInfo = Utils.GetCurrentUserInfo();
            IQueryable<AgencyPartsShippingOrder> agencyPartsShippingOrders = ObjectContext.AgencyPartsShippingOrders;
            if(userInfo.EnterpriseCode == "1101") {
                agencyPartsShippingOrders = agencyPartsShippingOrders.Where(r => r.LogisticCompanyId == userInfo.EnterpriseId || r.ShippingCompanyId == userInfo.EnterpriseId || r.ReceivingCompanyId == userInfo.EnterpriseId);
            }
            if(closedLoopStatus == 1) {
                //未闭环
                var statusArray = new[] {
                    (int)DcsPartsShippingOrderStatus.新建, (int)DcsPartsShippingOrderStatus.已发货, (int)DcsPartsShippingOrderStatus.待收货, (int)DcsPartsShippingOrderStatus.待提货
                };
                agencyPartsShippingOrders = agencyPartsShippingOrders.Where(r => statusArray.Contains(r.Status));
            }
            if(closedLoopStatus == 2) {
                //闭环 
                var statusArray = new[] {
                    (int)DcsPartsShippingOrderStatus.收货确认, (int)DcsPartsShippingOrderStatus.回执确认, (int)DcsPartsShippingOrderStatus.作废
                };
                agencyPartsShippingOrders = agencyPartsShippingOrders.Where(r => statusArray.Contains(r.Status));
            }
            if(!string.IsNullOrEmpty(ERPSourceOrderCodeQuery))
                agencyPartsShippingOrders = agencyPartsShippingOrders.Where(r => ObjectContext.PartsSalesOrders.Any(x => x.Code == r.OriginalRequirementBillCode && x.ERPSourceOrderCode.Contains(ERPSourceOrderCodeQuery)));

            return agencyPartsShippingOrders.Include("APartsShippingOrderDetails").Include("PartsSalesCategory").OrderBy(e => e.Status).ThenBy(e => e.RequestedArrivalDate);
        }
        public IQueryable<ShippingOrderOutDetail> 查询发运单出库明细信息代理库() {
            var tempStruct = from t in
                                 (from d in ObjectContext.AgencyPartsShippingOrders
                                  join e in ObjectContext.APartsShippingOrderDetails on d.Id equals e.PartsShippingOrderId
                                  select new {
                                      d.Id,
                                      e.SettlementPrice,
                                      e.ShippingAmount
                                  })
                             group t by t.Id into tempTable
                             select new {
                                 PartsShippingOrderId = tempTable.Key,
                                 TotalAmount = tempTable.Sum(r => r.SettlementPrice * r.ShippingAmount)
                             };
            IQueryable<ShippingOrderOutDetail> result = from a in ObjectContext.AgencyPartsShippingOrderRefs
                                                        join b in ObjectContext.AgencyPartsOutboundBills on a.PartsOutboundBillId equals b.Id
                                                        join c in tempStruct on a.PartsShippingOrderId equals c.PartsShippingOrderId
                                                        select new ShippingOrderOutDetail {
                                                            PartsShippingOrderRefId = a.Id,
                                                            PartsShippingOrderId = c.PartsShippingOrderId,
                                                            PartsOutboundBillId = b.Id,
                                                            PartsOutboundBillCode = b.Code,
                                                            WarehouseId = b.WarehouseId,
                                                            WarehouseCode = b.WarehouseCode,
                                                            WarehouseName = b.WarehouseName,
                                                            OutboundType = b.OutboundType,
                                                            PartsSalesOrderTypeName = b.PartsSalesOrderTypeName,
                                                            CreateTime = b.CreateTime,
                                                            ShippingMethod = b.ShippingMethod,
                                                            Remark = b.Remark,
                                                            TotalAmount = b.APartsOutboundBillDetails.Sum(detail => detail.OutboundAmount * detail.SettlementPrice),
                                                            OrderApproveComment = b.OrderApproveComment
                                                        };
            return result.OrderBy(e => e.PartsOutboundBillId);
        }


    }
    partial class DcsDomainService {

        public void InsertAgencyPartsShippingOrder(AgencyPartsShippingOrder AgencyPartsShippingOrder) {
            new AgencyPartsShippingOrderAch(this).InsertAgencyPartsShippingOrder(AgencyPartsShippingOrder);
        }

        public void UpdateAgencyPartsShippingOrder(AgencyPartsShippingOrder AgencyPartsShippingOrder) {
            new AgencyPartsShippingOrderAch(this).UpdateAgencyPartsShippingOrder(AgencyPartsShippingOrder);
        }
        public IQueryable<AgencyPartsShippingOrder> 查询配件发运单代理库(int? closedLoopStatus, string ERPSourceOrderCodeQuery) {
            return new AgencyPartsShippingOrderAch(this).查询配件发运单代理库(closedLoopStatus, ERPSourceOrderCodeQuery);
        }

        public IQueryable<ShippingOrderOutDetail> 查询发运单出库明细信息代理库() {
            return new AgencyPartsShippingOrderAch(this).查询发运单出库明细信息代理库();
        }
    }
}
