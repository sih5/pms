﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel.DomainServices.Server;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class EcommerceFreightDisposalAch : DcsSerivceAchieveBase {
        public EcommerceFreightDisposalAch(DcsDomainService domainService)
            : base(domainService) {
        }

        internal void InsertEcommerceFreightDisposalValidate(EcommerceFreightDisposal ecommerceFreightDisposal) {
            var userInfo = Utils.GetCurrentUserInfo();
            ecommerceFreightDisposal.CreatorId = userInfo.Id;
            ecommerceFreightDisposal.CreatorName = userInfo.Name;
            ecommerceFreightDisposal.CreateTime = DateTime.Now;
            if(string.IsNullOrWhiteSpace(ecommerceFreightDisposal.Code) || ecommerceFreightDisposal.Code == GlobalVar.ASSIGNED_BY_SERVER)
                ecommerceFreightDisposal.Code = CodeGenerator.Generate("EcommerceFreightDisposal");
        }

        public void InsertEcommerceFreightDisposal(EcommerceFreightDisposal ecommerceFreightDisposal) {
            InsertToDatabase(ecommerceFreightDisposal);
            this.InsertEcommerceFreightDisposalValidate(ecommerceFreightDisposal);
        }

        internal void UpdateEcommerceFreightDisposalValidate(EcommerceFreightDisposal ecommerceFreightDisposal) {
            var userInfo = Utils.GetCurrentUserInfo();
            ecommerceFreightDisposal.ModifierId = userInfo.Id;
            ecommerceFreightDisposal.ModifierName = userInfo.Name;
            ecommerceFreightDisposal.ModifyTime = DateTime.Now;
        }

        public void UpdateEcommerceFreightDisposal(EcommerceFreightDisposal ecommerceFreightDisposal) {
            UpdateToDatabase(ecommerceFreightDisposal);
            this.UpdateEcommerceFreightDisposalValidate(ecommerceFreightDisposal);
        }
        [Query(HasSideEffects = true)]
        public IQueryable<EcommerceFreightDisposal> GetEcommerceFreightDisposalByPartsOutboundBillCode(string[] codes) {
            return this.ObjectContext.EcommerceFreightDisposals.Where(r => codes.Contains(r.PartsOutboundBillCode));
        }

        [Query(HasSideEffects = true)]
        public IQueryable<EcommerceFreightDisposal> GetEcommerceFreightDisposalByPartsInboundBillIds(int[] partsInboundBillIds, int[] partsOutboundBillIds) {
            return this.ObjectContext.EcommerceFreightDisposals.Where(r => partsInboundBillIds.Contains(r.PartsInboundCheckBillId ?? 0));
        }

        [Query(HasSideEffects = true)]
        public IQueryable<ReturnFreightDisposal> GetReturnFreightDisposalsByPartsOutboundBillIdss(int[] partsInboundBillIds, int[] partsOutboundBillIds) {
            return this.ObjectContext.ReturnFreightDisposals.Where(r => partsOutboundBillIds.Contains(r.PartsOutboundBillId ?? 0));
        }
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:33
        //原分布类的函数全部转移到Ach                                                               
        public void InsertEcommerceFreightDisposal(EcommerceFreightDisposal ecommerceFreightDisposal) {
            new EcommerceFreightDisposalAch(this).InsertEcommerceFreightDisposal(ecommerceFreightDisposal);
        }

        public void UpdateEcommerceFreightDisposal(EcommerceFreightDisposal ecommerceFreightDisposal) {
            new EcommerceFreightDisposalAch(this).UpdateEcommerceFreightDisposal(ecommerceFreightDisposal);
        }
        [Query(HasSideEffects = true)]
        public IQueryable<EcommerceFreightDisposal> GetEcommerceFreightDisposalByPartsOutboundBillCode(string[] codes) {
            return new EcommerceFreightDisposalAch(this).GetEcommerceFreightDisposalByPartsOutboundBillCode(codes);
        }

        [Query(HasSideEffects = true)]
        public IQueryable<EcommerceFreightDisposal> GetEcommerceFreightDisposalByPartsInboundBillIds(int[] partsInboundBillIds, int[] partsOutboundBillIds) {
            return new EcommerceFreightDisposalAch(this).GetEcommerceFreightDisposalByPartsInboundBillIds(partsInboundBillIds, partsOutboundBillIds);
        }

        [Query(HasSideEffects = true)]
        public IQueryable<ReturnFreightDisposal> GetReturnFreightDisposalsByPartsOutboundBillIdss(int[] partsInboundBillIds, int[] partsOutboundBillIds) {
            return new EcommerceFreightDisposalAch(this).GetReturnFreightDisposalsByPartsOutboundBillIdss(partsInboundBillIds, partsOutboundBillIds);
        }
    }
}
