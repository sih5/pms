﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using System.Text;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class ExpressAch : DcsSerivceAchieveBase {
        public ExpressAch(DcsDomainService domainService)
            : base(domainService) {
        }

        internal void InsertExpressValidate(Express express) {
            var userInfo = Utils.GetCurrentUserInfo();
            express.CreatorId = userInfo.Id;
            express.CreatorName = userInfo.Name;
            express.CreateTime = DateTime.Now;
        }
        public void InsertExpress(Express express) {
            InsertToDatabase(express);
            this.InsertExpressValidate(express);
        }
        internal void UpdateExpressValidate(Express express) {
            var userInfo = Utils.GetCurrentUserInfo();
            express.ModifierId = userInfo.Id;
            express.ModifierName = userInfo.Name;
            express.ModifyTime = DateTime.Now;
        }
        public void UpdateExpress(Express express) {
            UpdateToDatabase(express);
            this.UpdateExpressValidate(express);
        }

        public void 作废快递公司(int id) {
            var userInfo = Utils.GetCurrentUserInfo();
            var express = ObjectContext.Expresses.FirstOrDefault(r => id==r.Id);
            express.CancelId = userInfo.Id;
            express.CancelName = userInfo.Name;
            express.CancelTime = DateTime.Now;
            express.Status = (int)DcsMasterDataStatus.作废;
            UpdateToDatabase(express);
            var expressToLogistics=ObjectContext.ExpressToLogistics.Where(r=>r.ExpressId==id).ToArray();
            foreach(var expressToLogistic in expressToLogistics) {
                expressToLogistic.CancelId = userInfo.Id;
                expressToLogistic.CancelName = userInfo.Name;
                expressToLogistic.CancelTime = DateTime.Now;
                expressToLogistic.Status = (int)DcsMasterDataStatus.作废;
                UpdateToDatabase(expressToLogistic);
            }
            ObjectContext.SaveChanges();
        }

        public void 停用快递公司(int id) {
            var userInfo = Utils.GetCurrentUserInfo();
            var express = ObjectContext.Expresses.FirstOrDefault(r => id==r.Id);
            express.ModifierId = userInfo.Id;
            express.ModifierName = userInfo.Name;
            express.ModifyTime = DateTime.Now;
            express.Status = (int)DcsMasterDataStatus.停用;
            UpdateToDatabase(express);
            var expressToLogistics=ObjectContext.ExpressToLogistics.Where(r=>r.ExpressId==id).ToArray();
            foreach(var expressToLogistic in expressToLogistics) {
                expressToLogistic.ModifierId = userInfo.Id;
                expressToLogistic.ModifierName = userInfo.Name;
                expressToLogistic.ModifyTime = DateTime.Now;
                expressToLogistic.Status = (int)DcsMasterDataStatus.停用;
                UpdateToDatabase(expressToLogistic);
            }
            ObjectContext.SaveChanges();
        }

        public void 恢复快递公司(int id) {
            var userInfo = Utils.GetCurrentUserInfo();
            var express = ObjectContext.Expresses.FirstOrDefault(r => id==r.Id);
            express.ModifierId = userInfo.Id;
            express.ModifierName = userInfo.Name;
            express.ModifyTime = DateTime.Now;
            express.Status = (int)DcsMasterDataStatus.有效;
            UpdateToDatabase(express);
            var expressToLogistics=ObjectContext.ExpressToLogistics.Where(r=>r.ExpressId==id).ToArray();
            foreach(var expressToLogistic in expressToLogistics) {
                expressToLogistic.ModifierId = userInfo.Id;
                expressToLogistic.ModifierName = userInfo.Name;
                expressToLogistic.ModifyTime = DateTime.Now;
                expressToLogistic.Status = (int)DcsMasterDataStatus.有效;
                UpdateToDatabase(expressToLogistic);
            }
            ObjectContext.SaveChanges();
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:33
        //原分布类的函数全部转移到Ach                                                               

        public void InsertExpress(Express express) {
            new ExpressAch(this).InsertExpress(express);
        }

        public void UpdateExpress(Express express) {
            new ExpressAch(this).UpdateExpress(express);
        }
        
        [Invoke(HasSideEffects = true)]
        public void 作废快递公司(int id) {
            new ExpressAch(this).作废快递公司(id);
        }
        
        [Invoke(HasSideEffects = true)]
        public void 停用快递公司(int id) {
            new ExpressAch(this).停用快递公司(id);
        }
        
        [Invoke(HasSideEffects = true)]
        public void 恢复快递公司(int id) {
            new ExpressAch(this).恢复快递公司(id);
        }
    }
}
