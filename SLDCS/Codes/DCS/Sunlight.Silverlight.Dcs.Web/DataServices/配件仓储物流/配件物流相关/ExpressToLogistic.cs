﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.Text;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class ExpressToLogisticAch : DcsSerivceAchieveBase {
        public ExpressToLogisticAch(DcsDomainService domainService)
            : base(domainService) {
        }

        internal void InsertExpressToLogisticValidate(ExpressToLogistic expressToLogistic) {
            var userInfo = Utils.GetCurrentUserInfo();
            expressToLogistic.CreatorId = userInfo.Id;
            expressToLogistic.CreatorName = userInfo.Name;
            expressToLogistic.CreateTime = DateTime.Now;
        }
        public void InsertExpressToLogistic(ExpressToLogistic expressToLogistic) {
            var dbexpressToLogistic = ObjectContext.ExpressToLogistics.Where(r => r.FocufingCoreId == expressToLogistic.FocufingCoreId && r.ExpressId == expressToLogistic.ExpressId && r.Status == (int)DcsMasterDataStatus.有效).ToArray();
            if(dbexpressToLogistic.Any()) {
                throw new ValidationException(ErrorStrings.ExPressToLogistic_Validation1);
            }
            InsertToDatabase(expressToLogistic);
            this.InsertExpressToLogisticValidate(expressToLogistic);
        }
        internal void UpdateExpressToLogisticValidate(ExpressToLogistic expressToLogistic) {
            var userInfo = Utils.GetCurrentUserInfo();
            expressToLogistic.ModifierId = userInfo.Id;
            expressToLogistic.ModifierName = userInfo.Name;
            expressToLogistic.ModifyTime = DateTime.Now;
        }
        public void UpdateExpressToLogistic(ExpressToLogistic expressToLogistic) {
            var dbexpressToLogistic = ObjectContext.ExpressToLogistics.Where(r => r.FocufingCoreId == expressToLogistic.FocufingCoreId && r.ExpressId == expressToLogistic.ExpressId && r.Status == (int)DcsMasterDataStatus.有效 && expressToLogistic.Id != r.Id).ToArray();
            if(dbexpressToLogistic.Any()) {
                throw new ValidationException(ErrorStrings.ExPressToLogistic_Validation1);
            }
            UpdateToDatabase(expressToLogistic);
            this.UpdateExpressToLogisticValidate(expressToLogistic);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:33
        //原分布类的函数全部转移到Ach                                                               

        public void InsertExpressToLogistic(ExpressToLogistic expressToLogistic) {
            new ExpressToLogisticAch(this).InsertExpressToLogistic(expressToLogistic);
        }

        public void UpdateExpressToLogistic(ExpressToLogistic expressToLogistic) {
            new ExpressToLogisticAch(this).UpdateExpressToLogistic(expressToLogistic);
        }
    }
}
