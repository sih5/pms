﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class HauDistanceInforAch : DcsSerivceAchieveBase {
        internal void InsertHauDistanceInforValidate(HauDistanceInfor hauDistanceInfor) {
            var dbHauDistanceInfor = ObjectContext.HauDistanceInfors.Where(r => r.WarehouseId == hauDistanceInfor.WarehouseId && r.CompanyId == hauDistanceInfor.CompanyId && r.CompanyAddressId == hauDistanceInfor.CompanyAddressId && r.Status != (int)DcsBaseDataStatus.作废).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbHauDistanceInfor != null)
                throw new ValidationException(ErrorStrings.HauDistanceInfor_Validation1);
            var userInfo = Utils.GetCurrentUserInfo();
            hauDistanceInfor.CreatorId = userInfo.Id;
            hauDistanceInfor.CreatorName = userInfo.Name;
            hauDistanceInfor.CreateTime = DateTime.Now;
        }
        internal void UpdateHauDistanceInforValidate(HauDistanceInfor hauDistanceInfor) {
            var dbHauDistanceInfor = ObjectContext.HauDistanceInfors.Where(r => r.Id != hauDistanceInfor.Id && r.WarehouseId == hauDistanceInfor.WarehouseId && r.CompanyId == hauDistanceInfor.CompanyId && r.CompanyAddressId == hauDistanceInfor.CompanyAddressId && r.Status != (int)DcsBaseDataStatus.作废).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbHauDistanceInfor != null)
                throw new ValidationException(ErrorStrings.HauDistanceInfor_Validation2);
            var userInfo = Utils.GetCurrentUserInfo();
            hauDistanceInfor.ModifierId = userInfo.Id;
            hauDistanceInfor.ModifierName = userInfo.Name;
            hauDistanceInfor.ModifyTime = DateTime.Now;
        }
        public void InsertHauDistanceInfor(HauDistanceInfor hauDistanceInfor) {
            InsertToDatabase(hauDistanceInfor);
            this.InsertHauDistanceInforValidate(hauDistanceInfor);
        }
        public void UpdateHauDistanceInfor(HauDistanceInfor hauDistanceInfor) {
            UpdateToDatabase(hauDistanceInfor);
            this.UpdateHauDistanceInforValidate(hauDistanceInfor);
        }
        public IQueryable<HauDistanceInfor> GetHauDistanceInforWithDetails() {
            return ObjectContext.HauDistanceInfors.Include("CompanyAddress").Include("Company").Include("Company1").Include("Warehouse").OrderBy(v => v.Id);
        }

    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:33
        //原分布类的函数全部转移到Ach                                                               

        public void InsertHauDistanceInfor(HauDistanceInfor hauDistanceInfor) {
            new HauDistanceInforAch(this).InsertHauDistanceInfor(hauDistanceInfor);
        }

        public void UpdateHauDistanceInfor(HauDistanceInfor hauDistanceInfor) {
            new HauDistanceInforAch(this).UpdateHauDistanceInfor(hauDistanceInfor);
        }
                public IQueryable<HauDistanceInfor> GetHauDistanceInforWithDetails() {
            return new HauDistanceInforAch(this).GetHauDistanceInforWithDetails();
        }
    }
}
