﻿using System;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class LogisticAch : DcsSerivceAchieveBase {
        public LogisticAch(DcsDomainService domainService)
            : base(domainService) {
        }


        internal void UpdateLogisticsValidate(Logistic logistic) {
            var userInfo = Utils.GetCurrentUserInfo();
            logistic.ModifierId = userInfo.Id;
            logistic.ModifierName = userInfo.Name;
            logistic.ModifyTime = DateTime.Now;
        }

        public void InsertLogistics(Logistic logistic) {
            InsertToDatabase(logistic);
            var logisticsDetails = ChangeSet.GetAssociatedChanges(logistic, v => v.LogisticsDetails, ChangeOperation.Insert);
            foreach(LogisticsDetail logisticsDetail in logisticsDetails) {
                InsertToDatabase(logisticsDetail);
            }
        }

        public void UpdateLogistics(Logistic logistic) {
            logistic.LogisticsDetails.Clear();
            UpdateToDatabase(logistic);
            var logisticsDetails = ChangeSet.GetAssociatedChanges(logistic, v => v.LogisticsDetails);
            foreach(LogisticsDetail logisticsDetail in logisticsDetails) {
                switch(ChangeSet.GetChangeOperation(logisticsDetail)) {
                    case ChangeOperation.Insert:
                        InsertToDatabase(logisticsDetail);
                        break;
                    case ChangeOperation.Update:
                        UpdateToDatabase(logisticsDetail);
                        break;
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(logisticsDetail);
                        break;
                }
            }
            this.UpdateLogisticsValidate(logistic);
        }

        public IQueryable<VirtualLogistic> GetLogisticsWithDetailCommon(int? signStatus, bool isFilterItemNull, int[] processIds, int[] logisticIds) {
            var userInfo = Utils.GetCurrentUserInfo();
            IQueryable<Logistic> logisties = ObjectContext.Logistics;
            IQueryable<PartsSalesOrderProcess> partsSalesOrderProcesses = this.ObjectContext.PartsSalesOrderProcesses;
            IQueryable<Warehouse> warehouses = this.ObjectContext.Warehouses;
            var company = this.ObjectContext.Companies.FirstOrDefault(r => r.Id == userInfo.EnterpriseId);
            if(company == null)
                return null;

            if(company.Type == (int)DcsCompanyType.分公司 || company.Type == (int)DcsCompanyType.代理库) {
                var salesUnitAffis = this.ObjectContext.SalesUnitAffiPersonnels.Where(r => r.PersonnelId == userInfo.Id).Select(r => new {
                    r.SalesUnitId,
                    r.Id
                });
                var salesUnits = this.ObjectContext.SalesUnits.Where(r => salesUnitAffis.Any(e => e.SalesUnitId == r.Id));
                var partsSalesOrders = this.ObjectContext.PartsSalesOrders.Where(r => r.SalesUnitOwnerCompanyId == userInfo.EnterpriseId && salesUnits.Any(unit => r.SalesUnitId == unit.Id));

                if(signStatus.HasValue) {
                    logisties = logisties.Where(r => r.SignStatus == signStatus.Value);
                }
                IQueryable<PartsShippingOrder> partsShippingOrders = this.ObjectContext.PartsShippingOrders.Where(r => logisties.Any(e => e.ShippingCode == r.Code));
                partsShippingOrders = partsShippingOrders.Where(r => r.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件销售订单);
                partsSalesOrders = partsSalesOrders.Where(e => partsShippingOrders.Any(r => r.OriginalRequirementBillCode == e.Code));
                var resultProcess = from process in partsSalesOrderProcesses
                                    join order in partsSalesOrders on process.OriginalSalesOrderId equals order.Id
                                    join warehouse in warehouses on order.WarehouseId equals warehouse.Id
                                    select new VirtualLogistic {
                                        Id = process.Id,
                                        PartsSalesOrderStatus = process.BillStatusAfterProcess,
                                        ShippingCode = "",
                                        OrderCode = order.Code,
                                        ShippingCompanyName = "",
                                        ShippingCompanyNumber = "",
                                        ApproveTime = process.BillStatusAfterProcess == (int)DcsPartsSalesOrderStatus.部分审批 || process.BillStatusAfterProcess == (int)DcsPartsSalesOrderStatus.审批完成 ? process.CreateTime : null,
                                        WarehouseName = warehouse.Name,
                                        ShippingDate = null,
                                        RequestedArrivalDate = order.RequestedDeliveryTime,
                                        ReceivingAddress = order.ReceivingAddress,
                                        LogisticName = "",
                                        TransportDriverPhone = "",
                                        ModifierId = null,
                                        ModifierName = "",
                                        ModifyTime = null,
                                        CancelId = null,
                                        CancelName = "",
                                        CancelTime = null,
                                        Status = null,
                                        ShippingMethod = order.ShippingMethod,
                                        SignStatus = -1,
                                        CurrentOrderType = (int)DcsCurrentOrderType.配件销售订单
                                    };

                var logistics = from l in logisties
                                join t1 in ObjectContext.PartsShippingOrders.Where(e => e.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件销售订单) on l.ShippingCode equals t1.Code
                                join order in partsSalesOrders on t1.OriginalRequirementBillCode equals order.Code
                                select new VirtualLogistic {
                                    Id = l.Id,
                                    PartsSalesOrderStatus = l.PartsSalesOrderStatus,
                                    ShippingCode = l.ShippingCode,
                                    OrderCode = l.OrderCode,
                                    ShippingCompanyName = l.ShippingCompanyName,
                                    ShippingCompanyNumber = l.ShippingCompanyNumber,
                                    ApproveTime = l.ApproveTime,
                                    WarehouseName = l.WarehouseName,
                                    ShippingDate = l.ShippingDate,
                                    RequestedArrivalDate = l.RequestedArrivalDate,
                                    ReceivingAddress = l.ReceivingAddress,
                                    LogisticName = l.LogisticName,
                                    TransportDriverPhone = l.TransportDriverPhone,
                                    ModifierId = l.ModifierId,
                                    ModifierName = l.ModifierName,
                                    ModifyTime = l.ModifyTime,
                                    CancelId = l.CancelId,
                                    CancelName = l.CancelName,
                                    CancelTime = l.CancelTime,
                                    Status = l.Status,
                                    ShippingMethod = l.ShippingMethod,
                                    SignStatus = l.SignStatus,
                                    CurrentOrderType = (int)DcsCurrentOrderType.物流信息主单
                                };

                if(processIds != null) {
                    resultProcess = resultProcess.Where(e => processIds.Contains(e.Id));
                }
                if(logisticIds != null) {
                    logistics = logistics.Where(e => logisticIds.Contains(e.Id));
                }
                return resultProcess.Union(logistics).OrderBy(r => new {
                    r.OrderCode,
                    r.Status
                });
            }
            if(company.Type == (int)DcsCompanyType.服务站) {
                IQueryable<PartsSalesOrder> partsSalesOrders = this.ObjectContext.PartsSalesOrders.Where(r => r.SubmitCompanyId == userInfo.EnterpriseId);

                if(signStatus.HasValue) {
                    logisties = logisties.Where(r => r.SignStatus == signStatus.Value);
                }

                IQueryable<PartsShippingOrder> partsShippingOrders = this.ObjectContext.PartsShippingOrders.Where(r => logisties.Any(e => e.ShippingCode == r.Code));
                partsShippingOrders = partsShippingOrders.Where(r => r.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件销售订单);
                partsSalesOrders = partsSalesOrders.Where(e => partsShippingOrders.Any(r => r.OriginalRequirementBillCode == e.Code));

                var resultProcess = from process in partsSalesOrderProcesses
                                    join order in partsSalesOrders on process.OriginalSalesOrderId equals order.Id
                                    join warehouse in warehouses on order.WarehouseId equals warehouse.Id
                                    select new VirtualLogistic {
                                        Id = process.Id,
                                        PartsSalesOrderStatus = process.BillStatusAfterProcess,
                                        ShippingCode = "",
                                        OrderCode = order.Code,
                                        ShippingCompanyName = "",
                                        ShippingCompanyNumber = "",
                                        ApproveTime = process.BillStatusAfterProcess == (int)DcsPartsSalesOrderStatus.部分审批 || process.BillStatusAfterProcess == (int)DcsPartsSalesOrderStatus.审批完成 ? process.CreateTime : null,
                                        WarehouseName = warehouse.Name,
                                        ShippingDate = null,
                                        RequestedArrivalDate = order.RequestedDeliveryTime,
                                        ReceivingAddress = order.ReceivingAddress,
                                        LogisticName = "",
                                        TransportDriverPhone = "",
                                        ModifierId = null,
                                        ModifierName = "",
                                        ModifyTime = null,
                                        CancelId = null,
                                        CancelName = "",
                                        CancelTime = null,
                                        Status = null,
                                        ShippingMethod = order.ShippingMethod,
                                        SignStatus = -1,
                                        CurrentOrderType = (int)DcsCurrentOrderType.配件销售订单
                                    };

                var logistics = from l in logisties
                                join t1 in ObjectContext.PartsShippingOrders.Where(e => e.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件销售订单) on l.ShippingCode equals t1.Code
                                join order in partsSalesOrders on t1.OriginalRequirementBillCode equals order.Code
                                select new VirtualLogistic {
                                    Id = l.Id,
                                    PartsSalesOrderStatus = l.PartsSalesOrderStatus,
                                    ShippingCode = l.ShippingCode,
                                    OrderCode = l.OrderCode,
                                    ShippingCompanyName = l.ShippingCompanyName,
                                    ShippingCompanyNumber = l.ShippingCompanyNumber,
                                    ApproveTime = l.ApproveTime,
                                    WarehouseName = l.WarehouseName,
                                    ShippingDate = l.ShippingDate,
                                    RequestedArrivalDate = l.RequestedArrivalDate,
                                    ReceivingAddress = l.ReceivingAddress,
                                    LogisticName = l.LogisticName,
                                    TransportDriverPhone = l.TransportDriverPhone,
                                    ModifierId = l.ModifierId,
                                    ModifierName = l.ModifierName,
                                    ModifyTime = l.ModifyTime,
                                    CancelId = l.CancelId,
                                    CancelName = l.CancelName,
                                    CancelTime = l.CancelTime,
                                    Status = l.Status,
                                    ShippingMethod = l.ShippingMethod,
                                    SignStatus = l.SignStatus,
                                    CurrentOrderType = (int)DcsCurrentOrderType.配件销售订单
                                };
                if(processIds != null) {
                    resultProcess = resultProcess.Where(e => processIds.Contains(e.Id));
                }
                if(logisticIds != null) {
                    logistics = logistics.Where(e => logisticIds.Contains(e.Id));
                }
                return resultProcess.Union(logistics).OrderBy(r => new {
                    r.OrderCode,
                    r.Status
                });
            }
            if(company.Type == (int)DcsCompanyType.物流公司) {
                var result = this.根据签收状态查询物流信息(signStatus, isFilterItemNull);
                if(logisticIds != null) {
                    result = result.Where(e => logisticIds.Contains(e.Id));
                }
                return result;
            }
            return null;
        }

        public IQueryable<VirtualLogistic> GetLogisticsMergeDetail(int? signStatus, bool isFilterItemNull, int[] processIds, int[] logisticIds) {
            var userInfo = Utils.GetCurrentUserInfo();
            IQueryable<Logistic> logisties = ObjectContext.Logistics;
            IQueryable<LogisticsDetail> logisticsDetails = this.ObjectContext.LogisticsDetails;
            IQueryable<PartsSalesOrderProcess> partsSalesOrderProcesses = this.ObjectContext.PartsSalesOrderProcesses;
            IQueryable<Warehouse> warehouses = this.ObjectContext.Warehouses;
            var company = this.ObjectContext.Companies.FirstOrDefault(r => r.Id == userInfo.EnterpriseId);
            if(company == null)
                return null;

            if(company.Type == (int)DcsCompanyType.分公司 || company.Type == (int)DcsCompanyType.代理库) {
                var salesUnitAffis = this.ObjectContext.SalesUnitAffiPersonnels.Where(r => r.PersonnelId == userInfo.Id).Select(r => new {
                    r.SalesUnitId,
                    r.Id
                });
                var salesUnits = this.ObjectContext.SalesUnits.Where(r => salesUnitAffis.Any(e => e.SalesUnitId == r.Id));
                var partsSalesOrders = this.ObjectContext.PartsSalesOrders.Where(r => r.SalesUnitOwnerCompanyId == userInfo.EnterpriseId && salesUnits.Any(unit => r.SalesUnitId == unit.Id));

                if(signStatus.HasValue) {
                    logisties = logisties.Where(r => r.SignStatus == signStatus.Value);
                }
                IQueryable<PartsShippingOrder> partsShippingOrders = this.ObjectContext.PartsShippingOrders.Where(r => logisties.Any(e => e.ShippingCode == r.Code));
                partsShippingOrders = partsShippingOrders.Where(r => r.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件销售订单);
                partsSalesOrders = partsSalesOrders.Where(e => partsShippingOrders.Any(r => r.OriginalRequirementBillCode == e.Code));
                var resultProcess = from process in partsSalesOrderProcesses
                                    join order in partsSalesOrders on process.OriginalSalesOrderId equals order.Id
                                    join warehouse in warehouses on order.WarehouseId equals warehouse.Id
                                    select new VirtualLogistic {
                                        Id = process.Id,
                                        PartsSalesOrderStatus = process.BillStatusAfterProcess,
                                        ShippingCode = "",
                                        OrderCode = order.Code,
                                        ShippingCompanyName = "",
                                        ShippingCompanyNumber = "",
                                        ApproveTime = process.BillStatusAfterProcess == (int)DcsPartsSalesOrderStatus.部分审批 || process.BillStatusAfterProcess == (int)DcsPartsSalesOrderStatus.审批完成 ? process.CreateTime : null,
                                        WarehouseName = warehouse.Name,
                                        ShippingDate = null,
                                        RequestedArrivalDate = order.RequestedDeliveryTime,
                                        ReceivingAddress = order.ReceivingAddress,
                                        LogisticName = "",
                                        TransportDriverPhone = "",
                                        ModifierId = null,
                                        ModifierName = "",
                                        ModifyTime = null,
                                        CancelId = null,
                                        CancelName = "",
                                        CancelTime = null,
                                        Status = null,
                                        ShippingMethod = order.ShippingMethod,
                                        SignStatus = -1,
                                        CurrentOrderType = (int)DcsCurrentOrderType.配件销售订单
                                    };

                var logistics = from l in logisties
                                join t1 in ObjectContext.PartsShippingOrders.Where(e => e.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件销售订单) on l.ShippingCode equals t1.Code
                                join order in partsSalesOrders on t1.OriginalRequirementBillCode equals order.Code
                                join detail in logisticsDetails on l.Id equals detail.Logisticsid
                                select new VirtualLogistic {
                                    Id = l.Id,
                                    PartsSalesOrderStatus = l.PartsSalesOrderStatus,
                                    ShippingCode = l.ShippingCode,
                                    OrderCode = l.OrderCode,
                                    ShippingCompanyName = l.ShippingCompanyName,
                                    ShippingCompanyNumber = l.ShippingCompanyNumber,
                                    ApproveTime = l.ApproveTime,
                                    WarehouseName = l.WarehouseName,
                                    ShippingDate = l.ShippingDate,
                                    RequestedArrivalDate = l.RequestedArrivalDate,
                                    ReceivingAddress = l.ReceivingAddress,
                                    LogisticName = l.LogisticName,
                                    TransportDriverPhone = l.TransportDriverPhone,
                                    ModifierId = l.ModifierId,
                                    ModifierName = l.ModifierName,
                                    ModifyTime = l.ModifyTime,
                                    CancelId = l.CancelId,
                                    CancelName = l.CancelName,
                                    CancelTime = l.CancelTime,
                                    Status = l.Status,
                                    ShippingMethod = l.ShippingMethod,
                                    SignStatus = l.SignStatus,
                                    CurrentOrderType = (int)DcsCurrentOrderType.物流信息主单
                                };

                if(processIds != null) {
                    resultProcess = resultProcess.Where(e => processIds.Contains(e.Id));
                }
                if(logisticIds != null) {
                    logistics = logistics.Where(e => logisticIds.Contains(e.Id));
                }
                return resultProcess.Union(logistics).OrderBy(r => new {
                    r.OrderCode,
                    r.Status
                });
            }
            if(company.Type == (int)DcsCompanyType.服务站) {
                IQueryable<PartsSalesOrder> partsSalesOrders = this.ObjectContext.PartsSalesOrders.Where(r => r.SubmitCompanyId == userInfo.EnterpriseId);

                if(signStatus.HasValue) {
                    logisties = logisties.Where(r => r.SignStatus == signStatus.Value);
                }

                IQueryable<PartsShippingOrder> partsShippingOrders = this.ObjectContext.PartsShippingOrders.Where(r => logisties.Any(e => e.ShippingCode == r.Code));
                partsShippingOrders = partsShippingOrders.Where(r => r.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件销售订单);
                partsSalesOrders = partsSalesOrders.Where(e => partsShippingOrders.Any(r => r.OriginalRequirementBillCode == e.Code));

                var resultProcess = from process in partsSalesOrderProcesses
                                    join order in partsSalesOrders on process.OriginalSalesOrderId equals order.Id
                                    join warehouse in warehouses on order.WarehouseId equals warehouse.Id
                                    select new VirtualLogistic {
                                        Id = process.Id,
                                        PartsSalesOrderStatus = process.BillStatusAfterProcess,
                                        ShippingCode = "",
                                        OrderCode = order.Code,
                                        ShippingCompanyName = "",
                                        ShippingCompanyNumber = "",
                                        ApproveTime = process.BillStatusAfterProcess == (int)DcsPartsSalesOrderStatus.部分审批 || process.BillStatusAfterProcess == (int)DcsPartsSalesOrderStatus.审批完成 ? process.CreateTime : null,
                                        WarehouseName = warehouse.Name,
                                        ShippingDate = null,
                                        RequestedArrivalDate = order.RequestedDeliveryTime,
                                        ReceivingAddress = order.ReceivingAddress,
                                        LogisticName = "",
                                        TransportDriverPhone = "",
                                        ModifierId = null,
                                        ModifierName = "",
                                        ModifyTime = null,
                                        CancelId = null,
                                        CancelName = "",
                                        CancelTime = null,
                                        Status = null,
                                        ShippingMethod = order.ShippingMethod,
                                        SignStatus = -1,
                                        CurrentOrderType = (int)DcsCurrentOrderType.配件销售订单
                                    };

                var logistics = from l in logisties
                                join t1 in ObjectContext.PartsShippingOrders.Where(e => e.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件销售订单) on l.ShippingCode equals t1.Code
                                join order in partsSalesOrders on t1.OriginalRequirementBillCode equals order.Code
                                select new VirtualLogistic {
                                    Id = l.Id,
                                    PartsSalesOrderStatus = l.PartsSalesOrderStatus,
                                    ShippingCode = l.ShippingCode,
                                    OrderCode = l.OrderCode,
                                    ShippingCompanyName = l.ShippingCompanyName,
                                    ShippingCompanyNumber = l.ShippingCompanyNumber,
                                    ApproveTime = l.ApproveTime,
                                    WarehouseName = l.WarehouseName,
                                    ShippingDate = l.ShippingDate,
                                    RequestedArrivalDate = l.RequestedArrivalDate,
                                    ReceivingAddress = l.ReceivingAddress,
                                    LogisticName = l.LogisticName,
                                    TransportDriverPhone = l.TransportDriverPhone,
                                    ModifierId = l.ModifierId,
                                    ModifierName = l.ModifierName,
                                    ModifyTime = l.ModifyTime,
                                    CancelId = l.CancelId,
                                    CancelName = l.CancelName,
                                    CancelTime = l.CancelTime,
                                    Status = l.Status,
                                    ShippingMethod = l.ShippingMethod,
                                    SignStatus = l.SignStatus,
                                    CurrentOrderType = (int)DcsCurrentOrderType.配件销售订单
                                };
                if(processIds != null) {
                    resultProcess = resultProcess.Where(e => processIds.Contains(e.Id));
                }
                if(logisticIds != null) {
                    logistics = logistics.Where(e => logisticIds.Contains(e.Id));
                }
                return resultProcess.Union(logistics).OrderBy(r => new {
                    r.OrderCode,
                    r.Status
                });
            }
            if(company.Type == (int)DcsCompanyType.物流公司) {
                var result = this.根据签收状态查询物流信息(signStatus, isFilterItemNull);
                if(logisticIds != null) {
                    result = result.Where(e => logisticIds.Contains(e.Id));
                }
                return result;
            }
            return null;
        }

        //用于导出
        public IQueryable<VirtualLogistic> 导出物流信息主单以及履历(int? signStatus, bool isFilterItemNull, int[] processIds, int[] logisticIds) {
            return GetLogisticsWithDetailCommon(signStatus, isFilterItemNull, processIds, logisticIds);
        }

        public IQueryable<VirtualLogistic> 导出物流信息主清单以及履历(int? signStatus, bool isFilterItemNull, int[] processIds, int[] logisticIds) {
            return GetLogisticsWithDetailCommon(signStatus, isFilterItemNull, processIds, logisticIds);
        }

        public IQueryable<VirtualLogistic> GetLogisticsWithDetail(int? signStatus, bool isFilterItemNull) {
            return GetLogisticsWithDetailCommon(signStatus, isFilterItemNull, null, null);
        }

        public IQueryable<Logistic> 查询物流信息() {
            return ObjectContext.Logistics.OrderBy(r => r.Id).Include("LogisticsDetails");
        }

        public IQueryable<VirtualLogistic> 根据签收状态查询物流信息(int? signStatus, bool isFilterItemNull) {
            IQueryable<Logistic> logisties = ObjectContext.Logistics;
            var userInfo = Utils.GetCurrentUserInfo();

            if(signStatus.HasValue) {
                logisties = logisties.Where(e => e.SignStatus == signStatus.Value);
                //if(signStatus == (int)DcsPartsShippingOrderStatus.收货确认)
                //    logisties = logisties.Where(r => ObjectContext.PartsShippingOrders.Any(so => so.Status == (int)DcsPartsShippingOrderStatus.收货确认 && r.ShippingCode == so.Code));
                //else
                //    logisties = logisties.Where(r => ObjectContext.PartsShippingOrders.Any(so => so.Status != (int)DcsPartsShippingOrderStatus.收货确认 && r.ShippingCode == so.Code));
            }
            if(ObjectContext.LogisticCompanies.Any(r => r.Id == userInfo.EnterpriseId)) {
                var logistics = from l in logisties
                                join t1 in ObjectContext.PartsShippingOrders.Where(r => r.LogisticCompanyId == userInfo.EnterpriseId) on l.ShippingCode equals t1.Code
                                select new VirtualLogistic {
                                    Id = l.Id,
                                    PartsSalesOrderStatus = l.PartsSalesOrderStatus,
                                    ShippingCode = l.ShippingCode,
                                    OrderCode = l.OrderCode,
                                    ShippingCompanyName = l.ShippingCompanyName,
                                    ShippingCompanyNumber = l.ShippingCompanyNumber,
                                    ApproveTime = l.ApproveTime,
                                    WarehouseName = l.WarehouseName,
                                    ShippingDate = l.ShippingDate,
                                    RequestedArrivalDate = l.RequestedArrivalDate,
                                    ReceivingAddress = l.ReceivingAddress,
                                    LogisticName = l.LogisticName,
                                    TransportDriverPhone = l.TransportDriverPhone,
                                    ModifierId = l.ModifierId,
                                    ModifierName = l.ModifierName,
                                    ModifyTime = l.ModifyTime,
                                    CancelId = l.CancelId,
                                    CancelName = l.CancelName,
                                    CancelTime = l.CancelTime,
                                    Status = l.Status,
                                    ShippingMethod = l.ShippingMethod,
                                    SignStatus = l.SignStatus,
                                    CurrentOrderType = (int)DcsCurrentOrderType.物流信息主单
                                };
                return logistics.OrderBy(r => r.Id);
            } else {
                var logistics = from l in logisties
                                join t1 in ObjectContext.PartsShippingOrders.Where(r => r.ReceivingCompanyId == userInfo.EnterpriseId) on l.ShippingCode equals t1.Code
                                select new VirtualLogistic {
                                    Id = l.Id,
                                    PartsSalesOrderStatus = l.PartsSalesOrderStatus,
                                    ShippingCode = l.ShippingCode,
                                    OrderCode = l.OrderCode,
                                    ShippingCompanyName = l.ShippingCompanyName,
                                    ShippingCompanyNumber = l.ShippingCompanyNumber,
                                    ApproveTime = l.ApproveTime,
                                    WarehouseName = l.WarehouseName,
                                    ShippingDate = l.ShippingDate,
                                    RequestedArrivalDate = l.RequestedArrivalDate,
                                    ReceivingAddress = l.ReceivingAddress,
                                    LogisticName = l.LogisticName,
                                    TransportDriverPhone = l.TransportDriverPhone,
                                    ModifierId = l.ModifierId,
                                    ModifierName = l.ModifierName,
                                    ModifyTime = l.ModifyTime,
                                    CancelId = l.CancelId,
                                    CancelName = l.CancelName,
                                    CancelTime = l.CancelTime,
                                    Status = l.Status,
                                    ShippingMethod = l.ShippingMethod,
                                    SignStatus = l.SignStatus,
                                    CurrentOrderType = (int)DcsCurrentOrderType.物流信息主单
                                };
                var logistics2 = from l in logisties.Where(r => r.ShippingCompanyNumber == userInfo.EnterpriseCode)
                                 join t1 in this.ObjectContext.PartsShippingOrders on l.ShippingCode equals t1.Code
                                 select new VirtualLogistic {
                                     Id = l.Id,
                                     PartsSalesOrderStatus = l.PartsSalesOrderStatus,
                                     ShippingCode = l.ShippingCode,
                                     OrderCode = l.OrderCode,
                                     ShippingCompanyName = l.ShippingCompanyName,
                                     ShippingCompanyNumber = l.ShippingCompanyNumber,
                                     ApproveTime = l.ApproveTime,
                                     WarehouseName = l.WarehouseName,
                                     ShippingDate = l.ShippingDate,
                                     RequestedArrivalDate = l.RequestedArrivalDate,
                                     ReceivingAddress = l.ReceivingAddress,
                                     LogisticName = l.LogisticName,
                                     TransportDriverPhone = l.TransportDriverPhone,
                                     ModifierId = l.ModifierId,
                                     ModifierName = l.ModifierName,
                                     ModifyTime = l.ModifyTime,
                                     CancelId = l.CancelId,
                                     CancelName = l.CancelName,
                                     CancelTime = l.CancelTime,
                                     Status = l.Status,
                                     ShippingMethod = l.ShippingMethod,
                                     SignStatus = l.SignStatus,
                                     CurrentOrderType = (int)DcsCurrentOrderType.物流信息主单
                                 };
                return logistics.Union(logistics2).OrderBy(r => r.Id);
            }
        }
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:33
        //原分布类的函数全部转移到Ach                                                               
        public void InsertLogistics(Logistic logistic) {
            new LogisticAch(this).InsertLogistics(logistic);
        }
        public void UpdateLogistics(Logistic logistic) {
            new LogisticAch(this).UpdateLogistics(logistic);
        }
        public IQueryable<Logistic> 查询物流信息() {
            return new LogisticAch(this).查询物流信息();
        }
        public IQueryable<VirtualLogistic> 根据签收状态查询物流信息(int? signStatus, bool isFilterItemNull) {
            return new LogisticAch(this).根据签收状态查询物流信息(signStatus, isFilterItemNull);
        }
        public IQueryable<VirtualLogistic> 导出物流信息主单以及履历(int? signStatus, bool isFilterItemNull, int[] processIds, int[] logisticIds) {
            return new LogisticAch(this).导出物流信息主单以及履历(signStatus, isFilterItemNull, processIds, logisticIds);
        }
        public IQueryable<VirtualLogistic> GetLogisticsWithDetail(int? signStatus, bool isFilterItemNull) {
            return new LogisticAch(this).GetLogisticsWithDetail(signStatus, isFilterItemNull);
        }
    }
}
