﻿using System;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class LogisticsAch : DcsSerivceAchieveBase {
        public LogisticsAch(DcsDomainService domainService)
            : base(domainService) {
        }


        internal void UpdateLogisticsValidate(Logistic logistic) {
            var userInfo = Utils.GetCurrentUserInfo();
            logistic.ModifierId = userInfo.Id;
            logistic.ModifierName = userInfo.Name;
            logistic.ModifyTime = DateTime.Now;
        }

        public void InsertLogistics(Logistic logistic) {
            InsertToDatabase(logistic);

        }

        public void UpdateLogistics(Logistic logistic) {
            logistic.LogisticsDetails.Clear();
            UpdateToDatabase(logistic);
            var logisticsDetails = ChangeSet.GetAssociatedChanges(logistic, v => v.LogisticsDetails);
            foreach(LogisticsDetail logisticsDetail in logisticsDetails) {
                switch(ChangeSet.GetChangeOperation(logisticsDetail)) {
                    case ChangeOperation.Insert:
                        InsertToDatabase(logisticsDetail);
                        break;
                    case ChangeOperation.Update:
                        UpdateToDatabase(logisticsDetail);
                        break;
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(logisticsDetail);
                        break;
                }
            }
            this.UpdateLogisticsValidate(logistic);
        }

        public IQueryable<Logistic> 查询物流信息() {
            return ObjectContext.Logistics.OrderBy(r => r.Id).Include("LogisticsDetails");
        }

        public IQueryable<VirtualLogistic> 根据签收状态查询物流信息(int? signStatus, bool isFilterItemNull) {
            IQueryable<Logistic> logisties = ObjectContext.Logistics;
            var userInfo = Utils.GetCurrentUserInfo();
            var companyType = ObjectContext.Companies.FirstOrDefault(r => r.Id == userInfo.EnterpriseId).Type;
            //if(companyType != (int)DcsCompanyType.分公司)
            //    logisties = ObjectContext.Logistics.Where(r => r.ShippingCompanyNumber == userInfo.EnterpriseCode);

            if(signStatus.HasValue) {
                if(signStatus == (int)DcsPartsShippingOrderStatus.收货确认)
                    logisties = logisties.Where(r => ObjectContext.PartsShippingOrders.Any(so => so.Status == (int)DcsPartsShippingOrderStatus.收货确认 && r.ShippingCode == so.Code));
                else
                    logisties = logisties.Where(r => ObjectContext.PartsShippingOrders.Any(so => so.Status != (int)DcsPartsShippingOrderStatus.收货确认 && r.ShippingCode == so.Code));
            }
            if(companyType == (int)DcsCompanyType.物流公司) {
                var logistics = from l in logisties
                    join so in ObjectContext.PartsShippingOrders.Where(r => r.LogisticCompanyId == userInfo.EnterpriseId) on l.ShippingCode equals so.Code into tempT1
                    from t1 in tempT1.DefaultIfEmpty()
                    select new VirtualLogistic {
                        Id = l.Id,
                        ShippingCode = l.ShippingCode,
                        OrderCode = l.OrderCode,
                        ShippingCompanyName = l.ShippingCompanyName,
                        ShippingCompanyNumber = l.ShippingCompanyNumber,
                        ApproveTime = l.ApproveTime,
                        WarehouseName = l.WarehouseName,
                        ShippingDate = l.ShippingDate,
                        RequestedArrivalDate = l.RequestedArrivalDate,
                        ReceivingAddress = l.ReceivingAddress,
                        LogisticName = l.LogisticName,
                        TransportDriverPhone = l.TransportDriverPhone,
                        ModifierId = l.ModifierId,
                        ModifierName = l.ModifierName,
                        ModifyTime = l.ModifyTime,
                        CancelId = l.CancelId,
                        CancelName = l.CancelName,
                        CancelTime = l.CancelTime,
                        Status = l.Status,
                        ShippingMethod = l.ShippingMethod,
                        SignStatus = (t1.Status == (int)DcsPartsShippingOrderStatus.收货确认) ? 2 : 1
                    };
                return logistics.OrderBy(r => r.Id);
            } else {
                var logistics = from l in logisties
                                join so in ObjectContext.PartsShippingOrders.Where(r => r.LogisticCompanyId == userInfo.EnterpriseId) on l.ShippingCode equals so.Code into tempT1
                                from t1 in tempT1.DefaultIfEmpty()
                                select new VirtualLogistic {
                                    Id = l.Id,
                                    ShippingCode = l.ShippingCode,
                                    OrderCode = l.OrderCode,
                                    ShippingCompanyName = l.ShippingCompanyName,
                                    ShippingCompanyNumber = l.ShippingCompanyNumber,
                                    ApproveTime = l.ApproveTime,
                                    WarehouseName = l.WarehouseName,
                                    ShippingDate = l.ShippingDate,
                                    RequestedArrivalDate = l.RequestedArrivalDate,
                                    ReceivingAddress = l.ReceivingAddress,
                                    LogisticName = l.LogisticName,
                                    TransportDriverPhone = l.TransportDriverPhone,
                                    ModifierId = l.ModifierId,
                                    ModifierName = l.ModifierName,
                                    ModifyTime = l.ModifyTime,
                                    CancelId = l.CancelId,
                                    CancelName = l.CancelName,
                                    CancelTime = l.CancelTime,
                                    Status = l.Status,
                                    ShippingMethod = l.ShippingMethod,
                                    SignStatus = (t1.Status == (int)DcsPartsShippingOrderStatus.收货确认) ? 2 : 1
                                };
                var logistics2 = from l in logisties.Where(r => r.ShippingCompanyNumber == userInfo.EnterpriseCode)
                                join so in this.ObjectContext.PartsShippingOrders on l.ShippingCode equals so.Code into tempT1
                                from t1 in tempT1.DefaultIfEmpty()
                                select new VirtualLogistic {
                                    Id = l.Id,
                                    ShippingCode = l.ShippingCode,
                                    OrderCode = l.OrderCode,
                                    ShippingCompanyName = l.ShippingCompanyName,
                                    ShippingCompanyNumber = l.ShippingCompanyNumber,
                                    ApproveTime = l.ApproveTime,
                                    WarehouseName = l.WarehouseName,
                                    ShippingDate = l.ShippingDate,
                                    RequestedArrivalDate = l.RequestedArrivalDate,
                                    ReceivingAddress = l.ReceivingAddress,
                                    LogisticName = l.LogisticName,
                                    TransportDriverPhone = l.TransportDriverPhone,
                                    ModifierId = l.ModifierId,
                                    ModifierName = l.ModifierName,
                                    ModifyTime = l.ModifyTime,
                                    CancelId = l.CancelId,
                                    CancelName = l.CancelName,
                                    CancelTime = l.CancelTime,
                                    Status = l.Status,
                                    ShippingMethod = l.ShippingMethod,
                                    SignStatus = (t1.Status == (int)DcsPartsShippingOrderStatus.收货确认) ? 2 : 1
                                };
                return logistics.Union(logistics2).OrderBy(r => r.Id);
            }
        }
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:33
        //原分布类的函数全部转移到Ach                                                               
        public void InsertLogistics(Logistic logistic) {
            new LogisticsAch(this).InsertLogistics(logistic);
        }
        public void UpdateLogistics(Logistic logistic) {
            new LogisticsAch(this).UpdateLogistics(logistic);
        }
        public IQueryable<Logistic> 查询物流信息() {
            return new LogisticsAch(this).查询物流信息();
        }
        public IQueryable<VirtualLogistic> 根据签收状态查询物流信息(int? signStatus, bool isFilterItemNull) {
            return new LogisticsAch(this).根据签收状态查询物流信息(signStatus, isFilterItemNull);
        }
    }
}
