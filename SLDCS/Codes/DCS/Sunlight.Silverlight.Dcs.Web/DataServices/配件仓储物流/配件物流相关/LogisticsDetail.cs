﻿using System;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Quartz.Xml;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class LogisticsDetailAch : DcsSerivceAchieveBase {
        public LogisticsDetailAch(DcsDomainService domainService)
            : base(domainService) {
        }
        public IQueryable<LogisticsDetail> 查询物流信息清单() {
            return ObjectContext.LogisticsDetails.OrderBy(r => r.Id);
        }

        public IQueryable<LogisticsDetail> 查询已签收物流清单信息(int[] iDs) {
            return ObjectContext.LogisticsDetails.Where(r => iDs.Contains(r.Logisticsid) && r.Signatory != null);
        }

    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:33
        public IQueryable<LogisticsDetail> 查询物流信息清单() {
            return new LogisticsDetailAch(this).查询物流信息清单();
        }

        public IQueryable<LogisticsDetail> 查询已签收物流清单信息(int[] iDs) {
            return new LogisticsDetailAch(this).查询已签收物流清单信息(iDs);
        }
    }
}
