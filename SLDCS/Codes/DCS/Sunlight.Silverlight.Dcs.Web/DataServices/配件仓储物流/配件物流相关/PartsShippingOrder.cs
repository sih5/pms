﻿using System;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Quartz.Xml;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        internal void InsertPartsShippingOrderValidate(PartsShippingOrder partsShippingOrder) {
            var userInfo = Utils.GetCurrentUserInfo();
            partsShippingOrder.CreatorId = userInfo.Id;
            partsShippingOrder.CreatorName = userInfo.Name;
            partsShippingOrder.CreateTime = DateTime.Now;
            if(string.IsNullOrWhiteSpace(partsShippingOrder.Code) || partsShippingOrder.Code == GlobalVar.ASSIGNED_BY_SERVER)
                partsShippingOrder.Code = CodeGenerator.Generate("PartsShippingOrder", partsShippingOrder.ShippingCompanyCode);
        }

        private void UpdatePartsShippingOrderValidate(PartsShippingOrder partsShippingOrder) {
            var userInfo = Utils.GetCurrentUserInfo();
            partsShippingOrder.ModifierId = userInfo.Id;
            partsShippingOrder.ModifierName = userInfo.Name;
            partsShippingOrder.ModifyTime = DateTime.Now;
        }

        public void InsertPartsShippingOrder(PartsShippingOrder partsShippingOrder) {
            InsertToDatabase(partsShippingOrder);
            var partsShippingOrderRefs = ChangeSet.GetAssociatedChanges(partsShippingOrder, r => r.PartsShippingOrderRefs, ChangeOperation.Insert);
            foreach(PartsShippingOrderRef partsShippingOrderRef in partsShippingOrderRefs) {
                InsertToDatabase(partsShippingOrderRef);
            }
            var partsShippingOrderDetails = ChangeSet.GetAssociatedChanges(partsShippingOrder, r => r.PartsShippingOrderDetails, ChangeOperation.Insert);
            foreach(PartsShippingOrderDetail partsShippingOrderDetail in partsShippingOrderDetails) {
                InsertToDatabase(partsShippingOrderDetail);
            }
            InsertPartsShippingOrderValidate(partsShippingOrder);
        }

        public void UpdatePartsShippingOrder(PartsShippingOrder partsShippingOrder) {
            //删除与强绑定清单的关系
            partsShippingOrder.PartsShippingOrderDetails.Clear();
            partsShippingOrder.PartsShippingOrderRefs.Clear();
            UpdateToDatabase(partsShippingOrder);
            var partsShippingOrderDetails = ChangeSet.GetAssociatedChanges(partsShippingOrder, r => r.PartsShippingOrderDetails);
            foreach(PartsShippingOrderDetail partsShippingOrderDetail in partsShippingOrderDetails) {
                switch(ChangeSet.GetChangeOperation(partsShippingOrderDetail)) {
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(partsShippingOrderDetail);
                        break;
                    case ChangeOperation.Insert:
                        InsertToDatabase(partsShippingOrderDetail);
                        break;
                    case ChangeOperation.Update:
                        UpdateToDatabase(partsShippingOrderDetail);
                        break;
                }
                var partsShippingOrderRefs = ChangeSet.GetAssociatedChanges(partsShippingOrder, r => r.PartsShippingOrderRefs);
                foreach(PartsShippingOrderRef partsShippingOrderRef in partsShippingOrderRefs) {
                    switch(ChangeSet.GetChangeOperation(partsShippingOrderRef)) {
                        case ChangeOperation.Delete:
                            DeleteFromDatabase(partsShippingOrderRef);
                            break;
                        case ChangeOperation.Insert:
                            InsertToDatabase(partsShippingOrderRef);
                            break;
                        case ChangeOperation.Update:
                            UpdateToDatabase(partsShippingOrderRef);
                            break;
                    }
                    UpdatePartsShippingOrderValidate(partsShippingOrder);
                }

            }

        }

        public PartsShippingOrder GetPartsShippingOrderWithDetailsById(int id) {
            var dbPartsShippingOrder = ObjectContext.PartsShippingOrders.SingleOrDefault(e => e.Id == id);
            if(dbPartsShippingOrder != null) {
                var dbPartsShippingOrderDetails = ObjectContext.PartsShippingOrderDetails.Where(e => e.PartsShippingOrderId == id).ToArray();
                var partsShippingOrderRefs = ObjectContext.PartsShippingOrderRefs.Where(e => e.PartsShippingOrderId == id).ToArray();
                if(partsShippingOrderRefs.Any()) {
                    var refIds = partsShippingOrderRefs.Select(e => e.PartsOutboundBillId).ToArray();
                    var partsOutBounds = ObjectContext.PartsOutboundBills.Where(e => refIds.Contains(e.Id)).ToArray();
                }
            }
            return dbPartsShippingOrder;
        }

        public PartsShippingOrder 根据出库单查询发运单(int id) {
            var dbPartsShippingOrder = ObjectContext.PartsShippingOrders.SingleOrDefault(e => e.Id == id);
            if(dbPartsShippingOrder != null) {
                var dbPartsShippingOrderDetails = ObjectContext.PartsShippingOrderDetails.Where(e => e.PartsShippingOrderId == id).ToArray();
                var partsShippingOrderRefs = ObjectContext.PartsShippingOrderRefs.Where(e => e.PartsShippingOrderId == id).ToArray();
                if(partsShippingOrderRefs.Any()) {
                    var refIds = partsShippingOrderRefs.Select(e => e.PartsOutboundBillId).ToArray();
                    var partsOutBounds = ObjectContext.PartsOutboundBills.Where(e => refIds.Contains(e.Id)).ToArray();
                }
            }
            return dbPartsShippingOrder;
        }

        public IQueryable<PartsShippingOrder> 查询配件发运单(int? closedLoopStatus, string ERPSourceOrderCodeQuery, string PartsOutboundPlanCode) {
            var userInfo = Utils.GetCurrentUserInfo();
            IQueryable<PartsShippingOrder> partsShippingOrders = ObjectContext.PartsShippingOrders.Where(r => ObjectContext.WarehouseOperators.Any(v => v.OperatorId == userInfo.Id && v.WarehouseId == r.WarehouseId) || (r.ReceivingCompanyId == userInfo.EnterpriseId && r.Type!=(int)DcsPartsShippingOrderType.调拨));
            if(closedLoopStatus == 1) {
                //未闭环
                var statusArray = new[] {
                    (int)DcsPartsShippingOrderStatus.新建, (int)DcsPartsShippingOrderStatus.已发货, (int)DcsPartsShippingOrderStatus.待收货, (int)DcsPartsShippingOrderStatus.待提货
                };
                partsShippingOrders = partsShippingOrders.Where(r => statusArray.Contains(r.Status));
            }
            if(closedLoopStatus == 2) {
                //闭环 
                var statusArray = new[] {
                    (int)DcsPartsShippingOrderStatus.收货确认, (int)DcsPartsShippingOrderStatus.回执确认, (int)DcsPartsShippingOrderStatus.作废
                };
                partsShippingOrders = partsShippingOrders.Where(r => statusArray.Contains(r.Status));
            }
            if(PartsOutboundPlanCode != null) {
                partsShippingOrders = partsShippingOrders.Where(s => ObjectContext.PartsShippingOrderDetails.Any(d => d.PartsOutboundPlanCode == PartsOutboundPlanCode && s.Id == d.PartsShippingOrderId));
            }
            if(!string.IsNullOrEmpty(ERPSourceOrderCodeQuery)) {
                partsShippingOrders = partsShippingOrders.Where(r => ObjectContext.PartsSalesOrders.Any(x => x.Code == r.OriginalRequirementBillCode && x.ERPSourceOrderCode.Contains(ERPSourceOrderCodeQuery)));
            }
            partsShippingOrders = partsShippingOrders.Include("PartsShippingOrderDetails").Include("PartsSalesCategory").OrderBy(e => e.Status).ThenBy(e => e.RequestedArrivalDate);
            //var partsSale = (from a in partsShippingOrders
            //                 join b in ObjectContext.PartsShippingOrderDetails on a.Id equals b.PartsShippingOrderId
            //                 join c in ObjectContext.PartsOutboundPlans on b.PartsOutboundPlanId equals c.Id
            //                 join d in ObjectContext.PartsSalesOrders on c.OriginalRequirementBillId equals d.Id
            //                 select new {
            //                     a,
            //                     ContactPerson = d.ContactPerson,
            //                     ContactPhone = d.ContactPhone
            //                 }).ToList();
            //foreach(var item in partsSale) {
            //    item.a.ContactPerson = item.ContactPerson;
            //    item.a.ContactPhone = item.ContactPhone;
            //}
            return partsShippingOrders;
        }

        public IQueryable<PartsShippingOrder> 查询配件发运单New(int? closedLoopStatus, string ERPSourceOrderCodeQuery) {
            IQueryable<PartsShippingOrder> partsShippingOrders = ObjectContext.PartsShippingOrders;

            if(closedLoopStatus == 1) {
                //未闭环
                var statusArray = new[] {
                    (int)DcsPartsShippingOrderStatus.新建, (int)DcsPartsShippingOrderStatus.已发货, (int)DcsPartsShippingOrderStatus.待收货, (int)DcsPartsShippingOrderStatus.待提货
                };
                partsShippingOrders = partsShippingOrders.Where(r => statusArray.Contains(r.Status));
            }
            if(closedLoopStatus == 2) {
                //闭环 
                var statusArray = new[] {
                    (int)DcsPartsShippingOrderStatus.收货确认, (int)DcsPartsShippingOrderStatus.回执确认, (int)DcsPartsShippingOrderStatus.作废
                };
                partsShippingOrders = partsShippingOrders.Where(r => statusArray.Contains(r.Status));
            }
            if(!string.IsNullOrEmpty(ERPSourceOrderCodeQuery)) {
                partsShippingOrders = partsShippingOrders.Where(r => ObjectContext.PartsSalesOrders.Any(x => x.Code == r.OriginalRequirementBillCode && x.ERPSourceOrderCode.Contains(ERPSourceOrderCodeQuery)));
            }
            return partsShippingOrders.Include("PartsSalesCategory").OrderBy(e => e.Status).ThenBy(e => e.RequestedArrivalDate);
        }

        public IQueryable<PartsShippingOrder> 查询配件发运单仓库人员过滤(int? closedLoopStatus, string ERPSourceOrderCodeQuery) {
            var userInfo = Utils.GetCurrentUserInfo();
            IQueryable<PartsShippingOrder> partsShippingOrders = ObjectContext.PartsShippingOrders.Where(r => ObjectContext.WarehouseOperators.Any(v => v.OperatorId == userInfo.Id && v.WarehouseId == r.WarehouseId) || ObjectContext.Warehouses.Any(v => v.Id == r.WarehouseId && v.Type == (int)DcsWarehouseType.虚拟库 && v.Status == (int)DcsBaseDataStatus.有效));
            if(closedLoopStatus == 1) {
                //未闭环
                var statusArray = new[] {
                    (int)DcsPartsShippingOrderStatus.新建, (int)DcsPartsShippingOrderStatus.已发货, (int)DcsPartsShippingOrderStatus.待收货, (int)DcsPartsShippingOrderStatus.待提货
                };
                partsShippingOrders = partsShippingOrders.Where(r => statusArray.Contains(r.Status));
            }
            if(closedLoopStatus == 2) {
                //闭环 
                var statusArray = new[] {
                    (int)DcsPartsShippingOrderStatus.收货确认, (int)DcsPartsShippingOrderStatus.回执确认, (int)DcsPartsShippingOrderStatus.作废
                };
                partsShippingOrders = partsShippingOrders.Where(r => statusArray.Contains(r.Status));
            }
            if(!string.IsNullOrEmpty(ERPSourceOrderCodeQuery)) {
                partsShippingOrders = partsShippingOrders.Where(r => ObjectContext.PartsSalesOrders.Any(x => x.Code == r.OriginalRequirementBillCode && x.ERPSourceOrderCode.Contains(ERPSourceOrderCodeQuery)));
            }
            return partsShippingOrders.Include("PartsShippingOrderDetails").Include("PartsSalesCategory").OrderBy(e => e.Status).ThenBy(e => e.RequestedArrivalDate);
        }

        public IQueryable<PartsShippingOrder> GetPartsShippingOrdersByIds(int[] ids) {
            if(ids != null && ids.Length > 0)
                return ObjectContext.PartsShippingOrders.Where(t => ids.Contains(t.Id)).OrderBy(e => e.Id);
            return null;
        }

        [Update(UsingCustomMethod = true)]
        public void 发货确认(PartsShippingOrder partsshippingorder) {
            var dbRepairWorkOrder = ObjectContext.PartsShippingOrders.Where(r => r.Id == partsshippingorder.Id).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbRepairWorkOrder == null)
                throw new ValidationException(ErrorStrings.PartsShippingOrder_Validation26);
            if(dbRepairWorkOrder.Status != (int)DcsPartsShippingOrderStatus.新建)
                throw new ValidationException(ErrorStrings.PartsShippingOrder_Validation1);

            var logisticCompany = ObjectContext.LogisticCompanies.Where(v => v.Name == partsshippingorder.LogisticCompanyName).FirstOrDefault();
            if(logisticCompany != null) {
                partsshippingorder.LogisticCompanyId = logisticCompany.Id;
                partsshippingorder.LogisticCompanyCode = logisticCompany.Code;
            }

            var userInfo = Utils.GetCurrentUserInfo();
            partsshippingorder.Status = (int)DcsPartsShippingOrderStatus.已发货;
            partsshippingorder.ModifierId = userInfo.Id;
            partsshippingorder.ModifierName = userInfo.Name;
            partsshippingorder.ModifyTime = DateTime.Now;
            UpdatePartsShippingOrderValidate(partsshippingorder);
            UpdateToDatabase(partsshippingorder);
        }

        [Update(UsingCustomMethod = true)]
        public void 送达确认(PartsShippingOrder partsshippingorder) {
            var dbRepairWorkOrder = ObjectContext.PartsShippingOrders.Where(r => r.Id == partsshippingorder.Id).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbRepairWorkOrder == null)
                throw new ValidationException(ErrorStrings.PartsShippingOrder_Validation26);
            if(dbRepairWorkOrder.Status != (int)DcsPartsShippingOrderStatus.已发货)
                throw new ValidationException(ErrorStrings.PartsShippingOrder_Validation27);

            var userInfo = Utils.GetCurrentUserInfo();
            partsshippingorder.Status = partsshippingorder.ArrivalMode == (int)DcsArrivalMode.货运站 ? (int)DcsPartsShippingOrderStatus.待提货 : (int)DcsPartsShippingOrderStatus.待收货;
            partsshippingorder.ModifierId = userInfo.Id;
            partsshippingorder.ModifierName = userInfo.Name;
            partsshippingorder.ModifyTime = DateTime.Now;
            UpdatePartsShippingOrderValidate(partsshippingorder);
            UpdateToDatabase(partsshippingorder);
        }

        [Update(UsingCustomMethod = true)]
        public void 超期或电商运单流水反馈(PartsShippingOrder partsshippingorder) {
            var dbRepairWorkOrder = ObjectContext.PartsShippingOrders.Where(r => r.Id == partsshippingorder.Id).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbRepairWorkOrder == null)
                throw new ValidationException(ErrorStrings.PartsShippingOrder_Validation26);
            if(dbRepairWorkOrder.Status != (int)DcsPartsShippingOrderStatus.新建
             && dbRepairWorkOrder.Status != (int)DcsPartsShippingOrderStatus.已发货
             && dbRepairWorkOrder.Status != (int)DcsPartsShippingOrderStatus.待提货
             && dbRepairWorkOrder.Status != (int)DcsPartsShippingOrderStatus.待收货)
                throw new ValidationException(ErrorStrings.PartsShippingOrder_Validation28);

            var userInfo = Utils.GetCurrentUserInfo();
            if(!string.IsNullOrEmpty(dbRepairWorkOrder.FlowFeedback)) {
                partsshippingorder.FlowFeedback = dbRepairWorkOrder.FlowFeedback + partsshippingorder.FlowFeedback;
            }
            //partsshippingorder.FlowFeedback = string.Format("{0},{1},{2};\n", partsshippingorder.FlowFeedback, userInfo.Name, DateTime.Now.ToString());
            partsshippingorder.FlowFeedback = string.Format("{0},{1};\n", partsshippingorder.FlowFeedback, DateTime.Now.ToString());
            partsshippingorder.ModifierId = userInfo.Id;
            partsshippingorder.ModifierName = userInfo.Name;
            partsshippingorder.ModifyTime = DateTime.Now;
            UpdatePartsShippingOrderValidate(partsshippingorder);
            UpdateToDatabase(partsshippingorder);
        }
        [Update(UsingCustomMethod = true)]
        public void GPS定位器设备号登记(PartsShippingOrder partsshippingorder) {
            var dbRepairWorkOrder = ObjectContext.PartsShippingOrders.Where(r => r.Id == partsshippingorder.Id).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbRepairWorkOrder == null)
                throw new ValidationException(ErrorStrings.PartsShippingOrder_Validation26);

            var userInfo = Utils.GetCurrentUserInfo();

            partsshippingorder.ModifierId = userInfo.Id;
            partsshippingorder.ModifierName = userInfo.Name;
            partsshippingorder.ModifyTime = DateTime.Now;
            UpdatePartsShippingOrderValidate(partsshippingorder);
            UpdateToDatabase(partsshippingorder);
        }

        public IQueryable<TaskListDetail> 根据任务单号查询任务单信息(int taskId, int taskType) {
            if(taskType == 1) {
                var result = from bx in ObjectContext.BoxUpTaskDetails.Where(o => o.BoxUpTaskId == taskId)
                             select new TaskListDetail {
                                 Id = bx.Id,
                                 PartsOutboundPlanCode = bx.PartsOutboundPlanCode,
                                 SourceCode = bx.SourceCode,
                                 SparePartId = bx.SparePartId,
                                 SparePartCode = bx.SparePartCode,
                                 SparePartName = bx.SparePartName,
                                 SihCode = bx.SihCode,
                                 TaskQty = bx.BoxUpQty.Value,
                                 ContainerNumber = bx.ContainerNumber
                             };
                return result.OrderBy(o => o.PartsOutboundPlanCode);
            } else {
                var result = from ptd in ObjectContext.PickingTaskDetails.Where(o => o.PickingTaskId == taskId)
                             join sp in ObjectContext.SpareParts on ptd.SparePartId equals sp.Id
                             select new TaskListDetail {
                                 Id = ptd.Id,
                                 PartsOutboundPlanCode = ptd.PartsOutboundPlanCode,
                                 SourceCode = ptd.SourceCode,
                                 SparePartId = ptd.SparePartId,
                                 SparePartCode = ptd.SparePartCode,
                                 SparePartName = ptd.SparePartName,
                                 SihCode = sp.ReferenceCode,
                                 TaskQty = ptd.PickingQty.Value
                             };
                return result.OrderBy(o => o.PartsOutboundPlanCode);
            }
        }
    }
}
