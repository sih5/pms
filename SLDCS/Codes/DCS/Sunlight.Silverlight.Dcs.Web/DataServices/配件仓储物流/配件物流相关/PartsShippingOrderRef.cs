﻿using System.Linq;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsShippingOrderRefAch : DcsSerivceAchieveBase {
        public PartsShippingOrderRefAch(DcsDomainService domainService)
            : base(domainService) {
        }
        
        public void DeletePartsShippingOrderRef(PartsShippingOrderRef partsShippingOrderRef)
        {
            DeleteFromDatabase(partsShippingOrderRef);
        }
        public IQueryable<PartsShippingOrderRef> 查询发运单出库明细() {
            return ObjectContext.PartsShippingOrderRefs.Include("PartsOutboundBill").Include("PartsOutboundBill.PartsOutboundBillDetails").OrderBy(e => e.Id);
        }
        public IQueryable<ShippingOrderOutDetail> 查询发运单出库明细信息() {
            var tempStruct = from t in
                                 (from d in ObjectContext.PartsShippingOrders
                                  join e in ObjectContext.PartsShippingOrderDetails on d.Id equals e.PartsShippingOrderId
                                  select new {
                                      d.Id,
                                      e.SettlementPrice,
                                      e.ShippingAmount
                                  })
                             group t by t.Id into tempTable
                             select new {
                                 PartsShippingOrderId = tempTable.Key,
                                 TotalAmount = tempTable.Sum(r => r.SettlementPrice * r.ShippingAmount)
                             };
            IQueryable<ShippingOrderOutDetail> result = from a in ObjectContext.PartsShippingOrderRefs
                                                        join b in ObjectContext.PartsOutboundBills on a.PartsOutboundBillId equals b.Id
                                                        join c in tempStruct on a.PartsShippingOrderId equals c.PartsShippingOrderId
                                                        select new ShippingOrderOutDetail {
                                                            PartsShippingOrderRefId = a.Id,
                                                            PartsShippingOrderId = c.PartsShippingOrderId,
                                                            PartsOutboundBillId = b.Id,
                                                            PartsOutboundBillCode = b.Code,
                                                            WarehouseId = b.WarehouseId,
                                                            WarehouseCode = b.WarehouseCode,
                                                            WarehouseName = b.WarehouseName,
                                                            OutboundType = b.OutboundType,
                                                            PartsSalesOrderTypeName = b.PartsSalesOrderTypeName,
                                                            CreateTime = b.CreateTime,
                                                            ShippingMethod = b.ShippingMethod,
                                                            Remark = b.Remark,
                                                            TotalAmount = b.PartsOutboundBillDetails.Sum(detail => detail.OutboundAmount * detail.SettlementPrice),
                                                            OrderApproveComment = b.OrderApproveComment
                                                        };
            return result.OrderBy(e => e.PartsOutboundBillId);
        }

    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:33
        //原分布类的函数全部转移到Ach                                                               
        
        public void DeletePartsShippingOrderRef(PartsShippingOrderRef partsShippingOrderRef)
        {
            new PartsShippingOrderRefAch(this).DeletePartsShippingOrderRef(partsShippingOrderRef);
        }

        public IQueryable<PartsShippingOrderRef> 查询发运单出库明细() {
            return new PartsShippingOrderRefAch(this).查询发运单出库明细();
        }

        public IQueryable<ShippingOrderOutDetail> 查询发运单出库明细信息() {
            return new PartsShippingOrderRefAch(this).查询发运单出库明细信息();
        }
    }
}
