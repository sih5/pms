﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Web;
using System.Collections.Generic;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DealerPartsTransferOrderAch : DcsSerivceAchieveBase {
        public DealerPartsTransferOrderAch(DcsDomainService domainService)
            : base(domainService) {
        }

        internal void InsertDealerPartsTransferOrderValidate(DealerPartsTransferOrder dealerPartsTransferOrder) {
            if(string.IsNullOrWhiteSpace(dealerPartsTransferOrder.Code) || dealerPartsTransferOrder.Code == GlobalVar.ASSIGNED_BY_SERVER)
                dealerPartsTransferOrder.Code = CodeGenerator.Generate("DealerPartsTransferOrder", dealerPartsTransferOrder.DealerCode);
            var userInfo = Utils.GetCurrentUserInfo();
            dealerPartsTransferOrder.CreatorId = userInfo.Id;
            dealerPartsTransferOrder.CreatorName = userInfo.Name;
            dealerPartsTransferOrder.CreateTime = DateTime.Now;
        }

        internal void UpdateDealerPartsTransferOrderValidate(DealerPartsTransferOrder dealerPartsTransferOrder) {
            var userInfo = Utils.GetCurrentUserInfo();
            dealerPartsTransferOrder.ModifierId = userInfo.Id;
            dealerPartsTransferOrder.ModifierName = userInfo.Name;
            dealerPartsTransferOrder.ModifyTime = DateTime.Now;
        }

        public void InsertDealerPartsTransferOrder(DealerPartsTransferOrder dealerPartsTransferOrder) {
            InsertToDatabase(dealerPartsTransferOrder);
            var dealerPartsTransOrderDetails = ChangeSet.GetAssociatedChanges(dealerPartsTransferOrder, v => v.DealerPartsTransOrderDetails, ChangeOperation.Insert);
            foreach(DealerPartsTransOrderDetail dealerPartsTransOrderDetail in dealerPartsTransOrderDetails)
                InsertToDatabase(dealerPartsTransOrderDetail);
            this.InsertDealerPartsTransferOrderValidate(dealerPartsTransferOrder);
        }

        public void UpdateDealerPartsTransferOrder(DealerPartsTransferOrder dealerPartsTransferOrder) {
            dealerPartsTransferOrder.DealerPartsTransOrderDetails.Clear();
            UpdateToDatabase(dealerPartsTransferOrder);
            var dealerPartsTransOrderDetails = ChangeSet.GetAssociatedChanges(dealerPartsTransferOrder, v => v.DealerPartsTransOrderDetails);
            foreach(DealerPartsTransOrderDetail dealerPartsTransOrderDetail in dealerPartsTransOrderDetails) {
                //获取清单实体对象的操作类型
                switch(ChangeSet.GetChangeOperation(dealerPartsTransOrderDetail)) {
                    //新增操作
                    case ChangeOperation.Insert:
                        InsertToDatabase(dealerPartsTransOrderDetail);
                        break;
                    //更新操作
                    case ChangeOperation.Update:
                        UpdateToDatabase(dealerPartsTransOrderDetail);
                        break;
                    //删除操作
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(dealerPartsTransOrderDetail);
                        break;
                }
            }
            this.UpdateDealerPartsTransferOrderValidate(dealerPartsTransferOrder);
        }

        public DealerPartsTransferOrder GetDealerPartsTransferOrderById(int id) {
            var dealerPartsTransferOrder = this.ObjectContext.DealerPartsTransferOrders.SingleOrDefault(r => r.Id == id);
            if(dealerPartsTransferOrder != null) {
                var dealerPartsTransOrderDetails = this.ObjectContext.DealerPartsTransOrderDetails.Where(r => r.DealerPartsTransferOrderId == dealerPartsTransferOrder.Id).ToArray();
                var sparePartIds = dealerPartsTransOrderDetails.Select(r => r.SparePartId).ToArray();
                var dealerPartsStocks = this.ObjectContext.DealerPartsStocks.Where(r => sparePartIds.Contains(r.SparePartId) && r.SalesCategoryId == dealerPartsTransferOrder.NoWarrantyBrandId && r.DealerId == dealerPartsTransferOrder.DealerId).Select(r => new {
                    r.SparePartId,
                    r.Quantity
                }).ToArray();
                foreach(var dealerPartsTransOrderDetail in dealerPartsTransOrderDetails) {
                    dealerPartsTransOrderDetail.Stock = dealerPartsStocks.Where(r => r.SparePartId == dealerPartsTransOrderDetail.SparePartId).Sum(r => r.Quantity);
                }
            }
            return dealerPartsTransferOrder;
        }

        public List<VirtualDealerPartsTransOrderDetail> 查询待件清单的库存及价格(int repairOrderId, int? noWarrantyBrandId, int[] sparePartIds) {
            //var repairOrder = ObjectContext.RepairOrders.SingleOrDefault(r => r.Id == repairOrderId);
            //if(repairOrder == null) {
            //    throw new ValidationException("未找到对应维修单");
            //}
            //var sumMaterials = ObjectContext.RepairOrderFaultReasons.Where(r => r.RepairOrderId == repairOrderId).SelectMany(r => r.RepairOrderItemDetails).SelectMany(r => r.RepairOrderMaterialDetails).Where(r => r.NewPartsId != -1).GroupBy(r => new {
            //    r.NewPartsId,
            //    r.NewPartsCode,
            //    r.NewPartsName
            //}).Select(r => new {
            //    r.Key.NewPartsId,
            //    r.Key.NewPartsCode,
            //    r.Key.NewPartsName,
            //    Quantity = r.Sum(v => v.Quantity)
            //});
            //var dealerPartsStock = this.ObjectContext.DealerPartsStocks.Where(r => r.BranchId == repairOrder.BranchId && r.DealerCode == repairOrder.DealerCode && r.SalesCategoryId == repairOrder.PartsSalesCategoryId);
            //var partsSalesPrice = this.ObjectContext.PartsSalesPrices.Where(r => r.BranchId == repairOrder.BranchId && r.PartsSalesCategoryId == repairOrder.PartsSalesCategoryId);
            //if(sparePartIds != null && sparePartIds.Any()) {
            //    dealerPartsStock = dealerPartsStock.Where(r => sparePartIds.Contains(r.SparePartId));
            //    partsSalesPrice = partsSalesPrice.Where(r => sparePartIds.Contains(r.SparePartId));
            //}

            //var nowarranty = from a in sumMaterials
            //                 join b in dealerPartsStock on a.NewPartsId equals b.SparePartId into temp1
            //                 from stock in temp1.DefaultIfEmpty()
            //                 join c in partsSalesPrice on a.NewPartsId equals c.SparePartId into temp2
            //                 from price in temp2.DefaultIfEmpty()
            //                 where (a.Quantity - (stock == null ? 0 : stock.Quantity)) > 0
            //                 select new {
            //                     a.NewPartsId,
            //                     a.NewPartsName,
            //                     a.NewPartsCode,
            //                     WarrantyPrice = price.SalesPrice, //保内销售价格
            //                     Quantity = a.Quantity - (stock == null ? 0 : stock.Quantity), //待件数量
            //                     NoWarrantyPrice = 0, //保外销售价格
            //                     Stock = 0 //可用库存
            //                 };
            //if(noWarrantyBrandId != null && noWarrantyBrandId != default(int)) {
            //    //保外
            //    var dealerPartsStockNo = this.ObjectContext.DealerPartsStocks.Where(r => r.BranchId == repairOrder.BranchId && r.DealerCode == repairOrder.DealerCode && r.SalesCategoryId == noWarrantyBrandId);
            //    var partsSalesPriceNo = this.ObjectContext.PartsSalesPrices.Where(r => r.BranchId == repairOrder.BranchId && r.PartsSalesCategoryId == noWarrantyBrandId);
            //    if(sparePartIds != null && sparePartIds.Any()) {
            //        dealerPartsStockNo = dealerPartsStockNo.Where(r => sparePartIds.Contains(r.SparePartId));
            //        partsSalesPriceNo = partsSalesPriceNo.Where(r => sparePartIds.Contains(r.SparePartId));
            //    }
            //    return (from a in nowarranty
            //            join b in dealerPartsStockNo on a.NewPartsId equals b.SparePartId into temp1
            //            from stock in temp1.DefaultIfEmpty()
            //            join c in partsSalesPriceNo on a.NewPartsId equals c.SparePartId into temp2
            //            from price in temp2.DefaultIfEmpty()
            //            select new VirtualDealerPartsTransOrderDetail {
            //                PartsId = a.NewPartsId,
            //                PartsName = a.NewPartsName,
            //                PartsCode = a.NewPartsCode,
            //                WarrantyPrice = a.WarrantyPrice, //保内销售价格
            //                Quantity = a.Quantity, //待件数量
            //                NoWarrantyPrice = price.SalesPrice, //保外销售价格
            //                Stock = (stock == null ? 0 : stock.Quantity) //可用库存
            //            }).ToList();
            //}
            //return (from a in nowarranty
            //        select new VirtualDealerPartsTransOrderDetail {
            //            PartsId = a.NewPartsId,
            //            PartsName = a.NewPartsName,
            //            PartsCode = a.NewPartsCode,
            //            WarrantyPrice = a.WarrantyPrice, //保内销售价格
            //            Quantity = a.Quantity, //待件数量
            //            NoWarrantyPrice = a.NoWarrantyPrice, //保外销售价格
            //            Stock = a.Stock //可用库存
            //        }).ToList();
            return null;
        }
    }

    partial class DcsDomainService {
        public void InsertDealerPartsTransferOrder(DealerPartsTransferOrder dealerPartsTransferOrder) {
            new DealerPartsTransferOrderAch(this).InsertDealerPartsTransferOrder(dealerPartsTransferOrder);
        }
        public void UpdateDealerPartsTransferOrder(DealerPartsTransferOrder dealerPartsTransferOrder) {
            new DealerPartsTransferOrderAch(this).UpdateDealerPartsTransferOrder(dealerPartsTransferOrder);
        }
        public DealerPartsTransferOrder GetDealerPartsTransferOrderById(int id) {
            return new DealerPartsTransferOrderAch(this).GetDealerPartsTransferOrderById(id);
        }
        public List<VirtualDealerPartsTransOrderDetail> 查询待件清单的库存及价格(int repairOrderId, int? noWarrantyBrandId, int[] sparePartIds) {
            return new DealerPartsTransferOrderAch(this).查询待件清单的库存及价格(repairOrderId, noWarrantyBrandId, sparePartIds);
        }
    }
}
