﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunlight.Silverlight.Dcs.Web.Entities;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class FinancialInquiryReportAch : DcsSerivceAchieveBase {
        public FinancialInquiryReportAch(DcsDomainService domainService)
            : base(domainService) {
        }
        public IEnumerable<FinancialInquiryReport> 财务查询统计(int? warehouseId) {
            string SQL = @"select tmp.code as WarehouseCode,
       tmp.name as WarehouseName,
       tmp.id as WarehouseId,
       (select sum(b.outboundamount * b.costprice) cost
          from partsoutboundbill a
         inner join partsoutboundbilldetail b
            on a.id = b.partsoutboundbillid
         where a.settlementstatus = 2
           and a.storagecompanytype = 1
           and a.outboundtype = 1
           and a.warehouseid = tmp.id
         group by a.warehouseid) as SaleOutBound, --销售出库,
       (select sum(b.outboundamount * b.costprice) cost
          from partsoutboundbill a
         inner join partsoutboundbilldetail b
            on a.id = b.partsoutboundbillid
         where a.settlementstatus = 2
           and a.storagecompanytype = 1
           and a.outboundtype = 2
           and a.warehouseid = tmp.id
         group by a.warehouseid) as PartsPurchaseReturn,--采购退货,
       (select sum(b.outboundamount * b.costprice) cost
          from partsoutboundbill a
         inner join partsoutboundbilldetail b
            on a.id = b.partsoutboundbillid
         where a.settlementstatus = 2
           and a.storagecompanytype = 1
           and a.outboundtype = 3
           and a.warehouseid = tmp.id
         group by a.warehouseid) as  Internal,--内部领出,
       (select sum(b.inspectedquantity * b.costprice) cost
          from partsinboundcheckbill a
         inner join partsinboundcheckbilldetail b
            on a.id = b.partsinboundcheckbillid
         where a.settlementstatus = 2
           and a.storagecompanytype = 1
           and a.inboundtype = 1
           and a.warehouseid = tmp.id
         group by a.warehouseid) as PartsPurchaseInBound,--采购入库,
       (select sum(b.inspectedquantity * b.costprice) cost
          from partsinboundcheckbill a
         inner join partsinboundcheckbilldetail b
            on a.id = b.partsinboundcheckbillid
         where a.settlementstatus = 2
           and a.storagecompanytype = 1
           and a.inboundtype = 2
           and a.warehouseid = tmp.id
         group by a.warehouseid) as SaleReturn,--销售退货,
         Rownum
        from warehouse tmp
        where tmp.storagecompanytype = 1";
            if(warehouseId.HasValue) {
                SQL = SQL + " and tmp.id=" + warehouseId.Value;
            }
            var search = ObjectContext.ExecuteStoreQuery<FinancialInquiryReport>(SQL).ToList();
            return search;
        }
    }
    partial class DcsDomainService {
        public IEnumerable<FinancialInquiryReport> 财务查询统计(int? warehouseId) {
            return new FinancialInquiryReportAch(this).财务查询统计(warehouseId);
        }
    }
}
