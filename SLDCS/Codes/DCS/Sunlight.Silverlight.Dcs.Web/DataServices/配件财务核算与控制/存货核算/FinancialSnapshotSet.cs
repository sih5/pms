﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class FinancialSnapshotSetAch : DcsSerivceAchieveBase {
        internal void InsertFinancialSnapshotSetValidate(FinancialSnapshotSet financialSnapshotSet) {
            //判断Code是否需要生成编号
            var tempdate = financialSnapshotSet.SnapshotTime;
            var userinfo = Utils.GetCurrentUserInfo();
            var check = this.ObjectContext.FinancialSnapshotSets.Any(r => r.SnapshotTime != null && Math.Abs(EntityFunctions.DiffDays(r.SnapshotTime, tempdate).HasValue ? EntityFunctions.DiffDays(r.SnapshotTime, tempdate).Value : 0) < 1 && r.Status == (int)DcsSnapshoStatus.新建 && r.BranchId==userinfo.EnterpriseId);
            if(check) {
                throw new Exception(ErrorStrings.FinancialSnapshotSet_Validation3);
            }
            if(string.IsNullOrWhiteSpace(financialSnapshotSet.Code) || financialSnapshotSet.Code == GlobalVar.ASSIGNED_BY_SERVER)
                financialSnapshotSet.Code = CodeGenerator.Generate("FinancialSnapshotSet");
            var userInfo = Utils.GetCurrentUserInfo();
            financialSnapshotSet.CreatorId = userInfo.Id;
            financialSnapshotSet.CreatorName = userInfo.Name;
            financialSnapshotSet.CreateTime = DateTime.Now;
        }

        internal void UpdateFinancialSnapshotSetValidate(FinancialSnapshotSet financialSnapshotSet) {
            var dbFinancialSnapshotSet = ObjectContext.FinancialSnapshotSets.Where(v => v.Id == financialSnapshotSet.Id && v.Status == (int)DcsSnapshoStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbFinancialSnapshotSet == null) {
                throw new ValidationException(ErrorStrings.FinancialSnapshotSet_Validation1);
            }
            var check = ObjectContext.FinancialSnapshotSets.Where(r => r.Code == dbFinancialSnapshotSet.Code && r.Id != financialSnapshotSet.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(check != null) {
                throw new ValidationException(ErrorStrings.FinancialSnapshotSet_Validation2);
            }
            var userInfo = Utils.GetCurrentUserInfo();
            financialSnapshotSet.ModifierId = userInfo.Id;
            financialSnapshotSet.ModifierName = userInfo.Name;
            financialSnapshotSet.ModifyTime = DateTime.Now;
        }
        public void InsertFinancialSnapshotSet(FinancialSnapshotSet financialSnapshotSet) {
            InsertToDatabase(financialSnapshotSet);
            this.InsertFinancialSnapshotSetValidate(financialSnapshotSet);
        }
        public void UpdateFinancialSnapshotSet(FinancialSnapshotSet financialSnapshotSet) {
            UpdateToDatabase(financialSnapshotSet);
            this.UpdateFinancialSnapshotSetValidate(financialSnapshotSet);
        }
        public FinancialSnapshotSet GetFinancialSnapshotSetById(int id) {
            return ObjectContext.FinancialSnapshotSets.SingleOrDefault(e => e.Id == id);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:33
        //原分布类的函数全部转移到Ach                                                               

        public void InsertFinancialSnapshotSet(FinancialSnapshotSet financialSnapshotSet) {
            new FinancialSnapshotSetAch(this).InsertFinancialSnapshotSet(financialSnapshotSet);
        }

        public void UpdateFinancialSnapshotSet(FinancialSnapshotSet financialSnapshotSet) {
            new FinancialSnapshotSetAch(this).UpdateFinancialSnapshotSet(financialSnapshotSet);
        }

        public FinancialSnapshotSet GetFinancialSnapshotSetById(int id) {
            return new FinancialSnapshotSetAch(this).GetFinancialSnapshotSetById(id);
        }
    }
}
