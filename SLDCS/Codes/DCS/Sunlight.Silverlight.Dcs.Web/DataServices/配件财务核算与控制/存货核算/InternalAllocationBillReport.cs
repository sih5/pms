﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunlight.Silverlight.Dcs.Web.Entities;

namespace Sunlight.Silverlight.Dcs.Web {
    class InternalAllocationBillReportAch : DcsSerivceAchieveBase {
        public InternalAllocationBillReportAch(DcsDomainService domainService)
            : base(domainService) {
        }
        public IQueryable<InternalAllocationBillReport> 内部领用统计() {

            var internall = from c in ObjectContext.PartsOutboundBills
                            join a in ObjectContext.InternalAllocationBills on new {
                                Id = c.OriginalRequirementBillId,
                                c.OriginalRequirementBillType
                            } equals new {
                                a.Id,
                                OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.内部领出单
                            }
                            join d in ObjectContext.PartsRequisitionSettleRefs on new {
                                c.Id,
                                c.Code
                            } equals new {
                            Id=d.SourceId,
                            Code=d.SourceCode
                            }
                            into dd
                            from dp in dd.DefaultIfEmpty()
                            join f in ObjectContext.PartsRequisitionSettleBills.Where(r => r.Status != (int)DcsPartsRequisitionSettleBillStatus.作废) on dp.PartsRequisitionSettleBillId equals f.Id into fg
                            from ff in fg.DefaultIfEmpty()
                            join d in ObjectContext.PartsOutboundBillDetails on c.Id equals d.PartsOutboundBillId
                            select new InternalAllocationBillReport {
                                Id = d.Id,
                                PartsOutboundBillId = c.Id,
                                WarehouseId = c.WarehouseId,
                                WarehouseCode = c.WarehouseCode,
                                WarehouseName = a.WarehouseName,
                                PartsOutboundBillTime = c.CreateTime,
                                OrderType = "内部领出",
                                Type = a.Type,
                                SparePartCode = d.SparePartCode,
                                SparePartName = d.SparePartName,
                                OutboundAmount = d.OutboundAmount,
                                CostPrice = d.CostPrice,
                                CostPriceAll = d.CostPrice * d.OutboundAmount,
                                PartsRequisitionSettleBillCode = ff.Code,
                                PartsRequisitionSettleBillDate=ff.ApproveTime
                            };
            return internall.OrderBy(r => r.PartsOutboundBillTime);
        }
        public IEnumerable<InternalAllocationBillReport> 导出内部领用统计查询(int? warehouseId, string sparePartCode, DateTime? bPartsOutboundBillTime, DateTime? ePartsOutboundBillTime, DateTime? bPartsRequisitionSettleBillDate, DateTime? ePartsRequisitionSettleBillDate) {
            string SQL = @"SELECT Extent1.Id,
       Extent1.WarehouseId,
       Extent1.WarehouseCode,
       Extent1.CreateTime as PartsOutboundBillTime,
       Extent2.WarehouseName,
       Extent2.Type,
       Extent3.Id AS PartsOutboundBillId,
       Extent3.SparePartCode,
       Extent3.SparePartName,
       Extent3.OutboundAmount,
       Extent3.CostPrice,
      cast('内部领出' as varchar2(100)) AS OrderType,
       Extent3.CostPrice * Extent3.OutboundAmount AS CostPriceAll,
       pb.code as PartsRequisitionSettleBillCode,
       pb.ApproveTime as PartsRequisitionSettleBillDate
  FROM PartsOutboundBill Extent1
 INNER JOIN InternalAllocationBill Extent2
    ON (Extent1.OriginalRequirementBillId = Extent2.Id)
   AND (Extent1.OriginalRequirementBillType = 8)
 INNER JOIN PartsOutboundBillDetail Extent3
    ON Extent1.Id = Extent3.PartsOutboundBillId 
   left join PartsRequisitionSettleRef pf on Extent1.Id=pf.sourceid and pf.sourcecode =pf.sourcecode
    left join PartsRequisitionSettleBill pb on pf.partsrequisitionsettlebillid=pb.id and pb.status<>99
  where 1=1 ";
            if(warehouseId.HasValue) {
                SQL = SQL + " and Extent1.warehouseId=" + warehouseId.Value;
            }
            if(!string.IsNullOrEmpty(sparePartCode)){
                SQL = SQL + " and Extent3.sparePartCode like '%" + sparePartCode+"%'";
            }
            if(bPartsOutboundBillTime.HasValue && bPartsOutboundBillTime.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
                bPartsOutboundBillTime = null;
            }
            if(ePartsOutboundBillTime.HasValue && ePartsOutboundBillTime.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
                ePartsOutboundBillTime = null;
            }
            if(bPartsOutboundBillTime.HasValue) {
                SQL = SQL + " and Extent1.CreateTime>= to_date('" + bPartsOutboundBillTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if(ePartsOutboundBillTime.HasValue) {
                SQL = SQL + " and Extent1.CreateTime<= to_date('" + ePartsOutboundBillTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if(bPartsRequisitionSettleBillDate.HasValue) {
                SQL = SQL + " and pb.ApproveTime>= to_date('" + bPartsRequisitionSettleBillDate.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if(ePartsRequisitionSettleBillDate.HasValue) {
                SQL = SQL + " and pb.ApproveTime<= to_date('" + ePartsRequisitionSettleBillDate.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            var search = ObjectContext.ExecuteStoreQuery<InternalAllocationBillReport>(SQL).ToList();
            return search;
        }
    }
    partial class DcsDomainService {
        public IQueryable<InternalAllocationBillReport> 内部领用统计() {
            return new InternalAllocationBillReportAch(this).内部领用统计();
        }
        public IEnumerable<InternalAllocationBillReport> 导出内部领用统计查询(int? warehouseId, string sparePartCode, DateTime? bPartsOutboundBillTime, DateTime? ePartsOutboundBillTime, DateTime? bPartsRequisitionSettleBillDate, DateTime? ePartsRequisitionSettleBillDate) {
            return new InternalAllocationBillReportAch(this).导出内部领用统计查询(warehouseId, sparePartCode, bPartsOutboundBillTime, ePartsOutboundBillTime, bPartsRequisitionSettleBillDate,ePartsRequisitionSettleBillDate);
        }
    }
}
