﻿using System.Linq;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsHistoryStockAch : DcsSerivceAchieveBase {
        public PartsHistoryStockAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public IQueryable<PartsHistoryStock> GetPartsHistoryStocksWithSpartsParts(bool? greaterThanZero) {
            var partsHistoryStocks = ObjectContext.PartsHistoryStocks.Include("Branch").Include("Warehouse").Include("Company").Include("WarehouseArea").Include("WarehouseAreaCategory").Include("SparePart").Include("PartsSalesCategory");
            if(greaterThanZero.HasValue && greaterThanZero.Value)
                return partsHistoryStocks.Where(r => r.Quantity > 0).OrderBy(e => e.Id);
            if(greaterThanZero.HasValue && !greaterThanZero.Value)
                return partsHistoryStocks.Where(r => r.Quantity == 0).OrderBy(e => e.Id);
            return partsHistoryStocks.OrderBy(e => e.Id);
        }
        public IQueryable<PartsHistoryStockBill> HistoryStockSumSets(bool? greaterThanZero) {
            //var partsHistoryStocks = ObjectContext.PartsHistoryStocks.Include("Branch").Include("Warehouse").Include("Company").Include("WarehouseArea").Include("WarehouseAreaCategory").Include("SparePart").Include("PartsSalesCategory");
            var partsHistoryStocks = from partsHistoryStock in ObjectContext.PartsHistoryStocks
                                     join branch in ObjectContext.Branches on partsHistoryStock.BranchId equals branch.Id
                                     join warehouse in ObjectContext.Warehouses on partsHistoryStock.WarehouseId equals warehouse.Id
                                     join warehouseArea in ObjectContext.WarehouseAreas on partsHistoryStock.WarehouseAreaId equals warehouseArea.Id
                                     join warehouseAreaCategory in ObjectContext.WarehouseAreaCategories on partsHistoryStock.WarehouseAreaCategoryId equals warehouseAreaCategory.Id
                                     join partsSalesCategory in ObjectContext.PartsSalesCategories on partsHistoryStock.PartsSalesCategoryId equals partsSalesCategory.Id
                                     join company in ObjectContext.Companies on partsHistoryStock.StorageCompanyId equals company.Id
                                     select new {
                                         Id = partsHistoryStock.Id,
                                         BranchName = branch.Name,
                                         WarehouseName = warehouse.Name,
                                         WarehouseCode = warehouse.Code,
                                         PartsSalesCategoryName = partsSalesCategory.Name,
                                         StorageCompanyType = partsHistoryStock.StorageCompanyType,
                                         CompanyName = company.Name,
                                         CompanyCode = company.Code,
                                         PlannedPriceAmount = partsHistoryStock.PlannedPriceAmount ?? 0,
                                         CreateTime = partsHistoryStock.CreateTime,
                                         Quantity = partsHistoryStock.Quantity,
                                         WarehouseId = partsHistoryStock.WarehouseId,
                                         SnapshoTime = partsHistoryStock.SnapshoTime,
                                         PartsSalesCategoryId = partsHistoryStock.PartsSalesCategoryId
                                     };
            IQueryable<PartsHistoryStockBill> a = null;
            if(greaterThanZero.HasValue && greaterThanZero.Value) {
                a = partsHistoryStocks.Where(r => r.Quantity > 0).GroupBy(s => new {
                    s.BranchName,
                    s.WarehouseId,
                    s.WarehouseName,
                    s.WarehouseCode,
                    s.PartsSalesCategoryId,
                    s.PartsSalesCategoryName,
                    s.StorageCompanyType,
                    s.CompanyName,
                    s.CreateTime,
                    s.SnapshoTime,
                    s.CompanyCode
                }).Select(v => new PartsHistoryStockBill {
                    Id = v.Key.WarehouseId,
                    BranchName = v.Key.BranchName,
                    WarehouseName = v.Key.WarehouseName,
                    WarehouseCode = v.Key.WarehouseCode,
                    StorageCompanyType = v.Key.StorageCompanyType,
                    PartsSalesCategoryName = v.Key.PartsSalesCategoryName,
                    CompanyName = v.Key.CompanyName,
                    CompanyCode = v.Key.CompanyCode,
                    PlannedPriceAmount = v.Sum(o => o.PlannedPriceAmount),
                    CreateTime = v.Key.CreateTime,
                    WarehouseId = v.Key.WarehouseId,
                    SnapshoTime = v.Key.SnapshoTime,
                    PartsSalesCategoryId = v.Key.PartsSalesCategoryId
                }).OrderBy(s => new {
                    s.BranchName,
                    s.WarehouseName,
                    s.WarehouseCode,
                    s.PartsSalesCategoryName,
                    s.StorageCompanyType,
                    s.CompanyName,
                    s.CreateTime
                });
                return a;
            }
            if(greaterThanZero.HasValue && !greaterThanZero.Value) {
                a = partsHistoryStocks.Where(r => r.Quantity > 0).GroupBy(s => new {
                    s.BranchName,
                    s.WarehouseId,
                    s.WarehouseName,
                    s.WarehouseCode,
                    s.PartsSalesCategoryId,
                    s.PartsSalesCategoryName,
                    s.StorageCompanyType,
                    s.CompanyName,
                    s.CreateTime,
                    s.SnapshoTime,
                    s.CompanyCode
                }).Select(v => new PartsHistoryStockBill {
                    Id = v.Key.WarehouseId,
                    BranchName = v.Key.BranchName,
                    WarehouseName = v.Key.WarehouseName,
                    WarehouseCode = v.Key.WarehouseCode,
                    StorageCompanyType = v.Key.StorageCompanyType,
                    PartsSalesCategoryName = v.Key.PartsSalesCategoryName,
                    CompanyName = v.Key.CompanyName,
                    CompanyCode = v.Key.CompanyCode,
                    PlannedPriceAmount = v.Sum(o => o.PlannedPriceAmount),
                    CreateTime = v.Key.CreateTime,
                    WarehouseId = v.Key.WarehouseId,
                    SnapshoTime = v.Key.SnapshoTime,
                    PartsSalesCategoryId = v.Key.PartsSalesCategoryId
                }).OrderBy(s => new {
                    s.BranchName,
                    s.WarehouseName,
                    s.WarehouseCode,
                    s.PartsSalesCategoryName,
                    s.StorageCompanyType,
                    s.CompanyName,
                    s.CreateTime
                });
                return a;
            }
            return a;
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:33
        //原分布类的函数全部转移到Ach                                                               

        public IQueryable<PartsHistoryStock> GetPartsHistoryStocksWithSpartsParts(bool? greaterThanZero) {
            return new PartsHistoryStockAch(this).GetPartsHistoryStocksWithSpartsParts(greaterThanZero);
        }
                public IQueryable<PartsHistoryStockBill> HistoryStockSumSets(bool? greaterThanZero) {
            return new PartsHistoryStockAch(this).HistoryStockSumSets(greaterThanZero);
        }
    }
}
