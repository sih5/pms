﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsPlannedPriceAch : DcsSerivceAchieveBase {
        public PartsPlannedPriceAch(DcsDomainService domainService)
            : base(domainService) {
        }

        internal void InsertPartsPlannedPriceValidate(PartsPlannedPrice partsPlannedPrice) {
            var dbPartsPlannedPrice = ObjectContext.PartsPlannedPrices.Where(r => r.OwnerCompanyId == partsPlannedPrice.OwnerCompanyId && r.PartsSalesCategoryId == partsPlannedPrice.PartsSalesCategoryId && r.SparePartId == partsPlannedPrice.SparePartId).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbPartsPlannedPrice != null)
                throw new ValidationException(ErrorStrings.PartsPlannedPrice_Validation1);
            var userInfo = Utils.GetCurrentUserInfo();
            partsPlannedPrice.CreatorId = userInfo.Id;
            partsPlannedPrice.CreatorName = userInfo.Name;
            partsPlannedPrice.CreateTime = DateTime.Now;
        }
        internal void UpdatePartsPlannedPriceValidate(PartsPlannedPrice partsPlannedPrice) {
            var userInfo = Utils.GetCurrentUserInfo();
            partsPlannedPrice.ModifierId = userInfo.Id;
            partsPlannedPrice.ModifierName = userInfo.Name;
            partsPlannedPrice.ModifyTime = DateTime.Now;
        }
        public void InsertPartsPlannedPrice(PartsPlannedPrice partsPlannedPrice) {
            InsertToDatabase(partsPlannedPrice);
            this.InsertPartsPlannedPriceValidate(partsPlannedPrice);
        }
        public void UpdatePartsPlannedPrice(PartsPlannedPrice partsPlannedPrice) {
            UpdateToDatabase(partsPlannedPrice);
            this.UpdatePartsPlannedPriceValidate(partsPlannedPrice);
        }
        public IQueryable<PartsPlannedPrice> 查询配件计划价() {
            return ObjectContext.PartsPlannedPrices.Include("SparePart").OrderBy(e => e.Id);
        }
        public IQueryable<PartsPlannedPriceWithStockQuantity> 获取配件变动前价格(int partsSalesCategoryId, int[] partsids) {
            // return ObjectContext.PartsPlannedPrices.Where(r => partsids.Contains(r.SparePartId) && r.PartsSalesCategoryId == partsSalesCategoryId).OrderBy(e => e.Id);
            #region
            #region sql
            /*
                其中partssalescategoryid = 1,sparepartid in (1, 2, 3)是参数
                with 
                temptable1 as
                 (select partsoutboundbill.originalrequirementbillid,
                         partsoutboundbill.originalrequirementbillcode,
                         partsoutboundbilldetail.sparepartid,
                         sum(partsoutboundbilldetail.outboundamount) as totaloutquantity
                    from partsoutboundbill
                   inner join partsoutboundbilldetail
                      on partsoutboundbill.id = partsoutboundbilldetail.partsoutboundbillid
                   where partsoutboundbill.outboundtype = 4
                     and partsoutboundbill.originalrequirementbilltype = 6
                   group by partsoutboundbill.originalrequirementbillid,
                            partsoutboundbill.originalrequirementbillcode,
                            partsoutboundbilldetail.sparepartid),
                ------------------------------------------------------    
                temptable2 as
                 (select partsinboundcheckbill.originalrequirementbillid,
                         partsinboundcheckbill.originalrequirementbillcode,
                         partsinboundcheckbilldetail.sparepartid,
                         sum(partsinboundcheckbilldetail.inspectedquantity) as totalinquantity
                    from partsinboundcheckbill
                   inner join partsinboundcheckbilldetail
                      on partsinboundcheckbill.id =
                         partsinboundcheckbilldetail.partsinboundcheckbillid
                   where partsinboundcheckbill.inboundtype = 3
                     and partsinboundcheckbill.originalrequirementbilltype = 6
                   group by partsinboundcheckbill.originalrequirementbillid,
                            partsinboundcheckbill.originalrequirementbillcode,
                            partsinboundcheckbilldetail.sparepartid),
                --------------------------------------------------------
                temptable3 as
                 (select sparepartid, tempquantity, sum(tempquantity) as totalquantity
                    from (select t1.sparepartid,
                                 (t1.totaloutquantity - nvl(t2.totalinquantity, 0)) as tempquantity,
                                 'onroad' as flag
                            from partstransferorder a
                           inner join salesunitaffiwarehouse b
                              on a.destwarehouseid = b.warehouseid
                           inner join salesunit c
                              on b.salesunitid = c.id
                           inner join temptable1 t1
                              on a.id = t1.originalrequirementbillid
                            left join temptable2 t2
                              on t1.originalrequirementbillid = t2.originalrequirementbillid
                             and t1.sparepartid = t2.sparepartid
                           where a.storagecompanytype = 1
                             and t1.sparepartid in (1, 2, 3)
                             and c.partssalescategoryid = 1
                          union
                          select a.partid as sparepartid,
                                 nvl(a.quantity, 0) as tempquantity,
                                 'inwarehouse' as flag
                            from partsstock a
                           inner join salesunitaffiwarehouse
                              on a.warehouseid = salesunitaffiwarehouse.warehouseid
                           inner join salesunit
                              on salesunitaffiwarehouse.salesunitid = salesunit.id
                           where a.storagecompanytype = 1
                             and a.partid in (1, 2, 3)
                             and salesunit.partssalescategoryid = 1)
                   group by sparepartid, tempquantity),
                ------------------------------------------------------   
                tempTable4 as
                 (select sparepartid, plannedprice
                    from partsplannedprice
                   where sparepartid in (1, 2, 3)
                     and partssalescategoryid = 1)
                ------------------------------------------------------
                select t3.sparepartid,
                       t3.totalquantity,
                       nvl(t4.plannedprice, 0) as plannedprice
                  from temptable3 t3
                  left join temptable4 t4
                    on t3.sparepartid = t4.sparepartid;
            */
            #endregion
            var tempQuery1 = from a in ObjectContext.PartsOutboundBills
                             from b in a.PartsOutboundBillDetails
                             where a.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件调拨单 && a.OutboundType == (int)DcsPartsOutboundType.配件调拨
                             select new {
                                 a.OriginalRequirementBillId,
                                 a.OriginalRequirementBillCode,
                                 b.SparePartId,
                                 b.OutboundAmount
                             };
            var tempGroup1 = from a in tempQuery1
                             group a by new {
                                 a.OriginalRequirementBillId,
                                 a.OriginalRequirementBillCode,
                                 a.SparePartId
                             }
                                 into tempTable
                             select new {
                                 tempTable.Key.OriginalRequirementBillId,
                                 tempTable.Key.OriginalRequirementBillCode,
                                 tempTable.Key.SparePartId,
                                 totalOutQuantity = tempTable.Sum(r => r.OutboundAmount)
                             };
            var tempQuery2 = from a in ObjectContext.PartsInboundCheckBills
                             from b in a.PartsInboundCheckBillDetails
                             where a.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件调拨单 && a.InboundType == (int)DcsPartsInboundType.配件调拨
                             select new {
                                 a.OriginalRequirementBillId,
                                 a.OriginalRequirementBillCode,
                                 b.SparePartId,
                                 b.InspectedQuantity
                             };
            var tempGroup2 = from a in tempQuery2
                             group a by new {
                                 a.OriginalRequirementBillId,
                                 a.OriginalRequirementBillCode,
                                 a.SparePartId
                             }
                                 into tempTable
                             select new {
                                 tempTable.Key.OriginalRequirementBillId,
                                 tempTable.Key.OriginalRequirementBillCode,
                                 tempTable.Key.SparePartId,
                                 totalInQuantity = (int?)tempTable.Sum(r => r.InspectedQuantity)
                             };
            var tempQueryForStockOnRoad = from a in tempGroup1
                                          join b in tempGroup2 on new {
                                              a.OriginalRequirementBillId,
                                              a.OriginalRequirementBillCode,
                                              a.SparePartId
                                          } equals new {
                                              b.OriginalRequirementBillId,
                                              b.OriginalRequirementBillCode,
                                              b.SparePartId
                                          } into tempTable
                                          from t in tempTable.DefaultIfEmpty()
                                          select new {
                                              a.OriginalRequirementBillId,
                                              a.OriginalRequirementBillCode,
                                              a.SparePartId,
                                              OnRoadQuantity = a.totalOutQuantity - (t.totalInQuantity ?? 0)
                                          };
            //这里没过滤状态
            var onRoadQuery = from a in ObjectContext.PartsTransferOrders.Where(r => r.StorageCompanyType == (int)DcsCompanyType.分公司)
                              join b in ObjectContext.SalesUnitAffiWarehouses on a.DestWarehouseId equals b.WarehouseId
                              join c in ObjectContext.SalesUnits on b.SalesUnitId equals c.Id
                              join d in tempQueryForStockOnRoad on a.Id equals d.OriginalRequirementBillId
                              where c.PartsSalesCategoryId == partsSalesCategoryId && partsids.Contains(d.SparePartId)
                              select new {
                                  IdOfPart = d.SparePartId,
                                  TempQuantity = d.OnRoadQuantity,
                                  Flag = 1
                              };
            var inStockQuery = from a in ObjectContext.PartsStocks.Where(r => r.StorageCompanyType == (int)DcsCompanyType.分公司)
                               join b in ObjectContext.SalesUnitAffiWarehouses on a.WarehouseId equals b.WarehouseId
                               join c in ObjectContext.SalesUnits on b.SalesUnitId equals c.Id
                               where c.PartsSalesCategoryId == partsSalesCategoryId && partsids.Contains(a.PartId)
                               select new {
                                   IdOfPart = a.PartId,
                                   TempQuantity = a.Quantity,
                                   Flag = 2
                               };
            var allQuantityQuery = from a in (onRoadQuery.Union(inStockQuery))
                                   group a by new {
                                       a.IdOfPart
                                   }
                                       into tempTable
                                   select new {
                                       SparePartId = tempTable.Key.IdOfPart,
                                       TotalQuantity = tempTable.Sum(r => r.TempQuantity)
                                   };
            var tempPlannedPriceQuery = from a in ObjectContext.PartsPlannedPrices
                                        where partsids.Contains(a.SparePartId) && a.PartsSalesCategoryId == partsSalesCategoryId
                                        select new {
                                            a.SparePartId,
                                            PlannedPrice = (decimal)a.PlannedPrice
                                        };
            var resultQuery = from a in tempPlannedPriceQuery
                              join b in allQuantityQuery on a.SparePartId equals b.SparePartId into tempTable
                              from t in tempTable.DefaultIfEmpty()
                              select new PartsPlannedPriceWithStockQuantity {
                                  SparePartId = a.SparePartId,
                                  TotalQuantity = t.TotalQuantity,
                                  PlannedPrice = a.PlannedPrice
                              };
            return resultQuery;
            #endregion
        }

        public IQueryable<PartsPlannedPriceWithStockQuantity> 获取配件变动前价格_计划价申请单清单(int partsSalesCategoryId, int plannedPriceAppId) {
            // return ObjectContext.PartsPlannedPrices.Where(r => partsids.Contains(r.SparePartId) && r.PartsSalesCategoryId == partsSalesCategoryId).OrderBy(e => e.Id);
            #region
            #region sql
            /*
                其中partssalescategoryid = 1,sparepartid in (1, 2, 3)是参数
                with 
                temptable1 as
                 (select partsoutboundbill.originalrequirementbillid,
                         partsoutboundbill.originalrequirementbillcode,
                         partsoutboundbilldetail.sparepartid,
                         sum(partsoutboundbilldetail.outboundamount) as totaloutquantity
                    from partsoutboundbill
                   inner join partsoutboundbilldetail
                      on partsoutboundbill.id = partsoutboundbilldetail.partsoutboundbillid
                   where partsoutboundbill.outboundtype = 4
                     and partsoutboundbill.originalrequirementbilltype = 6
                   group by partsoutboundbill.originalrequirementbillid,
                            partsoutboundbill.originalrequirementbillcode,
                            partsoutboundbilldetail.sparepartid),
                ------------------------------------------------------    
                temptable2 as
                 (select partsinboundcheckbill.originalrequirementbillid,
                         partsinboundcheckbill.originalrequirementbillcode,
                         partsinboundcheckbilldetail.sparepartid,
                         sum(partsinboundcheckbilldetail.inspectedquantity) as totalinquantity
                    from partsinboundcheckbill
                   inner join partsinboundcheckbilldetail
                      on partsinboundcheckbill.id =
                         partsinboundcheckbilldetail.partsinboundcheckbillid
                   where partsinboundcheckbill.inboundtype = 3
                     and partsinboundcheckbill.originalrequirementbilltype = 6
                   group by partsinboundcheckbill.originalrequirementbillid,
                            partsinboundcheckbill.originalrequirementbillcode,
                            partsinboundcheckbilldetail.sparepartid),
                --------------------------------------------------------
                temptable3 as
                 (select sparepartid, tempquantity, sum(tempquantity) as totalquantity
                    from (select t1.sparepartid,
                                 (t1.totaloutquantity - nvl(t2.totalinquantity, 0)) as tempquantity,
                                 'onroad' as flag
                            from partstransferorder a
                           inner join salesunitaffiwarehouse b
                              on a.destwarehouseid = b.warehouseid
                           inner join salesunit c
                              on b.salesunitid = c.id
                           inner join temptable1 t1
                              on a.id = t1.originalrequirementbillid
                            left join temptable2 t2
                              on t1.originalrequirementbillid = t2.originalrequirementbillid
                             and t1.sparepartid = t2.sparepartid
                           where a.storagecompanytype = 1
                             and t1.sparepartid in (1, 2, 3)
                             and c.partssalescategoryid = 1
                          union
                          select a.partid as sparepartid,
                                 nvl(a.quantity, 0) as tempquantity,
                                 'inwarehouse' as flag
                            from partsstock a
                           inner join salesunitaffiwarehouse
                              on a.warehouseid = salesunitaffiwarehouse.warehouseid
                           inner join salesunit
                              on salesunitaffiwarehouse.salesunitid = salesunit.id
                           where a.storagecompanytype = 1
                             and a.partid in (1, 2, 3)
                             and salesunit.partssalescategoryid = 1)
                   group by sparepartid, tempquantity),
                ------------------------------------------------------   
                tempTable4 as
                 (select sparepartid, plannedprice
                    from partsplannedprice
                   where sparepartid in (1, 2, 3)
                     and partssalescategoryid = 1)
                ------------------------------------------------------
                select t3.sparepartid,
                       t3.totalquantity,
                       nvl(t4.plannedprice, 0) as plannedprice
                  from temptable3 t3
                  left join temptable4 t4
                    on t3.sparepartid = t4.sparepartid;
            */
            #endregion
            var tempQuery1 = from a in ObjectContext.PartsOutboundBills
                             from b in a.PartsOutboundBillDetails
                             where a.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件调拨单 && a.OutboundType == (int)DcsPartsOutboundType.配件调拨
                             select new {
                                 a.OriginalRequirementBillId,
                                 a.OriginalRequirementBillCode,
                                 b.SparePartId,
                                 b.OutboundAmount
                             };
            var tempGroup1 = from a in tempQuery1
                             group a by new {
                                 a.OriginalRequirementBillId,
                                 a.OriginalRequirementBillCode,
                                 a.SparePartId
                             }
                                 into tempTable
                             select new {
                                 tempTable.Key.OriginalRequirementBillId,
                                 tempTable.Key.OriginalRequirementBillCode,
                                 tempTable.Key.SparePartId,
                                 totalOutQuantity = tempTable.Sum(r => r.OutboundAmount)
                             };
            var tempQuery2 = from a in ObjectContext.PartsInboundCheckBills
                             from b in a.PartsInboundCheckBillDetails
                             where a.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件调拨单 && a.InboundType == (int)DcsPartsInboundType.配件调拨
                             select new {
                                 a.OriginalRequirementBillId,
                                 a.OriginalRequirementBillCode,
                                 b.SparePartId,
                                 b.InspectedQuantity
                             };
            var tempGroup2 = from a in tempQuery2
                             group a by new {
                                 a.OriginalRequirementBillId,
                                 a.OriginalRequirementBillCode,
                                 a.SparePartId
                             }
                                 into tempTable
                             select new {
                                 tempTable.Key.OriginalRequirementBillId,
                                 tempTable.Key.OriginalRequirementBillCode,
                                 tempTable.Key.SparePartId,
                                 totalInQuantity = tempTable.Sum(r => r.InspectedQuantity)
                             };
            var tempQueryForStockOnRoad = from a in tempGroup1
                                          join b in tempGroup2 on new {
                                              a.OriginalRequirementBillId,
                                              a.OriginalRequirementBillCode,
                                              a.SparePartId
                                          } equals new {
                                              b.OriginalRequirementBillId,
                                              b.OriginalRequirementBillCode,
                                              b.SparePartId
                                          } into tempTable
                                          from t in tempTable.DefaultIfEmpty()
                                          select new {
                                              a.OriginalRequirementBillId,
                                              a.OriginalRequirementBillCode,
                                              a.SparePartId,
                                              OnRoadQuantity = a.totalOutQuantity - t.totalInQuantity
                                          };
            //这里没过滤状态
            var onRoadQuery = from t in ObjectContext.PlannedPriceApps.Where(r => r.Id == plannedPriceAppId)
                              join t1 in ObjectContext.PlannedPriceAppDetails on t.Id equals t1.PlannedPriceAppId
                              join d in tempQueryForStockOnRoad on t1.SparePartId equals d.SparePartId
                              join a in ObjectContext.PartsTransferOrders.Where(r => r.StorageCompanyType == (int)DcsCompanyType.分公司) on d.OriginalRequirementBillId equals a.Id
                              join b in ObjectContext.SalesUnitAffiWarehouses on a.DestWarehouseId equals b.WarehouseId
                              join c in ObjectContext.SalesUnits on b.SalesUnitId equals c.Id
                              where c.PartsSalesCategoryId == partsSalesCategoryId
                              select new {
                                  IdOfPart = d.SparePartId,
                                  TempQuantity = d.OnRoadQuantity,
                                  Flag = 1
                              };
            var inStockQuery = from t in ObjectContext.PlannedPriceApps.Where(r => r.Id == plannedPriceAppId)
                               join t1 in ObjectContext.PlannedPriceAppDetails on t.Id equals t1.PlannedPriceAppId
                               join a in ObjectContext.PartsStocks.Where(r => r.StorageCompanyType == (int)DcsCompanyType.分公司) on t1.SparePartId equals a.PartId
                               join b in ObjectContext.SalesUnitAffiWarehouses on a.WarehouseId equals b.WarehouseId
                               join c in ObjectContext.SalesUnits on b.SalesUnitId equals c.Id
                               where c.PartsSalesCategoryId == partsSalesCategoryId
                               select new {
                                   IdOfPart = a.PartId,
                                   TempQuantity = a.Quantity,
                                   Flag = 2
                               };
            var allQuantityQuery = from a in (onRoadQuery.Union(inStockQuery))
                                   group a by new {
                                       a.IdOfPart
                                   }
                                       into tempTable
                                   select new {
                                       SparePartId = tempTable.Key.IdOfPart,
                                       TotalQuantity = tempTable.Sum(r => r.TempQuantity)
                                   };
            var tempPlannedPriceQuery = from t in ObjectContext.PlannedPriceApps.Where(r => r.Id == plannedPriceAppId)
                                        join t1 in ObjectContext.PlannedPriceAppDetails on t.Id equals t1.PlannedPriceAppId
                                        join a in ObjectContext.PartsPlannedPrices on t1.SparePartId equals a.SparePartId
                                        where a.PartsSalesCategoryId == partsSalesCategoryId
                                        select new {
                                            a.SparePartId,
                                            PlannedPrice = (decimal?)a.PlannedPrice
                                        };
            var resultQuery = from a in tempPlannedPriceQuery
                              join b in allQuantityQuery on a.SparePartId equals b.SparePartId into tempTable
                              from t in tempTable.DefaultIfEmpty()
                              select new PartsPlannedPriceWithStockQuantity {
                                  SparePartId = a.SparePartId,
                                  TotalQuantity = t.TotalQuantity,
                                  PlannedPrice = a.PlannedPrice ?? 0
                              };
            return resultQuery;
            #endregion
        }
        [Query(HasSideEffects = true)]
        public IQueryable<PartsPlannedPrice> GetPartsPlannedPriceBySparePartId(int[] ids, int partsSalesCategoryId) {
            return ObjectContext.PartsPlannedPrices.Where(entity => ids.Contains(entity.SparePartId) && entity.PartsSalesCategoryId == partsSalesCategoryId);
        }

        [Query(HasSideEffects = true)]
        public IQueryable<GetAllPartsPlannedPrice> 查询配件计划价格品牌(string Code, string Name, string PartsSalesCategory, DateTime? startTime, DateTime? endTime) {
            IQueryable<GetAllPartsPlannedPrice> partsPlannedPrices = ObjectContext.GetAllPartsPlannedPrices;
            if(!string.IsNullOrEmpty(Code)) {
                partsPlannedPrices = partsPlannedPrices.Where(r => Code.Contains(r.Code));
            }
            if(!string.IsNullOrEmpty(Name)) {
                partsPlannedPrices = partsPlannedPrices.Where(r => Name.Contains(r.Name));
            }
            if(!string.IsNullOrEmpty(PartsSalesCategory)) {
                var partsSalesCategory = ObjectContext.PartsSalesCategoryViews.Where(r => r.BranchCode == PartsSalesCategory).FirstOrDefault();
                if(partsSalesCategory != null)
                    partsPlannedPrices = partsPlannedPrices.Where(r => r.PartsSalesCategoryName == partsSalesCategory.Name);
            }
            if(startTime.ToString() != "0001/1/1 0:00:00" && !string.IsNullOrEmpty(startTime.ToString())) {
                partsPlannedPrices = partsPlannedPrices.Where(r => r.CreateTime >= startTime);
            }
            if(endTime.ToString() != "0001/1/1 0:00:00" && !string.IsNullOrEmpty(endTime.ToString())) {
                partsPlannedPrices = partsPlannedPrices.Where(r => r.CreateTime <= endTime);
            }
            return partsPlannedPrices.OrderBy(r => r.Code);
        }


    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:33
        //原分布类的函数全部转移到Ach                                                               

        public void InsertPartsPlannedPrice(PartsPlannedPrice partsPlannedPrice) {
            new PartsPlannedPriceAch(this).InsertPartsPlannedPrice(partsPlannedPrice);
        }

        public void UpdatePartsPlannedPrice(PartsPlannedPrice partsPlannedPrice) {
            new PartsPlannedPriceAch(this).UpdatePartsPlannedPrice(partsPlannedPrice);
        }

        public IQueryable<PartsPlannedPrice> 查询配件计划价() {
            return new PartsPlannedPriceAch(this).查询配件计划价();
        }

        public IQueryable<PartsPlannedPriceWithStockQuantity> 获取配件变动前价格(int partsSalesCategoryId, int[] partsids) {
            return new PartsPlannedPriceAch(this).获取配件变动前价格(partsSalesCategoryId, partsids);
        }


        public IQueryable<PartsPlannedPriceWithStockQuantity> 获取配件变动前价格_计划价申请单清单(int partsSalesCategoryId, int plannedPriceAppId) {
            return new PartsPlannedPriceAch(this).获取配件变动前价格_计划价申请单清单(partsSalesCategoryId, plannedPriceAppId);
        }


        [Query(HasSideEffects = true)]
        public IQueryable<PartsPlannedPrice> GetPartsPlannedPriceBySparePartId(int[] ids, int partsSalesCategoryId) {
            return new PartsPlannedPriceAch(this).GetPartsPlannedPriceBySparePartId(ids, partsSalesCategoryId);
        }

        [Query(HasSideEffects = true)]
        public IQueryable<GetAllPartsPlannedPrice> 查询配件计划价格品牌(string Code, string Name, string PartsSalesCategory, DateTime? startTime, DateTime? endTime) {
            return new PartsPlannedPriceAch(this).查询配件计划价格品牌(Code, Name, PartsSalesCategory, startTime, endTime);
        }
    }
}
