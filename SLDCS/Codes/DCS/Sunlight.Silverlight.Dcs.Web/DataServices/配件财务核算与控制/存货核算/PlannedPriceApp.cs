﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PlannedPriceAppAch : DcsSerivceAchieveBase {
        internal void InsertPlannedPriceAppValidate(PlannedPriceApp plannedPriceApp) {
            //增加校验新增、修改、导入时，同一个图号且状态=新增，只能存在一张申请单，若已经存在新增状态的申请单，则报错：XX图号已经存在未审核的计划价申请单，请先审核
            foreach(var item in plannedPriceApp.PlannedPriceAppDetails) {
                var dbPlannedPriceApp = this.ObjectContext.PlannedPriceApps.FirstOrDefault(r => r.Id != plannedPriceApp.Id && r.Status == (int)DcsPlannedPriceAppStatus.新增 && r.PartsSalesCategoryId == plannedPriceApp.PartsSalesCategoryId && this.ObjectContext.PlannedPriceAppDetails.Any(ex => ex.PlannedPriceAppId == r.Id && ex.SparePartId == item.SparePartId));
                if(dbPlannedPriceApp != null) {
                    throw new ValidationException(string.Format("{0}图号已经存在未审核的计划价申请单，请先审核", item.SparePartCode));
                }
            }
            if(string.IsNullOrWhiteSpace(plannedPriceApp.Code) || plannedPriceApp.Code == GlobalVar.ASSIGNED_BY_SERVER)
                plannedPriceApp.Code = CodeGenerator.Generate("PlannedPriceApp", plannedPriceApp.OwnerCompanyCode);
            var userInfo = Utils.GetCurrentUserInfo();
            plannedPriceApp.CreatorId = userInfo.Id;
            plannedPriceApp.CreatorName = userInfo.Name;
            plannedPriceApp.CreateTime = DateTime.Now;
        }
        internal void UpdatePlannedPriceAppValidate(PlannedPriceApp plannedPriceApp) {
            //增加校验新增、修改、导入时，同一个图号且状态=新增，只能存在一张申请单，若已经存在新增状态的申请单，则报错：XX图号已经存在未审核的计划价申请单，请先审核
            foreach(var item in plannedPriceApp.PlannedPriceAppDetails) {
                var dbPlannedPriceApp = this.ObjectContext.PlannedPriceApps.FirstOrDefault(r => r.Id != plannedPriceApp.Id && r.Status == (int)DcsPlannedPriceAppStatus.新增 && r.PartsSalesCategoryId == plannedPriceApp.PartsSalesCategoryId && this.ObjectContext.PlannedPriceAppDetails.Any(ex => ex.PlannedPriceAppId == r.Id && ex.SparePartId == item.SparePartId));
                if(dbPlannedPriceApp != null) {
                    throw new ValidationException(string.Format("{0}图号已经存在未审核的计划价申请单，请先审核", item.SparePartCode));
                }
            }
            var userInfo = Utils.GetCurrentUserInfo();
            plannedPriceApp.ModifierId = userInfo.Id;
            plannedPriceApp.ModifierName = userInfo.Name;
            plannedPriceApp.ModifyTime = DateTime.Now;
        }
        public void InsertPlannedPriceApp(PlannedPriceApp plannedPriceApp) {
            InsertToDatabase(plannedPriceApp);
            var plannedPriceAppDetails = ChangeSet.GetAssociatedChanges(plannedPriceApp, v => v.PlannedPriceAppDetails, ChangeOperation.Insert);
            foreach(PlannedPriceAppDetail plannedPriceAppDetail in plannedPriceAppDetails) {
                plannedPriceAppDetail.RequestedPrice = Math.Round(plannedPriceAppDetail.RequestedPrice, 2, MidpointRounding.AwayFromZero);
                InsertToDatabase(plannedPriceAppDetail);
            }
            this.InsertPlannedPriceAppValidate(plannedPriceApp);
        }
        public void UpdatePlannedPriceApp(PlannedPriceApp plannedPriceApp) {
            plannedPriceApp.PlannedPriceAppDetails.Clear();
            UpdateToDatabase(plannedPriceApp);
            var plannedPriceAppDetails = ChangeSet.GetAssociatedChanges(plannedPriceApp, v => v.PlannedPriceAppDetails);
            foreach(PlannedPriceAppDetail plannedPriceAppDetail in plannedPriceAppDetails) {
                plannedPriceAppDetail.RequestedPrice = Math.Round(plannedPriceAppDetail.RequestedPrice, 2, MidpointRounding.AwayFromZero);
                //获取清单实体对象的操作类型
                switch(ChangeSet.GetChangeOperation(plannedPriceAppDetail)) {
                    //新增操作
                    case ChangeOperation.Insert:
                        InsertToDatabase(plannedPriceAppDetail);
                        break;
                    //更新操作
                    case ChangeOperation.Update:
                        UpdateToDatabase(plannedPriceAppDetail);
                        break;
                    //删除操作
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(plannedPriceAppDetail);
                        break;
                }
            }
            this.UpdatePlannedPriceAppValidate(plannedPriceApp);
        }
        public IQueryable<PlannedPriceApp> GetPlannedPriceAppWithPlannedPriceAppDetailById(int Id) {
            return ObjectContext.PlannedPriceApps.Include("PlannedPriceAppDetails").Include("PartsSalesCategory").Where(e => e.Id == Id).OrderBy(entity => entity.Id);
        }
        public IQueryable<PlannedPriceAppDetail> 查询计划价变更履历() {
            var plannedPriceApp = ObjectContext.PlannedPriceApps.Where(v => v.Status == (int)DcsPlannedPriceAppStatus.已执行).Select(o => o.Id);
            var plannedPriceAppDetails = ObjectContext.PlannedPriceAppDetails.Where(v => plannedPriceApp.Contains(v.PlannedPriceAppId));
            var result = plannedPriceAppDetails.Include("PlannedPriceApp").Include("PlannedPriceApp.PartsSalesCategory").OrderBy(entity => entity.Id);
            return result;
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:33
        //原分布类的函数全部转移到Ach                                                               

        public void InsertPlannedPriceApp(PlannedPriceApp plannedPriceApp) {
            new PlannedPriceAppAch(this).InsertPlannedPriceApp(plannedPriceApp);
        }

        public void UpdatePlannedPriceApp(PlannedPriceApp plannedPriceApp) {
            new PlannedPriceAppAch(this).UpdatePlannedPriceApp(plannedPriceApp);
        }

        public IQueryable<PlannedPriceApp> GetPlannedPriceAppWithPlannedPriceAppDetailById(int Id) {
            return new PlannedPriceAppAch(this).GetPlannedPriceAppWithPlannedPriceAppDetailById(Id);
        }

        public IQueryable<PlannedPriceAppDetail> 查询计划价变更履历() {
            return new PlannedPriceAppAch(this).查询计划价变更履历();
        }
    }
}
