﻿using System.Linq;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class WarehouseCostChangeBillAch : DcsSerivceAchieveBase {
        public WarehouseCostChangeBillAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public IQueryable<WarehouseCostChangeBill> GetWarehouseCostChangeBillWithWarehouseCostChangeDetails() {
            return ObjectContext.WarehouseCostChangeBills.Include("WarehouseCostChangeDetails").OrderBy(entity => entity.Id);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:33
        //原分布类的函数全部转移到Ach                                                               

        public IQueryable<WarehouseCostChangeBill> GetWarehouseCostChangeBillWithWarehouseCostChangeDetails() {
            return new WarehouseCostChangeBillAch(this).GetWarehouseCostChangeBillWithWarehouseCostChangeDetails();
        }
    }
}