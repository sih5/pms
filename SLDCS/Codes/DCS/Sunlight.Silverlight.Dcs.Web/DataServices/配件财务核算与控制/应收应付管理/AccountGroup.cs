﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class AccountGroupAch : DcsSerivceAchieveBase {
        internal void InsertAccountGroupValidate(AccountGroup accountGroup) {
            var userInfo = Utils.GetCurrentUserInfo();
            var dbCompany = ObjectContext.Companies.SingleOrDefault(r => r.Id == accountGroup.SalesCompanyId && r.Status != (int)DcsMasterDataStatus.作废);
            if(dbCompany == null)
                throw new ValidationException(ErrorStrings.AccountGroup_Validation3);
            //判断Code是否需要生成编号
            if(string.IsNullOrWhiteSpace(accountGroup.Code) || accountGroup.Code == GlobalVar.ASSIGNED_BY_SERVER)
                accountGroup.Code = CodeGenerator.Generate("AccountGroup", dbCompany.Code);
            accountGroup.CreatorId = userInfo.Id;
            accountGroup.CreatorName = userInfo.Name;
            accountGroup.CreateTime = DateTime.Now;
        }
        internal void UpdateAccountGroupValidate(AccountGroup accountGroup) {
            var userInfo = Utils.GetCurrentUserInfo();
            accountGroup.ModifierId = userInfo.Id;
            accountGroup.ModifierName = userInfo.Name;
            accountGroup.ModifyTime = DateTime.Now;
        }
        public void InsertAccountGroup(AccountGroup accountGroup) {
            InsertToDatabase(accountGroup);
            this.InsertAccountGroupValidate(accountGroup);
        }
        public void UpdateAccountGroup(AccountGroup accountGroup) {
            UpdateToDatabase(accountGroup);
            this.UpdateAccountGroupValidate(accountGroup);
        }
        public IQueryable<AccountGroup> GetAccountGroupWithCompanies() {
            var userInfo = Utils.GetCurrentUserInfo();
            return ObjectContext.AccountGroups.Include("Company").Where(r => r.SalesCompanyId == userInfo.EnterpriseId || ObjectContext.AgencyAffiBranches.Any(v => v.BranchId == userInfo.EnterpriseId && r.SalesCompanyId == v.AgencyId)).OrderBy(r => r.Id);
        }
        public IQueryable<AccountGroup> GetAccountGroupsByOwnerCompanyId(int ownerCompanyId) {
            return ObjectContext.AccountGroups.Where(r => ObjectContext.SalesUnits.Where(e => e.OwnerCompanyId == ownerCompanyId && e.Status == (int)DcsMasterDataStatus.有效).Select(e => e.AccountGroupId).Contains(r.Id) && r.Status == (int)DcsMasterDataStatus.有效).OrderBy(r => r.Id);
        }
        public IQueryable<AccountGroup> GetAccountGroupsByPartsSalesCategoryId(int partsSalesCategoryId) {
            var userInfo = Utils.GetCurrentUserInfo();
            return ObjectContext.AccountGroups.Where(r => r.Status == (int)DcsMasterDataStatus.有效 && r.SalesCompanyId == userInfo.EnterpriseId && this.ObjectContext.SalesUnits.Where(x => x.PartsSalesCategoryId == partsSalesCategoryId && x.Status == (int)DcsMasterDataStatus.有效).Select(e => e.AccountGroupId).Contains(r.Id));
        }
        public AccountGroup GetAccountGroupById(int id) {
            return ObjectContext.AccountGroups.Include("Company").FirstOrDefault(e => e.Id == id);
        }
        public IQueryable<AccountGroup> GetAccountGroupsByAccountGroupId(List<int> accountGroupIds) {
            return ObjectContext.AccountGroups.Where(r => accountGroupIds.Contains(r.Id) && r.Status == (int)DcsMasterDataStatus.有效);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:33
        //原分布类的函数全部转移到Ach                                                               

        public void InsertAccountGroup(AccountGroup accountGroup) {
            new AccountGroupAch(this).InsertAccountGroup(accountGroup);
        }

        public void UpdateAccountGroup(AccountGroup accountGroup) {
            new AccountGroupAch(this).UpdateAccountGroup(accountGroup);
        }

        public IQueryable<AccountGroup> GetAccountGroupWithCompanies() {
            return new AccountGroupAch(this).GetAccountGroupWithCompanies();
        }

        public IQueryable<AccountGroup> GetAccountGroupsByOwnerCompanyId(int ownerCompanyId) {
            return new AccountGroupAch(this).GetAccountGroupsByOwnerCompanyId(ownerCompanyId);
        }

        public IQueryable<AccountGroup> GetAccountGroupsByPartsSalesCategoryId(int partsSalesCategoryId) {
            return new AccountGroupAch(this).GetAccountGroupsByPartsSalesCategoryId(partsSalesCategoryId);
        }

        public AccountGroup GetAccountGroupById(int id) {
            return new AccountGroupAch(this).GetAccountGroupById(id);
        }

        public IQueryable<AccountGroup> GetAccountGroupsByAccountGroupId(List<int> accountGroupIds) {
            return new AccountGroupAch(this).GetAccountGroupsByAccountGroupId(accountGroupIds);
        }
    }
}
