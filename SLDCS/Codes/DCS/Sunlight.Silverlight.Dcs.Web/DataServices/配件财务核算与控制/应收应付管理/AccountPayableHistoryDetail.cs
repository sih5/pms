﻿using System.Linq;
using Microsoft.Data.Extensions;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class AccountPayableHistoryDetailAch : DcsSerivceAchieveBase {
        internal void InsertAccountPayableHistoryDetailValidate(AccountPayableHistoryDetail accountPayableHistoryDetail) {

        }

        internal void UpdateAccountPayableHistoryDetailValidate(AccountPayableHistoryDetail accountPayableHistoryDetail) {

        }

        public void InsertAccountPayableHistoryDetail(AccountPayableHistoryDetail accountPayableHistoryDetail) {
            InsertToDatabase(accountPayableHistoryDetail);
            this.InsertAccountPayableHistoryDetailValidate(accountPayableHistoryDetail);
        }

        public void UpdateAccountPayableHistoryDetail(AccountPayableHistoryDetail accountPayableHistoryDetail) {
            UpdateToDatabase(accountPayableHistoryDetail);
            this.UpdateAccountPayableHistoryDetailValidate(accountPayableHistoryDetail);
        }

        public IQueryable<AccountPayableHistoryDetail> GetAccountPayableHistoryDetailWithCompany(int BuyCompanyId) {
            return ObjectContext.AccountPayableHistoryDetails.Where(r => ObjectContext.SupplierAccounts.Any(v => v.BuyerCompanyId == BuyCompanyId && v.Status == (int)DcsMasterDataStatus.有效 && v.Id == r.SupplierAccountId)).Include("SupplierAccount.PartsSalesCategory").Include("SupplierAccount.Company").OrderBy(e => e.Id);
        }
        /*
         若选择企业为供应商，则该字段取 应付账款变更明细.发生后金额（选择企业Id=供应商账户.供应商企业Id，供应商账户.Id = 应付账款变更明细.供应商账户Id）
         */
        public IQueryable<AccountPayableHistoryDetail> GetAccountPayableHistoryDetailWithSupplierAccountId(int supplierAccountId, int partsSalesCategoryId) {
            return ObjectContext.AccountPayableHistoryDetails.Where(r => ObjectContext.SupplierAccounts.Any(v => v.PartsSalesCategoryId == partsSalesCategoryId && v.SupplierCompanyId == supplierAccountId && v.Id == r.SupplierAccountId));
        }

    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:33
        //原分布类的函数全部转移到Ach                                                               

        public void InsertAccountPayableHistoryDetail(AccountPayableHistoryDetail accountPayableHistoryDetail) {
            new AccountPayableHistoryDetailAch(this).InsertAccountPayableHistoryDetail(accountPayableHistoryDetail);
        }

        public void UpdateAccountPayableHistoryDetail(AccountPayableHistoryDetail accountPayableHistoryDetail) {
            new AccountPayableHistoryDetailAch(this).UpdateAccountPayableHistoryDetail(accountPayableHistoryDetail);
        }

        public IQueryable<AccountPayableHistoryDetail> GetAccountPayableHistoryDetailWithCompany(int BuyCompanyId) {
            return new AccountPayableHistoryDetailAch(this).GetAccountPayableHistoryDetailWithCompany(BuyCompanyId);
        }
        public IQueryable<AccountPayableHistoryDetail> GetAccountPayableHistoryDetailWithSupplierAccountId(int supplierAccountId, int partsSalesCategoryId) {
            return new AccountPayableHistoryDetailAch(this).GetAccountPayableHistoryDetailWithSupplierAccountId(supplierAccountId, partsSalesCategoryId);
        }
    }
}
