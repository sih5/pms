﻿using System;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {

    partial class CAReconciliationAch : DcsSerivceAchieveBase {
        public CAReconciliationAch(DcsDomainService domainService)
            : base(domainService) {
        }

        internal void CAReconciliationValidate(CAReconciliation cAReconciliation) {
            var userInfo = Utils.GetCurrentUserInfo();
            cAReconciliation.CreatorId = userInfo.Id;
            cAReconciliation.CreatorName = userInfo.Name;
            cAReconciliation.CreateTime = DateTime.Now;

            if(string.IsNullOrWhiteSpace(cAReconciliation.StatementCode) || cAReconciliation.StatementCode == GlobalVar.ASSIGNED_BY_SERVER)
                cAReconciliation.StatementCode = CodeGenerator.Generate("CAReconciliation");
        }

        internal void UpdateCAReconciliationValidate(CAReconciliation cAReconciliation) {
            var userInfo = Utils.GetCurrentUserInfo();
            cAReconciliation.ModifierId = userInfo.Id;
            cAReconciliation.ModifierName = userInfo.Name;
            cAReconciliation.ModifierTime = DateTime.Now;

        }

        public void InsertCAReconciliation(CAReconciliation cAReconciliation) {
            InsertToDatabase(cAReconciliation);
            var cAReconciliationLists = ChangeSet.GetAssociatedChanges(cAReconciliation, v => v.CAReconciliationLists, ChangeOperation.Insert);
            foreach(CAReconciliationList cAReconciliationList in cAReconciliationLists) {
                InsertToDatabase(cAReconciliationList);
            }
            this.CAReconciliationValidate(cAReconciliation);
        }

        public void UpdateCAReconciliation(CAReconciliation cAReconciliation) {
            cAReconciliation.CAReconciliationLists.Clear();
            UpdateToDatabase(cAReconciliation);
            var cAReconciliationLists = ChangeSet.GetAssociatedChanges(cAReconciliation, v => v.CAReconciliationLists);
            foreach(CAReconciliationList cAReconciliationList in cAReconciliationLists) {
                switch(ChangeSet.GetChangeOperation(cAReconciliationList)) {
                    case ChangeOperation.Insert:
                        InsertToDatabase(cAReconciliationList);
                        break;
                    case ChangeOperation.Update:
                        UpdateToDatabase(cAReconciliationList);
                        break;
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(cAReconciliationList);
                        break;
                }
            }
            this.UpdateCAReconciliationValidate(cAReconciliation);
        }

        public CAReconciliation GetCAReconciliationForListById(int id) {
            var cAReconciliation = ObjectContext.CAReconciliations.Include("CAReconciliationLists").SingleOrDefault(r => r.Id == id);
            if(cAReconciliation.CorporationId != default(int)) {
                cAReconciliation.CompanyType = ObjectContext.Companies.Single(r => r.Id == cAReconciliation.CorporationId).Type;
            }
            return cAReconciliation;
        }

        public IQueryable<CAReconciliation> GetCAReconciliationByCompanyType() {
            var userInfo = Utils.GetCurrentUserInfo();
            var company = this.ObjectContext.Companies.Single(r => r.Id == userInfo.EnterpriseId);
            if(company.Type == (int)DcsCompanyType.分公司) {
                return ObjectContext.CAReconciliations.OrderBy(e => e.Id);
            }
            return ObjectContext.CAReconciliations.Where(r => r.CorporationId == userInfo.EnterpriseId).OrderBy(e => e.Id);
        }
    }
    partial class DcsDomainService {
        public void InsertInternalAcquisitionBill(CAReconciliation cAReconciliation) {
            new CAReconciliationAch(this).InsertCAReconciliation(cAReconciliation);
        }

        public void UpdateCAReconciliation(CAReconciliation cAReconciliation) {
            new CAReconciliationAch(this).UpdateCAReconciliation(cAReconciliation);
        }
        public CAReconciliation GetCAReconciliationForListById(int id) {
            return new CAReconciliationAch(this).GetCAReconciliationForListById(id);
        }
        public IQueryable<CAReconciliation> GetCAReconciliationByCompanyType() {
            return new CAReconciliationAch(this).GetCAReconciliationByCompanyType();
        }
    }
}
