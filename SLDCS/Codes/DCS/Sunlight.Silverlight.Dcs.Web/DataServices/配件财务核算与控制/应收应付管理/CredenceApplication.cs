﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class CredenceApplicationAch : DcsSerivceAchieveBase {
        internal void InsertCredenceApplicationValidate(CredenceApplication credenceApplication) {
            var userInfo = Utils.GetCurrentUserInfo();
            if(string.IsNullOrWhiteSpace(credenceApplication.Code) || credenceApplication.Code == GlobalVar.ASSIGNED_BY_SERVER) {
                var company = ObjectContext.Companies.SingleOrDefault(r => r.Id == credenceApplication.SalesCompanyId);
                if(company == null)
                    throw new ValidationException(ErrorStrings.CredenceApplication_Validation7);
                credenceApplication.Code = CodeGenerator.Generate("CredenceApplication", company.Code);
            }
            credenceApplication.CreatorId = userInfo.Id;
            credenceApplication.CreatorName = userInfo.Name;
            credenceApplication.CreateTime = DateTime.Now;
        }

        internal void UpdateCredenceApplicationValidate(CredenceApplication credenceApplication) {

            var userInfo = Utils.GetCurrentUserInfo();
            credenceApplication.ModifierId = userInfo.Id;
            credenceApplication.ModifierName = userInfo.Name;
            credenceApplication.ModifyTime = DateTime.Now;
        }

        public void InsertCredenceApplication(CredenceApplication credenceApplication) {
            InsertToDatabase(credenceApplication);
            this.InsertCredenceApplicationValidate(credenceApplication);
        }

        public void UpdateCredenceApplication(CredenceApplication credenceApplication) {
            UpdateToDatabase(credenceApplication);
            this.UpdateCredenceApplicationValidate(credenceApplication);
        }

        /// <summary>
        /// 获取信用申请单并获取关联企业信息、账户组。
        /// </summary>
        /// <returns></returns>
        public IQueryable<CredenceApplication> GetCredenceApplication() {
            return ObjectContext.CredenceApplications.Include("CustomerCompany").Include("AccountGroup").OrderBy(v => v.Id);
        }
        public IQueryable<VirtualCredenceApplication> GetVirtualCredenceApplications(string companyName, string companyCode, DateTime? beginCreateTime, DateTime? endCreateTime, DateTime? beginApproveTime, DateTime? endApproveTime, int? status, string businessCode, int? creditType) {
            var userInfo = Utils.GetCurrentUserInfo();
            var company = ObjectContext.Companies.Single(r => r.Id == userInfo.EnterpriseId);
            var customerAccountHisDetailsquery = from p in ObjectContext.CredenceApplications.Where(r => r.SalesCompanyId == userInfo.EnterpriseId)
                                                 join b in ObjectContext.Companies.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on p.CustomerCompanyId equals b.Id
                                                 join d in ObjectContext.AccountGroups.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on p.AccountGroupId equals d.Id
                                                 into c
                                                 from accountGroup in c.DefaultIfEmpty()
                                                 join salesUnit in ObjectContext.SalesUnits.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on new {
                                                     accountGroupId = p.AccountGroupId,
                                                     customerCompanyId = accountGroup.SalesCompanyId
                                                 } equals new {
                                                     accountGroupId = salesUnit.AccountGroupId,
                                                     customerCompanyId = salesUnit.OwnerCompanyId
                                                 } into sale
                                                 from salesUnit in sale.DefaultIfEmpty()
                                                 join dealerServiceInfo in ObjectContext.DealerServiceInfoes.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on new {
                                                     customerCompanyId = p.CustomerCompanyId,
                                                     PartsSalesCategoryId = salesUnit.PartsSalesCategoryId
                                                 } equals new {
                                                     customerCompanyId = dealerServiceInfo.DealerId,
                                                     PartsSalesCategoryId = dealerServiceInfo.PartsSalesCategoryId
                                                 } into acc1
                                                 from dealerServiceInfo in acc1.DefaultIfEmpty()
                                                 orderby p.Id
                                                 select new VirtualCredenceApplication {
                                                     Id = p.Id,
                                                     BusinessCode =
                                                     b.Type == (int)DcsCompanyType.服务站 || b.Type == (int)DcsCompanyType.服务站兼代理库 ? dealerServiceInfo.BusinessCode :
                                                     b.Type == (int)DcsCompanyType.代理库 ? salesUnit.BusinessCode : null,
                                                     GuarantorCompanyName = p.GuarantorCompanyName,
                                                     AccountGroupName = accountGroup.Name,
                                                     CustomerCompanyId = p.CustomerCompanyId,
                                                     AccountGroupId = p.AccountGroupId,
                                                     CompanyName = b.Name,
                                                     CompanyCode = b.Code,
                                                     ExpireDate = p.ExpireDate,
                                                     ValidationDate = p.ValidationDate,
                                                     CredenceLimit = p.CredenceLimit,
                                                     Status = p.Status,
                                                     CreatorName = p.CreatorName,
                                                     CreateTime = p.CreateTime,
                                                     ModifierName = p.ModifierName,
                                                     ModifyTime = p.ModifyTime,
                                                     ApproverName = p.ApproverName,
                                                     ApproveTime = p.ApproveTime,
                                                     CreditType = p.CreditType,
                                                     CompanyType = company.Type,
                                                     InitialApproverName=p.InitialApproverName,
                                                     InitialApproveTime=p.InitialApproveTime,
                                                     CheckerName=p.CheckerName,
                                                     CheckTime=p.CheckTime,
                                                     UpperApproverName=p.UpperApproverName,
                                                     UpperApproveTime=p.UpperApproveTime,
                                                     UpperCheckTime=p.UpperCheckTime,
                                                     UpperCheckerName=p.UpperCheckerName,
                                                     IsUplodFile = p.Path != null && p.Path.Length > 0
                                                 };
            if(!string.IsNullOrEmpty(companyName)) {
                customerAccountHisDetailsquery = customerAccountHisDetailsquery.Where(r => r.CompanyName.Contains(companyName));
            }

            if(!string.IsNullOrEmpty(companyCode)) {
                customerAccountHisDetailsquery = customerAccountHisDetailsquery.Where(r => r.CompanyCode.Contains(companyCode));
            }

            if(status.HasValue) {
                customerAccountHisDetailsquery = customerAccountHisDetailsquery.Where(r => r.Status == status);
            }
            if(beginApproveTime.HasValue) {
                customerAccountHisDetailsquery = customerAccountHisDetailsquery.Where(r => r.ApproveTime >= beginApproveTime);
            }
            if(endApproveTime.HasValue) {
                customerAccountHisDetailsquery = customerAccountHisDetailsquery.Where(r => r.ApproveTime <= endApproveTime);
            }
            if(beginCreateTime.HasValue) {
                customerAccountHisDetailsquery = customerAccountHisDetailsquery.Where(r => r.CreateTime >= beginCreateTime);
            }
            if(endCreateTime.HasValue) {
                customerAccountHisDetailsquery = customerAccountHisDetailsquery.Where(r => r.CreateTime <= endCreateTime);
            }
            if(!string.IsNullOrEmpty(businessCode)) {
                customerAccountHisDetailsquery = customerAccountHisDetailsquery.Where(r => r.BusinessCode.Contains(businessCode));
            }
            if(creditType.HasValue) {
                customerAccountHisDetailsquery = customerAccountHisDetailsquery.Where(r => r.CreditType == creditType);
            }
            return customerAccountHisDetailsquery.Distinct().OrderBy(r => r.Id);
        }
        /// <summary>
        /// 根据ID获取信用申请单并获取关联企业信息、账户组。
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public CredenceApplication GetCredenceApplicationById(int id) {
            var credenceApplication = ObjectContext.CredenceApplications.SingleOrDefault(r => r.Id == id);
            if(credenceApplication != null) {
                var customerCompany = ObjectContext.Companies.SingleOrDefault(r => r.Id == credenceApplication.CustomerCompanyId);
                var accountGroup = ObjectContext.AccountGroups.SingleOrDefault(r => r.Id == credenceApplication.AccountGroupId);
            }
            return credenceApplication;
        }


    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:33
        //原分布类的函数全部转移到Ach                                                               

        public void InsertCredenceApplication(CredenceApplication credenceApplication) {
            new CredenceApplicationAch(this).InsertCredenceApplication(credenceApplication);
        }

        public void UpdateCredenceApplication(CredenceApplication credenceApplication) {
            new CredenceApplicationAch(this).UpdateCredenceApplication(credenceApplication);
        }
        /// <summary>
        /// 获取信用申请单并获取关联企业信息、账户组。
        /// </summary>
        /// <returns></returns>
        public IQueryable<CredenceApplication> GetCredenceApplication() {
            return new CredenceApplicationAch(this).GetCredenceApplication();
        }
        public IQueryable<VirtualCredenceApplication> GetVirtualCredenceApplications(string companyName, string companyCode, DateTime? beginCreateTime, DateTime? endCreateTime, DateTime? beginApproveTime, DateTime? endApproveTime, int? status, string businessCode, int? creditType) {
            return new CredenceApplicationAch(this).GetVirtualCredenceApplications(companyName, companyCode, beginCreateTime, endCreateTime, beginApproveTime, endApproveTime, status, businessCode, creditType);
        }
        /// <summary>
        /// 根据ID获取信用申请单并获取关联企业信息、账户组。
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public CredenceApplication GetCredenceApplicationById(int id) {
            return new CredenceApplicationAch(this).GetCredenceApplicationById(id);
        }
        public IEnumerable<VirtualCredenceApplication> getCurrentCustomerAccount(int id) {
            var sql = @"select SIHCredit,
                   zcLimit,
                   LsLimit,
                   nvl(SIHCredit, 0) + nvl(zcLimit, 0) + nvl(LsLimit, 0) as HjLimit,
                   zsNow,
                   LsNow,
                   nvl(zsNow, 0) + nvl(LsNow, 0) as hjNow,
                   nvl(zsNow, zcLimit) as DifZc,
                   nvl(LsNow, LsLimit) as DifLs,
                    nvl(SIHCredit, 0) + nvl(nvl(zsNow, zcLimit),0) + nvl(nvl(LsNow, LsLimit),0) hjDif
              from (select ca.SIHCredit,
                           cpt.CredenceLimit as zcLimit,
                           cpt2.CredenceLimit LsLimit,
                           (case
                             when cp.credittype = 1 then
                              cp.credencelimit
                             else
                              null
                           end) as zsNow,
                           (case
                             when cp.credittype = 2 then
                              cp.credencelimit
                             else
                              null
                           end) as LsNow
                      from CredenceApplication cp
                      left join CredenceApplication cpt --有效的正常授信
                        on cp.customercompanyid = cpt.CustomerCompanyId
                       and cpt.status = 3
                       and cpt.CreditType = 1
                      left join CredenceApplication cpt2 --有效的临时授信
                        on cp.customercompanyid = cpt2.CustomerCompanyId
                       and cpt2.status = 3
                       and cpt.CreditType = 2
                      left join CustomerAccount ca --当前审核的信用单
                        on ca.customercompanyid = cp.CustomerCompanyId
                      left join AccountGroup ag on ca.AccountGroupId=ag.id 
                     where ca.status = 1
                           and cp.id = " + id + " and ag.SalesCompanyId="+Utils.GetCurrentUserInfo().EnterpriseId+")";
            var search = ObjectContext.ExecuteStoreQuery<VirtualCredenceApplication>(sql).ToList();
            return search;
         }
    }
}
