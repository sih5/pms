﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Entities;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class CustomerAccountAch : DcsSerivceAchieveBase {
        internal void InsertCustomerAccountValidate(CustomerAccount customerAccount) {
            var dbCustomerAccount = ObjectContext.CustomerAccounts.Where(r => r.AccountGroupId == customerAccount.AccountGroupId && r.CustomerCompanyId == customerAccount.CustomerCompanyId && r.Status != (int)DcsMasterDataStatus.作废).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbCustomerAccount != null)
                throw new ValidationException(ErrorStrings.CustomerAccount_Validation1);

            var userInfo = Utils.GetCurrentUserInfo();
            customerAccount.CreatorId = userInfo.Id;
            customerAccount.CreatorName = userInfo.Name;
            customerAccount.CreateTime = DateTime.Now;
        }

        internal void UpdateCustomerAccountValidate(CustomerAccount customerAccount) {
            var dbCustomerAccount = ObjectContext.CustomerAccounts.Where(r => r.AccountGroupId == customerAccount.AccountGroupId && r.CustomerCompanyId == customerAccount.CustomerCompanyId && r.Id == customerAccount.Id).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbCustomerAccount == null)
                throw new ValidationException(ErrorStrings.CustomerAccount_Validation2);

            var userInfo = Utils.GetCurrentUserInfo();
            customerAccount.ModifierId = userInfo.Id;
            customerAccount.ModifierName = userInfo.Name;
            customerAccount.ModifyTime = DateTime.Now;
        }

        public CustomerAccount 查询客户可用资金(int? customerAccountId, int? accountGroupId, int? customerCompanyId) {
            CustomerAccount customerAccount = null;
            if(customerAccountId.HasValue)
                customerAccount = ObjectContext.CustomerAccounts.SingleOrDefault(r => r.Status != (int)DcsMasterDataStatus.作废 && r.Id == customerAccountId);
            if(accountGroupId.HasValue && customerCompanyId.HasValue)
                customerAccount = ObjectContext.CustomerAccounts.FirstOrDefault(r => r.Status != (int)DcsMasterDataStatus.作废 && r.AccountGroupId == accountGroupId.Value && r.CustomerCompanyId == customerCompanyId.Value);
            if(customerAccount == null)
                return null;
            decimal accountBalanceAmount = 0;
            var dbPartsRebateAccount = ObjectContext.PartsRebateAccounts.FirstOrDefault(r => r.AccountGroupId == accountGroupId && r.CustomerCompanyId == customerCompanyId);
            if(dbPartsRebateAccount != null)
                accountBalanceAmount = dbPartsRebateAccount.AccountBalanceAmount;
            customerAccount.UseablePosition = customerAccount.AccountBalance + customerAccount.CustomerCredenceAmount + (customerAccount.TempCreditTotalFee ?? 0) + (customerAccount.SIHCredit ?? 0) - customerAccount.PendingAmount - customerAccount.ShippedProductValue + accountBalanceAmount;
            return customerAccount;
        }
        public CustomerAccount 获取客户可用资金(int partsSalesOrderId) {
            var partsSalesOrder = ObjectContext.PartsSalesOrders.SingleOrDefault(r => r.Id == partsSalesOrderId);
            var salesUnit = ObjectContext.SalesUnits.SingleOrDefault(r => r.Id == partsSalesOrder.SalesUnitId);
            var partsRebateAccount = ObjectContext.PartsRebateAccounts.SingleOrDefault(r => r.AccountGroupId == salesUnit.AccountGroupId && r.CustomerCompanyId == partsSalesOrder.SubmitCompanyId && r.BranchId == salesUnit.OwnerCompanyId);
            var customerAccount = DomainService.查询客户可用资金(null, salesUnit.AccountGroupId, partsSalesOrder.InvoiceReceiveCompanyId);
            var lnotApprovePrice = DomainService.查询未审核订单金额(partsSalesOrder.SubmitCompanyId, partsSalesOrder.SalesUnitId, partsSalesOrder.SalesCategoryId);
            decimal lRebateAmount = 0;
            if(partsRebateAccount != null) {
                lRebateAmount = partsRebateAccount.AccountBalanceAmount;
            }
            var salesCenterstrategies = ObjectContext.SalesCenterstrategies.FirstOrDefault(r => r.PartsSalesCategoryId == partsSalesOrder.SalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效);
            if(salesCenterstrategies != null)
                customerAccount.AllowCustomerCredit = salesCenterstrategies.AllowCustomerCredit;
            //4.查询销售中心策略（销售中心策略.配件销售类型id=配件销售订单.配件销售类型id）如果 销售中心策略.允许客户欠款策略=否
            customerAccount.OrderUseablePosition = customerAccount.AccountBalance + lRebateAmount + customerAccount.CustomerCredenceAmount + (customerAccount.TempCreditTotalFee ?? 0) + (customerAccount.SIHCredit ?? 0) - customerAccount.PendingAmount - customerAccount.ShippedProductValue - lnotApprovePrice;
            return customerAccount;
        }
        public IQueryable<VirtualCustomerAccount> GetCustomerAccountsWithCustomerCompanyAndAccountGroupNoParam() {
            var userInfo = Utils.GetCurrentUserInfo();
            var tempStruct = from g in
                                 (from a in ObjectContext.PartsSalesOrders
                                  join b in ObjectContext.PartsSalesOrderDetails on a.Id equals b.PartsSalesOrderId
                                  join c in ObjectContext.SalesUnits on a.SalesUnitId equals c.Id
                                  where (a.Status == (int)DcsPartsSalesOrderStatus.提交 || a.Status == (int)DcsPartsSalesOrderStatus.部分审批)
                                  select new {
                                      a.SalesCategoryId,
                                      a.SubmitCompanyId,
                                      unApproveAmount = (b.OrderedQuantity - (b.ApproveQuantity ?? 0)) * b.OrderPrice,
                                      c.AccountGroupId
                                  })
                             group g by new {
                                 g.SalesCategoryId,
                                 g.SubmitCompanyId,
                                 g.AccountGroupId
                             } into temp
                             select new {
                                 accountGroupId = temp.Key.AccountGroupId,
                                 customerCompanyId = temp.Key.SubmitCompanyId,
                                 partsSalesCategoryId = temp.Key.SalesCategoryId,
                                 unApproveTotalAmount = temp.Sum(r => r.unApproveAmount)
                             };
            return (from a in ObjectContext.CustomerAccounts
                    join b in ObjectContext.Companies.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on a.CustomerCompanyId equals b.Id
                    join d in ObjectContext.AccountGroups on a.AccountGroupId equals d.Id
                    join c in ObjectContext.PartsRebateAccounts on new {
                        accountGroupId = a.AccountGroupId,
                        customerCompanyId = a.CustomerCompanyId
                    } equals new {
                        accountGroupId = c.AccountGroupId,
                        customerCompanyId = c.CustomerCompanyId
                    } into tempTable1
                    from t1 in tempTable1.DefaultIfEmpty()
                    join e in tempStruct on new {
                        customerCompanyId = a.CustomerCompanyId,
                        accountGroupId = d.Id
                    } equals new {
                        e.customerCompanyId,
                        e.accountGroupId
                    } into tempTable2
                    from t2 in tempTable2.DefaultIfEmpty()
                    where a.CustomerCompanyId == userInfo.EnterpriseId
                    orderby a.Id
                    select new VirtualCustomerAccount {
                        Id = a.Id,
                        AccountGroupId = d.Id,
                        CustomerCompanyId = b.Id,
                        CompanyCode = b.Code,
                        Status = a.Status,
                        CompanyName = b.Name,
                        AccountGroupName = d.Name,
                        AccountGroupCode = d.Code,
                        CompanyType = b.Type,
                        RebateAccount = t1.AccountBalanceAmount,
                        UnApproveAmount = ((decimal?)(t2.unApproveTotalAmount) ?? 0),
                        CreateTime = a.CreateTime,
                        ModifierName = a.ModifierName,
                        CreatorName = a.CreatorName,
                        ModifyTime = a.ModifyTime,
                        AccountBalance = a.AccountBalance,
                        ShippedProductValue = a.ShippedProductValue,
                        PendingAmount = a.PendingAmount,
                        CustomerCredenceAmount = a.CustomerCredenceAmount,
                        SIHCredit = a.SIHCredit,
                        //因为左连接，存在可能为空的返利台账，若为空则赋值为0
                   //     UsableBalance = a.AccountBalance + ((decimal?)(t1.AccountBalanceAmount) ?? 0) + a.CustomerCredenceAmount + (a.TempCreditTotalFee ?? 0) - a.ShippedProductValue - a.PendingAmount - ((decimal?)(t2.unApproveTotalAmount) ?? 0),
                        UsableBalance = a.AccountBalance + ((decimal?)(t1.AccountBalanceAmount) ?? 0) + a.CustomerCredenceAmount + (a.TempCreditTotalFee ?? 0) + (a.SIHCredit ?? 0) - a.ShippedProductValue - a.PendingAmount - ((decimal?)(t2.unApproveTotalAmount) ?? 0),
                        Balance = a.AccountBalance - a.ShippedProductValue,
                        LockBalance = a.ShippedProductValue + a.PendingAmount,
                        TempCreditTotalFee = a.TempCreditTotalFee
                    });
        }

        public IQueryable<VirtualCustomerAccount> GetVirtualCustomerAccountsWithCompanyAndAccountGroup(int? accountGroupId, string businessCode, bool? isZeroForAccountBalance) {
            var userInfo = Utils.GetCurrentUserInfo();
            var dealerServiceInfos = this.ObjectContext.DealerServiceInfoes.Where(r => r.Status == (int)DcsBaseDataStatus.有效);
            var salesUnits = ObjectContext.SalesUnits.Where(r => r.Status == (int)DcsBaseDataStatus.有效);
            var accountGroups = ObjectContext.AccountGroups.Where(r => r.Status == (int)DcsBaseDataStatus.有效);
            var companies = ObjectContext.Companies.Where(r => r.Status == (int)DcsBaseDataStatus.有效);
            if(accountGroupId.HasValue) {
                accountGroups = accountGroups.Where(p => p.Id == accountGroupId);
            } else {
                accountGroups = accountGroups.Where(p => p.SalesCompanyId == userInfo.EnterpriseId);

            }
            var tempStruct = from g in
                                 (from a in ObjectContext.PartsSalesOrders
                                  join b in ObjectContext.PartsSalesOrderDetails on a.Id equals b.PartsSalesOrderId
                                  join c in ObjectContext.SalesUnits on a.SalesUnitId equals c.Id
                                  where (a.Status == (int)DcsPartsSalesOrderStatus.提交 || a.Status == (int)DcsPartsSalesOrderStatus.部分审批) && c.OwnerCompanyId == userInfo.EnterpriseId
                                  select new {
                                      a.SalesCategoryId,
                                      a.SubmitCompanyId,
                                      unApproveAmount = (b.OrderedQuantity - (b.ApproveQuantity == null ? 0 : b.ApproveQuantity.Value)) * b.OrderPrice,
                                      c.AccountGroupId
                                  })
                             group g by new {
                                 g.SalesCategoryId,
                                 g.SubmitCompanyId,
                                 g.AccountGroupId
                             } into temp
                             select new {
                                 accountGroupId = temp.Key.AccountGroupId,
                                 customerCompanyId = temp.Key.SubmitCompanyId,
                                 partsSalesCategoryId = temp.Key.SalesCategoryId,
                                 unApproveTotalAmount = temp.Sum(r => r.unApproveAmount)
                             };

            var results = from a in ObjectContext.CustomerAccounts
                          join b in ObjectContext.Companies.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on a.CustomerCompanyId equals b.Id
                          join d in accountGroups on a.AccountGroupId equals d.Id
                          join c in ObjectContext.PartsRebateAccounts on new {
                              accountGroupId = a.AccountGroupId,
                              customerCompanyId = a.CustomerCompanyId
                          } equals new {
                              accountGroupId = c.AccountGroupId,
                              customerCompanyId = c.CustomerCompanyId
                          } into tempTable1
                          from t1 in tempTable1.DefaultIfEmpty()
                          join e in tempStruct on new {
                              customerCompanyId = a.CustomerCompanyId,
                              accountGroupId = d.Id
                          } equals new {
                              e.customerCompanyId,
                              e.accountGroupId
                          } into tempTable2
                          from t2 in tempTable2.DefaultIfEmpty()
                          join salesUnit in salesUnits on new {
                              accountGroupId = a.AccountGroupId,
                              customerCompanyId = d.SalesCompanyId
                          } equals new {
                              accountGroupId = salesUnit.AccountGroupId,
                              customerCompanyId = salesUnit.OwnerCompanyId
                          } into acc
                          from salesUnit in acc.DefaultIfEmpty()
                          join dealerServiceInfo in dealerServiceInfos on new {
                              customerCompanyId = a.CustomerCompanyId,
                              PartsSalesCategoryId = salesUnit.PartsSalesCategoryId
                          } equals new {
                              customerCompanyId = dealerServiceInfo.DealerId,
                              PartsSalesCategoryId = dealerServiceInfo.PartsSalesCategoryId
                          } into acc1
                          from dealerServiceInfo in acc1.DefaultIfEmpty()
                          //where accountGroupId == null ? d.SalesCompanyId == userInfo.EnterpriseId : d.Id == accountGroupId
                          orderby a.Id
                          select new VirtualCustomerAccount {
                              Id = a.Id,
                              BusinessCode =
                              b.Type == (int)DcsCompanyType.服务站 || b.Type == (int)DcsCompanyType.服务站兼代理库 ? dealerServiceInfo.BusinessCode :
                              b.Type == (int)DcsCompanyType.代理库 ? salesUnit.BusinessCode : null,
                              AccountGroupId = d.Id,
                              CustomerCompanyId = b.Id,
                              CompanyCode = b.Code,
                              Status = a.Status,
                              CompanyName = b.Name,
                              AccountGroupName = d.Name,
                              AccountGroupCode = d.Code,
                              CompanyType = b.Type,
                              RebateAccount = t1.AccountBalanceAmount,
                              UnApproveAmount = ((decimal?)(t2.unApproveTotalAmount) ?? 0),
                              CreateTime = a.CreateTime,
                              ModifierName = a.ModifierName,
                              CreatorName = a.CreatorName,
                              ModifyTime = a.ModifyTime,
                              AccountBalance = a.AccountBalance,
                              ShippedProductValue = a.ShippedProductValue,
                              PendingAmount = a.PendingAmount,
                              CustomerCredenceAmount = a.CustomerCredenceAmount,
                              SIHCredit = a.SIHCredit,
                              //因为左连接，存在可能为空的返利台账，若为空则赋值为0
                              UsableBalance = a.AccountBalance + ((decimal?)(t1.AccountBalanceAmount) ?? 0) + a.CustomerCredenceAmount + (a.TempCreditTotalFee ?? 0) + (a.SIHCredit ?? 0) - a.ShippedProductValue - a.PendingAmount - ((decimal?)(t2.unApproveTotalAmount) ?? 0),
                              Balance = a.AccountBalance - a.ShippedProductValue,
                              LockBalance = a.ShippedProductValue + a.PendingAmount,
                              TempCreditTotalFee = a.TempCreditTotalFee
                          };
            if(!string.IsNullOrEmpty(businessCode)) {
                results = results.Where(p => p.BusinessCode.Contains(businessCode));
            }
            if(isZeroForAccountBalance.HasValue) {
                results = isZeroForAccountBalance.Value ? results.Where(p => p.AccountBalance == 0) : results.Where(p => p.AccountBalance != 0);
            }
            return results.OrderBy(r => r.Id);
        }


        public CustomerAccount GetCustomerAccountByIds(int partsSalesCategoryId, int customerCompanyId, int ownerCompanyId) {
            var customerAccount = ObjectContext.CustomerAccounts.SingleOrDefault(e => ObjectContext.SalesUnits.Any(ex =>
                ex.AccountGroupId == e.AccountGroupId && ex.PartsSalesCategoryId == partsSalesCategoryId && ex.Status == (int)DcsMasterDataStatus.有效
                && ex.OwnerCompanyId == ownerCompanyId
                ) && e.CustomerCompanyId == customerCompanyId && e.Status == (int)DcsMasterDataStatus.有效);
            if(customerAccount != null) {
                var branchstrateg = ObjectContext.Branchstrategies.Single(r => r.BranchId == ownerCompanyId);
                if(branchstrateg != null)
                    customerAccount.IsDebtManage = branchstrateg.IsDebtManage;
            }
            return customerAccount;
        }

        public CustomerAccount 查询客户可用资金含未审批订单(int? customerAccountId, int? accountGroupId, int? customerCompanyId, int? partsSalesOrderId, int salesCategoryId) {
            CustomerAccount customerAccount = null;
            if(customerAccountId.HasValue)
                customerAccount = ObjectContext.CustomerAccounts.SingleOrDefault(r => r.Status != (int)DcsMasterDataStatus.作废 && r.Id == customerAccountId);
            else if(accountGroupId.HasValue && customerCompanyId.HasValue)
                customerAccount = ObjectContext.CustomerAccounts.FirstOrDefault(r => r.Status != (int)DcsMasterDataStatus.作废 && r.AccountGroupId == accountGroupId.Value && r.CustomerCompanyId == customerCompanyId.Value);
            if(customerAccount == null) {
                return null;
            }
            decimal amount = 0;
            if(partsSalesOrderId.HasValue) {
                var partsSalesOrders = ObjectContext.PartsSalesOrders.Where(r => r.SalesCategoryId == salesCategoryId && r.CustomerAccountId == customerAccount.Id && (r.Status == (int)DcsPartsSalesOrderStatus.部分审批 || r.Status == (int)DcsPartsSalesOrderStatus.提交) && r.Id != partsSalesOrderId).ToArray();
                amount = partsSalesOrders.Sum(r => r.TotalAmount);
            } else {
                var partsSalesOrders = ObjectContext.PartsSalesOrders.Where(r => r.SalesCategoryId == salesCategoryId && r.CustomerAccountId == customerAccount.Id && (r.Status == (int)DcsPartsSalesOrderStatus.部分审批 || r.Status == (int)DcsPartsSalesOrderStatus.提交)).ToArray();
                amount = partsSalesOrders.Sum(r => r.TotalAmount);
            }

            customerAccount.UseablePosition = customerAccount.AccountBalance + customerAccount.CustomerCredenceAmount + (customerAccount.TempCreditTotalFee ?? 0) - customerAccount.PendingAmount - customerAccount.ShippedProductValue - amount;

            var salesCenterstrategy = ObjectContext.SalesCenterstrategies.Where(r => r.PartsSalesCategoryId == salesCategoryId).FirstOrDefault();
            if(salesCenterstrategy != null) {
                customerAccount.AllowCustomerCredit = salesCenterstrategy.AllowCustomerCredit;
            }
            return customerAccount;
        }

        public CustomerAccount 查询客户可用资金含未审批订单2(int? customerAccountId, int? accountGroupId, int? customerCompanyId, int? partsSalesOrderId, int salesCategoryId, int salesUnitId) {
            CustomerAccount customerAccount = null;
            if(customerAccountId.HasValue)
                customerAccount = ObjectContext.CustomerAccounts.SingleOrDefault(r => r.Status != (int)DcsMasterDataStatus.作废 && r.Id == customerAccountId);
            else if(accountGroupId.HasValue && customerCompanyId.HasValue)
                customerAccount = ObjectContext.CustomerAccounts.FirstOrDefault(r => r.Status != (int)DcsMasterDataStatus.作废 && r.AccountGroupId == accountGroupId.Value && r.CustomerCompanyId == customerCompanyId.Value);
            if(customerAccount == null) {
                return null;
            }
            decimal amount = new PartsSalesOrderAch(this.DomainService).查询未审核订单金额(customerCompanyId ?? 0, salesUnitId, salesCategoryId);

            customerAccount.UseablePosition = customerAccount.AccountBalance + customerAccount.CustomerCredenceAmount + (customerAccount.TempCreditTotalFee ?? 0) + (customerAccount.SIHCredit ?? 0) - customerAccount.PendingAmount - customerAccount.ShippedProductValue - amount;

            var salesCenterstrategy = ObjectContext.SalesCenterstrategies.Where(r => r.PartsSalesCategoryId == salesCategoryId).FirstOrDefault();
            if(salesCenterstrategy != null) {
                customerAccount.AllowCustomerCredit = salesCenterstrategy.AllowCustomerCredit;
            }
            return customerAccount;

        }

        public IQueryable<VirtualAgencyCustomerAccount> 查询服务站在代理库账户明细(string dealerCode, string dealerName, string agencyCode, string agencyName) {

            var dealers = ObjectContext.Dealers.Where(r => r.Status == (int)DcsBaseDataStatus.有效);
            if(!string.IsNullOrEmpty(dealerCode))
                dealers = dealers.Where(r => r.Code.Contains(dealerCode));
            if(!string.IsNullOrEmpty(dealerName))
                dealers = dealers.Where(r => r.Name.Contains(dealerName));
            var agencys = ObjectContext.Agencies.Where(r => r.Status == (int)DcsBaseDataStatus.有效);
            if(!string.IsNullOrEmpty(agencyCode))
                agencys = agencys.Where(r => r.Code.Contains(agencyCode));
            if(!string.IsNullOrEmpty(agencyName))
                agencys = agencys.Where(r => r.Name.Contains(agencyName));

            var customerAccount = from c in ObjectContext.CustomerAccounts.Where(r => r.Status == (int)DcsBaseDataStatus.有效)
                                  join d in ObjectContext.CustomerAccountHisDetails on c.Id equals d.CustomerAccountId
                                  join g in ObjectContext.AccountGroups on c.AccountGroupId equals g.Id
                                  join r in dealers on c.CustomerCompanyId equals r.Id
                                  join a in agencys on g.SalesCompanyId equals a.Id
                                  select new VirtualAgencyCustomerAccount {
                                      Id = d.Id,
                                      DealerCode = r.Code,
                                      DealerName = r.Name,
                                      AgencyCode = a.Code,
                                      AgencyName = a.Name,
                                      ProcessDate = d.ProcessDate,
                                      ChangeAmount = d.ChangeAmount,
                                      BusinessType = d.BusinessType,
                                      SourceCode = d.SourceCode,
                                      Summary = d.SourceCode,
                                      BeforeChangeAmount = d.BeforeChangeAmount,
                                      AfterChangeAmount = d.AfterChangeAmount,
                                      Debit = d.Debit,
                                      Credit = d.Credit,
                                      AccountGroupName = g.Name

                                  };
            return customerAccount.OrderBy(entity => entity.Id);
        }
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:33
        //原分布类的函数全部转移到Ach                                                               

        public CustomerAccount 查询客户可用资金(int? customerAccountId, int? accountGroupId, int? customerCompanyId) {
            return new CustomerAccountAch(this).查询客户可用资金(customerAccountId, accountGroupId, customerCompanyId);
        }
        public CustomerAccount 获取客户可用资金(int partsSalesOrderId) {
            return new CustomerAccountAch(this).获取客户可用资金(partsSalesOrderId);
        }
        public IQueryable<VirtualCustomerAccount> GetCustomerAccountsWithCustomerCompanyAndAccountGroupNoParam() {
            return new CustomerAccountAch(this).GetCustomerAccountsWithCustomerCompanyAndAccountGroupNoParam();
        }

        public IQueryable<VirtualCustomerAccount> GetVirtualCustomerAccountsWithCompanyAndAccountGroup(int? accountGroupId, string businessCode, bool? isZeroForAccountBalance) {
            return new CustomerAccountAch(this).GetVirtualCustomerAccountsWithCompanyAndAccountGroup(accountGroupId, businessCode, isZeroForAccountBalance);
        }


        public CustomerAccount GetCustomerAccountByIds(int partsSalesCategoryId, int customerCompanyId, int ownerCompanyId) {
            return new CustomerAccountAch(this).GetCustomerAccountByIds(partsSalesCategoryId, customerCompanyId, ownerCompanyId);
        }

        public CustomerAccount 查询客户可用资金含未审批订单(int? customerAccountId, int? accountGroupId, int? customerCompanyId, int? partsSalesOrderId, int salesCategoryId) {
            return new CustomerAccountAch(this).查询客户可用资金含未审批订单(customerAccountId, accountGroupId, customerCompanyId, partsSalesOrderId, salesCategoryId);
        }

        public CustomerAccount 查询客户可用资金含未审批订单2(int? customerAccountId, int? accountGroupId, int? customerCompanyId, int? partsSalesOrderId, int salesCategoryId, int salesUnitId) {
            return new CustomerAccountAch(this).查询客户可用资金含未审批订单2(customerAccountId, accountGroupId, customerCompanyId, partsSalesOrderId, salesCategoryId, salesUnitId);
        }

        public IQueryable<VirtualAgencyCustomerAccount> 查询服务站在代理库账户明细(string dealerCode, string dealerName, string agencyCode, string agencyName) {
            return new CustomerAccountAch(this).查询服务站在代理库账户明细(dealerCode, dealerName, agencyCode, agencyName);
        }
    }
}
