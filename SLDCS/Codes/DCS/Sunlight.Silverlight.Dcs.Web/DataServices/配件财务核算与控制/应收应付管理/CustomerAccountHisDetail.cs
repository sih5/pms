﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class CustomerAccountHisDetailAch : DcsSerivceAchieveBase {
        public IEnumerable<CustomerAccountHisDetail> GetCustomerAccountHisDetailsWithCompany(int salesCompanyId) {
            var customerAccounts = ObjectContext.CustomerAccounts.Where(r => ObjectContext.AccountGroups.Any(v => v.SalesCompanyId == salesCompanyId && v.Id == r.AccountGroupId && v.Status != (int)DcsMasterDataStatus.作废) && r.Status != (int)DcsMasterDataStatus.作废).ToArray();
            if(!customerAccounts.Any())
                return null;
            var customerAccountIds = customerAccounts.Select(r => r.Id).ToArray();
            var companyIds = customerAccounts.Select(r => r.CustomerCompanyId).ToArray();
            var accountGroupIds = customerAccounts.Select(r => r.AccountGroupId).ToArray();
            var accountGroups = ObjectContext.AccountGroups.Where(t => accountGroupIds.Contains(t.Id) && t.Status != (int)DcsMasterDataStatus.作废).ToArray();
            var customerAccountHisDetails = ObjectContext.CustomerAccountHisDetails.Where(r => customerAccountIds.Contains(r.CustomerAccountId)).ToArray();
            var companies = ObjectContext.Companies.Where(r => companyIds.Contains(r.Id) && r.Status != (int)DcsMasterDataStatus.作废).ToArray();
            return customerAccountHisDetails.OrderBy(r => r.Id);
        }

        public IQueryable<VirtualCustomerAccountHisDetails> GetCustomerAccountHisDetailsWithCompanyVirtual(int salesCompanyId, string companyCode, string companyName, string sourcecode, int? businessType, int? accountGroupId, DateTime? beginProcessDate, DateTime? endProcessDate, string businessCode, DateTime? beginInvoiceDate, DateTime? endInvoiceDate, int? serialType) {
            var customerAccountHisDetailsquery = from d in ObjectContext.CustomerAccountHisDetails
                                                 join a in ObjectContext.CustomerAccounts.Where(x => x.Status != (int)DcsMasterDataStatus.作废) on d.CustomerAccountId equals a.Id
                                                 join g in ObjectContext.AccountGroups.Where(y => y.SalesCompanyId == salesCompanyId && y.Status != (int)DcsMasterDataStatus.作废) on a.AccountGroupId equals g.Id
                                                 join c in ObjectContext.Companies.Where(z => z.Status != (int)DcsMasterDataStatus.作废) on a.CustomerCompanyId equals c.Id
                                                 join salesUnit in ObjectContext.SalesUnits on new {
                                                     accountGroupId = a.AccountGroupId,
                                                     customerCompanyId = g.SalesCompanyId
                                                 } equals new {
                                                     accountGroupId = salesUnit.AccountGroupId,
                                                     customerCompanyId = salesUnit.OwnerCompanyId
                                                 } into acc
                                                 from salesUnit in acc.DefaultIfEmpty()
                                                 join dealerServiceInfo in ObjectContext.DealerServiceInfoes.Where(z => z.Status == (int)DcsMasterDataStatus.有效) on new {
                                                     customerCompanyId = a.CustomerCompanyId,
                                                     PartsSalesCategoryId = salesUnit.PartsSalesCategoryId
                                                 } equals new {
                                                     customerCompanyId = dealerServiceInfo.DealerId,
                                                     PartsSalesCategoryId = dealerServiceInfo.PartsSalesCategoryId
                                                 } into acc1
                                                 from dealerServiceInfo in acc1.DefaultIfEmpty()
                                                 orderby a.Id
                                                 select new VirtualCustomerAccountHisDetails {
                                                     ID = d.Id,
                                                     BusinessCode =
                                                     c.Type == (int)DcsCompanyType.服务站 || c.Type == (int)DcsCompanyType.服务站兼代理库 ? dealerServiceInfo.BusinessCode :
                                                     c.Type == (int)DcsCompanyType.代理库 ? salesUnit.BusinessCode : null,
                                                     CompanyCode = c.Code,
                                                     CompanyName = c.Name,
                                                     AccountGroupId = g.Id,
                                                     AccountGroupName = g.Name,
                                                     ChangeAmount = d.ChangeAmount,
                                                     ProcessDate = d.ProcessDate,
                                                     BusinessType = d.BusinessType,
                                                     SourceCode = d.SourceCode,
                                                     Summary = d.Summary,
                                                     BeforeChangeAmount = d.BeforeChangeAmount,
                                                     AfterChangeAmount = d.AfterChangeAmount,
                                                     Debit = d.Debit,
                                                     Credit = d.Credit,
                                                     CreatorName = d.CreatorName,
                                                     InvoiceDate = d.InvoiceDate,
                                                     SerialType = d.SerialType
                                                 };
            if(!string.IsNullOrEmpty(companyCode)) {
                customerAccountHisDetailsquery = customerAccountHisDetailsquery.Where(r => r.CompanyCode.Contains(companyCode));
            }

            if(!string.IsNullOrEmpty(companyName)) {
                customerAccountHisDetailsquery = customerAccountHisDetailsquery.Where(r => r.CompanyName.Contains(companyName));
            }

            if(!string.IsNullOrEmpty(sourcecode)) {
                customerAccountHisDetailsquery = customerAccountHisDetailsquery.Where(r => r.SourceCode.Contains(sourcecode));
            }

            if(businessType.HasValue) {
                customerAccountHisDetailsquery = customerAccountHisDetailsquery.Where(r => r.BusinessType == businessType);
            }
            if(accountGroupId.HasValue) {
                customerAccountHisDetailsquery = customerAccountHisDetailsquery.Where(r => r.AccountGroupId == accountGroupId);
            }

            if(beginProcessDate.HasValue) {
                customerAccountHisDetailsquery = customerAccountHisDetailsquery.Where(r => r.ProcessDate >= beginProcessDate);
            }
            if(endProcessDate.HasValue) {
                customerAccountHisDetailsquery = customerAccountHisDetailsquery.Where(r => r.ProcessDate <= endProcessDate);
            }
            if(!string.IsNullOrEmpty(businessCode)) {
                customerAccountHisDetailsquery = customerAccountHisDetailsquery.Where(r => r.BusinessCode.Contains(businessCode));
            }
            if(beginInvoiceDate.HasValue) {
                customerAccountHisDetailsquery = customerAccountHisDetailsquery.Where(r => r.InvoiceDate >= beginInvoiceDate);
            }
            if(endInvoiceDate.HasValue) {
                customerAccountHisDetailsquery = customerAccountHisDetailsquery.Where(r => r.InvoiceDate <= endInvoiceDate);
            }
            if(serialType.HasValue) {
                customerAccountHisDetailsquery = customerAccountHisDetailsquery.Where(r => r.SerialType == serialType);
            }
            return customerAccountHisDetailsquery.OrderBy(r => r.ID);
        }

        public IEnumerable<CustomerAccountHisDetail> GetCustomerAccountHisDetailsForWithCompany(int customerCompanyId,string sourceCode) {
            var customerAccounts = ObjectContext.CustomerAccounts.Where(r => r.CustomerCompanyId == customerCompanyId && r.Status != (int)DcsMasterDataStatus.作废).ToArray();
            if(!customerAccounts.Any())
                return null;
            var customerAccountIds = customerAccounts.Select(r => r.Id).ToArray();
            var accountGroupIds = customerAccounts.Select(r => r.AccountGroupId).ToArray();
            var accountGroups = ObjectContext.AccountGroups.Where(t => accountGroupIds.Contains(t.Id) && t.Status != (int)DcsMasterDataStatus.作废).ToArray();
            var customerAccountHisDetails = ObjectContext.CustomerAccountHisDetails.Where(r => customerAccountIds.Contains(r.CustomerAccountId));
            var companies = ObjectContext.Companies.Where(r => r.Id == customerCompanyId && r.Status != (int)DcsMasterDataStatus.作废).ToArray();
            if(!string.IsNullOrEmpty(sourceCode)) {
                customerAccountHisDetails = customerAccountHisDetails.Where(t => t.SourceCode.Contains(sourceCode));
            }
            return customerAccountHisDetails.ToArray().OrderBy(r => r.Id);
        }

        public IQueryable<CustomerAccountHistory> GetCustomerAccountHistoriesWithAccountGroup(DateTime? theDate) {
            if(theDate.HasValue) {
                theDate = theDate.Value.AddMonths(1);
                return ObjectContext.CustomerAccountHistories.Include("AccountGroup").Where(r => (!theDate.HasValue || r.TheDate == theDate)).OrderBy(e => e.Id);
            }
            return ObjectContext.CustomerAccountHistories.Include("AccountGroup").OrderBy(e => e.Id);
        }

        public IQueryable<CustomerAccountHisDetail> GetCustomerAccountHistoryByCustomerCompanyId(int customerCompanyId, int accountGroupId) {
            return this.ObjectContext.CustomerAccountHisDetails.Where(r => ObjectContext.CustomerAccounts.Any(x => x.AccountGroupId == accountGroupId && x.Id == r.CustomerAccountId && x.CustomerCompanyId == customerCompanyId));
        }
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:33
        //原分布类的函数全部转移到Ach                                                               

        public IEnumerable<CustomerAccountHisDetail> GetCustomerAccountHisDetailsWithCompany(int salesCompanyId) {
            return new CustomerAccountHisDetailAch(this).GetCustomerAccountHisDetailsWithCompany(salesCompanyId);
        }

        public IQueryable<CustomerAccountHisDetail> GetCustomerAccountHistoryByCustomerCompanyId(int customerCompanyId, int accountGroupId) {
            return new CustomerAccountHisDetailAch(this).GetCustomerAccountHistoryByCustomerCompanyId(customerCompanyId, accountGroupId);
        }

        public IQueryable<VirtualCustomerAccountHisDetails> GetCustomerAccountHisDetailsWithCompanyVirtual(int salesCompanyId, string companyCode, string companyName, string sourcecode, int? businessType, int? accountGroupId, DateTime? beginProcessDate, DateTime? endProcessDate, string businessCode, DateTime? beginInvoiceDate, DateTime? endInvoiceDate, int? serialType) {
            return new CustomerAccountHisDetailAch(this).GetCustomerAccountHisDetailsWithCompanyVirtual(salesCompanyId, companyCode, companyName, sourcecode, businessType, accountGroupId, beginProcessDate, endProcessDate, businessCode, beginInvoiceDate, endInvoiceDate, serialType);
        }

        public IEnumerable<CustomerAccountHisDetail> GetCustomerAccountHisDetailsForWithCompany(int customerCompanyId, string sourceCode) {
            return new CustomerAccountHisDetailAch(this).GetCustomerAccountHisDetailsForWithCompany(customerCompanyId,sourceCode);
        }

        public IQueryable<CustomerAccountHistory> GetCustomerAccountHistoriesWithAccountGroup(DateTime? theDate) {
            return new CustomerAccountHisDetailAch(this).GetCustomerAccountHistoriesWithAccountGroup(theDate);
        }
    }
}
