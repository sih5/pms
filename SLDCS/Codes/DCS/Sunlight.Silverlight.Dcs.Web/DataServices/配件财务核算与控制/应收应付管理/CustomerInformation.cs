﻿using System.Linq;
using System.Collections.Generic;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class CustomerInformationAch : DcsSerivceAchieveBase {
        public IQueryable<CustomerInformation> GetCustomerInformationsWithCustomerAndSalesCompany() {
            return this.ObjectContext.CustomerInformations.Include("CustomerCompany").Include("SalesCompany").OrderBy(entity => entity.Id);
        }

        public IEnumerable<VirtualCustomerInformationForTransfer> GetCustomerInformationWithCustomerCompanyForTransfer(string companyCode,string companyName,int? companyType) {
            var user = Utils.GetCurrentUserInfo();
            var company = ObjectContext.Companies.Single(r => r.Id == user.EnterpriseId);
            string sql = "";
            if (company.Type == (int)DcsCompanyType.分公司) {
                 sql = "select Id,CustomerCompanyId,ContactPerson,ContactPhone,Status,CreatorName,CreateTime,ModifierName,ModifyTime,CompanyCode,CompanyName,CompanyType from(select a.Id,a.CustomerCompanyId,a.ContactPerson,a.ContactPhone,a.Status,a.CreatorName,a.CreateTime,a.ModifierName,a.ModifyTime,c.Code as CompanyCode,c.Name as CompanyName,c.Type as CompanyType from CustomerInformation a inner join company c on c.id = a.CustomerCompanyId where a.status = 1 and a.SalesCompanyId = "+user.EnterpriseId+""
                                + " union select b.id,b.id,b.ContactPerson,b.ContactPhone,b.status,b.CreatorName,b.CreateTime,b.ModifierName,b.ModifyTime,b.Code as CompanyCode,b.Name as CompanyName,b.Type as CompanyType from company b where b.type = 6 and b.status =1) temp where 1=1 ";
                if (!string.IsNullOrEmpty(companyCode)) {
                    sql += " and companycode like '%" + companyCode + "%'";
                }
                if (!string.IsNullOrEmpty(companyName)) {
                    sql += " and companyName like '%" + companyName + "%'";
                }
                if (companyType.HasValue) { 
                    sql += " and companyType = " + companyType + "";
                }
                sql += " order by createtime desc";
            } else {
                 sql = "select a.Id,a.CustomerCompanyId,a.ContactPerson,a.ContactPhone,a.Status,a.CreatorName,a.CreateTime,a.ModifierName,a.ModifyTime,c.Code as CompanyCode,c.Name as CompanyName,c.Type as CompanyType from CustomerInformation a inner join company c on c.id = a.CustomerCompanyId where a.status = 1 and a.SalesCompanyId = " + user.EnterpriseId + "";
                if (!string.IsNullOrEmpty(companyCode)) {
                    sql += " and c.code like '%" + companyCode + "%'";
                }
                if (!string.IsNullOrEmpty(companyName)) {
                    sql += " and c.Name like '%" + companyName + "%'";
                }
                if (companyType.HasValue) { 
                    sql += " and c.Type = " + companyType + "";
                }
                sql += " order by a.createtime desc";
            }
            var search = ObjectContext.ExecuteStoreQuery<VirtualCustomerInformationForTransfer>(sql).ToList();
            return search;
        }

        public IQueryable<CustomerInformation> GetCustomerInformationWithCustomerCompany() {
            return this.ObjectContext.CustomerInformations.Include("CustomerCompany").Where(r => r.Status == (int)DcsMasterDataStatus.有效).OrderByDescending(entity => entity.Id);
        }
    
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:33
        //原分布类的函数全部转移到Ach                                                               
        public IEnumerable<VirtualCustomerInformationForTransfer> GetCustomerInformationWithCustomerCompanyForTransfer(string companyCode,string companyName,int? companyType) {
            return new CustomerInformationAch(this).GetCustomerInformationWithCustomerCompanyForTransfer(companyCode,companyName,companyType);
        }
        public IQueryable<CustomerInformation> GetCustomerInformationsWithCustomerAndSalesCompany() {
            return new CustomerInformationAch(this).GetCustomerInformationsWithCustomerAndSalesCompany();
        }

        public IQueryable<CustomerInformation> GetCustomerInformationWithCustomerCompany() {
            return new CustomerInformationAch(this).GetCustomerInformationWithCustomerCompany();
        }
    }
}
