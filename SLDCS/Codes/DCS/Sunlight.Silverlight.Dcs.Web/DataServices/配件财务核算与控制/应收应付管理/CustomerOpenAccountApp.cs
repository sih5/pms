﻿using System;
using System.Linq;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class CustomerOpenAccountAppAch : DcsSerivceAchieveBase {
        internal void InsertCustomerOpenAccountAppValidate(CustomerOpenAccountApp customerOpenAccountApp) {
            var userInfo = Utils.GetCurrentUserInfo();
            if(string.IsNullOrWhiteSpace(customerOpenAccountApp.Code) || customerOpenAccountApp.Code == GlobalVar.ASSIGNED_BY_SERVER) {
                var accountGroup = ObjectContext.AccountGroups.SingleOrDefault(r => r.Id == customerOpenAccountApp.AccountGroupId);
                customerOpenAccountApp.Code = CodeGenerator.Generate("CustomerOpenAccountApp", accountGroup.Code);
            }

            customerOpenAccountApp.CreatorId = userInfo.Id;
            customerOpenAccountApp.CreatorName = userInfo.Name;
            customerOpenAccountApp.CreateTime = DateTime.Now;
        }

        internal void UpdateCustomerOpenAccountAppValidate(CustomerOpenAccountApp customerOpenAccountApp) {

            var userInfo = Utils.GetCurrentUserInfo();
            customerOpenAccountApp.ModifierId = userInfo.Id;
            customerOpenAccountApp.ModifierName = userInfo.Name;
            customerOpenAccountApp.ModifyTime = DateTime.Now;
        }

        public void InsertCustomerOpenAccountApp(CustomerOpenAccountApp customerOpenAccountApp) {
            InsertToDatabase(customerOpenAccountApp);
            this.InsertCustomerOpenAccountAppValidate(customerOpenAccountApp);
        }

        public void UpdateCustomerOpenAccountApp(CustomerOpenAccountApp customerOpenAccountApp) {
            UpdateToDatabase(customerOpenAccountApp);
            this.UpdateCustomerOpenAccountAppValidate(customerOpenAccountApp);
        }

        public IQueryable<CustomerOpenAccountApp> GetCustomerOpenAccountAppsWithCustomerCompany() {
            return ObjectContext.CustomerOpenAccountApps.Include("Company").Include("AccountGroup").OrderBy(entity => entity.Id);
        }
        public IQueryable<VirtualCustomerOpenAccountApp> GetVirtualCustomerOpenAccountAppsWithCustomerCompany(string companyName, string companyCode, DateTime? beginCreateTime, DateTime? endCreateTime, int? status, string businessCode) {
            var userInfo = Utils.GetCurrentUserInfo();
            var customerAccountHisDetailsquery = from p in ObjectContext.CustomerOpenAccountApps
                                                 join b in ObjectContext.Companies.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on p.CustomerCompanyId equals b.Id
                                                 join d in ObjectContext.AccountGroups.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.SalesCompanyId == userInfo.EnterpriseId) on p.AccountGroupId equals d.Id
                                                 join salesUnit in ObjectContext.SalesUnits.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on new {
                                                     accountGroupId = p.AccountGroupId,
                                                     customerCompanyId = d.SalesCompanyId
                                                 }equals new {
                                                     accountGroupId = salesUnit.AccountGroupId,
                                                     customerCompanyId = salesUnit.OwnerCompanyId
                                                 }into sale
                                                 from salesUnit in sale.DefaultIfEmpty()
                                                 join dealerServiceInfo in ObjectContext.DealerServiceInfoes.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on new {
                                                     customerCompanyId = p.CustomerCompanyId,
                                                     PartsSalesCategoryId = salesUnit.PartsSalesCategoryId
                                                 }equals new {
                                                     customerCompanyId = dealerServiceInfo.DealerId,
                                                     PartsSalesCategoryId = dealerServiceInfo.PartsSalesCategoryId
                                                 }into acc1
                                                 from dealerServiceInfo in acc1.DefaultIfEmpty()
                                                 orderby p.Id
                                                 select new VirtualCustomerOpenAccountApp {
                                                     Id = p.Id,
                                                     BusinessCode =
                                                     b.Type == (int)DcsCompanyType.服务站 || b.Type == (int)DcsCompanyType.服务站兼代理库 ? dealerServiceInfo.BusinessCode :
                                                     b.Type == (int)DcsCompanyType.代理库 ? salesUnit.BusinessCode : null,
                                                     Code = p.Code,
                                                     AccountGroupName = d.Name,
                                                     CompanyName = b.Name,
                                                     CompanyCode = b.Code,
                                                     AccountInitialDeposit = p.AccountInitialDeposit,
                                                     Remark = p.Remark,
                                                     Operator = p.Operator,
                                                     Status = p.Status,
                                                     CreatorName = p.CreatorName,
                                                     CreateTime = p.CreateTime,
                                                     ModifierName = p.ModifierName,
                                                     ModifyTime = p.ModifyTime,
                                                     AbandonerName = p.AbandonerName,
                                                     AbandonTime = p.AbandonTime
                                                 };
            if(!string.IsNullOrEmpty(companyName)) {
                customerAccountHisDetailsquery = customerAccountHisDetailsquery.Where(r => r.CompanyName.Contains(companyName));
            }

            if(!string.IsNullOrEmpty(companyCode)) {
                customerAccountHisDetailsquery = customerAccountHisDetailsquery.Where(r => r.CompanyCode.Contains(companyCode));
            }

            if(status.HasValue) {
                customerAccountHisDetailsquery = customerAccountHisDetailsquery.Where(r => r.Status == status);
            }

            if(beginCreateTime.HasValue) {
                customerAccountHisDetailsquery = customerAccountHisDetailsquery.Where(r => r.CreateTime >= beginCreateTime);
            }
            if(endCreateTime.HasValue) {
                customerAccountHisDetailsquery = customerAccountHisDetailsquery.Where(r => r.CreateTime <= endCreateTime);
            }
            if(!string.IsNullOrEmpty(businessCode)) {
                customerAccountHisDetailsquery = customerAccountHisDetailsquery.Where(r => r.BusinessCode.Contains(businessCode));
            }
            return customerAccountHisDetailsquery.Distinct().OrderBy(r => r.Id);
        }
    
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:33
        //原分布类的函数全部转移到Ach                                                               

        public void InsertCustomerOpenAccountApp(CustomerOpenAccountApp customerOpenAccountApp) {
            new CustomerOpenAccountAppAch(this).InsertCustomerOpenAccountApp(customerOpenAccountApp);
        }

        public void UpdateCustomerOpenAccountApp(CustomerOpenAccountApp customerOpenAccountApp) {
            new CustomerOpenAccountAppAch(this).UpdateCustomerOpenAccountApp(customerOpenAccountApp);
        }

        public IQueryable<CustomerOpenAccountApp> GetCustomerOpenAccountAppsWithCustomerCompany() {
            return new CustomerOpenAccountAppAch(this).GetCustomerOpenAccountAppsWithCustomerCompany();
        }
                public IQueryable<VirtualCustomerOpenAccountApp> GetVirtualCustomerOpenAccountAppsWithCustomerCompany(string companyName, string companyCode, DateTime? beginCreateTime, DateTime? endCreateTime, int? status, string businessCode) {
            return new CustomerOpenAccountAppAch(this).GetVirtualCustomerOpenAccountAppsWithCustomerCompany(companyName,companyCode,beginCreateTime,endCreateTime,status,businessCode);
        }
    }
}
