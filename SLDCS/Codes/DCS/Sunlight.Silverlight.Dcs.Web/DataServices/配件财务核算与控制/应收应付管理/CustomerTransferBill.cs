﻿using System;
using System.Linq;
using Sunlight.Silverlight.Web;
using Quartz.Xml;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class CustomerTransferBillAch : DcsSerivceAchieveBase {
        internal void InsertCustomerTransferBillValidate(CustomerTransferBill customerTransferBill) {
            var userInfo = Utils.GetCurrentUserInfo();
            customerTransferBill.CreatorId = userInfo.Id;
            customerTransferBill.CreatorName = userInfo.Name;
            customerTransferBill.CreateTime = DateTime.Now;
            var dbCompany = ObjectContext.Companies.SingleOrDefault(r => r.Id == customerTransferBill.SalesCompanyId);

            if(string.IsNullOrWhiteSpace(customerTransferBill.Code) || customerTransferBill.Code == GlobalVar.ASSIGNED_BY_SERVER)
                customerTransferBill.Code = CodeGenerator.Generate("CustomerTransferBill", dbCompany.Code);
        }

        internal void UpdateCustomerTransferBillValidate(CustomerTransferBill customerTransferBill) {

            var userInfo = Utils.GetCurrentUserInfo();
            customerTransferBill.ModifierId = userInfo.Id;
            customerTransferBill.ModifierName = userInfo.Name;
            customerTransferBill.ModifyTime = DateTime.Now;
        }

        public void InsertCustomerTransferBill(CustomerTransferBill customerTransferBill) {
            if(null != customerTransferBill.InvoiceDate.Value) {
                //当过帐日期不为空时，验证过帐日期是否有效
                this.验证过账日期是否有效(customerTransferBill.InvoiceDate);
            }
            InsertToDatabase(customerTransferBill);
            this.InsertCustomerTransferBillValidate(customerTransferBill);
        }

        public void UpdateCustomerTransferBill(CustomerTransferBill customerTransferBill) {
            if(null != customerTransferBill.InvoiceDate.Value) {
                //当过帐日期不为空时，验证过帐日期是否有效
                this.验证过账日期是否有效(customerTransferBill.InvoiceDate);
            }
            UpdateToDatabase(customerTransferBill);
            this.UpdateCustomerTransferBillValidate(customerTransferBill);
        }

        public CustomerTransferBill GetCustomerTransferBillWithDetailsBtId(int id) {
            var customerTransferBill = ObjectContext.CustomerTransferBills.SingleOrDefault(r => r.Id == id);
            if(customerTransferBill == null)
                return null;

            var outboundCustomerCompany = ObjectContext.Companies.SingleOrDefault(r => r.Id == customerTransferBill.OutboundCustomerCompanyId);
            var inboundCustomerCompany = ObjectContext.Companies.SingleOrDefault(r => r.Id == customerTransferBill.InboundCustomerCompanyId);
            return customerTransferBill;
        }

        public IQueryable<CustomerTransferBill> GetCustomerTransferBillWithDetails() {
            var customerTransferBills = from ctb in ObjectContext.CustomerTransferBills.Include("OutboundCustomerCompany").Include("InboundCustomerCompany").Include("OutboundAccountGroup").Include("InboundAccountGroup")
                                        join ctbs in ObjectContext.CustomerTransferBill_Sync on ctb.Code equals ctbs.Objid
                                        into newctbs
                                        from a in newctbs.DefaultIfEmpty()
                                        select new {
                                            ctb,
                                            Message = a.Message,
                                            ctb.OutboundCustomerCompany,
                                            ctb.InboundCustomerCompany,
                                            ctb.OutboundAccountGroup,
                                            ctb.InboundAccountGroup
                                        };
            var result = customerTransferBills.Select(o => o.ctb);
            result = result.OrderByDescending(r => r.Id);
            foreach(var newCtb in customerTransferBills) {
                newCtb.ctb.Message = newCtb.Message;
                newCtb.ctb.OutboundCustomerCompany = newCtb.OutboundCustomerCompany;
                newCtb.ctb.InboundCustomerCompany = newCtb.InboundCustomerCompany;
                newCtb.ctb.OutboundAccountGroup = newCtb.OutboundAccountGroup;
                newCtb.ctb.InboundAccountGroup = newCtb.InboundAccountGroup;
            }
            return result.OrderBy(v => v.Id);
        }
        public void 验证过账日期是否有效(DateTime? invoiceDate) {
            string year = invoiceDate.Value.Year.ToString();
            string month = invoiceDate.Value.Month.ToString();
            var accountPeriod = ObjectContext.AccountPeriods.Where(r => r.Month == month && r.Year == year).SingleOrDefault();
            if(null == accountPeriod) {
                throw new ValidationException(ErrorStrings.AccountPeriod_Validation2);
            } else if((int)DcsAccountPeriodStatus.已关闭 == accountPeriod.Status) {
                throw new ValidationException(ErrorStrings.AccountPeriod_Validation3);
            }
        }
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:33
        //原分布类的函数全部转移到Ach                                                               

        public void InsertCustomerTransferBill(CustomerTransferBill customerTransferBill) {
            new CustomerTransferBillAch(this).InsertCustomerTransferBill(customerTransferBill);
        }

        public void UpdateCustomerTransferBill(CustomerTransferBill customerTransferBill) {
            new CustomerTransferBillAch(this).UpdateCustomerTransferBill(customerTransferBill);
        }

        public CustomerTransferBill GetCustomerTransferBillWithDetailsBtId(int id) {
            return new CustomerTransferBillAch(this).GetCustomerTransferBillWithDetailsBtId(id);
        }

        public IQueryable<CustomerTransferBill> GetCustomerTransferBillWithDetails() {
            return new CustomerTransferBillAch(this).GetCustomerTransferBillWithDetails();
        }
    }
}