﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class InvoiceInformationAch : DcsSerivceAchieveBase {
        internal void InsertInvoiceInformationValidate(InvoiceInformation invoiceInformation) {

            var dbInvoiceInformation = ObjectContext.InvoiceInformations.Where(r => r.Status != (int)DcsInvoiceInformationStatus.作废 && r.InvoiceNumber == invoiceInformation.InvoiceNumber).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbInvoiceInformation != null) {
                throw new ValidationException(ErrorStrings.PartsPurchaseSettleBill_Validation25);
            }
            var tempTaxValue = invoiceInformation.InvoiceTax == null ? 0 : invoiceInformation.InvoiceTax.Value;
            invoiceInformation.InvoiceTax = Math.Round(tempTaxValue, 2);
            var compy = ObjectContext.Companies.SingleOrDefault(r => r.Id == invoiceInformation.OwnerCompanyId);
            if(string.IsNullOrWhiteSpace(invoiceInformation.Code) || invoiceInformation.Code == GlobalVar.ASSIGNED_BY_SERVER)
                invoiceInformation.Code = CodeGenerator.Generate("InvoiceInformation", compy.Code);


            var userInfo = Utils.GetCurrentUserInfo();
            invoiceInformation.CreatorId = userInfo.Id;
            invoiceInformation.CreatorName = userInfo.Name;
            invoiceInformation.CreateTime = DateTime.Now;
        }

        internal void UpdateInvoiceInformationValidate(InvoiceInformation invoiceInformation) {

            var userInfo = Utils.GetCurrentUserInfo();
            invoiceInformation.ModifierId = userInfo.Id;
            invoiceInformation.ModifierName = userInfo.Name;
            invoiceInformation.ModifyTime = DateTime.Now;
        }
        internal void DeleteInvoiceInformationValidate(InvoiceInformation invoiceInformation) {

        }

        //public void DeletePersonSalesCenterLink(InvoiceInformation invoiceInformation) {
        //    DeleteFromDatabase(invoiceInformation);
        //    this.DeleteInvoiceInformationValidate(invoiceInformation);
        //}

        public void InsertInvoiceInformation(InvoiceInformation invoiceInformation) {
            if(invoiceInformation.SourceType == (int)DcsInvoiceInformationSourceType.配件销售退货结算单) {
                var salesRtnSettleInvoiceRel = new SalesRtnSettleInvoiceRel();
                salesRtnSettleInvoiceRel.PartsSalesRtnSettlementId = invoiceInformation.SourceId;
                invoiceInformation.SalesRtnSettleInvoiceRels.Add(salesRtnSettleInvoiceRel);
            }
            InsertToDatabase(invoiceInformation);
            this.InsertInvoiceInformationValidate(invoiceInformation);
        }

        public void DeleteInvoiceInformation(InvoiceInformation invoiceInformation) {
            DeleteFromDatabase(invoiceInformation);
            this.DeleteInvoiceInformationValidate(invoiceInformation);
        }

        public void UpdateInvoiceInformation(InvoiceInformation invoiceInformation) {
            UpdateToDatabase(invoiceInformation);
            this.UpdateInvoiceInformationValidate(invoiceInformation);
        }

        public IQueryable<InvoiceInformation> GetInvoiceInformationsByPartsSalesCategoryId(int? partsSalesCategoryId) {
            if(partsSalesCategoryId.HasValue) {
                var partsSalesRtnSettlementId = ObjectContext.PartsSalesRtnSettlements.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId).Select(r => r.Id);
                var result1 = ObjectContext.InvoiceInformations.Where(t => t.SourceType == (int)DcsInvoiceInformationSourceType.配件销售退货结算单 && partsSalesRtnSettlementId.Contains(t.SourceId));
                var partsSalesSettlementId = ObjectContext.PartsSalesSettlements.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId).Select(r => r.Id);
                var result2 = ObjectContext.InvoiceInformations.Where(t => t.SourceType == (int)DcsInvoiceInformationSourceType.配件销售结算单 && partsSalesSettlementId.Contains(t.SourceId));
                return result1.Union(result2).OrderBy(r => r.Id);
            }
            return ObjectContext.InvoiceInformations.OrderBy(r => r.Id);
        }

        public IQueryable<VirtualInvoiceInformation> 根据业务类型查询发票信息() {
            //销售退货结算发票（品牌取值为退货结算单上的品牌）
            var partsSalesRtnInvoiceInformations = from a in ObjectContext.InvoiceInformations.Where(t => t.SourceType == (int)DcsInvoiceInformationSourceType.配件销售退货结算单)
                                                   join b in ObjectContext.PartsSalesRtnSettlements
                                                   on a.SourceId equals b.Id
                                                   select new {
                                                       Id = a.Id,
                                                       InvoiceCode = a.InvoiceCode,
                                                       InvoiceNumber = a.InvoiceNumber,
                                                       PartsSalesCategoryId = b.PartsSalesCategoryId,
                                                       InvoiceCompanyCode = a.InvoiceCompanyCode,
                                                       InvoiceCompanyName = a.InvoiceCompanyName,
                                                       InvoiceReceiveCompanyCode = a.InvoiceReceiveCompanyCode,
                                                       InvoiceReceiveCompanyName = a.InvoiceReceiveCompanyName,
                                                       InvoiceAmount = a.InvoiceAmount,
                                                       TaxRate = a.TaxRate,
                                                       InvoiceTax = a.InvoiceTax,
                                                       InvoiceDate = a.InvoiceDate,
                                                       SourceType = a.SourceType,
                                                       SourceCode = a.SourceCode,
                                                       OwnerCompanyId = a.OwnerCompanyId,
                                                       InvoicePurpose = a.InvoicePurpose,
                                                       Type = a.Type,
                                                       Status = a.Status,
                                                       Code = a.Code,
                                                       CreatorName = a.CreatorName,
                                                       CreateTime = a.CreateTime,
                                                       ApproverName = a.ApproverName,
                                                       ApproveTime = a.ApproveTime,
                                                       InvoiceCompanyId = a.InvoiceCompanyId,
                                                       InvoiceDownloadLink = a.InvoiceDownloadLink
                                                   };
            //销售结算发票（品牌取值为销售结算单上的品牌）
            var partsSalesInvoiceInformations = from a in ObjectContext.InvoiceInformations.Where(t => t.SourceType == (int)DcsInvoiceInformationSourceType.配件销售结算单)
                                                join b in ObjectContext.PartsSalesSettlements
                                                on a.SourceId equals b.Id
                                                select new {
                                                    Id = a.Id,
                                                    InvoiceCode = a.InvoiceCode,
                                                    InvoiceNumber = a.InvoiceNumber,
                                                    PartsSalesCategoryId = b.PartsSalesCategoryId,
                                                    InvoiceCompanyCode = a.InvoiceCompanyCode,
                                                    InvoiceCompanyName = a.InvoiceCompanyName,
                                                    InvoiceReceiveCompanyCode = a.InvoiceReceiveCompanyCode,
                                                    InvoiceReceiveCompanyName = a.InvoiceReceiveCompanyName,
                                                    InvoiceAmount = a.InvoiceAmount,
                                                    TaxRate = a.TaxRate,
                                                    InvoiceTax = a.InvoiceTax,
                                                    InvoiceDate = a.InvoiceDate,
                                                    SourceType = a.SourceType,
                                                    SourceCode = a.SourceCode,
                                                    OwnerCompanyId = a.OwnerCompanyId,
                                                    InvoicePurpose = a.InvoicePurpose,
                                                    Type = a.Type,
                                                    Status = a.Status,
                                                    Code = a.Code,
                                                    CreatorName = a.CreatorName,
                                                    CreateTime = a.CreateTime,
                                                    ApproverName = a.ApproverName,
                                                    ApproveTime = a.ApproveTime,
                                                    InvoiceCompanyId = a.InvoiceCompanyId,
                                                    InvoiceDownloadLink = a.InvoiceDownloadLink
                                                };
            var tempInvoiceInformation = partsSalesRtnInvoiceInformations.Concat(partsSalesInvoiceInformations);
            //根据企业类型赋值企业编码
            #region 客户企业类型为服务站或服务站兼代理库，业务编码赋值为经销商管理信息对应的业务编码
            var result1 = from a in tempInvoiceInformation
                          join c in ObjectContext.Companies.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && (r.Type == (int)DcsCompanyType.服务站 || r.Type == (int)DcsCompanyType.服务站兼代理库)) on a.InvoiceCompanyId equals c.Id
                          join d in ObjectContext.DealerServiceInfoes.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on new {
                              k1 = a.InvoiceCompanyId,
                              k2 = a.PartsSalesCategoryId
                          } equals new {
                              k1 = d.DealerId,
                              k2 = d.PartsSalesCategoryId
                          } into tempTable
                          from ss in tempTable.DefaultIfEmpty()
                          select new VirtualInvoiceInformation {
                              Id = a.Id,
                              InvoiceCode = a.InvoiceCode,
                              InvoiceNumber = a.InvoiceNumber,
                              PartsSalesCategoryId = a.PartsSalesCategoryId,
                              InvoiceCompanyCode = a.InvoiceCompanyCode,
                              InvoiceCompanyName = a.InvoiceCompanyName,
                              InvoiceReceiveCompanyCode = a.InvoiceReceiveCompanyCode,
                              InvoiceReceiveCompanyName = a.InvoiceReceiveCompanyName,
                              InvoiceAmount = a.InvoiceAmount,
                              TaxRate = a.TaxRate,
                              InvoiceTax = a.InvoiceTax,
                              InvoiceDate = a.InvoiceDate,
                              SourceType = a.SourceType,
                              SourceCode = a.SourceCode,
                              OwnerCompanyId = a.OwnerCompanyId,
                              InvoicePurpose = a.InvoicePurpose,
                              Type = a.Type,
                              Status = a.Status,
                              Code = a.Code,
                              CreatorName = a.CreatorName,
                              CreateTime = a.CreateTime,
                              ApproverName = a.ApproverName,
                              ApproveTime = a.ApproveTime,
                              InvoiceCompanyId = a.InvoiceCompanyId,
                              BusinessCode = ss.BusinessCode,
                              InvoiceDownloadLink = a.InvoiceDownloadLink
                          };
            #endregion
            #region 客户企业类型为代理库 业务编码赋值为客户企业对应的销售组织的业务编码
            var result2 = from a in tempInvoiceInformation
                          join b in ObjectContext.Companies.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.Type == (int)DcsCompanyType.代理库) on a.InvoiceCompanyId equals b.Id
                          join c in ObjectContext.SalesUnits.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on new {
                              k1 = a.InvoiceCompanyId,
                              k2 = a.PartsSalesCategoryId
                          } equals new {
                              k1 = c.OwnerCompanyId,
                              k2 = c.PartsSalesCategoryId
                          } into tempTable
                          from ss in tempTable.DefaultIfEmpty()
                          select new VirtualInvoiceInformation {
                              Id = a.Id,
                              InvoiceCode = a.InvoiceCode,
                              InvoiceNumber = a.InvoiceNumber,
                              PartsSalesCategoryId = a.PartsSalesCategoryId,
                              InvoiceCompanyCode = a.InvoiceCompanyCode,
                              InvoiceCompanyName = a.InvoiceCompanyName,
                              InvoiceReceiveCompanyCode = a.InvoiceReceiveCompanyCode,
                              InvoiceReceiveCompanyName = a.InvoiceReceiveCompanyName,
                              InvoiceAmount = a.InvoiceAmount,
                              TaxRate = a.TaxRate,
                              InvoiceTax = a.InvoiceTax,
                              InvoiceDate = a.InvoiceDate,
                              SourceType = a.SourceType,
                              SourceCode = a.SourceCode,
                              OwnerCompanyId = a.OwnerCompanyId,
                              InvoicePurpose = a.InvoicePurpose,
                              Type = a.Type,
                              Status = a.Status,
                              Code = a.Code,
                              CreatorName = a.CreatorName,
                              CreateTime = a.CreateTime,
                              ApproverName = a.ApproverName,
                              ApproveTime = a.ApproveTime,
                              InvoiceCompanyId = a.InvoiceCompanyId,
                              BusinessCode = ss.BusinessCode,
                              InvoiceDownloadLink = a.InvoiceDownloadLink
                          };
            #endregion
            #region 客户企业类型为其他,业务编码赋值为空
            var exceptCompanyType = new int[] { (int)DcsCompanyType.代理库, (int)DcsCompanyType.服务站, (int)DcsCompanyType.服务站兼代理库 };
            var result3 = from a in tempInvoiceInformation
                          join b in ObjectContext.Companies.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && (!exceptCompanyType.Contains(r.Type))) on a.InvoiceCompanyId equals b.Id
                          select new VirtualInvoiceInformation {
                              Id = a.Id,
                              InvoiceCode = a.InvoiceCode,
                              InvoiceNumber = a.InvoiceNumber,
                              PartsSalesCategoryId = a.PartsSalesCategoryId,
                              InvoiceCompanyCode = a.InvoiceCompanyCode,
                              InvoiceCompanyName = a.InvoiceCompanyName,
                              InvoiceReceiveCompanyCode = a.InvoiceReceiveCompanyCode,
                              InvoiceReceiveCompanyName = a.InvoiceReceiveCompanyName,
                              InvoiceAmount = a.InvoiceAmount,
                              TaxRate = a.TaxRate,
                              InvoiceTax = a.InvoiceTax,
                              InvoiceDate = a.InvoiceDate,
                              SourceType = a.SourceType,
                              SourceCode = a.SourceCode,
                              OwnerCompanyId = a.OwnerCompanyId,
                              InvoicePurpose = a.InvoicePurpose,
                              Type = a.Type,
                              Status = a.Status,
                              Code = a.Code,
                              CreatorName = a.CreatorName,
                              CreateTime = a.CreateTime,
                              ApproverName = a.ApproverName,
                              ApproveTime = a.ApproveTime,
                              InvoiceCompanyId = a.InvoiceCompanyId,
                              BusinessCode = "",
                              InvoiceDownloadLink = a.InvoiceDownloadLink
                          };
            #endregion
            var tempResult = result1.Concat(result2).Concat(result3);
            return tempResult.OrderBy(r => r.Id);
        }

        public IQueryable<InvoiceInformation> GetInvoiceInformationDetails() {

            return this.ObjectContext.InvoiceInformations.Include("PartsSalesCategory").OrderByDescending(r => r.ApproveTime);
        }

        public IQueryable<VirtualInvoiceInformation> GetVirtualInvoiceInformation() {
            var query = from a in ObjectContext.InvoiceInformations.Where(r => r.SourceType == (int)DcsInvoiceInformationSourceType.配件采购退货结算单 || r.SourceType == (int)DcsInvoiceInformationSourceType.配件采购结算单)
                        join c in ObjectContext.PartsSalesCategories.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on a.PartsSalesCategoryId equals c.Id into tempTable
                        from t in tempTable.DefaultIfEmpty()
                        join s in ObjectContext.BranchSupplierRelations.Where(r => r.Status == (int)DcsBaseDataStatus.有效)
                        on new {
                            SupplierId = a.InvoiceCompanyId,
                            PartsSalesCategoryId = a.PartsSalesCategoryId ?? 0
                        } equals new {
                            SupplierId = s.SupplierId,
                            PartsSalesCategoryId = s.PartsSalesCategoryId
                        } into tempTable1

                        from t1 in tempTable1.DefaultIfEmpty()
                        select new VirtualInvoiceInformation {
                            Id = a.Id,
                            InvoiceCode = a.InvoiceCode,
                            InvoiceNumber = a.InvoiceNumber,
                            PartsSalesCategoryId = a.PartsSalesCategoryId,
                            PartsSalesCategoryName = t.Name,
                            InvoiceCompanyCode = a.InvoiceCompanyCode,
                            InvoiceCompanyName = a.InvoiceCompanyName,
                            InvoiceReceiveCompanyCode = a.InvoiceReceiveCompanyCode,
                            InvoiceReceiveCompanyName = a.InvoiceReceiveCompanyName,
                            InvoiceAmount = a.InvoiceAmount,
                            TaxRate = a.TaxRate,
                            InvoiceTax = a.InvoiceTax,
                            InvoiceDate = a.InvoiceDate,
                            SourceType = a.SourceType,
                            SourceCode = a.SourceCode,
                            OwnerCompanyId = a.OwnerCompanyId,
                            InvoicePurpose = a.InvoicePurpose,
                            Type = a.Type,
                            Status = a.Status,
                            Code = a.Code,
                            CreatorName = a.CreatorName,
                            CreateTime = a.CreateTime,
                            ApproverName = a.ApproverName,
                            ApproveTime = a.ApproveTime,
                            InvoiceCompanyId = a.InvoiceCompanyId,
                            BusinessCode = t1.BusinessCode

                        };
            return query.OrderByDescending(r => r.ApproveTime);
        }


        public IQueryable<InvoiceInformation> 根据采购结算单获取发票与采购结算单信息(int id) {
            /*
            查询采购结算单发票关联单（采购结算单发票关联单.采购结算单Id=采购结算单.id）
            1、返回发票信息（采购结算单发票关联单.发票Id=发票信息.id）
            2、返回配件采购结算单（采购结算单发票关联单.发票id=步骤1 发票信息.id，采购结算单发票关联单.采购结算单id=配件采购结算单.id）需要返回相关的所有配件采购结算单
            */
            var invoiceId = ObjectContext.PurchaseSettleInvoiceRels.Where(r => r.PartsPurchaseSettleBillId == id).Select(r => r.InvoiceId).FirstOrDefault();
            var invoiceInformation = this.ObjectContext.InvoiceInformations.SingleOrDefault(t => t.Id == invoiceId);
            if(invoiceInformation == null)
                return null;
            return this.ObjectContext.InvoiceInformations.Where(r => r.Id == invoiceInformation.Id).Include("PurchaseSettleInvoiceRels").Include("PurchaseSettleInvoiceRels.PartsPurchaseSettleBill");


        }

        public IQueryable<PurchaseSettleInvoiceResource> 查询采购结算发票源单据(int invoiceId, string type) {
            /*
            查询采购结算发票源单据（发票id=参数 发票id ,类型=参数 类型）
             */
            return ObjectContext.PurchaseSettleInvoiceResources.Where(r => r.InvoiceId == invoiceId && r.Type == type);
        }

        public IQueryable<SalesSettleInvoiceResource> 查询销售结算发票源单据(int invoiceId, string type) {
            /*
            查询销售结算发票源单据(视图)（发票Id=参数 发票id ，类型=参数 类型）
             */
            return ObjectContext.SalesSettleInvoiceResources.Where(r => r.InvoiceId == invoiceId && r.Type == type);
        }

    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:33
        //原分布类的函数全部转移到Ach                                                               

        public void InsertInvoiceInformation(InvoiceInformation invoiceInformation) {
            new InvoiceInformationAch(this).InsertInvoiceInformation(invoiceInformation);
        }

        public void DeleteInvoiceInformation(InvoiceInformation invoiceInformation) {
            new InvoiceInformationAch(this).DeleteInvoiceInformation(invoiceInformation);
        }

        public void UpdateInvoiceInformation(InvoiceInformation invoiceInformation) {
            new InvoiceInformationAch(this).UpdateInvoiceInformation(invoiceInformation);
        }

        public IQueryable<InvoiceInformation> GetInvoiceInformationsByPartsSalesCategoryId(int? partsSalesCategoryId) {
            return new InvoiceInformationAch(this).GetInvoiceInformationsByPartsSalesCategoryId(partsSalesCategoryId);
        }

        public IQueryable<VirtualInvoiceInformation> 根据业务类型查询发票信息() {
            return new InvoiceInformationAch(this).根据业务类型查询发票信息();
        }

        public IQueryable<InvoiceInformation> GetInvoiceInformationDetails() {
            return new InvoiceInformationAch(this).GetInvoiceInformationDetails();
        }

        public IQueryable<VirtualInvoiceInformation> GetVirtualInvoiceInformation() {
            return new InvoiceInformationAch(this).GetVirtualInvoiceInformation();
        }


        public IQueryable<InvoiceInformation> 根据采购结算单获取发票与采购结算单信息(int id) {
            return new InvoiceInformationAch(this).根据采购结算单获取发票与采购结算单信息(id);
        }

        public IQueryable<PurchaseSettleInvoiceResource> 查询采购结算发票源单据(int invoiceId, string type) {
            return new InvoiceInformationAch(this).查询采购结算发票源单据(invoiceId, type);
        }

        public IQueryable<SalesSettleInvoiceResource> 查询销售结算发票源单据(int invoiceId, string type) {
            return new InvoiceInformationAch(this).查询销售结算发票源单据(invoiceId, type);
        }
    }
}
