﻿using System;
using System.Linq;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {

    partial class PayOutBillAch : DcsSerivceAchieveBase {
        internal void InsertPayOutBillValidate(PayOutBill payOutBill) {
            var userInfo = Utils.GetCurrentUserInfo();
            payOutBill.CreatorId = userInfo.Id;
            payOutBill.CreatorName = userInfo.Name;
            payOutBill.CreateTime = DateTime.Now;
            var company = ObjectContext.Companies.Where(r => r.Id == payOutBill.SupplierCompanyId).SingleOrDefault();
            if(string.IsNullOrWhiteSpace(payOutBill.Code) || payOutBill.Code == GlobalVar.ASSIGNED_BY_SERVER)
                payOutBill.Code = CodeGenerator.Generate("PayOutBill", company.Code);
        }

        internal void UpdatePayOutBillValidate(PayOutBill payOutBill) {
            var userInfo = Utils.GetCurrentUserInfo();
            payOutBill.ModifierId = userInfo.Id;
            payOutBill.ModifierName = userInfo.Name;
            payOutBill.ModifyTime = DateTime.Now;
        }

        public void InsertPayOutBill(PayOutBill payOutBill) {
            InsertToDatabase(payOutBill);
            this.InsertPayOutBillValidate(payOutBill);
        }

        public void UpdatePayOutBill(PayOutBill payOutBill) {
            UpdateToDatabase(payOutBill);
            this.UpdatePayOutBillValidate(payOutBill);
        }
        public IQueryable<PayOutBill> GetPayOutBillWithCompany() {
            return ObjectContext.PayOutBills.Include("Company").Include("BankAccount").OrderBy(entity => entity.Id);
        }
        public PayOutBill GetPayOutBillWithDetailsById(int id) {
            var dbPayOutBill = ObjectContext.PayOutBills.SingleOrDefault(e => e.Id == id);
            if(dbPayOutBill != null) {
                var dbSupplierCompany = ObjectContext.Companies.SingleOrDefault(e => e.Id == dbPayOutBill.SupplierCompanyId && e.Status == (int)DcsMasterDataStatus.有效);
                var dbBankAccount = ObjectContext.BankAccounts.SingleOrDefault(e => e.Id == dbPayOutBill.BankAccountId && e.Status == (int)DcsMasterDataStatus.有效);
            }
            return dbPayOutBill;
        }
    
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:33
        //原分布类的函数全部转移到Ach                                                               

        public void InsertPayOutBill(PayOutBill payOutBill) {
            new PayOutBillAch(this).InsertPayOutBill(payOutBill);
        }

        public void UpdatePayOutBill(PayOutBill payOutBill) {
            new PayOutBillAch(this).UpdatePayOutBill(payOutBill);
        }
                public IQueryable<PayOutBill> GetPayOutBillWithCompany() {
            return new PayOutBillAch(this).GetPayOutBillWithCompany();
        }
                public PayOutBill GetPayOutBillWithDetailsById(int id) {
            return new PayOutBillAch(this).GetPayOutBillWithDetailsById(id);
        }
    }
}
