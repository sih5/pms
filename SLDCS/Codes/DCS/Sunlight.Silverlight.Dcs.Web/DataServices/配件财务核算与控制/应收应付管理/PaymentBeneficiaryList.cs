﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PaymentBeneficiaryListAch : DcsSerivceAchieveBase {
        public PaymentBeneficiaryListAch(DcsDomainService domainService)
            : base(domainService) {
        }

        internal void InsertPaymentBeneficiaryListValidate(PaymentBeneficiaryList paymentBeneficiaryList) {
            var userInfo = Utils.GetCurrentUserInfo();
            paymentBeneficiaryList.CreatorId = userInfo.Id;
            paymentBeneficiaryList.CreatorName = userInfo.Name;
            paymentBeneficiaryList.CreateTime = DateTime.Now;
            if(string.IsNullOrWhiteSpace(paymentBeneficiaryList.Code) || paymentBeneficiaryList.Code == GlobalVar.ASSIGNED_BY_SERVER) {
                var company = ObjectContext.Companies.SingleOrDefault(r => r.Id == paymentBeneficiaryList.SalesCompanyId);
                if(company == null)
                    throw new ValidationException(ErrorStrings.PaymentBeneficiaryList_Validation1);
                paymentBeneficiaryList.Code = CodeGenerator.Generate("PaymentBeneficiaryList", company.Code);
            }
        }
        internal void UpdatePaymentBeneficiaryListValidate(PaymentBeneficiaryList paymentBeneficiaryList) {
            var userInfo = Utils.GetCurrentUserInfo();
            paymentBeneficiaryList.ModifierId = userInfo.Id;
            paymentBeneficiaryList.ModifierName = userInfo.Name;
            paymentBeneficiaryList.ModifyTime = DateTime.Now;
        }
        public void InsertPaymentBeneficiaryList(PaymentBeneficiaryList paymentBeneficiaryList) {
            InsertToDatabase(paymentBeneficiaryList);
            this.InsertPaymentBeneficiaryListValidate(paymentBeneficiaryList);
        }
        public void UpdatePaymentBeneficiaryList(PaymentBeneficiaryList paymentBeneficiaryList) {
            UpdateToDatabase(paymentBeneficiaryList);
            this.UpdatePaymentBeneficiaryListValidate(paymentBeneficiaryList);
        }

        public IQueryable<PaymentBeneficiaryList> GetPaymentBeneficiaryListsWithDetails() {
            return ObjectContext.PaymentBeneficiaryLists.Include("Company").Include("PaymentBill.BankAccount").OrderBy(v => v.Id);
        }
        public IQueryable<PaymentBeneficiaryList> GetPaymentBeneficiaryListsWithAccountGroups() {
            return ObjectContext.PaymentBeneficiaryLists.Include("AccountGroup").OrderBy(v => v.Id);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:33
        //原分布类的函数全部转移到Ach                                                               

        public void InsertPaymentBeneficiaryList(PaymentBeneficiaryList paymentBeneficiaryList) {
            new PaymentBeneficiaryListAch(this).InsertPaymentBeneficiaryList(paymentBeneficiaryList);
        }

        public void UpdatePaymentBeneficiaryList(PaymentBeneficiaryList paymentBeneficiaryList) {
            new PaymentBeneficiaryListAch(this).UpdatePaymentBeneficiaryList(paymentBeneficiaryList);
        }
        
        public IQueryable<PaymentBeneficiaryList> GetPaymentBeneficiaryListsWithDetails() {
            return new PaymentBeneficiaryListAch(this).GetPaymentBeneficiaryListsWithDetails();
        }

        public IQueryable<PaymentBeneficiaryList> GetPaymentBeneficiaryListsWithAccountGroups() {
            return new PaymentBeneficiaryListAch(this).GetPaymentBeneficiaryListsWithAccountGroups();
        }
    }
}
