﻿using System;
using System.Linq;
using System.Text;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PaymentBillAch : DcsSerivceAchieveBase {
        internal void InsertPaymentBillValidate(PaymentBill paymentBill) {
            var userInfo = Utils.GetCurrentUserInfo();
            paymentBill.CreatorId = userInfo.Id;
            paymentBill.CreatorName = userInfo.Name;
            paymentBill.CreateTime = DateTime.Now;

            if(string.IsNullOrWhiteSpace(paymentBill.Code) || paymentBill.Code == GlobalVar.ASSIGNED_BY_SERVER)
                paymentBill.Code = CodeGenerator.Generate("PaymentBill", userInfo.EnterpriseCode);
            foreach(var paymentBeneficiaryList in paymentBill.PaymentBeneficiaryLists) {
                paymentBeneficiaryList.PaymentBillCode = paymentBill.Code;
            }
        }

        internal void UpdatePaymentBillValidate(PaymentBill paymentBill) {
            var userInfo = Utils.GetCurrentUserInfo();
            paymentBill.ModifierId = userInfo.Id;
            paymentBill.ModifierName = userInfo.Name;
            paymentBill.ModifyTime = DateTime.Now;
        }

        public void InsertPaymentBill(PaymentBill paymentBill) {
            if(null != paymentBill.InvoiceDate) {
                //验证过账日期是否有效
                this.DomainService.验证过账日期是否有效(paymentBill.InvoiceDate);
            }
            InsertToDatabase(paymentBill);
            this.InsertPaymentBillValidate(paymentBill);
        }

        public void UpdatePaymentBill(PaymentBill paymentBill) {
            if(null != paymentBill.InvoiceDate) {
                //验证过账日期是否有效
                this.DomainService.验证过账日期是否有效(paymentBill.InvoiceDate);
            }
            UpdateToDatabase(paymentBill);
            this.UpdatePaymentBillValidate(paymentBill);
        }

        public IQueryable<PaymentBill> GetPaymentBillWithDetails() {
            return ObjectContext.PaymentBills.Include("CustomerCompany").Include("BankAccount").Include("AccountGroup").OrderBy(e => e.Id);
        }
        public IQueryable<VirtualPaymentBill> GetVirtualPaymentBillWithDetails(string customerCompanyName, string customerCompanyCode, DateTime? beginCreateTime, DateTime? endCreateTime, int? paymentMethod, int? status, string creatorName, string businessCode, DateTime? beginInvoiceDate, DateTime? endInvoiceDate) {
            var userInfo = Utils.GetCurrentUserInfo();
            var customerAccountHisDetailsquery = from p in ObjectContext.PaymentBills.Where(r => r.SalesCompanyId == userInfo.EnterpriseId)
                                                 join pSync in ObjectContext.PaymentBill_Sync on p.Code equals pSync.Objid
                                                 into newPSync
                                                 from paymentBill_Sync in newPSync.DefaultIfEmpty()
                                                 join b in ObjectContext.Companies.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on p.CustomerCompanyId equals b.Id
                                                 join d in ObjectContext.AccountGroups.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on p.AccountGroupId equals d.Id
                                                 into c
                                                 from accountGroup in c.DefaultIfEmpty()
                                                 join g in ObjectContext.BankAccounts.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on p.BankAccountId equals g.Id
                                                 into bank
                                                 from g in bank.DefaultIfEmpty()
                                                 join salesUnit in ObjectContext.SalesUnits.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on new {
                                                     accountGroupId = p.AccountGroupId.Value,
                                                     customerCompanyId = accountGroup.SalesCompanyId
                                                 } equals new {
                                                     accountGroupId = salesUnit.AccountGroupId,
                                                     customerCompanyId = salesUnit.OwnerCompanyId
                                                 } into sale
                                                 from salesUnit in sale.DefaultIfEmpty()
                                                 join dealerServiceInfo in ObjectContext.DealerServiceInfoes.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on new {
                                                     customerCompanyId = p.CustomerCompanyId,
                                                     PartsSalesCategoryId = salesUnit.PartsSalesCategoryId
                                                 } equals new {
                                                     customerCompanyId = dealerServiceInfo.DealerId,
                                                     PartsSalesCategoryId = dealerServiceInfo.PartsSalesCategoryId
                                                 } into acc1
                                                 from dealerServiceInfo in acc1.DefaultIfEmpty()
                                                 orderby p.Id
                                                 select new VirtualPaymentBill {
                                                     Id = p.Id,
                                                     BusinessCode =
                                                     b.Type == (int)DcsCompanyType.服务站 || b.Type == (int)DcsCompanyType.服务站兼代理库 ? dealerServiceInfo.BusinessCode :
                                                     b.Type == (int)DcsCompanyType.代理库 ? salesUnit.BusinessCode : null,
                                                     Code = p.Code,
                                                     AccountGroupName = accountGroup.Name,
                                                     CustomerCompanyName = b.Name,
                                                     CustomerCompanyCode = b.Code,
                                                     Amount = p.Amount,
                                                     PaymentMethod = p.PaymentMethod,
                                                     Summary = p.Summary,
                                                     BankAccountNumber = g.BankAccountNumber,
                                                     Operator = p.Operator,
                                                     Status = p.Status,
                                                     CreatorName = p.CreatorName,
                                                     CreateTime = p.CreateTime,
                                                     ModifierName = p.ModifierName,
                                                     ModifyTime = p.ModifyTime,
                                                     InvoiceDate = p.InvoiceDate,
                                                     Drawer = p.Drawer,
                                                     IssueDate = p.IssueDate,
                                                     DueDate = p.DueDate,
                                                     PayBank = p.PayBank,
                                                     MoneyOrderNum = p.MoneyOrderNum,
                                                     Message = paymentBill_Sync.Message
                                                 };
            if(!string.IsNullOrEmpty(customerCompanyName)) {
                customerAccountHisDetailsquery = customerAccountHisDetailsquery.Where(r => r.CustomerCompanyName.Contains(customerCompanyName));
            }

            if(!string.IsNullOrEmpty(customerCompanyCode)) {
                customerAccountHisDetailsquery = customerAccountHisDetailsquery.Where(r => r.CustomerCompanyCode.Contains(customerCompanyCode));
            }

            if(!string.IsNullOrEmpty(creatorName)) {
                customerAccountHisDetailsquery = customerAccountHisDetailsquery.Where(r => r.CreatorName.Contains(creatorName));
            }

            if(paymentMethod.HasValue) {
                customerAccountHisDetailsquery = customerAccountHisDetailsquery.Where(r => r.PaymentMethod == paymentMethod);
            }
            if(status.HasValue) {
                customerAccountHisDetailsquery = customerAccountHisDetailsquery.Where(r => r.Status == status);
            }

            if(beginCreateTime.HasValue) {
                customerAccountHisDetailsquery = customerAccountHisDetailsquery.Where(r => r.CreateTime >= beginCreateTime);
            }
            if(endCreateTime.HasValue) {
                customerAccountHisDetailsquery = customerAccountHisDetailsquery.Where(r => r.CreateTime <= endCreateTime);
            }
            if(!string.IsNullOrEmpty(businessCode)) {
                customerAccountHisDetailsquery = customerAccountHisDetailsquery.Where(r => r.BusinessCode.Contains(businessCode));
            }
            if(beginInvoiceDate.HasValue) {
                customerAccountHisDetailsquery = customerAccountHisDetailsquery.Where(r => r.InvoiceDate >= beginInvoiceDate);
            }
            if(endInvoiceDate.HasValue) {
                customerAccountHisDetailsquery = customerAccountHisDetailsquery.Where(r => r.InvoiceDate <= endInvoiceDate);
            }

            return customerAccountHisDetailsquery.Distinct().OrderBy(r => r.Id);
        }

        public PaymentBill GetPaymentBillWithDetailsById(int id) {
            var dbPaymentBill = ObjectContext.PaymentBills.Include("PaymentBeneficiaryLists").SingleOrDefault(e => e.Id == id);
            if(dbPaymentBill != null) {
                var dbCustomerCompany = ObjectContext.Companies.SingleOrDefault(e => e.Id == dbPaymentBill.CustomerCompanyId && e.Status == (int)DcsMasterDataStatus.有效);
                var dbBankAccount = ObjectContext.BankAccounts.SingleOrDefault(e => e.Id == dbPaymentBill.BankAccountId && e.Status == (int)DcsMasterDataStatus.有效);
            }
            return dbPaymentBill;
        }

    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:33
        //原分布类的函数全部转移到Ach                                                               

        public void InsertPaymentBill(PaymentBill paymentBill) {
            new PaymentBillAch(this).InsertPaymentBill(paymentBill);
        }

        public void UpdatePaymentBill(PaymentBill paymentBill) {
            new PaymentBillAch(this).UpdatePaymentBill(paymentBill);
        }

        public IQueryable<PaymentBill> GetPaymentBillWithDetails() {
            return new PaymentBillAch(this).GetPaymentBillWithDetails();
        }
        public IQueryable<VirtualPaymentBill> GetVirtualPaymentBillWithDetails(string customerCompanyName, string customerCompanyCode, DateTime? beginCreateTime, DateTime? endCreateTime, int? paymentMethod, int? status, string creatorName, string businessCode, DateTime? beginInvoiceDate, DateTime? endInvoiceDate) {
            return new PaymentBillAch(this).GetVirtualPaymentBillWithDetails(customerCompanyName, customerCompanyCode, beginCreateTime, endCreateTime, paymentMethod, status, creatorName, businessCode, beginInvoiceDate, endInvoiceDate);
        }

        public PaymentBill GetPaymentBillWithDetailsById(int id) {
            return new PaymentBillAch(this).GetPaymentBillWithDetailsById(id);
        }
    }
}

