﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PurchaseSettleInvoiceRelAch : DcsSerivceAchieveBase {
        public PurchaseSettleInvoiceRelAch(DcsDomainService domainService)
            : base(domainService) {
        }
        
        public void InsertPurchaseSettleInvoiceRel(PurchaseSettleInvoiceRel purchaseSettleInvoiceRel) {
            InsertToDatabase(purchaseSettleInvoiceRel);
            this.InsertPurchaseSettleInvoiceRelValidate(purchaseSettleInvoiceRel);
        }
        public void UpdatePurchaseSettleInvoiceRel(PurchaseSettleInvoiceRel purchaseSettleInvoiceRel) {
            UpdateToDatabase(purchaseSettleInvoiceRel);
            this.UpdatePurchaseSettleInvoiceRelValidate(purchaseSettleInvoiceRel);
        }
        internal void InsertPurchaseSettleInvoiceRelValidate(PurchaseSettleInvoiceRel purchaseSettleInvoiceRel) {
        }
        internal void UpdatePurchaseSettleInvoiceRelValidate(PurchaseSettleInvoiceRel purchaseSettleInvoiceRel) {
        }
        public void DeletePurchaseSettleInvoiceRel(PurchaseSettleInvoiceRel purchaseSettleInvoiceRel) {
            DeleteFromDatabase(purchaseSettleInvoiceRel);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:33
        //原分布类的函数全部转移到Ach                                                               
        
        public void InsertPurchaseSettleInvoiceRel(PurchaseSettleInvoiceRel purchaseSettleInvoiceRel) {
            new PurchaseSettleInvoiceRelAch(this).InsertPurchaseSettleInvoiceRel(purchaseSettleInvoiceRel);
        }

        public void UpdatePurchaseSettleInvoiceRel(PurchaseSettleInvoiceRel purchaseSettleInvoiceRel) {
            new PurchaseSettleInvoiceRelAch(this).UpdatePurchaseSettleInvoiceRel(purchaseSettleInvoiceRel);
        }

        public void DeletePurchaseSettleInvoiceRel(PurchaseSettleInvoiceRel purchaseSettleInvoiceRel) {
            new PurchaseSettleInvoiceRelAch(this).DeletePurchaseSettleInvoiceRel(purchaseSettleInvoiceRel);
        }
    }
}
