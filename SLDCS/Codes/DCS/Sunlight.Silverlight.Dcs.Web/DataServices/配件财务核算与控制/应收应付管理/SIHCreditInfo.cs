﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class SIHCreditInfoAch : DcsSerivceAchieveBase {
        internal void InsertSIHCreditInfoValidate(SIHCreditInfo sihCreditInfo) {
            var userInfo = Utils.GetCurrentUserInfo();
            var company = ObjectContext.Companies.Single(r => r.Id == userInfo.EnterpriseId);
            sihCreditInfo.Code = CodeGenerator.Generate("SIHCreditInfo", company.Code);
            sihCreditInfo.CreatorId = userInfo.Id;
            sihCreditInfo.CreatorName = userInfo.Name;
            sihCreditInfo.CreateTime = DateTime.Now;
        }

        internal void UpdateSIHCreditInfoValidate(SIHCreditInfo sihCreditInfo) {
            var userInfo = Utils.GetCurrentUserInfo();
            sihCreditInfo.ModifierId = userInfo.Id;
            sihCreditInfo.ModifierName = userInfo.Name;
            sihCreditInfo.ModifyTime = DateTime.Now;
        }

        public void InsertSIHCreditInfo(SIHCreditInfo sihCreditInfo) {
            InsertToDatabase(sihCreditInfo);
            var sihCreditInfoDetails = ChangeSet.GetAssociatedChanges(sihCreditInfo, v => v.SIHCreditInfoDetails);
            foreach(SIHCreditInfoDetail sihCreditInfoDetail in sihCreditInfoDetails) {
                InsertToDatabase(sihCreditInfoDetail);
            }
            this.InsertSIHCreditInfoValidate(sihCreditInfo);
        }

        public void UpdateSIHCreditInfo(SIHCreditInfo sihCreditInfo) {
            sihCreditInfo.SIHCreditInfoDetails.Clear();
            UpdateToDatabase(sihCreditInfo);
            var sihCreditInfoDetails = ChangeSet.GetAssociatedChanges(sihCreditInfo, v => v.SIHCreditInfoDetails);
            foreach(SIHCreditInfoDetail sihCreditInfoDetail in sihCreditInfoDetails) {
                //获取清单实体对象的操作类型
                switch(ChangeSet.GetChangeOperation(sihCreditInfoDetail)) {
                    //新增操作
                    case ChangeOperation.Insert:
                        InsertToDatabase(sihCreditInfoDetail);
                        break;
                    //更新操作
                    case ChangeOperation.Update:
                        UpdateToDatabase(sihCreditInfoDetail);
                        break;
                    //删除操作
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(sihCreditInfoDetail);
                        break;
                }
            }
            this.UpdateSIHCreditInfoValidate(sihCreditInfo);
        }

        public IQueryable<SIHCreditInfo> GetSIHCreditInfos(string code, int? status, DateTime? beginCreateTime,
            DateTime? endCreateTime, string companyCode, string companyName, string dealerCode, string dealerName) {
            var result = ObjectContext.SIHCreditInfoes.Include("SIHCreditInfoDetails").Where(o => o.Type == (int)DCSSIHCreditInfoType.配件中心);
            result = GetSIHCreditInfoScreen(code, status, beginCreateTime, endCreateTime, companyCode, companyName, dealerCode, dealerName, result);
            var userInfo = Utils.GetCurrentUserInfo();
            var marketDptPersonnelRelations = ObjectContext.MarketDptPersonnelRelations.Where(t => t.PersonnelId == userInfo.Id && t.Status == (int)DcsBaseDataStatus.有效).ToArray();
            if(marketDptPersonnelRelations.Count()>0) {
                result = result.Where(t => ObjectContext.SIHCreditInfoDetails.Any(y => y.SIHCreditInfoId == t.Id && ObjectContext.MarketDptPersonnelRelations.Any(r => r.PersonnelId == userInfo.Id && r.MarketDepartmentId == y.MarketingDepartmentId && r.Status == (int)DcsBaseDataStatus.有效)));
            }
            return result.OrderByDescending(v => v.CreateTime);
        }

        public IQueryable<SIHCreditInfo> GetSIHCreditInfoFs(string code, int? status, DateTime? beginCreateTime,
            DateTime? endCreateTime, string companyCode, string companyName, string dealerCode, string dealerName) {
            var user = Utils.GetCurrentUserInfo();
            var company = ObjectContext.Companies.Where(t=>t.Id==user.EnterpriseId).First();
            var result = ObjectContext.SIHCreditInfoes.Include("SIHCreditInfoDetails")
                .Where(o => o.Type == (int)DCSSIHCreditInfoType.分销中心);
            if(company.Type==(int)DcsCompanyType.分公司) {
                var marketDptPersonnelRelations = ObjectContext.MarketDptPersonnelRelations.Where(t => t.PersonnelId == user.Id && t.Status == (int)DcsBaseDataStatus.有效).ToArray();
                if(marketDptPersonnelRelations.Count() > 0) {
                    result = result.Where(t => ObjectContext.SIHCreditInfoDetails.Any(y => y.SIHCreditInfoId == t.Id && ObjectContext.MarketDptPersonnelRelations.Any(r => r.PersonnelId == user.Id && r.MarketDepartmentId == y.MarketingDepartmentId && r.Status == (int)DcsBaseDataStatus.有效)));
                }
            } else if(company.Type == (int)DcsCompanyType.代理库) {
                result = result.Where(t=>ObjectContext.SIHCreditInfoDetails.Any(r=>r.SIHCreditInfoId==t.Id && r.CompanyId==user.EnterpriseId));
            } else {
                result = result.Where(t => ObjectContext.SIHCreditInfoDetails.Any(r => r.SIHCreditInfoId == t.Id && r.DealerId == user.EnterpriseId));

            }
           
            result = GetSIHCreditInfoScreen(code, status, beginCreateTime, endCreateTime, companyCode, companyName, dealerCode, dealerName, result);
            return result.OrderByDescending(v => v.CreateTime);
        }

        private IQueryable<SIHCreditInfo> GetSIHCreditInfoScreen(string code, int? status, DateTime? beginCreateTime,
            DateTime? endCreateTime, string companyCode, string companyName, string dealerCode, string dealerName, IQueryable<SIHCreditInfo> result) {
            if(!string.IsNullOrEmpty(companyCode))
                result = result.Where(r => ObjectContext.SIHCreditInfoDetails.Any(t => r.Id == t.SIHCreditInfoId && t.CompanyCode.Contains(companyCode)));
            if(!string.IsNullOrEmpty(companyName))
                result = result.Where(r => ObjectContext.SIHCreditInfoDetails.Any(t => r.Id == t.SIHCreditInfoId && t.CompanyName.Contains(companyName)));
            if(!string.IsNullOrEmpty(dealerCode))
                result = result.Where(r => ObjectContext.SIHCreditInfoDetails.Any(t => r.Id == t.SIHCreditInfoId && t.DealerCode.Contains(dealerCode)));
            if(!string.IsNullOrEmpty(dealerName))
                result = result.Where(r => ObjectContext.SIHCreditInfoDetails.Any(t => r.Id == t.SIHCreditInfoId && t.DealerName.Contains(dealerName)));
            if(!string.IsNullOrEmpty(code))
                result = result.Where(r => r.Code.ToUpper().Contains(code.ToUpper()));
            if(status.HasValue)
                result = result.Where(r => r.Status == status);
            if(beginCreateTime.HasValue)
                result = result.Where(r => r.CreateTime >= beginCreateTime);
            if(endCreateTime.HasValue)
                result = result.Where(r => r.CreateTime <= endCreateTime);
            return result;
        }

        public SIHCreditInfo GetSIHCreditInfoById(int id) {
            var sihCreditInfo = ObjectContext.SIHCreditInfoes.Include("SIHCreditInfoDetails").FirstOrDefault(v => v.Id == id);
            var dealerIds = sihCreditInfo.SIHCreditInfoDetails.Select(o => o.DealerId).ToList();
            var customerAccounts = ObjectContext.CustomerAccounts.Where(o => dealerIds.Contains(o.CustomerCompanyId) && o.Status == (int)DcsMasterDataStatus.有效).ToList();
            foreach(var item in sihCreditInfo.SIHCreditInfoDetails) {
                var customerAccount = customerAccounts.FirstOrDefault(o => o.CustomerCompanyId == item.DealerId && o.AccountGroupId == item.AccountGroupId);
                item.SIHCredit = customerAccount == null ? 0m : (customerAccount.SIHCredit ?? 0m);
                item.SIHCreditZB = customerAccount == null ? 0m : (customerAccount.SIHCreditZB ?? 0m);
                item.ThisSIHCredit = (item.ASIHCredit ?? 0m);
            }
            return sihCreditInfo;
        }

        public IQueryable<VirtualSIHCreditInfoDetail> GetCompaniesAndDealers(string code, string name, int marketingDepartmentId) {
            var userInfo = Utils.GetCurrentUserInfo();
            var company = ObjectContext.Companies.Where(r => (r.Type == (int)DcsCompanyType.服务站 || r.Type == (int)DcsCompanyType.服务站兼代理库) && r.Status == (int)DcsMasterDataStatus.有效);
            if(!string.IsNullOrWhiteSpace(code))
                company = company.Where(o => o.Code.Contains(code));
            if(!string.IsNullOrWhiteSpace(name))
                company = company.Where(o => o.Name.Contains(name));
            var query = (from c in company
                         join dsi in ObjectContext.DealerServiceInfoes.Where(o => o.Status == (int)DcsMasterDataStatus.有效) on c.Id equals dsi.DealerId
                         join md in ObjectContext.MarketingDepartments on dsi.MarketingDepartmentId equals md.Id
                         join adr in ObjectContext.AgencyDealerRelations on c.Id equals adr.DealerId
                         join ag in ObjectContext.AccountGroups on adr.AgencyId equals ag.SalesCompanyId
                         join ca in ObjectContext.CustomerAccounts on new {
                             CustomerCompanyId = c.Id,
                             AccountGroupId = ag.Id
                         } equals new {
                             ca.CustomerCompanyId,
                             ca.AccountGroupId
                         }
                         where dsi.MarketingDepartmentId == marketingDepartmentId && c.Status == (int)DcsMasterDataStatus.有效 &&
                         adr.Status == (int)DcsMasterDataStatus.有效 && ca.Status == (int)DcsMasterDataStatus.有效
                         && ag.Status == (int)DcsMasterDataStatus.有效
                         select new VirtualSIHCreditInfoDetail {
                             Id = c.Id,
                             MarketingDepartmentId = md.Id,
                             MarketingDepartmentCode = md.Code,
                             MarketingDepartmentName = md.Name,
                             CompanyId = adr.AgencyId,
                             CompanyCode = adr.AgencyCode,
                             CompanyName = adr.AgencyName,
                             DealerId = c.Id,
                             DealerCode = c.Code,
                             DealerName = c.Name,
                             AccountGroupId = ag.Id,
                             AccountGroupCode = ag.Code,
                             AccountGroupName = ag.Name,
                             SIHCredit = ca.SIHCredit,
                             SIHCreditZB = ca.SIHCreditZB,
                             ValidationTime = ca.ValidationTime,
                             ExpireTime = ca.ExpireTime
                         }).OrderBy(o => o.DealerCode);
            return query;
        }
    }

    partial class DcsDomainService {
        public void InsertSIHCreditInfo(SIHCreditInfo sihCreditInfo) {
            new SIHCreditInfoAch(this).InsertSIHCreditInfo(sihCreditInfo);
        }

        public void UpdateSIHCreditInfo(SIHCreditInfo sihCreditInfo) {
            new SIHCreditInfoAch(this).UpdateSIHCreditInfo(sihCreditInfo);
        }

        public IQueryable<SIHCreditInfo> GetSIHCreditInfos(string code, int? status, DateTime? beginCreateTime,
            DateTime? endCreateTime, string companyCode, string companyName, string dealerCode, string dealerName) {
            return new SIHCreditInfoAch(this).GetSIHCreditInfos(code, status, beginCreateTime, endCreateTime, companyCode, companyName, dealerCode, dealerName);
        }

        public IQueryable<SIHCreditInfo> GetSIHCreditInfoFs(string code, int? status, DateTime? beginCreateTime,
            DateTime? endCreateTime, string companyCode, string companyName, string dealerCode, string dealerName) {
            return new SIHCreditInfoAch(this).GetSIHCreditInfoFs(code, status, beginCreateTime, endCreateTime, companyCode, companyName, dealerCode, dealerName);
        }

        public SIHCreditInfo GetSIHCreditInfoById(int id) {
            return new SIHCreditInfoAch(this).GetSIHCreditInfoById(id);
        }

        public IQueryable<VirtualSIHCreditInfoDetail> GetCompaniesAndDealers(string code, string name, int marketingDepartmentId) {
            return new SIHCreditInfoAch(this).GetCompaniesAndDealers(code, name, marketingDepartmentId);
        }
    }
}
