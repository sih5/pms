﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class SIHCreditInfoDetailAch : DcsSerivceAchieveBase {
        public SIHCreditInfoDetailAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public void InsertSIHCreditInfoDetail(SIHCreditInfoDetail sihCreditInfoDetail) {
        }

        public void UpdateSIHCreditInfoDetail(SIHCreditInfoDetail sihCreditInfoDetail) {
        }
    }

    partial class DcsDomainService {
        public void InsertSIHCreditInfoDetail(SIHCreditInfoDetail sihCreditInfoDetail) {
            new SIHCreditInfoDetailAch(this).InsertSIHCreditInfoDetail(sihCreditInfoDetail);
        }

        public void UpdateSIHCreditInfoDetail(SIHCreditInfoDetail sihCreditInfoDetail) {
            new SIHCreditInfoDetailAch(this).UpdateSIHCreditInfoDetail(sihCreditInfoDetail);
        }
    }
}
