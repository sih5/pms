﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class SupplierAccountAch : DcsSerivceAchieveBase {
        internal void InsertSupplierAccountValidate(SupplierAccount supplierAccount) {
            var dbSupplierAccount = ObjectContext.SupplierAccounts.Where(r => r.BuyerCompanyId == supplierAccount.BuyerCompanyId && r.PartsSalesCategoryId == supplierAccount.PartsSalesCategoryId && r.SupplierCompanyId == supplierAccount.SupplierCompanyId && r.Status != (int)DcsMasterDataStatus.作废).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbSupplierAccount != null)
                throw new ValidationException(ErrorStrings.SupplierAccount_Validation1);
            var userInfo = Utils.GetCurrentUserInfo();
            supplierAccount.CreatorId = userInfo.Id;
            supplierAccount.CreatorName = userInfo.Name;
            supplierAccount.CreateTime = DateTime.Now;
        }
        internal void UpdateSupplierAccountValidate(SupplierAccount supplierAccount) {
            var dbSupplierAccount = ObjectContext.SupplierAccounts.Where(r => r.BuyerCompanyId == supplierAccount.BuyerCompanyId && r.PartsSalesCategoryId == supplierAccount.PartsSalesCategoryId && r.SupplierCompanyId == supplierAccount.SupplierCompanyId && r.Status != (int)DcsMasterDataStatus.作废 && r.Id != supplierAccount.Id).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbSupplierAccount != null)
                throw new ValidationException(ErrorStrings.SupplierAccount_Validation1);
            var userInfo = Utils.GetCurrentUserInfo();
            supplierAccount.ModifierId = userInfo.Id;
            supplierAccount.ModifierName = userInfo.Name;
            supplierAccount.ModifyTime = DateTime.Now;
        }
        public void InsertSupplierAccount(SupplierAccount supplierAccount) {
            InsertToDatabase(supplierAccount);
            this.InsertSupplierAccountValidate(supplierAccount);
        }
        public void UpdateSupplierAccount(SupplierAccount supplierAccount) {
            UpdateToDatabase(supplierAccount);
            this.UpdateSupplierAccountValidate(supplierAccount);
        }
        public IQueryable<SupplierAccount> GetSupplierAccountsWithCompany() {
            var userInfo = Utils.GetCurrentUserInfo();
            var supplierAccounts = ObjectContext.SupplierAccounts.Where(v => v.BuyerCompanyId == userInfo.EnterpriseId);
            return supplierAccounts.Include("Company").Include("PartsSalesCategory").OrderBy(entity => entity.Id);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:33
        //原分布类的函数全部转移到Ach                                                               

        public void InsertSupplierAccount(SupplierAccount supplierAccount) {
            new SupplierAccountAch(this).InsertSupplierAccount(supplierAccount);
        }

        public void UpdateSupplierAccount(SupplierAccount supplierAccount) {
            new SupplierAccountAch(this).UpdateSupplierAccount(supplierAccount);
        }
                public IQueryable<SupplierAccount> GetSupplierAccountsWithCompany() {
            return new SupplierAccountAch(this).GetSupplierAccountsWithCompany();
        }
    }
}