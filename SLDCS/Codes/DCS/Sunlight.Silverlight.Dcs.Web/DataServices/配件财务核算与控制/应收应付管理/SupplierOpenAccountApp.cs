﻿using System;
using System.Data.Objects;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class SupplierOpenAccountAppAch : DcsSerivceAchieveBase {
        internal void InsertSupplierOpenAccountAppValidate(SupplierOpenAccountApp supplierOpenAccountApp) {
            var userInfo = Utils.GetCurrentUserInfo();
            supplierOpenAccountApp.CreatorId = userInfo.Id;
            supplierOpenAccountApp.CreatorName = userInfo.Name;
            supplierOpenAccountApp.CreateTime = DateTime.Now;
            var company = ObjectContext.Companies.Where(r => r.Id == supplierOpenAccountApp.SupplierCompanyId).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(string.IsNullOrWhiteSpace(supplierOpenAccountApp.Code) || supplierOpenAccountApp.Code == GlobalVar.ASSIGNED_BY_SERVER)
                supplierOpenAccountApp.Code = CodeGenerator.Generate("SupplierOpenAccountApp", company.Code);
        }
        internal void UpdateSupplierOpenAccountAppValidate(SupplierOpenAccountApp supplierOpenAccountApp) {
            var userInfo = Utils.GetCurrentUserInfo();
            supplierOpenAccountApp.ModifierId = userInfo.Id;
            supplierOpenAccountApp.ModifierName = userInfo.Name;
            supplierOpenAccountApp.ModifyTime = DateTime.Now;
        }
        public void InsertSupplierOpenAccountApp(SupplierOpenAccountApp supplierOpenAccountApp) {
            InsertToDatabase(supplierOpenAccountApp);
            this.InsertSupplierOpenAccountAppValidate(supplierOpenAccountApp);
        }
        public IQueryable<SupplierOpenAccountApp> GetSupplierOpenAccountAppWithDetails() {
            return ObjectContext.SupplierOpenAccountApps.Include("Company").Include("PartsSalesCategory").OrderBy(entity => entity.Id);
        }
        public IQueryable<SupplierOpenAccountApp> GetSupplierOpenAccountAppFilterCompanyId() {
            var userInfo = Utils.GetCurrentUserInfo();
            return ObjectContext.SupplierOpenAccountApps.Include("Company").Include("PartsSalesCategory").Where(r => r.BuyerCompanyId == userInfo.EnterpriseId).OrderBy(entity => entity.Id);
        }
        public void UpdateSupplierOpenAccountApp(SupplierOpenAccountApp supplierOpenAccountApp) {
            UpdateToDatabase(supplierOpenAccountApp);
            this.UpdateSupplierOpenAccountAppValidate(supplierOpenAccountApp);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:33
        //原分布类的函数全部转移到Ach                                                               

        public void InsertSupplierOpenAccountApp(SupplierOpenAccountApp supplierOpenAccountApp) {
            new SupplierOpenAccountAppAch(this).InsertSupplierOpenAccountApp(supplierOpenAccountApp);
        }

        public IQueryable<SupplierOpenAccountApp> GetSupplierOpenAccountAppWithDetails() {
            return new SupplierOpenAccountAppAch(this).GetSupplierOpenAccountAppWithDetails();
        }

        public IQueryable<SupplierOpenAccountApp> GetSupplierOpenAccountAppFilterCompanyId() {
            return new SupplierOpenAccountAppAch(this).GetSupplierOpenAccountAppFilterCompanyId();
        }

        public void UpdateSupplierOpenAccountApp(SupplierOpenAccountApp supplierOpenAccountApp) {
            new SupplierOpenAccountAppAch(this).UpdateSupplierOpenAccountApp(supplierOpenAccountApp);
        }
    }
}
