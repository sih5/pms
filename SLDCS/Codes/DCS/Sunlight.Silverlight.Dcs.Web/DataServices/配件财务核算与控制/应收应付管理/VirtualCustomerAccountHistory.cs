﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class VirtualCustomerAccountHistoryAch : DcsSerivceAchieveBase {
        public VirtualCustomerAccountHistoryAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public IQueryable<VirtualCustomerAccountHistory> GetVirtualCustomerAccountHistories(DateTime? theDate) {
            var theEndDate = theDate.HasValue ? ((DateTime)theDate).AddDays(1 - ((DateTime)theDate).Day).AddMonths(1).AddSeconds(-1) : DateTime.Now;

            var userInfo = Utils.GetCurrentUserInfo();

            var result = from a in theDate.HasValue ? this.ObjectContext.CustomerAccountHistories.Where(r => r.TheDate <= theEndDate && r.TheDate >= theDate) : this.ObjectContext.CustomerAccountHistories
                         join b in this.ObjectContext.AccountGroups on a.AccountGroupId equals b.Id
                         join c in this.ObjectContext.AgencyAffiBranches.Where(r => r.BranchId == userInfo.EnterpriseId) on b.SalesCompanyId equals c.AgencyId
                         join d in this.ObjectContext.Agencies on c.AgencyId equals d.Id
                         join e in this.ObjectContext.DealerServiceInfoes on a.CustomerCompanyId equals e.DealerId
                         join f in this.ObjectContext.ChannelCapabilities on e.ChannelCapabilityId equals f.Id
                         select new VirtualCustomerAccountHistory {
                             Id = a.Id,
                             Code = a.Code,
                             Name = a.Name,
                             Type = a.Type,
                             AccountGroupCode = a.AccountGroupCode,
                             AccountGroupName = a.AccountGroupName,
                             AccountBalance = a.AccountBalance,
                             ShippedProductValue = a.ShippedProductValue,
                             PendingAmount = a.PendingAmount,
                             CustomerCredenceAmount = a.CustomerCredenceAmount,
                             FlBalance = a.FlBalance,
                             WshBalance = a.WshBalance,
                             SdBalance = a.SdBalance,
                             YeBalance = a.YeBalance,
                             KyBalance = a.KyBalance,
                             TheDate = a.TheDate,
                             AccountGroupId = a.AccountGroupId,
                             AgencyCode = d.Code,
                             AgencyName = d.Name,
                             SalesCompanyId = b.SalesCompanyId,
                             ChannelCapabilityName = f.Name,
                             SIHCredit = a.SIHCredit,
                             TempCreditTotalFee = a.TempCreditTotalFee
                         };
            return result.OrderBy(r => r.Id);
        }

        public IEnumerable<VirtualCustomerAccountHistory> GetVirtualCustomerAccounts(string code, string name) {

            var userInfo = Utils.GetCurrentUserInfo();
            if(userInfo.Id == 0) {
                throw new Exception("用户已失效，请重新登录");
            }
            string SQL = @"SELECT rownum as id,Extent1.Id Accountid,
       Extent1.AccountBalance,
       Extent1.ShippedProductValue,
       Extent1.PendingAmount,
       Extent1.CustomerCredenceAmount,
       Extent1.Status,
       Extent2.Type,
       Extent2.Code,
       Extent2.Name,
       Extent3.Code AS AccountGroupCode,
       Extent3.Name AS AccountGroupName,
       Extent3.SalesCompanyId,
       a.Code as AgencyCode,
       a.Name as AgencyName,
       cc.Name as ChannelCapabilityName,
       CASE
         WHEN GroupBy1.A1 IS NULL THEN
          0
         ELSE
          GroupBy1.A1
       END AS WSHBalance,
       ((((Extent1.AccountBalance + (CASE
         WHEN Extent4.AccountBalanceAmount IS NULL THEN
          0
         ELSE
          Extent4.AccountBalanceAmount
       END)) + Extent1.CustomerCredenceAmount + (CASE
         WHEN Extent1.SIHCredit IS NULL THEN
          0
         ELSE
          Extent1.SIHCredit
       END) + (CASE
         WHEN Extent1.TempCreditTotalFee IS NULL THEN
          0
         ELSE
          Extent1.TempCreditTotalFee
       END)) -
       Extent1.ShippedProductValue) - Extent1.PendingAmount) - (CASE
         WHEN GroupBy1.A1 IS NULL THEN
          0
         ELSE
          GroupBy1.A1
       END) AS KYBalance,
       Extent1.AccountBalance - Extent1.ShippedProductValue AS YEBalance,
       Extent1.ShippedProductValue + Extent1.PendingAmount AS SDBalance,
       Extent4.AccountBalanceAmount as FLBalance, to_date(to_char(trunc(sysdate-1,'dd'),'yyyy-mm-dd')||' 23:59:59','yyyy-mm-dd hh24,mi,ss'),s_CustomerAccountHistory.Nextval,
       Extent1.Customercompanyid,Extent3.id,
       Extent1.SIHCredit,Extent1.TempCreditTotalFee 
  FROM CustomerAccount Extent1
 INNER JOIN Company Extent2 ON Extent1.CustomerCompanyId = Extent2.Id
 INNER JOIN AccountGroup Extent3 ON Extent1.AccountGroupId = Extent3.Id
 inner join AgencyAffiBranch aab on Extent3.SalesCompanyId = aab.AgencyId
 inner join Agency a on aab.AgencyId = a.Id 
 inner join DealerServiceInfo dsi on Extent1.Customercompanyid = dsi.DealerId
 inner join ChannelCapability cc on dsi.ChannelCapabilityId = cc.Id
  LEFT OUTER JOIN PartsRebateAccount Extent4 ON (Extent1.AccountGroupId = Extent4.AccountGroupId)
   AND (Extent1.CustomerCompanyId = Extent4.CustomerCompanyId)
  LEFT OUTER JOIN (SELECT Filter1.SalesCategoryId AS K1,
                          Filter1.SubmitCompanyId AS K2,
                          Extent7.AccountGroupId AS K3,
                          Sum((TO_NUMBER(CASE
                                           WHEN Filter1.OrderedQuantity -
                                                Filter1.ApproveQuantity IS NULL THEN
                                            0
                                           ELSE
                                            Filter1.OrderedQuantity -
                                            Filter1.ApproveQuantity
                                         END)) * Filter1.OrderPrice) AS A1
                     FROM (SELECT
                                  Extent5.SalesCategoryId,
                                  Extent5.SalesUnitId,
                                  Extent5.SubmitCompanyId,
                                  Extent6.OrderedQuantity,
                                  Extent6.ApproveQuantity,
                                  Extent6.OrderPrice
                             FROM PartsSalesOrder Extent5
                            INNER JOIN PartsSalesOrderDetail Extent6
                               ON Extent5.Id = Extent6.PartsSalesOrderId
                            WHERE Extent5.Status IN (2, 4)) Filter1
                    INNER JOIN SalesUnit Extent7
                       ON Filter1.SalesUnitId = Extent7.Id
                    WHERE Extent7.OwnerCompanyId in(select id from branch)
                    GROUP BY Filter1.SalesCategoryId,
                             Filter1.SubmitCompanyId,
                             Extent7.AccountGroupId) GroupBy1
    ON (Extent1.CustomerCompanyId = GroupBy1.K2)
   AND (Extent3.Id = GroupBy1.K3)
 WHERE ((Extent2.Status = 1) AND (Extent1.Status = 1)) and aab.BranchId = " + userInfo.EnterpriseId;
            if(!string.IsNullOrEmpty(code)) {
                SQL = SQL + " and LOWER(Extent2.code) like '%" + code.ToLower() + "%'";
            }
            if(!string.IsNullOrEmpty(name)) {
                SQL = SQL + " and LOWER(Extent2.name) like '%" + name.ToLower() + "%'";
            }
            var search = ObjectContext.ExecuteStoreQuery<VirtualCustomerAccountHistory>(SQL).ToList();
            return search;
        }
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:33
        //原分布类的函数全部转移到Ach                                                               

        public IQueryable<VirtualCustomerAccountHistory> GetVirtualCustomerAccountHistories(DateTime? theDate) {
            return new VirtualCustomerAccountHistoryAch(this).GetVirtualCustomerAccountHistories(theDate);
        }

        public IEnumerable<VirtualCustomerAccountHistory> GetVirtualCustomerAccounts(string code, string name) {
            return new VirtualCustomerAccountHistoryAch(this).GetVirtualCustomerAccounts(code, name);
        }

        public string ExportCustomerAccounts(int[] ids, string code, string name) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("服务站在中心库账面余额_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var CustomerAccounts = this.GetVirtualCustomerAccounts(null, null);

            if(ids != null && ids.Any()) {
                CustomerAccounts = CustomerAccounts.Where(r => ids.Contains(r.Id));
            }
            if(!string.IsNullOrEmpty(code)) {
                CustomerAccounts = CustomerAccounts.Where(r => r.Code.ToLower().Contains(code.ToLower()));
            }
            if(!string.IsNullOrEmpty(name)) {
                CustomerAccounts = CustomerAccounts.Where(r => r.Name.ToLower().Contains(name.ToLower()));
            }

            if(CustomerAccounts.Any()) {
                excelColumns.Add("客户企业编号");
                excelColumns.Add("客户企业名称");
                excelColumns.Add("中心库编号");
                excelColumns.Add("中心库名称");
                excelColumns.Add("客户属性");
                excelColumns.Add("账户组编号");
                excelColumns.Add("账户组");
                excelColumns.Add("账户金额");
                excelColumns.Add("发出商品金额");
                excelColumns.Add("审批待发金额");
                excelColumns.Add("客户信用总额");
                excelColumns.Add("返利台账");
                excelColumns.Add("未审核订单金额");
                excelColumns.Add("锁定金额");
                excelColumns.Add("账户余额");
                excelColumns.Add("可用金额");
                excelColumns.Add("临时信用额度");
                excelColumns.Add("SIH授信(服务商)");

                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == CustomerAccounts.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = CustomerAccounts.ToArray()[index - 1];
                        var values = new object[] {
                                detail.Code,
                                detail.Name,
                                detail.AgencyCode,
                                detail.AgencyName,
                                detail.ChannelCapabilityName,
                                detail.AccountGroupCode,
                                detail.AccountGroupName,
                                detail.AccountBalance,
                                detail.ShippedProductValue,
                                detail.PendingAmount,
                                detail.CustomerCredenceAmount,
                                detail.FlBalance,
                                detail.WshBalance,
                                detail.SdBalance,
                                detail.YeBalance,
                                detail.KyBalance,
                                detail.TempCreditTotalFee,
                                detail.SIHCredit
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
    }
}
