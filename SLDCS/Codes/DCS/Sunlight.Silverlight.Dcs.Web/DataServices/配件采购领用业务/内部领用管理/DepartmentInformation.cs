﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class DepartmentInformationAch : DcsSerivceAchieveBase {
        public DepartmentInformationAch(DcsDomainService domainService)
            : base(domainService) {
        }

        internal void InsertDepartmentInformationValidate(DepartmentInformation departmentInformation) {
            var dbdepartmentInformation = ObjectContext.DepartmentInformations.Where(r => r.BranchId == departmentInformation.BranchId && r.Code.ToLower() == departmentInformation.Code.ToLower() && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbdepartmentInformation != null)
                throw new ValidationException(string.Format(ErrorStrings.DepartmentInformation_Validation1, departmentInformation.Code));
            dbdepartmentInformation = ObjectContext.DepartmentInformations.Where(r => r.BranchId == departmentInformation.BranchId && r.Name.Trim() == departmentInformation.Name.Trim() && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbdepartmentInformation != null)
                throw new ValidationException(string.Format(ErrorStrings.DepartmentInformation_Validation3, dbdepartmentInformation.Name));
            var userInfo = Utils.GetCurrentUserInfo();
            departmentInformation.CreatorId = userInfo.Id;
            departmentInformation.CreatorName = userInfo.Name;
            departmentInformation.CreateTime = DateTime.Now;
            departmentInformation.Status = (int)DcsBaseDataStatus.有效;
        }
        internal void UpdateDepartmentInformationValidate(DepartmentInformation departmentInformation) {
            var dbdepartmentInformation = ObjectContext.DepartmentInformations.Where(r => r.Id != departmentInformation.Id && r.BranchId == departmentInformation.BranchId && r.Code.ToLower() == departmentInformation.Code.ToLower() && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbdepartmentInformation != null)
                throw new ValidationException(string.Format(ErrorStrings.DepartmentInformation_Validation1, dbdepartmentInformation.Code));
            dbdepartmentInformation = ObjectContext.DepartmentInformations.Where(r => r.Id != departmentInformation.Id && r.BranchId == departmentInformation.BranchId && r.Name.Trim() == departmentInformation.Name.Trim() && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbdepartmentInformation != null)
                throw new ValidationException(string.Format(ErrorStrings.DepartmentInformation_Validation3, dbdepartmentInformation.Name));
            dbdepartmentInformation = ObjectContext.DepartmentInformations.Where(r => r.Id == departmentInformation.Id && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbdepartmentInformation == null)
                throw new ValidationException(ErrorStrings.DepartmentInformation_Validation2);
            var userInfo = Utils.GetCurrentUserInfo();
            departmentInformation.ModifierId = userInfo.Id;
            departmentInformation.ModifierName = userInfo.Name;
            departmentInformation.ModifyTime = DateTime.Now;
        }
        public void InsertDepartmentInformation(DepartmentInformation departmentInformation) {
            InsertToDatabase(departmentInformation);
            this.InsertDepartmentInformationValidate(departmentInformation);
        }
        public void UpdateDepartmentInformation(DepartmentInformation departmentInformation) {
            UpdateToDatabase(departmentInformation);
            this.UpdateDepartmentInformationValidate(departmentInformation);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:33
        //原分布类的函数全部转移到Ach                                                               

        public void InsertDepartmentInformation(DepartmentInformation departmentInformation) {
            new DepartmentInformationAch(this).InsertDepartmentInformation(departmentInformation);
        }

        public void UpdateDepartmentInformation(DepartmentInformation departmentInformation) {
            new DepartmentInformationAch(this).UpdateDepartmentInformation(departmentInformation);
        }
    }
}
