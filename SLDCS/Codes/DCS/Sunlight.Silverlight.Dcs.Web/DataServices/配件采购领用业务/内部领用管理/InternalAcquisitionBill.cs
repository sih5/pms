﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class InternalAcquisitionBillAch : DcsSerivceAchieveBase {
        internal void InsertInternalAcquisitionBillValidate(InternalAcquisitionBill internalAcquisitionBill) {
            var branch = ObjectContext.Branches.SingleOrDefault(r => r.Id == internalAcquisitionBill.BranchId && r.Status != (int)DcsMasterDataStatus.作废);
            if(branch == null)
                throw new ValidationException(ErrorStrings.InternalAcquisitionBill_Validation4);
            if(internalAcquisitionBill.InternalAcquisitionDetails.Any(r => r.SalesPrice == null)) {
                throw new ValidationException("存在批发价为空的清单");
            }
            var userInfo = Utils.GetCurrentUserInfo();
            internalAcquisitionBill.CreatorId = userInfo.Id;
            internalAcquisitionBill.CreatorName = userInfo.Name;
            internalAcquisitionBill.CreateTime = DateTime.Now;
            if(string.IsNullOrWhiteSpace(internalAcquisitionBill.Code) || internalAcquisitionBill.Code == GlobalVar.ASSIGNED_BY_SERVER)
                internalAcquisitionBill.Code = CodeGenerator.Generate("InternalAcquisitionBill", branch.Code);
        }
        internal void UpdateInternalAcquisitionBillValidate(InternalAcquisitionBill internalAcquisitionBill) {
            var dbinternalAcquisitionBill = ObjectContext.InternalAcquisitionBills.Where(r => r.Id == internalAcquisitionBill.Id && r.Status == (int)DcsWorkflowOfSimpleApprovalStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbinternalAcquisitionBill == null)
                throw new ValidationException(ErrorStrings.InternalAcquisitionBill_Validation2);
            if(internalAcquisitionBill.InternalAcquisitionDetails.Any(r => r.SalesPrice == null)) {
                throw new ValidationException("存在批发价为空的清单");
            }
            var userInfo = Utils.GetCurrentUserInfo();
            internalAcquisitionBill.ModifierId = userInfo.Id;
            internalAcquisitionBill.ModifierName = userInfo.Name;
            internalAcquisitionBill.ModifyTime = DateTime.Now;
        }
        public void InsertInternalAcquisitionBill(InternalAcquisitionBill internalAcquisitionBill) {
            InsertToDatabase(internalAcquisitionBill);
            var internalAcquisitionDetails = ChangeSet.GetAssociatedChanges(internalAcquisitionBill, v => v.InternalAcquisitionDetails, ChangeOperation.Insert);
            foreach(InternalAcquisitionDetail internalAcquisitionDetail in internalAcquisitionDetails) {
                InsertToDatabase(internalAcquisitionDetail);
            }
            this.InsertInternalAcquisitionBillValidate(internalAcquisitionBill);
        }
        public void UpdateInternalAcquisitionBill(InternalAcquisitionBill internalAcquisitionBill) {
            internalAcquisitionBill.InternalAcquisitionDetails.Clear();
            UpdateToDatabase(internalAcquisitionBill);
            var internalAcquisitionDetails = ChangeSet.GetAssociatedChanges(internalAcquisitionBill, v => v.InternalAcquisitionDetails);
            foreach(InternalAcquisitionDetail internalAcquisitionDetail in internalAcquisitionDetails) {
                switch(ChangeSet.GetChangeOperation(internalAcquisitionDetail)) {
                    case ChangeOperation.Insert:
                        InsertToDatabase(internalAcquisitionDetail);
                        break;
                    case ChangeOperation.Update:
                        UpdateToDatabase(internalAcquisitionDetail);
                        break;
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(internalAcquisitionDetail);
                        break;
                }
            }
            this.UpdateInternalAcquisitionBillValidate(internalAcquisitionBill);
        }
        public IQueryable<InternalAcquisitionBill> GetInternalAcquisitionBillWithInternalAcquisitionDetails() {
            return ObjectContext.InternalAcquisitionBills.Include("PartsPurchaseOrderType").Include("InternalAcquisitionDetails").OrderBy(v => v.Id);
        }
        public IQueryable<InternalAcquisitionBill> GetInternalAcquisitionBillsForSort() {
            return ObjectContext.InternalAcquisitionBills.Include("PartsPurchaseOrderType").OrderByDescending(v => v.CreateTime);
        }

        public IQueryable<SparePartWithQuantity> 查询清单入库数量(int id) {
            var query = from a in ObjectContext.InternalAcquisitionDetails
                        join b in ObjectContext.PartsInboundPlans.Where(r => r.InboundType == (int)DcsPartsInboundType.内部领入) on a.InternalAcquisitionBillId equals b.SourceId
                        join c in ObjectContext.PartsInboundPlanDetails on new {
                            a.SparePartId,
                            PartsInboundPlanId = b.Id
                        }equals new {
                            c.SparePartId,
                            c.PartsInboundPlanId
                        }
                        where a.InternalAcquisitionBillId == id
                        select new SparePartWithQuantity {
                            SerialNumber = a.SerialNumber,
                            SparePartId = c.SparePartId,
                            SparePartCode = c.SparePartCode,
                            SparePartName = c.SparePartName,
                            Quantity = a.Quantity,
                            InspectedQuantity = c.InspectedQuantity ?? 0
                        };
            return query.OrderBy(r => r.SerialNumber);
        }

    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:33
        //原分布类的函数全部转移到Ach                                                               

        public void InsertInternalAcquisitionBill(InternalAcquisitionBill internalAcquisitionBill) {
            new InternalAcquisitionBillAch(this).InsertInternalAcquisitionBill(internalAcquisitionBill);
        }

        public void UpdateInternalAcquisitionBill(InternalAcquisitionBill internalAcquisitionBill) {
            new InternalAcquisitionBillAch(this).UpdateInternalAcquisitionBill(internalAcquisitionBill);
        }

        public IQueryable<InternalAcquisitionBill> GetInternalAcquisitionBillWithInternalAcquisitionDetails() {
            return new InternalAcquisitionBillAch(this).GetInternalAcquisitionBillWithInternalAcquisitionDetails();
        }

        public IQueryable<InternalAcquisitionBill> GetInternalAcquisitionBillsForSort() {
            return new InternalAcquisitionBillAch(this).GetInternalAcquisitionBillsForSort();
        }
        
        public IQueryable<SparePartWithQuantity> 查询清单入库数量(int id) {
            return new InternalAcquisitionBillAch(this).查询清单入库数量(id);
        }
    }
}
