﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class InternalAllocationBillAch : DcsSerivceAchieveBase {
        internal void InsertInternalAllocationBillValidate(InternalAllocationBill internalAllocationBill) {
            var branch = ObjectContext.Branches.FirstOrDefault(r => r.Id == internalAllocationBill.BranchId);
            if(branch == null)
                throw new ValidationException(ErrorStrings.InternalAllocationBill_Validation6);
            if(internalAllocationBill.InternalAllocationDetails.Any(r => r.SalesPrice == null)) {
                throw new ValidationException("存在批发价为空的清单");
            }
            var userInfo = Utils.GetCurrentUserInfo();
            internalAllocationBill.CreatorId = userInfo.Id;
            internalAllocationBill.CreatorName = userInfo.Name;
            internalAllocationBill.CreateTime = DateTime.Now;
            if(string.IsNullOrWhiteSpace(internalAllocationBill.Code) || internalAllocationBill.Code == GlobalVar.ASSIGNED_BY_SERVER)
                internalAllocationBill.Code = CodeGenerator.Generate("InternalAllocationBill", branch.Code);
        }
        internal void UpdateInternalAllocationBillValidate(InternalAllocationBill internalAllocationBill) {
            var dbinternalAcquisitionBill = ObjectContext.InternalAllocationBills.Where(r => r.Id == internalAllocationBill.Id && (r.Status == (int)DcsInternalAllocationBillStatus.新建 || r.Status == (int)DcsInternalAllocationBillStatus.已初审)).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbinternalAcquisitionBill == null)
                throw new ValidationException(ErrorStrings.InternalAllocationBill_Validation4);
            if(internalAllocationBill.InternalAllocationDetails.Any(r => r.SalesPrice == null)) {
                throw new ValidationException("存在批发价为空的清单");
            }
            var userInfo = Utils.GetCurrentUserInfo();
            internalAllocationBill.ModifierId = userInfo.Id;
            internalAllocationBill.ModifierName = userInfo.Name;
            internalAllocationBill.ModifyTime = DateTime.Now;
        }
        public void InsertInternalAllocationBill(InternalAllocationBill internalAllocationBill) {
            InsertToDatabase(internalAllocationBill);
            var internalAllocationDetails = ChangeSet.GetAssociatedChanges(internalAllocationBill, v => v.InternalAllocationDetails, ChangeOperation.Insert);
            foreach(InternalAllocationDetail internalAllocationDetail in internalAllocationDetails) {
                InsertToDatabase(internalAllocationDetail);
            }
            this.InsertInternalAllocationBillValidate(internalAllocationBill);
        }
        public void UpdateInternalAllocationBill(InternalAllocationBill internalAllocationBill) {
            internalAllocationBill.InternalAllocationDetails.Clear();
            UpdateToDatabase(internalAllocationBill);
            var internalAllocationDetails = ChangeSet.GetAssociatedChanges(internalAllocationBill, v => v.InternalAllocationDetails);
            foreach(InternalAllocationDetail internalAllocationDetail in internalAllocationDetails) {
                switch(ChangeSet.GetChangeOperation(internalAllocationDetail)) {
                    case ChangeOperation.Insert:
                        InsertToDatabase(internalAllocationDetail);
                        break;
                    case ChangeOperation.Update:
                        UpdateToDatabase(internalAllocationDetail);
                        break;
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(internalAllocationDetail);
                        break;
                }
            }
            this.UpdateInternalAllocationBillValidate(internalAllocationBill);
        }
        public InternalAllocationBill GetInternalAllocationBillWithInternalAllocationDetails(int Id) {
            return ObjectContext.InternalAllocationBills.Include("InternalAllocationDetails").SingleOrDefault(e => e.Id == Id);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:33
        //原分布类的函数全部转移到Ach                                                               

        public void InsertInternalAllocationBill(InternalAllocationBill internalAllocationBill) {
            new InternalAllocationBillAch(this).InsertInternalAllocationBill(internalAllocationBill);
        }

        public void UpdateInternalAllocationBill(InternalAllocationBill internalAllocationBill) {
            new InternalAllocationBillAch(this).UpdateInternalAllocationBill(internalAllocationBill);
        }

        public InternalAllocationBill GetInternalAllocationBillWithInternalAllocationDetails(int Id) {
            return new InternalAllocationBillAch(this).GetInternalAllocationBillWithInternalAllocationDetails(Id);
        }
    }
}
