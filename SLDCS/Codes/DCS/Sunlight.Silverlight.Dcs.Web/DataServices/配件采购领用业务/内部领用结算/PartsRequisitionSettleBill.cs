﻿using System;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsRequisitionSettleBillAch : DcsSerivceAchieveBase {
        internal void InsertPartsRequisitionSettleBillValidate(PartsRequisitionSettleBill partsRequisitionSettleBill) {
            var userInfo = Utils.GetCurrentUserInfo();
            partsRequisitionSettleBill.CreatorId = userInfo.Id;
            partsRequisitionSettleBill.CreatorName = userInfo.Name;
            partsRequisitionSettleBill.CreateTime = DateTime.Now;
            if(string.IsNullOrWhiteSpace(partsRequisitionSettleBill.Code) || partsRequisitionSettleBill.Code == GlobalVar.ASSIGNED_BY_SERVER)
                partsRequisitionSettleBill.Code = CodeGenerator.Generate("PartsRequisitionSettleBill", partsRequisitionSettleBill.BranchCode);
        }

        internal void UpdatePartsRequisitionSettleBillValidate(PartsRequisitionSettleBill partsRequisitionSettleBill) {
            var userInfo = Utils.GetCurrentUserInfo();
            partsRequisitionSettleBill.ModifierId = userInfo.Id;
            partsRequisitionSettleBill.ModifierName = userInfo.Name;
            partsRequisitionSettleBill.ModifyTime = DateTime.Now;
        }

        public void InsertPartsRequisitionSettleBill(PartsRequisitionSettleBill partsRequisitionSettleBill) {
            InsertToDatabase(partsRequisitionSettleBill);
            var partsRequisitionSettleDetails = ChangeSet.GetAssociatedChanges(partsRequisitionSettleBill, v => v.PartsRequisitionSettleDetails, ChangeOperation.Insert);
            foreach(PartsRequisitionSettleDetail partsRequisitionSettleDetail in partsRequisitionSettleDetails)
                InsertToDatabase(partsRequisitionSettleDetail);
            var partsRequisitionSettleRefs = ChangeSet.GetAssociatedChanges(partsRequisitionSettleBill, v => v.PartsRequisitionSettleRefs, ChangeOperation.Insert);
            foreach(PartsRequisitionSettleRef partsRequisitionSettleRef in partsRequisitionSettleRefs)
                InsertToDatabase(partsRequisitionSettleRef);
            this.InsertPartsRequisitionSettleBillValidate(partsRequisitionSettleBill);
        }

        public void UpdatePartsRequisitionSettleBill(PartsRequisitionSettleBill partsRequisitionSettleBill) {
            partsRequisitionSettleBill.PartsRequisitionSettleDetails.Clear();
            partsRequisitionSettleBill.PartsRequisitionSettleRefs.Clear();
            UpdateToDatabase(partsRequisitionSettleBill);
            var partsRequisitionSettleDetails = ChangeSet.GetAssociatedChanges(partsRequisitionSettleBill, v => v.PartsRequisitionSettleDetails);
            foreach(PartsRequisitionSettleDetail partsRequisitionSettleDetail in partsRequisitionSettleDetails) {
                switch(ChangeSet.GetChangeOperation(partsRequisitionSettleDetail)) {
                    case ChangeOperation.Insert:
                        InsertToDatabase(partsRequisitionSettleDetail);
                        break;
                    case ChangeOperation.Update:
                        UpdateToDatabase(partsRequisitionSettleDetail);
                        break;
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(partsRequisitionSettleDetail);
                        break;
                }
            }
            var partsRequisitionSettleRefs = ChangeSet.GetAssociatedChanges(partsRequisitionSettleBill, v => v.PartsRequisitionSettleRefs);
            foreach(PartsRequisitionSettleRef partsRequisitionSettleRef in partsRequisitionSettleRefs) {
                switch(ChangeSet.GetChangeOperation(partsRequisitionSettleRef)) {
                    case ChangeOperation.Insert:
                        InsertToDatabase(partsRequisitionSettleRef);
                        break;
                    case ChangeOperation.Update:
                        UpdateToDatabase(partsRequisitionSettleRef);
                        break;
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(partsRequisitionSettleRef);
                        break;
                }
            }
            this.UpdatePartsRequisitionSettleBillValidate(partsRequisitionSettleBill);
        }

        public PartsRequisitionSettleBill GetPartsRequisitionSettleBillsWithDetailsAndRefsById(int id) {
            var dbPartsRequisitionSettleBill = ObjectContext.PartsRequisitionSettleBills.SingleOrDefault(entity => entity.Id == id && entity.Status == (int)DcsPartsRequisitionSettleBillStatus.新建);
            if(dbPartsRequisitionSettleBill != null) {
                var dbDetails = ObjectContext.PartsRequisitionSettleDetails.Where(e => e.PartsRequisitionSettleBillId == id).ToArray();
                var dbRefs = ObjectContext.PartsRequisitionSettleRefs.Where(e => e.PartsRequisitionSettleBillId == id).ToArray();
            }
            return dbPartsRequisitionSettleBill;
        }


        public IQueryable<PartsRequisitionSettleBillExport> 查询配件领用结算单计划价合计(int settleType) {
            //找出库单
            var partsOutboundBillDetails = from a in this.ObjectContext.PartsRequisitionSettleRefs.Where(r => r.SourceType == (int)DcsPartsRequisitionSettleRefSourceType.配件出库单)
                                           from b in this.ObjectContext.PartsOutboundBillDetails
                                           where a.SourceId == b.PartsOutboundBillId
                                           select new {
                                               a.PartsRequisitionSettleBillId,
                                               tempAmont = b.CostPrice * b.OutboundAmount
                                           };
            //找入库单
            var partsInboundCheckBillDetails = from a in this.ObjectContext.PartsRequisitionSettleRefs.Where(r => r.SourceType == (int)DcsPartsRequisitionSettleRefSourceType.配件入库检验单)
                                               from b in this.ObjectContext.PartsInboundCheckBillDetails
                                               where a.SourceId == b.PartsInboundCheckBillId
                                               select new {
                                                   a.PartsRequisitionSettleBillId,
                                                   tempAmont = (-1) * b.CostPrice * b.InspectedQuantity
                                               };
            var tempStructQuery = from a in (partsOutboundBillDetails.Concat(partsInboundCheckBillDetails))
                                  group a by new {
                                      a.PartsRequisitionSettleBillId
                                  }
                                      into tempTable
                                  select new {
                                      settlementId = tempTable.Key.PartsRequisitionSettleBillId,
                                      totalAmount = (decimal?)tempTable.Sum(r => r.tempAmont)
                                  };
            var partsRequisitionSettleBillExports = from a in ObjectContext.PartsRequisitionSettleBills.Where(r => r.SettleType == settleType)
                                                    join c in tempStructQuery on a.Id equals c.settlementId into tempTable1
                                                    from t in tempTable1.DefaultIfEmpty()
                                                    select new PartsRequisitionSettleBillExport {
                                                        Id = a.Id,
                                                        PartsSalesCategoryId = a.PartsSalesCategoryId,
                                                        DepartmentId = a.DepartmentId,
                                                        Code = a.Code,//结算单编号
                                                        BranchName = a.BranchName, //营销分公司
                                                        BranchId = a.BranchId,
                                                        PartsSalesCategoryName = a.PartsSalesCategoryName, //品牌
                                                        DepartmentName = a.DepartmentName,//部门
                                                        SettleType = a.SettleType, //结算类型
                                                        TotalSettlementAmount = a.TotalSettlementAmount, //结算总金额
                                                        Status = a.Status,//状态
                                                        Remark = a.Remark, //备注
                                                        CreatorName = a.CreatorName, //创建人
                                                        CreateTime = a.CreateTime, //创建时间
                                                        ModifierName = a.ModifierName,  //修改人
                                                        ModifyTime = a.ModifyTime,//修改时间
                                                        ApproverName = a.ApproverName, //审批人
                                                        ApproveTime = a.ApproveTime,//审批时间
                                                        AbandonerName = a.AbandonerName, //作废人
                                                        AbandonTime = a.AbandonTime, //作废时间
                                                        //PlannedPriceTotalAmount = (settleType == (int)DcsRequisitionSettleType.领入 ? (t.totalAmount) * (-1) : (t.totalAmount)) ?? 0,
                                                        TotalTaxAmount = (a.PartsRequisitionSettleDetails.Sum(r => (Math.Round(((r.SettlementPrice * r.Quantity) / (decimal)1.16 * (decimal)0.16), 2)))),
                                                        InvoiceDate = a.InvoiceDate,
                                                        Type=a.Type
                                                    };
            return partsRequisitionSettleBillExports.OrderBy(r => r.Id);
        }

    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:33
        //原分布类的函数全部转移到Ach                                                               

        public void InsertPartsRequisitionSettleBill(PartsRequisitionSettleBill partsRequisitionSettleBill) {
            new PartsRequisitionSettleBillAch(this).InsertPartsRequisitionSettleBill(partsRequisitionSettleBill);
        }

        public void UpdatePartsRequisitionSettleBill(PartsRequisitionSettleBill partsRequisitionSettleBill) {
            new PartsRequisitionSettleBillAch(this).UpdatePartsRequisitionSettleBill(partsRequisitionSettleBill);
        }

        public PartsRequisitionSettleBill GetPartsRequisitionSettleBillsWithDetailsAndRefsById(int id) {
            return new PartsRequisitionSettleBillAch(this).GetPartsRequisitionSettleBillsWithDetailsAndRefsById(id);
        }


        public IQueryable<PartsRequisitionSettleBillExport> 查询配件领用结算单计划价合计(int settleType) {
            return new PartsRequisitionSettleBillAch(this).查询配件领用结算单计划价合计(settleType);
        }
    }
}
