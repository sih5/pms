﻿using System;
using System.Linq;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class IntelligentOrderMonthlyBaseAch : DcsSerivceAchieveBase {
        public IntelligentOrderMonthlyBaseAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public IQueryable<IntelligentOrderMonthlyBase> GetIntelligentOrderMonthlyBasesByFrquency(int? startFrquency,int? endFrquency) {
            IQueryable<IntelligentOrderMonthlyBase> frquency = ObjectContext.IntelligentOrderMonthlyBases;
            if(startFrquency.HasValue) {
                frquency = frquency.Where(r => r.TotalMonthlyFrequency >= startFrquency.Value);
            }
            if(endFrquency.HasValue) {
                frquency = frquency.Where(r => r.TotalMonthlyFrequency <= endFrquency.Value);
            }
            return frquency.OrderByDescending(e => e.Id);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:33
        //原分布类的函数全部转移到Ach                                                               

        public IQueryable<IntelligentOrderMonthlyBase> GetIntelligentOrderMonthlyBasesByFrquency(int? startFrquency,int? endFrquency) {
            return new IntelligentOrderMonthlyBaseAch(this).GetIntelligentOrderMonthlyBasesByFrquency(startFrquency,endFrquency);
        }
    }
}
