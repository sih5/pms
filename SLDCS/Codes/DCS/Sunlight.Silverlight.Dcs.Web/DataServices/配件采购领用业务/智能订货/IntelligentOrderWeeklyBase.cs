﻿using System;
using System.Linq;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class IntelligentOrderWeeklyBaseAch : DcsSerivceAchieveBase {
        public IntelligentOrderWeeklyBaseAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public IQueryable<IntelligentOrderWeeklyBase> GetIntelligentOrderWeeklyBaseByWeeks(int? yearD,int? startWeeklyd,int? endWeeklyd) {
            IQueryable<IntelligentOrderWeeklyBase> orderWeekly = ObjectContext.IntelligentOrderWeeklyBases;
            if(yearD.HasValue) {
                orderWeekly = orderWeekly.Where(r => r.YearD == yearD.Value);
            }
            if(startWeeklyd.HasValue){
               orderWeekly = orderWeekly.Where(r => r.WeeklyD >= startWeeklyd.Value);
            }
            if(endWeeklyd.HasValue){
                orderWeekly = orderWeekly.Where(r => r.WeeklyD <= endWeeklyd.Value);
            }
            return orderWeekly.OrderBy(e => e.Id);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:33
        //原分布类的函数全部转移到Ach                                                               

        public IQueryable<IntelligentOrderWeeklyBase> GetIntelligentOrderWeeklyBaseByWeeks(int? yearD,int? startWeeklyd,int? endWeeklyd) {
            return new IntelligentOrderWeeklyBaseAch(this).GetIntelligentOrderWeeklyBaseByWeeks(yearD,startWeeklyd,endWeeklyd);
        }
    }
}
