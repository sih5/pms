﻿using System;
using System.Linq;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartConPurchasePlanAch : DcsSerivceAchieveBase {
        public PartConPurchasePlanAch(DcsDomainService domainService)
            : base(domainService) {
        }

        internal void UpdatePartConPurchasePlanValidate(PartConPurchasePlan partConPurchasePlan) {
            var userInfo = Utils.GetCurrentUserInfo();
            partConPurchasePlan.ModifierId = userInfo.Id;
            partConPurchasePlan.ModifierName = userInfo.Name;
            partConPurchasePlan.ModifyTime = DateTime.Now;
        }
        public void UpdatePartsServiceLevel(PartConPurchasePlan partConPurchasePlan) {
            UpdateToDatabase(partConPurchasePlan);
            this.UpdatePartConPurchasePlanValidate(partConPurchasePlan);
        }
        public IQueryable<PartConPurchasePlan> GetPartConPurchasePlansByTime(DateTime? theDate) {
            if(theDate.HasValue) {
                //每个月18号凌晨0点，自动结转。
                theDate = new DateTime(theDate.Value.Year, theDate.Value.Month, 18);
                return ObjectContext.PartConPurchasePlans.Where(r => (!theDate.HasValue || r.DateT == theDate)).OrderBy(e => e.Id);
            }
            return ObjectContext.PartConPurchasePlans.OrderBy(e => e.Id);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:33
        //原分布类的函数全部转移到Ach                                                               

        public void UpdatePartsServiceLevel(PartConPurchasePlan partConPurchasePlan) {
            new PartConPurchasePlanAch(this).UpdatePartsServiceLevel(partConPurchasePlan);
        }

        public IQueryable<PartConPurchasePlan> GetPartConPurchasePlansByTime(DateTime? theDate) {
            return new PartConPurchasePlanAch(this).GetPartConPurchasePlansByTime(theDate);
        }
    }
}
