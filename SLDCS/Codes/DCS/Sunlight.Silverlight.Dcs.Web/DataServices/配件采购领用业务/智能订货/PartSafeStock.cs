﻿using System;
using System.Linq;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartSafeStockAch : DcsSerivceAchieveBase {
        public PartSafeStockAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public IQueryable<PartSafeStock> GetPartSafeStocksByQuantity(int? startQuantityFor12M,int? endQuantityFor12M) {
            IQueryable<PartSafeStock> quantity = ObjectContext.PartSafeStocks;
            if(startQuantityFor12M.HasValue){
                quantity = quantity.Where(r => r.OrderQuantityFor12M >= startQuantityFor12M.Value);
            }
            if(endQuantityFor12M.HasValue){
                quantity = quantity.Where(r => r.OrderQuantityFor12M <= endQuantityFor12M.Value);
            }
            return quantity.OrderBy(e => e.Id);
   
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:33
        //原分布类的函数全部转移到Ach                                                               

        public IQueryable<PartSafeStock> GetPartSafeStocksByQuantity(int? startQuantityFor12M,int? endQuantityFor12M) {
            return new PartSafeStockAch(this).GetPartSafeStocksByQuantity(startQuantityFor12M,endQuantityFor12M);
        }
    }
}
