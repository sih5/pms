﻿using System;
using System.Linq;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsServiceLevelAch : DcsSerivceAchieveBase {
        public PartsServiceLevelAch(DcsDomainService domainService)
            : base(domainService) {
        }

        internal void InsertPartsServiceLevelValidate(PartsServiceLevel partsServiceLevel) {
            var userInfo = Utils.GetCurrentUserInfo();
            partsServiceLevel.CreatorId = userInfo.Id;
            partsServiceLevel.CreatorName = userInfo.Name;
            partsServiceLevel.CreateTime = DateTime.Now;
        }
        internal void UpdatePartsServiceLevelValidate(PartsServiceLevel partsServiceLevel) {
            var userInfo = Utils.GetCurrentUserInfo();
            partsServiceLevel.ModifierId = userInfo.Id;
            partsServiceLevel.ModifierName = userInfo.Name;
            partsServiceLevel.ModifyTime = DateTime.Now;
        }
        public void InsertPartsServiceLevel(PartsServiceLevel partsServiceLevel) {
            InsertToDatabase(partsServiceLevel);
            this.InsertPartsServiceLevelValidate(partsServiceLevel);
        }
        public void UpdatePartsServiceLevel(PartsServiceLevel partsServiceLevel) {
            UpdateToDatabase(partsServiceLevel);
            this.UpdatePartsServiceLevelValidate(partsServiceLevel);
        }
        public PartsServiceLevel GetPartsServiceLevelById(int id) {
            return ObjectContext.PartsServiceLevels.SingleOrDefault(r => r.Id == id);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:33
        //原分布类的函数全部转移到Ach                                                               

        public void InsertPartsServiceLevel(PartsServiceLevel partsServiceLevel) {
            new PartsServiceLevelAch(this).InsertPartsServiceLevel(partsServiceLevel);
        }

        public void UpdatePartsServiceLevel(PartsServiceLevel partsServiceLevel) {
            new PartsServiceLevelAch(this).UpdatePartsServiceLevel(partsServiceLevel);
        }

        public PartsServiceLevel GetPartsServiceLevelById(int id) {
            return new PartsServiceLevelAch(this).GetPartsServiceLevelById(id);
        }
    }
}
