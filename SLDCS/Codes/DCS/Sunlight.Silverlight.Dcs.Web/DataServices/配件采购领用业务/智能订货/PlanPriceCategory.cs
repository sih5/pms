﻿using System;
using System.Linq;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PlanPriceCategoryAch : DcsSerivceAchieveBase {
        public PlanPriceCategoryAch(DcsDomainService domainService)
            : base(domainService) {
        }

        internal void InsertPlanPriceCategoryValidate(PlanPriceCategory planPriceCategory) {
            var userInfo = Utils.GetCurrentUserInfo();
            planPriceCategory.CreatorId = userInfo.Id;
            planPriceCategory.CreatorName = userInfo.Name;
            planPriceCategory.CreateTime = DateTime.Now;
        }
        internal void UpdatePlanPriceCategoryValidate(PlanPriceCategory planPriceCategory) {
            var userInfo = Utils.GetCurrentUserInfo();
            planPriceCategory.ModifierId = userInfo.Id;
            planPriceCategory.ModifierName = userInfo.Name;
            planPriceCategory.ModifyTime = DateTime.Now;
        }
        public void InsertPlanPriceCategory(PlanPriceCategory planPriceCategory) {
            InsertToDatabase(planPriceCategory);
            this.InsertPlanPriceCategoryValidate(planPriceCategory);
        }
        public void UpdatePlanPriceCategory(PlanPriceCategory planPriceCategory) {
            UpdateToDatabase(planPriceCategory);
            this.UpdatePlanPriceCategoryValidate(planPriceCategory);
        }

        public PlanPriceCategory GetPlanPriceCategoryById(int id) {
            return this.ObjectContext.PlanPriceCategories.SingleOrDefault(r => r.Id == id);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:33
        //原分布类的函数全部转移到Ach                                                               

        public void InsertPlanPriceCategory(PlanPriceCategory planPriceCategory) {
            new PlanPriceCategoryAch(this).InsertPlanPriceCategory(planPriceCategory);
        }

        public void UpdatePlanPriceCategory(PlanPriceCategory planPriceCategory) {
            new PlanPriceCategoryAch(this).UpdatePlanPriceCategory(planPriceCategory);
        }
        
        public PlanPriceCategory GetPlanPriceCategoryById(int id) {
            return new PlanPriceCategoryAch(this).GetPlanPriceCategoryById(id);
        }
    }
}
