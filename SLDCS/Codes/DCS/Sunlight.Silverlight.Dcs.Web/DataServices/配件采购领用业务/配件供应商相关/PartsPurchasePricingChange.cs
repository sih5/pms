﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Microsoft.VisualBasic.ApplicationServices;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsPurchasePricingChangeAch : DcsSerivceAchieveBase {
        internal void InsertPartsPurchasePricingChangeValidate(PartsPurchasePricingChange partsPurchasePricingChange) {
            var partsPurchasePricingDetails = partsPurchasePricingChange.PartsPurchasePricingDetails;
            var errorDetail = partsPurchasePricingDetails.GroupBy(v => new {
                v.PartId,
                v.SupplierId
            }).Where(v1 => v1.Count() > 1);
            if(errorDetail.Count() > 0)
                throw new ValidationException(ErrorStrings.PartsPurchasePricingChange_Validation4);
            var userInfo = Utils.GetCurrentUserInfo();
            if(userInfo.EnterpriseId==0) {
                throw new ValidationException("用户信息已失效，请重新登录");
            }           
            partsPurchasePricingChange.BranchId = userInfo.EnterpriseId;
            partsPurchasePricingChange.CreatorId = userInfo.Id;
            partsPurchasePricingChange.CreatorName = userInfo.Name;
            partsPurchasePricingChange.CreateTime = DateTime.Now;

            //判断Code是否需要生成编号
            if(string.IsNullOrWhiteSpace(partsPurchasePricingChange.Code) || partsPurchasePricingChange.Code == GlobalVar.ASSIGNED_BY_SERVER)
                partsPurchasePricingChange.Code = CodeGenerator.Generate("PartsPurchasePricingChange");

        }

        private void fullOthers(PartsPurchasePricingChange partsPurchasePricingChange) {
            //var partIds = partsPurchasePricingChange.PartsPurchasePricingDetails.Select(r => r.PartId);
            //var supplierIds = partsPurchasePricingChange.PartsPurchasePricingDetails.Select(r => r.SupplierId);
            //var partsSupplierRelations = ObjectContext.PartsSupplierRelations.Where(r => r.PartsSalesCategoryId == partsPurchasePricingChange.PartsSalesCategoryId && partIds.Contains(r.PartId) && supplierIds.Contains(r.SupplierId) && r.Status != (int)DcsBaseDataStatus.作废);
            //var allPartsPurchasePricings = ObjectContext.PartsPurchasePricings.Where(r => r.Status != (int)DcsBaseDataStatus.作废 && partIds.Contains(r.PartId) && r.PartsSalesCategoryId == partsPurchasePricingChange.PartsSalesCategoryId).Include("PartsSupplier");    

            //foreach (var detail in partsPurchasePricingChange.PartsPurchasePricingDetails)
            //{
            //    //其他供应商的价格
            //    var partsPurchasePricings = allPartsPurchasePricings.Where(r => detail.PartId == r.PartId && r.PartsSupplierId != detail.SupplierId && DateTime.Now >= r.ValidFrom && DateTime.Now <= r.ValidTo);
            //    if (partsPurchasePricings != null && partsPurchasePricings.Any())
            //    {
            //        var partsPurchasePricing = partsPurchasePricings.FirstOrDefault(r => r.PurchasePrice == partsPurchasePricings.Min(d => d.PurchasePrice));
            //        //如果本身的价格比最低价低，不赋值最低采购价，最低价供应商，最低价是否首选 三个字段
            //        if ((decimal)detail.Price >= partsPurchasePricing.PurchasePrice)
            //        {
            //            detail.MinPurchasePrice = partsPurchasePricing.PurchasePrice;
            //            detail.MinPriceSupplierId = partsPurchasePricing.PartsSupplierId;
            //            detail.MinPriceSupplier = partsPurchasePricing.PartsSupplier.Name;

            //            var partsSupplierRelation = partsSupplierRelations.Where(r => r.PartId == detail.PartId && r.SupplierId == detail.SupplierId).FirstOrDefault();
            //            if (partsSupplierRelation != null)
            //            {
            //                detail.IsPrimaryMinPrice = partsSupplierRelation.IsPrimary ?? false;
            //            }
            //        }
            //    }
            //    //如果没有，不赋值最低采购价，最低价供应商，最低价是否首选 三个字段
            //    else {
            //        detail.MinPurchasePrice = null;
            //        detail.MinPriceSupplierId = null;
            //        detail.MinPriceSupplier = null;
            //        detail.IsPrimaryMinPrice = null;
            //    }
            //}
        }

        internal void UpdatePartsPurchasePricingChangeValidate(PartsPurchasePricingChange partsPurchasePricingChange) {
            var partsPurchasePricingDetails = partsPurchasePricingChange.PartsPurchasePricingDetails;
            var errorDetail = partsPurchasePricingDetails.GroupBy(v => new {
                v.PartId,
                v.SupplierId
            }).Where(v1 => v1.Count() > 1);
            if(errorDetail.Count() > 0)
                throw new ValidationException(ErrorStrings.PartsPurchasePricingChange_Validation4);
            var userInfo = Utils.GetCurrentUserInfo();
            if(userInfo.EnterpriseId == 0) {
                throw new ValidationException("用户信息已失效，请重新登录");
            }
            partsPurchasePricingChange.ModifierId = userInfo.Id;
            partsPurchasePricingChange.ModifierName = userInfo.Name;
            partsPurchasePricingChange.ModifyTime = DateTime.Now;
        }

        public PartsPurchasePricingDetail fullOthersForPartsPurchasePricingDetail(int partsSalesCategoryId, int partId, int supplierId, decimal price) {
            var partsPurchasePricingDetail = new PartsPurchasePricingDetail();
            var partsSupplierRelations = ObjectContext.PartsSupplierRelations.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId && partId == r.PartId && r.Status != (int)DcsBaseDataStatus.作废).Include("PartsSupplier").ToArray();
            var allPartsPurchasePricing = ObjectContext.PartsPurchasePricings.Where(r =>  partId == r.PartId && r.PartsSalesCategoryId == partsSalesCategoryId).Include("PartsSupplier").ToArray();

            //其他供应商的价格
            var partsPurchasePricings = allPartsPurchasePricing.Where(r => r.PartsSupplierId != supplierId && DateTime.Now >= r.ValidFrom && DateTime.Now <= r.ValidTo);
            if(partsPurchasePricings != null && partsPurchasePricings.Any()) {
                var partsPurchasePricing = partsPurchasePricings.FirstOrDefault(r => r.PurchasePrice == partsPurchasePricings.Min(d => d.PurchasePrice));
                //如果本身的价格比最低价低，不赋值最低采购价，最低价供应商，最低价是否首选 三个字段
                if(price >= partsPurchasePricing.PurchasePrice) {
                    partsPurchasePricingDetail.MinPurchasePrice = partsPurchasePricing.PurchasePrice;
                    partsPurchasePricingDetail.MinPriceSupplierId = partsPurchasePricing.PartsSupplierId;
                    partsPurchasePricingDetail.MinPriceSupplier = partsPurchasePricing.PartsSupplier.Name;

                    var partsSupplierRelation = partsSupplierRelations.Where(r => r.SupplierId == partsPurchasePricingDetail.MinPriceSupplierId).FirstOrDefault();
                    if(partsSupplierRelation != null) {
                        partsPurchasePricingDetail.IsPrimaryMinPrice = partsSupplierRelation.IsPrimary;
                    }
                }
            }
                //如果没有，不赋值最低采购价，最低价供应商，最低价是否首选 三个字段
            else {
                partsPurchasePricingDetail.MinPurchasePrice = null;
                partsPurchasePricingDetail.MinPriceSupplierId = null;
                partsPurchasePricingDetail.MinPriceSupplier = null;
                partsPurchasePricingDetail.IsPrimaryMinPrice = null;
            }
            //首选供应商
            var primary = partsSupplierRelations.FirstOrDefault(r => r.IsPrimary == true);
            if (primary != null) {
                partsPurchasePricingDetail.OldPriSupplierId = primary.SupplierId;
                partsPurchasePricingDetail.OldPriSupplierCode = primary.PartsSupplier.Code;
                partsPurchasePricingDetail.OldPriSupplierName = primary.PartsSupplier.Name;
                var primaryPrice = allPartsPurchasePricing.FirstOrDefault(r => r.PartsSupplierId == primary.SupplierId && DateTime.Now >= r.ValidFrom && DateTime.Now <= r.ValidTo && r.Status != (int)DcsBaseDataStatus.作废);
                if (primaryPrice != null) {
                    partsPurchasePricingDetail.OldPriPrice = primaryPrice.PurchasePrice;
                    partsPurchasePricingDetail.OldPriPriceType = primaryPrice.PriceType;
                } else {
                    var oldprimaryPrice = allPartsPurchasePricing.Where(r => r.PartsSupplierId == primary.SupplierId &&  DateTime.Now >= r.ValidTo && r.Status == (int)DcsBaseDataStatus.作废).OrderByDescending(t=>t.ValidTo).FirstOrDefault();
                    if(oldprimaryPrice != null) {
                        partsPurchasePricingDetail.OldPriPrice = oldprimaryPrice.PurchasePrice;
                        partsPurchasePricingDetail.OldPriPriceType = oldprimaryPrice.PriceType;
                    }
                }
            }

            var partsPurchasePricings2 = allPartsPurchasePricing.Where(r => DateTime.Now >= r.ValidFrom && DateTime.Now <= r.ValidTo);
            if(!partsPurchasePricings2.Any()) {
                partsPurchasePricingDetail.IsNewPart = true;
            }

            return partsPurchasePricingDetail;
        }

        public void InsertPartsPurchasePricingChange(PartsPurchasePricingChange partsPurchasePricingChange) {
           
            //var partsPurchasePricingDetails = ChangeSet.GetAssociatedChanges(partsPurchasePricingChange, v => v.PartsPurchasePricingDetails, ChangeOperation.Insert);
            //foreach(PartsPurchasePricingDetail partsPurchasePricingDetail in partsPurchasePricingDetails) {
            //    partsPurchasePricingDetail.Price = Math.Round(partsPurchasePricingDetail.Price, 2, MidpointRounding.AwayFromZero);
            //    InsertToDatabase(partsPurchasePricingDetail);
            //}           
            InsertToDatabase(partsPurchasePricingChange);
            this.InsertPartsPurchasePricingChangeValidate(partsPurchasePricingChange);
            this.fullOthers(partsPurchasePricingChange);
        }

        public void UpdatePartsPurchasePricingChange(PartsPurchasePricingChange partsPurchasePricingChange) {
            var tempartsPurchasePricingDetails = partsPurchasePricingChange.PartsPurchasePricingDetails.ToArray();
            //判断是否存在相同的采购价格，生效时间，供应商，价格类型
            //var spIds = tempartsPurchasePricingDetails.Select(t => t.PartId).Distinct().ToArray();
            //var partsPurchasePricings = ObjectContext.PartsPurchasePricings.Where(t => spIds.Contains(t.PartId) && t.Status == (int)DcsPartsPurchasePricingStatus.生效).ToArray();
            //foreach(var item in tempartsPurchasePricingDetails) {
            //    if(partsPurchasePricings.Any(t => t.PartId == item.PartId && t.PartsSupplierId == item.SupplierId && t.PriceType == item.PriceType && t.PurchasePrice == item.Price && t.ValidFrom == t.ValidFrom && t.ValidTo == item.ValidTo)) {
            //        throw new Exception("配件" + item.PartCode + "已存在相同供应商，价格，价格类型，生效时间");
            //    }
            //}
            partsPurchasePricingChange.PartsPurchasePricingDetails.Clear();
            UpdateToDatabase(partsPurchasePricingChange);
            var partsPurchasePricingDetails = ChangeSet.GetAssociatedChanges(partsPurchasePricingChange, v => v.PartsPurchasePricingDetails);
           
            //清单有更改时重新查询最低采购价，最低价供应商
            bool flag = false;
            foreach(PartsPurchasePricingDetail partsPurchasePricingDetail in partsPurchasePricingDetails) {
                partsPurchasePricingDetail.Price = Math.Round(partsPurchasePricingDetail.Price, 2, MidpointRounding.AwayFromZero);
                switch(ChangeSet.GetChangeOperation(partsPurchasePricingDetail)) {
                    case ChangeOperation.Insert:
                        InsertToDatabase(partsPurchasePricingDetail);
                        flag = true;
                        break;
                    case ChangeOperation.Update:
                        UpdateToDatabase(partsPurchasePricingDetail);
                        flag = true;
                        break;
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(partsPurchasePricingDetail);
                        flag = true;
                        break;
                }
            }
            this.UpdatePartsPurchasePricingChangeValidate(partsPurchasePricingChange);
            if(flag)
                this.fullOthers(partsPurchasePricingChange);
        }

        public IQueryable<PartsPurchasePricingChange> GetPartsPurchasePricingChangesWithBranch(string sparePartCode, string sparePartName) {
            var userInfo = Utils.GetCurrentUserInfo();
            //var partsPurchasePricingChanges=ObjectContext.PartsPurchasePricingChanges.Where(r => r.BranchId == userInfo.EnterpriseId);
            //var result = partsPurchasePricingChanges.OrderByDescending(e => e.CreateTime);
            //IQueryable<PartsPurchasePricingDetail> details = ObjectContext.PartsPurchasePricingDetails;
            //if(!string.IsNullOrEmpty(sparePartCode))
            //    details = details.Where(r => r.PartCode.Contains(sparePartCode));
            //if(!string.IsNullOrEmpty(sparePartName))
            //    details = details.Where(r => r.PartName.Contains(sparePartName));

            //var result2 = from a in ObjectContext.PartsPurchasePricingChanges.Where(r => r.BranchId == userInfo.EnterpriseId)
            //              join b in details on a.Id equals b.ParentId
            //              select a;
            //foreach(var item in result2) {
            //    item.IsUplodFile = item.Path != null && item.Path.Length > 0;
            //}
            var result2 = ObjectContext.PartsPurchasePricingChanges.Where(r => r.BranchId == userInfo.EnterpriseId);
            if(!string.IsNullOrEmpty(sparePartCode))
                result2 = result2.Where(t => ObjectContext.PartsPurchasePricingDetails.Any(y => y.ParentId == t.Id && y.PartCode.Contains(sparePartCode)));
            if(!string.IsNullOrEmpty(sparePartName))
                result2 = result2.Where(t => ObjectContext.PartsPurchasePricingDetails.Any(y => y.ParentId == t.Id && y.PartName.Contains(sparePartName)));            
            return result2.Include("Branch").Include("PartsSalesCategory").OrderBy(t=>t.Id);
        }

        public IQueryable<PartsPurchasePricingChange> GetPartsPurchasePricingChangeWithDetails() {
            return ObjectContext.PartsPurchasePricingChanges.Include("PartsPurchasePricingDetails").Include("PartsPurchasePricingDetails.SparePart").OrderBy(e => e.Id);
        }

        public PartsPurchasePricingChange GetPartsPurchasePricingChangeWithOthers(int id) {
            var partsPurchasePricingChanges = ObjectContext.PartsPurchasePricingChanges.Where(r => r.Id == id).Include("PartsPurchasePricingDetails").Include("PartsPurchasePricingDetails.SparePart").FirstOrDefault();
            //var partIds = partsPurchasePricingChanges.PartsPurchasePricingDetails.Select(r => r.PartId);

            //var partsPurchasePricings = (from a in ObjectContext.PartsPurchasePricings
            //                            where partIds.Contains(a.PartId)
            //                            select new
            //                            {
            //                                ValidFrom = a.ValidFrom,
            //                                ValidTo = a.ValidTo,
            //                                PartId = a.PartId
            //                            }).ToArray();

            //foreach (var detail in partsPurchasePricingChanges.PartsPurchasePricingDetails) {
            //    var partsPurchasePrice = partsPurchasePricings.Where(r => r.PartId == detail.PartId && ((detail.ValidTo >= r.ValidFrom && detail.ValidTo <= r.ValidTo) || (detail.ValidFrom >= r.ValidFrom && detail.ValidFrom <= r.ValidTo)));
            //    if (partsPurchasePrice.Any())
            //    {
            //        detail.IsNewPart = false;
            //    }
            //    else {
            //        detail.IsNewPart = true;
            //    }
            //}
            return partsPurchasePricingChanges;
        }
        public IQueryable<PartsPurchasePricing> GetPartsPurchasePricingWithSpIdss(int[] spIds) {
            return ObjectContext.PartsPurchasePricings.Where(t => spIds.Contains(t.PartId)).OrderBy(e => e.Id);
        }
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:33
        //原分布类的函数全部转移到Ach                                                               

        public void InsertPartsPurchasePricingChange(PartsPurchasePricingChange partsPurchasePricingChange) {
            new PartsPurchasePricingChangeAch(this).InsertPartsPurchasePricingChange(partsPurchasePricingChange);
        }

        public void UpdatePartsPurchasePricingChange(PartsPurchasePricingChange partsPurchasePricingChange) {
            new PartsPurchasePricingChangeAch(this).UpdatePartsPurchasePricingChange(partsPurchasePricingChange);
        }

        public IQueryable<PartsPurchasePricingChange> GetPartsPurchasePricingChangesWithBranch(string sparePartCode, string sparePartName) {
            return new PartsPurchasePricingChangeAch(this).GetPartsPurchasePricingChangesWithBranch(sparePartCode, sparePartName);
        }

        public IQueryable<PartsPurchasePricingChange> GetPartsPurchasePricingChangeWithDetails() {
            return new PartsPurchasePricingChangeAch(this).GetPartsPurchasePricingChangeWithDetails();
        }

        public PartsPurchasePricingChange GetPartsPurchasePricingChangeWithOthers(int id) {
            return new PartsPurchasePricingChangeAch(this).GetPartsPurchasePricingChangeWithOthers(id);
        }

        public PartsPurchasePricingDetail fullOthersForPartsPurchasePricingDetail(int partsSalesCategoryId, int partId, int supplierId, decimal price) {
            return new PartsPurchasePricingChangeAch(this).fullOthersForPartsPurchasePricingDetail(partsSalesCategoryId, partId, supplierId, price);
        }
        public IQueryable<PartsPurchasePricing> GetPartsPurchasePricingWithSpIdss(int[] spIds) {
            return new PartsPurchasePricingChangeAch(this).GetPartsPurchasePricingWithSpIdss(spIds);
        }
    }
}
