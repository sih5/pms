﻿using System;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Devart.Common;
using Microsoft.Data.Extensions;
using Utils = Sunlight.Silverlight.Web.Utils;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsPurchasePricingDetailAch : DcsSerivceAchieveBase {
        public PartsPurchasePricingDetailAch(DcsDomainService domainService)
            : base(domainService) {
        }

        [Query(HasSideEffects = true)]
        public IQueryable<PartsPurchasePricingDetail> 查询配件采购价格按天(DateTime time, int? partsSupplierId, int[] partIds, int partsSalesCategoryId) {
            var result =DomainService. GetPurchasePricingsByDate(time, partsSupplierId, partIds, partsSalesCategoryId);
            return result.Include("PartsPurchasePricingChange").OrderBy(r => r.Id);
        }

        public IQueryable<PartsPurchasePricingDetail> 查询配件采购价格变更履历() {
            IQueryable<PartsPurchasePricingDetail> result = ObjectContext.PartsPurchasePricingDetails.Where(r=>r.PartsPurchasePricingChange.Status == (int)DcsPartsPurchasePricingChangeStatus.生效);
            //if(validFrom.HasValue)
            //    result = result.Where(r => r.ValidFrom >= validFrom);
            //if(validTo.HasValue)
            //    result = result.Where(r => r.ValidTo <= validTo);
            return result.Include("PartsPurchasePricingChange").Include("SparePart").OrderBy(r => r.Id);
        }
        public IQueryable<PartsPurchasePricingDetail> 查询配件采购价格变更履历1() {
            var userinfo=Utils.GetCurrentUserInfo();
            var partsPurchasePricingChangIds = ObjectContext.PartsPurchasePricingChanges.Where(r => r.BranchId == userinfo.EnterpriseId).Select(r=>r.Id);
            IQueryable<PartsPurchasePricingDetail> result = ObjectContext.PartsPurchasePricingDetails.Where(r => partsPurchasePricingChangIds.Contains(r.ParentId)&&r.PartsPurchasePricingChange.Status == (int)DcsPartsPurchasePricingChangeStatus.生效);
            //if(validFrom.HasValue)
            //    result = result.Where(r => r.ValidFrom >= validFrom);
            //if(validTo.HasValue)
            //    result = result.Where(r => r.ValidTo <= validTo);
            return result.Include("PartsPurchasePricingChange").Include("SparePart").OrderBy(r => r.Id);
        }

        public IQueryable<PartsPurchasePricingDetail> GetPartsPurchasePricingDetailsWithPartsPurchasePricingChange() {
            return ObjectContext.PartsPurchasePricingDetails.Include("PartsPurchasePricingChange").OrderBy(v => v.Id);
        }
        [Query(HasSideEffects = true)]
        public IQueryable<PartsPurchasePricingDetail> GetPurchasePricingsByDate(DateTime time, int? partsSupplierId, int[] partIds, int partsSalesCategoryId) {
            IQueryable<PartsPurchasePricingDetail> partsPurchasePricingDetails = ObjectContext.PartsPurchasePricingDetails;
            if(partsSupplierId.HasValue)
                partsPurchasePricingDetails = partsPurchasePricingDetails.Where(r => r.SupplierId == partsSupplierId.Value);
            if(partIds != null && partIds.Any())
                partsPurchasePricingDetails = partsPurchasePricingDetails.Where(r => partIds.Contains(r.PartId));
            var filters = from detail in partsPurchasePricingDetails
                          from change in ObjectContext.PartsPurchasePricingChanges.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId)
                          where change.Id == detail.ParentId && change.Status == (int)DcsPartsPurchasePricingChangeStatus.生效 && change.ApproveTime <= time
                          && detail.ValidFrom <= time && detail.ValidTo >= time
                          select new {
                              detail.PartId,
                              detail.SupplierId,
                              change.ApproveTime
                          }into tmp
                          group tmp by new {
                              tmp.PartId,
                              tmp.SupplierId
                          }into groupTmp
                          select new {
                              groupTmp.Key.PartId,
                              groupTmp.Key.SupplierId,
                              ApproveTime = groupTmp.Max(v => v.ApproveTime)
                          };
            var result = ObjectContext.PartsPurchasePricingDetails.Where(v => ObjectContext.PartsPurchasePricingChanges.Any(change => change.Id == v.ParentId && filters.Any(f => f.ApproveTime == change.ApproveTime && f.PartId == v.PartId && f.SupplierId == v.SupplierId)));
            return result.OrderBy(r => r.Id);
        }
        public IQueryable<PartsPurchasePricingDetail> GetPartsPurchasePricingDetailsWithSparePart() {
            var result =ObjectContext.PartsPurchasePricingDetails.OrderBy(e => e.Id).Include("SparePart");
            //foreach (var res in result) {
            //    if (res.IsPrimary == null) res.IsPrimary = false;
            //}
            return result.OrderBy(r => r.Id);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:33
        //原分布类的函数全部转移到Ach                                                               

        [Query(HasSideEffects = true)]
        public IQueryable<PartsPurchasePricingDetail> 查询配件采购价格按天(DateTime time, int? partsSupplierId, int[] partIds, int partsSalesCategoryId) {
            return new PartsPurchasePricingDetailAch(this).查询配件采购价格按天(time,partsSupplierId,partIds,partsSalesCategoryId);
        }

        public IQueryable<PartsPurchasePricingDetail> 查询配件采购价格变更履历() {
            return new PartsPurchasePricingDetailAch(this).查询配件采购价格变更履历();
        }

        public IQueryable<PartsPurchasePricingDetail> GetPartsPurchasePricingDetailsWithPartsPurchasePricingChange() {
            return new PartsPurchasePricingDetailAch(this).GetPartsPurchasePricingDetailsWithPartsPurchasePricingChange();
        }

        public IQueryable<PartsPurchasePricingDetail> 查询配件采购价格变更履历1() {
            return new PartsPurchasePricingDetailAch(this).查询配件采购价格变更履历1();
        }

        [Query(HasSideEffects = true)]
        public IQueryable<PartsPurchasePricingDetail> GetPurchasePricingsByDate(DateTime time, int? partsSupplierId, int[] partIds, int partsSalesCategoryId) {
            return new PartsPurchasePricingDetailAch(this).GetPurchasePricingsByDate(time,partsSupplierId,partIds,partsSalesCategoryId);
        }
                public IQueryable<PartsPurchasePricingDetail> GetPartsPurchasePricingDetailsWithSparePart() {
            return new PartsPurchasePricingDetailAch(this).GetPartsPurchasePricingDetailsWithSparePart();
        }
    }
}
