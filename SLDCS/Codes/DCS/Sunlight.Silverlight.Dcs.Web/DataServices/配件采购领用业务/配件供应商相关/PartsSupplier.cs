﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Entities;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;using System.Collections.Generic;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsSupplierAch : DcsSerivceAchieveBase {
        internal void InsertPartsSupplierValidate(PartsSupplier partsSupplier) {
            var dbpartsSupplier = ObjectContext.PartsSuppliers.Where(r => r.Id == partsSupplier.Id).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbpartsSupplier != null)
                throw new ValidationException(string.Format(ErrorStrings.PartsSupplier_Validation5, partsSupplier.Id));
            var userInfo = Utils.GetCurrentUserInfo();
            partsSupplier.CreatorId = userInfo.Id;
            partsSupplier.CreatorName = userInfo.Name;
            partsSupplier.CreateTime = DateTime.Now;
        }
        internal void UpdatePartsSupplierValidate(PartsSupplier partsSupplier) {
            var userInfo = Utils.GetCurrentUserInfo();
            partsSupplier.ModifierId = userInfo.Id;
            partsSupplier.ModifierName = userInfo.Name;
            partsSupplier.ModifyTime = DateTime.Now;
        }
        public void UpdatePartsSupplier(PartsSupplier partsSupplier) {
            UpdateToDatabase(partsSupplier);
            this.UpdatePartsSupplierValidate(partsSupplier);
        }
        public void InsertPartsSupplier(PartsSupplier partsSupplier) {
            InsertToDatabase(partsSupplier);
            this.InsertPartsSupplierValidate(partsSupplier);
        }
        public IQueryable<PartsSupplier> GetPartsSuppliersWithCompany() {
            return ObjectContext.PartsSuppliers.Include("Company").OrderBy(entity => entity.Id);
        }
        /// <summary>
        /// 配件与供应商关系新增时查询供应商
        /// </summary>
        /// <param name="businessCode">供应商业务编码</param>
        /// <returns></returns>
        public IQueryable<VirtualPartsSupplier> GetPartsSuppliersWithCompanyAndBusinessCode(int partsSalesCategoryId) {
            var branch = from sup in ObjectContext.PartsSuppliers.Where(r => r.Status == (int)DcsBaseDataStatus.有效)
                         join com in ObjectContext.Companies.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on sup.Id equals com.Id
                         join rel in ObjectContext.BranchSupplierRelations.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效) on sup.Id equals rel.SupplierId
                         select new VirtualPartsSupplier {
                             Id = sup.Id,
                             BusinessCode = rel.BusinessCode,
                             BusinessName = rel.BusinessName,
                             CityName = com.CityName,
                             BusinessScope = com.BusinessScope,
                             Code = sup.Code,
                             ContactMail = com.ContactMail,
                             ContactPerson = com.ContactPerson,
                             ContactPhone = com.ContactPhone,
                             CountyName = com.CountyName,
                             Fax = com.Fax,
                             LegalRepresentative = com.LegalRepresentative,
                             Name = sup.Name,
                             ProvinceName = com.ProvinceName,
                             Remark = sup.Remark,
                             ShortName = sup.ShortName,
                             Status = sup.Status,
                             SupplierCode = com.SupplierCode,
                             SupplierType = sup.SupplierType,
                             PartsSalesCategoryId = rel.PartsSalesCategoryId
                         };
            return branch.OrderBy(entity => entity.Id);
        }
        public IQueryable<VirtualPartsSupplier> GetPartsSuppliersWithVirtualPartsSupplier() {
            var resultQuery = from p in ObjectContext.PartsSuppliers.Where(r => r.Status == (int)DcsBaseDataStatus.有效)
                              join c in ObjectContext.Companies on p.Id equals c.Id
                              join b in ObjectContext.BranchSupplierRelations.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on p.Id equals b.SupplierId
                              select new VirtualPartsSupplier {
                                  Id = p.Id,
                                  Code = b.BusinessCode,
                                  Name = b.BusinessName,
                                  ShortName = p.ShortName,
                                  ProvinceName = c.ProvinceName,
                                  CityName = c.CityName,
                                  CountyName = c.CountyName,
                                  ContactPerson = c.ContactPerson,
                                  ContactPhone = c.ContactPhone,
                                  Fax = c.Fax,
                                  ContactMail = c.ContactMail,
                                  LegalRepresentative = c.LegalRepresentative,
                                  BusinessScope = c.BusinessScope,
                                  Status = p.Status,
                                  Remark = p.Remark,
                                  SupplierType = p.SupplierType
                              };
            return resultQuery.OrderBy(r => r.Id);
        }
        [Query(HasSideEffects = true)]
        public IQueryable<PartsSupplier> GetPartsSuppliersWithCompanyByIds(int[] ids) {
            return ObjectContext.PartsSuppliers.Where(entity => ids.Contains(entity.Id)).Include("Company").OrderBy(entity => entity.Id);
        }
        public IQueryable<PartsSupplier> GetPartsSuppliersWithCompanyByPartId(int partId, string overseasPartsFigure) {
            int[] ids;
            if(!string.IsNullOrWhiteSpace(overseasPartsFigure))
                ids = ObjectContext.PartsSupplierRelations.Where(r => ObjectContext.SpareParts.Any(p => p.OverseasPartsFigure == overseasPartsFigure && r.PartId == p.Id) && r.Status != (int)DcsBaseDataStatus.作废).Select(m => m.SupplierId).ToArray();
            else
                ids = ObjectContext.PartsSupplierRelations.Where(r => r.PartId == partId).Select(v => v.SupplierId).ToArray();
            if(!ids.Any())
                return null;
            return ObjectContext.PartsSuppliers.Where(v => ids.Contains(v.Id)).Include("Company").OrderBy(entity => entity.Id);
        }
        public IQueryable<PartsSupplier> GetPartsSuppliersWithCompanyByPartIdWithPartsSalesCategory(int partId, string overseasPartsFigure, int partsSalesCategoryId) {
            int[] ids;
            if(!string.IsNullOrWhiteSpace(overseasPartsFigure))
                ids = ObjectContext.PartsSupplierRelations.Where(r => ObjectContext.SpareParts.Any(p => p.OverseasPartsFigure == overseasPartsFigure && r.PartId == p.Id)).Select(m => m.SupplierId).ToArray();
            else
                ids = ObjectContext.PartsSupplierRelations.Where(r => r.PartId == partId && r.PartsSalesCategoryId == partsSalesCategoryId).Select(v => v.SupplierId).ToArray();
            if(!ids.Any())
                return null;
            return ObjectContext.PartsSuppliers.Where(v => ids.Contains(v.Id)).Include("Company").OrderBy(entity => entity.Id);
        }
        public IQueryable<VirtualPartsSupplier> GetPartsSuppliersByBranchId(int? branchId, int? partsSalesCategoryId) {
            //var partsSuppliers = ObjectContext.PartsSuppliers.Where(r => r.Status == (int)DcsBaseDataStatus.有效);
            //if(branchId != null) {
            //    partsSuppliers = partsSuppliers.Where(r => ObjectContext.BranchSupplierRelations.Any(e => e.BranchId == branchId && e.PartsSalesCategoryId == partsSalesCategoryId && e.Status == (int)DcsBaseDataStatus.有效 && e.SupplierId == r.Id));
            //}
            //return partsSuppliers.Include("Company").OrderBy(entity => entity.Id);.
            var resultQuery = from p in ObjectContext.PartsSuppliers.Where(r => r.Status == (int)DcsBaseDataStatus.有效)
                              join c in ObjectContext.Companies on p.Id equals c.Id
                              join b in ObjectContext.BranchSupplierRelations.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.PartsSalesCategoryId == partsSalesCategoryId && r.BranchId == branchId) on p.Id equals b.SupplierId
                              select new VirtualPartsSupplier {
                                  Id = p.Id,
                                  Code = b.BusinessCode,
                                  Name = b.BusinessName,
                                  ShortName = p.ShortName,
                                  ProvinceName = c.ProvinceName,
                                  CityName = c.CityName,
                                  CountyName = c.CountyName,
                                  ContactPerson = c.ContactPerson,
                                  ContactPhone = c.ContactPhone,
                                  Fax = c.Fax,
                                  ContactMail = c.ContactMail,
                                  LegalRepresentative = c.LegalRepresentative,
                                  BusinessScope = c.BusinessScope,
                                  Status = p.Status,
                                  Remark = p.Remark,
                                  SupplierType = p.SupplierType
                              };
            return resultQuery.OrderBy(r => r.Id);
        }
        public PartsSupplier GetPartsSupplierById(int id) {
            var result = ObjectContext.PartsSuppliers.SingleOrDefault(r => r.Id == id);
            if(result != null) {
                var company = ObjectContext.Companies.SingleOrDefault(r => r.Id == id);
            }
            return result;
        }


        public IEnumerable<VirtualPartsSupplier> 查询配件供应商基本信息关联相关业务(string code, string name, string mdmSupplierCode, int? supplierType) {
            //查询配件供应商基本信息 且存在入库检验单（仓储企业Id=关联企业Id，配件供应商基本信息.Id=对方单位Id，结算状态=未结算）
            var userInfo = Utils.GetCurrentUserInfo();
            string sql = @" select distinct s.id, s.code,s.Status,s.Name,s.ShortName,c.ProvinceName,c.CityName,c.CountyName,c.ContactPerson,c.ContactPhone,c.Fax,c.ContactMail,c.LegalRepresentative,c.BusinessScope,s.Remark,
        (select sum(b.inspectedquantity*b.settlementprice) from partsinboundcheckbill a 
      inner join partsinboundcheckbilldetail b on a.id =  b.partsinboundcheckbillid
      where a.counterpartcompanyid = s.id and a.StorageCompanyId = " + userInfo.EnterpriseId + " and a.SettlementStatus = 2) as WaitSettlementAmount from partssupplier s " +
       "left join partsinboundcheckbill b on s.id = b.counterpartcompanyid and b.SettlementStatus = 2 and b.StorageCompanyId = " + userInfo.EnterpriseId + "left join company c on s.id = c.id where b.storagecompanyid = " + userInfo.EnterpriseId + " and s.status =1 ";
            if (!string.IsNullOrEmpty(code)) {
                sql += " and s.code like '%" + code + "%'";
            }
            if (!string.IsNullOrEmpty(name)) {
                sql += " and s.name like '%" + name + "%'";
            }
            if (!string.IsNullOrEmpty(mdmSupplierCode)) {
                sql += " and c.SupplierCode like '%" + mdmSupplierCode + "%'";
            }
            if (supplierType.HasValue) { 
                sql += " and s.supplierType = " + supplierType;
            }
            sql += " order by WaitSettlementAmount desc";
            var search = ObjectContext.ExecuteStoreQuery<VirtualPartsSupplier>(sql).ToList();
            return search;
        }
        public IQueryable<PartsSupplier> 查询配件供应商基本信息关联相关业务2() {
            //查询配件供应商基本信息 且存在入库检验单（仓储企业Id=关联企业Id，配件供应商基本信息.Id=对方单位Id，结算状态=未结算）
            var userInfo = Utils.GetCurrentUserInfo();
            var partsSuppliers = from a in ObjectContext.PartsSuppliers.Where(r => r.Status == (int)DcsBaseDataStatus.有效)
                                 join b in ObjectContext.PartsOutboundBills.Where(r => r.StorageCompanyId == userInfo.EnterpriseId && r.SettlementStatus == (int)DcsPartsSettlementStatus.待结算 && r.OutboundType == (int)DcsPartsOutboundType.采购退货) on a.Id equals b.CounterpartCompanyId
                                 orderby a.Id
                                 select a;
            return partsSuppliers.Include("Company").Distinct();
        }
        //根据供应商编号查询suppler_tmp表中的供应商名称
        public VirtualIdNameForTmp getSupplierNameByCodeForTmp(string code) {
            if (string.IsNullOrEmpty(code))
                return null;
            string SQL = @"select id,suppcode as code,suppname as name from supplier_tmp where suppcode = '" + code + "'";
            var search = ObjectContext.ExecuteStoreQuery<VirtualIdNameForTmp>(SQL).ToList();
            return search.FirstOrDefault();
        }

    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:33
        //原分布类的函数全部转移到Ach                                                               

        public void UpdatePartsSupplier(PartsSupplier partsSupplier) {
            new PartsSupplierAch(this).UpdatePartsSupplier(partsSupplier);
        }

        public void InsertPartsSupplier(PartsSupplier partsSupplier) {
            new PartsSupplierAch(this).InsertPartsSupplier(partsSupplier);
        }

        public IQueryable<PartsSupplier> GetPartsSuppliersWithCompany() {
            return new PartsSupplierAch(this).GetPartsSuppliersWithCompany();
        }
        /// <summary>
        /// 配件与供应商关系新增时查询供应商
        /// </summary>
        /// <param name="businessCode">供应商业务编码</param>
        /// <returns></returns>
        public IQueryable<VirtualPartsSupplier> GetPartsSuppliersWithCompanyAndBusinessCode(int partsSalesCategoryId) {
            return new PartsSupplierAch(this).GetPartsSuppliersWithCompanyAndBusinessCode(partsSalesCategoryId);
        }

        public IQueryable<VirtualPartsSupplier> GetPartsSuppliersWithVirtualPartsSupplier() {
            return new PartsSupplierAch(this).GetPartsSuppliersWithVirtualPartsSupplier();
        }
        
        [Query(HasSideEffects = true)]
        public IQueryable<PartsSupplier> GetPartsSuppliersWithCompanyByIds(int[] ids) {
            return new PartsSupplierAch(this).GetPartsSuppliersWithCompanyByIds(ids);
        }

        public IQueryable<PartsSupplier> GetPartsSuppliersWithCompanyByPartId(int partId, string overseasPartsFigure) {
            return new PartsSupplierAch(this).GetPartsSuppliersWithCompanyByPartId(partId,overseasPartsFigure);
        }
                public IQueryable<PartsSupplier> GetPartsSuppliersWithCompanyByPartIdWithPartsSalesCategory(int partId, string overseasPartsFigure, int partsSalesCategoryId) {
            return new PartsSupplierAch(this).GetPartsSuppliersWithCompanyByPartIdWithPartsSalesCategory(partId,overseasPartsFigure,partsSalesCategoryId);
        }
                public IQueryable<VirtualPartsSupplier> GetPartsSuppliersByBranchId(int? branchId, int? partsSalesCategoryId) {
            return new PartsSupplierAch(this).GetPartsSuppliersByBranchId(branchId,partsSalesCategoryId);
        }

        public PartsSupplier GetPartsSupplierById(int id) {
            return new PartsSupplierAch(this).GetPartsSupplierById(id);
        }
        
        public IEnumerable<VirtualPartsSupplier> 查询配件供应商基本信息关联相关业务(string code, string name, string mdmSupplierCode, int? supplierType) {
            return new PartsSupplierAch(this).查询配件供应商基本信息关联相关业务(code,name,mdmSupplierCode,supplierType);
        }

        public IQueryable<PartsSupplier> 查询配件供应商基本信息关联相关业务2() {
            return new PartsSupplierAch(this).查询配件供应商基本信息关联相关业务2();
        }

        public VirtualIdNameForTmp getSupplierNameByCodeForTmp(string code) {
            return new PartsSupplierAch(this).getSupplierNameByCodeForTmp(code);
        }
    }
}
