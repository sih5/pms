﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Configuration;
using Microsoft.Data.Extensions;
using Newtonsoft.Json;
using StackExchange.Redis;
using Sunlight.Silverlight.Dcs.Web.Redis;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsSupplierRelationAch : DcsSerivceAchieveBase {
        internal void InsertPartsSupplierRelationValidate(PartsSupplierRelation partsSupplierRelation) {
            var dbpartsBranch = ObjectContext.PartsSupplierRelations.Where(r => r.PartsSalesCategoryId == partsSupplierRelation.PartsSalesCategoryId && r.PartId == partsSupplierRelation.PartId && r.SupplierId == partsSupplierRelation.SupplierId && r.Status != (int)DcsBaseDataStatus.作废).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbpartsBranch != null)
                throw new ValidationException(ErrorStrings.PartsSupplierRelation_Validation1);
            dbpartsBranch = ObjectContext.PartsSupplierRelations.Where(r => r.PartsSalesCategoryId == partsSupplierRelation.PartsSalesCategoryId && r.PartId == partsSupplierRelation.PartId && r.SupplierId == partsSupplierRelation.SupplierId && r.IsPrimary == true && r.Status != (int)DcsBaseDataStatus.作废).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbpartsBranch != null)
                throw new ValidationException(ErrorStrings.PartsSupplierRelation_Validation4);
            var userInfo = Utils.GetCurrentUserInfo();
            partsSupplierRelation.CreatorId = userInfo.Id;
            partsSupplierRelation.CreateTime = DateTime.Now;
            partsSupplierRelation.CreatorName = userInfo.Name;
        }

        internal void UpdatePartsSupplierRelationValidate(PartsSupplierRelation partsSupplierRelation) {
            var dbpartsBranch = ObjectContext.PartsSupplierRelations.Where(r => r.Id == partsSupplierRelation.Id && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbpartsBranch == null)
                throw new ValidationException(ErrorStrings.PartsSupplierRelation_Validation2);
            var rpartsBranch = ObjectContext.PartsSupplierRelations.FirstOrDefault(r => r.Id != partsSupplierRelation.Id && r.PartsSalesCategoryId == partsSupplierRelation.PartsSalesCategoryId && r.PartId == partsSupplierRelation.PartId && r.SupplierId == partsSupplierRelation.SupplierId && r.Status != (int)DcsBaseDataStatus.作废);
            if(rpartsBranch != null)
                throw new ValidationException(ErrorStrings.PartsSupplierRelation_Validation1);
            var userInfo = Utils.GetCurrentUserInfo();
            partsSupplierRelation.ModifierId = userInfo.Id;
            partsSupplierRelation.ModifierName = userInfo.Name;
            partsSupplierRelation.ModifyTime = DateTime.Now;
        }

        public void InsertPartsSupplierRelation(PartsSupplierRelation partsSupplierRelation) {           
            InsertToDatabase(partsSupplierRelation);
            海外生成采购申请单(partsSupplierRelation);
            this.InsertPartsSupplierRelationValidate(partsSupplierRelation);
            var partsSupplierRelationHistory = new PartsSupplierRelationHistory {
                PartsSupplierRelationId = partsSupplierRelation.Id,
                BranchId = partsSupplierRelation.BranchId,
                PartsSalesCategoryId = partsSupplierRelation.PartsSalesCategoryId,
                PartsSalesCategoryName = partsSupplierRelation.PartsSalesCategoryName,
                PartId = partsSupplierRelation.PartId,
                SupplierId = partsSupplierRelation.SupplierId,
                SupplierPartCode = partsSupplierRelation.SupplierPartCode,
                SupplierPartName = partsSupplierRelation.SupplierPartName,
                IsPrimary = partsSupplierRelation.IsPrimary,
                PurchasePercentage = partsSupplierRelation.PurchasePercentage,
                EconomicalBatch = partsSupplierRelation.EconomicalBatch,
                MinBatch = partsSupplierRelation.MinBatch,
                Status = partsSupplierRelation.Status,
                MonthlyArrivalPeriod = partsSupplierRelation.MonthlyArrivalPeriod,
                EmergencyArrivalPeriod = partsSupplierRelation.EmergencyArrivalPeriod,
                ArrivalReplenishmentCycle = partsSupplierRelation.ArrivalReplenishmentCycle,
                Remark = partsSupplierRelation.Remark
            };
            partsSupplierRelation.PartsSupplierRelationHistories.Add(partsSupplierRelationHistory);
            DomainService.InsertPartsSupplierRelationHistory(partsSupplierRelationHistory);
        }

        public void 海外生成采购申请单(PartsSupplierRelation partsSupplierRelation) {
            //校验最小采购批量：必须为 配件营销信息.最小包装.系数的  数量的整数倍
            //查询配件的主包装属性
            var pack = ObjectContext.PartsBranchPackingProps.Where(r => r.SparePartId == partsSupplierRelation.PartId && ObjectContext.PartsBranches.Any(d => d.PartId == r.SparePartId && d.MainPackingType == r.PackingType && r.PartsBranchId == d.Id && d.Status == (int)DcsMasterDataStatus.有效)).FirstOrDefault();
           if(null==pack){
               throw new ValidationException("请先维护配件的包装设置");
           }
            int remainder = partsSupplierRelation.MinBatch.Value % Convert.ToInt32(pack.PackingCoefficient);
            if (remainder != 0)
            {
                throw new ValidationException("最小采购批量：必须为 配件营销信息最小包装的包装数量的整数倍");
            }
            //如果配件在集团配件下存在采购价格 则生成 采购价格申请单
            if(partsSupplierRelation.Status == (int)DcsBaseDataStatus.有效 && partsSupplierRelation.PartsSalesCategoryName == "海外国际") {
                var partsPurchasePricing = ObjectContext.PartsPurchasePricings.FirstOrDefault(r => r.PartsSalesCategoryName == "集团配件" && r.PartsSupplierId == partsSupplierRelation.SupplierId && r.Status == (int)DcsPartsPurchasePricingStatus.生效 && r.PartId == partsSupplierRelation.PartId && r.ValidFrom < DateTime.Now && (r.ValidTo ?? DateTime.MaxValue) > DateTime.Now);
                if(partsPurchasePricing == null)
                    return;
                var partsPurchasePricingChange = new PartsPurchasePricingChange {
                    Code = GlobalVar.ASSIGNED_BY_SERVER,
                    BranchId = partsSupplierRelation.BranchId,
                    PartsSalesCategoryId = partsSupplierRelation.PartsSalesCategoryId,
                    PartsSalesCategoryName = partsSupplierRelation.PartsSalesCategoryName,
                    Status = (int)DcsPartsPurchasePricingChangeStatus.新增,

                };
                InsertToDatabase(partsPurchasePricingChange);
                new PartsPurchasePricingChangeAch(this.DomainService).InsertPartsPurchasePricingChangeValidate(partsPurchasePricingChange);
                var sparePart = ObjectContext.SpareParts.Single(r => r.Id == partsSupplierRelation.PartId);
                var partsSupplier = ObjectContext.PartsSuppliers.Single(r => r.Id == partsSupplierRelation.SupplierId);
                var partsPurchasePricingDetail = new PartsPurchasePricingDetail {
                    ParentId = partsPurchasePricingChange.Id,
                    PartId = partsSupplierRelation.PartId,
                    PartCode = sparePart.Code,
                    PartName = sparePart.Name,
                    SupplierId = partsSupplierRelation.SupplierId,
                    SupplierCode = partsSupplier.Code,
                    SupplierName = partsSupplier.Name,
                    PriceType = partsPurchasePricing.PriceType,
                    Price = partsPurchasePricing.PurchasePrice,
                    ReferencePrice = partsPurchasePricing.PurchasePrice,
                    ValidFrom = DateTime.Now,
                    ValidTo = (partsPurchasePricing.ValidTo ?? DateTime.MaxValue)
                };
                InsertToDatabase(partsPurchasePricingDetail);
            }
        }

        public void UpdatePartsSupplierRelation(PartsSupplierRelation partsSupplierRelation) {
            UpdateToDatabase(partsSupplierRelation);
            if (partsSupplierRelation.Status == (int)DcsBaseDataStatus.有效)
                海外生成采购申请单(partsSupplierRelation);
            this.UpdatePartsSupplierRelationValidate(partsSupplierRelation);
            var partsSupplierRelationHistory = new PartsSupplierRelationHistory {
                PartsSupplierRelationId = partsSupplierRelation.Id,
                BranchId = partsSupplierRelation.BranchId,
                PartsSalesCategoryId = partsSupplierRelation.PartsSalesCategoryId,
                PartsSalesCategoryName = partsSupplierRelation.PartsSalesCategoryName,
                PartId = partsSupplierRelation.PartId,
                SupplierId = partsSupplierRelation.SupplierId,
                SupplierPartCode = partsSupplierRelation.SupplierPartCode,
                SupplierPartName = partsSupplierRelation.SupplierPartName,
                IsPrimary = partsSupplierRelation.IsPrimary,
                PurchasePercentage = partsSupplierRelation.PurchasePercentage,
                EconomicalBatch = partsSupplierRelation.EconomicalBatch,
                MinBatch = partsSupplierRelation.MinBatch,
                Status = partsSupplierRelation.Status,
                MonthlyArrivalPeriod = partsSupplierRelation.MonthlyArrivalPeriod,
                EmergencyArrivalPeriod = partsSupplierRelation.EmergencyArrivalPeriod,
                ArrivalReplenishmentCycle = partsSupplierRelation.ArrivalReplenishmentCycle,
                Remark = partsSupplierRelation.Remark
            };
            partsSupplierRelation.PartsSupplierRelationHistories.Add(partsSupplierRelationHistory);
            DomainService.InsertPartsSupplierRelationHistory(partsSupplierRelationHistory);
        }

        public IQueryable<PartsSupplierRelation> GetPartsSupplierRelationsWithDetails() {
            return ObjectContext.PartsSupplierRelations.Include("PartsSupplier").Include("SparePart").Include("Branch").OrderBy(entity => entity.Id);
        }

        public IQueryable<PartsClaimPrice> GetPartsClaimPriceWithDetails() {
            return ObjectContext.PartsClaimPrices.Include("SparePart").OrderBy(e => e.PartsSupplierId);
        }

        internal async Task<IEnumerable<VirtualPartsClaimPrice>> GetPartsClaimPriceWithDetailsWithDealerIsNull(int dealerId, int subDealerId, int branchId, int partsSalesCategoryId, int? isNotWarranty) {
            var isCache = Convert.ToBoolean(WebConfigurationManager.AppSettings["isCache"]);
            if(isCache) {
                var paramDic = new Dictionary<string, object> {
                { NameOfExtention.Nameof(() => dealerId), dealerId },
                { NameOfExtention.Nameof(() => subDealerId), subDealerId },
                { NameOfExtention.Nameof(() => branchId), branchId },
                { NameOfExtention.Nameof(() => partsSalesCategoryId), partsSalesCategoryId },
                { NameOfExtention.Nameof(() => isNotWarranty), isNotWarranty }
            };
                // 生成key virtualPartsClaimPricesList:dealerId:3200:subDealerId:-1:branchId:2:partsSalesCategoryId:1:isNotWarranty:1:Sunlight.Silverlight.Dcs.Web.VirtualPartsClaimPrice[].Take(15)
                var dataKey = GenerateRedisKey("virtualPartsClaimPricesList", this.ParamQueryable, paramDic);
                // 生成总数key virtualPartsClaimPricesList:dealerId:3200:subDealerId:-1:branchId:2:partsSalesCategoryId:1:isNotWarranty:1:Sunlight.Silverlight.Dcs.Web.VirtualPartsClaimPrice[]
                var countKey = GenerateRedisTotalCountKey("virtualPartsClaimPricesList", this.ParamQueryable, paramDic);
                var originalExpression = GetVirtualPartsClaimPricesFromDb(dealerId, subDealerId, branchId, partsSalesCategoryId, isNotWarranty);
                var reusltExpression = originalExpression.ComposeWithoutPaging(t => t, this.ParamQueryable);
                try {
                    var cache = RedisConnectionHelp.Instance.GetDatabase();
                    int totalCount;
                    if(!int.TryParse(await cache.StringGetAsync(countKey), out totalCount)) {
                        // 查询数据库的表达式合并分页以外的查询条件来计算总数
                        totalCount = reusltExpression.Count();
                        await cache.StringSetAsync(countKey, totalCount, new TimeSpan(0, 5, 0));
                    }
                    var serializedVirtualPartsClaimPrices = await cache.StringGetAsync(dataKey);
                    List<VirtualPartsClaimPrice> virtualPartsClaimPrices;
                    if(!serializedVirtualPartsClaimPrices.IsNullOrEmpty) {
                        virtualPartsClaimPrices = JsonConvert.DeserializeObject<List<VirtualPartsClaimPrice>>(serializedVirtualPartsClaimPrices);
                    } else {
                        // 返回客户端的数据必须合并分页条件
                        virtualPartsClaimPrices = reusltExpression.ComposeOnlyPaging(this.ParamQueryable).ToList();
                        await cache.StringSetAsync(dataKey, await Task.Factory.StartNew(() => JsonConvert.SerializeObject(virtualPartsClaimPrices)), new TimeSpan(1, 0, 0));
                    }
                    DomainService.SetQueryResultManually(virtualPartsClaimPrices, totalCount);
                    // 手动设置了查询结构后，后面的内容将不会返回给客户端，必须返回null，减少计算
                    return null;
                } catch(Exception) {
                    // Redis 异常时，直接从数据库取数据
                    return originalExpression;
                }
            } else
                return this.GetVirtualPartsClaimPricesFromDb(dealerId, subDealerId, branchId, partsSalesCategoryId, isNotWarranty);
        }

        public IQueryable<VirtualPartsClaimPrice> GetVirtualPartsClaimPricesFromDb(int dealerId, int subDealerId, int branchId, int partsSalesCategoryId, int? isNotWarranty) {
            IQueryable<DealerPartsStock> dealerPartsStocks = ObjectContext.DealerPartsStocks;
            var partsSalesCategories = ObjectContext.PartsSalesCategories.Where(r => r.Status == (int)DcsBaseDataStatus.有效);
            if(isNotWarranty == (int)DcsWarrantyStatus.保内) {
                partsSalesCategories = partsSalesCategories.Where(r => !r.IsNotWarranty);
                dealerPartsStocks = dealerPartsStocks.Where(r => this.ObjectContext.PartsSalesCategories.Any(x => x.Id == r.SalesCategoryId && x.Status == (int)DcsBaseDataStatus.有效 && !x.IsNotWarranty));
            } else
                dealerPartsStocks = dealerPartsStocks.Where(r => this.ObjectContext.PartsSalesCategories.Any());
            dealerPartsStocks = dealerPartsStocks.Where(r => r.DealerId == dealerId && r.SubDealerId == subDealerId);
            var result = from sparePart in ObjectContext.SpareParts.Where(r => r.Status == (int)DcsMasterDataStatus.有效)
                         join partsBranch in ObjectContext.PartsBranches.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.PartsSalesCategoryId == partsSalesCategoryId) on sparePart.Id equals partsBranch.PartId
                         join partsSalesCategory in partsSalesCategories on partsBranch.PartsSalesCategoryId equals partsSalesCategory.Id
                         join partsSupplierRelation in ObjectContext.PartsSupplierRelations.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on new {
                             PartId = sparePart.Id,
                             PartsSalesCategoryId = partsSalesCategory.Id
                         } equals new {
                             partsSupplierRelation.PartId,
                             partsSupplierRelation.PartsSalesCategoryId
                         }
                         join partsSupplier in ObjectContext.PartsSuppliers.Where(r => r.Status == (int)DcsMasterDataStatus.有效) on partsSupplierRelation.SupplierId equals partsSupplier.Id
                         join tempBranchSupplierRelation in ObjectContext.BranchSupplierRelations.Where(r => r.BranchId == branchId) on new {
                             PartsSalesCategoryId = partsSalesCategory.Id,
                             SupplierId = partsSupplier.Id
                         } equals new {
                             tempBranchSupplierRelation.PartsSalesCategoryId,
                             tempBranchSupplierRelation.SupplierId
                         } into t4
                         from branchSupplierRelation in t4.DefaultIfEmpty()

                         join tempPartsSalesPrice in ObjectContext.PartsSalesPrices.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on new {
                             SparePartId = sparePart.Id,
                             PartsSalesCategoryId = partsSalesCategory.Id
                         } equals new {
                             tempPartsSalesPrice.SparePartId,
                             tempPartsSalesPrice.PartsSalesCategoryId
                         } into t1
                         from partsSalesPrice in t1.DefaultIfEmpty()
                         join tempdealerPartsStock in dealerPartsStocks on
                         new {
                             partsBranch.BranchId,
                             SparePartId = sparePart.Id
                         } equals
                         new {
                             tempdealerPartsStock.BranchId,
                             tempdealerPartsStock.SparePartId
                         } into t3
                         from dealerPartsStock in t3.DefaultIfEmpty()
                         join tempPartDeleaveInformation in ObjectContext.PartDeleaveInformations on new {
                             OldPartId = dealerPartsStock.SparePartId,
                             dealerPartsStock.BranchId,
                             PartsSalesCategoryId = dealerPartsStock.SalesCategoryId
                         } equals new {
                             tempPartDeleaveInformation.OldPartId,
                             tempPartDeleaveInformation.BranchId,
                             tempPartDeleaveInformation.PartsSalesCategoryId
                         } into t2
                         from partDeleaveInformation in t2.DefaultIfEmpty()
                         select new VirtualPartsClaimPrice {
                             BranchId = partsBranch.BranchId,
                             Quantity = dealerPartsStock.Quantity,
                             DealerId = dealerPartsStock.DealerId,
                             DeleaveAmount = partDeleaveInformation.DeleaveAmount,
                             DeleavePartCode = partDeleaveInformation.DeleavePartCode,
                             MeasureUnit = sparePart.MeasureUnit,
                             PartsSalesCategoryId = dealerPartsStock == null ? partsSalesCategory.Id : dealerPartsStock.SalesCategoryId,
                             PartsSalesCategoryName = dealerPartsStock == null ? partsSalesCategory.Name : dealerPartsStock.SalesCategoryName,
                             PartsSupplierCode = branchSupplierRelation.BusinessCode,
                             PartsSupplierId = partsSupplierRelation.SupplierId,
                             PartsSupplierName = branchSupplierRelation.BusinessName,
                             PartsWarrantyCategoryId = partsBranch.PartsWarrantyCategoryId,
                             PartsWarrantyCategoryName = partsBranch.PartsWarrantyCategoryName,
                             PartType = sparePart.PartType,
                             SalesPrice = partsSalesPrice.SalesPrice,
                             Specification = sparePart.Specification,
                             SubDealerId = dealerPartsStock.SubDealerId,
                             UsedPartsCode = sparePart.Code,
                             UsedPartsId = sparePart.Id,
                             UsedPartsName = sparePart.Name,
                             UsedPartsReturnPolicy = partsBranch.PartsReturnPolicy,
                             SparePartId = sparePart.Id,
                             SparePartCode = sparePart.Code,
                             SparePartName = sparePart.Name,
                             SparePartPartType = sparePart.PartType,
                             SparePartMeasureUnit = sparePart.MeasureUnit,
                             SparePartSpecification = sparePart.Specification,
                             SparePartFeature = sparePart.Feature
                         };
            return result.OrderBy(r => r.SparePartId);
        }

        public IQueryable<VirtualPartsClaimPrice> GetPartsClaimPriceWithDetailsWithDealerIsNull2(int dealerId, int subDealerId, int branchId, int partsSalesCategoryId, int? isNotWarranty) {
            if(isNotWarranty == (int)DcsWarrantyStatus.保内) {

                var result = from sparePart in ObjectContext.SpareParts.Where(r => r.Status == (int)DcsMasterDataStatus.有效)
                             join partsBranch in ObjectContext.PartsBranches.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.PartsSalesCategoryId == partsSalesCategoryId) on sparePart.Id equals partsBranch.PartId
                             join partsSalesCategory in ObjectContext.PartsSalesCategories.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && !r.IsNotWarranty) on partsBranch.PartsSalesCategoryId equals partsSalesCategory.Id
                             join partsSupplierRelation in ObjectContext.PartsSupplierRelations.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on new {
                                 PartId = sparePart.Id,
                                 PartsSalesCategoryId = partsSalesCategory.Id
                             } equals new {
                                 partsSupplierRelation.PartId,
                                 partsSupplierRelation.PartsSalesCategoryId
                             }
                             join partsSupplier in ObjectContext.PartsSuppliers.Where(r => r.Status == (int)DcsMasterDataStatus.有效) on partsSupplierRelation.SupplierId equals partsSupplier.Id
                             join tempBranchSupplierRelation in ObjectContext.BranchSupplierRelations.Where(r => r.BranchId == branchId) on new {
                                 PartsSalesCategoryId = partsSalesCategory.Id,
                                 SupplierId = partsSupplier.Id
                             } equals new {
                                 tempBranchSupplierRelation.PartsSalesCategoryId,
                                 tempBranchSupplierRelation.SupplierId
                             } into t4
                             from branchSupplierRelation in t4.DefaultIfEmpty()

                             join tempPartsSalesPrice in ObjectContext.PartsSalesPrices.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on new {
                                 SparePartId = sparePart.Id,
                                 PartsSalesCategoryId = partsSalesCategory.Id
                             } equals new {
                                 tempPartsSalesPrice.SparePartId,
                                 tempPartsSalesPrice.PartsSalesCategoryId
                             } into t1
                             from partsSalesPrice in t1.DefaultIfEmpty()
                             join tempdealerPartsStock in ObjectContext.DealerPartsStocks.Where(r => this.ObjectContext.PartsSalesCategories.Any(x => x.Id == r.SalesCategoryId && x.Status == (int)DcsBaseDataStatus.有效 && !x.IsNotWarranty) && r.DealerId == dealerId && r.SubDealerId == subDealerId) on
                             new {
                                 partsBranch.BranchId,
                                 SparePartId = sparePart.Id
                             } equals
                             new {
                                 tempdealerPartsStock.BranchId,
                                 tempdealerPartsStock.SparePartId
                             } into t3
                             from dealerPartsStock in t3.DefaultIfEmpty()
                             join tempPartDeleaveInformation in ObjectContext.PartDeleaveInformations on new {
                                 OldPartId = dealerPartsStock.SparePartId,
                                 dealerPartsStock.BranchId,
                                 PartsSalesCategoryId = dealerPartsStock.SalesCategoryId
                             } equals new {
                                 tempPartDeleaveInformation.OldPartId,
                                 tempPartDeleaveInformation.BranchId,
                                 tempPartDeleaveInformation.PartsSalesCategoryId
                             } into t2
                             from partDeleaveInformation in t2.DefaultIfEmpty()
                             select new VirtualPartsClaimPrice {
                                 BranchId = partsBranch.BranchId,
                                 Quantity = dealerPartsStock.Quantity,
                                 DealerId = dealerPartsStock.DealerId,
                                 DeleaveAmount = partDeleaveInformation.DeleaveAmount,
                                 DeleavePartCode = partDeleaveInformation.DeleavePartCode,
                                 MeasureUnit = sparePart.MeasureUnit,
                                 PartsSalesCategoryId = dealerPartsStock == null ? partsSalesCategory.Id : dealerPartsStock.SalesCategoryId,
                                 PartsSalesCategoryName = dealerPartsStock == null ? partsSalesCategory.Name : dealerPartsStock.SalesCategoryName,
                                 PartsSupplierCode = branchSupplierRelation.BusinessCode,
                                 PartsSupplierId = partsSupplierRelation.SupplierId,
                                 PartsSupplierName = branchSupplierRelation.BusinessName,
                                 PartsWarrantyCategoryId = partsBranch.PartsWarrantyCategoryId,
                                 PartsWarrantyCategoryName = partsBranch.PartsWarrantyCategoryName,
                                 PartType = sparePart.PartType,
                                 SalesPrice = partsSalesPrice.SalesPrice,
                                 Specification = sparePart.Specification,
                                 SubDealerId = dealerPartsStock.SubDealerId,
                                 UsedPartsCode = sparePart.Code,
                                 UsedPartsId = sparePart.Id,
                                 UsedPartsName = sparePart.Name,
                                 UsedPartsReturnPolicy = partsBranch.PartsReturnPolicy,
                                 SparePartId = sparePart.Id,
                                 SparePartCode = sparePart.Code,
                                 SparePartName = sparePart.Name,
                                 SparePartPartType = sparePart.PartType,
                                 SparePartMeasureUnit = sparePart.MeasureUnit,
                                 SparePartSpecification = sparePart.Specification,
                                 SparePartFeature = sparePart.Feature
                             };
                return result.OrderBy(r => r.SparePartId);
            } else {
                var salesCenterstrategyIds = this.ObjectContext.SalesCenterstrategies.Where(p => p.AllowUseInWarrantyPart == true).Select(p => p.PartsSalesCategoryId);
                var result = from sparePart in ObjectContext.SpareParts.Where(r => r.Status == (int)DcsMasterDataStatus.有效)
                             join dealerPartsStock in ObjectContext.DealerPartsStocks.Where(r => this.ObjectContext.PartsSalesCategories.Any(x => r.DealerId == dealerId && r.SubDealerId == subDealerId)) on sparePart.Id equals dealerPartsStock.SparePartId
                             join partsSupplierRelation in ObjectContext.PartsSupplierRelations.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on new {
                                 PartId = dealerPartsStock.SparePartId,
                                 PartsSalesCategoryId = dealerPartsStock.SalesCategoryId
                             } equals new {
                                 partsSupplierRelation.PartId,
                                 partsSupplierRelation.PartsSalesCategoryId
                             }
                             join partsSupplier in ObjectContext.PartsSuppliers.Where(r => r.Status == (int)DcsMasterDataStatus.有效) on partsSupplierRelation.SupplierId equals partsSupplier.Id
                             join tempPartsBranch in ObjectContext.PartsBranches.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on new {
                                 PartId = dealerPartsStock.SparePartId,
                                 PartsSalesCategoryId = dealerPartsStock.SalesCategoryId
                             } equals new {
                                 tempPartsBranch.PartId,
                                 tempPartsBranch.PartsSalesCategoryId
                             } into t3
                             from partsBranch in t3.DefaultIfEmpty()
                             join partsSalesCategory in ObjectContext.PartsSalesCategories.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on partsSupplierRelation.PartsSalesCategoryId equals partsSalesCategory.Id
                             join tempPartsSalesPrice in ObjectContext.PartsSalesPrices.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on new {
                                 SparePartId = sparePart.Id,
                                 PartsSalesCategoryId = partsSalesCategory.Id
                             } equals new {
                                 tempPartsSalesPrice.SparePartId,
                                 tempPartsSalesPrice.PartsSalesCategoryId
                             } into t1
                             from partsSalesPrice in t1.DefaultIfEmpty()
                             join tempPartDeleaveInformation in ObjectContext.PartDeleaveInformations on new {
                                 OldPartId = dealerPartsStock.SparePartId,
                                 dealerPartsStock.BranchId,
                                 PartsSalesCategoryId = dealerPartsStock.SalesCategoryId
                             } equals new {
                                 tempPartDeleaveInformation.OldPartId,
                                 tempPartDeleaveInformation.BranchId,
                                 tempPartDeleaveInformation.PartsSalesCategoryId
                             } into t2
                             from partDeleaveInformation in t2.DefaultIfEmpty()
                             join tempBranchSupplierRelation in ObjectContext.BranchSupplierRelations.Where(r => r.BranchId == branchId) on new {
                                 PartsSalesCategoryId = partsSalesCategory.Id,
                                 SupplierId = partsSupplier.Id
                             } equals new {
                                 tempBranchSupplierRelation.PartsSalesCategoryId,
                                 tempBranchSupplierRelation.SupplierId
                             } into t4
                             from branchSupplierRelation in t4.DefaultIfEmpty()
                             select new VirtualPartsClaimPrice {
                                 BranchId = partsBranch.BranchId,
                                 Quantity = dealerPartsStock.Quantity,
                                 DealerId = dealerPartsStock.DealerId,
                                 DeleaveAmount = partDeleaveInformation.DeleaveAmount,
                                 DeleavePartCode = partDeleaveInformation.DeleavePartCode,
                                 MeasureUnit = sparePart.MeasureUnit,
                                 PartsSalesCategoryId = dealerPartsStock == null ? partsSalesCategory.Id : dealerPartsStock.SalesCategoryId,
                                 PartsSalesCategoryName = dealerPartsStock == null ? partsSalesCategory.Name : dealerPartsStock.SalesCategoryName,
                                 PartsSupplierCode = branchSupplierRelation.BusinessCode,
                                 PartsSupplierId = partsSupplierRelation.SupplierId,
                                 PartsSupplierName = branchSupplierRelation.BusinessName,
                                 PartsWarrantyCategoryId = partsBranch.PartsWarrantyCategoryId,
                                 PartsWarrantyCategoryName = partsBranch.PartsWarrantyCategoryName,
                                 PartType = sparePart.PartType,
                                 SalesPrice = partsSalesPrice.SalesPrice,
                                 Specification = sparePart.Specification,
                                 SubDealerId = dealerPartsStock.SubDealerId,
                                 UsedPartsCode = sparePart.Code,
                                 UsedPartsId = sparePart.Id,
                                 UsedPartsName = sparePart.Name,
                                 UsedPartsReturnPolicy = partsBranch.PartsReturnPolicy,
                                 SparePartId = sparePart.Id,
                                 SparePartCode = sparePart.Code,
                                 SparePartName = sparePart.Name,
                                 SparePartPartType = sparePart.PartType,
                                 SparePartMeasureUnit = sparePart.MeasureUnit,
                                 SparePartSpecification = sparePart.Specification,
                                 SparePartFeature = sparePart.Feature
                             };

                return result.OrderBy(r => r.SparePartId).Where(r => salesCenterstrategyIds.Contains(r.PartsSalesCategoryId));
            }
        }
        public IQueryable<PartsClaimPriceForbw> GetPartsClaimPriceForbwWithDetails() {
            return ObjectContext.PartsClaimPriceForbws.Include("SparePart").OrderBy(e => e.PartsSupplierId);
        }
        public IQueryable<SparePartHistory> GetPartsBranchWithSparePartHistorys() {
            return ObjectContext.SparePartHistories.Include("SparePart").OrderBy(e => e.Id);
        }

        public IQueryable<PartsSupplierRelation> GetPartsSupplierRelationsWithPartsSupplier() {
            return ObjectContext.PartsSupplierRelations.Include("PartsSupplier").OrderBy(entity => entity.Id);
        }

        [Query(HasSideEffects = true)]
        public IQueryable<PartsSupplierRelation> GetPartsSupplierRelationsWithPartsSupplierByIds(int[] sparePartIds, int partsSalesCategoryId) {
            return ObjectContext.PartsSupplierRelations.Where(r => sparePartIds.Contains(r.PartId) && r.PartsSalesCategoryId == partsSalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效).Include("PartsSupplier").OrderBy(entity => entity.Id);
        }

        [Query(HasSideEffects = true)]
        public IQueryable<PartsSupplierRelation> GetPartsSupplierRelationsByPartsIds(int[] ids) {
            var userInfo = Utils.GetCurrentUserInfo();
            return ObjectContext.PartsSupplierRelations.Where(r => ids.Contains(r.PartId) && r.BranchId == userInfo.EnterpriseId).Include("PartsSupplier").OrderBy(r => r.Id);
        }


        public IQueryable<PartsSupplierRelation> 查询配件实际下达供应商编码(int partId, int partsSalesCategoryId) {
            return ObjectContext.PartsSupplierRelations.Where(r => r.PartId == partId && r.PartsSalesCategoryId == partsSalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效).Include("PartsSalesCategory").Include("SparePart").Include("PartsSupplier").OrderBy(entity => entity.Id);
        }



        [Query(HasSideEffects = true)]
        public IQueryable<PartsSupplierRelation> GetPartsSupplierRelationsByPartsIdsForTGFX(int[] ids, int warehouseId) {
            var userInfo = Utils.GetCurrentUserInfo();
            var salesunit = DomainService.GetSalesUnitByWarehouseId(warehouseId);
            if(salesunit == null) {
                throw new ValidationException("未找到对应的供应商，无法生成采购订单");
            }
            var partsSalesCategoryId = salesunit.PartsSalesCategoryId;
            return ObjectContext.PartsSupplierRelations.Where(r => ids.Contains(r.PartId) && r.BranchId == userInfo.EnterpriseId && r.PartsSalesCategoryId == partsSalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效).Include("PartsSupplier").OrderBy(r => r.Id);

        }

        public IQueryable<PartsSupplierRelationWithPurchasePrice> 查询配件与供应商关系及采购价(int supplierId, DateTime? time) {
            var result = from a in ObjectContext.PartsSupplierRelations
                         join b in ObjectContext.SpareParts on a.PartId equals b.Id
                         join c in ObjectContext.PartsSalesCategories on a.PartsSalesCategoryId equals c.Id
                         join d in ObjectContext.PartsSuppliers on a.SupplierId equals d.Id
                         join e in ObjectContext.PartsPurchasePricings on new {
                             PartId = a.PartId,
                             PartsSalesCategoryId = a.PartsSalesCategoryId,
                             PartsSupplierid = a.SupplierId
                         } equals new {
                             PartId = e.PartId,
                             PartsSalesCategoryId = e.PartsSalesCategoryId,
                             PartsSupplierid = e.PartsSupplierId
                         }
                         where (time == null || (e.ValidFrom <= time.Value && e.ValidTo >= time.Value)) && a.SupplierId == supplierId
                         select new PartsSupplierRelationWithPurchasePrice {
                             PartsSupplierRelationId = a.Id,
                             SparePartId = b.Id,
                             SparePartCode = b.Code,
                             SparePartName = b.Name,
                             ReferenceCode = b.ReferenceCode,
                             ReferenceName = b.ReferenceName,
                             PartsSalesCategoryId = c.Id,
                             PartsSalesCategoryCode = c.Code,
                             PartsSalesCategoryName = c.Name,
                             PartsSupplierId = d.Id,
                             SupplierCode = d.Code,
                             SupplierName = d.Name,
                             PartsPurchasePricingId = e.Id,
                             PriceType = e.PriceType,
                             PurchasePrice = e.PurchasePrice,
                             ValidFrom = e.ValidFrom,
                             ValidTo = e.ValidTo,
                             CreatorId = e.CreatorId,
                             CreatorName = e.CreatorName,
                             CreateTime = e.CreateTime,
                             SupplierPartCode=a.SupplierPartCode
                         };
            return result.OrderBy(r => r.PartsSupplierRelationId);
        }


    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:33
        //原分布类的函数全部转移到Ach

        public void InsertPartsSupplierRelation(PartsSupplierRelation partsSupplierRelation) {
            new PartsSupplierRelationAch(this).InsertPartsSupplierRelation(partsSupplierRelation);
        }

        public void 海外生成采购申请单(PartsSupplierRelation partsSupplierRelation) {
            new PartsSupplierRelationAch(this).海外生成采购申请单(partsSupplierRelation);
        }

        public void UpdatePartsSupplierRelation(PartsSupplierRelation partsSupplierRelation) {
            new PartsSupplierRelationAch(this).UpdatePartsSupplierRelation(partsSupplierRelation);
        }

        public IQueryable<PartsSupplierRelation> GetPartsSupplierRelationsWithDetails() {
            return new PartsSupplierRelationAch(this).GetPartsSupplierRelationsWithDetails();
        }

        public IQueryable<PartsClaimPrice> GetPartsClaimPriceWithDetails() {
            return new PartsSupplierRelationAch(this).GetPartsClaimPriceWithDetails();
        }

        public IEnumerable<VirtualPartsClaimPrice> GetPartsClaimPriceWithDetailsWithDealerIsNull(int dealerId, int subDealerId, int branchId, int partsSalesCategoryId, int? isNotWarranty) {
            return new PartsSupplierRelationAch(this).GetPartsClaimPriceWithDetailsWithDealerIsNull(dealerId, subDealerId, branchId, partsSalesCategoryId, isNotWarranty).Result;
        }

        public IQueryable<VirtualPartsClaimPrice> GetPartsClaimPriceWithDetailsWithDealerIsNull2(int dealerId, int subDealerId, int branchId, int partsSalesCategoryId, int? isNotWarranty) {
            return new PartsSupplierRelationAch(this).GetPartsClaimPriceWithDetailsWithDealerIsNull2(dealerId, subDealerId, branchId, partsSalesCategoryId, isNotWarranty);
        }
        public IQueryable<PartsClaimPriceForbw> GetPartsClaimPriceForbwWithDetails() {
            return new PartsSupplierRelationAch(this).GetPartsClaimPriceForbwWithDetails();
        }
        public IQueryable<SparePartHistory> GetPartsBranchWithSparePartHistorys() {
            return new PartsSupplierRelationAch(this).GetPartsBranchWithSparePartHistorys();
        }

        public IQueryable<PartsSupplierRelation> GetPartsSupplierRelationsWithPartsSupplier() {
            return new PartsSupplierRelationAch(this).GetPartsSupplierRelationsWithPartsSupplier();
        }


        [Query(HasSideEffects = true)]
        public IQueryable<PartsSupplierRelation> GetPartsSupplierRelationsWithPartsSupplierByIds(int[] sparePartIds, int partsSalesCategoryId) {
            return new PartsSupplierRelationAch(this).GetPartsSupplierRelationsWithPartsSupplierByIds(sparePartIds, partsSalesCategoryId);
        }


        [Query(HasSideEffects = true)]
        public IQueryable<PartsSupplierRelation> GetPartsSupplierRelationsByPartsIds(int[] ids) {
            return new PartsSupplierRelationAch(this).GetPartsSupplierRelationsByPartsIds(ids);
        }


        public IQueryable<PartsSupplierRelation> 查询配件实际下达供应商编码(int partId, int partsSalesCategoryId) {
            return new PartsSupplierRelationAch(this).查询配件实际下达供应商编码(partId, partsSalesCategoryId);
        }

        [Query(HasSideEffects = true)]
        public IQueryable<PartsSupplierRelation> GetPartsSupplierRelationsByPartsIdsForTGFX(int[] ids, int warehouseId) {
            return new PartsSupplierRelationAch(this).GetPartsSupplierRelationsByPartsIdsForTGFX(ids, warehouseId);
        }

        public IQueryable<PartsSupplierRelationWithPurchasePrice> 查询配件与供应商关系及采购价(int supplierId, DateTime? time) {
            return new PartsSupplierRelationAch(this).查询配件与供应商关系及采购价(supplierId, time);
        }
    }
}
