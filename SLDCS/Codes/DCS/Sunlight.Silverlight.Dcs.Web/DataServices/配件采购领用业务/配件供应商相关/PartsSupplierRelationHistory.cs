﻿using System;
using System.Linq;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsSupplierRelationHistoryAch : DcsSerivceAchieveBase {
        public PartsSupplierRelationHistoryAch(DcsDomainService domainService)
            : base(domainService) {
        }

        internal void InsertPartsSupplierRelationHistoryValidate(PartsSupplierRelationHistory partsSupplierRelation) {
        }
        public void InsertPartsSupplierRelationHistory(PartsSupplierRelationHistory partsSupplierRelationHistory) {
            var userInfo = Utils.GetCurrentUserInfo();
            partsSupplierRelationHistory.CreatorId = userInfo.Id;
            partsSupplierRelationHistory.CreateTime = DateTime.Now;
            partsSupplierRelationHistory.CreatorName = userInfo.Name;
            InsertToDatabase(partsSupplierRelationHistory);
            this.InsertPartsSupplierRelationHistoryValidate(partsSupplierRelationHistory);
        }
        public IQueryable<PartsSupplierRelationHistory> GetPartsSupplierRelationHistoriesWithDetails() {
            return ObjectContext.PartsSupplierRelationHistories.Include("PartsSupplier").Include("SparePart").Include("Branch").OrderBy(entity => entity.Id);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:33
        //原分布类的函数全部转移到Ach                                                               

        public void InsertPartsSupplierRelationHistory(PartsSupplierRelationHistory partsSupplierRelationHistory) {
            new PartsSupplierRelationHistoryAch(this).InsertPartsSupplierRelationHistory(partsSupplierRelationHistory);
        }

        public IQueryable<PartsSupplierRelationHistory> GetPartsSupplierRelationHistoriesWithDetails() {
            return new PartsSupplierRelationHistoryAch(this).GetPartsSupplierRelationHistoriesWithDetails();
        }
    }
}
