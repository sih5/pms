﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsOuterPurchaseChangeAch : DcsSerivceAchieveBase {
        internal void InsertPartsOuterPurchaseChangeValidate(PartsOuterPurchaseChange partsOuterPurchaseChange) {
            var userInfo = Utils.GetCurrentUserInfo();
            partsOuterPurchaseChange.CreatorId = userInfo.Id;
            partsOuterPurchaseChange.CreatorName = userInfo.Name;
            partsOuterPurchaseChange.CreateTime = DateTime.Now;

            if(string.IsNullOrWhiteSpace(partsOuterPurchaseChange.Code) || partsOuterPurchaseChange.Code == GlobalVar.ASSIGNED_BY_SERVER)
                partsOuterPurchaseChange.Code = CodeGenerator.Generate("PartsOuterPurchaseChange", partsOuterPurchaseChange.CustomerCompanyCode);
        }

        internal void UpdatePartsOuterPurchaseChangeValidate(PartsOuterPurchaseChange partsOuterPurchaseChange) {
            var userInfo = Utils.GetCurrentUserInfo();
            partsOuterPurchaseChange.ModifierId = userInfo.Id;
            partsOuterPurchaseChange.ModifierName = userInfo.Name;
            partsOuterPurchaseChange.ModifyTime = DateTime.Now;
        }

        public void InsertPartsOuterPurchaseChange(PartsOuterPurchaseChange partsOuterPurchaseChange) {
            InsertToDatabase(partsOuterPurchaseChange);
            var partsOuterPurchaselists = ChangeSet.GetAssociatedChanges(partsOuterPurchaseChange, v => v.PartsOuterPurchaselists, ChangeOperation.Insert);
            foreach(PartsOuterPurchaselist partsOuterPurchaselist in partsOuterPurchaselists) {
                InsertToDatabase(partsOuterPurchaselist);
            }
            this.InsertPartsOuterPurchaseChangeValidate(partsOuterPurchaseChange);
        }

        public void UpdatePartsPurchaseOrder(PartsOuterPurchaseChange partsOuterPurchaseChange) {
            partsOuterPurchaseChange.PartsOuterPurchaselists.Clear();
            UpdateToDatabase(partsOuterPurchaseChange);
            var partsOuterPurchaselists = ChangeSet.GetAssociatedChanges(partsOuterPurchaseChange, v => v.PartsOuterPurchaselists);
            foreach(PartsOuterPurchaselist partsOuterPurchaselist in partsOuterPurchaselists) {
                switch(ChangeSet.GetChangeOperation(partsOuterPurchaselist)) {
                    case ChangeOperation.Insert:
                        InsertToDatabase(partsOuterPurchaselist);
                        break;
                    case ChangeOperation.Update:
                        UpdateToDatabase(partsOuterPurchaselist);
                        break;
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(partsOuterPurchaselist);
                        break;
                }
            }
            this.UpdatePartsOuterPurchaseChangeValidate(partsOuterPurchaseChange);
        }


        public PartsOuterPurchaseChange GetPartsOuterPurchaseChangeWithDetailsById(int id) {
            var result = ObjectContext.PartsOuterPurchaseChanges.SingleOrDefault(r => r.Id == id && r.Status != (int)DcsPartsOuterPurchaseChangeStatus.作废);
            if(result != null) {
                var orderDetail = ObjectContext.PartsOuterPurchaselists.Where(r => r.PartsOuterPurchaseChangeId == id).ToArray();
            }
            return result;
        }

        public PartsOuterPurchaseChange GetPartsOuterPurchaseChangeById(int id) {
            return ObjectContext.PartsOuterPurchaseChanges.FirstOrDefault(r => r.Id == id);
        }

        public IQueryable<PartsOuterPurchaseChange> GetPartsOuterPurchaseChangeWithPartsSalesCategory() {
            return ObjectContext.PartsOuterPurchaseChanges.OrderBy(r => r.Id).Include("PartsSalesCategory").Include("Branch");
        }

        public IQueryable<PartsOuterPurchaseChange> GetPartsOuterPurchaseChangeWithPartsSalesCategoryandRelation() {
            var userInfo = Utils.GetCurrentUserInfo();
            var marketDptPersonnelRelationsCount = ObjectContext.MarketDptPersonnelRelations.Count(r => r.PersonnelId == userInfo.Id && r.Status == (int)DcsBaseDataStatus.有效);
            var tempResult = ObjectContext.PartsOuterPurchaseChanges;
            if(marketDptPersonnelRelationsCount > 0) {
                var result = from a in tempResult
                             join b in ObjectContext.DealerServiceInfoes on new {
                                 dealerId = a.CustomerCompanyId,
                                 PartsSalesCategoryrId = a.PartsSalesCategoryrId
                             } equals new {
                                 dealerId = b.DealerId,
                                 PartsSalesCategoryrId = b.PartsSalesCategoryId
                             }
                             join c in ObjectContext.MarketDptPersonnelRelations on b.MarketingDepartmentId equals c.MarketDepartmentId
                             where c.PersonnelId == userInfo.Id && b.Status == (int)DcsBaseDataStatus.有效 && c.Status == (int)DcsBaseDataStatus.有效
                             select a;
                return result.OrderBy(r => r.Id).Include("PartsSalesCategory").Include("Branch");
            } else {
                var result = tempResult;
                return result.OrderBy(r => r.Id).Include("PartsSalesCategory").Include("Branch");
            }
        }

        public IQueryable<PartsOuterPurchaseChange> GetPartsOuterPurchaseChangeWithPartsSalesCategoryandRelationByMarketDepartmentId(int? marketDepartmentId) {
            var userInfo = Utils.GetCurrentUserInfo();
            var company = ObjectContext.Companies.Where(r => r.Id == userInfo.EnterpriseId).First();
            var marketDptPersonnelRelationsCount = ObjectContext.MarketDptPersonnelRelations.Count(r => r.PersonnelId == userInfo.Id && r.Status == (int)DcsBaseDataStatus.有效);
            var result = from a in ObjectContext.PartsOuterPurchaseChanges                         
                         select a;           
            if(company.Type == (int)DcsCompanyType.代理库) {
                result=result.Where(t => ObjectContext.AgencyDealerRelations.Any(y => y.AgencyId == userInfo.EnterpriseId && y.DealerId == t.CustomerCompanyId && y.Status == (int)DcsBaseDataStatus.有效));
            } else if(marketDptPersonnelRelationsCount>0) {
                    result = result.Where(t => ObjectContext.DealerServiceInfoes.Any(y => y.DealerId == t.CustomerCompanyId && ObjectContext.MarketDptPersonnelRelations.Any(p => p.MarketDepartmentId == y.MarketingDepartmentId && p.Status == (int)DcsBaseDataStatus.有效 && p.PersonnelId==userInfo.Id)));          
            }
            if(marketDepartmentId.HasValue) {
                    result = result.Where(t => ObjectContext.DealerServiceInfoes.Any(y => y.DealerId == t.CustomerCompanyId && y.MarketingDepartmentId == marketDepartmentId ));
                }
            return result.Include("PartsSalesCategory").Include("Branch").OrderBy(t=>t.Id);
        }


    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:33
        //原分布类的函数全部转移到Ach                                                               

        public void InsertPartsOuterPurchaseChange(PartsOuterPurchaseChange partsOuterPurchaseChange) {
            new PartsOuterPurchaseChangeAch(this).InsertPartsOuterPurchaseChange(partsOuterPurchaseChange);
        }

        public void UpdatePartsPurchaseOrder(PartsOuterPurchaseChange partsOuterPurchaseChange) {
            new PartsOuterPurchaseChangeAch(this).UpdatePartsPurchaseOrder(partsOuterPurchaseChange);
        }


        public PartsOuterPurchaseChange GetPartsOuterPurchaseChangeWithDetailsById(int id) {
            return new PartsOuterPurchaseChangeAch(this).GetPartsOuterPurchaseChangeWithDetailsById(id);
        }

        public PartsOuterPurchaseChange GetPartsOuterPurchaseChangeById(int id) {
            return new PartsOuterPurchaseChangeAch(this).GetPartsOuterPurchaseChangeById(id);
        }

        public IQueryable<PartsOuterPurchaseChange> GetPartsOuterPurchaseChangeWithPartsSalesCategory() {
            return new PartsOuterPurchaseChangeAch(this).GetPartsOuterPurchaseChangeWithPartsSalesCategory();
        }

        public IQueryable<PartsOuterPurchaseChange> GetPartsOuterPurchaseChangeWithPartsSalesCategoryandRelation() {
            return new PartsOuterPurchaseChangeAch(this).GetPartsOuterPurchaseChangeWithPartsSalesCategoryandRelation();
        }

        public IQueryable<PartsOuterPurchaseChange> GetPartsOuterPurchaseChangeWithPartsSalesCategoryandRelationByMarketDepartmentId(int? marketDepartmentId) {
            return new PartsOuterPurchaseChangeAch(this).GetPartsOuterPurchaseChangeWithPartsSalesCategoryandRelationByMarketDepartmentId(marketDepartmentId);
        }
    }
}
