﻿using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class BranchSupplierRelationAch : DcsSerivceAchieveBase {
        internal void InsertBranchSupplierRelationValidate(BranchSupplierRelation branchSupplierRelation) {
            var dbBranchSupplierRelation = ObjectContext.BranchSupplierRelations.Where(r => r.PartsSalesCategoryId == branchSupplierRelation.PartsSalesCategoryId && r.SupplierId == branchSupplierRelation.SupplierId && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbBranchSupplierRelation != null)
                throw new ValidationException(ErrorStrings.BranchSupplierRelation_Validation1);
            var userinfo = Utils.GetCurrentUserInfo();
            branchSupplierRelation.CreatorId = userinfo.Id;
            branchSupplierRelation.CreatorName = userinfo.Name;
            branchSupplierRelation.CreateTime = System.DateTime.Now;
        }
        internal void UpdateBranchSupplierRelationValidate(BranchSupplierRelation branchSupplierRelation) {
            var dbBranchSupplierRelation = ObjectContext.BranchSupplierRelations.Where(r => r.Id == branchSupplierRelation.Id && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbBranchSupplierRelation == null)
                throw new ValidationException(ErrorStrings.Common_Validation3);
            var checkBranchSupplierRelation = ObjectContext.BranchSupplierRelations.Where(r => r.Id != branchSupplierRelation.Id && r.PartsSalesCategoryId == branchSupplierRelation.PartsSalesCategoryId && r.SupplierId == branchSupplierRelation.SupplierId && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(checkBranchSupplierRelation != null)
                throw new ValidationException(ErrorStrings.BranchSupplierRelation_Validation1);
            var usrinfo = Utils.GetCurrentUserInfo();
            branchSupplierRelation.ModifierId = usrinfo.Id;
            branchSupplierRelation.ModifierName = usrinfo.Name;
            branchSupplierRelation.ModifyTime = System.DateTime.Now;
        }
        public void InsertBranchSupplierRelation(BranchSupplierRelation branchSupplierRelation) {
            InsertToDatabase(branchSupplierRelation);
            this.InsertBranchSupplierRelationValidate(branchSupplierRelation);
        }
        public void UpdateBranchSupplierRelation(BranchSupplierRelation branchSupplierRelation) {
            UpdateToDatabase(branchSupplierRelation);
            this.UpdateBranchSupplierRelationValidate(branchSupplierRelation);
        }
        public IQueryable<BranchSupplierRelation> GetBranchSupplierRelationWithPartsSalesCategoryByPersonelId() {
            var usrinfo = Utils.GetCurrentUserInfo();
            return ObjectContext.BranchSupplierRelations.Include("Branch").Include("PartsSupplier").Include("PartsSalesCategory")/*.Include("PartsManagementCostGrade")*/.Where(r => this.ObjectContext.PersonSalesCenterLinks.Any(x => x.PartsSalesCategoryId == r.PartsSalesCategoryId && x.PersonId == usrinfo.Id && x.Status == (int)DcsBaseDataStatus.有效)).OrderBy(e => e.Id);
        }
        public IQueryable<BranchSupplierRelation> GetBranchSupplierRelationWithPartsSalesCategory() {
            return ObjectContext.BranchSupplierRelations.Include("Branch").Include("PartsSupplier").Include("PartsSalesCategory").Include("PartsManagementCostGrade").Where(r => r.Status == (int)DcsBaseDataStatus.有效).OrderBy(e => e.Id);
        }
        public IQueryable<BranchSupplierRelation> GetBranchSupplierRelationWithPartsSalesCategoryCompany() {
            return ObjectContext.BranchSupplierRelations.Include("Company").Include("PartsSalesCategory").OrderBy(e => e.Id);
        }
        public IQueryable<BranchSupplierRelation> GetBranchSupplierRelationWithPartsSupplier(int partsSalesCategoryId) {
            return ObjectContext.BranchSupplierRelations.Include("PartsSupplier").Include("Branch").Where(e => e.PartsSalesCategoryId == partsSalesCategoryId).OrderBy(e => e.Id);
        }
        public IQueryable<BranchSupplierRelation> GetBranchSupplierRelationBySuppIdAndPartsCategoryId(int partsSalesCategoryId, int autoApproveStrategyId) {
            return this.ObjectContext.BranchSupplierRelations.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效 && ObjectContext.AppointSupplierDtls.Any(x => x.AutoApproveStrategyId == autoApproveStrategyId && x.SupplierId == r.SupplierId));
        }
        public BranchSupplierRelation GetBranchSupplierRelationWithPartsManagementCostRate(int partsSalesCategoryId, int supplierId) {
            var branchSupplierRelation = ObjectContext.BranchSupplierRelations.SingleOrDefault(r => r.SupplierId == supplierId && r.PartsSalesCategoryId == partsSalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效);
            if(branchSupplierRelation != null) {
                var partsManagementCostRate = this.ObjectContext.PartsManagementCostRates.SingleOrDefault(r => r.Status == (int)DcsBaseDataStatus.有效 && r.PartsManagementCostGradeId == branchSupplierRelation.PartsManagementCostGradeId);
                if(partsManagementCostRate != null) {
                    branchSupplierRelation.Rate = partsManagementCostRate.Rate;
                }
                return branchSupplierRelation;
            }
            return null;
        }

        public IQueryable<BranchSupplierRelation> GetSupplierRelationUsrinfoEnterprise() {
            var usrinfo = Utils.GetCurrentUserInfo();
            return ObjectContext.BranchSupplierRelations.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.SupplierId == usrinfo.EnterpriseId).OrderBy(e => e.Id);
        }
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:33
        //原分布类的函数全部转移到Ach                                                               

        public void InsertBranchSupplierRelation(BranchSupplierRelation branchSupplierRelation) {
            new BranchSupplierRelationAch(this).InsertBranchSupplierRelation(branchSupplierRelation);
        }

        public void UpdateBranchSupplierRelation(BranchSupplierRelation branchSupplierRelation) {
            new BranchSupplierRelationAch(this).UpdateBranchSupplierRelation(branchSupplierRelation);
        }
                public IQueryable<BranchSupplierRelation> GetBranchSupplierRelationWithPartsSalesCategoryByPersonelId() {
            return new BranchSupplierRelationAch(this).GetBranchSupplierRelationWithPartsSalesCategoryByPersonelId();
        }

        public IQueryable<BranchSupplierRelation> GetBranchSupplierRelationWithPartsSalesCategory() {
            return new BranchSupplierRelationAch(this).GetBranchSupplierRelationWithPartsSalesCategory();
        }

        public IQueryable<BranchSupplierRelation> GetBranchSupplierRelationWithPartsSalesCategoryCompany() {
            return new BranchSupplierRelationAch(this).GetBranchSupplierRelationWithPartsSalesCategoryCompany();
        }

        public IQueryable<BranchSupplierRelation> GetBranchSupplierRelationWithPartsSupplier(int partsSalesCategoryId) {
            return new BranchSupplierRelationAch(this).GetBranchSupplierRelationWithPartsSupplier(partsSalesCategoryId);
        }

        public IQueryable<BranchSupplierRelation> GetBranchSupplierRelationBySuppIdAndPartsCategoryId(int partsSalesCategoryId, int autoApproveStrategyId) {
            return new BranchSupplierRelationAch(this).GetBranchSupplierRelationBySuppIdAndPartsCategoryId(partsSalesCategoryId,autoApproveStrategyId);
        }

        public BranchSupplierRelation GetBranchSupplierRelationWithPartsManagementCostRate(int partsSalesCategoryId, int supplierId) {
            return new BranchSupplierRelationAch(this).GetBranchSupplierRelationWithPartsManagementCostRate(partsSalesCategoryId,supplierId);
        }

        public IQueryable<BranchSupplierRelation> GetSupplierRelationUsrinfoEnterprise() {
            return new BranchSupplierRelationAch(this).GetSupplierRelationUsrinfoEnterprise();
        }
    }
}
