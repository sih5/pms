﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsPurchaseOrderTypeAch : DcsSerivceAchieveBase {
        internal void InsertPartsPurchaseOrderTypeValidate(PartsPurchaseOrderType partsPurchaseOrderType) {
            if(string.IsNullOrWhiteSpace(partsPurchaseOrderType.Code) || partsPurchaseOrderType.Code == GlobalVar.ASSIGNED_BY_SERVER)
                partsPurchaseOrderType.Code = CodeGenerator.Generate("PartsPurchaseOrderType");
            var dbPartsPurchaseOrderType = ObjectContext.PartsPurchaseOrderTypes.Where(r => r.Code == partsPurchaseOrderType.Code && r.PartsSalesCategoryId == partsPurchaseOrderType.PartsSalesCategoryId && r.Status != (int)DcsBaseDataStatus.作废).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbPartsPurchaseOrderType != null)
                throw new ValidationException(ErrorStrings.PartsPurchaseOrderType_Validation1);
            var userInfo = Utils.GetCurrentUserInfo();
            partsPurchaseOrderType.BranchId = userInfo.EnterpriseId;
            partsPurchaseOrderType.CreatorId = userInfo.Id;
            partsPurchaseOrderType.CreatorName = userInfo.Name;
            partsPurchaseOrderType.CreateTime = DateTime.Now;
        }
        internal void UpdatePartsPurchaseOrderTypeValidate(PartsPurchaseOrderType partsPurchaseOrderType) {
            var dbPartsPurchaseOrderType = ObjectContext.PartsPurchaseOrderTypes.Where(r => r.Id != partsPurchaseOrderType.Id && r.Code == partsPurchaseOrderType.Code && r.PartsSalesCategoryId == partsPurchaseOrderType.PartsSalesCategoryId && r.Status != (int)DcsBaseDataStatus.作废).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbPartsPurchaseOrderType != null)
                throw new ValidationException(ErrorStrings.PartsPurchaseOrderType_Validation1);
            var userInfo = Utils.GetCurrentUserInfo();
            partsPurchaseOrderType.ModifierId = userInfo.Id;
            partsPurchaseOrderType.ModifierName = userInfo.Name;
            partsPurchaseOrderType.ModifyTime = DateTime.Now;
        }
        public void InsertPartsPurchaseOrderType(PartsPurchaseOrderType partsPurchaseOrderType) {
            InsertToDatabase(partsPurchaseOrderType);
            this.InsertPartsPurchaseOrderTypeValidate(partsPurchaseOrderType);
        }
        public void UpdatePartsPurchaseOrderType(PartsPurchaseOrderType partsPurchaseOrderType) {
            UpdateToDatabase(partsPurchaseOrderType);
            this.UpdatePartsPurchaseOrderTypeValidate(partsPurchaseOrderType);
        }
        public IQueryable<PartsPurchaseOrderType> GetPartsPurchaseOrderTypesByWarehouseId(int warehouseId) {
            var salesUnitAffiWarehouse = ObjectContext.SalesUnitAffiWarehouses.SingleOrDefault(r => r.WarehouseId == warehouseId);
            if(salesUnitAffiWarehouse == null)
                return null;
            return ObjectContext.PartsPurchaseOrderTypes.Where(r => ObjectContext.SalesUnits.Any(v => v.Id == salesUnitAffiWarehouse.SalesUnitId && r.PartsSalesCategoryId == v.PartsSalesCategoryId && v.Status == (int)DcsBaseDataStatus.有效) && r.Status == (int)DcsBaseDataStatus.有效);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               

        public void InsertPartsPurchaseOrderType(PartsPurchaseOrderType partsPurchaseOrderType) {
            new PartsPurchaseOrderTypeAch(this).InsertPartsPurchaseOrderType(partsPurchaseOrderType);
        }

        public void UpdatePartsPurchaseOrderType(PartsPurchaseOrderType partsPurchaseOrderType) {
            new PartsPurchaseOrderTypeAch(this).UpdatePartsPurchaseOrderType(partsPurchaseOrderType);
        }

        public IQueryable<PartsPurchaseOrderType> GetPartsPurchaseOrderTypesByWarehouseId(int warehouseId) {
            return new PartsPurchaseOrderTypeAch(this).GetPartsPurchaseOrderTypesByWarehouseId(warehouseId);
        }
    }
}
