﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
using System.Collections.Generic;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsPurchasePricingAch : DcsSerivceAchieveBase {
        public PartsPurchasePricingAch(DcsDomainService domainService)
            : base(domainService) {
        }

        internal void InsertPartsPurchasePricingValidate(PartsPurchasePricing partsPurchasePricing) {
            //var dbPartsPurchasePricing = ObjectContext.PartsPurchasePricings.Where(r => r.PartId == partsPurchasePricing.PartId && r.PartsSupplierId == partsPurchasePricing.PartsSupplierId && r.PartsSalesCategoryId == partsPurchasePricing.PartsSalesCategoryId && r.Status != (int)DcsPartsPurchasePricingStatus.作废 && r.ValidFrom == partsPurchasePricing.ValidFrom && r.ValidTo == partsPurchasePricing.ValidTo).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            //if(dbPartsPurchasePricing != null) {
            //    var sparepartsCode = ObjectContext.SpareParts.FirstOrDefault(r => r.Id == partsPurchasePricing.PartId && r.Status != (int)DcsBaseDataStatus.作废).Code;
            //    throw new ValidationException(string.Format(ErrorStrings.PartsPurchasePricing_Validation1, sparepartsCode));
            //}
            var userInfo = Utils.GetCurrentUserInfo();
            partsPurchasePricing.CreatorId = userInfo.Id;
            partsPurchasePricing.CreatorName = userInfo.Name;
            partsPurchasePricing.CreateTime = DateTime.Now;
        }

        internal void UpdatePartsPurchasePricingValidate(PartsPurchasePricing partsPurchasePricing) {
            var isPartsPurchasePricing = ObjectContext.PartsPurchasePricings.Where(r => r.Id == partsPurchasePricing.Id && r.Status == (int)DcsPartsPurchasePricingStatus.新增);
            if(isPartsPurchasePricing == null)
                throw new ValidationException(ErrorStrings.PartsPurchasePricing_Validation2);
            var userInfo = Utils.GetCurrentUserInfo();
            partsPurchasePricing.ModifierId = userInfo.Id;
            partsPurchasePricing.ModifierName = userInfo.Name;
            partsPurchasePricing.ModifyTime = DateTime.Now;
        }

        public void InsertPartsPurchasePricing(PartsPurchasePricing partsPurchasePricing) {
            InsertToDatabase(partsPurchasePricing);
            this.InsertPartsPurchasePricingValidate(partsPurchasePricing);
        }

        public void UpdatePartsPurchasePricing(PartsPurchasePricing partsPurchasePricing) {
            UpdateToDatabase(partsPurchasePricing);
            this.UpdatePartsPurchasePricingValidate(partsPurchasePricing);
        }


        public IEnumerable<VirtualPartsPurchasePricing> 查询配件采购价格2(DateTime? availibleTime, string partCode, string partName, string supplierCode, string supplierName, bool? isOrderable, bool? isSalable, int? partsSalesCategoryId,string referenceCode,string supplierPartCode)
        {

            string SQL = @"select ppp.Id,ppp.BranchId,b.name as BranchName,ppp.PartId,sp.code as PartCode,sp.Name as PartName,sp.ReferenceCode,ppp.PartsSupplierId,ps.code as PartsSupplierCode,ps.name as PartsSupplierName,
                            ppp.PartsSalesCategoryId,ppp.PartsSalesCategoryName,ppp.ValidFrom,ppp.Validto,ppp.DataSource,
                            ppp.PriceType,ppp.PurchasePrice,ppp.Status,ppp.CreatorId,ppp.CreatorName,ppp.CreateTime,ppp.ModifierId,ppp.ModifierName,ppp.ModifyTime,
                            ppp.AbandonerId,ppp.AbandonerName,ppp.AbandonTime,ppp.Remark,ppp.OverseasPartsFigure,ppp.LimitQty,ppp.UsedQty,psr.IsPrimary,psr.SupplierPartCode 
                             from PartsPurchasePricing ppp
                             inner join sparepart sp on sp.id = ppp.partid
                             inner join branch b on b.id = ppp.branchid
                             inner join PartsSupplier ps on ps.id = ppp.PartsSupplierId
                             left join PartsSupplierRelation psr on psr.partid = ppp.partid and psr.supplierid = ppp.PartsSupplierId and psr.status <> 99 and psr.partssalescategoryid = ppp.partssalescategoryid
                             left join partsbranch pb on pb.PartId = ppp.PartId and pb.PartsSalesCategoryId = ppp.PartsSalesCategoryId and pb.status <>99                              
                            where ppp.status <>99  ";
            SQL = SQL + " and ppp.branchid =" + Utils.GetCurrentUserInfo().EnterpriseId;

            if (!string.IsNullOrEmpty(partCode)) {
                var spareparts = partCode.Split(',');
                if (spareparts.Length == 1) {
                    var sparepartcode = spareparts[0];
                    SQL = SQL + " and sp.Code like '%" + sparepartcode + "%'";
                } else {
                    for (int i = 0; i < spareparts.Length; i++) {
                        spareparts[i] = "'" + spareparts[i] + "'";
                    }
                    string codes = string.Join(",", spareparts);
                    SQL = SQL + " and sp.Code in (" + codes + ")";
                }
            }
            if (!string.IsNullOrEmpty(partName))
            {
                SQL = SQL + " and sp.Name like '%" + partName + "%'";
            }
            if (!string.IsNullOrEmpty(supplierCode))
            {
                SQL = SQL + " and ps.Code like '%" + supplierCode + "%'";
            }
            if (!string.IsNullOrEmpty(supplierName))
            {
                SQL = SQL + " and ps.Name like '%" + supplierName + "%'";
            }
            if (!string.IsNullOrEmpty(referenceCode))
            {
                SQL = SQL + " and sp.ReferenceCode like '%" + referenceCode + "%'";
            }
            if (!string.IsNullOrEmpty(supplierPartCode))
            {
                SQL = SQL + " and psr.SupplierPartCode like '%" + supplierPartCode + "%'";
            }
            if (partsSalesCategoryId.HasValue)
            {
                SQL = SQL + " and ppp.partsSalesCategoryId =" + partsSalesCategoryId + "";
            }
            if (isOrderable.HasValue)
            {
                int a = isOrderable == true ? 1 : 0;
                SQL = SQL + " and pb.isOrderable=" + a;
            }
            if (isSalable.HasValue)
            {
                int b = isSalable == true ? 1 : 0;
                SQL = SQL + " and pb.isSalable=" + b;
            }
            if (availibleTime.HasValue)
            {
                SQL = SQL + " and ppp.ValidFrom <= to_date('" + availibleTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
                SQL = SQL + " and ppp.ValidTo >= to_date('" + availibleTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }

            var search = ObjectContext.ExecuteStoreQuery<VirtualPartsPurchasePricing>(SQL).ToList();

            return search;
        }


        [Query(HasSideEffects = true)]
        public IQueryable<PartsPurchasePricing> 查询配件采购价格(DateTime? availibleTime, int? partsSupplierId, int[] partsIds, int? brachId, bool? isOrderable, bool? isSalable, int? partsSalesCategoryId) {

            var partsPurchasePricings = ObjectContext.PartsPurchasePricings.Where(r => r.ValidFrom <= availibleTime && r.ValidTo >= availibleTime && r.Status != (int)DcsPartsPurchasePricingStatus.作废);
            
            if(partsIds != null && partsIds.Length > 0) {
                partsPurchasePricings = partsPurchasePricings.Where(r => partsIds.Contains(r.PartId));

            }
            if(partsSalesCategoryId.HasValue) {
                partsPurchasePricings = partsPurchasePricings.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId.Value);

            }
            if(brachId.HasValue) {
                partsPurchasePricings = partsPurchasePricings.Where(r => r.BranchId == brachId.Value);

            }
            if(partsSupplierId.HasValue) {
                partsPurchasePricings = partsPurchasePricings.Where(r => r.PartsSupplierId == partsSupplierId.Value);
            }
            if(isOrderable.HasValue) {
                partsPurchasePricings = partsPurchasePricings.Where(r => ObjectContext.PartsBranches.Any(v => v.BranchId == r.BranchId && r.PartsSalesCategoryId == v.PartsSalesCategoryId && r.PartId == v.PartId && v.IsOrderable == isOrderable.Value));

            }

            if(isSalable.HasValue) {
                partsPurchasePricings = partsPurchasePricings.Where(r => ObjectContext.PartsBranches.Any(v => v.BranchId == r.BranchId && r.PartsSalesCategoryId == v.PartsSalesCategoryId && r.PartId == v.PartId && v.IsSalable == isSalable.Value));

            }
            return partsPurchasePricings.Include("SparePart").Include("Branch").Include("PartsSupplier").Include("PartsSalesCategory").OrderBy(r => r.Id);
        }


        [Query(HasSideEffects = true)]
        public IQueryable<GetAllPartsPurchasePricing> 查询配件采购价格品牌(DateTime? availibleTime, string partsSalesCategoryName, string partCode) {

            IQueryable<GetAllPartsPurchasePricing> partsPurchasePricings = ObjectContext.GetAllPartsPurchasePricings;
            if(availibleTime.ToString() != "0001/1/1 0:00:00" && !string.IsNullOrEmpty(availibleTime.ToString())) {
                partsPurchasePricings = partsPurchasePricings.Where(r => r.ValidFrom <= availibleTime && r.ValidTo >= availibleTime && r.Status != (int)DcsPartsPurchasePricingStatus.作废);
            }
            if(!string.IsNullOrEmpty(partsSalesCategoryName)) {
                partsPurchasePricings = partsPurchasePricings.Where(r => r.PartsSalesCategoryName == partsSalesCategoryName);
            }
            return partsPurchasePricings.OrderBy(r => r.PartCode);
        }

        [Query(HasSideEffects = true)]
        public IQueryable<PartsPurchasePricing> 查询统购分销配件采购价格(int[] partsIds, int partsSalesCategoryId, int partsSupplierId, int brachId) {
            var partsPurchasePricings = ObjectContext.PartsPurchasePricings.Where(r => r.ValidFrom <= DateTime.Now && r.ValidTo > DateTime.Now && r.Status != (int)DcsPartsPurchasePricingStatus.作废 && r.PartsSalesCategoryId == partsSalesCategoryId && r.PartsSupplierId == partsSupplierId && r.BranchId == brachId && r.PriceType == (int)DcsPurchasePriceType.正式价格);
            if(partsIds != null && partsIds.Length > 0) {
                partsPurchasePricings = partsPurchasePricings.Where(r => partsIds.Contains(r.PartId));
            }
            return partsPurchasePricings.OrderBy(r => r.Id);
        }

        public IQueryable<PartsPurchasePricing> GetPartsPurchasePricingByPartId(int partId) {
            return ObjectContext.PartsPurchasePricings.Where(r => r.PartId == partId);
        }

        public IQueryable<PartsPurchasePricing> GetPrimaryPriceByPartId(int partId) {
            var result = from a in ObjectContext.PartsPurchasePricings.Where(r => r.PartId == partId && r.Status == (int)DcsPartsPurchasePricingStatus.生效 && r.ValidFrom <= DateTime.Now && r.ValidTo > DateTime.Now)
                         join b in ObjectContext.PartsSupplierRelations.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.IsPrimary == true) on new {
                             a.PartId,
                             a.PartsSupplierId
                         } equals new {
                             PartId = b.PartId,
                             PartsSupplierId = b.SupplierId
                         }
                         select a;
            return result;
        }

        public IQueryable<PartsPurchasePricing> GetPartsPurchasePricingByPartIds(int[] partIds) {
            return ObjectContext.PartsPurchasePricings.Where(r => r.ValidFrom <= DateTime.Now && r.ValidTo > DateTime.Now && partIds.Contains(r.PartId) && r.Status == (int)DcsPartsPurchasePricingStatus.生效);
        }
        public IQueryable<SparePart> GetSparePartWithPartsPurchasePricing() {
            return ObjectContext.SpareParts.Include("PartsPurchasePricings").Include("PartsPurchasePricings.PartsSupplier").Where(r => r.PartsPurchasePricings.Any(o => o.CreateTime >= r.CreateTime) && r.Status == (int)DcsBaseDataStatus.有效);
        }

        public IEnumerable<PartsPurchasePricingForQuery> getPartsPurchasePricingLists(string code, string name, DateTime? bCreateTime, DateTime? eCreateTime, int? isprice,DateTime? bPartCreateTime, DateTime? ePartCreateTime,string referenceCode)
        {
            string SQL = @"select * from (select a.id,a.code,a.referenceCode,a.name,d.code as companyCode,d.name as companyName,c.purchaseprice,c.createtime,case when c.id is null then 0 else 1 end as isprice,b.isprimary , c.ValidTo,c.CreatorName,c.ModifierName,c.ModifyTime,a.createtime as partcreatetime
                          from sparepart a join partsbranch pb on a.id =PartId
                          left join partssupplierrelation b on a.id = b.partid and b.status = 1 and b.isprimary=1
                          left join partspurchasepricing c on a.id = c.partid and c.createtime>=a.createtime and c.partssupplierid = b.supplierid and c.validfrom<=sysdate and c.validto>sysdate
                          left join company d on d.id = b.supplierid where pb.IsOrderable=1 ";

            if (!string.IsNullOrEmpty(code)) {
                var spareparts = code.Split(',');
                if (spareparts.Length == 1) {
                    var sparepartcode = spareparts[0];
                    SQL += " and a.code like '%" + sparepartcode + "%'";
                } else {
                    for (int i = 0; i < spareparts.Length; i++) {
                        spareparts[i] = "'" + spareparts[i] + "'";
                    }
                    string codes = string.Join(",", spareparts);
                    SQL += " and a.code in (" + codes + ")";
                }
            }
            if (!string.IsNullOrEmpty(name))
            {
                SQL = SQL + " and a.name like '%" + name + "%'";
            }
            if (!string.IsNullOrEmpty(referenceCode))
            {
                SQL = SQL + " and a.referenceCode like '%" + referenceCode + "%'";
            }
            if (bPartCreateTime.HasValue)
            {
                SQL = SQL + " and a.CreateTime>= to_date('" + bPartCreateTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if (ePartCreateTime.HasValue)
            {
                SQL = SQL + " and a.CreateTime<= to_date('" + ePartCreateTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if (bCreateTime.HasValue)
            {
                SQL = SQL + " and c.CreateTime>= to_date('" + bCreateTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if (eCreateTime.HasValue)
            {
                SQL = SQL + " and c.CreateTime<= to_date('" + eCreateTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            SQL = SQL + ")p";
            if (isprice.HasValue)
            {
                SQL = SQL + " where p.isprice=" + isprice.Value;
            }
            var search = ObjectContext.ExecuteStoreQuery<PartsPurchasePricingForQuery>(SQL).ToList();
            return search;
        }
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               

        public void InsertPartsPurchasePricing(PartsPurchasePricing partsPurchasePricing) {
            new PartsPurchasePricingAch(this).InsertPartsPurchasePricing(partsPurchasePricing);
        }

        public void UpdatePartsPurchasePricing(PartsPurchasePricing partsPurchasePricing) {
            new PartsPurchasePricingAch(this).UpdatePartsPurchasePricing(partsPurchasePricing);
        }


        [Query(HasSideEffects = true)]
        public IQueryable<PartsPurchasePricing> 查询配件采购价格(DateTime? availibleTime, int? partsSupplierId, int[] partsIds, int? brachId, bool? isOrderable, bool? isSalable, int? partsSalesCategoryId) {
            return new PartsPurchasePricingAch(this).查询配件采购价格(availibleTime, partsSupplierId, partsIds, brachId, isOrderable, isSalable, partsSalesCategoryId);
        }

        [Query(HasSideEffects = true)]
        public IEnumerable<VirtualPartsPurchasePricing> 查询配件采购价格2(DateTime? availibleTime, string partCode, string partName, string supplierCode, string supplierName, bool? isOrderable, bool? isSalable, int? partsSalesCategoryId,string referenceCode,string supplierPartCode)
        {
            return new PartsPurchasePricingAch(this).查询配件采购价格2(availibleTime, partCode, partName, supplierCode, supplierName, isOrderable, isSalable, partsSalesCategoryId,referenceCode,supplierPartCode);
        }

        [Query(HasSideEffects = true)]
        public IQueryable<GetAllPartsPurchasePricing> 查询配件采购价格品牌(DateTime? availibleTime, string partsSalesCategoryName, string partCode) {
            return new PartsPurchasePricingAch(this).查询配件采购价格品牌(availibleTime, partsSalesCategoryName, partCode);
        }


        [Query(HasSideEffects = true)]
        public IQueryable<PartsPurchasePricing> 查询统购分销配件采购价格(int[] partsIds, int partsSalesCategoryId, int partsSupplierId, int brachId) {
            return new PartsPurchasePricingAch(this).查询统购分销配件采购价格(partsIds, partsSalesCategoryId, partsSupplierId, brachId);
        }

        public IQueryable<PartsPurchasePricing> GetPartsPurchasePricingByPartId(int partId) {
            return new PartsPurchasePricingAch(this).GetPartsPurchasePricingByPartId(partId);
        }
        public IQueryable<PartsPurchasePricing> GetPrimaryPriceByPartId(int partId) { 
            return new PartsPurchasePricingAch(this).GetPrimaryPriceByPartId(partId);
        }
        [Query(HasSideEffects = true)]
        public IQueryable<PartsPurchasePricing> GetPartsPurchasePricingByPartIds(int[] partIds) {
            return new PartsPurchasePricingAch(this).GetPartsPurchasePricingByPartIds(partIds);
        }

        [Query(HasSideEffects = true)]
        public IQueryable<SparePart> GetSparePartWithPartsPurchasePricing() {
            return new PartsPurchasePricingAch(this).GetSparePartWithPartsPurchasePricing();
        }
        [Query(HasSideEffects = true)]
        public IEnumerable<PartsPurchasePricingForQuery> getPartsPurchasePricingLists(string code, string name, DateTime? bCreateTime, DateTime? eCreateTime, int? isprice,DateTime? bPartCreateTime, DateTime? ePartCreateTime,string referenceCode)
        {
            return new PartsPurchasePricingAch(this).getPartsPurchasePricingLists(code, name, bCreateTime, eCreateTime, isprice,bPartCreateTime,ePartCreateTime,referenceCode);
        }
    }
}
