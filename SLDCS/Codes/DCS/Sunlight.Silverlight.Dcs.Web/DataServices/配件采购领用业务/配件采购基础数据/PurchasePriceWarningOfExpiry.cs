﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunlight.Silverlight.Dcs.Web.Entities;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PurchasePriceWarningOfExpiryAch : DcsSerivceAchieveBase {
        public PurchasePriceWarningOfExpiryAch(DcsDomainService domainService)
            : base(domainService) {
        }
        public IEnumerable<PurchasePriceWarningOfExpiry> 获取采购价格有效到期提醒(int? daysRemaining, string sparepartCode, string supplierPartCode, string partsSupplierCode, string partsSupplierName, int? priceType) {
            string SQL = @"select Rownum, tt.*
                          from (select sp.code as sparepartCode,
                                       sp.name as sparepartName,
                                       pr.code as PartsSupplierCode,
                                       pr.name as PartsSupplierName,
                                       b.IsPrimary,
                                       a.PriceType,
                                       a.PurchasePrice,
                                       a.ValidFrom,
                                       a.ValidTo,
                                       (trunc(a.validto) - trunc(sysdate)) as DaysRemaining,
                                       b.SupplierPartCode
                                  from partspurchasepricing a
                                 inner join partssupplierrelation b
                                    on a.partid = b.partid
                                   and a.partssupplierid = b.supplierid
                                  join sparepart sp
                                    on a.partid = sp.id
                                  join PartsSupplier pr
                                    on a.partssupplierid = pr.id
                                 where  trunc(a.validfrom) <= trunc(sysdate)
                                   and trunc(a.validto) >= trunc(sysdate)
                                   and (trunc(a.validto) - trunc(sysdate))<=30
                                   and not exists
                                 (select 1
                                          from partspurchasepricing tmp
                                         where trunc(tmp.validfrom) = trunc(a.validto) + 1
                                           and trunc(tmp.validfrom) > trunc(sysdate))
                                union all
                                select sp.code as sparepartCode,
                                       sp.name as sparepartName,
                                       pr.code as PartsSupplierCode,
                                       pr.name as PartsSupplierName,
                                       b.IsPrimary,
                                       a.PriceType,
                                       a.PurchasePrice,
                                       a.ValidFrom,
                                       a.ValidTo,
                                       (trunc(a.validto) - trunc(sysdate)) as DaysRemaining,
                                       b.SupplierPartCode
                                  from partspurchasepricing a
                                 inner join partssupplierrelation b
                                    on a.partid = b.partid
                                   and a.partssupplierid = b.supplierid
                                  join sparepart sp
                                    on a.partid = sp.id
                                  join PartsSupplier pr
                                    on a.partssupplierid = pr.id
                                 where trunc(a.validfrom) > trunc(sysdate)
                                   and trunc(a.validto) > trunc(sysdate)
                                   and (trunc(a.validto) - trunc(sysdate))<=30) tt where 1=1 ";

            if(!string.IsNullOrEmpty(supplierPartCode)) {
                SQL = SQL + " and LOWER(tt.supplierPartCode) like '%" + supplierPartCode.ToLower() + "%'";
            }
            if(!string.IsNullOrEmpty(sparepartCode)) {
                SQL = SQL + " and LOWER(tt.sparepartCode) like '%" + sparepartCode.ToLower() + "%'";
            }
            if(!string.IsNullOrEmpty(partsSupplierName)) {
                SQL = SQL + " and LOWER(tt.partsSupplierName) like '%" + partsSupplierName.ToLower() + "%'";
            }
            if(daysRemaining.HasValue) {
                SQL = SQL + " and tt.daysRemaining=" + daysRemaining.Value;
            }
            if(!string.IsNullOrEmpty(partsSupplierName)) {
                SQL = SQL + " and LOWER(tt.partsSupplierName) like '%" + partsSupplierName.ToLower() + "%'";
            }
            if(priceType.HasValue) {
                SQL = SQL + " and tt.priceType=" + priceType.Value;
            }
            var search = ObjectContext.ExecuteStoreQuery<PurchasePriceWarningOfExpiry>(SQL).ToList();
            return search;
        }
    }
    partial class DcsDomainService {
        public IEnumerable<PurchasePriceWarningOfExpiry> 获取采购价格有效到期提醒(int? daysRemaining, string sparepartCode, string supplierPartCode, string partsSupplierCode, string partsSupplierName, int? priceType) {
            return new PurchasePriceWarningOfExpiryAch(this).获取采购价格有效到期提醒(daysRemaining, sparepartCode, supplierPartCode, partsSupplierCode, partsSupplierName, priceType);
        }
    }
}
