﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsPurReturnOrderAch : DcsSerivceAchieveBase {
        internal void InsertPartsPurReturnOrderValidate(PartsPurReturnOrder partsPurReturnOrder) {
            var userInfo = Utils.GetCurrentUserInfo();
            partsPurReturnOrder.CreatorId = userInfo.Id;
            partsPurReturnOrder.CreatorName = userInfo.Name;
            partsPurReturnOrder.CreateTime = DateTime.Now;
            if(string.IsNullOrWhiteSpace(partsPurReturnOrder.Code) || partsPurReturnOrder.Code == GlobalVar.ASSIGNED_BY_SERVER)
                partsPurReturnOrder.Code = CodeGenerator.Generate("PartsPurReturnOrder", partsPurReturnOrder.BranchCode);
            var branchstrategie = ObjectContext.Branchstrategies.FirstOrDefault(r => r.BranchId == partsPurReturnOrder.BranchId && r.Status == (int)DcsBaseDataStatus.有效);
            if(branchstrategie == null) {
                throw new ValidationException(ErrorStrings.PartsSalesOrderProcess_Validation43);
            }
            var maxInvoiceAmount = branchstrategie.MaxInvoiceAmount;
            var popdSumAmount = partsPurReturnOrder.PartsPurReturnOrderDetails.Sum(r => r.UnitPrice * r.Quantity);
            if(popdSumAmount > maxInvoiceAmount) {
                throw new ValidationException(ErrorStrings.PartsPurReturnOrder_Validation16);
            }
        }
        internal void UpdatePartsPurReturnOrderValidate(PartsPurReturnOrder partsPurReturnOrder) {
            var userInfo = Utils.GetCurrentUserInfo();
            partsPurReturnOrder.ModifierId = userInfo.Id;
            partsPurReturnOrder.ModifierName = userInfo.Name;
            partsPurReturnOrder.ModifyTime = DateTime.Now;
        }

        public void InsertPartsPurReturnOrder(PartsPurReturnOrder partsPurReturnOrder) {
            InsertToDatabase(partsPurReturnOrder);
            var partsPurReturnOrderDetails = ChangeSet.GetAssociatedChanges(partsPurReturnOrder, v => v.PartsPurReturnOrderDetails, ChangeOperation.Insert);
            foreach(PartsPurReturnOrderDetail partsPurReturnOrderDetail in partsPurReturnOrderDetails) {
                InsertToDatabase(partsPurReturnOrderDetail);
            }
            this.InsertPartsPurReturnOrderValidate(partsPurReturnOrder);
        }
        public void UpdatePartsPurReturnOrder(PartsPurReturnOrder partsPurReturnOrder) {
            partsPurReturnOrder.PartsPurReturnOrderDetails.Clear();
            UpdateToDatabase(partsPurReturnOrder);
            var partsPurReturnOrderDetails = ChangeSet.GetAssociatedChanges(partsPurReturnOrder, v => v.PartsPurReturnOrderDetails);
            foreach(PartsPurReturnOrderDetail partsPurReturnOrderDetail in partsPurReturnOrderDetails) {
                switch(ChangeSet.GetChangeOperation(partsPurReturnOrderDetail)) {
                    case ChangeOperation.Insert:
                        InsertToDatabase(partsPurReturnOrderDetail);
                        break;
                    case ChangeOperation.Update:
                        UpdateToDatabase(partsPurReturnOrderDetail);
                        break;
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(partsPurReturnOrderDetail);
                        break;
                }
            }
            this.UpdatePartsPurReturnOrderValidate(partsPurReturnOrder);
        }
        public PartsPurReturnOrder GetPartsPurReturnOrderWithDetailsById(int id) {
            var partsPurReturnOrder = ObjectContext.PartsPurReturnOrders.SingleOrDefault(ex => ex.Id == id);
            if(partsPurReturnOrder != null) {
                var details = ObjectContext.PartsPurReturnOrderDetails.Where(ex => ex.PartsPurReturnOrderId == id).ToArray();
                foreach(var detail in details)
                    detail.CheckUnitPrice = detail.UnitPrice;
            }
            return partsPurReturnOrder;
        }
        public IQueryable<PartsPurReturnOrder> GetPartsPurReturnOrderWithDetails() {
            return ObjectContext.PartsPurReturnOrders.Include("PartsPurReturnOrderDetails").OrderBy(entity => entity.Id);
        }
        /// <summary>
        /// 查询采购退货清单出库数量
        /// </summary>
        /// <returns></returns>
        public IQueryable<VirtualPartsPurReturnOrderDetail> GetVirtualPartsPurReturnOrderDetail() {
            var result = from pprod in this.ObjectContext.PartsPurReturnOrderDetails
                         join pop in this.ObjectContext.PartsOutboundPlans on pprod.PartsPurReturnOrderId equals pop.SourceId
                         join popd in this.ObjectContext.PartsOutboundPlanDetails on pop.Id equals popd.PartsOutboundPlanId
                         where pprod.SparePartId == popd.SparePartId
                         select new VirtualPartsPurReturnOrderDetail {
                             Id = pprod.Id,
                             SparePartCode = pprod.SparePartCode,
                             SparePartId = pprod.SparePartId,
                             SparePartName = pprod.SparePartName,
                             Quantity = pprod.Quantity,
                             PartsPurReturnOrderId = pprod.PartsPurReturnOrderId,
                             OutboundFulfillment = popd.OutboundFulfillment
                         };
            return result.OrderBy(r => r.Id);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               

        public void InsertPartsPurReturnOrder(PartsPurReturnOrder partsPurReturnOrder) {
            new PartsPurReturnOrderAch(this).InsertPartsPurReturnOrder(partsPurReturnOrder);
        }

        public void UpdatePartsPurReturnOrder(PartsPurReturnOrder partsPurReturnOrder) {
            new PartsPurReturnOrderAch(this).UpdatePartsPurReturnOrder(partsPurReturnOrder);
        }

        public PartsPurReturnOrder GetPartsPurReturnOrderWithDetailsById(int id) {
            return new PartsPurReturnOrderAch(this).GetPartsPurReturnOrderWithDetailsById(id);
        }

        public IQueryable<PartsPurReturnOrder> GetPartsPurReturnOrderWithDetails() {
            return new PartsPurReturnOrderAch(this).GetPartsPurReturnOrderWithDetails();
        }
        /// <summary>
        /// 查询采购退货清单出库数量
        /// </summary>
        /// <returns></returns>
        public IQueryable<VirtualPartsPurReturnOrderDetail> GetVirtualPartsPurReturnOrderDetail() {
            return new PartsPurReturnOrderAch(this).GetVirtualPartsPurReturnOrderDetail();
        }
    }
}
