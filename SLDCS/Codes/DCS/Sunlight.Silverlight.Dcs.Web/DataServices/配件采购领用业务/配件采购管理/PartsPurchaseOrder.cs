﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Dcs.Web.Entities;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsPurchaseOrderAch : DcsSerivceAchieveBase {
        internal void InsertPartsPurchaseOrderValidate(PartsPurchaseOrder partsPurchaseOrder) {
            var userInfo = Utils.GetCurrentUserInfo();
            partsPurchaseOrder.CreatorId = userInfo.Id;
            partsPurchaseOrder.CreatorName = userInfo.Name;
            partsPurchaseOrder.CreateTime = DateTime.Now;

            if(string.IsNullOrWhiteSpace(partsPurchaseOrder.Code) || partsPurchaseOrder.Code == GlobalVar.ASSIGNED_BY_SERVER)
                partsPurchaseOrder.Code = CodeGenerator.Generate("PartsPurchaseOrder", partsPurchaseOrder.BranchCode);
        }

        internal void UpdatePartsPurchaseOrderValidate(PartsPurchaseOrder partsPurchaseOrder) {
            var userInfo = Utils.GetCurrentUserInfo();
            partsPurchaseOrder.ModifierId = userInfo.Id;
            partsPurchaseOrder.ModifierName = userInfo.Name;
            partsPurchaseOrder.ModifyTime = DateTime.Now;
        }

        public void InsertPartsPurchaseOrder(PartsPurchaseOrder partsPurchaseOrder) {
            InsertToDatabase(partsPurchaseOrder);
            var partsPurchaseOrderDetails = ChangeSet.GetAssociatedChanges(partsPurchaseOrder, v => v.PartsPurchaseOrderDetails, ChangeOperation.Insert);
            foreach(PartsPurchaseOrderDetail partsPurchaseOrderDetail in partsPurchaseOrderDetails) {
                InsertToDatabase(partsPurchaseOrderDetail);
            }
            this.InsertPartsPurchaseOrderValidate(partsPurchaseOrder);
        }

        public void UpdatePartsPurchaseOrder(PartsPurchaseOrder partsPurchaseOrder) {
            partsPurchaseOrder.PartsPurchaseOrderDetails.Clear();
            UpdateToDatabase(partsPurchaseOrder);
            var partsPurchaseOrderDetails = ChangeSet.GetAssociatedChanges(partsPurchaseOrder, v => v.PartsPurchaseOrderDetails);
            foreach(PartsPurchaseOrderDetail partsPurchaseOrderDetail in partsPurchaseOrderDetails) {
                switch(ChangeSet.GetChangeOperation(partsPurchaseOrderDetail)) {
                    case ChangeOperation.Insert:
                        InsertToDatabase(partsPurchaseOrderDetail);
                        break;
                    case ChangeOperation.Update:
                        UpdateToDatabase(partsPurchaseOrderDetail);
                        break;
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(partsPurchaseOrderDetail);
                        break;
                }
            }
            this.UpdatePartsPurchaseOrderValidate(partsPurchaseOrder);
        }

        public PartsPurchaseOrder GetPartsPurchaseOrdersWithDetailsById(int id) {
            var dbPartsPurchaseOrder = ObjectContext.PartsPurchaseOrders.Include("PartsPurchaseOrderType").SingleOrDefault(entity => entity.Id == id && entity.Status != (int)DcsPartsPurchaseOrderStatus.作废);
            if(dbPartsPurchaseOrder != null) {
                var dbDetails = ObjectContext.PartsPurchaseOrderDetails.Include("SparePart").Where(e => e.PartsPurchaseOrderId == id).OrderBy(e => e.SerialNumber).ToArray();
            }
            return dbPartsPurchaseOrder;
        }

        public PartsPurchaseOrder GetPartsPurchaseOrdersWithDetailsByIdForChannelChange(int id) {
            var dbPartsPurchaseOrder = ObjectContext.PartsPurchaseOrders.Include("PartsPurchaseOrderType").SingleOrDefault(entity => entity.Id == id && entity.Status != (int)DcsPartsPurchaseOrderStatus.作废);
            if(dbPartsPurchaseOrder != null) {
                var dbDetails = ObjectContext.PartsPurchaseOrderDetails.Where(e => e.PartsPurchaseOrderId == id && e.ConfirmedAmount < e.OrderAmount).OrderBy(e => e.SerialNumber).ToArray();
            }
            return dbPartsPurchaseOrder;
        }

        public IQueryable<PartsPurchaseOrder> GetPartsPurchaseOrdersWithOrderType() {
            return ObjectContext.PartsPurchaseOrders.Include("PartsPurchaseOrderType").OrderBy(e => e.Id);
        }

        public IQueryable<PartsPurchaseOrder> GetPartsPurchaseOrdersWithOrderTypeStatus(string sparePartCode, string sparePartName) {
            var res1 = from c in ObjectContext.PartsInboundCheckBills.Where(c => c.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件采购订单)
                       join d in ObjectContext.PartsInboundCheckBillDetails on c.Id equals d.PartsInboundCheckBillId
                       group d by new {
                           c.OriginalRequirementBillCode
                       } into ss
                       select new {
                           ss.Key.OriginalRequirementBillCode,
                           sumQuantity = ss.Sum(g => g.InspectedQuantity)
                       };
            var res2 = from e in ObjectContext.PartsPurReturnOrders.Where(r => r.Status != 99)
                       join f in ObjectContext.PartsPurReturnOrderDetails on e.Id equals f.PartsPurReturnOrderId
                       group f by new {
                           e.PartsPurchaseOrderCode
                       } into s
                       select new {
                           s.Key.PartsPurchaseOrderCode,
                           sumQuantity = s.Sum(g => g.Quantity)
                       };
            IQueryable<PartsPurchaseOrderDetail> res3 = ObjectContext.PartsPurchaseOrderDetails;
            if(!string.IsNullOrEmpty(sparePartCode))
                res3 = res3.Where(r => r.SparePartCode.Contains(sparePartCode));
            if(!string.IsNullOrEmpty(sparePartName))
                res3 = res3.Where(r => r.SparePartName.Contains(sparePartName));

            var result = from t1 in ObjectContext.PartsPurchaseOrders.Where(r => r.Status == (int)DcsPartsPurchaseOrderStatus.部分发运 || r.Status == (int)DcsPartsPurchaseOrderStatus.发运完毕 || r.Status == (int)DcsPartsPurchaseOrderStatus.终止).Include("PartsPurchaseOrderType")
                         join t5 in res3 on t1.Id equals t5.PartsPurchaseOrderId
                         join t2 in res1 on new {
                             Code = t1.Code
                         } equals new {
                             Code = t2.OriginalRequirementBillCode
                         }
                         join t3 in res2 on new {
                             Code = t1.Code
                         } equals new {
                             Code = t3.PartsPurchaseOrderCode
                         } into temp1
                         from t4 in temp1.DefaultIfEmpty()
                         where t2.sumQuantity - ((int?)t4.sumQuantity ?? 0) > 0
                         select t1;
            return result.Include("PartsPurchaseOrderType").Distinct().OrderBy(r => r.Id);
        }

        public IEnumerable<PartsPurchaseOrderDetail> 查询采购订单看板(int partsPurchaseOrderId, int originalRequirementBillId) {
            var partsPurchaseOrders = ObjectContext.PartsPurchaseOrders.Where(r => r.Id == partsPurchaseOrderId && r.Status != (int)DcsPartsPurchaseOrderStatus.作废).ToArray();
            if(!partsPurchaseOrders.Any())
                return Enumerable.Empty<PartsPurchaseOrderDetail>();
            var partsPurchaseOrderIds = partsPurchaseOrders.Select(v => v.Id).ToArray();
            var partsPurchaseOrderDetails = ObjectContext.PartsPurchaseOrderDetails.Where(r => partsPurchaseOrderIds.Contains(r.PartsPurchaseOrderId)).ToArray();
            var supplierShippingOrders = ObjectContext.SupplierShippingOrders.Where(r => partsPurchaseOrderIds.Contains(r.PartsPurchaseOrderId) && r.Status != (int)DcsSupplierShippingOrderStatus.作废).ToArray();
            if(supplierShippingOrders.Any()) {
                var supplierShippingOrderIds = supplierShippingOrders.Select(r => r.Id).ToArray();
                var supplierShippingOrderDetails = ObjectContext.SupplierShippingDetails.Where(r => supplierShippingOrderIds.Contains(r.SupplierShippingOrderId)).ToArray();
                foreach(var partsPurchaseOrderDetail in partsPurchaseOrderDetails) {
                    partsPurchaseOrderDetail.ShippingQuantity = 0;
                    PartsPurchaseOrderDetail detail = partsPurchaseOrderDetail;
                    var temp = supplierShippingOrderDetails.Where(r => r.SparePartId == detail.SparePartId);
                    foreach(var supplierShippingOrderDetail in temp) {
                        partsPurchaseOrderDetail.ShippingQuantity += supplierShippingOrderDetail.Quantity;
                        partsPurchaseOrderDetail.ReceiptAmount += supplierShippingOrderDetail.ConfirmedAmount ?? 0;
                    }
                }
            }

            var p = partsPurchaseOrders.FirstOrDefault();
            if(p.OriginalRequirementBillType == 4) {
                var partsInboundCheckBills = ObjectContext.PartsInboundCheckBills.Where(r => r.OriginalRequirementBillId == originalRequirementBillId && r.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件采购订单 && r.StorageCompanyType == (int)DcsCompanyType.分公司).ToArray();
                if(partsInboundCheckBills.Any()) {
                    var partsInboundCheckBillIds = partsInboundCheckBills.Select(r => r.Id).ToArray();
                    var partsInboundCheckBillDetails = ObjectContext.PartsInboundCheckBillDetails.Where(r => partsInboundCheckBillIds.Contains(r.PartsInboundCheckBillId)).ToArray();
                    foreach(var partsPurchaseOrderDetail in partsPurchaseOrderDetails) {
                        partsPurchaseOrderDetail.InspectedQuantity = 0;
                        PartsPurchaseOrderDetail detail = partsPurchaseOrderDetail;
                        var temp = partsInboundCheckBillDetails.Where(r => r.SparePartId == detail.SparePartId);
                        foreach(var partsInboundCheckBillDetail in temp) {
                            partsPurchaseOrderDetail.InspectedQuantity += partsInboundCheckBillDetail.InspectedQuantity;
                        }
                    }
                }
            } else if(p.OriginalRequirementBillType == 1) {
                var dbSupplierShippingOrders = ObjectContext.SupplierShippingOrders.Where(r => partsPurchaseOrderIds.Contains(r.PartsPurchaseOrderId) && r.Status != (int)DcsSupplierShippingOrderStatus.作废).ToArray();
                if(dbSupplierShippingOrders.Any()) {
                    var supplierShippingOrderCodes = dbSupplierShippingOrders.Select(r => r.Code).ToArray();
                    var partsInboundPlans = ObjectContext.PartsInboundPlans.Where(r => supplierShippingOrderCodes.Contains(r.SourceCode)).ToArray();
                    if(partsInboundPlans.Any()) {
                        var partsInboundPlanIds = partsInboundPlans.Select(r => r.Id).ToArray();
                        var partsInboundCheckBills = ObjectContext.PartsInboundCheckBills.Where(r => partsInboundPlanIds.Contains(r.PartsInboundPlanId) && r.StorageCompanyType == (int)DcsCompanyType.分公司).ToArray();
                        if(partsInboundCheckBills.Any()) {
                            var partsInboundCheckBillIds = partsInboundCheckBills.Select(r => r.Id).ToArray();
                            var partsInboundCheckBillDetails = ObjectContext.PartsInboundCheckBillDetails.Where(r => partsInboundCheckBillIds.Contains(r.PartsInboundCheckBillId)).ToArray();
                            foreach(var partsPurchaseOrderDetail in partsPurchaseOrderDetails) {
                                partsPurchaseOrderDetail.InspectedQuantity = 0;
                                PartsPurchaseOrderDetail detail = partsPurchaseOrderDetail;
                                var temp = partsInboundCheckBillDetails.Where(r => r.SparePartId == detail.SparePartId);
                                foreach(var partsInboundCheckBillDetail in temp) {
                                    partsPurchaseOrderDetail.InspectedQuantity += partsInboundCheckBillDetail.InspectedQuantity;
                                }
                            }
                        }
                    }
                }
            }
            return partsPurchaseOrderDetails.OrderBy(v => v.Id);
        }

        public IQueryable<VirtualPartsPurchaseOrderDetail> 查询采购订单未入库量(int partsSalesCategoryId, int[] partIds) {
            var partsPurchaseOrderDetails = from a in ObjectContext.PartsPurchaseOrders.Where(a => a.Status != (int)DcsPartsPurchaseOrderStatus.作废 && a.PartsSalesCategoryId == partsSalesCategoryId)
                                            join b in ObjectContext.PartsPurchaseOrderDetails.Where(r => partIds.Contains(r.SparePartId)) on a.Id equals b.PartsPurchaseOrderId
                                            group b by new {
                                                b.SparePartId,
                                                a.Id,
                                                a.Code,
                                                a.Status,
                                                b.SparePartCode,
                                                b.SparePartName
                                            } into ss
                                            select new {
                                                ss.Key.SparePartId,
                                                ss.Key.Id,
                                                ss.Key.SparePartCode,
                                                ss.Key.SparePartName,
                                                ss.Key.Status,
                                                ss.Key.Code,
                                                sumQuantity = ss.Sum(g => g.OrderAmount),
                                                confirmQuantity = ss.Sum(t => t.ConfirmedAmount)
                                            };
            var partsInboundCheckBillDetails = from c in ObjectContext.PartsInboundCheckBills.Where(c => c.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件采购订单 && c.PartsSalesCategoryId == partsSalesCategoryId)
                                               join d in ObjectContext.PartsInboundCheckBillDetails.Where(r => partIds.Contains(r.SparePartId)) on c.Id equals d.PartsInboundCheckBillId
                                               group d by new {
                                                   d.SparePartId,
                                                   c.OriginalRequirementBillId
                                               } into ss
                                               select new {
                                                   ss.Key.SparePartId,
                                                   ss.Key.OriginalRequirementBillId,
                                                   sumQuantity = ss.Sum(g => g.InspectedQuantity)
                                               };
            var supplierShippingDetails = from e in ObjectContext.SupplierShippingOrders.Where(t => t.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件采购订单 && t.Status != (int)DcsSupplierShippingOrderStatus.作废 && t.PartsSalesCategoryId == partsSalesCategoryId)
                                          join s in ObjectContext.SupplierShippingDetails.Where(r => partIds.Contains(r.SparePartId)) on e.Id equals s.SupplierShippingOrderId
                                          group s by new {
                                              s.SparePartId,
                                              e.PartsPurchaseOrderId
                                          } into ss
                                          select new {
                                              ss.Key.SparePartId,
                                              ss.Key.PartsPurchaseOrderId,
                                              sumQuantity = ss.Sum(g => g.Quantity)
                                          };
            var result = from t1 in partsPurchaseOrderDetails
                         join t2 in partsInboundCheckBillDetails
                         on new {
                             SparePartId = t1.SparePartId,
                             orderId = t1.Id
                         } equals new {
                             SparePartId = t2.SparePartId,
                             orderId = t2.OriginalRequirementBillId
                         } into temp1
                         from t3 in temp1.DefaultIfEmpty()
                         join t4 in supplierShippingDetails
                         on new {
                             SparePartId = t1.SparePartId,
                             orderid = t1.Id
                         } equals new {
                             SparePartId = t4.SparePartId,
                             orderid = t4.PartsPurchaseOrderId
                         } into temp2
                         from t5 in temp2.DefaultIfEmpty()
                         select new VirtualPartsPurchaseOrderDetail {
                             PartsPurchaseOrderCode = t1.Code,
                             SparePartId = t1.SparePartId,
                             SparePartCode = t1.SparePartCode,
                             SparePartName = t1.SparePartName,
                             OrderAmount = t1.sumQuantity,
                             ShippingAmount = t5.sumQuantity,
                             ConfirmAmount = t1.confirmQuantity,
                             UnInboundAmount = t5.sumQuantity - t3.sumQuantity,
                             PartsPurchaseOrderStatus = t1.Status
                         };
            return result.OrderBy(r => r.SparePartId);
        }

        public IQueryable<PartsPurchaseOrder> GetPartsPurchaseOrdersWithOrderTypeForSupplier() {
            return ObjectContext.PartsPurchaseOrders.Include("PartsPurchaseOrderType").Include("PartsSupplier").Include("PartsSupplier.BranchSupplierRelations").OrderBy(entity => entity.Id);
        }

        public IQueryable<VirtualPartsPurchaseOrder> GetPartsPurchaseOrdersWithOrderTypes() {
            var partsPurchaseOrders = from p in ObjectContext.PartsPurchaseOrders
                                      join t in ObjectContext.PartsPurchaseOrderTypes on p.PartsPurchaseOrderTypeId equals t.Id
                                      into temp1
                                      from t1 in temp1.DefaultIfEmpty()
                                      join b in ObjectContext.BranchSupplierRelations.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on new {
                                          SupplierId = p.PartsSupplierId,
                                          PartsSalesCategoryId = p.PartsSalesCategoryId
                                      } equals new {
                                          SupplierId = b.SupplierId,
                                          PartsSalesCategoryId = b.PartsSalesCategoryId
                                      }
                                      into temp2
                                      from t2 in temp2.DefaultIfEmpty()
                                      select new VirtualPartsPurchaseOrder {
                                          Id = p.Id,
                                          OriginalRequirementBillType = p.OriginalRequirementBillType,
                                          Code = p.Code,
                                          BranchName = p.BranchName,
                                          WarehouseName = p.WarehouseName,
                                          PartsSalesCategoryName = p.PartsSalesCategoryName,
                                          PartsSupplierCode = p.PartsSupplierCode,
                                          PartsSupplierName = p.PartsSupplierName,
                                          PartsPurchaseOrderTypeName = t1.Name,
                                          ReceivingAddress = p.ReceivingAddress,
                                          IfDirectProvision = p.IfDirectProvision,
                                          OriginalRequirementBillCode = p.OriginalRequirementBillCode,
                                          ReceivingCompanyName = p.ReceivingCompanyName,
                                          TotalAmount = p.TotalAmount,
                                          ShippingMethod = p.ShippingMethod,
                                          Status = p.Status,
                                          InStatus = p.InStatus,
                                          RequestedDeliveryTime = p.RequestedDeliveryTime,
                                          PlanSource = p.PlanSource,
                                          GPMSPurOrderCode = p.GPMSPurOrderCode,
                                          AbandonOrStopReason = p.AbandonOrStopReason,
                                          ConfirmationRemark = p.ConfirmationRemark,
                                          Remark = p.Remark,
                                          CreatorName = p.CreatorName,
                                          CreateTime = p.CreateTime,
                                          BusinessCode = t2.BusinessCode,
                                          PartsSalesCategoryId = p.PartsSalesCategoryId,
                                          WarehouseId = p.WarehouseId,
                                          PartsPurchaseOrderTypeId = p.PartsPurchaseOrderTypeId,
                                          BranchId = p.BranchId,
                                          OriginalRequirementBillId = p.OriginalRequirementBillId,
                                          ModifierName = p.ModifierName,
                                          ModifyTime = p.ModifyTime,
                                          AbandonerName = p.AbandonerName,
                                          AbandonTime = p.AbandonTime,
                                          SubmitterName = p.SubmitterName,
                                          SubmitTime = p.SubmitTime,
                                          ApproverName = p.ApproverName,
                                          ApproveTime = p.ApproveTime,
                                          CloserName = p.CloserName,
                                          CloseTime = p.CloseTime
                                      };
            return partsPurchaseOrders.OrderBy(entity => entity.Id);
        }
        public IQueryable<VirtualPartsPurchaseOrder> 根据供应商编码查询人员采购订单(string businessCode, int? ioStatus, string sparePartCode) {
            var userInfo = Utils.GetCurrentUserInfo();
            //var branchSupplierRelations = ObjectContext.BranchSupplierRelations.Where(r => r.Status == (int)DcsBaseDataStatus.有效);
            //if (!string.IsNullOrEmpty(businessCode))
            //    branchSupplierRelations = branchSupplierRelations.Where(r => r.BusinessCode.Contains(businessCode));

            //var personnelSupplierRelations = ObjectContext.PersonnelSupplierRelations.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.PersonId == userInfo.Id);
            if(!ObjectContext.PersonnelSupplierRelations.Any(r => r.Status == (int)DcsBaseDataStatus.有效 && r.PersonId == userInfo.Id)) {
                var partsPurchaseOrders = from p in ObjectContext.PartsPurchaseOrders
                                          join t in ObjectContext.PartsPurchaseOrderTypes on p.PartsPurchaseOrderTypeId equals t.Id into temp1
                                          from t1 in temp1.DefaultIfEmpty()
                                          join b in ObjectContext.BranchSupplierRelations.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && (!string.IsNullOrEmpty(businessCode) ? r.BusinessCode.Contains(businessCode) : true)) on new {
                                              SupplierId = p.PartsSupplierId,
                                              PartsSalesCategoryId = p.PartsSalesCategoryId
                                          } equals new {
                                              SupplierId = b.SupplierId,
                                              PartsSalesCategoryId = b.PartsSalesCategoryId
                                          } into temp2
                                          from t2 in temp2.DefaultIfEmpty()
                                          join c in ObjectContext.PersonSalesCenterLinks on new {
                                              PartsSalesCategoryId = p.PartsSalesCategoryId,
                                              PersonId = userInfo.Id

                                          } equals new {
                                              PartsSalesCategoryId = c.PartsSalesCategoryId,
                                              PersonId = c.PersonId
                                          }
                                          join d in ObjectContext.PartsPurchaseOrder_Sync on p.Code equals d.Objid into temp4
                                          from t4 in temp4.DefaultIfEmpty()
                                          select new VirtualPartsPurchaseOrder {
                                              HWPurOrderCode = p.HWPurOrderCode,
                                              Id = p.Id,
                                              OriginalRequirementBillType = p.OriginalRequirementBillType,
                                              Code = p.Code,
                                              BranchName = p.BranchName,
                                              BranchCode = p.BranchCode,
                                              WarehouseName = p.WarehouseName,
                                              PartsSalesCategoryName = p.PartsSalesCategoryName,
                                              PartsSupplierId = p.PartsSupplierId,
                                              PartsSupplierCode = p.PartsSupplierCode,
                                              PartsSupplierName = p.PartsSupplierName,
                                              PartsPurchaseOrderTypeName = t1.Name,
                                              ReceivingAddress = p.ReceivingAddress,
                                              IfDirectProvision = p.IfDirectProvision,
                                              OriginalRequirementBillCode = p.OriginalRequirementBillCode,
                                              ReceivingCompanyName = p.ReceivingCompanyName,
                                              TotalAmount = p.TotalAmount,
                                              ShippingMethod = p.ShippingMethod,
                                              Status = p.Status,
                                              InStatus = p.InStatus,
                                              RequestedDeliveryTime = p.RequestedDeliveryTime,
                                              PlanSource = p.PlanSource,
                                              GPMSPurOrderCode = p.GPMSPurOrderCode,
                                              SAPPurchasePlanCode = p.SAPPurchasePlanCode,
                                              AbandonOrStopReason = p.AbandonOrStopReason,
                                              ConfirmationRemark = p.ConfirmationRemark,
                                              Remark = p.Remark,
                                              CreatorName = p.CreatorName,
                                              CreateTime = p.CreateTime,
                                              BusinessCode = t2.BusinessCode,
                                              PartsSalesCategoryId = p.PartsSalesCategoryId,
                                              WarehouseId = p.WarehouseId,
                                              PartsPurchaseOrderTypeId = p.PartsPurchaseOrderTypeId,
                                              BranchId = p.BranchId,
                                              OriginalRequirementBillId = p.OriginalRequirementBillId,
                                              ModifierName = p.ModifierName,
                                              ModifyTime = p.ModifyTime,
                                              AbandonerName = p.AbandonerName,
                                              AbandonTime = p.AbandonTime,
                                              SubmitterName = p.SubmitterName,
                                              SubmitTime = p.SubmitTime,
                                              ApproverName = p.ApproverName,
                                              ApproveTime = p.ApproveTime,
                                              CloserName = p.CloserName,
                                              CloseTime = p.CloseTime,
                                              IfInnerDirectProvision = p.IfInnerDirectProvision,
                                              ERPSourceOrderCode = p.ERPSourceOrderCode,
                                              CPPartsPurchaseOrderCode = p.CPPartsPurchaseOrderCode,
                                              CPPartsInboundCheckCode = p.CPPartsInboundCheckCode,
                                              IoStatus = t4.IOStatus,
                                              IsTransSap = p.IsTransSap,
                                              Message = t4.Message,
                                              IsPack=p.IsPack
                                          };
                if (ioStatus.HasValue) {
                    partsPurchaseOrders = partsPurchaseOrders.Where(r => r.IoStatus == ioStatus);
                }
                if(!string.IsNullOrEmpty(sparePartCode)) {
                    partsPurchaseOrders = partsPurchaseOrders.Where(r => ObjectContext.PartsPurchaseOrderDetails.Any(y=>y.PartsPurchaseOrderId==r.Id && y.SparePartCode.Contains(sparePartCode)));
                }
                return partsPurchaseOrders.OrderByDescending(entity => entity.CreateTime);
            } else {
                var partsPurchaseOrders = from p in ObjectContext.PartsPurchaseOrders
                                          join t in ObjectContext.PartsPurchaseOrderTypes on p.PartsPurchaseOrderTypeId equals t.Id into temp1
                                          from t1 in temp1.DefaultIfEmpty()
                                          join b in ObjectContext.BranchSupplierRelations.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && (!string.IsNullOrEmpty(businessCode) ? r.BusinessCode.Contains(businessCode) : true)) on new {
                                              SupplierId = p.PartsSupplierId,
                                              PartsSalesCategoryId = p.PartsSalesCategoryId
                                          } equals new {
                                              SupplierId = b.SupplierId,
                                              PartsSalesCategoryId = b.PartsSalesCategoryId
                                          } into temp2
                                          from t2 in temp2.DefaultIfEmpty()
                                          join d in ObjectContext.PersonnelSupplierRelations.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on new {
                                              PartsSupplierId = p.PartsSupplierId,
                                              PersonId = userInfo.Id

                                          } equals new {
                                              PartsSupplierId = d.SupplierId,
                                              PersonId = d.PersonId
                                          }
                                          join e in ObjectContext.PartsPurchaseOrder_Sync on p.Code equals e.Objid into temp4
                                          from t4 in temp4.DefaultIfEmpty()
                                          select new VirtualPartsPurchaseOrder {
                                              HWPurOrderCode = p.HWPurOrderCode,
                                              Id = p.Id,
                                              OriginalRequirementBillType = p.OriginalRequirementBillType,
                                              Code = p.Code,
                                              BranchName = p.BranchName,
                                              BranchCode = p.BranchCode,
                                              WarehouseName = p.WarehouseName,
                                              PartsSalesCategoryName = p.PartsSalesCategoryName,
                                              PartsSupplierId = p.PartsSupplierId,
                                              PartsSupplierCode = p.PartsSupplierCode,
                                              PartsSupplierName = p.PartsSupplierName,
                                              PartsPurchaseOrderTypeName = t1.Name,
                                              ReceivingAddress = p.ReceivingAddress,
                                              IfDirectProvision = p.IfDirectProvision,
                                              OriginalRequirementBillCode = p.OriginalRequirementBillCode,
                                              ReceivingCompanyName = p.ReceivingCompanyName,
                                              TotalAmount = p.TotalAmount,
                                              ShippingMethod = p.ShippingMethod,
                                              Status = p.Status,
                                              InStatus = p.InStatus,
                                              RequestedDeliveryTime = p.RequestedDeliveryTime,
                                              PlanSource = p.PlanSource,
                                              GPMSPurOrderCode = p.GPMSPurOrderCode,
                                              SAPPurchasePlanCode = p.SAPPurchasePlanCode,
                                              AbandonOrStopReason = p.AbandonOrStopReason,
                                              ConfirmationRemark = p.ConfirmationRemark,
                                              Remark = p.Remark,
                                              CreatorName = p.CreatorName,
                                              CreateTime = p.CreateTime,
                                              BusinessCode = t2.BusinessCode,
                                              PartsSalesCategoryId = p.PartsSalesCategoryId,
                                              WarehouseId = p.WarehouseId,
                                              PartsPurchaseOrderTypeId = p.PartsPurchaseOrderTypeId,
                                              BranchId = p.BranchId,
                                              OriginalRequirementBillId = p.OriginalRequirementBillId,
                                              ModifierName = p.ModifierName,
                                              ModifyTime = p.ModifyTime,
                                              AbandonerName = p.AbandonerName,
                                              AbandonTime = p.AbandonTime,
                                              SubmitterName = p.SubmitterName,
                                              SubmitTime = p.SubmitTime,
                                              ApproverName = p.ApproverName,
                                              ApproveTime = p.ApproveTime,
                                              CloserName = p.CloserName,
                                              CloseTime = p.CloseTime,
                                              IfInnerDirectProvision = p.IfInnerDirectProvision,
                                              ERPSourceOrderCode = p.ERPSourceOrderCode,
                                              CPPartsPurchaseOrderCode = p.CPPartsPurchaseOrderCode,
                                              CPPartsInboundCheckCode = p.CPPartsInboundCheckCode,
                                              IoStatus = t4.IOStatus,
                                              IsTransSap = p.IsTransSap,
                                              Message = t4.Message
                                          };
                if (ioStatus.HasValue) {
                    partsPurchaseOrders = partsPurchaseOrders.Where(r => r.IoStatus == ioStatus);
                }
                if(!string.IsNullOrEmpty(sparePartCode)) {
                    partsPurchaseOrders = partsPurchaseOrders.Where(r => ObjectContext.PartsPurchaseOrderDetails.Any(y => y.PartsPurchaseOrderId == r.Id && y.SparePartCode.Contains(sparePartCode)));
                }
                return partsPurchaseOrders.OrderByDescending(entity => entity.CreateTime);
            }

        }



        public IQueryable<PartsPurchaseOrder> GetPartsPurchaseOrderInfos() {
            return ObjectContext.PartsPurchaseOrders.Include("PartsPurchaseOrderDetails").Where(v => v.PartsPurchaseOrderDetails.Any(r => r.ConfirmedAmount != 0 && (r.ShippingAmount == null || r.ConfirmedAmount != r.ShippingAmount))).OrderBy(entity => entity.Id);
        }

        public IQueryable<PartsPurchaseOrderType> GetPartsPurchaseOrderTypesWithPartsSalesCategory() {
            var userInfo = Utils.GetCurrentUserInfo();
            var partsPurchaseOrderTypes = ObjectContext.PartsPurchaseOrderTypes.Where(r => r.BranchId == userInfo.EnterpriseId);
            return partsPurchaseOrderTypes.Include("PartsSalesCategory").OrderBy(e => e.Id);
        }

        public IQueryable<PartsPurchaseOrder> GetPartsPurchaseOrdersForSuppliers() {
            return ObjectContext.PartsPurchaseOrders.Include("PartsPurchaseOrderType").Include("Branch").OrderBy(entity => entity.Id);
        }

        public IQueryable<PartsPurchaseOrder> 根据人员查询采购订单() {
            //1、查询人员与销售中心关系（人员Id=登录人员Id)
            //只查询出登录人员对应的配件销售类型Id对应的采购订单（配件采购订单.配件销售类型Id=人员与销售中心关系.配件销售类型Id)
            var userInfo = Utils.GetCurrentUserInfo();
            var result = ObjectContext.PartsPurchaseOrders.Where(r => ObjectContext.PersonSalesCenterLinks.Any(v => v.PartsSalesCategoryId == r.PartsSalesCategoryId && v.PersonId == userInfo.Id));
            return result;
        }

        public PartsPurchaseOrder GetPartsPurchaseOrderById(int id) {
            return ObjectContext.PartsPurchaseOrders.SingleOrDefault(entity => entity.Id == id);
        }

        public IQueryable<PartsSupplierRelation> GetPartsSupplierRelationBySpareIdAndSupplierId(int partsSalesCategoryId, int SupplierId, int[] SpareIds) {
            //return ObjectContext.PartsSupplierRelations.Where(entity => entity.SupplierId == SupplierId && SpareIds.Contains(entity.PartId) && entity.Status != (int)DcsBaseDataStatus.作废);
            return ObjectContext.PartsSupplierRelations.Where(r => SpareIds.Contains(r.PartId) && r.SupplierId == SupplierId && r.PartsSalesCategoryId == partsSalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效).Include("PartsSalesCategory").Include("SparePart").Include("PartsSupplier").OrderBy(entity => entity.Id);
        }

        //查询分公司与供应商关系
        public IQueryable<BranchSupplierRelation> GetBranchSupplierRelationBySupplierId(int partsSalesCategoryId, int SupplierId, int BranchId) {
            return ObjectContext.BranchSupplierRelations.Where(r => r.BranchId == BranchId && r.SupplierId == SupplierId && r.PartsSalesCategoryId == partsSalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效).OrderBy(entity => entity.Id);
        }

        public IQueryable<VirtualContactPerson> getVirtualContactPerson(int id)
        {
            var dbPartsPurchaseOrder = ObjectContext.PartsPurchaseOrders.SingleOrDefault(entity => entity.Id == id && entity.Status != (int)DcsPartsPurchaseOrderStatus.作废);
            if (dbPartsPurchaseOrder.IfDirectProvision)
            {
               var  person = from a in ObjectContext.PartsSalesOrders
                         where a.Id==dbPartsPurchaseOrder.OriginalRequirementBillId
                         select new VirtualContactPerson
                         {
                             Id = a.Id,
                             ContactPerson = a.ContactPerson,
                             ContactPhone = a.ContactPhone
                         };
               return person;
            }
            else
            {
              var  person = from a in ObjectContext.Warehouses
                         where a.Id == dbPartsPurchaseOrder.WarehouseId
                         select new VirtualContactPerson
                         {
                             Id = a.Id,
                             ContactPerson = a.Contact,
                             ContactPhone = a.PhoneNumber
                         };
                return person;
            }
           
        }
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               

        public void InsertPartsPurchaseOrder(PartsPurchaseOrder partsPurchaseOrder) {
            new PartsPurchaseOrderAch(this).InsertPartsPurchaseOrder(partsPurchaseOrder);
        }

        public void UpdatePartsPurchaseOrder(PartsPurchaseOrder partsPurchaseOrder) {
            new PartsPurchaseOrderAch(this).UpdatePartsPurchaseOrder(partsPurchaseOrder);
        }

        public PartsPurchaseOrder GetPartsPurchaseOrdersWithDetailsById(int id) {
            return new PartsPurchaseOrderAch(this).GetPartsPurchaseOrdersWithDetailsById(id);
        }

        public PartsPurchaseOrder GetPartsPurchaseOrdersWithDetailsByIdForChannelChange(int id) {
            return new PartsPurchaseOrderAch(this).GetPartsPurchaseOrdersWithDetailsByIdForChannelChange(id);
        }

        public IQueryable<PartsPurchaseOrder> GetPartsPurchaseOrdersWithOrderType() {
            return new PartsPurchaseOrderAch(this).GetPartsPurchaseOrdersWithOrderType();
        }

        public IQueryable<PartsPurchaseOrder> GetPartsPurchaseOrdersWithOrderTypeStatus(string sparePartCode, string sparePartName) {
            return new PartsPurchaseOrderAch(this).GetPartsPurchaseOrdersWithOrderTypeStatus(sparePartCode, sparePartName);
        }

        public IEnumerable<PartsPurchaseOrderDetail> 查询采购订单看板(int partsPurchaseOrderId, int originalRequirementBillId) {
            return new PartsPurchaseOrderAch(this).查询采购订单看板(partsPurchaseOrderId, originalRequirementBillId);
        }

        public IQueryable<VirtualPartsPurchaseOrderDetail> 查询采购订单未入库量(int partsSalesCategoryId, int[] partIds) {
            return new PartsPurchaseOrderAch(this).查询采购订单未入库量(partsSalesCategoryId, partIds);
        }

        public IQueryable<PartsPurchaseOrder> GetPartsPurchaseOrdersWithOrderTypeForSupplier() {
            return new PartsPurchaseOrderAch(this).GetPartsPurchaseOrdersWithOrderTypeForSupplier();
        }

        public IQueryable<VirtualPartsPurchaseOrder> GetPartsPurchaseOrdersWithOrderTypes() {
            return new PartsPurchaseOrderAch(this).GetPartsPurchaseOrdersWithOrderTypes();
        }
        public IQueryable<VirtualPartsPurchaseOrder> 根据供应商编码查询人员采购订单(string businessCode, int? ioStatus, string sparePartCode) {
            return new PartsPurchaseOrderAch(this).根据供应商编码查询人员采购订单(businessCode, ioStatus, sparePartCode);
        }

        public IQueryable<PartsPurchaseOrder> GetPartsPurchaseOrderInfos() {
            return new PartsPurchaseOrderAch(this).GetPartsPurchaseOrderInfos();
        }

        public IQueryable<PartsPurchaseOrderType> GetPartsPurchaseOrderTypesWithPartsSalesCategory() {
            return new PartsPurchaseOrderAch(this).GetPartsPurchaseOrderTypesWithPartsSalesCategory();
        }

        public IQueryable<PartsPurchaseOrder> GetPartsPurchaseOrdersForSuppliers() {
            return new PartsPurchaseOrderAch(this).GetPartsPurchaseOrdersForSuppliers();
        }

        public IQueryable<PartsPurchaseOrder> 根据人员查询采购订单() {
            return new PartsPurchaseOrderAch(this).根据人员查询采购订单();
        }

        public PartsPurchaseOrder GetPartsPurchaseOrderById(int id) {
            return new PartsPurchaseOrderAch(this).GetPartsPurchaseOrderById(id);
        }

        [Query(HasSideEffects = true)]
        public IQueryable<PartsSupplierRelation> GetPartsSupplierRelationBySpareIdAndSupplierId(int partsSalesCategoryId, int SupplierId, int[] SpareIds) {
            return new PartsPurchaseOrderAch(this).GetPartsSupplierRelationBySpareIdAndSupplierId(partsSalesCategoryId, SupplierId, SpareIds);
        }
        public IQueryable<BranchSupplierRelation> GetBranchSupplierRelationBySupplierId(int partsSalesCategoryId, int SupplierId, int BranchId) {
            return new PartsPurchaseOrderAch(this).GetBranchSupplierRelationBySupplierId(partsSalesCategoryId, SupplierId, BranchId);
        }
        public IQueryable<VirtualContactPerson> getVirtualContactPerson(int id)
        {
            return new PartsPurchaseOrderAch(this).getVirtualContactPerson(id);
        }
    }
}
