﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Devart.Data.Oracle;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsPurchaseOrderDetailAch : DcsSerivceAchieveBase {
        public PartsPurchaseOrderDetailAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public IQueryable<PartsPurchaseOrderDetail> GetPartsPurchaseOrderDetailsWithOrder() {
            return ObjectContext.PartsPurchaseOrderDetails.Include("PartsPurchaseOrder").Where(r => r.ConfirmedAmount != 0 && (r.ShippingAmount == null || r.ConfirmedAmount != r.ShippingAmount)).OrderBy(entity => entity.Id);
        }

        public IQueryable<PartsPurchaseOrderDetail> GetPartsPurchaseOrderDetailsWithSpareParts() {
            return ObjectContext.PartsPurchaseOrderDetails.Include("SparePart").OrderBy(r => r.Id);
        }

        /// <summary>
        /// 根据sql查询采购清单上一次采购变更申请单参考价格
        /// </summary>
        /// <param name="partsSalesCategoryId">采购订单品牌</param>
        /// <param name="partsSupplierId">采购订单供应商</param>
        /// <param name="partId">清单配件id</param>
        /// <param name="createTime">采购订单创建时间</param>
        /// <returns></returns>
        public ReferencepriceRecord GetPartsPurchaseOrderDetailsByExecuteSql(int partsSalesCategoryId, int partsSupplierId, int partId, DateTime? createTime) {
            var sqlBuilder = new StringBuilder();
            sqlBuilder.Append(@"select * from (select b.referenceprice,b.pricetype,
                  dense_rank() over(order by a.approvetime desc) rank
             from PartsPurchasePricingDetail b
            inner join PartsPurchasePricingChange a
               on a.id = b.parentid
            where b.supplierid =:partsSupplierId
              and a.partssalescategoryid = :partsSalesCategoryId
              and a.approvetime <= :createTime
              and b.ValidFrom <=:createTime
              and b.validto >=:createTime
              and a.status = 2
              and b.partid= :partId
            ) where rank =1");
            var paramaterList = new List<Object>();
            paramaterList.Add(new OracleParameter("partsSalesCategoryId", partsSalesCategoryId));
            paramaterList.Add(new OracleParameter("partsSupplierId", partsSupplierId));
            paramaterList.Add(new OracleParameter("partId", partId));
            paramaterList.Add(new OracleParameter("createTime", createTime));
            var sql = sqlBuilder.ToString();
            var search = ObjectContext.ExecuteStoreQuery<ReferencepriceRecord>(sql, paramaterList.ToArray());
            return search.SingleOrDefault();
        }
        public List<ReferencepriceRecord> GetPartsPurchaseOrderDetailsByExecuteSql2(int partsSalesCategoryId, int partsSupplierId, int[] partId, DateTime? createTime) {
            var sqlBuilder = new StringBuilder();
            sqlBuilder.Append(@"select * from (select b.referenceprice,b.pricetype,b.PartId,
                  dense_rank() over(order by a.approvetime desc) rank
             from PartsPurchasePricingDetail b
            inner join PartsPurchasePricingChange a
               on a.id = b.parentid
            where b.supplierid =:partsSupplierId
              and a.partssalescategoryid = :partsSalesCategoryId
              and a.approvetime <= :createTime
              and b.ValidFrom <=:createTime
              and b.validto >=:createTime
              and a.status = 2            
            ");
            sqlBuilder.Append(" and  b.partid in(");
            for(var i = 0; i < partId.Length; i++) {
                if(partId.Length == i + 1) {
                    sqlBuilder.Append(partId[i].ToString());
                } else {
                    sqlBuilder.Append(partId[i].ToString() + ",");
                }
            }
            sqlBuilder.Append(" )) where rank =1  ");
            var paramaterList = new List<Object>();
            paramaterList.Add(new OracleParameter("partsSalesCategoryId", partsSalesCategoryId));
            paramaterList.Add(new OracleParameter("partsSupplierId", partsSupplierId));
          //  paramaterList.Add(new OracleParameter("partId", partId));
            paramaterList.Add(new OracleParameter("createTime", createTime));
            var sql = sqlBuilder.ToString();
            var search = ObjectContext.ExecuteStoreQuery<ReferencepriceRecord>(sql, paramaterList.ToArray());
            return search.ToList();
        }
        /// <summary>
        /// 记录采购清单申请变更前价格
        /// </summary>
        public class ReferencepriceRecord {
            public decimal? ReferencePrice {
                get;
                set;
            }
            public int? Rank {
                get;
                set;
            }
            public int? PriceType
            {
                get;
                set;
            }
            public int? PartId {
                get;
                set;
            }
        }
    
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               

        public IQueryable<PartsPurchaseOrderDetail> GetPartsPurchaseOrderDetailsWithOrder() {
            return new PartsPurchaseOrderDetailAch(this).GetPartsPurchaseOrderDetailsWithOrder();
        }

        public IQueryable<PartsPurchaseOrderDetail> GetPartsPurchaseOrderDetailsWithSpareParts() {
            return new PartsPurchaseOrderDetailAch(this).GetPartsPurchaseOrderDetailsWithSpareParts();
        }
    }
}
