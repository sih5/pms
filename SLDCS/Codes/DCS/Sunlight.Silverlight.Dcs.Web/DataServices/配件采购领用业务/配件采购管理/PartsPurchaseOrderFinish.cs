﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
using System.Configuration;
using System.Data;
using System.Text;
using Devart.Data.Oracle;
using NPOI.SS.Formula.Functions;
using System.Transactions;
using System.Data.Objects.SqlClient;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsPurchaseOrderFinishAch : DcsSerivceAchieveBase {
        public PartsPurchaseOrderFinishAch(DcsDomainService domainService)
            : base(domainService) {
        }
        public IEnumerable<PartsPurchaseOrderFinish> 采购订单完成情况统计(DateTime? bPurchasePlanTime, DateTime? ePurchasePlanTime, DateTime? bTheoryDeliveryTime, DateTime? eTheoryDeliveryTime,bool isExport) {
            string SQL = @"select nvl(tt.month, yy.month) as month,
                                  nvl(tt.day, yy.day) as day,
                                  nvl(tt.allItem, 0) + nvl(yy.allItem, 0) as allItem, --采购订单总条目数
                                  nvl(tt.arriveItem, 0) + nvl(yy.arriveItem, 0) as arriveItem, --采购订单到期总条目数
                                  yy.onTime,
                                  yy.outTime,
                                  tt.sevenUn,
                                  tt.eightUn,
                                  tt.TwentyUn,
                                  tt.OverTwentyUn,
                                  yy.StopAmount,
                                  tt.Undue,
                                  (case
                                     when (nvl(tt.ArriveItem, 0) + nvl(yy.ArriveItem, 0) -
                                          nvl(yy.StopAmount, 0)) = 0 then
                                      0
                                     else
                                      round(nvl(yy.OnTime, 0) /
                                            (nvl(tt.ArriveItem, 0) + nvl(yy.ArriveItem, 0) -
                                             nvl(yy.StopAmount, 0)) * 100,
                                            2)
                                   end) as mzl,
                                 rownum
                                 from (select mm.day,
                                              mm.month,
                                              sum(mm.allItem) as allItem,
                                              sum(mm.arriveItem) as arriveItem,
                                              sum(mm.sevenUn) as sevenUn,
                                              sum(mm.eightUn) as eightUn,
                                              sum(mm.TwentyUn) as TwentyUn,
                                              sum(mm.OverTwentyUn) as OverTwentyUn,
                                              sum(mm.Undue) as Undue
                                              from (select 1 as allItem,
                                                   to_char(pu.TheoryDeliveryTime, 'dd') as day,
                       to_char(pu.TheoryDeliveryTime, 'MM') as month,
                       (case
                         when  --pu.SupplyConfirmQty <> 0
                          (to_char(pu.TheoryDeliveryTime, 'yyyy-mm-dd') <
                          to_char(sysdate, 'yyyy-mm-dd') and
                          pu.supplyconfirmreason is null) or
                          (to_char(pu.TheoryDeliveryTime, 'yyyy-mm-dd') <
                          to_char(sysdate, 'yyyy-mm-dd') and
                          pu.supplyconfirmreason = '最小包装数量')  then
                          1
                         else
                          0
                       end) as arriveItem, --采购订单到期总条目数
                       (case
                         when --pu.SupplyConfirmQty <> 0 and
                              to_char(pu.TheoryDeliveryTime, 'yyyy-mm-dd') >=
                              (to_char(sysdate - 7, 'yyyy-mm-dd')) and
                                 to_char(pu.TheoryDeliveryTime, 'yyyy-mm-dd') <
                               (to_char(sysdate , 'yyyy-mm-dd')) then
                          1
                         else
                          0
                       end) as sevenUn, --7天未完成
                       (case
                         when -- pu.SupplyConfirmQty <> 0 and
                              to_char(pu.TheoryDeliveryTime, 'yyyy-mm-dd') <
                              (to_char(sysdate - 7, 'yyyy-mm-dd')) and
                              to_char(pu.TheoryDeliveryTime, 'yyyy-mm-dd') >=
                              (to_char(sysdate - 14, 'yyyy-mm-dd')) then
                          1
                         else
                          0
                       end) as eightUn, --14天未完成
                       (case
                         when --pu.SupplyConfirmQty <> 0 and
                              to_char(pu.TheoryDeliveryTime, 'yyyy-mm-dd') <
                              (to_char(sysdate - 14, 'yyyy-mm-dd')) and
                              to_char(pu.TheoryDeliveryTime, 'yyyy-mm-dd') >=
                              (to_char(sysdate - 21, 'yyyy-mm-dd')) then
                          1
                         else
                          0
                       end) as TwentyUn, --21天未完成
                       (case
                         when --pu.SupplyConfirmQty <> 0 and
                              to_char(pu.TheoryDeliveryTime, 'yyyy-mm-dd') <
                              (to_char(sysdate - 21, 'yyyy-mm-dd')) then
                          1
                         else
                          0
                       end) as OverTwentyUn, -- 超过21天未完成,        
                       (case
                         when --pu.SupplyConfirmQty <> 0 and
                              to_char(pu.TheoryDeliveryTime, 'yyyy-mm-dd') >=
                              (to_char(sysdate, 'yyyy-mm-dd')) then
                          1
                         else
                          0
                       end) as Undue -- 未到期条目数
                             from PurchaseOrderUnFinishDetail pu where 1=1 {0}) mm
                             group by mm.month, mm.day) tt
                          full join (select sum(ff.allItem) as allItem,
                                            sum(ff.arriveItem) as arriveItem,
                                            sum(ff.onTime) as onTime,
                                            sum(ff.outTime) as outTime,
                                            sum(ff.StopAmount) as StopAmount,
                                            ff.month,
                                            ff.day
                         from (select 1 as allItem,
                            to_char(pu.TheoryDeliveryTime, 'dd') as day,
                            to_char(pu.TheoryDeliveryTime, 'MM') as month,
                              (case
                              when pu.SupplyConfirmReason = '最小包装数量' or
                                   pu.SupplyConfirmReason is null then
                               1
                              else
                               0
                            end) as arriveItem,
                            pu.onlineqty,
                            (case
                              when pu.onlineqty = 0 and
                                   pu.SupplyConfirmQty > 0 and
                                   to_char(pu.InboundTime, 'yyyy-mm-dd') <=
                                   to_char(pu.TheoryDeliveryTime, 'yyyy-mm-dd') then
                               1
                              else
                               0
                            end) as onTime, -- 按期完成条目数
                            (case
                              when pu.onlineqty = 0 and
                                   pu.SupplyConfirmQty > 0 and
                                   to_char(pu.InboundTime, 'yyyy-mm-dd') >
                                   to_char(pu.TheoryDeliveryTime, 'yyyy-mm-dd') then
                               1
                              else
                               0
                            end) as outTime, --超期完成条目数
                                 (case
                              when pu.SupplyConfirmQty = 0 or
                                   (pu.SupplyConfirmQty > 0 and
                                   pu.InboundTime is null) or
                                   (pu.SupplyConfirmReason is not null and
                                   pu.SupplyConfirmReason <> '最小包装数量') then
                               1
                              else
                               0
                            end) as StopAmount --终止数量
                       from PurchaseOrderFinishedDetail pu where 1=1 {1}) ff
                           group by ff.month, ff.day) yy
                        on tt.month = yy.month
                       and tt.day = yy.day
                          order by nvl(tt.month, yy.month), nvl(tt.day, yy.day)";
            var serachSQL = new StringBuilder();
            if(bPurchasePlanTime.HasValue && bPurchasePlanTime.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
                bPurchasePlanTime = null;
            }
            if(ePurchasePlanTime.HasValue && ePurchasePlanTime.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
                ePurchasePlanTime = null;
            }
            if(bTheoryDeliveryTime.HasValue && bTheoryDeliveryTime.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
                bTheoryDeliveryTime = null;
            }
            if(eTheoryDeliveryTime.HasValue && eTheoryDeliveryTime.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
                eTheoryDeliveryTime = null;
            }
            if(bPurchasePlanTime.HasValue) {
                serachSQL.Append(" and pu.PurchasePlanTime>= to_date('" + bPurchasePlanTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')");
            }
            if(ePurchasePlanTime.HasValue) {
                serachSQL.Append(" and pu.PurchasePlanTime<= to_date('" + ePurchasePlanTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')");
            }
            if(bTheoryDeliveryTime.HasValue) {
                serachSQL.Append(" and pu.TheoryDeliveryTime>= to_date('" + bTheoryDeliveryTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')");
            }
            if(eTheoryDeliveryTime.HasValue) {
                serachSQL.Append(" and pu.TheoryDeliveryTime<= to_date('" + eTheoryDeliveryTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')");
            }
            SQL = string.Format(SQL, serachSQL, serachSQL);
            var search = ObjectContext.ExecuteStoreQuery<PartsPurchaseOrderFinish>(SQL).ToList();
            if(search.Count() > 0) {
                PartsPurchaseOrderFinish summary = new PartsPurchaseOrderFinish();
                // summary.Type = "汇总";
                summary.AllItem = search.Sum(r => r.AllItem);
                summary.ArriveItem = search.Sum(r => r.ArriveItem);
                summary.OnTime = search.Sum(r => r.OnTime);
                summary.OutTime = search.Sum(r => r.OutTime);
                summary.SevenUn = search.Sum(r => r.SevenUn);
                summary.EightUn = search.Sum(r => r.EightUn);
                summary.TwentyUn = search.Sum(r => r.TwentyUn);
                summary.OverTwentyUn = search.Sum(r => r.OverTwentyUn);
                summary.Undue = search.Sum(r => r.Undue);
                summary.StopAmount = search.Sum(r=>r.StopAmount);
                summary.Mzl = summary.ArriveItem==0? 0: Double.Parse(Math.Round((Decimal.Parse((summary.OnTime * 100).ToString()) /Decimal.Parse(summary.ArriveItem.ToString())), 2).ToString());
                summary.Rownum = 0;

                List<PartsPurchaseOrderFinish> returnList = new List<PartsPurchaseOrderFinish>();
                List<PartsPurchaseOrderFinish> itemList = new List<PartsPurchaseOrderFinish>();
                foreach(var item in search) {
                    itemList.Add(item);
                }
                if(isExport) {
                    int j = 0;
                    for(int i = 0; i < itemList.Count() + 1; i++) {
                        if(i == 0) {
                            returnList.Add(summary);
                        } else {
                            returnList.Add(itemList[j]);
                            j++;
                        }
                    }
                } else {
                    int j = 0;
                    int addItem = itemList.Count() + (itemList.Count() / 50) + 1;
                    for(int i = 0; i < addItem; i++) {
                        if(i == 0 || (i % 50) == 0) {
                            returnList.Add(summary);
                        } else {
                            returnList.Add(itemList[j]);
                            j++;
                        }
                    }
                } 
                return returnList;
            }
            return null;
        }
        public IEnumerable<PartsPurchaseOrderFinish> 供应商采购订单完成情况统计(DateTime? bPurchasePlanTime, DateTime? ePurchasePlanTime, DateTime? bTheoryDeliveryTime, DateTime? eTheoryDeliveryTime, string supplierName, bool isExport) {
            string SQL = @"select nvl(tt.month, yy.month) as month,
                           nvl(tt.allItem, 0) + nvl(yy.allItem, 0) as allItem, --采购订单总条目数
                           nvl(tt.arriveItem, 0) + nvl(yy.arriveItem, 0) as arriveItem, --采购订单到期总条目数
                           yy.onTime,
                           yy.outTime,
                           tt.sevenUn,
                           tt.eightUn,
                           tt.TwentyUn,
                           tt.OverTwentyUn,
                           yy.StopAmount,
                           nvl(tt.SupplierName, yy.SupplierName) as SupplierName,
                           tt.Undue,
                           (case
                             when (nvl(tt.ArriveItem, 0) + nvl(yy.ArriveItem, 0)) = 0 then
                              0
                             else
                              round(nvl(yy.OnTime, 0) /
                                    (nvl(tt.ArriveItem, 0) + nvl(yy.ArriveItem, 0)) * 100,
                                    2)
                           end) as mzl,
                           rownum
                      from (select
                                   mm.month,
                                   mm.SupplierName,
                                   sum(mm.allItem) as allItem,
                                   sum(mm.arriveItem) as arriveItem,
                                   sum(mm.sevenUn) as sevenUn,
                                   sum(mm.eightUn) as eightUn,
                                   sum(mm.TwentyUn) as TwentyUn,
                                   sum(mm.OverTwentyUn) as OverTwentyUn,
                                   sum(mm.Undue) as Undue
                              from (select 1 as allItem,
                                           pu.suppliername,
                                           to_char(pu.TheoryDeliveryTime, 'MM') as month,
                                           (case
                                             when  /*pu.SupplyConfirmQty <> 0 */
                                                         ( to_char(pu.TheoryDeliveryTime, 'yyyy-mm-dd') <to_char(sysdate , 'yyyy-mm-dd') and pu.supplyconfirmreason is null )
                                                then
                                              1
                                             else
                                              0
                                           end) as arriveItem, --采购订单到期总条目数
                                           (case
                                             when  -- pu.SupplyConfirmQty <> 0 and
                                                  to_char(pu.TheoryDeliveryTime, 'yyyy-mm-dd') >=
                                                  (to_char(sysdate - 7, 'yyyy-mm-dd')) and
                                                  to_char(pu.TheoryDeliveryTime, 'yyyy-mm-dd') <
                                                  (to_char(sysdate, 'yyyy-mm-dd')) then
                                              1
                                             else
                                              0
                                           end) as sevenUn, --7天未完成
                                           (case
                                             when --pu.SupplyConfirmQty <> 0 and
                                                  to_char(pu.TheoryDeliveryTime, 'yyyy-mm-dd') <
                                                  (to_char(sysdate - 7, 'yyyy-mm-dd')) and
                                                  to_char(pu.TheoryDeliveryTime, 'yyyy-mm-dd') >=
                                                  (to_char(sysdate - 14, 'yyyy-mm-dd')) then
                                              1
                                             else
                                              0
                                           end) as eightUn, --14天未完成
                                           (case
                                             when --pu.SupplyConfirmQty <> 0 and
                                                  to_char(pu.TheoryDeliveryTime, 'yyyy-mm-dd') <
                                                  (to_char(sysdate - 14, 'yyyy-mm-dd')) and
                                                  to_char(pu.TheoryDeliveryTime, 'yyyy-mm-dd') >=
                                                  (to_char(sysdate - 21, 'yyyy-mm-dd')) then
                                              1
                                             else
                                              0
                                           end) as TwentyUn, --21天未完成
                                           (case
                                             when --pu.SupplyConfirmQty <> 0 and
                                                  to_char(pu.TheoryDeliveryTime, 'yyyy-mm-dd') <
                                                  (to_char(sysdate - 21, 'yyyy-mm-dd')) then
                                              1
                                             else
                                              0
                                           end) as OverTwentyUn, -- 超过21天未完成,        
                                           (case
                                             when --pu.SupplyConfirmQty <> 0 and
                                                  to_char(pu.TheoryDeliveryTime, 'yyyy-mm-dd') >=
                                                  (to_char(sysdate, 'yyyy-mm-dd')) then
                                              1
                                             else
                                              0
                                           end) as Undue -- 未到期条目数
                                      from PurchaseOrderUnFinishDetail pu
                                     where 1 = 1 {0}
                                      ) mm
                             group by mm.month, mm.SupplierName) tt
                      full join (select sum(ff.allItem) as allItem,
                                        sum(ff.arriveItem) as arriveItem,
                                        sum(ff.onTime) as onTime,
                                        sum(ff.outTime) as outTime,
                                        sum(ff.StopAmount) as StopAmount,
                                        ff.month,
                                        ff.SupplierName
                                   from (select 1 as allItem,
                                                pu.SupplierName,
                                                to_char(pu.TheoryDeliveryTime, 'MM') as month,
                                                1 as arriveItem,
                                                pu.onlineqty,
                                                (case
                                                  when pu.onlineqty = 0 and
                                                       pu.SupplyConfirmQty > 0 and
                                                       to_char(pu.InboundTime, 'yyyy-mm-dd') <=
                                                       to_char(pu.TheoryDeliveryTime, 'yyyy-mm-dd') then
                                                   1
                                                  else
                                                   0
                                                end) as onTime, -- 按期完成条目数
                                                (case
                                                  when pu.onlineqty = 0 and
                                                       pu.SupplyConfirmQty > 0 and
                                                       to_char(pu.InboundTime, 'yyyy-mm-dd') >
                                                       to_char(pu.TheoryDeliveryTime, 'yyyy-mm-dd') then
                                                   1
                                                  else
                                                   0
                                                end) as outTime, --超期完成条目数
                                    (  case when  pu.SupplyConfirmReason is not null  then 1 else 0 end ) as StopAmount --终止数量
                                           from PurchaseOrderFinishedDetail pu
                                          where 1 = 1 {1}
                                           ) ff
                                  group by ff.month,  ff.SupplierName) yy
                        on tt.month = yy.month
                       and tt.SupplierName = yy.SupplierName
                     order by nvl(tt.month, yy.month),
                              nvl(tt.SupplierName, yy.SupplierName)";
            var serachSQL = new StringBuilder();
            if(bPurchasePlanTime.HasValue && bPurchasePlanTime.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
                bPurchasePlanTime = null;
            }
            if(ePurchasePlanTime.HasValue && ePurchasePlanTime.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
                ePurchasePlanTime = null;
            }
            if(bTheoryDeliveryTime.HasValue && bTheoryDeliveryTime.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
                bTheoryDeliveryTime = null;
            }
            if(eTheoryDeliveryTime.HasValue && eTheoryDeliveryTime.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
                eTheoryDeliveryTime = null;
            }
            if(!string.IsNullOrEmpty(supplierName)) {
                serachSQL.Append(" and pu.supplierName like '%" + supplierName + "%'");
            }
            if(bPurchasePlanTime.HasValue) {
                serachSQL.Append(" and pu.PurchasePlanTime>= to_date('" + bPurchasePlanTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')");
            }
            if(ePurchasePlanTime.HasValue) {
                serachSQL.Append(" and pu.PurchasePlanTime<= to_date('" + ePurchasePlanTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')");
            }
            if(bTheoryDeliveryTime.HasValue) {
                serachSQL.Append(" and pu.TheoryDeliveryTime>= to_date('" + bTheoryDeliveryTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')");
            }
            if(eTheoryDeliveryTime.HasValue) {
                serachSQL.Append(" and pu.TheoryDeliveryTime<= to_date('" + eTheoryDeliveryTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')");
            }
            var userInfo = Utils.GetCurrentUserInfo();
            if(userInfo.Id==0){
                throw new ValidationException("登录已超时，请重新登录");
            }
            var company = ObjectContext.Companies.Where(r => r.Id == userInfo.EnterpriseId).First();
            if(company.Type==(int)DcsCompanyType.配件供应商){
                serachSQL.Append(" and pu.SupplierPartCode = '" + company.Code + "'");
            }
            SQL = string.Format(SQL, serachSQL, serachSQL);
            var search = ObjectContext.ExecuteStoreQuery<PartsPurchaseOrderFinish>(SQL).ToList();
            if(search.Count() > 0) {
               
                PartsPurchaseOrderFinish summary = new PartsPurchaseOrderFinish();
                 summary.SupplierName = "汇总";
                summary.AllItem = search.Sum(r => r.AllItem);
                summary.ArriveItem = search.Sum(r => r.ArriveItem);
                summary.OnTime = search.Sum(r => r.OnTime);
                summary.OutTime = search.Sum(r => r.OutTime);
                summary.SevenUn = search.Sum(r => r.SevenUn);
                summary.EightUn = search.Sum(r => r.EightUn);
                summary.TwentyUn = search.Sum(r => r.TwentyUn);
                summary.OverTwentyUn = search.Sum(r => r.OverTwentyUn);
                summary.Undue = search.Sum(r => r.Undue);
                summary.StopAmount = search.Sum(r => r.StopAmount);
                summary.Mzl = summary.ArriveItem == 0 ? 0 : Double.Parse(Math.Round((Decimal.Parse((summary.OnTime * 100).ToString()) / Decimal.Parse(summary.ArriveItem.ToString())), 2).ToString());
                summary.Rownum = 0;

                List<PartsPurchaseOrderFinish> returnList = new List<PartsPurchaseOrderFinish>();
                List<PartsPurchaseOrderFinish> itemList = new List<PartsPurchaseOrderFinish>();
                foreach(var item in search) {
                    itemList.Add(item);
                }
                if(isExport) {
                    int j = 0;
                    for(int i = 0; i < itemList.Count()+1; i++) {
                        if(i == 0 ) {
                            returnList.Add(summary);
                        } else {
                            returnList.Add(itemList[j]);
                            j++;
                        }
                    }
                } else {
                    int j = 0;
                    int addItem = itemList.Count() + (itemList.Count() / 50) + 1;
                    for(int i = 0; i < addItem; i++) {
                        if(i == 0 || (i % 50) == 0) {
                            returnList.Add(summary);
                        } else {
                            returnList.Add(itemList[j]);
                            j++;
                        }
                    }
                }                
                return returnList;
            }
            return null;
        }
        public IEnumerable<PartsPurchaseOrderFinish> 采购单在途明细表(DateTime? bPurchasePlanTime, DateTime? ePurchasePlanTime, DateTime? bTheoryDeliveryTime, DateTime? eTheoryDeliveryTime, string kvFinishi, int? kvOverTime, string kvUndueTime) {
            var SQL = @"select *
                        from (select pu.*,
                       (case
                         when nvl(pu.inboundqty, 0) < pu.onlineqty and
                              (sysdate - pu.ExpectDeliveryTime) > 30 then
                          cast('超期30天' as varchar2(100))
                         when nvl(pu.inboundqty, 0) < pu.onlineqty and
                              (sysdate - pu.ExpectDeliveryTime) > 20 then
                          cast('超期20天' as varchar2(100))
                         when nvl(pu.inboundqty, 0) < pu.onlineqty and
                              (sysdate - pu.ExpectDeliveryTime) > 10 then
                          cast('超期10天' as varchar2(100))
                         when nvl(pu.inboundqty, 0) < pu.onlineqty and
                              (sysdate - pu.ExpectDeliveryTime) > 0 then
                          cast('超期10天内' as varchar2(100))
                       end) as OverTime,
                       cast((case
                              when pu.ExpectDeliveryTime > sysdate then
                               '差' || (round(pu.ExpectDeliveryTime - sysdate, 0)) || '天到期'
                              else
                               ''
                            end) as varchar2(100)) as UndueTime,
                       cast((case
                              when pu.onlineqty > 0 and pu.TheoryDeliveryTime < sysdate then
                               '超期未完成'
                              when pu.onlineqty = 0 and pu.inboundtime <= pu.TheoryDeliveryTime then
                               '按期完成'
                              when pu.onlineqty = 0 and pu.inboundtime > pu.TheoryDeliveryTime then
                               '超期完成' else '未到期未完成'
                            end) as varchar2(100)) as FinishDescribe,
                         Rownum
                              from (select b.*,
                                   sp.weight  as weight,
                                   sp.volume  as volume,
                                   f.salesprice,
                            (select to_char(wm_concat(PersonName))
                          from PersonnelSupplierRelation p
                         where p.SupplierId = c.PartsSupplierId
                           and p.status = 1) as PersonName
                              from PurchaseOrderUnFinishDetail b
                              join PartsPurchaseOrder c
                                on b.PurchaseCode = c.code
                              join PartsPurchaseOrderDetail d
                                on c.id = d.partspurchaseorderid
                               and b.SpareCode = d.sparepartcode
                              join sparepart sp
                                on d.sparepartid = sp.id
                              left join PartsSalesPrice f
                                on c.PartsSalesCategoryId = f.PartsSalesCategoryId
                               and f.status = 1
                               and f.sparepartid = sp.id
                               and c.branchid = f.branchid
                             where 1 = 1 {0}
                            union
                            select b.*,
                                   sp.weight  as weight,
                                   sp.volume  as volume,
                                   f.salesprice,
                        (select to_char(wm_concat(PersonName))
                          from PersonnelSupplierRelation p
                         where p.SupplierId = c.PartsSupplierId
                           and p.status = 1) as PersonName
                              from PurchaseOrderFinishedDetail b
                              join PartsPurchaseOrder c
                                on b.PurchaseCode = c.code
                              join PartsPurchaseOrderDetail d
                                on c.id = d.partspurchaseorderid
                               and b.SpareCode = d.sparepartcode
                              join sparepart sp
                                on d.sparepartid = sp.id
                              left join PartsSalesPrice f
                                on c.PartsSalesCategoryId = f.PartsSalesCategoryId
                               and f.status = 1
                               and f.sparepartid = sp.id
                               and c.branchid = f.branchid
                             where 1 = 1 and b.inboundtime is not null {1}) pu ) tt  where tt.FinishDescribe <> '按期完成'";
            var serachSQL = new StringBuilder();
            if(bPurchasePlanTime.HasValue && bPurchasePlanTime.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
                bPurchasePlanTime = null;
            }
            if(ePurchasePlanTime.HasValue && ePurchasePlanTime.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
                ePurchasePlanTime = null;
            }
            if(bTheoryDeliveryTime.HasValue && bTheoryDeliveryTime.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
                bTheoryDeliveryTime = null;
            }
            if(eTheoryDeliveryTime.HasValue && eTheoryDeliveryTime.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
                eTheoryDeliveryTime = null;
            }
            if(bPurchasePlanTime.HasValue) {
                serachSQL.Append(" and b.PurchasePlanTime>= to_date('" + bPurchasePlanTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')");
            }
            if(ePurchasePlanTime.HasValue) {
                serachSQL.Append(" and b.PurchasePlanTime<= to_date('" + ePurchasePlanTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')");
            }
            if(bTheoryDeliveryTime.HasValue) {
                serachSQL.Append(" and b.TheoryDeliveryTime>= to_date('" + bTheoryDeliveryTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')");
            }
            if(eTheoryDeliveryTime.HasValue) {
                serachSQL.Append(" and b.TheoryDeliveryTime<= to_date('" + eTheoryDeliveryTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')");
            }
            if(kvOverTime.HasValue) {
                if(kvOverTime == 1) {
                    serachSQL.Append("and nvl(b.inboundqty, 0) < b.onlineqty and (sysdate - b.ExpectDeliveryTime) > 0 and (sysdate - b.ExpectDeliveryTime)<7");
                }
                if(kvOverTime == 2) {
                    serachSQL.Append("and nvl(b.inboundqty, 0) < b.onlineqty and (sysdate - b.ExpectDeliveryTime) > 7 and (sysdate - b.ExpectDeliveryTime)<=14");
                }
                if(kvOverTime == 3) {
                    serachSQL.Append(" and nvl(b.inboundqty, 0) < b.onlineqty and (sysdate - b.ExpectDeliveryTime) > 14 and (sysdate - b.ExpectDeliveryTime)<=21");
                }
                if(kvOverTime == 4) {
                    serachSQL.Append("and nvl(b.inboundqty, 0) < b.onlineqty and (sysdate - b.ExpectDeliveryTime) > 21 ");
                }
            }
            SQL = string.Format(SQL, serachSQL, serachSQL);
            if(!string.IsNullOrEmpty(kvFinishi)){
                SQL = SQL + " and tt.FinishDescribe='" + kvFinishi+"'";
            }
          
            if(!string.IsNullOrEmpty(kvUndueTime)) {
                SQL = SQL + " and tt.UndueTime='" + kvUndueTime + "'";
            }
            var search = ObjectContext.ExecuteStoreQuery<PartsPurchaseOrderFinish>(SQL).ToList();

            return search;
        }
        public IEnumerable<PartsPurchaseOrderFinish> 采购单短供明细(DateTime? bPurchasePlanTime, DateTime? ePurchasePlanTime, DateTime? bTheoryDeliveryTime, DateTime? eTheoryDeliveryTime) {
            var SQL = @"select *
                      from (select tt.code as PurchaseCode,
                                   tt.sparepartcode as SpareCode,
                                   tt.PartsPurchasePlanCode as PurchasePlanCode,
                                   tt.PartsSupplierCode as SupplierPartCode,
                                   tt.partssuppliername as SupplierName,
                                   tt.sparepartname as MapName,
                                   tt.referencecode as SihCode,
                                   tt.partabc as PartABC,
                                   tt.PlanAmount as PurchasePlanQty,
                                   tt.Confirmedamount as SupplyConfirmQty,
                                   tt.ShortSupReason as SupplyConfirmReason,
                                   tt.ShippingAmount as SupplyShippingQty,
                                   tt.InspectedQuantity as InboundQty,
                                   tt.Confirmedamount - nvl(tt.InspectedQuantity, 0) -
                                   nvl(tt.CompletionQuantity, 0) as OwningQty,
                                   tt.PartsPurchaseOrderDate as PurchaseCreateTime,
                                   tt.ConfirmationDate as SupplyConfirmTime,
                                   (nvl(tt.SubmitTime, tt.PartsPurchaseOrderDate) +
                                   nvl(tt.ArrivalCycle, 0) + nvl(tt.OrderCycle, 0)) as TheoryDeliveryTime, --理论到货时间
                                   tt.PurchasePlanTime as PurchasePlanTime,
                                    tt.PersonName,
                                   Rownum
                              from (select pp.code, --  采购单号    
                                           pl.code as PartsPurchasePlanCode, --采购计划单号       
                                           pp.PartsSupplierCode, --供应商图号
                                           pp.partssuppliername, --供应商名称
                                           pd.sparepartcode,
                                           pd.sparepartname,
                                           sp.referencecode, --红岩号
                                           pb.partabc, --配件属性
                                           ppd.PlanAmount, -- 采购计划数
                                           pd.Confirmedamount, -- 供应商确认量
                                           kv.value ShortSupReason,
                                           pd.ShippingAmount, --发运数量
                                           (select sum(InspectedQuantity)
                                              from PartsInboundCheckBillDetail a
                                             where exists
                                             (select 1
                                                      from PartsInboundCheckBill tmp
                                                     where a.partsinboundcheckbillid = tmp.id
                                                       and tmp.OriginalRequirementBillType = 4
                                                       and tmp.originalrequirementbillid = pp.id)
                                               and a.sparepartid = pd.sparepartid) as InspectedQuantity, --入库数量   配件入库检验单清单.检验量
                                           (select sum(nvl(a.PlannedAmount, 0)) -
                                                   sum(nvl(a.InspectedQuantity, 0))
                                              from PartsInboundPlanDetail a
                                             where exists
                                             (select 1
                                                      from PartsInboundPlan b
                                                     where a.PartsInboundPlanId = b.id
                                                       and b.originalrequirementbillid = pp.id
                                                       and b.originalrequirementbilltype = 4
                                                       and b.status = 3)
                                               and a.sparepartid = pd.Sparepartid) as CompletionQuantity, --入库强制完成数量  入库计划清单。计划量-检验量  计划单状态为终止
                                           pp.CreateTime as PartsPurchaseOrderDate, --采购订单生成时间
                                           pp.ApproveTime as ConfirmationDate, -- 供应商确认时间
                                           (select max(a.CreateTime)
                                              from SupplierShippingOrder a
                                             where exists (select 1
                                                      from SupplierShippingDetail tmp
                                                     where tmp.SupplierShippingOrderId = a.id
                                                       and tmp.SparePartId = pd.SparePartId)
                                               and a.PartsPurchaseOrderId = pp.id) as ShippingDate, --供应商发运时间  供应商发运单最大时间
                                           nvl(psRelation.OrderCycle, 0) as OrderCycle, -- 供应商发货周期
                                           nvl(relation.ArrivalCycle, 0) as ArrivalCycle, --物流周期
                                           pl.createtime as PurchasePlanTime,
                                           pp.SubmitTime,
                                          (select  to_char(wm_concat(PersonName))
                                          from PersonnelSupplierRelation p
                                         where p.SupplierId = pp.PartsSupplierId
                                           and p.status = 1) as PersonName
                                      from PartsPurchaseOrder pp
                                      join partspurchaseorderdetail pd
                                        on pp.id = pd.partspurchaseorderid
                                      join partspurchaseplan pl
                                        on pp.originalrequirementbillid = pl.id
                                       and pp.originalrequirementbillcode = pl.code
                                      join PartsPurchasePlanDetail ppd
                                        on pl.id = ppd.purchaseplanid
                                       and pd.sparepartid = ppd.sparepartid
                                      join sparepart sp
                                        on pd.sparepartid = sp.id
                                      join partsbranch pb
                                        on sp.id = pb.partid
                                       and pb.status = 1
                                      left join PartsSupplier supplier
                                        on pp.PartsSupplierId = supplier.id
                                       and supplier.status = 1
                                      left join PartsSupplierRelation psRelation
                                        on supplier.id = psRelation.SupplierId
                                       and pd.SparePartId = psRelation.PartId
                                       and psRelation.IsPrimary = 1
                                      left join BranchSupplierRelation relation
                                        on supplier.id = relation.SupplierId
                                       and relation.status = 1
                                      left join keyvalueitem kv
                                        on pd.shortsupreason = kv.key
                                       and kv.name = 'PartsPurchaseOrderDetail_ShortSupReason'
                                     where pp.status in (3, 4, 5, 6)
                                       and pd.confirmedamount < pd.orderamount) tt) yy where 1=1 {0}";
            var serachSQL = new StringBuilder();
            if(bPurchasePlanTime.HasValue && bPurchasePlanTime.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
                bPurchasePlanTime = null;
            }
            if(ePurchasePlanTime.HasValue && ePurchasePlanTime.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
                ePurchasePlanTime = null;
            }
            if(bTheoryDeliveryTime.HasValue && bTheoryDeliveryTime.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
                bTheoryDeliveryTime = null;
            }
            if(eTheoryDeliveryTime.HasValue && eTheoryDeliveryTime.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
                eTheoryDeliveryTime = null;
            }
            if(bPurchasePlanTime.HasValue) {
                serachSQL.Append(" and yy.PurchasePlanTime>= to_date('" + bPurchasePlanTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')");
            }
            if(ePurchasePlanTime.HasValue) {
                serachSQL.Append(" and yy.PurchasePlanTime<= to_date('" + ePurchasePlanTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')");
            }
            if(bTheoryDeliveryTime.HasValue) {
                serachSQL.Append(" and yy.TheoryDeliveryTime>= to_date('" + bTheoryDeliveryTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')");
            }
            if(eTheoryDeliveryTime.HasValue) {
                serachSQL.Append(" and yy.TheoryDeliveryTime<= to_date('" + eTheoryDeliveryTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')");
            }
            SQL = string.Format(SQL, serachSQL);
            var search = ObjectContext.ExecuteStoreQuery<PartsPurchaseOrderFinish>(SQL).ToList();
            return search;
        }
    }
    partial class DcsDomainService {
        public IEnumerable<PartsPurchaseOrderFinish> 采购订单完成情况统计(DateTime? bPurchasePlanTime, DateTime? ePurchasePlanTime, DateTime? bTheoryDeliveryTime, DateTime? eTheoryDeliveryTime, bool isExport) {
            return new PartsPurchaseOrderFinishAch(this).采购订单完成情况统计(bPurchasePlanTime, ePurchasePlanTime, bTheoryDeliveryTime, eTheoryDeliveryTime,isExport);
        }
        public IEnumerable<PartsPurchaseOrderFinish> 供应商采购订单完成情况统计(DateTime? bPurchasePlanTime, DateTime? ePurchasePlanTime, DateTime? bTheoryDeliveryTime, DateTime? eTheoryDeliveryTime, string supplierName, bool isExport) {
            return new PartsPurchaseOrderFinishAch(this).供应商采购订单完成情况统计(bPurchasePlanTime, ePurchasePlanTime, bTheoryDeliveryTime, eTheoryDeliveryTime, supplierName, isExport);
        }
        public IEnumerable<PartsPurchaseOrderFinish> 采购单在途明细表(DateTime? bPurchasePlanTime, DateTime? ePurchasePlanTime, DateTime? bTheoryDeliveryTime, DateTime? eTheoryDeliveryTime, string kvFinishi, int? kvOverTime, string kvUndueTime) {
            return new PartsPurchaseOrderFinishAch(this).采购单在途明细表(bPurchasePlanTime, ePurchasePlanTime, bTheoryDeliveryTime, eTheoryDeliveryTime, kvFinishi, kvOverTime, kvUndueTime);
        }
        public IEnumerable<PartsPurchaseOrderFinish> 采购单短供明细(DateTime? bPurchasePlanTime, DateTime? ePurchasePlanTime, DateTime? bTheoryDeliveryTime, DateTime? eTheoryDeliveryTime) {
            return new PartsPurchaseOrderFinishAch(this).采购单短供明细(bPurchasePlanTime, ePurchasePlanTime, bTheoryDeliveryTime, eTheoryDeliveryTime);

        }
    }
}
