﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
using System.Configuration;
using System.Data;
using System.Text;
using Devart.Data.Oracle;
using NPOI.SS.Formula.Functions;
using System.Transactions;
using System.Data.Objects.SqlClient;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsPurchaseOrderForDirectAch : DcsSerivceAchieveBase {
        public PartsPurchaseOrderForDirectAch(DcsDomainService domainService)
            : base(domainService) {
        }
        public IEnumerable<PartsPurchaseOrderForDirect> 直供采购订单完成情况统计(string partsSupplierName, string code, string sparepartCode, int? finishStatus, int? status, string originalRequirementBillCode, string personName, DateTime? bSubmitTime, DateTime? eSubmitTime) {
            string SQL = @"select rownum,ty.* from (select 
                           PartsSupplierCode,
                           partssuppliername,
                           code,
                           status,
                           PartsSalesOrderTypeName,
                           remark,
                           PersonName,
                           creatorname,
                           OriginalRequirementBillCode,
                           SupplierPartCode,
                           sparepartcode,
                           sparepartname,
                           ABCStrategy,
                           OrderAmount,
                           confirmedamount,
                           ShortSupReason,
                           Quantity,
                           shipCon,
                           inspectedquantity,
                           orderShippingTime,
                           closername,
                           IsTransSap,
                           submittime,
                           approvetime,
                           ShippingDate, --供应商发运时间  供应商发运单最大时间
                           ShippingDate+nvl(OrderCycle,0) as ExpectedArrivalDate,
                           ConfirmationTime, --中心库收货确认时间
                           InStatus, --入库状态
                           LogisticCompany, --物流公司
                           shippingmethod, --发运方式
                           DeliveryBillNumber,
                           SIHInboundDate, --配件公司入库时间
                           ReceivingCompanyName,
                           ReceivingAddress,
                           contactperson,
                           contactphone,
                           shipRemark,
                           --1:未到考核期  2:超期未满足 3:按期满足  4:超期满足  5:不计考核       
                            (case
                             when appointshippingdays is null then
                              null
                             when ShortSupReason is not null or
                                  (status = 7 and CloserName is not null) then
                              5
                             when (Quantity < confirmedamount or  status=2) and
                                   trunc(sysdate) <= trunc(orderShippingTime) then
                              1
                             when (Quantity < confirmedamount or status in ( 2,3, 4)) and
                                  trunc(sysdate) > trunc(orderShippingTime) then
                              2
                             when Quantity = confirmedamount and
                                  trunc(ShippingDate) <= trunc(orderShippingTime) then
                              3
                             when Quantity = confirmedamount and
                                  trunc(ShippingDate) > trunc(orderShippingTime) then
                              4
                           end) as FinishStatus
                      from (select po.PartsSupplierCode,
                                   po.partssuppliername,
                                   po.code,
                                   po.status,
                                   ps.PartsSalesOrderTypeName,
                                   po.remark,
                                   pr.PersonName,
                                   po.creatorname,
                                   ps.code as OriginalRequirementBillCode,
                                   pd.SupplierPartCode,
                                   pd.sparepartcode,
                                   pd.sparepartname,
                                   psd.ABCStrategy,
                                   pd.OrderAmount,
                                   pd.confirmedamount,
                                   pd.ShortSupReason,
                                   nvl(ty.Quantity, 0) as Quantity,
                                   ty.shipCon,
                                   ppi.inspectedquantity,
                                   (case
                                     when ps.originalrequirementbillid is null and
                                          (ps.PartsSalesOrderTypeName = '正常订单' or
                                          ps.PartsSalesOrderTypeName = '油品订单') then
                                          po.createtime + nvl(cs.appointshippingdays, 0)
                                     else
                                      po.createtime + 1
                                   end) as orderShippingTime,
                                   po.closername,
                                   nvl(po.IsTransSap,0) as IsTransSap,
                                   po.createtime as  submittime,
                                   po.approvetime,
                                   ship.ShippingDate, --供应商发运时间  供应商发运单最大时间
                                   ship.confirmationtime as ConfirmationTime, --中心库收货确认时间
                                   psRelation.OrderCycle as OrderCycle, -- 供应商发货周期
                                   po.InStatus, --入库状态
                                   ship.LogisticCompany, --物流公司
                                   ship.shippingmethod, --发运方式
                                   ship.DeliveryBillNumber,
                                   pip.SIHInboundDate, --配件公司入库时间
                                   po.ReceivingCompanyName,
                                   po.ReceivingAddress,
                                   ps.contactperson,
                                   ps.contactphone,
                                   ship.shipRemark,cs.appointshippingdays
                              from partspurchaseorder po
                              join partspurchaseorderdetail pd
                                on po.id = pd.partspurchaseorderid
                              join partssalesorder ps
                                on po.originalrequirementbillid = ps.id
                               and po.originalrequirementbilltype = 1
                              join partssalesorderdetail psd
                                on ps.id = psd.partssalesorderid
                               and pd.sparepartid = psd.sparepartid
                              left join PersonnelSupplierRelation pr
                                on po.partssupplierid = pr.supplierid
                               and pr.status = 1
                              left join (select *
                                          from (select sso.createtime as ShippingDate, --供应商发运时间  供应商发运单最大时间
                                                       sso.confirmationtime as ConfirmationTime, --中心库收货确认时间
                                                       sso.LogisticCompany, --物流公司
                                                       sso.shippingmethod, --发运方式
                                                       sso.DeliveryBillNumber, --发运单号
                                                       sso.remark as shipRemark,
                                                       sso.id as shippingId,
                                                       sso.code,
                                                       sso.partspurchaseorderid,
                                                       shd.sparepartid,
                                                       row_number() over(partition by sso.partspurchaseorderid, shd.sparepartid order by sso.createtime desc) as ydy
                                                  from SupplierShippingOrder sso
                                                  join suppliershippingdetail shd
                                                    on sso.id = shd.suppliershippingorderid
                                                 where sso.status <> 99) yy
                                         where yy.ydy = 1) ship
                                on po.id = ship.partspurchaseorderid
                               and pd.sparepartid = ship.sparepartid
                              left join (select sum(ckd.inspectedquantity) as inspectedquantity,
                                               ck.OriginalRequirementBillCode,
                                               ckd.sparepartid
                                          from partsinboundcheckbill ck
                                          join partsinboundcheckbilldetail ckd
                                            on ck.id = ckd.partsinboundcheckbillid
                                         where ck.originalrequirementbilltype = 4
                                         group by ck.OriginalRequirementBillCode,
                                                  ckd.sparepartid) ppi
                                on po.code = ppi.OriginalRequirementBillCode
                               and pd.sparepartid = ppi.sparepartid
                              left join PartsSupplierRelation psRelation
                                on po.PartsSupplierId = psRelation.SupplierId
                               and pd.SparePartId = psRelation.PartId
                              left join CustomerSupplyInitialFeeSet cs
                                on po.partssupplierid = cs.supplierid
                               and cs.customerid = po.ReceivingCompanyId
                              left join (select max(pip.createtime) as SIHInboundDate,
                                               pip.sourceid,
                                               pip.sourcecode
                                          from PartsInboundPlan pip
                                         group by pip.sourceid, pip.sourcecode) pip
                                on ship.shippingId = pip.sourceid
                               and ship.code = pip.sourcecode
                              left join (select sum(sh.confirmedamount) as shipCon,
                                               sum(Quantity) as Quantity,
                                               ss.partspurchaseorderid,
                                               sh.sparepartid
                                          from SupplierShippingOrder ss
                                          join suppliershippingdetail sh
                                            on ss.id = sh.suppliershippingorderid
                                         where ss.status <> 99
                                         group by ss.partspurchaseorderid, sh.sparepartid) ty
                                on ty.partspurchaseorderid = po.id
                               and ty.sparepartid = pd.sparepartid
                             where po.ifdirectprovision = 1
                               and po.status not in (1, 99) {0} )) ty where 1=1 {1}   ";
            var serachSQL = new StringBuilder();
            var serachSQL1 = new StringBuilder();
            if(bSubmitTime.HasValue && bSubmitTime.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
                bSubmitTime = null;
            }
            if(eSubmitTime.HasValue && eSubmitTime.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
                eSubmitTime = null;
            }
            if(bSubmitTime.HasValue) {
                serachSQL.Append(" and po.CreateTime>= to_date('" + bSubmitTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')");
            }
            if(eSubmitTime.HasValue) {
                serachSQL.Append(" and po.CreateTime<= to_date('" + eSubmitTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')");
            }
            if(!string.IsNullOrEmpty(partsSupplierName)) {
                serachSQL.Append(" and po.partsSupplierName like '%" + partsSupplierName + "%'");
            }
            if(!string.IsNullOrEmpty(code)) {
                serachSQL.Append(" and lower(po.code) like '%" + code.ToLower() + "%'");
            }
            if(!string.IsNullOrEmpty(sparepartCode)) {
                serachSQL.Append(" and lower(pd.sparepartCode) like '%" + sparepartCode.ToLower() + "%'");
            }
            if(finishStatus.HasValue) {
                serachSQL1.Append(" and ty.FinishStatus=" + finishStatus);
            }
            if(status.HasValue) {
                serachSQL.Append(" and po.status=" + status);
            }
            if(!string.IsNullOrEmpty(originalRequirementBillCode)) {
                serachSQL.Append(" and lower(ps.code) like '%" + originalRequirementBillCode.ToLower() + "%'");
            }
            if(!string.IsNullOrEmpty(personName)) {
                serachSQL.Append(" and lower(pr.personName) like '%" + personName.ToLower() + "%'");
            }

            SQL = string.Format(SQL, serachSQL, serachSQL1);
            var search = ObjectContext.ExecuteStoreQuery<PartsPurchaseOrderForDirect>(SQL).ToList();
            return search;
        }
    }
    partial class DcsDomainService {
        public IEnumerable<PartsPurchaseOrderForDirect> 直供采购订单完成情况统计(string partsSupplierName, string code, string sparepartCode, int? finishStatus, int? status, string originalRequirementBillCode, string personName, DateTime? bSubmitTime, DateTime? eSubmitTime) {
            return new PartsPurchaseOrderForDirectAch(this).直供采购订单完成情况统计(partsSupplierName, code, sparepartCode, finishStatus, status, originalRequirementBillCode, personName, bSubmitTime, eSubmitTime);
        }
    }
}
