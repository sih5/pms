﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;


namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsPurchasePlanAch : DcsSerivceAchieveBase {
        public PartsPurchasePlanAch(DcsDomainService domainService)
            : base(domainService) {
        }
        internal void InsertPartsPurchasePlanValidate(PartsPurchasePlan partsPurchasePlan) {
            var userInfo = Utils.GetCurrentUserInfo();
            partsPurchasePlan.CreatorId = userInfo.Id;
            partsPurchasePlan.CreatorName = userInfo.Name;
            partsPurchasePlan.CreateTime = DateTime.Now;

            if(string.IsNullOrWhiteSpace(partsPurchasePlan.Code) || partsPurchasePlan.Code == GlobalVar.ASSIGNED_BY_SERVER)
                partsPurchasePlan.Code = CodeGenerator.Generate("PartsPurchasePlan", partsPurchasePlan.BranchCode);
        }

        internal void UpdatePartsPurchasePlanValidate(PartsPurchasePlan partsPurchasePlan) {
            var userInfo = Utils.GetCurrentUserInfo();
            partsPurchasePlan.ModifierId = userInfo.Id;
            partsPurchasePlan.ModifierName = userInfo.Name;
            partsPurchasePlan.ModifyTime = DateTime.Now;
        }
        public void InsertPartsPurchasePlan(PartsPurchasePlan partsPurchasePlan) {
            InsertToDatabase(partsPurchasePlan);
            var partsPurchasePlanDetails = ChangeSet.GetAssociatedChanges(partsPurchasePlan, v => v.PartsPurchasePlanDetails, ChangeOperation.Insert);
            foreach(PartsPurchasePlanDetail partsPurchasePlanDetail in partsPurchasePlanDetails) {
                InsertToDatabase(partsPurchasePlanDetail);
            }
            //this.InsertPartsPurchasePlanValidate(partsPurchasePlan);
        }

        public void UpdatePartsPurchasePlan(PartsPurchasePlan partsPurchasePlan) {
            partsPurchasePlan.PartsPurchasePlanDetails.Clear();
            UpdateToDatabase(partsPurchasePlan);
            var partsPurchasePlanDetails = ChangeSet.GetAssociatedChanges(partsPurchasePlan, v => v.PartsPurchasePlanDetails);
            foreach(PartsPurchasePlanDetail partsPurchasePlanDetail in partsPurchasePlanDetails) {
                switch(ChangeSet.GetChangeOperation(partsPurchasePlanDetail)) {
                    case ChangeOperation.Insert:
                        InsertToDatabase(partsPurchasePlanDetail);
                        break;
                    case ChangeOperation.Update:
                        UpdateToDatabase(partsPurchasePlanDetail);
                        break;
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(partsPurchasePlanDetail);
                        break;
                }
            }
            this.UpdatePartsPurchasePlanValidate(partsPurchasePlan);
        }

        //[Query(HasSideEffects = true)]
        //public IQueryable<SupplierShippingDetail> GetSupplierShippingDetailById(int[] supplierShippingOrderId)
        //{
        //    return ObjectContext.SupplierShippingDetails.Where(r => supplierShippingOrderId.Any(v => v == r.SupplierShippingOrderId)).Include("SupplierShippingOrder").OrderBy(r => r.SparePartId);
        //}

        public IQueryable<PartsPurchasePlan> GetPartsPurchasePlanOrder(string sparePartCode) {
            if(string.IsNullOrEmpty(sparePartCode)) {
                return ObjectContext.PartsPurchasePlans.Include("PartsPurchasePlanDetails").Include("PartsPurchaseOrderType").OrderByDescending(r => r.Id);
            } else {
                return ObjectContext.PartsPurchasePlans.Include("PartsPurchasePlanDetails").Include("PartsPurchaseOrderType").OrderByDescending(r => r.Id).Where(o => o.PartsPurchasePlanDetails.Any(p => p.SparePartCode.IndexOf(sparePartCode) >= 0));
            }
            #region 此方式 作废，问题原因：修改界面 第一次进入订单类型不显示，第二次后方显示
            //var userInfo = Utils.GetCurrentUserInfo();
            //var partsPurchasePlans = from p in ObjectContext.PartsPurchasePlans
            //                         join t in ObjectContext.PartsPurchaseOrderTypes on p.PartsPlanTypeId equals t.Id into temp1
            //                         from t1 in temp1.DefaultIfEmpty()
            //                         select new VirtualPartsPurchasePlan
            //                         {
            //                             Id = p.Id,
            //                             Code = p.Code,
            //                             BranchId = p.BranchId,
            //                             BranchCode = p.BranchCode,
            //                             BranchName = p.BranchName,
            //                             WarehouseId = p.WarehouseId,
            //                             WarehouseCode = p.WarehouseCode,
            //                             WarehouseName = p.WarehouseName,
            //                             PartsPlanTypeId = (int)p.PartsPlanTypeId,
            //                             PlanTypeName = t1.Name,
            //                             Status = p.Status,
            //                             Memo = p.Memo,
            //                             CreatorId = p.CreatorId,
            //                             CreatorName = p.CreatorName,
            //                             CreateTime = p.CreateTime,
            //                             ModifierId = p.ModifierId,
            //                             ModifierName = p.ModifierName,
            //                             ModifyTime = p.ModifyTime,
            //                             AbandonerId = p.AbandonerId,
            //                             AbandonerName = p.AbandonerName,
            //                             AbandonTime = p.AbandonTime,
            //                             SubmitterId = p.SubmitterId,
            //                             SubmitterName = p.SubmitterName,
            //                             SubmitTime = p.SubmitTime,
            //                             ApproverId = p.ApproverId,
            //                             ApproverName = p.ApproverName,
            //                             ApproveTime = p.ApproveTime,
            //                             CloserId = p.CloserId,
            //                             CloserName = p.CloserName,
            //                             CloseTime = p.CloseTime,
            //                             PartsSalesCategoryId = p.PartsSalesCategoryId,
            //                             PartsSalesCategoryName = p.PartsSalesCategoryName,
            //                             ApproveMemo = p.ApproveMemo,
            //                             CloseMemo = p.CloseMemo
            //                         };
            //return partsPurchasePlans.OrderByDescending(entity => entity.CreateTime); 
            #endregion
        }

        //初审、终审进入界面查询方法
        public PartsPurchasePlan GetPartsPurchasePlansWithDetailsById(int id) {
            var dbPartsPurchasePlan = ObjectContext.PartsPurchasePlans.Include("PartsPurchaseOrderType").SingleOrDefault(entity => entity.Id == id && entity.Status != (int)DcsPurchasePlanOrderStatus.作废);
            if(dbPartsPurchasePlan != null) {
                var dbDetails = ObjectContext.PartsPurchasePlanDetails.Where(e => e.PurchasePlanId == id).OrderBy(e => e.SerialNumber).ToArray();
            }
            return dbPartsPurchasePlan;
        }
        public PartsPurchasePlan GetPartsPurchasePlansById(int id) {
            var dbPartsPurchasePlan = ObjectContext.PartsPurchasePlans.Include("PartsPurchasePlanDetails").Include("PartsPurchaseOrderType").SingleOrDefault(entity => entity.Id == id);
            return dbPartsPurchasePlan;
        }

        //生成采购订单
        internal void InsertPartsPurchaseOrderForPlanValidate(PartsPurchaseOrder partsPurchaseOrder) {
            var userInfo = Utils.GetCurrentUserInfo();
            partsPurchaseOrder.CreatorId = userInfo.Id;
            partsPurchaseOrder.CreatorName = userInfo.Name;
            partsPurchaseOrder.CreateTime = DateTime.Now;

            if(string.IsNullOrWhiteSpace(partsPurchaseOrder.Code) || partsPurchaseOrder.Code == GlobalVar.ASSIGNED_BY_SERVER)
                partsPurchaseOrder.Code = CodeGenerator.Generate("PartsPurchaseOrder", partsPurchaseOrder.BranchCode);
        }


    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               

        //[Query(HasSideEffects = true)]
        //public IQueryable<SupplierShippingDetail> GetSupplierShippingDetailById(int[] supplierShippingOrderId)
        //{
        //    return new SupplierShippingDetailAch(this).GetSupplierShippingDetailById(supplierShippingOrderId);
        //}

        public IQueryable<PartsPurchasePlan> GetPartsPurchasePlanOrder(string sparePartCode) {
            return new PartsPurchasePlanAch(this).GetPartsPurchasePlanOrder(sparePartCode);
        }
        public void InsertPartsPurchasePlan(PartsPurchasePlan partsPurchasePlan) {
            new PartsPurchasePlanAch(this).InsertPartsPurchasePlan(partsPurchasePlan);
        }
        public void UpdatePartsPurchasePlan(PartsPurchasePlan partsPurchasePlan) {
            new PartsPurchasePlanAch(this).UpdatePartsPurchasePlan(partsPurchasePlan);
        }

        public PartsPurchasePlan GetPartsPurchasePlansWithDetailsById(int id) {
            return new PartsPurchasePlanAch(this).GetPartsPurchasePlansWithDetailsById(id);
        }

        public PartsPurchasePlan GetPartsPurchasePlansById(int id) {
            return new PartsPurchasePlanAch(this).GetPartsPurchasePlansById(id);
        }
    }
}


