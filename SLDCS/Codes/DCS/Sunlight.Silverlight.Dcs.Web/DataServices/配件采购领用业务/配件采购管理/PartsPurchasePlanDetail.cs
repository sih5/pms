﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Devart.Data.Oracle;

namespace Sunlight.Silverlight.Dcs.Web
{
    partial class PartsPurchasePlanDetailAch : DcsSerivceAchieveBase
    {
        public PartsPurchasePlanDetailAch(DcsDomainService domainService)
            : base(domainService)
        {
        }
        /// <summary>
        /// 主界面配件明细查看使用
        /// </summary>
        /// <returns></returns>
        public IQueryable<PartsPurchasePlanDetail> GetPartsPurchasePlanDetailsWithOrder()
        {
            return ObjectContext.PartsPurchasePlanDetails.Include("PartsPurchasePlan").OrderBy(entity => entity.SerialNumber);
        }
        /// <summary>
        /// 查询配件信息
        /// </summary>
        /// <returns></returns>
        public IQueryable<PartsPurchasePlanDetail> GetPartsPurchasePlanDetailsWithSpareParts()
        {
            return ObjectContext.PartsPurchasePlanDetails.Include("SparePart").OrderBy(r => r.id);
        }

        /// <summary>
        /// 根据sql查询采购清单上一次采购变更申请单参考价格
        /// </summary>
        /// <param name="partsSalesCategoryId">品牌</param>
        /// <param name="partId">配件</param>
        /// <param name="createTime">创建时间</param>
        /// <returns></returns>
        public ReferencepriceRecord GetPartsPurchasePlanDetailsByExecuteSql(int partsSalesCategoryId, int partId, DateTime? createTime)
        {
            var sqlBuilder = new StringBuilder();
            sqlBuilder.Append(@"select * from (select b.referenceprice,
                  dense_rank() over(order by a.approvetime desc) rank
             from PartsPurchasePricingDetail b
            inner join PartsPurchasePricingChange a
               on a.id = b.parentid
            where a.partssalescategoryid = :partsSalesCategoryId
              and a.approvetime <= :createTime
              and b.ValidFrom <=:createTime
              and b.validto >=:createTime
              and a.status = 2
              and b.partid= :partId
            ) where rank =1");
            var paramaterList = new List<Object>();
            paramaterList.Add(new OracleParameter("partsSalesCategoryId", partsSalesCategoryId));
            paramaterList.Add(new OracleParameter("partId", partId));
            paramaterList.Add(new OracleParameter("createTime", createTime));
            var sql = sqlBuilder.ToString();
            var search = ObjectContext.ExecuteStoreQuery<ReferencepriceRecord>(sql, paramaterList.ToArray());
            return search.SingleOrDefault();
        }

        /// <summary>
        /// 记录采购清单申请变更前价格
        /// </summary>
        public class ReferencepriceRecord
        {
            public decimal? ReferencePrice
            {
                get;
                set;
            }
            public int? Rank
            {
                get;
                set;
            }
        }

    }

    partial class DcsDomainService
    {
        public IQueryable<PartsPurchasePlanDetail> GetPartsPurchasePlanDetailsWithOrder()
        {
            return new PartsPurchasePlanDetailAch(this).GetPartsPurchasePlanDetailsWithOrder();
        }

        public IQueryable<PartsPurchasePlanDetail> GetPartsPurchasePlanDetailsWithSpareParts()
        {
            return new PartsPurchasePlanDetailAch(this).GetPartsPurchasePlanDetailsWithSpareParts();
        }
    }
}
