﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
using System.Configuration;
using System.Data;
using System.Text;
using Devart.Data.Oracle;
using NPOI.SS.Formula.Functions;
using System.Transactions;
using System.Data.Objects.SqlClient;
namespace Sunlight.Silverlight.Dcs.Web
{
    partial class PartsPurchasePlanReportAch : DcsSerivceAchieveBase
    {
        public PartsPurchasePlanReportAch(DcsDomainService domainService)
            : base(domainService)
        {
        }
        public IEnumerable<PartsPurchasePlanReport> GetPartsPurchasePlanReports(string sparePartCode, string partsSupplierName, int? status, int? partsPurchaseOrderStatus, DateTime? bPartsPurchasePlanTime, DateTime? ePartsPurchasePlanTime, DateTime? bPartsPurchaseOrderDate, DateTime? ePartsPurchaseOrderDate, bool? isOnWayNumber, string remark, string supplierPartCode,DateTime? bSubmitTime,DateTime? eSubmitTime)
        {
            string SQL = @"select a.id,
       a.code,
       a.PartsPurchaseOrderId,
       a.PartsPurchaseOrderCode,
       a.sparepartcode,
       a.sparepartname,
       a.sparepartid,
       a.referencecode,
       a.PartABC,
       a.PlanAmount,
       a.Confirmedamount,
       a.Shortsupreason,
       a.Shippingamount,
       a.InspectedQuantity, --入库数量
       a.CompletionQuantity, --入库强制完成数量
         (case
         when a.partsSupplierCode in ('1000002367', '1000003829') then
          (case
            when a.PartsPurchaseOrderStatus in (1, 2, 3, 4) then
             a.PlanAmount
            when a.PartsPurchaseOrderStatus = 99 then
             0
            when a.PartsPurchaseOrderStatus = 7 then
             (case
               when days > 60 then
                0
               else
                (nvl(a.PlanAmount, 0) - nvl(a.InspectedQuantity, 0) -
                nvl(a.CompletionQuantity, 0) - finishiQty)
             end)
            else
             (a.PlanAmount - nvl(a.InspectedQuantity, 0) -
             nvl(a.CompletionQuantity, 0) - finishiQty)
          end)
         else
          (case
            when a.PartsPurchaseOrderStatus = 99 then
             0
            when a.PartsPurchaseOrderStatus = 1 then
             a.PlanAmount
            when a.PartsPurchaseOrderStatus = 2 then
             (case
               when nvl(a.ConfirmedAmount, 0) = 0 and a.Shortsupreason is not null then
                0
               else
                (a.PlanAmount - nvl(a.InspectedQuantity, 0) -
                nvl(a.CompletionQuantity, 0) - finishiQty)
             end)
            when a.PartsPurchaseOrderStatus = 7 then
             (case
               when days > 60 then
                0
               else
                (nvl(a.ConfirmedAmount, 0) - nvl(a.InspectedQuantity, 0) -
                nvl(a.CompletionQuantity, 0) - finishiQty)
             end)
            else
             (case
               when nvl(a.ConfirmedAmount, 0) = 0 and a.Shortsupreason is not null then
                0
               else
                (a.ConfirmedAmount - nvl(a.InspectedQuantity, 0) -
                nvl(a.CompletionQuantity, 0) - finishiQty)
             end)
          end)
       end) as OnWayNumber, --在途数量
        a.PartsPurchaseOrderDate,
        a.ConfirmationDate, --供应商确认时间
       a.ArrivalCycle, -- 物流周期
        a.ShippingDate + nvl(a.ArrivalCycle, 0) as ExpectedArrivalDate, --预计到货时间   供应商发货时间+物流周期
        a.PartsInboundCheckBillDate, --入库时间
       a.PartsPlanTypeId, --计划类型
       a.Status,
         (case
         when a.PartsPurchaseOrderStatus = 2 and
              nvl(a.ConfirmedAmount, 0) = 0 and a.Shortsupreason is not null then
          4
         else
          a.PartsPurchaseOrderStatus
       end) as PartsPurchaseOrderStatus,
       a.WarehouseName,
       a.SubmitterName,
       a.OrderCycle,
       (a.PartsPurchaseOrderDate + nvl(a.ArrivalCycle, 0) +  nvl(a.OrderCycle, 0)) as TheoreticalArrivalDate, --理论到货时间
       a.InStatus,
       (case
         when a.Confirmedamount >= nvl(a.Shippingamount, 0) and
              a.Confirmedamount > 0 then
cast ('供应商发运完成' as varchar2(100))
         else
cast ('供应商部分发运' as varchar2(100))
       end) as ShippingStatus, --供应商物流状态
       a.PackingMaterial,
       a.PackingCoefficient,
       p.ReferenceCode as PackingMaterialReF,
       a.Remark,
       a.PartsPlanTypeName,
       a.partsSupplierName,
       a.ShippingDate,
       a.SupplierPartCode,
       a.PersonName,
       a.SubmitTime
  from (select detail.id,
               plans.code,
               orders.id as PartsPurchaseOrderId,
               orders.code as PartsPurchaseOrderCode,
               detail.sparepartcode,
               detail.sparepartname,
               detail.sparepartid,
               part.referencecode,
               branch.PartABC,
               detail.PlanAmount,
               puDetail.Confirmedamount,
               puDetail.Shortsupreason,
               puDetail.Shippingamount,
              (select sum(InspectedQuantity)
                  from PartsInboundCheckBillDetail a
                  where exists (select 1 from PartsInboundCheckBill tmp 
                  where a.partsinboundcheckbillid = tmp.id 
                  and tmp.OriginalRequirementBillType = 4 and tmp.originalrequirementbillid = orders.id)
                  and a.sparepartid = detail.sparepartid
                   ) as InspectedQuantity, --入库数量   配件入库检验单清单.检验量
               (select sum(nvl(a.PlannedAmount, 0)) -
                       sum(nvl(a.InspectedQuantity, 0))
                  from PartsInboundPlanDetail a
                  join PartsInboundPlan b
                    on a.PartsInboundPlanId = b.id
                 where b.originalrequirementbillid = orders.id
                   and b.originalrequirementbilltype = 4
                   and a.sparepartid = puDetail.Sparepartid
                   and b.status = 3) as CompletionQuantity, --入库强制完成数量  入库计划清单。计划量-检验量  计划单状态为终止
                (case when orders.status=7 then puDetail.Confirmedamount -nvl(puDetail.Shippingamount,0) else 0 end  ) as finishiQty,
               orders.createtime as PartsPurchaseOrderDate, -- 采购订单生成时间
                (select max(a.CreateTime)
                  from SupplierShippingOrder a
                  where exists (select 1 from SupplierShippingDetail tmp where tmp.SupplierShippingOrderId = a.id
                  and tmp.SparePartId = puDetail.SparePartId)
                 and a.PartsPurchaseOrderId = orders.id
               ) as ShippingDate, --供应商发运时间  供应商发运单最大时间
               relation.ArrivalCycle as ArrivalCycle, --物流周期
              (select max(a.CreateTime)
                  from PartsInboundCheckBill a
                  WHERE exists(select 1 from partsinboundcheckbilldetail tmp 
                  where tmp.SparePartId = puDetail.SparePartId and a.id = tmp.PartsInboundCheckBillId)
                  and a.originalrequirementbillid = orders.id
                   and a.OriginalRequirementBillType = 4
                   ) as PartsInboundCheckBillDate, --入库时间  入库检验单最大时间
               plans.PartsPlanTypeId, --计划类型
               plans.Status as Status,
               orders.status as PartsPurchaseOrderStatus,
               plans.WarehouseName as WarehouseName,
               plans.CreatorName as SubmitterName, --计划下达人员
               psRelation.OrderCycle as OrderCycle, -- 供应商发货周期
               orders.InStatus,
               (select PackingMaterial
                  from PartsBranchPackingProp a
                 where a.PartsBranchId = branch.id
                   and a.PackingType =
                       (select max(PackingType)
                          from PartsBranchPackingProp s
                         where s.partsbranchid = branch.id)) as PackingMaterial, --包材编号
               (select PackingCoefficient
                  from PartsBranchPackingProp a
                 where a.PartsBranchId = branch.id
                   and a.PackingType =
                       (select max(PackingType)
                          from PartsBranchPackingProp s
                         where s.partsbranchid = branch.id)) as PackingCoefficient, --包材转换量
               plans.Memo as Remark,
               pt.name as PartsPlanTypeName,
          supplier.name as partsSupplierName,
          supplier.code as partsSupplierCode,
           puDetail.SupplierPartCode,
           orders.ApproveTime  as   ConfirmationDate,
(select  to_char(wm_concat(PersonName)) from PersonnelSupplierRelation p where p.SupplierId=orders.PartsSupplierId and p.status=1) as PersonName,
         orders.SubmitTime,trunc(sysdate) - nvl(orders.submittime, sysdate) as days
          from PartsPurchaseOrder orders
          left join PartsPurchaseOrderDetail puDetail
            on orders.id = puDetail.Partspurchaseorderid
          join partspurchaseplan plans
            on plans.id = orders.originalrequirementbillid
           and plans.code = orders.originalrequirementbillcode
          join partspurchaseplandetail detail
            on plans.id = detail.purchaseplanid
           and detail.sparepartid = puDetail.Sparepartid
          join sparepart part
            on detail.sparepartid = part.id and part.status = 1
         left join PartsBranch branch
            on part.id = branch.PartId
           and branch.status = 1
           and orders.PartsSalesCategoryId = branch.PartsSalesCategoryId
        left  join PartsSupplier supplier
            on orders.PartsSupplierId = supplier.id
           and supplier.status = 1
         left join BranchSupplierRelation relation
            on supplier.id = relation.SupplierId
           and relation.status = 1
          left join PartsSupplierRelation psRelation
            on orders.PartsSupplierId = psRelation.SupplierId
           and detail.SparePartId = psRelation.PartId
          left join PartsPurchaseOrderType pt
            on plans.PartsPlanTypeId = pt.id
          where 1=1";
            if (!string.IsNullOrEmpty(sparePartCode))
            {
                SQL = SQL + " and LOWER(detail.sparePartCode) like '%" + sparePartCode.ToLower() + "%'";
            }
            if (!string.IsNullOrEmpty(partsSupplierName))
            {
                SQL = SQL + " and LOWER(supplier.Name) like '%" + partsSupplierName.ToLower() + "%'";
            }
            if(!string.IsNullOrEmpty(remark)) {
                SQL = SQL + " and orders.remark like '%" + remark + "%'";
            }
            if(!string.IsNullOrEmpty(supplierPartCode)) {
                SQL = SQL + " and puDetail.SupplierPartCode like '%" + supplierPartCode + "%'";
            }
            if (status.HasValue)
            {
                SQL = SQL + " and plans.status=" + status.Value;
            }
            if (partsPurchaseOrderStatus.HasValue)
            {
                SQL = SQL + " and orders.status=" + partsPurchaseOrderStatus.Value;
            }
            if (bPartsPurchasePlanTime.HasValue)
            {
                SQL = SQL + " and plans.CreateTime>= to_date('" + bPartsPurchasePlanTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if (ePartsPurchasePlanTime.HasValue)
            {
                SQL = SQL + " and plans.CreateTime<= to_date('" + ePartsPurchasePlanTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }

            if (bPartsPurchaseOrderDate.HasValue)
            {
                SQL = SQL + " and orders.CreateTime>= to_date('" + bPartsPurchaseOrderDate.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if (ePartsPurchaseOrderDate.HasValue)
            {
                SQL = SQL + " and orders.CreateTime<= to_date('" + ePartsPurchaseOrderDate.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if(bSubmitTime.HasValue) {
                SQL = SQL + " and orders.SubmitTime>= to_date('" + bSubmitTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if(eSubmitTime.HasValue) {
                SQL = SQL + " and orders.SubmitTime<= to_date('" + eSubmitTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            SQL = SQL + ") a left join sparepart p  on a.PackingMaterial = p.code  and p.status = 1";
            if (isOnWayNumber.HasValue && isOnWayNumber.Value==true)
            {
                SQL = SQL + "  where ((case when a.partsSupplierCode in ('1000002367', '1000003829') then (case when a.PartsPurchaseOrderStatus in (1, 2, 3, 4) then  a.PlanAmount  when  (a.PartsPurchaseOrderStatus in (7, 99) and nvl(a.Shippingamount, 0) = 0) then  0  else (a.PlanAmount - nvl(a.InspectedQuantity, 0) -  nvl(a.CompletionQuantity, 0) - finishiQty) end) else  (case when (a.PartsPurchaseOrderStatus in (7, 99) and nvl(a.Shippingamount, 0) = 0) then  0 when a.PartsPurchaseOrderStatus in (1, 2) then (a.PlanAmount - nvl(a.InspectedQuantity, 0) -   nvl(a.CompletionQuantity, 0) - finishiQty) else  (a.ConfirmedAmount - nvl(a.InspectedQuantity, 0) - nvl(a.CompletionQuantity, 0) - finishiQty)  end)  end)) = 0";
            }
            else if (isOnWayNumber.HasValue && isOnWayNumber.Value==false)
            {
                SQL = SQL + "  where ((case when a.partsSupplierCode in ('1000002367', '1000003829') then (case when a.PartsPurchaseOrderStatus in (1, 2, 3, 4) then  a.PlanAmount  when  (a.PartsPurchaseOrderStatus in (7, 99) and nvl(a.Shippingamount, 0) = 0) then  0  else (a.PlanAmount - nvl(a.InspectedQuantity, 0) -  nvl(a.CompletionQuantity, 0) - finishiQty) end) else  (case when (a.PartsPurchaseOrderStatus in (7, 99) and nvl(a.Shippingamount, 0) = 0) then  0 when a.PartsPurchaseOrderStatus in (1, 2) then (a.PlanAmount - nvl(a.InspectedQuantity, 0) -   nvl(a.CompletionQuantity, 0) - finishiQty) else  (a.ConfirmedAmount - nvl(a.InspectedQuantity, 0) - nvl(a.CompletionQuantity, 0) - finishiQty)  end)  end)) > 0";
            }
            var search = ObjectContext.ExecuteStoreQuery<PartsPurchasePlanReport>(SQL).ToList();
            return search;
        }
    }
    partial class DcsDomainService
    {
        public IEnumerable<PartsPurchasePlanReport> GetPartsPurchasePlanReports(string sparePartCode, string partsSupplierName, int? status, int? partsPurchaseOrderStatus, DateTime? bPartsPurchasePlanTime, DateTime? ePartsPurchasePlanTime, DateTime? bPartsPurchaseOrderDate, DateTime? ePartsPurchaseOrderDate, bool? isOnWayNumber, string remark, string supplierPartCode, DateTime? bSubmitTime, DateTime? eSubmitTime)
        {
            return new PartsPurchasePlanReportAch(this).GetPartsPurchasePlanReports(sparePartCode, partsSupplierName, status, partsPurchaseOrderStatus, bPartsPurchasePlanTime, ePartsPurchasePlanTime, bPartsPurchaseOrderDate, ePartsPurchaseOrderDate, isOnWayNumber, remark, supplierPartCode, bSubmitTime, eSubmitTime);
        }
    }
}