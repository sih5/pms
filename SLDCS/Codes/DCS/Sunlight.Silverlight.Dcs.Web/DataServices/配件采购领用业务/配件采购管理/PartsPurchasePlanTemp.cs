﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsPurchasePlanTempAch : DcsSerivceAchieveBase {
        public PartsPurchasePlanTempAch(DcsDomainService domainService)
            : base(domainService) {
        }
        public IEnumerable<VirtualPartsPurchasePlanTemp> 采购订单对应包材明细查询(int? partsPurchaseOrderStatus, string partsPurchaseOrderCode, string sparePartCode, string packSparePartCode) {
            string SQL = @"select pt.id,
                           pt.partspurchaseorderid,
                           pt.partspurchaseordercode,
                           pt.warehouseid,
                           pt.warehousename,
                           pt.packsparepartid,
                           pt.packsparepartcode,
                           pt.packsparepartname,
                           pt.packplanqty,
                           pt.packnum,
                           pt.sparepartid,
                           pt.sparepartcode,
                           pt.sparepartname,
                           pt.orderamount,
                           pt.status,
                           pt.creatorid,
                           pt.creatorname,
                           pt.createtime,
                           pt.modifierid,
                           pt.modifiername,
                           pt.modifytime,
                           po.status as PartsPurchaseOrderStatus,pt.status
                      from PartsPurchasePlanTemp pt
                      join partspurchaseorder po
                        on pt.partspurchaseorderid = po.id
                       where 1=1 ";
            if(!string.IsNullOrEmpty(partsPurchaseOrderCode)) {
                SQL = SQL + " and po.code like '%" + partsPurchaseOrderCode + "%'";
            }
            if(partsPurchaseOrderStatus.HasValue) {
                SQL = SQL + " and po.status=" + partsPurchaseOrderStatus.Value;
            }
            if(!string.IsNullOrEmpty(sparePartCode)) {
                SQL = SQL + " and pt.sparePartCode like '%" + sparePartCode + "%'";
            }
            if(!string.IsNullOrEmpty(packSparePartCode)) {
                SQL = SQL + " and pt.packSparePartCode like '%" + packSparePartCode + "%'";
            }
            var search = ObjectContext.ExecuteStoreQuery<VirtualPartsPurchasePlanTemp>(SQL).ToList();
            return search;
        }
    }
    partial class DcsDomainService {
        public IEnumerable<VirtualPartsPurchasePlanTemp> 采购订单对应包材明细查询(int? partsPurchaseOrderStatus, string partsPurchaseOrderCode, string sparePartCode, string packSparePartCode) {
            return new PartsPurchasePlanTempAch(this).采购订单对应包材明细查询(partsPurchaseOrderStatus, partsPurchaseOrderCode, sparePartCode, packSparePartCode);
        }

    }
}
