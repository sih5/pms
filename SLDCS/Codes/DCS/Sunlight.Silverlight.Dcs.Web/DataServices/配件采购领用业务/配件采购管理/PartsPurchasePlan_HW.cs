﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsPurchasePlan_HWAch : DcsSerivceAchieveBase {
        public IQueryable<PartsPurchasePlan_HW> GetPartsPurchasePlan_HWWithDetailsById(int id) {
            var tempResult = ObjectContext.PartsPurchasePlan_HW.Where(e => e.Id == id).OrderBy(e => e.Id);
            return tempResult.Include("PartsPurchasePlanDetail_HWs");
        }
        //public IQueryable<PartsPurchasePlanDetail_HW> GetPartsPurchasePlanDetail_HW() {
        //    return ObjectContext.PartsPurchasePlanDetail_HW.OrderBy(e => e.Id);
        //}
        public IQueryable<PartsPurchasePlanDetail_HWEx> GetPartsPurchasePlanDetail_HWsWithPIP() {
            var partsPurchasePlan_HWs = ObjectContext.PartsPurchasePlan_HW;
            var warehouseStockBJ = from a in ObjectContext.PartsStocks
                                   join d in ObjectContext.PartsLockedStocks on new {
                                       a.BranchId,
                                       a.WarehouseId,
                                       a.PartId
                                   }equals new {
                                       d.BranchId,
                                       d.WarehouseId,
                                       d.PartId
                                   }into temp
                                   from x in temp.DefaultIfEmpty()
                                   join b in ObjectContext.Warehouses.Where(r => r.Name == "配件物流公司北京CDC仓库") on a.WarehouseId equals b.Id
                                   join c in ObjectContext.WarehouseAreaCategories.Where(r => r.Category == (int)DcsAreaType.保管区 || r.Category == (int)DcsAreaType.检验区) on a.WarehouseAreaCategoryId equals c.Id
                                   group new {
                                       a,
                                       x
                                   }by a.PartId into g
                                   select new {
                                       partId = g.Key,
                                       quantity = g.Sum(r => r.a.Quantity - (((int?)r.x.LockedQuantity) ?? 0))
                                   };
            var warehouseStockSD = from a in ObjectContext.PartsStocks
                                   join d in ObjectContext.PartsLockedStocks on new {
                                       a.BranchId,
                                       a.WarehouseId,
                                       a.PartId
                                   }equals new {
                                       d.BranchId,
                                       d.WarehouseId,
                                       d.PartId
                                   }into temp
                                   from x in temp.DefaultIfEmpty()
                                   join b in ObjectContext.Warehouses.Where(r => r.Name == "配件物流公司山东CDC仓库") on a.WarehouseId equals b.Id
                                   join c in ObjectContext.WarehouseAreaCategories.Where(r => r.Category == (int)DcsAreaType.保管区 || r.Category == (int)DcsAreaType.检验区) on a.WarehouseAreaCategoryId equals c.Id
                                   group new {
                                       a,
                                       x
                                   }by a.PartId into g
                                   select new {
                                       partId = g.Key,
                                       quantity = g.Sum(r => r.a.Quantity - (((int?)r.x.LockedQuantity) ?? 0))
                                   };
            var warehouseStockGD= from a in ObjectContext.PartsStocks
                                   join d in ObjectContext.PartsLockedStocks on new {
                                       a.BranchId,
                                       a.WarehouseId,
                                       a.PartId
                                   }equals new {
                                       d.BranchId,
                                       d.WarehouseId,
                                       d.PartId
                                   }into temp
                                   from x in temp.DefaultIfEmpty()
                                   join b in ObjectContext.Warehouses.Where(r => r.Name == "配件物流公司广东CDC仓库") on a.WarehouseId equals b.Id
                                   join c in ObjectContext.WarehouseAreaCategories.Where(r => r.Category == (int)DcsAreaType.保管区 || r.Category == (int)DcsAreaType.检验区) on a.WarehouseAreaCategoryId equals c.Id
                                   group new {
                                       a,
                                       x
                                   }by a.PartId into g
                                   select new {
                                       partId = g.Key,
                                       quantity = g.Sum(r => r.a.Quantity - (((int?)r.x.LockedQuantity) ?? 0))
                                   };
            var tempResult = from a in partsPurchasePlan_HWs
                join t in ObjectContext.PartsPurchasePlanDetail_HW on a.Id equals t.PartsPurchasePlanId into table
                from b in table.DefaultIfEmpty()
                join wa in warehouseStockBJ on b.PartId equals wa.partId into table2
                from t2 in table2.DefaultIfEmpty()
                join wb in warehouseStockSD on b.PartId equals wb.partId into table3
                from t3 in table3.DefaultIfEmpty()
                join wc in warehouseStockGD on b.PartId equals wc.partId into table4
                from t4 in table4.DefaultIfEmpty()
                //join c in ObjectContext.PartsPurchaseOrders
                //on a.Code equals c.PurOrderCode into PPO
                //from ppo in PPO.DefaultIfEmpty()
                //join d in ObjectContext.PartsInboundCheckBills
                //on ppo.Code equals d.OriginalRequirementBillCode into PIC
                //from pic in PIC.DefaultIfEmpty()
                //join e in ObjectContext.PartsInboundCheckBillDetails
                //on pic.Id equals e.PartsInboundCheckBillId into PICD
                //from picd in PICD.Where(v => v.SparePartId == b.PartId).DefaultIfEmpty()
                //join ct in ObjectContext.PartsTransferOrders
                //on a.Code equals ct.PurOrderCode into PPOt
                //from ppot in PPOt.DefaultIfEmpty()
                //join dt in ObjectContext.PartsInboundCheckBills
                //on ppot.Code equals dt.OriginalRequirementBillCode into PICt
                //from pict in PICt.DefaultIfEmpty()
                //join et in ObjectContext.PartsInboundCheckBillDetails
                //on pict.Id equals et.PartsInboundCheckBillId into PICDt
                //from picdt in PICDt.Where(v => v.SparePartId == b.PartId).DefaultIfEmpty()
                select new PartsPurchasePlanDetail_HWEx {
                    Id = b.Id,
                    PartId = b.PartId,
                    PartsPurchasePlanId = b.PartsPurchasePlanId,
                    PartCode = b.PartCode,
                    PartName = b.PartName,
                    GPMSPartCode = b.GPMSPartCode,
                    PartsPurchasePlanQty = b.PartsPurchasePlanQty,
                    UntFulfilledQty = b.UntFulfilledQty,
                    PurchaseQty = b.PurchaseQty,
                    PurchaseConfimQty = b.PurchaseConfimQty,
                    TransferQty = b.TransferQty,
                    TransferConfimQty = b.TransferConfimQty,
                    UnitPrice = b.UnitPrice,
                    MeasureUnit = b.MeasureUnit,
                    GPMSRowNum = b.GPMSRowNum,
                    OverseasSuplierCode = b.OverseasSuplierCode,
                    PartsSupplierId = b.PartsSupplierId,
                    PartsSupplierCode = b.PartsSupplierCode,
                    PartsSupplierName = b.PartsSupplierName,
                    FaultReason = b.FaultReason,
                    Remark = b.Remark,
                    POCode = b.POCode,
                    SuplierCode = b.SuplierCode,
                    RequestedDeliveryTime = b.RequestedDeliveryTime,
                    BJCDCStock = t2.quantity,
                    SDCDCStock = t3.quantity,
                    GDCDCStock = t4.quantity,
                    WarehouseId = b.WarehouseId,
                    WarehouseCode = b.WarehouseCode,
                    WarehouseName = b.WarehouseName,
                    EndAmount = b.EndAmount??0,
                    CanAnalyzeQty = ((b.UntFulfilledQty ?? 0) - (b.EndAmount ?? 0))
                };
            return tempResult.Distinct().OrderBy(e => e.Id);
        }

        internal void UpdatePartsPurchasePlan_HWValidate(PartsPurchasePlan_HW partsPurchasePlan_HW) {
            var dbPartsPurchasePlan_HW = ObjectContext.PartsPurchasePlan_HW.FirstOrDefault(r => r.Id== partsPurchasePlan_HW.Id);
            if(dbPartsPurchasePlan_HW == null)
                throw new ValidationException("采购计划不存在");
            int status = 0;
            if(dbPartsPurchasePlan_HW.PartsPurchasePlanDetail_HWs.Any(r => !string.IsNullOrEmpty(r.FaultReason)))
                status = (int)DcsPurchasePlanStatus.数据异常;
            else if(dbPartsPurchasePlan_HW.PartsPurchasePlanDetail_HWs.Sum(r => r.UntFulfilledQty) == dbPartsPurchasePlan_HW.PartsPurchasePlanDetail_HWs.Sum(r => r.PartsPurchasePlanQty))
                status = (int)DcsPurchasePlanStatus.可分解;
            else if(dbPartsPurchasePlan_HW.PartsPurchasePlanDetail_HWs.Any(r => ((r.UntFulfilledQty??0)-(r.EndAmount??0))>0))
                status = (int)DcsPurchasePlanStatus.部分分解;
            else //if(dbPartsPurchasePlan_HW.PartsPurchasePlanDetail_HWs.Any(r => r.UntFulfilledQty == 0))
                status = (int)DcsPurchasePlanStatus.分解完成;
            partsPurchasePlan_HW.Status = status;
            var userInfo = Utils.GetCurrentUserInfo();
            partsPurchasePlan_HW.ModifierId = userInfo.Id;
            partsPurchasePlan_HW.ModifierName = userInfo.Name;
            partsPurchasePlan_HW.ModifyTime = DateTime.Now;
        }
        public void UpdatePartsPurchasePlan_HW(PartsPurchasePlan_HW partsPurchasePlan_HW) {
            UpdateToDatabase(partsPurchasePlan_HW);
            this.UpdatePartsPurchasePlan_HWValidate(partsPurchasePlan_HW);
        }
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               

        public IQueryable<PartsPurchasePlan_HW> GetPartsPurchasePlan_HWWithDetailsById(int id) {
            return new PartsPurchasePlan_HWAch(this).GetPartsPurchasePlan_HWWithDetailsById(id);
        }

        public IQueryable<PartsPurchasePlanDetail_HWEx> GetPartsPurchasePlanDetail_HWsWithPIP() {
            return new PartsPurchasePlan_HWAch(this).GetPartsPurchasePlanDetail_HWsWithPIP();
        }

        public void UpdatePartsPurchasePlan_HW(PartsPurchasePlan_HW partsPurchasePlan_HW) {
            new PartsPurchasePlan_HWAch(this).UpdatePartsPurchasePlan_HW(partsPurchasePlan_HW);
        }
    }
}
