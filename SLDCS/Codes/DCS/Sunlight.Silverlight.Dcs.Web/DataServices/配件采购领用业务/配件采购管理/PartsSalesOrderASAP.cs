﻿
using System;
using System.Linq;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsSalesOrderASAPAch : DcsSerivceAchieveBase {

        public PartsSalesOrderASAPAch(DcsDomainService domainService)
            : base(domainService) {
        }

        internal void InsertPartsSalesOrderASAPValidation(PartsSalesOrderASAP partsSalesOrderASAP) {


        }

        internal void UpdatePartsSalesOrderASAPValidation(PartsSalesOrderASAP partsSalesOrderASAP) {



        }

        public void InsertPartsSalesOrderASAP(PartsSalesOrderASAP partsSalesOrderASAP) {
            this.InsertToDatabase(partsSalesOrderASAP);
            //InsertPartsSalesOrderASAPValidation(partsSalesOrderASAP);

        }

        public void UpdatePartsSalesOrderASAP(PartsSalesOrderASAP partsSalesOrderASAP) {

            this.UpdateToDatabase(partsSalesOrderASAP);
            //UpdatePartsSalesOrderASAPValidation(partsSalesOrderASAP);
        }
        public IQueryable<VirtualPartsSalesOrderOutboundPlanHw> GetPartsSalesOrderAndPartsOutboundPlanASAP() {
            int id = 1;
            var partsSalesOrder = from a in this.ObjectContext.PartsSalesOrderASAPs
                                  select new VirtualPartsSalesOrderOutboundPlanHw {
                                      InterfaceId = (int)DcsInterfaceType.销售订单,
                                      Code = a.OverseasDemandSheetNo,
                                      ModifyTime = a.ModifyTime,
                                      Message = a.Message,
                                      HandleStatus = a.HandleStatus,
                                      CodeId = a.Id
                                    };
            //var partsOutboundPlan = from b in this.ObjectContext.PartsOutboundPlanASAPs
            //                      select new VirtualPartsSalesOrderOutboundPlanHw {
            //                          InterfaceId = (int)DcsInterfaceType.出库计划单,
            //                          Code = b.Shipment_Id,
            //                          ModifyTime = b.ModifyTime,
            //                          Message = b.Message,
            //                          HandleStatus = b.HandleStatus,
            //                          CodeId = b.Id
            //                      };
            var results = partsSalesOrder./*Union(partsOutboundPlan).*/ToArray();
            foreach(var result in results) {
                result.Id = id++;
                result.Status = Convert.ToInt32(result.HandleStatus);
            }
            return results.AsQueryable().OrderBy(r => r.Id);
        }
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               

        public void InsertPartsSalesOrderASAP(PartsSalesOrderASAP partsSalesOrderASAP) {
            new PartsSalesOrderASAPAch(this).InsertPartsSalesOrderASAP(partsSalesOrderASAP);
        }

        public void UpdatePartsSalesOrderASAP(PartsSalesOrderASAP partsSalesOrderASAP) {
            new PartsSalesOrderASAPAch(this).UpdatePartsSalesOrderASAP(partsSalesOrderASAP);
        }
        public IQueryable<VirtualPartsSalesOrderOutboundPlanHw> GetPartsSalesOrderAndPartsOutboundPlanASAP() {
            return new PartsSalesOrderASAPAch(this).GetPartsSalesOrderAndPartsOutboundPlanASAP();
        }
    }
   
}


