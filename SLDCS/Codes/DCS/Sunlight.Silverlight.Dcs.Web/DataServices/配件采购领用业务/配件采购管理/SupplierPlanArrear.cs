﻿using System.Linq;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class SupplierPlanArrearAch : DcsSerivceAchieveBase {
        internal void InsertSupplierPlanArrearValidation(SupplierPlanArrear supplierPlanArrear) {


        }

        internal void UpdateSupplierPlanArrearValidation(SupplierPlanArrear supplierPlanArrear) {



        }

        public void InsertSupplierPlanArrear(SupplierPlanArrear supplierPlanArrear) {
            this.InsertToDatabase(supplierPlanArrear);
            InsertSupplierPlanArrearValidation(supplierPlanArrear);

        }

        public void UpdateSupplierPlanArrear(SupplierPlanArrear supplierPlanArrear) {

            this.UpdateToDatabase(supplierPlanArrear);
            UpdateSupplierPlanArrearValidation(supplierPlanArrear);
        }

        public IQueryable<SupplierPlanArrear> GetSupplierPlanArrearnIncludeSupplierPlanArrear() {
            return this.ObjectContext.SupplierPlanArrears.Include("PartsSalesCategory").OrderBy(r => r.Id);
        }

    
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               

        public void InsertSupplierPlanArrear(SupplierPlanArrear supplierPlanArrear) {
            new SupplierPlanArrearAch(this).InsertSupplierPlanArrear(supplierPlanArrear);
        }

        public void UpdateSupplierPlanArrear(SupplierPlanArrear supplierPlanArrear) {
            new SupplierPlanArrearAch(this).UpdateSupplierPlanArrear(supplierPlanArrear);
        }

        public IQueryable<SupplierPlanArrear> GetSupplierPlanArrearnIncludeSupplierPlanArrear() {
            return new SupplierPlanArrearAch(this).GetSupplierPlanArrearnIncludeSupplierPlanArrear();
        }
    }
}
