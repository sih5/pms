﻿
using System.Linq;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class SupplierPreAppLoanDetailAch : DcsSerivceAchieveBase {
        public SupplierPreAppLoanDetailAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public IQueryable<VirtualSupplierPreAppLoanDetail> GetSupplierPreAppLoanDetailByPartsSalesCategoryId(int partsSalesCategoryId) {
            var result = from supplierPlanArrear in this.ObjectContext.SupplierPlanArrears.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效)
                         join tempsupplierAccount in this.ObjectContext.SupplierAccounts.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效) on supplierPlanArrear.SupplierId equals tempsupplierAccount.SupplierCompanyId into t1
                         from supplierAccount in t1.DefaultIfEmpty()
                         join tempCompanyInvoiceInfo in this.ObjectContext.CompanyInvoiceInfoes.Where(r => r.Status == (int)DcsMasterDataStatus.有效) on supplierPlanArrear.SupplierId equals tempCompanyInvoiceInfo.CompanyId into t2
                         from companyInvoiceInfo in t2.DefaultIfEmpty()
                         select new VirtualSupplierPreAppLoanDetail {
                             SupplierCode = supplierPlanArrear.SupplierCode,
                             SupplierId = supplierPlanArrear.SupplierId,
                             SupplierCompanyCode = supplierPlanArrear.SupplierCompanyCode,
                             SupplierCompanyName = supplierPlanArrear.SupplierCompanyName,
                             PlannedAmountOwed = supplierPlanArrear.PlannedAmountOwed,
                             ActualDebt = supplierAccount.DueAmount,
                             BankName = companyInvoiceInfo.BankName,
                             BankCode = companyInvoiceInfo.BankAccount,
                             ExceedAmount = (supplierAccount.DueAmount - supplierPlanArrear.PlannedAmountOwed) < 0 ? 0 : supplierAccount.DueAmount - supplierPlanArrear.PlannedAmountOwed
                         };
            return result.OrderBy(r => r.SupplierId);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               

        public IQueryable<VirtualSupplierPreAppLoanDetail> GetSupplierPreAppLoanDetailByPartsSalesCategoryId(int partsSalesCategoryId) {
            return new SupplierPreAppLoanDetailAch(this).GetSupplierPreAppLoanDetailByPartsSalesCategoryId(partsSalesCategoryId);
        }
    }
}
