﻿using System.Linq;
using System.ServiceModel.DomainServices.Server;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class SupplierPreApprovedLoanAch : DcsSerivceAchieveBase {
        internal void InsertSupplierPreApprovedLoanValidate(SupplierPreApprovedLoan supplierPreApprovedLoan) {
        }

        internal void UpdateSupplierPreApprovedLoanValidate(SupplierPreApprovedLoan supplierPreApprovedLoan) {
        }

        public void InsertSupplierPreApprovedLoan(SupplierPreApprovedLoan supplierPreApprovedLoan) {
            this.InsertToDatabase(supplierPreApprovedLoan);
            var supplierPreAppLoanDetails = ChangeSet.GetAssociatedChanges(supplierPreApprovedLoan, r => r.SupplierPreAppLoanDetails, ChangeOperation.Insert);
            foreach(SupplierPreAppLoanDetail supplierPreAppLoanDetail in supplierPreAppLoanDetails) {
                this.InsertToDatabase(supplierPreAppLoanDetail);
            }
            this.InsertSupplierPreApprovedLoanValidate(supplierPreApprovedLoan);
        }
        public void UpdateSupplierPreApprovedLoan(SupplierPreApprovedLoan supplierPreApprovedLoan) {
            supplierPreApprovedLoan.SupplierPreAppLoanDetails.Clear();
            this.UpdateToDatabase(supplierPreApprovedLoan);
            var supplierPreAppLoanDetails = ChangeSet.GetAssociatedChanges(supplierPreApprovedLoan, r => r.SupplierPreAppLoanDetails);
            foreach(SupplierPreAppLoanDetail supplierPreAppLoanDetail in supplierPreAppLoanDetails) {
                switch(ChangeSet.GetChangeOperation(supplierPreAppLoanDetail)) {
                    case ChangeOperation.Insert:
                        InsertToDatabase(supplierPreAppLoanDetail);
                        break;
                    case ChangeOperation.Update:
                        UpdateToDatabase(supplierPreAppLoanDetail);
                        break;
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(supplierPreAppLoanDetail);
                        break;
                }
            }
            this.UpdateSupplierPreApprovedLoanValidate(supplierPreApprovedLoan);
        }
        public IQueryable<SupplierPreApprovedLoan> GetSupplierPreApprovedLoanIncludePartsSalesCategory() {
            return this.ObjectContext.SupplierPreApprovedLoans.Include("PartsSalesCategory").OrderBy(r => r.Id);
        }
        public SupplierPreApprovedLoan GetSupplierPreApprovedLoanIncludeDetailsById(int id) {
            var supplierPreApprovedLoan = this.ObjectContext.SupplierPreApprovedLoans.Single(r => r.Id == id);
            if(supplierPreApprovedLoan != null) {
                var partsSalesCategory = this.ObjectContext.PartsSalesCategories.SingleOrDefault(r => r.Id == supplierPreApprovedLoan.PartsSalesCategoryId);
                var detaile = this.ObjectContext.SupplierPreAppLoanDetails.Where(r => r.SupplierPreApprovedLoanId == supplierPreApprovedLoan.Id).ToArray();
            }
            return supplierPreApprovedLoan;
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               
        
        public void InsertSupplierPreApprovedLoan(SupplierPreApprovedLoan supplierPreApprovedLoan) {
            new SupplierPreApprovedLoanAch(this).InsertSupplierPreApprovedLoan(supplierPreApprovedLoan);
        }

        public void UpdateSupplierPreApprovedLoan(SupplierPreApprovedLoan supplierPreApprovedLoan) {
            new SupplierPreApprovedLoanAch(this).UpdateSupplierPreApprovedLoan(supplierPreApprovedLoan);
        }

        public IQueryable<SupplierPreApprovedLoan> GetSupplierPreApprovedLoanIncludePartsSalesCategory() {
            return new SupplierPreApprovedLoanAch(this).GetSupplierPreApprovedLoanIncludePartsSalesCategory();
        }

        public SupplierPreApprovedLoan GetSupplierPreApprovedLoanIncludeDetailsById(int id) {
            return new SupplierPreApprovedLoanAch(this).GetSupplierPreApprovedLoanIncludeDetailsById(id);
        }
    }
}
