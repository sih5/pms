﻿using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Entities;
using System.Collections.Generic;
using System;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class SupplierShippingDetailAch : DcsSerivceAchieveBase {
        public SupplierShippingDetailAch(DcsDomainService domainService)
            : base(domainService) {
        }
        public IQueryable<VirtualSupplierShippingDetail> GetSupplierShippingDetailByIdWithOthers(int supplierShippingOrderId) {
            var result = from a in ObjectContext.SupplierShippingDetails.Where(r => r.SupplierShippingOrderId == supplierShippingOrderId)
                         join b in ObjectContext.SupplierShippingOrders on a.SupplierShippingOrderId equals b.Id
                         join c in ObjectContext.PartsPurchaseOrders on b.PartsPurchaseOrderId equals c.Id
                         join d in ObjectContext.PartsPurchaseOrderDetails on new {
                             c.Id,
                             a.SparePartId
                         } equals new {
                             Id = d.PartsPurchaseOrderId,
                             d.SparePartId
                         } into temp1
                         from t1 in temp1.DefaultIfEmpty()
                         select new VirtualSupplierShippingDetail {
                             Id = a.Id,
                             SerialNumber = a.SerialNumber,
                             SparePartCode = a.SparePartCode,
                             SparePartName = a.SparePartName,
                             SupplierPartCode = a.SupplierPartCode,
                             MeasureUnit = a.MeasureUnit,
                             Quantity = a.Quantity,
                             ConfirmedAmount = a.ConfirmedAmount,
                             Remark = a.Remark,
                             SpareOrderRemark = a.SpareOrderRemark,
                             PlanAmount = t1.OrderAmount,
                             WaitShippingAmount = t1.OrderAmount - (t1.ShippingAmount ?? 0),
                             Weight=a.Weight,
                             Volume=a.Volume
                         };

            return result.Distinct().OrderBy(r => r.Id);
        }


        [Query(HasSideEffects = true)]
        public IQueryable<SupplierShippingDetail> GetSupplierShippingDetailById(int[] supplierShippingOrderId) {
            return ObjectContext.SupplierShippingDetails.Where(r => supplierShippingOrderId.Any(v => v == r.SupplierShippingOrderId)).Include("SupplierShippingOrder").OrderBy(r => r.SparePartId);
        }
        public IEnumerable<VirtualSupplierShippingDetail> GetupplierShippingDetailForPrintPc(int[] ids)
        {
            string SQL = @"SELECT Extent1.Id,
       Extent1.SparePartCode,
       Extent1.SparePartName,
       Extent1.SparePartId,
       Extent1.Quantity,
       Extent2.EnglishName,
       Extent1.SupplierShippingOrderId,
       substr(Extent3.Code, length(Extent3.Code) - 11) BachMunber,     
       Extent3.Code as Code,
         Extent2.Measureunit,
       p1.measureunit as FirMeasureUnit,
       p2.measureunit as SecMeasureUnit,
       p3.measureunit as ThidMeasureUnit,
      ( case when p1.isprintpartstandard=1 and p1.packingcoefficient>0  then ceil(Extent1.Quantity/p1.packingcoefficient) else 0 end )as FirPrintNumber,
           ( case when p1.isprintpartstandard=1 and p1.packingcoefficient>0  then ceil(Extent1.Quantity/p1.packingcoefficient) else 0 end )as FirPrintNumber,
      ( case when p2.isprintpartstandard=1 and p2.packingcoefficient>0  then ceil(Extent1.Quantity/p2.packingcoefficient) else 0 end )as SecPrintNumber,
       ( case when p3.isprintpartstandard=1 and p3.packingcoefficient>0  then ceil(Extent1.Quantity/p3.packingcoefficient) else 0 end )as ThidPrintNumber,
       p1.packingcoefficient as FirPackingCoefficient,
       p2.packingcoefficient as SecPackingCoefficient,
       p3.packingcoefficient as ThidPackingCoefficient
  FROM SupplierShippingDetail Extent1
  join SupplierShippingOrder Extent3
    on Extent1.SupplierShippingOrderId = Extent3.Id
 INNER JOIN SparePart Extent2
    ON Extent1.SparePartId = Extent2.Id
     left join  partsbranch pb
    on Extent1.Sparepartid = pb.partid
   and pb.status = 1
  left join PartsBranchPackingProp p1
    on p1.partsbranchid = pb.id
   and p1.PackingType = 1
  left join PartsBranchPackingProp p2
    on p2.partsbranchid = pb.id
   and p2.PackingType = 2
  left join PartsBranchPackingProp p3
    on p3.partsbranchid = pb.id
   and p3.PackingType = 3

 WHERE Extent1.SupplierShippingOrderId in (";
            for (var i = 0; i < ids.Length; i++)
            {
                if (ids.Length == i + 1)
                {
                    SQL = SQL + ids[i];
                }
                else
                {
                    SQL = SQL + ids[i] + ",";
                }
            }
            SQL = SQL + ")";

            var detail = ObjectContext.ExecuteStoreQuery<VirtualSupplierShippingDetail>(SQL).ToList();
            return detail;
        }
        public IEnumerable<VirtualSupplierShippingDetail> GetupplierShippingDetailForPrint(int[] ids)
        {
            string SQL = @"SELECT Extent1.Id,
       Extent1.SparePartCode,
       Extent1.SparePartName,
       Extent1.SparePartId,
       Extent1.Quantity,
       Extent2.EnglishName,
       Extent1.SupplierShippingOrderId,
       substr(Extent3.Code, length(Extent3.Code) - 11) BachMunber,     
       Extent3.Code as Code,
         Extent2.Measureunit,
       p1.measureunit as FirMeasureUnit,
       p2.measureunit as SecMeasureUnit,
       p3.measureunit as ThidMeasureUnit,
      ( case when p1.isboxstandardprint=1 and p1.packingcoefficient>0  then ceil(Extent1.Quantity/p1.packingcoefficient) else 0 end )as FirPrintNumber,
           ( case when p1.isboxstandardprint=1 and p1.packingcoefficient>0  then ceil(Extent1.Quantity/p1.packingcoefficient) else 0 end )as FirPrintNumber,
      ( case when p2.isboxstandardprint=1 and p2.packingcoefficient>0  then ceil(Extent1.Quantity/p2.packingcoefficient) else 0 end )as SecPrintNumber,
       ( case when p3.isboxstandardprint=1 and p3.packingcoefficient>0  then ceil(Extent1.Quantity/p3.packingcoefficient) else 0 end )as ThidPrintNumber,
       p1.packingcoefficient as FirPackingCoefficient,
       p2.packingcoefficient as SecPackingCoefficient,
       p3.packingcoefficient as ThidPackingCoefficient
  FROM SupplierShippingDetail Extent1
  join SupplierShippingOrder Extent3
    on Extent1.SupplierShippingOrderId = Extent3.Id
 INNER JOIN SparePart Extent2
    ON Extent1.SparePartId = Extent2.Id
     left join  partsbranch pb
    on Extent1.Sparepartid = pb.partid
   and pb.status = 1
  left join PartsBranchPackingProp p1
    on p1.partsbranchid = pb.id
   and p1.PackingType = 1
  left join PartsBranchPackingProp p2
    on p2.partsbranchid = pb.id
   and p2.PackingType = 2
  left join PartsBranchPackingProp p3
    on p3.partsbranchid = pb.id
   and p3.PackingType = 3

 WHERE Extent1.SupplierShippingOrderId in (";
            for (var i = 0; i < ids.Length; i++)
            {
                if (ids.Length == i + 1)
                {
                    SQL = SQL + ids[i];
                }
                else
                {
                    SQL = SQL + ids[i] + ",";
                }
            }
            SQL = SQL + ")";

            var detail = ObjectContext.ExecuteStoreQuery<VirtualSupplierShippingDetail>(SQL).ToList();
            return detail;
        }
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               

        [Query(HasSideEffects = true)]
        public IQueryable<SupplierShippingDetail> GetSupplierShippingDetailById(int[] supplierShippingOrderId) {
            return new SupplierShippingDetailAch(this).GetSupplierShippingDetailById(supplierShippingOrderId);
        }
        public IEnumerable<VirtualSupplierShippingDetail> GetupplierShippingDetailForPrintPc(int[] id)
        {
            return new SupplierShippingDetailAch(this).GetupplierShippingDetailForPrintPc(id);
        }
        public IEnumerable<VirtualSupplierShippingDetail> GetupplierShippingDetailForPrint(int[] ids)
        {
            return new SupplierShippingDetailAch(this).GetupplierShippingDetailForPrint(ids);
        }

        public IQueryable<VirtualSupplierShippingDetail> GetSupplierShippingDetailByIdWithOthers(int supplierShippingOrderId) {
            return new SupplierShippingDetailAch(this).GetSupplierShippingDetailByIdWithOthers(supplierShippingOrderId);
        }
    }
}


