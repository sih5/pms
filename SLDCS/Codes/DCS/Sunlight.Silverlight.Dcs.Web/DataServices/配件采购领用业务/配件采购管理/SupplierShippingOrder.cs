﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Dcs.Web.Entities;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class SupplierShippingOrderAch : DcsSerivceAchieveBase {
        internal void InsertSupplierShippingOrderValidate(SupplierShippingOrder supplierShippingOrder) {
            var userInfo = Utils.GetCurrentUserInfo();
            if(string.IsNullOrWhiteSpace(supplierShippingOrder.Code) || supplierShippingOrder.Code == GlobalVar.ASSIGNED_BY_SERVER)
                supplierShippingOrder.Code = CodeGenerator.Generate("SupplierShippingOrder", supplierShippingOrder.BranchCode);
            supplierShippingOrder.CreatorId = userInfo.Id;
            supplierShippingOrder.CreatorName = userInfo.Name;
            supplierShippingOrder.CreateTime = DateTime.Now;
        }

        internal void UpdateSupplierShippingOrderValidate(SupplierShippingOrder supplierShippingOrder) {

            var userInfo = Utils.GetCurrentUserInfo();
            supplierShippingOrder.ModifierId = userInfo.Id;
            supplierShippingOrder.ModifierName = userInfo.Name;
            supplierShippingOrder.ModifyTime = DateTime.Now;
        }

        public void InsertSupplierShippingOrder(SupplierShippingOrder supplierShippingOrder) {
            InsertToDatabase(supplierShippingOrder);
            var supplierShippingDetails = ChangeSet.GetAssociatedChanges(supplierShippingOrder, v => v.SupplierShippingDetails, ChangeOperation.Insert);
            foreach(SupplierShippingDetail supplierShippingDetail in supplierShippingDetails)
                InsertToDatabase(supplierShippingDetail);
            this.InsertSupplierShippingOrderValidate(supplierShippingOrder);
        }

        public void UpdateSupplierShippingOrder(SupplierShippingOrder supplierShippingOrder) {
            supplierShippingOrder.SupplierShippingDetails.Clear();
            UpdateToDatabase(supplierShippingOrder);
            var supplierShippingDetails = ChangeSet.GetAssociatedChanges(supplierShippingOrder, v => v.SupplierShippingDetails);
            foreach(SupplierShippingDetail supplierShippingDetail in supplierShippingDetails) {
                switch(ChangeSet.GetChangeOperation(supplierShippingDetail)) {
                    case ChangeOperation.Insert:
                        InsertToDatabase(supplierShippingDetail);
                        break;
                    case ChangeOperation.Update:
                        UpdateToDatabase(supplierShippingDetail);
                        break;
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(supplierShippingDetail);
                        break;
                }
            }
            this.UpdateSupplierShippingOrderValidate(supplierShippingOrder);
        }

        public SupplierShippingOrder GetSupplierShippingOrderWithDetailsById(int id) {
            var dbsupplierShippingOrder = ObjectContext.SupplierShippingOrders.SingleOrDefault(r => r.Id == id && r.Status != (int)DcsSupplierShippingOrderStatus.作废);
            if(dbsupplierShippingOrder != null) {
                var supplierShippingDetails = ObjectContext.SupplierShippingDetails.Where(r => r.SupplierShippingOrderId == id).OrderBy(entity => entity.SerialNumber).ToArray();
            }
            return dbsupplierShippingOrder;
        }

        public SupplierShippingOrder GetSupplierShippingOrderWithDetailsAndBatchInfoById(int id) {
            var dbsupplierShippingOrder = ObjectContext.SupplierShippingOrders.SingleOrDefault(r => r.Id == id && r.Status != (int)DcsSupplierShippingOrderStatus.作废);
            if(dbsupplierShippingOrder != null) {
                //var partsLogisticBatchBillDetail = ObjectContext.PartsLogisticBatchBillDetails.FirstOrDefault(r => r.BillId == id && r.BillType == (int)DcsPartsLogisticBatchBillDetailBillType.供应商发运单);
                //if(partsLogisticBatchBillDetail != null) {
                //    var partsLogisticBatch = ObjectContext.PartsLogisticBatches.SingleOrDefault(r => r.Id == partsLogisticBatchBillDetail.PartsLogisticBatchId);
                //    if(partsLogisticBatch != null) {
                //        var partsLogisticBatchItemDetails = ObjectContext.PartsLogisticBatchItemDetails.Where(r => r.PartsLogisticBatchId == partsLogisticBatch.Id).ToArray();
                //    }
                //}
                var supplierShippingDetails = ObjectContext.SupplierShippingDetails.Where(r => r.SupplierShippingOrderId == id).OrderBy(entity => entity.SerialNumber).ToArray();
                if(supplierShippingDetails.Any()) {
                    var partIds = supplierShippingDetails.Select(r => r.SparePartId);
                    var spareParts = ObjectContext.SpareParts.Where(e => partIds.Contains(e.Id)).ToArray();
                    var partsBranches = ObjectContext.PartsBranches.Where(e => e.BranchId == dbsupplierShippingOrder.BranchId && partIds.Contains(e.PartId)).ToArray();
                }
                var dbPartsPurchaseOrder = ObjectContext.PartsPurchaseOrders.SingleOrDefault(r => r.Id == dbsupplierShippingOrder.PartsPurchaseOrderId);
                if(dbPartsPurchaseOrder != null) {
                    var partsPurchaseOrderDetails = ObjectContext.PartsPurchaseOrderDetails.Where(r => r.PartsPurchaseOrderId == dbPartsPurchaseOrder.Id).ToArray();
                }
            }
            return dbsupplierShippingOrder;
        }

        public IQueryable<VirtualSupplierShippingOrder> 根据人员查询供应商发运单(int? partsSalesCategoryId, int? receivingWarehouseId, string code, string partsPurchaseOrderCode, bool? directProvisionFinished, bool? ifDirectProvision, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, string eRPSourceOrderCode, string partsSupplierCode, string partsSupplierName,int branchId) {
            var userinfo = Utils.GetCurrentUserInfo();
            var personId = userinfo.Id;
            var dbPersonSalesCenterLink = ObjectContext.PersonSalesCenterLinks.Where(r => r.PersonId == personId).ToArray();
            var dbPersonSalesCenterLinkPartsSalesCategoryIds = dbPersonSalesCenterLink.Select(r => r.PartsSalesCategoryId);
            var detail = from a in ObjectContext.SupplierShippingDetails
                         join b in ObjectContext.SupplierShippingOrders.Where(r => dbPersonSalesCenterLinkPartsSalesCategoryIds.Contains(r.PartsSalesCategoryId) && r.BranchId==userinfo.EnterpriseId) on a.SupplierShippingOrderId equals b.Id
                         join c in ObjectContext.PartsRetailGuidePrices on new {
                             a.SparePartId,
                             b.PartsSalesCategoryId,
                             Status = (int)DcsBaseDataStatus.有效
                         } equals new {
                             c.SparePartId,
                             c.PartsSalesCategoryId,
                             Status = c.Status
                         } into cc
                         from cd in cc.DefaultIfEmpty()
                         select new {
                             SupplierShippingOrderId = b.Id,
                             SalesPrice = cd.RetailGuidePrice,
                             Quantity = a.Quantity,
                             Weight=a.Weight,
                             Volume=a.Volume
                         };
            var detailSum = from a in detail
                            group a by a.SupplierShippingOrderId into tempt
                            select new {
                                SupplierShippingOrderId = tempt.Key,
                                TotalAmount = tempt.Sum(r => r.SalesPrice * r.Quantity),
                                TotalWeight = tempt.Sum(t=>t.Weight),
                                TotalVolume=tempt.Sum(t=>t.Volume)
                            };
            var supplierShippingOrders = from a in ObjectContext.SupplierShippingOrders.Where(r => dbPersonSalesCenterLinkPartsSalesCategoryIds.Contains(r.PartsSalesCategoryId) && r.BranchId == userinfo.EnterpriseId)
                         join b in detailSum on a.Id equals b.SupplierShippingOrderId into bb 
                         from bt in bb.DefaultIfEmpty()
                         select new VirtualSupplierShippingOrder{
                             Id=a.Id,
                             Status=a.Status,
                             Code=a.Code,
                             ReceivingWarehouseName=a.ReceivingWarehouseName,
                             PartsSupplierName=a.PartsSupplierName,
                             PartsSupplierCode=a.PartsSupplierCode,
                             PartsPurchaseOrderCode=a.PartsPurchaseOrderCode,
                             PlanSource=a.PlanSource,
                             ReceivingAddress=a.ReceivingAddress,
                             ReceivingCompanyName=a.ReceivingCompanyName,
                             IfDirectProvision=a.IfDirectProvision,
                             OriginalRequirementBillCode=a.OriginalRequirementBillCode,
                             TotalAmount= bt.TotalAmount,
                             DirectProvisionFinished = a.DirectProvisionFinished,
                             DeliveryBillNumber = a.DeliveryBillNumber,
                             RequestedDeliveryTime = a.RequestedDeliveryTime,
                             PlanDeliveryTime = a.PlanDeliveryTime,
                             ShippingDate = a.ShippingDate,
                             ArrivalDate = a.ArrivalDate,
                             LogisticArrivalDate = a.LogisticArrivalDate,
                             ShippingMethod = a.ShippingMethod,
                             LogisticCompany = a.LogisticCompany,
                             Driver = a.Driver,
                             Phone = a.Phone,
                             VehicleLicensePlate = a.VehicleLicensePlate,
                             Remark = a.Remark,
                             ERPSourceOrderCode = a.ERPSourceOrderCode,
                             CreatorName = a.CreatorName,
                             CreateTime = a.CreateTime,
                             BranchName = a.BranchName,
                             PartsSalesCategoryName = a.PartsSalesCategoryName,
                             PartsSalesCategoryId = a.PartsSalesCategoryId,
                             ReceivingWarehouseId = a.ReceivingWarehouseId,
                             BranchId=a.BranchId,
                             ModifyArrive=a.ModifyArrive,
                             InboundTime=a.InboundTime,
                             ApproveDate=a.ApproveDate,
                             Approver=a.Approver,
                             TotalVolume=bt.TotalVolume,
                             TotalWeight=bt.TotalWeight

                         };

            if(partsSalesCategoryId.HasValue) {
                partsSalesCategoryId = Convert.ToInt32(partsSalesCategoryId);
                supplierShippingOrders = supplierShippingOrders.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId);
            }
            if(receivingWarehouseId.HasValue) {
                receivingWarehouseId = Convert.ToInt32(receivingWarehouseId);
                supplierShippingOrders = supplierShippingOrders.Where(r => r.ReceivingWarehouseId == receivingWarehouseId);
            }
            if(!string.IsNullOrEmpty(code)) {
                supplierShippingOrders = supplierShippingOrders.Where(r => r.Code.Contains(code));
            }
            if(!string.IsNullOrEmpty(partsPurchaseOrderCode)) {
                supplierShippingOrders = supplierShippingOrders.Where(r => r.PartsPurchaseOrderCode.Contains(partsPurchaseOrderCode));
            }
            if(!string.IsNullOrEmpty(eRPSourceOrderCode)) {
                supplierShippingOrders = supplierShippingOrders.Where(r => r.ERPSourceOrderCode.Contains(eRPSourceOrderCode));
            }
            if(!string.IsNullOrEmpty(partsSupplierCode)) {
                supplierShippingOrders = supplierShippingOrders.Where(r => r.PartsSupplierCode.Contains(partsSupplierCode));
            }
            if(!string.IsNullOrEmpty(partsSupplierName)) {
                supplierShippingOrders = supplierShippingOrders.Where(r => r.PartsSupplierName.Contains(partsSupplierName));
            }
            if(directProvisionFinished.HasValue) {
                directProvisionFinished = Convert.ToBoolean(directProvisionFinished);
                supplierShippingOrders = supplierShippingOrders.Where(r => (r.DirectProvisionFinished ?? false) == directProvisionFinished);
            }
            if(ifDirectProvision.HasValue) {
                ifDirectProvision = Convert.ToBoolean(ifDirectProvision);
                supplierShippingOrders = supplierShippingOrders.Where(r => r.IfDirectProvision == ifDirectProvision);
            }
            if(status.HasValue) {
                status = Convert.ToInt32(status);
                supplierShippingOrders = supplierShippingOrders.Where(r => r.Status == status);
            }
            //if(!string.IsNullOrEmpty(createTimeBegin.ToString())) {
            //    supplierShippingOrders = supplierShippingOrders.Where(r => r.CreateTime >= createTimeBegin);
            //}
            //if(!string.IsNullOrEmpty(createTimeEnd.ToString())) {
            //    supplierShippingOrders = supplierShippingOrders.Where(r => r.CreateTime <= createTimeEnd);
            //}
            var ss = supplierShippingOrders.AsQueryable();
            return ss.OrderBy(r => r.Id);
        }

        public IQueryable<SupplierShippingOrder> GetSupplierShippingOrderWithCompany() {
            return ObjectContext.SupplierShippingOrders.Include("Company1").OrderBy(r => r.Code);
        }

        public IEnumerable<SupplierShippingOrder> 查询供应商直供发运单(string partsSalesOrderCode) {
            if(!string.IsNullOrEmpty(partsSalesOrderCode)) {
                var partsSalesOrder = ObjectContext.PartsSalesOrders.FirstOrDefault(r => r.Code == partsSalesOrderCode && r.Status != (int)DcsPartsSalesOrderStatus.作废);
                if(partsSalesOrder == null)
                    return null;
                var supplierShippingOrders = ObjectContext.SupplierShippingOrders.Where(r => r.OriginalRequirementBillId == partsSalesOrder.Id && r.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件销售订单 && r.IfDirectProvision == true && r.Status != (int)DcsSupplierShippingOrderStatus.作废);
                return supplierShippingOrders;
            }
            var allSupplierShippingOrders = ObjectContext.SupplierShippingOrders.Where(r => r.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件销售订单 && r.IfDirectProvision == true && r.Status != (int)DcsSupplierShippingOrderStatus.作废).ToArray();
            var partsSalesOrderIds = allSupplierShippingOrders.Select(r => r.OriginalRequirementBillId).Distinct().ToArray();
            if(partsSalesOrderIds.Length == 0)
                return null;
            var partsSalesOrders = ObjectContext.PartsSalesOrders.Where(r => partsSalesOrderIds.Contains(r.Id) && r.Status != (int)DcsPartsSalesOrderStatus.作废).ToArray();
            return allSupplierShippingOrders.OrderBy(r => r.Id);
        }

        public IEnumerable<SupplierShippingOrder> 查询供应商直供发运单New() {
            return ObjectContext.SupplierShippingOrders.Where(r => r.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件销售订单 && r.IfDirectProvision == true && r.Status != (int)DcsSupplierShippingOrderStatus.作废).OrderBy(r => r.Id);
        }
        //供应商汇总发运 界面查询方法
        public IQueryable<VirtualPartsPurchaseOrderDetail> 供应商人员查询待汇总发运采购订单信息(int? partsSupplierId) {
            var result = from t1 in ObjectContext.PartsPurchaseOrders.Where(r => (r.Status == (int)DcsPartsPurchaseOrderStatus.确认完毕 || r.Status == (int)DcsPartsPurchaseOrderStatus.部分发运 || r.Status == (int)DcsPartsPurchaseOrderStatus.部分确认) && r.PartsSupplierId == partsSupplierId)
                         join t2 in ObjectContext.PartsPurchaseOrderDetails on t1.Id equals t2.PartsPurchaseOrderId into temp
                         from t3 in temp.DefaultIfEmpty()
                         join t5 in ObjectContext.PartsBranches on t3.SparePartId equals t5.PartId
                         join t9 in ObjectContext.SpareParts on t3.SparePartId equals t9.Id
                         join t6 in ObjectContext.PartsBranchPackingProps on new {
                             Id=t5.Id,
                             PackingType= t5.MainPackingType
                         } equals new {
                             Id= t6.PartsBranchId.Value,
                             t6.PackingType
                         } into t7
                         from t8 in t7.DefaultIfEmpty()
                         select new VirtualPartsPurchaseOrderDetail {
                             PartsSupplierCode = t1.PartsSupplierCode,
                             SparePartId = t3.SparePartId,
                             SparePartCode = t3.SparePartCode,
                             SupplierPartCode = t3.SupplierPartCode,
                             SparePartName = t3.SparePartName,
                             PartsPurchaseOrderCode = t1.Code,
                             CreatorName = t1.CreatorName,
                             CreateTime = t1.CreateTime,
                             OrderAmount = t3.ConfirmedAmount,
                             ShippingAmount = t3.ShippingAmount ?? 0,
                             UnShippingAmount = t3.ConfirmedAmount - t3.ShippingAmount ?? 0,
                             MeasureUnit = t3.MeasureUnit,
                             PartsPurchaseOrderTypeId = t1.PartsPurchaseOrderTypeId,
                             IfDirectProvision = t1.IfDirectProvision,
                             WarehouseId = t1.WarehouseId,
                             MInPackingAmount = t8.PackingCoefficient ?? 0,
                             PartsPurchaseOrderTypeName = t1.PartsPurchaseOrderType.Name,
                             TraceProperty=t9.TraceProperty,
                             
                         };
            return result.Where(r => r.OrderAmount > r.ShippingAmount).OrderBy(r => r.PartsPurchaseOrderCode);
        }
        //供应商汇总发运 根据采购订单编号查询采购信息主清单
        public IQueryable<PartsPurchaseOrder> 供应商根据采购订单查询待汇总发运清单信息(string[] partsPurchaseOrderCodes) {
            if(partsPurchaseOrderCodes == null || !partsPurchaseOrderCodes.Any())
                return null;
            return ObjectContext.PartsPurchaseOrders.Where(r => partsPurchaseOrderCodes.Contains(r.Code)).OrderBy(r => r.Code);
        }
        public IQueryable<VirtualContactPerson> getVirtualContactPersonShip(int id) {
            var dbPartsPurchaseOrder = ObjectContext.SupplierShippingOrders.SingleOrDefault(entity => entity.Id == id);
            if(dbPartsPurchaseOrder.IfDirectProvision) {
                var person = from a in ObjectContext.PartsSalesOrders
                             where a.Id == dbPartsPurchaseOrder.OriginalRequirementBillId
                             select new VirtualContactPerson {
                                 Id = a.Id,
                                 ContactPerson = a.ContactPerson,
                                 ContactPhone = a.ContactPhone
                             };
                return person;
            } else {
                var person = from a in ObjectContext.Warehouses
                             where a.Id == dbPartsPurchaseOrder.ReceivingWarehouseId
                             select new VirtualContactPerson {
                                 Id = a.Id,
                                 ContactPerson = a.Contact,
                                 ContactPhone = a.PhoneNumber
                             };
                return person;
            }
             
        }
        public IQueryable<SupplierShippingOrder> GetSupplierShippingOrderWithDifCompany(string code, string partsPurchaseOrderCode, string partsSupplierName, string partsSupplierCode, int? arriveMethod, int? comfirmStatus, bool? ifDirectProvision, DateTime? createTimeBegin, DateTime? createTimeEnd, int? modifyStatus) {
            var userInfo = Utils.GetCurrentUserInfo();
            var company = ObjectContext.Companies.Where(t => t.Id == userInfo.EnterpriseId).First();
            var shipping = ObjectContext.SupplierShippingOrders.Include("SupplierShippingDetails").Where(t => 1 == 1);
            if(!string.IsNullOrEmpty(code)) {
                shipping = shipping.Where(t => t.Code.Contains(code));
            }
            if(!string.IsNullOrEmpty(partsPurchaseOrderCode)) {
                shipping = shipping.Where(t => t.PartsPurchaseOrderCode.Contains(partsPurchaseOrderCode));
            }
            if(!string.IsNullOrEmpty(partsSupplierName)) {
                shipping = shipping.Where(t => t.PartsSupplierName.Contains(partsSupplierName));
            }
            if(!string.IsNullOrEmpty(partsSupplierCode)) {
                shipping = shipping.Where(t => t.PartsSupplierCode.Contains(partsSupplierCode));
            }
            if(arriveMethod.HasValue){
                shipping = shipping.Where(t => t.ArriveMethod == arriveMethod);
            }
            if(comfirmStatus.HasValue) {
                shipping = shipping.Where(t => t.ComfirmStatus == comfirmStatus);
            }
            if(ifDirectProvision.HasValue && ifDirectProvision.Value==true) {
                shipping = shipping.Where(t => t.IfDirectProvision == true);
            }
             if(ifDirectProvision.HasValue && ifDirectProvision.Value==false) {
                 shipping = shipping.Where(t => t.IfDirectProvision == false);
            }
            if(createTimeBegin.HasValue){
                shipping = shipping.Where(t => t.CreateTime >=createTimeBegin);
            }
            if(createTimeEnd.HasValue) {
                shipping = shipping.Where(t => t.CreateTime <= createTimeEnd);
            }
            if(modifyStatus.HasValue) {
                shipping = shipping.Where(t=>t.ModifyStatus==modifyStatus);
            }
             if(company.Type == (int)DcsCompanyType.配件供应商) {
                 shipping = shipping.Where(t => t.PartsSupplierId == company.Id);
            }
             return shipping.OrderByDescending(r => r.Code);
        }
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               

        public void InsertSupplierShippingOrder(SupplierShippingOrder supplierShippingOrder) {
            new SupplierShippingOrderAch(this).InsertSupplierShippingOrder(supplierShippingOrder);
        }

        public void UpdateSupplierShippingOrder(SupplierShippingOrder supplierShippingOrder) {
            new SupplierShippingOrderAch(this).UpdateSupplierShippingOrder(supplierShippingOrder);
        }

        public SupplierShippingOrder GetSupplierShippingOrderWithDetailsById(int id) {
            return new SupplierShippingOrderAch(this).GetSupplierShippingOrderWithDetailsById(id);
        }

        public SupplierShippingOrder GetSupplierShippingOrderWithDetailsAndBatchInfoById(int id) {
            return new SupplierShippingOrderAch(this).GetSupplierShippingOrderWithDetailsAndBatchInfoById(id);
        }

        public IQueryable<VirtualSupplierShippingOrder> 根据人员查询供应商发运单(int? partsSalesCategoryId, int? receivingWarehouseId, string code, string partsPurchaseOrderCode, bool? directProvisionFinished, bool? ifDirectProvision, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, string eRPSourceOrderCode, string partsSupplierCode, string partsSupplierName, int branchId) {
            return new SupplierShippingOrderAch(this).根据人员查询供应商发运单(partsSalesCategoryId, receivingWarehouseId, code, partsPurchaseOrderCode, directProvisionFinished, ifDirectProvision, status, createTimeBegin, createTimeEnd, eRPSourceOrderCode, partsSupplierCode, partsSupplierName,branchId);
        }

        public IQueryable<SupplierShippingOrder> GetSupplierShippingOrderWithCompany() {
            return new SupplierShippingOrderAch(this).GetSupplierShippingOrderWithCompany();
        }

        public IEnumerable<SupplierShippingOrder> 查询供应商直供发运单(string partsSalesOrderCode) {
            return new SupplierShippingOrderAch(this).查询供应商直供发运单(partsSalesOrderCode);
        }

        public IEnumerable<SupplierShippingOrder> 查询供应商直供发运单New() {
            return new SupplierShippingOrderAch(this).查询供应商直供发运单New();
        }

        public IQueryable<VirtualPartsPurchaseOrderDetail> 供应商人员查询待汇总发运采购订单信息(int? partsSupplierId) {
            return new SupplierShippingOrderAch(this).供应商人员查询待汇总发运采购订单信息(partsSupplierId);
        }
        //供应商汇总发运 填充清单 查询方法
        public IQueryable<PartsPurchaseOrder> 供应商根据采购订单查询待汇总发运清单信息(string[] partsPurchaseOrderCodes) {
            return new SupplierShippingOrderAch(this).供应商根据采购订单查询待汇总发运清单信息(partsPurchaseOrderCodes);
        }
        public IQueryable<VirtualContactPerson> getVirtualContactPersonShip(int id) {
            return new SupplierShippingOrderAch(this).getVirtualContactPersonShip(id);
        }
        public IQueryable<SupplierShippingOrder> GetSupplierShippingOrderWithDifCompany(string code, string partsPurchaseOrderCode, string partsSupplierName, string partsSupplierCode, int? arriveMethod, int? comfirmStatus, bool? ifDirectProvision, DateTime? createTimeBegin, DateTime? createTimeEnd, int? modifyStatus) {
            return new SupplierShippingOrderAch(this).GetSupplierShippingOrderWithDifCompany(code, partsPurchaseOrderCode, partsSupplierName, partsSupplierCode, arriveMethod, comfirmStatus, ifDirectProvision, createTimeBegin, createTimeEnd, modifyStatus);
        }
    }
}
