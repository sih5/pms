﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Dcs.Web.Entities;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class SupplierTraceCodeAch : DcsSerivceAchieveBase {
        internal void InsertSupplierTraceCodeValidate(SupplierTraceCode supplierTraceCode) {
            var userInfo = Utils.GetCurrentUserInfo();
            supplierTraceCode.CreatorId = userInfo.Id;
            supplierTraceCode.CreatorName = userInfo.Name;
            supplierTraceCode.CreateTime = DateTime.Now;

            if(string.IsNullOrWhiteSpace(supplierTraceCode.Code) || supplierTraceCode.Code == GlobalVar.ASSIGNED_BY_SERVER)
                supplierTraceCode.Code = CodeGenerator.Generate("SupplierTraceCode", "SIH");
        }
        internal void UpdateSupplierTraceCodeValidate(SupplierTraceCode supplierTraceCode) {
            var userInfo = Utils.GetCurrentUserInfo();
            supplierTraceCode.ModifierId = userInfo.Id;
            supplierTraceCode.ModifierName = userInfo.Name;
            supplierTraceCode.ModifyTime = DateTime.Now;
        }
        public void InsertSupplierTraceCode(SupplierTraceCode supplierTraceCode) {
            InsertToDatabase(supplierTraceCode);
            this.InsertSupplierTraceCodeValidate(supplierTraceCode);
        }

        public void UpdateSupplierTraceCode(SupplierTraceCode supplierTraceCode) {
          
        }
        public IQueryable<SupplierTraceCode> GetSupplierTraceCodeOrderByTime(string partsSupplierCode, string partsSupplierName, string sparePartCode, string partsPurchaseOrderCode, string oldTraceCode, string newTraceCode, string shippingCode,string code,int? status) {
            var traces=from a in  ObjectContext.SupplierTraceCodes
                           select a ;
            if(!string.IsNullOrEmpty(partsSupplierCode)){
                traces = traces.Where(t => t.SupplierTraceCodeDetails.Any(r => r.SupplierTraceCodeId == t.Id && r.PartsSupplierCode.Contains(partsSupplierCode)));
            }
            if(!string.IsNullOrEmpty(partsSupplierName)) {
                traces = traces.Where(t => t.SupplierTraceCodeDetails.Any(r => r.SupplierTraceCodeId == t.Id && r.PartsSupplierName.Contains(partsSupplierName)));
            }
            if(!string.IsNullOrEmpty(sparePartCode)) {
                traces = traces.Where(t => t.SupplierTraceCodeDetails.Any(r => r.SupplierTraceCodeId == t.Id && r.SparePartCode.Contains(sparePartCode)));
            }
            if(!string.IsNullOrEmpty(partsPurchaseOrderCode)) {
                traces = traces.Where(t => t.SupplierTraceCodeDetails.Any(r => r.SupplierTraceCodeId == t.Id && r.PartsPurchaseOrderCode.Contains(partsPurchaseOrderCode)));
            }
            if(!string.IsNullOrEmpty(oldTraceCode)) {
                traces = traces.Where(t => t.SupplierTraceCodeDetails.Any(r => r.SupplierTraceCodeId == t.Id && r.OldTraceCode.Contains(oldTraceCode)));
            }
            if(!string.IsNullOrEmpty(newTraceCode)) {
                traces = traces.Where(t => t.SupplierTraceCodeDetails.Any(r => r.SupplierTraceCodeId == t.Id && r.NewTraceCode.Contains(newTraceCode)));
            }
            if(!string.IsNullOrEmpty(shippingCode)) {
                traces = traces.Where(t => t.SupplierTraceCodeDetails.Any(r => r.SupplierTraceCodeId == t.Id && r.ShippingCode.Contains(shippingCode)));
            }
            if(status.HasValue){
                traces = traces.Where(t=>t.Status==status);
            }
            if(!string.IsNullOrEmpty(code)){
                traces = traces.Where(t=>t.Code.Contains(code));
            }
            return traces.OrderByDescending(t => t.CreateTime);
        }
        public IEnumerable<SupplierTraceCodeForAdd> getSupplierTraceCodeForAdd(string packingCode, string sparePartCode, string oldTraceCode) {
              string SQL = @"select pk.id,
                                   pk.code                         as PackingCode,
                                   atr.tracecode                   as OldTraceCode,
                                   pk.sparepartid,
                                   pk.sparepartcode,
                                   pk.SparePartName,
                                   pip.sourceid                    as ShippingId,
                                   pip.SourceCode                  as ShippingCode,
                                   pip.originalrequirementbillcode as PartsPurchaseOrderCode,
                                   pip.originalrequirementbillid   as PartsPurchaseOrderId,
                                   atr.traceproperty,
                                   pco.PartsSupplierId,
                                   pco.PartsSupplierCode,
                                   pco.PartsSupplierName
                              from packingtask pk
                              join partsinboundplan pip
                                on pk.partsinboundplanid = pip.id
                              join AccurateTrace atr
                                on pk.sparepartid = atr.partid
                               and pk.partsinboundplanid = atr.sourcebillid
                              join partspurchaseorder pco
                                on pip.originalrequirementbillid = pco.id
                               and pip.originalrequirementbilltype = 4
                        where  pk.status in(1,2)  
                                and not exists (select 1
                                  from SupplierTraceCodeDetail stcd
                                  join SupplierTraceCode stc
                                    on stcd.suppliertracecodeid = stc.id
                                 where stcd.oldtracecode = atr.tracecode
                                   and stcd.PartsPurchaseOrderId = pco.id
                                   and stc.status <> 99)";

            if(!string.IsNullOrEmpty(packingCode)) {
                SQL = SQL + " and pk.Code like '%" + packingCode + "%'";
            }
            if(!string.IsNullOrEmpty(sparePartCode)) {
                SQL = SQL + " and pk.sparePartCode like '%" + sparePartCode + "%'";
            }
            if(!string.IsNullOrEmpty(oldTraceCode)) {
                SQL = SQL + " and atr.TraceCode like '%" + oldTraceCode + "%'";
            }
            var search = ObjectContext.ExecuteStoreQuery<SupplierTraceCodeForAdd>(SQL).ToList();
            return search;
        }
        public SupplierTraceCode GetSupplierTraceCodesWithDetailsById(int id) {
            var dbSupplierTraceCode = ObjectContext.SupplierTraceCodes.Include("SupplierTraceCodeDetails").SingleOrDefault(entity => entity.Id == id && entity.Status != (int)DCSSupplierTraceCodeStatus.已作废);
            if(dbSupplierTraceCode != null) {
                var dbDetails = ObjectContext.SupplierTraceCodeDetails.Where(e => e.SupplierTraceCodeId == id).ToArray();
            }
            return dbSupplierTraceCode;
        }
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               

        public void InsertSupplierTraceCode(SupplierTraceCode supplierTraceCode) {
            new SupplierTraceCodeAch(this).InsertSupplierTraceCode(supplierTraceCode);
        }       
        public void UpdateSupplierTraceCode(SupplierTraceCode supplierTraceCode) {
            new SupplierTraceCodeAch(this).UpdateSupplierTraceCode(supplierTraceCode);
        }
        public IQueryable<SupplierTraceCode> GetSupplierTraceCodeOrderByTime(string partsSupplierCode, string partsSupplierName, string sparePartCode, string partsPurchaseOrderCode, string oldTraceCode, string newTraceCode, string shippingCode, string code, int? status) {
            return new SupplierTraceCodeAch(this).GetSupplierTraceCodeOrderByTime(partsSupplierCode, partsSupplierName, sparePartCode, partsPurchaseOrderCode, oldTraceCode, newTraceCode, shippingCode,code,status);
        }
        public IEnumerable<SupplierTraceCodeForAdd> getSupplierTraceCodeForAdd(string packingCode, string sparePartCode, string oldTraceCode) {
            return new SupplierTraceCodeAch(this).getSupplierTraceCodeForAdd(packingCode, sparePartCode, oldTraceCode);   
        }

         public SupplierTraceCode GetSupplierTraceCodesWithDetailsById(int id) {
             return new  SupplierTraceCodeAch(this).GetSupplierTraceCodesWithDetailsById(id);
         }

    }
}
