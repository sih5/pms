﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class SupplierTraceCodeDetailAch : DcsSerivceAchieveBase {
        public SupplierTraceCodeDetailAch(DcsDomainService domainService)
            : base(domainService) {
        }
        public void InsertSupplierTraceCodeDetail(SupplierTraceCodeDetail supplierTraceCode) {
        }

        public void UpdateSupplierTraceCodeDetail(SupplierTraceCodeDetail supplierTraceCode) {
        }
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               

        public void InsertSupplierTraceCodeDetail(SupplierTraceCodeDetail supplierTraceCode) {
            new SupplierTraceCodeDetailAch(this).InsertSupplierTraceCodeDetail(supplierTraceCode);
        }

        public void UpdateSupplierTraceCodeDetail(SupplierTraceCodeDetail supplierTraceCode) {
            new SupplierTraceCodeDetailAch(this).UpdateSupplierTraceCodeDetail(supplierTraceCode);
        }
    }
}
