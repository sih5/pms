﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Dcs.Web.Entities;

namespace Sunlight.Silverlight.Dcs.Web {
   partial class TemPurchaseOrderAch : DcsSerivceAchieveBase {
       public TemPurchaseOrderAch(DcsDomainService domainService)
            : base(domainService) {
        }
       public IQueryable<TemPurchaseOrder> GetTemPurchaseOrderForMan(string sparePartCode, bool? isSaleCode, bool? isPurchaseOrder) {
           var userInfo = Utils.GetCurrentUserInfo();
           var company = ObjectContext.Companies.Where(t => t.Id == userInfo.EnterpriseId).First();
           var result = ObjectContext.TemPurchaseOrders.Where(t => 1 == 1);
           if(!string.IsNullOrEmpty(sparePartCode)) {
               result = result.Where(o => o.TemPurchaseOrderDetails.Any(p => p.SparePartCode.Contains(sparePartCode)));
           }
           if(isSaleCode.HasValue && isSaleCode.Value) {
               result = result.Where(o => o.PartsSalesOrderCode!=null);

           } if(isSaleCode.HasValue && !isSaleCode.Value) {
               result = result.Where(o => o.PartsSalesOrderCode == null);

           }
           if(isPurchaseOrder.HasValue && isPurchaseOrder.Value) {
               result = result.Where(o => o.PartsPurchaseOrderCode != null);
           } 
           if(isPurchaseOrder.HasValue && !isPurchaseOrder.Value) {
               result = result.Where(o => o.PartsPurchaseOrderCode == null);
           }
           if(company.Type == (int)DcsCompanyType.分公司) {
               result = result.Where(t => t.OrderCompanyId != null);
           } else {
               result = result.Where(t =>t.SuplierId== userInfo.EnterpriseId);
           }
           var personnelSupplierRelation = ObjectContext.PersonnelSupplierRelations.Where(t => t.PersonId == userInfo.Id && t.Status == (int)DcsBaseDataStatus.有效).ToArray();
           if(personnelSupplierRelation.Count()>0) {
               result = result.Where(t => ObjectContext.PersonnelSupplierRelations.Any(y => y.PersonId == userInfo.Id && y.SupplierId == t.SuplierId && y.Status == (int)DcsBaseDataStatus.有效));
           }
           return result.OrderByDescending(t => t.Id);
       }
       public TemPurchaseOrder GetTemPurchaseOrdersWithDetailsById(int id) {
           var dbPartsPurchaseOrder = ObjectContext.TemPurchaseOrders.SingleOrDefault(entity => entity.Id == id );
           if(dbPartsPurchaseOrder != null) {
               var dbDetails = ObjectContext.TemPurchaseOrderDetails.Where(e => e.TemPurchaseOrderId == id).OrderBy(e => e.SparePartCode).ToArray();
           }
           return dbPartsPurchaseOrder;
       }
       internal void InsertTemPurchaseOrderValidate(TemPurchaseOrder temPurchaseOrder) {
           var userInfo = Utils.GetCurrentUserInfo();
           temPurchaseOrder.CreatorId = userInfo.Id;
           temPurchaseOrder.CreatorName = userInfo.Name;
           temPurchaseOrder.CreateTime = DateTime.Now;

           if(string.IsNullOrWhiteSpace(temPurchaseOrder.Code) || temPurchaseOrder.Code == GlobalVar.ASSIGNED_BY_SERVER)
               temPurchaseOrder.Code = CodeGenerator.Generate("TemPurchaseOrder", temPurchaseOrder.ReceCompanyCode);
       }
       public void InsertTemPurchaseOrder(TemPurchaseOrder temPurchaseOrder) {
           InsertToDatabase(temPurchaseOrder);
           var TemPurchaseOrderDetails = ChangeSet.GetAssociatedChanges(temPurchaseOrder, v => v.TemPurchaseOrderDetails, ChangeOperation.Insert);
           foreach(TemPurchaseOrderDetail detail in TemPurchaseOrderDetails) {
               InsertToDatabase(detail);
           }
       }
       internal void UpdateTemPurchaseOrderValidate(TemPurchaseOrder temPurchaseOrder) {
           var userInfo = Utils.GetCurrentUserInfo();
           temPurchaseOrder.ModifierId = userInfo.Id;
           temPurchaseOrder.ModifierName = userInfo.Name;
           temPurchaseOrder.ModifyTime = DateTime.Now;
       }
       public void UpdateTemPurchaseOrder(TemPurchaseOrder temPurchaseOrder) {
           temPurchaseOrder.TemPurchaseOrderDetails.Clear();
           UpdateToDatabase(temPurchaseOrder);
           var temPurchaseOrderDetails = ChangeSet.GetAssociatedChanges(temPurchaseOrder, v => v.TemPurchaseOrderDetails);
           foreach(TemPurchaseOrderDetail temPurchaseOrderDetail in temPurchaseOrderDetails) {
               switch(ChangeSet.GetChangeOperation(temPurchaseOrderDetail)) {
                   case ChangeOperation.Insert:
                       InsertToDatabase(temPurchaseOrderDetail);
                       break;
                   case ChangeOperation.Update:
                       UpdateToDatabase(temPurchaseOrderDetail);
                       break;
                   case ChangeOperation.Delete:
                       DeleteFromDatabase(temPurchaseOrderDetail);
                       break;
               }
           }
           this.UpdateTemPurchaseOrderValidate(temPurchaseOrder);
       }
       public IEnumerable<TemPurchaseOrderDetail> 查询临时采购订单看板(int temPurchaseOrderId) {
           var partsPurchaseOrders = ObjectContext.TemPurchaseOrders.Where(r => r.Id == temPurchaseOrderId ).ToArray();
           if(!partsPurchaseOrders.Any())
               return Enumerable.Empty<TemPurchaseOrderDetail>();
           var partsPurchaseOrderIds = partsPurchaseOrders.Select(v => v.Id).ToArray();
           var partsPurchaseOrderDetails = ObjectContext.TemPurchaseOrderDetails.Where(r => partsPurchaseOrderIds.Contains(r.TemPurchaseOrderId)).ToArray();
           var supplierShippingOrders = ObjectContext.TemSupplierShippingOrders.Where(r => partsPurchaseOrderIds.Contains(r.TemPurchaseOrderId.Value)).ToArray();
           if(supplierShippingOrders.Any()) {
               var supplierShippingOrderIds = supplierShippingOrders.Select(r => r.Id).ToArray();
               var supplierShippingOrderDetails = ObjectContext.TemShippingOrderDetails.Where(r => supplierShippingOrderIds.Contains(r.ShippingOrderId)).ToArray();
               foreach(var partsPurchaseOrderDetail in partsPurchaseOrderDetails) {
                   TemPurchaseOrderDetail detail = partsPurchaseOrderDetail;
                   var temp = supplierShippingOrderDetails.Where(r => r.SparePartId == detail.SparePartId);
                   foreach(var supplierShippingOrderDetail in temp) {
                       partsPurchaseOrderDetail.ReceiptAmount += supplierShippingOrderDetail.ConfirmedAmount ?? 0;
                   }
               }
           }
           return partsPurchaseOrderDetails.OrderBy(v => v.Id);
       }
    }
   partial class DcsDomainService {
       public IEnumerable<TemPurchaseOrderDetail> 查询临时采购订单看板(int temPurchaseOrderId) {
           return new TemPurchaseOrderAch(this).查询临时采购订单看板(temPurchaseOrderId);
       }
       public IQueryable<TemPurchaseOrder> GetTemPurchaseOrderForMan(string sparePartCode, bool? isSaleCode, bool? isPurchaseOrder) {
           return new TemPurchaseOrderAch(this).GetTemPurchaseOrderForMan(sparePartCode, isSaleCode, isPurchaseOrder);
       }
       public TemPurchaseOrder GetTemPurchaseOrdersWithDetailsById(int id) {
           return new TemPurchaseOrderAch(this).GetTemPurchaseOrdersWithDetailsById(id);
       }
   }
}
