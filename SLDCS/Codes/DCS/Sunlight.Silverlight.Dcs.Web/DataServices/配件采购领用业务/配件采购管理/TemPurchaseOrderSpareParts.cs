﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
using System.Configuration;
using System.Data;
using System.Text;
using Devart.Data.Oracle;
using NPOI.SS.Formula.Functions;
using System.Transactions;

namespace Sunlight.Silverlight.Dcs.Web
{
    partial class TemPurchaseOrderSparePartsAch : DcsSerivceAchieveBase {
        public TemPurchaseOrderSparePartsAch(DcsDomainService domainService)
            : base(domainService) {
        }
        public IEnumerable<TemPurchaseOrderSpareParts> getTemPurchaseOrderSpareParts(string code, string sparePartCode, string suplierCode, string suplierName, bool? isPurchase, bool? isSales)
        {
            var userInfo = Utils.GetCurrentUserInfo();
            string SQL = @"select tho.code,
                           tod.sparepartcode,
                           tod.sparepartname,
                           tho.suplierid,
                           tho.supliercode,
                           tho.supliername,
                           ppp.purchaseprice,
                           psp.SalesPrice,
                           psp.centerprice,
                           tod.id,
                           decode(ppp.id, null, 0, 1) as IsPurchase,
                           decode(psp.id, null, 0, 1) as IsSales
                      from TemPurchaseOrder tho
                      join TemPurchaseOrderDetail tod
                        on tho.id = tod.TemPurchaseOrderId
                      left join PartsPurchasePricing ppp
                        on tho.suplierid = ppp.partssupplierid
                       and tod.sparepartid = ppp.partid
                       and ppp.status = 2
                       and trunc(ppp.validfrom) <= trunc(sysdate)
                       and trunc(ppp.validto) >= trunc(sysdate)
                      left join partssalesprice psp
                        on tod.sparepartid = psp.sparepartid
                       and psp.status = 1
                     where tho.status <> 7
                       and (not exists
                            (select 1
                               from PartsSupplierRelation pr
                              where pr.supplierid = tho.suplierid
                                and tod.sparepartid = pr.partid
                                and pr.status = 1) or ppp.id is null or psp.id is null)";
            if (!string.IsNullOrEmpty(code))
            {
                SQL = SQL + " and tho.code like '%" + code + "%'";
            }
            if (!string.IsNullOrEmpty(sparePartCode))
            {
                SQL = SQL + " and tod.sparePartCode like '%" + sparePartCode + "%'";
            }

            if (!string.IsNullOrEmpty(suplierCode))
            {
                SQL = SQL + " and tho.suplierCode like '%" + suplierCode + "%'";
            }
            if (!string.IsNullOrEmpty(suplierName))
            {
                SQL = SQL + " and tho.suplierName like '%" + suplierName + "%'";

            }

            if (isPurchase.HasValue)
            {
                if (isPurchase.Value)
                {
                    SQL = SQL + " and ppp.id is not null  ";
                }
                else
                {
                    SQL = SQL + " and  ppp.id is  null  ";
                }
           }
            if (isSales.HasValue)
            {
                if (isSales.Value)
                {
                    SQL = SQL + " and  psp.id is not null  ";
                }
                else
                {
                    SQL = SQL + " and  psp.id is  null  ";
                }
            }
            var search = ObjectContext.ExecuteStoreQuery<TemPurchaseOrderSpareParts>(SQL).ToList();
            return search;
        }
    }
    partial class DcsDomainService
    {
        public IEnumerable<TemPurchaseOrderSpareParts> getTemPurchaseOrderSpareParts(string code, string sparePartCode, string suplierCode, string suplierName, bool? isPurchase, bool? isSales)
          
        {
            return new TemPurchaseOrderSparePartsAch(this).getTemPurchaseOrderSpareParts(code, sparePartCode, suplierCode, suplierName, isPurchase, isSales);
        }
    }
}
