﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class TemPurchasePlanOrderAch : DcsSerivceAchieveBase {
        public TemPurchasePlanOrderAch(DcsDomainService domainService)
            : base(domainService) {
        }
        public IQueryable<TemPurchasePlanOrder> GetTemPurchasePlanOrder(string sparePartCode) {
            if(string.IsNullOrEmpty(sparePartCode)) {
                return ObjectContext.TemPurchasePlanOrders.Include("TemPurchasePlanOrderDetails").OrderByDescending(r => r.Id);
            } else {
                return ObjectContext.TemPurchasePlanOrders.Include("TemPurchasePlanOrderDetails").OrderByDescending(r => r.Id).Where(o => o.TemPurchasePlanOrderDetails.Any(p => p.SparePartCode.IndexOf(sparePartCode) >= 0));
            }
        }
        public IQueryable<TemPurchasePlanOrder> GetTemPurchasePlanOrderForMan(string sparePartCode) {
            var userInfo = Utils.GetCurrentUserInfo();
            var company = ObjectContext.Companies.Where(t=>t.Id==userInfo.EnterpriseId).First();
            var result = ObjectContext.TemPurchasePlanOrders.Include("TemPurchasePlanOrderDetails").Where(t=>1==1);
            if(!string.IsNullOrEmpty(sparePartCode)) {
              result=  result.Where(o => o.TemPurchasePlanOrderDetails.Any(p => p.SparePartCode.IndexOf(sparePartCode) >= 0));
            }
            if(company.Type==(int)DcsCompanyType.分公司) {
                result = result.Where(t=>t.OrderCompanyId!=null);
            } else {
                result = result.Where(t => ObjectContext.AgencyDealerRelations.Any(y=>y.DealerId==t.ReceCompanyId && y.AgencyId==userInfo.EnterpriseId));
            }
            return result.OrderByDescending(t=>t.Id);
        }
        public TemPurchasePlanOrder GetTemPurchasePlansWithDetailsById(int id) {
            var dbPartsPurchasePlan = ObjectContext.TemPurchasePlanOrders.SingleOrDefault(entity => entity.Id == id);
            if(dbPartsPurchasePlan != null) {
                var dbDetails = ObjectContext.TemPurchasePlanOrderDetails.Where(e => e.TemPurchasePlanOrderId == id).OrderBy(e => e.SparePartCode).ToArray();
            }
            return dbPartsPurchasePlan;
        }
        internal void InsertTemPurchasePlanOrderValidate(TemPurchasePlanOrder temPurchasePlanOrder) {
            var userInfo = Utils.GetCurrentUserInfo();
            temPurchasePlanOrder.CreatorId = userInfo.Id;
            temPurchasePlanOrder.CreatorName = userInfo.Name;
            temPurchasePlanOrder.CreateTime = DateTime.Now;

            if(string.IsNullOrWhiteSpace(temPurchasePlanOrder.Code) || temPurchasePlanOrder.Code == GlobalVar.ASSIGNED_BY_SERVER)
                temPurchasePlanOrder.Code = CodeGenerator.Generate("TemPurchasePlanOrder", temPurchasePlanOrder.ReceCompanyCode);
        }
        public void InsertTemPurchasePlanOrder(TemPurchasePlanOrder temPurchasePlanOrder) {
            InsertToDatabase(temPurchasePlanOrder);
            var temPurchasePlanOrderDetails = ChangeSet.GetAssociatedChanges(temPurchasePlanOrder, v => v.TemPurchasePlanOrderDetails, ChangeOperation.Insert);
            foreach(TemPurchasePlanOrderDetail detail in temPurchasePlanOrderDetails) {
                InsertToDatabase(detail);
            }
        }
        internal void UpdateTemPurchasePlanOrderValidate(TemPurchasePlanOrder temPurchasePlanOrder) {
            var userInfo = Utils.GetCurrentUserInfo();
            temPurchasePlanOrder.ModifierId = userInfo.Id;
            temPurchasePlanOrder.ModifierName = userInfo.Name;
            temPurchasePlanOrder.ModifyTime = DateTime.Now;
        }
        public void UpdateTemPurchasePlanOrder(TemPurchasePlanOrder temPurchasePlanOrder) {
            temPurchasePlanOrder.TemPurchasePlanOrderDetails.Clear();
            UpdateToDatabase(temPurchasePlanOrder);
            var temPurchasePlanOrderDetails = ChangeSet.GetAssociatedChanges(temPurchasePlanOrder, v => v.TemPurchasePlanOrderDetails);
            foreach(TemPurchasePlanOrderDetail temPurchasePlanOrderDetail in temPurchasePlanOrderDetails) {
                switch(ChangeSet.GetChangeOperation(temPurchasePlanOrderDetail)) {
                    case ChangeOperation.Insert:
                        InsertToDatabase(temPurchasePlanOrderDetail);
                        break;
                    case ChangeOperation.Update:
                        UpdateToDatabase(temPurchasePlanOrderDetail);
                        break;
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(temPurchasePlanOrderDetail);
                        break;
                }
            }
            this.UpdateTemPurchasePlanOrderValidate(temPurchasePlanOrder);
        }
    }
    partial class DcsDomainService {
        public IQueryable<TemPurchasePlanOrder> GetTemPurchasePlanOrder(string sparePartCode) {
            return new TemPurchasePlanOrderAch(this).GetTemPurchasePlanOrder(sparePartCode);
        }
        public TemPurchasePlanOrder GetTemPurchasePlansWithDetailsById(int id) {
            return new TemPurchasePlanOrderAch(this).GetTemPurchasePlansWithDetailsById(id);
        }
        public void InsertTemPurchasePlanOrder(TemPurchasePlanOrder temPurchasePlanOrder) {
            new TemPurchasePlanOrderAch(this).InsertTemPurchasePlanOrder(temPurchasePlanOrder);
        }
        public void UpdatePartsPurchasePlan(TemPurchasePlanOrder temPurchasePlanOrder) {
            new TemPurchasePlanOrderAch(this).UpdateTemPurchasePlanOrder(temPurchasePlanOrder);
        }
        public IQueryable<TemPurchasePlanOrder> GetTemPurchasePlanOrderForMan(string sparePartCode) {

            return new TemPurchasePlanOrderAch(this).GetTemPurchasePlanOrderForMan(sparePartCode);
        }
    }
}
