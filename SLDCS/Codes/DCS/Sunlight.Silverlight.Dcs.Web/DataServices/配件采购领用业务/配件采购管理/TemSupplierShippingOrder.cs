﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
using System.Collections.Generic;
using Sunlight.Silverlight.Dcs.Web.Entities;
using System.Transactions;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class TemSupplierShippingOrderAch : DcsSerivceAchieveBase {
        public TemSupplierShippingOrderAch(DcsDomainService domainService)
            : base(domainService) {
        }
        internal void InsertTemSupplierShippingOrderValidate(TemSupplierShippingOrder temSupplierShippingOrder) {
            var userInfo = Utils.GetCurrentUserInfo();
            temSupplierShippingOrder.CreatorId = userInfo.Id;
            temSupplierShippingOrder.CreatorName = userInfo.Name;
            temSupplierShippingOrder.CreateTime = DateTime.Now;

            if(string.IsNullOrWhiteSpace(temSupplierShippingOrder.Code) || temSupplierShippingOrder.Code == GlobalVar.ASSIGNED_BY_SERVER)
                temSupplierShippingOrder.Code = CodeGenerator.Generate("TemSupplierShippingOrder", temSupplierShippingOrder.PartsSupplierCode);
        }
        public void InsertTemSupplierShippingOrder(TemSupplierShippingOrder temPurchasePlanOrder) {
            InsertToDatabase(temPurchasePlanOrder);
            var temPurchasePlanOrderDetails = ChangeSet.GetAssociatedChanges(temPurchasePlanOrder, v => v.TemShippingOrderDetails, ChangeOperation.Insert);
            foreach(TemShippingOrderDetail detail in temPurchasePlanOrderDetails) {
                InsertToDatabase(detail);
            }
        }
        internal void UpdateTemSupplierShippingOrderValidate(TemSupplierShippingOrder temPurchasePlanOrder) {
            var userInfo = Utils.GetCurrentUserInfo();
            temPurchasePlanOrder.ModifierId = userInfo.Id;
            temPurchasePlanOrder.ModifierName = userInfo.Name;
            temPurchasePlanOrder.ModifyTime = DateTime.Now;
        }
        public void UpdateTemSupplierShippingOrder(TemSupplierShippingOrder temPurchasePlanOrder) {
            temPurchasePlanOrder.TemShippingOrderDetails.Clear();
            UpdateToDatabase(temPurchasePlanOrder);
            var temPurchasePlanOrderDetails = ChangeSet.GetAssociatedChanges(temPurchasePlanOrder, v => v.TemShippingOrderDetails);
            foreach(TemShippingOrderDetail temPurchasePlanOrderDetail in temPurchasePlanOrderDetails) {
                switch(ChangeSet.GetChangeOperation(temPurchasePlanOrderDetail)) {
                    case ChangeOperation.Insert:
                        InsertToDatabase(temPurchasePlanOrderDetail);
                        break;
                    case ChangeOperation.Update:
                        UpdateToDatabase(temPurchasePlanOrderDetail);
                        break;
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(temPurchasePlanOrderDetail);
                        break;
                }
            }
            this.UpdateTemSupplierShippingOrderValidate(temPurchasePlanOrder);
        }
        public IQueryable<VirtualTemPurchaseOrderDetail> 临时订单汇总发运查询(int? partsSupplierId) {
            var result = from t1 in ObjectContext.TemPurchaseOrders.Where(r => (r.Status == (int)DCSTemPurchaseOrderStatus.确认完毕||r.Status == (int)DCSTemPurchaseOrderStatus.部分发运) && r.SuplierId == partsSupplierId)
                         join t2 in ObjectContext.TemPurchaseOrderDetails on t1.Id equals t2.TemPurchaseOrderId
                         select new VirtualTemPurchaseOrderDetail {
                             SuplierCode = t1.SuplierCode,
                             SparePartId = t2.SparePartId.Value,
                             SparePartCode = t2.SparePartCode,
                             SupplierPartCode = t2.SupplierPartCode,
                             SparePartName = t2.SparePartName,
                             TemPurchaseOrderCode = t1.Code,
                             CreateTime = t1.CreateTime,
                             PlanAmount = t2.ConfirmedAmount,
                             ShippingAmount = t2.ShippingAmount ?? 0,
                             UnShippingAmount = t2.ConfirmedAmount - t2.ShippingAmount ?? 0,
                             MeasureUnit = t2.MeasureUnit,
                             TemPurchaseOrderId=t1.Id,
                             TemPurchaseOrderStatus=t1.Status,
                             OrderType=t1.OrderType.Value,
                             ConfirmAmount = t2.ConfirmedAmount,
                             OrderCompanyCode=t1.OrderCompanyCode,
                             OrderCompanyName=t1.OrderCompanyName,
                             Remark=t2.Remark                             
                         };
            return result.Where(r => r.PlanAmount > r.ShippingAmount).OrderBy(r => r.TemPurchaseOrderCode);
        }
        public TemSupplierShippingOrder GetTemSupplierShippingOrderWithDetailsById(int id) {
            var dbPartsPurchaseOrder = ObjectContext.TemSupplierShippingOrders.SingleOrDefault(entity => entity.Id == id);
            if(dbPartsPurchaseOrder != null) {
                var dbDetails = ObjectContext.TemShippingOrderDetails.Where(e => e.ShippingOrderId == id).OrderBy(e => e.SparePartCode).ToArray();
            }
            return dbPartsPurchaseOrder;
        }
    }
    partial class DcsDomainService {
        public void InsertTemSupplierShippingOrder(TemSupplierShippingOrder temPurchasePlanOrder) {
            new TemSupplierShippingOrderAch(this).InsertTemSupplierShippingOrder(temPurchasePlanOrder);
        }
        public void UpdatePartsPurchasePlan(TemSupplierShippingOrder temPurchasePlanOrder) {
            new TemSupplierShippingOrderAch(this).UpdateTemSupplierShippingOrder(temPurchasePlanOrder);
        }
        public IQueryable<VirtualTemPurchaseOrderDetail> 临时订单汇总发运查询(int? partsSupplierId) {
            return new TemSupplierShippingOrderAch(this).临时订单汇总发运查询(partsSupplierId);
        }
        public TemSupplierShippingOrder GetTemSupplierShippingOrderWithDetailsById(int id) {
            return new TemSupplierShippingOrderAch(this).GetTemSupplierShippingOrderWithDetailsById(id);
        }
    }
}
