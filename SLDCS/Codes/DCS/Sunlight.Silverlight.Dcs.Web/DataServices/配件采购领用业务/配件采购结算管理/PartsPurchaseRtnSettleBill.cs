﻿using System;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Devart.Common.Entity;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsPurchaseRtnSettleBillAch : DcsSerivceAchieveBase {
        internal void InsertPartsPurchaseRtnSettleBillValidate(PartsPurchaseRtnSettleBill partsPurchaseRtnSettleBill) {
            var userInfo = Utils.GetCurrentUserInfo();
            partsPurchaseRtnSettleBill.CreatorId = userInfo.Id;
            partsPurchaseRtnSettleBill.CreatorName = userInfo.Name;
            partsPurchaseRtnSettleBill.CreateTime = DateTime.Now;

            if(string.IsNullOrWhiteSpace(partsPurchaseRtnSettleBill.Code) || partsPurchaseRtnSettleBill.Code == GlobalVar.ASSIGNED_BY_SERVER)
                partsPurchaseRtnSettleBill.Code = CodeGenerator.Generate("PartsPurchaseRtnSettleBill", partsPurchaseRtnSettleBill.BranchCode);
        }

        internal void UpdatePartsPurchaseRtnSettleBillValidate(PartsPurchaseRtnSettleBill partsPurchaseRtnSettleBill) {
            var userInfo = Utils.GetCurrentUserInfo();
            partsPurchaseRtnSettleBill.ModifierId = userInfo.Id;
            partsPurchaseRtnSettleBill.ModifierName = userInfo.Name;
            partsPurchaseRtnSettleBill.ModifyTime = DateTime.Now;
        }

        public void InsertPartsPurchaseRtnSettleBill(PartsPurchaseRtnSettleBill partsPurchaseRtnSettleBill) {
            InsertToDatabase(partsPurchaseRtnSettleBill);
            var partsPurchaseRtnSettleDetails = ChangeSet.GetAssociatedChanges(partsPurchaseRtnSettleBill, v => v.PartsPurchaseRtnSettleDetails, ChangeOperation.Insert);
            foreach(PartsPurchaseRtnSettleDetail partsPurchaseRtnSettleDetail in partsPurchaseRtnSettleDetails) {
                InsertToDatabase(partsPurchaseRtnSettleDetail);
            }
            var partsPurchaseRtnSettleRefs = ChangeSet.GetAssociatedChanges(partsPurchaseRtnSettleBill, v => v.PartsPurchaseRtnSettleRefs, ChangeOperation.Insert);
            foreach(PartsPurchaseRtnSettleRef partsPurchaseRtnSettleRef in partsPurchaseRtnSettleRefs) {
                InsertToDatabase(partsPurchaseRtnSettleRef);
            }
            this.InsertPartsPurchaseRtnSettleBillValidate(partsPurchaseRtnSettleBill);
        }

        public void UpdatePartsPurchaseRtnSettleBill(PartsPurchaseRtnSettleBill partsPurchaseRtnSettleBill) {
            partsPurchaseRtnSettleBill.PartsPurchaseRtnSettleDetails.Clear();
            partsPurchaseRtnSettleBill.PartsPurchaseRtnSettleRefs.Clear();
            UpdateToDatabase(partsPurchaseRtnSettleBill);
            var partsPurchaseRtnSettleDetails = ChangeSet.GetAssociatedChanges(partsPurchaseRtnSettleBill, v => v.PartsPurchaseRtnSettleDetails);
            foreach(PartsPurchaseRtnSettleDetail partsPurchaseRtnSettleDetail in partsPurchaseRtnSettleDetails) {
                switch(ChangeSet.GetChangeOperation(partsPurchaseRtnSettleDetail)) {
                    case ChangeOperation.Insert:
                        InsertToDatabase(partsPurchaseRtnSettleDetail);
                        break;
                    case ChangeOperation.Update:
                        UpdateToDatabase(partsPurchaseRtnSettleDetail);
                        break;
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(partsPurchaseRtnSettleDetail);
                        break;
                }
            }
            var partsPurchaseRtnSettleRefs = ChangeSet.GetAssociatedChanges(partsPurchaseRtnSettleBill, v => v.PartsPurchaseRtnSettleRefs);
            foreach(PartsPurchaseRtnSettleRef partsPurchaseRtnSettleRef in partsPurchaseRtnSettleRefs) {
                switch(ChangeSet.GetChangeOperation(partsPurchaseRtnSettleRef)) {
                    case ChangeOperation.Insert:
                        InsertToDatabase(partsPurchaseRtnSettleRef);
                        break;
                    case ChangeOperation.Update:
                        UpdateToDatabase(partsPurchaseRtnSettleRef);
                        break;
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(partsPurchaseRtnSettleRef);
                        break;
                }
            }
            this.UpdatePartsPurchaseRtnSettleBillValidate(partsPurchaseRtnSettleBill);
        }

        public PartsPurchaseRtnSettleBill GetPartsPurchaseRtnSettleBillsWithDetailsAndRefsById(int id) {
            var partsPurchaseRtnSettleBill = ObjectContext.PartsPurchaseRtnSettleBills.SingleOrDefault(entity => entity.Id == id);
            if(partsPurchaseRtnSettleBill != null) {
                var dbDetails = ObjectContext.PartsPurchaseRtnSettleDetails.Where(e => e.PartsPurchaseRtnSettleBillId == id).ToArray();
                var dbRefs = ObjectContext.PartsPurchaseRtnSettleRefs.Where(e => e.PartsPurchaseRtnSettleBillId == id).ToArray();
            }
            return partsPurchaseRtnSettleBill;
        }

        public IQueryable<VirtualPartsPurchaseRtnSettleBill> 查询虚拟配件采购退货结算成本(int partsPurchaseRtnSettleBillId) {
            var partsPurchaseRtnSettleBills = from partsPurchaseRtnSettleBill in ObjectContext.PartsPurchaseRtnSettleBills.Where(r => r.Id == partsPurchaseRtnSettleBillId)
                                              from partsPurchaseRtnSettleRef in ObjectContext.PartsPurchaseRtnSettleRefs
                                              from partsOutboundBill in ObjectContext.PartsOutboundBills
                                              from partsOutboundBillDetail in ObjectContext.PartsOutboundBillDetails
                                              from sparePart in ObjectContext.SpareParts
                                              where sparePart.Id == partsOutboundBillDetail.SparePartId && partsPurchaseRtnSettleRef.PartsPurchaseRtnSettleBillId == partsPurchaseRtnSettleBill.Id && partsPurchaseRtnSettleRef.SourceId == partsOutboundBill.Id && partsOutboundBillDetail.PartsOutboundBillId == partsOutboundBill.Id
                                              select new VirtualPartsPurchaseRtnSettleBill {
                                                  Code = partsPurchaseRtnSettleBill.Code,
                                                  BillCode = partsOutboundBill.Code,
                                                  BillBusinessType = "出库",
                                                  SparePartCode = sparePart.Code,
                                                  SparePartName = sparePart.Name,
                                                  Quantity = partsOutboundBillDetail.OutboundAmount,
                                                  SettlementPrice = partsOutboundBillDetail.SettlementPrice,
                                                  SettlementAmount = partsOutboundBillDetail.SettlementPrice * partsOutboundBillDetail.OutboundAmount,
                                                  PlanPrice = partsOutboundBillDetail.CostPrice,
                                                  PlanAmount = partsOutboundBillDetail.CostPrice * partsOutboundBillDetail.OutboundAmount,
                                                  MaterialCostVariance = partsOutboundBillDetail.SettlementPrice * partsOutboundBillDetail.OutboundAmount - partsOutboundBillDetail.CostPrice * partsOutboundBillDetail.OutboundAmount,
                                              };
            var partsPurchaseRtnSettleBillss = from partsPurchaseRtnSettleBill in ObjectContext.PartsPurchaseRtnSettleBills.Where(r => r.Id == partsPurchaseRtnSettleBillId)
                                               from partsPurchaseRtnSettleRef in ObjectContext.PartsPurchaseRtnSettleRefs
                                               from partsInboundCheckBill in ObjectContext.PartsInboundCheckBills
                                               from partsInboundCheckBillDetail in ObjectContext.PartsInboundCheckBillDetails
                                               from sparePart in ObjectContext.SpareParts
                                               where sparePart.Id == partsInboundCheckBillDetail.SparePartId && partsPurchaseRtnSettleRef.PartsPurchaseRtnSettleBillId == partsPurchaseRtnSettleBill.Id && partsPurchaseRtnSettleRef.SourceId == partsInboundCheckBill.Id && partsInboundCheckBillDetail.PartsInboundCheckBillId == partsInboundCheckBill.Id
                                               select new VirtualPartsPurchaseRtnSettleBill {
                                                   Code = partsPurchaseRtnSettleBill.Code,
                                                   BillCode = partsInboundCheckBill.Code,
                                                   BillBusinessType = "入库",
                                                   SparePartCode = sparePart.Code,
                                                   SparePartName = sparePart.Name,
                                                   Quantity = partsInboundCheckBillDetail.InspectedQuantity,
                                                   SettlementPrice = partsInboundCheckBillDetail.SettlementPrice,
                                                   SettlementAmount = partsInboundCheckBillDetail.SettlementPrice * partsInboundCheckBillDetail.InspectedQuantity,
                                                   PlanPrice = partsInboundCheckBillDetail.CostPrice,
                                                   PlanAmount = partsInboundCheckBillDetail.CostPrice * partsInboundCheckBillDetail.InspectedQuantity,
                                                   MaterialCostVariance = partsInboundCheckBillDetail.SettlementPrice * partsInboundCheckBillDetail.InspectedQuantity - partsInboundCheckBillDetail.CostPrice * partsInboundCheckBillDetail.InspectedQuantity,
                                               };
            return partsPurchaseRtnSettleBills.Union(partsPurchaseRtnSettleBillss).OrderBy(r => r.Code);
        }

        public IQueryable<InvoiceInformation> 查询配件采购退货发票(int id) {
            return ObjectContext.InvoiceInformations.Where(r => r.SourceId == id);
        }

        public IQueryable<VirtualPartsPurchaseRtnSettleBillWithBusinessCode> GetPartsPurchaseRtnSettleBillsWithBusinessCode() {
            var result = from a in ObjectContext.PartsPurchaseRtnSettleBills

                         join i in ObjectContext.InvoiceInformations.Where(r => r.SourceType == (int)DcsInvoiceInformationSourceType.配件采购退货结算单)
                         on a.Id equals i.SourceId into tempdata
                         from t in tempdata.DefaultIfEmpty()
                         group t by a into v
                         join b in ObjectContext.BranchSupplierRelations.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on new {
                             PartsSalesCategoryId = v.Key.PartsSalesCategoryId ?? 0,
                             SupplierId = v.Key.PartsSupplierId
                         }equals new {
                             PartsSalesCategoryId = b.PartsSalesCategoryId,
                             SupplierId = b.SupplierId
                         }
                         select new VirtualPartsPurchaseRtnSettleBillWithBusinessCode {
                             Id = v.Key.Id,
                             Code = v.Key.Code,
                             WarehouseId = v.Key.WarehouseId,
                             WarehouseCode = v.Key.WarehouseCode,
                             WarehouseName = v.Key.WarehouseName,
                             BranchId = v.Key.BranchId,
                             BranchCode = v.Key.BranchCode,
                             BranchName = v.Key.BranchName,
                             PartsSalesCategoryId = v.Key.PartsSalesCategoryId,
                             PartsSalesCategoryName = v.Key.PartsSalesCategoryName,
                             PartsSupplierId = v.Key.PartsSupplierId,
                             PartsSupplierCode = v.Key.PartsSupplierCode,
                             PartsSupplierName = v.Key.PartsSupplierName,
                             TotalSettlementAmount = v.Key.TotalSettlementAmount,
                             OffsettedSettlementBillId = v.Key.OffsettedSettlementBillId,
                             OffsettedSettlementBillCode = v.Key.OffsettedSettlementBillCode,
                             InvoiceAmountDifference = v.Key.InvoiceAmountDifference,
                             InvoiceTotalAmount = v.Sum(t => t.InvoiceAmount ?? 0m),
                             TaxRate = v.Key.TaxRate,
                             Tax = Math.Round(v.Key.Tax, 2),
                             InvoicePath = v.Key.InvoicePath,
                             Status = v.Key.Status,
                             SettlementPath = v.Key.SettlementPath,
                             Remark = v.Key.Remark,
                             CreatorId = v.Key.CreatorId,
                             CreatorName = v.Key.CreatorName,
                             CreateTime = v.Key.CreateTime,
                             ModifierId = v.Key.ModifierId,
                             ModifierName = v.Key.ModifierName,
                             ModifyTime = v.Key.ModifyTime,
                             AbandonerId = v.Key.AbandonerId,
                             AbandonerName = v.Key.AbandonerName,
                             AbandonTime = v.Key.AbandonTime,
                             ApproverId = v.Key.ApproverId,
                             ApproverName = v.Key.ApproverName,
                             ApproveTime = v.Key.ApproveTime,
                             BusinessCode = b.BusinessCode,
                             InvoiceDate = v.Key.InvoiceDate,
                             TotalAmount = v.Key.TotalSettlementAmount + v.Key.Tax
                         };
            return result.OrderBy(e => e.Id);
        }
        
        public IQueryable<PartsPurchaseRtnSettleDetail> GetPartsPurchaseRtnSettleDetailsWithSupplierPartCode() {
            var result = from a in ObjectContext.PartsPurchaseRtnSettleDetails
                         join b in ObjectContext.PartsPurchaseRtnSettleBills on a.PartsPurchaseRtnSettleBillId equals b.Id
                         join c in ObjectContext.PartsSupplierRelations.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on new {
                             a.SparePartId,
                             b.PartsSupplierId
                         } equals new {
                             SparePartId = c.PartId,
                             PartsSupplierId = c.SupplierId
                         } into temp
                         from t in temp.DefaultIfEmpty()
                         select new {
                             a,
                             t.SupplierPartCode
                         };
            var result1 = result.Select(q => q.a);
            result1 = result1.OrderByDescending(r => r.Id);
            var ids1 = DcsDomainService.QueryComposer.Compose(result1, ParamQueryable).Cast<PartsPurchaseRtnSettleDetail>().Select(q => q.Id).ToArray();
            var temp1 = from q in result.Where(p => ids1.Contains(p.a.Id))
                       select new {
                           q.a,
                           SupplierPartCode = q.SupplierPartCode
                       };
            foreach(var tc in temp1) {
                tc.a.SupplierPartCode = tc.SupplierPartCode;
            }
            return result1.OrderBy(t=>t.SparePartId);
        }
    
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               

        public void InsertPartsPurchaseRtnSettleBill(PartsPurchaseRtnSettleBill partsPurchaseRtnSettleBill) {
            new PartsPurchaseRtnSettleBillAch(this).InsertPartsPurchaseRtnSettleBill(partsPurchaseRtnSettleBill);
        }

        public void UpdatePartsPurchaseRtnSettleBill(PartsPurchaseRtnSettleBill partsPurchaseRtnSettleBill) {
            new PartsPurchaseRtnSettleBillAch(this).UpdatePartsPurchaseRtnSettleBill(partsPurchaseRtnSettleBill);
        }

        public PartsPurchaseRtnSettleBill GetPartsPurchaseRtnSettleBillsWithDetailsAndRefsById(int id) {
            return new PartsPurchaseRtnSettleBillAch(this).GetPartsPurchaseRtnSettleBillsWithDetailsAndRefsById(id);
        }

        public IQueryable<VirtualPartsPurchaseRtnSettleBill> 查询虚拟配件采购退货结算成本(int partsPurchaseRtnSettleBillId) {
            return new PartsPurchaseRtnSettleBillAch(this).查询虚拟配件采购退货结算成本(partsPurchaseRtnSettleBillId);
        }

        public IQueryable<InvoiceInformation> 查询配件采购退货发票(int id) {
            return new PartsPurchaseRtnSettleBillAch(this).查询配件采购退货发票(id);
        }

        public IQueryable<VirtualPartsPurchaseRtnSettleBillWithBusinessCode> GetPartsPurchaseRtnSettleBillsWithBusinessCode() {
            return new PartsPurchaseRtnSettleBillAch(this).GetPartsPurchaseRtnSettleBillsWithBusinessCode();
        }
        public IQueryable<PartsPurchaseRtnSettleDetail> GetPartsPurchaseRtnSettleDetailsWithSupplierPartCode() {
            return new PartsPurchaseRtnSettleBillAch(this).GetPartsPurchaseRtnSettleDetailsWithSupplierPartCode();
        }
    }
}
