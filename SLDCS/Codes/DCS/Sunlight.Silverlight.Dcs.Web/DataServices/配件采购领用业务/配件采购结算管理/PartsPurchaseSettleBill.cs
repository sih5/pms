﻿using System;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Devart.Common.Entity;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsPurchaseSettleBillAch : DcsSerivceAchieveBase {
        internal void InsertPartsPurchaseSettleBillValidate(PartsPurchaseSettleBill partsPurchaseSettleBill) {
            var userInfo = Utils.GetCurrentUserInfo();
            partsPurchaseSettleBill.CreatorId = userInfo.Id;
            partsPurchaseSettleBill.CreatorName = userInfo.Name;
            partsPurchaseSettleBill.CreateTime = DateTime.Now;

            if(string.IsNullOrWhiteSpace(partsPurchaseSettleBill.Code) || partsPurchaseSettleBill.Code == GlobalVar.ASSIGNED_BY_SERVER)
                partsPurchaseSettleBill.Code = CodeGenerator.Generate("PartsPurchaseSettleBill", partsPurchaseSettleBill.BranchCode);
        }

        internal void UpdatePartsPurchaseSettleBillValidate(PartsPurchaseSettleBill partsPurchaseSettleBill) {
            var userInfo = Utils.GetCurrentUserInfo();
            partsPurchaseSettleBill.ModifierId = userInfo.Id;
            partsPurchaseSettleBill.ModifierName = userInfo.Name;
            partsPurchaseSettleBill.ModifyTime = DateTime.Now;
        }

        public void InsertPartsPurchaseSettleBill(PartsPurchaseSettleBill partsPurchaseSettleBill) {
            InsertToDatabase(partsPurchaseSettleBill);
            var partsPurchaseSettleDetails = ChangeSet.GetAssociatedChanges(partsPurchaseSettleBill, v => v.PartsPurchaseSettleDetails, ChangeOperation.Insert);
            foreach(PartsPurchaseSettleDetail partsPurchaseSettleDetail in partsPurchaseSettleDetails) {
                InsertToDatabase(partsPurchaseSettleDetail);
            }
            var partsPurchaseSettleRefs = ChangeSet.GetAssociatedChanges(partsPurchaseSettleBill, v => v.PartsPurchaseSettleRefs, ChangeOperation.Insert);
            foreach(PartsPurchaseSettleRef partsPurchaseSettleRef in partsPurchaseSettleRefs) {
                InsertToDatabase(partsPurchaseSettleRef);
            }
            this.InsertPartsPurchaseSettleBillValidate(partsPurchaseSettleBill);
        }

        public void UpdatePartsPurchaseSettleBill(PartsPurchaseSettleBill partsPurchaseSettleBill) {
            partsPurchaseSettleBill.PartsPurchaseSettleDetails.Clear();
            partsPurchaseSettleBill.PartsPurchaseSettleRefs.Clear();
            UpdateToDatabase(partsPurchaseSettleBill);
            var partsPurchaseSettleDetails = ChangeSet.GetAssociatedChanges(partsPurchaseSettleBill, v => v.PartsPurchaseSettleDetails);
            foreach(PartsPurchaseSettleDetail partsPurchaseSettleDetail in partsPurchaseSettleDetails) {
                switch(ChangeSet.GetChangeOperation(partsPurchaseSettleDetail)) {
                    case ChangeOperation.Insert:
                        InsertToDatabase(partsPurchaseSettleDetail);
                        break;
                    case ChangeOperation.Update:
                        UpdateToDatabase(partsPurchaseSettleDetail);
                        break;
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(partsPurchaseSettleDetail);
                        break;
                }
            }
            var partsPurchaseSettleRefs = ChangeSet.GetAssociatedChanges(partsPurchaseSettleBill, v => v.PartsPurchaseSettleRefs);
            foreach(PartsPurchaseSettleRef partsPurchaseSettleRef in partsPurchaseSettleRefs) {
                switch(ChangeSet.GetChangeOperation(partsPurchaseSettleRef)) {
                    case ChangeOperation.Insert:
                        InsertToDatabase(partsPurchaseSettleRef);
                        break;
                    case ChangeOperation.Update:
                        UpdateToDatabase(partsPurchaseSettleRef);
                        break;
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(partsPurchaseSettleRef);
                        break;
                }
            }
            this.UpdatePartsPurchaseSettleBillValidate(partsPurchaseSettleBill);
        }

        public PartsPurchaseSettleBill GetPartsPurchaseSettleBillsWithDetailsAndRefsById(int id) {
            var dbPartsPurchaseSettleBill = ObjectContext.PartsPurchaseSettleBills.SingleOrDefault(entity => entity.Id == id);
            if(dbPartsPurchaseSettleBill != null) {
                var dbDetails = ObjectContext.PartsPurchaseSettleDetails.Where(e => e.PartsPurchaseSettleBillId == id).ToArray();
                var dbRefs = ObjectContext.PartsPurchaseSettleRefs.Where(e => e.PartsPurchaseSettleBillId == id).ToArray();
                var sourceIds = dbRefs.Where(r => r.SourceType == (int)DcsPartsPurchaseSettleRefSourceType.配件出库单).Select(r => r.SourceId);
                var tempStructArray = (from a in ObjectContext.PartsPurReturnOrders
                                       join b in ObjectContext.PartsOutboundBills on a.Id equals b.OriginalRequirementBillId
                                       where b.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件采购退货单 && sourceIds.Contains(b.Id) && b.OutboundType == (int)DcsPartsOutboundType.采购退货
                                       select new {
                                           outboundBillId = b.Id,
                                           returnReason = a.ReturnReason
                                       }).ToArray();
                foreach(var item in dbRefs.Where(r => r.SourceType == (int)DcsPartsPurchaseSettleRefSourceType.配件出库单)) {
                    var tempStruct = tempStructArray.FirstOrDefault(r => r.outboundBillId == item.SourceId);
                    if(tempStruct != null) {
                        item.ReturnReason = tempStruct.returnReason == (int)DcsPartsPurReturnOrderReturnReason.质量件退货 ? "质量件退货" : "质量件退货";
                    }
                }
            }
            return dbPartsPurchaseSettleBill;
        }

        public PartsPurchaseSettleBill GetPartsPurchaseSettleBillsWithDetailsAndRefsByIdSupply(int id)
        {
            var dbPartsPurchaseSettleBill = ObjectContext.PartsPurchaseSettleBills.SingleOrDefault(entity => entity.Id == id);
            if (dbPartsPurchaseSettleBill != null)
            {
                var dbDetails = ObjectContext.PartsPurchaseSettleDetails.Where(e => e.PartsPurchaseSettleBillId == id).ToArray();
                var dbRefs = ObjectContext.PartsPurchaseSettleRefs.Where(e => e.PartsPurchaseSettleBillId == id).ToArray();
                var sourceIds = dbRefs.Where(r => r.SourceType == (int)DcsPartsPurchaseSettleRefSourceType.配件出库单).Select(r => r.SourceId);
                var sourceInIds = dbRefs.Where(r => r.SourceType == (int)DcsPartsPurchaseSettleRefSourceType.配件入库检验单).Select(r => r.SourceId);
                var tempStructArray = (from a in ObjectContext.PartsPurReturnOrders
                                       join b in ObjectContext.PartsOutboundBills on a.Id equals b.OriginalRequirementBillId
                                       where b.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件采购退货单 && sourceIds.Contains(b.Id) && b.OutboundType == (int)DcsPartsOutboundType.采购退货
                                       select new
                                       {
                                           outboundBillId = b.Id,
                                           returnReason = a.ReturnReason
                                       }).ToArray();
                var tempStructInArray = (from a in ObjectContext.PartsInboundCheckBills
                                         where sourceInIds.Contains(a.Id)
                                         select new
                                         {
                                             InboundCheckBillId = a.Id,
                                             CPPartsPurchaseOrderCode = a.CPPartsPurchaseOrderCode,
                                             CPPartsInboundCheckCode = a.CPPartsInboundCheckCode
                                         }).ToArray();

                foreach (var item in dbRefs.Where(r => r.SourceType == (int)DcsPartsPurchaseSettleRefSourceType.配件出库单))
                {
                    var tempStruct = tempStructArray.FirstOrDefault(r => r.outboundBillId == item.SourceId);
                    if (tempStruct != null)
                    {
                        item.ReturnReason = tempStruct.returnReason == (int)DcsPartsPurReturnOrderReturnReason.质量件退货 ? "质量件退货" : "质量件退货";
                    }
                }
                foreach (var item in dbRefs.Where(r => r.SourceType == (int)DcsPartsPurchaseSettleRefSourceType.配件入库检验单))
                {
                    var tempInStruct = tempStructInArray.FirstOrDefault(r => r.InboundCheckBillId == item.SourceId);
                    if (tempInStruct != null)
                    {
                        item.CPPartsPurchaseOrderCode = tempInStruct.CPPartsPurchaseOrderCode;
                        item.CPPartsInboundCheckCode = tempInStruct.CPPartsInboundCheckCode;
                    }
                }


            }
            return dbPartsPurchaseSettleBill;
        }

        public IQueryable<VirtualPartsPurchaseSettleBill> 查询虚拟配件采购结算成本(int partsPurchaseSettleBillId) {
            var partsPurchaseSettleBills = from partsPurchaseSettleBill in ObjectContext.PartsPurchaseSettleBills.Where(r => r.Id == partsPurchaseSettleBillId)
                                           from partsPurchaseSettleRef in ObjectContext.PartsPurchaseSettleRefs
                                           from partsOutboundBill in ObjectContext.PartsOutboundBills
                                           from partsOutboundBillDetail in ObjectContext.PartsOutboundBillDetails
                                           from sparePart in ObjectContext.SpareParts
                                           where sparePart.Id == partsOutboundBillDetail.SparePartId && partsPurchaseSettleRef.PartsPurchaseSettleBillId == partsPurchaseSettleBill.Id && partsPurchaseSettleRef.SourceId == partsOutboundBill.Id && partsOutboundBillDetail.PartsOutboundBillId == partsOutboundBill.Id
                                           select new VirtualPartsPurchaseSettleBill {
                                               Code = partsPurchaseSettleBill.Code,
                                               BillCode = partsOutboundBill.Code,
                                               BillBusinessType = "出库",
                                               SparePartCode = sparePart.Code,
                                               SparePartName = sparePart.Name,
                                               Quantity = partsOutboundBillDetail.OutboundAmount,
                                               SettlementPrice = partsOutboundBillDetail.SettlementPrice,
                                               SettlementAmount = partsOutboundBillDetail.SettlementPrice * partsOutboundBillDetail.OutboundAmount,
                                               PlanPrice = partsOutboundBillDetail.CostPrice,
                                               PlanAmount = partsOutboundBillDetail.CostPrice * partsOutboundBillDetail.OutboundAmount,
                                               MaterialCostVariance = partsOutboundBillDetail.SettlementPrice * partsOutboundBillDetail.OutboundAmount - partsOutboundBillDetail.CostPrice * partsOutboundBillDetail.OutboundAmount,
                                           };
            var partsPurchaseSettleBillss = from partsPurchaseSettleBill in ObjectContext.PartsPurchaseSettleBills.Where(r => r.Id == partsPurchaseSettleBillId)
                                            from partsPurchaseSettleRef in ObjectContext.PartsPurchaseSettleRefs
                                            from partsInboundCheckBill in ObjectContext.PartsInboundCheckBills
                                            from partsInboundCheckBillDetail in ObjectContext.PartsInboundCheckBillDetails
                                            from sparePart in ObjectContext.SpareParts
                                            where sparePart.Id == partsInboundCheckBillDetail.SparePartId && partsPurchaseSettleRef.PartsPurchaseSettleBillId == partsPurchaseSettleBill.Id && partsPurchaseSettleRef.SourceId == partsInboundCheckBill.Id && partsInboundCheckBillDetail.PartsInboundCheckBillId == partsInboundCheckBill.Id
                                            select new VirtualPartsPurchaseSettleBill {
                                                Code = partsPurchaseSettleBill.Code,
                                                BillCode = partsInboundCheckBill.Code,
                                                BillBusinessType = "入库",
                                                SparePartCode = sparePart.Code,
                                                SparePartName = sparePart.Name,
                                                Quantity = partsInboundCheckBillDetail.InspectedQuantity,
                                                SettlementPrice = partsInboundCheckBillDetail.SettlementPrice,
                                                SettlementAmount = partsInboundCheckBillDetail.SettlementPrice * partsInboundCheckBillDetail.InspectedQuantity,
                                                PlanPrice = partsInboundCheckBillDetail.CostPrice,
                                                PlanAmount = partsInboundCheckBillDetail.CostPrice * partsInboundCheckBillDetail.InspectedQuantity,
                                                MaterialCostVariance = partsInboundCheckBillDetail.SettlementPrice * partsInboundCheckBillDetail.InspectedQuantity - partsInboundCheckBillDetail.CostPrice * partsInboundCheckBillDetail.InspectedQuantity,
                                            };
            return partsPurchaseSettleBills.Union(partsPurchaseSettleBillss).OrderBy(r => r.BillCode);
        }

        public IQueryable<VirtualPartsPurchaseSettleBillWithSumPlannedPrice> GetPartsPurchaseSettleBillWithPartsSalesCategory() {
            var results = from a in ObjectContext.PartsPurchaseSettleBills
                          join b in ObjectContext.PartsPurchaseSettleRefs on a.Id equals b.PartsPurchaseSettleBillId
                          join c in ObjectContext.PartsInboundCheckBills on new {
                              Id = b.SourceId,
                              b.SourceType
                          }equals new {
                              c.Id,
                              SourceType = (int)DcsPartsPurchaseSettleRefSourceType.配件入库检验单
                          }into t1
                          from d in t1.DefaultIfEmpty()
                          join e in ObjectContext.PartsOutboundBills on new {
                              Id = b.SourceId,
                              b.SourceType
                          }equals new {
                              e.Id,
                              SourceType = (int)DcsPartsPurchaseSettleRefSourceType.配件出库单
                          }into t2
                          from f in t2.DefaultIfEmpty()
                          orderby a.Id
                          group new {
                              SumPlannedPrice = (decimal?)d.PartsInboundCheckBillDetails.Sum(r => r.CostPrice * r.InspectedQuantity) ?? 0m - ((decimal?)f.PartsOutboundBillDetails.Sum(r => r.CostPrice * r.OutboundAmount) ?? 0m)
                          }
                          by a into v
                          join bsr in ObjectContext.BranchSupplierRelations.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on new {
                              v.Key.BranchId,
                              PartsSalesCategoryId = v.Key.PartsSalesCategoryId ?? 0,
                              SupplierId = v.Key.PartsSupplierId
                          }equals new {
                              bsr.BranchId,
                              bsr.PartsSalesCategoryId,
                              bsr.SupplierId
                          }into tr
                          from sr in tr.DefaultIfEmpty()
                          select new VirtualPartsPurchaseSettleBillWithSumPlannedPrice {
                              Id = v.Key.Id,
                              Code = v.Key.Code,
                              InvoiceFlag = v.Key.InvoiceFlag,
                              WarehouseId = v.Key.WarehouseId,
                              WarehouseCode = v.Key.WarehouseCode,
                              WarehouseName = v.Key.WarehouseName,
                              BranchId = v.Key.BranchId,
                              BranchCode = v.Key.BranchCode,
                              BranchName = v.Key.BranchName,
                              PartsSalesCategoryId = v.Key.PartsSalesCategoryId,
                              PartsSalesCategoryName = v.Key.PartsSalesCategory != null ? v.Key.PartsSalesCategory.Name : string.Empty,
                              PartsSupplierId = v.Key.PartsSupplierId,
                              PartsSupplierCode = v.Key.PartsSupplierCode,
                              BusinessCode = sr.BusinessCode,
                              PartsSupplierName = v.Key.PartsSupplierName,
                              TotalSettlementAmount = v.Key.TotalSettlementAmount,
                              OffsettedSettlementBillId = v.Key.OffsettedSettlementBillId,
                              OffsettedSettlementBillCode = v.Key.OffsettedSettlementBillCode,
                              TaxRate = v.Key.TaxRate,
                              Tax = v.Key.Tax,
                              InvoiceAmountDifference = v.Key.InvoiceAmountDifference,
                              TotalCostAmount = v.Key.TotalCostAmount,
                              CostAmountDifference = v.Key.CostAmountDifference,
                              Status = v.Key.Status,
                              SettlementPath = v.Key.SettlementPath,
                              Remark = v.Key.Remark,
                              CreatorId = v.Key.CreatorId,
                              CreatorName = v.Key.CreatorName,
                              CreateTime = v.Key.CreateTime,
                              ModifierId = v.Key.ModifierId,
                              ModifierName = v.Key.ModifierName,
                              ModifyTime = v.Key.ModifyTime,
                              AbandonerId = v.Key.AbandonerId,
                              AbandonerName = v.Key.AbandonerName,
                              AbandonTime = v.Key.AbandonTime,
                              ApproverId = v.Key.ApproverId,
                              ApproverName = v.Key.ApproverName,
                              ApproveTime = v.Key.ApproveTime,
                              InvoiceApproverId = v.Key.InvoiceApproverId,
                              InvoiceApproverName = v.Key.InvoiceApproverName,
                              InvoiceApproveTime = v.Key.InvoiceApproveTime,
                              //left join 所以可能为空 不能改为d.PartsInboundCheckBillDetails.Sum(r => r.SettlementPrice * r.InspectedQuantity)-f.PartsOutboundBillDetails.Sum(r => r.SettlementPrice * r.OutboundAmount)
                              SumPlannedPrice = v.Sum(g => g.SumPlannedPrice),
                              InvoiceTotalAmount = v.Key.PurchaseSettleInvoiceRels.Any() ? v.Key.PurchaseSettleInvoiceRels.Sum(ex => ex.InvoiceInformation.InvoiceAmount ?? 0) : 0,
                              IsUnifiedSettle=v.Key.IsUnifiedSettle
                          };
            return results.OrderBy(r => r.Id);
        }

        //采购结算管理-查询（含是否统一结算的过滤）
        public IQueryable<VirtualPartsPurchaseSettleBillWithSumPlannedPrice> GetPartsPurchaseSettleBillWithPartsSalesCategoryByUnifiedSettle()
        {
            var results = from a in ObjectContext.PartsPurchaseSettleBills.Where(r => r.IsUnifiedSettle == null || r.IsUnifiedSettle == false)
                          join b in ObjectContext.PartsPurchaseSettleRefs on a.Id equals b.PartsPurchaseSettleBillId
                          join c in ObjectContext.PartsInboundCheckBills on new
                          {
                              Id = b.SourceId,
                              b.SourceType
                          } equals new
                          {
                              c.Id,
                              SourceType = (int)DcsPartsPurchaseSettleRefSourceType.配件入库检验单
                          } into t1
                          from d in t1.DefaultIfEmpty()
                          join e in ObjectContext.PartsOutboundBills on new
                          {
                              Id = b.SourceId,
                              b.SourceType
                          } equals new
                          {
                              e.Id,
                              SourceType = (int)DcsPartsPurchaseSettleRefSourceType.配件出库单
                          } into t2
                          from f in t2.DefaultIfEmpty()
                          orderby a.Id
                          group new
                          {
                              SumPlannedPrice = (decimal?)d.PartsInboundCheckBillDetails.Sum(r => r.CostPrice * r.InspectedQuantity) ?? 0m - ((decimal?)f.PartsOutboundBillDetails.Sum(r => r.CostPrice * r.OutboundAmount) ?? 0m)
                          }
                          by a into v
                          join bsr in ObjectContext.BranchSupplierRelations.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on new
                          {
                              v.Key.BranchId,
                              PartsSalesCategoryId = v.Key.PartsSalesCategoryId ?? 0,
                              SupplierId = v.Key.PartsSupplierId
                          } equals new
                          {
                              bsr.BranchId,
                              bsr.PartsSalesCategoryId,
                              bsr.SupplierId
                          } into tr
                          from sr in tr.DefaultIfEmpty()
                          select new VirtualPartsPurchaseSettleBillWithSumPlannedPrice
                          {
                              Id = v.Key.Id,
                              Code = v.Key.Code,
                              InvoiceFlag = v.Key.InvoiceFlag,
                              WarehouseId = v.Key.WarehouseId,
                              WarehouseCode = v.Key.WarehouseCode,
                              WarehouseName = v.Key.WarehouseName,
                              BranchId = v.Key.BranchId,
                              BranchCode = v.Key.BranchCode,
                              BranchName = v.Key.BranchName,
                              PartsSalesCategoryId = v.Key.PartsSalesCategoryId,
                              PartsSalesCategoryName = v.Key.PartsSalesCategory != null ? v.Key.PartsSalesCategory.Name : string.Empty,
                              PartsSupplierId = v.Key.PartsSupplierId,
                              PartsSupplierCode = v.Key.PartsSupplierCode,
                              BusinessCode = sr.BusinessCode,
                              PartsSupplierName = v.Key.PartsSupplierName,
                              TotalSettlementAmount = v.Key.TotalSettlementAmount,
                              OffsettedSettlementBillId = v.Key.OffsettedSettlementBillId,
                              OffsettedSettlementBillCode = v.Key.OffsettedSettlementBillCode,
                              TaxRate = v.Key.TaxRate,
                              Tax = v.Key.Tax,
                              InvoiceAmountDifference = v.Key.InvoiceAmountDifference,
                              TotalCostAmount = v.Key.TotalCostAmount,
                              CostAmountDifference = v.Key.CostAmountDifference,
                              Status = v.Key.Status,
                              SettlementPath = v.Key.SettlementPath,
                              Remark = v.Key.Remark,
                              CreatorId = v.Key.CreatorId,
                              CreatorName = v.Key.CreatorName,
                              CreateTime = v.Key.CreateTime,
                              ModifierId = v.Key.ModifierId,
                              ModifierName = v.Key.ModifierName,
                              ModifyTime = v.Key.ModifyTime,
                              AbandonerId = v.Key.AbandonerId,
                              AbandonerName = v.Key.AbandonerName,
                              AbandonTime = v.Key.AbandonTime,
                              ApproverId = v.Key.ApproverId,
                              ApproverName = v.Key.ApproverName,
                              ApproveTime = v.Key.ApproveTime,
                              InvoiceApproverId = v.Key.InvoiceApproverId,
                              InvoiceApproverName = v.Key.InvoiceApproverName,
                              InvoiceApproveTime = v.Key.InvoiceApproveTime,
                              //left join 所以可能为空 不能改为d.PartsInboundCheckBillDetails.Sum(r => r.SettlementPrice * r.InspectedQuantity)-f.PartsOutboundBillDetails.Sum(r => r.SettlementPrice * r.OutboundAmount)
                              SumPlannedPrice = v.Sum(g => g.SumPlannedPrice),
                              InvoiceDate = v.Key.InvoiceDate,
                              InvoiceTotalAmount = v.Key.PurchaseSettleInvoiceRels.Any() ? v.Key.PurchaseSettleInvoiceRels.Sum(ex => ex.InvoiceInformation.InvoiceAmount ?? 0) : 0
                          };
            return results.OrderBy(r => r.Id);
        }

        [Query(HasSideEffects = true)]
        public IQueryable<PartsPurchaseSettleBill> GetPartsPurchaseSettleBillByIds(int[] ids) {
            return ObjectContext.PartsPurchaseSettleBills.Where(r => ids.Contains(r.Id));
        }

        public IQueryable<VirtualPartsPurchaseSettleDetail> GetDetailsWithSupplierPartCode() {
            var result = from a in ObjectContext.PartsPurchaseSettleDetails
                         join b in ObjectContext.PartsPurchaseSettleBills on a.PartsPurchaseSettleBillId equals b.Id
                         join c in ObjectContext.PartsSupplierRelations on new {
                             a.SparePartId,
                             b.PartsSupplierId
                         } equals new {
                             SparePartId = c.PartId,
                             PartsSupplierId = c.SupplierId
                         } into t
                         from t1 in t.DefaultIfEmpty()
                         select new VirtualPartsPurchaseSettleDetail {
                             Id = a.Id,
                             SerialNumber = a.SerialNumber,
                             PartsPurchaseSettleBillId = a.PartsPurchaseSettleBillId,
                             SparePartId = a.SparePartId,
                             SparePartCode = a.SparePartCode,
                             SparePartName = a.SparePartName,
                             SettlementPrice = a.SettlementPrice,
                             QuantityToSettle = a.QuantityToSettle,
                             SettlementAmount = a.SettlementAmount,
                             Remark = a.Remark,
                             SupplierPartCode = t1.SupplierPartCode
                         };

            return result.OrderBy(r => r.Id);
        }
    
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               

        public void InsertPartsPurchaseSettleBill(PartsPurchaseSettleBill partsPurchaseSettleBill) {
            new PartsPurchaseSettleBillAch(this).InsertPartsPurchaseSettleBill(partsPurchaseSettleBill);
        }

        public void UpdatePartsPurchaseSettleBill(PartsPurchaseSettleBill partsPurchaseSettleBill) {
            new PartsPurchaseSettleBillAch(this).UpdatePartsPurchaseSettleBill(partsPurchaseSettleBill);
        }

        public PartsPurchaseSettleBill GetPartsPurchaseSettleBillsWithDetailsAndRefsById(int id) {
            return new PartsPurchaseSettleBillAch(this).GetPartsPurchaseSettleBillsWithDetailsAndRefsById(id);
        }

        public PartsPurchaseSettleBill GetPartsPurchaseSettleBillsWithDetailsAndRefsByIdSupply(int id)
        {
            return new PartsPurchaseSettleBillAch(this).GetPartsPurchaseSettleBillsWithDetailsAndRefsByIdSupply(id);
        }

        public IQueryable<VirtualPartsPurchaseSettleBill> 查询虚拟配件采购结算成本(int partsPurchaseSettleBillId) {
            return new PartsPurchaseSettleBillAch(this).查询虚拟配件采购结算成本(partsPurchaseSettleBillId);
        }

        public IQueryable<VirtualPartsPurchaseSettleBillWithSumPlannedPrice> GetPartsPurchaseSettleBillWithPartsSalesCategory() {
            return new PartsPurchaseSettleBillAch(this).GetPartsPurchaseSettleBillWithPartsSalesCategory();
        }

        public IQueryable<VirtualPartsPurchaseSettleBillWithSumPlannedPrice> GetPartsPurchaseSettleBillWithPartsSalesCategoryByUnifiedSettle()
        {
            return new PartsPurchaseSettleBillAch(this).GetPartsPurchaseSettleBillWithPartsSalesCategoryByUnifiedSettle();
        }


        [Query(HasSideEffects = true)]
        public IQueryable<PartsPurchaseSettleBill> GetPartsPurchaseSettleBillByIds(int[] ids) {
            return new PartsPurchaseSettleBillAch(this).GetPartsPurchaseSettleBillByIds(ids);
        }

        public IQueryable<VirtualPartsPurchaseSettleDetail> GetDetailsWithSupplierPartCode() {
            return new PartsPurchaseSettleBillAch(this).GetDetailsWithSupplierPartCode();
        }
    }
}
