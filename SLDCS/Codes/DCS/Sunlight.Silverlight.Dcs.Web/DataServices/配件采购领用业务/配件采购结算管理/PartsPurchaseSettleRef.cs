﻿using System.Linq;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsPurchaseSettleRefAch : DcsSerivceAchieveBase {
        public PartsPurchaseSettleRefAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public IQueryable<VirtualPartsPurchaseSettleRef> GetPartsPurchaseSettleRefWithOriginalRequirementBillCode() {
            //return ObjectContext.PartsPurchaseSettleRefs.OrderBy(r => r.Id).Include("PartsInboundCheckBill").Include("PartsOutboundBill");
            var VirtualPartsPurchaseSettleRefInQuery = from r in ObjectContext.PartsPurchaseSettleRefs.Where(r => r.SourceType == (int)DcsPartsPurchaseSettleRefSourceType.配件入库检验单)
                                                       join i in ObjectContext.PartsInboundCheckBills on r.SourceId equals i.Id
                                                       join p in ObjectContext.PartsInboundPlans on i.PartsInboundPlanId equals p.Id
                                                       join s in ObjectContext.SupplierShippingOrders on p.SourceId equals s.Id
                                                       join c in ObjectContext.PartsPurchaseOrders on s.PartsPurchaseOrderId equals c.Id
                                                       select new {
                                                           PartsPurchaseSettleBillId = r.PartsPurchaseSettleBillId,
                                                           SerialNumber = r.SerialNumber,
                                                           SourceCode = i.Code,
                                                           OriginalRequirementBillCode = c.Code,
                                                           SourceType = r.SourceType,
                                                           WarehouseName = i.WarehouseName,
                                                           SourceBillCreateTime = i.CreateTime
                                                       };
            var VirtualPartsPurchaseSettleRefOutQuery = from r in ObjectContext.PartsPurchaseSettleRefs.Where(r => r.SourceType == (int)DcsPartsPurchaseSettleRefSourceType.配件出库单)
                                                        join o in ObjectContext.PartsOutboundBills on r.SourceId equals o.Id
                                                        join p in ObjectContext.PartsOutboundPlans on o.PartsOutboundPlanId equals p.Id
                                                        join s in ObjectContext.PartsPurReturnOrders on p.SourceId equals s.Id
                                                        select new {
                                                            PartsPurchaseSettleBillId = r.PartsPurchaseSettleBillId,
                                                            SerialNumber = r.SerialNumber,
                                                            SourceCode = o.Code,
                                                            OriginalRequirementBillCode = s.Code,
                                                            SourceType = r.SourceType,
                                                            WarehouseName = o.WarehouseName,
                                                            SourceBillCreateTime = o.CreateTime
                                                        };
            var VirtualPartsPurchaseSettleRefQuery = VirtualPartsPurchaseSettleRefInQuery.Union(VirtualPartsPurchaseSettleRefOutQuery);
            var vppsrq = from v in VirtualPartsPurchaseSettleRefQuery
                         select new VirtualPartsPurchaseSettleRef {
                             PartsPurchaseSettleBillId = v.PartsPurchaseSettleBillId,
                             SerialNumber = v.SerialNumber,
                             SourceCode = v.SourceCode,
                             OriginalRequirementBillCode = v.OriginalRequirementBillCode,
                             SourceType = v.SourceType,
                             WarehouseName = v.WarehouseName,
                             SourceBillCreateTime = v.SourceBillCreateTime
                         };
            return vppsrq.OrderBy(v => v.SerialNumber);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               

        public IQueryable<VirtualPartsPurchaseSettleRef> GetPartsPurchaseSettleRefWithOriginalRequirementBillCode() {
            return new PartsPurchaseSettleRefAch(this).GetPartsPurchaseSettleRefWithOriginalRequirementBillCode();
        }
    }
}
