﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class ABCTypeSafeDayAch : DcsSerivceAchieveBase {
        public ABCTypeSafeDayAch(DcsDomainService domainService)
            : base(domainService) {
        }
        internal void InsertABCTypeSafeDayValidate(ABCTypeSafeDay aBCTypeSafeDay) {
            if(ObjectContext.ABCTypeSafeDays.Any(r => r.PRODUCTTYPE == aBCTypeSafeDay.PRODUCTTYPE )) {
                throw new ValidationException("已存在相同数据");
            }
            var userInfo = Utils.GetCurrentUserInfo();
            aBCTypeSafeDay.CreatorId = userInfo.Id;
            aBCTypeSafeDay.CreatorName = userInfo.Name;
            aBCTypeSafeDay.CreateTime = DateTime.Now;
        }
        internal void UpdateABCTypeSafeDayValidate(ABCTypeSafeDay aBCTypeSafeDay) {
            if(ObjectContext.ABCTypeSafeDays.Any(r => r.PRODUCTTYPE == aBCTypeSafeDay.PRODUCTTYPE  && r.Id != aBCTypeSafeDay.Id)) {
                throw new ValidationException("已存在相同数据");
            }           
            var userInfo = Utils.GetCurrentUserInfo();
            aBCTypeSafeDay.ModifierId = userInfo.Id;
            aBCTypeSafeDay.ModifierName = userInfo.Name;
            aBCTypeSafeDay.ModifyTime = DateTime.Now;
        }
        public void InsertABCTypeSafeDay(ABCTypeSafeDay aBCTypeSafeDay) {
            InsertToDatabase(aBCTypeSafeDay);
            this.InsertABCTypeSafeDayValidate(aBCTypeSafeDay);
        }
        public void UpdateABCTypeSafeDay(ABCTypeSafeDay aBCTypeSafeDay) {
            UpdateToDatabase(aBCTypeSafeDay);
            this.UpdateABCTypeSafeDayValidate(aBCTypeSafeDay);
        }

    }
    partial class DcsDomainService {
        public void InsertABCTypeSafeDay(ABCTypeSafeDay aBCTypeSafeDay) {
            new ABCTypeSafeDayAch(this).InsertABCTypeSafeDay(aBCTypeSafeDay);
        }
        public void UpdateABCTypeSafeDay(ABCTypeSafeDay aBCTypeSafeDay) {
            new ABCTypeSafeDayAch(this).UpdateABCTypeSafeDay(aBCTypeSafeDay);
        }
    }
}
