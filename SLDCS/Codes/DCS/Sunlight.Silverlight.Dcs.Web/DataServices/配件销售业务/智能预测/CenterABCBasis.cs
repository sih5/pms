﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class CenterABCBasisAch : DcsSerivceAchieveBase {
        public CenterABCBasisAch(DcsDomainService domainService)
            : base(domainService) {
        }
        public IQueryable<CenterABCBasi> QueryCenterABCBasis() {
            var userInfo = Utils.GetCurrentUserInfo();
            if(userInfo.EnterpriseId==0) {
                throw new ValidationException("登录已失效，请重新登录");
            }
            var company = ObjectContext.Companies.Where(s=>s.Id==userInfo.EnterpriseId).First();
            if(company.Type == (int)DcsCompanyType.代理库) {
                return ObjectContext.CenterABCBasis.Where(r => r.CenterId == userInfo.EnterpriseId).OrderBy(r => r.SparePartCode);
            }
            return ObjectContext.CenterABCBasis.OrderBy(r=>r.SparePartCode);
        }
    }
    partial class DcsDomainService {
        public IQueryable<CenterABCBasi> QueryCenterABCBasis() {
            return new CenterABCBasisAch(this).QueryCenterABCBasis();
        }
    }
}
