﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    class CenterAccumulateDailyExpAch : DcsSerivceAchieveBase {
        public CenterAccumulateDailyExpAch(DcsDomainService domainService)
            : base(domainService) {
        }
        public IQueryable<CenterAccumulateDailyExp> CenterAccumulateDailyExpByCenter() {
            var userInfo = Utils.GetCurrentUserInfo();
            if(userInfo.EnterpriseId==0) {
                throw new ValidationException("登录已失效，请重新登录");
            }
            var company = ObjectContext.Companies.Where(s=>s.Id==userInfo.EnterpriseId).First();
            if(company.Type == (int)DcsCompanyType.代理库) {
                return ObjectContext.CenterAccumulateDailyExps.Where(r => r.CenterId == userInfo.EnterpriseId).OrderBy(r => r.SparePartCode);
            }
            return ObjectContext.CenterAccumulateDailyExps.OrderBy(r => r.SparePartCode);
        }
    }
    partial class DcsDomainService {
        public IQueryable<CenterAccumulateDailyExp> CenterAccumulateDailyExpByCenter() {
            return new CenterAccumulateDailyExpAch(this).CenterAccumulateDailyExpByCenter();
        }
    }
}