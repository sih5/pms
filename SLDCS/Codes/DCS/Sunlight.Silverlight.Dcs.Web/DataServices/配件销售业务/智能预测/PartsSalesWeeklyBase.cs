﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsSalesWeeklyBaseAch : DcsSerivceAchieveBase {
        public PartsSalesWeeklyBaseAch(DcsDomainService domainService)
            : base(domainService) {
        }
        public IQueryable<PartsSalesWeeklyBase> QueryPartsSalesWeeklyBasesForCenter() {
            var userInfo = Utils.GetCurrentUserInfo();
            if(userInfo.EnterpriseId == 0) {
                throw new ValidationException("登录已失效，请重新登录");
            }
            var company = ObjectContext.Companies.Where(s => s.Id == userInfo.EnterpriseId).First();
            if(company.Type == (int)DcsCompanyType.代理库) {
                return ObjectContext.PartsSalesWeeklyBases.Where(r => ObjectContext.Warehouses.Any(t=>t.StorageCompanyId==company.Id && r.WarehouseId==t.Id)).OrderBy(r => r.SparePartCode);
            }
            return ObjectContext.PartsSalesWeeklyBases.OrderBy(r => r.SparePartCode);
        }
    }
    partial class DcsDomainService {
        public IQueryable<PartsSalesWeeklyBase> QueryPartsSalesWeeklyBasesForCenter() {
            return new PartsSalesWeeklyBaseAch(this).QueryPartsSalesWeeklyBasesForCenter();
        }
    }
}
