﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web{
    partial class PartsStockCoefficientAch : DcsSerivceAchieveBase {
        public PartsStockCoefficientAch(DcsDomainService domainService)
            : base(domainService) {
        }
        internal void InsertPartsStockCoefficientValidate(PartsStockCoefficient partsStockCoefficient) {
            if(ObjectContext.PartsStockCoefficients.Any(r=>r.PRODUCTTYPE==partsStockCoefficient.PRODUCTTYPE&& r.Type==partsStockCoefficient.Type)) {
                throw new ValidationException("已存在相同数据");
            }            
            var userInfo = Utils.GetCurrentUserInfo();
            partsStockCoefficient.CreatorId = userInfo.Id;
            partsStockCoefficient.CreatorName = userInfo.Name;
            partsStockCoefficient.CreateTime = DateTime.Now;
        }
        internal void UpdatePartsStockCoefficientValidate(PartsStockCoefficient partsStockCoefficient) {
            if(ObjectContext.PartsStockCoefficients.Any(r => r.PRODUCTTYPE == partsStockCoefficient.PRODUCTTYPE && r.Type == partsStockCoefficient.Type&& r.Id!=partsStockCoefficient.Id)) {
                throw new ValidationException("已存在相同数据");
            }
            var oldCoeff = ObjectContext.PartsStockCoefficients.Where(r => r.Id == partsStockCoefficient.Id).First();
            if(oldCoeff.Status != (int)DcsBaseDataStatus.有效) {
                throw new ValidationException("数据已被作废");
            }
            var userInfo = Utils.GetCurrentUserInfo();
            partsStockCoefficient.ModifierId = userInfo.Id;
            partsStockCoefficient.ModifierName = userInfo.Name;
            partsStockCoefficient.ModifyTime = DateTime.Now;
        }
        public void InsertPartsStockCoefficient(PartsStockCoefficient partsStockCoefficient) {
            InsertToDatabase(partsStockCoefficient);
            this.InsertPartsStockCoefficientValidate(partsStockCoefficient);
        }
        public void UpdatePartsStockCoefficient(PartsStockCoefficient partsStockCoefficient) {
            partsStockCoefficient.PartsStockCoefficientPrices.Clear();           
            UpdateToDatabase(partsStockCoefficient);
            var details = ChangeSet.GetAssociatedChanges(partsStockCoefficient, v => v.PartsStockCoefficientPrices);
            foreach(PartsStockCoefficientPrice detail in details) {
                switch(ChangeSet.GetChangeOperation(detail)) {
                    case ChangeOperation.Insert:
                        InsertToDatabase(detail);
                        break;
                    case ChangeOperation.Update:
                        UpdateToDatabase(detail);
                        break;
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(detail);
                        break;
                }
            }          
            this.UpdatePartsStockCoefficientValidate(partsStockCoefficient);
        }
        public IQueryable<PartsStockCoefficient> GetPartsStockCoefficientForDetails() {
            return this.ObjectContext.PartsStockCoefficients.Include("PartsStockCoefficientPrices").OrderBy(r => r.Id);
        }
        public void InsertPartsStockCoefficientPrice(PartsStockCoefficientPrice partsStockCoefficientPrice) {

        }
        public void UpdatePartsStockCoefficientPrice(PartsStockCoefficientPrice partsStockCoefficientPrice) {

        }
        public void 作废配件库存系数(PartsStockCoefficient partsStockCoefficient) {
            this.UpdatePartsStockCoefficientValidate(partsStockCoefficient);
            partsStockCoefficient.Status = (int)DcsBaseDataStatus.作废;
            UpdateToDatabase(partsStockCoefficient);
           
        }
        public IEnumerable<ExportPartsStockCoefficient> ExportForPartsStockCoefficient(int? type, int? productType, int? status) {
            var SQL = @"select p.type,
                       p.producttype,
                       p.status,
                       p.creatorid,
                       p.creatorname,
                       p.createtime,
                       p.modifierid,
                       p.modifiername,
                       p.modifytime,
                       s.minprice,
                       s.maxprice,
                       s.coefficient
                  from PartsStockCoefficient p
                  join PartsStockCoefficientPrice s
                    on p.id = s.partsstockcoefficientid
                 where 1 = 1 ";
            if(type.HasValue){
                SQL = SQL + " and p.type = "+type;
            }
            if(productType.HasValue){
                SQL = SQL + " and p.productType = " + productType;
            }
            if(status.HasValue){
                SQL = SQL + " and p.status="+status;
            }
            var search = ObjectContext.ExecuteStoreQuery<ExportPartsStockCoefficient>(SQL).ToArray();
            return search;
        }
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               

        public void InsertPartsStockCoefficient(PartsStockCoefficient partsStockCoefficient) {
            new PartsStockCoefficientAch(this).InsertPartsStockCoefficient(partsStockCoefficient);
        }

        public void UpdatePartsStockCoefficient(PartsStockCoefficient partsStockCoefficient) {
            new PartsStockCoefficientAch(this).UpdatePartsStockCoefficient(partsStockCoefficient);
        }
        public IQueryable<PartsStockCoefficient> GetPartsStockCoefficientForDetails() {
           return  new PartsStockCoefficientAch(this).GetPartsStockCoefficientForDetails();
        }

        public void InsertPartsStockCoefficientPrice(PartsStockCoefficientPrice partsStockCoefficientPrice) {
            new PartsStockCoefficientAch(this).InsertPartsStockCoefficientPrice(partsStockCoefficientPrice);
        }
        public void UpdatePartsStockCoefficientPrice(PartsStockCoefficientPrice partsStockCoefficientPrice) {
            new PartsStockCoefficientAch(this).UpdatePartsStockCoefficientPrice(partsStockCoefficientPrice);
        }
        public void 作废配件库存系数(PartsStockCoefficient partsStockCoefficient) {
            new PartsStockCoefficientAch(this).作废配件库存系数(partsStockCoefficient);
        }
        public IEnumerable<ExportPartsStockCoefficient> ExportForPartsStockCoefficient(int? type, int? productType, int? status) {
            return new PartsStockCoefficientAch(this).ExportForPartsStockCoefficient(type, productType, status);

        }
    }
}
