﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class AgencyDealerRelationAch : DcsSerivceAchieveBase {
        internal void InsertAgencyDealerRelationValidate(AgencyDealerRelation agencyDealerRelation) {
            var dbAgencyDealerRelation = ObjectContext.AgencyDealerRelations
                .Where(r => r.BranchId == agencyDealerRelation.BranchId && r.DealerId == agencyDealerRelation.DealerId && r.AgencyId == agencyDealerRelation.AgencyId && r.PartsSalesOrderTypeId == agencyDealerRelation.PartsSalesOrderTypeId && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbAgencyDealerRelation != null) {
                throw new ValidationException(ErrorStrings.AgencyDealerRelation_Validation1);
            }
            var userInfo = Utils.GetCurrentUserInfo();
            agencyDealerRelation.CreatorId = userInfo.Id;
            agencyDealerRelation.CreatorName = userInfo.Name;
            agencyDealerRelation.CreateTime = DateTime.Now;
        }
        internal void UpdateAgencyDealerRelationValidate(AgencyDealerRelation agencyDealerRelation) {
            var dbAgencyDealerRelation = ObjectContext.AgencyDealerRelations.Where(r => r.Id == agencyDealerRelation.Id && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbAgencyDealerRelation == null) {
                throw new ValidationException(ErrorStrings.AgencyDealerRelation_Validation2);
            }
            var checkAgencyDealerRelation = ObjectContext.AgencyDealerRelations
                             .Where(r => r.BranchId == agencyDealerRelation.BranchId && r.DealerId == agencyDealerRelation.DealerId && r.AgencyId == agencyDealerRelation.AgencyId && r.PartsSalesOrderTypeId == agencyDealerRelation.PartsSalesOrderTypeId && r.Status == (int)DcsBaseDataStatus.有效 && r.Id != agencyDealerRelation.Id).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(checkAgencyDealerRelation != null) {
                throw new ValidationException(ErrorStrings.AgencyDealerRelation_Validation3);
            }
            var userInfo = Utils.GetCurrentUserInfo();
            agencyDealerRelation.ModifierId = userInfo.Id;
            agencyDealerRelation.ModifierName = userInfo.Name;
            agencyDealerRelation.ModifyTime = DateTime.Now;
        }
        public void InsertAgencyDealerRelation(AgencyDealerRelation agencyDealerRelation) {
            InsertToDatabase(agencyDealerRelation);
            this.InsertAgencyDealerRelationValidate(agencyDealerRelation);
            var agencyDealerRelationHistory = new AgencyDealerRelationHistory();
            agencyDealerRelationHistory.BranchId = agencyDealerRelation.BranchId;
            agencyDealerRelationHistory.PartsSalesOrderTypeId = agencyDealerRelation.PartsSalesOrderTypeId;
            agencyDealerRelationHistory.AgencyId = agencyDealerRelation.AgencyId;
            agencyDealerRelationHistory.AgencyCode = agencyDealerRelation.AgencyCode;
            agencyDealerRelationHistory.AgencyName = agencyDealerRelation.AgencyName;
            agencyDealerRelationHistory.DealerId = agencyDealerRelation.DealerId;
            agencyDealerRelationHistory.DealerCode = agencyDealerRelation.DealerCode;
            agencyDealerRelationHistory.DealerName = agencyDealerRelation.DealerName;
            agencyDealerRelationHistory.Status = agencyDealerRelation.Status;
            agencyDealerRelationHistory.Remark = agencyDealerRelation.Remark;
            agencyDealerRelationHistory.CreatorId = agencyDealerRelation.CreatorId;
            agencyDealerRelationHistory.CreatorName = agencyDealerRelation.CreatorName;
            agencyDealerRelationHistory.CreateTime = agencyDealerRelation.CreateTime;
            agencyDealerRelationHistory.AgencyDealerRelation = agencyDealerRelation;
            InsertToDatabase(agencyDealerRelationHistory);
        }
        public void UpdateAgencyDealerRelation(AgencyDealerRelation agencyDealerRelation) {
            UpdateToDatabase(agencyDealerRelation);
            this.UpdateAgencyDealerRelationValidate(agencyDealerRelation);
            var agencyDealerRelationHistory = new AgencyDealerRelationHistory();
            agencyDealerRelationHistory.BranchId = agencyDealerRelation.BranchId;
            agencyDealerRelationHistory.PartsSalesOrderTypeId = agencyDealerRelation.PartsSalesOrderTypeId;
            agencyDealerRelationHistory.AgencyId = agencyDealerRelation.AgencyId;
            agencyDealerRelationHistory.AgencyCode = agencyDealerRelation.AgencyCode;
            agencyDealerRelationHistory.AgencyName = agencyDealerRelation.AgencyName;
            agencyDealerRelationHistory.DealerId = agencyDealerRelation.DealerId;
            agencyDealerRelationHistory.DealerCode = agencyDealerRelation.DealerCode;
            agencyDealerRelationHistory.DealerName = agencyDealerRelation.DealerName;
            agencyDealerRelationHistory.Status = agencyDealerRelation.Status;
            agencyDealerRelationHistory.Remark = agencyDealerRelation.Remark;
            agencyDealerRelationHistory.CreatorId = agencyDealerRelation.CreatorId;
            agencyDealerRelationHistory.CreatorName = agencyDealerRelation.CreatorName;
            agencyDealerRelationHistory.CreateTime = agencyDealerRelation.CreateTime;
            agencyDealerRelationHistory.AgencyDealerRelation = agencyDealerRelation;
            InsertToDatabase(agencyDealerRelationHistory);
        }
        public IQueryable<AgencyDealerRelation> GetAgencyDealerRelationsWithBranch(string salesRegionName, string marketingDepartmentName) {
            var result = from a in ObjectContext.AgencyDealerRelations
                         select a;
            if(!String.IsNullOrWhiteSpace(salesRegionName)) {
                result = from a in result
                         join b in ObjectContext.DealerMarketDptRelations on a.DealerId equals b.DealerId into t1
                         from c in t1.DefaultIfEmpty()
                         join d in ObjectContext.RegionMarketDptRelations on c.MarketId equals d.MarketDepartmentId into t2
                         from e in t2.DefaultIfEmpty()
                         join f in ObjectContext.SalesRegions on e.SalesRegionId equals f.Id into t3
                         from g in t3.DefaultIfEmpty()
                         where g.RegionName.Contains(salesRegionName)
                         select a;
            }
            if(!String.IsNullOrWhiteSpace(marketingDepartmentName)) {
                result = from a in result
                         join b in ObjectContext.DealerMarketDptRelations on a.DealerId equals b.DealerId into t1
                         from c in t1.DefaultIfEmpty()
                         join d in ObjectContext.RegionMarketDptRelations on c.MarketId equals d.MarketDepartmentId into t2
                         from e in t2.DefaultIfEmpty()
                         join f in ObjectContext.MarketingDepartments on e.MarketDepartmentId equals f.Id into t3
                         from g in t3.DefaultIfEmpty()
                         where g.Name.Contains(marketingDepartmentName)
                         select a;
            }
            return result.Include("Dealer").Include("Agency").Include("Branch").Include("PartsSalesCategory").Include("Company").Include("Company1").OrderBy(r => r.Id);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               

        public void InsertAgencyDealerRelation(AgencyDealerRelation agencyDealerRelation) {
            new AgencyDealerRelationAch(this).InsertAgencyDealerRelation(agencyDealerRelation);
        }

        public void UpdateAgencyDealerRelation(AgencyDealerRelation agencyDealerRelation) {
            new AgencyDealerRelationAch(this).UpdateAgencyDealerRelation(agencyDealerRelation);
        }

        public IQueryable<AgencyDealerRelation> GetAgencyDealerRelationsWithBranch(string salesRegionName, string marketingDepartmentName) {
            return new AgencyDealerRelationAch(this).GetAgencyDealerRelationsWithBranch(salesRegionName,marketingDepartmentName);
        }
    }
}
