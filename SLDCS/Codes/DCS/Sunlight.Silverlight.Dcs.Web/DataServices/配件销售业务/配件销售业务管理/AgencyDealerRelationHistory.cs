﻿
using System.Linq;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class AgencyDealerRelationHistoryAch : DcsSerivceAchieveBase {
        public AgencyDealerRelationHistoryAch(DcsDomainService domainService)
            : base(domainService) {
        }

        internal void InsertAgencyDealerRelationHistoryValidation(AgencyDealerRelationHistory agencyDealerRelationHistory) {
        }
        public void InsertAgencyDealerRelationHistory(AgencyDealerRelationHistory agencyDealerRelationHistory) {
            InsertToDatabase(agencyDealerRelationHistory);
        }
        internal void UpdateAgencyDealerRelationHistoryValidation(AgencyDealerRelationHistory agencyDealerRelationHistory) {
        }
        public void UpdateAgencyDealerRelationHistory(AgencyDealerRelationHistory agencyDealerRelationHistory) {
            UpdateToDatabase(agencyDealerRelationHistory);
        }
        public IQueryable<AgencyDealerRelationHistoryEx> GetHistoriesByAgencyDealerRelationId(int agencyDealerRelationId) {
            IQueryable<AgencyDealerRelationHistoryEx> result = from a in ObjectContext.AgencyDealerRelationHistories
                                                             join b in ObjectContext.PartsSalesCategories on a.PartsSalesOrderTypeId equals b.Id
                                                             join c in ObjectContext.Branches on a.BranchId equals c.Id
                                                             where a.AgencyDealerRelationId == agencyDealerRelationId
                                                             select new AgencyDealerRelationHistoryEx {
                                                                 Id = a.Id,
                                                                 BranchId = a.BranchId,
                                                                 PartsSalesCategoryId = a.PartsSalesOrderTypeId,
                                                                 AgencyId = a.AgencyId,
                                                                 AgencyCode = a.AgencyCode,
                                                                 AgencyName = a.AgencyName,
                                                                 DealerId = a.DealerId,
                                                                 DealerCode = a.DealerCode,
                                                                 DealerName = a.DealerName,
                                                                 Status = a.Status,
                                                                 Remark = a.Remark,
                                                                 CreatorId = a.CreatorId,
                                                                 CreatorName = a.CreatorName,
                                                                 CreateTime = a.CreateTime,
                                                                 AgencyDealerRelationId = a.AgencyDealerRelationId,
                                                                 BranchCode = c.Code,
                                                                 BranchName = c.Name,
                                                                 PartsSalesCategoryCode = b.Code,
                                                                 PartsSalesCategoryName = b.Name
                                                             };
            return result;
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               

        public void InsertAgencyDealerRelationHistory(AgencyDealerRelationHistory agencyDealerRelationHistory) {
            new AgencyDealerRelationHistoryAch(this).InsertAgencyDealerRelationHistory(agencyDealerRelationHistory);
        }

        public void UpdateAgencyDealerRelationHistory(AgencyDealerRelationHistory agencyDealerRelationHistory) {
            new AgencyDealerRelationHistoryAch(this).UpdateAgencyDealerRelationHistory(agencyDealerRelationHistory);
        }

        public IQueryable<AgencyDealerRelationHistoryEx> GetHistoriesByAgencyDealerRelationId(int agencyDealerRelationId) {
            return new AgencyDealerRelationHistoryAch(this).GetHistoriesByAgencyDealerRelationId(agencyDealerRelationId);
        }
    }
}
