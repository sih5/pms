﻿using System;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class AgencyRetailerOrderAch : DcsSerivceAchieveBase {
        public AgencyRetailerOrderAch(DcsDomainService domainService)
            : base(domainService) {
        }
        internal void InsertAgencyRetailerOrderValidate(AgencyRetailerOrder agencyRetailerOrder) {
            var userInfo = Utils.GetCurrentUserInfo();
            agencyRetailerOrder.CreatorId = userInfo.Id;
            agencyRetailerOrder.CreatorName = userInfo.Name;
            agencyRetailerOrder.CreateTime = DateTime.Now;
        }

        public void InsertAgencyRetailerOrder(AgencyRetailerOrder agencyRetailerOrder) {
            InsertToDatabase(agencyRetailerOrder);
            var agencyRetailerLists = ChangeSet.GetAssociatedChanges(agencyRetailerOrder, r => r.AgencyRetailerLists, ChangeOperation.Insert);
            foreach(AgencyRetailerList theAgencyRetailerList in agencyRetailerLists) {
                InsertToDatabase(theAgencyRetailerList);
            }
            this.InsertAgencyRetailerOrderValidate(agencyRetailerOrder);

        }

        public void UpdateAgencyRetailerOrder(AgencyRetailerOrder agencyRetailerOrder) {
            //删除与强绑定清单的关系
            agencyRetailerOrder.AgencyRetailerLists.Clear();
            UpdateToDatabase(agencyRetailerOrder);
            var agencyRetailerLists = ChangeSet.GetAssociatedChanges(agencyRetailerOrder, r => r.AgencyRetailerLists);
            foreach(AgencyRetailerList theAgencyRetailerList in agencyRetailerLists) {
                //判断更新的操作
                switch(ChangeSet.GetChangeOperation(theAgencyRetailerList)) {
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(theAgencyRetailerList);
                        break;
                    case ChangeOperation.Insert:
                        InsertToDatabase(theAgencyRetailerList);
                        break;
                    case ChangeOperation.Update:
                        UpdateToDatabase(theAgencyRetailerList);
                        break;
                } 
            }
            //this.UpdateAgencyRetailerOrderValidate(agencyRetailerOrder);
        }

        public IQueryable<AgencyRetailerOrder> GetAgencyRetailerOrderWithDetails() {
            return this.ObjectContext.AgencyRetailerOrders.Include("AgencyRetailerLists").OrderBy(r => r.Id);
        }

        public IQueryable<AgencyRetailerOrder> GetAgencyRetailerOrdersWithVehiclePartsHandleOrder() {
            return this.ObjectContext.AgencyRetailerOrders.Include("VehiclePartsHandleOrder").OrderBy(r => r.Id);
        }
    }

    partial class DcsDomainService {

        public void InsertAgencyRetailerOrder(AgencyRetailerOrder agencyRetailerOrder) {
            new AgencyRetailerOrderAch(this).InsertAgencyRetailerOrder(agencyRetailerOrder);
        }

        public void UpdateAgencyRetailerOrder(AgencyRetailerOrder agencyRetailerOrder) {
            new AgencyRetailerOrderAch(this).UpdateAgencyRetailerOrder(agencyRetailerOrder);
        }

        public IQueryable<AgencyRetailerOrder> GetAgencyRetailerOrderWithDetails() {
            return new AgencyRetailerOrderAch(this).GetAgencyRetailerOrderWithDetails();
        }

        public IQueryable<AgencyRetailerOrder> GetAgencyRetailerOrdersWithVehiclePartsHandleOrder() {
            return new AgencyRetailerOrderAch(this).GetAgencyRetailerOrdersWithVehiclePartsHandleOrder();
        }
    }
}
