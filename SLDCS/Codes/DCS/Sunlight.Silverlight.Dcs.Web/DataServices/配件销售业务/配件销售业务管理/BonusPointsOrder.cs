﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using System.Text;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class BonusPointsOrderAch : DcsSerivceAchieveBase {
        public BonusPointsOrderAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public void UpdateBonusPointsOrder(BonusPointsOrder bonusPointsOrder) {
            UpdateToDatabase(bonusPointsOrder);
            //UpdateBonusPointsOrderValidate(bonusPointsOrder);
        }

        //internal void UpdateBonusPointsOrderValidate(BonusPointsOrder bonusPointsOrder) {
        //    var userInfo = Utils.GetCurrentUserInfo();
        //    bonusPointsOrder.ModifierId = userInfo.Id;
        //    bonusPointsOrder.ModifierName = userInfo.Name;
        //    bonusPointsOrder.ModifyTime = DateTime.Now;
        //}

        [Query(HasSideEffects = true)]
        public IQueryable<BonusPointsOrder> GetBonusPointsOrderByPartsOutboundBillIds(int[] partsOutboundBillIds) {
            return this.ObjectContext.BonusPointsOrders.Where(r => partsOutboundBillIds.Contains(r.PartsOutboundBillId ?? 0));
        }

        [Query(HasSideEffects = true)]
        public IQueryable<BonusPointsOrder> GetBonusPointsOrderByPartsInboundBillIds(int[] partsInboundBillIds) {
            return this.ObjectContext.BonusPointsOrders.Where(r => partsInboundBillIds.Contains(r.PartsInboundCheckBillId ?? 0));
        }
        //[Query(HasSideEffects = true)]
        //public PartsPurchaseSettleDetail GetXNBonusPointsOrder(int[] partsOutboundBillIds, int[] partsInboundBillIds) {
        //    var bonusPointsOrders1 = this.ObjectContext.BonusPointsOrders.Where(r => partsOutboundBillIds.Contains(r.PartsOutboundBillId ?? 0));
        //    var bonusPointsOrders2 = this.ObjectContext.BonusPointsOrders.Where(r => partsInboundBillIds.Contains(r.PartsInboundCheckBillId ?? 0));

        //    var partsPurchaseSettleDetail = new PartsPurchaseSettleDetail();
        //    partsPurchaseSettleDetail.SettlementPrice = bonusPointsOrders1.Sum(r => r.BonusPointsAmount ?? 0) + bonusPointsOrders2.Sum(r => r.BonusPointsAmount ?? 0);
        //    partsPurchaseSettleDetail.SettlementAmount = bonusPointsOrders1.Sum(r => r.BonusPointsAmount ?? 0) + bonusPointsOrders2.Sum(r => r.BonusPointsAmount ?? 0);
        //    partsPurchaseSettleDetail.QuantityToSettle = 1;
        //    var sparePart = this.ObjectContext.SpareParts.FirstOrDefault(r => r.Name == "虚拟积分配件");
        //    if(sparePart != null) {
        //        partsPurchaseSettleDetail.SparePartId = sparePart.Id;
        //        partsPurchaseSettleDetail.SparePartCode = sparePart.Code;
        //        partsPurchaseSettleDetail.SparePartName = sparePart.Name;
        //    }

        //    return partsPurchaseSettleDetail;
        //}
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               

        public void UpdateBonusPointsOrder(BonusPointsOrder bonusPointsOrder) {
            new BonusPointsOrderAch(this).UpdateBonusPointsOrder(bonusPointsOrder);
        }

        [Query(HasSideEffects = true)]
        public IQueryable<BonusPointsOrder> GetBonusPointsOrderByPartsOutboundBillIds(int[] partsOutboundBillIds) {
            return new BonusPointsOrderAch(this).GetBonusPointsOrderByPartsOutboundBillIds(partsOutboundBillIds);
        }

        [Query(HasSideEffects = true)]
        public IQueryable<BonusPointsOrder> GetBonusPointsOrderByPartsInboundBillIds(int[] partsInboundBillIds) {
            return new BonusPointsOrderAch(this).GetBonusPointsOrderByPartsInboundBillIds(partsInboundBillIds);
        }

        //[Query(HasSideEffects = true)]
        //public PartsPurchaseSettleDetail GetXNBonusPointsOrder(int[] partsOutboundBillIds, int[] partsInboundBillIds) {
        //    return new BonusPointsOrderAch(this).GetXNBonusPointsOrder(partsOutboundBillIds, partsInboundBillIds);
        //}
    }
}
