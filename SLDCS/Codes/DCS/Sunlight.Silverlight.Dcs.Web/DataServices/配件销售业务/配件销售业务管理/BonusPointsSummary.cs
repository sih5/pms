﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using System.Text;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class BonusPointsSummaryAch : DcsSerivceAchieveBase {
        public void InsertBonusPointsSummary(BonusPointsSummary bonusPointsSummary) {

            InsertToDatabase(bonusPointsSummary);
            this.InsertBonusPointsSummaryValidate(bonusPointsSummary);
            if(bonusPointsSummary.BonusPointsSummaryLists.Count > 0) {
                var bonusPointsSummaryLists = ChangeSet.GetAssociatedChanges(bonusPointsSummary, v => v.BonusPointsSummaryLists, ChangeOperation.Insert);
                foreach(BonusPointsSummaryList bonusPointsSummaryList in bonusPointsSummary.BonusPointsSummaryLists.ToList())
                    InsertToDatabase(bonusPointsSummaryList);
            }
        }

        internal void InsertBonusPointsSummaryValidate(BonusPointsSummary bonusPointsSummary) {
            var userInfo = Utils.GetCurrentUserInfo();
            bonusPointsSummary.CreatorId = userInfo.Id;
            bonusPointsSummary.CreatorName = userInfo.Name;
            bonusPointsSummary.CreateTime = DateTime.Now;
        }


        public IQueryable<BonusPointsOrder> GetBonusPointsOrdersByBrandIdAndCutTime(int? brandId, DateTime? cutTime) {
            return ObjectContext.BonusPointsOrders.Where(r => r.BrandId == brandId && r.CreateTime <= cutTime && r.SettlementStatus == (int)DcsBonusPointsSettlementStatus.未汇总).OrderBy(e => e.Id);
        }

    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               

        public void InsertBonusPointsSummary(BonusPointsSummary bonusPointsSummary) {
            new BonusPointsSummaryAch(this).InsertBonusPointsSummary(bonusPointsSummary);
        }


        public IQueryable<BonusPointsOrder> GetBonusPointsOrdersByBrandIdAndCutTime(int? brandId, DateTime? cutTime) {
            return new BonusPointsSummaryAch(this).GetBonusPointsOrdersByBrandIdAndCutTime(brandId, cutTime);
        }
    }
}
