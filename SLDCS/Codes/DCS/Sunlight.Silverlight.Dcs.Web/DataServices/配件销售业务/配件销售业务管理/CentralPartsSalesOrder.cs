﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
using System.Configuration;
using System.Data;
using System.Text;
using Devart.Data.Oracle;
using NPOI.SS.Formula.Functions;
using System.Transactions;
using System.Data.Objects.SqlClient;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class CentralPartsSalesOrderAch : DcsSerivceAchieveBase {
        public CentralPartsSalesOrderAch(DcsDomainService domainService)
            : base(domainService) {
        }
        public IEnumerable<CentralPartsSalesOrder> 获取中心库销售一次满足率日期报表(int? type, DateTime? bApproveTime, DateTime? eApproveTime) {
            string SQL = @"select kk.year,
                                   kk.month,
                                   kk.day,
                                   kk.OrderAmount,
                                   kk.SatisfactionAmount,
                                   kk.OrderAmount - kk.SatisfactionAmount as UnSatisfactionAmount,
                                   round(decode(kk.OrderAmount,
                                                0,
                                                0,
                                                kk.SatisfactionAmount / kk.OrderAmount),
                                         4)*100 as OneTimeRate,
                                   kk.TotalOrderAmount,
                                   kk.ApproveTotalOrderAmount,
                                   rownum
                              from (select yy.year,
                                           yy.month,
                                           yy.day,
                                           sum(OrderAmount) as OrderAmount,
                                           sum(SatisfactionAmount) as SatisfactionAmount,
                                           sum(TotalOrderAmount) as TotalOrderAmount,
                                           sum(ApproveTotalOrderAmount) as ApproveTotalOrderAmount
                                      from (select to_char(css.ApproveTime, 'yyyy') as year,
                                                   (case
                                                     when {0} = 1 then
                                                      cast('' as varchar2(50))
                                                     else
                                                      to_char(css.approvetime, 'MM')
                                                   end) as month,
                                                   (case
                                                     when {0} = 1then cast('年度汇总' as varchar2(50)) when
                                                      {0} = 2 then
                                                      cast('月度汇总' as varchar2(50))
                                                     else
                                                      to_char(css.approvetime, 'DD')
                                                   end) as day,
                                                   1 as OrderAmount,
                                                   (case
                                                     when css.orderqty = css.FirstApproveQty then
                                                      1
                                                     else
                                                      0
                                                   end) as SatisfactionAmount,
                                                   css.orderqty * css.retailprice as TotalOrderAmount,
                                                   css.firstapproveqty * css.retailprice as ApproveTotalOrderAmount
                                              from CenterSaleSettle css  {1}) yy
                                     group by yy.year, yy.month, yy.day
                                     order by yy.year, yy.month, yy.day) kk";
            var serachSQL = new StringBuilder();
            if(bApproveTime.HasValue && bApproveTime.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
                bApproveTime = null;
            } if(eApproveTime.HasValue && eApproveTime.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
                eApproveTime = null;
            }
            if(null==type){
                type = 3;
            }
            serachSQL.Append(" where css.OBJECTTYPE=2 ");
            if(bApproveTime != null) {
                if(type==1){
                    serachSQL.Append(" and  to_char(css.ApproveTime, 'yyyy')>=" + bApproveTime.Value.Year);
                } else if(type==2 ) {
                    serachSQL.Append(" and  to_char(css.ApproveTime, 'yyyy-MM')>= to_char(to_date('" + bApproveTime.Value.ToString()+ "', 'yyyy-MM-dd HH24:mi:ss'),'yyyy-MM')");
                } else {
                    serachSQL.Append(" and  css.ApproveTime >=to_date('" + bApproveTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')");
                }
            }
            if(eApproveTime!=null ){
                if(type == 1) {                    
                    serachSQL.Append(" and  to_char(css.ApproveTime, 'yyyy')<=" + eApproveTime.Value.Year);
                } else if(type == 2) {
                    serachSQL.Append(" and  to_char(css.ApproveTime, 'yyyy-MM')<= to_char(to_date('" + eApproveTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss'),'yyyy-MM')");
                } else {
                    serachSQL.Append(" and  css.ApproveTime <=to_date('" + eApproveTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')");
                }
            }
            var userInfo = Utils.GetCurrentUserInfo();

            var company = ObjectContext.Companies.Where(r => r.Id == userInfo.EnterpriseId).FirstOrDefault();
            if(company.Type == (int)DcsCompanyType.分公司) {
                //判断是否维护了人员与分销中心关系
                var isHas = ObjectContext.MarketDptPersonnelRelations.Where(r => r.PersonnelId == userInfo.Id && r.Status == (int)DcsBaseDataStatus.有效).Count();
                if(isHas > 0) {
                    serachSQL.Append(" and  (exists(select 1 from  MarketDptPersonnelRelation mr join Agency ag on mr.marketdepartmentid = ag.marketingdepartmentid  where mr.PersonnelId = " + userInfo.Id + " and ag.id=css.CenterId and mr.status=1) )" );
                }
            } else return null;  
            SQL = string.Format(SQL,type,serachSQL);
            var search = ObjectContext.ExecuteStoreQuery<CentralPartsSalesOrder>(SQL).ToList();
            return search;
        }
        public IEnumerable<CentralPartsSalesOrder> 获取中心库销售一次满足率服务站报表(int? type, DateTime? bApproveTime, DateTime? eApproveTime, string centerName, string dealerName) {
            string SQL = @"select kk.centername,
                           kk.dealername,
                           kk.OrderAmount,
                           kk.SatisfactionAmount,
                           kk.OrderAmount - kk.SatisfactionAmount as UnSatisfactionAmount,
                           round(decode(kk.OrderAmount,
                                        0,
                                        0,
                                        kk.SatisfactionAmount / kk.OrderAmount),
                                 4) * 100 as OneTimeRate,
                           kk.TotalOrderAmount,
                           kk.ApproveTotalOrderAmount,
                           rownum
                      from (select yy.centername,
                                   yy.dealername,
                                   sum(OrderAmount) as OrderAmount,
                                   sum(SatisfactionAmount) as SatisfactionAmount,
                                   sum(TotalOrderAmount) as TotalOrderAmount,
                                   sum(ApproveTotalOrderAmount) as ApproveTotalOrderAmount
                              from (select (case
                                             when {0} = 1 or {0}= 2 then
                                              css.centername
                                             else
                                              cast('汇总' as varchar2(50))
                                           end) as centername,
                                           (case
                                             when {0}= 1 or {0}= 3 then
                                              cast('汇总' as varchar2(50))
                                             when {0} = 2 then
                                              css.dealername
                                           end) as dealername,
                                           1 as OrderAmount,
                                           (case
                                             when css.orderqty = css.FirstApproveQty then
                                              1
                                             else
                                              0
                                           end) as SatisfactionAmount,
                                           css.orderqty * css.retailprice as TotalOrderAmount,
                                           css.firstapproveqty * css.retailprice as ApproveTotalOrderAmount
                                      from CenterSaleSettle css {1}
                                     ) yy
                             group by yy.centername, yy.dealername
                             order by yy.centername, yy.dealername) kk";

            var serachSQL = new StringBuilder();
            if(bApproveTime.HasValue && bApproveTime.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
                bApproveTime = null;
            } if(eApproveTime.HasValue && eApproveTime.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
                eApproveTime = null;
            }
            if(null == type) {
                type = 3;
            }

            serachSQL.Append(" where css.OBJECTTYPE=2  ");
            if(bApproveTime != null) {
                serachSQL.Append(" and  css.ApproveTime >=to_date('" + bApproveTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')");
            }
            if(eApproveTime != null) {
                serachSQL.Append(" and  css.ApproveTime <=to_date('" + eApproveTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')");
            }
            if(!string.IsNullOrEmpty(centerName)) {
                serachSQL.Append(" and css.CenterName like '%"+centerName+"%'");
            }
            if(!string.IsNullOrEmpty(dealerName)) {
                serachSQL.Append(" and css.DealerName like '%" + dealerName + "%'");
                type = 2;
            }
            //查询当前登录人所属公司性质，若是代理库，则只查询其下属区域内的订单
            var userInfo = Utils.GetCurrentUserInfo();

            var company = ObjectContext.Companies.Where(r => r.Id == userInfo.EnterpriseId).FirstOrDefault();
            if(company.Type == (int)DcsCompanyType.代理库) {
                serachSQL.Append(" and css.CenterId ="+company.Id);
            } else if(company.Type == (int)DcsCompanyType.分公司) {
                //判断是否维护了人员与分销中心关系
                var isHas = ObjectContext.MarketDptPersonnelRelations.Where(r => r.PersonnelId == userInfo.Id && r.Status == (int)DcsBaseDataStatus.有效).Count();
                if(isHas > 0) {
                    serachSQL.Append(" and  (exists(select 1 from  MarketDptPersonnelRelation mr join Agency ag on mr.marketdepartmentid = ag.marketingdepartmentid  where mr.PersonnelId = " + userInfo.Id + " and ag.id=css.CenterId and mr.status=1) )");
                }
            } else return null;  
            SQL = string.Format(SQL, type, serachSQL);
            var search = ObjectContext.ExecuteStoreQuery<CentralPartsSalesOrder>(SQL).ToList();
            return search;
        }
        public IEnumerable<CentralPartsSalesOrder> 获取配件公司销售一次满足率日期报表(int? type, DateTime? bApproveTime, DateTime? eApproveTime) {
            string SQL = @"select kk.year,
                                   kk.month,
                                   kk.day,
                                   kk.OrderAmount,
                                   kk.SatisfactionAmount,
                                   kk.OrderAmount - kk.SatisfactionAmount as UnSatisfactionAmount,
                                   round(decode(kk.OrderAmount,
                                                0,
                                                0,
                                                kk.SatisfactionAmount / kk.OrderAmount),
                                         4)*100 as OneTimeRate,
                                   kk.TotalOrderAmount,
                                   kk.ApproveTotalOrderAmount,
                                   rownum
                              from (select yy.year,
                                           yy.month,
                                           yy.day,
                                           sum(OrderAmount) as OrderAmount,
                                           sum(SatisfactionAmount) as SatisfactionAmount,
                                           sum(TotalOrderAmount) as TotalOrderAmount,
                                           sum(ApproveTotalOrderAmount) as ApproveTotalOrderAmount
                                      from (select to_char(css.ApproveTime, 'yyyy') as year,
                                                   (case
                                                     when {0} = 1 then
                                                      cast('' as varchar2(50))
                                                     else
                                                      to_char(css.approvetime, 'MM')
                                                   end) as month,
                                                   (case
                                                     when {0} = 1then cast('年度汇总' as varchar2(50)) when
                                                      {0} = 2 then
                                                      cast('月度汇总' as varchar2(50))
                                                     else
                                                      to_char(css.approvetime, 'DD')
                                                   end) as day,
                                                   1 as OrderAmount,
                                                   (case
                                                     when css.orderqty = css.FirstApproveQty then
                                                      1
                                                     else
                                                      0
                                                   end) as SatisfactionAmount,
                                                   css.orderqty * css.retailprice as TotalOrderAmount,
                                                   css.firstapproveqty * css.retailprice as ApproveTotalOrderAmount
                                              from CenterSaleSettle css  {1}) yy
                                     group by yy.year, yy.month, yy.day
                                     order by yy.year, yy.month, yy.day) kk";
            var serachSQL = new StringBuilder();
            if(bApproveTime.HasValue && bApproveTime.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
                bApproveTime = null;
            } if(eApproveTime.HasValue && eApproveTime.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
                eApproveTime = null;
            }
            if(null == type) {
                type = 3;
            }
            serachSQL.Append(" where css.OBJECTTYPE=1  and css.CenterCode not in('PJ03000577','PJ03000574','PJ02042716')");
            if(bApproveTime != null) {
                if(type == 1) {
                    serachSQL.Append(" and  to_char(css.ApproveTime, 'yyyy')>=" + bApproveTime.Value.Year);
                } else if(type == 2) {
                    serachSQL.Append(" and  to_char(css.ApproveTime, 'yyyy-MM')>= to_char(to_date('" + bApproveTime.Value.ToString() + "', 'yyyy-MM-dd HH24:mi:ss'),'yyyy-MM')");
                } else {
                    serachSQL.Append(" and  css.ApproveTime >=to_date('" + bApproveTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')");
                }
            }
            if(eApproveTime != null) {
                if(type == 1) {
                    serachSQL.Append(" and  to_char(css.ApproveTime, 'yyyy')<=" + eApproveTime.Value.Year);
                } else if(type == 2) {
                    serachSQL.Append(" and  to_char(css.ApproveTime, 'yyyy-MM')<= to_char(to_date('" + eApproveTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss'),'yyyy-MM')");
                } else {
                    serachSQL.Append(" and  css.ApproveTime <=to_date('" + eApproveTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')");
                }
            }
            var userInfo = Utils.GetCurrentUserInfo();

            var company = ObjectContext.Companies.Where(r => r.Id == userInfo.EnterpriseId).FirstOrDefault();
            if(company.Type == (int)DcsCompanyType.分公司) {
                //判断是否维护了人员与分销中心关系
                var isHas = ObjectContext.MarketDptPersonnelRelations.Where(r => r.PersonnelId == userInfo.Id && r.Status == (int)DcsBaseDataStatus.有效).Count();
                if(isHas > 0) {
                    serachSQL.Append(" and  (exists(select 1 from  MarketDptPersonnelRelation mr join Agency ag on mr.marketdepartmentid = ag.marketingdepartmentid  where mr.PersonnelId = " + userInfo.Id + " and ag.id=css.CenterId and mr.status=1) )");
                }
            } else return null;  
            SQL = string.Format(SQL, type, serachSQL);
            var search = ObjectContext.ExecuteStoreQuery<CentralPartsSalesOrder>(SQL).ToList();
            return search;
        }
        public IEnumerable<CentralPartsSalesOrder> 获取配件公司销售一次满足率对中心库报表( DateTime? bApproveTime, DateTime? eApproveTime, string centerName) {
            string SQL = @"select kk.centername,
                           kk.OrderAmount,
                           kk.SatisfactionAmount,
                           kk.OrderAmount - kk.SatisfactionAmount as UnSatisfactionAmount,
                           round(decode(kk.OrderAmount,
                                        0,
                                        0,
                                        kk.SatisfactionAmount / kk.OrderAmount),
                                 4) * 100 as OneTimeRate,
                           kk.TotalOrderAmount,
                           kk.ApproveTotalOrderAmount,
                           rownum
                      from (select yy.centername,
                                   sum(OrderAmount) as OrderAmount,
                                   sum(SatisfactionAmount) as SatisfactionAmount,
                                   sum(TotalOrderAmount) as TotalOrderAmount,
                                   sum(ApproveTotalOrderAmount) as ApproveTotalOrderAmount
                              from (select css.centername as centername,                       
                                           1 as OrderAmount,
                                           (case
                                             when css.orderqty = css.FirstApproveQty then
                                              1
                                             else
                                              0
                                           end) as SatisfactionAmount,
                                           css.orderqty * css.retailprice as TotalOrderAmount,
                                           css.firstapproveqty * css.retailprice as ApproveTotalOrderAmount
                                      from CenterSaleSettle css
                                     where css.objecttype = 1 {0}) yy
                             group by yy.centername
                             order by yy.centername) kk";

            var serachSQL = new StringBuilder();
            if(bApproveTime.HasValue && bApproveTime.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
                bApproveTime = null;
            } if(eApproveTime.HasValue && eApproveTime.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
                eApproveTime = null;
            }       
            if(bApproveTime != null) {
                serachSQL.Append(" and  css.ApproveTime >=to_date('" + bApproveTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')");
            }
            if(eApproveTime != null) {
                serachSQL.Append(" and  css.ApproveTime <=to_date('" + eApproveTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')");
            }
            if(!string.IsNullOrEmpty(centerName)) {
                serachSQL.Append(" and css.CenterName like '%" + centerName + "%'");
            }
            var userInfo = Utils.GetCurrentUserInfo();

            var company = ObjectContext.Companies.Where(r => r.Id == userInfo.EnterpriseId).FirstOrDefault();
            if(company.Type == (int)DcsCompanyType.分公司) {
                //判断是否维护了人员与分销中心关系
                var isHas = ObjectContext.MarketDptPersonnelRelations.Where(r => r.PersonnelId == userInfo.Id && r.Status == (int)DcsBaseDataStatus.有效).Count();
                if(isHas > 0) {
                    serachSQL.Append(" and  (exists(select 1 from  MarketDptPersonnelRelation mr join Agency ag on mr.marketdepartmentid = ag.marketingdepartmentid  where mr.PersonnelId = " + userInfo.Id + " and ag.id=css.CenterId and mr.status=1) )");
                }
            } else return null;  
            SQL = string.Format(SQL,  serachSQL);
            var search = ObjectContext.ExecuteStoreQuery<CentralPartsSalesOrder>(SQL).ToList();
            if(search.Count() > 0) {
                CentralPartsSalesOrder summary = new CentralPartsSalesOrder();
                summary.CenterName = "汇总";
                summary.OrderAmount = search.Sum(r => r.OrderAmount);
                summary.SatisfactionAmount = search.Sum(r => r.SatisfactionAmount);
                summary.UnSatisfactionAmount = search.Sum(r => r.UnSatisfactionAmount);
                summary.OneTimeRate = Math.Round(Decimal.Parse((summary.SatisfactionAmount * 100).ToString()) / Decimal.Parse(summary.OrderAmount.ToString()), 2);
                summary.TotalOrderAmount = search.Sum(r => r.TotalOrderAmount);
                summary.ApproveTotalOrderAmount = search.Sum(r => r.ApproveTotalOrderAmount);
                summary.RowNum = 0;
                List<CentralPartsSalesOrder> returnList = new List<CentralPartsSalesOrder>();
                List<CentralPartsSalesOrder> itemList = new List<CentralPartsSalesOrder>();
                foreach(var item in search) {
                    itemList.Add(item);
                }
                int j = 0;
                int addItem = itemList.Count() + (itemList.Count() / 50) + 1;
                for(int i = 0; i < addItem; i++) {
                    if(i == 0 || (i % 50) == 0) {
                        returnList.Add(summary);
                    } else {
                        returnList.Add(itemList[j]);
                        j++;
                    }
                }
                return returnList;
            }
            return null;
        }
    }
    partial class DcsDomainService {
        public IEnumerable<CentralPartsSalesOrder> 获取中心库销售一次满足率日期报表(int? type, DateTime? bApproveTime, DateTime? eApproveTime) {
            return new CentralPartsSalesOrderAch(this).获取中心库销售一次满足率日期报表(type, bApproveTime, eApproveTime);
        }
        public IEnumerable<CentralPartsSalesOrder> 获取中心库销售一次满足率服务站报表(int? type, DateTime? bApproveTime, DateTime? eApproveTime, string centerName, string dealerName) {
            return new CentralPartsSalesOrderAch(this).获取中心库销售一次满足率服务站报表(type, bApproveTime, eApproveTime, centerName, dealerName);
        }
        public IEnumerable<CentralPartsSalesOrder> 获取配件公司销售一次满足率日期报表(int? type, DateTime? bApproveTime, DateTime? eApproveTime) {
            return new CentralPartsSalesOrderAch(this).获取配件公司销售一次满足率日期报表(type, bApproveTime, eApproveTime);
        }
        public IEnumerable<CentralPartsSalesOrder> 获取配件公司销售一次满足率对中心库报表(DateTime? bApproveTime, DateTime? eApproveTime, string centerName) {
            return new CentralPartsSalesOrderAch(this).获取配件公司销售一次满足率对中心库报表(bApproveTime, eApproveTime, centerName);
        }
    }
}
