﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Net;
using System.ServiceModel.DomainServices.Server;
using System.Text;
using System.Transactions;
using System.Web.Configuration;
using Devart.Data.Oracle;
using FTDCSWebService;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class CrossSalesOrderAch : DcsSerivceAchieveBase {
        public CrossSalesOrderAch(DcsDomainService domainService)
            : base(domainService) {
        }
        internal void InsertCrossSalesOrderValidate(CrossSalesOrder crossSalesOrder) {
            if(string.IsNullOrWhiteSpace(crossSalesOrder.Code) || crossSalesOrder.Code == GlobalVar.ASSIGNED_BY_SERVER) {
                //获取业务代码             
                crossSalesOrder.Code = CodeGenerator.Generate("CrossSalesOrder", crossSalesOrder.ApplyCompnayCode);
            }
            var userInfo = Utils.GetCurrentUserInfo();
            crossSalesOrder.CreatorId = userInfo.Id;
            crossSalesOrder.CreatorName = userInfo.Name;
            crossSalesOrder.CreateTime = DateTime.Now;
        }
        internal void UpdateCrossSalesOrderValidate(CrossSalesOrder crossSalesOrder) {
            var userInfo = Utils.GetCurrentUserInfo();
            crossSalesOrder.ModifierId = userInfo.Id;
            crossSalesOrder.ModifierName = userInfo.Name;
            crossSalesOrder.ModifyTime = DateTime.Now;

        }


        public void InsertCrossSalesOrder(CrossSalesOrder crossSalesOrder) {
            InsertToDatabase(crossSalesOrder);
            var crossSalesOrderDetails = ChangeSet.GetAssociatedChanges(crossSalesOrder, v => v.CrossSalesOrderDetails, ChangeOperation.Insert);
            foreach(CrossSalesOrderDetail crossSalesOrderDetail in crossSalesOrderDetails) {
                InsertToDatabase(crossSalesOrderDetail);
            }
            this.InsertCrossSalesOrderValidate(crossSalesOrder);
        }

        public void UpdateCrossSalesOrder(CrossSalesOrder crossSalesOrder) {
            crossSalesOrder.CrossSalesOrderDetails.Clear();
            UpdateToDatabase(crossSalesOrder);
            var crossSalesOrderDetails = ChangeSet.GetAssociatedChanges(crossSalesOrder, v => v.CrossSalesOrderDetails);
            foreach(CrossSalesOrderDetail crossSalesOrderDetail in crossSalesOrderDetails) {
                switch(ChangeSet.GetChangeOperation(crossSalesOrderDetail)) {
                    case ChangeOperation.Insert:
                        InsertToDatabase(crossSalesOrderDetail);
                        break;
                    case ChangeOperation.Update:
                        UpdateToDatabase(crossSalesOrderDetail);
                        break;
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(crossSalesOrderDetail);
                        break;
                }
            }
            this.UpdateCrossSalesOrderValidate(crossSalesOrder);
        }
        public IQueryable<CrossSalesOrder> GetCrossSalesOrderBySparepart(string sparePartName, string sparePartCode) {
            var userInfo = Utils.GetCurrentUserInfo();
            var company = ObjectContext.Companies.Where(t => t.Id == userInfo.EnterpriseId).First();
            var returnOrder = ObjectContext.CrossSalesOrders.Where(t => 1 == 1);
            if(company.Type != (int)DcsCompanyType.分公司) {
                returnOrder = returnOrder.Where(t => t.ApplyCompnayId == company.Id || t.SubCompanyId == company.Id);
            }
            if(!string.IsNullOrEmpty(sparePartCode)) {
                returnOrder = returnOrder.Where(t => ObjectContext.CrossSalesOrderDetails.Any(p => p.CrossSalesOrderId == t.Id && p.SparePartCode.Contains(sparePartCode)));
            }
            if(!string.IsNullOrEmpty(sparePartName)) {
                returnOrder = returnOrder.Where(t => ObjectContext.CrossSalesOrderDetails.Any(p => p.CrossSalesOrderId == t.Id && p.SparePartName.Contains(sparePartName)));
            }
            return returnOrder.OrderByDescending(t => t.CreateTime);
        }
        public IQueryable<PartsSalesOrderDetailCross> 跨区域销售销售订单看板查询(string partsSalesOrderCode) {
            var salesOrderDetail = from a in ObjectContext.PartsSalesOrders.Where(t => t.Code == partsSalesOrderCode)
                                   join b in ObjectContext.PartsSalesOrderDetails on a.Id equals b.PartsSalesOrderId
                                   select new PartsSalesOrderDetailCross {
                                       Id = b.Id,
                                       SparePartId = b.SparePartId,
                                       SparePartCode = b.SparePartCode,
                                       SparePartName = b.SparePartName,
                                       ApproveQuantity = b.ApproveQuantity,
                                       OrderedQuantity = b.OrderedQuantity,
                                       OrderPrice = b.OrderPrice,
                                       PartsSalesOrderCode = a.Code,
                                       SalesPrice = b.SalesPrice,
                                   };
            return salesOrderDetail.OrderBy(t => t.SparePartCode);
        }
        public CrossSalesOrder GetCrossSalesOrderWithDetailsById(int id) {
            var dbCrossSalesOrder = ObjectContext.CrossSalesOrders.Include("CrossSalesOrderDetails").SingleOrDefault(entity => entity.Id == id && entity.Status != (int)DCSCrossSalesOrderStatus.作废);
            return dbCrossSalesOrder;
        }
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               


        public void InsertCrossSalesOrder(CrossSalesOrder crossSalesOrder) {
            new CrossSalesOrderAch(this).InsertCrossSalesOrder(crossSalesOrder);
        }
        public void UpdateCrossSalesOrder(CrossSalesOrder crossSalesOrder) {
            new CrossSalesOrderAch(this).UpdateCrossSalesOrder(crossSalesOrder);
        }
        public IQueryable<CrossSalesOrder> GetCrossSalesOrderBySparepart(string sparePartName, string sparePartCode) {
            return new CrossSalesOrderAch(this).GetCrossSalesOrderBySparepart(sparePartName, sparePartCode);
        }
        public IQueryable<PartsSalesOrderDetailCross> 跨区域销售销售订单看板查询(string partsSalesOrderCode) {
            return new CrossSalesOrderAch(this).跨区域销售销售订单看板查询(partsSalesOrderCode);
        }
        public CrossSalesOrder GetCrossSalesOrderWithDetailsById(int id) {
            return new CrossSalesOrderAch(this).GetCrossSalesOrderWithDetailsById(id);
        }
    }
}
