﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Net;
using System.ServiceModel.DomainServices.Server;
using System.Text;
using System.Transactions;
using System.Web.Configuration;
using Devart.Data.Oracle;
using FTDCSWebService;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class CrossSalesOrderDetailAch : DcsSerivceAchieveBase {
        public CrossSalesOrderDetailAch(DcsDomainService domainService)
            : base(domainService) {
        }
        public void InsertCrossSalesOrderDetail(CrossSalesOrderDetail crossSalesOrderDetail) {

        }

        public void UpdateCrossSalesOrderDetail(CrossSalesOrderDetail crossSalesOrderDetail) {
        }
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               


        public void InsertCrossSalesOrderDetail(CrossSalesOrderDetail crossSalesOrderDetail) {
            new CrossSalesOrderDetailAch(this).InsertCrossSalesOrderDetail(crossSalesOrderDetail);
        }
        public void UpdateCrossSalesOrderDetail(CrossSalesOrderDetail crossSalesOrderDetail) {
            new CrossSalesOrderDetailAch(this).UpdateCrossSalesOrderDetail(crossSalesOrderDetail);
        }
    }
}
