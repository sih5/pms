﻿using System;
using System.Linq;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class CustomerDirectSpareListAch : DcsSerivceAchieveBase {
        public CustomerDirectSpareListAch(DcsDomainService domainService)
            : base(domainService) {
        }

        internal void InsertCustomerDirectSpareListValidate(CustomerDirectSpareList customerDirectSpareList) {
            var userInfo = Utils.GetCurrentUserInfo();
            customerDirectSpareList.CreatorId = userInfo.Id;
            customerDirectSpareList.CreatorName = userInfo.Name;
            customerDirectSpareList.CreateTime = DateTime.Now;
        }

        internal void UpdateCustomerDirectSpareListValidate(CustomerDirectSpareList customerDirectSpareList) {
            var userInfo = Utils.GetCurrentUserInfo();
            customerDirectSpareList.ModifierId = userInfo.Id;
            customerDirectSpareList.ModifierName = userInfo.Name;
            customerDirectSpareList.ModifyTime = DateTime.Now;
        }

        public void InsertCustomerDirectSpareList(CustomerDirectSpareList customerDirectSpareList) {
            InsertToDatabase(customerDirectSpareList);
            this.InsertCustomerDirectSpareListValidate(customerDirectSpareList);
        }

        public void UpdateCustomerDirectSpareList(CustomerDirectSpareList customerDirectSpareList) {
            UpdateToDatabase(customerDirectSpareList);
            this.UpdateCustomerDirectSpareListValidate(customerDirectSpareList);
        }
    }

    partial class DcsDomainService {

        public void InsertCustomerDirectSpareList(CustomerDirectSpareList customerDirectSpareList) {
            new CustomerDirectSpareListAch(this).InsertCustomerDirectSpareList(customerDirectSpareList);
        }

        public void UpdateCustomerDirectSpareList(CustomerDirectSpareList customerDirectSpareList) {
            new CustomerDirectSpareListAch(this).UpdateCustomerDirectSpareList(customerDirectSpareList);
        }
    }
}
