﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.EntityClient;
using System.Data.Objects;
using System.Linq;
using System.Text;
using Devart.Data.Oracle;
using Microsoft.Data.Extensions;
using Quartz.Xml;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class CustomerInformationAch : DcsSerivceAchieveBase {

        internal void InsertCustomerInformationValidate(CustomerInformation customerInformation) {
            var dbCustomerInformation = ObjectContext.CustomerInformations.Where(r => r.SalesCompanyId == customerInformation.SalesCompanyId && r.CustomerCompanyId == customerInformation.CustomerCompanyId && r.Status == (int)DcsMasterDataStatus.有效).SetMergeOption(MergeOption.NoTracking).ToArray();
            if(dbCustomerInformation.Length > 0) {
                throw new ValidationException(ErrorStrings.CustomerInformation_Validation5);
            }
            var userInfo = Utils.GetCurrentUserInfo();
            customerInformation.CreatorId = userInfo.Id;
            customerInformation.CreatorName = userInfo.Name;
            customerInformation.CreateTime = DateTime.Now;
        }

        internal void UpdateCustomerInformationValidate(CustomerInformation customerInformation) {
            var userInfo = Utils.GetCurrentUserInfo();
            customerInformation.ModifierId = userInfo.Id;
            customerInformation.ModifierName = userInfo.Name;
            customerInformation.ModifyTime = DateTime.Now;
        }

        public void InsertCustomerInformation(CustomerInformation customerInformation) {
            InsertToDatabase(customerInformation);
            this.InsertCustomerInformationValidate(customerInformation);
        }

        public void UpdateCustomerInformation(CustomerInformation customerInformation) {
            UpdateToDatabase(customerInformation);
            this.UpdateCustomerInformationValidate(customerInformation);
        }

        public CustomerInformation GetCustomerInformationWithDetailsById(int id) {
            var result = ObjectContext.CustomerInformations.SingleOrDefault(entity => entity.Id == id && entity.Status == (int)DcsMasterDataStatus.有效);
            if(result != null) {
                var customerCompany = ObjectContext.Companies.SingleOrDefault(e => e.Id == result.CustomerCompanyId && e.Status == (int)DcsMasterDataStatus.有效);
                var salesCompany = ObjectContext.Companies.SingleOrDefault(e => e.Id == result.SalesCompanyId && e.Status == (int)DcsMasterDataStatus.有效);
            }
            return result;
        }

        public IQueryable<CustomerInformation> GetCustomerInformationWithDetails() {
            return ObjectContext.CustomerInformations.Include("CustomerCompany").Include("SalesCompany").OrderBy(v => v.Id);
        }

        public List<VirtualCustomerInformation> 获取配件销售退货经销商(int accountGroupId, DateTime cutoffTime, string name, string code) {
            var salesUnit = ObjectContext.SalesUnits.FirstOrDefault(r => r.AccountGroupId == accountGroupId);
            if(salesUnit == null) {
                return null;
            }
            //int[] dcsPartsInboundTypes = { (int)DcsPartsInboundType.销售退货, (int)DcsPartsInboundType.配件零售退货, (int)DcsPartsInboundType.质量件索赔, (int)DcsPartsInboundType.代发退货 };
            ////var companies = ObjectContext.Companies.Where(company => company.Status == (int)DcsMasterDataStatus.有效);
            //var customerAccounts = ObjectContext.CustomerAccounts.Where(r => r.Status == (int)DcsMasterDataStatus.有效 && r.AccountGroupId == accountGroupId);
            //var partsInboundCheckBills = ObjectContext.PartsInboundCheckBills.Where(p => dcsPartsInboundTypes.Contains(p.InboundType)
            //    && p.CreateTime <= cutoffTime && p.SettlementStatus == (int)DcsPartsSettlementStatus.待结算 && p.PartsSalesCategoryId == salesUnit.PartsSalesCategoryId);
            //var customerInformations = ObjectContext.CustomerInformations.Where(r => r.Status == (int)DcsMasterDataStatus.有效);
            var userinfo = Utils.GetCurrentUserInfo();
            //var result = customerInformations.Where(r => r.SalesCompanyId == userinfo.EnterpriseId
            //    //&& companies.Any(c => c.Id == r.CustomerCompanyId)
            //   && customerAccounts.Any(c => c.CustomerCompanyId == r.CustomerCompanyId)
            //   && partsInboundCheckBills.Any(p => p.StorageCompanyId == r.SalesCompanyId
            //       && p.CounterpartCompanyId == r.CustomerCompanyId)
            //   ).Include("CustomerCompany").Select(e => new VirtualCustomerInformation {
            //       Id = e.Id,
            //       CustomerCompanyId = e.CustomerCompanyId,
            //       CustomerCompanyName = e.CustomerCompany.Name,
            //       CustomerCompanyCode = e.CustomerCompany.Code
            //   }).OrderBy(r => r.Id);
            //if(string.IsNullOrEmpty(name))
            //    return result;
            var stringBuilder = new StringBuilder();

            stringBuilder.AppendFormat(@"with t_Customerinformation as
                         (select ci.Id  as Id,
                                 c.id   as CustomerCompanyId,
                                 c.code as CustomerCompanyCode,
                                 c.name as CustomerCompanyName
                            from Customerinformation ci, company c
                           where ci.customercompanyid = c.id
                             and ci.status = {0}
                             and ci.salescompanyid = {1} -- 当前登录企业Id参数
                             and c.status = {2} ", (int)DcsMasterDataStatus.有效, userinfo.EnterpriseId, (int)DcsMasterDataStatus.有效);
            if(!string.IsNullOrEmpty(name))
                stringBuilder.AppendLine(string.Format("and c.name like '%{0}%' ", name));
            if(!string.IsNullOrEmpty(code))
                stringBuilder.AppendLine(string.Format("and c.code like '%{0}%' ", code));
            stringBuilder.AppendFormat(@") select c1.Id,
                               c1.CustomerCompanyId,
                               c1.CustomerCompanyCode,
                               c1.CustomerCompanyName
                          from t_Customerinformation c1
                         where exists
                         (select 1
                                  from PartsInboundCheckBill p
                                 where (p.InboundType = 2 or p.InboundType = 5 or p.InboundType = 8)
                                   and p.SettlementStatus = {0}
                                   and p.PartsSalesCategoryId = {1} --品牌 参数
                                   and p.StorageCompanyId =  {2} --当前登录企业
                                   ", (int)DcsPartsSettlementStatus.待结算, salesUnit.PartsSalesCategoryId, userinfo.EnterpriseId);
            if(cutoffTime.Year > 1)
                stringBuilder.AppendLine(string.Format("and p.createTime <= to_date('{0}','yyyy-mm-dd,hh24:mi:ss')", cutoffTime));
            stringBuilder.AppendFormat(@"and p.CounterpartCompanyId = c1.CustomerCompanyId) 
                            and exists
                         (select 1
                                  from customeraccount ca
                                 where ca.status = {0}
                                   and ca.AccountGroupId = {1} -- 账户组Id参数
                                   and ca.CustomerCompanyId = c1.CustomerCompanyId)", (int)DcsMasterDataStatus.有效, accountGroupId);
            return this.ObjectContext.ExecuteStoreQuery<VirtualCustomerInformation>(stringBuilder.ToString(), null).ToList();
        }

        public IEnumerable<VirtualCustomerInformation> 获取配件销售结算经销商(int companyId, int accountGroupId, DateTime cutoffTime, string provinceName) {
            string connectionString = "";
            StringBuilder sql = new StringBuilder();
            var entityConnStr = ConfigurationManager.ConnectionStrings["DcsEntities"].ConnectionString;
            if(!string.IsNullOrEmpty(entityConnStr)) {
                var entityConnection = new EntityConnectionStringBuilder(entityConnStr);
                connectionString = entityConnection.ProviderConnectionString;
            }
            OracleConnection conn = new OracleConnection(connectionString);
            conn.Open();
            OracleCommand command = new OracleCommand(null, conn);
            sql.Append(@"select distinct Filter1.Code,Filter1.name,Filter1.id,Filter1.id1, Filter1.Customeraccountid from 
                            (select Extent1.Id id1,  Extent2.Code,
                                   Extent2.Name,
                                   Extent2.Id,
                                   Extent3.Createtime,
                                   Extent3.Customeraccountid,ProvinceName,SalesCompanyId
                              FROM CustomerInformation Extent1
                             INNER JOIN Company Extent2
                                ON Extent1.CustomerCompanyId = Extent2.Id
                             INNER JOIN PartsOutboundBill Extent3
                                ON (Extent1.SalesCompanyId = Extent3.StorageCompanyId)
                               AND (Extent1.CustomerCompanyId = Extent3.CounterpartCompanyId)
                             WHERE ((Extent1.Status = 1) AND (Extent2.Status = 1))
                               AND (Extent3.SettlementStatus = 2)
                            )Filter1 
                            inner join  CustomerAccount Extent4 ON Filter1.CustomerAccountId = Extent4.Id where (Filter1.SalesCompanyId = " + companyId + ") and (Extent4.AccountGroupId = " + accountGroupId + ") and (Filter1.CreateTime <= to_date('" + cutoffTime.ToString("yyyy-MM-dd") + "','yyyy-mm-dd')) ");
            if(!string.IsNullOrEmpty(provinceName)) {
                sql.Append(" and (Filter1.ProvinceName = '" + provinceName + "')");
            }
            command.CommandText = sql.ToString();
            OracleDataAdapter orda = new OracleDataAdapter(command);
            DataSet ds = new DataSet();
            orda.Fill(ds);

            DataTable dtVirtualCustomerInformation = ds.Tables[0];
            List<VirtualCustomerInformation> virtualCustomerInformation = new List<VirtualCustomerInformation>();
            foreach(DataRow item in dtVirtualCustomerInformation.Rows) {
                var company = new VirtualCustomerInformation {
                    Id = int.Parse(item["Id"].ToString()),
                    Code = item["Code"].ToString(),
                    Name = item["Name"].ToString(),
                    CustomerId = int.Parse(item["Id1"].ToString()),
                    Customeraccountid = int.Parse(item["Customeraccountid"].ToString())
                };
                virtualCustomerInformation.Add(company);
            }
            return virtualCustomerInformation.Distinct();

        }

    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               

        public void InsertCustomerInformation(CustomerInformation customerInformation) {
            new CustomerInformationAch(this).InsertCustomerInformation(customerInformation);
        }

        public void UpdateCustomerInformation(CustomerInformation customerInformation) {
            new CustomerInformationAch(this).UpdateCustomerInformation(customerInformation);
        }

        public CustomerInformation GetCustomerInformationWithDetailsById(int id) {
            return new CustomerInformationAch(this).GetCustomerInformationWithDetailsById(id);
        }

        public IQueryable<CustomerInformation> GetCustomerInformationWithDetails() {
            return new CustomerInformationAch(this).GetCustomerInformationWithDetails();
        }

        public List<VirtualCustomerInformation> 获取配件销售退货经销商(int accountGroupId, DateTime cutoffTime, string name, string code) {
            return new CustomerInformationAch(this).获取配件销售退货经销商(accountGroupId, cutoffTime, name, code);
        }

        public IEnumerable<VirtualCustomerInformation> 获取配件销售结算经销商(int companyId, int accountGroupId, DateTime cutoffTime, string provinceName) {
            return new CustomerInformationAch(this).获取配件销售结算经销商(companyId, accountGroupId, cutoffTime, provinceName);
        }
    }
}
