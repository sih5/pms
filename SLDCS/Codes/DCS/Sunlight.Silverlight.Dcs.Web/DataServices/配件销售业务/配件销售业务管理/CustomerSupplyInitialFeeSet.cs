﻿using System;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class CustomerSupplyInitialFeeSetAch : DcsSerivceAchieveBase {
        public CustomerSupplyInitialFeeSetAch(DcsDomainService domainService)
            : base(domainService) {
        }

        internal void InsertCustomerSupplyInitialFeeSetValidate(CustomerSupplyInitialFeeSet customerSupplyInitialFeeSet) {
            var userInfo = Utils.GetCurrentUserInfo();
            customerSupplyInitialFeeSet.CreatorId = userInfo.Id;
            customerSupplyInitialFeeSet.CreatorName = userInfo.Name;
            customerSupplyInitialFeeSet.CreateTime = DateTime.Now;
        }

        internal void UpdateCustomerSupplyInitialFeeSetValidate(CustomerSupplyInitialFeeSet customerSupplyInitialFeeSet) {
            var userInfo = Utils.GetCurrentUserInfo();
            customerSupplyInitialFeeSet.ModifierId = userInfo.Id;
            customerSupplyInitialFeeSet.ModifierName = userInfo.Name;
            customerSupplyInitialFeeSet.ModifyTime = DateTime.Now;
        }

        public void InsertCustomerSupplyInitialFeeSet(CustomerSupplyInitialFeeSet customerSupplyInitialFeeSet) {
            InsertToDatabase(customerSupplyInitialFeeSet);
            this.InsertCustomerSupplyInitialFeeSetValidate(customerSupplyInitialFeeSet);
        }

        public void UpdateCustomerSupplyInitialFeeSet(CustomerSupplyInitialFeeSet customerSupplyInitialFeeSet) {
            UpdateToDatabase(customerSupplyInitialFeeSet);
            this.UpdateCustomerSupplyInitialFeeSetValidate(customerSupplyInitialFeeSet);
        }
    }

    partial class DcsDomainService {

        public void InsertCustomerSupplyInitialFeeSet(CustomerSupplyInitialFeeSet customerSupplyInitialFeeSet) {
            new CustomerSupplyInitialFeeSetAch(this).InsertCustomerSupplyInitialFeeSet(customerSupplyInitialFeeSet);
        }

        public void UpdateCustomerSupplyInitialFeeSet(CustomerSupplyInitialFeeSet customerSupplyInitialFeeSet) {
            new CustomerSupplyInitialFeeSetAch(this).UpdateCustomerSupplyInitialFeeSet(customerSupplyInitialFeeSet);
        }
    }
}
