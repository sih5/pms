﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using System.Text;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DealerPartsOutboundBillAch : DcsSerivceAchieveBase {

        public DealerPartsOutboundBillAch(DcsDomainService domainService)
            : base(domainService) {
        }
        public IEnumerable<DealerPartsOutboundBill> GetDealerPartsOutboundBills(string code, string outInBandtype, string sparePartCode, string sparePartName, string dealerName, DateTime? bOutBoundTime, DateTime? eOutBoundTime, string marketingDepartmentName, string centerName) {
            string SQL = @"select yy.*, rownum
                              from (select po.Code,
                                           cast('零售订单' as varchar2(50)) as OutInBandtype,
                                           cast('零售客户' as varchar2(50)) as CustomerType,
                                           polist.PartsId,
                                           polist.PartsCode,
                                           polist.PartsName,
                                           polist.Quantity,
                                           po.ApproveTime,
                                           po.DealerId,
                                           polist.groupcode as groupname,
                                           sp.ReferenceCode,
                                           polist.RetailGuidePrice as returnprice,
                                           polist.SalesPrice as OriginalPrice,
                                           polist.SalesPrice as CostPrice,
                                           cp.code as DealerCode,
                                           cp.name as DealerName,
                                           md.name as MarketingDepartmentName,
                                           cy.name as CenterName,
                                           cy.code as CenterCode,
                                           po.Customer,
                                           po.approvetime as OutBoundTime
                                      from DealerPartsRetailOrder po
                                      join company cp
                                        on po.dealerid = cp.id
                                      left join DealerServiceInfo ds
                                        on cp.id = ds.dealerid
                                       and ds.Status = 1
                                      left join MarketingDepartment md
                                        on md.id = ds.marketingdepartmentid
                                      left join AgencyDealerRelation ar
                                        on ar.dealerid = po.dealerid
                                       and ar.status = 1
                                      left join company cy
                                        on ar.agencyid = cy.id
                                     inner join DealerRetailOrderDetail polist
                                        on po.id = polist.dealerpartsretailorderid
                                      join sparepart sp
                                        on polist.PartsId = sp.id
                                     where po.Status = 2 --已审核
                                       {0}
                                       
                                    union all
                                    select prlist.partssalesordercode as code,
                                           cast('销售退货单' as varchar2(50)) as OutInBandtype,
                                           cast('中心库' as varchar2(50)) as CustomerType,
                                           prlist.sparepartid as PartsId,
                                           prlist.sparepartcode as PartsCode,
                                           prlist.sparepartname as PartsName,
                                           prlist.ApproveQuantity as Quantity,
                                           pr.ApproveTime,
                                           pr.SubmitCompanyId as dealerid,
                                           psd.PriceTypeName as groupname,
                                           sp.ReferenceCode,
                                           prlist.returnprice,
                                           prlist.returnprice as OriginalPrice,
                                           prlist.OriginalOrderPrice as CostPrice,
                                           cp.code as DealerCode,
                                           cp.name as DealerName,
                                           md.name as MarketingDepartmentName,
                                           cy.name as CenterName,
                                           cy.code as CenterCode,
                                           pr.ReturnCompanyName as Customer,
                                           pr.approvetime as OutBoundTime
                                      from PartsSalesReturnBill pr
                                      join company cp
                                        on pr.submitcompanyid = cp.id
                                       and cp.type in (2, 7)
                                      left join DealerServiceInfo ds
                                        on cp.id = ds.dealerid
                                       and ds.Status = 1
                                      left join MarketingDepartment md
                                        on md.id = ds.marketingdepartmentid
                                      left join AgencyDealerRelation ar
                                        on cp.id = ar.dealerid
                                       and ar.status = 1
                                      left join company cy
                                        on ar.agencyid = cy.id
                                     inner join PartsSalesReturnBillDetail prlist
                                        on pr.id = prlist.partssalesreturnbillid
                                      join sparepart sp
                                        on prlist.sparepartid = sp.id
                                      join partssalesorderdetail psd
                                   on prlist.sparepartid = psd.sparepartid
                                   and psd.partssalesorderid = prlist.partssalesorderid
                                       /* left join (select psd.PriceTypeName,
                                                       pd.partssalesordercode,
                                                       pd.sparepartid,
                                                       row_number() over(partition by pd.sparepartid, pd.partssalesordercode order by psd.PriceTypeName desc) as ydy
                                                  from PartsSalesReturnBillDetail pd
                                                  join PartsSalesOrder pso
                                                    on pd.partssalesorderid = pso.id
                                                  join partssalesorderdetail psd
                                                    on pso.id = psd.partssalesorderid
                                                   and psd.sparepartid = pd.sparepartid
                                                 where pso.CustomerType = 2) pp
                                        on prlist.partssalesordercode = pp.partssalesordercode
                                       and prlist.sparepartid = pp.sparepartid
                                       and ydy = 1 */
                                     where pr.Status = 6 --审批完成
                                        {1}                                      
                                    union all
                                    select dp.ClaimCode as code,
                                           cast('索赔领料' as varchar2(50)) as OutInBandtype,
                                           cast('索赔领料' as varchar2(50)) as CustomerType,
                                           dp.sparepartid as partsid,
                                           dp.sparepartcode as PartsCode,
                                           dp.sparepartname as partsname,
                                           dp.quantity as Quantity,
                                           dp.createtime,
                                           dp.DealerId as dealerid,
                                           pp.GroupCode as groupname,
                                           sp.ReferenceCode,
                                           pp.RetailGuidePrice as returnprice,
                                           dp.Price as OriginalPrice,
                                           pp.SalesPrice as CostPrice,
                                           cp.code as DealerCode,
                                           cp.name as DealerName,
                                           md.name as MarketingDepartmentName,
                                           cy.name as CenterName,
                                           cy.code as CenterCode,
                                           cast('' as varchar2(50)) as Customer,
                                           dp.createtime as OutBoundTime
                                      from DealerClaimSpareParts dp
                                      left join (select pst.GroupCode,
                                                       do.SparePartId,
                                                       do.DealerId,
                                                       pst.RetailGuidePrice,
                                                       pst.SalesPrice,
                                                       row_number() over(partition by do.SparePartId, do.DealerId order by pst.GroupCode desc) as ydy
                                                  from DealerClaimSpareParts do
                                                  left join PostSaleClaims_Tmp pst
                                                    on do.dealercode = pst.dealer_code
                                                   and pst.claimbill_code = do.ClaimCode
                                                   and do.SparePartCode = pst.SparePart_Code) pp
                                        on pp.SparePartId = dp.sparepartid
                                       and pp.DealerId = dp.DealerId
                                       and pp.ydy = 1
                                      join sparepart sp
                                        on dp.sparepartid = sp.id
                                      join company cp
                                        on dp.dealerid = cp.id
                                      left join DealerServiceInfo ds
                                        on dp.dealerid = ds.dealerid
                                       and ds.Status = 1
                                      left join MarketingDepartment md
                                        on md.id = ds.marketingdepartmentid
                                      left join AgencyDealerRelation ar
                                        on cp.id = ar.dealerid
                                       and ar.status = 1
                                      left join company cy
                                        on ar.agencyid = cy.id
                                     where 1=1 {2}  ) yy";
            var serachSQL = new StringBuilder();
            var serachSQL1 = new StringBuilder();
            var serachSQL2 = new StringBuilder();
            if(!string.IsNullOrEmpty(code)) {
                serachSQL.Append(" and LOWER(po.code) like '%" + code.ToLower() + "%'");
                serachSQL1.Append(" and LOWER(prlist.partssalesordercode) like '%" + code.ToLower() + "%'");
                serachSQL2.Append(" and LOWER(dp.ClaimCode) like '%" + code.ToLower() + "%'");
            }
            if(!string.IsNullOrEmpty(outInBandtype)) {
                serachSQL.Append(" and '零售订单' like '%" + outInBandtype.ToLower() + "%'");
                serachSQL1.Append(" and '销售退货单' like '%" + outInBandtype.ToLower() + "%'");
                serachSQL2.Append(" and '索赔领料' like '%" + outInBandtype.ToLower() + "%'");
            }
            if(!string.IsNullOrEmpty(sparePartCode)) {
                serachSQL.Append(" and LOWER(sp.code) like '%" + sparePartCode.ToLower() + "%'");
                serachSQL1.Append(" and LOWER(sp.code) like '%" + sparePartCode.ToLower() + "%'");
                serachSQL2.Append(" and LOWER(sp.code) like '%" + sparePartCode.ToLower() + "%'");
            }
            if(!string.IsNullOrEmpty(sparePartName)) {
                serachSQL.Append(" and LOWER(sp.name) like '%" + sparePartName.ToLower() + "%'");
                serachSQL1.Append(" and LOWER(sp.name) like '%" + sparePartName.ToLower() + "%'");
                serachSQL2.Append(" and LOWER(sp.name) like '%" + sparePartName.ToLower() + "%'");
            }
            if(!string.IsNullOrEmpty(dealerName)) {
                serachSQL.Append(" and LOWER(cp.name) like '%" + dealerName.ToLower() + "%'");
                serachSQL1.Append(" and LOWER(cp.name) like '%" + dealerName.ToLower() + "%'");
                serachSQL2.Append(" and LOWER(cp.name) like '%" + dealerName.ToLower() + "%'");
            }
            if(!string.IsNullOrEmpty(marketingDepartmentName)) {
                serachSQL.Append(" and LOWER(md.name) like '%" + marketingDepartmentName.ToLower() + "%'");
                serachSQL1.Append(" and LOWER(md.name) like '%" + marketingDepartmentName.ToLower() + "%'");
                serachSQL2.Append(" and LOWER(md.name) like '%" + marketingDepartmentName.ToLower() + "%'");
            }
            if(!string.IsNullOrEmpty(centerName)) {
                serachSQL.Append(" and LOWER(cy.name) like '%" + centerName.ToLower() + "%'");
                serachSQL1.Append(" and LOWER(cy.name) like '%" + centerName.ToLower() + "%'");
                serachSQL2.Append(" and LOWER(cy.name) like '%" + centerName.ToLower() + "%'");
            }
            if(bOutBoundTime.HasValue && bOutBoundTime.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
                bOutBoundTime = null;
            }
            if(eOutBoundTime.HasValue && eOutBoundTime.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
                eOutBoundTime = null;
            }
            if(bOutBoundTime.HasValue) {
                serachSQL.Append(" and po.ApproveTime>=to_date('" + bOutBoundTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')");
                serachSQL1.Append(" and pr.ApproveTime>=to_date('" + bOutBoundTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')");
                serachSQL2.Append(" and dp.CreateTime>=to_date('" + bOutBoundTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')");
            }
            if(eOutBoundTime.HasValue) {
                serachSQL.Append(" and po.ApproveTime<=to_date('" + eOutBoundTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')");
                serachSQL1.Append(" and pr.ApproveTime<=to_date('" + eOutBoundTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')");
                serachSQL2.Append(" and dp.CreateTime<=to_date('" + eOutBoundTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')");
            }
            //查询当前登录人所属公司性质，若是代理库，则只查询其下属区域内的订单
            var userInfo = Utils.GetCurrentUserInfo();

            var company = ObjectContext.Companies.Where(r => r.Id == userInfo.EnterpriseId).FirstOrDefault();
            if(company.Type != (int)DcsCompanyType.分公司) {
                if(company.Type == (int)DcsCompanyType.代理库) {
                    serachSQL.Append(" and cy.code='" + userInfo.EnterpriseCode + "'");
                    serachSQL1.Append(" and cy.code='" + userInfo.EnterpriseCode + "'");
                    serachSQL2.Append(" and cy.code ='" + userInfo.EnterpriseCode + "'");
                } else {
                    serachSQL.Append(" and cp.id =" + userInfo.EnterpriseId );
                    serachSQL1.Append(" and cp.id =" + userInfo.EnterpriseId);
                    serachSQL2.Append(" and cp.id=" + userInfo.EnterpriseId);
                }
            }
            SQL = string.Format(SQL, serachSQL, serachSQL1, serachSQL2);

            var search = ObjectContext.ExecuteStoreQuery<DealerPartsOutboundBill>(SQL).ToList();
            return search;
        }
    }
    partial class DcsDomainService {
        public IEnumerable<DealerPartsOutboundBill> GetDealerPartsOutboundBills(string code, string outInBandtype, string sparePartCode, string sparePartName, string dealerName, DateTime? bOutBoundTime, DateTime? eOutBoundTime, string marketingDepartmentName, string centerName) {
            return new DealerPartsOutboundBillAch(this).GetDealerPartsOutboundBills(code, outInBandtype, sparePartCode, sparePartName, dealerName, bOutBoundTime, eOutBoundTime, marketingDepartmentName, centerName);
        }
    }
}
