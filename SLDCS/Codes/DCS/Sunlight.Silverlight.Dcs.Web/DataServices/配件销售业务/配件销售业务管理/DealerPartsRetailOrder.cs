﻿using System;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DealerPartsRetailOrderAch : DcsSerivceAchieveBase {
        internal void InsertDealerPartsRetailOrderValidate(DealerPartsRetailOrder dealerPartsRetailOrder) {
            if(string.IsNullOrWhiteSpace(dealerPartsRetailOrder.Code) || dealerPartsRetailOrder.Code == GlobalVar.ASSIGNED_BY_SERVER)
                dealerPartsRetailOrder.Code = CodeGenerator.Generate("DealerPartsRetailOrder", dealerPartsRetailOrder.DealerCode);
            var userInfo = Utils.GetCurrentUserInfo();
            dealerPartsRetailOrder.CreatorId = userInfo.Id;
            dealerPartsRetailOrder.CreatorName = userInfo.Name;
            dealerPartsRetailOrder.CreateTime = DateTime.Now;
        }

        internal void UpdateDealerPartsRetailOrderValidate(DealerPartsRetailOrder dealerPartsRetailOrder) {
            var userInfo = Utils.GetCurrentUserInfo();
            dealerPartsRetailOrder.ModifierId = userInfo.Id;
            dealerPartsRetailOrder.ModifierName = userInfo.Name;
            dealerPartsRetailOrder.ModifyTime = DateTime.Now;
        }


        public void InsertPartsRetailOrder(DealerPartsRetailOrder dealerPartsRetailOrder) {
            InsertToDatabase(dealerPartsRetailOrder);
            var dealerRetailOrderDetails = ChangeSet.GetAssociatedChanges(dealerPartsRetailOrder, r => r.DealerRetailOrderDetails, ChangeOperation.Insert);
            foreach(DealerRetailOrderDetail dealerRetailOrderDetail in dealerRetailOrderDetails) {
                InsertToDatabase(dealerRetailOrderDetail);
            }
            this.InsertDealerPartsRetailOrderValidate(dealerPartsRetailOrder);

        }

        public void UpdateDealerPartsRetailOrder(DealerPartsRetailOrder dealerPartsRetailOrder) {
            //删除与强绑定清单的关系
            dealerPartsRetailOrder.DealerRetailOrderDetails.Clear();
            UpdateToDatabase(dealerPartsRetailOrder);
            var dealerRetailOrderDetails = ChangeSet.GetAssociatedChanges(dealerPartsRetailOrder, r => r.DealerRetailOrderDetails);
            foreach(DealerRetailOrderDetail dealerRetailOrderDetail in dealerRetailOrderDetails) {
                //判断更新的操作
                switch(ChangeSet.GetChangeOperation(dealerRetailOrderDetail)) {
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(dealerRetailOrderDetail);
                        break;
                    case ChangeOperation.Insert:
                        InsertToDatabase(dealerRetailOrderDetail);
                        break;
                    case ChangeOperation.Update:
                        UpdateToDatabase(dealerRetailOrderDetail);
                        break;
                }
            }
            this.UpdateDealerPartsRetailOrderValidate(dealerPartsRetailOrder);
        }

        public IQueryable<DealerPartsRetailOrder> GetPartsRetailOrdersWithPartsSalesCategory() {
            return ObjectContext.DealerPartsRetailOrders.Include("PartsSalesCategory").OrderBy(e => e.Id);
        }

        public IQueryable<DealerPartsRetailOrder> GetPartsRetailOrderWithDetails() {
            return ObjectContext.DealerPartsRetailOrders.Include("DealerRetailOrderDetails").Include("Dealer").OrderBy(e => e.Id);
        }

        public IQueryable<DealerPartsRetailOrder> 查询二级站配件零售订单() {
            var userInfo = Utils.GetCurrentUserInfo();
            IQueryable<DealerPartsRetailOrder> result = ObjectContext.DealerPartsRetailOrders.Where(r => ObjectContext.PersonSubDealers.Any(v => v.SubDealerId == r.SubDealerId && v.PersonId == userInfo.Id)).Include("PartsRetailOrderDetails");
            return result.OrderBy(r => r.Id);
        }
        public IQueryable<DealerPartsRetailOrder> GetDealerPartsRetailOrdersBySubDealerId() {
            var userInfo = Utils.GetCurrentUserInfo();
            var subDealerId = ObjectContext.PersonSubDealers.OrderBy(e => e.Id).Where(e => e.PersonId == userInfo.Id).Select(e => e.SubDealerId).ToArray();
            var dealerPartsRetailOrder = ObjectContext.DealerPartsRetailOrders.Include("DealerRetailOrderDetails").Include("Dealer").OrderBy(e => e.Id).Where(e => e.SubDealerlOrderType == (int)DcsDealerPartsRetailOrderSubDealerlOrderType.配件零售);
            var dea = from dealerpartsretailorder in dealerPartsRetailOrder
                      join subdealerid in subDealerId
                      on dealerpartsretailorder.SubDealerId equals subdealerid
                      select dealerpartsretailorder;
            return dea;
        }
        public IQueryable<DealerPartsRetailOrder> GetPartsRetailOrderBySubDealerlOrderType() {
            return ObjectContext.DealerPartsRetailOrders.Include("DealerRetailOrderDetails").Include("Dealer").OrderBy(e => e.Id).Where(e => e.SubDealerlOrderType == 0);
        }
        public IQueryable<DealerPartsRetailOrderEx> GetPartsRetailOrderWithDetailsForExport() {
            var result = from a in ObjectContext.DealerPartsRetailOrders.Where(e => e.SubDealerlOrderType == 0)
                         join b in ObjectContext.DealerRetailOrderDetails on a.Id equals b.DealerPartsRetailOrderId
                         select new DealerPartsRetailOrderEx {
                             Id = a.Id,
                             Code = a.Code,
                             RetailOrderType = a.RetailOrderType,
                             BranchId = a.BranchId,
                             BranchName = a.BranchName,
                             DealerId = a.DealerId,
                             DealerCode = a.DealerCode,
                             DealerName = a.DealerName,
                             CustomerUnit= a.CustomerUnit,
                             SubDealerCode = a.SubDealerCode,
                             SubDealerName = a.SubDealerName,
                             SubDealerlOrderType = a.SubDealerlOrderType,
                             ApprovalComment = a.ApprovalComment,
                             Customer = a.Customer,
                             CustomerPhone = a.CustomerPhone,
                             CustomerCellPhone = a.CustomerCellPhone,
                             Address = a.Address,
                             PartsSalesCategoryId = a.PartsSalesCategoryId,
                             SalesCategoryName = a.SalesCategoryName,
                             TotalAmount = a.TotalAmount,
                             Remark = a.Remark,
                             CreatorName = a.CreatorName,
                             CreateTime = a.CreateTime,
                             ModifierName = a.ModifierName,
                             ModifyTime = a.ModifyTime,
                             ApproverName = a.ApproverName,
                             ApproveTime = a.ApproveTime,
                             AbandonerName = a.AbandonerName,
                             AbandonTime = a.AbandonTime,
                             Status = a.Status,
                             SerialNumber = b.SerialNumber,
                             PartsCode = b.PartsCode,
                             PartsName = b.PartsName,
                             Quantity = b.Quantity,
                             Price = b.Price,
                             DetailRemark = b.Remark,
                             TraceProperty=b.TraceProperty,
                             SIHLabelCode=b.SIHLabelCode,
                             RetailGuidePrice=b.RetailGuidePrice,
                             SalesPrice=b.SalesPrice,
                             GroupCode=b.GroupCode
                         };
            return result.OrderBy(e => e.Id);
        }
        public IQueryable<DealerPartsRetailOrder> GetDealerPartsRetailOrderBySubDealerlOrderType() {
            var dealerPartsRetailOrder = ObjectContext.DealerPartsRetailOrders.Include("DealerRetailOrderDetails").Include("Dealer").OrderBy(e => e.Id).Where(e => e.SubDealerlOrderType == (int)DcsDealerPartsRetailOrderSubDealerlOrderType.配件零售);
            return dealerPartsRetailOrder;
        }

        public IQueryable<DealerPartsRetailOrder> 查询配件索赔服务站零售单(int partid, int dealerId) {
            if (dealerId == 0) {
                dealerId = Utils.GetCurrentUserInfo().EnterpriseId;
            }
            var dealerPartsRetailOrder = ObjectContext.DealerPartsRetailOrders.Where(r => ObjectContext.DealerRetailOrderDetails.Any(d => d.DealerPartsRetailOrderId == r.Id && d.PartsId == partid) && r.DealerId == dealerId && r.Status == (int)DcsWorkflowOfSimpleApprovalStatus.已审核);
            return dealerPartsRetailOrder;
        }
        public IQueryable<DealerRetailOrderDetail> 服务站零售订单退货查询(int? dealerPartsRetailOrderId) {
            return ObjectContext.DealerRetailOrderDetails.Include("DealerPartsRetailOrder").Where(r => r.DealerPartsRetailOrderId == dealerPartsRetailOrderId).OrderBy(r => r.id);
        }
        public IQueryable<DealerPartsRetailOrder> 服务站零售订单退货查询主单(string partsCode) {
            var nowDate = DateTime.Now.Date.AddDays(-3);
            if(!string.IsNullOrEmpty(partsCode)) {
                return ObjectContext.DealerPartsRetailOrders.Where(r => ObjectContext.DealerRetailOrderDetails.Any(t => t.PartsCode.Contains(partsCode) && t.DealerPartsRetailOrderId == r.Id) && r.ApproveTime>=nowDate).OrderBy(p => p.Code);
            } else {
                return ObjectContext.DealerPartsRetailOrders.Where(t=>t.ApproveTime>=nowDate).OrderBy(p => p.Code); ;
            }
        }
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               
        

        public void InsertPartsRetailOrder(DealerPartsRetailOrder dealerPartsRetailOrder) {
            new DealerPartsRetailOrderAch(this).InsertPartsRetailOrder(dealerPartsRetailOrder);
        }

        public void UpdateDealerPartsRetailOrder(DealerPartsRetailOrder dealerPartsRetailOrder) {
            new DealerPartsRetailOrderAch(this).UpdateDealerPartsRetailOrder(dealerPartsRetailOrder);
        }

        public IQueryable<DealerPartsRetailOrder> GetPartsRetailOrdersWithPartsSalesCategory() {
            return new DealerPartsRetailOrderAch(this).GetPartsRetailOrdersWithPartsSalesCategory();
        }

        public IQueryable<DealerPartsRetailOrder> GetPartsRetailOrderWithDetails() {
            return new DealerPartsRetailOrderAch(this).GetPartsRetailOrderWithDetails();
        }
        public IQueryable<DealerPartsRetailOrderEx> GetPartsRetailOrderWithDetailsForExport() { 
            return new DealerPartsRetailOrderAch(this).GetPartsRetailOrderWithDetailsForExport();
        }
        public IQueryable<DealerPartsRetailOrder> 查询二级站配件零售订单() {
            return new DealerPartsRetailOrderAch(this).查询二级站配件零售订单();
        }
                public IQueryable<DealerPartsRetailOrder> GetDealerPartsRetailOrdersBySubDealerId() {
            return new DealerPartsRetailOrderAch(this).GetDealerPartsRetailOrdersBySubDealerId();
        }
                public IQueryable<DealerPartsRetailOrder> GetPartsRetailOrderBySubDealerlOrderType() {
            return new DealerPartsRetailOrderAch(this).GetPartsRetailOrderBySubDealerlOrderType();
        }

        public IQueryable<DealerPartsRetailOrder> GetDealerPartsRetailOrderBySubDealerlOrderType() {
            return new DealerPartsRetailOrderAch(this).GetDealerPartsRetailOrderBySubDealerlOrderType();
        }

        public IQueryable<DealerPartsRetailOrder> 查询配件索赔服务站零售单(int partid, int dealerId) {
            return new DealerPartsRetailOrderAch(this).查询配件索赔服务站零售单(partid,dealerId);
        }
        public IQueryable<DealerRetailOrderDetail> 服务站零售订单退货查询(int? dealerPartsRetailOrderId) {
            return new DealerPartsRetailOrderAch(this).服务站零售订单退货查询(dealerPartsRetailOrderId);
        }
        public IQueryable<DealerPartsRetailOrder> 服务站零售订单退货查询主单(string partsCode) {
            return new DealerPartsRetailOrderAch(this).服务站零售订单退货查询主单(partsCode);
        }
    }
}
