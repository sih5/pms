﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
using System.Collections.Generic;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DealerPartsSalesReturnBillAch : DcsSerivceAchieveBase {
        internal void InsertDealerPartsSalesReturnBillValidate(DealerPartsSalesReturnBill partsRetailReturnBill) {
            var userInfo = Utils.GetCurrentUserInfo();
            partsRetailReturnBill.CreatorId = userInfo.Id;
            partsRetailReturnBill.CreatorName = userInfo.Name;
            partsRetailReturnBill.CreateTime = DateTime.Now;
            if(string.IsNullOrWhiteSpace(partsRetailReturnBill.Code) || partsRetailReturnBill.Code == GlobalVar.ASSIGNED_BY_SERVER)
                partsRetailReturnBill.Code = CodeGenerator.Generate("DealerPartsSalesReturnBill", partsRetailReturnBill.DealerCode);
        }

        internal void UpdateDealerPartsSalesReturnBillValidate(DealerPartsSalesReturnBill partsRetailReturnBill) {
            var userInfo = Utils.GetCurrentUserInfo();
            partsRetailReturnBill.ModifierId = userInfo.Id;
            partsRetailReturnBill.ModifierName = userInfo.Name;
            partsRetailReturnBill.ModifyTime = DateTime.Now;
        }

        public void InsertDealerPartsSalesReturnBill(DealerPartsSalesReturnBill partsRetailReturnBill) {
            InsertToDatabase(partsRetailReturnBill);
            this.InsertDealerPartsSalesReturnBillValidate(partsRetailReturnBill);
        }

        public void UpdateDealerPartsSalesReturnBill(DealerPartsSalesReturnBill partsRetailReturnBill) {
            UpdateToDatabase(partsRetailReturnBill);
            this.UpdateDealerPartsSalesReturnBillValidate(partsRetailReturnBill);
        }
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               

        public void InsertDealerPartsSalesReturnBill(DealerPartsSalesReturnBill partsRetailReturnBill) {
            new DealerPartsSalesReturnBillAch(this).InsertDealerPartsSalesReturnBill(partsRetailReturnBill);
        }

        public void UpdateDealerPartsSalesReturnBill(DealerPartsSalesReturnBill partsRetailReturnBill) {
            new DealerPartsSalesReturnBillAch(this).UpdateDealerPartsSalesReturnBill(partsRetailReturnBill);
        }

    }
}

