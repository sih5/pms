﻿using System;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    class DealerRetailReturnBillDetailAch : DcsSerivceAchieveBase {
        public DealerRetailReturnBillDetailAch(DcsDomainService domainService)
            : base(domainService) {
        }
        public void InsertDealerRetailReturnBillDetail(DealerRetailReturnBillDetail partsRetailReturnBill) {
         //   InsertToDatabase(partsRetailReturnBill);
        }
        public void UpdateDealerRetailReturnBillDetail(DealerRetailReturnBillDetail partsRetailReturnBill) {
            UpdateToDatabase(partsRetailReturnBill);
        }
    }
    partial class DcsDomainService {
        public void InsertDealerRetailReturnBillDetail(DealerRetailReturnBillDetail partsRetailReturnBill) {
            new DealerRetailReturnBillDetailAch(this).InsertDealerRetailReturnBillDetail(partsRetailReturnBill);
        }
        public void UpdateDealerRetailReturnBillDetail(DealerRetailReturnBillDetail partsRetailReturnBill) {
            new DealerRetailReturnBillDetailAch(this).UpdateDealerRetailReturnBillDetail(partsRetailReturnBill);
        }
    }
}
