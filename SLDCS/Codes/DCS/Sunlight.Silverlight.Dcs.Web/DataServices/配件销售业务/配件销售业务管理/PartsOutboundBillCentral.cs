﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
using System.Configuration;
using System.Data;
using System.Text;
using Devart.Data.Oracle;
using NPOI.SS.Formula.Functions;
using System.Transactions;
using System.Data.Objects.SqlClient;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsOutboundBillCentralAch : DcsSerivceAchieveBase {
        public PartsOutboundBillCentralAch(DcsDomainService domainService)
            : base(domainService) {
        }
        public IEnumerable<PartsOutboundBillCentral> GetPartsOutboundBillCentrals(DateTime? bShippingDate, DateTime? eShippingDate, string companyCode, string companyName, string code, string sparePartCode, string referenceCode, string partsSalesOrderCode, string submitCompanyName, int? customerTypeInt, int? customerTypeTy)
        {
            string SQL = @"select *
  from (select pd.id,
               pb.id as partsoutboundbillid,
               (case
                 when pb.storagecompanytype = 3 then
                  (select md.name
                     from agency ag
                     join MarketingDepartment md
                       on ag.marketingdepartmentid = md.id
                    where pb.StorageCompanyId = ag.id)
                 else
                  (select mk.name
                     from DealerServiceInfo ds
                     left join MarketingDepartment mk
                       on ds.marketingdepartmentid = mk.id
                    where ds.dealerid = pb.storagecompanyid)
               end) as MarketingDepartmentName,
               pb.StorageCompanyName as CompanyName,
               pb.StorageCompanyCode as CompanyCode,
               pb.WarehouseName,
               cp.ProvinceName as province,
               pb.CounterpartCompanyCode as submitcompanycode,
               pb.CounterpartCompanyName as submitcompanyname,
               (case
                 when cp.type is null then
                  cast('零售' as varchar2(100))
                 when cp.type = 2 then
                  (select c.name
                     from ChannelCapability c
                     join DealerServiceInfo d
                       on c.id = d.ChannelCapabilityId
                    where d.dealerid = pb.CounterpartCompanyId)
                 else
                  kv.value
               end) as CustomerType,
               (select code
                  from PartsSalesOrder ps
                 where ps.id = pb.originalrequirementbillId
                   and pb.originalrequirementbilltype = 1) as PartsSalesOrderCode,
               (select max(a.code)
                  from PartsSalesOrderProcess a
                  join PartsSalesOrderProcessDetail b
                    on a.id = b.partssalesorderprocessid
                  join PartsSalesOrder ps
                    on a.originalsalesorderid = ps.id
                 where b.sparepartid = pd.sparepartid
                   and ps.id = pb.originalrequirementbillId
                   and pb.originalrequirementbilltype = 1) as PartsSalesOrderProcessCode,
               pb.code,
               sp.referencecode,
               pd.sparepartcode,
               pd.sparepartname,
               pb.PartsSalesOrderTypeName as partssalesordertypename,
               ph.partabc,
               pd.outboundamount,
               pd.costprice,
               pd.costprice * pd.outboundamount as CostPriceAmount,
               nvl(prod.salesprice, pd.originalprice) as originalprice,
               nvl(prod.salesprice, pd.originalprice) * pd.outboundamount as OriginalPriceAmount,
               (nvl(prod.salesprice, pd.originalprice) * pd.outboundamount -
               pd.settlementprice * pd.outboundamount) as ConcessionPrice,
                (case
                 when cp.type is null then
                  prod.delaerprice
               
                 else
                  pd.settlementprice
               end) as settlementprice,
               (case
                 when cp.type is null then
                  prod.delaerprice
               
                 else
                  pd.settlementprice
               end) * pd.outboundamount as SettlementPriceAmount,
               round((case
                       when (nvl(prod.salesprice, pd.originalprice) *
                            pd.outboundamount) > 0 then
                        (nvl(prod.salesprice, pd.originalprice) * pd.outboundamount -
                        pd.settlementprice * pd.outboundamount) /
                        (nvl(prod.salesprice, pd.originalprice) * pd.outboundamount) * 100
                       else
                        0
                     end),
                     2) as ConcessionRate,
               (select ShippingDate
                  from partsshippingorder ps
                 where ps.id = pb.PartsShippingOrderId) as ShippingDate,
               (select sum(QuantityToSettle)
                  from PartsSalesSettlementDetail psd
                  join PartsSalesSettlementRef pf
                    on pf.PartsSalesSettlementId = psd.partssalessettlementid
                 where psd.sparepartid = pd.sparepartid
                   and pf.SourceId = pb.id
                   and pf.sourcecode = pb.code) as QuantityToSettle,
               pb.remark,
               pb.settlementstatus,
               pr.groupname,
               pb.StorageCompanyType as CustomerTypeInt
          from partsoutboundbilldetail pd
         inner join partsoutboundbill pb
            on pd.partsoutboundbillid = pb.id
          left join company cp
            on pb.CounterpartCompanyId = cp.id
          left join keyvalueitem kv
            on cp.type = kv.key
           and kv.name = 'Company_Type'
          join sparepart sp
            on pd.sparepartid = sp.id
          join partsbranch ph
            on pd.sparepartid = ph.partid
           and ph.status = 1
           and pb.branchid = ph.branchid
          left join PartsSalePriceIncreaseRate pr
            on ph.IncreaseRateGroupId = pr.id
          left join PartsRetailOrder pro
            on pb.originalrequirementbillcode = pro.code
          left join PartsRetailOrderDetail prod
            on pro.id = prod.partsretailorderid
           and pd.sparepartid = prod.sparepartid
         where pb.StorageCompanyType in (3, 7) ";
            if(!string.IsNullOrEmpty(code)) {
                SQL = SQL + " and LOWER(pb.code) like '%" + code.ToLower() + "%'";
            }
            if(!string.IsNullOrEmpty(referenceCode)) {
                SQL = SQL + " and LOWER(sp.referenceCode) like '%" + referenceCode.ToLower() + "%'";
            }
            if(customerTypeInt.HasValue) {
                SQL = SQL + " and pb.StorageCompanyType=" + customerTypeInt + "";
           }
            if(!string.IsNullOrEmpty(submitCompanyName)) {
                SQL = SQL + " and LOWER(pb.CounterpartCompanyName) like '%" + submitCompanyName.ToLower() + "%'";
            }
            if(!string.IsNullOrEmpty(sparePartCode)) {
                SQL = SQL + " and LOWER(pd.sparePartCode) like '%" + sparePartCode.ToLower() + "%'";
            }
            if(!string.IsNullOrEmpty(companyCode)) {
                SQL = SQL + " and LOWER(pb.StorageCompanyCode) like '%" + companyCode.ToLower() + "%'";
            }
            if(!string.IsNullOrEmpty(companyName)) {
                SQL = SQL + " and LOWER(pb.StorageCompanyName) like '%" + companyName.ToLower() + "%'";
            }
            if(customerTypeTy.HasValue){
                if(customerTypeTy.Value==(int)DcsCompanyType.分公司){
                    SQL = SQL + " and cp.type=" + (int)DcsCompanyType.分公司;
                }
                if (customerTypeTy.Value == 2)
                {
                    SQL = SQL + " and cp.type=" + (int)DcsCompanyType.代理库;
                }
                if (customerTypeTy.Value == 3)
                {
                    SQL = SQL + " and cp.type=" + (int)DcsCompanyType.服务站 + @" and   (select c.name
                     from ChannelCapability c
                     join DealerServiceInfo d
                       on c.id = d.ChannelCapabilityId
                    where d.dealerid = pb.CounterpartCompanyId)='服务站'";
                }
                if (customerTypeTy.Value == 4)
                {
                    SQL = SQL + " and cp.type=" + (int)DcsCompanyType.服务站兼代理库;
                }
                if (customerTypeTy.Value == 5)
                {
                    SQL = SQL + " and cp.type=" + (int)DcsCompanyType.服务站 + @" and   (select c.name
                     from ChannelCapability c
                     join DealerServiceInfo d
                       on c.id = d.ChannelCapabilityId
                    where d.dealerid = pb.CounterpartCompanyId)='经销商'";
                }
                if (customerTypeTy.Value == 6)
                {
                    SQL = SQL + " and cp.type is null "  ;
                }
            }
           
            if(bShippingDate.HasValue && bShippingDate.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
                bShippingDate = null;
            }
            //查询当前登录人所属公司性质，若是代理库，则只查询其下属区域内的订单
            var userInfo = Utils.GetCurrentUserInfo();

            var company = ObjectContext.Companies.Where(r => r.Id == userInfo.EnterpriseId).FirstOrDefault();
            if(company.Type != (int)DcsCompanyType.分公司) {
                if(company.Type == (int)DcsCompanyType.代理库 || company.Type == (int)DcsCompanyType.服务站兼代理库) {
                    SQL = SQL + " and  pb.StorageCompanyId=" + userInfo.EnterpriseId;
                } else {
                    SQL = SQL + " and  pb.CounterpartCompanyId=" + userInfo.EnterpriseId;
                }
            }

            SQL = SQL + ")tt where 1=1 ";
            if(!string.IsNullOrEmpty(partsSalesOrderCode)) {
                SQL = SQL + " and LOWER(tt.PartsSalesOrderCode) like '%" + partsSalesOrderCode.ToLower() + "%'";
            }
            if(bShippingDate.HasValue) {
                SQL = SQL + " and tt.ShippingDate>= to_date('" + bShippingDate.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if(eShippingDate.HasValue) {
                SQL = SQL + " and tt.ShippingDate<= to_date('" + eShippingDate.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            var search = ObjectContext.ExecuteStoreQuery<PartsOutboundBillCentral>(SQL).ToList();
            return search;
        }
        public IEnumerable<PartsOutboundBillCentral> 采购退货统计(DateTime? bOutBoundTime, DateTime? eOutBoundTime, string partsPurReturnOrderCode, string settlementStatus, string settlementNo, string sparePartCode) {
            string SQL = @"select pr.partssuppliercode,
                           pr.partssuppliername,
                           pr.code as PartsPurReturnOrderCode,       
                           pb.createtime as OutBoundTime,
                           pb.code,
                           pb.warehousecode,
                           pb.warehousename,
                           pd.sparepartcode,
                           pd.sparepartname,
                           pd.outboundamount,
                           ph.partabc,
                           pd.costprice,
                           pd.costprice * pd.outboundamount as CostPriceAmount,
                           pd.settlementprice,
                           pd.settlementprice * pd.outboundamount as SettlementPriceAmount,
                           tt.code as SettlementNo,
                           tt.status as SettlementStatus,
                           rownum as id ,
                           pb.id as PartsOutboundBillId
                      from partsoutboundbill pb
                      join partsoutboundbilldetail pd
                        on pb.id = pd.partsoutboundbillid
                      join PartsPurReturnOrder pr
                        on pb.originalrequirementbillid = pr.id
                       and pb.originalrequirementbilltype = 5
                      left join partsbranch ph
                        on pd.sparepartid = ph.partid
                       and ph.status = 1
                       and pb.partssalescategoryid = ph.partssalescategoryid
                      left join (select pf.sourceid, pf.sourcecode, ppba.code, ppba.status
                              from PartsPurchaseSettleRef pf
                             inner join PartsPurchaseSettleBill ppba
                                on pf.partspurchasesettlebillid = ppba.id
                               and ppba.status not in (99, 4)
                             where pf.SourceType = 1
                            union all
                            select prf.sourceid, prf.sourcecode, prs.code, prs.status
                              from PartsPurchaseRtnSettleRef prf
                             inner join PartsPurchaseRtnSettleBill prs
                                on prf.PartsPurchaseRtnSettleBillId = prs.id
                               and prs.status not in (99, 4)) tt
                        on pb.id = tt.sourceid
                       and pb.code = tt.sourcecode
                     where pb.outboundtype = 2";
            if(!string.IsNullOrEmpty(partsPurReturnOrderCode)) {
                SQL = SQL + " and LOWER(pr.code) like '%" + partsPurReturnOrderCode.ToLower() + "%'";
            }           
            if(!string.IsNullOrEmpty(sparePartCode)) {
                SQL = SQL + " and LOWER(pd.sparePartCode) like '%" + sparePartCode.ToLower() + "%'";
            }           

            if(bOutBoundTime.HasValue && bOutBoundTime.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
                bOutBoundTime = null;
            }
            if(eOutBoundTime.HasValue && eOutBoundTime.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
                eOutBoundTime = null;
            }
            if(bOutBoundTime.HasValue) {
                SQL = SQL + " and pb.CreateTime>= to_date('" + bOutBoundTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if(eOutBoundTime.HasValue) {
                SQL = SQL + " and pb.CreateTime<= to_date('" + eOutBoundTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }

            if(!string.IsNullOrEmpty(settlementNo)) {
                SQL = SQL + " and LOWER(tt.code) like '%" + settlementNo.ToLower() + "%'";
            }
            if(!string.IsNullOrEmpty(settlementStatus)) {
                SQL = SQL + " and tt.status  in (" + settlementStatus+")";
            }          
            var search = ObjectContext.ExecuteStoreQuery<PartsOutboundBillCentral>(SQL).ToList();
            return search;
        }
    }
    partial class DcsDomainService {
        public IEnumerable<PartsOutboundBillCentral> GetPartsOutboundBillCentrals(DateTime? bShippingDate, DateTime? eShippingDate, string companyCode, string companyName, string code, string sparePartCode, string referenceCode, string partsSalesOrderCode, string submitCompanyName, int? customerTypeInt, int? customerTypeTy)
        {
            return new PartsOutboundBillCentralAch(this).GetPartsOutboundBillCentrals(bShippingDate, eShippingDate, companyCode, companyName, code, sparePartCode, referenceCode, partsSalesOrderCode, submitCompanyName, customerTypeInt, customerTypeTy);
        }
        public IEnumerable<PartsOutboundBillCentral> 采购退货统计(DateTime? bOutBoundTime, DateTime? eOutBoundTime, string partsPurReturnOrderCode, string settlementStatus, string settlementNo, string sparePartCode) {
            return new PartsOutboundBillCentralAch(this).采购退货统计(bOutBoundTime, eOutBoundTime, partsPurReturnOrderCode, settlementStatus, settlementNo, sparePartCode);
        }
    }
}
