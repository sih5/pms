﻿using System;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsRetailOrderAch : DcsSerivceAchieveBase {
        internal void InsertPartsRetailOrderValidate(PartsRetailOrder partsRetailOrder) {
            if(string.IsNullOrWhiteSpace(partsRetailOrder.Code) || partsRetailOrder.Code == GlobalVar.ASSIGNED_BY_SERVER)
                partsRetailOrder.Code = CodeGenerator.Generate("PartsRetailOrder", partsRetailOrder.SalesUnitOwnerCompanyCode);
            var userInfo = Utils.GetCurrentUserInfo();
            partsRetailOrder.CreatorId = userInfo.Id;
            partsRetailOrder.CreatorName = userInfo.Name;
            partsRetailOrder.CreateTime = DateTime.Now;
        }

        internal void UpdatePartsRetailOrderValidate(PartsRetailOrder partsRetailOrder) {
            var userInfo = Utils.GetCurrentUserInfo();
            partsRetailOrder.ModifierId = userInfo.Id;
            partsRetailOrder.ModifierName = userInfo.Name;
            partsRetailOrder.ModifyTime = DateTime.Now;
        }


        public void InsertPartsRetailOrder(PartsRetailOrder partsRetailOrder) {
            InsertToDatabase(partsRetailOrder);
            var partsRetailOrderDerDetails = ChangeSet.GetAssociatedChanges(partsRetailOrder, r => r.PartsRetailOrderDetails, ChangeOperation.Insert);
            foreach(PartsRetailOrderDetail partsRetailOrderDetail in partsRetailOrderDerDetails) {
                InsertToDatabase(partsRetailOrderDetail);
            }
            this.InsertPartsRetailOrderValidate(partsRetailOrder);

        }

        public void UpdatePartsRetailOrder(PartsRetailOrder partsRetailOrder) {
            //删除与强绑定清单的关系
            partsRetailOrder.PartsRetailOrderDetails.Clear();
            UpdateToDatabase(partsRetailOrder);
            var partsRetailOrderDerDetails = ChangeSet.GetAssociatedChanges(partsRetailOrder, r => r.PartsRetailOrderDetails);
            foreach(PartsRetailOrderDetail partsRetailOrderDetail in partsRetailOrderDerDetails) {
                //判断更新的操作
                switch(ChangeSet.GetChangeOperation(partsRetailOrderDetail)) {
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(partsRetailOrderDetail);
                        break;
                    case ChangeOperation.Insert:
                        InsertToDatabase(partsRetailOrderDetail);
                        break;
                    case ChangeOperation.Update:
                        UpdateToDatabase(partsRetailOrderDetail);
                        break;
                }
            }
            this.UpdatePartsRetailOrderValidate(partsRetailOrder);
        }

        public PartsRetailOrder GetPartsRetailOrderWithDetailsById(int id) {
            var dbPartsRetailOrder = ObjectContext.PartsRetailOrders.SingleOrDefault(entity => entity.Id == id);
            if(dbPartsRetailOrder != null) {
                var dbPartsRetailOrderDetails = ObjectContext.PartsRetailOrderDetails.Where(entity => entity.PartsRetailOrderId == id).ToArray();
            }
            return dbPartsRetailOrder;
        }

        public IQueryable<PartsRetailOrder> GetPartsRetailOrderForSelect(string sparePartCode) {
            if (!string.IsNullOrEmpty(sparePartCode)) {
                return ObjectContext.PartsRetailOrders.Where(r => ObjectContext.PartsRetailOrderDetails.Any(d => d.PartsRetailOrderId == r.Id && d.SparePartCode.Contains(sparePartCode))).OrderBy(r => r.Id);
            } else { 
                return ObjectContext.PartsRetailOrders.OrderBy(r => r.Id);
            }
            
        }
        public IQueryable<PartsRetailOrder> GetPartsRetailOrderByPartsOutboundBillDetails(string sparePartCode) {
            var partsOutboundBillIds = ObjectContext.PartsOutboundBills.Where(r => r.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件零售订单 && r.PartsOutboundBillDetails.Any()).Select(r => r.OriginalRequirementBillId);
            var partsRetailOrders = ObjectContext.PartsRetailOrders.Where(r => partsOutboundBillIds.Contains(r.Id));
            if (!string.IsNullOrEmpty(sparePartCode)) {
                partsRetailOrders = partsRetailOrders.Where(r => ObjectContext.PartsRetailOrderDetails.Any(d => d.PartsRetailOrderId == r.Id && d.SparePartCode.Contains(sparePartCode)));
            }
            return partsRetailOrders;
        }

        public PartsOutboundPlan GetPartsOutboundPlanById(string partsRetailOrderCode) {
            return ObjectContext.PartsOutboundPlans.SingleOrDefault(entity => entity.SourceCode == partsRetailOrderCode);
        }

        public IQueryable<PartsRetailOrder> 查询配件索赔零售订单(int salesUnitOwnerCompanyId, int partid, int salesCategoryId) {
            var partsSalesOrderQuery = from a in ObjectContext.PartsRetailOrders.Where(r => r.SalesUnitOwnerCompanyId == salesUnitOwnerCompanyId && (r.Status == (int)DcsPartsRetailOrderStatus.审批 || r.Status == (int)DcsPartsRetailOrderStatus.开票) && ObjectContext.PartsRetailOrderDetails.Any(b => r.Id == b.PartsRetailOrderId && b.SparePartId == partid) && ObjectContext.SalesUnits.Any(c => r.SalesUnitId == c.Id && c.PartsSalesCategoryId == salesCategoryId))
                                       select a;
            return partsSalesOrderQuery.OrderBy(r => r.Id);
        }

    
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               
        

        public void InsertPartsRetailOrder(PartsRetailOrder partsRetailOrder) {
            new PartsRetailOrderAch(this).InsertPartsRetailOrder(partsRetailOrder);
        }

        public void UpdatePartsRetailOrder(PartsRetailOrder partsRetailOrder) {
            new PartsRetailOrderAch(this).UpdatePartsRetailOrder(partsRetailOrder);
        }

        public PartsRetailOrder GetPartsRetailOrderWithDetailsById(int id) {
            return new PartsRetailOrderAch(this).GetPartsRetailOrderWithDetailsById(id);
        }

        public IQueryable<PartsRetailOrder> GetPartsRetailOrderByPartsOutboundBillDetails(string sparePartCode) {
            return new PartsRetailOrderAch(this).GetPartsRetailOrderByPartsOutboundBillDetails(sparePartCode);
        }

        public PartsOutboundPlan GetPartsOutboundPlanById(string partsRetailOrderCode) {
            return new PartsRetailOrderAch(this).GetPartsOutboundPlanById(partsRetailOrderCode);
        }

        public IQueryable<PartsRetailOrder> 查询配件索赔零售订单(int salesUnitOwnerCompanyId, int partid, int salesCategoryId) {
            return new PartsRetailOrderAch(this).查询配件索赔零售订单(salesUnitOwnerCompanyId,partid,salesCategoryId);
        }

        public IQueryable<PartsRetailOrder> GetPartsRetailOrderForSelect(string sparePartCode) { 
            return new PartsRetailOrderAch(this).GetPartsRetailOrderForSelect(sparePartCode);
        }
    }
}
