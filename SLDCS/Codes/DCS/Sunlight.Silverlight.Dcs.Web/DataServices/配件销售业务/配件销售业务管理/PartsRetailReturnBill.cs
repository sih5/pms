﻿using System;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsRetailReturnBillAch : DcsSerivceAchieveBase {
        internal void InsertPartsRetailReturnBillValidate(PartsRetailReturnBill partsRetailReturnBill) {
            var userInfo = Utils.GetCurrentUserInfo();
            partsRetailReturnBill.CreatorId = userInfo.Id;
            partsRetailReturnBill.CreatorName = userInfo.Name;
            partsRetailReturnBill.CreateTime = DateTime.Now;
            if(string.IsNullOrWhiteSpace(partsRetailReturnBill.Code) || partsRetailReturnBill.Code == GlobalVar.ASSIGNED_BY_SERVER)
                partsRetailReturnBill.Code = CodeGenerator.Generate("PartsRetailReturnBill", partsRetailReturnBill.SalesUnitOwnerCompanyCode);
        }

        internal void UpdatePartsRetailReturnBillValidate(PartsRetailReturnBill partsRetailReturnBill) {
            var userInfo = Utils.GetCurrentUserInfo();
            partsRetailReturnBill.ModifierId = userInfo.Id;
            partsRetailReturnBill.ModifierName = userInfo.Name;
            partsRetailReturnBill.ModifyTime = DateTime.Now;
        }

        public void InsertPartsRetailReturnBill(PartsRetailReturnBill partsRetailReturnBill) {
            InsertToDatabase(partsRetailReturnBill);
            var partsRetailReturnBillDetails = ChangeSet.GetAssociatedChanges(partsRetailReturnBill, v => v.PartsRetailReturnBillDetails, ChangeOperation.Insert);
            foreach(PartsRetailReturnBillDetail partsRetailReturnBillDetail in partsRetailReturnBillDetails)
                InsertToDatabase(partsRetailReturnBillDetail);
            this.InsertPartsRetailReturnBillValidate(partsRetailReturnBill);
        }

        public void UpdatePartsRetailReturnBill(PartsRetailReturnBill partsRetailReturnBill) {
            partsRetailReturnBill.PartsRetailReturnBillDetails.Clear();
            UpdateToDatabase(partsRetailReturnBill);
            var partsRetailReturnBillDetails = ChangeSet.GetAssociatedChanges(partsRetailReturnBill, v => v.PartsRetailReturnBillDetails);
            foreach(PartsRetailReturnBillDetail partsRetailReturnBillDetail in partsRetailReturnBillDetails) {
                switch(ChangeSet.GetChangeOperation(partsRetailReturnBillDetail)) {
                    case ChangeOperation.Insert:
                        InsertToDatabase(partsRetailReturnBillDetail);
                        break;
                    case ChangeOperation.Update:
                        UpdateToDatabase(partsRetailReturnBillDetail);
                        break;
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(partsRetailReturnBillDetail);
                        break;
                }
            }
            this.UpdatePartsRetailReturnBillValidate(partsRetailReturnBill);
        }

        public PartsRetailReturnBill GetPartsRetailReturnBillWithDetailsById(int id) {
            var dbPartsRetailReturnBill = ObjectContext.PartsRetailReturnBills.SingleOrDefault(entity => entity.Id == id);
            if(dbPartsRetailReturnBill != null) {
                var partsRetailReturnBillDetails = ObjectContext.PartsRetailReturnBillDetails.Where(r => r.PartsRetailReturnBillId == id).ToArray();
            }
            return dbPartsRetailReturnBill;
        }

        public IQueryable<PartsRetailReturnBill> GetPartsRetailReturnBillWithDetails() {
            return ObjectContext.PartsRetailReturnBills.Include("PartsRetailReturnBillDetails").OrderBy(v => v.Id);
        }
    
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               

        public void InsertPartsRetailReturnBill(PartsRetailReturnBill partsRetailReturnBill) {
            new PartsRetailReturnBillAch(this).InsertPartsRetailReturnBill(partsRetailReturnBill);
        }

        public void UpdatePartsRetailReturnBill(PartsRetailReturnBill partsRetailReturnBill) {
            new PartsRetailReturnBillAch(this).UpdatePartsRetailReturnBill(partsRetailReturnBill);
        }

        public PartsRetailReturnBill GetPartsRetailReturnBillWithDetailsById(int id) {
            return new PartsRetailReturnBillAch(this).GetPartsRetailReturnBillWithDetailsById(id);
        }

        public IQueryable<PartsRetailReturnBill> GetPartsRetailReturnBillWithDetails() {
            return new PartsRetailReturnBillAch(this).GetPartsRetailReturnBillWithDetails();
        }
    }
}
