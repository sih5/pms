﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Net;
using System.ServiceModel.DomainServices.Server;
using System.Text;
using System.Transactions;
using System.Web.Configuration;
using Devart.Data.Oracle;
using FTDCSWebService;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsSalesOrderAch : DcsSerivceAchieveBase {
        internal void InsertPartsSalesOrderValidate(PartsSalesOrder partsSalesOrder) {
            if(string.IsNullOrWhiteSpace(partsSalesOrder.Code) || partsSalesOrder.Code == GlobalVar.ASSIGNED_BY_SERVER) {
                //获取业务代码
                var partsSalesOrderType = ObjectContext.PartsSalesOrderTypes.Where(v => v.Id == partsSalesOrder.PartsSalesOrderTypeId).FirstOrDefault();
                if(partsSalesOrderType == null)
                    partsSalesOrder.Code = CodeGenerator.Generate("PartsSalesOrder", partsSalesOrder.SalesUnitOwnerCompanyCode);
                else
                    partsSalesOrder.Code = partsSalesOrderType.BusinessCode + CodeGenerator.Generate("PartsSalesOrder", partsSalesOrder.SalesUnitOwnerCompanyCode);
            }

            var userInfo = Utils.GetCurrentUserInfo();
            partsSalesOrder.CreatorId = userInfo.Id;
            partsSalesOrder.CreatorName = userInfo.Name;
            partsSalesOrder.CreateTime = DateTime.Now;

            if(partsSalesOrder.Status == (int)DcsPartsSalesOrderStatus.提交) {
                partsSalesOrder.SubmitterId = userInfo.Id;
                partsSalesOrder.SubmitterName = userInfo.Name;
                partsSalesOrder.SubmitTime = DateTime.Now;
            }

        }
        internal void InsertPartsSalesOrderValidate1(PartsSalesOrder partsSalesOrder) {
            if(string.IsNullOrWhiteSpace(partsSalesOrder.Code) || partsSalesOrder.Code == GlobalVar.ASSIGNED_BY_SERVER) {
                //获取业务代码
                var partsSalesOrderType = ObjectContext.PartsSalesOrderTypes.Where(v => v.Id == partsSalesOrder.PartsSalesOrderTypeId).FirstOrDefault();
                if(partsSalesOrderType == null)
                    partsSalesOrder.Code = CodeGenerator.Generate("PartsSalesOrder", partsSalesOrder.BranchCode);
                else
                    partsSalesOrder.Code = partsSalesOrderType.BusinessCode + CodeGenerator.Generate("PartsSalesOrder", partsSalesOrder.BranchCode);

            }
            var userInfo = Utils.GetCurrentUserInfo();
            partsSalesOrder.CreatorId = userInfo.Id;
            partsSalesOrder.CreatorName = userInfo.Name;
            partsSalesOrder.CreateTime = DateTime.Now;

            if(partsSalesOrder.Status == (int)DcsPartsSalesOrderStatus.提交) {
                partsSalesOrder.SubmitterId = userInfo.Id;
                partsSalesOrder.SubmitterName = userInfo.Name;
                partsSalesOrder.SubmitTime = DateTime.Now;
            }

        }
        internal void UpdatePartsSalesOrderValidate(PartsSalesOrder partsSalesOrder) {
            var userInfo = Utils.GetCurrentUserInfo();
            partsSalesOrder.ModifierId = userInfo.Id;
            partsSalesOrder.ModifierName = userInfo.Name;
            partsSalesOrder.ModifyTime = DateTime.Now;

        }


        public void InsertPartsSalesOrder(PartsSalesOrder partsSalesOrder) {
            InsertToDatabase(partsSalesOrder);
            var partsSalesOrderDetails = ChangeSet.GetAssociatedChanges(partsSalesOrder, v => v.PartsSalesOrderDetails, ChangeOperation.Insert);
            foreach(PartsSalesOrderDetail partsSalesOrderDetail in partsSalesOrderDetails) {
                InsertToDatabase(partsSalesOrderDetail);
            }
            this.InsertPartsSalesOrderValidate(partsSalesOrder);
        }

        public void UpdatePartsSalesOrder(PartsSalesOrder partsSalesOrder) {
            partsSalesOrder.PartsSalesOrderDetails.Clear();
            UpdateToDatabase(partsSalesOrder);
            var partsSalesOrderDetails = ChangeSet.GetAssociatedChanges(partsSalesOrder, v => v.PartsSalesOrderDetails);
            foreach(PartsSalesOrderDetail partsSalesOrderDetail in partsSalesOrderDetails) {
                switch(ChangeSet.GetChangeOperation(partsSalesOrderDetail)) {
                    case ChangeOperation.Insert:
                        InsertToDatabase(partsSalesOrderDetail);
                        break;
                    case ChangeOperation.Update:
                        UpdateToDatabase(partsSalesOrderDetail);
                        break;
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(partsSalesOrderDetail);
                        break;
                }
            }
            this.UpdatePartsSalesOrderValidate(partsSalesOrder);
        }

        public PartsSalesOrder GetPartsSalesOrderWithPartsSalesOrderDetails(int id) {
            var result = ObjectContext.PartsSalesOrders.SingleOrDefault(r => r.Id == id && r.Status != (int)DcsPartsSalesOrderStatus.作废);
            if(result != null) {
                var orderDetail = ObjectContext.PartsSalesOrderDetails.Where(r => r.PartsSalesOrderId == id).ToArray();
                var salesUnit = ObjectContext.SalesUnits.SingleOrDefault(v => v.Id == result.SalesUnitId && v.Status == (int)DcsMasterDataStatus.有效);
            }
            return result;
        }

        public PartsSalesOrder GetPartsSalesOrderWithPartsSalesOrderDetailsAndCustomerAccount(int id) {
            var result = ObjectContext.PartsSalesOrders.SingleOrDefault(r => r.Id == id && r.Status != (int)DcsPartsSalesOrderStatus.作废);
            if(result != null) {
                var orderDetail = ObjectContext.PartsSalesOrderDetails.Where(r => r.PartsSalesOrderId == id && (r.ApproveQuantity.HasValue ? r.ApproveQuantity.Value : 0) != r.OrderedQuantity).ToArray();
                foreach(var item in orderDetail) {
                    item.UnfulfilledQuantity = item.OrderedQuantity - (item.ApproveQuantity ?? 0);
                }
                var partId = orderDetail.Select(x => x.SparePartId).ToArray();
                var sparePart = ObjectContext.SpareParts.Where(r => partId.Contains(r.Id)).ToArray();
                var salesUnit = ObjectContext.SalesUnits.SingleOrDefault(v => v.Id == result.SalesUnitId && v.Status == (int)DcsMasterDataStatus.有效);
                var customerAccount = ObjectContext.CustomerAccounts.SingleOrDefault(v => v.Id == result.CustomerAccountId && v.Status == (int)DcsMasterDataStatus.有效);
                var customerAccounts = DomainService.查询客户可用资金(result.CustomerAccountId, result.SalesUnit.AccountGroupId, result.CustomerAccount.CustomerCompanyId);
            }
            return result;
        }

        public PartsSalesOrder GetPartsSalesOrderWithPartsSalesOrderDetailsAndCustomerAccountForEditDetail(int id) {
            var result = ObjectContext.PartsSalesOrders.SingleOrDefault(r => r.Id == id && r.Status != (int)DcsPartsSalesOrderStatus.作废);
            if(result != null) {
                var orderDetail = ObjectContext.PartsSalesOrderDetails.Where(r => r.PartsSalesOrderId == id).ToArray();
                foreach(var item in orderDetail) {
                    item.ApproveQuantity = item.ApproveQuantity ?? 0;
                }
                var salesUnit = ObjectContext.SalesUnits.SingleOrDefault(v => v.Id == result.SalesUnitId && v.Status == (int)DcsMasterDataStatus.有效);
                var customerAccount = ObjectContext.CustomerAccounts.SingleOrDefault(v => v.Id == result.CustomerAccountId && v.Status == (int)DcsMasterDataStatus.有效);
                var customerAccounts = DomainService.查询客户可用资金(result.CustomerAccountId, result.SalesUnit.AccountGroupId, result.CustomerAccount.CustomerCompanyId);
            }
            return result;
        }

        public IQueryable<PartsSalesOrder> 查询可退货销售订单(int submitCompanyId) {
            return ObjectContext.PartsSalesOrders.Where(r => r.SubmitCompanyId == submitCompanyId && r.Status == (int)DcsPartsSalesOrderStatus.审批完成).OrderBy(r => r.Id);
        }

        public IQueryable<PartsSalesOrderDetail> GetPartsSalesOrderDetailInfo() {
            return ObjectContext.PartsSalesOrderDetails.Include("SparePart").OrderBy(v => v.Id);
        }

        public IQueryable<PartsSalesOrder> 根据审批人员查询销售订单(int personnelId) {
            var userInfo = Utils.GetCurrentUserInfo();
            #region 改的跟设计一样了

            var companyQuery = from a in ObjectContext.Companies
                               join b in ObjectContext.Personnels on a.Id equals b.CorporationId
                               where b.Id == personnelId && a.Status == (int)DcsMasterDataStatus.有效 && b.Status == (int)DcsMasterDataStatus.有效
                               select a;
            var companyType = companyQuery.Single().Type;
            var salesUnitAffiPersonnelQuery = ObjectContext.SalesUnitAffiPersonnels.Where(r => r.PersonnelId == personnelId);
            var tempQuery = from a in ObjectContext.PartsSalesOrders.Include("Warehouse").Include("PartsSalesOrderType")
                            where a.IsPurDistribution == null || a.IsPurDistribution == false
                            where salesUnitAffiPersonnelQuery.Any(r => r.SalesUnitId == a.SalesUnitId && ObjectContext.SalesUnits.Any(v => v.Id == r.SalesUnitId && v.OwnerCompanyId == userInfo.EnterpriseId))
                            select a;
            if(companyType == (int)DcsCompanyType.分公司) {
                var marketDptPersonnelRelationQuery = ObjectContext.MarketDptPersonnelRelations.Where(r => r.PersonnelId == personnelId && r.Status == 1);
                var AgenciesCompanyQuery = ObjectContext.Companies.Where(r => r.Status == (int)DcsMasterDataStatus.有效 && r.Type == (int)DcsCompanyType.代理库 || r.Type == (int)DcsCompanyType.服务站兼代理库);
                var tempFilterQueryForServiceStationWhenBranch = from a in ObjectContext.MarketDptPersonnelRelations.Where(r => r.PersonnelId == personnelId && r.Status == 1)
                                                                 join b in ObjectContext.DealerServiceInfoes.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on a.MarketDepartmentId equals b.MarketingDepartmentId
                                                                 join c in ObjectContext.Companies.Where(r => r.Status == (int)DcsMasterDataStatus.有效) on b.DealerId equals c.Id
                                                                 where c.Type == (int)DcsCompanyType.服务站 || c.Type == (int)DcsCompanyType.出口客户
                                                                 select b;
                tempQuery = from a in tempQuery
                            where !marketDptPersonnelRelationQuery.Any() || (AgenciesCompanyQuery.Any(r => r.Id == a.SubmitCompanyId) || tempFilterQueryForServiceStationWhenBranch.Any(r => r.DealerId == a.SubmitCompanyId && r.PartsSalesCategoryId == a.SalesCategoryId))
                            select a;

                var tempFilterQueryForAgencyWhenBranch = from a in ObjectContext.SalesUnitAffiPersonnels.Where(r => r.PersonnelId == personnelId)
                                                         join b in ObjectContext.SalesUnits.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on a.SalesUnitId equals b.Id
                                                         join c in ObjectContext.Companies.Where(r => r.Status == (int)DcsMasterDataStatus.有效) on b.OwnerCompanyId equals c.Id
                                                         where c.Type == (int)DcsCompanyType.代理库 || c.Type == (int)DcsCompanyType.服务站兼代理库
                                                         select c;
                var StationCompanyQuery = ObjectContext.Companies.Where(r => r.Status == (int)DcsMasterDataStatus.有效 && (r.Type == (int)DcsCompanyType.服务站 || r.Type == (int)DcsCompanyType.出口客户));
                var salesUnitOwnerCompanyQuery = from a in ObjectContext.SalesUnitAffiPersonnels.Where(r => r.PersonnelId == personnelId)
                                                 join b in ObjectContext.SalesUnits.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on a.SalesUnitId equals b.Id
                                                 join c in ObjectContext.Companies.Where(r => r.Status == (int)DcsMasterDataStatus.有效) on b.OwnerCompanyId equals c.Id
                                                 where c.Type != (int)DcsCompanyType.分公司
                                                 select c;


                tempQuery = from a in tempQuery
                            join b in ObjectContext.Companies.Where(r => r.Status == (int)DcsMasterDataStatus.有效) on a.SalesUnitOwnerCompanyId equals b.Id
                            where !salesUnitOwnerCompanyQuery.Any() || (StationCompanyQuery.Any(r => r.Id == a.SubmitCompanyId) || (tempFilterQueryForAgencyWhenBranch.Any(r => r.Id == a.SubmitCompanyId && r.Id != b.Id)))
                            select a;
                return tempQuery.OrderByDescending(r => r.Id);

            } else if(companyType == (int)DcsCompanyType.代理库 || companyType == (int)DcsCompanyType.服务站兼代理库) {
                return tempQuery.OrderByDescending(r => r.Id);
            } else {
                return tempQuery.OrderByDescending(r => r.Id);
            }
            #endregion
        }

        public IQueryable<PartsSalesOrder> 根据审批人员查询销售订单省市(int personnelId, int? provinceName, int? cityName, string sparePartCode) {
            var userInfo = Utils.GetCurrentUserInfo();
            string pName = "", cName = "";
            IQueryable<PartsSalesOrder> partsSalesOrders = ObjectContext.PartsSalesOrders.Where(r => (r.IsAgencyShip == null || r.IsAgencyShip == false) && (!r.IsExport.HasValue || r.IsExport != 1));
            if(provinceName.HasValue) {
                var province = this.ObjectContext.Regions.FirstOrDefault(v => v.Id == provinceName.Value);
                if(province != null) {
                    pName = province.Name;
                    partsSalesOrders = partsSalesOrders.Where(v => v.Province.Contains(pName));
                }
            }
            if(cityName.HasValue) {
                var city = this.ObjectContext.Regions.FirstOrDefault(v => v.Id == cityName.Value);
                if(city != null) {
                    cName = city.Name;
                    partsSalesOrders = partsSalesOrders.Where(v => v.City.Contains(cName));
                }
            }

            if(!string.IsNullOrEmpty(sparePartCode)) {
                partsSalesOrders = from a in partsSalesOrders
                                   join b in this.ObjectContext.PartsSalesOrderDetails.Where(r => r.SparePartCode.Contains(sparePartCode)) on a.Id equals b.PartsSalesOrderId
                                   select a;
            }

            #region 改的跟设计一样了

            var companyQuery = from a in ObjectContext.Companies
                               join b in ObjectContext.Personnels on a.Id equals b.CorporationId
                               where b.Id == personnelId && a.Status == (int)DcsMasterDataStatus.有效 && b.Status == (int)DcsMasterDataStatus.有效
                               select a;
            var companyType = companyQuery.Single().Type;
            var salesUnitAffiPersonnelQuery = ObjectContext.SalesUnitAffiPersonnels.Where(r => r.PersonnelId == personnelId);
            var tempQuery = from a in partsSalesOrders.Include("Warehouse").Include("PartsSalesOrderType")
                            where a.IsPurDistribution == null || a.IsPurDistribution == false
                            where salesUnitAffiPersonnelQuery.Any(r => r.SalesUnitId == a.SalesUnitId && ObjectContext.SalesUnits.Any(v => v.Id == r.SalesUnitId && v.OwnerCompanyId == userInfo.EnterpriseId))
                            select a;
            if(companyType == (int)DcsCompanyType.分公司) {
                var marketDptPersonnelRelationQuery = ObjectContext.MarketDptPersonnelRelations.Where(r => r.PersonnelId == personnelId && r.Status == 1);
                var AgenciesCompanyQuery = ObjectContext.Companies.Where(r => r.Status == (int)DcsMasterDataStatus.有效 && r.Type == (int)DcsCompanyType.代理库);
                var tempFilterQueryForServiceStationWhenBranch = from a in ObjectContext.MarketDptPersonnelRelations.Where(r => r.PersonnelId == personnelId && r.Status == 1)
                                                                 join b in ObjectContext.DealerServiceInfoes.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on a.MarketDepartmentId equals b.MarketingDepartmentId
                                                                 join c in ObjectContext.Companies.Where(r => r.Status == (int)DcsMasterDataStatus.有效) on b.DealerId equals c.Id
                                                                 where c.Type == (int)DcsCompanyType.服务站 || c.Type == (int)DcsCompanyType.出口客户 || c.Type == (int)DcsCompanyType.服务站兼代理库 || c.Type == (int)DcsCompanyType.集团企业
                                                                 select b;
                tempQuery = from a in tempQuery
                            where !marketDptPersonnelRelationQuery.Any() || (AgenciesCompanyQuery.Any(r => r.Id == a.SubmitCompanyId) || tempFilterQueryForServiceStationWhenBranch.Any(r => r.DealerId == a.SubmitCompanyId && r.PartsSalesCategoryId == a.SalesCategoryId))
                            select a;

                var tempFilterQueryForAgencyWhenBranch = from a in ObjectContext.SalesUnitAffiPersonnels.Where(r => r.PersonnelId == personnelId)
                                                         join b in ObjectContext.SalesUnits.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on a.SalesUnitId equals b.Id
                                                         join c in ObjectContext.Companies.Where(r => r.Status == (int)DcsMasterDataStatus.有效) on b.OwnerCompanyId equals c.Id
                                                         where c.Type == (int)DcsCompanyType.代理库
                                                         select c;
                var StationCompanyQuery = ObjectContext.Companies.Where(r => r.Status == (int)DcsMasterDataStatus.有效 && (r.Type == (int)DcsCompanyType.服务站 || r.Type == (int)DcsCompanyType.出口客户 || r.Type == (int)DcsCompanyType.服务站兼代理库 || r.Type == (int)DcsCompanyType.集团企业));
                var salesUnitOwnerCompanyQuery = from a in ObjectContext.SalesUnitAffiPersonnels.Where(r => r.PersonnelId == personnelId)
                                                 join b in ObjectContext.SalesUnits.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on a.SalesUnitId equals b.Id
                                                 join c in ObjectContext.Companies.Where(r => r.Status == (int)DcsMasterDataStatus.有效) on b.OwnerCompanyId equals c.Id
                                                 where c.Type != (int)DcsCompanyType.分公司
                                                 select c;


                tempQuery = from a in tempQuery
                            join b in ObjectContext.Companies.Where(r => r.Status == (int)DcsMasterDataStatus.有效) on a.SalesUnitOwnerCompanyId equals b.Id
                            where !salesUnitOwnerCompanyQuery.Any() || (StationCompanyQuery.Any(r => r.Id == a.SubmitCompanyId) || (tempFilterQueryForAgencyWhenBranch.Any(r => r.Id == a.SubmitCompanyId && r.Id != b.Id)))
                            select a;
                return tempQuery.OrderByDescending(r => r.Id);

            } else if(companyType == (int)DcsCompanyType.代理库 || companyType == (int)DcsCompanyType.服务站兼代理库) {
                return tempQuery.OrderByDescending(r => r.Id);
            } else {
                return tempQuery.OrderByDescending(r => r.Id);
            }
            #endregion
        }

        public IQueryable<PartsSalesOrder> 根据审批人员查询销售订单省市代理库(int personnelId, int? provinceName, int? cityName) {
            var userInfo = Utils.GetCurrentUserInfo();
            string pName = "", cName = "";
            IQueryable<PartsSalesOrder> partsSalesOrders = ObjectContext.PartsSalesOrders.Where(r => r.IsAgencyShip == true);
            if(provinceName.HasValue) {
                var province = this.ObjectContext.Regions.FirstOrDefault(v => v.Id == provinceName.Value);
                if(province != null) {
                    pName = province.Name;
                    partsSalesOrders = partsSalesOrders.Where(v => v.Province.Contains(pName));
                }
            }
            if(cityName.HasValue) {
                var city = this.ObjectContext.Regions.FirstOrDefault(v => v.Id == cityName.Value);
                if(city != null) {
                    cName = city.Name;
                    partsSalesOrders = partsSalesOrders.Where(v => v.City.Contains(cName));
                }
            }


            #region 改的跟设计一样了

            var companyQuery = from a in ObjectContext.Companies
                               join b in ObjectContext.Personnels on a.Id equals b.CorporationId
                               where b.Id == personnelId && a.Status == (int)DcsMasterDataStatus.有效 && b.Status == (int)DcsMasterDataStatus.有效
                               select a;
            var companyType = companyQuery.Single().Type;
            var salesUnitAffiPersonnelQuery = ObjectContext.SalesUnitAffiPersonnels.Where(r => r.PersonnelId == personnelId);
            var tempQuery = from a in partsSalesOrders.Include("Warehouse").Include("PartsSalesOrderType")
                            where a.IsPurDistribution == null || a.IsPurDistribution == false
                            where salesUnitAffiPersonnelQuery.Any(r => r.SalesUnitId == a.SalesUnitId && ObjectContext.SalesUnits.Any(v => v.Id == r.SalesUnitId && v.OwnerCompanyId == userInfo.EnterpriseId))
                            select a;
            if(companyType == (int)DcsCompanyType.分公司) {
                var marketDptPersonnelRelationQuery = ObjectContext.MarketDptPersonnelRelations.Where(r => r.PersonnelId == personnelId && r.Status == 1);
                var AgenciesCompanyQuery = ObjectContext.Companies.Where(r => r.Status == (int)DcsMasterDataStatus.有效 && r.Type == (int)DcsCompanyType.代理库 || r.Type == (int)DcsCompanyType.服务站兼代理库);
                var tempFilterQueryForServiceStationWhenBranch = from a in ObjectContext.MarketDptPersonnelRelations.Where(r => r.PersonnelId == personnelId && r.Status == 1)
                                                                 join b in ObjectContext.DealerServiceInfoes.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on a.MarketDepartmentId equals b.MarketingDepartmentId
                                                                 join c in ObjectContext.Companies.Where(r => r.Status == (int)DcsMasterDataStatus.有效) on b.DealerId equals c.Id
                                                                 where c.Type == (int)DcsCompanyType.服务站 || c.Type == (int)DcsCompanyType.出口客户
                                                                 select b;
                tempQuery = from a in tempQuery
                            where !marketDptPersonnelRelationQuery.Any() || (AgenciesCompanyQuery.Any(r => r.Id == a.SubmitCompanyId) || tempFilterQueryForServiceStationWhenBranch.Any(r => r.DealerId == a.SubmitCompanyId && r.PartsSalesCategoryId == a.SalesCategoryId))
                            select a;

                var tempFilterQueryForAgencyWhenBranch = from a in ObjectContext.SalesUnitAffiPersonnels.Where(r => r.PersonnelId == personnelId)
                                                         join b in ObjectContext.SalesUnits.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on a.SalesUnitId equals b.Id
                                                         join c in ObjectContext.Companies.Where(r => r.Status == (int)DcsMasterDataStatus.有效) on b.OwnerCompanyId equals c.Id
                                                         where c.Type == (int)DcsCompanyType.代理库 || c.Type == (int)DcsCompanyType.服务站兼代理库
                                                         select c;
                var StationCompanyQuery = ObjectContext.Companies.Where(r => r.Status == (int)DcsMasterDataStatus.有效 && (r.Type == (int)DcsCompanyType.服务站 || r.Type == (int)DcsCompanyType.出口客户));
                var salesUnitOwnerCompanyQuery = from a in ObjectContext.SalesUnitAffiPersonnels.Where(r => r.PersonnelId == personnelId)
                                                 join b in ObjectContext.SalesUnits.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on a.SalesUnitId equals b.Id
                                                 join c in ObjectContext.Companies.Where(r => r.Status == (int)DcsMasterDataStatus.有效) on b.OwnerCompanyId equals c.Id
                                                 where c.Type != (int)DcsCompanyType.分公司
                                                 select c;


                tempQuery = from a in tempQuery
                            join b in ObjectContext.Companies.Where(r => r.Status == (int)DcsMasterDataStatus.有效) on a.SalesUnitOwnerCompanyId equals b.Id
                            where !salesUnitOwnerCompanyQuery.Any() || (StationCompanyQuery.Any(r => r.Id == a.SubmitCompanyId) || (tempFilterQueryForAgencyWhenBranch.Any(r => r.Id == a.SubmitCompanyId && r.Id != b.Id)))
                            select a;
                return tempQuery.OrderByDescending(r => r.Id);

            } else if(companyType == (int)DcsCompanyType.代理库 || companyType == (int)DcsCompanyType.服务站兼代理库) {
                return tempQuery.OrderByDescending(r => r.Id);
            } else {
                return tempQuery.OrderByDescending(r => r.Id);
            }
            #endregion
        }

        public IEnumerable<VehiclePartsSalesOrdercs> 根据登陆人员查询销售订单(DateTime submitTimeBegin, DateTime submitTimeEnd, string erpSourceOrderCode, int? salesUnitOwnerCompanyId) {
            var userInfo = Utils.GetCurrentUserInfo();
            var company = this.ObjectContext.Companies.SingleOrDefault(a => a.Status == (int)DcsMasterDataStatus.有效 && a.Id == userInfo.EnterpriseId);
            if(company == null)
                return null;
            var sqlBulider = new StringBuilder();
            sqlBulider.Append(@"select a.code as PartsSalesOrderCode, --订单号
                               a.SubmitCompanyId as SubmitCompanyId,
                               a.BranchId as BranchId,
                               a.Id as Id,
                               b.Id as DetailId,
                               c.name as SalesCategoryName, ---品牌 
                                c.Id as SalesCategoryId,
                               d.code as  WarehouseCode, -- 发货 仓库编号 
                               d.name as WarehouseName, -- 发货 仓库名称
                               e.code as SubmitCompanyCode, --- 企业编号 
                               e.name as SubmitCompanyName, -- 企业名称 
                               e.type as CustomerType, -- 企业类型 
                               a.partssalesordertypename as PartsSalesOrderTypeName, --- 订单类型
                               a.status as Status, --订单状态
                               a.submittime as SubmitTime, -- 提交时间 
                               a.SalesUnitOwnerCompanyName,--代理库
                               a.approvetime as ApproveTime, --- 审核时间 
                               nvl((select sum(pod.outboundamount)
                                  from partsoutboundbill po
                                 inner join partsoutboundbilldetail pod
                                    on po.id = pod.partsoutboundbillid
                                 where po.originalrequirementbillcode = a.code
                                   and pod.sparepartid = b.sparepartid
                                 group by po.originalrequirementbillcode, pod.sparepartid),0) as OutboundQuantity, --- 出库数量 
                               nvl(pso.code,' ') as PartsShippingOrderCode, ---发运单号,
                               pso.status as ShippingStatus, -- 发运状态
                               pso.shippingdate as ShippingDate, ---发运日期
                               pso.shippingmethod as ShippingMethod, ---发运方式
                               pso.consigneename as PSOrderConsigneeName, --收货确认人
                               pso.confirmedreceptiontime as ConfirmedReceptionTime, --收货确认时间
                               a.ERPSourceOrderCode as ERPSourceOrderCode,--ERP平台单号
                               nvl(pip.code,' ') as PartsInboundPlanCode, -- 代理库入库计划单号
                               nvl(pib.code,' ') as PartsInboundCheckBillCode, ---代理库入库单号
                               b.sparepartcode as SparePartCode,
                               b.sparepartname as SparePartName,
                               b.SparePartId as SparePartId,
                               b.orderedquantity as OrderedQuantity,
                               nvl(b.approvequantity,0) as ApproveQuantity,
                               nvl(psod.shippingamount,0) as ShippingQuantity,
                               nvl(psod.confirmedamount,0) as ConfirmQuantity,
                               nvl(pid.inspectedquantity,0) as InboundQuantity, ---代理库入库数量
                               a.SalesUnitOwnerCompanyId as SalesUnitOwnerCompanyId,--销售组织隶属企业
                               a.SalesUnitOwnerCompanyName,
                               partsOutboundPlan.code as PartsOutboundPlanCode,
                               partsOutboundPlan.status  as PartsOutboundPlanStatus
                          from partssalesorder a
                         inner join partssalesorderdetail b
                            on a.id = b.partssalesorderid
                         inner join partssalescategory c
                            on c.id = a.salescategoryid
                         inner join warehouse d
                            on d.id = a.warehouseid
                         inner join company e
                            on e.id = a.submitcompanyid
                            left join   partsshippingorder pso on pso.originalrequirementbillcode = a.code
                            left join partsshippingorderdetail psod on   pso.id = psod.partsshippingorderid and  psod.Sparepartid = b.sparepartid
                            left join  partsinboundplan pip on  pip.sourcecode = pso.code and pip.storagecompanytype <>1
                            left join partsinboundcheckbill pib on  pib.partsinboundplanid = pip.id  
                            left join  partsinboundcheckbilldetail pid
                                    on pib.id = pid.partsinboundcheckbillid and pid.sparepartid = b.sparepartid
                             left join partsoutboundplan partsOutboundPlan on a.code=partsOutboundPlan.OriginalRequirementBillCode");
            var paramaterList = new List<Object>();

            sqlBulider.Append(" where SubmitTime>=:submitTimeBegin ");
            paramaterList.Add((object)new OracleParameter("submitTimeBegin", OracleDbType.Date, submitTimeBegin, System.Data.ParameterDirection.Input));

            sqlBulider.Append(" and SubmitTime<:submitTimeEnd ");
            paramaterList.Add((object)new OracleParameter("submitTimeEnd", OracleDbType.Date, submitTimeEnd, System.Data.ParameterDirection.Input));
            if(!string.IsNullOrEmpty(erpSourceOrderCode)) {
                sqlBulider.Append(string.Format(" and ERPSourceOrderCode like '%{0}%'", erpSourceOrderCode));
            }
            if(salesUnitOwnerCompanyId.HasValue) {
                sqlBulider.Append(string.Format(" and SalesUnitOwnerCompanyId = {0}", salesUnitOwnerCompanyId));
            }
            var sql = sqlBulider.ToString();
            var search = ObjectContext.ExecuteStoreQuery<VehiclePartsSalesOrdercs>(sql, paramaterList.ToArray()).ToList();



            if(company.Type == (int)DcsCompanyType.分公司) {
                return search.Where(r => r.BranchId == userInfo.EnterpriseId).Distinct().OrderBy(r => r.DetailId);
            }
            return search.Where(r => r.SubmitCompanyId == userInfo.EnterpriseId).Distinct().OrderBy(r => r.DetailId);
        }

        public PartsSalesOrder GetPartsSalesOrderWithDetailsById(int id) {
            var result = ObjectContext.PartsSalesOrders.SingleOrDefault(r => r.Id == id && r.Status != (int)DcsPartsSalesOrderStatus.作废);
            if(result != null) {
                var orderDetail = ObjectContext.PartsSalesOrderDetails.Where(r => r.PartsSalesOrderId == id).ToArray();
                var salesUnit = ObjectContext.SalesUnits.SingleOrDefault(v => v.Id == result.SalesUnitId && result.Status == (int)DcsMasterDataStatus.有效);
            }
            return result;
        }

        public PartsSalesOrder GetPartsSalesOrderByIdAndOriginalRequirementBillId(int originalRequirementBillId, int originalRequirementBillType) {
            return ObjectContext.PartsSalesOrders.SingleOrDefault(r => r.OriginalRequirementBillId == originalRequirementBillId && r.OriginalRequirementBillType == originalRequirementBillType);
        }

        public decimal 查询未审核订单金额(int submitCompanyId, int salesUnitId, int partsSalesCategoryId) {
            var partsSalesOrdersQuery = ObjectContext.PartsSalesOrders.Where(r => r.SubmitCompanyId == submitCompanyId && r.SalesUnitId == salesUnitId && r.SalesCategoryId == partsSalesCategoryId && (r.Status == (int)DcsPartsSalesOrderStatus.提交 || r.Status == (int)DcsPartsSalesOrderStatus.部分审批));
            var partSalesOrderDetails = partsSalesOrdersQuery.SelectMany(r => r.PartsSalesOrderDetails).ToArray();
            var result = partSalesOrderDetails.Sum(r => (r.OrderedQuantity - (r.ApproveQuantity ?? 0)) * r.OrderPrice);
            return result;
        }

        public IQueryable<PartsSalesOrder> GetPartsSalesOrderHaveOutBound() {
            var status = new int[] {
                (int)DcsPartsSalesOrderStatus.部分审批, (int)DcsPartsSalesOrderStatus.审批完成, (int)DcsPartsSalesOrderStatus.终止
            };
            var result = (from a in ObjectContext.PartsSalesOrders.Where(r => status.Contains(r.Status))
                          join b in ObjectContext.PartsOutboundBills.Where(r => r.OutboundType != (int)DcsPartsOutboundType.配件调拨) on a.Id equals b.OriginalRequirementBillId
                          where b.OriginalRequirementBillType == 1
                          //join d in ObjectContext.PartsShippingOrderRefs on c.Id equals d.PartsOutboundBillId
                          //join e in ObjectContext.PartsShippingOrders.Where(r => r.Status == (int)DcsPartsShippingOrderStatus.收货确认 || r.Status == (int)DcsPartsShippingOrderStatus.回执确认) on d.PartsShippingOrderId equals e.Id
                          //where a.ReceivingCompanyId == e.ReceivingCompanyId
                          select a).Distinct();
            return result.OrderBy(r => r.Id);
        }

        public PartsSalesOrder GetPartsSalesOrderById(int id) {
            return ObjectContext.PartsSalesOrders.FirstOrDefault(r => r.Id == id);
        }

        //非进口商使用
        public IQueryable<PartsSalesOrder> GetPartsSalesOrderWithWarehouse(string sparePartCode, string sparePartName) {
            var user = Utils.GetCurrentUserInfo();
            //IQueryable<PartsSalesOrderDetail> detail = ObjectContext.PartsSalesOrderDetails;
            //if(!string.IsNullOrEmpty(sparePartCode))
            //    detail = detail.Where(r => r.SparePartCode.Contains(sparePartCode));
            //if(!string.IsNullOrEmpty(sparePartName))
            //    detail = detail.Where(r => r.SparePartName.Contains(sparePartName));
            //var result = from a in ObjectContext.PartsSalesOrders.Where(r => !r.IsExport.HasValue || r.IsExport != 1)
            //             join b in detail on a.Id equals b.PartsSalesOrderId
            //             where a.SubmitCompanyId == user.EnterpriseId
            //             select a;
            var result1 = ObjectContext.PartsSalesOrders.Where(r => (!r.IsExport.HasValue || r.IsExport != 1) && r.SubmitCompanyId == user.EnterpriseId);
            if(!string.IsNullOrEmpty(sparePartCode))
                result1 = result1.Where(r => ObjectContext.PartsSalesOrderDetails.Any(d => d.PartsSalesOrderId == r.Id && d.SparePartCode.Contains(sparePartCode)));
            if(!string.IsNullOrEmpty(sparePartName))
                result1 = result1.Where(r => ObjectContext.PartsSalesOrderDetails.Any(d => d.PartsSalesOrderId == r.Id && d.SparePartName.Contains(sparePartName)));
            return result1.Include("Warehouse").OrderBy(r => r.Id);
        }


        public IQueryable<PartsSalesOrder> 配件索赔查询销售订单(int sparePartId) {
            var user = Utils.GetCurrentUserInfo();
            var result = from a in ObjectContext.PartsSalesOrders.Where(r => r.SubmitCompanyId == user.EnterpriseId && (r.Status == (int)DcsPartsSalesOrderStatus.部分审批 || r.Status == (int)DcsPartsSalesOrderStatus.审批完成 || r.Status == (int)DcsPartsSalesOrderStatus.终止))
                         from b in a.PartsSalesOrderDetails.Where(r => r.SparePartId == sparePartId)
                         select a;
            if(result != null) {
                var partsSalesOrderDetails = ObjectContext.PartsSalesOrderDetails.Where(r => result.Select(d => d.Id).Contains(r.PartsSalesOrderId));
                foreach(var item in result) {
                    var partsSalesOrderDetails1 = partsSalesOrderDetails.Where(r => r.PartsSalesOrderId == item.Id);
                    foreach(var detail in partsSalesOrderDetails1) {
                        item.PartsSalesOrderDetails.Add(detail);
                    }
                }
            }

            return result.OrderBy(r => r.Id);
        }

        public IQueryable<PartsSalesOrder> 查询配件索赔销售订单(int submitCompanyId, int partid) {
            var partsSalesOrderQuery = from a in ObjectContext.PartsSalesOrders.Where(r => r.SubmitCompanyId == submitCompanyId && (r.Status == (int)DcsPartsSalesOrderStatus.审批完成 || r.Status == (int)DcsPartsSalesOrderStatus.部分审批 || r.Status == (int)DcsPartsSalesOrderStatus.终止) && ObjectContext.PartsSalesOrderDetails.Any(b => r.Id == b.PartsSalesOrderId && b.SparePartId == partid))
                                       select a;
            return partsSalesOrderQuery.OrderBy(r => r.Id);
        }

        public IQueryable<PartsSalesOrder> 配件公司人员查询销售订单(int personnelId) {
            var userInfo = Utils.GetCurrentUserInfo();
            //1、销售组织人员关系.人员Id=参数人员Id  返回值：销售组织人员关系.销售组织Id
            var salesUnitAffiPersonnel = this.ObjectContext.SalesUnitAffiPersonnels.Where(r => r.PersonnelId == personnelId);
            var salesUnitIds = salesUnitAffiPersonnel.Select(r => r.SalesUnitId).ToArray();
            //查询配件销售订单（配件销售订单.销售组织.Id=销售组织人员关系.销售组织Id且 销售订单.销售组织隶属企业ID = 登陆人员所属企业ID）
            var result = this.ObjectContext.PartsSalesOrders.Where(r => salesUnitIds.Contains(r.SalesUnitId) && r.SalesUnitOwnerCompanyId == userInfo.EnterpriseId);
            return result.Include("Warehouse").OrderBy(r => r.Id);
        }

        public IEnumerable<VirtualPartsSalesPriceWithStock> 根据销售类型查询配件销售价带库存(int partsSalesCategoryId, int? partsSalesOrderTypeId, int customerId, int warehouseId) {
            var warehouseAreaCategorys = this.ObjectContext.WarehouseAreaCategories.Where(p => p.Category == (int)DcsAreaType.保管区 || p.Category == (int)DcsAreaType.检验区);
            var warehouseAreaCategoryIds = warehouseAreaCategorys.Select(p => p.Id);
            var partsStocks = this.ObjectContext.PartsStocks.Where(p => warehouseAreaCategoryIds.Contains(p.WarehouseAreaCategoryId.Value) && p.WarehouseId == warehouseId && (p.StorageCompanyType == (int)DcsCompanyType.代理库 || p.StorageCompanyType == (int)DcsCompanyType.服务站兼代理库));
            var result = ((from s in partsStocks
                           join l in ObjectContext.PartsLockedStocks on new {
                               s.WarehouseId,
                               s.PartId
                           } equals new {
                               l.WarehouseId,
                               l.PartId
                           }
                           join w in ObjectContext.Warehouses on
                           s.WarehouseId equals w.Id
                           join a in ObjectContext.Agencies on
                           s.StorageCompanyId equals a.Id
                           join sp in ObjectContext.SpareParts on
                           s.PartId equals sp.Id
                           join pr in ObjectContext.PartsSalesPrices on new {
                               PartsSalesCategoryId = partsSalesCategoryId,
                               PartId = s.PartId,
                           } equals new {
                               PartsSalesCategoryId = pr.PartsSalesCategoryId,
                               PartId = pr.SparePartId
                           }
                           join c in ObjectContext.CustomerOrderPriceGrades on new {
                               PartsSalesCategoryId = partsSalesCategoryId,
                               CustomerCompanyId = customerId
                           } equals new {
                               PartsSalesCategoryId = c.PartsSalesCategoryId,
                               CustomerCompanyId = c.CustomerCompanyId
                           } into temp
                           from c in temp.DefaultIfEmpty()
                           where a.Status == 1 && sp.Status == 1 && pr.Status == 1 && c.Status == 1 && c.PartsSalesOrderTypeId == partsSalesOrderTypeId
                           select new {
                               s.WarehouseId,
                               warehouseCode = w.Code,
                               warehouseName = w.Name,
                               agencyCode = a.Code,
                               agencyName = a.Name,
                               s.StorageCompanyId,
                               s.StorageCompanyType,
                               s.BranchId,
                               s.PartId,
                               s.Quantity,
                               l.LockedQuantity,
                               sparePartCode = sp.Code,
                               sparePartName = sp.Name,
                               sp.MeasureUnit,
                               sp.ReferenceCode,
                               pr.SalesPrice,
                               Coefficient = c == null ? 0 : c.Coefficient
                           }).GroupBy(s => new {
                               s.WarehouseId,
                               s.warehouseCode,
                               s.warehouseName,
                               s.agencyCode,
                               s.agencyName,
                               s.sparePartCode,
                               s.sparePartName,
                               s.StorageCompanyId,
                               s.StorageCompanyType,
                               s.BranchId,
                               s.PartId,
                               s.MeasureUnit,
                               s.ReferenceCode,
                               s.SalesPrice,
                               s.Coefficient
                           }).Select(p => new VirtualPartsSalesPriceWithStock {
                               Id = p.Key.WarehouseId,
                               WarehouseCode = p.Key.warehouseCode,
                               WarehouseName = p.Key.warehouseName,
                               AgencyCode = p.Key.agencyCode,
                               AgencyName = p.Key.agencyName,
                               SparePartId = p.Key.PartId,
                               SparePartCode = p.Key.sparePartCode,
                               SparePartName = p.Key.sparePartName,
                               MeasureUnit = p.Key.MeasureUnit,
                               AcademyCode = p.Key.ReferenceCode,
                               SalesPrice = p.Key.SalesPrice,
                               PriceGradeCoefficient = p.Key.Coefficient,
                               StockQuantity = p.Sum(o => o.Quantity) - p.Sum(o => o.LockedQuantity)
                           })).ToArray();
            //var result = from a in virtualPartsSalesPriceWithStocks
            //             join b in virtualPartsSalesPrices on a.SparePartId equals b.SparePartId
            //             where ((a.SumQuantity ?? 0) - (a.SumLockedQuantity ?? 0)) > 0
            //             select new VirtualPartsSalesPriceWithStock {
            //                 ID = a.ID,
            //                 WarehouseId = a.WarehouseId,
            //                 WarehouseCode = a.WarehouseCode,
            //                 WarehouseName = a.WarehouseName,
            //                 AgencyCode = a.AgencyCode,
            //                 AgencyName = a.AgencyName,
            //                 SparePartId = a.SparePartId,
            //                 SparePartCode = b.SparePartCode,
            //                 SparePartName = b.SparePartName,
            //                 MeasureUnit = b.MeasureUnit,
            //                 AcademyCode = b.ReferenceName,
            //                 SalesPrice = b.PartsSalesPrice,
            //                 SpecialTreatyPrice = b.PartsTreatyPrice,
            //                 DealerSalesPrice = b.DealerSalesPrice,
            //                 PriceGradeCoefficient = b.Coefficient,
            //                 Price = b.Price,
            //                 StockQuantity = (a.SumQuantity ?? 0) - (a.SumLockedQuantity ?? 0)
            //             };

            return result;
        }
        public void PMS_SYC_150_SendPMSUpdateOrderStatus_PS(PartsSalesOrder partsSalesOrder) {
            bool interfaceEnabled;
            Boolean.TryParse(WebConfigurationManager.AppSettings["isInterfaceEnabled150"], out interfaceEnabled);
            var tempContext = new DcsEntities(this.ObjectContext.Connection.ConnectionString);
            if(!interfaceEnabled)
                return;
            var partsSalesOrderDetails = this.ObjectContext.PartsSalesOrderDetails.Where(v => v.PartsSalesOrderId == partsSalesOrder.Id).ToArray();
            if(partsSalesOrder.SalesCategoryName == "随车行" || partsSalesOrder.SalesCategoryName == "欧曼电商") {
                StringBuilder sb1 = new StringBuilder();
                sb1.Append("<DATA>");
                sb1.Append("<HEAD>");
                sb1.Append("<BIZTRANSACTIONID>PMS_SYC_150_" + DateTime.Now.ToString("yyyyMMddhhmmssfff") + "</BIZTRANSACTIONID>");
                sb1.Append("<COUNT>1</COUNT>");
                sb1.Append("<CONSUMER></CONSUMER>");
                sb1.Append("<SRVLEVEL>1</SRVLEVEL>");
                sb1.Append("<ACCOUNT></ACCOUNT>");
                sb1.Append("<PASSWORD></PASSWORD>");
                sb1.Append("</HEAD>");
                sb1.Append("<LIST>");
                sb1.Append("<ITEM>");
                sb1.Append("<orderNumber>" + partsSalesOrder.ERPSourceOrderCode + "</orderNumber>");
                sb1.Append("<Code>" + partsSalesOrder.Code + "</Code>");
                sb1.Append("<status>" + partsSalesOrder.Status + "</status>");
                sb1.Append("<orderPattern>" + (partsSalesOrder.IfDirectProvision == true ? "S" : "W") + "</orderPattern>");
                sb1.Append("<remark>" + partsSalesOrder.ApprovalComment + "</remark>");
                sb1.Append("<orderList>");
                foreach(var partsSalesOrderDetail in partsSalesOrderDetails) {
                    sb1.Append("<ITEM>");
                    sb1.Append("<goodCode>" + partsSalesOrderDetail.SparePartCode + "</goodCode>");
                    sb1.Append("<Quantity>" + partsSalesOrderDetail.OrderedQuantity + "</Quantity>");
                    sb1.Append("<ApproveQuantity>" + partsSalesOrderDetail.ApproveQuantity + "</ApproveQuantity>");
                    sb1.Append("</ITEM>");
                }
                sb1.Append("</orderList>");
                sb1.Append("</ITEM>");
                sb1.Append("</LIST>");
                sb1.Append("</DATA>");
                var listXml = sb1.ToString();
                var listPartsSalesOrder = listXml;
                var report = "";
                try {
                    var retailerUpdateOrderStatus = new PMS_SYC_150_SendPMSUpdateOrderStatusService_pttbindingQSService();

                    retailerUpdateOrderStatus.Credentials = new NetworkCredential("PMS", "pms201603111515");
                    var error = retailerUpdateOrderStatus.PMS_SYC_150_SendPMSUpdateOrderStatusService(listXml, out report);
                    using(TransactionScope scope = new TransactionScope(TransactionScopeOption.RequiresNew)) {
                        //var nccesb = new ESBLogDianShang();
                        //nccesb.InterfaceName = "RetailerOrderStatus";
                        //nccesb.Message = report + "----" + listPartsSalesOrder;
                        //nccesb.SyncNumberBegin = partsSalesOrder.Id;
                        //nccesb.SyncNumberEnd = 0;
                        //nccesb.TheDate = DateTime.Now;
                        //tempContext.ESBLogDianShangs.AddObject(nccesb);
                        //tempContext.SaveChanges();
                        //InsertToDatabase(nccesb);
                        //this.ObjectContext.SaveChanges();
                        scope.Complete();
                    }
                } catch(Exception e) {
                    using(TransactionScope scope = new TransactionScope(TransactionScopeOption.RequiresNew)) {
                        //var nccesb = new ESBLogDianShang();
                        //nccesb.InterfaceName = "RetailerOrderStatus";
                        //nccesb.Message = report + e.Message + "----" + listPartsSalesOrder;
                        //nccesb.SyncNumberBegin = partsSalesOrder.Id;
                        //nccesb.SyncNumberEnd = 0;
                        //nccesb.TheDate = DateTime.Now;
                        //tempContext.ESBLogDianShangs.AddObject(nccesb);
                        //tempContext.SaveChanges();
                        //InsertToDatabase(nccesb);
                        //this.ObjectContext.SaveChanges();
                        string s = e.Message;
                        scope.Complete();
                    }
                }
            }
        }

        public void PMS_SYC_150_记录日志用(PartsSalesOrder partsSalesOrder) {
            bool interfaceEnabled;
            Boolean.TryParse(WebConfigurationManager.AppSettings["isInterfaceEnabled150"], out interfaceEnabled);
            var tempContext = new DcsEntities(this.ObjectContext.Connection.ConnectionString);

            var partsSalesOrderDetails = this.ObjectContext.PartsSalesOrderDetails.Where(v => v.PartsSalesOrderId == partsSalesOrder.Id).ToArray();
            if(partsSalesOrder.SalesCategoryName == "随车行" || partsSalesOrder.SalesCategoryName == "欧曼电商") {
                StringBuilder sb1 = new StringBuilder();
                sb1.Append("<DATA>");
                sb1.Append("<HEAD>");
                sb1.Append("<BIZTRANSACTIONID>PMS_SYC_150_" + DateTime.Now.ToString("yyyyMMddhhmmssfff") + "</BIZTRANSACTIONID>");
                sb1.Append("<COUNT>1</COUNT>");
                sb1.Append("<CONSUMER></CONSUMER>");
                sb1.Append("<SRVLEVEL>1</SRVLEVEL>");
                sb1.Append("<ACCOUNT></ACCOUNT>");
                sb1.Append("<PASSWORD></PASSWORD>");
                sb1.Append("</HEAD>");
                sb1.Append("<LIST>");
                sb1.Append("<ITEM>");
                sb1.Append("<orderNumber>" + partsSalesOrder.ERPSourceOrderCode + "</orderNumber>");
                sb1.Append("<Code>" + partsSalesOrder.Code + "</Code>");
                sb1.Append("<status>" + partsSalesOrder.Status + "</status>");
                sb1.Append("<orderPattern>" + (partsSalesOrder.IfDirectProvision == true ? "S" : "W") + "</orderPattern>");
                sb1.Append("<remark>" + partsSalesOrder.ApprovalComment + "</remark>");
                sb1.Append("<orderList>");
                foreach(var partsSalesOrderDetail in partsSalesOrderDetails) {
                    sb1.Append("<ITEM>");
                    sb1.Append("<goodCode>" + partsSalesOrderDetail.SparePartCode + "</goodCode>");
                    sb1.Append("<Quantity>" + partsSalesOrderDetail.OrderedQuantity + "</Quantity>");
                    sb1.Append("<ApproveQuantity>" + partsSalesOrderDetail.ApproveQuantity + "</ApproveQuantity>");
                    sb1.Append("</ITEM>");
                }
                sb1.Append("</orderList>");
                sb1.Append("</ITEM>");
                sb1.Append("</LIST>");
                sb1.Append("</DATA>");
                var listXml = sb1.ToString();
                var listPartsSalesOrder = listXml;
                try {
                    using(TransactionScope scope = new TransactionScope(TransactionScopeOption.RequiresNew)) {
                        //var nccesb = new ESBLogDianShang();
                        //nccesb.InterfaceName = "RetailerOrderStatus";
                        //nccesb.Message = interfaceEnabled.ToString() + listPartsSalesOrder;
                        //nccesb.SyncNumberBegin = partsSalesOrder.Id;
                        //nccesb.SyncNumberEnd = -1;
                        //nccesb.TheDate = DateTime.Now;
                        //tempContext.ESBLogDianShangs.AddObject(nccesb);
                        //tempContext.SaveChanges();
                        //InsertToDatabase(nccesb);
                        //this.ObjectContext.SaveChanges();
                        scope.Complete();
                    }
                } catch(Exception e) {
                    using(TransactionScope scope = new TransactionScope(TransactionScopeOption.RequiresNew)) {
                        //var nccesb = new ESBLogDianShang();
                        //nccesb.InterfaceName = "RetailerOrderStatus";
                        //nccesb.Message = interfaceEnabled.ToString() + e.Message + "----" + listPartsSalesOrder;
                        //nccesb.SyncNumberBegin = partsSalesOrder.Id;
                        //nccesb.SyncNumberEnd = -1;
                        //nccesb.TheDate = DateTime.Now;
                        //tempContext.ESBLogDianShangs.AddObject(nccesb);
                        //tempContext.SaveChanges();
                        //InsertToDatabase(nccesb);
                        //this.ObjectContext.SaveChanges();
                        string s = e.Message;
                        scope.Complete();
                    }
                }
            }
        }

        public IQueryable<PartsSalesOrder> 查询出口订单(string sparePartCode) {
            if(string.IsNullOrEmpty(sparePartCode)) {
                return ObjectContext.PartsSalesOrders.OrderByDescending(r => r.Id);
            } else {
                IQueryable<PartsSalesOrder> partsSalesOrders = ObjectContext.PartsSalesOrders.Where(r => r.PartsSalesOrderDetails.Any(o => o.SparePartCode.IndexOf(sparePartCode) >= 0));
                return partsSalesOrders.OrderByDescending(r => r.Id);
            }
        }
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               


        public void InsertPartsSalesOrder(PartsSalesOrder partsSalesOrder) {
            new PartsSalesOrderAch(this).InsertPartsSalesOrder(partsSalesOrder);
        }

        public void UpdatePartsSalesOrder(PartsSalesOrder partsSalesOrder) {
            new PartsSalesOrderAch(this).UpdatePartsSalesOrder(partsSalesOrder);
        }

        public PartsSalesOrder GetPartsSalesOrderWithPartsSalesOrderDetails(int id) {
            return new PartsSalesOrderAch(this).GetPartsSalesOrderWithPartsSalesOrderDetails(id);
        }

        public PartsSalesOrder GetPartsSalesOrderWithPartsSalesOrderDetailsAndCustomerAccount(int id) {
            return new PartsSalesOrderAch(this).GetPartsSalesOrderWithPartsSalesOrderDetailsAndCustomerAccount(id);
        }

        public PartsSalesOrder GetPartsSalesOrderWithPartsSalesOrderDetailsAndCustomerAccountForEditDetail(int id) {
            return new PartsSalesOrderAch(this).GetPartsSalesOrderWithPartsSalesOrderDetailsAndCustomerAccountForEditDetail(id);
        }

        public IQueryable<PartsSalesOrder> 查询可退货销售订单(int submitCompanyId) {
            return new PartsSalesOrderAch(this).查询可退货销售订单(submitCompanyId);
        }

        public IQueryable<PartsSalesOrderDetail> GetPartsSalesOrderDetailInfo() {
            return new PartsSalesOrderAch(this).GetPartsSalesOrderDetailInfo();
        }

        public IQueryable<PartsSalesOrder> 根据审批人员查询销售订单(int personnelId) {
            return new PartsSalesOrderAch(this).根据审批人员查询销售订单(personnelId);
        }

        public IQueryable<PartsSalesOrder> 根据审批人员查询销售订单省市(int personnelId, int? provinceName, int? cityName, string sparePartCode) {
            return new PartsSalesOrderAch(this).根据审批人员查询销售订单省市(personnelId, provinceName, cityName, sparePartCode);
        }
        public IQueryable<PartsSalesOrder> 根据审批人员查询销售订单省市代理库(int personnelId, int? provinceName, int? cityName) {
            return new PartsSalesOrderAch(this).根据审批人员查询销售订单省市代理库(personnelId, provinceName, cityName);
        }

        public IEnumerable<VehiclePartsSalesOrdercs> 根据登陆人员查询销售订单(DateTime submitTimeBegin, DateTime submitTimeEnd, string erpSourceOrderCode, int? salesUnitOwnerCompanyId) {
            return new PartsSalesOrderAch(this).根据登陆人员查询销售订单(submitTimeBegin, submitTimeEnd, erpSourceOrderCode, salesUnitOwnerCompanyId);
        }

        public PartsSalesOrder GetPartsSalesOrderWithDetailsById(int id) {
            return new PartsSalesOrderAch(this).GetPartsSalesOrderWithDetailsById(id);
        }

        public PartsSalesOrder GetPartsSalesOrderByIdAndOriginalRequirementBillId(int originalRequirementBillId, int originalRequirementBillType) {
            return new PartsSalesOrderAch(this).GetPartsSalesOrderByIdAndOriginalRequirementBillId(originalRequirementBillId, originalRequirementBillType);
        }

        public decimal 查询未审核订单金额(int submitCompanyId, int salesUnitId, int partsSalesCategoryId) {
            return new PartsSalesOrderAch(this).查询未审核订单金额(submitCompanyId, salesUnitId, partsSalesCategoryId);
        }

        public IQueryable<PartsSalesOrder> GetPartsSalesOrderHaveOutBound() {
            return new PartsSalesOrderAch(this).GetPartsSalesOrderHaveOutBound();
        }

        public PartsSalesOrder GetPartsSalesOrderById(int id) {
            return new PartsSalesOrderAch(this).GetPartsSalesOrderById(id);
        }

        public IQueryable<PartsSalesOrder> GetPartsSalesOrderWithWarehouse(string sparePartCode, string sparePartName) {
            return new PartsSalesOrderAch(this).GetPartsSalesOrderWithWarehouse(sparePartCode, sparePartName);
        }


        public IQueryable<PartsSalesOrder> 配件索赔查询销售订单(int sparePartId) {
            return new PartsSalesOrderAch(this).配件索赔查询销售订单(sparePartId);
        }

        public IQueryable<PartsSalesOrder> 查询配件索赔销售订单(int submitCompanyId, int partid) {
            return new PartsSalesOrderAch(this).查询配件索赔销售订单(submitCompanyId, partid);
        }

        public IQueryable<PartsSalesOrder> 配件公司人员查询销售订单(int personnelId) {
            return new PartsSalesOrderAch(this).配件公司人员查询销售订单(personnelId);
        }

        public IEnumerable<VirtualPartsSalesPriceWithStock> 根据销售类型查询配件销售价带库存(int partsSalesCategoryId, int? partsSalesOrderTypeId, int customerId, int warehouseId) {
            return new PartsSalesOrderAch(this).根据销售类型查询配件销售价带库存(partsSalesCategoryId, partsSalesOrderTypeId, customerId, warehouseId);
        }

        public IQueryable<PartsSalesOrder> 查询出口订单(string sparePartCode) {
            return new PartsSalesOrderAch(this).查询出口订单(sparePartCode);
        }
    }
}
