﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
using System.Configuration;
using System.Data;
using System.Text;
using Devart.Data.Oracle;
using NPOI.SS.Formula.Functions;
using System.Transactions;
using System.Data.Objects.SqlClient;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsSalesOrderFinishReportAch : DcsSerivceAchieveBase {
        public PartsSalesOrderFinishReportAch(DcsDomainService domainService)
            : base(domainService) {
        }
        public IEnumerable<PartsSalesOrderFinishReport> GetPartsSalesOrderFinishReports(string code, string referenceCode, DateTime? bCreateTime, DateTime? eCreateTime, DateTime? bApproveTime, DateTime? eApproveTime, DateTime? bPickingFinishTime, DateTime? ePickingFinishTime, DateTime? bShippingDate, DateTime? eShippingDate, int? warehouseId, string submitCompanyName, string sparePartCode, int? status, string partsOutboundBillCode, string contractCode, string partsPurchaseOrderCode, string marketingDepartmentName, DateTime? bSubmitTime, DateTime? eSubmitTime, int? partsSalesOrderTypeId) {
            string SQL = @"select tt.*,
       (tt.ApproveQuantity - nvl(tt.OutboundAmount, 0)) as DifferenceQuantity
  from (select p.id,
               po.id as PartsSalesOrderId,
               po.warehouseid,
               w.name as WarehouseName,
               po.province,
               po.submitcompanycode,
               po.submitcompanyname,
               sp.referencecode,
               p.sparepartcode,
               p.sparepartname,
               sp.englishname,
               b.partabc,
               b.PurchaseRoute,
               nvl(pspd.OrderedQuantity, p.orderedquantity) as OrderedQuantity,
               pspd.CURRENTFULFILLEDQUANTITY as ApproveQuantity,
               pspd.OrderedQuantity - pspd.CURRENTFULFILLEDQUANTITY as UnApproveQuantity,
               0 as PickingQty,
               nvl((select Sum(nvl(OutboundAmount, 0))
                     from PartsOutboundBillDetail pd
                     join partsoutboundbill tmp
                       on tmp.originalrequirementbilltype = 1
                      and pd.partsoutboundbillid = tmp.id
                    WHERE pd.sparepartid = p.sparepartid
                      and tmp.OriginalRequirementBillId = po.id),
                   0) as OutboundAmount,
               pr.groupname,
               p.OrderPrice,
               p.OrderPrice * p.OrderedQuantity as OrderSum,
               p.originalprice,
               p.originalprice * p.OrderedQuantity as OriginalPriceSum,
               sp.weight * p.orderedquantity as weight,
               sp.volume * p.orderedquantity as volume,
               po.code,
               po.status,
               nvl(pspd.createtime, po.approvetime) as approvetime,
               po.Remark,
               null as pickingfinishtime,
               (select ph.ShippingDate
                  from PartsShippingOrder ph
                  join Partsshippingorderdetail psd
                    on psd.partsshippingorderid = ph.id
                  join partsoutboundplan tmp
                    on tmp.id = psd.partsoutboundplanid
                   and tmp.outboundtype = 1
                 where psd.sparepartid = p.sparepartid
                   and rownum = 1
                   and tmp.SOURCEID = po.id
                   and ph.status <> 99) as ShippingDate,
                (select ph.createtime
                  from PartsShippingOrder ph
                  join Partsshippingorderdetail psd
                    on psd.partsshippingorderid = ph.id
                  join partsoutboundplan tmp
                    on tmp.id = psd.partsoutboundplanid
                   and tmp.outboundtype = 1
                 where psd.sparepartid = p.sparepartid
                   and rownum = 1
                   and tmp.SOURCEID = po.id
                   and ph.status <> 99) as ShippingCreatTime,
               po.PartsSalesOrderTypeName,po.PartsSalesOrderTypeId,
               po.ContractCode,
               po.createtime,
               (select p1.code
                  from PartsOutboundBill p1
                  join Partsoutboundbilldetail p2
                    on p1.id = p2.partsoutboundbillid
                 where p1.OriginalRequirementBillId = po.id
                   and p1.outboundtype = 1
                   and p1.storagecompanytype = 1
                   and p2.sparepartid = p.sparepartid
                   and rownum = 1) as PartsOutboundBillCode,
               (select pu.status
                  from partsoutboundplan pu
                  join partsoutboundplandetail pts
                    on pu.id = pts.partsoutboundplanid
                 where pts.sparepartid = p.sparepartid
                   and pu.sourceid = po.id
                   and pu.outboundtype = 1
                   and rownum = 1) as partsoutboundplanStatus,
               (case
                 when cp.type = 3 then
                  (select mt.Name
                     from MarketingDepartment mt
                     join Agency ag
                       on mt.id = ag.marketingdepartmentid
                    where ag.id = cp.id)
                 when cp.type = 2 or cp.type = 7 then
                  (select mt.Name
                     from MarketingDepartment mt
                     join DealerServiceInfo ag
                       on mt.id = ag.marketingdepartmentid
                    where ag.DealerId = cp.id)
                 else
                  null
               end) as MarketingDepartmentName,
               pspd.code as PartsSalesOrderProcessCode,
               Rownum,
               po.IfDirectProvision,
               pspd.OrderProcessMethod,
               pspd.time,
               (select tmp.CREATETIME
                  from partssalesorderprocess tmp
                 where tmp.ORIGINALSALESORDERID = po.id
                   and tmp.time = 1) firstapprovetime,
               po.ReceivingCompanyName,po.SubmitTime
          from partssalesorder po
         inner join partssalesorderdetail p
            on p.partssalesorderid = po.id
          left join (select psp.time,
                           psp.code,
                           psp.originalsalesorderid,
                           psp.createtime,
                           pspd.orderedquantity,
                           pspd.currentfulfilledquantity,
                           pspd.sparepartid,
                           pspd.OrderProcessMethod
                      from partssalesorderprocess psp
                     inner join partssalesorderprocessdetail pspd
                        on psp.id = pspd.partssalesorderprocessid) pspd
            on pspd.originalsalesorderid = po.id
           and p.sparepartid = pspd.sparepartid
         inner join warehouse w
            on po.warehouseid = w.id
           and w.storagecompanytype = 1
          join sparepart sp
            on p.sparepartid = sp.id
          join partsbranch b
            on p.sparepartid = b.partid
           and b.status = 1
          left join PartsSalePriceIncreaseRate pr
            on b.IncreaseRateGroupId = pr.id
          join company cp
            on po.SubmitCompanyId = cp.id        
         where p.orderedquantity > 0   and po.status in (1, 2, 4, 5, 6)";
            if(!string.IsNullOrEmpty(code)) {
                SQL = SQL + " and LOWER(po.code) like '%" + code.ToLower() + "%'";
            }
            if(!string.IsNullOrEmpty(referenceCode)) {
                SQL = SQL + " and LOWER(sp.referenceCode) like '%" + referenceCode.ToLower() + "%'";
            }
            if(warehouseId.HasValue) {
                SQL = SQL + " and po.warehouseId=" + warehouseId.Value;
            }
            if(!string.IsNullOrEmpty(submitCompanyName)) {
                SQL = SQL + " and LOWER(po.submitCompanyName) like '%" + submitCompanyName.ToLower() + "%'";
            }
            if(!string.IsNullOrEmpty(sparePartCode)) {
                SQL = SQL + " and LOWER(p.sparePartCode) like '%" + sparePartCode.ToLower() + "%'";
            }
            if(partsSalesOrderTypeId.HasValue) {
                SQL = SQL + " and po.partsSalesOrderTypeId=" + partsSalesOrderTypeId.Value;
            }
            if(!string.IsNullOrEmpty(contractCode)) {
                SQL = SQL + " and LOWER(po.contractCode) like '%" + contractCode.ToLower() + "%'";
            }
            if(status.HasValue) {
                SQL = SQL + " and po.status=" + status.Value;
            }
            if(!string.IsNullOrEmpty(partsPurchaseOrderCode)) {
                SQL = SQL + " and exists (select 1 from partsPurchaseOrder pp join partspurchaseorderdetail ppd  on pp.id = ppd.partspurchaseorderid  where pp.OriginalRequirementBillCode = po.code and ppd.sparepartid = p.sparepartid and  LOWER(pp.code)  like '%" + partsPurchaseOrderCode.ToLower() + "%')";
            }
            if(bShippingDate.HasValue && bShippingDate.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
                bShippingDate = null;
            }
            if(eShippingDate.HasValue && eShippingDate.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
                eShippingDate = null;
            }
            if(bPickingFinishTime.HasValue && bPickingFinishTime.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
                bPickingFinishTime = null;
            }
            if(bCreateTime.HasValue && bCreateTime.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
                bCreateTime = null;
            }
            if(bApproveTime.HasValue && bApproveTime.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
                bApproveTime = null;
            }
            if(eApproveTime.HasValue && eApproveTime.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
                eApproveTime = null;
            }
            if(eSubmitTime.HasValue && eSubmitTime.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
                eSubmitTime = null;
            }
            if(bCreateTime.HasValue) {
                SQL = SQL + " and po.CreateTime>= to_date('" + bCreateTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if(eCreateTime.HasValue) {
                SQL = SQL + " and po.CreateTime<= to_date('" + eCreateTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if(bSubmitTime.HasValue) {
                SQL = SQL + " and po.SubmitTime>= to_date('" + bSubmitTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if(eSubmitTime.HasValue) {
                SQL = SQL + " and po.SubmitTime<= to_date('" + eSubmitTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if(bApproveTime.HasValue && !eApproveTime.HasValue) {
               // SQL = SQL + " and nvl(pspd.createtime, po.approvetime)>= to_date('" + bApproveTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
                SQL = SQL + "  and exists  (select 1 from partssalesorderprocess psop where psop.originalsalesorderid = po.id and psop.Time=1 and psop.CreateTime >=  to_date('" + bApproveTime.Value.ToString() + "', 'yyyy-MM-dd HH24:mi:ss'))";

            }
            if(!bApproveTime.HasValue && eApproveTime.HasValue) {
                SQL = SQL + " and exists  (select 1 from partssalesorderprocess psop where psop.originalsalesorderid = po.id  and psop.Time = 1  and psop.CreateTime <= to_date('" + eApproveTime.Value.ToString() + "', 'yyyy-MM-dd HH24:mi:ss'))";
            }
            if(bApproveTime.HasValue && eApproveTime.HasValue) {
               // SQL = SQL + " and   nvl(pspd.createtime, po.approvetime)<= to_date('" + eApproveTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
                SQL = SQL + " and exists  (select 1 from partssalesorderprocess psop where psop.originalsalesorderid = po.id  and psop.Time=1 and psop.CreateTime >=  to_date('" + bApproveTime.Value.ToString() + "', 'yyyy-MM-dd HH24:mi:ss') and psop.CreateTime <= to_date('" + eApproveTime.Value.ToString() + "', 'yyyy-MM-dd HH24:mi:ss'))";

            }

            if(bShippingDate.HasValue && !eShippingDate.HasValue) {
                SQL = SQL + "  and exists(select 1 from PartsShippingOrder ph join Partsshippingorderdetail psd on psd.partsshippingorderid = ph.id  join partsoutboundplan tmp  on tmp.id = psd.partsoutboundplanid and tmp.outboundtype = 1  where psd.sparepartid = p.sparepartid  and tmp.SOURCEID = po.id  and ph.status <> 99  and ph.ShippingDate >= to_date('" + bShippingDate.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss'))";
            }
            if(eShippingDate.HasValue && !bShippingDate.HasValue) {
                SQL = SQL + "  and exists(select 1 from PartsShippingOrder ph join Partsshippingorderdetail psd on psd.partsshippingorderid = ph.id  join partsoutboundplan tmp  on tmp.id = psd.partsoutboundplanid and tmp.outboundtype = 1  where psd.sparepartid = p.sparepartid  and tmp.SOURCEID = po.id  and ph.status <> 99  and ph.ShippingDate <= to_date('" + eShippingDate.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss'))";

            }
            if(eShippingDate.HasValue && bShippingDate.HasValue) {
                SQL = SQL + "  and exists(select 1 from PartsShippingOrder ph join Partsshippingorderdetail psd on psd.partsshippingorderid = ph.id  join partsoutboundplan tmp  on tmp.id = psd.partsoutboundplanid and tmp.outboundtype = 1  where psd.sparepartid = p.sparepartid  and tmp.SOURCEID = po.id  and ph.status <> 99  and ph.ShippingDate <= to_date('" + eShippingDate.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss') and ph.ShippingDate >= to_date('" + bShippingDate.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss'))";

            }

            SQL = SQL + ") tt where 1 = 1";

            if(!string.IsNullOrEmpty(marketingDepartmentName)) {
                SQL = SQL + " and tt.marketingDepartmentName like '%" + marketingDepartmentName.ToLower() + "%'";
            }

            if(bPickingFinishTime.HasValue) {
                SQL = SQL + " and tt.Pickingfinishtime>= to_date('" + bPickingFinishTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if(ePickingFinishTime.HasValue) {
                SQL = SQL + "  and tt.Pickingfinishtime<= to_date('" + ePickingFinishTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }

          
            if(!string.IsNullOrEmpty(partsOutboundBillCode)) {
                SQL = SQL + " and LOWER(tt.partsOutboundBillCode) like '%" + partsOutboundBillCode.ToLower() + "%'";
            }
            var search = ObjectContext.ExecuteStoreQuery<PartsSalesOrderFinishReport>(SQL).ToList();
            return search;
        }
        //中心库销售订单完成情况
        public IEnumerable<PartsSalesOrderFinishReport> GetCentralPartsSalesOrderFinishs(string code, string referenceCode, DateTime? bCreateTime, DateTime? eCreateTime, DateTime? bApproveTime, DateTime? eApproveTime, DateTime? bShippingDate, DateTime? eShippingDate, int? warehouseId, string submitCompanyName, string sparePartCode, int? status, string partsOutboundBillCode, string contractCode, string partsPurchaseOrderCode, string marketingDepartmentName, int? retailStatus, DateTime? bSubmitTime, DateTime? eSubmitTime,int? partsSalesOrderTypeId) {
            //查询当前登录人所属公司性质，若是代理库，则只查询其下属区域内的订单
            var userInfo = Utils.GetCurrentUserInfo();

            var company = ObjectContext.Companies.Where(r => r.Id == userInfo.EnterpriseId).FirstOrDefault();        
            string SQL = @"select yy.*,rownum from ( select tt.*,
       (tt.ApproveQuantity - nvl(tt.OutboundAmount, 0)) as DifferenceQuantity
  from (select (select mt.Name
                  from MarketingDepartment mt
                  join Agency ag
                    on mt.id = ag.marketingdepartmentid
                 where ag.id = po.SalesUnitOwnerCompanyId) as MarketingDepartmentName,
               po.warehousename,po.warehouseId,
               null as Province,
               null as SubmitCompanyCode,
               po.CustomerName as submitcompanyname,
               null as ReceivingCompanyName,
               pd.sparepartcode,
               sp.referencecode,
               pd.sparepartname,
               pb.PartABC,
               sp.englishname,
               pb.PurchaseRoute,
               null as IfDirectProvision,
               null as OrderProcessMethod,
               pd.quantity as OrderedQuantity,
               decode(po.status, 2, pd.quantity, 3, pd.quantity, 0) as ApproveQuantity,
               decode(po.status, 1, pd.quantity, 0) as UnApproveQuantity,
               0 as PickingQty,
               (select sum(p2.OutboundAmount)
                  from PartsOutboundBill p1
                  join Partsoutboundbilldetail p2
                    on p1.id = p2.partsoutboundbillid
                 where p1.OriginalRequirementBillId = po.id
                   and p1.OriginalRequirementBillCode = po.code
                   and p2.sparepartid = pd.sparepartid) as OutboundAmount,
               pr.groupname,
               pd.DiscountedPrice as OrderPrice,
               pd.DiscountedAmount as OrderSum,
               pd.SalesPrice as originalprice,
               pd.SalesPrice * pd.Quantity as OriginalPriceSum,
               sp.weight * pd.quantity as weight,
               sp.volume * pd.quantity as volume,
               null as PartsSalesOrderProcessCode,
               po.code, kv.value as StatusStr,
               null as time,
               (select pu.status
                  from partsoutboundplan pu
                  join partsoutboundplandetail pts
                    on pu.id = pts.partsoutboundplanid                 
                 where pts.sparepartid = pd.sparepartid
                   and pu.sourceid = po.id
                   and pu.sourcecode = po.code
                   and rownum = 1) as PartsOutboundPlanStatus,
               po.approvetime,
               po.remark,
               po.ApproveTime as firstapprovetime,
               cast('零售订单' as varchar2(100)) as PartsSalesOrderTypeName,
			   null as PartsSalesOrderTypeId,
               null as ContractCode,
               po.createtime,
               (select p1.code
                  from PartsOutboundBill p1
                  join Partsoutboundbilldetail p2
                    on p1.id = p2.partsoutboundbillid
                 where p1.OriginalRequirementBillId = po.id
                   and p1.OriginalRequirementBillCode = po.code
                   and p2.sparepartid = pd.sparepartid
                   and rownum = 1) as PartsOutboundBillCode,
               (select ph.ShippingDate
                  from PartsShippingOrder ph
                  join Partsshippingorderdetail psd
                    on psd.partsshippingorderid = ph.id
                  join partsoutboundplan tmp
                    on tmp.id = psd.partsoutboundplanid
                   and tmp.outboundtype = 6
                 where psd.sparepartid = pd.sparepartid
                   and rownum = 1
                   and tmp.originalrequirementbillid = po.id
                   and ph.status <> 99) as ShippingDate,
               (select ph.createtime
                  from PartsShippingOrder ph
                  join Partsshippingorderdetail psd
                    on psd.partsshippingorderid = ph.id
                  join partsoutboundplan tmp
                    on tmp.id = psd.partsoutboundplanid
                   and tmp.outboundtype = 6
                 where psd.sparepartid = pd.sparepartid
                   and rownum = 1
                   and tmp.originalrequirementbillid = po.id
                   and ph.status <> 99) as ShippingCreatTime,
               po.SalesUnitOwnerCompanyName,null as SubmitTime
          from PartsRetailOrderDetail pd
          join PartsRetailOrder po
            on pd.partsretailorderid = po.id
          join keyvalueitem kv
            on po.status = kv.key
           and kv.name = 'PartsRetailOrder_Status'
         inner join sparepart sp
            on pd.sparepartid = sp.id
          left join partsbranch pb
            on pd.sparepartid = pb.partid
           and pb.status = 1
           and pb.branchid = po.branchid
          left join PartsSalePriceIncreaseRate pr
            on pb.IncreaseRateGroupId = pr.id
         where po.status <> 99 {0})tt  where 1=1 {2}
union all
select tt.*,
       (tt.ApproveQuantity - nvl(tt.OutboundAmount, 0)) as DifferenceQuantity
  from (select (case
                 when cp.type = 3 then
                  (select mt.Name
                     from MarketingDepartment mt
                     join Agency ag
                       on mt.id = ag.marketingdepartmentid
                    where ag.id = cp.id)
                 when cp.type = 2 or cp.type = 7 then
                  (select mt.Name
                     from MarketingDepartment mt
                     join DealerServiceInfo ag
                       on mt.id = ag.marketingdepartmentid
                    where ag.DealerId = cp.id)
                 else
                  null
               end) as MarketingDepartmentName,
               w.name as WarehouseName,po.warehouseId,
               po.province,
               po.submitcompanycode,
               po.submitcompanyname,
               po.ReceivingCompanyName,
               p.sparepartcode,
               sp.referencecode,
               p.sparepartname,
               b.partabc,
               sp.englishname,
               b.PurchaseRoute,
               po.IfDirectProvision,
               pspd.OrderProcessMethod,
               nvl(pspd.OrderedQuantity, p.orderedquantity) as OrderedQuantity,
               pspd.CURRENTFULFILLEDQUANTITY as ApproveQuantity,
               pspd.OrderedQuantity - pspd.CURRENTFULFILLEDQUANTITY as UnApproveQuantity,
               0 as PickingQty,
               nvl((select Sum(nvl(OutboundAmount, 0))
                     from PartsOutboundBillDetail pd
                     join partsoutboundbill tmp
                       on tmp.originalrequirementbilltype = 1
                      and pd.partsoutboundbillid = tmp.id
                    WHERE pd.sparepartid = p.sparepartid
                      and tmp.OriginalRequirementBillId = po.id),
                   0) as OutboundAmount,
               pr.groupname,
               p.OrderPrice,
               p.OrderPrice * p.OrderedQuantity as OrderSum,
               p.originalprice,
               p.originalprice * p.OrderedQuantity as OriginalPriceSum,
               sp.weight * p.orderedquantity as weight,
               sp.volume * p.orderedquantity as volume,
               pspd.code as PartsSalesOrderProcessCode,
               po.code, kv.value as StatusStr,
               pspd.time,
               (select pu.status
                  from partsoutboundplan pu
                  join partsoutboundplandetail pts
                    on pu.id = pts.partsoutboundplanid                 
                 where pts.sparepartid = p.sparepartid
                   and pu.sourceid = po.id
                   and pu.outboundtype = 1
                   and rownum = 1) as PartsOutboundPlanStatus,
               nvl(pspd.createtime, po.approvetime) as approvetime,
               po.Remark, 
               (select tmp.CREATETIME
                  from partssalesorderprocess tmp
                 where tmp.ORIGINALSALESORDERID = po.id
                   and tmp.time = 1) firstapprovetime,
               po.PartsSalesOrderTypeName,
			   po.partsSalesOrderTypeId,
               po.ContractCode,
               po.createtime,
               (select p1.code
                  from PartsOutboundBill p1
                  join Partsoutboundbilldetail p2
                    on p1.id = p2.partsoutboundbillid
                 where p1.OriginalRequirementBillId = po.id
                   and p1.outboundtype = 1
                   and p1.storagecompanytype = 1
                   and p2.sparepartid = p.sparepartid
                   and rownum = 1) as PartsOutboundBillCode,
               (select ph.ShippingDate
                  from PartsShippingOrder ph
                  join Partsshippingorderdetail psd
                    on psd.partsshippingorderid = ph.id
                  join partsoutboundplan tmp
                    on tmp.id = psd.partsoutboundplanid
                   and tmp.outboundtype = 1
                 where psd.sparepartid = p.sparepartid
                   and rownum = 1
                   and tmp.SOURCEID = po.id
                   and ph.status <> 99) as ShippingDate,
               (select ph.createtime
                  from PartsShippingOrder ph
                  join Partsshippingorderdetail psd
                    on psd.partsshippingorderid = ph.id
                  join partsoutboundplan tmp
                    on tmp.id = psd.partsoutboundplanid
                   and tmp.outboundtype = 1
                 where psd.sparepartid = p.sparepartid
                   and rownum = 1
                   and tmp.SOURCEID = po.id
                   and ph.status <> 99) as ShippingCreatTime,
                  po.SalesUnitOwnerCompanyName, po.SubmitTime
          from partssalesorder po
         inner join partssalesorderdetail p
            on p.partssalesorderid = po.id
          left join (select psp.time,
                           psp.code,
                           psp.originalsalesorderid,
                           psp.createtime,
                           pspd.orderedquantity,
                           pspd.currentfulfilledquantity,
                           pspd.sparepartid,
                           pspd.OrderProcessMethod
                      from partssalesorderprocess psp
                     inner join partssalesorderprocessdetail pspd
                        on psp.id = pspd.partssalesorderprocessid) pspd
            on pspd.originalsalesorderid = po.id
           and p.sparepartid = pspd.sparepartid
         inner join warehouse w
            on po.warehouseid = w.id
           and w.storagecompanytype = 3
          join sparepart sp
            on p.sparepartid = sp.id
          join partsbranch b
            on p.sparepartid = b.partid
           and b.status = 1
          left join PartsSalePriceIncreaseRate pr
            on b.IncreaseRateGroupId = pr.id
          join company cp
            on po.SubmitCompanyId = cp.id
    join keyvalueitem kv
            on po.status = kv.key
           and kv.name = 'PartsSalesOrder_Status'
         where p.orderedquantity > 0
           and po.status in (1, 2, 4, 5, 6) {1}) tt where 1=1 {3} )yy";
            var serachSQL = new StringBuilder();
            var serachSQL2 = new StringBuilder();
            var serachSQL3 = new StringBuilder();
            var serachSQL4 = new StringBuilder();
            if(bShippingDate.HasValue && bShippingDate.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
                bShippingDate = null;
            }
            if(eShippingDate.HasValue && eShippingDate.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
                eShippingDate = null;
            }  
            if(bCreateTime.HasValue && bCreateTime.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
                bCreateTime = null;
            }
            if(bApproveTime.HasValue && bApproveTime.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
                bApproveTime = null;
            }
            if(eApproveTime.HasValue && eApproveTime.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
                eApproveTime = null;
            }
            if(eSubmitTime.HasValue && eSubmitTime.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
                eSubmitTime = null;
            }
            if(!string.IsNullOrEmpty(code)) {
                serachSQL.Append(" and LOWER(po.code) like '%" + code.ToLower() + "%'");
                serachSQL2.Append(" and LOWER(po.code) like '%" + code.ToLower() + "%'");
            }
            if(!string.IsNullOrEmpty(referenceCode)) {
                serachSQL.Append(" and LOWER(sp.referenceCode) like '%" + referenceCode.ToLower() + "%'");
                serachSQL2.Append(" and LOWER(sp.referenceCode) like '%" + referenceCode.ToLower() + "%'");
            }           
            if(!string.IsNullOrEmpty(sparePartCode)) {
                serachSQL.Append(" and LOWER( pd.sparePartCode) like '%" + sparePartCode.ToLower() + "%'");
                serachSQL2.Append(" and LOWER( p.sparePartCode ) like '%" + sparePartCode.ToLower() + "%'");
            }
            if(!string.IsNullOrEmpty(submitCompanyName)) {
                serachSQL2.Append(" and LOWER( po.submitCompanyName) like '%" + submitCompanyName.ToLower() + "%'");
                serachSQL.Append(" and LOWER( po.CustomerName) like '%" + submitCompanyName.ToLower() + "%'");
            }
            if(warehouseId.HasValue){
                serachSQL.Append(" and po.warehouseId =" + warehouseId);
                serachSQL2.Append(" and po.warehouseId ="+warehouseId);
            }
			 if(partsSalesOrderTypeId.HasValue) {
				serachSQL2.Append(" and po.partsSalesOrderTypeId=" + partsSalesOrderTypeId.Value );
                serachSQL.Append(" and 1<>1");
            }
            if(status.HasValue) {
                serachSQL2.Append(" and po.status=" + status.Value );
                serachSQL.Append(" and 1<>1");
            }
            if(retailStatus.HasValue) {
                serachSQL.Append(" and po.status=" + retailStatus.Value);
                serachSQL2.Append(" and 1<>1");
            }
            if(!string.IsNullOrEmpty(partsPurchaseOrderCode)) {
                serachSQL2.Append(" and exists (select 1 from partsPurchaseOrder pp join partspurchaseorderdetail ppd  on pp.id = ppd.partspurchaseorderid  where pp.OriginalRequirementBillCode = po.code and ppd.sparepartid = p.sparepartid and  LOWER(pp.code)  like '%" + partsPurchaseOrderCode.ToLower() + "%')");
                serachSQL.Append(" and 1<>1 ");
            }
            if(!string.IsNullOrEmpty(contractCode)) {
                serachSQL2.Append(" and po.contractCode like '%" + contractCode+"%'");
                serachSQL.Append(" and 1<>1 ");
            }
            if(bCreateTime.HasValue) {
                serachSQL.Append(" and po.CreateTime>= to_date('" + bCreateTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')");
                serachSQL2.Append(" and po.CreateTime>= to_date('" + bCreateTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')");
            }
            if(eCreateTime.HasValue) {
                serachSQL.Append(" and po.CreateTime<= to_date('" + eCreateTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')");
                serachSQL2.Append(" and po.CreateTime<= to_date('" + eCreateTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')");
            }
            if(bSubmitTime.HasValue) {
                serachSQL.Append(" and  1<>1 ");
                serachSQL2.Append(" and po.SubmitTime>= to_date('" + bSubmitTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')");
            }
            if(eSubmitTime.HasValue) {
                serachSQL.Append(" and  1<>1 ");
                serachSQL2.Append(" and po.SubmitTime<= to_date('" + eSubmitTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')");
            }
            if(bApproveTime.HasValue) {
                serachSQL.Append(" and po.ApproveTime>= to_date('" + bApproveTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')");
            }
            if(eApproveTime.HasValue) {
                serachSQL.Append(" and po.ApproveTime<= to_date('" + eApproveTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')");
            }
            if(bApproveTime.HasValue && !eApproveTime.HasValue) {
                 serachSQL2.Append( "  and exists  (select 1 from partssalesorderprocess psop where psop.originalsalesorderid = po.id and psop.Time=1 and psop.CreateTime >=  to_date('" + bApproveTime.Value.ToString() + "', 'yyyy-MM-dd HH24:mi:ss'))");
            }
            if(!bApproveTime.HasValue && eApproveTime.HasValue) {
                serachSQL2.Append(" and exists  (select 1 from partssalesorderprocess psop where psop.originalsalesorderid = po.id  and psop.Time = 1  and psop.CreateTime <= to_date('" + eApproveTime.Value.ToString() + "', 'yyyy-MM-dd HH24:mi:ss'))");
            }
            if(bApproveTime.HasValue && eApproveTime.HasValue) {
               serachSQL2.Append(" and exists  (select 1 from partssalesorderprocess psop where psop.originalsalesorderid = po.id  and psop.Time=1 and psop.CreateTime >=  to_date('" + bApproveTime.Value.ToString() + "', 'yyyy-MM-dd HH24:mi:ss') and psop.CreateTime <= to_date('" + eApproveTime.Value.ToString() + "', 'yyyy-MM-dd HH24:mi:ss'))");
            }
            if(company.Type == (int)DcsCompanyType.代理库) {
                serachSQL2.Append(" and  cp.id=" + userInfo.EnterpriseId);
                serachSQL.Append(" and po.SalesUnitOwnerCompanyId=" + userInfo.EnterpriseId);
            }  

            if(!string.IsNullOrEmpty(partsOutboundBillCode)) {
                serachSQL.Append(" and exists (select 1  from PartsOutboundBill p1  join Partsoutboundbilldetail p2  on p1.id = p2.partsoutboundbillid  where p1.OriginalRequirementBillId = po.id   and p1.OriginalRequirementBillCode = po.code  and p2.sparepartid = pd.sparepartid and LOWER( p1.) like '%" + partsOutboundBillCode.ToLower()+"%'");
                serachSQL2.Append(" and exists (select 1  from PartsOutboundBill p1  join Partsoutboundbilldetail p2  on p1.id = p2.partsoutboundbillid  where p1.OriginalRequirementBillId = po.id   and p1.OriginalRequirementBillCode = po.code  and p2.sparepartid = pd.sparepartid and LOWER( p1.) like '%" + partsOutboundBillCode.ToLower() + "%'");

            }
            //if(bShippingDate.HasValue) {
            //    serachSQL3.Append(" and tt.ShippingDate>= to_date('" + bShippingDate.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')");
            //    serachSQL4.Append(" and tt.ShippingDate>= to_date('" + bShippingDate.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')");
            //}
            //if(eShippingDate.HasValue) {
            //    serachSQL3.Append(" and tt.ShippingDate<= to_date('" + eShippingDate.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')");
            //    serachSQL4.Append(" and tt.ShippingDate<= to_date('" + eShippingDate.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')");
            //}
            if(!string.IsNullOrEmpty(marketingDepartmentName)) {
                serachSQL3.Append(" and tt.marketingDepartmentName like '%" + marketingDepartmentName + "%'");
                serachSQL4.Append(" and tt.marketingDepartmentName like '%" + marketingDepartmentName+"%'");
                }

            if(bShippingDate.HasValue && !eShippingDate.HasValue) {
                serachSQL2.Append("  and exists(select 1 from PartsShippingOrder ph join Partsshippingorderdetail psd on psd.partsshippingorderid = ph.id  join partsoutboundplan tmp  on tmp.id = psd.partsoutboundplanid and tmp.outboundtype = 1  where psd.sparepartid = p.sparepartid  and tmp.SOURCEID = po.id  and ph.status <> 99  and ph.ShippingDate >= to_date('" + bShippingDate.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss'))");
               serachSQL.Append("  and exists(select 1 from PartsShippingOrder ph join Partsshippingorderdetail psd on psd.partsshippingorderid = ph.id  join partsoutboundplan tmp on tmp.id = psd.partsoutboundplanid and tmp.outboundtype = 6 where psd.sparepartid = pd.sparepartid  and tmp.originalrequirementbillid = po.id and ph.status <> 99 and ph.createtime >= to_date('" + bShippingDate.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss'))");

            }
            if(eShippingDate.HasValue && !bShippingDate.HasValue) {
                serachSQL2.Append("  and exists(select 1 from PartsShippingOrder ph join Partsshippingorderdetail psd on psd.partsshippingorderid = ph.id  join partsoutboundplan tmp  on tmp.id = psd.partsoutboundplanid and tmp.outboundtype = 1  where psd.sparepartid = p.sparepartid  and tmp.SOURCEID = po.id  and ph.status <> 99  and ph.ShippingDate <= to_date('" + eShippingDate.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss'))");
                serachSQL.Append("  and exists(select 1 from PartsShippingOrder ph join Partsshippingorderdetail psd on psd.partsshippingorderid = ph.id  join partsoutboundplan tmp on tmp.id = psd.partsoutboundplanid and tmp.outboundtype = 6 where psd.sparepartid = pd.sparepartid  and tmp.originalrequirementbillid = po.id and ph.status <> 99 and ph.createtime <= to_date('" + eShippingDate.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss'))");

            }
            if(eShippingDate.HasValue && bShippingDate.HasValue) {
                serachSQL2.Append("  and exists(select 1 from PartsShippingOrder ph join Partsshippingorderdetail psd on psd.partsshippingorderid = ph.id  join partsoutboundplan tmp  on tmp.id = psd.partsoutboundplanid and tmp.outboundtype = 1  where psd.sparepartid = p.sparepartid  and tmp.SOURCEID = po.id  and ph.status <> 99  and ph.ShippingDate <= to_date('" + eShippingDate.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss') and ph.ShippingDate >= to_date('" + bShippingDate.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss'))");
                serachSQL.Append("  and exists(select 1 from PartsShippingOrder ph join Partsshippingorderdetail psd on psd.partsshippingorderid = ph.id  join partsoutboundplan tmp on tmp.id = psd.partsoutboundplanid and tmp.outboundtype = 6 where psd.sparepartid = pd.sparepartid  and tmp.originalrequirementbillid = po.id and ph.status <> 99 and ph.createtime <= to_date('" + eShippingDate.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss') and ph.createtime >= to_date('" + bShippingDate.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss'))");
            }
            SQL = string.Format(SQL, serachSQL, serachSQL2, serachSQL3, serachSQL4);
           
            var search = ObjectContext.ExecuteStoreQuery<PartsSalesOrderFinishReport>(SQL).ToList();
            return search;
        }
        //SIH销售出库单统计
        public IEnumerable<PartsSalesOrderFinishReport> GetCentralPartsSalesOutBoundList(int? warehouseId, DateTime? bShippingDate, DateTime? eShippingDate, string marketingDepartmentName, string province, string code, string partsOutboundBillCode, string sparePartName, string referenceCode, string submitCompanyName, string settlementStatus, int? partsSalesOrderTypeId, int? groupNameId, string sparePartCode) 
        {
            //查询当前登录人所属公司性质，若是代理库，则只查询其下属区域内的订单
            var userInfo = Utils.GetCurrentUserInfo();

            var company = ObjectContext.Companies.Where(r => r.Id == userInfo.EnterpriseId).FirstOrDefault();

            string SQL = @"select * from (select (case
         when cp.type = 3 then
          (select mt.Name
             from MarketingDepartment mt
             join Agency ag
               on mt.id = ag.marketingdepartmentid
            where ag.id = cp.id)
         when cp.type = 2 or cp.type = 7 then
          (select mt.Name
             from MarketingDepartment mt
             join DealerServiceInfo ag
               on mt.id = ag.marketingdepartmentid
            where ag.DealerId = cp.id)
         else
          null
       end) as MarketingDepartmentName,
       pd.id,
       po.code,
       pt.code as PartsOutboundBillCode,
       pt.WarehouseId,
       pt.warehouseName,
       po.province,
       po.submitcompanycode,
       po.submitcompanyname,
       po.CustomerType,
      (case when po.CustomerType=2 then ( select c.name from ChannelCapability c join DealerServiceInfo d on c.id=d.ChannelCapabilityId where d.dealerid=po.SubmitCompanyId) else (select k.value from  keyvalueitem k where k.key=po.customertype and k.name='Company_Type') end) as CustomerTypeStr,
       (select min(a.code)
          from PartsSalesOrderProcess a
          join PartsSalesOrderProcessDetail b
            on a.id = b.partssalesorderprocessid
         where a.OriginalSalesOrderId = po.id
           and b.sparepartid = pd.sparepartid) as PartsSalesOrderProcessCode,
       sp.referencecode,
       pd.sparepartcode,
       pd.sparepartname,
       po.PartsSalesOrderTypeName,
       po.PartsSalesOrderTypeId,
       pb.partabc,
       pd.outboundamount,
       pd.costprice,
       pd.costprice * pd.outboundamount as CostPriceAll,
       pd.originalprice,
       pd.originalprice * pd.outboundamount as OriginalPriceSum,
       case when pd.OriginalPrice > pd.settlementprice then (pd.OriginalPrice - pd.settlementprice)*pd.outboundamount else 0 end as DiscountAmount,
       (case when pd.OriginalPrice > pd.settlementprice  and nvl(pd.settlementprice,0)>0 then
          round((pd.OriginalPrice - pd.settlementprice) / pd.settlementprice * 100,2) else 0 
       end) as DiscountAmountRate,
       pd.settlementprice,
       pd.settlementprice * pd.outboundamount as SettlementpriceAll,
       psd.SalesPrice as SalesPrice,
        psd.SalesPrice * pd.outboundamount as SalesPriceAll,
       pt.CreateTime as  ShippingDate,
       (select sum(b.outboundamount)  as QuantityToSettle
          from partsoutboundbill a 
          inner join partsoutboundbilldetail b on a.id = b.partsoutboundbillid
          where exists ( select 1 from 
                partssalessettlement pss
          join partssalessettlementref pf
            on pf.partssalessettlementid = pss.id and pf.SourceType = 1
         where pss.status != 4
           and pss.status != 99
           and pf.sourceid = a.id
           )and b.id = pd.id ) as QuantityToSettle,
           pt.remark,
            psd.pricetypename groupname,
           pb.IncreaseRateGroupId as groupNameId,
             pss.code as SettlementCode,
               0                    as IsReplace,
               0                    as IsReplenishment,
               Rownum,
               po.ifdirectprovision,
               pss.status as SettlementStatus
            from PartsOutboundBillDetail pd
            join partsoutboundbill pt
            on pd.partsoutboundbillid = pt.id
            join partssalesorder po
            on pt.originalrequirementbillid = po.id
            and pt.originalrequirementbilltype = 1
          join partssalesorderdetail psd on po.id=psd.partssalesorderid and pd.sparepartid= psd.sparepartid
            join SalesUnit su on po.SalesUnitId=su.id
            join company cp
             on po.submitcompanyid = cp.id
             join sparepart sp
             on pd.sparepartid = sp.id
             left join partsbranch pb
             on pd.sparepartid = pb.partid
             and pb.status = 1            
              left join partsshippingorder psh
              on pt.partsshippingorderid = psh.id and psh.status<>99            
           left join partssalessettlementref psf
            on psf.sourceid = pt.id
           and psf.SourceType = 1
           and exists (select 1
                  from partssalessettlement pss
                 where psf.partssalessettlementid = pss.id
                   and pss.status not in(4,99))
          left join partssalessettlement pss
            on psf.partssalessettlementid = pss.id
           and pss.status not in(4,99)
               where 1=1  ";
            if(!string.IsNullOrEmpty(code)) {
                SQL = SQL + " and LOWER(po.code) like '%" + code.ToLower() + "%'";
            }
            if(!string.IsNullOrEmpty(referenceCode)) {
                SQL = SQL + " and LOWER(sp.referenceCode) like '%" + referenceCode.ToLower() + "%'";
            }
            if(!string.IsNullOrEmpty(submitCompanyName)) {
                SQL = SQL + " and LOWER( po.submitCompanyName) like '%" + submitCompanyName.ToLower() + "%'";
            }
          
            if(!string.IsNullOrEmpty(sparePartName)) {
                SQL = SQL + " and LOWER(pd.sparePartName) like '%" + sparePartName.ToLower() + "%'";
            }

            if(warehouseId.HasValue) {
                SQL = SQL + " and pt.warehouseId=" + warehouseId.Value;
            }
            if(groupNameId.HasValue) {
                SQL = SQL + " and pb.IncreaseRateGroupId=" + groupNameId.Value;
            }
            if(!string.IsNullOrEmpty(sparePartCode)) {
                var spareparts = sparePartCode.Split(',');
                if(spareparts.Length == 1) {
                    var sparepartcode = spareparts[0];
                    SQL += " and pd.sparePartCode = '" + sparePartCode + "'";
                } else {
                    for(int i = 0; i < spareparts.Length; i++) {
                        spareparts[i] = "'" + spareparts[i] + "'";
                    }
                    string codes = string.Join(",", spareparts);
                    SQL += " and pd.sparePartCode in (" + codes + ")";
                }
            } 
            if(partsSalesOrderTypeId.HasValue) {
                SQL = SQL + " and po.partsSalesOrderTypeId=" + partsSalesOrderTypeId.Value;
            }
            if(!string.IsNullOrEmpty(province)) {
                SQL = SQL + " and LOWER(po.province) like '%" + province.ToLower() + "%'";
            }
            if(bShippingDate.HasValue && bShippingDate.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
                bShippingDate = null;
            }
            if(eShippingDate.HasValue && eShippingDate.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
                eShippingDate = null;
            }
            if(bShippingDate.HasValue) {
                SQL = SQL + " and pt.CreateTime>= to_date('" + bShippingDate.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if(eShippingDate.HasValue) {
                SQL = SQL + " and pt.CreateTime<= to_date('" + eShippingDate.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
          
            if(!string.IsNullOrEmpty(partsOutboundBillCode)) {
                SQL = SQL + " and LOWER(pt.code) like '%" + partsOutboundBillCode.ToLower() + "%'";
            }
            if(company.Type == (int)DcsCompanyType.分公司) {
                SQL = SQL + " and  su.OwnerCompanyId=" + userInfo.EnterpriseId;
            } else {
                SQL = SQL + " and  po.SubmitCompanyId=" + userInfo.EnterpriseId;
            }
            SQL = SQL + ")tt where 1=1  ";
            if(!string.IsNullOrEmpty(marketingDepartmentName)) {
                SQL = SQL + " and   tt.marketingDepartmentName like '%" + marketingDepartmentName + "%'";
            }
            if(!string.IsNullOrEmpty(settlementStatus)){
             //   SQL = SQL + " and tt.settlementStatus=" + settlementStatus.Value;
                SQL = SQL + " and tt.settlementStatus in (" + settlementStatus + ")";
            }
            var search = ObjectContext.ExecuteStoreQuery<PartsSalesOrderFinishReport>(SQL).ToList();
            return search;
        }
        public IEnumerable<PartsSalesOrderFinishReport> 销售在途统计(DateTime? bApproveTime, DateTime? eApproveTime, DateTime? bShippingDate, DateTime? eShippingDate, string shippingCode, string submitCompanyName, int? submitCompanyType) {
            string SQL = @"select *
                          from (select (case
                                         when cp.type = 3 then
                                          (select mt.Name
                                             from MarketingDepartment mt
                                             join Agency ag
                                               on mt.id = ag.marketingdepartmentid
                                            where ag.id = cp.id)
                                         when cp.type = 2 or cp.type = 7 then
                                          (select mt.Name
                                             from MarketingDepartment mt
                                             join DealerServiceInfo ag
                                               on mt.id = ag.marketingdepartmentid
                                            where ag.DealerId = cp.id)
                                         else
                                          null
                                       end) as MarketingDepartmentName,
                                       w.name as WarehouseName,
                                       po.province,
                                       po.code,
                                       (select min(ps.Code)
                                      from PartsSalesOrderProcess ps
                                      join PartsSalesOrderProcessDetail pss
                                        on ps.id = pss.partssalesorderprocessid
                                     where pss.sparepartid = p.sparepartid
                                       and ps.originalsalesorderid = po.id) as PartsSalesOrderProcessCode,
                                       po.ApproveTime,
                                       po.submitcompanyname,
                                       p.sparepartcode,
                                       p.sparepartname,
                                       po.partssalesordertypename,
                                       pb.PartABC,
                                       p.OrderedQuantity,
                                       p.OrderPrice,
                                       p.OrderPrice * p.orderedquantity as OrderSum,
                                       p.OriginalPrice,
                                       po.RequestedDeliveryTime,
                                       po.remark,
                                      (select max(ph.ShippingDate)
                                          from PartsShippingOrder ph
                                          join Partsshippingorderdetail psd
                                            on psd.partsshippingorderid = ph.id
                                          join partsoutboundplan pl
                                            on psd.partsoutboundplanid = pl.id
                                         where psd.sparepartid = p.sparepartid
                                           and pl.sourceid = po.id
                                           and pl.sourcecode = po.code
                                           and ph.status <> 99) as ShippingDate,
                                       (select max(ph.code)
                                            from PartsShippingOrder ph
                                          join Partsshippingorderdetail psd
                                            on psd.partsshippingorderid = ph.id
                                          join partsoutboundplan pl
                                            on psd.partsoutboundplanid = pl.id
                                         where psd.sparepartid = p.sparepartid
                                           and pl.sourceid = po.id
                                           and pl.sourcecode = po.code
                                           and ph.status <> 99) as ShippingCode,
                                       p.id,
                                       sp.referencecode,
                                       nvl((select Sum(nvl(OutboundAmount, 0))
                                             from PartsOutboundBillDetail pd
                                             join partsoutboundbill pb
                                               on pb.OriginalRequirementBillType = 1
                                              and pd.partsoutboundbillid = pb.id
                                            where pd.sparepartid = p.sparepartid
                                              and pb.OriginalRequirementBillId = po.id),
                                           0) as OutboundAmount,
                                       Rownum,p.ApproveQuantity,
                                   cp.type as submitCompanyType
                                  from partssalesorderdetail p
                                  join partssalesorder po
                                    on p.partssalesorderid = po.id
                                  join SalesUnit su
                                    on po.SalesUnitId = su.id
                                  join company cp
                                    on po.SubmitCompanyId = cp.id
                                  left join warehouse w
                                    on po.warehouseid = w.id
                                  left join partsbranch pb
                                    on p.sparepartid = pb.partid
                                   and po.SalesCategoryId = pb.partssalescategoryid
                                   and pb.branchid = po.branchid
                                   and pb.status = 1
                                  join sparepart sp
                                    on p.sparepartid = sp.id
                                 where po.status in (2, 4, 5,6)  and not exists
                                 (select 1
                                          from partsinboundcheckbill pc
                                         where pc.originalrequirementbillid = po.id
                                           and pc.originalrequirementbilltype = 1)  ";
            if(eApproveTime.HasValue && eApproveTime.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
                eApproveTime = null;
            }
            if(bApproveTime.HasValue && bApproveTime.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
                bApproveTime = null;
            }

            if(submitCompanyType.HasValue) {
                SQL = SQL + " and  cp.type=" + submitCompanyType;
            }
            if(bApproveTime.HasValue) {
                SQL = SQL + " and po.ApproveTime>= to_date('" + bApproveTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if(eApproveTime.HasValue) {
                SQL = SQL + " and   po.ApproveTime<= to_date('" + eApproveTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if(!string.IsNullOrEmpty(submitCompanyName)) {
                SQL = SQL + " and LOWER(po.submitCompanyName) like '%" + submitCompanyName.ToLower() + "%'";
            }
            if(!string.IsNullOrEmpty(shippingCode)) {
                SQL = SQL + " and  (select LOWER(max(ph.code)) from PartsShippingOrder ph join Partsshippingorderdetail psd  on psd.partsshippingorderid = ph.id join partsoutboundplan pl on psd.partsoutboundplanid = pl.id  where psd.sparepartid = p.sparepartid  and pl.sourceid = po.id and pl.sourcecode = po.code and ph.status <> 99) like '%" + shippingCode.ToLower() + "%'";
            }
            if(bShippingDate.HasValue && bShippingDate.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
                bShippingDate = null;
            }
            if(eShippingDate.HasValue && eShippingDate.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
                eShippingDate = null;
            }
            if(bShippingDate.HasValue) {
                SQL = SQL + " and    (select max(ph.ShippingDate)  from PartsShippingOrder ph  join Partsshippingorderdetail psd on psd.partsshippingorderid = ph.id join partsoutboundplan pl on psd.partsoutboundplanid = pl.id   where psd.sparepartid = p.sparepartid  and pl.sourceid = po.id and pl.sourcecode = po.code and ph.status <> 99)>= to_date('" + bShippingDate.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if(eShippingDate.HasValue) {
                SQL = SQL + " and    (select max(ph.ShippingDate)  from PartsShippingOrder ph  join Partsshippingorderdetail psd on psd.partsshippingorderid = ph.id join partsoutboundplan pl on psd.partsoutboundplanid = pl.id   where psd.sparepartid = p.sparepartid  and pl.sourceid = po.id and pl.sourcecode = po.code and ph.status <> 99)<= to_date('" + eShippingDate.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
                 
          
            var userInfo = Utils.GetCurrentUserInfo();

            var company = ObjectContext.Companies.Where(r => r.Id == userInfo.EnterpriseId).FirstOrDefault();
            if(company.Type == (int)DcsCompanyType.分公司) {
                SQL = SQL + " and  su.OwnerCompanyId=" + userInfo.EnterpriseId;
            } else {
                SQL = SQL + " and  po.SubmitCompanyId=" + userInfo.EnterpriseId;
            }
            SQL = SQL + " )tt where 1=1  ";
            
           
            var search = ObjectContext.ExecuteStoreQuery<PartsSalesOrderFinishReport>(SQL).ToList();
            return search;
        }

      
    }
    partial class DcsDomainService {
        public IEnumerable<PartsSalesOrderFinishReport> GetPartsSalesOrderFinishReports(string code, string referenceCode, DateTime? bCreateTime, DateTime? eCreateTime, DateTime? bApproveTime, DateTime? eApproveTime, DateTime? bPickingFinishTime, DateTime? ePickingFinishTime, DateTime? bShippingDate, DateTime? eShippingDate, int? warehouseId, string submitCompanyName, string sparePartCode, int? status, string partsOutboundBillCode, string contractCode, string partsPurchaseOrderCode, string marketingDepartmentName, DateTime? bSubmitTime, DateTime? eSubmitTime, int? partsSalesOrderTypeId) {
            return new PartsSalesOrderFinishReportAch(this).GetPartsSalesOrderFinishReports(code, referenceCode, bCreateTime, eCreateTime, bApproveTime, eApproveTime, bPickingFinishTime, ePickingFinishTime, bShippingDate, eShippingDate, warehouseId, submitCompanyName, sparePartCode, status, partsOutboundBillCode, contractCode, partsPurchaseOrderCode, marketingDepartmentName, bSubmitTime, eSubmitTime, partsSalesOrderTypeId);
        }
        public IEnumerable<PartsSalesOrderFinishReport> GetCentralPartsSalesOrderFinishs(string code, string referenceCode, DateTime? bCreateTime, DateTime? eCreateTime, DateTime? bApproveTime, DateTime? eApproveTime, DateTime? bShippingDate, DateTime? eShippingDate, int? warehouseId, string submitCompanyName, string sparePartCode, int? status, string partsOutboundBillCode, string contractCode, string partsPurchaseOrderCode, string marketingDepartmentName, int? retailStatus, DateTime? bSubmitTime, DateTime? eSubmitTime,int? partsSalesOrderTypeId) {

            return new PartsSalesOrderFinishReportAch(this).GetCentralPartsSalesOrderFinishs(code, referenceCode, bCreateTime, eCreateTime, bApproveTime, eApproveTime, bShippingDate, eShippingDate, warehouseId, submitCompanyName, sparePartCode, status, partsOutboundBillCode, contractCode, partsPurchaseOrderCode, marketingDepartmentName, retailStatus, bSubmitTime, eSubmitTime,partsSalesOrderTypeId);
        }
        public IEnumerable<PartsSalesOrderFinishReport> GetCentralPartsSalesOutBoundList(int? warehouseId, DateTime? bShippingDate, DateTime? eShippingDate, string marketingDepartmentName, string province, string code, string partsOutboundBillCode, string sparePartName, string referenceCode, string submitCompanyName, string settlementStatus, int? partsSalesOrderTypeId, int? groupNameId, string sparePartCode) {
            return new PartsSalesOrderFinishReportAch(this).GetCentralPartsSalesOutBoundList(warehouseId, bShippingDate, eShippingDate, marketingDepartmentName, province, code, partsOutboundBillCode, sparePartName, referenceCode, submitCompanyName, settlementStatus, partsSalesOrderTypeId, groupNameId,sparePartCode);

        }
        public IEnumerable<PartsSalesOrderFinishReport> 销售在途统计(DateTime? bApproveTime, DateTime? eApproveTime, DateTime? bShippingDate, DateTime? eShippingDate, string shippingCode, string submitCompanyName, int? submitCompanyType) {
            return new PartsSalesOrderFinishReportAch(this).销售在途统计(bApproveTime, eApproveTime, bShippingDate, eShippingDate, shippingCode, submitCompanyName, submitCompanyType);

        }
    }
}
