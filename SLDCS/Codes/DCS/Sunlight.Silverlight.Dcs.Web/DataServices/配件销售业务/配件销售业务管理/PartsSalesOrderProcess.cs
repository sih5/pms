﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Web;using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsSalesOrderProcessAch : DcsSerivceAchieveBase {
        internal void InsertPartsSalesOrderProcessValidate(PartsSalesOrderProcess partsSalesOrderProcess) {
            var userInfo = Utils.GetCurrentUserInfo();
            partsSalesOrderProcess.CreatorId = userInfo.Id;
            partsSalesOrderProcess.CreatorName = userInfo.Name;
            partsSalesOrderProcess.CreateTime = DateTime.Now;
            if(string.IsNullOrWhiteSpace(partsSalesOrderProcess.Code) || partsSalesOrderProcess.Code == GlobalVar.ASSIGNED_BY_SERVER)
                partsSalesOrderProcess.Code = CodeGenerator.Generate("PartsSalesOrderProcess", userInfo.EnterpriseCode);
            var branchstrategie = ObjectContext.Branchstrategies.Where(r => ObjectContext.PartsSalesOrders.Any(v => v.Id == partsSalesOrderProcess.OriginalSalesOrderId && v.BranchId == r.BranchId) && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(branchstrategie == null) {
                throw new ValidationException(ErrorStrings.PartsSalesOrderProcess_Validation43);
            }
            var maxInvoiceAmount = branchstrategie.MaxInvoiceAmount;
            var popdSumAmount = partsSalesOrderProcess.PartsSalesOrderProcessDetails.Sum(r => r.OrderPrice * r.CurrentFulfilledQuantity);
            if(popdSumAmount > maxInvoiceAmount) {
                throw new ValidationException(ErrorStrings.PartsSalesOrderProcess_Validation44);
            }
        }
        //按照设计增加步骤
        internal void UpdatePartsSalesOrderProcessValidate(PartsSalesOrderProcess partsSalesOrderProcess) {
        }
        public void InsertPartsSalesOrderProcess(PartsSalesOrderProcess partsSalesOrderProcess) {
            InsertToDatabase(partsSalesOrderProcess);
            var partsSalesOrderProcessDetails = ChangeSet.GetAssociatedChanges(partsSalesOrderProcess, v => v.PartsSalesOrderProcessDetails, ChangeOperation.Insert);
            foreach(PartsSalesOrderProcessDetail partsSalesOrderProcessDetail in partsSalesOrderProcessDetails) {
                InsertToDatabase(partsSalesOrderProcessDetail);
            }
            this.InsertPartsSalesOrderProcessValidate(partsSalesOrderProcess);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               

        public void InsertPartsSalesOrderProcess(PartsSalesOrderProcess partsSalesOrderProcess) {
            new PartsSalesOrderProcessAch(this).InsertPartsSalesOrderProcess(partsSalesOrderProcess);
        }
    }
}
