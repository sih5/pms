﻿using System.Linq;
using Sunlight.Silverlight.Dcs.Web.Entities;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsSalesQueryAch : DcsSerivceAchieveBase {
        public PartsSalesQueryAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public IQueryable<PartsSalesQuery> 配件销售在途查询() {
            var result = from order in ObjectContext.PartsShippingOrders
                         join orderDetail in ObjectContext.PartsShippingOrderDetails on order.Id equals orderDetail.PartsShippingOrderId
                         join branch in ObjectContext.Branches on order.BranchId equals branch.Id
                         where order.Status == (int)DcsPartsShippingOrderStatus.新建 && (order.ShippingCompanyCode == "2230" || order.ShippingCompanyCode == "2470" || order.ShippingCompanyCode == "2450" || order.ShippingCompanyCode == "1101" || order.ShippingCompanyCode == "2601" || order.ShippingCompanyCode == "2290")
                         select new PartsSalesQuery {
                             Id = order.Id,
                             BranchId = order.BranchId,
                             BranchCode = branch.Code,
                             WarehouseId = order.WarehouseId,
                             WarehouseCode = order.WarehouseCode,
                             WarehouseName = order.WarehouseName,
                             ReceivingCompanyId = order.ReceivingCompanyId,
                             ReceivingCompanyCode = order.ReceivingCompanyCode,
                             ReceivingCompanyName = order.ReceivingCompanyName,
                             SparePartId = orderDetail.SparePartId,
                             SparePartCode = orderDetail.SparePartCode,
                             SparePartName = orderDetail.SparePartName,
                             SettlementPrice = orderDetail.SettlementPrice,
                             Qty = orderDetail.ShippingAmount,
                             TotalPrice = orderDetail.SettlementPrice * orderDetail.ShippingAmount
                         };
            return result.OrderBy(r => r.Id);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               

        public IQueryable<PartsSalesQuery> 配件销售在途查询() {
            return new PartsSalesQueryAch(this).配件销售在途查询();
        }
    }
}
