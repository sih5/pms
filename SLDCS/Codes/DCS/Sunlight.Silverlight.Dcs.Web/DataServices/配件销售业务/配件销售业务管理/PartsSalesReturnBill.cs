﻿using System;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsSalesReturnBillAch : DcsSerivceAchieveBase {
        internal void InsertPartsSalesReturnBillValidate(PartsSalesReturnBill partsSalesReturnBill) {
            var userInfo = Utils.GetCurrentUserInfo();
            partsSalesReturnBill.CreatorId = userInfo.Id;
            partsSalesReturnBill.CreatorName = userInfo.Name;
            partsSalesReturnBill.CreateTime = DateTime.Now;
            if(string.IsNullOrWhiteSpace(partsSalesReturnBill.Code) || partsSalesReturnBill.Code == GlobalVar.ASSIGNED_BY_SERVER)
                partsSalesReturnBill.Code = CodeGenerator.Generate("PartsSalesReturnBill", partsSalesReturnBill.SalesUnitOwnerCompanyCode);
        }

        internal void UpdatePartsSalesReturnBillValidate(PartsSalesReturnBill partsSalesReturnBill) {
            var userInfo = Utils.GetCurrentUserInfo();
            partsSalesReturnBill.ModifierId = userInfo.Id;
            partsSalesReturnBill.ModifierName = userInfo.Name;
            partsSalesReturnBill.ModifyTime = DateTime.Now;
        }

        public void InsertPartsSalesReturnBill(PartsSalesReturnBill partsSalesReturnBill) {
            InsertToDatabase(partsSalesReturnBill);
            var partsSalesReturnBillDetails = ChangeSet.GetAssociatedChanges(partsSalesReturnBill, v => v.PartsSalesReturnBillDetails, ChangeOperation.Insert);
            foreach(PartsSalesReturnBillDetail partsSalesReturnBillDetail in partsSalesReturnBillDetails)
                InsertToDatabase(partsSalesReturnBillDetail);
            this.InsertPartsSalesReturnBillValidate(partsSalesReturnBill);
        }

        public void UpdatePartsSalesReturnBill(PartsSalesReturnBill partsSalesReturnBill) {
            partsSalesReturnBill.PartsSalesReturnBillDetails.Clear();
            UpdateToDatabase(partsSalesReturnBill);
            var partsSalesReturnBillDetails = ChangeSet.GetAssociatedChanges(partsSalesReturnBill, v => v.PartsSalesReturnBillDetails);
            foreach(PartsSalesReturnBillDetail partsSalesReturnBillDetail in partsSalesReturnBillDetails) {
                switch(ChangeSet.GetChangeOperation(partsSalesReturnBillDetail)) {
                    case ChangeOperation.Insert:
                        InsertToDatabase(partsSalesReturnBillDetail);
                        break;
                    case ChangeOperation.Update:
                        UpdateToDatabase(partsSalesReturnBillDetail);
                        break;
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(partsSalesReturnBillDetail);
                        break;
                }
            }
            this.UpdatePartsSalesReturnBillValidate(partsSalesReturnBill);
        }

        public PartsSalesReturnBill GetPartsSalesReturnBillWithPartsSalesReturnBillDetails(int id) {
            var partsSalesReturnBill = ObjectContext.PartsSalesReturnBills.SingleOrDefault(r => r.Id == id);
            if(partsSalesReturnBill != null) {
                var partsSalesReturnBillDetails = ObjectContext.PartsSalesReturnBillDetails.Where(r => r.PartsSalesReturnBillId == id).ToArray();
            }
            return partsSalesReturnBill;
        }

        public PartsSalesReturnBill GetPartsSalesReturnBillWithDetailsAndSalesUnit(int id) {
            var partsSalesReturnBill = ObjectContext.PartsSalesReturnBills.SingleOrDefault(r => r.Id == id);
            if(partsSalesReturnBill != null) {
                var partsSalesReturnBillDetails = ObjectContext.PartsSalesReturnBillDetails.Where(r => r.PartsSalesReturnBillId == id).ToArray();
                var salesUnit = ObjectContext.SalesUnits.Where(r => r.Id == partsSalesReturnBill.SalesUnitId && r.Status == (int)DcsBaseDataStatus.有效).ToArray();
            }
            return partsSalesReturnBill;
        }

        public IQueryable<PartsSalesReturnBill> 根据审批人员查询销售退货单(int personnelId, string partsSalesOrderCode) {
            if(string.IsNullOrEmpty(partsSalesOrderCode)){
                var result= ObjectContext.PartsSalesReturnBills.Include("PartsSalesReturnBillDetails.PartsSalesOrder").Where(r => ObjectContext.SalesUnitAffiPersonnels.Any(v => v.PersonnelId == personnelId && v.SalesUnitId == r.SalesUnitId) ).OrderBy(r => r.Id);
                //foreach(var item in result) {
                //    item.IsUplodFile = item.Path != null && item.Path.Length > 0;
                //}
                return result;
            } else {
                var result = ObjectContext.PartsSalesReturnBills.Include("PartsSalesReturnBillDetails.PartsSalesOrder").Where(r => ObjectContext.SalesUnitAffiPersonnels.Any(v => v.PersonnelId == personnelId && v.SalesUnitId == r.SalesUnitId) && ObjectContext.PartsSalesReturnBillDetails.Any(q => q.PartsSalesOrderCode.Contains(partsSalesOrderCode) && q.PartsSalesReturnBillId == r.Id)).OrderBy(r => r.Id);
                //foreach(var item in result) {
                //    item.IsUplodFile = item.Path != null && item.Path.Length > 0;
                //}
                return result;
            }
           
        }

        public PartsSalesReturnBill GetPartsSalesReturnBillsQueryById(int id) {
            return ObjectContext.PartsSalesReturnBills.SingleOrDefault(r => r.Id == id);
        }

        public IQueryable<PartsSalesReturnBill> GetPartsSalesReturnBillsQueryByIds(int[] ids) {
            if(ids == null || !ids.Any())
                return null;
            return ObjectContext.PartsSalesReturnBills.Where(r => ids.Contains(r.Id));
        }

        public IQueryable<PartsSalesReturnBillDetail> GetPartsSalesReturnBillDetailForOrders() {
            return ObjectContext.PartsSalesReturnBillDetails.Include("PartsSalesOrder").OrderBy(r => r.Id);
        }
        public IQueryable<PartsSalesReturnBill> GetPartsSalesReturnBillsReport(string partsSalesOrderCode) {
            if(!string.IsNullOrEmpty(partsSalesOrderCode)){
                return ObjectContext.PartsSalesReturnBills.Where(t=>ObjectContext.PartsSalesReturnBillDetails.Any(p=>p.PartsSalesReturnBillId==t.Id&& p.PartsSalesOrderCode.Contains(partsSalesOrderCode))).OrderBy(e => e.Id);

            } else {
                return ObjectContext.PartsSalesReturnBills.OrderBy(e => e.Id);

            }
        }

    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               

        public void InsertPartsSalesReturnBill(PartsSalesReturnBill partsSalesReturnBill) {
            new PartsSalesReturnBillAch(this).InsertPartsSalesReturnBill(partsSalesReturnBill);
        }

        public void UpdatePartsSalesReturnBill(PartsSalesReturnBill partsSalesReturnBill) {
            new PartsSalesReturnBillAch(this).UpdatePartsSalesReturnBill(partsSalesReturnBill);
        }

        public PartsSalesReturnBill GetPartsSalesReturnBillWithPartsSalesReturnBillDetails(int id) {
            return new PartsSalesReturnBillAch(this).GetPartsSalesReturnBillWithPartsSalesReturnBillDetails(id);
        }

        public PartsSalesReturnBill GetPartsSalesReturnBillWithDetailsAndSalesUnit(int id) {
            return new PartsSalesReturnBillAch(this).GetPartsSalesReturnBillWithDetailsAndSalesUnit(id);
        }

        public IQueryable<PartsSalesReturnBill> GetPartsSalesReturnBillsQueryByIds(int[] ids) {
            return new PartsSalesReturnBillAch(this).GetPartsSalesReturnBillsQueryByIds(ids);
        }

        public IQueryable<PartsSalesReturnBill> 根据审批人员查询销售退货单(int personnelId, string partsSalesOrderCode) {
            return new PartsSalesReturnBillAch(this).根据审批人员查询销售退货单(personnelId, partsSalesOrderCode);
        }

        public PartsSalesReturnBill GetPartsSalesReturnBillsQueryById(int id) {
            return new PartsSalesReturnBillAch(this).GetPartsSalesReturnBillsQueryById(id);
        }

        public IQueryable<PartsSalesReturnBillDetail> GetPartsSalesReturnBillDetailForOrders() {
            return new PartsSalesReturnBillAch(this).GetPartsSalesReturnBillDetailForOrders();
        }
        public IQueryable<PartsSalesReturnBill> GetPartsSalesReturnBillsReport(string partsSalesOrderCode) {
            return new PartsSalesReturnBillAch(this).GetPartsSalesReturnBillsReport(partsSalesOrderCode);
        }
    }
}
