﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PersonSalesCenterLinkAch : DcsSerivceAchieveBase {
        internal void InsertPersonSalesCenterLinkValidate(PersonSalesCenterLink personSalesCenterLink) {
            var idAndSaleCenter = ObjectContext.PersonSalesCenterLinks.Where(v => v.PartsSalesCategoryId == personSalesCenterLink.PartsSalesCategoryId && v.PersonId == personSalesCenterLink.PersonId).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(idAndSaleCenter != null)
                throw new ValidationException(ErrorStrings.personSalesCenterLink_Validation2);
            var userInfo = Utils.GetCurrentUserInfo();
            personSalesCenterLink.CreatorId = userInfo.Id;
            personSalesCenterLink.CreatorName = userInfo.Name;
            personSalesCenterLink.CreateTime = DateTime.Now;
        }
        internal void UpdatePersonSalesCenterLinkValidate(PersonSalesCenterLink personSalesCenterLink) {
            var idAndSaleCenter = ObjectContext.PersonSalesCenterLinks.Where(v => v.PartsSalesCategoryId == personSalesCenterLink.PartsSalesCategoryId && v.PersonId == personSalesCenterLink.PersonId).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(idAndSaleCenter != null)
                throw new ValidationException(ErrorStrings.personSalesCenterLink_Validation2);
            var userInfo = Utils.GetCurrentUserInfo();
            personSalesCenterLink.ModifierId = userInfo.Id;
            personSalesCenterLink.ModifierName = userInfo.Name;
            personSalesCenterLink.ModifyTime = DateTime.Now;
        }
        public void InsertPersonSalesCenterLink(PersonSalesCenterLink personSalesCenterLink) {
            InsertToDatabase(personSalesCenterLink);
            this.InsertPersonSalesCenterLinkValidate(personSalesCenterLink);
        }
        public void UpdatePersonSalesCenterLink(PersonSalesCenterLink personSalesCenterLink) {
            UpdateToDatabase(personSalesCenterLink);
            this.UpdatePersonSalesCenterLinkValidate(personSalesCenterLink);
        }
        internal void DeletePersonSalesCenterLinkValidate(PersonSalesCenterLink personSalesCenterLink) {
        }
        public void DeletePersonSalesCenterLink(PersonSalesCenterLink personSalesCenterLink) {
            DeleteFromDatabase(personSalesCenterLink);
            this.DeletePersonSalesCenterLinkValidate(personSalesCenterLink);
        }
        public IQueryable<PersonSalesCenterLink> GetPersonSalesCenterLinkWithPersonnel() {
            return ObjectContext.PersonSalesCenterLinks.Include("Personnel").Include("PartsSalesCategory").Where(r => r.Status != (int)DcsBaseDataStatus.作废).OrderBy(e => e.Id);
        }
        public IQueryable<PersonSalesCenterLink> GetPersonSalesCenterLinksByPartsSalesCategoryId(int PartsSalesCategoryId) {
            return ObjectContext.PersonSalesCenterLinks.Include("Personnel").Include("PartsSalesCategory").Where(r => r.PartsSalesCategoryId == PartsSalesCategoryId && r.Status != (int)DcsBaseDataStatus.作废).OrderBy(r => r.Id);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               

        public void InsertPersonSalesCenterLink(PersonSalesCenterLink personSalesCenterLink) {
            new PersonSalesCenterLinkAch(this).InsertPersonSalesCenterLink(personSalesCenterLink);
        }

        public void UpdatePersonSalesCenterLink(PersonSalesCenterLink personSalesCenterLink) {
            new PersonSalesCenterLinkAch(this).UpdatePersonSalesCenterLink(personSalesCenterLink);
        }

        public void DeletePersonSalesCenterLink(PersonSalesCenterLink personSalesCenterLink) {
            new PersonSalesCenterLinkAch(this).DeletePersonSalesCenterLink(personSalesCenterLink);
        }

        public IQueryable<PersonSalesCenterLink> GetPersonSalesCenterLinkWithPersonnel() {
            return new PersonSalesCenterLinkAch(this).GetPersonSalesCenterLinkWithPersonnel();
        }
                public IQueryable<PersonSalesCenterLink> GetPersonSalesCenterLinksByPartsSalesCategoryId(int PartsSalesCategoryId) {
            return new PersonSalesCenterLinkAch(this).GetPersonSalesCenterLinksByPartsSalesCategoryId(PartsSalesCategoryId);
        }
    }
}
