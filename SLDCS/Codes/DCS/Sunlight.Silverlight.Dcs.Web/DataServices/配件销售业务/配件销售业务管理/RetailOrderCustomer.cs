﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using System.Text;
using System.Web.Configuration;
using Microsoft.Data.Extensions;
using Quartz.Xml;
using RetailerInterfaceSys;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class RetailOrderCustomerAch : DcsSerivceAchieveBase {

        internal void InsertRetailOrderCustomerValdate(RetailOrderCustomer retailOrderCustomer) {
            var userInfo = Utils.GetCurrentUserInfo();
            retailOrderCustomer.CreatorId = userInfo.Id;
            retailOrderCustomer.CreatorName = userInfo.Name;
            retailOrderCustomer.CreateTime = DateTime.Now;
        }

        internal void UpdateRetailOrderCustomerValdate(RetailOrderCustomer retailOrderCustomer) {

        }

        public void InsertRetailOrderCustomer(RetailOrderCustomer retailOrderCustomer) {
            var userInfo = Utils.GetCurrentUserInfo();
            var ss=ObjectContext.RetailOrderCustomers.Where(r => r.CustomerName == retailOrderCustomer.CustomerName  && r.Address == retailOrderCustomer.Address && r.CustomerCellPhone == retailOrderCustomer.CustomerCellPhone && r.企业Id == userInfo.EnterpriseId).ToArray();
            if(ss.Count()>0) {
                return;
            }
            InsertToDatabase(retailOrderCustomer);
            InsertRetailOrderCustomerValdate(retailOrderCustomer);
        }

        public void UpdateRetailOrderCustomer(RetailOrderCustomer retailOrderCustomer) {
            UpdateToDatabase(retailOrderCustomer);
            UpdateRetailOrderCustomerValdate(retailOrderCustomer);
        }
    
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               

        public void InsertRetailOrderCustomer(RetailOrderCustomer retailOrderCustomer) {
            new RetailOrderCustomerAch(this).InsertRetailOrderCustomer(retailOrderCustomer);
        }

        public void UpdateRetailOrderCustomer(RetailOrderCustomer retailOrderCustomer) {
            new RetailOrderCustomerAch(this).UpdateRetailOrderCustomer(retailOrderCustomer);
        }
    }
}
