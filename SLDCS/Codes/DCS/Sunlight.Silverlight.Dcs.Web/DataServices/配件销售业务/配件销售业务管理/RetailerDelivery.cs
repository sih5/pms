﻿using System.Data.Objects;
using System.Linq;
using System;
using System.ServiceModel.DomainServices.Server;
using System.Web.Configuration;
using Microsoft.Data.Extensions;
using Quartz.Xml;
using RetailerInterfaceSys;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class RetailerDeliveryAch : DcsSerivceAchieveBase {
        public RetailerDeliveryAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public IQueryable<Retailer_Delivery> GetRetailer_DeliveryWithDetails(DateTime? syncTimeBegin, DateTime? syncTimeEnd, int? syncStatus) {
            if(syncTimeBegin.HasValue) {
                syncTimeBegin = Convert.ToDateTime(syncTimeBegin).Date;
            }
            if(syncTimeEnd.HasValue) {
                syncTimeEnd = Convert.ToDateTime(syncTimeEnd).Date.AddDays(1);
            }
            var query = from r in ObjectContext.Retailer_Delivery
                        select new {
                            r
                        };
            var result = query.Select(q => q.r).Where(r => r.Retailer_DeliveryDetail.Any(p => !p.SyncTime.HasValue || (p.SyncTime >= syncTimeBegin && p.SyncTime <= syncTimeEnd))).OrderBy(r => r.Id);
            var ids = DcsDomainService.QueryComposer.Compose(result, ParamQueryable).Cast<Retailer_Delivery>().Select(q => q.Id).ToArray();
            var tempWithLeft = from q in query.Where(p => ids.Contains(p.r.Id))
                               join sp in ObjectContext.PartsSalesOrders on q.r.OrderNumber equals sp.ERPSourceOrderCode into tempTable1
                               from partsSalesOrders in tempTable1.DefaultIfEmpty()
                               join ve in ObjectContext.Warehouses on partsSalesOrders.WarehouseId equals ve.Id into tempTable2
                               from warehouses in tempTable2.DefaultIfEmpty()
                               select new {
                                   Warehouse_Code = warehouses.Code,
                                   Warehouse_Name = warehouses.Name,
                                   CustomerCode = partsSalesOrders.SubmitCompanyCode,
                                   CustomerName = partsSalesOrders.SubmitCompanyName,
                                   q.r,
                               };
            foreach(var tc in tempWithLeft) {
                tc.r.Warehouse_Code = tc.Warehouse_Code;
                tc.r.Warehouse_Name = tc.Warehouse_Name;
                tc.r.CustomerCode = tc.CustomerCode;
                tc.r.CustomerName = tc.CustomerName;
            }
            if(syncStatus.HasValue)
                return result.Include("Retailer_DeliveryDetail").Where(r => r.Retailer_DeliveryDetail.Any(p => p.SyncStatus == syncStatus)).OrderBy(r => r.Id);
            else
                return result.Include("Retailer_DeliveryDetail").OrderBy(r => r.Id);
            //return ObjectContext.Retailer_Delivery.Include("Retailer_DeliveryDetail").Where(r => r.Retailer_DeliveryDetail.Any(p => !p.SyncTime.HasValue || (p.SyncTime >= syncTimeBegin && p.SyncTime <= syncTimeEnd))).OrderBy(r => r.Id);
        }
        public IQueryable<ERPDeliveryDetailVirtual> GetRetailer_DeliveryDetailWithSparePart() {
            var result = from a in ObjectContext.Retailer_DeliveryDetail
                         join b in ObjectContext.SpareParts.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on a.GoodsCode equals b.Code into tempTable
                         from sparePart in tempTable.DefaultIfEmpty()
                         select new ERPDeliveryDetailVirtual {
                             Id = a.Id,
                             Sku_Code = a.GoodsCode,
                             DeliveryId = a.OrderDeliveryId,
                             Item_Name = sparePart.Name,
                             ErrorMessage = a.Message,
                             Qty = a.Quantity
                         };
            return result.OrderBy(r => r.Id);
        }


        public void 手工设置新电商发货单(int[] eRPDeliveryIds) {
            var dbERPDelivery = ObjectContext.Retailer_Delivery.Where(r => eRPDeliveryIds.Contains(r.Id)).SetMergeOption(MergeOption.NoTracking).ToArray();
            var webServiceCalled = new WebServiceCalledByRetailerByMySelf();
            try {
                string pmsdb = "";
                string databaseName = WebConfigurationManager.AppSettings["DataBaseName"];
                switch(databaseName) {
                    case "YXDCS":
                        pmsdb = "YX_PMS";
                        break;
                    case "DCS":
                        pmsdb = "PMS";
                        break;   
                }
                webServiceCalled.随车行重新下发错误销售订单(eRPDeliveryIds, pmsdb);
                var erpDelivery = ObjectContext.Retailer_Delivery.Where(r => eRPDeliveryIds.Contains(r.Id)).ToArray();
                var userInfo = Utils.GetCurrentUserInfo();
                foreach(var item in erpDelivery) {
                    ManualScheduling manual = new ManualScheduling();
                    manual.InterfaceName = "RetailerOrder:" + item.OrderNumber;
                    manual.OldSyncStatus = dbERPDelivery.FirstOrDefault(p => p.Id == item.Id).Retailer_DeliveryDetail.FirstOrDefault().SyncStatus;
                    manual.NewSyncStatus = item.Retailer_DeliveryDetail.FirstOrDefault().SyncStatus;
                    manual.CreatorId = userInfo.Id;
                    manual.CreatorName = userInfo.Name;
                    manual.CreateTime = DateTime.Now;
                    InsertToDatabase(manual);
                }
                ObjectContext.SaveChanges();
            }catch(Exception ex) {
                throw ex;
            }
        }

        public void 更新新电商订单接口日志(Retailer_Delivery retailerDelivery) {
            var dbretailerDelivery = ObjectContext.Retailer_Delivery.Include("Retailer_DeliveryDetail").Where(v => v.Id == retailerDelivery.Id && v.Retailer_DeliveryDetail.All(r => r.SyncStatus == (int)DcsEADeliverySyncStatus.同步失败)).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbretailerDelivery == null)
                throw new ValidationException("只能终止失败状态的单据");
            if(retailerDelivery.Retailer_DeliveryDetail.Any()) {
                foreach(var detail in retailerDelivery.Retailer_DeliveryDetail) {
                    detail.SyncStatus = (int)DcsEADeliverySyncStatus.终止;
                    UpdateToDatabase(detail);
                }
            }

            UpdateToDatabase(retailerDelivery);

        }

    
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               

        public IQueryable<Retailer_Delivery> GetRetailer_DeliveryWithDetails(DateTime? syncTimeBegin, DateTime? syncTimeEnd, int? syncStatus) {
            if(syncTimeBegin.HasValue) {
                syncTimeBegin = Convert.ToDateTime(syncTimeBegin).Date;
            }
            if(syncTimeEnd.HasValue) {
                syncTimeEnd = Convert.ToDateTime(syncTimeEnd).Date.AddDays(1);
            }
            var query = from r in ObjectContext.Retailer_Delivery
                        select new {
                            r
                        };
            var result = query.Select(q => q.r).Where(r => r.Retailer_DeliveryDetail.Any(p => !p.SyncTime.HasValue || (p.SyncTime >= syncTimeBegin && p.SyncTime <= syncTimeEnd))).OrderBy(r => r.Id);
            var ids = QueryComposer.Compose(result, ParamQueryable).Cast<Retailer_Delivery>().Select(q => q.Id).ToArray();
            var tempWithLeft = from q in query.Where(p => ids.Contains(p.r.Id))
                               join sp in ObjectContext.PartsSalesOrders on q.r.OrderNumber equals sp.ERPSourceOrderCode into tempTable1
                               from partsSalesOrders in tempTable1.DefaultIfEmpty()
                               join ve in ObjectContext.Warehouses on partsSalesOrders.WarehouseId equals ve.Id into tempTable2
                               from warehouses in tempTable2.DefaultIfEmpty()
                               select new {
                                   Warehouse_Code = warehouses.Code,
                                   Warehouse_Name = warehouses.Name,
                                   CustomerCode = partsSalesOrders.SubmitCompanyCode,
                                   CustomerName = partsSalesOrders.SubmitCompanyName,
                                   q.r,
                               };
            foreach(var tc in tempWithLeft) {
                tc.r.Warehouse_Code = tc.Warehouse_Code;
                tc.r.Warehouse_Name = tc.Warehouse_Name;
                tc.r.CustomerCode = tc.CustomerCode;
                tc.r.CustomerName = tc.CustomerName;
            }
            if(syncStatus.HasValue)
                return result.Include("Retailer_DeliveryDetail").Where(r => r.Retailer_DeliveryDetail.Any(p => p.SyncStatus == syncStatus)).OrderBy(r => r.Id);
            else
                return result.Include("Retailer_DeliveryDetail").OrderBy(r => r.Id);
            //return ObjectContext.Retailer_Delivery.Include("Retailer_DeliveryDetail").Where(r => r.Retailer_DeliveryDetail.Any(p => !p.SyncTime.HasValue || (p.SyncTime >= syncTimeBegin && p.SyncTime <= syncTimeEnd))).OrderBy(r => r.Id);
        }//保持原有的public方法，NoChangePublic
 
                public IQueryable<ERPDeliveryDetailVirtual> GetRetailer_DeliveryDetailWithSparePart() {
            return new RetailerDeliveryAch(this).GetRetailer_DeliveryDetailWithSparePart();
        }
        

        [Invoke(HasSideEffects = true)]
        public void 手工设置新电商发货单(int[] eRPDeliveryIds) {
            new RetailerDeliveryAch(this).手工设置新电商发货单(eRPDeliveryIds);
        }

        [Update(UsingCustomMethod = true)]
        public void 更新新电商订单接口日志(Retailer_Delivery retailerDelivery) {
            new RetailerDeliveryAch(this).更新新电商订单接口日志(retailerDelivery);
        }
    }
}
