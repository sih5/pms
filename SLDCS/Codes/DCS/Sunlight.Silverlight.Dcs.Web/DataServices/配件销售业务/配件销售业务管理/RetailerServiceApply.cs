﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using System.Text;
using System.Web.Configuration;
using Microsoft.Data.Extensions;
using Quartz.Xml;
using RetailerInterfaceSys;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class RetailerServiceApplyAch : DcsSerivceAchieveBase {
        public RetailerServiceApplyAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public IQueryable<Retailer_ServiceApply> GetRetailer_ServiceApply1() {
            var query = from r in ObjectContext.Retailer_ServiceApply
                select new {
                    r
                };
            var result = query.Select(q => q.r).OrderBy(r => r.Id);
            var ids = DcsDomainService.QueryComposer.Compose(result, ParamQueryable).Cast<Retailer_ServiceApply>().Select(q => q.Id).ToArray();
            var tempWithLeft = from q in query.Where(p => ids.Contains(p.r.Id))
                               join sp in ObjectContext.PartsSalesOrders on q.r.OrderNumber equals sp.ERPSourceOrderCode into tempTable1
                               from partsSalesOrders in tempTable1.DefaultIfEmpty()
                               join ve in ObjectContext.Warehouses on partsSalesOrders.WarehouseId equals ve.Id into tempTable2
                               from warehouses in tempTable2.DefaultIfEmpty()
                               select new {
                                   WarehouseName = warehouses.Name,
                                   CustomerCode = partsSalesOrders.SubmitCompanyCode,
                                   CustomerName = partsSalesOrders.SubmitCompanyName,
                                   q.r,
                               };
            foreach(var tc in tempWithLeft) {
                tc.r.WarehouseName = tc.WarehouseName;
                tc.r.CustomerCode = tc.CustomerCode;
                tc.r.CustomerName = tc.CustomerName;
            }
            return result.OrderBy(r => r.Id);
        }
        public IQueryable<ERPDeliveryDetailVirtual> GetRetailer_ServiceApplyDetailWithSparePart() {
            var result = from a in ObjectContext.Retailer_ServiceApplyDetail
                         join b in ObjectContext.SpareParts.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on a.GoodsCode equals b.Code into tempTable
                         from sparePart in tempTable.DefaultIfEmpty()
                         select new ERPDeliveryDetailVirtual {
                             Id = a.Id,
                             Sku_Code = a.GoodsCode,
                             DeliveryId = a.ServiceApplyId,
                             Item_Name = sparePart.Name,
                             ErrorMessage = a.Message,
                             Qty = a.ReturnQuantity
                         };
            return result.OrderBy(r => r.Id);
        }

        public void 手工设置新电商退货单(int[] eRPDeliveryIds) {
            var dbERPDelivery = ObjectContext.Retailer_ServiceApply.Where(r => eRPDeliveryIds.Contains(r.Id)).SetMergeOption(MergeOption.NoTracking).ToArray();
            var webServiceCalled = new WebServiceCalledByRetailerByMySelf();
            try {
                string pmsdb = "";
                string databaseName = WebConfigurationManager.AppSettings["DataBaseName"];
                switch(databaseName) {
                    case "YXDCS":
                        pmsdb = "YX_PMS";
                        break;
                    case "DCS":
                        pmsdb = "PMS";
                        break;
                }
                webServiceCalled.随车行重新下发错误退货订单(eRPDeliveryIds, pmsdb);
                var erpDelivery = ObjectContext.Retailer_ServiceApply.Where(r => eRPDeliveryIds.Contains(r.Id)).ToArray();
                var userInfo = Utils.GetCurrentUserInfo();
                foreach(var item in erpDelivery) {
                    ManualScheduling manual = new ManualScheduling();
                    manual.InterfaceName = "RetailerOrder:" + item.OrderNumber;
                    manual.OldSyncStatus = dbERPDelivery.FirstOrDefault(p => p.Id == item.Id).SyncStatus;
                    manual.NewSyncStatus = item.SyncStatus;
                    manual.CreatorId = userInfo.Id;
                    manual.CreatorName = userInfo.Name;
                    manual.CreateTime = DateTime.Now;
                    InsertToDatabase(manual);
                }
                ObjectContext.SaveChanges();
            }catch(Exception ex) {
                throw ex;
            }
        }

        public void 更新电商配件退货单接口日志(Retailer_ServiceApply retailerServiceApply) {
            var dbRetailerServiceApply = ObjectContext.Retailer_ServiceApply.Where(v => v.Id == retailerServiceApply.Id && v.SyncStatus == (int)DcsEADeliverySyncStatus.同步失败).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbRetailerServiceApply == null)
                throw new ValidationException("只能终止失败状态的单据");
            if(retailerServiceApply.Retailer_ServiceApplyDetail.Any()) {
                foreach(var detail in retailerServiceApply.Retailer_ServiceApplyDetail) {
                    detail.SyncStatus = (int)DcsEADeliverySyncStatus.终止;
                    UpdateToDatabase(detail);
                }

            }
            retailerServiceApply.SyncStatus = (int)DcsEADeliverySyncStatus.终止;
            UpdateToDatabase(retailerServiceApply);

        }
    
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               

        public IQueryable<Retailer_ServiceApply> GetRetailer_ServiceApply1() {
            var query = from r in ObjectContext.Retailer_ServiceApply
                select new {
                    r
                };
            var result = query.Select(q => q.r).OrderBy(r => r.Id);
            var ids = QueryComposer.Compose(result, ParamQueryable).Cast<Retailer_ServiceApply>().Select(q => q.Id).ToArray();
            var tempWithLeft = from q in query.Where(p => ids.Contains(p.r.Id))
                               join sp in ObjectContext.PartsSalesOrders on q.r.OrderNumber equals sp.ERPSourceOrderCode into tempTable1
                               from partsSalesOrders in tempTable1.DefaultIfEmpty()
                               join ve in ObjectContext.Warehouses on partsSalesOrders.WarehouseId equals ve.Id into tempTable2
                               from warehouses in tempTable2.DefaultIfEmpty()
                               select new {
                                   WarehouseName = warehouses.Name,
                                   CustomerCode = partsSalesOrders.SubmitCompanyCode,
                                   CustomerName = partsSalesOrders.SubmitCompanyName,
                                   q.r,
                               };
            foreach(var tc in tempWithLeft) {
                tc.r.WarehouseName = tc.WarehouseName;
                tc.r.CustomerCode = tc.CustomerCode;
                tc.r.CustomerName = tc.CustomerName;
            }
            return result.OrderBy(r => r.Id);
        }//保持原有的public方法，NoChangePublic
 
                public IQueryable<ERPDeliveryDetailVirtual> GetRetailer_ServiceApplyDetailWithSparePart() {
            return new RetailerServiceApplyAch(this).GetRetailer_ServiceApplyDetailWithSparePart();
        }

        [Invoke(HasSideEffects = true)]
        public void 手工设置新电商退货单(int[] eRPDeliveryIds) {
            new RetailerServiceApplyAch(this).手工设置新电商退货单(eRPDeliveryIds);
        }

        [Update(UsingCustomMethod = true)]
        public void 更新电商配件退货单接口日志(Retailer_ServiceApply retailerServiceApply) {
            new RetailerServiceApplyAch(this).更新电商配件退货单接口日志(retailerServiceApply);
        }
    }
}
