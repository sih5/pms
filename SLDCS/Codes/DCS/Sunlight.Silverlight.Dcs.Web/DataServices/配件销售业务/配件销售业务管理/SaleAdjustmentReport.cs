﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class SaleAdjustmentReportAch : DcsSerivceAchieveBase {
        public SaleAdjustmentReportAch(DcsDomainService domainService)
            : base(domainService) {
        }
        public IEnumerable<SaleAdjustmentReport> GetSaleAdjustmentReport(int? customerType, string agencyName, string customerName, string sparePartCodes,DateTime? bAdjustTime, DateTime? eAdjustTime) {
            var user = Utils.GetCurrentUserInfo();
            var company = ObjectContext.Companies.Single(r => r.Id == user.EnterpriseId);
            string sql1 = @"select pp.*,
                           nvl(pp.onwaysales, 0) + nvl(pp.outboundamount, 0) as AdjustQty,
                           (nvl(pp.onwaysales, 0) + nvl(pp.outboundamount, 0)) * difference as AdjustAmount,
                           (StockQty * difference) as StockQtyfee
                      from (select tp.id,
                                   tp.customertype,
                                   tp.agencyname,
                                   tp.submitcompanyid,
                                   tp.submitcompanyname as CustomerName,
                                   tp.submitcompanycode as CustomerCode,
                                   tp.sparepartid,
                                    sp.name as sparepartname,
                                   sp.code sparepartcode,
                                   tp.oldprice,
                                   tp.price,
                                   tp.difference,
                                   tp.differencerate,
                                   tp.pastthreemonthspruchase,
                                   tp.usableqty,
                                   tp.stockqty,
                                   tp.normalstock,
                                   tp.adjusttime,
                                   tp.onwaysales,
                                   tp.hisid,
                                   nvl(ps.outboundamount,0) as outboundamount
                              from PartsSalesPriceHistoryTemp tp
                               left join PartsSalesPriceOwingTemp ps on tp.hisid=ps.hisid and tp.submitcompanyid=ps.submitcompanyid and tp.salesunitownercompanyid =ps.salesunitownercompanyid
                                  join sparepart sp
                                  on tp.sparepartid = sp.id
                             where 1=1 {0}) pp ";
            
            
			string searchParam1 = "";
			
			if(customerType.HasValue){
                searchParam1 = searchParam1 + " and tp.customertype =" + customerType;
			}
			if(!string.IsNullOrEmpty(agencyName)){
                searchParam1 = searchParam1 + " and tp.submitcompanyname like '%" + agencyName + "%'";
			}
			if(!string.IsNullOrEmpty(customerName)){
                searchParam1 = searchParam1 + " and tp.submitcompanyname like '%" + customerName + "%'";
			}
			if(!string.IsNullOrEmpty(sparePartCodes)){
				var partsCodes = sparePartCodes.Split(',');
				if (partsCodes.Length == 1) {
					searchParam1 += "and sp.code LIKE '%" + partsCodes[0] + "%'";
				} else {
					for (var i = 0; i < partsCodes.Length; i++) {
						partsCodes[i] = "'" + partsCodes[i] + "'";
					}
					string codes = string.Join(",", partsCodes);
					searchParam1 += "and sp.code in (" + codes + ")";
				}
			}
            if (bAdjustTime.HasValue) {
                searchParam1 = searchParam1 + " and tp.adjusttime>= to_date('" + bAdjustTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if (eAdjustTime.HasValue) {
                searchParam1 = searchParam1 + " and tp.adjusttime<= to_date('" + eAdjustTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }

            if(company.Type == (int)DcsCompanyType.代理库) {
                searchParam1 = searchParam1+ " and (tp.submitcompanyid = " + user.EnterpriseId + " or exists(select 1 from AgencyDealerRelation ar where tp.submitcompanyid =ar.dealerid and ar.agencyid = " + user.EnterpriseId + " ))";
               
			}else if(company.Type == (int)DcsCompanyType.服务站){
                searchParam1 = searchParam1 + " and tp.submitcompanyid = " + user.EnterpriseId;
               
			}
            sql1 = string.Format(sql1, searchParam1);
            var detail = ObjectContext.ExecuteStoreQuery<SaleAdjustmentReport>(sql1).ToList();
            return detail;
        }
    
        public IEnumerable<SaleAdjustmentReport> GetSaleAdjustmentSumReport(int? customerType, string agencyName, string customerName,DateTime? bAdjustTime, DateTime? eAdjustTime) {
            var user = Utils.GetCurrentUserInfo();
            var company = ObjectContext.Companies.Single(r => r.Id == user.EnterpriseId);
            string sql1 = @"select customerid as id,
                           customertype,
                           agencyname,
                           CustomerName,
                           CustomerCode,
                           AdjustTime,
                           sum(AdjustAmount) as AdjustAmount,
                           (Extract(year from AdjustTime) || '-S' ||
                           ceil(Extract(month from AdjustTime) / 3)) as batchnumber
                      from (select tp.customertype,
                                   tp.agencyname,
                                   tp.submitcompanyid as customerid,
                                   tp.submitcompanyname as CustomerName,
                                   tp.submitcompanycode as CustomerCode,
                                   tp.sparepartid,
                                   tp.sparepartname,
                                   tp.sparepartcode,
                                   tp.oldprice,
                                   tp.price,
                                   tp.PastThreeMonthsPruchase,
                                   tp.UsableQty,
                                   ps.OUTBOUNDAMOUNT as OnLineQty,
                                   least(nvl(tp.UsableQty, 0) + nvl(ps.OUTBOUNDAMOUNT, 0),
                                         PastThreeMonthsPruchase) as AdjustQty,
                                   (tp.Difference *
                                   least(nvl(tp.UsableQty, 0) + nvl(ps.OUTBOUNDAMOUNT, 0),
                                          PastThreeMonthsPruchase)) as AdjustAmount,
                                   tp.AdjustTime
                              from PartsSalesPriceHistoryTemp tp
                              left join PartsSalesPriceOwingTemp ps
                                on tp.hisid = ps.hisid
                               and tp.submitcompanyid = ps.submitcompanyid
                               and tp.salesunitownercompanyid = ps.salesunitownercompanyid
                             where 1=1 {0})
                     where AdjustAmount <> 0
                     group by customerid,
                              customertype,
                              agencyname,
                              CustomerName,
                              CustomerCode,
                              AdjustTime";
            
            
			string searchParam1 = "";
			
			if(customerType.HasValue){
				searchParam1 = searchParam1 + " and tp.customertype =" + customerType;
			}
			if(!string.IsNullOrEmpty(agencyName)){
                searchParam1 = searchParam1 + " and tp.agencyName like '%" + agencyName + "%'";
			}
			if(!string.IsNullOrEmpty(customerName)){
                searchParam1 = searchParam1 + " and tp.submitcompanyname like '%" + customerName + "%'";
			}
			
            if (bAdjustTime.HasValue) {
                searchParam1 = searchParam1 + " and tp.AdjustTime>= to_date('" + bAdjustTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if (eAdjustTime.HasValue) {
                searchParam1 = searchParam1 + " and tp.AdjustTime<= to_date('" + eAdjustTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
			
            if(company.Type == 1){
                sql1 = string.Format(sql1, searchParam1, "");
			}else if(company.Type == 3){
                 searchParam1 = " and (tp.submitcompanyid = " + user.EnterpriseId +"or exists(select 1 from AgencyDealerRelation ar where tp.submitcompanyid =ar.dealerid and ar.agencyid = "+user.EnterpriseId+" ))"; 
			}else{
                 searchParam1 = " and tp.submitcompanyid = " + user.EnterpriseId; 
			}
            sql1 = string.Format(sql1, searchParam1 );
            var detail = ObjectContext.ExecuteStoreQuery<SaleAdjustmentReport>(sql1).ToList();
            return detail;
        }
  
    }

    partial class DcsDomainService {
        public IEnumerable<SaleAdjustmentReport> GetSaleAdjustmentReport( int? customerType, string agencyName, string customerName, string sparePartCodes,DateTime? bAdjustTime, DateTime? eAdjustTime) {
            return new SaleAdjustmentReportAch(this).GetSaleAdjustmentReport(customerType, agencyName, customerName, sparePartCodes,bAdjustTime, eAdjustTime);
        }

		public IEnumerable<SaleAdjustmentReport> GetSaleAdjustmentSumReport( int? customerType, string agencyName, string customerName,DateTime? bAdjustTime, DateTime? eAdjustTime) {
            return new SaleAdjustmentReportAch(this).GetSaleAdjustmentSumReport(customerType, agencyName, customerName,bAdjustTime, eAdjustTime);
        }
    }
}
