﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    class SaleOrderTerminalSatisfactionRateAch : DcsSerivceAchieveBase {
        public SaleOrderTerminalSatisfactionRateAch(DcsDomainService domainService)
            : base(domainService) {
        }
        public IEnumerable<SaleOrderTerminalSatisfactionRate> 终端满足率明细查询(string centerName, DateTime? bSubmitTime, DateTime? eSubmitTime, string marketingDepartmentName, string dealerName) {
            string SQL = @"select mt.name as marketingDepartmentName,
                           ps.submitcompanyname as dealerName,
                           cy.name                    as CenterName,
                           ps.code,
                           ps.submittime,
                           ps.partssalesordertypename,
                           pd.sparepartcode,
                           pd.orderedquantity,
                           rownum
                      from partssalesorder ps
                      left join partssalesorderdetail pd
                        on ps.id = pd.partssalesorderid
                      join SalesUnit su
                        on ps.salesunitid = su.id
                      join company cy
                        on su.ownercompanyid = cy.id
                       and cy.type = 3
                      left join partsbranch pb
                        on pd.sparepartid = pb.partid
                       and pb.status = 1
                      left join keyvalueitem kv
                        on pb.partabc = kv.key
                       and kv.name = 'ABCStrategy_Category'
                      left join DealerServiceInfo ag
                        on ag.DealerId = ps.SubmitCompanyId
                      left join MarketingDepartment mt
                        on mt.id = ag.marketingdepartmentid
                     where (ps.status in (2, 4, 5) or
                           (ps.status = 6 and pd.ApproveQuantity > 0))
                       and pd.orderedquantity > 0
                       and (ps.partssalesordertypename = '紧急订单' or
                           ps.partssalesordertypename = '特急订单') {0}";
            if(bSubmitTime.HasValue && bSubmitTime.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
                bSubmitTime = null;
            }
            if(eSubmitTime.HasValue && eSubmitTime.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
                eSubmitTime = null;
            }
            var serachSQL = new StringBuilder();
            if(bSubmitTime.HasValue) {
                serachSQL.Append("  and ps.SubmitTime >=  to_date('" + bSubmitTime.Value.ToString() + "', 'yyyy-MM-dd HH24:mi:ss')");
            }

            if(eSubmitTime.HasValue) {
                serachSQL.Append(" and  ps.SubmitTime <=  to_date('" + eSubmitTime.Value.ToString() + "', 'yyyy-MM-dd HH24:mi:ss') ");
            }
            if(!string.IsNullOrEmpty(centerName)) {
                serachSQL.Append(" and  cy.name like '%" + centerName + "%'");
            }
            if(!string.IsNullOrEmpty(dealerName)) {
                serachSQL.Append(" and ps.submitcompanyname like '%" + dealerName + "%'");
            }
            if(!string.IsNullOrEmpty(marketingDepartmentName)) {
                serachSQL.Append(" and mt.name like '%" + marketingDepartmentName + "%'");
            }
            var userInfo = Utils.GetCurrentUserInfo();

            var company = ObjectContext.Companies.Where(r => r.Id == userInfo.EnterpriseId).FirstOrDefault();
            //登录人为配件公司的，判断如果维护了人员与分销中心关系的，只能查询分销中心内的中心库订单。
            //登录人为中心库的只能查询自己辖区内服务站数据，登录人为服务站或者大型服务站的，只能查询自己提报的销售订单
            if(company.Type == (int)DcsCompanyType.分公司) {
                //判断是否维护了人员与分销中心关系
                var isHas = ObjectContext.MarketDptPersonnelRelations.Where(r => r.PersonnelId == userInfo.Id && r.Status == (int)DcsBaseDataStatus.有效).Count();
                if(isHas > 0) {
                    serachSQL.Append(" and  exists(select 1 from  MarketDptPersonnelRelation mr join Agency ag on mr.marketdepartmentid = ag.marketingdepartmentid  where mr.PersonnelId = " + userInfo.Id + " and ag.id= cy.id and mr.status=1)");
                }
            }
            if(company.Type == (int)DcsCompanyType.代理库) {
                serachSQL.Append(" and cy.id=" + userInfo.EnterpriseId);
            }
            if(company.Type == (int)DcsCompanyType.服务站 || company.Type == (int)DcsCompanyType.服务站兼代理库) {
                serachSQL.Append(" and ps.SubmitCompanyId=" + userInfo.EnterpriseId);
            }
            SQL = string.Format(SQL, serachSQL);

            var search = ObjectContext.ExecuteStoreQuery<SaleOrderTerminalSatisfactionRate>(SQL).ToArray().OrderBy(r => r.Code);
            return search;
        }
        public IEnumerable<SaleOrderTerminalSatisfactionRate> 查询终端满足率汇总报表(string centerName, int? year, int? month, int? unit, string marketingDepartmentName, string dealerName) {
          
            string SQL = @"select rownum,
                           pp.MarketingDepartmentName,
                           pp.year,
                           pp.month,
                           pp.CenterName,
                           pp.dealerName,
                           pp.EmergencyOrderNum,
                           pp.ExEmergencyOrderNum,
                           pp.AbNum,
                           pp.OrderNum,
                           round((nvl(EmergencyOrderNum, 0) + nvl(ExEmergencyOrderNum, 0)) * 100 /
                                 OrderNum,
                                 2) as EmergencyRate,
                           (case
                             when (nvl(EmergencyOrderNum, 0) + nvl(ExEmergencyOrderNum, 0)) = 0 or
                                  nvl(AbNum, 0) = 0 then
                              0
                             else
                              round(nvl(AbNum, 0) * 100 /
                                    (nvl(EmergencyOrderNum, 0) + nvl(ExEmergencyOrderNum, 0)),
                                    2)
                           end) as AbRate,
                           round((1 - ((nvl(EmergencyOrderNum, 0) + nvl(ExEmergencyOrderNum, 0)) /
                                 OrderNum)) * 0.95 * 100,
                                 2) as TerminalRate
                      from (select tt.name as MarketingDepartmentName,
                                   tt.year,
                                   tt.month,
                                   tt.CenterName,
                                   tt.dealerName,
                                   sum(EmergencyOrderNum) as EmergencyOrderNum,
                                   sum(ExEmergencyOrderNum) as ExEmergencyOrderNum,
                                   sum(AbNum) as AbNum,
                                   sum(OrderNum) as OrderNum
                              from (select 
                                           (case when {0}=1 or {0}=2 then  to_char(ps.submittime, 'yyyy') else  cast('' as varchar2(50)) end )as year,
                                         --  to_char(ps.submittime, 'yyyy') as year,
                                           (case when {0}=2 then  to_char(ps.submittime, 'mm') else  cast('' as varchar2(50)) end )as month,
                                        --   to_char(ps.submittime, 'mm') as month,
                                              (case when {1}=0 then cast('' as varchar2(50))else mt.name end ) as name,
                                              (case when {1}=0 or {1}=1 then cast('' as varchar2(50)) else cy.name end ) as CenterName,
                                           (case when {1}=0 or {1}=1 or {1}=2 then cast('' as varchar2(50)) else ps.submitcompanyname end ) as dealerName,
                                        --   ps.submitcompanyname as dealerName,
                                        --   cy.name as CenterName,
                                           
                                           ps.PartsSalesOrderTypeName,
                                           (case
                                             when ps.PartsSalesOrderTypeName = '紧急订单' then
                                              1
                                             else
                                              0
                                           end) as EmergencyOrderNum,
                                           (case
                                             when ps.PartsSalesOrderTypeName = '特急订单' then
                                              1
                                             else
                                              0
                                           end) as ExEmergencyOrderNum,
                                           (case
                                             when (kv.value = 'A' or kv.value = 'B') and
                                                  (ps.PartsSalesOrderTypeName = '特急订单' or
                                                  ps.PartsSalesOrderTypeName = '紧急订单') then
                                              1
                                             else
                                              0
                                           end) as AbNum,
                                           1 as OrderNum
                                      from partssalesorder ps
                                      left join partssalesorderdetail pd
                                        on ps.id = pd.partssalesorderid
                                      join SalesUnit su
                                        on ps.salesunitid = su.id
                                      join company cy
                                        on su.ownercompanyid = cy.id
                                       and cy.type = 3
                                      left join partsbranch pb
                                        on pd.sparepartid = pb.partid
                                       and pb.status = 1
                                      left join keyvalueitem kv
                                        on pb.partabc = kv.key
                                       and kv.name = 'ABCStrategy_Category'
                                      left join DealerServiceInfo ag
                                        on ag.DealerId = ps.SubmitCompanyId
                                      left join MarketingDepartment mt
                                        on mt.id = ag.marketingdepartmentid
                                     where (ps.status in (2, 4, 5) or
                                           (ps.status = 6 and pd.ApproveQuantity > 0))
                                       and pd.orderedquantity > 0 
                                      {2}) tt
                             group by tt.name, tt.year, tt.month, tt.CenterName, tt.dealerName  order by tt.year, tt.month, tt.name, tt.CenterName, tt.dealerName ) pp";
            var serachSQL = new StringBuilder();
             var yearType=2;
            var unitsType=4;
            var sum = 0;
            bool setSym = false;
            //当查询条件只输入年时，查询结果为选取年的1月至当月
            if(year.HasValue && (!month.HasValue && string.IsNullOrEmpty(centerName) && string.IsNullOrEmpty(dealerName) && string.IsNullOrEmpty(marketingDepartmentName) && !unit.HasValue || (DateTime.Now.Year > year && !month.HasValue))) {
                yearType = 2;
                serachSQL.Append(" and to_char(ps.submittime, 'yyyy')=" + year);
                unitsType = 0;
            } else {
                setSym = true;
                serachSQL.Append(" and to_char(ps.submittime, 'yyyy')=" + year);
            }
            //b、	当查询条件输入年、月时，查询结果显示1条总汇总数据和按月份显示汇总数据。
            // c、	选择以前年度是指销售订单提交时间：1月1日至12月31日的数据；选择当年是指销售订单提交时间1月1日至当日的数据；选择当月是指销售订单提交时间1月1日至当日的数据      
            if(year.HasValue && month.HasValue ) {
                if(string.IsNullOrEmpty(centerName) && string.IsNullOrEmpty(dealerName) && string.IsNullOrEmpty(marketingDepartmentName) && !unit.HasValue) {                    
                    unitsType = 0;
                } 
                yearType = 2;
                if(month<10){
                    serachSQL.Append(" and (to_char(ps.submittime, 'yyyy-mm')='" + year + "-0" + month.ToString() +"')");
                } else {
                    serachSQL.Append(" and (to_char(ps.submittime, 'yyyy-mm')='" + year + "-" + month.ToString() + "')");

                }
            }
              
            //d.当查询条件输入年或者年、月时，统计单位下拉菜单选择分销中心，查询结果按年或者按年、月、分销中心显示为汇总数据(中心库、服务商列为空白）。 每个分销中心一条数据
            if(year.HasValue && !month.HasValue && unit == 1) {
               // yearType = 2;
                unitsType = 1;
            } else if(year.HasValue && month.HasValue && unit == 1) {
               // yearType = 1;
                unitsType = 1;
            }
            //e、当查询条件输入年或者年、月时，统计单位下拉菜单选择中心库，查询结果按年或者按年、月、中心库显示为汇总数据（服务商列为空白）；每个中心库一条数据
            if(year.HasValue && !month.HasValue && unit == 2) {
             //   yearType = 2;
                unitsType = 2;
            } else if(year.HasValue && month.HasValue && unit == 2) {
             //   yearType = 2;
                unitsType = 2;
            }
            //f、当查询条件输入年或者年、月时，统计单位下拉菜单选择服务商时，查询结果按年或者按年、月、服务商显示为汇总数据。每个服务商一条数据。
            if(year.HasValue && !month.HasValue && unit == 3) {
              //  yearType = 2;
                unitsType = 3;
            } else if(year.HasValue && month.HasValue && unit == 3) {
              //  yearType = 1;
                unitsType = 3;
            }
            //G当统计单位选择分销中心或者不选，输入分销中心名称时，按照此分销中心汇总。
            if(!string.IsNullOrEmpty(marketingDepartmentName)){
                unitsType = 2;
                serachSQL.Append(" and  mt.name like '%" + marketingDepartmentName + "%'");
            }
            //g、当输入查询条件输入年或者年、月，输入中心库名称时，查询结果按年或者按年、月、中心库显示为汇总数据(服务商列为空白）。
            //此时统计单位可不选，或者选择为中心库。 如果统计单位选择了其他类型，按照其他类型统计，此条件无效。
            if(year.HasValue && !month.HasValue && !string.IsNullOrEmpty(centerName) && ((unit.HasValue && unit==2)||!unit.HasValue)) {
               // yearType = 2;
                unitsType = 2;
                serachSQL.Append(" and  cy.name like '%" + centerName + "%'");
            } else if(year.HasValue && month.HasValue && !string.IsNullOrEmpty(centerName) && ((unit.HasValue && unit == 2) || !unit.HasValue)) {
               // yearType = 1;
                unitsType = 2;
                serachSQL.Append(" and  cy.name like '%" + centerName + "%'");
            }
            //h、当输入查询条件输入年或者年、月，输入服务站名称时，查询结果按年或者按年、月、服务站显示为汇总数据。
            if(year.HasValue && !month.HasValue && !string.IsNullOrEmpty(dealerName)) {
               // yearType = 2;
                unitsType = 3;
                serachSQL.Append(" and  ps.submitcompanyname like '%" + dealerName + "%'");
            } else if( !string.IsNullOrEmpty(dealerName)) {
              //  yearType = 1;
                unitsType = 3;
                serachSQL.Append(" and  ps.submitcompanyname like '%" + dealerName + "%'");
            } 
            //i、当输入查询条件输入年或者年、月，输入中心库名称，统计单位选择：服务商，查询结果按年或者按年、月、中心库对应区域内服务站显示为汇总数据。按照中心库名称汇总。
            if(year.HasValue && !month.HasValue && !string.IsNullOrEmpty(centerName) && (unit.HasValue && unit == 3)) {
              //  yearType = 2;
                unitsType = 3;
                serachSQL.Append(" and  cy.name like '%" + centerName + "%'");
                sum = 2;
            }else if( !string.IsNullOrEmpty(centerName) && (unit.HasValue && unit == 3)){
               //  yearType = 1;
                unitsType = 3;
                serachSQL.Append(" and  cy.name like '%" + centerName + "%'");
                sum = 2;
            } else if(!string.IsNullOrEmpty(centerName) && !unit.HasValue) {
               // yearType = 1;
                unitsType = 2;
                serachSQL.Append(" and  cy.name like '%" + centerName + "%'");
                sum = 2;
            }
            //j、当输入查询条件输入年或者年、月，输入分销中心，统计单位选择：服务商，查询结果按年或者按年、月、分销中心对应区域内服务站显示为汇总数据。
            if(year.HasValue && !month.HasValue && !string.IsNullOrEmpty(marketingDepartmentName) && (unit.HasValue && unit == 3)) {
             //   yearType = 2;
                unitsType = 3;
                serachSQL.Append(" and  mt.name like '%" + marketingDepartmentName + "%'");
            } else if(!string.IsNullOrEmpty(marketingDepartmentName) && (unit.HasValue && unit == 3)) {
               // yearType = 1;
                unitsType = 3;
                serachSQL.Append(" and  mt.name like '%" + marketingDepartmentName + "%'");
            } else if(!string.IsNullOrEmpty(marketingDepartmentName) && !unit.HasValue) {
               // yearType = 1;
                unitsType = 1;
                serachSQL.Append(" and  mt.name like '%" + marketingDepartmentName + "%'");
            }
           
            var userInfo = Utils.GetCurrentUserInfo();
            var company = ObjectContext.Companies.Where(r => r.Id == userInfo.EnterpriseId).FirstOrDefault();
            //登录人为配件公司的，判断如果维护了人员与分销中心关系的，只能查询分销中心内的中心库订单。
            //登录人为中心库的只能查询自己辖区内服务站数据，登录人为服务站或者大型服务站的，只能查询自己提报的销售订单
            if(company.Type == (int)DcsCompanyType.分公司) {
                //判断是否维护了人员与分销中心关系
                var isHas = ObjectContext.MarketDptPersonnelRelations.Where(r => r.PersonnelId == userInfo.Id && r.Status == (int)DcsBaseDataStatus.有效).Count();
                if(isHas > 0) {
                    serachSQL.Append(" and  exists(select 1 from  MarketDptPersonnelRelation mr join Agency ag on mr.marketdepartmentid = ag.marketingdepartmentid  where mr.PersonnelId = " + userInfo.Id + " and ag.id= cy.id and mr.status=1)");
                }
            }
            if(company.Type == (int)DcsCompanyType.代理库) {
                serachSQL.Append(" and cy.id=" + userInfo.EnterpriseId);
            }
            if(company.Type == (int)DcsCompanyType.服务站 || company.Type == (int)DcsCompanyType.服务站兼代理库) {
                serachSQL.Append(" and ps.SubmitCompanyId=" + userInfo.EnterpriseId);
            }
            SQL = string.Format(SQL,yearType,unitsType, serachSQL);

            var search = ObjectContext.ExecuteStoreQuery<SaleOrderTerminalSatisfactionRate>(SQL).ToList();
            if(setSym){
                if(search.Count()>0){
                    SaleOrderTerminalSatisfactionRate summary = new SaleOrderTerminalSatisfactionRate();
                    summary.Rownum = 0;
                    summary.Year = year;
                    summary.Month = null;
                    summary.MarketingDepartmentName = "汇总";
                    if(sum==2){
                        summary.CenterName = search.First().CenterName;
                    } else {
                        summary.CenterName = null;
                    }                    
                    summary.DealerName = null;
                    summary.EmergencyOrderNum = search.Sum(r => r.EmergencyOrderNum);
                    summary.ExEmergencyOrderNum = search.Sum(r => r.ExEmergencyOrderNum);
                    summary.AbNum = search.Sum(r => r.AbNum);
                    summary.OrderNum = search.Sum(r => r.OrderNum);
                    if(summary.EmergencyOrderNum + summary.ExEmergencyOrderNum == 0 || summary.OrderNum==0) {
                        summary.EmergencyRate = 0;
                    } else {
                        summary.EmergencyRate = Math.Round( (Decimal.Parse(summary.EmergencyOrderNum.ToString()) + Decimal.Parse(summary.ExEmergencyOrderNum.ToString()))*100/Decimal.Parse(summary.OrderNum.ToString()),2);
                    }
                    if(summary.AbNum == 0 || (summary.EmergencyOrderNum + summary.ExEmergencyOrderNum == 0)) {
                        summary.AbRate = 0;
                    } else {
                        summary.AbRate =Math.Round( ( (Decimal.Parse(summary.AbNum.ToString()))*100/(Decimal.Parse(summary.EmergencyOrderNum.ToString()) + Decimal.Parse(summary.ExEmergencyOrderNum.ToString()))),2);
                    }
                    summary.TerminalRate =  Math.Round( Decimal.Parse( ((100- Double.Parse(summary.EmergencyRate.ToString()))*0.95).ToString()),2);
                    search.Add(summary);
                }
            }
            return search.OrderBy(r=>r.Rownum);
        }
    }
    partial class DcsDomainService {
        //终端满足率明细查询
        public IEnumerable<SaleOrderTerminalSatisfactionRate> 终端满足率明细查询(string centerName, DateTime? bSubmitTime, DateTime? eSubmitTime, string marketingDepartmentName, string dealerName) {
            return new SaleOrderTerminalSatisfactionRateAch(this).终端满足率明细查询(centerName, bSubmitTime, eSubmitTime, marketingDepartmentName, dealerName);

        }
        public IEnumerable<SaleOrderTerminalSatisfactionRate> 查询终端满足率汇总报表(string centerName, int? year, int? month, int? unit, string marketingDepartmentName, string dealerName) {
            return new SaleOrderTerminalSatisfactionRateAch(this).查询终端满足率汇总报表(centerName, year, month,unit, marketingDepartmentName, dealerName);

        }

    }
}
