﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sunlight.Silverlight.Dcs.Web
{
    class SalesSatisfactionRateAch : DcsSerivceAchieveBase
    {
        public SalesSatisfactionRateAch(DcsDomainService domainService)
            : base(domainService)
        {
        }
        public IEnumerable<SalesSatisfactionRate> GetSalesSatisfactionCentralRate()
        {
            string SQL = @"select code, name, type, ycpzs, totalpz,  decode(totalpz, 0, 0, ROUND(ycpzs / totalpz, 4) * 100) tmmzl,  ycfee, totalfee,  decode(totalfee, 0, 0, ROUND(ycfee / totalfee, 4) * 100) as jemzl
                          from (select code, name,  type,  sum(nvl(ycpzs, 0)) as ycpzs, sum(totalpz) as totalpz,  sum(nvl(ycfee, 0)) as ycfee,  sum(totalfee) as totalfee
                          from (select (select count(*)  from partssalesorderprocess tmpa  inner join partssalesorderprocessdetail tmpb    on tmpa.id = tmpb.partssalesorderprocessid   where tmpa.originalsalesorderid = a.id  and tmpa.time = 1) as ycpzs,
                           (select sum(tmpb.currentfulfilledquantity * tmppsd.originalprice) ycfee from partssalesorderprocess tmpa  inner join partssalesorderprocessdetail tmpb  on tmpa.id = tmpb.partssalesorderprocessid  inner join partssalesorderdetail tmppsd  on tmppsd.partssalesorderid =
                           tmpa.originalsalesorderid and tmppsd.sparepartid = tmpb.sparepartid  where tmpa.originalsalesorderid = a.id  and tmpa.time = 1) as ycfee,   a.id,  a.code,  a.name,  a.type,  a.totalfee, a.totalpz
                          from (select a.id,c.code, c.name, c.type,sum(b.originalprice * b.orderedquantity) as totalfee, count(*) as totalpz from partssalesorder a   inner join partssalesorderdetail b
                         on b.partssalesorderid = a.id  inner join company c on c.id = a.submitcompanyid  inner join partssalesorderprocess psop on psop.originalsalesorderid = a.id where a.salesunitownercompanyid =
                        (select id from branch where rownum = 1) and a.status in (4, 5, 6) and nvl(a.isexport, 0) <> 1  and c.type = 3    and psop.createtime >= TRUNC(SYSDATE - 1)  and psop.createtime <= TRUNC(SYSDATE)
                        group by a.id, c.code, c.name, c.type) a) tmp group by code, name, type) jk";
            var search = ObjectContext.ExecuteStoreQuery<SalesSatisfactionRate>(SQL).ToList();
            return search;
        }
        public IEnumerable<SalesSatisfactionRate> GetSalesSatisfactionCentralRateReport(string name, DateTime? bApproveTime, DateTime? eApproveTime)
        {
            string SQL = @"select code, name, cast ('中心库' as varchar2(100)) as type , ycpzs, totalpz,(totalpz-nvl(ycpzs,0)) as UnYcpzs, decode(totalpz, 0, 0, ROUND(ycpzs / totalpz, 4) * 100) tmmzl,  ycfee, totalfee,  decode(totalfee, 0, 0, ROUND(ycfee / totalfee, 4) * 100) as jemzl,companyId, rownum as id
                          from (select code, name,  type,  sum(nvl(ycpzs, 0)) as ycpzs, sum(totalpz) as totalpz,  sum(nvl(ycfee, 0)) as ycfee,  sum(totalfee) as totalfee,companyId
                          from (select (select count(1)from partssalesorderprocessdetail tmpb  where tmpb.partssalesorderprocessid = a.psopId) as ycpzs,
                           (select sum(tmpb.currentfulfilledquantity * tmppsd.originalprice) ycfee from partssalesorderprocess tmpa  inner join partssalesorderprocessdetail tmpb  on tmpa.id = tmpb.partssalesorderprocessid  inner join partssalesorderdetail tmppsd  on tmppsd.partssalesorderid =
                           tmpa.originalsalesorderid and tmppsd.sparepartid = tmpb.sparepartid  where   tmpa.id = a.psopId) as ycfee,   a.id,  a.code,  a.name,  a.totalfee, a.totalpz,a.companyId,a.type
                          from (select a.id,c.code, c.name,  c.type,c.id as companyId,sum(b.originalprice * b.orderedquantity) as totalfee, count(*) as totalpz,psop.id as psopId from partssalesorder a   inner join partssalesorderdetail b
                         on b.partssalesorderid = a.id  inner join company c on c.id = a.submitcompanyid  inner join partssalesorderprocess psop on psop.originalsalesorderid = a.id  {0}  where a.salesunitownercompanyid =
                        (select id from branch where rownum = 1) and a.status in (4, 5, 6) and nvl(a.isexport, 0) <> 1  and c.type = 3 and c.code not in('PJ03000577','PJ03000574','PJ02042716') and  b.orderedquantity>0 ";
            if(bApproveTime.HasValue && bApproveTime.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
                bApproveTime = null;
            }
            if(eApproveTime.HasValue && eApproveTime.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
                eApproveTime = null;
            }
            var serachSQL = new StringBuilder();
            if(bApproveTime.HasValue && !eApproveTime.HasValue)
            {
                serachSQL.Append(" and psop.Time=1 and psop.CreateTime >=  to_date('" + bApproveTime.Value.ToString() + "', 'yyyy-MM-dd HH24:mi:ss')");
            }
            if(!bApproveTime.HasValue&&eApproveTime.HasValue)
            {
                serachSQL.Append(" and psop.Time=1 and psop.CreateTime <=  to_date('" + eApproveTime.Value.ToString() + "', 'yyyy-MM-dd HH24:mi:ss')");
            }

            if(bApproveTime.HasValue && eApproveTime.HasValue) {
                serachSQL.Append(" and psop.Time=1 and psop.CreateTime >=  to_date('" + bApproveTime.Value.ToString() + "', 'yyyy-MM-dd HH24:mi:ss') and psop.CreateTime <= to_date('" + eApproveTime.Value.ToString() + "', 'yyyy-MM-dd HH24:mi:ss')");
            }
            SQL = string.Format(SQL, serachSQL);
            if (!string.IsNullOrEmpty(name))
            {
                SQL = SQL + " and c.name like '%" + name + "%'";
            }
            SQL = SQL + " group by a.id, c.code, c.name, c.type,c.id, psop.id) a) tmp group by code, name, type,companyId) jk";
            var search = ObjectContext.ExecuteStoreQuery<SalesSatisfactionRate>(SQL).ToArray().OrderBy(r => r.Code);
            //汇总

            if(search.Count()>0) {
                  SalesSatisfactionRate summary = new SalesSatisfactionRate();
                  summary.Type = "汇总";
                  summary.Ycpzs = search.Sum(r => r.Ycpzs);
                  summary.Totalpz = search.Sum(r=>r.Totalpz);
                  summary.Tmmzl = Math.Round(Decimal.Parse((summary.Ycpzs * 100).ToString()) / Decimal.Parse(summary.Totalpz.ToString()), 2);
                  summary.Ycfee = search.Sum(r => r.Ycfee);
                  summary.Totalfee = search.Sum(r=>r.Totalfee);
                  summary.Jemzl = Math.Round(summary.Ycfee * 100 / summary.Totalfee, 2);
                  summary.Id = 0;
                  summary.UnYcpzs = summary.Totalpz - summary.Ycpzs;

                  List<SalesSatisfactionRate> returnList = new List<SalesSatisfactionRate>();
                  List<SalesSatisfactionRate> itemList = new List<SalesSatisfactionRate>();
                      foreach(var item in search) {
                          itemList.Add(item);
                      }
                      int j = 0;
                      int addItem = itemList.Count() + (itemList.Count() / 50) + 1;
                      for(int i = 0; i < addItem; i++) {
                          if(i == 0 || (i % 50) == 0) {
                              returnList.Add(summary);
                          } else {
                              returnList.Add(itemList[j]);
                              j++;
                          }
                      }
                  return returnList;
             }
            return null;
        }
    }
    partial class DcsDomainService
    {
        //销售订单一次满足率（SIH 配件对中心库)查询
        public IEnumerable<SalesSatisfactionRate> GetSalesSatisfactionCentralRate()
        {
            return new SalesSatisfactionRateAch(this).GetSalesSatisfactionCentralRate();

        }
        //销售订单一次满足率（SIH 配件对中心库)查询报表
        public IEnumerable<SalesSatisfactionRate> GetSalesSatisfactionCentralRateReport(string name, DateTime? bApproveTime, DateTime? eApproveTime)
        {
            return new SalesSatisfactionRateAch(this).GetSalesSatisfactionCentralRateReport(name, bApproveTime, eApproveTime);

        }
       
    }
}
