﻿using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class SalesUnitAch : DcsSerivceAchieveBase {
        internal void InsertSalesUnitValidate(SalesUnit salesUnit) {
            var dbSaleUnit = ObjectContext.SalesUnits.Where(r => r.Name == salesUnit.Name || r.Code == salesUnit.Code).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbSaleUnit != null) {
                throw new ValidationException(ErrorStrings.SalesUnit_Validation5);
            }
        }

        internal void UpdateSalesUnitValidate(SalesUnit salesUnit) {
            //var t = ChangeSet.GetOriginal(salesUnit);
            //if(t != null && !(t.PartsSalesCategoryId == salesUnit.PartsSalesCategoryId && t.OwnerCompanyId == salesUnit.OwnerCompanyId)) {
            //    if(ObjectContext.SalesUnits.Any(r => r.PartsSalesCategoryId == salesUnit.PartsSalesCategoryId && r.OwnerCompanyId == salesUnit.OwnerCompanyId)) {
            //        throw new ValidationException(ErrorStrings.SalesUnit_Validation6);
            //    }
            //}
            var dbSaleUnit = ObjectContext.SalesUnits.Where(r => r.Id != salesUnit.Id && (r.Name == salesUnit.Name || r.Code == salesUnit.Code)).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbSaleUnit != null) {
                throw new ValidationException(ErrorStrings.SalesUnit_Validation5);
            }
        }

        public void InsertSalesUnit(SalesUnit salesUnit) {
            InsertToDatabase(salesUnit);
            this.InsertSalesUnitValidate(salesUnit);
        }

        public void UpdateSalesUnit(SalesUnit salesUnit) {
            UpdateToDatabase(salesUnit);
            this.UpdateSalesUnitValidate(salesUnit);
        }

        public IQueryable<SalesUnit> GetSalesUnitsWithCompanyAndAccountGroup() {
            return ObjectContext.SalesUnits.Include("Company").Include("AccountGroup").Include("PartsSalesCategory").OrderBy(entity => entity.Id);
        }

        public SalesUnit GetSalesUnitWithDetailsById(int id) {
            var dbSalesUnit = ObjectContext.SalesUnits.SingleOrDefault(entity => entity.Id == id);
            if(dbSalesUnit != null) {
                var dbSalesUnitAffiWarehouse = ObjectContext.SalesUnitAffiWarehouses.Where(entity => dbSalesUnit.Id == entity.SalesUnitId).ToArray();
                var dbSalesUnitAffiPersonnel = ObjectContext.SalesUnitAffiPersonnels.Where(entity => dbSalesUnit.Id == entity.SalesUnitId).ToArray();
                var dbCompany = ObjectContext.Companies.SingleOrDefault(entity => entity.Id == dbSalesUnit.OwnerCompanyId);
                var dbAccountGroup = ObjectContext.AccountGroups.SingleOrDefault(entity => entity.Id == dbSalesUnit.AccountGroupId);
                if(dbSalesUnitAffiWarehouse.Length > 0) {
                    var warehouseIds = dbSalesUnitAffiWarehouse.Select(entity => entity.WarehouseId);
                    var dbWarehouses = ObjectContext.Warehouses.Where(entity => warehouseIds.Contains(entity.Id)).ToArray();
                }
                if(dbSalesUnitAffiPersonnel.Length > 0) {
                    var personnelIds = dbSalesUnitAffiPersonnel.Select(entity => entity.PersonnelId);
                    var dbPersonnels = ObjectContext.Personnels.Where(entity => personnelIds.Contains(entity.Id)).ToArray();
                }
            }
            return dbSalesUnit;
        }

        public IQueryable<SalesUnit> 根据人员查询所属销售组织(int personnelId) {
            return ObjectContext.SalesUnits.Where(v => ObjectContext.SalesUnitAffiPersonnels.Any(r => r.PersonnelId == personnelId && r.SalesUnitId == v.Id) && v.Status == (int)DcsMasterDataStatus.有效).OrderBy(r => r.Id);
        }

        public IQueryable<SalesUnit> GetSalesUnitsForAgencyReportPSRB(int enterpriseId, int branchId) {
            return ObjectContext.SalesUnits.Where(v => ObjectContext.CustomerInformations.Any(r => r.CustomerCompanyId == enterpriseId && r.SalesCompanyId == v.OwnerCompanyId && r.Status == (int)DcsMasterDataStatus.有效) && v.Status == (int)DcsMasterDataStatus.有效 && v.OwnerCompanyId != enterpriseId && v.BranchId == branchId).OrderBy(r => r.Id);
        }

        public SalesUnit GetSalesUnitByWarehouseId(int WarehouseId) {
            return ObjectContext.SalesUnits.SingleOrDefault(e => ObjectContext.SalesUnitAffiWarehouses.Any(ex => ex.WarehouseId == WarehouseId && ex.SalesUnitId == e.Id));
        }

        public IQueryable<SalesUnit> GetSalesUnitByWarehouseIdField(int warehouseId) {
            return ObjectContext.SalesUnits.Where(e => ObjectContext.SalesUnitAffiWarehouses.Any(ex => ex.WarehouseId == warehouseId && ex.SalesUnitId == e.Id)).Include("PartsSalesCategory");
        }
    
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               

        public void InsertSalesUnit(SalesUnit salesUnit) {
            new SalesUnitAch(this).InsertSalesUnit(salesUnit);
        }

        public void UpdateSalesUnit(SalesUnit salesUnit) {
            new SalesUnitAch(this).UpdateSalesUnit(salesUnit);
        }

        public IQueryable<SalesUnit> GetSalesUnitsWithCompanyAndAccountGroup() {
            return new SalesUnitAch(this).GetSalesUnitsWithCompanyAndAccountGroup();
        }

        public SalesUnit GetSalesUnitWithDetailsById(int id) {
            return new SalesUnitAch(this).GetSalesUnitWithDetailsById(id);
        }

        public IQueryable<SalesUnit> 根据人员查询所属销售组织(int personnelId) {
            return new SalesUnitAch(this).根据人员查询所属销售组织(personnelId);
        }

        public IQueryable<SalesUnit> GetSalesUnitsForAgencyReportPSRB(int enterpriseId, int branchId) {
            return new SalesUnitAch(this).GetSalesUnitsForAgencyReportPSRB(enterpriseId,branchId);
        }

        public SalesUnit GetSalesUnitByWarehouseId(int WarehouseId) {
            return new SalesUnitAch(this).GetSalesUnitByWarehouseId(WarehouseId);
        }

        public IQueryable<SalesUnit> GetSalesUnitByWarehouseIdField(int warehouseId) {
            return new SalesUnitAch(this).GetSalesUnitByWarehouseIdField(warehouseId);
        }
    }
}
