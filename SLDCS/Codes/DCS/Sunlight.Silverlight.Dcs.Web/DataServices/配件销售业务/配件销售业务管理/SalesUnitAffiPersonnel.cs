﻿
namespace Sunlight.Silverlight.Dcs.Web {
    partial class SalesUnitAffiPersonnelAch : DcsSerivceAchieveBase {
        public SalesUnitAffiPersonnelAch(DcsDomainService domainService)
            : base(domainService) {
        }

        internal void InsertSalesUnitAffiPersonnelValidate(SalesUnitAffiPersonnel salesUnitAffiPersonnel) {
        }
        internal void UpdateSalesUnitAffiPersonnelValidate(SalesUnitAffiPersonnel salesUnitAffiPersonnel) {
        }
        public void InsertSalesUnitAffiPersonnel(SalesUnitAffiPersonnel salesUnitAffiPersonnel) {
            InsertToDatabase(salesUnitAffiPersonnel);
            this.InsertSalesUnitAffiPersonnelValidate(salesUnitAffiPersonnel);
        }
        public void UpdateSalesUnitAffiPersonnel(SalesUnitAffiPersonnel salesUnitAffiPersonnel) {
            UpdateToDatabase(salesUnitAffiPersonnel);
            this.UpdateSalesUnitAffiPersonnelValidate(salesUnitAffiPersonnel);
        }
        internal void DeleteSalesUnitAffiPersonnelValidate(SalesUnitAffiPersonnel salesUnitAffiPersonnel) {
        }
        public void DeleteSalesUnitAffiPersonnel(SalesUnitAffiPersonnel salesUnitAffiPersonnel) {
            DeleteFromDatabase(salesUnitAffiPersonnel);
            this.DeleteSalesUnitAffiPersonnelValidate(salesUnitAffiPersonnel);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               

        public void InsertSalesUnitAffiPersonnel(SalesUnitAffiPersonnel salesUnitAffiPersonnel) {
            new SalesUnitAffiPersonnelAch(this).InsertSalesUnitAffiPersonnel(salesUnitAffiPersonnel);
        }

        public void UpdateSalesUnitAffiPersonnel(SalesUnitAffiPersonnel salesUnitAffiPersonnel) {
            new SalesUnitAffiPersonnelAch(this).UpdateSalesUnitAffiPersonnel(salesUnitAffiPersonnel);
        }
                public void DeleteSalesUnitAffiPersonnel(SalesUnitAffiPersonnel salesUnitAffiPersonnel) {
            new SalesUnitAffiPersonnelAch(this).DeleteSalesUnitAffiPersonnel(salesUnitAffiPersonnel);
        }
    }
}
