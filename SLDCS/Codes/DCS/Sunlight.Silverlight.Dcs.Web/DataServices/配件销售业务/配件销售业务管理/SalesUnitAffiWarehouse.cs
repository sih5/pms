﻿using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class SalesUnitAffiWarehouseAch : DcsSerivceAchieveBase {
        public SalesUnitAffiWarehouseAch(DcsDomainService domainService)
            : base(domainService) {
        }

        internal void InsertSalesUnitAffiWarehouseValidate(SalesUnitAffiWarehouse salesUnitAffiWarehouse) {
        }

        internal void UpdateSalesUnitAffiWarehouseValidate(SalesUnitAffiWarehouse salesUnitAffiWarehouse) {
        }

        public void InsertSalesUnitAffiWarehouse(SalesUnitAffiWarehouse salesUnitAffiWarehouse) {
            InsertToDatabase(salesUnitAffiWarehouse);
            this.InsertSalesUnitAffiWarehouseValidate(salesUnitAffiWarehouse);
        }

        public void UpdateSalesUnitAffiWarehouse(SalesUnitAffiWarehouse salesUnitAffiWarehouse) {
            UpdateToDatabase(salesUnitAffiWarehouse);
            this.UpdateSalesUnitAffiWarehouseValidate(salesUnitAffiWarehouse);
        }

        internal void DeleteSalesUnitAffiWarehouseValidate(SalesUnitAffiWarehouse salesUnitAffiWarehouse) {
            int salesUnitId = salesUnitAffiWarehouse.SalesUnitId;
            int warehouseId = salesUnitAffiWarehouse.WarehouseId;
            var newDealerBusinessPermitHistory = new SalesUnitAffiWhouseHistory {
                SalesUnitId = salesUnitId,
                WarehouseId = warehouseId,
                Status = (int)DcsDealerBusinessPermitHistoryStatus.删除
            };
            DomainService.InsertSalesUnitAffiWhouseHistory(newDealerBusinessPermitHistory);
        }

        public void DeleteSalesUnitAffiWarehouse(SalesUnitAffiWarehouse salesUnitAffiWarehouse) {
            DeleteFromDatabase(salesUnitAffiWarehouse);
            this.DeleteSalesUnitAffiWarehouseValidate(salesUnitAffiWarehouse);
        }

        public IQueryable<SalesUnitAffiWarehouse> GetSalesUnitAffiWarehousesWithWarehouse() {
            return ObjectContext.SalesUnitAffiWarehouses.Include("Warehouse").OrderBy(entity => entity.Id);
        }
        public IQueryable<SalesUnitAffiWarehouse> GetWarehouseWithOriginalCompanyIdAndPartsSalesCategoryId(int originalCompanyId, int partsSalesCategoryId) {
            return ObjectContext.SalesUnitAffiWarehouses.Where(e => ObjectContext.SalesUnits.Any(r => r.OwnerCompanyId == originalCompanyId && r.PartsSalesCategoryId == partsSalesCategoryId && r.Id == e.SalesUnitId)).Include("Warehouse");
        }

        public IQueryable<SalesUnitAffiWarehouse> GetWarehousesBySalesUnitSalesCategoryId() {
            return ObjectContext.SalesUnitAffiWarehouses.Where(e => e.SalesUnitId == 100).Include("Warehouse");
        }

        public IQueryable<SalesUnitAffiWarehouse> 查询销售组织仓库(int enterpriseId, int? branchId, int? salesUnitId, int? partsSalesCategoryId, int? currentEnterPriseType) {
            var company = ObjectContext.Companies.SingleOrDefault(r => r.Id == enterpriseId);
            if(company == null) {
                return null;
            }
            var allOfsalesUnitsQuery = ObjectContext.SalesUnits.Where(r => r.Status == (int)DcsMasterDataStatus.有效 && (!branchId.HasValue || r.BranchId == branchId) && (!salesUnitId.HasValue || r.Id == salesUnitId) && (!partsSalesCategoryId.HasValue || r.PartsSalesCategoryId == partsSalesCategoryId));
            IQueryable<SalesUnit> resultOfSaleUnitsQuery = null;
            //当前登陆企业为代理库或服务站兼代理库时，查询提报企业类型为服务站兼代理库的企业订货仓库视同于提报单位企业类型为服务站
            if(currentEnterPriseType.HasValue && (currentEnterPriseType == (int)DcsCompanyType.代理库 || currentEnterPriseType == (int)DcsCompanyType.服务站兼代理库)) {
                switch(company.Type) {
                    case (int)DcsCompanyType.服务站:
                    case (int)DcsCompanyType.服务站兼代理库:
                        var dealerServiceInfoQuery = ObjectContext.DealerServiceInfoes.Where(r => r.Status == (int)DcsMasterDataStatus.有效 && r.DealerId == enterpriseId);
                        var agencyDealerRelationQuery = ObjectContext.AgencyDealerRelations.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.DealerId == enterpriseId);
                        resultOfSaleUnitsQuery = allOfsalesUnitsQuery.Where(r => dealerServiceInfoQuery.Any(v => v.BranchId == r.OwnerCompanyId && v.PartsSalesCategoryId == r.PartsSalesCategoryId) || agencyDealerRelationQuery.Any(v => v.BranchId == r.BranchId && v.PartsSalesOrderTypeId == r.PartsSalesCategoryId && v.AgencyId == r.OwnerCompanyId));
                        break;
                    case (int)DcsCompanyType.代理库:
                        var agencyAffiBranchQuery = ObjectContext.AgencyAffiBranches.Where(r => r.AgencyId == enterpriseId);
                        resultOfSaleUnitsQuery = allOfsalesUnitsQuery.Where(r => agencyAffiBranchQuery.Any(v => v.BranchId == r.OwnerCompanyId) || r.OwnerCompanyId == enterpriseId);
                        break;
                    case (int)DcsCompanyType.分公司:
                        resultOfSaleUnitsQuery = allOfsalesUnitsQuery.Where(r => !branchId.HasValue || r.OwnerCompanyId == branchId);
                        break;
                }
            } else {
                switch(company.Type) {
                    case (int)DcsCompanyType.服务站:
                    case (int)DcsCompanyType.集团企业:
                        var dealerServiceInfoQuery = ObjectContext.DealerServiceInfoes.Where(r => r.Status == (int)DcsMasterDataStatus.有效 && r.DealerId == enterpriseId);
                        var agencyDealerRelationQuery = ObjectContext.AgencyDealerRelations.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.DealerId == enterpriseId);
                        resultOfSaleUnitsQuery = allOfsalesUnitsQuery.Where(r => dealerServiceInfoQuery.Any(v => v.BranchId == r.OwnerCompanyId && v.PartsSalesCategoryId == r.PartsSalesCategoryId) || agencyDealerRelationQuery.Any(v => v.BranchId == r.BranchId && v.PartsSalesOrderTypeId == r.PartsSalesCategoryId && v.AgencyId == r.OwnerCompanyId));
                        break;
                    case (int)DcsCompanyType.服务站兼代理库:
                    case (int)DcsCompanyType.代理库:
                        var agencyAffiBranchQuery = ObjectContext.AgencyAffiBranches.Where(r => r.AgencyId == enterpriseId);
                        resultOfSaleUnitsQuery = allOfsalesUnitsQuery.Where(r => agencyAffiBranchQuery.Any(v => v.BranchId == r.OwnerCompanyId) || r.OwnerCompanyId == enterpriseId);
                        break;
                    case (int)DcsCompanyType.分公司:
                        resultOfSaleUnitsQuery = allOfsalesUnitsQuery.Where(r => !branchId.HasValue || r.OwnerCompanyId == branchId);
                        break;
                }
            }
            if(resultOfSaleUnitsQuery == null) {
                return null;
            }
            var result = ObjectContext.SalesUnitAffiWarehouses.Where(r => resultOfSaleUnitsQuery.Any(v => r.SalesUnitId == v.Id));
            return result.Include("SalesUnit").Include("Warehouse");
        }
        public IQueryable<SalesUnitAffiWarehouse> 查询销售组织仓库2(int enterpriseId, int? branchId, int? salesUnitId, int? partsSalesCategoryId) {
            var company = ObjectContext.Companies.SingleOrDefault(r => r.Id == enterpriseId);
            if(company == null) {
                return null;
            }
            var partsSalesCategory = ObjectContext.PartsSalesCategories.SingleOrDefault(r => r.Id == partsSalesCategoryId);
            if(partsSalesCategory == null) {
                return null;
            }
            var allOfsalesUnitsQuery = ObjectContext.SalesUnits.Where(r => r.Status == (int)DcsMasterDataStatus.有效 && (!branchId.HasValue || r.BranchId == branchId) && (!salesUnitId.HasValue || r.Id == salesUnitId) && (!partsSalesCategoryId.HasValue || r.PartsSalesCategoryId == partsSalesCategoryId));
            switch(company.Type) {
                case (int)DcsCompanyType.服务站:
                case (int)DcsCompanyType.服务站兼代理库:
                    allOfsalesUnitsQuery = allOfsalesUnitsQuery.Where(r => r.SubmitCompanyId == (int)DcsSalesUnit_SubmitCompanyId.服务站 || r.SubmitCompanyId == (int)DcsSalesUnit_SubmitCompanyId.中心库和服务站);
                    break;
                case (int)DcsCompanyType.代理库:
                    allOfsalesUnitsQuery = allOfsalesUnitsQuery.Where(r => r.SubmitCompanyId == (int)DcsSalesUnit_SubmitCompanyId.中心库 || r.SubmitCompanyId == (int)DcsSalesUnit_SubmitCompanyId.中心库和服务站);
                    break;
            }
            IQueryable<SalesUnit> resultOfSaleUnitsQuery = null;
            switch(company.Type) {
                case (int)DcsCompanyType.服务站:
                    var dealerServiceInfoQuery = ObjectContext.DealerServiceInfoes.Where(r => r.Status == (int)DcsMasterDataStatus.有效 && r.DealerId == enterpriseId);
                    var agencyDealerRelationQuery = ObjectContext.AgencyDealerRelations.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.DealerId == enterpriseId);
                    resultOfSaleUnitsQuery = allOfsalesUnitsQuery.Where(r => dealerServiceInfoQuery.Any(v => v.BranchId == r.OwnerCompanyId && v.PartsSalesCategoryId == r.PartsSalesCategoryId) || agencyDealerRelationQuery.Any(v => v.BranchId == r.BranchId && v.PartsSalesOrderTypeId == r.PartsSalesCategoryId && v.AgencyId == r.OwnerCompanyId));
                    break;
                case (int)DcsCompanyType.服务站兼代理库:
                case (int)DcsCompanyType.代理库:
                    resultOfSaleUnitsQuery = ObjectContext.SalesUnits.Where(v => ObjectContext.CustomerInformations.Any(r => r.CustomerCompanyId == enterpriseId && r.SalesCompanyId == v.OwnerCompanyId && r.Status == (int)DcsMasterDataStatus.有效) && v.Status == (int)DcsMasterDataStatus.有效 && v.OwnerCompanyId != enterpriseId && v.PartsSalesCategoryId == partsSalesCategoryId);
                    break;
                case (int)DcsCompanyType.分公司:
                    resultOfSaleUnitsQuery = allOfsalesUnitsQuery.Where(r => !branchId.HasValue || r.OwnerCompanyId == branchId);
                    break;
            }
            if(resultOfSaleUnitsQuery == null) {
                return null;
            }
            var result = ObjectContext.SalesUnitAffiWarehouses.Where(r => resultOfSaleUnitsQuery.Any(v => r.SalesUnitId == v.Id) && this.ObjectContext.Warehouses.Any(s => r.WarehouseId == s.Id && s.IsQualityWarehouse != true));
            return result.Include("SalesUnit").Include("Warehouse");
        }
        [Query(HasSideEffects = true)]
        public IQueryable<SalesUnitAffiWarehouse> GetSalesUnitAffiWarehousesWithWarehouseAndSalesUnitBySalesUnitIds(int[] ids) {
            return ObjectContext.SalesUnitAffiWarehouses.Where(r => ids.Contains(r.SalesUnitId) & r.SalesUnit.Status == (int)DcsBaseDataStatus.有效).Include("SalesUnit").Include("Warehouse").OrderBy(entity => entity.Id);
        }

    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               

        public void InsertSalesUnitAffiWarehouse(SalesUnitAffiWarehouse salesUnitAffiWarehouse) {
            new SalesUnitAffiWarehouseAch(this).InsertSalesUnitAffiWarehouse(salesUnitAffiWarehouse);
        }

        public void UpdateSalesUnitAffiWarehouse(SalesUnitAffiWarehouse salesUnitAffiWarehouse) {
            new SalesUnitAffiWarehouseAch(this).UpdateSalesUnitAffiWarehouse(salesUnitAffiWarehouse);
        }

        public void DeleteSalesUnitAffiWarehouse(SalesUnitAffiWarehouse salesUnitAffiWarehouse) {
            new SalesUnitAffiWarehouseAch(this).DeleteSalesUnitAffiWarehouse(salesUnitAffiWarehouse);
        }

        public IQueryable<SalesUnitAffiWarehouse> GetSalesUnitAffiWarehousesWithWarehouse() {
            return new SalesUnitAffiWarehouseAch(this).GetSalesUnitAffiWarehousesWithWarehouse();
        }
        public IQueryable<SalesUnitAffiWarehouse> GetWarehouseWithOriginalCompanyIdAndPartsSalesCategoryId(int originalCompanyId, int partsSalesCategoryId) {
            return new SalesUnitAffiWarehouseAch(this).GetWarehouseWithOriginalCompanyIdAndPartsSalesCategoryId(originalCompanyId, partsSalesCategoryId);
        }

        public IQueryable<SalesUnitAffiWarehouse> GetWarehousesBySalesUnitSalesCategoryId() {
            return new SalesUnitAffiWarehouseAch(this).GetWarehousesBySalesUnitSalesCategoryId();
        }

        public IQueryable<SalesUnitAffiWarehouse> 查询销售组织仓库(int enterpriseId, int? branchId, int? salesUnitId, int? partsSalesCategoryId, int? currentEnterPriseType) {
            return new SalesUnitAffiWarehouseAch(this).查询销售组织仓库(enterpriseId, branchId, salesUnitId, partsSalesCategoryId, currentEnterPriseType);
        }
        public IQueryable<SalesUnitAffiWarehouse> 查询销售组织仓库2(int enterpriseId, int? branchId, int? salesUnitId, int? partsSalesCategoryId) {
            return new SalesUnitAffiWarehouseAch(this).查询销售组织仓库2(enterpriseId, branchId, salesUnitId, partsSalesCategoryId);
        }

        [Query(HasSideEffects = true)]
        public IQueryable<SalesUnitAffiWarehouse> GetSalesUnitAffiWarehousesWithWarehouseAndSalesUnitBySalesUnitIds(int[] ids) {
            return new SalesUnitAffiWarehouseAch(this).GetSalesUnitAffiWarehousesWithWarehouseAndSalesUnitBySalesUnitIds(ids);
        }
    }
}
