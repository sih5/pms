﻿using System;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class SalesUnitAffiWhouseHistoryAch : DcsSerivceAchieveBase {
        public SalesUnitAffiWhouseHistoryAch(DcsDomainService domainService)
            : base(domainService) {
        }

        internal void InsertSalesUnitAffiWhouseHistoryValidate(SalesUnitAffiWhouseHistory salesUnitAffiWhouseHistory) {
            var userInfo = Utils.GetCurrentUserInfo();
            salesUnitAffiWhouseHistory.FilerId = userInfo.Id;
            salesUnitAffiWhouseHistory.FilerName = userInfo.Name;
            salesUnitAffiWhouseHistory.FileTime = DateTime.Now;
        }
        public void InsertSalesUnitAffiWhouseHistory(SalesUnitAffiWhouseHistory salesUnitAffiWhouseHistory) {
            InsertToDatabase(salesUnitAffiWhouseHistory);
            this.InsertSalesUnitAffiWhouseHistoryValidate(salesUnitAffiWhouseHistory);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               

        public void InsertSalesUnitAffiWhouseHistory(SalesUnitAffiWhouseHistory salesUnitAffiWhouseHistory) {
            new SalesUnitAffiWhouseHistoryAch(this).InsertSalesUnitAffiWhouseHistory(salesUnitAffiWhouseHistory);
        }
    }
}
