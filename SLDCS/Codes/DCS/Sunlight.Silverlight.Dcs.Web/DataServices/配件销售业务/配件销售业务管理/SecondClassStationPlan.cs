﻿using System;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class SecondClassStationPlanAch : DcsSerivceAchieveBase {
        internal void InsertSecondClassStationPlanValidate(SecondClassStationPlan secondClassStationPlan) {
            var userInfo = Utils.GetCurrentUserInfo();
            secondClassStationPlan.CreatorId = userInfo.Id;
            secondClassStationPlan.CreatorName = userInfo.Name;
            secondClassStationPlan.CreateTime = DateTime.Now;
            var subDealer = ObjectContext.SubDealers.SingleOrDefault(c => c.Id == secondClassStationPlan.SecondClassStationId);
            if(string.IsNullOrWhiteSpace(secondClassStationPlan.Code) || secondClassStationPlan.Code == GlobalVar.ASSIGNED_BY_SERVER)
                secondClassStationPlan.Code = CodeGenerator.Generate("SecondClassStationPlan", subDealer.Code);
        }
        internal void UpdateSecondClassStationPlanValidate(SecondClassStationPlan secondClassStationPlan) {
            var userInfo = Utils.GetCurrentUserInfo();
            secondClassStationPlan.ModifierId = userInfo.Id;
            secondClassStationPlan.ModifierName = userInfo.Name;
            secondClassStationPlan.ModifyTime = DateTime.Now;
        }
        public void InsertSecondClassStationPlan(SecondClassStationPlan secondClassStationPlan) {
            InsertToDatabase(secondClassStationPlan);
            var secondClassStationPlanDetails = ChangeSet.GetAssociatedChanges(secondClassStationPlan, c => c.SecondClassStationPlanDetails, ChangeOperation.Insert);
            foreach(SecondClassStationPlanDetail item in secondClassStationPlanDetails) {
                InsertToDatabase(item);
            }
            this.InsertSecondClassStationPlanValidate(secondClassStationPlan);
        }
        public void UpdateSecondClassStationPlan(SecondClassStationPlan secondClassStationPlan) {
            secondClassStationPlan.SecondClassStationPlanDetails.Clear();
            UpdateToDatabase(secondClassStationPlan);
            var secondClassStationPlanDetails = ChangeSet.GetAssociatedChanges(secondClassStationPlan, c => c.SecondClassStationPlanDetails);
            foreach(SecondClassStationPlanDetail item in secondClassStationPlanDetails) {
                switch(ChangeSet.GetChangeOperation(item)) {
                    case ChangeOperation.Insert:
                        InsertToDatabase(item);
                        break;
                    case ChangeOperation.Update:
                        UpdateToDatabase(item);
                        break;
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(item);
                        break;
                }
            }
            this.UpdateSecondClassStationPlanValidate(secondClassStationPlan);
        }
        public IQueryable<SecondClassStationPlan> GetSecondClassStationPlan() {
            return ObjectContext.SecondClassStationPlans.Include("SecondClassStationPlanDetails").OrderBy(c => c.Id);
        }
        public IQueryable<SecondClassStationPlan> GetSecondClassStationPlanWithDetail() {
            return ObjectContext.SecondClassStationPlans.Include("PartsSalesCategory").OrderBy(entity => entity.Id);
        }
        public IQueryable<SecondClassStationPlan> 查询二级站需求计划转销售订单() {
            var result = from a in ObjectContext.SecondClassStationPlans
                         from b in ObjectContext.SecondClassStationPlanDetails.Where(r => r.ProcessMode == (int)DcsProcessMode.转销售订单)
                         where a.Id == b.SecondClassStationPlanId
                         select a;
            return result.Include("SecondClassStationPlanDetails").OrderByDescending(r => r.CreateTime).Distinct();

        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               

        public void InsertSecondClassStationPlan(SecondClassStationPlan secondClassStationPlan) {
            new SecondClassStationPlanAch(this).InsertSecondClassStationPlan(secondClassStationPlan);
        }

        public void UpdateSecondClassStationPlan(SecondClassStationPlan secondClassStationPlan) {
            new SecondClassStationPlanAch(this).UpdateSecondClassStationPlan(secondClassStationPlan);
        }

        public IQueryable<SecondClassStationPlan> GetSecondClassStationPlan() {
            return new SecondClassStationPlanAch(this).GetSecondClassStationPlan();
        }

        public IQueryable<SecondClassStationPlan> GetSecondClassStationPlanWithDetail() {
            return new SecondClassStationPlanAch(this).GetSecondClassStationPlanWithDetail();
        }

        public IQueryable<SecondClassStationPlan> 查询二级站需求计划转销售订单() {
            return new SecondClassStationPlanAch(this).查询二级站需求计划转销售订单();
        }
    }
}
