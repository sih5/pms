﻿using System.Linq;
using Sunlight.Silverlight.Dcs.Web.Entities;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class ServiceAgentsHistoryQueryAch : DcsSerivceAchieveBase {
        public ServiceAgentsHistoryQueryAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public IQueryable<ServiceAgentsHistoryQuery> 服务站向代理库提报订单查询() {
            var result = from order in ObjectContext.PartsSalesOrders
                         join orderDetail in ObjectContext.PartsSalesOrderDetails on order.Id equals orderDetail.PartsSalesOrderId
                         join company in ObjectContext.Companies.Where(r => r.Type == 3 || r.Type == 7) on order.SalesUnitOwnerCompanyId equals company.Id
                         join warehouse in ObjectContext.Warehouses on order.WarehouseId equals warehouse.Id
                         select new ServiceAgentsHistoryQuery {
                             Id = order.Id,
                             SalesUnitOwnerCompanyId = order.SalesUnitOwnerCompanyId,
                             SalesUnitOwnerCompanyName = order.SalesUnitOwnerCompanyName,
                             SalesUnitOwnerCompanyCode = order.SalesUnitOwnerCompanyCode,
                             SubmitCompanyId = order.SubmitCompanyId,
                             SubmitCompanyName = order.SubmitCompanyName,
                             SubmitCompanyCode = order.SubmitCompanyCode,
                             SalesCategoryId = order.SalesCategoryId,
                             SalesCategoryName = order.SalesCategoryName,
                             Code = order.Code,
                             PartsSalesOrderTypeId = order.PartsSalesOrderTypeId,
                             PartsSalesOrderTypeName = order.PartsSalesOrderTypeName,
                             SparePartId = orderDetail.SparePartId,
                             SparePartCode = orderDetail.SparePartCode,
                             SparePartName = orderDetail.SparePartName,
                             OrderedQuantity = orderDetail.OrderedQuantity,
                             ApproveQuantity = orderDetail.ApproveQuantity,
                             OrderPrice = orderDetail.OrderPrice,
                             OrderSum = orderDetail.OrderSum,
                             WarehouseId = order.WarehouseId,
                             WarehouseName = warehouse.Name,
                             WarehouseCode = warehouse.Code,
                             IfDirectProvision = order.IfDirectProvision,
                             Status = order.Status,
                             IsDebt = order.IsDebt,
                             TotalAmount = order.TotalAmount,
                             Remark = order.Remark,
                             ShippingMethod = order.ShippingMethod,
                             ReceivingAddress = order.ReceivingAddress,
                             RequestedDeliveryTime = order.RequestedDeliveryTime,
                             StopComment = order.StopComment,
                             CreatorId = order.CreatorId,
                             CreatorName = order.CreatorName,
                             CreateTime = order.CreateTime,
                             ModifierId = order.ModifierId,
                             ModifierName = order.ModifierName,
                             ModifyTime = order.ModifyTime,
                             SubmitterId = order.SubmitterId,
                             SubmitterName = order.SubmitterName,
                             SubmitTime = order.SubmitTime,
                             ApproverId = order.ApproverId,
                             ApproverName = order.ApproverName,
                             ApproveTime = order.ApproveTime,
                             FirstApproveTime = order.FirstApproveTime,
                             AbandonerId = order.AbandonerId,
                             AbandonerName = order.AbandonerName,
                             AbandonTime = order.AbandonTime,
                             BranchId = order.BranchId,
                             BranchCode = order.BranchCode,
                             BranchName = order.BranchName
                         };
            return result.OrderBy(r => r.Id);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               

        public IQueryable<ServiceAgentsHistoryQuery> 服务站向代理库提报订单查询() {
            return new ServiceAgentsHistoryQueryAch(this).服务站向代理库提报订单查询();
        }
    }
}
