﻿using Sunlight.Silverlight.Web;
using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class CustomerOrderPriceGradeAch : DcsSerivceAchieveBase {
        internal void InsertCustomerOrderPriceGradeValidate(CustomerOrderPriceGrade customerOrderPriceGrade) {
            var dbcustomerOrderPriceGrade = ObjectContext.CustomerOrderPriceGrades.Where(r => r.CustomerCompanyId == customerOrderPriceGrade.CustomerCompanyId && r.PartsSalesOrderTypeId == customerOrderPriceGrade.PartsSalesOrderTypeId && r.Status != (int)DcsBaseDataStatus.作废).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbcustomerOrderPriceGrade != null)
                throw new ValidationException(string.Format(ErrorStrings.CustomerOrderPriceGrade_Validation1, customerOrderPriceGrade.PartsSalesOrderTypeName));
            var userInfo = Utils.GetCurrentUserInfo();
            customerOrderPriceGrade.CreatorId = userInfo.Id;
            customerOrderPriceGrade.CreatorName = userInfo.Name;
            customerOrderPriceGrade.CreateTime = DateTime.Now;
        }
        internal void UpdateCustomerOrderPriceGradeValidate(CustomerOrderPriceGrade customerOrderPriceGrade) {
            var dbcustomerOrderPriceGrade = ObjectContext.CustomerOrderPriceGrades.Where(r => r.Id != customerOrderPriceGrade.Id && r.CustomerCompanyId == customerOrderPriceGrade.CustomerCompanyId && r.PartsSalesOrderTypeId == customerOrderPriceGrade.PartsSalesOrderTypeId && r.Status != (int)DcsBaseDataStatus.作废).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbcustomerOrderPriceGrade != null)
                throw new ValidationException(string.Format(ErrorStrings.CustomerOrderPriceGrade_Validation1, customerOrderPriceGrade.PartsSalesOrderTypeName));
            var userInfo = Utils.GetCurrentUserInfo();
            customerOrderPriceGrade.ModifierId = userInfo.Id;
            customerOrderPriceGrade.ModifierName = userInfo.Name;
            customerOrderPriceGrade.ModifyTime = DateTime.Now;
        }
        public void InsertCustomerOrderPriceGrade(CustomerOrderPriceGrade customerOrderPriceGrade) {
            InsertToDatabase(customerOrderPriceGrade);
            this.InsertCustomerOrderPriceGradeValidate(customerOrderPriceGrade);
        }
        public void UpdateCustomerOrderPriceGrade(CustomerOrderPriceGrade customerOrderPriceGrade) {
            UpdateToDatabase(customerOrderPriceGrade);
            this.UpdateCustomerOrderPriceGradeValidate(customerOrderPriceGrade);
        }
        public IQueryable<CustomerOrderPriceGrade> GetCustomerCompanyAndPartsSalesOrderType() {
            return ObjectContext.CustomerOrderPriceGrades.Include("Company").Include("PartsSalesOrderType").OrderBy(v => v.Id);
        }
        public IQueryable<CustomerOrderPriceGrade> GetCustomerCompanyAndPartsSalesPartsSalesCategory() {
            return ObjectContext.CustomerOrderPriceGrades.Include("Company").Include("PartsSalesCategory").OrderBy(v => v.Id);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               

        public void InsertCustomerOrderPriceGrade(CustomerOrderPriceGrade customerOrderPriceGrade) {
            new CustomerOrderPriceGradeAch(this).InsertCustomerOrderPriceGrade(customerOrderPriceGrade);
        }

        public void UpdateCustomerOrderPriceGrade(CustomerOrderPriceGrade customerOrderPriceGrade) {
            new CustomerOrderPriceGradeAch(this).UpdateCustomerOrderPriceGrade(customerOrderPriceGrade);
        }

        public IQueryable<CustomerOrderPriceGrade> GetCustomerCompanyAndPartsSalesOrderType() {
            return new CustomerOrderPriceGradeAch(this).GetCustomerCompanyAndPartsSalesOrderType();
        }
                public IQueryable<CustomerOrderPriceGrade> GetCustomerCompanyAndPartsSalesPartsSalesCategory() {
            return new CustomerOrderPriceGradeAch(this).GetCustomerCompanyAndPartsSalesPartsSalesCategory();
        }
    }
}
