﻿using Sunlight.Silverlight.Web;
using System;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class CustomerOrderPriceGradeHistoryAch : DcsSerivceAchieveBase {
        public CustomerOrderPriceGradeHistoryAch(DcsDomainService domainService)
            : base(domainService) {
        }

        internal void InsertCustomerOrderPriceGradeHistoryValidate(CustomerOrderPriceGradeHistory customerOrderPriceGradeHistory) {
            var userInfo = Utils.GetCurrentUserInfo();
            customerOrderPriceGradeHistory.CreatorId = userInfo.Id;
            customerOrderPriceGradeHistory.CreatorName = userInfo.Name;
            customerOrderPriceGradeHistory.CreateTime = DateTime.Now;
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               
    //使用工具生成后没有任何共有方法 NoPublic
    }
}
