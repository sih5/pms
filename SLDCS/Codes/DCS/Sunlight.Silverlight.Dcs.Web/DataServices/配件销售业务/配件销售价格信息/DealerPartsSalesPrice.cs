﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Microsoft.Data.Extensions;
using System.Collections.Generic;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class DealerPartsSalesPriceAch : DcsSerivceAchieveBase {
        internal void InsertDealerPartsSalesPriceValidate(DealerPartsSalesPrice dealerPartsSalesPrice) {
            var dbDealerPartsSalesPrice = ObjectContext.DealerPartsSalesPrices.Where(r => r.PartsSalesCategoryId == dealerPartsSalesPrice.PartsSalesCategoryId && r.SparePartId == dealerPartsSalesPrice.SparePartId && r.Status != (int)DcsBaseDataStatus.作废).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbDealerPartsSalesPrice != null)
                throw new ValidationException(string.Format("名称为“{0}”配件，已经存在销售类型为“{1}”的服务站配件批发价", dealerPartsSalesPrice.SparePartName, dealerPartsSalesPrice.PartsSalesCategoryName));
            var userInfo = Utils.GetCurrentUserInfo();
            dealerPartsSalesPrice.CreatorId = userInfo.Id;
            dealerPartsSalesPrice.CreatorName = userInfo.Name;
            dealerPartsSalesPrice.CreateTime = DateTime.Now;
        }
        internal void UpdateDealerPartsSalesPriceValidate(DealerPartsSalesPrice dealerPartsSalesPrice) {
            var DealerPartsSalesPriceLegitimacy = ObjectContext.DealerPartsSalesPrices.Where(r => r.Id == dealerPartsSalesPrice.Id && r.Status == (int)DcsBaseDataStatus.作废).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(DealerPartsSalesPriceLegitimacy != null)
                throw new ValidationException("只能修改处于“有效”状态下的服务站配件批发价");
            var userInfo = Utils.GetCurrentUserInfo();
            dealerPartsSalesPrice.ModifierId = userInfo.Id;
            dealerPartsSalesPrice.ModifierName = userInfo.Name;
            dealerPartsSalesPrice.ModifyTime = DateTime.Now;
        }
        public void InsertDealerPartsSalesPrice(DealerPartsSalesPrice dealerPartsSalesPrice) {
            InsertToDatabase(dealerPartsSalesPrice);
            this.InsertDealerPartsSalesPriceValidate(dealerPartsSalesPrice);
        }
        public void UpdateDealerPartsSalesPrice(DealerPartsSalesPrice dealerPartsSalesPrice) {
            UpdateToDatabase(dealerPartsSalesPrice);
            this.UpdateDealerPartsSalesPriceValidate(dealerPartsSalesPrice);
        }

        public IEnumerable<VirtualDealerPartsSalesPrice> 查询配件经销价和零售价(string sparePartCode,string sparePartName,int? status,DateTime? bCreateTime, DateTime? eCreateTime) {
            var result = from a in ObjectContext.SpareParts.Where(r => r.Status != (int)DcsBaseDataStatus.作废)
                         join b in ObjectContext.PartsSalesPrices on a.Id equals b.SparePartId into temp1
                         from t1 in temp1.DefaultIfEmpty()
                         join c in ObjectContext.PartsRetailGuidePrices on a.Id equals c.SparePartId into temp2
                         from t2 in temp2.DefaultIfEmpty()
                         select new VirtualDealerPartsSalesPrice {
                             Id = a.Id,
                             PartCode = a.Code,
                             PartName = a.Name,
                             SalesPrice = t1.SalesPrice,
                             RetailGuidePrice = t2.RetailGuidePrice,
                             CreatorName = t1.CreatorName,
                             CreateTime = t1.CreateTime,
                             ModifierName = t1.ModifierName,
                             ModifyTime = t1.ModifyTime,
                             Status = t1.Status
                         };

            return result.Distinct().OrderBy(r => r.Id);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               

        public IEnumerable<VirtualDealerPartsSalesPrice> 查询配件经销价和零售价(string sparePartCode, string sparePartName, int? status, DateTime? bCreateTime, DateTime? eCreateTime) {
            return new DealerPartsSalesPriceAch(this).查询配件经销价和零售价(sparePartCode,sparePartName,status,bCreateTime,eCreateTime);
        }
        public void InsertDealerPartsSalesPrice(DealerPartsSalesPrice dealerPartsSalesPrice) {
            new DealerPartsSalesPriceAch(this).InsertDealerPartsSalesPrice(dealerPartsSalesPrice);
        }

        public void UpdateDealerPartsSalesPrice(DealerPartsSalesPrice dealerPartsSalesPrice) {
            new DealerPartsSalesPriceAch(this).UpdateDealerPartsSalesPrice(dealerPartsSalesPrice);
        }
    }
}
