﻿using Sunlight.Silverlight.Web;
using System;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class DealerPartsSalesPriceHistoryAch : DcsSerivceAchieveBase {
        public DealerPartsSalesPriceHistoryAch(DcsDomainService domainService)
            : base(domainService) {
        }

        internal void InsertDealerPartsSalesPriceHistoryValidate(DealerPartsSalesPriceHistory dealerPartsSalesPriceHistory) {
            var userInfo = Utils.GetCurrentUserInfo();
            dealerPartsSalesPriceHistory.CreatorId = userInfo.Id;
            dealerPartsSalesPriceHistory.CreatorName = userInfo.Name;
            dealerPartsSalesPriceHistory.CreateTime = DateTime.Now;
        }
        public void InsertDealerPartsSalesPriceHistory(DealerPartsSalesPriceHistory dealerPartsSalesPriceHistory) {
            InsertToDatabase(dealerPartsSalesPriceHistory);
            this.InsertDealerPartsSalesPriceHistoryValidate(dealerPartsSalesPriceHistory);
        }

    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               

        public void InsertDealerPartsSalesPriceHistory(DealerPartsSalesPriceHistory dealerPartsSalesPriceHistory) {
            new DealerPartsSalesPriceHistoryAch(this).InsertDealerPartsSalesPriceHistory(dealerPartsSalesPriceHistory);
        }
    }
}
