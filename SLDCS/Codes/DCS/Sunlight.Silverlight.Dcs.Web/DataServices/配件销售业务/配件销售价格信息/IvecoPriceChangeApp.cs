﻿using System;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class IvecoPriceChangeAppAch : DcsSerivceAchieveBase {
        public IvecoPriceChangeAppAch(DcsDomainService domainService)
            : base(domainService) {
        }
        internal void InsertIvecoPriceChangeAppValidate(IvecoPriceChangeApp ivecoPriceChangeApp) {
            if(string.IsNullOrWhiteSpace(ivecoPriceChangeApp.Code) || ivecoPriceChangeApp.Code == GlobalVar.ASSIGNED_BY_SERVER)
                ivecoPriceChangeApp.Code = CodeGenerator.Generate("IvecoPriceChangeApp");
            var userInfo = Utils.GetCurrentUserInfo();
            ivecoPriceChangeApp.CreatorId = userInfo.Id;
            ivecoPriceChangeApp.CreatorName = userInfo.Name;
            ivecoPriceChangeApp.CreateTime = DateTime.Now;
        }

        internal void UpdateIvecoPriceChangeAppValidate(IvecoPriceChangeApp ivecoPriceChangeApp) {
            var userInfo = Utils.GetCurrentUserInfo();
            ivecoPriceChangeApp.ModifierId = userInfo.Id;
            ivecoPriceChangeApp.ModifierName = userInfo.Name;
            ivecoPriceChangeApp.ModifyTime = DateTime.Now;
        }

        public void InsertIvecoPriceChangeApp(IvecoPriceChangeApp ivecoPriceChangeApp) {
            InsertToDatabase(ivecoPriceChangeApp);
            var ivecoPriceChangeAppDetails = ChangeSet.GetAssociatedChanges(ivecoPriceChangeApp, v => v.IvecoPriceChangeAppDetails, ChangeOperation.Insert);
            foreach(IvecoPriceChangeAppDetail ivecoPriceChangeAppDetail in ivecoPriceChangeAppDetails) {
                ivecoPriceChangeAppDetail.IvecoPrice = Math.Round(ivecoPriceChangeAppDetail.IvecoPrice ?? 0, 2, MidpointRounding.AwayFromZero);
                InsertToDatabase(ivecoPriceChangeAppDetail);
            }
            this.InsertIvecoPriceChangeAppValidate(ivecoPriceChangeApp);
        }

        public void UpdateIvecoPriceChangeApp(IvecoPriceChangeApp ivecoPriceChangeApp) {
            ivecoPriceChangeApp.IvecoPriceChangeAppDetails.Clear();
            UpdateToDatabase(ivecoPriceChangeApp);
            var ivecoPriceChangeAppDetails = ChangeSet.GetAssociatedChanges(ivecoPriceChangeApp, v => v.IvecoPriceChangeAppDetails);
            foreach(IvecoPriceChangeAppDetail ivecoPriceChangeAppDetail in ivecoPriceChangeAppDetails) {
                ivecoPriceChangeAppDetail.IvecoPrice = Math.Round(ivecoPriceChangeAppDetail.IvecoPrice ?? 0, 2, MidpointRounding.AwayFromZero);
                //获取清单实体对象的操作类型
                switch(ChangeSet.GetChangeOperation(ivecoPriceChangeAppDetail)) {
                    //新增操作
                    case ChangeOperation.Insert:
                        InsertToDatabase(ivecoPriceChangeAppDetail);
                        break;
                    //更新操作
                    case ChangeOperation.Update:
                        UpdateToDatabase(ivecoPriceChangeAppDetail);
                        break;
                    //删除操作
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(ivecoPriceChangeAppDetail);
                        break;
                }
            }
            this.UpdateIvecoPriceChangeAppValidate(ivecoPriceChangeApp);
        }

        public IvecoPriceChangeApp GetIvecoPriceChangeAppWithDetailsById(int id) {
            var dbPrtsSalesPriceChange = ObjectContext.IvecoPriceChangeApps.Include("PartsSalesCategory").SingleOrDefault(e => e.Id == id);
            if(dbPrtsSalesPriceChange != null) {
                var dbDetails = ObjectContext.IvecoPriceChangeAppDetails.Include("").Where(ex => ex.IvecoPriceChangeAppId == dbPrtsSalesPriceChange.Id).ToArray();
                var sparePartIds = dbDetails.Select(r => r.SparePartId).ToArray();
            }
            return dbPrtsSalesPriceChange;
        }

        public IQueryable<IvecoPriceChangeApp> GetIvecoPriceChangeAppById(int id) {
            return ObjectContext.IvecoPriceChangeApps.Where(r => r.Id == id);
        }

        public IQueryable<IvecoPriceChangeApp> GetIvecoPriceChangeAppsForOrder() {
            return ObjectContext.IvecoPriceChangeApps.OrderByDescending(r => r.CreateTime);
        }

    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               

        public void InsertIvecoPriceChangeApp(IvecoPriceChangeApp ivecoPriceChangeApp) {
            new IvecoPriceChangeAppAch(this).InsertIvecoPriceChangeApp(ivecoPriceChangeApp);
        }

        public void UpdateIvecoPriceChangeApp(IvecoPriceChangeApp ivecoPriceChangeApp) {
            new IvecoPriceChangeAppAch(this).UpdateIvecoPriceChangeApp(ivecoPriceChangeApp);
        }

        public IvecoPriceChangeApp GetIvecoPriceChangeAppWithDetailsById(int id) {
            return new IvecoPriceChangeAppAch(this).GetIvecoPriceChangeAppWithDetailsById(id);
        }

        public IQueryable<IvecoPriceChangeApp> GetIvecoPriceChangeAppById(int id) {
            return new IvecoPriceChangeAppAch(this).GetIvecoPriceChangeAppById(id);
        }

        public IQueryable<IvecoPriceChangeApp> GetIvecoPriceChangeAppsForOrder() {
            return new IvecoPriceChangeAppAch(this).GetIvecoPriceChangeAppsForOrder();
        }
    }
}
