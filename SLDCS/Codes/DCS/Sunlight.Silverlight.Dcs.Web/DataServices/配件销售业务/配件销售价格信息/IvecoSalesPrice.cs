﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Microsoft.Data.Extensions;
using NPOI.HSSF.Record.Chart;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
using System.Collections.Generic;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class IvecoSalesPriceAch : DcsSerivceAchieveBase {
        public IvecoSalesPriceAch(DcsDomainService domainService)
            : base(domainService) {
        }
        internal void InsertIvecoSalesPriceValidate(IvecoSalesPrice ivecoSalesPrice) {
            var dbivecoSalesPrice = ObjectContext.IvecoSalesPrices.Where(r => r.PartsSalesCategoryId == ivecoSalesPrice.PartsSalesCategoryId && r.SparePartId == ivecoSalesPrice.SparePartId).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbivecoSalesPrice != null)
                throw new ValidationException(string.Format(ErrorStrings.IvecoSalesPrice_Validation1, ivecoSalesPrice.SparePartName, ivecoSalesPrice.PartsSalesCategoryName));
            var userInfo = Utils.GetCurrentUserInfo();
            ivecoSalesPrice.CreatorId = userInfo.Id;
            ivecoSalesPrice.CreatorName = userInfo.Name;
            ivecoSalesPrice.CreateTime = DateTime.Now;
        }

        internal void UpdateIvecoSalesPriceValidate(IvecoSalesPrice ivecoSalesPrice) {
            var partsSalespriceLegitimacy = ObjectContext.IvecoSalesPrices.Where(r => r.Id == ivecoSalesPrice.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(partsSalespriceLegitimacy != null)
                throw new ValidationException(string.Format(ErrorStrings.IvecoSalesPrice_Validation2));
            var dbivecoSalesPriceGroup = ObjectContext.IvecoSalesPrices.Where(r => r.Id != ivecoSalesPrice.Id && r.PartsSalesCategoryId == ivecoSalesPrice.PartsSalesCategoryId && r.SparePartId == ivecoSalesPrice.SparePartId).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbivecoSalesPriceGroup != null)
                throw new ValidationException(string.Format(ErrorStrings.IvecoSalesPrice_Validation1, ivecoSalesPrice.SparePartName, ivecoSalesPrice.PartsSalesCategoryName));
            var userInfo = Utils.GetCurrentUserInfo();
            ivecoSalesPrice.ModifierId = userInfo.Id;
            ivecoSalesPrice.ModifierName = userInfo.Name;
            ivecoSalesPrice.ModifyTime = DateTime.Now;
        }

        public void InsertIvecoSalesPrice(IvecoSalesPrice ivecoSalesPrice) {
            InsertToDatabase(ivecoSalesPrice);
            this.InsertIvecoSalesPriceValidate(ivecoSalesPrice);
        }

        public void UpdateIvecoSalesPrice(IvecoSalesPrice ivecoSalesPrice) {
            UpdateToDatabase(ivecoSalesPrice);
            this.UpdateIvecoSalesPriceValidate(ivecoSalesPrice);
        }

        //public IQueryable<IvecoSalesPrice> GetIvecoSalesPriceWithHistory() {
        //    return ObjectContext.IvecoSalesPrices.Include("IvecoSalesPriceHistory");
        //}

        //public IQueryable<IvecoSalesPrice> 查询配件销售价(bool? isSale, bool? isOrderable, int? PartsSalesCategoryId) {
        //    var result = ObjectContext.IvecoSalesPrices;
        //    if(PartsSalesCategoryId.HasValue)
        //        result = result.Where(r => r.PartsSalesCategoryId == PartsSalesCategoryId.Value);
        //    if(isSale.HasValue)
        //        result = result.Where(r => ObjectContext.PartsBranches.Any(v => r.PartsSalesCategoryId == v.PartsSalesCategoryId && r.SparePartId == v.PartId && v.IsSalable == isSale.Value));
        //    if(isOrderable.HasValue)
        //        result = result.Where(r => ObjectContext.PartsBranches.Any(v => r.PartsSalesCategoryId == v.PartsSalesCategoryId && r.SparePartId == v.PartId && v.IsOrderable == isOrderable.Value));
        //    return result.Include("SparePart").OrderBy(r => r.Id);
        //}

        //public IQueryable<IvecoSalesPrice> GetIvecoSalesPriceBySparePartIds(int[] sparePartIds) {
        //    return ObjectContext.IvecoSalesPrices.Where(r => sparePartIds.Contains(r.SparePartId));
        //}
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               

        public void InsertIvecoSalesPrice(IvecoSalesPrice ivecoSalesPrice) {
            new IvecoSalesPriceAch(this).InsertIvecoSalesPrice(ivecoSalesPrice);
        }

        public void UpdateIvecoSalesPrice(IvecoSalesPrice ivecoSalesPrice) {
            new IvecoSalesPriceAch(this).UpdateIvecoSalesPrice(ivecoSalesPrice);
        }

        //public IQueryable<IvecoSalesPrice> GetIvecoSalesPriceWithHistory() {
        //    return new IvecoSalesPriceAch(this).GetIvecoSalesPriceWithHistory();
        //}

        //public IQueryable<IvecoSalesPrice> 查询配件销售价(bool? isSale, bool? isOrderable, int? PartsSalesCategoryId) {
        //    return new IvecoSalesPriceAch(this).查询配件销售价(isSale, isOrderable, PartsSalesCategoryId);
        //}

        //public IQueryable<IvecoSalesPrice> GetIvecoSalesPriceBySparePartIds(int[] sparePartIds) {
        //    return new IvecoSalesPriceAch(this).GetIvecoSalesPriceBySparePartIds(sparePartIds);
        //}
    }
}