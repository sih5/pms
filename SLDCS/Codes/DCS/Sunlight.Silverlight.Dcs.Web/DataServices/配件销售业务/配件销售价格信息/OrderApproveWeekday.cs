﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class OrderApproveWeekdayAch : DcsSerivceAchieveBase {
        internal void InsertOrderApproveWeekdayValidate(OrderApproveWeekday orderApproveWeekday) {
            var dbOrderApproveWeekday = ObjectContext.OrderApproveWeekdays.Where(r => r.PartsSalesCategoryId == orderApproveWeekday.PartsSalesCategoryId && r.ProvinceId == orderApproveWeekday.ProvinceId && r.Status == (int)DcsBaseDataStatus.有效 && r.WeeksName == orderApproveWeekday.WeeksName).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbOrderApproveWeekday != null)
                throw new ValidationException(string.Format("品牌为“{0}”，省份为“{1}”，储备订单审核日为“{2}” 有效状态的数据已存在", orderApproveWeekday.PartsSalesCategoryName, orderApproveWeekday.ProvinceName, Enum.GetName(typeof(DcsOrderAppDate), orderApproveWeekday.WeeksName)));
            var userInfo = Utils.GetCurrentUserInfo();
            orderApproveWeekday.CreatorId = userInfo.Id;
            orderApproveWeekday.CreatorName = userInfo.Name;
            orderApproveWeekday.CreateTime = DateTime.Now;
        }
        internal void UpdateOrderApproveWeekdayValidate(OrderApproveWeekday orderApproveWeekday) {
            var dbOrderApproveWeekday = ObjectContext.OrderApproveWeekdays.Where(r => r.Id != orderApproveWeekday.Id && r.PartsSalesCategoryId == orderApproveWeekday.PartsSalesCategoryId && r.ProvinceId == orderApproveWeekday.ProvinceId && r.Status == (int)DcsBaseDataStatus.有效 && r.WeeksName == orderApproveWeekday.WeeksName).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbOrderApproveWeekday != null)
                throw new ValidationException(string.Format("品牌为“{0}”，省份为“{1}”，储备订单审核日为“{2}” 有效状态的数据已存在", orderApproveWeekday.PartsSalesCategoryName, orderApproveWeekday.ProvinceName, Enum.GetName(typeof(DcsOrderAppDate), orderApproveWeekday.WeeksName)));
            var userInfo = Utils.GetCurrentUserInfo();
            orderApproveWeekday.ModifierId = userInfo.Id;
            orderApproveWeekday.ModifierName = userInfo.Name;
            orderApproveWeekday.ModifyTime = DateTime.Now;
        }
        public void InsertOrderApproveWeekday(OrderApproveWeekday orderApproveWeekday) {
            InsertToDatabase(orderApproveWeekday);
            this.InsertOrderApproveWeekdayValidate(orderApproveWeekday);
        }
        public void UpdateOrderApproveWeekday(OrderApproveWeekday orderApproveWeekday) {
            UpdateToDatabase(orderApproveWeekday);
            this.UpdateOrderApproveWeekdayValidate(orderApproveWeekday);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               

        public void InsertOrderApproveWeekday(OrderApproveWeekday orderApproveWeekday) {
            new OrderApproveWeekdayAch(this).InsertOrderApproveWeekday(orderApproveWeekday);
        }

        public void UpdateOrderApproveWeekday(OrderApproveWeekday orderApproveWeekday) {
            new OrderApproveWeekdayAch(this).UpdateOrderApproveWeekday(orderApproveWeekday);
        }
    }
}
