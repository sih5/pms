﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsRetailGuidePriceAch : DcsSerivceAchieveBase {
        internal void InsertPartsRetailGuidePriceValidate(PartsRetailGuidePrice partsRetailGuidePrice) {
            var dbpartsRetailGuidePrice = ObjectContext.PartsRetailGuidePrices.Where(r => r.SparePartId == partsRetailGuidePrice.SparePartId && r.PartsSalesCategoryId == partsRetailGuidePrice.PartsSalesCategoryId && r.Status != (int)DcsBaseDataStatus.作废).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbpartsRetailGuidePrice != null)
                throw new ValidationException(string.Format(ErrorStrings.PartsRetailGuidePrice_Validation1, dbpartsRetailGuidePrice.SparePartCode));
            var userInfo = Utils.GetCurrentUserInfo();
            partsRetailGuidePrice.CreatorId = userInfo.Id;
            partsRetailGuidePrice.CreatorName = userInfo.Name;
            partsRetailGuidePrice.CreateTime = DateTime.Now;
        }

        internal void UpdatePartsRetailGuidePriceValidate(PartsRetailGuidePrice partsRetailGuidePrice) {
            var dbpartsRetailGuidePrice = ObjectContext.PartsRetailGuidePrices.Where(r => r.Id == partsRetailGuidePrice.Id && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbpartsRetailGuidePrice == null)
                throw new ValidationException(ErrorStrings.PartsRetailGuidePrice_Validation2);
            var userInfo = Utils.GetCurrentUserInfo();
            partsRetailGuidePrice.ModifierId = userInfo.Id;
            partsRetailGuidePrice.ModifierName = userInfo.Name;
            partsRetailGuidePrice.ModifyTime = DateTime.Now;
        }

        public void InsertPartsRetailGuidePrice(PartsRetailGuidePrice partsRetailGuidePrice) {
            InsertToDatabase(partsRetailGuidePrice);
            this.InsertPartsRetailGuidePriceValidate(partsRetailGuidePrice);
        }

        public void UpdatePartsRetailGuidePrice(PartsRetailGuidePrice partsRetailGuidePrice) {
            UpdateToDatabase(partsRetailGuidePrice);
            this.UpdatePartsRetailGuidePriceValidate(partsRetailGuidePrice);
        }


        public IQueryable<PartsRetailGuidePrice> 查询配件零售指导价(bool? isSale, bool? isOrderable, int? PartsSalesCategoryId) {
            var result = ObjectContext.PartsRetailGuidePrices.Where(r => r.Status != (int)DcsBaseDataStatus.作废);
            if(PartsSalesCategoryId.HasValue)
                result = result.Where(r => r.PartsSalesCategoryId == PartsSalesCategoryId.Value);
            if(isSale.HasValue)
                result = result.Where(r => ObjectContext.PartsBranches.Any(v => r.PartsSalesCategoryId == v.PartsSalesCategoryId && r.SparePartId == v.PartId && v.IsSalable == isSale.Value));
            if(isOrderable.HasValue)
                result = result.Where(r => ObjectContext.PartsBranches.Any(v => r.PartsSalesCategoryId == v.PartsSalesCategoryId && r.SparePartId == v.PartId && v.IsOrderable == isOrderable.Value));
            return result.OrderBy(r => r.Id);
        }

        public IQueryable<PartsRetailGuidePrice> GetPartsRetailGuidePriceBySparePartIds(int[] ids) {
            return ObjectContext.PartsRetailGuidePrices.Where(r => r.Status != (int)DcsBaseDataStatus.作废 && ids.Contains(r.SparePartId) && r.ValidationTime <= DateTime.Now && r.ExpireTime >= DateTime.Now);
        }
        public IQueryable<VirTualPartsRetailGuidePrice> GetPartsRetailGuidePriceByBranchType(bool? isSale, bool? isOrderable)
        {
            var partsRetailGuidePrices = ObjectContext.PartsRetailGuidePrices.Where(r => r.Status == (int)DcsBaseDataStatus.有效);

            if(isSale.HasValue)
                partsRetailGuidePrices = partsRetailGuidePrices.Where(r => ObjectContext.PartsBranches.Any(v => r.PartsSalesCategoryId == v.PartsSalesCategoryId && r.SparePartId == v.PartId && v.IsSalable == isSale.Value));
            if(isOrderable.HasValue)
                partsRetailGuidePrices = partsRetailGuidePrices.Where(r => ObjectContext.PartsBranches.Any(v => r.PartsSalesCategoryId == v.PartsSalesCategoryId && r.SparePartId == v.PartId && v.IsOrderable == isOrderable.Value));
                    var result = from a in partsRetailGuidePrices
                                 join b in ObjectContext.PartsSalesPrices on new { a.SparePartId, a.BranchId, a.PartsSalesCategoryId, Status = (int)DcsBaseDataStatus.有效 } equals new { b.SparePartId, b.BranchId, b.PartsSalesCategoryId, b.Status } into c
                                 from d in c.DefaultIfEmpty()
                                 select new VirTualPartsRetailGuidePrice
                                 {
                                     Id=a.Id,
                                     SparePartName=a.SparePartName,
                                     BranchId=a.BranchId,
                                     PartsSalesCategoryId = a.PartsSalesCategoryId,
                                     PartsSalesCategoryCode = a.PartsSalesCategoryCode,
                                     PartsSalesCategoryName = a.PartsSalesCategoryName,
                                     SparePartId = a.SparePartId,
                                     SparePartCode = a.SparePartCode,
                                     RetailGuidePrice = a.RetailGuidePrice,
                                     ValidationTime = a.ValidationTime,
                                     ExpireTime = a.ExpireTime,
                                     Status = a.Status,
                                     SalesPrice=d.SalesPrice
                                 };
                    return result.OrderBy(r => r.Id);

           
        }
    
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               

        public void InsertPartsRetailGuidePrice(PartsRetailGuidePrice partsRetailGuidePrice) {
            new PartsRetailGuidePriceAch(this).InsertPartsRetailGuidePrice(partsRetailGuidePrice);
        }

        public void UpdatePartsRetailGuidePrice(PartsRetailGuidePrice partsRetailGuidePrice) {
            new PartsRetailGuidePriceAch(this).UpdatePartsRetailGuidePrice(partsRetailGuidePrice);
        }
        

        public IQueryable<PartsRetailGuidePrice> 查询配件零售指导价(bool? isSale, bool? isOrderable, int? PartsSalesCategoryId) {
            return new PartsRetailGuidePriceAch(this).查询配件零售指导价(isSale,isOrderable,PartsSalesCategoryId);
        }

        public IQueryable<PartsRetailGuidePrice> GetPartsRetailGuidePriceBySparePartIds(int[] ids) {
            return new PartsRetailGuidePriceAch(this).GetPartsRetailGuidePriceBySparePartIds(ids);
        }
        public IQueryable<VirTualPartsRetailGuidePrice> GetPartsRetailGuidePriceByBranchType(bool? isSale, bool? isOrderable)
        {
            return new PartsRetailGuidePriceAch(this).GetPartsRetailGuidePriceByBranchType(isSale,isOrderable);
        }
    }
}
