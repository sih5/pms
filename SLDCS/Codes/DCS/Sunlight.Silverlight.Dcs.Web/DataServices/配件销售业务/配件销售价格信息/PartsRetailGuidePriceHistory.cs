﻿using System;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsRetailGuidePriceHistoryAch : DcsSerivceAchieveBase {
        public PartsRetailGuidePriceHistoryAch(DcsDomainService domainService)
            : base(domainService) {
        }

        internal void InsertPartsRetailGuidePriceHistoryValidate(PartsRetailGuidePriceHistory partsRetailGuidePriceHistory) {
            var userInfo = Utils.GetCurrentUserInfo();
            partsRetailGuidePriceHistory.CreatorId = userInfo.Id;
            partsRetailGuidePriceHistory.CreatorName = userInfo.Name;
            partsRetailGuidePriceHistory.CreateTime = DateTime.Now;
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               
    //使用工具生成后没有任何共有方法 NoPublic
    }
}
