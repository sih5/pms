﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Web;using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsSalePriceIncreaseRateAch : DcsSerivceAchieveBase {
        public void UpdatePartsSalePriceIncreaseRate(PartsSalePriceIncreaseRate pspir) {
            this.UpdateToDatabase(pspir);
            this.UpdatePartsSalePriceIncreaseRateValidate(pspir);
        }
        internal void UpdatePartsSalePriceIncreaseRateValidate(PartsSalePriceIncreaseRate pspir) {
            var dbpspir = this.ObjectContext.PartsSalePriceIncreaseRates.Where(v => v.Id == pspir.Id && v.status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpspir == null)
                throw new ValidationException(ErrorStrings.PartsSalePriceIncreaseRate_Validation1);
            var userInfo = Utils.GetCurrentUserInfo();
            pspir.ModifierId = userInfo.Id;
            pspir.ModifierName = userInfo.Name;
            pspir.ModifyTime = DateTime.Now;
        }
        public void InsertPartsSalePriceIncreaseRate(PartsSalePriceIncreaseRate pspir) {
            this.InsertToDatabase(pspir);
            this.InsertPartsSalePriceIncreaseRateValidate(pspir);
        }
        internal void InsertPartsSalePriceIncreaseRateValidate(PartsSalePriceIncreaseRate pspir) {
            var dbpspir = this.ObjectContext.PartsSalePriceIncreaseRates.Where(v => v.GroupCode == pspir.GroupCode && v.status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpspir != null)
                throw new ValidationException(ErrorStrings.PartsSalePriceIncreaseRate_Validation3);
            var userInfo = Utils.GetCurrentUserInfo();
            pspir.CreatorId = userInfo.Id;
            pspir.CreatorName = userInfo.Name;
            pspir.CreateTime = DateTime.Now;
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               

        public void UpdatePartsSalePriceIncreaseRate(PartsSalePriceIncreaseRate pspir) {
            new PartsSalePriceIncreaseRateAch(this).UpdatePartsSalePriceIncreaseRate(pspir);
        }

        public void InsertPartsSalePriceIncreaseRate(PartsSalePriceIncreaseRate pspir) {
            new PartsSalePriceIncreaseRateAch(this).InsertPartsSalePriceIncreaseRate(pspir);
        }
    }
}
