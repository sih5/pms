﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsSalesCategoryAch : DcsSerivceAchieveBase {
        internal void InsertPartsSalesCategoryValidate(PartsSalesCategory partsSalesCategory) {
            var partsSalesCategoryCode = ObjectContext.PartsSalesCategories.Where(r => r.BranchId == partsSalesCategory.BranchId && r.Code.ToLower() == partsSalesCategory.Code.ToLower() && r.Status != (int)DcsBaseDataStatus.作废).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(partsSalesCategoryCode != null)
                throw new ValidationException(string.Format(ErrorStrings.PartsSalesCategory_Validation1, partsSalesCategory.Code));
            var partsSalesCategoryName = ObjectContext.PartsSalesCategories.Where(r => r.BranchId == partsSalesCategory.BranchId && r.Name.ToLower() == partsSalesCategory.Name.ToLower() && r.Status != (int)DcsBaseDataStatus.作废).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(partsSalesCategoryName != null)
                throw new ValidationException(string.Format(ErrorStrings.PartsSalesCategory_Validation2, partsSalesCategory.Name));
            var userInfo = Utils.GetCurrentUserInfo();
            partsSalesCategory.CreatorId = userInfo.Id;
            partsSalesCategory.CreatorName = userInfo.Name;
            partsSalesCategory.CreateTime = DateTime.Now;
        }
        internal void UpdatePartsSalesCategoryValidate(PartsSalesCategory partsSalesCategory) {
            var partsSalesCategoryCode = ObjectContext.PartsSalesCategories.Where(r => r.Id != partsSalesCategory.Id && r.BranchId == partsSalesCategory.BranchId && r.Code.ToLower() == partsSalesCategory.Code.ToLower() && r.Status != (int)DcsBaseDataStatus.作废).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(partsSalesCategoryCode != null)
                throw new ValidationException(string.Format(ErrorStrings.PartsSalesCategory_Validation1, partsSalesCategory.Code));
            var partsSalesCategoryName = ObjectContext.PartsSalesCategories.Where(r => r.Id != partsSalesCategory.Id && r.BranchId == partsSalesCategory.BranchId && r.Name.ToLower() == partsSalesCategory.Name.ToLower() && r.Status != (int)DcsBaseDataStatus.作废).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(partsSalesCategoryName != null)
                throw new ValidationException(string.Format(ErrorStrings.PartsSalesCategory_Validation2, partsSalesCategory.Name));
            var userInfo = Utils.GetCurrentUserInfo();
            partsSalesCategory.ModifierId = userInfo.Id;
            partsSalesCategory.ModifierName = userInfo.Name;
            partsSalesCategory.ModifyTime = DateTime.Now;
        }
        public void InsertPartsSalesCategory(PartsSalesCategory partsSalesCategory) {
            InsertToDatabase(partsSalesCategory);
            this.InsertPartsSalesCategoryValidate(partsSalesCategory);
        }
        public void UpdatePartsSalesCategory(PartsSalesCategory partsSalesCategory) {
            UpdateToDatabase(partsSalesCategory);
            this.UpdatePartsSalesCategoryValidate(partsSalesCategory);
        }
        public PartsSalesCategory GetPartsSalesCategoryWithPartsSalesOrderTypesById(int id) {
            var result = ObjectContext.PartsSalesCategories.SingleOrDefault(entity => entity.Id == id && entity.Status == (int)DcsBaseDataStatus.有效);
            if(result != null) {
                var resultDetails = ObjectContext.PartsSalesOrderTypes.Where(entity => entity.PartsSalesCategoryId == id && entity.Status == (int)DcsBaseDataStatus.有效).ToArray();
            }
            return result;
        }
        public IQueryable<PartsSalesCategory> GetPartsSalesCategoryByBreachId(int breachId) {
            var result = ObjectContext.PartsSalesCategories.Where(entity => entity.BranchId == breachId && entity.Status == (int)DcsBaseDataStatus.有效);
            return result.OrderBy(r => r.Id);
        }
        public IQueryable<PartsSalesCategory> GetPartsSalesCategoryByPersonSalesCenterLink() {
            var userInfo = Utils.GetCurrentUserInfo();
            var result = ObjectContext.PartsSalesCategories.Where(r => ObjectContext.PersonSalesCenterLinks.Any(v => v.PersonId == userInfo.Id && v.Status == (int)DcsBaseDataStatus.有效 && v.PartsSalesCategoryId == r.Id));
            return result.OrderBy(r => r.Id);
        }
        /// <summary>
        /// 根据 经销商分公司管理信息 查询 配件销售类型
        /// </summary>
        /// <param name="branchId">营销分公司Id</param>
        /// <param name="dealerId">经销商Id</param>
        /// <returns>配件销售类型</returns>
        public IQueryable<PartsSalesCategory> GetPartsSalesCategoriesByDealerServiceInfo(int branchId, int dealerId) {
            return ObjectContext.PartsSalesCategories.Where(r => ObjectContext.DealerServiceInfoes.Any(v => v.BranchId == branchId && v.DealerId == dealerId && v.Status == (int)DcsMasterDataStatus.有效 && v.PartsSalesCategoryId == r.Id)).OrderBy(r => r.Id);
        }

        public IQueryable<PartsSalesCategory> GetPartsSalesCategoriesWithBranchId() {
            var userInfo = Utils.GetCurrentUserInfo();
            return ObjectContext.PartsSalesCategories.Where(r => r.BranchId == userInfo.EnterpriseId && r.Status == (int)DcsMasterDataStatus.有效).OrderBy(r => r.Id);
        }

        /// <summary>
        /// 根据 代理库与经销商隶属关系 查询 配件销售类型
        /// </summary>
        /// <param name="agencyId">代理库Id</param>
        /// <param name="dealerId">经销商Id</param>
        /// <returns>配件销售类型</returns>
        public IQueryable<PartsSalesCategory> GetPartsSalesCategoriesByAgencyDealerRelation(int agencyId, int dealerId) {
            return ObjectContext.PartsSalesCategories.Where(r => ObjectContext.AgencyDealerRelations.Any(v => v.AgencyId == agencyId && v.DealerId == dealerId && v.Status == (int)DcsMasterDataStatus.有效 && v.PartsSalesOrderTypeId == r.Id)).OrderBy(r => r.Id);
        }
        public IQueryable<PartsSalesCategory> GetPartsSalesCategoriesByDealerServiceInfoDealerId(int dealerId) {
            return ObjectContext.PartsSalesCategories.Where(r => ObjectContext.DealerServiceInfoes.Any(v => v.DealerId == dealerId && v.Status == (int)DcsMasterDataStatus.有效 && v.PartsSalesCategoryId == r.Id)).OrderBy(r => r.Id);
        }
        public IQueryable<PartsSalesCategory> GetPartsSalesCategoriesBySalesUnitOwnerCompanyId(int ownerCompanyId) {
            return ObjectContext.PartsSalesCategories.Where(r => ObjectContext.SalesUnits.Any(v => v.OwnerCompanyId == ownerCompanyId && v.Status == (int)DcsMasterDataStatus.有效 && v.PartsSalesCategoryId == r.Id)).OrderBy(r => r.Id);
        }
        public IQueryable<PartsSalesCategory> GetPartsSalesCategoriesByDealerServiceInfoOrAgencyAffiBranchList(List<int> prtsSalesCategoryIds) {
            return ObjectContext.PartsSalesCategories.Where(r => prtsSalesCategoryIds.Contains(r.Id) && r.Status == (int)DcsBaseDataStatus.有效);
        }
        public IQueryable<PartsSalesCategory> GetPartsSalesCategoriesByDealerServiceInfoOrAgencyAffiBranchEpc(List<int> prtsSalesCategoryIds) {
            return ObjectContext.PartsSalesCategories.Where(r => /*ObjectContext.PMSEPCBRANDMAPPINGs.Any(v => v.PMSBrandId == r.Id && v.Syscode == (int)DcsSysCode.EPC && v.Status == (int)DcsBaseDataStatus.有效) && prtsSalesCategoryIds.Contains(r.Id) &&*/ r.Status == (int)DcsBaseDataStatus.有效);
        }
        public IQueryable<PartsSalesCategory> GetPartsSalesCategoriesByEpcSysCode() {
            return ObjectContext.PartsSalesCategories.Where(r => /*ObjectContext.PMSEPCBRANDMAPPINGs.Any(v => v.PMSBrandId == r.Id && v.Syscode == (int)DcsSysCode.EPC && v.Status == (int)DcsBaseDataStatus.有效) && */r.Status == (int)DcsBaseDataStatus.有效);
        }
        public IQueryable<PartsSalesCategory> GetPartsSalesCategoriesByAgencyAffiBranchList(List<int> branchs) {
            return this.ObjectContext.PartsSalesCategories.Where(r => branchs.Contains(r.BranchId) && r.Status == (int)DcsBaseDataStatus.有效);
        }

        public bool IsIncludeWarrantyInfo(int enterPriseId) {
            var company = ObjectContext.Companies.SingleOrDefault(r => r.Status == (int)DcsBaseDataStatus.有效 && r.Id == enterPriseId);
            if(company == null)
                return false;
            if(company.Type == (int)DcsCompanyType.服务站兼代理库 || company.Type == (int)DcsCompanyType.代理库) {
                var partsSalesCategorys = ObjectContext.PartsSalesCategories.Where(r => ObjectContext.SalesUnits.Any(v => v.OwnerCompanyId == enterPriseId && v.Status == (int)DcsMasterDataStatus.有效 && v.PartsSalesCategoryId == r.Id) && r.Status == (int)DcsMasterDataStatus.有效 && r.IsNotWarranty);
                if(partsSalesCategorys.Any())
                    return true;
            }
            if(company.Type == (int)DcsCompanyType.服务站) {
                var partsSalesCategorys = ObjectContext.PartsSalesCategories.Where(r => ObjectContext.DealerServiceInfoes.Any(v => v.DealerId == enterPriseId && v.Status == (int)DcsMasterDataStatus.有效 && v.PartsSalesCategoryId == r.Id) && r.Status == (int)DcsMasterDataStatus.有效 && r.IsNotWarranty);
                if(partsSalesCategorys.Any())
                    return true;
            }
            return false;
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               

        public void InsertPartsSalesCategory(PartsSalesCategory partsSalesCategory) {
            new PartsSalesCategoryAch(this).InsertPartsSalesCategory(partsSalesCategory);
        }

        public void UpdatePartsSalesCategory(PartsSalesCategory partsSalesCategory) {
            new PartsSalesCategoryAch(this).UpdatePartsSalesCategory(partsSalesCategory);
        }

        public PartsSalesCategory GetPartsSalesCategoryWithPartsSalesOrderTypesById(int id) {
            return new PartsSalesCategoryAch(this).GetPartsSalesCategoryWithPartsSalesOrderTypesById(id);
        }

        public IQueryable<PartsSalesCategory> GetPartsSalesCategoryByBreachId(int breachId) {
            return new PartsSalesCategoryAch(this).GetPartsSalesCategoryByBreachId(breachId);
        }

        public IQueryable<PartsSalesCategory> GetPartsSalesCategoryByPersonSalesCenterLink() {
            return new PartsSalesCategoryAch(this).GetPartsSalesCategoryByPersonSalesCenterLink();
        }
        /// <summary>
        /// 根据 经销商分公司管理信息 查询 配件销售类型
        /// </summary>
        /// <param name="branchId">营销分公司Id</param>
        /// <param name="dealerId">经销商Id</param>
        /// <returns>配件销售类型</returns>
        public IQueryable<PartsSalesCategory> GetPartsSalesCategoriesByDealerServiceInfo(int branchId, int dealerId) {
            return new PartsSalesCategoryAch(this).GetPartsSalesCategoriesByDealerServiceInfo(branchId,dealerId);
        }
        /// <summary>
        /// 根据 代理库与经销商隶属关系 查询 配件销售类型
        /// </summary>
        /// <param name="agencyId">代理库Id</param>
        /// <param name="dealerId">经销商Id</param>
        /// <returns>配件销售类型</returns>
        public IQueryable<PartsSalesCategory> GetPartsSalesCategoriesByAgencyDealerRelation(int agencyId, int dealerId) {
            return new PartsSalesCategoryAch(this).GetPartsSalesCategoriesByAgencyDealerRelation(agencyId,dealerId);
        }

        public IQueryable<PartsSalesCategory> GetPartsSalesCategoriesByDealerServiceInfoDealerId(int dealerId) {
            return new PartsSalesCategoryAch(this).GetPartsSalesCategoriesByDealerServiceInfoDealerId(dealerId);
        }

        public IQueryable<PartsSalesCategory> GetPartsSalesCategoriesBySalesUnitOwnerCompanyId(int ownerCompanyId) {
            return new PartsSalesCategoryAch(this).GetPartsSalesCategoriesBySalesUnitOwnerCompanyId(ownerCompanyId);
        }

        public IQueryable<PartsSalesCategory> GetPartsSalesCategoriesByDealerServiceInfoOrAgencyAffiBranchList(List<int> prtsSalesCategoryIds) {
            return new PartsSalesCategoryAch(this).GetPartsSalesCategoriesByDealerServiceInfoOrAgencyAffiBranchList(prtsSalesCategoryIds);
        }
                public IQueryable<PartsSalesCategory> GetPartsSalesCategoriesByDealerServiceInfoOrAgencyAffiBranchEpc(List<int> prtsSalesCategoryIds) {
            return new PartsSalesCategoryAch(this).GetPartsSalesCategoriesByDealerServiceInfoOrAgencyAffiBranchEpc(prtsSalesCategoryIds);
        }

        public IQueryable<PartsSalesCategory> GetPartsSalesCategoriesByEpcSysCode() {
            return new PartsSalesCategoryAch(this).GetPartsSalesCategoriesByEpcSysCode();
        }

        public IQueryable<PartsSalesCategory> GetPartsSalesCategoriesByAgencyAffiBranchList(List<int> branchs) {
            return new PartsSalesCategoryAch(this).GetPartsSalesCategoriesByAgencyAffiBranchList(branchs);
        }

        public IQueryable<PartsSalesCategory> GetPartsSalesCategoriesWithBranchId() {
            return new PartsSalesCategoryAch(this).GetPartsSalesCategoriesWithBranchId();
        }

        [Invoke]
        public bool IsIncludeWarrantyInfo(int enterPriseId) {
            return new PartsSalesCategoryAch(this).IsIncludeWarrantyInfo(enterPriseId);
        }
    }
}
