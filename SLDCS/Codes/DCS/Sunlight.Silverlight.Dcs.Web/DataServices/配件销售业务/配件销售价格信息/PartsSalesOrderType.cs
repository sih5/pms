﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsSalesOrderTypeAch : DcsSerivceAchieveBase {
        internal void InsertPartsSalesOrderTypeValidate(PartsSalesOrderType partsSalesOrderType) {
            var dbpartsSalesOrderTypeCode = ObjectContext.PartsSalesOrderTypes.Where(r => r.PartsSalesCategoryId == partsSalesOrderType.PartsSalesCategoryId && r.Code.ToLower() == partsSalesOrderType.Code.ToLower() && r.Status != (int)DcsBaseDataStatus.作废).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbpartsSalesOrderTypeCode != null)
                throw new ValidationException(string.Format(ErrorStrings.PartsSalesOrderType_Validation1, partsSalesOrderType.Code));
            var dbpartsSalesOrderTypeName = ObjectContext.PartsSalesOrderTypes.Where(r => r.PartsSalesCategoryId == partsSalesOrderType.PartsSalesCategoryId && r.Name.ToLower() == partsSalesOrderType.Name.ToLower() && r.Status != (int)DcsBaseDataStatus.作废).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbpartsSalesOrderTypeName != null)
                throw new ValidationException(string.Format(ErrorStrings.PartsSalesOrderType_Validation2, partsSalesOrderType.Name));
            var userInfo = Utils.GetCurrentUserInfo();
            partsSalesOrderType.CreatorId = userInfo.Id;
            partsSalesOrderType.CreatorName = userInfo.Name;
            partsSalesOrderType.CreateTime = DateTime.Now;
        }

        internal void UpdatePartsSalesOrderTypeValidate(PartsSalesOrderType partsSalesOrderType) {
            var dbpartsSalesOrderTypeCode = ObjectContext.PartsSalesOrderTypes.Where(r => r.Id != partsSalesOrderType.Id && r.PartsSalesCategoryId == partsSalesOrderType.PartsSalesCategoryId && r.Code.ToLower() == partsSalesOrderType.Code.ToLower() && r.Status != (int)DcsBaseDataStatus.作废).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbpartsSalesOrderTypeCode != null)
                throw new ValidationException(string.Format(ErrorStrings.PartsSalesOrderType_Validation1, partsSalesOrderType.Code));
            var dbpartsSalesOrderTypeName = ObjectContext.PartsSalesOrderTypes.Where(r => r.Id != partsSalesOrderType.Id && r.PartsSalesCategoryId == partsSalesOrderType.PartsSalesCategoryId && r.Name.ToLower() == partsSalesOrderType.Name.ToLower() && r.Status != (int)DcsBaseDataStatus.作废).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbpartsSalesOrderTypeName != null)
                throw new ValidationException(string.Format(ErrorStrings.PartsSalesOrderType_Validation2, partsSalesOrderType.Name));
            var userInfo = Utils.GetCurrentUserInfo();
            partsSalesOrderType.ModifierId = userInfo.Id;
            partsSalesOrderType.ModifierName = userInfo.Name;
            partsSalesOrderType.ModifyTime = DateTime.Now;
        }

        public void InsertPartsSalesOrderType(PartsSalesOrderType partsSalesOrderType) {
            InsertToDatabase(partsSalesOrderType);
            this.InsertPartsSalesOrderTypeValidate(partsSalesOrderType);
        }

        public void UpdatePartsSalesOrderType(PartsSalesOrderType partsSalesOrderType) {
            UpdateToDatabase(partsSalesOrderType);
            this.UpdatePartsSalesOrderTypeValidate(partsSalesOrderType);
        }

        public IQueryable<PartsSalesOrderType> GetPartsSalesOrderTypeByBranchId(int branchId) {
            var dbPartsSalesCategories = ObjectContext.PartsSalesCategories.Where(e => e.BranchId == branchId && e.Status == (int)DcsBaseDataStatus.有效);
            if(dbPartsSalesCategories.Any()) {
                var categoriesIds = dbPartsSalesCategories.Select(e => e.Id).ToArray();
                return ObjectContext.PartsSalesOrderTypes.Where(e => categoriesIds.Contains(e.PartsSalesCategoryId) && e.Status == (int)DcsBaseDataStatus.有效).OrderBy(r => r.Id);
            }
            return null;
        }

        public IQueryable<PartsSalesOrderType> GetPartsSalesOrderTypeBy() {
            var dbPartsSalesCategories = ObjectContext.PartsSalesCategories.Where(e => e.Status == (int)DcsBaseDataStatus.有效);
            if(dbPartsSalesCategories.Any()) {
                var categoriesIds = dbPartsSalesCategories.Select(e => e.Id).ToArray();
                return ObjectContext.PartsSalesOrderTypes.Where(e => categoriesIds.Contains(e.PartsSalesCategoryId) && e.Status == (int)DcsBaseDataStatus.有效).OrderBy(r => r.Id);
            }
            return null;
        }

        public IQueryable<PartsSalesOrderType> GetPartsSalesOrderTypeByPartsSalesCategoryId(int partsSalesCategoryId) {
            return ObjectContext.PartsSalesOrderTypes.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId).OrderBy(r => r.Id);
        }

        public IQueryable<PartsSalesOrderType> GetPartsSalesOrderTypeWithPartsSalesCategoriesByParameters(string categoryCode, string categoryName) {
            IQueryable<PartsSalesCategory> dbPartsSalesCategories = ObjectContext.PartsSalesCategories;
            if(!string.IsNullOrWhiteSpace(categoryCode))
                dbPartsSalesCategories = dbPartsSalesCategories.Where(e => e.Code.ToLower().Contains(categoryCode.ToLower()));
            if(!string.IsNullOrWhiteSpace(categoryName))
                dbPartsSalesCategories = dbPartsSalesCategories.Where(e => e.Name.ToLower().Contains(categoryName.ToLower()));
            var categoriesResult = dbPartsSalesCategories.ToArray();
            if(categoriesResult.Any()) {
                var categoryIds = categoriesResult.Select(e => e.Id);
                return ObjectContext.PartsSalesOrderTypes.Where(e => categoryIds.Contains(e.PartsSalesCategoryId)).OrderBy(e => e.Id).OrderBy(r => r.Id);
            }
            return null;
        }

        public IQueryable<PartsSalesOrderType> GetPartsSalesOrderTypeWithPartsSalesCategories() {
            return ObjectContext.PartsSalesOrderTypes.Include("PartsSalesCategory").Include("Branch").OrderBy(e => e.Id);
        }


        public IQueryable<VirtualSalesOrderType> GetVirtualSalesOrderType() {
            var userInfo = Utils.GetCurrentUserInfo();
            var salesOrderTypes = from a in ObjectContext.PartsSalesOrderTypes.Where(r => r.BranchId == userInfo.EnterpriseId && r.Status == (int)DcsBaseDataStatus.有效)
                                  select new VirtualSalesOrderType {
                                      Id = a.Id,
                                      Code = a.Code,
                                      Name = a.Name,
                                      PartsSalesCategoryId = a.PartsSalesCategoryId,

                                  };
            var purchaseOrderTypes = from b in ObjectContext.PartsPurchaseOrderTypes.Where(r => r.BranchId == userInfo.EnterpriseId && r.Status == (int)DcsBaseDataStatus.有效)
                                     select new VirtualSalesOrderType {
                                         Id = b.Id,
                                         Code = b.Code,
                                         Name = b.Name,
                                         PartsSalesCategoryId = b.PartsSalesCategoryId,

                                     };
            return salesOrderTypes.Union(purchaseOrderTypes);

        }

        //查询订单类型,要求实现：未维护价格等级的订单类型不显示。
        public IQueryable<PartsSalesOrderType> GetPartsSalesOrderTypeWithCustomerOrderPriceGrades(int partsSalesCategoryId, int submitCompanyId) {
            var result = from a in this.ObjectContext.PartsSalesOrderTypes.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效)
                         join b in this.ObjectContext.CustomerOrderPriceGrades.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.CustomerCompanyId == submitCompanyId) on a.Id equals b.PartsSalesOrderTypeId
                         select new {
                             a
                         };
            return result.Select(r => r.a).OrderBy(e => e.Id);
        }

        public IQueryable<PartsSalesOrderType> GetPartsSalesOrderTypeWithSettleTypes(int settleType) {
            var result = this.ObjectContext.PartsSalesOrderTypes.Where(r => r.SettleType == settleType && r.Status == (int)DcsBaseDataStatus.有效);
            return result.OrderBy(e => e.Id);
        }
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               

        public void InsertPartsSalesOrderType(PartsSalesOrderType partsSalesOrderType) {
            new PartsSalesOrderTypeAch(this).InsertPartsSalesOrderType(partsSalesOrderType);
        }

        public void UpdatePartsSalesOrderType(PartsSalesOrderType partsSalesOrderType) {
            new PartsSalesOrderTypeAch(this).UpdatePartsSalesOrderType(partsSalesOrderType);
        }

        public IQueryable<PartsSalesOrderType> GetPartsSalesOrderTypeByBranchId(int branchId) {
            return new PartsSalesOrderTypeAch(this).GetPartsSalesOrderTypeByBranchId(branchId);
        }

        public IQueryable<PartsSalesOrderType> GetPartsSalesOrderTypeBy() {
            return new PartsSalesOrderTypeAch(this).GetPartsSalesOrderTypeBy();
        }

        public IQueryable<PartsSalesOrderType> GetPartsSalesOrderTypeByPartsSalesCategoryId(int partsSalesCategoryId) {
            return new PartsSalesOrderTypeAch(this).GetPartsSalesOrderTypeByPartsSalesCategoryId(partsSalesCategoryId);
        }

        public IQueryable<PartsSalesOrderType> GetPartsSalesOrderTypeWithPartsSalesCategoriesByParameters(string categoryCode, string categoryName) {
            return new PartsSalesOrderTypeAch(this).GetPartsSalesOrderTypeWithPartsSalesCategoriesByParameters(categoryCode, categoryName);
        }

        public IQueryable<PartsSalesOrderType> GetPartsSalesOrderTypeWithPartsSalesCategories() {
            return new PartsSalesOrderTypeAch(this).GetPartsSalesOrderTypeWithPartsSalesCategories();
        }

        public IQueryable<VirtualSalesOrderType> GetVirtualSalesOrderType() {
            return new PartsSalesOrderTypeAch(this).GetVirtualSalesOrderType();
        }

        public IQueryable<PartsSalesOrderType> GetPartsSalesOrderTypeWithCustomerOrderPriceGrades(int partsSalesCategoryId, int submitCompanyId) {
            return new PartsSalesOrderTypeAch(this).GetPartsSalesOrderTypeWithCustomerOrderPriceGrades(partsSalesCategoryId, submitCompanyId);
        }

        public IQueryable<PartsSalesOrderType> GetPartsSalesOrderTypeWithSettleTypes(int settleType) {
            return new PartsSalesOrderTypeAch(this).GetPartsSalesOrderTypeWithSettleTypes(settleType);
        }
    }
}
