﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Microsoft.Data.Extensions;
using NPOI.HSSF.Record.Chart;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
using System.Collections.Generic;
using System.ServiceModel.DomainServices.Server;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsSalesPriceAch : DcsSerivceAchieveBase {
        internal void InsertPartsSalesPriceValidate(PartsSalesPrice partsSalesPrice) {
            var dbpartsSalesPrice = ObjectContext.PartsSalesPrices.Where(r => r.PartsSalesCategoryId == partsSalesPrice.PartsSalesCategoryId && r.SparePartId == partsSalesPrice.SparePartId && r.Status != (int)DcsBaseDataStatus.作废).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbpartsSalesPrice != null)
                throw new ValidationException(string.Format(ErrorStrings.PartsSalesPrice_Validation1, partsSalesPrice.SparePartName, partsSalesPrice.PartsSalesCategoryName));
            var userInfo = Utils.GetCurrentUserInfo();
            partsSalesPrice.CreatorId = userInfo.Id;
            partsSalesPrice.CreatorName = userInfo.Name;
            partsSalesPrice.CreateTime = DateTime.Now;
        }

        internal void UpdatePartsSalesPriceValidate(PartsSalesPrice partsSalesPrice) {
            var partsSalespriceLegitimacy = ObjectContext.PartsSalesPrices.Where(r => r.Id == partsSalesPrice.Id && r.Status == (int)DcsBaseDataStatus.作废).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(partsSalespriceLegitimacy != null)
                throw new ValidationException(string.Format(ErrorStrings.PartsSalesPrice_Validation2));
            var dbpartsSalesPriceGroup = ObjectContext.PartsSalesPrices.Where(r => r.Id != partsSalesPrice.Id && r.PartsSalesCategoryId == partsSalesPrice.PartsSalesCategoryId && r.SparePartId == partsSalesPrice.SparePartId && r.Status != (int)DcsBaseDataStatus.作废).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbpartsSalesPriceGroup != null)
                throw new ValidationException(string.Format(ErrorStrings.PartsSalesPrice_Validation1, partsSalesPrice.SparePartName, partsSalesPrice.PartsSalesCategoryName));
            var userInfo = Utils.GetCurrentUserInfo();
            partsSalesPrice.ModifierId = userInfo.Id;
            partsSalesPrice.ModifierName = userInfo.Name;
            partsSalesPrice.ModifyTime = DateTime.Now;
        }

        public void InsertPartsSalesPrice(PartsSalesPrice partsSalesPrice) {
            InsertToDatabase(partsSalesPrice);
            this.InsertPartsSalesPriceValidate(partsSalesPrice);
        }

        public void UpdatePartsSalesPrice(PartsSalesPrice partsSalesPrice) {
            UpdateToDatabase(partsSalesPrice);
            this.UpdatePartsSalesPriceValidate(partsSalesPrice);
        }

        public IQueryable<PartsSalesPrice> GetPartsSalesPriceWithHistory() {
            return ObjectContext.PartsSalesPrices.Include("PartsSalesPriceHistory");
        }

        public IQueryable<PartsSalesPrice> 主查询配件销售价(bool? isSale, bool? isOrderable, int? PartsSalesCategoryId,int? priceType,string sparePartCodes) {
            var salesPrices = ObjectContext.PartsSalesPrices.Where(r => r.Status != (int)DcsBaseDataStatus.作废);
            var branches = ObjectContext.PartsBranches.Where(r => r.Status != (int)DcsBaseDataStatus.作废);
            if(PartsSalesCategoryId.HasValue)
                salesPrices = salesPrices.Where(r => r.PartsSalesCategoryId == PartsSalesCategoryId.Value);
            if (isOrderable.HasValue)
                branches = branches.Where(v => v.IsOrderable == isOrderable.Value);
            if (isSale.HasValue)
                branches = branches.Where(v => v.IsSalable == isSale.Value);

            var result = from a in salesPrices
                         join b in branches on new {
                             a.SparePartId,
                             a.PartsSalesCategoryId,
                         } equals new {
                             SparePartId = b.PartId,
                             b.PartsSalesCategoryId
                         } into temp1
                         from t1 in temp1.DefaultIfEmpty()
                         select a;
            if(!string.IsNullOrEmpty(sparePartCodes)) {
                var spareparts = sparePartCodes.Split(',');
                if(spareparts.Length == 1) {
                    var sparepartcode = spareparts[0];
                    result = result.Where(r => r.SparePartCode.Contains(sparepartcode));
                } else {
                    result = result.Where(r => spareparts.Contains(r.SparePartCode));
                }
            }
            var partids = result.Select(r => r.SparePartId);
            var partsSalesCategoryIds = result.Select(r => r.PartsSalesCategoryId).Distinct();
            var priceTpyes = (from a in ObjectContext.PartsBranches.Where(r => r.Status != (int)DcsBaseDataStatus.作废 && partids.Contains(r.PartId) && partsSalesCategoryIds.Contains(r.PartsSalesCategoryId))
                             join b in ObjectContext.PartsSalePriceIncreaseRates.Where(r => r.status != (int)DcsBaseDataStatus.作废) on a.IncreaseRateGroupId equals b.Id into temp2
                             from t2 in temp2.DefaultIfEmpty()
                             select new {
                                 a.PartId,
                                 a.PartsSalesCategoryId,
                                 t2.Id,
                                 t2.GroupCode
                             }).ToArray();

            foreach (var res in result) { 
                var priceTpye = priceTpyes.Where(r => r.PartId == res.SparePartId && r.PartsSalesCategoryId == res.PartsSalesCategoryId).FirstOrDefault();
                if (priceTpye != null)
                    res.PriceType = priceTpye.Id;
            }
            if (priceType.HasValue)
                result = result.Where(r => ObjectContext.PartsBranches.Any(v => r.PartsSalesCategoryId == v.PartsSalesCategoryId && r.SparePartId == v.PartId && v.IncreaseRateGroupId == priceType));
           
            
            return result.Include("SparePart.PartsRetailGuidePrices").OrderBy(r => r.Id);
        }

        public IQueryable<PartsSalesPrice> 查询配件销售价(bool? isSale, bool? isOrderable, int? PartsSalesCategoryId) {
            var result = ObjectContext.PartsSalesPrices.Where(r => r.Status != (int)DcsBaseDataStatus.作废);
            if(PartsSalesCategoryId.HasValue)
                result = result.Where(r => r.PartsSalesCategoryId == PartsSalesCategoryId.Value);
            if(isSale.HasValue)
                result = result.Where(r => ObjectContext.PartsBranches.Any(v => r.PartsSalesCategoryId == v.PartsSalesCategoryId && r.SparePartId == v.PartId && v.IsSalable == isSale.Value));
            if(isOrderable.HasValue)
                result = result.Where(r => ObjectContext.PartsBranches.Any(v => r.PartsSalesCategoryId == v.PartsSalesCategoryId && r.SparePartId == v.PartId && v.IsOrderable == isOrderable.Value));
            return result.Include("SparePart.PartsRetailGuidePrices").OrderBy(r => r.Id);
        }

        public IQueryable<VirtualPartsSalesPrice> 查询配件销售价是否倒挂(bool? isSale, bool? isOrderable, int? PartsSalesCategoryId) {
            var result = ObjectContext.PartsSalesPrices.Where(r => r.Status != (int)DcsBaseDataStatus.作废);
            if(PartsSalesCategoryId.HasValue)
                result = result.Where(r => r.PartsSalesCategoryId == PartsSalesCategoryId.Value);
            if(isSale.HasValue)
                result = result.Where(r => ObjectContext.PartsBranches.Any(v => r.PartsSalesCategoryId == v.PartsSalesCategoryId && r.SparePartId == v.PartId && v.IsSalable == isSale.Value));
            if(isOrderable.HasValue)
                result = result.Where(r => ObjectContext.PartsBranches.Any(v => r.PartsSalesCategoryId == v.PartsSalesCategoryId && r.SparePartId == v.PartId && v.IsOrderable == isOrderable.Value));
            var partsPurchasePricings = (from a in ObjectContext.PartsPurchasePricings.Where(r => r.PurchasePrice == (int)DcsPurchasePriceType.正式价格 && r.Status == (int)DcsPartsPurchasePricingStatus.生效)
                                         join b in result on new {
                                             a.PartId,
                                             a.PartsSalesCategoryId
                                         } equals new {
                                             PartId = b.SparePartId,
                                             b.PartsSalesCategoryId
                                         }
                                         select new {
                                             a.PartId,
                                             a.PartsSalesCategoryId,
                                             a.PurchasePrice
                                         }).GroupBy(r => new {
                                             r.PartId,
                                             r.PartsSalesCategoryId
                                         }).Select(r => new {
                                             r.Key.PartsSalesCategoryId,
                                             r.Key.PartId,
                                             PurchasePrice = r.Min(x => x.PurchasePrice)
                                         });
            var partsExchanges = from a in ObjectContext.PartsExchanges
                                 join d in result on a.PartId equals d.SparePartId
                                 join b in ObjectContext.PartsExchanges on a.ExchangeCode equals b.ExchangeCode
                                 select new {
                                     a.PartId,
                                     SparePartId = b.PartId,
                                 };
            var salesPrice = (from a in ObjectContext.PartsSalesPrices.Where(r => r.Status == (int)DcsBaseDataStatus.有效)
                              join b in partsExchanges on a.SparePartId equals b.SparePartId into temp
                              from c in temp.DefaultIfEmpty()
                              select new {
                                  a.SalesPrice,
                                  c.PartId
                              }).GroupBy(r => new {
                                  r.PartId
                              }).Select(r => new {
                                  r.Key.PartId,
                                  SalesPrice = r.Max(x => x.SalesPrice)
                              });
            var rlt = from a in result
                      join f in ObjectContext.SpareParts on a.SparePartId equals f.Id
                      join b in partsPurchasePricings on a.SparePartId equals b.PartId into temp
                      from c in temp.DefaultIfEmpty()
                      join d in salesPrice on a.SparePartId equals d.PartId into temp1
                      from e in temp1.DefaultIfEmpty()
                      select new VirtualPartsSalesPrice {
                          Id = a.Id,
                          PartsSalesCategoryName = a.PartsSalesCategoryName,
                          SparePartId = a.SparePartId,
                          PartsSalesCategoryId = a.PartsSalesCategoryId,
                          SparePartCode = f.Code,
                          SparePartName = f.Name,
                          SalesPrice = a.SalesPrice,
                          PriceType = a.PriceType,
                          Status = a.Status,
                          BranchId = a.BranchId,
                          Remark = a.Remark,
                          CreatorName = a.CreatorName,
                          CreateTime = a.CreateTime,
                          ModifierName = a.ModifierName,
                          ModifyTime = a.ModifyTime,
                          ApproverName = a.ApproverName,
                          ApproveTime = a.ApproveTime,
                          AbandonerName = a.AbandonerName,
                          AbandonTime = a.AbandonTime,
                          MaxExchangeSalePrice = e.SalesPrice,
                          PurchasePrice = c.PurchasePrice
                      };

            return rlt.OrderBy(r => r.Id);
        }

        //public List<VehicleSparePart> 查询配件是否倒挂以及互换件最高价格(List<int> P = artsSalesPriceIds) {
        //    var purchasePricings = ().ToList();
        //    var sparePartIds = vehicleSpareParts.Select(r => r.SparePartId).ToArray();
        //    var partsExchanges = from a in ObjectContext.PartsExchanges.Where(r => sparePartIds.Contains(r.PartId))
        //                         join b in ObjectContext.PartsExchanges on a.ExchangeCode equals b.ExchangeCode
        //                         select new {
        //                             a.PartId,
        //                             SparePartId = b.PartId,
        //                         };
        //    var salesPrice = (from a in ObjectContext.PartsSalesPrices.Where(r => r.Status == (int)DcsBaseDataStatus.有效)
        //                      join b in partsExchanges on a.SparePartId equals b.SparePartId into temp
        //                      from c in temp.DefaultIfEmpty()
        //                      select new {
        //                          a.SalesPrice,
        //                          c.PartId
        //                      }).GroupBy(r => new {
        //                          r.PartId
        //                      }).Select(r => new {
        //                          r.Key.PartId,
        //                          SalesPrice = r.Max(x => x.SalesPrice)
        //                      }).ToList();
        //    foreach(var vehicleSparePart in vehicleSpareParts) {
        //        var purchasePricing = purchasePricings.FirstOrDefault(r => r.PartsSalesCategoryId == vehicleSparePart.PartsSalesCategoryId && r.PartId == vehicleSparePart.SparePartId);
        //        if(purchasePricing != null) {
        //            vehicleSparePart.PurchasePrice = purchasePricing.PurchasePrice;
        //        }
        //        var sales = salesPrice.FirstOrDefault(r => r.PartId == vehicleSparePart.SparePartId);
        //        if(sales != null) {
        //            vehicleSparePart.MaxExchangeSalePrice = sales.SalesPrice;
        //        }
        //    }
        //    return vehicleSpareParts;
        //}

        public IQueryable<PartsSalesPrice> GetPartsSalesPriceBySparePartIds(int[] sparePartIds) {
            return ObjectContext.PartsSalesPrices.Where(r => r.Status != (int)DcsBaseDataStatus.作废 && r.ValidationTime <= DateTime.Now && r.ExpireTime >= DateTime.Now && sparePartIds.Contains(r.SparePartId));
        }
        public IQueryable<PartsSalesPriceWithRetailGuidePrice> 查询配件销售价和零售指导价(bool? isSale, bool? isOrderable, int? partsSalesCategoryId) {
            var partsSalesPrices = ObjectContext.PartsSalesPrices.Where(r => r.Status != (int)DcsBaseDataStatus.作废);
            var partsRetailGuidePrices = ObjectContext.PartsRetailGuidePrices.Where(r => r.Status != (int)DcsBaseDataStatus.作废);
            if(partsSalesCategoryId.HasValue) {
                partsSalesPrices = partsSalesPrices.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId.Value);
                partsRetailGuidePrices = partsRetailGuidePrices.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId.Value);
            }
            if(isSale.HasValue) {
                partsSalesPrices = partsSalesPrices.Where(r => ObjectContext.PartsBranches.Any(v => r.PartsSalesCategoryId == v.PartsSalesCategoryId && r.SparePartId == v.PartId && v.IsSalable == isSale.Value));
                partsRetailGuidePrices = partsRetailGuidePrices.Where(r => ObjectContext.PartsBranches.Any(v => r.PartsSalesCategoryId == v.PartsSalesCategoryId && r.SparePartId == v.PartId && v.IsSalable == isSale.Value));
            }
            if(isOrderable.HasValue) {
                partsSalesPrices = partsSalesPrices.Where(r => ObjectContext.PartsBranches.Any(v => r.PartsSalesCategoryId == v.PartsSalesCategoryId && r.SparePartId == v.PartId && v.IsOrderable == isOrderable.Value));
                partsRetailGuidePrices = partsRetailGuidePrices.Where(r => ObjectContext.PartsBranches.Any(v => r.PartsSalesCategoryId == v.PartsSalesCategoryId && r.SparePartId == v.PartId && v.IsOrderable == isOrderable.Value));
            }
            var result = from a in partsSalesPrices
                         join b in partsRetailGuidePrices on new {
                             a.PartsSalesCategoryId,
                             a.SparePartId
                         }
                         equals new {
                             b.PartsSalesCategoryId,
                             b.SparePartId
                         } into temp
                         from t in temp.DefaultIfEmpty()
                         select new PartsSalesPriceWithRetailGuidePrice {
                             Id = a.Id,
                             BranchId = a.BranchId,
                             PartsSalesCategoryId = a.PartsSalesCategoryId,
                             PartsSalesCategoryCode = a.PartsSalesCategoryCode,
                             PartsSalesCategoryName = a.PartsSalesCategoryName,
                             SparePartId = a.SparePartId,
                             SparePartCode = a.SparePartCode,
                             SparePartName = a.SparePartName,
                             IfClaim = a.IfClaim,
                             SalesPrice = a.SalesPrice,
                             PriceType = a.PriceType,
                             Status = a.Status,
                             ValidationTime = a.ValidationTime,
                             ExpireTime = a.ExpireTime,
                             Remark = a.Remark,
                             CreatorId = a.CreatorId,
                             CreatorName = a.CreatorName,
                             CreateTime = a.CreateTime,
                             ModifierId = a.ModifierId,
                             ModifierName = a.ModifierName,
                             ModifyTime = a.ModifyTime,
                             ApproverId = a.ApproverId,
                             ApproverName = a.ApproverName,
                             ApproveTime = a.ApproveTime,
                             AbandonerId = a.AbandonerId,
                             AbandonerName = a.AbandonerName,
                             AbandonTime = a.AbandonTime,
                             RetailGuidePrice = t.RetailGuidePrice
                         };
            return result.OrderBy(r => r.Id);
        }

        public IQueryable<PartsSalesPriceWithRetailGuidePrice> GetPartsSalesPriceRetailGuidePriceByBranchType(bool? isSale, bool? isOrderable) {
            var partsSalesPrices = ObjectContext.PartsSalesPrices.Where(r => r.Status == (int)DcsBaseDataStatus.有效);
            var partsRetailGuidePrices = ObjectContext.PartsRetailGuidePrices.Where(r => r.Status == (int)DcsBaseDataStatus.有效);

            if(isSale.HasValue) {
                partsSalesPrices = partsSalesPrices.Where(r => ObjectContext.PartsBranches.Any(v => r.PartsSalesCategoryId == v.PartsSalesCategoryId && r.SparePartId == v.PartId && v.IsSalable == isSale.Value));
                partsRetailGuidePrices = partsRetailGuidePrices.Where(r => ObjectContext.PartsBranches.Any(v => r.PartsSalesCategoryId == v.PartsSalesCategoryId && r.SparePartId == v.PartId && v.IsSalable == isSale.Value));
            }
            if(isOrderable.HasValue) {
                partsSalesPrices = partsSalesPrices.Where(r => ObjectContext.PartsBranches.Any(v => r.PartsSalesCategoryId == v.PartsSalesCategoryId && r.SparePartId == v.PartId && v.IsOrderable == isOrderable.Value));
                partsRetailGuidePrices = partsRetailGuidePrices.Where(r => ObjectContext.PartsBranches.Any(v => r.PartsSalesCategoryId == v.PartsSalesCategoryId && r.SparePartId == v.PartId && v.IsOrderable == isOrderable.Value));
            }

            var userInfo = Utils.GetCurrentUserInfo();
            var company = ObjectContext.Companies.SingleOrDefault(r => r.Status == (int)DcsBaseDataStatus.有效 && r.Id == userInfo.EnterpriseId);
            if(company.Type == (int)DcsCompanyType.服务站) {
                //查询经销商分公司管理信息
                var dealerServiceInfos = ObjectContext.DealerServiceInfoes.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.DealerId == userInfo.EnterpriseId);
                if(dealerServiceInfos != null) {
                    var partsSalesCategoryIds = dealerServiceInfos.Select(r => r.PartsSalesCategoryId).ToArray();

                    var result = from a in partsSalesPrices.Where(r => partsSalesCategoryIds.Contains(r.PartsSalesCategoryId))
                                 join b in partsRetailGuidePrices on new {
                                     a.PartsSalesCategoryId,
                                     a.SparePartId
                                 } equals new {
                                     b.PartsSalesCategoryId,
                                     b.SparePartId
                                 } into temp
                                 from t in temp.DefaultIfEmpty()
                                 select new PartsSalesPriceWithRetailGuidePrice {
                                     Id = a.Id,
                                     BranchId = a.BranchId,
                                     PartsSalesCategoryId = a.PartsSalesCategoryId,
                                     PartsSalesCategoryCode = a.PartsSalesCategoryCode,
                                     PartsSalesCategoryName = a.PartsSalesCategoryName,
                                     SparePartId = a.SparePartId,
                                     SparePartCode = a.SparePartCode,
                                     SparePartName = a.SparePartName,
                                     IfClaim = a.IfClaim,
                                     SalesPrice = a.SalesPrice,
                                     PriceType = a.PriceType,
                                     Status = a.Status,
                                     ValidationTime = a.ValidationTime,
                                     ExpireTime = a.ExpireTime,
                                     Remark = a.Remark,
                                     CreatorId = a.CreatorId,
                                     CreatorName = a.CreatorName,
                                     CreateTime = a.CreateTime,
                                     ModifierId = a.ModifierId,
                                     ModifierName = a.ModifierName,
                                     ModifyTime = a.ModifyTime,
                                     ApproverId = a.ApproverId,
                                     ApproverName = a.ApproverName,
                                     ApproveTime = a.ApproveTime,
                                     AbandonerId = a.AbandonerId,
                                     AbandonerName = a.AbandonerName,
                                     AbandonTime = a.AbandonTime,
                                     RetailGuidePrice = t.RetailGuidePrice
                                 };
                    return result.OrderBy(r => r.Id);
                }

            }

            if(company.Type == (int)DcsCompanyType.服务站兼代理库 || company.Type == (int)DcsCompanyType.代理库) {
                //查询销售组织
                var salesUnits = ObjectContext.SalesUnits.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.OwnerCompanyId == userInfo.EnterpriseId);
                if(salesUnits != null) {
                    var partsSalesCategoryIds = salesUnits.Select(r => r.PartsSalesCategoryId).ToArray();

                    var result = from a in partsSalesPrices.Where(r => partsSalesCategoryIds.Contains(r.PartsSalesCategoryId))
                                 join b in partsRetailGuidePrices on new {
                                     a.PartsSalesCategoryId,
                                     a.SparePartId
                                 } equals new {
                                     b.PartsSalesCategoryId,
                                     b.SparePartId
                                 } into temp1
                                 from t1 in temp1.DefaultIfEmpty()
                                 select new PartsSalesPriceWithRetailGuidePrice {
                                     Id = a.Id,
                                     BranchId = a.BranchId,
                                     PartsSalesCategoryId = a.PartsSalesCategoryId,
                                     PartsSalesCategoryCode = a.PartsSalesCategoryCode,
                                     PartsSalesCategoryName = a.PartsSalesCategoryName,
                                     SparePartId = a.SparePartId,
                                     SparePartCode = a.SparePartCode,
                                     SparePartName = a.SparePartName,
                                     IfClaim = a.IfClaim,
                                     SalesPrice = a.SalesPrice,
                                     CenterPrice =a.CenterPrice,
                                     PriceType = a.PriceType,
                                     Status = a.Status,
                                     ValidationTime = a.ValidationTime,
                                     ExpireTime = a.ExpireTime,
                                     Remark = a.Remark,
                                     CreatorId = a.CreatorId,
                                     CreatorName = a.CreatorName,
                                     CreateTime = a.CreateTime,
                                     ModifierId = a.ModifierId,
                                     ModifierName = a.ModifierName,
                                     ModifyTime = a.ModifyTime,
                                     ApproverId = a.ApproverId,
                                     ApproverName = a.ApproverName,
                                     ApproveTime = a.ApproveTime,
                                     AbandonerId = a.AbandonerId,
                                     AbandonerName = a.AbandonerName,
                                     AbandonTime = a.AbandonTime,
                                     RetailGuidePrice = t1.RetailGuidePrice
                                 };
                    return result.OrderBy(r => r.Id);
                }

            }

            return null;
        }



    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               

        public void InsertPartsSalesPrice(PartsSalesPrice partsSalesPrice) {
            new PartsSalesPriceAch(this).InsertPartsSalesPrice(partsSalesPrice);
        }

        public void UpdatePartsSalesPrice(PartsSalesPrice partsSalesPrice) {
            new PartsSalesPriceAch(this).UpdatePartsSalesPrice(partsSalesPrice);
        }

        public IQueryable<PartsSalesPrice> GetPartsSalesPriceWithHistory() {
            return new PartsSalesPriceAch(this).GetPartsSalesPriceWithHistory();
        }
        [Query(HasSideEffects = true)] 
        public IQueryable<PartsSalesPrice> 主查询配件销售价(bool? isSale, bool? isOrderable, int? PartsSalesCategoryId,int? priceType,string sparePartCodes) {
            return new PartsSalesPriceAch(this).主查询配件销售价(isSale, isOrderable, PartsSalesCategoryId,priceType,sparePartCodes);
        }
        public IQueryable<PartsSalesPrice> 查询配件销售价(bool? isSale, bool? isOrderable, int? PartsSalesCategoryId) {
            return new PartsSalesPriceAch(this).查询配件销售价(isSale, isOrderable, PartsSalesCategoryId);
        }

        public IQueryable<PartsSalesPrice> GetPartsSalesPriceBySparePartIds(int[] sparePartIds) {
            return new PartsSalesPriceAch(this).GetPartsSalesPriceBySparePartIds(sparePartIds);
        }
        public IQueryable<PartsSalesPriceWithRetailGuidePrice> 查询配件销售价和零售指导价(bool? isSale, bool? isOrderable, int? partsSalesCategoryId) {
            return new PartsSalesPriceAch(this).查询配件销售价和零售指导价(isSale, isOrderable, partsSalesCategoryId);
        }

        public IQueryable<VirtualPartsSalesPrice> 查询配件销售价是否倒挂(bool? isSale, bool? isOrderable, int? PartsSalesCategoryId) {
            return new PartsSalesPriceAch(this).查询配件销售价是否倒挂(isSale, isOrderable, PartsSalesCategoryId);
        }

        public IQueryable<PartsSalesPriceWithRetailGuidePrice> GetPartsSalesPriceRetailGuidePriceByBranchType(bool? isSale, bool? isOrderable) {
            return new PartsSalesPriceAch(this).GetPartsSalesPriceRetailGuidePriceByBranchType(isSale, isOrderable);
        }
    }
}