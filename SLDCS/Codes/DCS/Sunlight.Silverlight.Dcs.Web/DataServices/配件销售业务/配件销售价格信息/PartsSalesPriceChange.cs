﻿using System;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsSalesPriceChangeAch : DcsSerivceAchieveBase {
        internal void InsertPartsSalesPriceChangeValidate(PartsSalesPriceChange partsSalesPriceChange) {
            if(string.IsNullOrWhiteSpace(partsSalesPriceChange.Code) || partsSalesPriceChange.Code == GlobalVar.ASSIGNED_BY_SERVER)
                partsSalesPriceChange.Code = CodeGenerator.Generate("PartsSalesPriceChange");
            var userInfo = Utils.GetCurrentUserInfo();
            partsSalesPriceChange.CreatorId = userInfo.Id;
            partsSalesPriceChange.CreatorName = userInfo.Name;
            partsSalesPriceChange.CreateTime = DateTime.Now;
        }

        internal void UpdatePartsSalesPriceChangeValidate(PartsSalesPriceChange partsSalesPriceChange) {
            var userInfo = Utils.GetCurrentUserInfo();
            partsSalesPriceChange.ModifierId = userInfo.Id;
            partsSalesPriceChange.ModifierName = userInfo.Name;
            partsSalesPriceChange.ModifyTime = DateTime.Now;
        }

        public void InsertPartsSalesPriceChange(PartsSalesPriceChange partsSalesPriceChange) {
            InsertToDatabase(partsSalesPriceChange);
            var partsSalesPriceChangeDetails = ChangeSet.GetAssociatedChanges(partsSalesPriceChange, v => v.PartsSalesPriceChangeDetails, ChangeOperation.Insert);
            foreach(PartsSalesPriceChangeDetail partsSalesPriceChangeDetail in partsSalesPriceChangeDetails) {
                InsertToDatabase(partsSalesPriceChangeDetail);
            }
            this.InsertPartsSalesPriceChangeValidate(partsSalesPriceChange);
        }

        public void UpdatePartsSalesPriceChange(PartsSalesPriceChange partsSalesPriceChange) {
            partsSalesPriceChange.PartsSalesPriceChangeDetails.Clear();
            UpdateToDatabase(partsSalesPriceChange);
            var partsSalesPriceChangeDetails = ChangeSet.GetAssociatedChanges(partsSalesPriceChange, v => v.PartsSalesPriceChangeDetails);
            foreach(PartsSalesPriceChangeDetail partsSalesPriceChangeDetail in partsSalesPriceChangeDetails) {
                //获取清单实体对象的操作类型
                switch(ChangeSet.GetChangeOperation(partsSalesPriceChangeDetail)) {
                    //新增操作
                    case ChangeOperation.Insert:
                        InsertToDatabase(partsSalesPriceChangeDetail);
                        break;
                    //更新操作
                    case ChangeOperation.Update:
                        UpdateToDatabase(partsSalesPriceChangeDetail);
                        break;
                    //删除操作
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(partsSalesPriceChangeDetail);
                        break;
                }
            }
            this.UpdatePartsSalesPriceChangeValidate(partsSalesPriceChange);
        }

        public PartsSalesPriceChange GetPartsSalesPriceChangeWithDetailsById(int id) {
            var dbPrtsSalesPriceChange = ObjectContext.PartsSalesPriceChanges.SingleOrDefault(e => e.Id == id);
            if(dbPrtsSalesPriceChange != null) {
                var dbDetails = ObjectContext.PartsSalesPriceChangeDetails.Where(ex => ex.ParentId == dbPrtsSalesPriceChange.Id).ToArray();
                var sparePartIds = dbDetails.Select(r => r.SparePartId).ToArray();
            }
            return dbPrtsSalesPriceChange;
        }

        public IQueryable<PartsSalesPriceChange> GetPartsSalesPriceChangeById(int id) {
            return ObjectContext.PartsSalesPriceChanges.Where(r => r.Id == id);
        }

        public IQueryable<PartsSalesPriceChange> GetPartsSalesPriceChangesForOrder(string sparePartCode, string sparePartName) {
            IQueryable<PartsSalesPriceChangeDetail> details = ObjectContext.PartsSalesPriceChangeDetails;
            if(!string.IsNullOrEmpty(sparePartCode))
                details = details.Where(r => r.SparePartCode.Contains(sparePartCode));
            if(!string.IsNullOrEmpty(sparePartName))
                details = details.Where(r => r.SparePartName.Contains(sparePartName));
            var result = from a in ObjectContext.PartsSalesPriceChanges
                         join b in details on a.Id equals b.ParentId
                         select a;
            foreach(var item in result) {
                item.IsUplodFile = item.Path != null && item.Path.Length > 0;
            }
            return result.Distinct().OrderByDescending(r => r.CreateTime);
        
        }
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               

        public void InsertPartsSalesPriceChange(PartsSalesPriceChange partsSalesPriceChange) {
            new PartsSalesPriceChangeAch(this).InsertPartsSalesPriceChange(partsSalesPriceChange);
        }

        public void UpdatePartsSalesPriceChange(PartsSalesPriceChange partsSalesPriceChange) {
            new PartsSalesPriceChangeAch(this).UpdatePartsSalesPriceChange(partsSalesPriceChange);
        }

        public PartsSalesPriceChange GetPartsSalesPriceChangeWithDetailsById(int id) {
            return new PartsSalesPriceChangeAch(this).GetPartsSalesPriceChangeWithDetailsById(id);
        }

        public IQueryable<PartsSalesPriceChange> GetPartsSalesPriceChangeById(int id) {
            return new PartsSalesPriceChangeAch(this).GetPartsSalesPriceChangeById(id);
        }

        public IQueryable<PartsSalesPriceChange> GetPartsSalesPriceChangesForOrder(string sparePartCode, string sparePartName) {
            return new PartsSalesPriceChangeAch(this).GetPartsSalesPriceChangesForOrder(sparePartCode, sparePartName);
        }
    }
}
