﻿using System;
using System.Linq;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsSalesPriceChangeDetailAch : DcsSerivceAchieveBase {
        public PartsSalesPriceChangeDetailAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public IQueryable<PartsSalesPriceChangeDetail> 查询配件销售价变更申请清单附带加价率(int parentId) {
            //var nowTime = DateTime.Now;
            var partsSalesPriceChangeDetails = ObjectContext.PartsSalesPriceChangeDetails.Where(r => r.ParentId == parentId);
            /*数据库存在的字段，新增修改默认赋值保存到数据库，不需要查询虚拟字段计算
            var partsSalesPriceChange = ObjectContext.PartsSalesPriceChanges.SingleOrDefault(r => r.Id == parentId);

            var partsSalesPriceChangeDetailSparePartIds = partsSalesPriceChangeDetails.Select(r => r.SparePartId).ToArray();
            
            var referencePrices = ObjectContext.PartsSalesPrices.Where(r => partsSalesPriceChangeDetailSparePartIds.Contains(r.SparePartId) && r.Status == (int)DcsBaseDataStatus.有效 && r.PartsSalesCategoryId == partsSalesPriceChange.PartsSalesCategoryId).ToArray();
            var partsPurchasePricings = ObjectContext.PartsPurchasePricings.Where(r => partsSalesPriceChangeDetailSparePartIds.Contains(r.PartId) && r.Status == (int)DcsPartsPurchasePricingStatus.生效 && r.PartsSalesCategoryId == partsSalesPriceChange.PartsSalesCategoryId && r.ValidFrom <= nowTime && r.ValidTo >= nowTime).ToArray();
                        
            foreach(var partsSalesPriceChangeDetail in partsSalesPriceChangeDetails) {
                var referencePrice = referencePrices.FirstOrDefault(r => r.SparePartId == partsSalesPriceChangeDetail.SparePartId);
                var partsPurchasePricing = partsPurchasePricings.FirstOrDefault(r => r.PartId == partsSalesPriceChangeDetail.SparePartId);

                partsSalesPriceChangeDetail.ReferencePrice = referencePrice != null ? referencePrice.SalesPrice : 0;
                partsSalesPriceChangeDetail.PurchasePrice = partsPurchasePricing != null ? partsPurchasePricing.PurchasePrice : 0;
                partsSalesPriceChangeDetail.IncreaseRateSale = (partsPurchasePricing == null || partsPurchasePricing.PurchasePrice == 0 || partsSalesPriceChangeDetail.SalesPrice == 0) ? 0 : partsSalesPriceChangeDetail.SalesPrice / partsSalesPriceChangeDetail.PurchasePrice - 1;
                partsSalesPriceChangeDetail.IncreaseRateRetail = (partsSalesPriceChangeDetail.SalesPrice == 0 || partsSalesPriceChangeDetail.RetailGuidePrice == null) ? 0 : partsSalesPriceChangeDetail.RetailGuidePrice.Value / partsSalesPriceChangeDetail.SalesPrice - 1;
                partsSalesPriceChangeDetail.IncreaseRateDealerSalesPrice = (partsSalesPriceChangeDetail.SalesPrice == 0 || partsSalesPriceChangeDetail.DealerSalesPrice == null || partsSalesPriceChangeDetail.DealerSalesPrice == 0) ? 0 : partsSalesPriceChangeDetail.DealerSalesPrice.Value / partsSalesPriceChangeDetail.SalesPrice - 1;
            }*/
            return partsSalesPriceChangeDetails.OrderBy(r => r.Id);
        }

        public VirtualPartsSalesPriceChangeDetail 查询配件销售价变更申请清单附带采购价指导价(int branchId, int partsSalesCategoryId, int partId, string categoryCode, int brandId) {
            decimal purchasePricing = default(decimal);
            decimal maxPurchasePricing = default(decimal);
            var nowTime = DateTime.Now;
            var purchasePricings = ObjectContext.PartsPurchasePricings.Where(r => r.BranchId == branchId && r.PartId == partId && r.PartsSalesCategoryId == partsSalesCategoryId && r.ValidFrom <= nowTime && r.ValidTo >= nowTime && r.Status == (int)DcsPartsPurchasePricingStatus.生效);
            if(purchasePricings != null) {
                purchasePricing = purchasePricings.Min(r => r.PurchasePrice);
                maxPurchasePricing = purchasePricings.Where(r => r.PriceType == (int)DcsPurchasePriceType.正式价格).Max(r => r.PurchasePrice);
            }
            var partsSalesPrice = ObjectContext.PartsSalesPrices.FirstOrDefault(r => r.BranchId == branchId && r.SparePartId == partId && r.PartsSalesCategoryId == partsSalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效);
            var partsRetailGuidePrice = ObjectContext.PartsRetailGuidePrices.FirstOrDefault(r => r.BranchId == branchId && r.SparePartId == partId && r.PartsSalesCategoryId == partsSalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效);
            var partsPriceIncreaseRate = ObjectContext.PartsPriceIncreaseRates.FirstOrDefault(r => r.BrandId == brandId && r.CategoryCode == categoryCode && r.Status == (int)DcsBaseDataStatus.有效);
            var partsSalePriceIncreaseRates = ObjectContext.PartsSalePriceIncreaseRates.Where(r => ObjectContext.PartsBranches.Any(b => r.Id == b.IncreaseRateGroupId && b.BranchId == branchId && b.PartId == partId && b.PartsSalesCategoryId == partsSalesCategoryId && b.Status == (int)DcsMasterDataStatus.有效 && r.status == (int)DcsBaseDataStatus.有效));
            //获取最高互换价
            var salesPrice = ObjectContext.PartsSalesPrices.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.ValidationTime <= nowTime && r.ExpireTime >= nowTime && ObjectContext.PartsExchanges.Any(x => r.SparePartId == x.PartId && ObjectContext.PartsExchanges.Any(y => y.PartId == partId && x.ExchangeCode == y.ExchangeCode))).Max(r => r.SalesPrice);
            var virtualpPartsSalesPriceChangeDetail = new VirtualPartsSalesPriceChangeDetail();
            virtualpPartsSalesPriceChangeDetail.SparePartId = partId;
            virtualpPartsSalesPriceChangeDetail.MaxPurchasePricing = maxPurchasePricing;
            virtualpPartsSalesPriceChangeDetail.PurchasePrice = purchasePricing;
            virtualpPartsSalesPriceChangeDetail.MaxExchangeSalePrice = salesPrice;
            virtualpPartsSalesPriceChangeDetail.PartsSalesPrice_SalePrice = partsSalesPrice != null ? partsSalesPrice.SalesPrice : 0;
            virtualpPartsSalesPriceChangeDetail.PartsRetailGuidePrice_RetailGuidePrice = partsRetailGuidePrice != null ? partsRetailGuidePrice.RetailGuidePrice : 0;
            if(partsSalePriceIncreaseRates.Any())
                virtualpPartsSalesPriceChangeDetail.PriceTypeName = partsSalePriceIncreaseRates.First().GroupCode;
            if(partsPriceIncreaseRate != null) {
                virtualpPartsSalesPriceChangeDetail.IncreaseRateSale = Convert.ToDecimal(partsPriceIncreaseRate.SaleIncreaseRate ?? 0);
                virtualpPartsSalesPriceChangeDetail.IncreaseRateRetail = Convert.ToDecimal(partsPriceIncreaseRate.retailIncreaseRate ?? 0);
                virtualpPartsSalesPriceChangeDetail.MaxSalesPriceFloating = partsPriceIncreaseRate.MaxSalesPriceFloating;
                virtualpPartsSalesPriceChangeDetail.MinSalesPriceFloating = partsPriceIncreaseRate.MinSalesPriceFloating;
                virtualpPartsSalesPriceChangeDetail.MaxRetailOrderPriceFloating = partsPriceIncreaseRate.MaxRetailOrderPriceFloating;
                virtualpPartsSalesPriceChangeDetail.MinRetailOrderPriceFloating = partsPriceIncreaseRate.MinRetailOrderPriceFloating;
            }
            return virtualpPartsSalesPriceChangeDetail;
        }

        public IQueryable<VirtualPartsSalePriceIncreaseRate> GetPartsSalePriceIncreaseRateBySparePartIds(int[] sparePartIds, int partsSalesCategoryId, int branchId) {
            var result = from a in ObjectContext.PartsSalePriceIncreaseRates.Where(r => r.status == (int)DcsBaseDataStatus.有效)
                         join f in ObjectContext.PartsBranches.Where(b => b.BranchId == branchId && sparePartIds.Contains(b.PartId) && b.PartsSalesCategoryId == partsSalesCategoryId && b.Status == (int)DcsMasterDataStatus.有效) on a.Id equals f.IncreaseRateGroupId
                         select new VirtualPartsSalePriceIncreaseRate {
                             Id = f.Id,
                             PartId = f.PartId,
                             GroupCode = a.GroupCode
                         };
            return result;
        }

        public IQueryable<PartsSalePriceIncreaseRate> 查询配件销售标准加价率(string sparePartCode, int partsSalesCategoryId, int branchId) {
            return ObjectContext.PartsSalePriceIncreaseRates.Where(r => ObjectContext.PartsBranches.Any(b => r.Id == b.IncreaseRateGroupId && b.BranchId == branchId && b.PartCode == sparePartCode && b.PartsSalesCategoryId == partsSalesCategoryId && b.Status == (int)DcsMasterDataStatus.有效));
        }
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               

        public IQueryable<PartsSalesPriceChangeDetail> 查询配件销售价变更申请清单附带加价率(int parentId) {
            return new PartsSalesPriceChangeDetailAch(this).查询配件销售价变更申请清单附带加价率(parentId);
        }

        public VirtualPartsSalesPriceChangeDetail 查询配件销售价变更申请清单附带采购价指导价(int branchId, int partsSalesCategoryId, int partId, string categoryCode, int brandId) {
            return new PartsSalesPriceChangeDetailAch(this).查询配件销售价变更申请清单附带采购价指导价(branchId, partsSalesCategoryId, partId, categoryCode, brandId);
        }
        public IQueryable<PartsSalePriceIncreaseRate> 查询配件销售标准加价率(string sparePartCode, int partsSalesCategoryId, int branchId) {
            return new PartsSalesPriceChangeDetailAch(this).查询配件销售标准加价率(sparePartCode, partsSalesCategoryId, branchId);
        }
        public IQueryable<VirtualPartsSalePriceIncreaseRate> GetPartsSalePriceIncreaseRateBySparePartIds(int[] sparePartIds, int partsSalesCategoryId, int branchId) {
            return new PartsSalesPriceChangeDetailAch(this).GetPartsSalePriceIncreaseRateBySparePartIds(sparePartIds, partsSalesCategoryId, branchId);
        }
    }
}
