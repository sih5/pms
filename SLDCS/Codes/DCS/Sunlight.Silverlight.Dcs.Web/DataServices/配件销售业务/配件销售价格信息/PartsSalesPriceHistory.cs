﻿using System;
using System.Linq;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsSalesPriceHistoryAch : DcsSerivceAchieveBase {
        public PartsSalesPriceHistoryAch(DcsDomainService domainService)
            : base(domainService) {
        }

        internal void InsertPartsSalesPriceHistoryValidate(PartsSalesPriceHistory partsSalesPriceHistory) {
            var userInfo = Utils.GetCurrentUserInfo();
            partsSalesPriceHistory.CreatorId = userInfo.Id;
            partsSalesPriceHistory.CreatorName = userInfo.Name;
            partsSalesPriceHistory.CreateTime = DateTime.Now;
        }
        public IQueryable<PartsSalesPriceChangeDetail> 查询配件销售价格变更申请() {
            var result = ObjectContext.PartsSalesPriceChangeDetails.Include("PartsSalesPriceChange");

            var result2 = result.Where(c => c.PartsSalesPriceChange.Status == (int)DcsPartsSalesPriceChangeStatus.生效);
            //if(!string.IsNullOrEmpty((SparePartCode))) {
            //    result= result.Where(r => r.SparePartCode == SparePartCode);
            //}
            //if(!string.IsNullOrEmpty((SparePartName))) {
            //    result= result.Where(r => r.SparePartName == SparePartName);
            //}
            //result = from r in result
            //         from v in ObjectContext.PartsSalesPriceChanges
            //         from c in ObjectContext.PartsSalesCategorys
            //         on v.PartsSalesCategoryId == c.Id and  r.ParentId == v.Id
            //         where v => v.CreateTime > CreateTime[0] && v.CreateTime < CreateTime[1]
            //         select new {};

            return result2.OrderBy(r => r.Id);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               

        public IQueryable<PartsSalesPriceChangeDetail> 查询配件销售价格变更申请() {
            return new PartsSalesPriceHistoryAch(this).查询配件销售价格变更申请();
        }
    }
}
