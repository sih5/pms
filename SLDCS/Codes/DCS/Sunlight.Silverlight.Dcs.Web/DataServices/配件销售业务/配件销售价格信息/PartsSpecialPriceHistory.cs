﻿using System;
using System.Linq;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsSpecialPriceHistoryAch : DcsSerivceAchieveBase {
        public PartsSpecialPriceHistoryAch(DcsDomainService domainService)
            : base(domainService) {
        }

        internal void InsertPartsSpecialPriceHistoryValidate(PartsSpecialPriceHistory PartsSpecialPriceHistory) {
            var userInfo = Utils.GetCurrentUserInfo();
            PartsSpecialPriceHistory.CreatorId = userInfo.Id;
            PartsSpecialPriceHistory.CreatorName = userInfo.Name;
            PartsSpecialPriceHistory.CreateTime = DateTime.Now;
        }
        internal void UpdatePartsSpecialPriceHistoryValidate(PartsSpecialPriceHistory PartsSpecialPriceHistory) {
        }
        public void InsertPartsSpecialPriceHistory(PartsSpecialPriceHistory partsSpecialPriceHistory) {
            InsertToDatabase(partsSpecialPriceHistory);
            this.InsertPartsSpecialPriceHistoryValidate(partsSpecialPriceHistory);
        }
        public void UpdatePartsSpecialPriceHistory(PartsSpecialPriceHistory partsSpecialPriceHistory) {
            UpdateToDatabase(partsSpecialPriceHistory);
            this.UpdatePartsSpecialPriceHistoryValidate(partsSpecialPriceHistory);
        }
        public IQueryable<PartsSpecialPriceHistory> GetPartsSpecialPriceHistoryWithCompanies() {
            return ObjectContext.PartsSpecialPriceHistories.Include("SparePart").Include("Company").OrderBy(v => v.Id);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               

        public void InsertPartsSpecialPriceHistory(PartsSpecialPriceHistory partsSpecialPriceHistory) {
            new PartsSpecialPriceHistoryAch(this).InsertPartsSpecialPriceHistory(partsSpecialPriceHistory);
        }

        public void UpdatePartsSpecialPriceHistory(PartsSpecialPriceHistory partsSpecialPriceHistory) {
            new PartsSpecialPriceHistoryAch(this).UpdatePartsSpecialPriceHistory(partsSpecialPriceHistory);
        }

        public IQueryable<PartsSpecialPriceHistory> GetPartsSpecialPriceHistoryWithCompanies() {
            return new PartsSpecialPriceHistoryAch(this).GetPartsSpecialPriceHistoryWithCompanies();
        }
    }
}

