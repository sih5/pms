﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsSpecialTreatyPriceAch : DcsSerivceAchieveBase {
        internal void InsertPartsSpecialTreatyPriceValidate(PartsSpecialTreatyPrice partsSpecialTreatyPrice) {
            var dbpartsSpecialTreatyPrice = ObjectContext.PartsSpecialTreatyPrices.Where(r => r.PartsSalesCategoryId == partsSpecialTreatyPrice.PartsSalesCategoryId && r.SparePartId == partsSpecialTreatyPrice.SparePartId && r.CompanyId == partsSpecialTreatyPrice.CompanyId && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbpartsSpecialTreatyPrice != null)
                throw new ValidationException(string.Format(ErrorStrings.PartsSpecialTreatyPrice_Validation1, partsSpecialTreatyPrice.PartsSalesCategoryName, partsSpecialTreatyPrice.SparePartName));
            var userInfo = Utils.GetCurrentUserInfo();
            partsSpecialTreatyPrice.CreatorId = userInfo.Id;
            partsSpecialTreatyPrice.CreatorName = userInfo.Name;
            partsSpecialTreatyPrice.CreateTime = DateTime.Now;
        }
        internal void UpdatePartsSpecialTreatyPriceValidate(PartsSpecialTreatyPrice partsSpecialTreatyPrice) {
            //var dbpartsSpecialTreatyPrice = ObjectContext.PartsSpecialTreatyPrices.Where(r => r.Id != partsSpecialTreatyPrice.Id && r.PartsSalesCategoryId == partsSpecialTreatyPrice.PartsSalesCategoryId && r.SparePartId == partsSpecialTreatyPrice.SparePartId && r.CompanyId == partsSpecialTreatyPrice.CompanyId && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            //if(dbpartsSpecialTreatyPrice != null)
            //    throw new ValidationException(string.Format(ErrorStrings.PartsSpecialTreatyPrice_Validation1, partsSpecialTreatyPrice.PartsSalesCategoryName, partsSpecialTreatyPrice.SparePartName));
            var userInfo = Utils.GetCurrentUserInfo();
            partsSpecialTreatyPrice.ModifierId = userInfo.Id;
            partsSpecialTreatyPrice.ModifierName = userInfo.Name;
            partsSpecialTreatyPrice.ModifyTime = DateTime.Now;
        }
        public void InsertPartsSpecialTreatyPrice(PartsSpecialTreatyPrice partsSpecialTreatyPrice) {
            InsertToDatabase(partsSpecialTreatyPrice);
            this.InsertPartsSpecialTreatyPriceValidate(partsSpecialTreatyPrice);
            var partsSpecialPriceHistory = new PartsSpecialPriceHistory {
                PartsSpecialTreatyPriceId = partsSpecialTreatyPrice.Id,
                BranchId = partsSpecialTreatyPrice.BranchId,
                PartsSalesCategoryId = partsSpecialTreatyPrice.PartsSalesCategoryId,
                PartsSalesCategoryCode = partsSpecialTreatyPrice.PartsSalesCategoryCode,
                PartsSalesCategoryName = partsSpecialTreatyPrice.PartsSalesCategoryName,
                SparePartId = partsSpecialTreatyPrice.SparePartId,
                SparePartCode = partsSpecialTreatyPrice.SparePartCode,
                SparePartName = partsSpecialTreatyPrice.SparePartName,
                CompanyId = partsSpecialTreatyPrice.CompanyId,
                TreatyPrice = partsSpecialTreatyPrice.TreatyPrice,
                Status = partsSpecialTreatyPrice.Status,
                Remark = partsSpecialTreatyPrice.Remark,
                CreatorId = partsSpecialTreatyPrice.CreatorId,
                CreatorName = partsSpecialTreatyPrice.CreatorName,
                CreateTime = partsSpecialTreatyPrice.CreateTime
            };
            partsSpecialTreatyPrice.PartsSpecialPriceHistories.Add(partsSpecialPriceHistory);
            DomainService.InsertPartsSpecialPriceHistory(partsSpecialPriceHistory);
        }
        public void UpdatePartsSpecialTreatyPrice(PartsSpecialTreatyPrice partsSpecialTreatyPrice) {
            UpdateToDatabase(partsSpecialTreatyPrice);
            this.UpdatePartsSpecialTreatyPriceValidate(partsSpecialTreatyPrice);
            var partsSpecialPriceHistory = new PartsSpecialPriceHistory {
                PartsSpecialTreatyPriceId = partsSpecialTreatyPrice.Id,
                BranchId = partsSpecialTreatyPrice.BranchId,
                PartsSalesCategoryId = partsSpecialTreatyPrice.PartsSalesCategoryId,
                PartsSalesCategoryCode = partsSpecialTreatyPrice.PartsSalesCategoryCode,
                PartsSalesCategoryName = partsSpecialTreatyPrice.PartsSalesCategoryName,
                SparePartId = partsSpecialTreatyPrice.SparePartId,
                SparePartCode = partsSpecialTreatyPrice.SparePartCode,
                SparePartName = partsSpecialTreatyPrice.SparePartName,
                CompanyId = partsSpecialTreatyPrice.CompanyId,
                TreatyPrice = partsSpecialTreatyPrice.TreatyPrice,
                Status = partsSpecialTreatyPrice.Status,
                Remark = partsSpecialTreatyPrice.Remark,
                CreatorId = partsSpecialTreatyPrice.CreatorId,
                CreatorName = partsSpecialTreatyPrice.CreatorName,
                CreateTime = partsSpecialTreatyPrice.CreateTime
            };
            partsSpecialTreatyPrice.PartsSpecialPriceHistories.Add(partsSpecialPriceHistory);
            DomainService.InsertPartsSpecialPriceHistory(partsSpecialPriceHistory);
        }
        public IQueryable<PartsSpecialTreatyPrice> GetPartsSpecialTreatyPricesWithCompany() {
            return ObjectContext.PartsSpecialTreatyPrices.Include("SparePart").Include("Company").OrderBy(v => v.Id);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               

        public void InsertPartsSpecialTreatyPrice(PartsSpecialTreatyPrice partsSpecialTreatyPrice) {
            new PartsSpecialTreatyPriceAch(this).InsertPartsSpecialTreatyPrice(partsSpecialTreatyPrice);
        }

        public void UpdatePartsSpecialTreatyPrice(PartsSpecialTreatyPrice partsSpecialTreatyPrice) {
            new PartsSpecialTreatyPriceAch(this).UpdatePartsSpecialTreatyPrice(partsSpecialTreatyPrice);
        }

        public IQueryable<PartsSpecialTreatyPrice> GetPartsSpecialTreatyPricesWithCompany() {
            return new PartsSpecialTreatyPriceAch(this).GetPartsSpecialTreatyPricesWithCompany();
        }
    }
}

