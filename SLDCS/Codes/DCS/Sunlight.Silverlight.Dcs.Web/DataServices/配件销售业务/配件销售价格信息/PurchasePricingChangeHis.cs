﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Microsoft.Data.Extensions;
using NPOI.HSSF.Record.Chart;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
using System.Collections.Generic;
using System.ServiceModel.DomainServices.Server;

namespace Sunlight.Silverlight.Dcs.Web
{
    partial class PurchasePricingChangeHisAch : DcsSerivceAchieveBase
    {
        public PurchasePricingChangeHisAch(DcsDomainService domainService)
            : base(domainService)
        {
        }
        public IQueryable<VirtualPurchasePricingChangeHi> 查询采购价格变动情况记录()
        {
            var date=DateTime.Now.Date;
            IQueryable<VirtualPurchasePricingChangeHi> result = (from a in ObjectContext.PurchasePricingChangeHis.Where(t => (t.ValidFrom <= date&& t.ValidTo>=date)|| t.ValidFrom ==null)
                                                                 join b in ObjectContext.PartsSalesCategories on a.PartsSalesCategoryId equals b.Id
                                                                 join sp in ObjectContext.SpareParts on a.SparePartId equals sp.Id
                                                                 join c in ObjectContext.PartsBranches on a.SparePartId equals c.PartId
                                                                 join d in ObjectContext.PartsSalePriceIncreaseRates on c.IncreaseRateGroupId equals d.Id into temp
                                                                 from tt in temp.DefaultIfEmpty()
                                                                 select new VirtualPurchasePricingChangeHi
                                                                 {
                                                                     Id = a.Id,
                                                                     PartsSalesCategoryName = b.Name,
                                                                     SparePartCode = a.SparePartCode,
                                                                     SparePartName = a.SparePartName,
                                                                     OldPurchasePrice = a.OldPurchasePrice.Value,
                                                                     PurchasePrice = a.PurchasePrice.Value,
                                                                     ChangeRatio = a.ChangeRatio.Value,
                                                                     CenterPrice = a.CenterPrice.Value,
                                                                     DistributionPrice = a.DistributionPrice.Value,
                                                                     RetailPrice = a.RetailPrice.Value,
                                                                     MaintenanceTime = a.MaintenanceTime,
                                                                     Status = a.Status.Value,
                                                                     Remark = a.Remark,
                                                                     CreatorName = a.CreatorName,
                                                                     CreateTime = a.CreateTime,
                                                                     ModifierName = a.ModifierName,
                                                                     ModifyTime = a.ModifyTime,
                                                                     IncreaseRateGroupId =tt.GroupName,
                                                                     IsPrimary = a.IsPrimary,
                                                                     ReferenceCode = sp.ReferenceCode,
                                                                     ValidFrom=a.ValidFrom,
                                                                     ValidTo=a.ValidTo
                                                                 }
                                                                   );

            return result.OrderBy(r=>r.CreateTime);
        }
        public void abandonVirtualPurchasePricingChangeHi(VirtualPurchasePricingChangeHi pick)
        {
            var userInfo = Utils.GetCurrentUserInfo();
            var hi = ObjectContext.PurchasePricingChangeHis.Where(r=>r.Id==pick.Id).FirstOrDefault();
            hi.Status = (int)DcsMasterDataStatus.作废;
            hi.ModifierId = userInfo.Id;
            hi.ModifierName = userInfo.Name;
            hi.ModifyTime = DateTime.Now;
            UpdateToDatabase(hi);
            ObjectContext.SaveChanges();
        }
    }
    partial class DcsDomainService
    {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach   
        public IQueryable<VirtualPurchasePricingChangeHi> 查询采购价格变动情况记录()
        {
            return new PurchasePricingChangeHisAch(this).查询采购价格变动情况记录();
        }
        [Update(UsingCustomMethod = true)]
        public void abandonVirtualPurchasePricingChangeHi(VirtualPurchasePricingChangeHi pick)
        {
            new PurchasePricingChangeHisAch(this).abandonVirtualPurchasePricingChangeHi(pick);
        }
    }
}
