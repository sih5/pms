﻿using System;
using System.Linq;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class SpecialPriceChangeListAch : DcsSerivceAchieveBase {
        public SpecialPriceChangeListAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public IQueryable<SpecialPriceChangeList> 查询配件特殊协议价变更申请清单(int parentId) {
            var specialPriceChangeLists = ObjectContext.SpecialPriceChangeLists.Where(r => r.SpecialTreatyPriceChangeId == parentId);
            return specialPriceChangeLists.OrderBy(r => r.Id);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               

        public IQueryable<SpecialPriceChangeList> 查询配件特殊协议价变更申请清单(int parentId) {
            return new SpecialPriceChangeListAch(this).查询配件特殊协议价变更申请清单(parentId);
        }
    }
}
