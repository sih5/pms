﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class SpecialTreatyPriceChangeAch : DcsSerivceAchieveBase {
        internal void InsertSpecialTreatyPriceChangeValidate(SpecialTreatyPriceChange specialTreatyPriceChange) {
            if(string.IsNullOrWhiteSpace(specialTreatyPriceChange.Code) || specialTreatyPriceChange.Code == GlobalVar.ASSIGNED_BY_SERVER)
                specialTreatyPriceChange.Code = CodeGenerator.Generate("SpecialTreatyPriceChange");
            var userInfo = Utils.GetCurrentUserInfo();
            specialTreatyPriceChange.CreatorId = userInfo.Id;
            specialTreatyPriceChange.CreatorName = userInfo.Name;
            specialTreatyPriceChange.CreateTime = DateTime.Now;
        }

        internal void UpdateSpecialTreatyPriceChangeValidate(SpecialTreatyPriceChange specialTreatyPriceChange) {
            var userInfo = Utils.GetCurrentUserInfo();
            specialTreatyPriceChange.ModifierId = userInfo.Id;
            specialTreatyPriceChange.ModifierName = userInfo.Name;
            specialTreatyPriceChange.ModifyTime = DateTime.Now;
        }

        public void InsertSpecialTreatyPriceChange(SpecialTreatyPriceChange specialTreatyPriceChange) {
            InsertToDatabase(specialTreatyPriceChange);
            var specialPriceChangeLists = ChangeSet.GetAssociatedChanges(specialTreatyPriceChange, v => v.SpecialPriceChangeLists, ChangeOperation.Insert);
            foreach(SpecialPriceChangeList specialPriceChangeList in specialPriceChangeLists) {
                specialPriceChangeList.SpecialTreatyPrice = Math.Round(specialPriceChangeList.SpecialTreatyPrice.Value, 2, MidpointRounding.AwayFromZero);
                InsertToDatabase(specialPriceChangeList);
            }
            this.InsertSpecialTreatyPriceChangeValidate(specialTreatyPriceChange);
        }

        public void UpdateSpecialTreatyPriceChange(SpecialTreatyPriceChange specialTreatyPriceChange) {
            specialTreatyPriceChange.SpecialPriceChangeLists.Clear();
            UpdateToDatabase(specialTreatyPriceChange);
            var specialPriceChangeLists = ChangeSet.GetAssociatedChanges(specialTreatyPriceChange, v => v.SpecialPriceChangeLists);
            foreach(SpecialPriceChangeList specialPriceChangeList in specialPriceChangeLists) {
                specialPriceChangeList.SpecialTreatyPrice = Math.Round(specialPriceChangeList.SpecialTreatyPrice.Value, 2, MidpointRounding.AwayFromZero);
                //获取清单实体对象的操作类型
                switch(ChangeSet.GetChangeOperation(specialPriceChangeList)) {
                    //新增操作
                    case ChangeOperation.Insert:
                        InsertToDatabase(specialPriceChangeList);
                        break;
                    //更新操作
                    case ChangeOperation.Update:
                        UpdateToDatabase(specialPriceChangeList);
                        break;
                    //删除操作
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(specialPriceChangeList);
                        break;
                }
            }
            this.UpdateSpecialTreatyPriceChangeValidate(specialTreatyPriceChange);
        }

        public SpecialTreatyPriceChange GetSpecialTreatyPriceChangesWithDetailsById(int id) {
             var userInfo = Utils.GetCurrentUserInfo();
             if(userInfo.EnterpriseId == 0) {
                 throw new ValidationException("登录已超时，请重新登录");
             }
            var dbSpecialTreatyPriceChange = ObjectContext.SpecialTreatyPriceChanges.SingleOrDefault(e => e.Id == id);
            if(dbSpecialTreatyPriceChange != null) {
                var specialPriceChangeLists = from a in ObjectContext.SpecialPriceChangeLists
                                              join b in ObjectContext.SpecialTreatyPriceChanges on a.SpecialTreatyPriceChangeId equals b.Id
                                              join c in ObjectContext.PartsPurchasePricings.Where(r => r.ValidFrom <= DateTime.Now
                                                  && r.ValidTo >= DateTime.Now
                                                  && r.Status == (int)DcsPartsPurchasePricingStatus.生效) on new {
                                                      n = b.BrandId,
                                                      m = a.SparePartId
                                                  } equals new {
                                                      n = (int?)c.PartsSalesCategoryId,
                                                      m = (int?)c.PartId
                                                  } into table
                                              from t in table.DefaultIfEmpty()
                                              join d in ObjectContext.EnterprisePartsCosts.Where(r => r.OwnerCompanyId == userInfo.EnterpriseId) on a.SparePartId equals d.SparePartId into cost
                                              from s in cost.DefaultIfEmpty()
                                              select new {
                                                  Id = a.Id,
                                                  PurchasePrice = t.PurchasePrice,
                                                  Remark = a.Remark,
                                                  SparePartCode = a.SparePartCode,
                                                  SparePartId = a.SparePartId,
                                                  SparePartName = a.SparePartName,
                                                  SpecialTreatyPrice = a.SpecialTreatyPrice,
                                                  SpecialTreatyPriceChangeId = a.SpecialTreatyPriceChangeId,
                                                  RetailPrice = a.RetailPrice,
                                                  Ratio = a.Ratio,
                                                  CostPrice=s.CostPrice
                                              };
                var dbDetails = specialPriceChangeLists.Where(ex => ex.SpecialTreatyPriceChangeId == dbSpecialTreatyPriceChange.Id).ToArray();
                foreach(var item in dbDetails.OrderByDescending(r => r.PurchasePrice)) {
                    SpecialPriceChangeList specialPriceChangeList = new SpecialPriceChangeList();
                    specialPriceChangeList.Id = item.Id;
                    specialPriceChangeList.PurchasePrice = item.PurchasePrice;
                    specialPriceChangeList.Remark = item.Remark;
                    specialPriceChangeList.SparePartCode = item.SparePartCode;
                    specialPriceChangeList.SparePartId = item.SparePartId;
                    specialPriceChangeList.SparePartName = item.SparePartName;
                    specialPriceChangeList.SpecialTreatyPrice = item.SpecialTreatyPrice;
                    specialPriceChangeList.SpecialTreatyPriceChangeId = item.SpecialTreatyPriceChangeId;
                    specialPriceChangeList.RetailPrice = item.RetailPrice;
                    specialPriceChangeList.Ratio = item.Ratio;
                    specialPriceChangeList.CostPrice = item.CostPrice;
                    dbSpecialTreatyPriceChange.SpecialPriceChangeLists.Add(specialPriceChangeList);
                }
            }
            return dbSpecialTreatyPriceChange;
        }

        public IQueryable<SpecialTreatyPriceChange> GetSpecialTreatyPriceChangeById(int id) {
            return ObjectContext.SpecialTreatyPriceChanges.Where(r => r.Id == id);
        }

        public IQueryable<SpecialTreatyPriceChange> GetSpecialTreatyPriceChangesForOrder() {
            var result = ObjectContext.SpecialTreatyPriceChanges;
            foreach (var item in result) { 
                item.IsUplodFile = item.Path != null && item.Path.Length > 0;
            }
            return result.OrderByDescending(r => r.CreateTime);
        }

        public IQueryable<SpecialPriceChangeList> GetSpecialPriceChangeListsRelation() {
            var userInfo = Utils.GetCurrentUserInfo();
            if(userInfo.EnterpriseId == 0) {
                throw new ValidationException("登录已超时，请重新登录");
            }
            var result = from spcl in ObjectContext.SpecialPriceChangeLists
                         join psr in ObjectContext.PartsSupplierRelations.Where(r=>r.IsPrimary==true) on spcl.SparePartId equals psr.PartId
                         into table
                         from t in table.DefaultIfEmpty()
                       //  where t.IsPrimary == true
                         join pppd in ObjectContext.PartsPurchasePricings.Where(r => r.ValidFrom <= DateTime.Now
                                                  && r.ValidTo >= DateTime.Now
                                                  && r.Status == (int)DcsPartsPurchasePricingStatus.生效) on new {
                                                      PartId = spcl.SparePartId.Value,
                                                      SupplierId = t.SupplierId
                                                  } equals new {
                                                      PartId = pppd.PartId,
                                                      SupplierId = pppd.PartsSupplierId
                                                  } into tablePurchas
                         from tp in tablePurchas.DefaultIfEmpty()
                         join psp in ObjectContext.PartsSalesPrices on spcl.SparePartId equals psp.SparePartId into tableSales
                         from ts in tableSales.DefaultIfEmpty()
                         join cs in ObjectContext.EnterprisePartsCosts.Where(t => t.OwnerCompanyId == userInfo.EnterpriseId) on spcl.SparePartId equals cs.SparePartId into tableCosts
                         from ct in tableCosts.DefaultIfEmpty()
                         select new {
                             spcl,
                             PurchasePrice = (decimal?)tp.PurchasePrice?? ct.CostPrice,
                             SalesPrice = ts.SalesPrice,
                             CenterPrice = ts.CenterPrice
                         };

            foreach(var item in result) {
                item.spcl.PurchasePrice = item.PurchasePrice;
                item.spcl.SalesPrice = item.SalesPrice;
                item.spcl.CenterPrice = item.CenterPrice;
            }
            return result.Select(o => o.spcl).OrderBy(o => o.SparePartId);
        }
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               

        public void InsertSpecialTreatyPriceChange(SpecialTreatyPriceChange specialTreatyPriceChange) {
            new SpecialTreatyPriceChangeAch(this).InsertSpecialTreatyPriceChange(specialTreatyPriceChange);
        }

        public void UpdateSpecialTreatyPriceChange(SpecialTreatyPriceChange specialTreatyPriceChange) {
            new SpecialTreatyPriceChangeAch(this).UpdateSpecialTreatyPriceChange(specialTreatyPriceChange);
        }

        public SpecialTreatyPriceChange GetSpecialTreatyPriceChangesWithDetailsById(int id) {
            return new SpecialTreatyPriceChangeAch(this).GetSpecialTreatyPriceChangesWithDetailsById(id);
        }

        public IQueryable<SpecialTreatyPriceChange> GetSpecialTreatyPriceChangeById(int id) {
            return new SpecialTreatyPriceChangeAch(this).GetSpecialTreatyPriceChangeById(id);
        }

        public IQueryable<SpecialTreatyPriceChange> GetSpecialTreatyPriceChangesForOrder() {
            return new SpecialTreatyPriceChangeAch(this).GetSpecialTreatyPriceChangesForOrder();
        }

        public IQueryable<SpecialPriceChangeList> GetSpecialPriceChangeListsRelation() {
            return new SpecialTreatyPriceChangeAch(this).GetSpecialPriceChangeListsRelation();
        }
    }
}
