﻿using System.Collections.Generic;
using System.Linq;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class VirtualPartsPriceHistoryAch : DcsSerivceAchieveBase {
        public VirtualPartsPriceHistoryAch(DcsDomainService domainService)
            : base(domainService) {
        }
        
        public IEnumerable<VirtualPartsPriceHistory> 配件价格查询() {
            const string SQL = @" SELECT branch.code as BranchCode,--营销公司
                                   branch.Id as BranchId,
                                   partssalescategory.id as PartsSalesCategoryId,--品牌Id
                                   partssalescategory.Name as PartsSalesCategoryName,--品牌
                                   SPAREPART.Id as PartId,
                                   SPAREPART.Code as PartCode,--配件图号
                                   SPAREPART.Name as PartName,--配件图号
                                   nvl(PARTSSALESPRICE.SalesPrice, 0) as SalesPrice,--配件销售价.销售价(批发价)
                                   nvl(PARTSRETAILGUIDEPRICE.RETAILGUIDEPRICE, 0) as RetailGuidePrice,--配件零售指导价.零售指导价(零售指导价)
                                   PARTSSUPPLIER.Id as SupplierId,
                                   PARTSSUPPLIER.CODE as SupplierCode, --供应商编号
                                   PARTSSUPPLIER.NAME as SupplierName,--供应商名称
                                   nvl(PARTSPURCHASEPRICING.PURCHASEPRICE, 0) as PurchasePrice,--配件采购价格.采购价格(采购价)
                                   nvl(PARTSPLANNEDPRICE.PLANNEDPRICE, 0) as PlannedPrice, --配件计划价.计划价(计划价)
                                   sparepart.measureunit as  Measureunit,--计量单位
                                   pec.exchangecode as ExchangeCode,--互换号
                                   pec.exchangename as ExchangeName,--互换号
                                   sparepart.Createtime,--配件创建时间
                                   partssalesprice.createtime as Screatetime,--销售价创建时间
                                   partsplannedprice.createtime as Pcreatetime,--计划价创建时间
                                   nvl(dps.dealersalesprice, 0) as Dealersalesprice,--服务站配件批发价.服务站批发价(服务站批发价)
                                   partsbranch.IsOrderable as IsOrderable,
                                   partsbranch.IsSalable  as  IsSalable,
                                   kvi.Key as PartsAttribution,
                                   rownum as Id
                              FROM sparepart
                             inner join partsbranch
                                on sparepart.id = partsbranch.partid
                             inner join branch
                                on branch.id = partsbranch.branchid
                             inner join partssalescategory
                                on partssalescategory.id = partsbranch.partssalescategoryid
                              left join PartsSupplierRelation psr
                                on psr.partssalescategoryid = partssalescategory.id
                               and psr.partid = sparepart.id
                               and psr.status <> 99
                              left join partssupplier
                                on partssupplier.id = psr.supplierid
                              left join PartsExchange pec
                                on pec.partid = sparepart.id
                               and pec.status <> 99
                              left join partssalesprice
                                on partssalesprice.sparepartid = sparepart.id
                               and partssalesprice.partssalescategoryid = partssalescategory.id
                              left join PartsRetailGuidePrice
                                on PartsRetailGuidePrice.sparepartid = sparepart.id
                               and PartsRetailGuidePrice.partssalescategoryid = partssalescategory.id
                               and PartsRetailGuidePrice.status = 1
                              left join PartsPlannedPrice
                                on PartsPlannedPrice.ownercompanyid = branch.id
                               and PartsPlannedPrice.partssalescategoryid = partssalescategory.id
                               and PartsPlannedPrice.sparepartid = sparepart.id
                              left join PartsPurchasePricing
                                on PartsPurchasePricing.branchid = branch.id
                               and PartsPurchasePricing.partid = sparepart.id
                               and PartsPurchasePricing.partssalescategoryid = partssalescategory.id
                               and PartsPurchasePricing.status = 2
                                  and validfrom <= to_date(to_char(sysdate,'yyyy-mm-dd'),'yyyy-mm-dd')
                                  and validto >= to_date(to_char(sysdate,'yyyy-mm-dd'),'yyyy-mm-dd')
                               and psr.supplierid = PartsPurchasePricing.partssupplierid
                              left join DealerPartsSalesPrice dps
                                on dps.partssalescategoryid = partssalescategory.id
                               and dps.branchid = branch.id
                               and dps.sparepartid = sparepart.id
                               and dps.status = 1
                              left join keyvalueitem kvi
                                on kvi.status = 1
                               and kvi.name = 'PartsAttribution'
                               and kvi.key = partsbranch.PartsAttribution
                             where sparepart.status = 1
                            ";
            var search = ObjectContext.ExecuteStoreQuery<VirtualPartsPriceHistory>(SQL).ToList();
            return search.Distinct().OrderBy(r => r.PartId);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               
        
        public IEnumerable<VirtualPartsPriceHistory> 配件价格查询() {
            return new VirtualPartsPriceHistoryAch(this).配件价格查询();
        }
    }
}
