﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
//using System.Text;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class VirtualPartsSalesPriceAch : DcsSerivceAchieveBase {
        public VirtualPartsSalesPriceAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public IEnumerable<VirtualPartsSalesPrice> 根据销售类型查询配件销售价出口使用(int partsSalesCategoryId, int? orderTypeId, int customerId, bool? ifDirectProvision, int priceType, int[] spaePartsIds) {
            var partsSalesCategory = ObjectContext.PartsSalesCategories.SingleOrDefault(r => r.Id == partsSalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效);
            if(partsSalesCategory == null) {
                return null;
            }
            var company = ObjectContext.Companies.SingleOrDefault(r => r.Id == customerId && r.Status == (int)DcsMasterDataStatus.有效);
            if(company == null) {
                return null;
            }
            var partsBranches = ObjectContext.PartsBranches.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.IsSalable == true);
            var customerDirectSpareLists = this.ObjectContext.CustomerDirectSpareLists.Where(r => r.CustomerId == customerId && r.PartsSalesCategoryId == partsSalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效); //客户直供配件清单
            if(ifDirectProvision.HasValue && ifDirectProvision.Value) {
                partsBranches = partsBranches.Where(r => r.IsDirectSupply == ifDirectProvision.Value && r.PartsSalesCategoryId == partsSalesCategoryId && customerDirectSpareLists.Any(c => c.SparePartId == r.PartId));
            }
            if(ifDirectProvision.HasValue && !ifDirectProvision.Value) {
                partsBranches = partsBranches.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId && (r.IsDirectSupply == ifDirectProvision.Value || (r.IsDirectSupply && !customerDirectSpareLists.Any(c => c.SparePartId == r.PartId))));
            }

            var spaeParts = ObjectContext.SpareParts.AsQueryable();
            if(spaePartsIds != null && spaePartsIds.Length > 0) {
                spaeParts = spaeParts.Where(r => spaePartsIds.Contains(r.Id));
            }

            var partsInfoes = from partBranch in partsBranches
                              from sparePart in spaeParts
                              from partsSalesPrice in ObjectContext.PartsSalesPrices
                              where sparePart.Id == partBranch.PartId && sparePart.Status == (int)DcsMasterDataStatus.有效 && partsSalesPrice.PartsSalesCategoryId == partBranch.PartsSalesCategoryId
                              && partsSalesPrice.Status == (int)DcsBaseDataStatus.有效 && partsSalesPrice.SparePartId == partBranch.PartId && partsSalesPrice.PartsSalesCategoryId == partsSalesCategoryId
                              select new {
                                  sparePart.Id,
                                  sparePart.Code,
                                  sparePart.Name,
                                  sparePart.ReferenceCode,
                                  sparePart.ReferenceName,
                                  sparePart.Feature,
                                  partBranch.MinSaleQuantity,
                                  partsSalesPrice.PriceType,
                                  partsSalesPrice.SalesPrice,
                                  partsSalesPrice.CenterPrice,
                                  sparePart.MeasureUnit,
                                  sparePart.MInPackingAmount
                              };
            var date = DateTime.Now.Date;
            var partsSpecialTreatyPrices = ObjectContext.PartsSpecialTreatyPrices.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.CompanyId == customerId && r.PartsSalesCategoryId == partsSalesCategoryId && r.ValidationTime <= date && r.ExpireTime >= date);
            var dealerPartsSalesPrices = ObjectContext.DealerPartsSalesPrices.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效);
            var partsRetailGuidePrices = ObjectContext.PartsRetailGuidePrices.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效); //配件零售指导价
            var partsBranchs = this.ObjectContext.PartsBranches.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId && r.Status == (int)DcsMasterDataStatus.有效);
            var partsSalePriceIncreaseRates = this.ObjectContext.PartsSalePriceIncreaseRates.Where(r => r.status == (int)DcsBaseDataStatus.有效);
            var abcStrategys = this.ObjectContext.KeyValueItems.Where(r => r.Name == "ABCStrategy_Category" && r.Status == (int)DcsMasterDataStatus.有效);
            var result = (from partsInfo in partsInfoes
                          join partsSpecialTreatyPrice in partsSpecialTreatyPrices.Where(r => r.CompanyId == customerId) on partsInfo.Id equals partsSpecialTreatyPrice.SparePartId into tempTable1
                          from t1 in tempTable1.DefaultIfEmpty()
                          join dealerPartsSalesPrice in dealerPartsSalesPrices on partsInfo.Id equals dealerPartsSalesPrice.SparePartId into tempTable2
                          from t2 in tempTable2.DefaultIfEmpty()
                          join partsRetailGuidePrice in partsRetailGuidePrices on partsInfo.Id equals partsRetailGuidePrice.SparePartId into tempTable3
                          from t3 in tempTable3.DefaultIfEmpty()
                          join partsBranch in partsBranchs on partsInfo.Id equals partsBranch.PartId into tempTable4
                          from t4 in tempTable4.DefaultIfEmpty()
                          join partsSalePriceIncreaseRate in partsSalePriceIncreaseRates on t4.IncreaseRateGroupId equals partsSalePriceIncreaseRate.Id into tempTable5
                          from t5 in tempTable5.DefaultIfEmpty()
                          join abcStrategy in abcStrategys on t4.PartABC equals abcStrategy.Key into tempTable6
                          from t6 in tempTable6.DefaultIfEmpty()
                          select new VirtualPartsSalesPrice {
                              SparePartId = partsInfo.Id,
                              SparePartCode = partsInfo.Code,
                              SparePartName = partsInfo.Name,
                              ReferenceCode = partsInfo.ReferenceCode,
                              ReferenceName = partsInfo.ReferenceName,
                              Feature = partsInfo.Feature,
                              MinSaleQuantity = partsInfo.MinSaleQuantity,
                              PriceType = partsInfo.PriceType,
                              Coefficient = 1,
                              PartsSalesPrice = partsInfo.SalesPrice,
                              PartsTreatyPrice = t1.TreatyPrice,
                              DealerSalesPrice = t2 != null ? t2.DealerSalesPrice : 0,
                              Price = t1 != null ? t1.TreatyPrice : partsInfo.SalesPrice,
                              MeasureUnit = partsInfo.MeasureUnit,
                              CenterPrice = partsInfo.CenterPrice,
                              RetailGuidePrice = t3 != null ? t3.RetailGuidePrice : 0,
                              PriceTypeName = t5 != null ? t5.GroupName : "",
                              ABCStrategy = t6 != null ? t6.Value : "",
                              MInPackingAmount = partsInfo.MInPackingAmount,
                              SalesPrice = partsInfo.SalesPrice
                              //Remark = t1 != null ? "协议价" : "-1"
                          }).ToArray();
            foreach(var item in result) {
                if(priceType == (int)DcsExportCustPriceType.中心库价) {
                    item.Price = item.CenterPrice ?? 0;
                }
                if(priceType == (int)DcsExportCustPriceType.服务站价) {
                    item.Price = item.PartsSalesPrice;
                }
                if(priceType == (int)DcsExportCustPriceType.建议售价) {
                    item.Price = item.RetailGuidePrice;
                }
                if(priceType == (int)DcsExportCustPriceType.特殊协议价) {
                    item.Price = item.PartsTreatyPrice ?? 0;
                }
                if(priceType == (int)DcsExportCustPriceType.其他) {
                    item.Price = 0;
                }
                item.Remark = Enum.GetName(typeof(DcsPartsSalesPricePriceType), item.PriceType);
                item.ReferenceCode = item.ReferenceCode ?? string.Empty;
                item.ReferenceName = item.ReferenceName ?? string.Empty;
            }
            return result.OrderBy(r => r.SparePartId);
        }

        [Query(HasSideEffects = true)]
        public IEnumerable<VirtualPartsSalesPrice> 根据配件清单获取销售价格出口使用(int appCategoryId, int customerId, int[] partIds, int priceType) {
            var partsSalesCategoryId = ObjectContext.PartsSalesOrderTypes.Where(v => v.Id == appCategoryId && v.Status == (int)DcsBaseDataStatus.有效).Select(v => v.PartsSalesCategoryId).SingleOrDefault();

            if(partsSalesCategoryId == 0) {
                return Enumerable.Empty<VirtualPartsSalesPrice>();
            }
            var partsSalesCategory = ObjectContext.PartsSalesCategories.SingleOrDefault(r => r.Id == partsSalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效);
            if(partsSalesCategory == null) {
                return Enumerable.Empty<VirtualPartsSalesPrice>();
            }
            var company = ObjectContext.Companies.SingleOrDefault(r => r.Id == customerId && r.Status == (int)DcsMasterDataStatus.有效);
            if(company == null) {
                return Enumerable.Empty<VirtualPartsSalesPrice>();
            }
            var partsBranches = ObjectContext.PartsBranches.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.IsSalable == true && partIds.Contains(r.PartId) && r.PartsSalesCategoryId == partsSalesCategoryId);
            var partsInfoes = from partBranch in partsBranches
                              from sparePart in ObjectContext.SpareParts
                              from partsSalesPrice in ObjectContext.PartsSalesPrices
                              where sparePart.Id == partBranch.PartId && sparePart.Status == (int)DcsMasterDataStatus.有效
                              && partsSalesPrice.Status == (int)DcsBaseDataStatus.有效 && partsSalesPrice.SparePartId == partBranch.PartId && partsSalesPrice.PartsSalesCategoryId == partsSalesCategoryId
                              select new {
                                  sparePart.Id,
                                  sparePart.Code,
                                  sparePart.Name,
                                  sparePart.ReferenceCode,
                                  sparePart.ReferenceName,
                                  sparePart.Feature,
                                  sparePart.MeasureUnit,
                                  partBranch.MinSaleQuantity,
                                  partsSalesPrice.PriceType,
                                  partsSalesPrice.SalesPrice,
                                  partsSalesPrice.CenterPrice
                              };
            var date = DateTime.Now.Date;
            var partsSpecialTreatyPrices = ObjectContext.PartsSpecialTreatyPrices.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.CompanyId == customerId && r.PartsSalesCategoryId == partsSalesCategoryId && partIds.Contains(r.SparePartId) && r.ValidationTime <= date && r.ExpireTime >= date);
            var dealerPartsSalesPrices = ObjectContext.DealerPartsSalesPrices.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效 && partIds.Contains(r.SparePartId));
            var partsRetailGuidePrices = ObjectContext.PartsRetailGuidePrices.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效 && partIds.Contains(r.SparePartId)); //配件零售指导价
            var partsBranchs = this.ObjectContext.PartsBranches.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId && r.Status == (int)DcsMasterDataStatus.有效 && partIds.Contains(r.PartId));
            var partsSalePriceIncreaseRates = this.ObjectContext.PartsSalePriceIncreaseRates.Where(r => r.status == (int)DcsBaseDataStatus.有效);
            var abcStrategys = this.ObjectContext.KeyValueItems.Where(r => r.Name == "ABCStrategy_Category" && r.Status == (int)DcsMasterDataStatus.有效);
            var result = from parts in ObjectContext.SpareParts.Where(r => partIds.Contains(r.Id))
                         join partsInfo in partsInfoes on parts.Id equals partsInfo.Id into tempTable3
                         from t3 in tempTable3.DefaultIfEmpty()
                         join partsSpecialTreatyPrice in partsSpecialTreatyPrices.Where(r => r.CompanyId == customerId) on parts.Id equals partsSpecialTreatyPrice.SparePartId into tempTable1
                         from t1 in tempTable1.DefaultIfEmpty()
                         join dealerPartsSalesPrice in dealerPartsSalesPrices on parts.Id equals dealerPartsSalesPrice.SparePartId into tempTable2
                         from t2 in tempTable2.DefaultIfEmpty()
                         join partsRetailGuidePrice in partsRetailGuidePrices on parts.Id equals partsRetailGuidePrice.SparePartId into tempTable5
                         from t5 in tempTable5.DefaultIfEmpty()
                         join partsBranch in partsBranchs on parts.Id equals partsBranch.PartId into tempTable4
                         from t4 in tempTable4.DefaultIfEmpty()
                         join partsSalePriceIncreaseRate in partsSalePriceIncreaseRates on t4.IncreaseRateGroupId equals partsSalePriceIncreaseRate.Id into tempTable6
                         from t6 in tempTable6.DefaultIfEmpty()
                         join abcStrategy in abcStrategys on t4.PartABC equals abcStrategy.Key into tempTable7
                         from t7 in tempTable7.DefaultIfEmpty()
                         select new VirtualPartsSalesPrice {
                             SparePartId = parts.Id,
                             SparePartCode = parts.Code,
                             SparePartName = parts.Name,
                             ReferenceCode = parts.ReferenceCode,
                             ReferenceName = parts.ReferenceName,
                             Feature = parts.Feature,
                             MinSaleQuantity = t3.MinSaleQuantity,
                             PriceType = t3.PriceType,
                             Coefficient = 1,
                             PartsSalesPrice = t3.SalesPrice,
                             CenterPrice = t3.CenterPrice,
                             PartsTreatyPrice = t1.TreatyPrice,
                             MeasureUnit = parts.MeasureUnit,
                             DealerSalesPrice = t2 != null ? t2.DealerSalesPrice : 0,
                             Price = t1 != null ? t1.TreatyPrice : t3.SalesPrice,
                             RetailGuidePrice = t5 != null ? t5.RetailGuidePrice : 0,
                             PriceTypeName = t6 != null ? t6.GroupName : "",
                             ABCStrategy = t7 != null ? t7.Value : "",
                             MInPackingAmount = parts.MInPackingAmount,
                             SalesPrice = t3.SalesPrice
                         };
            var resultArray = result.ToArray();
            foreach(var item in resultArray) {
                if(priceType == (int)DcsExportCustPriceType.中心库价) {
                    item.Price = item.CenterPrice ?? 0;
                }
                if(priceType == (int)DcsExportCustPriceType.服务站价) {
                    item.Price = item.PartsSalesPrice;
                }
                if(priceType == (int)DcsExportCustPriceType.建议售价) {
                    item.Price = item.RetailGuidePrice;
                }
                if(priceType == (int)DcsExportCustPriceType.特殊协议价) {
                    item.Price = item.PartsTreatyPrice ?? 0;
                }
                if(priceType == (int)DcsExportCustPriceType.其他) {
                    item.Price = 0;
                }
                item.Remark = Enum.GetName(typeof(DcsPartsSalesPricePriceType), item.PriceType);
                item.ReferenceCode = item.ReferenceCode ?? string.Empty;
                item.ReferenceName = item.ReferenceName ?? string.Empty;
            }
            return resultArray.OrderBy(r => r.SparePartId);
        }

        public IEnumerable<VirtualPartsSalesPrice> 根据销售类型查询配件销售价(int partsSalesCategoryId, int? orderTypeId, int customerId, bool? ifDirectProvision) {
            var partsSalesCategory = this.ObjectContext.PartsSalesCategories.SingleOrDefault(r => r.Id == partsSalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效);
            if(partsSalesCategory == null) {
                return null;
            }
            var company = this.ObjectContext.Companies.SingleOrDefault(r => r.Id == customerId && r.Status == (int)DcsMasterDataStatus.有效);
            if(company == null) {
                return null;
            }
            var partsBranches = this.ObjectContext.PartsBranches.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.IsSalable == true);
            var customerDirectSpareLists = this.ObjectContext.CustomerDirectSpareLists.Where(r => r.CustomerId == customerId && r.PartsSalesCategoryId == partsSalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效); //客户直供配件清单
            if(ifDirectProvision.HasValue && ifDirectProvision.Value) {
                partsBranches = partsBranches.Where(r => r.IsDirectSupply == ifDirectProvision.Value && r.PartsSalesCategoryId == partsSalesCategoryId && customerDirectSpareLists.Any(c => c.SparePartId == r.PartId));
            }
            if(ifDirectProvision.HasValue && !ifDirectProvision.Value) {
                partsBranches = partsBranches.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId && (r.IsDirectSupply == ifDirectProvision.Value || (r.IsDirectSupply && !customerDirectSpareLists.Any(c => c.SparePartId == r.PartId))));
            }
            IQueryable<SparePart> spareParts = this.ObjectContext.SpareParts;
            if(orderTypeId.HasValue) {
                var orderType = this.ObjectContext.PartsSalesOrderTypes.FirstOrDefault(r => r.Id == orderTypeId && r.Name == "油品订单");
                if(orderType != null)
                    spareParts = spareParts.Where(r => r.GoldenTaxClassifyCode == "1");
            }
            var partsInfoes = from partBranch in partsBranches
                              from sparePart in spareParts
                              from partsSalesPrice in ObjectContext.PartsSalesPrices
                              where sparePart.Id == partBranch.PartId && sparePart.Status == (int)DcsMasterDataStatus.有效 && partsSalesPrice.PartsSalesCategoryId == partBranch.PartsSalesCategoryId
                              && partsSalesPrice.Status == (int)DcsBaseDataStatus.有效 && partsSalesPrice.SparePartId == partBranch.PartId && partsSalesPrice.PartsSalesCategoryId == partsSalesCategoryId
                              select new {
                                  sparePart.Id,
                                  sparePart.Code,
                                  sparePart.Name,
                                  sparePart.ReferenceCode,
                                  sparePart.ReferenceName,
                                  sparePart.Feature,
                                  partBranch.MinSaleQuantity,
                                  partsSalesPrice.PriceType,
                                  partsSalesPrice.SalesPrice,
                                  partsSalesPrice.CenterPrice,
                                  sparePart.MeasureUnit,
                                  sparePart.MInPackingAmount
                              };
            var date = DateTime.Now.Date;
            var partsSpecialTreatyPrices = ObjectContext.PartsSpecialTreatyPrices.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.CompanyId == customerId && r.PartsSalesCategoryId == partsSalesCategoryId && r.ValidationTime <= date && r.ExpireTime >= date);
            var dealerPartsSalesPrices = ObjectContext.DealerPartsSalesPrices.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效);
            var partsRetailGuidePrices = ObjectContext.PartsRetailGuidePrices.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效); //配件零售指导价
            var customerOrderPriceGrade = ObjectContext.CustomerOrderPriceGrades.SingleOrDefault(c => c.PartsSalesCategoryId == partsSalesCategoryId && c.PartsSalesOrderTypeId == orderTypeId && c.CustomerCompanyId == customerId && c.Status == (int)DcsVehicleBaseDataStatus.有效);
            var partsBranchs = this.ObjectContext.PartsBranches.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId && r.Status == (int)DcsMasterDataStatus.有效);
            var partsSalePriceIncreaseRates = this.ObjectContext.PartsSalePriceIncreaseRates.Where(r => r.status == (int)DcsBaseDataStatus.有效);
            var abcStrategys = this.ObjectContext.KeyValueItems.Where(r => r.Name == "ABCStrategy_Category" && r.Status == (int)DcsMasterDataStatus.有效);
            var partsSalesOrderType = this.ObjectContext.PartsSalesOrderTypes.FirstOrDefault(r => r.Id == orderTypeId); //获取订单类型
            var result = (from partsInfo in partsInfoes
                          join partsSpecialTreatyPrice in partsSpecialTreatyPrices.Where(r => r.CompanyId == customerId) on partsInfo.Id equals partsSpecialTreatyPrice.SparePartId into tempTable1
                          from t1 in tempTable1.DefaultIfEmpty()
                          join dealerPartsSalesPrice in dealerPartsSalesPrices on partsInfo.Id equals dealerPartsSalesPrice.SparePartId into tempTable2
                          from t2 in tempTable2.DefaultIfEmpty()
                          join partsRetailGuidePrice in partsRetailGuidePrices on partsInfo.Id equals partsRetailGuidePrice.SparePartId into tempTable3
                          from t3 in tempTable3.DefaultIfEmpty()
                          join partsBranch in partsBranchs on partsInfo.Id equals partsBranch.PartId into tempTable4
                          from t4 in tempTable4.DefaultIfEmpty()
                          join partsSalePriceIncreaseRate in partsSalePriceIncreaseRates on t4.IncreaseRateGroupId equals partsSalePriceIncreaseRate.Id into tempTable5
                          from t5 in tempTable5.DefaultIfEmpty()
                          join abcStrategy in abcStrategys on t4.PartABC equals abcStrategy.Key into tempTable6
                          from t6 in tempTable6.DefaultIfEmpty()
                          join pbpp in ObjectContext.PartsBranchPackingProps on new {
                              Id = t4.Id,
                              PackingType = t4.MainPackingType,
                              SparePartId = t4.PartId
                          } equals new {
                              Id = pbpp.PartsBranchId.Value,
                              PackingType = pbpp.PackingType,
                              SparePartId = pbpp.SparePartId.Value
                          } into tempTable7
                          from t7 in tempTable7.DefaultIfEmpty()
                          select new VirtualPartsSalesPrice {
                              SparePartId = partsInfo.Id,
                              SparePartCode = partsInfo.Code,
                              SparePartName = partsInfo.Name,
                              ReferenceCode = partsInfo.ReferenceCode,
                              ReferenceName = partsInfo.ReferenceName,
                              Feature = partsInfo.Feature,
                              MinSaleQuantity = partsInfo.MinSaleQuantity,
                              PriceType = partsInfo.PriceType,
                              Coefficient = 1,
                              PartsSalesPrice = partsInfo.SalesPrice,
                              PartsTreatyPrice = t1.TreatyPrice,
                              DealerSalesPrice = t2 != null ? t2.DealerSalesPrice : 0,
                              Price = t1 != null ? t1.TreatyPrice : partsInfo.SalesPrice,
                              MeasureUnit = partsInfo.MeasureUnit,
                              CenterPrice = partsInfo.CenterPrice,
                              RetailGuidePrice = t3 != null ? t3.RetailGuidePrice : 0,
                              PriceTypeName = t5 != null ? t5.GroupName : "",
                              ABCStrategy = t6 != null ? t6.Value : "",
                              MInPackingAmount = partsSalesOrderType.Name.Equals("正常订单") ? partsInfo.MInPackingAmount : t7.PackingCoefficient
                              //Remark = t1 != null ? "协议价" : "-1"
                          }).ToArray();

            foreach(var item in result) {
                if(item.PartsTreatyPrice == null)
                    item.Remark = "-1";
                else
                    item.Remark = "协议价";
                if(company.Type == (int)DcsCompanyType.代理库 && item.PartsTreatyPrice == null) {
                    item.Price = item.CenterPrice ?? 0;
                }
                if((company.Type == (int)DcsCompanyType.服务站 || company.Type == (int)DcsCompanyType.服务站兼代理库) && item.PartsTreatyPrice == null) {
                    item.Price = item.PartsSalesPrice;
                }
                if(company.Type == (int)DcsCompanyType.集团企业 && partsSalesOrderType != null && item.PartsTreatyPrice == null) {
                    if(partsSalesOrderType.Name != "三包垫件" && partsSalesOrderType.Name != "6K1")
                        item.Price = item.RetailGuidePrice;
                    else
                        item.Price = item.PartsSalesPrice;
                }
                if(item.Remark != "协议价") {
                    if(customerOrderPriceGrade != null) {
                        item.Coefficient = customerOrderPriceGrade.Coefficient;
                    }
                    item.Price = Math.Round((item.Price * (decimal)item.Coefficient), 2);
                    item.Remark = Enum.GetName(typeof(DcsPartsSalesPricePriceType), item.PriceType);
                }
                item.ReferenceCode = item.ReferenceCode ?? string.Empty;
                item.ReferenceName = item.ReferenceName ?? string.Empty;

            }
            return result.OrderBy(r => r.SparePartId);
        }

        [Query(HasSideEffects = true)]
        public IEnumerable<VirtualPartsSalesPrice> 根据销售类型查询配件销售价2(int partsSalesCategoryId, int? orderTypeId, int customerId, bool? ifDirectProvision, int[] spaePartsIds, int salesUnitOwnerCompanyId, int warehouseId) {
            var partsSalesCategory = this.ObjectContext.PartsSalesCategories.SingleOrDefault(r => r.Id == partsSalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效);
            if(partsSalesCategory == null) {
                return null;
            }
            var companyAndsalesUnitOwnerCompany = this.ObjectContext.Companies.Where(r => (r.Id == customerId || r.Id == salesUnitOwnerCompanyId) && r.Status == (int)DcsMasterDataStatus.有效);
            var company = companyAndsalesUnitOwnerCompany.SingleOrDefault(r => r.Id == customerId);
            var salesUnitOwnerCompany = companyAndsalesUnitOwnerCompany.FirstOrDefault(r => r.Id == salesUnitOwnerCompanyId);
            if(company == null) {
                return null;
            }
            var partsBranches = this.ObjectContext.PartsBranches.Where(r => r.Status == (int)DcsBaseDataStatus.有效);
            if(salesUnitOwnerCompany != null && salesUnitOwnerCompany.Type == (int)DcsCompanyType.代理库) {
                var partsStocks = ObjectContext.PartsStocks.Where(r => r.StorageCompanyId == salesUnitOwnerCompanyId && r.WarehouseId == warehouseId && spaePartsIds.Contains(r.PartId) && ObjectContext.WarehouseAreaCategories.Any(v => v.Id == r.WarehouseAreaCategoryId && (v.Category == (int)DcsAreaType.保管区)));
                var returnpartsStocks = partsStocks.GroupBy(r => new {
                    r.WarehouseId,
                    r.StorageCompanyId,
                    r.BranchId,
                    r.PartId
                }).Select(v => new {
                    v.Key.WarehouseId,
                    v.Key.StorageCompanyId,
                    v.Key.BranchId,
                    v.Key.PartId,
                    sumQuantity = v.Sum(r => r.Quantity)
                });
                var stockrestuls = from partsStock in returnpartsStocks
                                   join partsLockedStock in ObjectContext.PartsLockedStocks on new {
                                       pId = partsStock.PartId,
                                       cId = partsStock.StorageCompanyId,
                                       wId = partsStock.WarehouseId
                                   } equals new {
                                       pId = partsLockedStock.PartId,
                                       cId = partsLockedStock.StorageCompanyId,
                                       wId = partsLockedStock.WarehouseId
                                   } into details
                                   from v in details.DefaultIfEmpty()
                                   select new {
                                       partsStock.WarehouseId,
                                       partsStock.PartId,
                                       UsableQuantity = partsStock.sumQuantity - (v == null ? 0 : v.LockedQuantity),
                                   };
                partsBranches = partsBranches.Where(r => r.IsSalable == true || (r.IsSalable == false && stockrestuls.Any(d => d.PartId == r.PartId && d.UsableQuantity > 0)));
            } else {
                partsBranches = partsBranches.Where(r => r.IsSalable == true);
            }

            var customerDirectSpareLists = this.ObjectContext.CustomerDirectSpareLists.Where(r => r.CustomerId == customerId && r.PartsSalesCategoryId == partsSalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效); //客户直供配件清单

            if(ifDirectProvision.HasValue && ifDirectProvision.Value) {
                partsBranches = partsBranches.Where(r => r.IsDirectSupply == ifDirectProvision.Value && r.PartsSalesCategoryId == partsSalesCategoryId && customerDirectSpareLists.Any(c => c.SparePartId == r.PartId));
            }
            if(ifDirectProvision.HasValue && !ifDirectProvision.Value) {
                partsBranches = partsBranches.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId && (r.IsDirectSupply == ifDirectProvision.Value || (r.IsDirectSupply && !customerDirectSpareLists.Any(c => c.SparePartId == r.PartId))));
            }

            IQueryable<SparePart> spareParts = this.ObjectContext.SpareParts;
            if(orderTypeId.HasValue) {
                var orderType = this.ObjectContext.PartsSalesOrderTypes.FirstOrDefault(r => r.Id == orderTypeId && r.Name == "油品订单");
                if(orderType != null)
                    spareParts = spareParts.Where(r => r.GoldenTaxClassifyCode == "1");
            }
            if(spaePartsIds != null && spaePartsIds.Length > 0) {
                spareParts = spareParts.Where(r => spaePartsIds.Contains(r.Id));
            }
            var partsInfoes = from partBranch in partsBranches
                              from sparePart in spareParts
                              from partsSalesPrice in ObjectContext.PartsSalesPrices
                              where sparePart.Id == partBranch.PartId && sparePart.Status == (int)DcsMasterDataStatus.有效 && partsSalesPrice.PartsSalesCategoryId == partBranch.PartsSalesCategoryId
                              && partsSalesPrice.Status == (int)DcsBaseDataStatus.有效 && partsSalesPrice.SparePartId == partBranch.PartId && partsSalesPrice.PartsSalesCategoryId == partsSalesCategoryId
                              select new {
                                  sparePart.Id,
                                  sparePart.Code,
                                  sparePart.Name,
                                  partsSalesPrice.PriceType,
                                  partsSalesPrice.SalesPrice,
                                  partsSalesPrice.CenterPrice,
                                  sparePart.MeasureUnit,
                                  sparePart.MInPackingAmount
                              };
            var date = DateTime.Now.Date;
            var partsSpecialTreatyPrices = ObjectContext.PartsSpecialTreatyPrices.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.CompanyId == customerId && r.PartsSalesCategoryId == partsSalesCategoryId && r.ValidationTime <= date && r.ExpireTime >= date);
            var dealerPartsSalesPrices = ObjectContext.DealerPartsSalesPrices.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效);
            var partsRetailGuidePrices = ObjectContext.PartsRetailGuidePrices.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效); //配件零售指导价
            var customerOrderPriceGrade = ObjectContext.CustomerOrderPriceGrades.SingleOrDefault(c => c.PartsSalesCategoryId == partsSalesCategoryId && c.PartsSalesOrderTypeId == orderTypeId && c.CustomerCompanyId == customerId && c.Status == (int)DcsVehicleBaseDataStatus.有效);
            var partsBranchs = this.ObjectContext.PartsBranches.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId && r.Status == (int)DcsMasterDataStatus.有效);
            var partsSalePriceIncreaseRates = this.ObjectContext.PartsSalePriceIncreaseRates.Where(r => r.status == (int)DcsBaseDataStatus.有效);
            var abcStrategys = this.ObjectContext.KeyValueItems.Where(r => r.Name == "ABCStrategy_Category" && r.Status == (int)DcsMasterDataStatus.有效);
            var result = (from partsInfo in partsInfoes
                          join partsSpecialTreatyPrice in partsSpecialTreatyPrices.Where(r => r.CompanyId == customerId) on partsInfo.Id equals partsSpecialTreatyPrice.SparePartId into tempTable1
                          from t1 in tempTable1.DefaultIfEmpty()
                          join dealerPartsSalesPrice in dealerPartsSalesPrices on partsInfo.Id equals dealerPartsSalesPrice.SparePartId into tempTable2
                          from t2 in tempTable2.DefaultIfEmpty()
                          join partsRetailGuidePrice in partsRetailGuidePrices on partsInfo.Id equals partsRetailGuidePrice.SparePartId into tempTable3
                          from t3 in tempTable3.DefaultIfEmpty()
                          join partsBranch in partsBranchs on partsInfo.Id equals partsBranch.PartId into tempTable4
                          from t4 in tempTable4.DefaultIfEmpty()
                          join partsSalePriceIncreaseRate in partsSalePriceIncreaseRates on t4.IncreaseRateGroupId equals partsSalePriceIncreaseRate.Id into tempTable5
                          from t5 in tempTable5.DefaultIfEmpty()
                          join abcStrategy in abcStrategys on t4.PartABC equals abcStrategy.Key into tempTable6
                          from t6 in tempTable6.DefaultIfEmpty()
                          select new VirtualPartsSalesPrice {
                              SparePartId = partsInfo.Id,
                              SparePartCode = partsInfo.Code,
                              SparePartName = partsInfo.Name,
                              PriceType = partsInfo.PriceType,
                              Coefficient = 1,
                              PartsSalesPrice = partsInfo.SalesPrice,
                              PartsTreatyPrice = t1.TreatyPrice,
                              DealerSalesPrice = t2 != null ? t2.DealerSalesPrice : 0,
                              Price = t1 != null ? t1.TreatyPrice : partsInfo.SalesPrice,
                              CenterPrice = partsInfo.CenterPrice,
                              RetailGuidePrice = t3 != null ? t3.RetailGuidePrice : 0,
                              MeasureUnit = partsInfo.MeasureUnit,
                              PriceTypeName = t5 != null ? t5.GroupName : "",
                              ABCStrategy = t6 != null ? t6.Value : "",
                              MInPackingAmount = partsInfo.MInPackingAmount,
                              SalesPrice = partsInfo.SalesPrice
                              //Remark = t1 != null ? "协议价" : "-1"
                          }).ToArray();
            var partsSalesOrderType = this.ObjectContext.PartsSalesOrderTypes.FirstOrDefault(r => r.Id == orderTypeId); //获取订单类型
            foreach(var item in result) {
                if(item.PartsTreatyPrice == null)
                    item.Remark = "-1";
                else
                    item.Remark = "协议价";
                if(company.Type == (int)DcsCompanyType.代理库 && item.PartsTreatyPrice == null) {
                    item.Price = item.CenterPrice ?? 0;
                }
                if((company.Type == (int)DcsCompanyType.服务站 || company.Type == (int)DcsCompanyType.服务站兼代理库) && item.PartsTreatyPrice == null) {
                    item.Price = item.PartsSalesPrice;
                }
                if(company.Type == (int)DcsCompanyType.集团企业 && partsSalesOrderType != null && item.PartsTreatyPrice == null) {
                    if(partsSalesOrderType.Name != "三包垫件" && partsSalesOrderType.Name != "6K1" && partsSalesOrderType.Name != "集团订单")
                        item.Price = item.RetailGuidePrice;
                    else
                        item.Price = item.PartsSalesPrice;
                }
                if(item.Remark != "协议价") {
                    if(customerOrderPriceGrade != null) {
                        item.Coefficient = customerOrderPriceGrade.Coefficient;
                    }
                    if(partsSalesOrderType.Name != "三包垫件" && partsSalesOrderType.Name != "6K1")
                        item.Price = Math.Round((item.Price * (decimal)item.Coefficient), 2);
                    item.Remark = Enum.GetName(typeof(DcsPartsSalesPricePriceType), item.PriceType);
                }
            }
            return result.OrderBy(r => r.SparePartId);
        }

        public IEnumerable<VirtualPartsSalesPrice> 根据销售类型查询配件销售价3(int partsSalesCategoryId, int? orderTypeId, int customerId, bool? ifDirectProvision, int salesUnitOwnerCompanyId, int warehouseId, string sparePartCode, string sparePartName, string referenceCode, string referenceName) {
            var partsSalesCategory = this.ObjectContext.PartsSalesCategories.SingleOrDefault(r => r.Id == partsSalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效);
            if(partsSalesCategory == null) {
                return null;
            }
            if(customerId == 0 || salesUnitOwnerCompanyId == 0)
                return null;
            var companyAndsalesUnitOwnerCompany = this.ObjectContext.Companies.Where(r => (r.Id == customerId || r.Id == salesUnitOwnerCompanyId) && r.Status == (int)DcsMasterDataStatus.有效);
            var company = companyAndsalesUnitOwnerCompany.SingleOrDefault(r => r.Id == customerId);
            var salesUnitOwnerCompany = companyAndsalesUnitOwnerCompany.FirstOrDefault(r => r.Id == salesUnitOwnerCompanyId);
            if(company == null) {
                return null;
            }
            var partsBranches = this.ObjectContext.PartsBranches.Where(r => r.Status == (int)DcsBaseDataStatus.有效);
            if(salesUnitOwnerCompany != null && salesUnitOwnerCompany.Type == (int)DcsCompanyType.代理库) {
                var partIds = partsBranches.Select(r => r.PartId);
                var partsStocks = ObjectContext.PartsStocks.Where(r => r.StorageCompanyId == salesUnitOwnerCompanyId && r.WarehouseId == warehouseId && partIds.Contains(r.PartId) && ObjectContext.WarehouseAreaCategories.Any(v => v.Id == r.WarehouseAreaCategoryId && (v.Category == (int)DcsAreaType.保管区)));
                var returnpartsStocks = partsStocks.GroupBy(r => new {
                    r.WarehouseId,
                    r.StorageCompanyId,
                    r.BranchId,
                    r.PartId
                }).Select(v => new {
                    v.Key.WarehouseId,
                    v.Key.StorageCompanyId,
                    v.Key.BranchId,
                    v.Key.PartId,
                    sumQuantity = v.Sum(r => r.Quantity)
                });
                var stockrestuls = from partsStock in returnpartsStocks
                                   join partsLockedStock in ObjectContext.PartsLockedStocks on new {
                                       pId = partsStock.PartId,
                                       cId = partsStock.StorageCompanyId,
                                       wId = partsStock.WarehouseId
                                   } equals new {
                                       pId = partsLockedStock.PartId,
                                       cId = partsLockedStock.StorageCompanyId,
                                       wId = partsLockedStock.WarehouseId
                                   } into details
                                   from v in details.DefaultIfEmpty()
                                   select new {
                                       partsStock.WarehouseId,
                                       partsStock.PartId,
                                       UsableQuantity = partsStock.sumQuantity - (v == null ? 0 : v.LockedQuantity),
                                   };
                partsBranches = partsBranches.Where(r => r.IsSalable == true || (r.IsSalable == false && stockrestuls.Any(d => d.PartId == r.PartId && d.UsableQuantity > 0)));
            } else {
                partsBranches = partsBranches.Where(r => r.IsSalable == true);
            }

            var customerDirectSpareLists = this.ObjectContext.CustomerDirectSpareLists.Where(r => r.CustomerId == customerId && r.PartsSalesCategoryId == partsSalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效); //客户直供配件清单
            if(ifDirectProvision.HasValue && ifDirectProvision.Value) {
                partsBranches = partsBranches.Where(r => r.IsDirectSupply == ifDirectProvision.Value && r.PartsSalesCategoryId == partsSalesCategoryId && customerDirectSpareLists.Any(c => c.SparePartId == r.PartId));
            }
            if(ifDirectProvision.HasValue && !ifDirectProvision.Value) {
                partsBranches = partsBranches.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId && (r.IsDirectSupply == ifDirectProvision.Value || (r.IsDirectSupply && !customerDirectSpareLists.Any(c => c.SparePartId == r.PartId))));
            }
            IQueryable<SparePart> spareParts = this.ObjectContext.SpareParts;
            if(!string.IsNullOrEmpty(sparePartCode))
                spareParts = spareParts.Where(r => r.Code.Contains(sparePartCode));
            if(!string.IsNullOrEmpty(sparePartName))
                spareParts = spareParts.Where(r => r.Name.Contains(sparePartName));
            if(!string.IsNullOrEmpty(referenceCode))
                spareParts = spareParts.Where(r => r.ReferenceCode.Contains(referenceCode));
            if(!string.IsNullOrEmpty(referenceName))
                spareParts = spareParts.Where(r => r.ReferenceName.Contains(referenceName));
            if(orderTypeId.HasValue) {
                var orderType = this.ObjectContext.PartsSalesOrderTypes.FirstOrDefault(r => r.Id == orderTypeId && r.Name == "油品订单");
                if(orderType != null)
                    spareParts = spareParts.Where(r => r.GoldenTaxClassifyCode == "1");
            }
            var partsInfoes = from partBranch in partsBranches
                              from sparePart in spareParts
                              from partsSalesPrice in ObjectContext.PartsSalesPrices
                              where sparePart.Id == partBranch.PartId && sparePart.Status == (int)DcsMasterDataStatus.有效 && partsSalesPrice.PartsSalesCategoryId == partBranch.PartsSalesCategoryId
                              && partsSalesPrice.Status == (int)DcsBaseDataStatus.有效 && partsSalesPrice.SparePartId == partBranch.PartId && partsSalesPrice.PartsSalesCategoryId == partsSalesCategoryId
                              select new {
                                  sparePart.Id,
                                  sparePart.Code,
                                  sparePart.Name,
                                  sparePart.ReferenceCode,
                                  sparePart.ReferenceName,
                                  sparePart.Feature,
                                  partBranch.MinSaleQuantity,
                                  partsSalesPrice.PriceType,
                                  partsSalesPrice.SalesPrice,
                                  partsSalesPrice.CenterPrice,
                                  sparePart.MeasureUnit,
                                  sparePart.MInPackingAmount
                              };
            var date = DateTime.Now.Date;
            var partsSpecialTreatyPrices = ObjectContext.PartsSpecialTreatyPrices.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.CompanyId == customerId && r.PartsSalesCategoryId == partsSalesCategoryId && r.ValidationTime <= date && r.ExpireTime >= date);
            var dealerPartsSalesPrices = ObjectContext.DealerPartsSalesPrices.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效);
            var partsRetailGuidePrices = ObjectContext.PartsRetailGuidePrices.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效); //配件零售指导价
            var customerOrderPriceGrade = ObjectContext.CustomerOrderPriceGrades.SingleOrDefault(c => c.PartsSalesCategoryId == partsSalesCategoryId && c.PartsSalesOrderTypeId == orderTypeId && c.CustomerCompanyId == customerId && c.Status == (int)DcsVehicleBaseDataStatus.有效);
            var partsBranchs = this.ObjectContext.PartsBranches.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId && r.Status == (int)DcsMasterDataStatus.有效);
            var partsSalePriceIncreaseRates = this.ObjectContext.PartsSalePriceIncreaseRates.Where(r => r.status == (int)DcsBaseDataStatus.有效);
            var abcStrategys = this.ObjectContext.KeyValueItems.Where(r => r.Name == "ABCStrategy_Category" && r.Status == (int)DcsMasterDataStatus.有效);
            var partsSalesOrderType = this.ObjectContext.PartsSalesOrderTypes.FirstOrDefault(r => r.Id == orderTypeId); //获取订单类型
            var result = (from partsInfo in partsInfoes
                          join partsSpecialTreatyPrice in partsSpecialTreatyPrices.Where(r => r.CompanyId == customerId) on partsInfo.Id equals partsSpecialTreatyPrice.SparePartId into tempTable1
                          from t1 in tempTable1.DefaultIfEmpty()
                          join dealerPartsSalesPrice in dealerPartsSalesPrices on partsInfo.Id equals dealerPartsSalesPrice.SparePartId into tempTable2
                          from t2 in tempTable2.DefaultIfEmpty()
                          join partsRetailGuidePrice in partsRetailGuidePrices on partsInfo.Id equals partsRetailGuidePrice.SparePartId into tempTable3
                          from t3 in tempTable3.DefaultIfEmpty()
                          join partsBranch in partsBranchs on partsInfo.Id equals partsBranch.PartId into tempTable4
                          from t4 in tempTable4.DefaultIfEmpty()
                          join partsSalePriceIncreaseRate in partsSalePriceIncreaseRates on t4.IncreaseRateGroupId equals partsSalePriceIncreaseRate.Id into tempTable5
                          from t5 in tempTable5.DefaultIfEmpty()
                          join abcStrategy in abcStrategys on t4.PartABC equals abcStrategy.Key into tempTable6
                          from t6 in tempTable6.DefaultIfEmpty()
                          join pbpp in ObjectContext.PartsBranchPackingProps on new {
                              Id = t4.Id,
                              PackingType = t4.MainPackingType,
                              SparePartId = t4.PartId
                          } equals new {
                              Id = pbpp.PartsBranchId.Value,
                              PackingType = pbpp.PackingType,
                              SparePartId = pbpp.SparePartId.Value
                          } into tempTable7
                          from t7 in tempTable7.DefaultIfEmpty()
                          select new VirtualPartsSalesPrice {
                              SparePartId = partsInfo.Id,
                              SparePartCode = partsInfo.Code,
                              SparePartName = partsInfo.Name,
                              ReferenceCode = partsInfo.ReferenceCode,
                              ReferenceName = partsInfo.ReferenceName,
                              Feature = partsInfo.Feature,
                              MinSaleQuantity = partsInfo.MinSaleQuantity,
                              PriceType = partsInfo.PriceType,
                              Coefficient = 1,
                              PartsSalesPrice = partsInfo.SalesPrice,
                              PartsTreatyPrice = t1.TreatyPrice,
                              DealerSalesPrice = t2 != null ? t2.DealerSalesPrice : 0,
                              Price = t1 != null ? t1.TreatyPrice : partsInfo.SalesPrice,
                              MeasureUnit = partsInfo.MeasureUnit,
                              CenterPrice = partsInfo.CenterPrice,
                              RetailGuidePrice = t3 != null ? t3.RetailGuidePrice : 0,
                              PriceTypeName = t5 != null ? t5.GroupName : "",
                              ABCStrategy = t6 != null ? t6.Value : "",
                              MInPackingAmount = partsSalesOrderType.Name.Equals("正常订单") ? partsInfo.MInPackingAmount : t7.PackingCoefficient
                              //Remark = t1 != null ? "协议价" : "-1"
                          }).ToArray();

            foreach(var item in result) {
                if(item.PartsTreatyPrice == null)
                    item.Remark = "-1";
                else
                    item.Remark = "协议价";
                if(company.Type == (int)DcsCompanyType.代理库 && item.PartsTreatyPrice == null) {
                    item.Price = item.CenterPrice ?? 0;
                }
                if((company.Type == (int)DcsCompanyType.服务站 || company.Type == (int)DcsCompanyType.服务站兼代理库) && item.PartsTreatyPrice == null) {
                    item.Price = item.PartsSalesPrice;
                }
                if(company.Type == (int)DcsCompanyType.集团企业 && partsSalesOrderType != null && item.PartsTreatyPrice == null) {
                    if(partsSalesOrderType.Name != "三包垫件" && partsSalesOrderType.Name != "6K1" && partsSalesOrderType.Name != "集团订单")
                        item.Price = item.RetailGuidePrice;
                    else
                        item.Price = item.PartsSalesPrice;
                }
                if(item.Remark != "协议价") {
                    if(customerOrderPriceGrade != null) {
                        item.Coefficient = customerOrderPriceGrade.Coefficient;
                    }
                    if(partsSalesOrderType.Name != "三包垫件" && partsSalesOrderType.Name != "6K1")
                        item.Price = Math.Round((item.Price * (decimal)item.Coefficient), 2);

                    item.Remark = Enum.GetName(typeof(DcsPartsSalesPricePriceType), item.PriceType);
                }
                item.ReferenceCode = item.ReferenceCode ?? string.Empty;
                item.ReferenceName = item.ReferenceName ?? string.Empty;

            }
            return result.OrderBy(r => r.SparePartId);
        }

        [Query(HasSideEffects = true)]
        public IEnumerable<VirtualPartsSalesPrice> 根据配件清单获取销售价格(int appCategoryId, int customerId, int[] partIds) {
            var partsSalesCategoryId = ObjectContext.PartsSalesOrderTypes.Where(v => v.Id == appCategoryId && v.Status == (int)DcsBaseDataStatus.有效).Select(v => v.PartsSalesCategoryId).SingleOrDefault();

            if(partsSalesCategoryId == 0) {
                return Enumerable.Empty<VirtualPartsSalesPrice>();
            }
            var partsSalesCategory = ObjectContext.PartsSalesCategories.SingleOrDefault(r => r.Id == partsSalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效);
            if(partsSalesCategory == null) {
                return Enumerable.Empty<VirtualPartsSalesPrice>();
            }
            var company = ObjectContext.Companies.SingleOrDefault(r => r.Id == customerId && r.Status == (int)DcsMasterDataStatus.有效);
            if(company == null) {
                return Enumerable.Empty<VirtualPartsSalesPrice>();
            }
            var customerOrderPriceGrade = ObjectContext.CustomerOrderPriceGrades.FirstOrDefault(r => r.PartsSalesOrderTypeId == appCategoryId && r.CustomerCompanyId == customerId && r.Status == (int)DcsBaseDataStatus.有效);
            if(customerOrderPriceGrade == null)
                return Enumerable.Empty<VirtualPartsSalesPrice>();
            var partsBranches = ObjectContext.PartsBranches.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && partIds.Contains(r.PartId) && r.PartsSalesCategoryId == partsSalesCategoryId);
            var coefficient = customerOrderPriceGrade.Coefficient;
            var dcoefficient = (decimal)customerOrderPriceGrade.Coefficient;
            var partsInfoes = from partBranch in partsBranches
                              from sparePart in ObjectContext.SpareParts
                              from partsSalesPrice in ObjectContext.PartsSalesPrices
                              where sparePart.Id == partBranch.PartId && sparePart.Status == (int)DcsMasterDataStatus.有效
                              && partsSalesPrice.Status == (int)DcsBaseDataStatus.有效 && partsSalesPrice.SparePartId == partBranch.PartId && partsSalesPrice.PartsSalesCategoryId == partsSalesCategoryId
                              select new {
                                  sparePart.Id,
                                  sparePart.Code,
                                  sparePart.Name,
                                  sparePart.ReferenceCode,
                                  sparePart.ReferenceName,
                                  sparePart.Feature,
                                  sparePart.MeasureUnit,
                                  partBranch.MinSaleQuantity,
                                  partsSalesPrice.PriceType,
                                  partsSalesPrice.SalesPrice,
                                  partsSalesPrice.CenterPrice
                              };
            var date = DateTime.Now.Date;
            var partsSpecialTreatyPrices = ObjectContext.PartsSpecialTreatyPrices.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.CompanyId == customerId && r.PartsSalesCategoryId == partsSalesCategoryId && partIds.Contains(r.SparePartId) && r.ValidationTime <= date && r.ExpireTime >= date);
            var dealerPartsSalesPrices = ObjectContext.DealerPartsSalesPrices.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效 && partIds.Contains(r.SparePartId));
            var partsRetailGuidePrices = ObjectContext.PartsRetailGuidePrices.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效 && partIds.Contains(r.SparePartId)); //配件零售指导价
            var partsBranchs = this.ObjectContext.PartsBranches.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId && r.Status == (int)DcsMasterDataStatus.有效 && partIds.Contains(r.PartId));
            var partsSalePriceIncreaseRates = this.ObjectContext.PartsSalePriceIncreaseRates.Where(r => r.status == (int)DcsBaseDataStatus.有效);
            var abcStrategys = this.ObjectContext.KeyValueItems.Where(r => r.Name == "ABCStrategy_Category" && r.Status == (int)DcsMasterDataStatus.有效);
            var result = from parts in ObjectContext.SpareParts.Where(r => partIds.Contains(r.Id))
                         join partsInfo in partsInfoes on parts.Id equals partsInfo.Id into tempTable3
                         from t3 in tempTable3.DefaultIfEmpty()
                         join partsSpecialTreatyPrice in partsSpecialTreatyPrices.Where(r => r.CompanyId == customerId) on parts.Id equals partsSpecialTreatyPrice.SparePartId into tempTable1
                         from t1 in tempTable1.DefaultIfEmpty()
                         join dealerPartsSalesPrice in dealerPartsSalesPrices on parts.Id equals dealerPartsSalesPrice.SparePartId into tempTable2
                         from t2 in tempTable2.DefaultIfEmpty()
                         join partsRetailGuidePrice in partsRetailGuidePrices on parts.Id equals partsRetailGuidePrice.SparePartId into tempTable5
                         from t5 in tempTable5.DefaultIfEmpty()
                         join partsBranch in partsBranchs on parts.Id equals partsBranch.PartId into tempTable4
                         from t4 in tempTable4.DefaultIfEmpty()
                         join partsSalePriceIncreaseRate in partsSalePriceIncreaseRates on t4.IncreaseRateGroupId equals partsSalePriceIncreaseRate.Id into tempTable6
                         from t6 in tempTable6.DefaultIfEmpty()
                         join abcStrategy in abcStrategys on t4.PartABC equals abcStrategy.Key into tempTable7
                         from t7 in tempTable7.DefaultIfEmpty()
                         select new VirtualPartsSalesPrice {
                             SparePartId = parts.Id,
                             SparePartCode = parts.Code,
                             SparePartName = parts.Name,
                             ReferenceCode = parts.ReferenceCode,
                             ReferenceName = parts.ReferenceName,
                             Feature = parts.Feature,
                             MinSaleQuantity = t3.MinSaleQuantity,
                             PriceType = t3.PriceType,
                             Coefficient = coefficient,
                             PartsSalesPrice = t3.SalesPrice,
                             CenterPrice = t3.CenterPrice,
                             PartsTreatyPrice = t1.TreatyPrice,
                             MeasureUnit = parts.MeasureUnit,
                             DealerSalesPrice = t2 != null ? t2.DealerSalesPrice : 0,
                             Price = t1 != null ? t1.TreatyPrice : t3.SalesPrice,
                             RetailGuidePrice = t5 != null ? t5.RetailGuidePrice : 0,
                             PriceTypeName = t6 != null ? t6.GroupName : "",
                             ABCStrategy = t7 != null ? t7.Value : "",
                             MInPackingAmount = parts.MInPackingAmount,
                             SalesPrice = t3.SalesPrice
                         };
            var resultArray = result.ToArray();
            var partsSalesOrderType = this.ObjectContext.PartsSalesOrderTypes.FirstOrDefault(r => r.Id == appCategoryId); //获取订单类型
            foreach(var item in resultArray) {
                if(item.PartsTreatyPrice == null)
                    item.Remark = "-1";
                else
                    item.Remark = "协议价";
                if(company.Type == (int)DcsCompanyType.代理库 && item.PartsTreatyPrice == null) {
                    item.Price = item.CenterPrice ?? 0;
                }
                if((company.Type == (int)DcsCompanyType.服务站 || company.Type == (int)DcsCompanyType.服务站兼代理库) && item.PartsTreatyPrice == null) {
                    item.Price = item.PartsSalesPrice;
                }
                if(company.Type == (int)DcsCompanyType.集团企业 && partsSalesOrderType != null && item.PartsTreatyPrice == null) {
                    if(partsSalesOrderType.Name != "三包垫件" && partsSalesOrderType.Name != "6K1")
                        item.Price = item.RetailGuidePrice;
                    else
                        item.Price = item.PartsSalesPrice;
                }
                if(item.Remark != "协议价") {
                    if(customerOrderPriceGrade != null) {
                        item.Coefficient = customerOrderPriceGrade.Coefficient;
                    }
                    item.Price = item.Price * (decimal)item.Coefficient;
                    item.Remark = Enum.GetName(typeof(DcsPartsSalesPricePriceType), item.PriceType);
                }
            }
            return resultArray.OrderBy(r => r.SparePartId);
        }

        public IQueryable<VirtualStructClaimPrice> 根据零售订单返回索赔价(int partsSalesCategoryId, int dealerPartsRetailOrderId) {
            var result = from a in ObjectContext.DealerRetailOrderDetails.Where(r => r.DealerPartsRetailOrderId == dealerPartsRetailOrderId)
                         join b in ObjectContext.PartsBranches.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId) on a.PartsId equals b.PartId
                         join c in ObjectContext.DealerPartsRetailOrders.Where(r => r.Status == (int)DcsWorkflowOfSimpleApprovalStatus.已审核) on a.DealerPartsRetailOrderId equals c.Id
                         select new VirtualStructClaimPrice {
                             PartsSalesOrderId = a.id,
                             PartsWarrantyCategoryId = b == null ? 0 : b.PartsWarrantyCategoryId,
                             PartsWarrantyCategoryName = b.PartsWarrantyCategoryName,
                             SparePartId = a.PartsId,
                             SparePartCode = a.PartsCode,
                             SparePartName = a.PartsName,
                             PartsReturnPolicy = b.PartsReturnPolicy,
                             SupplierId = -1,
                             SalesUnitId = -1,
                             PartsSalesCategoryName = b.PartsSalesCategoryName,
                             PartsSalesCategoryId = c.PartsSalesCategoryId
                         };
            return result;

        }

        public IQueryable<VirtualStructClaimPrice> 根据销售组织和经销商返回索赔价(int branchId, int salesUnitId, int dealerId) {
            var result = from a in ObjectContext.PartsSalesOrderDetails
                         join b in ObjectContext.PartsSalesOrders.Where(r => r.SalesUnitId == salesUnitId && r.InvoiceReceiveCompanyId == dealerId) on a.PartsSalesOrderId equals b.Id
                         join c in ObjectContext.PartsBranches.Where(r => r.BranchId == branchId) on a.SparePartId equals c.PartId
                         //join d in ObjectContext.PartsWarrantyTerms.Where(r => r.PartsWarrantyType == (int)DcsPartsWarrantyTermPartsWarrantyType.销售件保修) on c.PartsWarrantyCategoryId equals d.PartsWarrantyCategoryId
                         //join e in ObjectContext.WarrantyPolicies on d.WarrantyPolicyId equals e.Id
                         //join f in ObjectContext.WarrantyPolicyNServProdLines on e.Id equals f.WarrantyPolicyId
                         //join g in ObjectContext.ServiceProductLines.Where(r => r.Name == "配件索赔") on branchId equals g.BranchId
                         join h in ObjectContext.PartsSupplierRelations on a.SparePartId equals h.PartId
                         join i in ObjectContext.PartsSuppliers on h.SupplierId equals i.Id
                         //join j in ObjectContext.PartsWarrantyCategories on c.PartsWarrantyCategoryId equals j.Id
                         select new VirtualStructClaimPrice {
                             PartsSalesOrderId = b.Id,
                             PartsSalesOrderCode = b.Code,
                             PartsWarrantyCategoryId = c.PartsWarrantyCategoryId,
                             //PartsWarrantyCategoryName = j.Name,
                             SparePartId = c.PartId,
                             SparePartCode = c.PartCode,
                             SparePartName = c.PartName,
                             SupplierId = i.Id,
                             SupplierCode = i.Code,
                             SupplierName = i.Name,
                             SalesPrice = a.OrderPrice,
                             PartsReturnPolicy = c.PartsReturnPolicy,
                             SalesUnitId = b.SalesUnitId,
                             SalesUnitName = b.SalesUnitName,
                             PartsSalesCategoryId = b.SalesCategoryId,
                             PartsSalesCategoryName = b.SalesCategoryName,
                             BranchId = b.BranchId,
                             BranchName = b.BranchName,
                             CreateTime = b.CreateTime
                         };
            return result;
        }

    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               

        public IEnumerable<VirtualPartsSalesPrice> 根据销售类型查询配件销售价出口使用(int partsSalesCategoryId, int? orderTypeId, int customerId, bool? ifDirectProvision, int priceType, int[] spaePartsIds) {
            return new VirtualPartsSalesPriceAch(this).根据销售类型查询配件销售价出口使用(partsSalesCategoryId, orderTypeId, customerId, ifDirectProvision, priceType, spaePartsIds);
        }

        [Query(HasSideEffects = true)]
        public IEnumerable<VirtualPartsSalesPrice> 根据配件清单获取销售价格出口使用(int appCategoryId, int customerId, int[] partIds, int priceType) {
            return new VirtualPartsSalesPriceAch(this).根据配件清单获取销售价格出口使用(appCategoryId, customerId, partIds, priceType);
        }

        public IEnumerable<VirtualPartsSalesPrice> 根据销售类型查询配件销售价(int partsSalesCategoryId, int? orderTypeId, int customerId, bool? ifDirectProvision) {
            return new VirtualPartsSalesPriceAch(this).根据销售类型查询配件销售价(partsSalesCategoryId, orderTypeId, customerId, ifDirectProvision);
        }


        [Query(HasSideEffects = true)]
        public IEnumerable<VirtualPartsSalesPrice> 根据销售类型查询配件销售价2(int partsSalesCategoryId, int? orderTypeId, int customerId, bool? ifDirectProvision, int[] spaePartsIds, int salesUnitOwnerCompanyId, int warehouseId) {
            return new VirtualPartsSalesPriceAch(this).根据销售类型查询配件销售价2(partsSalesCategoryId, orderTypeId, customerId, ifDirectProvision, spaePartsIds, salesUnitOwnerCompanyId, warehouseId);
        }
        [Query(HasSideEffects = true)]
        public IEnumerable<VirtualPartsSalesPrice> 根据销售类型查询配件销售价3(int partsSalesCategoryId, int? orderTypeId, int customerId, bool? ifDirectProvision, int salesUnitOwnerCompanyId, int warehouseId, string sparePartCode, string sparePartName, string referenceCode, string referenceName) {
            return new VirtualPartsSalesPriceAch(this).根据销售类型查询配件销售价3(partsSalesCategoryId, orderTypeId, customerId, ifDirectProvision, salesUnitOwnerCompanyId, warehouseId, sparePartCode, sparePartName, referenceCode, referenceName);
        }

        [Query(HasSideEffects = true)]
        public IEnumerable<VirtualPartsSalesPrice> 根据配件清单获取销售价格(int appCategoryId, int customerId, int[] partIds) {
            return new VirtualPartsSalesPriceAch(this).根据配件清单获取销售价格(appCategoryId, customerId, partIds);
        }

        public IQueryable<VirtualStructClaimPrice> 根据零售订单返回索赔价(int partsSalesCategoryId, int dealerPartsRetailOrderId) {
            return new VirtualPartsSalesPriceAch(this).根据零售订单返回索赔价(partsSalesCategoryId, dealerPartsRetailOrderId);
        }

        public IQueryable<VirtualStructClaimPrice> 根据销售组织和经销商返回索赔价(int branchId, int salesUnitId, int dealerId) {
            return new VirtualPartsSalesPriceAch(this).根据销售组织和经销商返回索赔价(branchId, salesUnitId, dealerId);
        }
    }
}
