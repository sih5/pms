﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class WarehouseSequenceAch : DcsSerivceAchieveBase {
        internal void InsertWarehouseSequenceValidate(WarehouseSequence warehouseSequence) {
            var dbWarehouseSequence = ObjectContext.WarehouseSequences.Where(r => r.PartsSalesCategoryId == warehouseSequence.PartsSalesCategoryId && r.CustomerCompanyId == warehouseSequence.CustomerCompanyId && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbWarehouseSequence != null)
                throw new ValidationException(string.Format("品牌为“{0}”，客户为“{1}” 有效状态的数据已存在", warehouseSequence.PartsSalesCategoryName, warehouseSequence.CustomerCompanyName));
            var userInfo = Utils.GetCurrentUserInfo();
            warehouseSequence.CreatorId = userInfo.Id;
            warehouseSequence.CreatorName = userInfo.Name;
            warehouseSequence.CreateTime = DateTime.Now;
        }
        internal void UpdateWarehouseSequenceValidate(WarehouseSequence warehouseSequence) {
            var dbwarehouseSequence = ObjectContext.WarehouseSequences.Where(r => r.Id != warehouseSequence.Id && r.PartsSalesCategoryId == warehouseSequence.PartsSalesCategoryId && r.CustomerCompanyId == warehouseSequence.CustomerCompanyId && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbwarehouseSequence != null)
                throw new ValidationException(string.Format("品牌为“{0}”，客户为“{1}” 有效状态的数据已存在", warehouseSequence.PartsSalesCategoryName, warehouseSequence.CustomerCompanyName));
            var userInfo = Utils.GetCurrentUserInfo();
            warehouseSequence.ModifierId = userInfo.Id;
            warehouseSequence.ModifierName = userInfo.Name;
            warehouseSequence.ModifyTime = DateTime.Now;
        }
        public void InsertWarehouseSequence(WarehouseSequence warehouseSequence) {
            InsertToDatabase(warehouseSequence);
            this.InsertWarehouseSequenceValidate(warehouseSequence);
        }
        public void UpdateWarehouseSequence(WarehouseSequence warehouseSequence) {
            UpdateToDatabase(warehouseSequence);
            this.UpdateWarehouseSequenceValidate(warehouseSequence);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               

        public void InsertWarehouseSequence(WarehouseSequence warehouseSequence) {
            new WarehouseSequenceAch(this).InsertWarehouseSequence(warehouseSequence);
        }

        public void UpdateWarehouseSequence(WarehouseSequence warehouseSequence) {
            new WarehouseSequenceAch(this).UpdateWarehouseSequence(warehouseSequence);
        }
    }
}
