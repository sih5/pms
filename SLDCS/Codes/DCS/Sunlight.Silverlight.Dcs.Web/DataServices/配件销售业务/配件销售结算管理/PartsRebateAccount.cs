﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsRebateAccountAch : DcsSerivceAchieveBase {
        public PartsRebateAccountAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public PartsRebateAccount 查询客户可用返利(int accountGroupId, int customerCompanyId, int? partsSalesSettlementId) {
            var partsSalesSettlements = ObjectContext.PartsSalesSettlements.Where(r => r.AccountGroupId == accountGroupId && r.CustomerCompanyId == customerCompanyId && r.Status == (int)DcsPartsSalesSettlementStatus.新建);
            if(partsSalesSettlementId.HasValue)
                partsSalesSettlements = partsSalesSettlements.Where(r => r.Id != partsSalesSettlementId);
            var sumRebateAmount = partsSalesSettlements.Sum(r => r.RebateAmount.HasValue ? r.RebateAmount.Value : 0);
            var partsRebateAccount = ObjectContext.PartsRebateAccounts.FirstOrDefault(r => r.AccountGroupId == accountGroupId && r.CustomerCompanyId == customerCompanyId && r.Status == (int)DcsMasterDataStatus.有效);
            if(partsRebateAccount != null)
                partsRebateAccount.UseablePosition = partsRebateAccount.AccountBalanceAmount - sumRebateAmount;
            return partsRebateAccount;
        }

        internal void InsertPartsRebateAccountValidate(PartsRebateAccount partsRebateAccount) {
            var dbPartsRebateAccount = ObjectContext.PartsRebateAccounts.Where(r => r.AccountGroupId == partsRebateAccount.AccountGroupId && r.CustomerCompanyId == partsRebateAccount.CustomerCompanyId && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbPartsRebateAccount != null) {
                throw new ValidationException(ErrorStrings.PartsRebateAccount_Validation1);
            }
            var userInfo = Utils.GetCurrentUserInfo();
            partsRebateAccount.CreatorId = userInfo.Id;
            partsRebateAccount.CreatorName = userInfo.Name;
            partsRebateAccount.CreateTime = DateTime.Now;
        }
        internal void UpdatePartsRebateAccountValidate(PartsRebateAccount partsRebateAccount) {
            var userInfo = Utils.GetCurrentUserInfo();
            partsRebateAccount.ModifierId = userInfo.Id;
            partsRebateAccount.ModifierName = userInfo.Name;
            partsRebateAccount.ModifyTime = DateTime.Now;
        }
        public void InsertPartsRebateAccount(PartsRebateAccount partsRebateAccount) {
            InsertToDatabase(partsRebateAccount);
            this.InsertPartsRebateAccountValidate(partsRebateAccount);
        }
        public void UpdatePartsRebateAccount(PartsRebateAccount partsRebateAccount) {
            UpdateToDatabase(partsRebateAccount);
            this.UpdatePartsRebateAccountValidate(partsRebateAccount);
        }
        public IQueryable<PartsRebateAccount> GetPartsRebateAccountWithDetails() {
            return ObjectContext.PartsRebateAccounts.Include("AccountGroup").Include("Company").Include("Branch").OrderBy(e => e.Id);
        }
        public IQueryable<VirtualPartsRebateAccount> GetVirtualPartsRebateAccountWithDetails(int? branchId, int? accountGroupId, string companyCode, string companyName, DateTime? beginCreateTime, DateTime? endCreateTime, string businessCode) {
            var customerAccountHisDetailsquery = from p in ObjectContext.PartsRebateAccounts
                                                 join b in ObjectContext.Companies on p.CustomerCompanyId equals b.Id
                                                 join h in ObjectContext.Branches on p.BranchId equals h.Id
                                                 join d in ObjectContext.AccountGroups on p.AccountGroupId equals d.Id
                                                 into c
                                                 from accountGroup in c.DefaultIfEmpty()
                                                 join salesUnit in ObjectContext.SalesUnits.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on new {
                                                     accountGroupId = p.AccountGroupId,
                                                     customerCompanyId = accountGroup.SalesCompanyId
                                                 }equals new {
                                                     accountGroupId = salesUnit.AccountGroupId,
                                                     customerCompanyId = salesUnit.OwnerCompanyId
                                                 }into sale
                                                 from salesUnit in sale.DefaultIfEmpty()
                                                 join dealerServiceInfo in ObjectContext.DealerServiceInfoes.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on new {
                                                     customerCompanyId = p.CustomerCompanyId,
                                                     PartsSalesCategoryId = salesUnit.PartsSalesCategoryId
                                                 }equals new {
                                                     customerCompanyId = dealerServiceInfo.DealerId,
                                                     PartsSalesCategoryId = dealerServiceInfo.PartsSalesCategoryId
                                                 }into acc1
                                                 from dealerServiceInfo in acc1.DefaultIfEmpty()
                                                 orderby p.Id
                                                 select new VirtualPartsRebateAccount {
                                                     Id = p.Id,
                                                     BusinessCode =
                                                     b.Type == (int)DcsCompanyType.服务站 || b.Type == (int)DcsCompanyType.服务站兼代理库 ? dealerServiceInfo.BusinessCode :
                                                     b.Type == (int)DcsCompanyType.代理库 ? salesUnit.BusinessCode : null,
                                                     BranchName = h.Name,
                                                     AccountGroupName = accountGroup.Name,
                                                     AccountGroupId = p.AccountGroupId,
                                                     CompanyName = b.Name,
                                                     CompanyCode = b.Code,
                                                     AccountBalanceAmount = p.AccountBalanceAmount,
                                                     CreatorName = p.CreatorName,
                                                     CreateTime = p.CreateTime,
                                                     ModifierName = p.ModifierName,
                                                     ModifyTime = p.ModifyTime,
                                                     Status = p.Status,
                                                     BranchId = p.BranchId
                                                 };
            if(!string.IsNullOrEmpty(companyName)) {
                customerAccountHisDetailsquery = customerAccountHisDetailsquery.Where(r => r.CompanyName.Contains(companyName));
            }
            if(!string.IsNullOrEmpty(companyCode)) {
                customerAccountHisDetailsquery = customerAccountHisDetailsquery.Where(r => r.CompanyCode.Contains(companyCode));
            }
            if(branchId.HasValue) {
                customerAccountHisDetailsquery = customerAccountHisDetailsquery.Where(r => r.BranchId == branchId);
            }
            if(accountGroupId.HasValue) {
                customerAccountHisDetailsquery = customerAccountHisDetailsquery.Where(r => r.AccountGroupId == accountGroupId);
            }
            if(beginCreateTime.HasValue) {
                customerAccountHisDetailsquery = customerAccountHisDetailsquery.Where(r => r.CreateTime >= beginCreateTime);
            }
            if(endCreateTime.HasValue) {
                customerAccountHisDetailsquery = customerAccountHisDetailsquery.Where(r => r.CreateTime <= endCreateTime);
            }
            if(!string.IsNullOrEmpty(businessCode)) {
                customerAccountHisDetailsquery = customerAccountHisDetailsquery.Where(r => r.BusinessCode.Contains(businessCode));
            }
            return customerAccountHisDetailsquery.Distinct().OrderBy(r => r.Id);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               

        public PartsRebateAccount 查询客户可用返利(int accountGroupId, int customerCompanyId, int? partsSalesSettlementId) {
            return new PartsRebateAccountAch(this).查询客户可用返利(accountGroupId,customerCompanyId,partsSalesSettlementId);
        }

        public void InsertPartsRebateAccount(PartsRebateAccount partsRebateAccount) {
            new PartsRebateAccountAch(this).InsertPartsRebateAccount(partsRebateAccount);
        }

        public void UpdatePartsRebateAccount(PartsRebateAccount partsRebateAccount) {
            new PartsRebateAccountAch(this).UpdatePartsRebateAccount(partsRebateAccount);
        }
                public IQueryable<PartsRebateAccount> GetPartsRebateAccountWithDetails() {
            return new PartsRebateAccountAch(this).GetPartsRebateAccountWithDetails();
        }
                public IQueryable<VirtualPartsRebateAccount> GetVirtualPartsRebateAccountWithDetails(int? branchId, int? accountGroupId, string companyCode, string companyName, DateTime? beginCreateTime, DateTime? endCreateTime, string businessCode) {
            return new PartsRebateAccountAch(this).GetVirtualPartsRebateAccountWithDetails(branchId,accountGroupId,companyCode,companyName,beginCreateTime,endCreateTime,businessCode);
        }
    }
}
