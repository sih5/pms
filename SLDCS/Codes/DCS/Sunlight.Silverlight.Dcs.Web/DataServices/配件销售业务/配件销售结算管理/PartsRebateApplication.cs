﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsRebateApplicationAch : DcsSerivceAchieveBase {
        internal void InsertPartsRebateApplicationValidate(PartsRebateApplication partsRebateApplication) {
            if(string.IsNullOrWhiteSpace(partsRebateApplication.Code) || partsRebateApplication.Code == GlobalVar.ASSIGNED_BY_SERVER)
                partsRebateApplication.Code = CodeGenerator.Generate("PartsRebateApplication");
            var userInfo = Utils.GetCurrentUserInfo();
            partsRebateApplication.CreatorId = userInfo.Id;
            partsRebateApplication.CreatorName = userInfo.Name;
            partsRebateApplication.CreateTime = DateTime.Now;
        }
        internal void UpdatePartsRebateApplicationValidate(PartsRebateApplication partsRebateApplication) {
            var dbPartsRebateApplication = ObjectContext.PartsRebateApplications.Where(r => r.Id == partsRebateApplication.Id && r.Status == (int)DcsWorkflowOfSimpleApprovalStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbPartsRebateApplication == null) {
                throw new ValidationException(ErrorStrings.Common_Validation3);
            }
            var userInfo = Utils.GetCurrentUserInfo();
            partsRebateApplication.ModifierId = userInfo.Id;
            partsRebateApplication.ModifierName = userInfo.Name;
            partsRebateApplication.ModifyTime = DateTime.Now;
        }
        public void InsertPartsRebateApplication(PartsRebateApplication partsRebateApplication) {
            InsertToDatabase(partsRebateApplication);
            this.InsertPartsRebateApplicationValidate(partsRebateApplication);
        }
        public void UpdatePartsRebateApplication(PartsRebateApplication partsRebateApplication) {
            UpdateToDatabase(partsRebateApplication);
            this.UpdatePartsRebateApplicationValidate(partsRebateApplication);
        }
        public IQueryable<PartsRebateApplication> GetPartsRebateApplicationWithDetails() {
            return ObjectContext.PartsRebateApplications.Include("Branch").Include("PartsSalesCategory").Include("PartsRebateType").Include("AccountGroup").OrderBy(e => e.Id);
        }
        public IQueryable<VirtualPartsRebateApplication> GetVirtualPartsRebateApplicationWithDetails(int? partsSalesCategoryId, string code, string customerCompanyCode, string customerCompanyName, string accountGroupName, string partsRebateTypeName, int? status, string creatorName, DateTime? beginCreateTime, DateTime? endCreateTime, string businessCode) {
            var customerAccountHisDetailsquery = from p in ObjectContext.PartsRebateApplications
                                                 join m in ObjectContext.Companies on p.CustomerCompanyId equals m.Id
                                                 join b in ObjectContext.Branches on p.BranchId equals b.Id
                                                 join g in ObjectContext.PartsSalesCategories on p.PartsSalesCategoryId equals g.Id
                                                 join r in ObjectContext.PartsRebateTypes on p.PartsRebateTypeId equals r.Id
                                                 join d in ObjectContext.AccountGroups on p.AccountGroupId equals d.Id
                                                 into c
                                                 from accountGroup in c.DefaultIfEmpty()
                                                 join salesUnit in ObjectContext.SalesUnits.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on new {
                                                     accountGroupId = p.AccountGroupId,
                                                     customerCompanyId = accountGroup.SalesCompanyId
                                                 }equals new {
                                                     accountGroupId = salesUnit.AccountGroupId,
                                                     customerCompanyId = salesUnit.OwnerCompanyId
                                                 }into sale
                                                 from salesUnit in sale.DefaultIfEmpty()
                                                 join dealerServiceInfo in ObjectContext.DealerServiceInfoes.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on new {
                                                     customerCompanyId = p.CustomerCompanyId,
                                                     PartsSalesCategoryId = p.PartsSalesCategoryId
                                                 }equals new {
                                                     customerCompanyId = dealerServiceInfo.DealerId,
                                                     PartsSalesCategoryId = dealerServiceInfo.PartsSalesCategoryId
                                                 }into acc1
                                                 from dealerServiceInfo in acc1.DefaultIfEmpty()
                                                 orderby p.Id
                                                 select new VirtualPartsRebateApplication {
                                                     Id = p.Id,
                                                     BusinessCode =
                                                     m.Type == (int)DcsCompanyType.服务站 || m.Type == (int)DcsCompanyType.服务站兼代理库 ? dealerServiceInfo.BusinessCode :
                                                     m.Type == (int)DcsCompanyType.代理库 ? salesUnit.BusinessCode : null,
                                                     Code = p.Code,
                                                     AccountGroupName = accountGroup.Name,
                                                     BranchName = b.Name,
                                                     BranchId = p.BranchId,
                                                     CustomerCompanyCode = p.CustomerCompanyCode,
                                                     CustomerCompanyName = p.CustomerCompanyName,
                                                     PartsSalesCategoryName = g.Name,
                                                     PartsSalesCategoryId = p.PartsSalesCategoryId,
                                                     PartsRebateTypeName = r.Name,
                                                     RebateDirection = p.RebateDirection,
                                                     Amount = p.Amount,
                                                     Motive = p.Motive,
                                                     ApprovalComment = p.ApprovalComment,
                                                     Remark = p.Remark,
                                                     Status = p.Status,
                                                     CreatorName = p.CreatorName,
                                                     CreateTime = p.CreateTime,
                                                     ModifierName = p.ModifierName,
                                                     ModifyTime = p.ModifyTime,
                                                     ApproverName = p.ApproverName,
                                                     ApproveTime = p.ApproveTime,
                                                     AbandonerName = p.AbandonerName,
                                                     AbandonTime = p.AbandonTime
                                                 };
            if(!string.IsNullOrEmpty(code)) {
                customerAccountHisDetailsquery = customerAccountHisDetailsquery.Where(r => r.Code.Contains(code));
            }
            if(!string.IsNullOrEmpty(customerCompanyCode)) {
                customerAccountHisDetailsquery = customerAccountHisDetailsquery.Where(r => r.CustomerCompanyCode.Contains(customerCompanyCode));
            }
            if(!string.IsNullOrEmpty(customerCompanyName)) {
                customerAccountHisDetailsquery = customerAccountHisDetailsquery.Where(r => r.CustomerCompanyName.Contains(customerCompanyName));
            }
            if(!string.IsNullOrEmpty(accountGroupName)) {
                customerAccountHisDetailsquery = customerAccountHisDetailsquery.Where(r => r.AccountGroupName.Contains(accountGroupName));
            }
            if(!string.IsNullOrEmpty(partsRebateTypeName)) {
                customerAccountHisDetailsquery = customerAccountHisDetailsquery.Where(r => r.PartsRebateTypeName.Contains(partsRebateTypeName));
            }
            if(!string.IsNullOrEmpty(creatorName)) {
                customerAccountHisDetailsquery = customerAccountHisDetailsquery.Where(r => r.CreatorName.Contains(creatorName));
            }
            if(status.HasValue) {
                customerAccountHisDetailsquery = customerAccountHisDetailsquery.Where(r => r.Status == status);
            }
            if(partsSalesCategoryId.HasValue) {
                customerAccountHisDetailsquery = customerAccountHisDetailsquery.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId);
            }
            if(beginCreateTime.HasValue) {
                customerAccountHisDetailsquery = customerAccountHisDetailsquery.Where(r => r.CreateTime >= beginCreateTime);
            }
            if(endCreateTime.HasValue) {
                customerAccountHisDetailsquery = customerAccountHisDetailsquery.Where(r => r.CreateTime <= endCreateTime);
            }
            if(!string.IsNullOrEmpty(businessCode)) {
                customerAccountHisDetailsquery = customerAccountHisDetailsquery.Where(r => r.BusinessCode.Contains(businessCode));
            }
            return customerAccountHisDetailsquery.Distinct().OrderBy(r => r.Id);
        }
        public PartsRebateApplication GetPartsRebateApplicationById(int id) {
            return ObjectContext.PartsRebateApplications.Include("Branch").Include("PartsSalesCategory").Include("PartsRebateType").Include("AccountGroup").SingleOrDefault(e => e.Id == id);
        }
        [Query(HasSideEffects = true)]
        public IQueryable<PartsRebateApplication> GetPartsRebateApplicationByIds(int[] ids) {
            return ObjectContext.PartsRebateApplications.Where(r => ids.Contains(r.Id));
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               

        public void InsertPartsRebateApplication(PartsRebateApplication partsRebateApplication) {
            new PartsRebateApplicationAch(this).InsertPartsRebateApplication(partsRebateApplication);
        }

        public void UpdatePartsRebateApplication(PartsRebateApplication partsRebateApplication) {
            new PartsRebateApplicationAch(this).UpdatePartsRebateApplication(partsRebateApplication);
        }

        public IQueryable<PartsRebateApplication> GetPartsRebateApplicationWithDetails() {
            return new PartsRebateApplicationAch(this).GetPartsRebateApplicationWithDetails();
        }
                public IQueryable<VirtualPartsRebateApplication> GetVirtualPartsRebateApplicationWithDetails(int? partsSalesCategoryId, string code, string customerCompanyCode, string customerCompanyName, string accountGroupName, string partsRebateTypeName, int? status, string creatorName, DateTime? beginCreateTime, DateTime? endCreateTime, string businessCode) {
            return new PartsRebateApplicationAch(this).GetVirtualPartsRebateApplicationWithDetails(partsSalesCategoryId,code,customerCompanyCode,customerCompanyName,accountGroupName,partsRebateTypeName,status,creatorName,beginCreateTime,endCreateTime,businessCode);
        }

        public PartsRebateApplication GetPartsRebateApplicationById(int id) {
            return new PartsRebateApplicationAch(this).GetPartsRebateApplicationById(id);
        }
        
        [Query(HasSideEffects = true)]
        public IQueryable<PartsRebateApplication> GetPartsRebateApplicationByIds(int[] ids) {
            return new PartsRebateApplicationAch(this).GetPartsRebateApplicationByIds(ids);
        }
    }
}
