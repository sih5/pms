﻿using System;
using System.Linq;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsRebateChangeDetailAch : DcsSerivceAchieveBase {
        public PartsRebateChangeDetailAch(DcsDomainService domainService)
            : base(domainService) {
        }

        internal void InsertPartsRebateChangeDetailValidate(PartsRebateChangeDetail partsRebateChangeDetail) {
            var userInfo = Utils.GetCurrentUserInfo();
            partsRebateChangeDetail.CreatorId = userInfo.Id;
            partsRebateChangeDetail.CreatorName = userInfo.Name;
            partsRebateChangeDetail.CreateTime = DateTime.Now;
        }
        internal void UpdatePartsRebateChangeDetailValidate(PartsRebateChangeDetail partsRebateChangeDetail) {
        }
        public void InsertPartsRebateChangeDetail(PartsRebateChangeDetail partsRebateChangeDetail) {
            InsertToDatabase(partsRebateChangeDetail);
            this.InsertPartsRebateChangeDetailValidate(partsRebateChangeDetail);
        }
        public void UpdatePartsRebateChangeDetail(PartsRebateChangeDetail partsRebateChangeDetail) {
            UpdateToDatabase(partsRebateChangeDetail);
            this.UpdatePartsRebateChangeDetailValidate(partsRebateChangeDetail);
        }
        public IQueryable<PartsRebateChangeDetail> GetPartsRebateChangeDetailWithDetails() {
            return ObjectContext.PartsRebateChangeDetails.Include("AccountGroup").Include("Company").Include("Branch").OrderBy(e => e.Id);
        }
        //public IQueryable<VirtualPartsRebateChangeDetail> GetVirtualPartsRebateChangeDetailWithDetails(int? branchId, int? accountGroupId, string companyCode, string companyName, string sourceCode, int? sourceType, DateTime? beginCreateTime, DateTime? endCreateTime, string accountGroupName, string businessCode) {
        public IQueryable<VirtualPartsRebateChangeDetail> GetVirtualPartsRebateChangeDetailWithDetails(int? branchId, int? accountGroupId, string companyCode, string companyName, string sourceCode, int? sourceType, DateTime? beginCreateTime, DateTime? endCreateTime, string businessCode) {   
            var customerAccountHisDetailsquery = from p in ObjectContext.PartsRebateChangeDetails
                                                 join b in ObjectContext.Companies.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on p.CustomerCompanyId equals b.Id
                                                 join h in ObjectContext.Branches.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on p.BranchId equals h.Id
                                                 join d in ObjectContext.AccountGroups.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on p.AccountGroupId equals d.Id
                                                 into c
                                                 from accountGroup in c.DefaultIfEmpty()
                                                 join salesUnit in ObjectContext.SalesUnits.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on new {
                                                     accountGroupId = p.AccountGroupId,
                                                     customerCompanyId = accountGroup.SalesCompanyId
                                                 }equals new {
                                                     accountGroupId = salesUnit.AccountGroupId,
                                                     customerCompanyId = salesUnit.OwnerCompanyId
                                                 }into sale
                                                 from salesUnit in sale.DefaultIfEmpty()
                                                 join dealerServiceInfo in ObjectContext.DealerServiceInfoes.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on new {
                                                     customerCompanyId = p.CustomerCompanyId,
                                                     PartsSalesCategoryId = salesUnit.PartsSalesCategoryId
                                                 }equals new {
                                                     customerCompanyId = dealerServiceInfo.DealerId,
                                                     PartsSalesCategoryId = dealerServiceInfo.PartsSalesCategoryId
                                                 }into acc1
                                                 from dealerServiceInfo in acc1.DefaultIfEmpty()
                                                 orderby p.Id
                                                 select new VirtualPartsRebateChangeDetail {
                                                     Id = p.Id,
                                                     BusinessCode =
                                                     b.Type == (int)DcsCompanyType.服务站 || b.Type == (int)DcsCompanyType.服务站兼代理库 ? dealerServiceInfo.BusinessCode :
                                                     b.Type == (int)DcsCompanyType.代理库 ? salesUnit.BusinessCode : null,
                                                     BranchName = h.Name,
                                                     AccountGroupId = accountGroup.Id,
                                                     AccountGroupName = accountGroup.Name,
                                                     CompanyName = b.Name,
                                                     CompanyCode = b.Code,
                                                     Amount = p.Amount,
                                                     SourceCode = p.SourceCode,
                                                     SourceType = p.SourceType,
                                                     CreatorName = p.CreatorName,
                                                     CreateTime = p.CreateTime,
                                                     BranchId = p.BranchId
                                                 };
            if(!string.IsNullOrEmpty(companyName)) {
                customerAccountHisDetailsquery = customerAccountHisDetailsquery.Where(r => r.CompanyName.Contains(companyName));
            }
            if(!string.IsNullOrEmpty(companyCode)) {
                customerAccountHisDetailsquery = customerAccountHisDetailsquery.Where(r => r.CompanyCode.Contains(companyCode));
            }
            if(!string.IsNullOrEmpty(sourceCode)) {
                customerAccountHisDetailsquery = customerAccountHisDetailsquery.Where(r => r.SourceCode.Contains(sourceCode));
            }
            //if(!string.IsNullOrEmpty(accountGroupName)) {
            //    customerAccountHisDetailsquery = customerAccountHisDetailsquery.Where(r => r.AccountGroupName.Contains(accountGroupName));
            //}
            if(sourceType.HasValue) {
                customerAccountHisDetailsquery = customerAccountHisDetailsquery.Where(r => r.SourceType == sourceType);
            }
            if(branchId.HasValue) {
                customerAccountHisDetailsquery = customerAccountHisDetailsquery.Where(r => r.BranchId == branchId);
            }
            if(accountGroupId.HasValue) {
                customerAccountHisDetailsquery = customerAccountHisDetailsquery.Where(r => r.AccountGroupId == accountGroupId);
            }
            if(beginCreateTime.HasValue) {
                customerAccountHisDetailsquery = customerAccountHisDetailsquery.Where(r => r.CreateTime >= beginCreateTime);
            }
            if(endCreateTime.HasValue) {
                customerAccountHisDetailsquery = customerAccountHisDetailsquery.Where(r => r.CreateTime <= endCreateTime);
            }
            if(!string.IsNullOrEmpty(businessCode)) {
                customerAccountHisDetailsquery = customerAccountHisDetailsquery.Where(r => r.BusinessCode.Contains(businessCode));
            }
            return customerAccountHisDetailsquery.Distinct().OrderBy(r => r.Id);
        }

        public IQueryable<VirtualPartsRebateChangeDetail> GetVirtualPartsRebateChangeDetailForDealerWithDetails() {//}(int? branchId, int? accountGroupId, string companyCode, string companyName, string sourceCode, int? sourceType, DateTime? beginCreateTime, DateTime? endCreateTime, string accountGroupName, string businessCode) {
            var customerAccountHisDetailsquery = from p in ObjectContext.PartsRebateChangeDetails.Where(r => r.SourceType == (int)DcsPartsRebateChangeDetailSourceType.返利申请单)
                                                 join l in ObjectContext.PartsRebateApplications on p.SourceId equals l.Id
                                                 join m in ObjectContext.PartsRebateTypes on l.PartsRebateTypeId equals m.Id
                                                 join b in ObjectContext.Companies.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on p.CustomerCompanyId equals b.Id
                                                 join h in ObjectContext.Branches.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on p.BranchId equals h.Id
                                                 join d in ObjectContext.AccountGroups.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on p.AccountGroupId equals d.Id into c
                                                 from accountGroup in c.DefaultIfEmpty()
                                                 orderby p.Id
                                                 select new VirtualPartsRebateChangeDetail {
                                                     Id = p.Id,
                                                     PartsRebateType = m.Name,
                                                     Motive = l.Motive,
                                                     Remark = l.Remark,
                                                     BranchName = h.Name,
                                                     AccountGroupId = accountGroup.Id,
                                                     AccountGroupName = accountGroup.Name,
                                                     CompanyName = b.Name,
                                                     CompanyCode = b.Code,
                                                     Amount = p.Amount,
                                                     SourceCode = p.SourceCode,
                                                     SourceType = p.SourceType,
                                                     CreatorName = p.CreatorName,
                                                     CreateTime = p.CreateTime,
                                                     BranchId = p.BranchId
                                                 };
            return customerAccountHisDetailsquery.Distinct().OrderBy(r => r.Id);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               

        public void InsertPartsRebateChangeDetail(PartsRebateChangeDetail partsRebateChangeDetail) {
            new PartsRebateChangeDetailAch(this).InsertPartsRebateChangeDetail(partsRebateChangeDetail);
        }

        public void UpdatePartsRebateChangeDetail(PartsRebateChangeDetail partsRebateChangeDetail) {
            new PartsRebateChangeDetailAch(this).UpdatePartsRebateChangeDetail(partsRebateChangeDetail);
        }

        public IQueryable<PartsRebateChangeDetail> GetPartsRebateChangeDetailWithDetails() {
            return new PartsRebateChangeDetailAch(this).GetPartsRebateChangeDetailWithDetails();
        }
                public IQueryable<VirtualPartsRebateChangeDetail> GetVirtualPartsRebateChangeDetailWithDetails(int? branchId, int? accountGroupId, string companyCode, string companyName, string sourceCode, int? sourceType, DateTime? beginCreateTime, DateTime? endCreateTime, string businessCode) {
            return new PartsRebateChangeDetailAch(this).GetVirtualPartsRebateChangeDetailWithDetails(branchId,accountGroupId,companyCode,companyName,sourceCode,sourceType,beginCreateTime,endCreateTime,businessCode);
        }
        
        public IQueryable<VirtualPartsRebateChangeDetail> GetVirtualPartsRebateChangeDetailForDealerWithDetails() {
            return new PartsRebateChangeDetailAch(this).GetVirtualPartsRebateChangeDetailForDealerWithDetails();
        }
    }
}
