﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsRebateTypeAch : DcsSerivceAchieveBase {
        internal void InsertPartsRebateTypeValidate(PartsRebateType partsRebateType) {
            if(string.IsNullOrWhiteSpace(partsRebateType.Code) || partsRebateType.Code == GlobalVar.ASSIGNED_BY_SERVER)
                partsRebateType.Code = CodeGenerator.Generate("PartsRebateType");
            var userInfo = Utils.GetCurrentUserInfo();
            partsRebateType.CreatorId = userInfo.Id;
            partsRebateType.CreatorName = userInfo.Name;
            partsRebateType.CreateTime = DateTime.Now;
        }
        internal void UpdatePartsRebateTypeValidate(PartsRebateType partsRebateType) {
            var dbPartsRebateType = ObjectContext.PartsRebateTypes.Where(r => r.Id == partsRebateType.Id && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbPartsRebateType == null)
                throw new ValidationException(ErrorStrings.PartsRebateType_Validation1);
            var userInfo = Utils.GetCurrentUserInfo();
            partsRebateType.ModifierId = userInfo.Id;
            partsRebateType.ModifierName = userInfo.Name;
            partsRebateType.ModifyTime = DateTime.Now;
        }
        public void InsertPartsRebateType(PartsRebateType partsRebateType) {
            InsertToDatabase(partsRebateType);
            this.InsertPartsRebateTypeValidate(partsRebateType);
        }
        public void UpdatePartsRebateType(PartsRebateType partsRebateType) {
            UpdateToDatabase(partsRebateType);
            this.UpdatePartsRebateTypeValidate(partsRebateType);
        }
        public IQueryable<PartsRebateType> GetPartsRebateTypeWithPartsSalesCategory() {
            return ObjectContext.PartsRebateTypes.Include("PartsSalesCategory").OrderBy(r => r.Id);
        }
        public PartsRebateType GetPartsRebateTypeById(int Id) {
            return ObjectContext.PartsRebateTypes.Include("PartsSalesCategory").SingleOrDefault(ex => ex.Id == Id);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               

        public void InsertPartsRebateType(PartsRebateType partsRebateType) {
            new PartsRebateTypeAch(this).InsertPartsRebateType(partsRebateType);
        }

        public void UpdatePartsRebateType(PartsRebateType partsRebateType) {
            new PartsRebateTypeAch(this).UpdatePartsRebateType(partsRebateType);
        }

        public IQueryable<PartsRebateType> GetPartsRebateTypeWithPartsSalesCategory() {
            return new PartsRebateTypeAch(this).GetPartsRebateTypeWithPartsSalesCategory();
        }
                public PartsRebateType GetPartsRebateTypeById(int Id) {
            return new PartsRebateTypeAch(this).GetPartsRebateTypeById(Id);
        }
    }
}
