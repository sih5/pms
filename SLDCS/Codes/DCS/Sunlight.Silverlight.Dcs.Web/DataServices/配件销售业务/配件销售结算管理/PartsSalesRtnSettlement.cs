﻿using System;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsSalesRtnSettlementAch : DcsSerivceAchieveBase {
        internal void InsertPartsSalesRtnSettlementValidate(PartsSalesRtnSettlement partsSalesRtnSettlement) {
            var userInfo = Utils.GetCurrentUserInfo();
            partsSalesRtnSettlement.CreatorId = userInfo.Id;
            partsSalesRtnSettlement.CreatorName = userInfo.Name;
            partsSalesRtnSettlement.CreateTime = DateTime.Now;

            if(string.IsNullOrWhiteSpace(partsSalesRtnSettlement.Code) || partsSalesRtnSettlement.Code == GlobalVar.ASSIGNED_BY_SERVER)
                partsSalesRtnSettlement.Code = CodeGenerator.Generate("PartsSalesRtnSettlement", partsSalesRtnSettlement.SalesCompanyCode);
        }

        internal void UpdatePartsSalesRtnSettlementValidate(PartsSalesRtnSettlement partsSalesRtnSettlement) {
            var userInfo = Utils.GetCurrentUserInfo();
            partsSalesRtnSettlement.ModifierId = userInfo.Id;
            partsSalesRtnSettlement.ModifierName = userInfo.Name;
            partsSalesRtnSettlement.ModifyTime = DateTime.Now;
        }

        public void InsertPartsSalesRtnSettlement(PartsSalesRtnSettlement partsSalesRtnSettlement) {
            InsertToDatabase(partsSalesRtnSettlement);
            var partsSalesRtnSettlementDetails = ChangeSet.GetAssociatedChanges(partsSalesRtnSettlement, v => v.PartsSalesRtnSettlementDetails, ChangeOperation.Insert);
            foreach(PartsSalesRtnSettlementDetail partsSalesRtnSettlementDetail in partsSalesRtnSettlementDetails) {
                InsertToDatabase(partsSalesRtnSettlementDetail);
            }
            var partsSalesRtnSettlementRefs = ChangeSet.GetAssociatedChanges(partsSalesRtnSettlement, v => v.PartsSalesRtnSettlementRefs, ChangeOperation.Insert);
            foreach(PartsSalesRtnSettlementRef partsSalesRtnSettlementRef in partsSalesRtnSettlementRefs) {
                InsertToDatabase(partsSalesRtnSettlementRef);
            }
            this.InsertPartsSalesRtnSettlementValidate(partsSalesRtnSettlement);
        }

        public void UpdatePartsSalesRtnSettlement(PartsSalesRtnSettlement partsSalesRtnSettlement) {
            partsSalesRtnSettlement.PartsSalesRtnSettlementDetails.Clear();
            partsSalesRtnSettlement.PartsSalesRtnSettlementRefs.Clear();
            UpdateToDatabase(partsSalesRtnSettlement);
            var partsSalesRtnSettlementDetails = ChangeSet.GetAssociatedChanges(partsSalesRtnSettlement, v => v.PartsSalesRtnSettlementDetails);
            foreach(PartsSalesRtnSettlementDetail partsSalesRtnSettlementDetail in partsSalesRtnSettlementDetails) {
                switch(ChangeSet.GetChangeOperation(partsSalesRtnSettlementDetail)) {
                    case ChangeOperation.Insert:
                        InsertToDatabase(partsSalesRtnSettlementDetail);
                        break;
                    case ChangeOperation.Update:
                        UpdateToDatabase(partsSalesRtnSettlementDetail);
                        break;
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(partsSalesRtnSettlementDetail);
                        break;
                }
            }
            var partsSalesRtnSettlementRefs = ChangeSet.GetAssociatedChanges(partsSalesRtnSettlement, v => v.PartsSalesRtnSettlementRefs);
            foreach(PartsSalesRtnSettlementRef partsSalesRtnSettlementRef in partsSalesRtnSettlementRefs) {
                switch(ChangeSet.GetChangeOperation(partsSalesRtnSettlementRef)) {
                    case ChangeOperation.Insert:
                        InsertToDatabase(partsSalesRtnSettlementRef);
                        break;
                    case ChangeOperation.Update:
                        UpdateToDatabase(partsSalesRtnSettlementRef);
                        break;
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(partsSalesRtnSettlementRef);
                        break;
                }
            }
            this.UpdatePartsSalesRtnSettlementValidate(partsSalesRtnSettlement);
        }

        public PartsSalesRtnSettlement GetPartsSalesRtnSettlementsWithDetailsAndRefsById(int id) {
            var dbPartsSalesRtnSettlement = ObjectContext.PartsSalesRtnSettlements.SingleOrDefault(entity => entity.Id == id);
            if(dbPartsSalesRtnSettlement != null) {
                var dbDetails = ObjectContext.PartsSalesRtnSettlementDetails.Where(e => e.PartsSalesRtnSettlementId == id).ToArray();
                var dbRefs = ObjectContext.PartsSalesRtnSettlementRefs.Where(e => e.PartsSalesRtnSettlementId == id).ToArray();
                var sourceIds = dbRefs.Select(r => r.SourceId);

                var tempStructArray = (from a in ObjectContext.PartsSalesReturnBills
                                       join b in ObjectContext.PartsInboundCheckBills on a.Id equals b.OriginalRequirementBillId
                                       where b.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件销售退货单 && sourceIds.Contains(b.Id) && b.InboundType != (int)DcsPartsInboundType.质量件索赔
                                       select new {
                                           inboundCheckBillId = b.Id,
                                           returnReason = a.ReturnReason,
                                           returnRemark = a.Remark,
                                           returnType = a.ReturnType
                                       }).ToArray();
                var tempStructArrayNotWithPartsSalesReturnBill = (from b in ObjectContext.PartsInboundCheckBills
                                                                  where b.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件销售退货单 && sourceIds.Contains(b.Id) && b.InboundType == (int)DcsPartsInboundType.质量件索赔
                                                                  select new {
                                                                      inboundCheckBillId = b.Id,
                                                                      returnReason = "",
                                                                      returnRemark = "",
                                                                      returnType = -1
                                                                  }).ToArray();
                tempStructArray = (tempStructArray.Union(tempStructArrayNotWithPartsSalesReturnBill)).ToArray();
                foreach(var item in dbRefs) {
                    var tempStruct = tempStructArray.FirstOrDefault(r => r.inboundCheckBillId == item.SourceId);
                    if(tempStruct != null) {
                        item.ReturnReason = tempStruct.returnReason;
                        item.PartsSalesReturnBillRemark = tempStruct.returnRemark;
                        item.ReturnType = tempStruct.returnType;
                    }
                }
            }
            return dbPartsSalesRtnSettlement;
        }

        public IQueryable<PartsSalesRtnSettlement> 查询配件销售退货结算单() {
            var userInfo = Utils.GetCurrentUserInfo();
            var company = ObjectContext.Companies.SingleOrDefault(r => r.Id == userInfo.EnterpriseId && r.Status == (int)DcsMasterDataStatus.有效);
            if(company == null) {
                return null;
            }
            IQueryable<PartsSalesRtnSettlement> Query = ObjectContext.PartsSalesRtnSettlements;
            if(company.Type == (int)DcsCompanyType.分公司) {
                var partsSalesCategoryIds = ObjectContext.PersonSalesCenterLinks.Where(v => v.PersonId == userInfo.Id).Select(r => r.PartsSalesCategoryId).ToArray();
                if(partsSalesCategoryIds.Length > 0) {
                    Query = ObjectContext.PartsSalesRtnSettlements.Where(r => partsSalesCategoryIds.Contains(r.PartsSalesCategoryId));
                }
            }
            return Query.OrderBy(r => r.Id);
        }

        //public IQueryable<PartsSalesRtnSettlement> 根据业务类型查询配件销售退货结算单(string businessCode) {
        //    var userInfo = Utils.GetCurrentUserInfo();
        //    var company = ObjectContext.Companies.SingleOrDefault(r => r.Id == userInfo.EnterpriseId && r.Status == (int)DcsMasterDataStatus.有效);
        //    if(company == null) {
        //        return null;
        //    }
        //    IQueryable<PartsSalesRtnSettlement> result = ObjectContext.PartsSalesRtnSettlements;
        //    if(!string.IsNullOrEmpty(businessCode)) {
        //        //客户企业类型为服务站或服务站兼代理库，业务编码赋值为经销商管理信息对应的业务编码
        //        var result1 = from a in ObjectContext.PartsSalesRtnSettlements
        //                      join c in ObjectContext.Companies.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && (r.Type == (int)DcsCompanyType.服务站 || r.Type == (int)DcsCompanyType.服务站兼代理库)) on a.CustomerCompanyId equals c.Id
        //                      join d in ObjectContext.DealerServiceInfoes.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.BusinessCode.Contains(businessCode)) on new {
        //                          k1 = a.CustomerCompanyId,
        //                          k2 = a.PartsSalesCategoryId
        //                      }equals new {
        //                          k1 = d.DealerId,
        //                          k2 = d.PartsSalesCategoryId
        //                      }
        //                      select a;

        //        //客户企业类型为代理库 业务编码赋值为客户企业对应的销售组织的业务编码
        //        var result2 = from a in ObjectContext.PartsSalesRtnSettlements
        //                      join b in ObjectContext.Companies.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.Type == (int)DcsCompanyType.代理库) on a.CustomerCompanyId equals b.Id
        //                      join c in ObjectContext.SalesUnits.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.BusinessCode.Contains(businessCode)) on new {
        //                          k1 = a.CustomerCompanyId,
        //                          k2 = a.PartsSalesCategoryId
        //                      }equals new {
        //                          k1 = c.OwnerCompanyId,
        //                          k2 = c.PartsSalesCategoryId
        //                      }
        //                      select a;
        //        result = result1.Concat(result2);
        //    }
        //    if(company.Type == (int)DcsCompanyType.分公司) {
        //        var partsSalesCategoryIds = ObjectContext.PersonSalesCenterLinks.Where(v => v.PersonId == userInfo.Id).Select(r => r.PartsSalesCategoryId).ToArray();
        //        if(partsSalesCategoryIds.Length > 0) {
        //            result = result.Where(r => partsSalesCategoryIds.Contains(r.PartsSalesCategoryId));
        //        }
        //    }
        //    return result.OrderBy(r => r.Id);
        //}
        public IQueryable<PartsSalesRtnSettlementEx> 根据业务类型查询配件销售退货结算单(string businessCode) {
            var userInfo = Utils.GetCurrentUserInfo();
            var company = ObjectContext.Companies.SingleOrDefault(r => r.Id == userInfo.EnterpriseId && r.Status == (int)DcsMasterDataStatus.有效);
            if(company == null) {
                return null;
            }

            var queryResult = from a in ObjectContext.PartsSalesRtnSettlementRefs
                              from b in ObjectContext.PartsInboundCheckBillDetails
                              where a.SourceId == b.PartsInboundCheckBillId
                              select new {
                                  rtnSettlementId = a.PartsSalesRtnSettlementId,
                                  tempAmount = b.CostPrice * b.InspectedQuantity
                              };

            var tempQuery = from a in queryResult
                            group a by new {
                                a.rtnSettlementId
                            } into tempTable
                            select new {
                                rtnSettlementId = tempTable.Key.rtnSettlementId,
                                totalAmount = (decimal?)tempTable.Sum(r => r.tempAmount)

                            };

            IQueryable<PartsSalesRtnSettlementEx> result = from a in ObjectContext.PartsSalesRtnSettlements
                                                           from b in tempQuery
                                                           where a.Id == b.rtnSettlementId
                                                           select new PartsSalesRtnSettlementEx {
                                                               Id = a.Id,
                                                               Code = a.Code,
                                                               SettlementPath = a.SettlementPath,
                                                               SalesCompanyId = a.SalesCompanyId,
                                                               SalesCompanyCode = a.SalesCompanyCode,
                                                               SalesCompanyName = a.SalesCompanyName,
                                                               CustomerCompanyId = a.CustomerCompanyId,
                                                               CustomerCompanyCode = a.CustomerCompanyCode,
                                                               CustomerCompanyName = a.CustomerCompanyName,
                                                               PartsSalesCategoryId = a.PartsSalesCategoryId,
                                                               PartsSalesCategoryName = a.PartsSalesCategoryName,
                                                               AccountGroupId = a.AccountGroupId,
                                                               AccountGroupCode = a.AccountGroupCode,
                                                               AccountGroupName = a.AccountGroupName,
                                                               CustomerAccountId = a.CustomerAccountId,
                                                               TotalSettlementAmount = a.TotalSettlementAmount,
                                                               InvoiceAmountDifference = a.InvoiceAmountDifference,
                                                               OffsettedSettlementBillId = a.OffsettedSettlementBillId,
                                                               OffsettedSettlementBillCode = a.OffsettedSettlementBillCode,
                                                               TaxRate = a.TaxRate,
                                                               Tax = a.Tax,
                                                               DiscountRate = a.DiscountRate,
                                                               Discount = a.Discount,
                                                               InvoicePath = a.InvoicePath,
                                                               Remark = a.Remark,
                                                               Status = a.Status,
                                                               CreatorId = a.CreatorId,
                                                               CreatorName = a.CreatorName,
                                                               CreateTime = a.CreateTime,
                                                               ModifierId = a.ModifierId,
                                                               ModifierName = a.ModifierName,
                                                               ModifyTime = a.ModifyTime,
                                                               AbandonerId = a.AbandonerId,
                                                               AbandonerName = a.AbandonerName,
                                                               AbandonTime = a.AbandonTime,
                                                               InvoiceRegistrationOperatorId = a.InvoiceRegistrationOperatorId,
                                                               InvoiceRegistrationOperator = a.InvoiceRegistrationOperator,
                                                               InvoiceRegistrationTime = a.InvoiceRegistrationTime,
                                                               ApproverId = a.ApproverId,
                                                               ApproverName = a.ApproverName,
                                                               ApproveTime = a.ApproveTime,
                                                               PlannedPriceTotalAmount = (b.totalAmount ?? 0),
                                                               InvoiceDate=a.InvoiceDate,
                                                               SettleType = a.SettleType,
                                                               BusinessType = a.BusinessType
                                                           };

            if(!string.IsNullOrEmpty(businessCode)) {
                //客户企业类型为服务站或服务站兼代理库，业务编码赋值为经销商管理信息对应的业务编码
                var result1 = from a in ObjectContext.PartsSalesRtnSettlements
                              join c in ObjectContext.Companies.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && (r.Type == (int)DcsCompanyType.服务站 || r.Type == (int)DcsCompanyType.服务站兼代理库)) on a.CustomerCompanyId equals c.Id
                              join d in ObjectContext.DealerServiceInfoes.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.BusinessCode.Contains(businessCode)) on new {
                                  k1 = a.CustomerCompanyId,
                                  k2 = a.PartsSalesCategoryId
                              } equals new {
                                  k1 = d.DealerId,
                                  k2 = d.PartsSalesCategoryId
                              } into tempTable
                              from ss in tempTable.DefaultIfEmpty()
                              join b in tempQuery on a.Id equals b.rtnSettlementId into tempTable1
                              from t in tempTable1.DefaultIfEmpty()
                              select new PartsSalesRtnSettlementEx {
                                  Id = a.Id,
                                  Code = a.Code,
                                  SettlementPath = a.SettlementPath,
                                  SalesCompanyId = a.SalesCompanyId,
                                  SalesCompanyCode = a.SalesCompanyCode,
                                  SalesCompanyName = a.SalesCompanyName,
                                  CustomerCompanyId = a.CustomerCompanyId,
                                  CustomerCompanyCode = a.CustomerCompanyCode,
                                  CustomerCompanyName = a.CustomerCompanyName,
                                  PartsSalesCategoryId = a.PartsSalesCategoryId,
                                  PartsSalesCategoryName = a.PartsSalesCategoryName,
                                  AccountGroupId = a.AccountGroupId,
                                  AccountGroupCode = a.AccountGroupCode,
                                  AccountGroupName = a.AccountGroupName,
                                  CustomerAccountId = a.CustomerAccountId,
                                  TotalSettlementAmount = a.TotalSettlementAmount,
                                  InvoiceAmountDifference = a.InvoiceAmountDifference,
                                  OffsettedSettlementBillId = a.OffsettedSettlementBillId,
                                  OffsettedSettlementBillCode = a.OffsettedSettlementBillCode,
                                  TaxRate = a.TaxRate,
                                  Tax = a.Tax,
                                  DiscountRate = a.DiscountRate,
                                  Discount = a.Discount,
                                  InvoicePath = a.InvoicePath,
                                  Remark = a.Remark,
                                  Status = a.Status,
                                  CreatorId = a.CreatorId,
                                  CreatorName = a.CreatorName,
                                  CreateTime = a.CreateTime,
                                  ModifierId = a.ModifierId,
                                  ModifierName = a.ModifierName,
                                  ModifyTime = a.ModifyTime,
                                  AbandonerId = a.AbandonerId,
                                  AbandonerName = a.AbandonerName,
                                  AbandonTime = a.AbandonTime,
                                  InvoiceRegistrationOperatorId = a.InvoiceRegistrationOperatorId,
                                  InvoiceRegistrationOperator = a.InvoiceRegistrationOperator,
                                  InvoiceRegistrationTime = a.InvoiceRegistrationTime,
                                  ApproverId = a.ApproverId,
                                  ApproverName = a.ApproverName,
                                  ApproveTime = a.ApproveTime,
                                  PlannedPriceTotalAmount = (t.totalAmount ?? 0),
                                  InvoiceDate =a.InvoiceDate,
                                  SettleType = a.SettleType,
                                  BusinessType = a.BusinessType
                              };
                //select a;

                //客户企业类型为代理库 业务编码赋值为客户企业对应的销售组织的业务编码
                var result2 = from a in ObjectContext.PartsSalesRtnSettlements
                              join b in ObjectContext.Companies.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.Type == (int)DcsCompanyType.代理库) on a.CustomerCompanyId equals b.Id
                              join c in ObjectContext.SalesUnits.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.BusinessCode.Contains(businessCode)) on new {
                                  k1 = a.CustomerCompanyId,
                                  k2 = a.PartsSalesCategoryId
                              } equals new {
                                  k1 = c.OwnerCompanyId,
                                  k2 = c.PartsSalesCategoryId
                              } into tempTable
                              from ss in tempTable.DefaultIfEmpty()
                              join tq in tempQuery on a.Id equals tq.rtnSettlementId into tempTable1
                              from t in tempTable1.DefaultIfEmpty()
                              select new PartsSalesRtnSettlementEx {
                                  Id = a.Id,
                                  Code = a.Code,
                                  SettlementPath = a.SettlementPath,
                                  SalesCompanyId = a.SalesCompanyId,
                                  SalesCompanyCode = a.SalesCompanyCode,
                                  SalesCompanyName = a.SalesCompanyName,
                                  CustomerCompanyId = a.CustomerCompanyId,
                                  CustomerCompanyCode = a.CustomerCompanyCode,
                                  CustomerCompanyName = a.CustomerCompanyName,
                                  PartsSalesCategoryId = a.PartsSalesCategoryId,
                                  PartsSalesCategoryName = a.PartsSalesCategoryName,
                                  AccountGroupId = a.AccountGroupId,
                                  AccountGroupCode = a.AccountGroupCode,
                                  AccountGroupName = a.AccountGroupName,
                                  CustomerAccountId = a.CustomerAccountId,
                                  TotalSettlementAmount = a.TotalSettlementAmount,
                                  InvoiceAmountDifference = a.InvoiceAmountDifference,
                                  OffsettedSettlementBillId = a.OffsettedSettlementBillId,
                                  OffsettedSettlementBillCode = a.OffsettedSettlementBillCode,
                                  TaxRate = a.TaxRate,
                                  Tax = a.Tax,
                                  DiscountRate = a.DiscountRate,
                                  Discount = a.Discount,
                                  InvoicePath = a.InvoicePath,
                                  Remark = a.Remark,
                                  Status = a.Status,
                                  CreatorId = a.CreatorId,
                                  CreatorName = a.CreatorName,
                                  CreateTime = a.CreateTime,
                                  ModifierId = a.ModifierId,
                                  ModifierName = a.ModifierName,
                                  ModifyTime = a.ModifyTime,
                                  AbandonerId = a.AbandonerId,
                                  AbandonerName = a.AbandonerName,
                                  AbandonTime = a.AbandonTime,
                                  InvoiceRegistrationOperatorId = a.InvoiceRegistrationOperatorId,
                                  InvoiceRegistrationOperator = a.InvoiceRegistrationOperator,
                                  InvoiceRegistrationTime = a.InvoiceRegistrationTime,
                                  ApproverId = a.ApproverId,
                                  ApproverName = a.ApproverName,
                                  ApproveTime = a.ApproveTime,
                                  PlannedPriceTotalAmount = t.totalAmount ?? 0,
                                  InvoiceDate = a.InvoiceDate,
                                  SettleType = a.SettleType,
                                  BusinessType = a.BusinessType
                              };
                //select a;
                result = result1.Concat(result2);
            }
            if(company.Type == (int)DcsCompanyType.分公司) {
                var partsSalesCategoryIds = ObjectContext.PersonSalesCenterLinks.Where(v => v.PersonId == userInfo.Id).Select(r => r.PartsSalesCategoryId).ToArray();
                if(partsSalesCategoryIds.Length > 0) {
                    result = result.Where(r => partsSalesCategoryIds.Contains(r.PartsSalesCategoryId));
                }
            }
            return result.OrderBy(r => r.Id);
        }

        public IQueryable<VirtualPartsSalesRtnSettlement> 查询虚拟配件销售退货结算成本(int partsSalesRtnSettlementId) {
            var partsSalesRtnSettlements = from partsSalesRtnSettlement in ObjectContext.PartsSalesRtnSettlements.Where(r => r.Id == partsSalesRtnSettlementId)
                                           from partsSalesRtnSettlementRef in ObjectContext.PartsSalesRtnSettlementRefs
                                           from partsOutboundBill in ObjectContext.PartsOutboundBills
                                           from partsOutboundBillDetail in ObjectContext.PartsOutboundBillDetails
                                           from sparePart in ObjectContext.SpareParts
                                           from partsSalesRtnSettlementDetail in ObjectContext.PartsSalesRtnSettlementDetails
                                           where sparePart.Id == partsOutboundBillDetail.SparePartId && sparePart.Id == partsSalesRtnSettlementDetail.SparePartId && partsSalesRtnSettlementRef.PartsSalesRtnSettlementId == partsSalesRtnSettlement.Id && partsSalesRtnSettlementRef.SourceId == partsOutboundBill.Id && partsOutboundBillDetail.PartsOutboundBillId == partsOutboundBill.Id
                                           select new VirtualPartsSalesRtnSettlement {
                                               Code = partsSalesRtnSettlement.Code,
                                               BillCode = partsOutboundBill.Code,
                                               BillBusinessType = "出库",
                                               SparePartCode = sparePart.Code,
                                               SparePartName = sparePart.Name,
                                               Quantity = partsOutboundBillDetail.OutboundAmount,
                                               SettlementPrice = partsOutboundBillDetail.SettlementPrice,
                                               SettlementAmount = partsOutboundBillDetail.SettlementPrice * partsOutboundBillDetail.OutboundAmount,
                                               PlanPrice = partsOutboundBillDetail.CostPrice,
                                               PlanAmount = partsOutboundBillDetail.CostPrice * partsOutboundBillDetail.OutboundAmount,
                                               MaterialCostVariance = partsOutboundBillDetail.SettlementPrice * partsOutboundBillDetail.OutboundAmount - partsOutboundBillDetail.CostPrice * partsOutboundBillDetail.OutboundAmount,
                                           };
            var partsSalesRtnSettlementss = from partsSalesRtnSettlement in ObjectContext.PartsSalesRtnSettlements.Where(r => r.Id == partsSalesRtnSettlementId)
                                            from partsSalesRtnSettlementRef in ObjectContext.PartsSalesRtnSettlementRefs
                                            from partsInboundCheckBill in ObjectContext.PartsInboundCheckBills
                                            from partsInboundCheckBillDetail in ObjectContext.PartsInboundCheckBillDetails
                                            from sparePart in ObjectContext.SpareParts
                                            from partsSalesRtnSettlementDetail in ObjectContext.PartsSalesRtnSettlementDetails
                                            where sparePart.Id == partsInboundCheckBillDetail.SparePartId && sparePart.Id == partsSalesRtnSettlementDetail.SparePartId && partsSalesRtnSettlementRef.PartsSalesRtnSettlementId == partsSalesRtnSettlement.Id && partsSalesRtnSettlementRef.SourceId == partsInboundCheckBill.Id && partsInboundCheckBillDetail.PartsInboundCheckBillId == partsInboundCheckBill.Id
                                            select new VirtualPartsSalesRtnSettlement {
                                                Code = partsSalesRtnSettlement.Code,
                                                BillCode = partsInboundCheckBill.Code,
                                                BillBusinessType = "入库",
                                                SparePartCode = sparePart.Code,
                                                SparePartName = sparePart.Name,
                                                Quantity = partsInboundCheckBillDetail.InspectedQuantity,
                                                SettlementPrice = partsInboundCheckBillDetail.SettlementPrice,
                                                SettlementAmount = partsInboundCheckBillDetail.SettlementPrice * partsInboundCheckBillDetail.InspectedQuantity,
                                                PlanPrice = partsInboundCheckBillDetail.CostPrice,
                                                PlanAmount = partsInboundCheckBillDetail.CostPrice * partsInboundCheckBillDetail.InspectedQuantity,
                                                MaterialCostVariance = partsInboundCheckBillDetail.SettlementPrice * partsInboundCheckBillDetail.InspectedQuantity - partsInboundCheckBillDetail.CostPrice * partsInboundCheckBillDetail.InspectedQuantity,
                                            };
            return partsSalesRtnSettlements.Union(partsSalesRtnSettlementss).OrderBy(r => r.BillCode);
        }

    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               

        public void InsertPartsSalesRtnSettlement(PartsSalesRtnSettlement partsSalesRtnSettlement) {
            new PartsSalesRtnSettlementAch(this).InsertPartsSalesRtnSettlement(partsSalesRtnSettlement);
        }

        public void UpdatePartsSalesRtnSettlement(PartsSalesRtnSettlement partsSalesRtnSettlement) {
            new PartsSalesRtnSettlementAch(this).UpdatePartsSalesRtnSettlement(partsSalesRtnSettlement);
        }

        public PartsSalesRtnSettlement GetPartsSalesRtnSettlementsWithDetailsAndRefsById(int id) {
            return new PartsSalesRtnSettlementAch(this).GetPartsSalesRtnSettlementsWithDetailsAndRefsById(id);
        }

        public IQueryable<PartsSalesRtnSettlement> 查询配件销售退货结算单() {
            return new PartsSalesRtnSettlementAch(this).查询配件销售退货结算单();
        }
        public IQueryable<PartsSalesRtnSettlementEx> 根据业务类型查询配件销售退货结算单(string businessCode) {
            return new PartsSalesRtnSettlementAch(this).根据业务类型查询配件销售退货结算单(businessCode);
        }

        public IQueryable<VirtualPartsSalesRtnSettlement> 查询虚拟配件销售退货结算成本(int partsSalesRtnSettlementId) {
            return new PartsSalesRtnSettlementAch(this).查询虚拟配件销售退货结算成本(partsSalesRtnSettlementId);
        }
    }
}
