﻿using System;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsSalesSettlementAch : DcsSerivceAchieveBase {
        internal void InsertPartsSalesSettlementValidate(PartsSalesSettlement partsSalesSettlement) {
            var userInfo = Utils.GetCurrentUserInfo();
            partsSalesSettlement.CreatorId = userInfo.Id;
            partsSalesSettlement.CreatorName = userInfo.Name;
            partsSalesSettlement.CreateTime = DateTime.Now;

            if(string.IsNullOrWhiteSpace(partsSalesSettlement.Code) || partsSalesSettlement.Code == GlobalVar.ASSIGNED_BY_SERVER)
                partsSalesSettlement.Code = CodeGenerator.Generate("PartsSalesSettlement", partsSalesSettlement.SalesCompanyCode);
        }

        internal void UpdatePartsSalesSettlementValidate(PartsSalesSettlement partsSalesSettlement) {
            var userInfo = Utils.GetCurrentUserInfo();
            partsSalesSettlement.ModifierId = userInfo.Id;
            partsSalesSettlement.ModifierName = userInfo.Name;
            partsSalesSettlement.ModifyTime = DateTime.Now;
        }

        public void InsertPartsSalesSettlement(PartsSalesSettlement partsSalesSettlement) {
            InsertToDatabase(partsSalesSettlement);
            var partsSalesSettlementDetails = ChangeSet.GetAssociatedChanges(partsSalesSettlement, v => v.PartsSalesSettlementDetails, ChangeOperation.Insert);
            foreach(PartsSalesSettlementDetail partsSalesSettlementDetail in partsSalesSettlementDetails) {
                InsertToDatabase(partsSalesSettlementDetail);
            }
            var partsSalesSettlementRefs = ChangeSet.GetAssociatedChanges(partsSalesSettlement, v => v.PartsSalesSettlementRefs, ChangeOperation.Insert);
            foreach(PartsSalesSettlementRef partsSalesSettlementRef in partsSalesSettlementRefs) {
                InsertToDatabase(partsSalesSettlementRef);
            }
            this.InsertPartsSalesSettlementValidate(partsSalesSettlement);
        }

        public void UpdatePartsSalesSettlement(PartsSalesSettlement partsSalesSettlement) {
            partsSalesSettlement.PartsSalesSettlementDetails.Clear();
            partsSalesSettlement.PartsSalesSettlementRefs.Clear();
            UpdateToDatabase(partsSalesSettlement);
            var partsSalesSettlementDetails = ChangeSet.GetAssociatedChanges(partsSalesSettlement, v => v.PartsSalesSettlementDetails);
            foreach(PartsSalesSettlementDetail partsSalesSettlementDetail in partsSalesSettlementDetails) {
                switch(ChangeSet.GetChangeOperation(partsSalesSettlementDetail)) {
                    case ChangeOperation.Insert:
                        InsertToDatabase(partsSalesSettlementDetail);
                        break;
                    case ChangeOperation.Update:
                        UpdateToDatabase(partsSalesSettlementDetail);
                        break;
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(partsSalesSettlementDetail);
                        break;
                }
            }
            var partsSalesSettlementRefs = ChangeSet.GetAssociatedChanges(partsSalesSettlement, v => v.PartsSalesSettlementRefs);
            foreach(PartsSalesSettlementRef partsSalesSettlementRef in partsSalesSettlementRefs) {
                switch(ChangeSet.GetChangeOperation(partsSalesSettlementRef)) {
                    case ChangeOperation.Insert:
                        InsertToDatabase(partsSalesSettlementRef);
                        break;
                    case ChangeOperation.Update:
                        UpdateToDatabase(partsSalesSettlementRef);
                        break;
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(partsSalesSettlementRef);
                        break;
                }
            }
            this.UpdatePartsSalesSettlementValidate(partsSalesSettlement);
        }

        public PartsSalesSettlement GetPartsSalesSettlementWithDetailsById(int id) {
            var dbPartsSalesSettlement = ObjectContext.PartsSalesSettlements.SingleOrDefault(entity => entity.Id == id && entity.Status != (int)DcsPartsSalesSettlementStatus.作废);
            if(dbPartsSalesSettlement != null) {
                var dbRefs = ObjectContext.PartsSalesSettlementRefs.Where(e => e.PartsSalesSettlementId == id).ToArray();
                //查询入库检验单id集合
                var ids = dbRefs.Where(r => r.SourceType == (int)DcsPartsSalesSettlementRefSourceType.配件入库检验单).Select(r => r.SourceId);
                var partsInboundCheckBills = this.ObjectContext.PartsInboundCheckBills.Where(r => ids.Contains(r.Id) && r.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件销售退货单).Select(e => new {
                    e.Id,
                    e.OriginalRequirementBillId
                }).ToArray();
                var returnIds = partsInboundCheckBills.Select(r => r.OriginalRequirementBillId).ToArray();
                var partsSalesReturnBills = this.ObjectContext.PartsSalesReturnBills.Where(r => returnIds.Contains(r.Id)).Select(e => new {
                    e.Id,
                    e.ReturnType
                });

                foreach(var partsPurchaseSettleRef in dbRefs.Where(r => r.SourceType == (int)DcsPartsSalesSettlementRefSourceType.配件入库检验单)) {
                    if(partsInboundCheckBills.Any(e => e.OriginalRequirementBillId == partsPurchaseSettleRef.SourceId)) {
                        var checkBill = partsInboundCheckBills.FirstOrDefault(e => e.Id == partsPurchaseSettleRef.SourceId);
                        if(checkBill != null) {
                            var returnOrder = partsSalesReturnBills.FirstOrDefault(e => e.Id == checkBill.OriginalRequirementBillId);
                            if(returnOrder != null) {
                                partsPurchaseSettleRef.ReturnType = returnOrder.ReturnType;
                            }
                        }
                    }
                }


                var sourceIds = dbRefs.Where(r => r.SourceType == (int)DcsPartsPurchaseSettleRefSourceType.配件出库单).Select(r => r.SourceId);
                var partsOutboundBills = (from a in ObjectContext.PartsOutboundBills.Where(e => sourceIds.Contains(e.Id))
                                          join g in ObjectContext.PartsSalesOrders on a.OriginalRequirementBillCode equals g.Code into tempTable1
                                          from t3 in tempTable1
                                          select new {
                                              OutboundBillId = a.Id,
                                              InvoiceType = t3.InvoiceType,
                                              ERPSourceOrderCode = t3.ERPSourceOrderCode,
                                              InvoiceCustomerName = t3.InvoiceCustomerName
                                          }).ToArray();
                foreach(var item in dbRefs.Where(r => r.SourceType == (int)DcsPartsPurchaseSettleRefSourceType.配件出库单)) {
                    var tempStruct = partsOutboundBills.FirstOrDefault(r => r.OutboundBillId == item.SourceId);
                    if(tempStruct != null) {
                        item.InvoiceType = tempStruct.InvoiceType;
                        item.ERPSourceOrderCode = tempStruct.ERPSourceOrderCode;
                        item.InvoiceCustomerName = tempStruct.InvoiceCustomerName;
                    }
                }
                var dbDetails = ObjectContext.PartsSalesSettlementDetails.Where(e => e.PartsSalesSettlementId == id).ToArray();
            }
            return dbPartsSalesSettlement;
        }

        public IQueryable<PartsSalesSettlement> 查询配件销售结算单() {
            var userInfo = Utils.GetCurrentUserInfo();
            var company = ObjectContext.Companies.SingleOrDefault(r => r.Id == userInfo.EnterpriseId && r.Status == (int)DcsMasterDataStatus.有效);
            if(company == null) {
                return null;
            }
            IQueryable<PartsSalesSettlement> result;
            if(company.Type == (int)DcsCompanyType.分公司) {
                result = ObjectContext.PartsSalesSettlements.Where(r => ObjectContext.PersonSalesCenterLinks.Any(v => v.PartsSalesCategoryId == r.PartsSalesCategoryId && v.PersonId == userInfo.Id)).OrderBy(r => r.Id);
            } else {
                result = ObjectContext.PartsSalesSettlements.OrderBy(r => r.Id);
            }
            return result;
        }

        public PartsSalesSettlementEx 查询计划价合计(int id) {
            var queryOfOutbound = from a in this.ObjectContext.PartsSalesSettlementRefs.Where(r => r.SourceType == 1)
                                  from b in this.ObjectContext.PartsOutboundBillDetails
                                  where a.SourceId == b.PartsOutboundBillId
                                  select new {
                                      settlementId = a.PartsSalesSettlementId,
                                      bid = b.Id,
                                      a.SourceType,
                                      tempAmont = b.CostPrice * b.OutboundAmount
                                  };
            var queryOfInbound = from a in this.ObjectContext.PartsSalesSettlementRefs.Where(r => r.SourceType == 5)
                                 from b in this.ObjectContext.PartsInboundCheckBillDetails
                                 where a.SourceId == b.PartsInboundCheckBillId
                                 select new {
                                     settlementId = a.PartsSalesSettlementId,
                                     bid = b.Id,
                                     a.SourceType,
                                     tempAmont = (-1) * b.CostPrice * b.InspectedQuantity
                                 };
            var tempStructQuery = from a in (queryOfOutbound.Union(queryOfInbound))
                                  group a by new {
                                      a.settlementId
                                  }
                                      into tempTable
                                      select new {
                                          tempTable.Key.settlementId,
                                          totalAmount = tempTable.Sum(r => r.tempAmont)
                                      };

            var sum = tempStructQuery.Where(r => r.settlementId == id).Sum(r => r.totalAmount);
            return new PartsSalesSettlementEx {
                PlannedPriceTotalAmount = sum
            };
        }

        public IQueryable<PartsSalesSettlementEx> 查询配件销售结算单统计计划价合计() {
            #region 客户企业类型为服务站或服务站兼代理库，业务编码赋值为经销商管理信息对应的业务编码
            var result1 = from a in ObjectContext.PartsSalesSettlements.Where(r => r.IsUnifiedSettle == null || r.IsUnifiedSettle == false)
                          join c in ObjectContext.Companies.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && (r.Type == (int)DcsCompanyType.服务站 || r.Type == (int)DcsCompanyType.服务站兼代理库)) on a.CustomerCompanyId equals c.Id
                          join d in ObjectContext.DealerServiceInfoes.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on new {
                              k1 = a.CustomerCompanyId,
                              k2 = a.PartsSalesCategoryId
                          } equals new {
                              k1 = d.DealerId,
                              k2 = d.PartsSalesCategoryId
                          } into tempTable
                          from ss in tempTable.DefaultIfEmpty()
                          join e in ObjectContext.SalesInvoice_tmp on a.Code equals e.VoucherCode into invoice
                          from ee in invoice.DefaultIfEmpty()
                          select new PartsSalesSettlementEx {
                              Id = a.Id,
                              Code = a.Code,
                              SettlementPath = a.SettlementPath,
                              SalesCompanyId = a.SalesCompanyId,
                              SalesCompanyCode = a.SalesCompanyCode,
                              SalesCompanyName = a.SalesCompanyName,
                              WarehouseId = a.WarehouseId,
                              PartsSalesCategoryId = a.PartsSalesCategoryId,
                              PartsSalesCategoryName = a.PartsSalesCategoryName,
                              AccountGroupId = a.AccountGroupId,
                              AccountGroupCode = a.AccountGroupCode,
                              AccountGroupName = a.AccountGroupName,
                              CustomerCompanyId = a.CustomerCompanyId,
                              CustomerCompanyCode = a.CustomerCompanyCode,
                              CustomerCompanyName = a.CustomerCompanyName,
                              CustomerAccountId = a.CustomerAccountId,
                              TotalSettlementAmount = a.TotalSettlementAmount,
                              RebateMethod = a.RebateMethod,
                              RebateAmount = a.RebateAmount,
                              InvoiceAmountDifference = a.InvoiceAmountDifference,
                              OffsettedSettlementBillId = a.OffsettedSettlementBillId,
                              OffsettedSettlementBillCode = a.OffsettedSettlementBillCode,
                              TaxRate = a.TaxRate,
                              Tax = a.Tax,
                              Status = a.Status,
                              Remark = a.Remark,
                              CreatorId = a.CreatorId,
                              CreatorName = a.CreatorName,
                              CreateTime = a.CreateTime,
                              ModifierId = a.ModifierId,
                              ModifierName = a.ModifierName,
                              ModifyTime = a.ModifyTime,
                              AbandonerId = a.AbandonerId,
                              AbandonerName = a.AbandonerName,
                              AbandonTime = a.AbandonTime,
                              InvoiceRegistrationOperatorId = a.InvoiceRegistrationOperatorId,
                              InvoiceRegistrationOperator = a.InvoiceRegistrationOperator,
                              InvoiceRegistrationTime = a.InvoiceRegistrationTime,
                              ApproverId = a.ApproverId,
                              ApproverName = a.ApproverName,
                              ApproveTime = a.ApproveTime,
                              BusinessCode = ss.BusinessCode,
                              IsUnifiedSettle = a.IsUnifiedSettle,
                              InvoiceDate = a.InvoiceDate,
                              SettleType = a.SettleType,
                              BusinessType = a.BusinessType,
                              InvoiceCode=ee.InvoiceCode
                          };
            #endregion
            #region 客户企业类型为代理库 业务编码赋值为客户企业对应的销售组织的业务编码
            var result2 = from a in ObjectContext.PartsSalesSettlements.Where(r => r.IsUnifiedSettle == null || r.IsUnifiedSettle == false)
                          join b in ObjectContext.Companies.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.Type == (int)DcsCompanyType.代理库) on a.CustomerCompanyId equals b.Id
                          join c in ObjectContext.SalesUnits.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on new {
                              k1 = a.CustomerCompanyId,
                              k2 = a.PartsSalesCategoryId
                          } equals new {
                              k1 = c.OwnerCompanyId,
                              k2 = c.PartsSalesCategoryId
                          } into tempTable
                          from ss in tempTable.DefaultIfEmpty()
                          join e in ObjectContext.SalesInvoice_tmp on a.Code equals e.VoucherCode into invoice
                          from ee in invoice.DefaultIfEmpty()
                          select new PartsSalesSettlementEx {
                              Id = a.Id,
                              Code = a.Code,
                              SettlementPath = a.SettlementPath,
                              SalesCompanyId = a.SalesCompanyId,
                              SalesCompanyCode = a.SalesCompanyCode,
                              SalesCompanyName = a.SalesCompanyName,
                              WarehouseId = a.WarehouseId,
                              PartsSalesCategoryId = a.PartsSalesCategoryId,
                              PartsSalesCategoryName = a.PartsSalesCategoryName,
                              AccountGroupId = a.AccountGroupId,
                              AccountGroupCode = a.AccountGroupCode,
                              AccountGroupName = a.AccountGroupName,
                              CustomerCompanyId = a.CustomerCompanyId,
                              CustomerCompanyCode = a.CustomerCompanyCode,
                              CustomerCompanyName = a.CustomerCompanyName,
                              CustomerAccountId = a.CustomerAccountId,
                              TotalSettlementAmount = a.TotalSettlementAmount,
                              RebateMethod = a.RebateMethod,
                              RebateAmount = a.RebateAmount,
                              InvoiceAmountDifference = a.InvoiceAmountDifference,
                              OffsettedSettlementBillId = a.OffsettedSettlementBillId,
                              OffsettedSettlementBillCode = a.OffsettedSettlementBillCode,
                              TaxRate = a.TaxRate,
                              Tax = a.Tax,
                              Status = a.Status,
                              Remark = a.Remark,
                              CreatorId = a.CreatorId,
                              CreatorName = a.CreatorName,
                              CreateTime = a.CreateTime,
                              ModifierId = a.ModifierId,
                              ModifierName = a.ModifierName,
                              ModifyTime = a.ModifyTime,
                              AbandonerId = a.AbandonerId,
                              AbandonerName = a.AbandonerName,
                              AbandonTime = a.AbandonTime,
                              InvoiceRegistrationOperatorId = a.InvoiceRegistrationOperatorId,
                              InvoiceRegistrationOperator = a.InvoiceRegistrationOperator,
                              InvoiceRegistrationTime = a.InvoiceRegistrationTime,
                              ApproverId = a.ApproverId,
                              ApproverName = a.ApproverName,
                              ApproveTime = a.ApproveTime,
                              BusinessCode = ss.BusinessCode,
                              IsUnifiedSettle = a.IsUnifiedSettle,
                              InvoiceDate = a.InvoiceDate,
                              SettleType = a.SettleType,
                              BusinessType = a.BusinessType,
                              InvoiceCode=ee.InvoiceCode
                          };
            #endregion
            #region 客户企业类型为其他,业务编码赋值为空
            var exceptCompanyType = new int[] { (int)DcsCompanyType.代理库, (int)DcsCompanyType.服务站, (int)DcsCompanyType.服务站兼代理库 };
            var result3 = from a in ObjectContext.PartsSalesSettlements.Where(r => r.IsUnifiedSettle == null || r.IsUnifiedSettle == false)
                          join b in ObjectContext.Companies.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && (!exceptCompanyType.Contains(r.Type))) on a.CustomerCompanyId equals b.Id
                          join e in ObjectContext.SalesInvoice_tmp on a.Code equals e.VoucherCode into invoice
                          from ee in invoice.DefaultIfEmpty()
                          select new PartsSalesSettlementEx {
                              Id = a.Id,
                              Code = a.Code,
                              SettlementPath = a.SettlementPath,
                              SalesCompanyId = a.SalesCompanyId,
                              SalesCompanyCode = a.SalesCompanyCode,
                              SalesCompanyName = a.SalesCompanyName,
                              WarehouseId = a.WarehouseId,
                              PartsSalesCategoryId = a.PartsSalesCategoryId,
                              PartsSalesCategoryName = a.PartsSalesCategoryName,
                              AccountGroupId = a.AccountGroupId,
                              AccountGroupCode = a.AccountGroupCode,
                              AccountGroupName = a.AccountGroupName,
                              CustomerCompanyId = a.CustomerCompanyId,
                              CustomerCompanyCode = a.CustomerCompanyCode,
                              CustomerCompanyName = a.CustomerCompanyName,
                              CustomerAccountId = a.CustomerAccountId,
                              TotalSettlementAmount = a.TotalSettlementAmount,
                              RebateMethod = a.RebateMethod,
                              RebateAmount = a.RebateAmount,
                              InvoiceAmountDifference = a.InvoiceAmountDifference,
                              OffsettedSettlementBillId = a.OffsettedSettlementBillId,
                              OffsettedSettlementBillCode = a.OffsettedSettlementBillCode,
                              TaxRate = a.TaxRate,
                              Tax = a.Tax,
                              Status = a.Status,
                              Remark = a.Remark,
                              CreatorId = a.CreatorId,
                              CreatorName = a.CreatorName,
                              CreateTime = a.CreateTime,
                              ModifierId = a.ModifierId,
                              ModifierName = a.ModifierName,
                              ModifyTime = a.ModifyTime,
                              AbandonerId = a.AbandonerId,
                              AbandonerName = a.AbandonerName,
                              AbandonTime = a.AbandonTime,
                              InvoiceRegistrationOperatorId = a.InvoiceRegistrationOperatorId,
                              InvoiceRegistrationOperator = a.InvoiceRegistrationOperator,
                              InvoiceRegistrationTime = a.InvoiceRegistrationTime,
                              ApproverId = a.ApproverId,
                              ApproverName = a.ApproverName,
                              ApproveTime = a.ApproveTime,
                              BusinessCode = "",
                              IsUnifiedSettle = a.IsUnifiedSettle,
                              InvoiceDate = a.InvoiceDate,
                              SettleType = a.SettleType,
                              BusinessType = a.BusinessType,
                              InvoiceCode=ee.InvoiceCode
                          };
            #endregion
            var tempResult = result1.Union(result2).Union(result3);
            var userInfo = Utils.GetCurrentUserInfo();
            tempResult = tempResult.Where(r => r.SalesCompanyId == userInfo.EnterpriseId);
            var company = ObjectContext.Companies.SingleOrDefault(r => r.Id == userInfo.EnterpriseId && r.Status == (int)DcsMasterDataStatus.有效);
            if(company == null) {
                return null;
            }
            IQueryable<PartsSalesSettlementEx> result;
            if(company.Type == (int)DcsCompanyType.分公司) {
                result = tempResult.Where(r => ObjectContext.PersonSalesCenterLinks.Any(v => v.PartsSalesCategoryId == r.PartsSalesCategoryId && v.PersonId == userInfo.Id)).OrderBy(r => r.Id);
            } else {
                result = tempResult.OrderBy(r => r.Id);
            }
            return result;
        }

        public IQueryable<PartsSalesSettlementEx> 查询配件销售结算单统计计划价合计采埃孚() {
            #region 客户企业类型为服务站或服务站兼代理库，业务编码赋值为经销商管理信息对应的业务编码
            var result1 = from a in ObjectContext.PartsSalesSettlements
                          join c in ObjectContext.Companies.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && (r.Type == (int)DcsCompanyType.服务站 || r.Type == (int)DcsCompanyType.服务站兼代理库)) on a.CustomerCompanyId equals c.Id
                          join d in ObjectContext.DealerServiceInfoes.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on new {
                              k1 = a.CustomerCompanyId,
                              k2 = a.PartsSalesCategoryId
                          } equals new {
                              k1 = d.DealerId,
                              k2 = d.PartsSalesCategoryId
                          } into tempTable
                          from ss in tempTable.DefaultIfEmpty()
                          select new PartsSalesSettlementEx {
                              Id = a.Id,
                              Code = a.Code,
                              SettlementPath = a.SettlementPath,
                              SalesCompanyId = a.SalesCompanyId,
                              SalesCompanyCode = a.SalesCompanyCode,
                              SalesCompanyName = a.SalesCompanyName,
                              WarehouseId = a.WarehouseId,
                              PartsSalesCategoryId = a.PartsSalesCategoryId,
                              PartsSalesCategoryName = a.PartsSalesCategoryName,
                              AccountGroupId = a.AccountGroupId,
                              AccountGroupCode = a.AccountGroupCode,
                              AccountGroupName = a.AccountGroupName,
                              CustomerCompanyId = a.CustomerCompanyId,
                              CustomerCompanyCode = a.CustomerCompanyCode,
                              CustomerCompanyName = a.CustomerCompanyName,
                              CustomerAccountId = a.CustomerAccountId,
                              TotalSettlementAmount = a.TotalSettlementAmount,
                              RebateMethod = a.RebateMethod,
                              RebateAmount = a.RebateAmount,
                              InvoiceAmountDifference = a.InvoiceAmountDifference,
                              OffsettedSettlementBillId = a.OffsettedSettlementBillId,
                              OffsettedSettlementBillCode = a.OffsettedSettlementBillCode,
                              TaxRate = a.TaxRate,
                              Tax = a.Tax,
                              Status = a.Status,
                              Remark = a.Remark,
                              CreatorId = a.CreatorId,
                              CreatorName = a.CreatorName,
                              CreateTime = a.CreateTime,
                              ModifierId = a.ModifierId,
                              ModifierName = a.ModifierName,
                              ModifyTime = a.ModifyTime,
                              AbandonerId = a.AbandonerId,
                              AbandonerName = a.AbandonerName,
                              AbandonTime = a.AbandonTime,
                              InvoiceRegistrationOperatorId = a.InvoiceRegistrationOperatorId,
                              InvoiceRegistrationOperator = a.InvoiceRegistrationOperator,
                              InvoiceRegistrationTime = a.InvoiceRegistrationTime,
                              ApproverId = a.ApproverId,
                              ApproverName = a.ApproverName,
                              ApproveTime = a.ApproveTime,
                              BusinessCode = ss.BusinessCode,
                              IsUnifiedSettle = a.IsUnifiedSettle
                          };
            #endregion
            #region 客户企业类型为代理库 业务编码赋值为客户企业对应的销售组织的业务编码
            var result2 = from a in ObjectContext.PartsSalesSettlements
                          join b in ObjectContext.Companies.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.Type == (int)DcsCompanyType.代理库) on a.CustomerCompanyId equals b.Id
                          join c in ObjectContext.SalesUnits.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on new {
                              k1 = a.CustomerCompanyId,
                              k2 = a.PartsSalesCategoryId
                          } equals new {
                              k1 = c.OwnerCompanyId,
                              k2 = c.PartsSalesCategoryId
                          } into tempTable
                          from ss in tempTable.DefaultIfEmpty()
                          select new PartsSalesSettlementEx {
                              Id = a.Id,
                              Code = a.Code,
                              SettlementPath = a.SettlementPath,
                              SalesCompanyId = a.SalesCompanyId,
                              SalesCompanyCode = a.SalesCompanyCode,
                              SalesCompanyName = a.SalesCompanyName,
                              WarehouseId = a.WarehouseId,
                              PartsSalesCategoryId = a.PartsSalesCategoryId,
                              PartsSalesCategoryName = a.PartsSalesCategoryName,
                              AccountGroupId = a.AccountGroupId,
                              AccountGroupCode = a.AccountGroupCode,
                              AccountGroupName = a.AccountGroupName,
                              CustomerCompanyId = a.CustomerCompanyId,
                              CustomerCompanyCode = a.CustomerCompanyCode,
                              CustomerCompanyName = a.CustomerCompanyName,
                              CustomerAccountId = a.CustomerAccountId,
                              TotalSettlementAmount = a.TotalSettlementAmount,
                              RebateMethod = a.RebateMethod,
                              RebateAmount = a.RebateAmount,
                              InvoiceAmountDifference = a.InvoiceAmountDifference,
                              OffsettedSettlementBillId = a.OffsettedSettlementBillId,
                              OffsettedSettlementBillCode = a.OffsettedSettlementBillCode,
                              TaxRate = a.TaxRate,
                              Tax = a.Tax,
                              Status = a.Status,
                              Remark = a.Remark,
                              CreatorId = a.CreatorId,
                              CreatorName = a.CreatorName,
                              CreateTime = a.CreateTime,
                              ModifierId = a.ModifierId,
                              ModifierName = a.ModifierName,
                              ModifyTime = a.ModifyTime,
                              AbandonerId = a.AbandonerId,
                              AbandonerName = a.AbandonerName,
                              AbandonTime = a.AbandonTime,
                              InvoiceRegistrationOperatorId = a.InvoiceRegistrationOperatorId,
                              InvoiceRegistrationOperator = a.InvoiceRegistrationOperator,
                              InvoiceRegistrationTime = a.InvoiceRegistrationTime,
                              ApproverId = a.ApproverId,
                              ApproverName = a.ApproverName,
                              ApproveTime = a.ApproveTime,
                              BusinessCode = ss.BusinessCode,
                              IsUnifiedSettle = a.IsUnifiedSettle
                          };
            #endregion
            #region 客户企业类型为其他,业务编码赋值为空
            var exceptCompanyType = new int[] { (int)DcsCompanyType.代理库, (int)DcsCompanyType.服务站, (int)DcsCompanyType.服务站兼代理库 };
            var result3 = from a in ObjectContext.PartsSalesSettlements
                          join b in ObjectContext.Companies.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && (!exceptCompanyType.Contains(r.Type))) on a.CustomerCompanyId equals b.Id
                          select new PartsSalesSettlementEx {
                              Id = a.Id,
                              Code = a.Code,
                              SettlementPath = a.SettlementPath,
                              SalesCompanyId = a.SalesCompanyId,
                              SalesCompanyCode = a.SalesCompanyCode,
                              SalesCompanyName = a.SalesCompanyName,
                              WarehouseId = a.WarehouseId,
                              PartsSalesCategoryId = a.PartsSalesCategoryId,
                              PartsSalesCategoryName = a.PartsSalesCategoryName,
                              AccountGroupId = a.AccountGroupId,
                              AccountGroupCode = a.AccountGroupCode,
                              AccountGroupName = a.AccountGroupName,
                              CustomerCompanyId = a.CustomerCompanyId,
                              CustomerCompanyCode = a.CustomerCompanyCode,
                              CustomerCompanyName = a.CustomerCompanyName,
                              CustomerAccountId = a.CustomerAccountId,
                              TotalSettlementAmount = a.TotalSettlementAmount,
                              RebateMethod = a.RebateMethod,
                              RebateAmount = a.RebateAmount,
                              InvoiceAmountDifference = a.InvoiceAmountDifference,
                              OffsettedSettlementBillId = a.OffsettedSettlementBillId,
                              OffsettedSettlementBillCode = a.OffsettedSettlementBillCode,
                              TaxRate = a.TaxRate,
                              Tax = a.Tax,
                              Status = a.Status,
                              Remark = a.Remark,
                              CreatorId = a.CreatorId,
                              CreatorName = a.CreatorName,
                              CreateTime = a.CreateTime,
                              ModifierId = a.ModifierId,
                              ModifierName = a.ModifierName,
                              ModifyTime = a.ModifyTime,
                              AbandonerId = a.AbandonerId,
                              AbandonerName = a.AbandonerName,
                              AbandonTime = a.AbandonTime,
                              InvoiceRegistrationOperatorId = a.InvoiceRegistrationOperatorId,
                              InvoiceRegistrationOperator = a.InvoiceRegistrationOperator,
                              InvoiceRegistrationTime = a.InvoiceRegistrationTime,
                              ApproverId = a.ApproverId,
                              ApproverName = a.ApproverName,
                              ApproveTime = a.ApproveTime,
                              BusinessCode = "",
                              IsUnifiedSettle = a.IsUnifiedSettle
                          };
            #endregion
            var tempResult = result1.Union(result2).Union(result3);
            var userInfo = Utils.GetCurrentUserInfo();
            var company = ObjectContext.Companies.SingleOrDefault(r => r.Id == userInfo.EnterpriseId && r.Status == (int)DcsMasterDataStatus.有效);
            if(company == null) {
                return null;
            }
            IQueryable<PartsSalesSettlementEx> result;
            if(company.Type == (int)DcsCompanyType.分公司) {
                result = tempResult.Where(r => ObjectContext.PersonSalesCenterLinks.Any(v => v.PartsSalesCategoryId == r.PartsSalesCategoryId && v.PersonId == userInfo.Id)).OrderBy(r => r.Id);
            } else {
                result = tempResult.OrderBy(r => r.Id);
            }
            return result;
        }

        public IQueryable<VirtualPartsSalesSettlement> 查询虚拟配件销售结算成本(int partsSalesSettlementId) {
            var partsSalesSettlements = from partsSalesSettlement in ObjectContext.PartsSalesSettlements.Where(r => r.Id == partsSalesSettlementId)
                                        from partsSalesSettlementRef in ObjectContext.PartsSalesSettlementRefs
                                        from partsOutboundBill in ObjectContext.PartsOutboundBills
                                        from partsOutboundBillDetail in ObjectContext.PartsOutboundBillDetails
                                        from sparePart in ObjectContext.SpareParts
                                        where sparePart.Id == partsOutboundBillDetail.SparePartId && partsSalesSettlementRef.PartsSalesSettlementId == partsSalesSettlement.Id && partsSalesSettlementRef.SourceId == partsOutboundBill.Id && partsOutboundBillDetail.PartsOutboundBillId == partsOutboundBill.Id && partsSalesSettlementRef.SourceType == (int)DcsPartsSalesSettlementRefSourceType.配件出库单
                                        select new VirtualPartsSalesSettlement {
                                            Id = partsSalesSettlement.Id,
                                            Code = partsSalesSettlement.Code,
                                            BillCode = partsOutboundBill.Code,
                                            BillBusinessType = "出库",
                                            SparePartId = sparePart.Id,
                                            SparePartCode = sparePart.Code,
                                            SparePartName = sparePart.Name,
                                            Quantity = partsOutboundBillDetail.OutboundAmount,
                                            SettlementPrice = partsOutboundBillDetail.SettlementPrice,
                                            SettlementAmount = partsOutboundBillDetail.SettlementPrice * partsOutboundBillDetail.OutboundAmount,
                                            PlanPrice = partsOutboundBillDetail.CostPrice,
                                            PlanAmount = partsOutboundBillDetail.CostPrice * partsOutboundBillDetail.OutboundAmount,
                                            MaterialCostVariance = partsOutboundBillDetail.SettlementPrice * partsOutboundBillDetail.OutboundAmount - partsOutboundBillDetail.CostPrice * partsOutboundBillDetail.OutboundAmount,
                                        };
            var partsSalesSettlementss = from partsSalesSettlement in ObjectContext.PartsSalesSettlements.Where(r => r.Id == partsSalesSettlementId)
                                         from partsSalesSettlementRef in ObjectContext.PartsSalesSettlementRefs
                                         from partsInboundCheckBill in ObjectContext.PartsInboundCheckBills
                                         from partsInboundCheckBillDetail in ObjectContext.PartsInboundCheckBillDetails
                                         from sparePart in ObjectContext.SpareParts
                                         where sparePart.Id == partsInboundCheckBillDetail.SparePartId && partsSalesSettlementRef.PartsSalesSettlementId == partsSalesSettlement.Id && partsSalesSettlementRef.SourceId == partsInboundCheckBill.Id && partsInboundCheckBillDetail.PartsInboundCheckBillId == partsInboundCheckBill.Id && partsSalesSettlementRef.SourceType == (int)DcsPartsSalesSettlementRefSourceType.配件入库检验单
                                         select new VirtualPartsSalesSettlement {
                                             Id = partsSalesSettlement.Id,
                                             Code = partsSalesSettlement.Code,
                                             BillCode = partsInboundCheckBill.Code,
                                             BillBusinessType = "入库",
                                             SparePartId = sparePart.Id,
                                             SparePartCode = sparePart.Code,
                                             SparePartName = sparePart.Name,
                                             Quantity = partsInboundCheckBillDetail.InspectedQuantity,
                                             SettlementPrice = partsInboundCheckBillDetail.SettlementPrice,
                                             SettlementAmount = partsInboundCheckBillDetail.SettlementPrice * partsInboundCheckBillDetail.InspectedQuantity,
                                             PlanPrice = partsInboundCheckBillDetail.CostPrice,
                                             PlanAmount = partsInboundCheckBillDetail.CostPrice * partsInboundCheckBillDetail.InspectedQuantity,
                                             MaterialCostVariance = partsInboundCheckBillDetail.SettlementPrice * partsInboundCheckBillDetail.InspectedQuantity - partsInboundCheckBillDetail.CostPrice * partsInboundCheckBillDetail.InspectedQuantity,
                                         };
            return partsSalesSettlements.Union(partsSalesSettlementss).OrderBy(e => e.Code).ThenBy(r => r.BillCode).ThenBy(r => r.SparePartCode);
        }

        public IQueryable<PartsSalesSettlementDetail> GetPartsSalesSettlementDetailWithSparePart() {
            return ObjectContext.PartsSalesSettlementDetails.Include("SparePart").OrderBy(v => v.Id);
        }
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:35
        //原分布类的函数全部转移到Ach                                                               

        public void InsertPartsSalesSettlement(PartsSalesSettlement partsSalesSettlement) {
            new PartsSalesSettlementAch(this).InsertPartsSalesSettlement(partsSalesSettlement);
        }

        public void UpdatePartsSalesSettlement(PartsSalesSettlement partsSalesSettlement) {
            new PartsSalesSettlementAch(this).UpdatePartsSalesSettlement(partsSalesSettlement);
        }

        public PartsSalesSettlement GetPartsSalesSettlementWithDetailsById(int id) {
            return new PartsSalesSettlementAch(this).GetPartsSalesSettlementWithDetailsById(id);
        }

        public IQueryable<PartsSalesSettlement> 查询配件销售结算单() {
            return new PartsSalesSettlementAch(this).查询配件销售结算单();
        }

        public PartsSalesSettlementEx 查询计划价合计(int id) {
            return new PartsSalesSettlementAch(this).查询计划价合计(id);
        }

        public IQueryable<PartsSalesSettlementEx> 查询配件销售结算单统计计划价合计() {
            return new PartsSalesSettlementAch(this).查询配件销售结算单统计计划价合计();
        }

        public IQueryable<PartsSalesSettlementEx> 查询配件销售结算单统计计划价合计采埃孚() {
            return new PartsSalesSettlementAch(this).查询配件销售结算单统计计划价合计采埃孚();
        }

        public IQueryable<VirtualPartsSalesSettlement> 查询虚拟配件销售结算成本(int partsSalesSettlementId) {
            return new PartsSalesSettlementAch(this).查询虚拟配件销售结算成本(partsSalesSettlementId);
        }
        public IQueryable<PartsSalesSettlementDetail> GetPartsSalesSettlementDetailWithSparePart() {
            return new PartsSalesSettlementAch(this).GetPartsSalesSettlementDetailWithSparePart();
        }
    }
}
