﻿

using System.Linq;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PreOrderAch : DcsSerivceAchieveBase {
        public void UpdatePreOrder(PreOrder preOrder) {
            UpdateToDatabase(preOrder);
        }

        public IQueryable<PreOrder> GetPreOrdersByStatus() {
            return ObjectContext.PreOrders.Where(r => r.Status != (int)DcsPreOrderStatus.作废).OrderBy(e => e.Id);
        }

    
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:35
        //原分布类的函数全部转移到Ach                                                               
        

        public void UpdatePreOrder(PreOrder preOrder) {
            new PreOrderAch(this).UpdatePreOrder(preOrder);
        }

        public IQueryable<PreOrder> GetPreOrdersByStatus() {
            return new PreOrderAch(this).GetPreOrdersByStatus();
        }
    }
}
