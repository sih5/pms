﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using System.Text;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class SettlementAutomaticTaskSetAch : DcsSerivceAchieveBase {
        public SettlementAutomaticTaskSetAch(DcsDomainService domainService)
            : base(domainService) {
        }

        internal void InsertSettlementAutomaticTaskSetValidate(SettlementAutomaticTaskSet settlementAutomaticTaskSet) {
            var userInfo = Utils.GetCurrentUserInfo();
            settlementAutomaticTaskSet.CreatorId = userInfo.Id;
            settlementAutomaticTaskSet.CreatorName = userInfo.Name;
            settlementAutomaticTaskSet.CreateTime = DateTime.Now;
        }
        internal void UpdateSettlementAutomaticTaskSetValidate(SettlementAutomaticTaskSet settlementAutomaticTaskSet) {
            var userInfo = Utils.GetCurrentUserInfo();
            settlementAutomaticTaskSet.ModifierId = userInfo.Id;
            settlementAutomaticTaskSet.ModifierName = userInfo.Name;
            settlementAutomaticTaskSet.ModifyTime = DateTime.Now;
        }
        public void InsertSettlementAutomaticTaskSet(SettlementAutomaticTaskSet settlementAutomaticTaskSet) {
            InsertToDatabase(settlementAutomaticTaskSet);
            this.InsertSettlementAutomaticTaskSetValidate(settlementAutomaticTaskSet);
        }
        public void UpdateSettlementAutomaticTaskSet(SettlementAutomaticTaskSet settlementAutomaticTaskSet) {
            UpdateToDatabase(settlementAutomaticTaskSet);
            this.UpdateSettlementAutomaticTaskSetValidate(settlementAutomaticTaskSet);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:35
        //原分布类的函数全部转移到Ach                                                               

        public void InsertSettlementAutomaticTaskSet(SettlementAutomaticTaskSet settlementAutomaticTaskSet) {
            new SettlementAutomaticTaskSetAch(this).InsertSettlementAutomaticTaskSet(settlementAutomaticTaskSet);
        }

        public void UpdateSettlementAutomaticTaskSet(SettlementAutomaticTaskSet settlementAutomaticTaskSet) {
            new SettlementAutomaticTaskSetAch(this).UpdateSettlementAutomaticTaskSet(settlementAutomaticTaskSet);
        }
    }
}
