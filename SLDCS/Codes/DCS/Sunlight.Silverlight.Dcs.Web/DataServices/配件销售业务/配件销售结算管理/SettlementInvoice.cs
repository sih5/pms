﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
using System.Configuration;
using System.Data;
using System.Text;
using Devart.Data.Oracle;
using NPOI.SS.Formula.Functions;
using System.Transactions;
using System.Data.Objects.SqlClient;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class SettlementInvoiceAch : DcsSerivceAchieveBase {
        public SettlementInvoiceAch(DcsDomainService domainService)
            : base(domainService) {
        }
        public IEnumerable<SettlementInvoice> GetSettlementInvoices(DateTime? bOutCreateTime, DateTime? eOutCreateTime, DateTime? bCreateTime, DateTime? eCreateTime, string partsOutboundBillCode, string code, string invoiceNumber, string submitCompanyName) {
            string SQL = @" select * from (select (case
         when cp.type = 3 then
          (select mt.Name
             from MarketingDepartment mt
             join Agency ag
               on mt.id = ag.marketingdepartmentid
            where ag.id = cp.id)
         when cp.type = 2 or cp.type = 7 then
          (select mt.Name
             from MarketingDepartment mt
             join DealerServiceInfo ag
               on mt.id = ag.marketingdepartmentid
            where ag.DealerId = cp.id)
         else
          null
       end) as MarketingDepartmentName,
       wh.name as WarehouseName,
       po.province,
       po.code as PartsSalesOrderCode,
       pa.id,
       pa.PartsOutboundBillId,
       po.submitcompanyname,
       ps.code as PartsOutboundBillCode,
       sp.referencecode,
       pa.sparepartcode,
       pa.sparepartname,
       pb.PurchaseRoute,
       pb.partabc,
       pa.outboundamount,
       pa.CostPrice,
       pa.CostPrice * pa.outboundamount as CostPriceAmount,
       pa.settlementprice,
       pa.settlementprice * pa.outboundamount as SettlementPriceAmount,
       ps.createtime as PartsOutboundBillCreateTime,
       pt.status,
       pt.businesstype,
       pt.remark,
       pt.code,
       (select max(InvoiceNumber)
          from InvoiceInformation ifn
         where ifn.SourceCode = pt.code) as InvoiceNumber,
      pt.CreateTime
  from partssalesorder po
  join partssalesorderdetail pd
    on po.id = pd.partssalesorderid
  join company cp
    on po.SubmitCompanyId = cp.id
  join SalesUnit su
    on po.SalesUnitId = su.id
  join company co
    on su.ownercompanyid = co.id
   and co.type = 1
  left join warehouse wh
    on po.WarehouseId = wh.id
 inner join partsoutboundbill ps
    on ps.originalrequirementbillid = po.id
   and ps.originalrequirementbillcode = po.code
 inner join partsoutboundbilldetail pa
    on ps.id = pa.partsoutboundbillid
   and pa.sparepartid = pd.sparepartid
  join sparepart sp
    on pa.sparepartid = sp.id
  left join partsbranch pb
    on sp.id = pb.partid
   and pb.status = 1
  join PartsSalesSettlementRef pf
    on pf.sourceid = ps.id
   and pf.sourcecode = ps.code
  join PartsSalesSettlement pt
    on pt.id = pf.partssalessettlementid
   and pt.status != 99
   and pt.status != 4
  where 1=1 ";
            if(bOutCreateTime.HasValue && bOutCreateTime.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
                bOutCreateTime = null;
            }
            if(bCreateTime.HasValue && bCreateTime.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
                bCreateTime = null;
            }
            if(bOutCreateTime.HasValue) {
                SQL = SQL + " and ps.CreateTime>= to_date('" + bOutCreateTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if(eOutCreateTime.HasValue) {
                SQL = SQL + " and ps.CreateTime <= to_date('" + eOutCreateTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if(bCreateTime.HasValue) {
                SQL = SQL + " and pt.CreateTime>= to_date('" + bCreateTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if(eCreateTime.HasValue) {
                SQL = SQL + " and pt.CreateTime <= to_date('" + eCreateTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if(!string.IsNullOrEmpty(partsOutboundBillCode)) {
                SQL = SQL + " and LOWER(ps.code) like '%" + partsOutboundBillCode.ToLower() + "%'";
            }

            if(!string.IsNullOrEmpty(submitCompanyName)) {
                SQL = SQL + " and LOWER(po.submitCompanyName) like '%" + submitCompanyName.ToLower() + "%'";
            }
            if(!string.IsNullOrEmpty(code)) {
                SQL = SQL + " and LOWER(pt.code) like '%" + code.ToLower() + "%'";
            }

            if(!string.IsNullOrEmpty(submitCompanyName)) {
                SQL = SQL + " and LOWER(po.submitCompanyName) like '%" + submitCompanyName.ToLower() + "%'";

            }
                SQL = SQL + " )tt where 1=1";

            if(!string.IsNullOrEmpty(invoiceNumber)) {
                SQL = SQL + " and LOWER(tt.invoiceNumber) like '%" + invoiceNumber.ToLower() + "%'";
            }
            //查询当前登录人所属公司性质，中心库、服务站、经销商 自己查自己的报表。
            var userInfo = Utils.GetCurrentUserInfo();

            var company = ObjectContext.Companies.Where(r => r.Id == userInfo.EnterpriseId).FirstOrDefault();
            if(company.Type != (int)DcsCompanyType.分公司) {
                    SQL = SQL + " and  po.SubmitCompanyId=" + userInfo.EnterpriseId;
            }

            var search = ObjectContext.ExecuteStoreQuery<SettlementInvoice>(SQL).ToList();
            return search;
        }
    }
    partial class DcsDomainService {
        public IEnumerable<SettlementInvoice> GetSettlementInvoices(DateTime? bOutCreateTime, DateTime? eOutCreateTime, DateTime? bCreateTime, DateTime? eCreateTime, string partsOutboundBillCode, string code, string invoiceNumber, string submitCompanyName) {
            return new SettlementInvoiceAch(this).GetSettlementInvoices( bOutCreateTime,  eOutCreateTime,  bCreateTime,  eCreateTime,  partsOutboundBillCode,  code,  invoiceNumber,  submitCompanyName);
        }
    }
}
