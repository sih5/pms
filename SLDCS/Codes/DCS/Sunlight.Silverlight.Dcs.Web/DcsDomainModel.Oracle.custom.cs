﻿using System;
using System.Web.Configuration;
using Devart.Data.Oracle;
using Devart.Data.Oracle.Entity.Configuration;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class DcsEntities {
        static readonly OracleMonitor MyMonitor = new OracleMonitor();

        static DcsEntities() {
            var config = OracleEntityProviderConfig.Instance;
            config.Workarounds.IgnoreSchemaName = true;
            config.Workarounds.DisableQuoting = true;
            config.DmlOptions.BatchUpdates.Enabled = true;
            config.DmlOptions.ReuseParameters = true;
            config.DmlOptions.InsertNullBehaviour = InsertNullBehaviour.InsertDefaultOrNull;

            bool isActive;
            Boolean.TryParse(WebConfigurationManager.AppSettings["OracleMonitorIsActive"], out isActive);
            MyMonitor.IsActive = isActive;
        }
    }
}
