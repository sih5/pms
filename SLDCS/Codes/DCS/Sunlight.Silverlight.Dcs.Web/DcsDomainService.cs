﻿
using System.Data;
using System.Data.EntityClient;
using System.Data.Objects;
using System.Data.Objects.DataClasses;
using System.Linq;
using System.ServiceModel.DomainServices.EntityFramework;
using System.ServiceModel.DomainServices.Hosting;
using System.ServiceModel.DomainServices.Server;
using Sunlight.Silverlight.Shell.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    [EnableClientAccess]
    //[IdmRequiresAuthentication]
    public sealed partial class DcsDomainService : LinqToEntitiesDomainService<DcsEntities> {
        protected override DcsEntities CreateObjectContext() {
            var connString = DomainServiceHelper.CreateEntityConnectionString("Dcs");
            return connString == null ? base.CreateObjectContext() : new DcsEntities(new EntityConnection(connString));
        }

        internal void InsertToDatabase(EntityObject entity) {
            if(entity.EntityState != EntityState.Detached)
                ObjectContext.ObjectStateManager.ChangeObjectState(entity, EntityState.Added);
            else {
                switch(entity.GetType().Name) {
                    case "ABCSetting":
                        ObjectContext.ABCSettings.AddObject((ABCSetting)entity);
                        break;
                    case "ABCStrategy":
                        ObjectContext.ABCStrategies.AddObject((ABCStrategy)entity);
                        break;
                    case "ABCTypeSafeDay":
                        ObjectContext.ABCTypeSafeDays.AddObject((ABCTypeSafeDay)entity);
                        break;
                    case "ACCESSINFO_TMP":
                        ObjectContext.ACCESSINFO_TMP.AddObject((ACCESSINFO_TMP)entity);
                        break;
                    case "AccountGroup":
                        ObjectContext.AccountGroups.AddObject((AccountGroup)entity);
                        break;
                    case "AccountPayableHistoryDetail":
                        ObjectContext.AccountPayableHistoryDetails.AddObject((AccountPayableHistoryDetail)entity);
                        break;
                    case "AccountPeriod":
                        ObjectContext.AccountPeriods.AddObject((AccountPeriod)entity);
                        break;
                    case "AccurateTrace":
                        ObjectContext.AccurateTraces.AddObject((AccurateTrace)entity);
                        break;
                    case "Agency":
                        ObjectContext.Agencies.AddObject((Agency)entity);
                        break;
                    case "AgencyAffiBranch":
                        ObjectContext.AgencyAffiBranches.AddObject((AgencyAffiBranch)entity);
                        break;
                    case "AgencyDealerRelation":
                        ObjectContext.AgencyDealerRelations.AddObject((AgencyDealerRelation)entity);
                        break;
                    case "AgencyDealerRelationHistory":
                        ObjectContext.AgencyDealerRelationHistories.AddObject((AgencyDealerRelationHistory)entity);
                        break;
                    case "AgencyDifferenceBackBill":
                        ObjectContext.AgencyDifferenceBackBills.AddObject((AgencyDifferenceBackBill)entity);
                        break;
                    case "AgencyLogisticCompany":
                        ObjectContext.AgencyLogisticCompanies.AddObject((AgencyLogisticCompany)entity);
                        break;
                    case "AgencyPartsOutboundBill":
                        ObjectContext.AgencyPartsOutboundBills.AddObject((AgencyPartsOutboundBill)entity);
                        break;
                    case "AgencyPartsOutboundPlan":
                        ObjectContext.AgencyPartsOutboundPlans.AddObject((AgencyPartsOutboundPlan)entity);
                        break;
                    case "AgencyPartsShippingOrder":
                        ObjectContext.AgencyPartsShippingOrders.AddObject((AgencyPartsShippingOrder)entity);
                        break;
                    case "AgencyPartsShippingOrderRef":
                        ObjectContext.AgencyPartsShippingOrderRefs.AddObject((AgencyPartsShippingOrderRef)entity);
                        break;
                    case "AgencyRetailerList":
                        ObjectContext.AgencyRetailerLists.AddObject((AgencyRetailerList)entity);
                        break;
                    case "AgencyRetailerOrder":
                        ObjectContext.AgencyRetailerOrders.AddObject((AgencyRetailerOrder)entity);
                        break;
                    case "AgencyStockQryFrmOrder":
                        ObjectContext.AgencyStockQryFrmOrders.AddObject((AgencyStockQryFrmOrder)entity);
                        break;
                    case "APartsOutboundBillDetail":
                        ObjectContext.APartsOutboundBillDetails.AddObject((APartsOutboundBillDetail)entity);
                        break;
                    case "APartsOutboundPlanDetail":
                        ObjectContext.APartsOutboundPlanDetails.AddObject((APartsOutboundPlanDetail)entity);
                        break;
                    case "APartsShippingOrderDetail":
                        ObjectContext.APartsShippingOrderDetails.AddObject((APartsShippingOrderDetail)entity);
                        break;
                    case "ApplicationType":
                        ObjectContext.ApplicationTypes.AddObject((ApplicationType)entity);
                        break;
                    case "AppointDealerDtl":
                        ObjectContext.AppointDealerDtls.AddObject((AppointDealerDtl)entity);
                        break;
                    case "AppointFaultReasonDtl":
                        ObjectContext.AppointFaultReasonDtls.AddObject((AppointFaultReasonDtl)entity);
                        break;
                    case "AppointSupplierDtl":
                        ObjectContext.AppointSupplierDtls.AddObject((AppointSupplierDtl)entity);
                        break;
                    case "AssemblyPartRequisitionLink":
                        ObjectContext.AssemblyPartRequisitionLinks.AddObject((AssemblyPartRequisitionLink)entity);
                        break;
                    case "AuthenticationType":
                        ObjectContext.AuthenticationTypes.AddObject((AuthenticationType)entity);
                        break;
                    case "AutoSalesOrderProcess":
                        ObjectContext.AutoSalesOrderProcesses.AddObject((AutoSalesOrderProcess)entity);
                        break;
                    case "AutoTaskExecutResult":
                        ObjectContext.AutoTaskExecutResults.AddObject((AutoTaskExecutResult)entity);
                        break;
                    case "BankAccount":
                        ObjectContext.BankAccounts.AddObject((BankAccount)entity);
                        break;
                    case "BonusPointsOrder":
                        ObjectContext.BonusPointsOrders.AddObject((BonusPointsOrder)entity);
                        break;
                    case "BonusPointsOrderList":
                        ObjectContext.BonusPointsOrderLists.AddObject((BonusPointsOrderList)entity);
                        break;
                    case "BonusPointsSummary":
                        ObjectContext.BonusPointsSummaries.AddObject((BonusPointsSummary)entity);
                        break;
                    case "BonusPointsSummaryList":
                        ObjectContext.BonusPointsSummaryLists.AddObject((BonusPointsSummaryList)entity);
                        break;
                    case "BorrowBill":
                        ObjectContext.BorrowBills.AddObject((BorrowBill)entity);
                        break;
                    case "BorrowBillDetail":
                        ObjectContext.BorrowBillDetails.AddObject((BorrowBillDetail)entity);
                        break;
                    case "BottomStock":
                        ObjectContext.BottomStocks.AddObject((BottomStock)entity);
                        break;
                    case "BottomStockColVersion":
                        ObjectContext.BottomStockColVersions.AddObject((BottomStockColVersion)entity);
                        break;
                    case "BottomStockForceReserveSub":
                        ObjectContext.BottomStockForceReserveSubs.AddObject((BottomStockForceReserveSub)entity);
                        break;
                    case "BottomStockForceReserveType":
                        ObjectContext.BottomStockForceReserveTypes.AddObject((BottomStockForceReserveType)entity);
                        break;
                    case "BottomStockSettleTable":
                        ObjectContext.BottomStockSettleTables.AddObject((BottomStockSettleTable)entity);
                        break;
                    case "BottomStockSubVersion":
                        ObjectContext.BottomStockSubVersions.AddObject((BottomStockSubVersion)entity);
                        break;
                    case "BoxUpTask":
                        ObjectContext.BoxUpTasks.AddObject((BoxUpTask)entity);
                        break;
                    case "BoxUpTaskDetail":
                        ObjectContext.BoxUpTaskDetails.AddObject((BoxUpTaskDetail)entity);
                        break;
                    case "Branch":
                        ObjectContext.Branches.AddObject((Branch)entity);
                        break;
                    case "BranchDateBaseRel":
                        ObjectContext.BranchDateBaseRels.AddObject((BranchDateBaseRel)entity);
                        break;
                    case "Branchstrategy":
                        ObjectContext.Branchstrategies.AddObject((Branchstrategy)entity);
                        break;
                    case "BranchSupplierRelation":
                        ObjectContext.BranchSupplierRelations.AddObject((BranchSupplierRelation)entity);
                        break;
                    case "BrandGradeMessage":
                        ObjectContext.BrandGradeMessages.AddObject((BrandGradeMessage)entity);
                        break;
                    case "CAReconciliation":
                        ObjectContext.CAReconciliations.AddObject((CAReconciliation)entity);
                        break;
                    case "CAReconciliationList":
                        ObjectContext.CAReconciliationLists.AddObject((CAReconciliationList)entity);
                        break;
                    case "CenterABCBasi":
                        ObjectContext.CenterABCBasis.AddObject((CenterABCBasi)entity);
                        break;
                    case "CenterAccumulateDailyExp":
                        ObjectContext.CenterAccumulateDailyExps.AddObject((CenterAccumulateDailyExp)entity);
                        break;
                    case "CenterApproveBill":
                        ObjectContext.CenterApproveBills.AddObject((CenterApproveBill)entity);
                        break;
                    case "CenterAuthorizedProvince":
                        ObjectContext.CenterAuthorizedProvinces.AddObject((CenterAuthorizedProvince)entity);
                        break;
                    case "CenterQuarterRebate":
                        ObjectContext.CenterQuarterRebates.AddObject((CenterQuarterRebate)entity);
                        break;
                    case "CenterRebateSummary":
                        ObjectContext.CenterRebateSummaries.AddObject((CenterRebateSummary)entity);
                        break;
                    case "CenterSaleReturnStrategy":
                        ObjectContext.CenterSaleReturnStrategies.AddObject((CenterSaleReturnStrategy)entity);
                        break;
                    case "CenterSaleSettle":
                        ObjectContext.CenterSaleSettles.AddObject((CenterSaleSettle)entity);
                        break;
                    case "ChannelCapability":
                        ObjectContext.ChannelCapabilities.AddObject((ChannelCapability)entity);
                        break;
                    case "ChannelPlan":
                        ObjectContext.ChannelPlans.AddObject((ChannelPlan)entity);
                        break;
                    case "ChannelPlanDetail":
                        ObjectContext.ChannelPlanDetails.AddObject((ChannelPlanDetail)entity);
                        break;
                    case "ChannelPlanHistory":
                        ObjectContext.ChannelPlanHistories.AddObject((ChannelPlanHistory)entity);
                        break;
                    case "ChannelPlanHistoryDetail":
                        ObjectContext.ChannelPlanHistoryDetails.AddObject((ChannelPlanHistoryDetail)entity);
                        break;
                    case "ChassisInformation":
                        ObjectContext.ChassisInformations.AddObject((ChassisInformation)entity);
                        break;
                    case "ClaimApproverStrategy":
                        ObjectContext.ClaimApproverStrategies.AddObject((ClaimApproverStrategy)entity);
                        break;
                    case "ClaimSettleBillDealerInvoice":
                        ObjectContext.ClaimSettleBillDealerInvoices.AddObject((ClaimSettleBillDealerInvoice)entity);
                        break;
                    case "CodeTemplate":
                        ObjectContext.CodeTemplates.AddObject((CodeTemplate)entity);
                        break;
                    case "CodeTemplateSerial":
                        ObjectContext.CodeTemplateSerials.AddObject((CodeTemplateSerial)entity);
                        break;
                    case "CombinedPart":
                        ObjectContext.CombinedParts.AddObject((CombinedPart)entity);
                        break;
                    case "Company":
                        ObjectContext.Companies.AddObject((Company)entity);
                        break;
                    case "company_log":
                        ObjectContext.company_log.AddObject((company_log)entity);
                        break;
                    case "CompanyAddress":
                        ObjectContext.CompanyAddresses.AddObject((CompanyAddress)entity);
                        break;
                    case "CompanyDetail":
                        ObjectContext.CompanyDetails.AddObject((CompanyDetail)entity);
                        break;
                    case "CompanyInvoiceInfo":
                        ObjectContext.CompanyInvoiceInfoes.AddObject((CompanyInvoiceInfo)entity);
                        break;
                    case "CompanyLoginPicture":
                        ObjectContext.CompanyLoginPictures.AddObject((CompanyLoginPicture)entity);
                        break;
                    case "CompanyTransferOrder":
                        ObjectContext.CompanyTransferOrders.AddObject((CompanyTransferOrder)entity);
                        break;
                    case "CompanyTransferOrderDetail":
                        ObjectContext.CompanyTransferOrderDetails.AddObject((CompanyTransferOrderDetail)entity);
                        break;
                    case "Countersignature":
                        ObjectContext.Countersignatures.AddObject((Countersignature)entity);
                        break;
                    case "CountersignatureDetail":
                        ObjectContext.CountersignatureDetails.AddObject((CountersignatureDetail)entity);
                        break;
                    case "CredenceApplication":
                        ObjectContext.CredenceApplications.AddObject((CredenceApplication)entity);
                        break;
                    case "CrossSalesOrder":
                        ObjectContext.CrossSalesOrders.AddObject((CrossSalesOrder)entity);
                        break;
                    case "CrossSalesOrderDetail":
                        ObjectContext.CrossSalesOrderDetails.AddObject((CrossSalesOrderDetail)entity);
                        break;
                    case "Customer":
                        ObjectContext.Customers.AddObject((Customer)entity);
                        break;
                    case "CustomerAccount":
                        ObjectContext.CustomerAccounts.AddObject((CustomerAccount)entity);
                        break;
                    case "CustomerAccountHisDetail":
                        ObjectContext.CustomerAccountHisDetails.AddObject((CustomerAccountHisDetail)entity);
                        break;
                    case "CustomerAccountHistory":
                        ObjectContext.CustomerAccountHistories.AddObject((CustomerAccountHistory)entity);
                        break;
                    case "CustomerDirectSpareList":
                        ObjectContext.CustomerDirectSpareLists.AddObject((CustomerDirectSpareList)entity);
                        break;
                    case "CustomerInformation":
                        ObjectContext.CustomerInformations.AddObject((CustomerInformation)entity);
                        break;
                    case "CustomerOpenAccountApp":
                        ObjectContext.CustomerOpenAccountApps.AddObject((CustomerOpenAccountApp)entity);
                        break;
                    case "CustomerOrderPriceGrade":
                        ObjectContext.CustomerOrderPriceGrades.AddObject((CustomerOrderPriceGrade)entity);
                        break;
                    case "CustomerOrderPriceGradeHistory":
                        ObjectContext.CustomerOrderPriceGradeHistories.AddObject((CustomerOrderPriceGradeHistory)entity);
                        break;
                    case "CustomerSupplyInitialFeeSet":
                        ObjectContext.CustomerSupplyInitialFeeSets.AddObject((CustomerSupplyInitialFeeSet)entity);
                        break;
                    case "CustomerTransferBill":
                        ObjectContext.CustomerTransferBills.AddObject((CustomerTransferBill)entity);
                        break;
                    case "CustomerTransferBill_Sync":
                        ObjectContext.CustomerTransferBill_Sync.AddObject((CustomerTransferBill_Sync)entity);
                        break;
                    case "DailySalesWeight":
                        ObjectContext.DailySalesWeights.AddObject((DailySalesWeight)entity);
                        break;
                    case "DataBaseLink":
                        ObjectContext.DataBaseLinks.AddObject((DataBaseLink)entity);
                        break;
                    case "DataBaseLinkBranch":
                        ObjectContext.DataBaseLinkBranches.AddObject((DataBaseLinkBranch)entity);
                        break;
                    case "Dealer":
                        ObjectContext.Dealers.AddObject((Dealer)entity);
                        break;
                    case "DealerBusinessPermit":
                        ObjectContext.DealerBusinessPermits.AddObject((DealerBusinessPermit)entity);
                        break;
                    case "DealerBusinessPermitHistory":
                        ObjectContext.DealerBusinessPermitHistories.AddObject((DealerBusinessPermitHistory)entity);
                        break;
                    case "DealerClaimSparePart":
                        ObjectContext.DealerClaimSpareParts.AddObject((DealerClaimSparePart)entity);
                        break;
                    case "DealerFormat":
                        ObjectContext.DealerFormats.AddObject((DealerFormat)entity);
                        break;
                    case "DealerGradeInfo":
                        ObjectContext.DealerGradeInfoes.AddObject((DealerGradeInfo)entity);
                        break;
                    case "DealerHistory":
                        ObjectContext.DealerHistories.AddObject((DealerHistory)entity);
                        break;
                    case "DealerInvoice":
                        ObjectContext.DealerInvoices.AddObject((DealerInvoice)entity);
                        break;
                    case "DealerKeyEmployee":
                        ObjectContext.DealerKeyEmployees.AddObject((DealerKeyEmployee)entity);
                        break;
                    case "DealerKeyEmployeeHistory":
                        ObjectContext.DealerKeyEmployeeHistories.AddObject((DealerKeyEmployeeHistory)entity);
                        break;
                    case "DealerKeyPosition":
                        ObjectContext.DealerKeyPositions.AddObject((DealerKeyPosition)entity);
                        break;
                    case "DealerMarketDptRelation":
                        ObjectContext.DealerMarketDptRelations.AddObject((DealerMarketDptRelation)entity);
                        break;
                    case "DealerMobileNumberHistory":
                        ObjectContext.DealerMobileNumberHistories.AddObject((DealerMobileNumberHistory)entity);
                        break;
                    case "DealerMobileNumberList":
                        ObjectContext.DealerMobileNumberLists.AddObject((DealerMobileNumberList)entity);
                        break;
                    case "DealerPartsInventoryBill":
                        ObjectContext.DealerPartsInventoryBills.AddObject((DealerPartsInventoryBill)entity);
                        break;
                    case "DealerPartsInventoryDetail":
                        ObjectContext.DealerPartsInventoryDetails.AddObject((DealerPartsInventoryDetail)entity);
                        break;
                    case "DealerPartsRetailOrder":
                        ObjectContext.DealerPartsRetailOrders.AddObject((DealerPartsRetailOrder)entity);
                        break;
                    case "DealerPartsSalesPrice":
                        ObjectContext.DealerPartsSalesPrices.AddObject((DealerPartsSalesPrice)entity);
                        break;
                    case "DealerPartsSalesPriceHistory":
                        ObjectContext.DealerPartsSalesPriceHistories.AddObject((DealerPartsSalesPriceHistory)entity);
                        break;
                    case "DealerPartsSalesReturnBill":
                        ObjectContext.DealerPartsSalesReturnBills.AddObject((DealerPartsSalesReturnBill)entity);
                        break;
                    case "DealerPartsStock":
                        ObjectContext.DealerPartsStocks.AddObject((DealerPartsStock)entity);
                        break;
                    case "DealerPartsStockOutInRecord":
                        ObjectContext.DealerPartsStockOutInRecords.AddObject((DealerPartsStockOutInRecord)entity);
                        break;
                    case "DealerPartsStockQueryView":
                        ObjectContext.DealerPartsStockQueryViews.AddObject((DealerPartsStockQueryView)entity);
                        break;
                    case "DealerPartsTransferOrder":
                        ObjectContext.DealerPartsTransferOrders.AddObject((DealerPartsTransferOrder)entity);
                        break;
                    case "DealerPartsTransOrderDetail":
                        ObjectContext.DealerPartsTransOrderDetails.AddObject((DealerPartsTransOrderDetail)entity);
                        break;
                    case "DealerPerTrainAut":
                        ObjectContext.DealerPerTrainAuts.AddObject((DealerPerTrainAut)entity);
                        break;
                    case "DealerRetailOrderDetail":
                        ObjectContext.DealerRetailOrderDetails.AddObject((DealerRetailOrderDetail)entity);
                        break;
                    case "DealerRetailReturnBillDetail":
                        ObjectContext.DealerRetailReturnBillDetails.AddObject((DealerRetailReturnBillDetail)entity);
                        break;
                    case "DealerServiceExt":
                        ObjectContext.DealerServiceExts.AddObject((DealerServiceExt)entity);
                        break;
                    case "DealerServiceExtHistory":
                        ObjectContext.DealerServiceExtHistories.AddObject((DealerServiceExtHistory)entity);
                        break;
                    case "DealerServiceInfo":
                        ObjectContext.DealerServiceInfoes.AddObject((DealerServiceInfo)entity);
                        break;
                    case "DealerServiceInfoHistory":
                        ObjectContext.DealerServiceInfoHistories.AddObject((DealerServiceInfoHistory)entity);
                        break;
                    case "DeferredDiscountType":
                        ObjectContext.DeferredDiscountTypes.AddObject((DeferredDiscountType)entity);
                        break;
                    case "DepartmentBrandRelation":
                        ObjectContext.DepartmentBrandRelations.AddObject((DepartmentBrandRelation)entity);
                        break;
                    case "DepartmentInformation":
                        ObjectContext.DepartmentInformations.AddObject((DepartmentInformation)entity);
                        break;
                    case "EcommerceFreightDisposal":
                        ObjectContext.EcommerceFreightDisposals.AddObject((EcommerceFreightDisposal)entity);
                        break;
                    case "EngineModelview":
                        ObjectContext.EngineModelviews.AddObject((EngineModelview)entity);
                        break;
                    case "EnterprisePartsCost":
                        ObjectContext.EnterprisePartsCosts.AddObject((EnterprisePartsCost)entity);
                        break;
                    case "ExportCustomerInfo":
                        ObjectContext.ExportCustomerInfoes.AddObject((ExportCustomerInfo)entity);
                        break;
                    case "Express":
                        ObjectContext.Expresses.AddObject((Express)entity);
                        break;
                    case "ExpressToLogistic":
                        ObjectContext.ExpressToLogistics.AddObject((ExpressToLogistic)entity);
                        break;
                    case "ExtendedInfoTemplate":
                        ObjectContext.ExtendedInfoTemplates.AddObject((ExtendedInfoTemplate)entity);
                        break;
                    case "ExtendedSparePartList":
                        ObjectContext.ExtendedSparePartLists.AddObject((ExtendedSparePartList)entity);
                        break;
                    case "ExtendedWarrantyOrder":
                        ObjectContext.ExtendedWarrantyOrders.AddObject((ExtendedWarrantyOrder)entity);
                        break;
                    case "ExtendedWarrantyOrderList":
                        ObjectContext.ExtendedWarrantyOrderLists.AddObject((ExtendedWarrantyOrderList)entity);
                        break;
                    case "ExtendedWarrantyProduct":
                        ObjectContext.ExtendedWarrantyProducts.AddObject((ExtendedWarrantyProduct)entity);
                        break;
                    case "FactoryPurchacePrice_Tmp":
                        ObjectContext.FactoryPurchacePrice_Tmp.AddObject((FactoryPurchacePrice_Tmp)entity);
                        break;
                    case "FaultyPartsSupplierAssembly":
                        ObjectContext.FaultyPartsSupplierAssemblies.AddObject((FaultyPartsSupplierAssembly)entity);
                        break;
                    case "FDWarrantyMessage":
                        ObjectContext.FDWarrantyMessages.AddObject((FDWarrantyMessage)entity);
                        break;
                    case "FinancialSnapshotSet":
                        ObjectContext.FinancialSnapshotSets.AddObject((FinancialSnapshotSet)entity);
                        break;
                    case "ForceReserveBill":
                        ObjectContext.ForceReserveBills.AddObject((ForceReserveBill)entity);
                        break;
                    case "ForceReserveBillDetail":
                        ObjectContext.ForceReserveBillDetails.AddObject((ForceReserveBillDetail)entity);
                        break;
                    case "FullStockTask":
                        ObjectContext.FullStockTasks.AddObject((FullStockTask)entity);
                        break;
                    case "FundMonthlySettleBill":
                        ObjectContext.FundMonthlySettleBills.AddObject((FundMonthlySettleBill)entity);
                        break;
                    case "FundMonthlySettleBillDetail":
                        ObjectContext.FundMonthlySettleBillDetails.AddObject((FundMonthlySettleBillDetail)entity);
                        break;
                    case "GetAllPartsPlannedPrice":
                        ObjectContext.GetAllPartsPlannedPrices.AddObject((GetAllPartsPlannedPrice)entity);
                        break;
                    case "GetAllPartsPurchasePricing":
                        ObjectContext.GetAllPartsPurchasePricings.AddObject((GetAllPartsPurchasePricing)entity);
                        break;
                    case "GetAllPartsSalesPrice":
                        ObjectContext.GetAllPartsSalesPrices.AddObject((GetAllPartsSalesPrice)entity);
                        break;
                    case "GoldenTaxClassifyMsg":
                        ObjectContext.GoldenTaxClassifyMsgs.AddObject((GoldenTaxClassifyMsg)entity);
                        break;
                    case "GradeCoefficient":
                        ObjectContext.GradeCoefficients.AddObject((GradeCoefficient)entity);
                        break;
                    case "GradeCoefficientView":
                        ObjectContext.GradeCoefficientViews.AddObject((GradeCoefficientView)entity);
                        break;
                    case "HauDistanceInfor":
                        ObjectContext.HauDistanceInfors.AddObject((HauDistanceInfor)entity);
                        break;
                    case "InOutSettleSummary":
                        ObjectContext.InOutSettleSummaries.AddObject((InOutSettleSummary)entity);
                        break;
                    case "InOutWorkPerHour":
                        ObjectContext.InOutWorkPerHours.AddObject((InOutWorkPerHour)entity);
                        break;
                    case "IntegralSettleDictate":
                        ObjectContext.IntegralSettleDictates.AddObject((IntegralSettleDictate)entity);
                        break;
                    case "IntelligentOrderMonthly":
                        ObjectContext.IntelligentOrderMonthlies.AddObject((IntelligentOrderMonthly)entity);
                        break;
                    case "IntelligentOrderMonthlyBase":
                        ObjectContext.IntelligentOrderMonthlyBases.AddObject((IntelligentOrderMonthlyBase)entity);
                        break;
                    case "IntelligentOrderWeekly":
                        ObjectContext.IntelligentOrderWeeklies.AddObject((IntelligentOrderWeekly)entity);
                        break;
                    case "IntelligentOrderWeeklyBase":
                        ObjectContext.IntelligentOrderWeeklyBases.AddObject((IntelligentOrderWeeklyBase)entity);
                        break;
                    case "InternalAcquisitionBill":
                        ObjectContext.InternalAcquisitionBills.AddObject((InternalAcquisitionBill)entity);
                        break;
                    case "InternalAcquisitionDetail":
                        ObjectContext.InternalAcquisitionDetails.AddObject((InternalAcquisitionDetail)entity);
                        break;
                    case "InternalAllocationBill":
                        ObjectContext.InternalAllocationBills.AddObject((InternalAllocationBill)entity);
                        break;
                    case "InternalAllocationDetail":
                        ObjectContext.InternalAllocationDetails.AddObject((InternalAllocationDetail)entity);
                        break;
                    case "InteSettleBillInvoiceLink":
                        ObjectContext.InteSettleBillInvoiceLinks.AddObject((InteSettleBillInvoiceLink)entity);
                        break;
                    case "InvoiceInformation":
                        ObjectContext.InvoiceInformations.AddObject((InvoiceInformation)entity);
                        break;
                    case "IvecoPriceChangeApp":
                        ObjectContext.IvecoPriceChangeApps.AddObject((IvecoPriceChangeApp)entity);
                        break;
                    case "IvecoPriceChangeAppDetail":
                        ObjectContext.IvecoPriceChangeAppDetails.AddObject((IvecoPriceChangeAppDetail)entity);
                        break;
                    case "IvecoSalesPrice":
                        ObjectContext.IvecoSalesPrices.AddObject((IvecoSalesPrice)entity);
                        break;
                    case "KeyValueItem":
                        ObjectContext.KeyValueItems.AddObject((KeyValueItem)entity);
                        break;
                    case "LayerNodeType":
                        ObjectContext.LayerNodeTypes.AddObject((LayerNodeType)entity);
                        break;
                    case "LayerNodeTypeAffiliation":
                        ObjectContext.LayerNodeTypeAffiliations.AddObject((LayerNodeTypeAffiliation)entity);
                        break;
                    case "LayerStructure":
                        ObjectContext.LayerStructures.AddObject((LayerStructure)entity);
                        break;
                    case "LayerStructurePurpose":
                        ObjectContext.LayerStructurePurposes.AddObject((LayerStructurePurpose)entity);
                        break;
                    case "LinkAllocation":
                        ObjectContext.LinkAllocations.AddObject((LinkAllocation)entity);
                        break;
                    case "LoadingDetail":
                        ObjectContext.LoadingDetails.AddObject((LoadingDetail)entity);
                        break;
                    case "LocketUnitQuery":
                        ObjectContext.LocketUnitQueries.AddObject((LocketUnitQuery)entity);
                        break;
                    case "Logistic":
                        ObjectContext.Logistics.AddObject((Logistic)entity);
                        break;
                    case "LogisticCompany":
                        ObjectContext.LogisticCompanies.AddObject((LogisticCompany)entity);
                        break;
                    case "LogisticCompanyServiceRange":
                        ObjectContext.LogisticCompanyServiceRanges.AddObject((LogisticCompanyServiceRange)entity);
                        break;
                    case "LogisticsDetail":
                        ObjectContext.LogisticsDetails.AddObject((LogisticsDetail)entity);
                        break;
                    case "MaintainRedProductMap":
                        ObjectContext.MaintainRedProductMaps.AddObject((MaintainRedProductMap)entity);
                        break;
                    case "MalfunctionView":
                        ObjectContext.MalfunctionViews.AddObject((MalfunctionView)entity);
                        break;
                    case "ManualScheduling":
                        ObjectContext.ManualSchedulings.AddObject((ManualScheduling)entity);
                        break;
                    case "MarketABQualityInformation":
                        ObjectContext.MarketABQualityInformations.AddObject((MarketABQualityInformation)entity);
                        break;
                    case "MarketDptPersonnelRelation":
                        ObjectContext.MarketDptPersonnelRelations.AddObject((MarketDptPersonnelRelation)entity);
                        break;
                    case "MarketingDepartment":
                        ObjectContext.MarketingDepartments.AddObject((MarketingDepartment)entity);
                        break;
                    case "MultiLevelApproveConfig":
                        ObjectContext.MultiLevelApproveConfigs.AddObject((MultiLevelApproveConfig)entity);
                        break;
                    case "Notification":
                        ObjectContext.Notifications.AddObject((Notification)entity);
                        break;
                    case "NotificationLimit":
                        ObjectContext.NotificationLimits.AddObject((NotificationLimit)entity);
                        break;
                    case "NotificationOpLog":
                        ObjectContext.NotificationOpLogs.AddObject((NotificationOpLog)entity);
                        break;
                    case "NotificationReply":
                        ObjectContext.NotificationReplies.AddObject((NotificationReply)entity);
                        break;
                    case "OrderApproveWeekday":
                        ObjectContext.OrderApproveWeekdays.AddObject((OrderApproveWeekday)entity);
                        break;
                    case "OutboundOrder":
                        ObjectContext.OutboundOrders.AddObject((OutboundOrder)entity);
                        break;
                    case "OutofWarrantyPayment":
                        ObjectContext.OutofWarrantyPayments.AddObject((OutofWarrantyPayment)entity);
                        break;
                    case "OverstockPartsAdjustBill":
                        ObjectContext.OverstockPartsAdjustBills.AddObject((OverstockPartsAdjustBill)entity);
                        break;
                    case "OverstockPartsAdjustDetail":
                        ObjectContext.OverstockPartsAdjustDetails.AddObject((OverstockPartsAdjustDetail)entity);
                        break;
                    case "OverstockPartsApp":
                        ObjectContext.OverstockPartsApps.AddObject((OverstockPartsApp)entity);
                        break;
                    case "OverstockPartsAppDetail":
                        ObjectContext.OverstockPartsAppDetails.AddObject((OverstockPartsAppDetail)entity);
                        break;
                    case "OverstockPartsInformation":
                        ObjectContext.OverstockPartsInformations.AddObject((OverstockPartsInformation)entity);
                        break;
                    case "OverstockPartsPlatFormBill":
                        ObjectContext.OverstockPartsPlatFormBills.AddObject((OverstockPartsPlatFormBill)entity);
                        break;
                    case "OverstockPartsRecommendBill":
                        ObjectContext.OverstockPartsRecommendBills.AddObject((OverstockPartsRecommendBill)entity);
                        break;
                    case "OverstockTransferOrder":
                        ObjectContext.OverstockTransferOrders.AddObject((OverstockTransferOrder)entity);
                        break;
                    case "OverstockTransferOrderDetail":
                        ObjectContext.OverstockTransferOrderDetails.AddObject((OverstockTransferOrderDetail)entity);
                        break;
                    case "PackingTask":
                        ObjectContext.PackingTasks.AddObject((PackingTask)entity);
                        break;
                    case "PackingTaskDetail":
                        ObjectContext.PackingTaskDetails.AddObject((PackingTaskDetail)entity);
                        break;
                    case "PartABCDetail":
                        ObjectContext.PartABCDetails.AddObject((PartABCDetail)entity);
                        break;
                    case "PartConPurchasePlan":
                        ObjectContext.PartConPurchasePlans.AddObject((PartConPurchasePlan)entity);
                        break;
                    case "PartDeleaveHistory":
                        ObjectContext.PartDeleaveHistories.AddObject((PartDeleaveHistory)entity);
                        break;
                    case "PartDeleaveInformation":
                        ObjectContext.PartDeleaveInformations.AddObject((PartDeleaveInformation)entity);
                        break;
                    case "PartNotConPurchasePlan":
                        ObjectContext.PartNotConPurchasePlans.AddObject((PartNotConPurchasePlan)entity);
                        break;
                    case "PartRequisitionReturnBill":
                        ObjectContext.PartRequisitionReturnBills.AddObject((PartRequisitionReturnBill)entity);
                        break;
                    case "PartSafeStock":
                        ObjectContext.PartSafeStocks.AddObject((PartSafeStock)entity);
                        break;
                    case "PartsBatchRecord":
                        ObjectContext.PartsBatchRecords.AddObject((PartsBatchRecord)entity);
                        break;
                    case "PartsBranch":
                        ObjectContext.PartsBranches.AddObject((PartsBranch)entity);
                        break;
                    case "PartsBranchHistory":
                        ObjectContext.PartsBranchHistories.AddObject((PartsBranchHistory)entity);
                        break;
                    case "PartsBranchPackingProp":
                        ObjectContext.PartsBranchPackingProps.AddObject((PartsBranchPackingProp)entity);
                        break;
                    case "PartsBranchView":
                        ObjectContext.PartsBranchViews.AddObject((PartsBranchView)entity);
                        break;
                    case "PartsClaimOrderNew":
                        ObjectContext.PartsClaimOrderNews.AddObject((PartsClaimOrderNew)entity);
                        break;
                    case "PartsClaimPrice":
                        ObjectContext.PartsClaimPrices.AddObject((PartsClaimPrice)entity);
                        break;
                    case "PartsClaimPriceForbw":
                        ObjectContext.PartsClaimPriceForbws.AddObject((PartsClaimPriceForbw)entity);
                        break;
                    case "PartsCostTransferInTransit":
                        ObjectContext.PartsCostTransferInTransits.AddObject((PartsCostTransferInTransit)entity);
                        break;
                    case "PartsDifferenceBackBill":
                        ObjectContext.PartsDifferenceBackBills.AddObject((PartsDifferenceBackBill)entity);
                        break;
                    case "PartsDifferenceBackBillDtl":
                        ObjectContext.PartsDifferenceBackBillDtls.AddObject((PartsDifferenceBackBillDtl)entity);
                        break;
                    case "PartsExchange":
                        ObjectContext.PartsExchanges.AddObject((PartsExchange)entity);
                        break;
                    case "PartsExchangeGroup":
                        ObjectContext.PartsExchangeGroups.AddObject((PartsExchangeGroup)entity);
                        break;
                    case "PartsExchangeHistory":
                        ObjectContext.PartsExchangeHistories.AddObject((PartsExchangeHistory)entity);
                        break;
                    case "PartsHistoryStock":
                        ObjectContext.PartsHistoryStocks.AddObject((PartsHistoryStock)entity);
                        break;
                    case "PartsInboundCheckBill":
                        ObjectContext.PartsInboundCheckBills.AddObject((PartsInboundCheckBill)entity);
                        break;
                    case "PartsInboundCheckBill_Sync":
                        ObjectContext.PartsInboundCheckBill_Sync.AddObject((PartsInboundCheckBill_Sync)entity);
                        break;
                    case "PartsInboundCheckBillDetail":
                        ObjectContext.PartsInboundCheckBillDetails.AddObject((PartsInboundCheckBillDetail)entity);
                        break;
                    case "PartsInboundPackingDetail":
                        ObjectContext.PartsInboundPackingDetails.AddObject((PartsInboundPackingDetail)entity);
                        break;
                    case "PartsInboundPerformance":
                        ObjectContext.PartsInboundPerformances.AddObject((PartsInboundPerformance)entity);
                        break;
                    case "PartsInboundPlan":
                        ObjectContext.PartsInboundPlans.AddObject((PartsInboundPlan)entity);
                        break;
                    case "PartsInboundPlanDetail":
                        ObjectContext.PartsInboundPlanDetails.AddObject((PartsInboundPlanDetail)entity);
                        break;
                    case "PartsInventoryBill":
                        ObjectContext.PartsInventoryBills.AddObject((PartsInventoryBill)entity);
                        break;
                    case "PartsInventoryDetail":
                        ObjectContext.PartsInventoryDetails.AddObject((PartsInventoryDetail)entity);
                        break;
                    case "PartsLockedStock":
                        ObjectContext.PartsLockedStocks.AddObject((PartsLockedStock)entity);
                        break;
                    case "PartsLogisticBatch":
                        ObjectContext.PartsLogisticBatches.AddObject((PartsLogisticBatch)entity);
                        break;
                    case "PartsLogisticBatchBillDetail":
                        ObjectContext.PartsLogisticBatchBillDetails.AddObject((PartsLogisticBatchBillDetail)entity);
                        break;
                    case "PartsLogisticBatchItemDetail":
                        ObjectContext.PartsLogisticBatchItemDetails.AddObject((PartsLogisticBatchItemDetail)entity);
                        break;
                    case "PartsManagementCostGrade":
                        ObjectContext.PartsManagementCostGrades.AddObject((PartsManagementCostGrade)entity);
                        break;
                    case "PartsManagementCostRate":
                        ObjectContext.PartsManagementCostRates.AddObject((PartsManagementCostRate)entity);
                        break;
                    case "PartsOutboundBill":
                        ObjectContext.PartsOutboundBills.AddObject((PartsOutboundBill)entity);
                        break;
                    case "PartsOutboundBillDetail":
                        ObjectContext.PartsOutboundBillDetails.AddObject((PartsOutboundBillDetail)entity);
                        break;
                    case "PartsOutboundPerformance":
                        ObjectContext.PartsOutboundPerformances.AddObject((PartsOutboundPerformance)entity);
                        break;
                    case "PartsOutboundPlan":
                        ObjectContext.PartsOutboundPlans.AddObject((PartsOutboundPlan)entity);
                        break;
                    case "PartsOutboundPlanDetail":
                        ObjectContext.PartsOutboundPlanDetails.AddObject((PartsOutboundPlanDetail)entity);
                        break;
                    case "PartsOuterPurchaseChange":
                        ObjectContext.PartsOuterPurchaseChanges.AddObject((PartsOuterPurchaseChange)entity);
                        break;
                    case "PartsOuterPurchaselist":
                        ObjectContext.PartsOuterPurchaselists.AddObject((PartsOuterPurchaselist)entity);
                        break;
                    case "PartsPacking":
                        ObjectContext.PartsPackings.AddObject((PartsPacking)entity);
                        break;
                    case "PartsPackingPropAppDetail":
                        ObjectContext.PartsPackingPropAppDetails.AddObject((PartsPackingPropAppDetail)entity);
                        break;
                    case "PartsPackingPropertyApp":
                        ObjectContext.PartsPackingPropertyApps.AddObject((PartsPackingPropertyApp)entity);
                        break;
                    case "PartsPlannedPrice":
                        ObjectContext.PartsPlannedPrices.AddObject((PartsPlannedPrice)entity);
                        break;
                    case "PartsPriceIncreaseRate":
                        ObjectContext.PartsPriceIncreaseRates.AddObject((PartsPriceIncreaseRate)entity);
                        break;
                    case "PartsPriceTimeLimit":
                        ObjectContext.PartsPriceTimeLimits.AddObject((PartsPriceTimeLimit)entity);
                        break;
                    case "PartsPriorityBill":
                        ObjectContext.PartsPriorityBills.AddObject((PartsPriorityBill)entity);
                        break;
                    case "PartsPurchaseInfoSummary":
                        ObjectContext.PartsPurchaseInfoSummaries.AddObject((PartsPurchaseInfoSummary)entity);
                        break;
                    case "PartsPurchaseOrder":
                        ObjectContext.PartsPurchaseOrders.AddObject((PartsPurchaseOrder)entity);
                        break;
                    case "PartsPurchaseOrder_Sync":
                        ObjectContext.PartsPurchaseOrder_Sync.AddObject((PartsPurchaseOrder_Sync)entity);
                        break;
                    case "PartsPurchaseOrderDetail":
                        ObjectContext.PartsPurchaseOrderDetails.AddObject((PartsPurchaseOrderDetail)entity);
                        break;
                    case "PartsPurchaseOrderType":
                        ObjectContext.PartsPurchaseOrderTypes.AddObject((PartsPurchaseOrderType)entity);
                        break;
                    case "PartsPurchasePlan":
                        ObjectContext.PartsPurchasePlans.AddObject((PartsPurchasePlan)entity);
                        break;
                    case "PartsPurchasePlan_HW":
                        ObjectContext.PartsPurchasePlan_HW.AddObject((PartsPurchasePlan_HW)entity);
                        break;
                    case "PartsPurchasePlanDetail":
                        ObjectContext.PartsPurchasePlanDetails.AddObject((PartsPurchasePlanDetail)entity);
                        break;
                    case "PartsPurchasePlanDetail_HW":
                        ObjectContext.PartsPurchasePlanDetail_HW.AddObject((PartsPurchasePlanDetail_HW)entity);
                        break;
                    case "PartsPurchasePlanTemp":
                        ObjectContext.PartsPurchasePlanTemps.AddObject((PartsPurchasePlanTemp)entity);
                        break;
                    case "PartsPurchasePricing":
                        ObjectContext.PartsPurchasePricings.AddObject((PartsPurchasePricing)entity);
                        break;
                    case "PartsPurchasePricingChange":
                        ObjectContext.PartsPurchasePricingChanges.AddObject((PartsPurchasePricingChange)entity);
                        break;
                    case "PartsPurchasePricingDetail":
                        ObjectContext.PartsPurchasePricingDetails.AddObject((PartsPurchasePricingDetail)entity);
                        break;
                    case "PartsPurchaseRtnSettleBill":
                        ObjectContext.PartsPurchaseRtnSettleBills.AddObject((PartsPurchaseRtnSettleBill)entity);
                        break;
                    case "PartsPurchaseRtnSettleDetail":
                        ObjectContext.PartsPurchaseRtnSettleDetails.AddObject((PartsPurchaseRtnSettleDetail)entity);
                        break;
                    case "PartsPurchaseRtnSettleRef":
                        ObjectContext.PartsPurchaseRtnSettleRefs.AddObject((PartsPurchaseRtnSettleRef)entity);
                        break;
                    case "PartsPurchaseSettleBill":
                        ObjectContext.PartsPurchaseSettleBills.AddObject((PartsPurchaseSettleBill)entity);
                        break;
                    case "PartsPurchaseSettleDetail":
                        ObjectContext.PartsPurchaseSettleDetails.AddObject((PartsPurchaseSettleDetail)entity);
                        break;
                    case "PartsPurchaseSettleRef":
                        ObjectContext.PartsPurchaseSettleRefs.AddObject((PartsPurchaseSettleRef)entity);
                        break;
                    case "PartsPurReturnOrder":
                        ObjectContext.PartsPurReturnOrders.AddObject((PartsPurReturnOrder)entity);
                        break;
                    case "PartsPurReturnOrderDetail":
                        ObjectContext.PartsPurReturnOrderDetails.AddObject((PartsPurReturnOrderDetail)entity);
                        break;
                    case "PartsRebateAccount":
                        ObjectContext.PartsRebateAccounts.AddObject((PartsRebateAccount)entity);
                        break;
                    case "PartsRebateApplication":
                        ObjectContext.PartsRebateApplications.AddObject((PartsRebateApplication)entity);
                        break;
                    case "PartsRebateChangeDetail":
                        ObjectContext.PartsRebateChangeDetails.AddObject((PartsRebateChangeDetail)entity);
                        break;
                    case "PartsRebateType":
                        ObjectContext.PartsRebateTypes.AddObject((PartsRebateType)entity);
                        break;
                    case "PartsReplacement":
                        ObjectContext.PartsReplacements.AddObject((PartsReplacement)entity);
                        break;
                    case "PartsReplacementHistory":
                        ObjectContext.PartsReplacementHistories.AddObject((PartsReplacementHistory)entity);
                        break;
                    case "PartsRequisitionSettleBill":
                        ObjectContext.PartsRequisitionSettleBills.AddObject((PartsRequisitionSettleBill)entity);
                        break;
                    case "PartsRequisitionSettleDetail":
                        ObjectContext.PartsRequisitionSettleDetails.AddObject((PartsRequisitionSettleDetail)entity);
                        break;
                    case "PartsRequisitionSettleRef":
                        ObjectContext.PartsRequisitionSettleRefs.AddObject((PartsRequisitionSettleRef)entity);
                        break;
                    case "PartsRetailGuidePrice":
                        ObjectContext.PartsRetailGuidePrices.AddObject((PartsRetailGuidePrice)entity);
                        break;
                    case "PartsRetailGuidePriceHistory":
                        ObjectContext.PartsRetailGuidePriceHistories.AddObject((PartsRetailGuidePriceHistory)entity);
                        break;
                    case "PartsRetailOrder":
                        ObjectContext.PartsRetailOrders.AddObject((PartsRetailOrder)entity);
                        break;
                    case "PartsRetailOrderDetail":
                        ObjectContext.PartsRetailOrderDetails.AddObject((PartsRetailOrderDetail)entity);
                        break;
                    case "PartsRetailReturnBill":
                        ObjectContext.PartsRetailReturnBills.AddObject((PartsRetailReturnBill)entity);
                        break;
                    case "PartsRetailReturnBillDetail":
                        ObjectContext.PartsRetailReturnBillDetails.AddObject((PartsRetailReturnBillDetail)entity);
                        break;
                    case "PartsSalePriceIncreaseRate":
                        ObjectContext.PartsSalePriceIncreaseRates.AddObject((PartsSalePriceIncreaseRate)entity);
                        break;
                    case "PartsSalesCategory":
                        ObjectContext.PartsSalesCategories.AddObject((PartsSalesCategory)entity);
                        break;
                    case "PartsSalesCategoryView":
                        ObjectContext.PartsSalesCategoryViews.AddObject((PartsSalesCategoryView)entity);
                        break;
                    case "PartsSalesOrder":
                        ObjectContext.PartsSalesOrders.AddObject((PartsSalesOrder)entity);
                        break;
                    case "PartsSalesOrderASAP":
                        ObjectContext.PartsSalesOrderASAPs.AddObject((PartsSalesOrderASAP)entity);
                        break;
                    case "PartsSalesOrderDetail":
                        ObjectContext.PartsSalesOrderDetails.AddObject((PartsSalesOrderDetail)entity);
                        break;
                    case "PartsSalesOrderDetailASAP":
                        ObjectContext.PartsSalesOrderDetailASAPs.AddObject((PartsSalesOrderDetailASAP)entity);
                        break;
                    case "PartsSalesOrderProcess":
                        ObjectContext.PartsSalesOrderProcesses.AddObject((PartsSalesOrderProcess)entity);
                        break;
                    case "PartsSalesOrderProcessDetail":
                        ObjectContext.PartsSalesOrderProcessDetails.AddObject((PartsSalesOrderProcessDetail)entity);
                        break;
                    case "PartsSalesOrderType":
                        ObjectContext.PartsSalesOrderTypes.AddObject((PartsSalesOrderType)entity);
                        break;
                    case "PartsSalesPrice":
                        ObjectContext.PartsSalesPrices.AddObject((PartsSalesPrice)entity);
                        break;
                    case "PartsSalesPriceChange":
                        ObjectContext.PartsSalesPriceChanges.AddObject((PartsSalesPriceChange)entity);
                        break;
                    case "PartsSalesPriceChangeDetail":
                        ObjectContext.PartsSalesPriceChangeDetails.AddObject((PartsSalesPriceChangeDetail)entity);
                        break;
                    case "PartsSalesPriceHistory":
                        ObjectContext.PartsSalesPriceHistories.AddObject((PartsSalesPriceHistory)entity);
                        break;
                    case "PartsSalesReturnBill":
                        ObjectContext.PartsSalesReturnBills.AddObject((PartsSalesReturnBill)entity);
                        break;
                    case "PartsSalesReturnBillDetail":
                        ObjectContext.PartsSalesReturnBillDetails.AddObject((PartsSalesReturnBillDetail)entity);
                        break;
                    case "PartsSalesRtnSettlement":
                        ObjectContext.PartsSalesRtnSettlements.AddObject((PartsSalesRtnSettlement)entity);
                        break;
                    case "PartsSalesRtnSettlementDetail":
                        ObjectContext.PartsSalesRtnSettlementDetails.AddObject((PartsSalesRtnSettlementDetail)entity);
                        break;
                    case "PartsSalesRtnSettlementRef":
                        ObjectContext.PartsSalesRtnSettlementRefs.AddObject((PartsSalesRtnSettlementRef)entity);
                        break;
                    case "PartsSalesSettlement":
                        ObjectContext.PartsSalesSettlements.AddObject((PartsSalesSettlement)entity);
                        break;
                    case "PartsSalesSettlementDetail":
                        ObjectContext.PartsSalesSettlementDetails.AddObject((PartsSalesSettlementDetail)entity);
                        break;
                    case "PartsSalesSettlementRef":
                        ObjectContext.PartsSalesSettlementRefs.AddObject((PartsSalesSettlementRef)entity);
                        break;
                    case "PartsSalesWeeklyBase":
                        ObjectContext.PartsSalesWeeklyBases.AddObject((PartsSalesWeeklyBase)entity);
                        break;
                    case "PartsServiceLevel":
                        ObjectContext.PartsServiceLevels.AddObject((PartsServiceLevel)entity);
                        break;
                    case "PartsShelvesTask":
                        ObjectContext.PartsShelvesTasks.AddObject((PartsShelvesTask)entity);
                        break;
                    case "PartsShiftOrder":
                        ObjectContext.PartsShiftOrders.AddObject((PartsShiftOrder)entity);
                        break;
                    case "PartsShiftOrderDetail":
                        ObjectContext.PartsShiftOrderDetails.AddObject((PartsShiftOrderDetail)entity);
                        break;
                    case "PartsShiftSIHDetail":
                        ObjectContext.PartsShiftSIHDetails.AddObject((PartsShiftSIHDetail)entity);
                        break;
                    case "PartsShippingOrder":
                        ObjectContext.PartsShippingOrders.AddObject((PartsShippingOrder)entity);
                        break;
                    case "PartsShippingOrderDetail":
                        ObjectContext.PartsShippingOrderDetails.AddObject((PartsShippingOrderDetail)entity);
                        break;
                    case "PartsShippingOrderRef":
                        ObjectContext.PartsShippingOrderRefs.AddObject((PartsShippingOrderRef)entity);
                        break;
                    case "PartsSpecialPriceHistory":
                        ObjectContext.PartsSpecialPriceHistories.AddObject((PartsSpecialPriceHistory)entity);
                        break;
                    case "PartsSpecialTreatyPrice":
                        ObjectContext.PartsSpecialTreatyPrices.AddObject((PartsSpecialTreatyPrice)entity);
                        break;
                    case "PartsStandardStockBill":
                        ObjectContext.PartsStandardStockBills.AddObject((PartsStandardStockBill)entity);
                        break;
                    case "PartsStock":
                        ObjectContext.PartsStocks.AddObject((PartsStock)entity);
                        break;
                    case "PartsStockBatchDetail":
                        ObjectContext.PartsStockBatchDetails.AddObject((PartsStockBatchDetail)entity);
                        break;
                    case "PartsStockCoefficient":
                        ObjectContext.PartsStockCoefficients.AddObject((PartsStockCoefficient)entity);
                        break;
                    case "PartsStockCoefficientPrice":
                        ObjectContext.PartsStockCoefficientPrices.AddObject((PartsStockCoefficientPrice)entity);
                        break;
                    case "PartsStockHistory":
                        ObjectContext.PartsStockHistories.AddObject((PartsStockHistory)entity);
                        break;
                    case "PartsSupplier":
                        ObjectContext.PartsSuppliers.AddObject((PartsSupplier)entity);
                        break;
                    case "PartsSupplierCollection":
                        ObjectContext.PartsSupplierCollections.AddObject((PartsSupplierCollection)entity);
                        break;
                    case "PartsSupplierRelation":
                        ObjectContext.PartsSupplierRelations.AddObject((PartsSupplierRelation)entity);
                        break;
                    case "PartsSupplierRelationHistory":
                        ObjectContext.PartsSupplierRelationHistories.AddObject((PartsSupplierRelationHistory)entity);
                        break;
                    case "PartsSupplierStock":
                        ObjectContext.PartsSupplierStocks.AddObject((PartsSupplierStock)entity);
                        break;
                    case "PartsSupplierStockHistory":
                        ObjectContext.PartsSupplierStockHistories.AddObject((PartsSupplierStockHistory)entity);
                        break;
                    case "PartsTransferOrder":
                        ObjectContext.PartsTransferOrders.AddObject((PartsTransferOrder)entity);
                        break;
                    case "PartsTransferOrderDetail":
                        ObjectContext.PartsTransferOrderDetails.AddObject((PartsTransferOrderDetail)entity);
                        break;
                    case "PartsWarehousePendingCost":
                        ObjectContext.PartsWarehousePendingCosts.AddObject((PartsWarehousePendingCost)entity);
                        break;
                    case "PaymentBeneficiaryList":
                        ObjectContext.PaymentBeneficiaryLists.AddObject((PaymentBeneficiaryList)entity);
                        break;
                    case "PaymentBill":
                        ObjectContext.PaymentBills.AddObject((PaymentBill)entity);
                        break;
                    case "PaymentBill_Sync":
                        ObjectContext.PaymentBill_Sync.AddObject((PaymentBill_Sync)entity);
                        break;
                    case "PayOutBill":
                        ObjectContext.PayOutBills.AddObject((PayOutBill)entity);
                        break;
                    case "Personnel":
                        ObjectContext.Personnels.AddObject((Personnel)entity);
                        break;
                    case "PersonnelSupplierRelation":
                        ObjectContext.PersonnelSupplierRelations.AddObject((PersonnelSupplierRelation)entity);
                        break;
                    case "PersonSalesCenterLink":
                        ObjectContext.PersonSalesCenterLinks.AddObject((PersonSalesCenterLink)entity);
                        break;
                    case "PersonSubDealer":
                        ObjectContext.PersonSubDealers.AddObject((PersonSubDealer)entity);
                        break;
                    case "PickingTask":
                        ObjectContext.PickingTasks.AddObject((PickingTask)entity);
                        break;
                    case "PickingTaskBatchDetail":
                        ObjectContext.PickingTaskBatchDetails.AddObject((PickingTaskBatchDetail)entity);
                        break;
                    case "PickingTaskDetail":
                        ObjectContext.PickingTaskDetails.AddObject((PickingTaskDetail)entity);
                        break;
                    case "PlannedPriceApp":
                        ObjectContext.PlannedPriceApps.AddObject((PlannedPriceApp)entity);
                        break;
                    case "PlannedPriceAppDetail":
                        ObjectContext.PlannedPriceAppDetails.AddObject((PlannedPriceAppDetail)entity);
                        break;
                    case "PlanPriceCategory":
                        ObjectContext.PlanPriceCategories.AddObject((PlanPriceCategory)entity);
                        break;
                    case "PostSaleClaims_Tmp":
                        ObjectContext.PostSaleClaims_Tmp.AddObject((PostSaleClaims_Tmp)entity);
                        break;
                    case "PQIReport":
                        ObjectContext.PQIReports.AddObject((PQIReport)entity);
                        break;
                    case "PreInspectionOrder":
                        ObjectContext.PreInspectionOrders.AddObject((PreInspectionOrder)entity);
                        break;
                    case "PreOrder":
                        ObjectContext.PreOrders.AddObject((PreOrder)entity);
                        break;
                    case "PreSaleCheckOrder":
                        ObjectContext.PreSaleCheckOrders.AddObject((PreSaleCheckOrder)entity);
                        break;
                    case "PreSaleItem":
                        ObjectContext.PreSaleItems.AddObject((PreSaleItem)entity);
                        break;
                    case "PreSalesCheckDetail":
                        ObjectContext.PreSalesCheckDetails.AddObject((PreSalesCheckDetail)entity);
                        break;
                    case "PreSalesCheckFeeStandard":
                        ObjectContext.PreSalesCheckFeeStandards.AddObject((PreSalesCheckFeeStandard)entity);
                        break;
                    case "ProductCategoryView":
                        ObjectContext.ProductCategoryViews.AddObject((ProductCategoryView)entity);
                        break;
                    case "ProductView":
                        ObjectContext.ProductViews.AddObject((ProductView)entity);
                        break;
                    case "PurchaseNoSettleTable":
                        ObjectContext.PurchaseNoSettleTables.AddObject((PurchaseNoSettleTable)entity);
                        break;
                    case "PurchaseOrderFinishedDetail":
                        ObjectContext.PurchaseOrderFinishedDetails.AddObject((PurchaseOrderFinishedDetail)entity);
                        break;
                    case "PurchaseOrderUnFinishDetail":
                        ObjectContext.PurchaseOrderUnFinishDetails.AddObject((PurchaseOrderUnFinishDetail)entity);
                        break;
                    case "PurchasePersonSupplierLink":
                        ObjectContext.PurchasePersonSupplierLinks.AddObject((PurchasePersonSupplierLink)entity);
                        break;
                    case "PurchasePricingChangeHi":
                        ObjectContext.PurchasePricingChangeHis.AddObject((PurchasePricingChangeHi)entity);
                        break;
                    case "PurchaseRtnSettleInvoiceRel":
                        ObjectContext.PurchaseRtnSettleInvoiceRels.AddObject((PurchaseRtnSettleInvoiceRel)entity);
                        break;
                    case "PurchaseSettleInvoiceRel":
                        ObjectContext.PurchaseSettleInvoiceRels.AddObject((PurchaseSettleInvoiceRel)entity);
                        break;
                    case "PurchaseSettleInvoiceResource":
                        ObjectContext.PurchaseSettleInvoiceResources.AddObject((PurchaseSettleInvoiceResource)entity);
                        break;
                    case "PurDistributionDealerLog":
                        ObjectContext.PurDistributionDealerLogs.AddObject((PurDistributionDealerLog)entity);
                        break;
                    case "QTSDataQuery":
                        ObjectContext.QTSDataQueries.AddObject((QTSDataQuery)entity);
                        break;
                    case "QTSDataQueryForDGN":
                        ObjectContext.QTSDataQueryForDGNs.AddObject((QTSDataQueryForDGN)entity);
                        break;
                    case "QTSDataQueryForFD":
                        ObjectContext.QTSDataQueryForFDs.AddObject((QTSDataQueryForFD)entity);
                        break;
                    case "QTSDataQueryForNFGC":
                        ObjectContext.QTSDataQueryForNFGCs.AddObject((QTSDataQueryForNFGC)entity);
                        break;
                    case "QTSDataQueryForQXC":
                        ObjectContext.QTSDataQueryForQXCs.AddObject((QTSDataQueryForQXC)entity);
                        break;
                    case "QTSDataQueryForSD":
                        ObjectContext.QTSDataQueryForSDs.AddObject((QTSDataQueryForSD)entity);
                        break;
                    case "QTSDataQueryForSP":
                        ObjectContext.QTSDataQueryForSPs.AddObject((QTSDataQueryForSP)entity);
                        break;
                    case "Region":
                        ObjectContext.Regions.AddObject((Region)entity);
                        break;
                    case "RegionMarketDptRelation":
                        ObjectContext.RegionMarketDptRelations.AddObject((RegionMarketDptRelation)entity);
                        break;
                    case "RegionPersonnelRelation":
                        ObjectContext.RegionPersonnelRelations.AddObject((RegionPersonnelRelation)entity);
                        break;
                    case "RepairClaimBillQuery":
                        ObjectContext.RepairClaimBillQueries.AddObject((RepairClaimBillQuery)entity);
                        break;
                    case "RepairClaimBillWithSupplier":
                        ObjectContext.RepairClaimBillWithSuppliers.AddObject((RepairClaimBillWithSupplier)entity);
                        break;
                    case "RepairItemLaborHourView":
                        ObjectContext.RepairItemLaborHourViews.AddObject((RepairItemLaborHourView)entity);
                        break;
                    case "RepairItemView":
                        ObjectContext.RepairItemViews.AddObject((RepairItemView)entity);
                        break;
                    case "RepairOrderQuery":
                        ObjectContext.RepairOrderQueries.AddObject((RepairOrderQuery)entity);
                        break;
                    case "ReportDownloadMsg":
                        ObjectContext.ReportDownloadMsgs.AddObject((ReportDownloadMsg)entity);
                        break;
                    case "ReserveFactorMasterOrder":
                        ObjectContext.ReserveFactorMasterOrders.AddObject((ReserveFactorMasterOrder)entity);
                        break;
                    case "ReserveFactorOrderDetail":
                        ObjectContext.ReserveFactorOrderDetails.AddObject((ReserveFactorOrderDetail)entity);
                        break;
                    case "ResponsibleDetail":
                        ObjectContext.ResponsibleDetails.AddObject((ResponsibleDetail)entity);
                        break;
                    case "ResponsibleMember":
                        ObjectContext.ResponsibleMembers.AddObject((ResponsibleMember)entity);
                        break;
                    case "ResponsibleUnit":
                        ObjectContext.ResponsibleUnits.AddObject((ResponsibleUnit)entity);
                        break;
                    case "ResponsibleUnitAffiProduct":
                        ObjectContext.ResponsibleUnitAffiProducts.AddObject((ResponsibleUnitAffiProduct)entity);
                        break;
                    case "ResponsibleUnitBranch":
                        ObjectContext.ResponsibleUnitBranches.AddObject((ResponsibleUnitBranch)entity);
                        break;
                    case "ResponsibleUnitProductDetail":
                        ObjectContext.ResponsibleUnitProductDetails.AddObject((ResponsibleUnitProductDetail)entity);
                        break;
                    case "ResRelationship":
                        ObjectContext.ResRelationships.AddObject((ResRelationship)entity);
                        break;
                    case "Retailer_Delivery":
                        ObjectContext.Retailer_Delivery.AddObject((Retailer_Delivery)entity);
                        break;
                    case "Retailer_DeliveryDetail":
                        ObjectContext.Retailer_DeliveryDetail.AddObject((Retailer_DeliveryDetail)entity);
                        break;
                    case "Retailer_ServiceApply":
                        ObjectContext.Retailer_ServiceApply.AddObject((Retailer_ServiceApply)entity);
                        break;
                    case "Retailer_ServiceApplyDetail":
                        ObjectContext.Retailer_ServiceApplyDetail.AddObject((Retailer_ServiceApplyDetail)entity);
                        break;
                    case "RetailOrderCustomer":
                        ObjectContext.RetailOrderCustomers.AddObject((RetailOrderCustomer)entity);
                        break;
                    case "RetainedCustomer":
                        ObjectContext.RetainedCustomers.AddObject((RetainedCustomer)entity);
                        break;
                    case "RetainedCustomerExtended":
                        ObjectContext.RetainedCustomerExtendeds.AddObject((RetainedCustomerExtended)entity);
                        break;
                    case "ReturnFreightDisposal":
                        ObjectContext.ReturnFreightDisposals.AddObject((ReturnFreightDisposal)entity);
                        break;
                    case "ReturnVisitQuest":
                        ObjectContext.ReturnVisitQuests.AddObject((ReturnVisitQuest)entity);
                        break;
                    case "ReturnVisitQuestDetail":
                        ObjectContext.ReturnVisitQuestDetails.AddObject((ReturnVisitQuestDetail)entity);
                        break;
                    case "SaleNoSettleTable":
                        ObjectContext.SaleNoSettleTables.AddObject((SaleNoSettleTable)entity);
                        break;
                    case "SalesCenterstrategy":
                        ObjectContext.SalesCenterstrategies.AddObject((SalesCenterstrategy)entity);
                        break;
                    case "SalesInvoice_tmp":
                        ObjectContext.SalesInvoice_tmp.AddObject((SalesInvoice_tmp)entity);
                        break;
                    case "SalesOrderDailyAvg":
                        ObjectContext.SalesOrderDailyAvgs.AddObject((SalesOrderDailyAvg)entity);
                        break;
                    case "SalesOrderRecommendForce":
                        ObjectContext.SalesOrderRecommendForces.AddObject((SalesOrderRecommendForce)entity);
                        break;
                    case "SalesOrderRecommendWeekly":
                        ObjectContext.SalesOrderRecommendWeeklies.AddObject((SalesOrderRecommendWeekly)entity);
                        break;
                    case "SalesRegion":
                        ObjectContext.SalesRegions.AddObject((SalesRegion)entity);
                        break;
                    case "SalesRtnSettleInvoiceRel":
                        ObjectContext.SalesRtnSettleInvoiceRels.AddObject((SalesRtnSettleInvoiceRel)entity);
                        break;
                    case "SalesSettleInvoiceResource":
                        ObjectContext.SalesSettleInvoiceResources.AddObject((SalesSettleInvoiceResource)entity);
                        break;
                    case "SalesUnit":
                        ObjectContext.SalesUnits.AddObject((SalesUnit)entity);
                        break;
                    case "SalesUnitAffiPersonnel":
                        ObjectContext.SalesUnitAffiPersonnels.AddObject((SalesUnitAffiPersonnel)entity);
                        break;
                    case "SalesUnitAffiWarehouse":
                        ObjectContext.SalesUnitAffiWarehouses.AddObject((SalesUnitAffiWarehouse)entity);
                        break;
                    case "SalesUnitAffiWhouseHistory":
                        ObjectContext.SalesUnitAffiWhouseHistories.AddObject((SalesUnitAffiWhouseHistory)entity);
                        break;
                    case "SAP_YX_InvoiceverAccountInfo":
                        ObjectContext.SAP_YX_InvoiceverAccountInfo.AddObject((SAP_YX_InvoiceverAccountInfo)entity);
                        break;
                    case "SAPInvoiceInfo":
                        ObjectContext.SAPInvoiceInfoes.AddObject((SAPInvoiceInfo)entity);
                        break;
                    case "SAPInvoiceInfo_FD":
                        ObjectContext.SAPInvoiceInfo_FD.AddObject((SAPInvoiceInfo_FD)entity);
                        break;
                    case "ScheduleExportState":
                        ObjectContext.ScheduleExportStates.AddObject((ScheduleExportState)entity);
                        break;
                    case "SecondClassStationPlan":
                        ObjectContext.SecondClassStationPlans.AddObject((SecondClassStationPlan)entity);
                        break;
                    case "SecondClassStationPlanDetail":
                        ObjectContext.SecondClassStationPlanDetails.AddObject((SecondClassStationPlanDetail)entity);
                        break;
                    case "SendWaybill":
                        ObjectContext.SendWaybills.AddObject((SendWaybill)entity);
                        break;
                    case "ServiceDealerRebate":
                        ObjectContext.ServiceDealerRebates.AddObject((ServiceDealerRebate)entity);
                        break;
                    case "ServiceLevelBySafeCf":
                        ObjectContext.ServiceLevelBySafeCfs.AddObject((ServiceLevelBySafeCf)entity);
                        break;
                    case "ServiceProductLineView":
                        ObjectContext.ServiceProductLineViews.AddObject((ServiceProductLineView)entity);
                        break;
                    case "ServiceProductLineViewAllDB":
                        ObjectContext.ServiceProductLineViewAllDBs.AddObject((ServiceProductLineViewAllDB)entity);
                        break;
                    case "ServProdLineAffiProduct":
                        ObjectContext.ServProdLineAffiProducts.AddObject((ServProdLineAffiProduct)entity);
                        break;
                    case "SettleBillInvoiceLink":
                        ObjectContext.SettleBillInvoiceLinks.AddObject((SettleBillInvoiceLink)entity);
                        break;
                    case "SettlementAutomaticTaskSet":
                        ObjectContext.SettlementAutomaticTaskSets.AddObject((SettlementAutomaticTaskSet)entity);
                        break;
                    case "SIHCenterPer":
                        ObjectContext.SIHCenterPers.AddObject((SIHCenterPer)entity);
                        break;
                    case "SIHCreditInfo":
                        ObjectContext.SIHCreditInfoes.AddObject((SIHCreditInfo)entity);
                        break;
                    case "SIHCreditInfoDetail":
                        ObjectContext.SIHCreditInfoDetails.AddObject((SIHCreditInfoDetail)entity);
                        break;
                    case "SIHDailySalesAverage":
                        ObjectContext.SIHDailySalesAverages.AddObject((SIHDailySalesAverage)entity);
                        break;
                    case "SIHRecommendPlan":
                        ObjectContext.SIHRecommendPlans.AddObject((SIHRecommendPlan)entity);
                        break;
                    case "SIHSmartOrderBase":
                        ObjectContext.SIHSmartOrderBases.AddObject((SIHSmartOrderBase)entity);
                        break;
                    case "SmartCompany":
                        ObjectContext.SmartCompanies.AddObject((SmartCompany)entity);
                        break;
                    case "SmartOrderCalendar":
                        ObjectContext.SmartOrderCalendars.AddObject((SmartOrderCalendar)entity);
                        break;
                    case "SmartProcessMethod":
                        ObjectContext.SmartProcessMethods.AddObject((SmartProcessMethod)entity);
                        break;
                    case "SparePart":
                        ObjectContext.SpareParts.AddObject((SparePart)entity);
                        break;
                    case "SparePartHistory":
                        ObjectContext.SparePartHistories.AddObject((SparePartHistory)entity);
                        break;
                    case "SpecialPriceChangeList":
                        ObjectContext.SpecialPriceChangeLists.AddObject((SpecialPriceChangeList)entity);
                        break;
                    case "SpecialTreatyPriceChange":
                        ObjectContext.SpecialTreatyPriceChanges.AddObject((SpecialTreatyPriceChange)entity);
                        break;
                    case "SsUsedPartsDisposalBill":
                        ObjectContext.SsUsedPartsDisposalBills.AddObject((SsUsedPartsDisposalBill)entity);
                        break;
                    case "SsUsedPartsDisposalDetail":
                        ObjectContext.SsUsedPartsDisposalDetails.AddObject((SsUsedPartsDisposalDetail)entity);
                        break;
                    case "SsUsedPartsStorage":
                        ObjectContext.SsUsedPartsStorages.AddObject((SsUsedPartsStorage)entity);
                        break;
                    case "SubChannelRelation":
                        ObjectContext.SubChannelRelations.AddObject((SubChannelRelation)entity);
                        break;
                    case "SubDealer":
                        ObjectContext.SubDealers.AddObject((SubDealer)entity);
                        break;
                    case "Summary":
                        ObjectContext.Summaries.AddObject((Summary)entity);
                        break;
                    case "SupplierAccount":
                        ObjectContext.SupplierAccounts.AddObject((SupplierAccount)entity);
                        break;
                    case "SupplierCollectionDetail":
                        ObjectContext.SupplierCollectionDetails.AddObject((SupplierCollectionDetail)entity);
                        break;
                    case "SupplierExpenseAdjustBill":
                        ObjectContext.SupplierExpenseAdjustBills.AddObject((SupplierExpenseAdjustBill)entity);
                        break;
                    case "SupplierInformation":
                        ObjectContext.SupplierInformations.AddObject((SupplierInformation)entity);
                        break;
                    case "SupplierOpenAccountApp":
                        ObjectContext.SupplierOpenAccountApps.AddObject((SupplierOpenAccountApp)entity);
                        break;
                    case "SupplierPlanArrear":
                        ObjectContext.SupplierPlanArrears.AddObject((SupplierPlanArrear)entity);
                        break;
                    case "SupplierPreAppLoanDetail":
                        ObjectContext.SupplierPreAppLoanDetails.AddObject((SupplierPreAppLoanDetail)entity);
                        break;
                    case "SupplierPreApprovedLoan":
                        ObjectContext.SupplierPreApprovedLoans.AddObject((SupplierPreApprovedLoan)entity);
                        break;
                    case "SupplierShippingDetail":
                        ObjectContext.SupplierShippingDetails.AddObject((SupplierShippingDetail)entity);
                        break;
                    case "SupplierShippingOrder":
                        ObjectContext.SupplierShippingOrders.AddObject((SupplierShippingOrder)entity);
                        break;
                    case "SupplierSpareOutBoud":
                        ObjectContext.SupplierSpareOutBouds.AddObject((SupplierSpareOutBoud)entity);
                        break;
                    case "SupplierStoreInboundDetail":
                        ObjectContext.SupplierStoreInboundDetails.AddObject((SupplierStoreInboundDetail)entity);
                        break;
                    case "SupplierStorePlanDetail":
                        ObjectContext.SupplierStorePlanDetails.AddObject((SupplierStorePlanDetail)entity);
                        break;
                    case "SupplierStorePlanOrder":
                        ObjectContext.SupplierStorePlanOrders.AddObject((SupplierStorePlanOrder)entity);
                        break;
                    case "SupplierTraceCode":
                        ObjectContext.SupplierTraceCodes.AddObject((SupplierTraceCode)entity);
                        break;
                    case "SupplierTraceCodeDetail":
                        ObjectContext.SupplierTraceCodeDetails.AddObject((SupplierTraceCodeDetail)entity);
                        break;
                    case "SupplierTraceDetail":
                        ObjectContext.SupplierTraceDetails.AddObject((SupplierTraceDetail)entity);
                        break;
                    case "SupplierTransferBill":
                        ObjectContext.SupplierTransferBills.AddObject((SupplierTransferBill)entity);
                        break;
                    case "TemPurchaseOrder":
                        ObjectContext.TemPurchaseOrders.AddObject((TemPurchaseOrder)entity);
                        break;
                    case "TemPurchaseOrderDetail":
                        ObjectContext.TemPurchaseOrderDetails.AddObject((TemPurchaseOrderDetail)entity);
                        break;
                    case "TemPurchasePlanOrder":
                        ObjectContext.TemPurchasePlanOrders.AddObject((TemPurchasePlanOrder)entity);
                        break;
                    case "TemPurchasePlanOrderDetail":
                        ObjectContext.TemPurchasePlanOrderDetails.AddObject((TemPurchasePlanOrderDetail)entity);
                        break;
                    case "TemShippingOrderDetail":
                        ObjectContext.TemShippingOrderDetails.AddObject((TemShippingOrderDetail)entity);
                        break;
                    case "TemSupplierShippingOrder":
                        ObjectContext.TemSupplierShippingOrders.AddObject((TemSupplierShippingOrder)entity);
                        break;
                    case "TiledRegion":
                        ObjectContext.TiledRegions.AddObject((TiledRegion)entity);
                        break;
                    case "TraceTempType":
                        ObjectContext.TraceTempTypes.AddObject((TraceTempType)entity);
                        break;
                    case "TransactionInterfaceList":
                        ObjectContext.TransactionInterfaceLists.AddObject((TransactionInterfaceList)entity);
                        break;
                    case "TransactionInterfaceLog":
                        ObjectContext.TransactionInterfaceLogs.AddObject((TransactionInterfaceLog)entity);
                        break;
                    case "UsedPartsDisposalBill":
                        ObjectContext.UsedPartsDisposalBills.AddObject((UsedPartsDisposalBill)entity);
                        break;
                    case "UsedPartsDisposalDetail":
                        ObjectContext.UsedPartsDisposalDetails.AddObject((UsedPartsDisposalDetail)entity);
                        break;
                    case "UsedPartsDistanceInfor":
                        ObjectContext.UsedPartsDistanceInfors.AddObject((UsedPartsDistanceInfor)entity);
                        break;
                    case "UsedPartsInboundDetail":
                        ObjectContext.UsedPartsInboundDetails.AddObject((UsedPartsInboundDetail)entity);
                        break;
                    case "UsedPartsInboundOrder":
                        ObjectContext.UsedPartsInboundOrders.AddObject((UsedPartsInboundOrder)entity);
                        break;
                    case "UsedPartsLoanBill":
                        ObjectContext.UsedPartsLoanBills.AddObject((UsedPartsLoanBill)entity);
                        break;
                    case "UsedPartsLoanDetail":
                        ObjectContext.UsedPartsLoanDetails.AddObject((UsedPartsLoanDetail)entity);
                        break;
                    case "UsedPartsLogisticLossBill":
                        ObjectContext.UsedPartsLogisticLossBills.AddObject((UsedPartsLogisticLossBill)entity);
                        break;
                    case "UsedPartsLogisticLossDetail":
                        ObjectContext.UsedPartsLogisticLossDetails.AddObject((UsedPartsLogisticLossDetail)entity);
                        break;
                    case "UsedPartsOutboundDetail":
                        ObjectContext.UsedPartsOutboundDetails.AddObject((UsedPartsOutboundDetail)entity);
                        break;
                    case "UsedPartsOutboundOrder":
                        ObjectContext.UsedPartsOutboundOrders.AddObject((UsedPartsOutboundOrder)entity);
                        break;
                    case "UsedPartsRefitBill":
                        ObjectContext.UsedPartsRefitBills.AddObject((UsedPartsRefitBill)entity);
                        break;
                    case "UsedPartsRefitReqDetail":
                        ObjectContext.UsedPartsRefitReqDetails.AddObject((UsedPartsRefitReqDetail)entity);
                        break;
                    case "UsedPartsRefitReturnDetail":
                        ObjectContext.UsedPartsRefitReturnDetails.AddObject((UsedPartsRefitReturnDetail)entity);
                        break;
                    case "UsedPartsReturnDetail":
                        ObjectContext.UsedPartsReturnDetails.AddObject((UsedPartsReturnDetail)entity);
                        break;
                    case "UsedPartsReturnOrder":
                        ObjectContext.UsedPartsReturnOrders.AddObject((UsedPartsReturnOrder)entity);
                        break;
                    case "UsedPartsReturnPolicyHistory":
                        ObjectContext.UsedPartsReturnPolicyHistories.AddObject((UsedPartsReturnPolicyHistory)entity);
                        break;
                    case "UsedPartsShiftDetail":
                        ObjectContext.UsedPartsShiftDetails.AddObject((UsedPartsShiftDetail)entity);
                        break;
                    case "UsedPartsShiftOrder":
                        ObjectContext.UsedPartsShiftOrders.AddObject((UsedPartsShiftOrder)entity);
                        break;
                    case "UsedPartsShippingDetail":
                        ObjectContext.UsedPartsShippingDetails.AddObject((UsedPartsShippingDetail)entity);
                        break;
                    case "UsedPartsShippingOrder":
                        ObjectContext.UsedPartsShippingOrders.AddObject((UsedPartsShippingOrder)entity);
                        break;
                    case "UsedPartsStock":
                        ObjectContext.UsedPartsStocks.AddObject((UsedPartsStock)entity);
                        break;
                    case "UsedPartsStockView":
                        ObjectContext.UsedPartsStockViews.AddObject((UsedPartsStockView)entity);
                        break;
                    case "UsedPartsTransferDetail":
                        ObjectContext.UsedPartsTransferDetails.AddObject((UsedPartsTransferDetail)entity);
                        break;
                    case "UsedPartsTransferOrder":
                        ObjectContext.UsedPartsTransferOrders.AddObject((UsedPartsTransferOrder)entity);
                        break;
                    case "UsedPartsWarehouse":
                        ObjectContext.UsedPartsWarehouses.AddObject((UsedPartsWarehouse)entity);
                        break;
                    case "UsedPartsWarehouseArea":
                        ObjectContext.UsedPartsWarehouseAreas.AddObject((UsedPartsWarehouseArea)entity);
                        break;
                    case "UsedPartsWarehouseManager":
                        ObjectContext.UsedPartsWarehouseManagers.AddObject((UsedPartsWarehouseManager)entity);
                        break;
                    case "UsedPartsWarehouseStaff":
                        ObjectContext.UsedPartsWarehouseStaffs.AddObject((UsedPartsWarehouseStaff)entity);
                        break;
                    case "VehicleCategoryAffiProduct":
                        ObjectContext.VehicleCategoryAffiProducts.AddObject((VehicleCategoryAffiProduct)entity);
                        break;
                    case "VehicleModelAffiProduct":
                        ObjectContext.VehicleModelAffiProducts.AddObject((VehicleModelAffiProduct)entity);
                        break;
                    case "VeriCodeEffectTime":
                        ObjectContext.VeriCodeEffectTimes.AddObject((VeriCodeEffectTime)entity);
                        break;
                    case "VersionInfo":
                        ObjectContext.VersionInfoes.AddObject((VersionInfo)entity);
                        break;
                    case "View_ErrorTable_SAP_YX":
                        ObjectContext.View_ErrorTable_SAP_YX.AddObject((View_ErrorTable_SAP_YX)entity);
                        break;
                    case "ViewSYNCWMSINOUTLOGINFO":
                        ObjectContext.ViewSYNCWMSINOUTLOGINFOes.AddObject((ViewSYNCWMSINOUTLOGINFO)entity);
                        break;
                    case "Warehouse":
                        ObjectContext.Warehouses.AddObject((Warehouse)entity);
                        break;
                    case "WarehouseArea":
                        ObjectContext.WarehouseAreas.AddObject((WarehouseArea)entity);
                        break;
                    case "WarehouseAreaCategory":
                        ObjectContext.WarehouseAreaCategories.AddObject((WarehouseAreaCategory)entity);
                        break;
                    case "WarehouseAreaHistory":
                        ObjectContext.WarehouseAreaHistories.AddObject((WarehouseAreaHistory)entity);
                        break;
                    case "WarehouseAreaManager":
                        ObjectContext.WarehouseAreaManagers.AddObject((WarehouseAreaManager)entity);
                        break;
                    case "WarehouseAreaWithPartsStock":
                        ObjectContext.WarehouseAreaWithPartsStocks.AddObject((WarehouseAreaWithPartsStock)entity);
                        break;
                    case "WarehouseCostChangeBill":
                        ObjectContext.WarehouseCostChangeBills.AddObject((WarehouseCostChangeBill)entity);
                        break;
                    case "WarehouseCostChangeDetail":
                        ObjectContext.WarehouseCostChangeDetails.AddObject((WarehouseCostChangeDetail)entity);
                        break;
                    case "WarehouseOperator":
                        ObjectContext.WarehouseOperators.AddObject((WarehouseOperator)entity);
                        break;
                    case "WarehouseSequence":
                        ObjectContext.WarehouseSequences.AddObject((WarehouseSequence)entity);
                        break;
                    case "WarehousingInspection":
                        ObjectContext.WarehousingInspections.AddObject((WarehousingInspection)entity);
                        break;
                    case "WmsCongelationStockView":
                        ObjectContext.WmsCongelationStockViews.AddObject((WmsCongelationStockView)entity);
                        break;
                    case "WorkGruopPersonNumber":
                        ObjectContext.WorkGruopPersonNumbers.AddObject((WorkGruopPersonNumber)entity);
                        break;
                }
            }
        }

        private void UpdateToDatabase<T>(ObjectSet<T> objectSet, T entity) where T : class {
            if(ChangeSet == null || !ChangeSet.ChangeSetEntries.Any(e => ReferenceEquals(e.Entity, entity))) {
                objectSet.AttachAsModified(entity);
                return;
            }
            var original = ChangeSet.GetOriginal(entity);
            if(original == null)
                objectSet.AttachAsModified(entity);
            else
                objectSet.AttachAsModified(entity, original);
        }

        internal void UpdateToDatabase(EntityObject entity) {
            switch(entity.GetType().Name) {
                case "ABCSetting":
                    UpdateToDatabase(ObjectContext.ABCSettings, (ABCSetting)entity);
                    break;
                case "ABCStrategy":
                    UpdateToDatabase(ObjectContext.ABCStrategies, (ABCStrategy)entity);
                    break;
                case "ABCTypeSafeDay":
                    UpdateToDatabase(ObjectContext.ABCTypeSafeDays, (ABCTypeSafeDay)entity);
                    break;
                case "ACCESSINFO_TMP":
                    UpdateToDatabase(ObjectContext.ACCESSINFO_TMP, (ACCESSINFO_TMP)entity);
                    break;
                case "AccountGroup":
                    UpdateToDatabase(ObjectContext.AccountGroups, (AccountGroup)entity);
                    break;
                case "AccountPayableHistoryDetail":
                    UpdateToDatabase(ObjectContext.AccountPayableHistoryDetails, (AccountPayableHistoryDetail)entity);
                    break;
                case "AccountPeriod":
                    UpdateToDatabase(ObjectContext.AccountPeriods, (AccountPeriod)entity);
                    break;
                case "AccurateTrace":
                    UpdateToDatabase(ObjectContext.AccurateTraces, (AccurateTrace)entity);
                    break;
                case "Agency":
                    UpdateToDatabase(ObjectContext.Agencies, (Agency)entity);
                    break;
                case "AgencyAffiBranch":
                    UpdateToDatabase(ObjectContext.AgencyAffiBranches, (AgencyAffiBranch)entity);
                    break;
                case "AgencyDealerRelation":
                    UpdateToDatabase(ObjectContext.AgencyDealerRelations, (AgencyDealerRelation)entity);
                    break;
                case "AgencyDealerRelationHistory":
                    UpdateToDatabase(ObjectContext.AgencyDealerRelationHistories, (AgencyDealerRelationHistory)entity);
                    break;
                case "AgencyDifferenceBackBill":
                    UpdateToDatabase(ObjectContext.AgencyDifferenceBackBills, (AgencyDifferenceBackBill)entity);
                    break;
                case "AgencyLogisticCompany":
                    UpdateToDatabase(ObjectContext.AgencyLogisticCompanies, (AgencyLogisticCompany)entity);
                    break;
                case "AgencyPartsOutboundBill":
                    UpdateToDatabase(ObjectContext.AgencyPartsOutboundBills, (AgencyPartsOutboundBill)entity);
                    break;
                case "AgencyPartsOutboundPlan":
                    UpdateToDatabase(ObjectContext.AgencyPartsOutboundPlans, (AgencyPartsOutboundPlan)entity);
                    break;
                case "AgencyPartsShippingOrder":
                    UpdateToDatabase(ObjectContext.AgencyPartsShippingOrders, (AgencyPartsShippingOrder)entity);
                    break;
                case "AgencyPartsShippingOrderRef":
                    UpdateToDatabase(ObjectContext.AgencyPartsShippingOrderRefs, (AgencyPartsShippingOrderRef)entity);
                    break;
                case "AgencyRetailerList":
                    UpdateToDatabase(ObjectContext.AgencyRetailerLists, (AgencyRetailerList)entity);
                    break;
                case "AgencyRetailerOrder":
                    UpdateToDatabase(ObjectContext.AgencyRetailerOrders, (AgencyRetailerOrder)entity);
                    break;
                case "AgencyStockQryFrmOrder":
                    UpdateToDatabase(ObjectContext.AgencyStockQryFrmOrders, (AgencyStockQryFrmOrder)entity);
                    break;
                case "APartsOutboundBillDetail":
                    UpdateToDatabase(ObjectContext.APartsOutboundBillDetails, (APartsOutboundBillDetail)entity);
                    break;
                case "APartsOutboundPlanDetail":
                    UpdateToDatabase(ObjectContext.APartsOutboundPlanDetails, (APartsOutboundPlanDetail)entity);
                    break;
                case "APartsShippingOrderDetail":
                    UpdateToDatabase(ObjectContext.APartsShippingOrderDetails, (APartsShippingOrderDetail)entity);
                    break;
                case "ApplicationType":
                    UpdateToDatabase(ObjectContext.ApplicationTypes, (ApplicationType)entity);
                    break;
                case "AppointDealerDtl":
                    UpdateToDatabase(ObjectContext.AppointDealerDtls, (AppointDealerDtl)entity);
                    break;
                case "AppointFaultReasonDtl":
                    UpdateToDatabase(ObjectContext.AppointFaultReasonDtls, (AppointFaultReasonDtl)entity);
                    break;
                case "AppointSupplierDtl":
                    UpdateToDatabase(ObjectContext.AppointSupplierDtls, (AppointSupplierDtl)entity);
                    break;
                case "AssemblyPartRequisitionLink":
                    UpdateToDatabase(ObjectContext.AssemblyPartRequisitionLinks, (AssemblyPartRequisitionLink)entity);
                    break;
                case "AuthenticationType":
                    UpdateToDatabase(ObjectContext.AuthenticationTypes, (AuthenticationType)entity);
                    break;
                case "AutoSalesOrderProcess":
                    UpdateToDatabase(ObjectContext.AutoSalesOrderProcesses, (AutoSalesOrderProcess)entity);
                    break;
                case "AutoTaskExecutResult":
                    UpdateToDatabase(ObjectContext.AutoTaskExecutResults, (AutoTaskExecutResult)entity);
                    break;
                case "BankAccount":
                    UpdateToDatabase(ObjectContext.BankAccounts, (BankAccount)entity);
                    break;
                case "BonusPointsOrder":
                    UpdateToDatabase(ObjectContext.BonusPointsOrders, (BonusPointsOrder)entity);
                    break;
                case "BonusPointsOrderList":
                    UpdateToDatabase(ObjectContext.BonusPointsOrderLists, (BonusPointsOrderList)entity);
                    break;
                case "BonusPointsSummary":
                    UpdateToDatabase(ObjectContext.BonusPointsSummaries, (BonusPointsSummary)entity);
                    break;
                case "BonusPointsSummaryList":
                    UpdateToDatabase(ObjectContext.BonusPointsSummaryLists, (BonusPointsSummaryList)entity);
                    break;
                case "BorrowBill":
                    UpdateToDatabase(ObjectContext.BorrowBills, (BorrowBill)entity);
                    break;
                case "BorrowBillDetail":
                    UpdateToDatabase(ObjectContext.BorrowBillDetails, (BorrowBillDetail)entity);
                    break;
                case "BottomStock":
                    UpdateToDatabase(ObjectContext.BottomStocks, (BottomStock)entity);
                    break;
                case "BottomStockColVersion":
                    UpdateToDatabase(ObjectContext.BottomStockColVersions, (BottomStockColVersion)entity);
                    break;
                case "BottomStockForceReserveSub":
                    UpdateToDatabase(ObjectContext.BottomStockForceReserveSubs, (BottomStockForceReserveSub)entity);
                    break;
                case "BottomStockForceReserveType":
                    UpdateToDatabase(ObjectContext.BottomStockForceReserveTypes, (BottomStockForceReserveType)entity);
                    break;
                case "BottomStockSettleTable":
                    UpdateToDatabase(ObjectContext.BottomStockSettleTables, (BottomStockSettleTable)entity);
                    break;
                case "BottomStockSubVersion":
                    UpdateToDatabase(ObjectContext.BottomStockSubVersions, (BottomStockSubVersion)entity);
                    break;
                case "BoxUpTask":
                    UpdateToDatabase(ObjectContext.BoxUpTasks, (BoxUpTask)entity);
                    break;
                case "BoxUpTaskDetail":
                    UpdateToDatabase(ObjectContext.BoxUpTaskDetails, (BoxUpTaskDetail)entity);
                    break;
                case "Branch":
                    UpdateToDatabase(ObjectContext.Branches, (Branch)entity);
                    break;
                case "BranchDateBaseRel":
                    UpdateToDatabase(ObjectContext.BranchDateBaseRels, (BranchDateBaseRel)entity);
                    break;
                case "Branchstrategy":
                    UpdateToDatabase(ObjectContext.Branchstrategies, (Branchstrategy)entity);
                    break;
                case "BranchSupplierRelation":
                    UpdateToDatabase(ObjectContext.BranchSupplierRelations, (BranchSupplierRelation)entity);
                    break;
                case "BrandGradeMessage":
                    UpdateToDatabase(ObjectContext.BrandGradeMessages, (BrandGradeMessage)entity);
                    break;
                case "CAReconciliation":
                    UpdateToDatabase(ObjectContext.CAReconciliations, (CAReconciliation)entity);
                    break;
                case "CAReconciliationList":
                    UpdateToDatabase(ObjectContext.CAReconciliationLists, (CAReconciliationList)entity);
                    break;
                case "CenterABCBasi":
                    UpdateToDatabase(ObjectContext.CenterABCBasis, (CenterABCBasi)entity);
                    break;
                case "CenterAccumulateDailyExp":
                    UpdateToDatabase(ObjectContext.CenterAccumulateDailyExps, (CenterAccumulateDailyExp)entity);
                    break;
                case "CenterApproveBill":
                    UpdateToDatabase(ObjectContext.CenterApproveBills, (CenterApproveBill)entity);
                    break;
                case "CenterAuthorizedProvince":
                    UpdateToDatabase(ObjectContext.CenterAuthorizedProvinces, (CenterAuthorizedProvince)entity);
                    break;
                case "CenterQuarterRebate":
                    UpdateToDatabase(ObjectContext.CenterQuarterRebates, (CenterQuarterRebate)entity);
                    break;
                case "CenterRebateSummary":
                    UpdateToDatabase(ObjectContext.CenterRebateSummaries, (CenterRebateSummary)entity);
                    break;
                case "CenterSaleReturnStrategy":
                    UpdateToDatabase(ObjectContext.CenterSaleReturnStrategies, (CenterSaleReturnStrategy)entity);
                    break;
                case "CenterSaleSettle":
                    UpdateToDatabase(ObjectContext.CenterSaleSettles, (CenterSaleSettle)entity);
                    break;
                case "ChannelCapability":
                    UpdateToDatabase(ObjectContext.ChannelCapabilities, (ChannelCapability)entity);
                    break;
                case "ChannelPlan":
                    UpdateToDatabase(ObjectContext.ChannelPlans, (ChannelPlan)entity);
                    break;
                case "ChannelPlanDetail":
                    UpdateToDatabase(ObjectContext.ChannelPlanDetails, (ChannelPlanDetail)entity);
                    break;
                case "ChannelPlanHistory":
                    UpdateToDatabase(ObjectContext.ChannelPlanHistories, (ChannelPlanHistory)entity);
                    break;
                case "ChannelPlanHistoryDetail":
                    UpdateToDatabase(ObjectContext.ChannelPlanHistoryDetails, (ChannelPlanHistoryDetail)entity);
                    break;
                case "ChassisInformation":
                    UpdateToDatabase(ObjectContext.ChassisInformations, (ChassisInformation)entity);
                    break;
                case "ClaimApproverStrategy":
                    UpdateToDatabase(ObjectContext.ClaimApproverStrategies, (ClaimApproverStrategy)entity);
                    break;
                case "ClaimSettleBillDealerInvoice":
                    UpdateToDatabase(ObjectContext.ClaimSettleBillDealerInvoices, (ClaimSettleBillDealerInvoice)entity);
                    break;
                case "CodeTemplate":
                    UpdateToDatabase(ObjectContext.CodeTemplates, (CodeTemplate)entity);
                    break;
                case "CodeTemplateSerial":
                    UpdateToDatabase(ObjectContext.CodeTemplateSerials, (CodeTemplateSerial)entity);
                    break;
                case "CombinedPart":
                    UpdateToDatabase(ObjectContext.CombinedParts, (CombinedPart)entity);
                    break;
                case "Company":
                    UpdateToDatabase(ObjectContext.Companies, (Company)entity);
                    break;
                case "company_log":
                    UpdateToDatabase(ObjectContext.company_log, (company_log)entity);
                    break;
                case "CompanyAddress":
                    UpdateToDatabase(ObjectContext.CompanyAddresses, (CompanyAddress)entity);
                    break;
                case "CompanyDetail":
                    UpdateToDatabase(ObjectContext.CompanyDetails, (CompanyDetail)entity);
                    break;
                case "CompanyInvoiceInfo":
                    UpdateToDatabase(ObjectContext.CompanyInvoiceInfoes, (CompanyInvoiceInfo)entity);
                    break;
                case "CompanyLoginPicture":
                    UpdateToDatabase(ObjectContext.CompanyLoginPictures, (CompanyLoginPicture)entity);
                    break;
                case "CompanyTransferOrder":
                    UpdateToDatabase(ObjectContext.CompanyTransferOrders, (CompanyTransferOrder)entity);
                    break;
                case "CompanyTransferOrderDetail":
                    UpdateToDatabase(ObjectContext.CompanyTransferOrderDetails, (CompanyTransferOrderDetail)entity);
                    break;
                case "Countersignature":
                    UpdateToDatabase(ObjectContext.Countersignatures, (Countersignature)entity);
                    break;
                case "CountersignatureDetail":
                    UpdateToDatabase(ObjectContext.CountersignatureDetails, (CountersignatureDetail)entity);
                    break;
                case "CredenceApplication":
                    UpdateToDatabase(ObjectContext.CredenceApplications, (CredenceApplication)entity);
                    break;
                case "CrossSalesOrder":
                    UpdateToDatabase(ObjectContext.CrossSalesOrders, (CrossSalesOrder)entity);
                    break;
                case "CrossSalesOrderDetail":
                    UpdateToDatabase(ObjectContext.CrossSalesOrderDetails, (CrossSalesOrderDetail)entity);
                    break;
                case "Customer":
                    UpdateToDatabase(ObjectContext.Customers, (Customer)entity);
                    break;
                case "CustomerAccount":
                    UpdateToDatabase(ObjectContext.CustomerAccounts, (CustomerAccount)entity);
                    break;
                case "CustomerAccountHisDetail":
                    UpdateToDatabase(ObjectContext.CustomerAccountHisDetails, (CustomerAccountHisDetail)entity);
                    break;
                case "CustomerAccountHistory":
                    UpdateToDatabase(ObjectContext.CustomerAccountHistories, (CustomerAccountHistory)entity);
                    break;
                case "CustomerDirectSpareList":
                    UpdateToDatabase(ObjectContext.CustomerDirectSpareLists, (CustomerDirectSpareList)entity);
                    break;
                case "CustomerInformation":
                    UpdateToDatabase(ObjectContext.CustomerInformations, (CustomerInformation)entity);
                    break;
                case "CustomerOpenAccountApp":
                    UpdateToDatabase(ObjectContext.CustomerOpenAccountApps, (CustomerOpenAccountApp)entity);
                    break;
                case "CustomerOrderPriceGrade":
                    UpdateToDatabase(ObjectContext.CustomerOrderPriceGrades, (CustomerOrderPriceGrade)entity);
                    break;
                case "CustomerOrderPriceGradeHistory":
                    UpdateToDatabase(ObjectContext.CustomerOrderPriceGradeHistories, (CustomerOrderPriceGradeHistory)entity);
                    break;
                case "CustomerSupplyInitialFeeSet":
                    UpdateToDatabase(ObjectContext.CustomerSupplyInitialFeeSets, (CustomerSupplyInitialFeeSet)entity);
                    break;
                case "CustomerTransferBill":
                    UpdateToDatabase(ObjectContext.CustomerTransferBills, (CustomerTransferBill)entity);
                    break;
                case "CustomerTransferBill_Sync":
                    UpdateToDatabase(ObjectContext.CustomerTransferBill_Sync, (CustomerTransferBill_Sync)entity);
                    break;
                case "DailySalesWeight":
                    UpdateToDatabase(ObjectContext.DailySalesWeights, (DailySalesWeight)entity);
                    break;
                case "DataBaseLink":
                    UpdateToDatabase(ObjectContext.DataBaseLinks, (DataBaseLink)entity);
                    break;
                case "DataBaseLinkBranch":
                    UpdateToDatabase(ObjectContext.DataBaseLinkBranches, (DataBaseLinkBranch)entity);
                    break;
                case "Dealer":
                    UpdateToDatabase(ObjectContext.Dealers, (Dealer)entity);
                    break;
                case "DealerBusinessPermit":
                    UpdateToDatabase(ObjectContext.DealerBusinessPermits, (DealerBusinessPermit)entity);
                    break;
                case "DealerBusinessPermitHistory":
                    UpdateToDatabase(ObjectContext.DealerBusinessPermitHistories, (DealerBusinessPermitHistory)entity);
                    break;
                case "DealerClaimSparePart":
                    UpdateToDatabase(ObjectContext.DealerClaimSpareParts, (DealerClaimSparePart)entity);
                    break;
                case "DealerFormat":
                    UpdateToDatabase(ObjectContext.DealerFormats, (DealerFormat)entity);
                    break;
                case "DealerGradeInfo":
                    UpdateToDatabase(ObjectContext.DealerGradeInfoes, (DealerGradeInfo)entity);
                    break;
                case "DealerHistory":
                    UpdateToDatabase(ObjectContext.DealerHistories, (DealerHistory)entity);
                    break;
                case "DealerInvoice":
                    UpdateToDatabase(ObjectContext.DealerInvoices, (DealerInvoice)entity);
                    break;
                case "DealerKeyEmployee":
                    UpdateToDatabase(ObjectContext.DealerKeyEmployees, (DealerKeyEmployee)entity);
                    break;
                case "DealerKeyEmployeeHistory":
                    UpdateToDatabase(ObjectContext.DealerKeyEmployeeHistories, (DealerKeyEmployeeHistory)entity);
                    break;
                case "DealerKeyPosition":
                    UpdateToDatabase(ObjectContext.DealerKeyPositions, (DealerKeyPosition)entity);
                    break;
                case "DealerMarketDptRelation":
                    UpdateToDatabase(ObjectContext.DealerMarketDptRelations, (DealerMarketDptRelation)entity);
                    break;
                case "DealerMobileNumberHistory":
                    UpdateToDatabase(ObjectContext.DealerMobileNumberHistories, (DealerMobileNumberHistory)entity);
                    break;
                case "DealerMobileNumberList":
                    UpdateToDatabase(ObjectContext.DealerMobileNumberLists, (DealerMobileNumberList)entity);
                    break;
                case "DealerPartsInventoryBill":
                    UpdateToDatabase(ObjectContext.DealerPartsInventoryBills, (DealerPartsInventoryBill)entity);
                    break;
                case "DealerPartsInventoryDetail":
                    UpdateToDatabase(ObjectContext.DealerPartsInventoryDetails, (DealerPartsInventoryDetail)entity);
                    break;
                case "DealerPartsRetailOrder":
                    UpdateToDatabase(ObjectContext.DealerPartsRetailOrders, (DealerPartsRetailOrder)entity);
                    break;
                case "DealerPartsSalesPrice":
                    UpdateToDatabase(ObjectContext.DealerPartsSalesPrices, (DealerPartsSalesPrice)entity);
                    break;
                case "DealerPartsSalesPriceHistory":
                    UpdateToDatabase(ObjectContext.DealerPartsSalesPriceHistories, (DealerPartsSalesPriceHistory)entity);
                    break;
                case "DealerPartsSalesReturnBill":
                    UpdateToDatabase(ObjectContext.DealerPartsSalesReturnBills, (DealerPartsSalesReturnBill)entity);
                    break;
                case "DealerPartsStock":
                    UpdateToDatabase(ObjectContext.DealerPartsStocks, (DealerPartsStock)entity);
                    break;
                case "DealerPartsStockOutInRecord":
                    UpdateToDatabase(ObjectContext.DealerPartsStockOutInRecords, (DealerPartsStockOutInRecord)entity);
                    break;
                case "DealerPartsStockQueryView":
                    UpdateToDatabase(ObjectContext.DealerPartsStockQueryViews, (DealerPartsStockQueryView)entity);
                    break;
                case "DealerPartsTransferOrder":
                    UpdateToDatabase(ObjectContext.DealerPartsTransferOrders, (DealerPartsTransferOrder)entity);
                    break;
                case "DealerPartsTransOrderDetail":
                    UpdateToDatabase(ObjectContext.DealerPartsTransOrderDetails, (DealerPartsTransOrderDetail)entity);
                    break;
                case "DealerPerTrainAut":
                    UpdateToDatabase(ObjectContext.DealerPerTrainAuts, (DealerPerTrainAut)entity);
                    break;
                case "DealerRetailOrderDetail":
                    UpdateToDatabase(ObjectContext.DealerRetailOrderDetails, (DealerRetailOrderDetail)entity);
                    break;
                case "DealerRetailReturnBillDetail":
                    UpdateToDatabase(ObjectContext.DealerRetailReturnBillDetails, (DealerRetailReturnBillDetail)entity);
                    break;
                case "DealerServiceExt":
                    UpdateToDatabase(ObjectContext.DealerServiceExts, (DealerServiceExt)entity);
                    break;
                case "DealerServiceExtHistory":
                    UpdateToDatabase(ObjectContext.DealerServiceExtHistories, (DealerServiceExtHistory)entity);
                    break;
                case "DealerServiceInfo":
                    UpdateToDatabase(ObjectContext.DealerServiceInfoes, (DealerServiceInfo)entity);
                    break;
                case "DealerServiceInfoHistory":
                    UpdateToDatabase(ObjectContext.DealerServiceInfoHistories, (DealerServiceInfoHistory)entity);
                    break;
                case "DeferredDiscountType":
                    UpdateToDatabase(ObjectContext.DeferredDiscountTypes, (DeferredDiscountType)entity);
                    break;
                case "DepartmentBrandRelation":
                    UpdateToDatabase(ObjectContext.DepartmentBrandRelations, (DepartmentBrandRelation)entity);
                    break;
                case "DepartmentInformation":
                    UpdateToDatabase(ObjectContext.DepartmentInformations, (DepartmentInformation)entity);
                    break;
                case "EcommerceFreightDisposal":
                    UpdateToDatabase(ObjectContext.EcommerceFreightDisposals, (EcommerceFreightDisposal)entity);
                    break;
                case "EngineModelview":
                    UpdateToDatabase(ObjectContext.EngineModelviews, (EngineModelview)entity);
                    break;
                case "EnterprisePartsCost":
                    UpdateToDatabase(ObjectContext.EnterprisePartsCosts, (EnterprisePartsCost)entity);
                    break;
                case "ExportCustomerInfo":
                    UpdateToDatabase(ObjectContext.ExportCustomerInfoes, (ExportCustomerInfo)entity);
                    break;
                case "Express":
                    UpdateToDatabase(ObjectContext.Expresses, (Express)entity);
                    break;
                case "ExpressToLogistic":
                    UpdateToDatabase(ObjectContext.ExpressToLogistics, (ExpressToLogistic)entity);
                    break;
                case "ExtendedInfoTemplate":
                    UpdateToDatabase(ObjectContext.ExtendedInfoTemplates, (ExtendedInfoTemplate)entity);
                    break;
                case "ExtendedSparePartList":
                    UpdateToDatabase(ObjectContext.ExtendedSparePartLists, (ExtendedSparePartList)entity);
                    break;
                case "ExtendedWarrantyOrder":
                    UpdateToDatabase(ObjectContext.ExtendedWarrantyOrders, (ExtendedWarrantyOrder)entity);
                    break;
                case "ExtendedWarrantyOrderList":
                    UpdateToDatabase(ObjectContext.ExtendedWarrantyOrderLists, (ExtendedWarrantyOrderList)entity);
                    break;
                case "ExtendedWarrantyProduct":
                    UpdateToDatabase(ObjectContext.ExtendedWarrantyProducts, (ExtendedWarrantyProduct)entity);
                    break;
                case "FactoryPurchacePrice_Tmp":
                    UpdateToDatabase(ObjectContext.FactoryPurchacePrice_Tmp, (FactoryPurchacePrice_Tmp)entity);
                    break;
                case "FaultyPartsSupplierAssembly":
                    UpdateToDatabase(ObjectContext.FaultyPartsSupplierAssemblies, (FaultyPartsSupplierAssembly)entity);
                    break;
                case "FDWarrantyMessage":
                    UpdateToDatabase(ObjectContext.FDWarrantyMessages, (FDWarrantyMessage)entity);
                    break;
                case "FinancialSnapshotSet":
                    UpdateToDatabase(ObjectContext.FinancialSnapshotSets, (FinancialSnapshotSet)entity);
                    break;
                case "ForceReserveBill":
                    UpdateToDatabase(ObjectContext.ForceReserveBills, (ForceReserveBill)entity);
                    break;
                case "ForceReserveBillDetail":
                    UpdateToDatabase(ObjectContext.ForceReserveBillDetails, (ForceReserveBillDetail)entity);
                    break;
                case "FullStockTask":
                    UpdateToDatabase(ObjectContext.FullStockTasks, (FullStockTask)entity);
                    break;
                case "FundMonthlySettleBill":
                    UpdateToDatabase(ObjectContext.FundMonthlySettleBills, (FundMonthlySettleBill)entity);
                    break;
                case "FundMonthlySettleBillDetail":
                    UpdateToDatabase(ObjectContext.FundMonthlySettleBillDetails, (FundMonthlySettleBillDetail)entity);
                    break;
                case "GetAllPartsPlannedPrice":
                    UpdateToDatabase(ObjectContext.GetAllPartsPlannedPrices, (GetAllPartsPlannedPrice)entity);
                    break;
                case "GetAllPartsPurchasePricing":
                    UpdateToDatabase(ObjectContext.GetAllPartsPurchasePricings, (GetAllPartsPurchasePricing)entity);
                    break;
                case "GetAllPartsSalesPrice":
                    UpdateToDatabase(ObjectContext.GetAllPartsSalesPrices, (GetAllPartsSalesPrice)entity);
                    break;
                case "GoldenTaxClassifyMsg":
                    UpdateToDatabase(ObjectContext.GoldenTaxClassifyMsgs, (GoldenTaxClassifyMsg)entity);
                    break;
                case "GradeCoefficient":
                    UpdateToDatabase(ObjectContext.GradeCoefficients, (GradeCoefficient)entity);
                    break;
                case "GradeCoefficientView":
                    UpdateToDatabase(ObjectContext.GradeCoefficientViews, (GradeCoefficientView)entity);
                    break;
                case "HauDistanceInfor":
                    UpdateToDatabase(ObjectContext.HauDistanceInfors, (HauDistanceInfor)entity);
                    break;
                case "InOutSettleSummary":
                    UpdateToDatabase(ObjectContext.InOutSettleSummaries, (InOutSettleSummary)entity);
                    break;
                case "InOutWorkPerHour":
                    UpdateToDatabase(ObjectContext.InOutWorkPerHours, (InOutWorkPerHour)entity);
                    break;
                case "IntegralSettleDictate":
                    UpdateToDatabase(ObjectContext.IntegralSettleDictates, (IntegralSettleDictate)entity);
                    break;
                case "IntelligentOrderMonthly":
                    UpdateToDatabase(ObjectContext.IntelligentOrderMonthlies, (IntelligentOrderMonthly)entity);
                    break;
                case "IntelligentOrderMonthlyBase":
                    UpdateToDatabase(ObjectContext.IntelligentOrderMonthlyBases, (IntelligentOrderMonthlyBase)entity);
                    break;
                case "IntelligentOrderWeekly":
                    UpdateToDatabase(ObjectContext.IntelligentOrderWeeklies, (IntelligentOrderWeekly)entity);
                    break;
                case "IntelligentOrderWeeklyBase":
                    UpdateToDatabase(ObjectContext.IntelligentOrderWeeklyBases, (IntelligentOrderWeeklyBase)entity);
                    break;
                case "InternalAcquisitionBill":
                    UpdateToDatabase(ObjectContext.InternalAcquisitionBills, (InternalAcquisitionBill)entity);
                    break;
                case "InternalAcquisitionDetail":
                    UpdateToDatabase(ObjectContext.InternalAcquisitionDetails, (InternalAcquisitionDetail)entity);
                    break;
                case "InternalAllocationBill":
                    UpdateToDatabase(ObjectContext.InternalAllocationBills, (InternalAllocationBill)entity);
                    break;
                case "InternalAllocationDetail":
                    UpdateToDatabase(ObjectContext.InternalAllocationDetails, (InternalAllocationDetail)entity);
                    break;
                case "InteSettleBillInvoiceLink":
                    UpdateToDatabase(ObjectContext.InteSettleBillInvoiceLinks, (InteSettleBillInvoiceLink)entity);
                    break;
                case "InvoiceInformation":
                    UpdateToDatabase(ObjectContext.InvoiceInformations, (InvoiceInformation)entity);
                    break;
                case "IvecoPriceChangeApp":
                    UpdateToDatabase(ObjectContext.IvecoPriceChangeApps, (IvecoPriceChangeApp)entity);
                    break;
                case "IvecoPriceChangeAppDetail":
                    UpdateToDatabase(ObjectContext.IvecoPriceChangeAppDetails, (IvecoPriceChangeAppDetail)entity);
                    break;
                case "IvecoSalesPrice":
                    UpdateToDatabase(ObjectContext.IvecoSalesPrices, (IvecoSalesPrice)entity);
                    break;
                case "KeyValueItem":
                    UpdateToDatabase(ObjectContext.KeyValueItems, (KeyValueItem)entity);
                    break;
                case "LayerNodeType":
                    UpdateToDatabase(ObjectContext.LayerNodeTypes, (LayerNodeType)entity);
                    break;
                case "LayerNodeTypeAffiliation":
                    UpdateToDatabase(ObjectContext.LayerNodeTypeAffiliations, (LayerNodeTypeAffiliation)entity);
                    break;
                case "LayerStructure":
                    UpdateToDatabase(ObjectContext.LayerStructures, (LayerStructure)entity);
                    break;
                case "LayerStructurePurpose":
                    UpdateToDatabase(ObjectContext.LayerStructurePurposes, (LayerStructurePurpose)entity);
                    break;
                case "LinkAllocation":
                    UpdateToDatabase(ObjectContext.LinkAllocations, (LinkAllocation)entity);
                    break;
                case "LoadingDetail":
                    UpdateToDatabase(ObjectContext.LoadingDetails, (LoadingDetail)entity);
                    break;
                case "LocketUnitQuery":
                    UpdateToDatabase(ObjectContext.LocketUnitQueries, (LocketUnitQuery)entity);
                    break;
                case "Logistic":
                    UpdateToDatabase(ObjectContext.Logistics, (Logistic)entity);
                    break;
                case "LogisticCompany":
                    UpdateToDatabase(ObjectContext.LogisticCompanies, (LogisticCompany)entity);
                    break;
                case "LogisticCompanyServiceRange":
                    UpdateToDatabase(ObjectContext.LogisticCompanyServiceRanges, (LogisticCompanyServiceRange)entity);
                    break;
                case "LogisticsDetail":
                    UpdateToDatabase(ObjectContext.LogisticsDetails, (LogisticsDetail)entity);
                    break;
                case "MaintainRedProductMap":
                    UpdateToDatabase(ObjectContext.MaintainRedProductMaps, (MaintainRedProductMap)entity);
                    break;
                case "MalfunctionView":
                    UpdateToDatabase(ObjectContext.MalfunctionViews, (MalfunctionView)entity);
                    break;
                case "ManualScheduling":
                    UpdateToDatabase(ObjectContext.ManualSchedulings, (ManualScheduling)entity);
                    break;
                case "MarketABQualityInformation":
                    UpdateToDatabase(ObjectContext.MarketABQualityInformations, (MarketABQualityInformation)entity);
                    break;
                case "MarketDptPersonnelRelation":
                    UpdateToDatabase(ObjectContext.MarketDptPersonnelRelations, (MarketDptPersonnelRelation)entity);
                    break;
                case "MarketingDepartment":
                    UpdateToDatabase(ObjectContext.MarketingDepartments, (MarketingDepartment)entity);
                    break;
                case "MultiLevelApproveConfig":
                    UpdateToDatabase(ObjectContext.MultiLevelApproveConfigs, (MultiLevelApproveConfig)entity);
                    break;
                case "Notification":
                    UpdateToDatabase(ObjectContext.Notifications, (Notification)entity);
                    break;
                case "NotificationLimit":
                    UpdateToDatabase(ObjectContext.NotificationLimits, (NotificationLimit)entity);
                    break;
                case "NotificationOpLog":
                    UpdateToDatabase(ObjectContext.NotificationOpLogs, (NotificationOpLog)entity);
                    break;
                case "NotificationReply":
                    UpdateToDatabase(ObjectContext.NotificationReplies, (NotificationReply)entity);
                    break;
                case "OrderApproveWeekday":
                    UpdateToDatabase(ObjectContext.OrderApproveWeekdays, (OrderApproveWeekday)entity);
                    break;
                case "OutboundOrder":
                    UpdateToDatabase(ObjectContext.OutboundOrders, (OutboundOrder)entity);
                    break;
                case "OutofWarrantyPayment":
                    UpdateToDatabase(ObjectContext.OutofWarrantyPayments, (OutofWarrantyPayment)entity);
                    break;
                case "OverstockPartsAdjustBill":
                    UpdateToDatabase(ObjectContext.OverstockPartsAdjustBills, (OverstockPartsAdjustBill)entity);
                    break;
                case "OverstockPartsAdjustDetail":
                    UpdateToDatabase(ObjectContext.OverstockPartsAdjustDetails, (OverstockPartsAdjustDetail)entity);
                    break;
                case "OverstockPartsApp":
                    UpdateToDatabase(ObjectContext.OverstockPartsApps, (OverstockPartsApp)entity);
                    break;
                case "OverstockPartsAppDetail":
                    UpdateToDatabase(ObjectContext.OverstockPartsAppDetails, (OverstockPartsAppDetail)entity);
                    break;
                case "OverstockPartsInformation":
                    UpdateToDatabase(ObjectContext.OverstockPartsInformations, (OverstockPartsInformation)entity);
                    break;
                case "OverstockPartsPlatFormBill":
                    UpdateToDatabase(ObjectContext.OverstockPartsPlatFormBills, (OverstockPartsPlatFormBill)entity);
                    break;
                case "OverstockPartsRecommendBill":
                    UpdateToDatabase(ObjectContext.OverstockPartsRecommendBills, (OverstockPartsRecommendBill)entity);
                    break;
                case "OverstockTransferOrder":
                    UpdateToDatabase(ObjectContext.OverstockTransferOrders, (OverstockTransferOrder)entity);
                    break;
                case "OverstockTransferOrderDetail":
                    UpdateToDatabase(ObjectContext.OverstockTransferOrderDetails, (OverstockTransferOrderDetail)entity);
                    break;
                case "PackingTask":
                    UpdateToDatabase(ObjectContext.PackingTasks, (PackingTask)entity);
                    break;
                case "PackingTaskDetail":
                    UpdateToDatabase(ObjectContext.PackingTaskDetails, (PackingTaskDetail)entity);
                    break;
                case "PartABCDetail":
                    UpdateToDatabase(ObjectContext.PartABCDetails, (PartABCDetail)entity);
                    break;
                case "PartConPurchasePlan":
                    UpdateToDatabase(ObjectContext.PartConPurchasePlans, (PartConPurchasePlan)entity);
                    break;
                case "PartDeleaveHistory":
                    UpdateToDatabase(ObjectContext.PartDeleaveHistories, (PartDeleaveHistory)entity);
                    break;
                case "PartDeleaveInformation":
                    UpdateToDatabase(ObjectContext.PartDeleaveInformations, (PartDeleaveInformation)entity);
                    break;
                case "PartNotConPurchasePlan":
                    UpdateToDatabase(ObjectContext.PartNotConPurchasePlans, (PartNotConPurchasePlan)entity);
                    break;
                case "PartRequisitionReturnBill":
                    UpdateToDatabase(ObjectContext.PartRequisitionReturnBills, (PartRequisitionReturnBill)entity);
                    break;
                case "PartSafeStock":
                    UpdateToDatabase(ObjectContext.PartSafeStocks, (PartSafeStock)entity);
                    break;
                case "PartsBatchRecord":
                    UpdateToDatabase(ObjectContext.PartsBatchRecords, (PartsBatchRecord)entity);
                    break;
                case "PartsBranch":
                    UpdateToDatabase(ObjectContext.PartsBranches, (PartsBranch)entity);
                    break;
                case "PartsBranchHistory":
                    UpdateToDatabase(ObjectContext.PartsBranchHistories, (PartsBranchHistory)entity);
                    break;
                case "PartsBranchPackingProp":
                    UpdateToDatabase(ObjectContext.PartsBranchPackingProps, (PartsBranchPackingProp)entity);
                    break;
                case "PartsBranchView":
                    UpdateToDatabase(ObjectContext.PartsBranchViews, (PartsBranchView)entity);
                    break;
                case "PartsClaimOrderNew":
                    UpdateToDatabase(ObjectContext.PartsClaimOrderNews, (PartsClaimOrderNew)entity);
                    break;
                case "PartsClaimPrice":
                    UpdateToDatabase(ObjectContext.PartsClaimPrices, (PartsClaimPrice)entity);
                    break;
                case "PartsClaimPriceForbw":
                    UpdateToDatabase(ObjectContext.PartsClaimPriceForbws, (PartsClaimPriceForbw)entity);
                    break;
                case "PartsCostTransferInTransit":
                    UpdateToDatabase(ObjectContext.PartsCostTransferInTransits, (PartsCostTransferInTransit)entity);
                    break;
                case "PartsDifferenceBackBill":
                    UpdateToDatabase(ObjectContext.PartsDifferenceBackBills, (PartsDifferenceBackBill)entity);
                    break;
                case "PartsDifferenceBackBillDtl":
                    UpdateToDatabase(ObjectContext.PartsDifferenceBackBillDtls, (PartsDifferenceBackBillDtl)entity);
                    break;
                case "PartsExchange":
                    UpdateToDatabase(ObjectContext.PartsExchanges, (PartsExchange)entity);
                    break;
                case "PartsExchangeGroup":
                    UpdateToDatabase(ObjectContext.PartsExchangeGroups, (PartsExchangeGroup)entity);
                    break;
                case "PartsExchangeHistory":
                    UpdateToDatabase(ObjectContext.PartsExchangeHistories, (PartsExchangeHistory)entity);
                    break;
                case "PartsHistoryStock":
                    UpdateToDatabase(ObjectContext.PartsHistoryStocks, (PartsHistoryStock)entity);
                    break;
                case "PartsInboundCheckBill":
                    UpdateToDatabase(ObjectContext.PartsInboundCheckBills, (PartsInboundCheckBill)entity);
                    break;
                case "PartsInboundCheckBill_Sync":
                    UpdateToDatabase(ObjectContext.PartsInboundCheckBill_Sync, (PartsInboundCheckBill_Sync)entity);
                    break;
                case "PartsInboundCheckBillDetail":
                    UpdateToDatabase(ObjectContext.PartsInboundCheckBillDetails, (PartsInboundCheckBillDetail)entity);
                    break;
                case "PartsInboundPackingDetail":
                    UpdateToDatabase(ObjectContext.PartsInboundPackingDetails, (PartsInboundPackingDetail)entity);
                    break;
                case "PartsInboundPerformance":
                    UpdateToDatabase(ObjectContext.PartsInboundPerformances, (PartsInboundPerformance)entity);
                    break;
                case "PartsInboundPlan":
                    UpdateToDatabase(ObjectContext.PartsInboundPlans, (PartsInboundPlan)entity);
                    break;
                case "PartsInboundPlanDetail":
                    UpdateToDatabase(ObjectContext.PartsInboundPlanDetails, (PartsInboundPlanDetail)entity);
                    break;
                case "PartsInventoryBill":
                    UpdateToDatabase(ObjectContext.PartsInventoryBills, (PartsInventoryBill)entity);
                    break;
                case "PartsInventoryDetail":
                    UpdateToDatabase(ObjectContext.PartsInventoryDetails, (PartsInventoryDetail)entity);
                    break;
                case "PartsLockedStock":
                    UpdateToDatabase(ObjectContext.PartsLockedStocks, (PartsLockedStock)entity);
                    break;
                case "PartsLogisticBatch":
                    UpdateToDatabase(ObjectContext.PartsLogisticBatches, (PartsLogisticBatch)entity);
                    break;
                case "PartsLogisticBatchBillDetail":
                    UpdateToDatabase(ObjectContext.PartsLogisticBatchBillDetails, (PartsLogisticBatchBillDetail)entity);
                    break;
                case "PartsLogisticBatchItemDetail":
                    UpdateToDatabase(ObjectContext.PartsLogisticBatchItemDetails, (PartsLogisticBatchItemDetail)entity);
                    break;
                case "PartsManagementCostGrade":
                    UpdateToDatabase(ObjectContext.PartsManagementCostGrades, (PartsManagementCostGrade)entity);
                    break;
                case "PartsManagementCostRate":
                    UpdateToDatabase(ObjectContext.PartsManagementCostRates, (PartsManagementCostRate)entity);
                    break;
                case "PartsOutboundBill":
                    UpdateToDatabase(ObjectContext.PartsOutboundBills, (PartsOutboundBill)entity);
                    break;
                case "PartsOutboundBillDetail":
                    UpdateToDatabase(ObjectContext.PartsOutboundBillDetails, (PartsOutboundBillDetail)entity);
                    break;
                case "PartsOutboundPerformance":
                    UpdateToDatabase(ObjectContext.PartsOutboundPerformances, (PartsOutboundPerformance)entity);
                    break;
                case "PartsOutboundPlan":
                    UpdateToDatabase(ObjectContext.PartsOutboundPlans, (PartsOutboundPlan)entity);
                    break;
                case "PartsOutboundPlanDetail":
                    UpdateToDatabase(ObjectContext.PartsOutboundPlanDetails, (PartsOutboundPlanDetail)entity);
                    break;
                case "PartsOuterPurchaseChange":
                    UpdateToDatabase(ObjectContext.PartsOuterPurchaseChanges, (PartsOuterPurchaseChange)entity);
                    break;
                case "PartsOuterPurchaselist":
                    UpdateToDatabase(ObjectContext.PartsOuterPurchaselists, (PartsOuterPurchaselist)entity);
                    break;
                case "PartsPacking":
                    UpdateToDatabase(ObjectContext.PartsPackings, (PartsPacking)entity);
                    break;
                case "PartsPackingPropAppDetail":
                    UpdateToDatabase(ObjectContext.PartsPackingPropAppDetails, (PartsPackingPropAppDetail)entity);
                    break;
                case "PartsPackingPropertyApp":
                    UpdateToDatabase(ObjectContext.PartsPackingPropertyApps, (PartsPackingPropertyApp)entity);
                    break;
                case "PartsPlannedPrice":
                    UpdateToDatabase(ObjectContext.PartsPlannedPrices, (PartsPlannedPrice)entity);
                    break;
                case "PartsPriceIncreaseRate":
                    UpdateToDatabase(ObjectContext.PartsPriceIncreaseRates, (PartsPriceIncreaseRate)entity);
                    break;
                case "PartsPriceTimeLimit":
                    UpdateToDatabase(ObjectContext.PartsPriceTimeLimits, (PartsPriceTimeLimit)entity);
                    break;
                case "PartsPriorityBill":
                    UpdateToDatabase(ObjectContext.PartsPriorityBills, (PartsPriorityBill)entity);
                    break;
                case "PartsPurchaseInfoSummary":
                    UpdateToDatabase(ObjectContext.PartsPurchaseInfoSummaries, (PartsPurchaseInfoSummary)entity);
                    break;
                case "PartsPurchaseOrder":
                    UpdateToDatabase(ObjectContext.PartsPurchaseOrders, (PartsPurchaseOrder)entity);
                    break;
                case "PartsPurchaseOrder_Sync":
                    UpdateToDatabase(ObjectContext.PartsPurchaseOrder_Sync, (PartsPurchaseOrder_Sync)entity);
                    break;
                case "PartsPurchaseOrderDetail":
                    UpdateToDatabase(ObjectContext.PartsPurchaseOrderDetails, (PartsPurchaseOrderDetail)entity);
                    break;
                case "PartsPurchaseOrderType":
                    UpdateToDatabase(ObjectContext.PartsPurchaseOrderTypes, (PartsPurchaseOrderType)entity);
                    break;
                case "PartsPurchasePlan":
                    UpdateToDatabase(ObjectContext.PartsPurchasePlans, (PartsPurchasePlan)entity);
                    break;
                case "PartsPurchasePlan_HW":
                    UpdateToDatabase(ObjectContext.PartsPurchasePlan_HW, (PartsPurchasePlan_HW)entity);
                    break;
                case "PartsPurchasePlanDetail":
                    UpdateToDatabase(ObjectContext.PartsPurchasePlanDetails, (PartsPurchasePlanDetail)entity);
                    break;
                case "PartsPurchasePlanDetail_HW":
                    UpdateToDatabase(ObjectContext.PartsPurchasePlanDetail_HW, (PartsPurchasePlanDetail_HW)entity);
                    break;
                case "PartsPurchasePlanTemp":
                    UpdateToDatabase(ObjectContext.PartsPurchasePlanTemps, (PartsPurchasePlanTemp)entity);
                    break;
                case "PartsPurchasePricing":
                    UpdateToDatabase(ObjectContext.PartsPurchasePricings, (PartsPurchasePricing)entity);
                    break;
                case "PartsPurchasePricingChange":
                    UpdateToDatabase(ObjectContext.PartsPurchasePricingChanges, (PartsPurchasePricingChange)entity);
                    break;
                case "PartsPurchasePricingDetail":
                    UpdateToDatabase(ObjectContext.PartsPurchasePricingDetails, (PartsPurchasePricingDetail)entity);
                    break;
                case "PartsPurchaseRtnSettleBill":
                    UpdateToDatabase(ObjectContext.PartsPurchaseRtnSettleBills, (PartsPurchaseRtnSettleBill)entity);
                    break;
                case "PartsPurchaseRtnSettleDetail":
                    UpdateToDatabase(ObjectContext.PartsPurchaseRtnSettleDetails, (PartsPurchaseRtnSettleDetail)entity);
                    break;
                case "PartsPurchaseRtnSettleRef":
                    UpdateToDatabase(ObjectContext.PartsPurchaseRtnSettleRefs, (PartsPurchaseRtnSettleRef)entity);
                    break;
                case "PartsPurchaseSettleBill":
                    UpdateToDatabase(ObjectContext.PartsPurchaseSettleBills, (PartsPurchaseSettleBill)entity);
                    break;
                case "PartsPurchaseSettleDetail":
                    UpdateToDatabase(ObjectContext.PartsPurchaseSettleDetails, (PartsPurchaseSettleDetail)entity);
                    break;
                case "PartsPurchaseSettleRef":
                    UpdateToDatabase(ObjectContext.PartsPurchaseSettleRefs, (PartsPurchaseSettleRef)entity);
                    break;
                case "PartsPurReturnOrder":
                    UpdateToDatabase(ObjectContext.PartsPurReturnOrders, (PartsPurReturnOrder)entity);
                    break;
                case "PartsPurReturnOrderDetail":
                    UpdateToDatabase(ObjectContext.PartsPurReturnOrderDetails, (PartsPurReturnOrderDetail)entity);
                    break;
                case "PartsRebateAccount":
                    UpdateToDatabase(ObjectContext.PartsRebateAccounts, (PartsRebateAccount)entity);
                    break;
                case "PartsRebateApplication":
                    UpdateToDatabase(ObjectContext.PartsRebateApplications, (PartsRebateApplication)entity);
                    break;
                case "PartsRebateChangeDetail":
                    UpdateToDatabase(ObjectContext.PartsRebateChangeDetails, (PartsRebateChangeDetail)entity);
                    break;
                case "PartsRebateType":
                    UpdateToDatabase(ObjectContext.PartsRebateTypes, (PartsRebateType)entity);
                    break;
                case "PartsReplacement":
                    UpdateToDatabase(ObjectContext.PartsReplacements, (PartsReplacement)entity);
                    break;
                case "PartsReplacementHistory":
                    UpdateToDatabase(ObjectContext.PartsReplacementHistories, (PartsReplacementHistory)entity);
                    break;
                case "PartsRequisitionSettleBill":
                    UpdateToDatabase(ObjectContext.PartsRequisitionSettleBills, (PartsRequisitionSettleBill)entity);
                    break;
                case "PartsRequisitionSettleDetail":
                    UpdateToDatabase(ObjectContext.PartsRequisitionSettleDetails, (PartsRequisitionSettleDetail)entity);
                    break;
                case "PartsRequisitionSettleRef":
                    UpdateToDatabase(ObjectContext.PartsRequisitionSettleRefs, (PartsRequisitionSettleRef)entity);
                    break;
                case "PartsRetailGuidePrice":
                    UpdateToDatabase(ObjectContext.PartsRetailGuidePrices, (PartsRetailGuidePrice)entity);
                    break;
                case "PartsRetailGuidePriceHistory":
                    UpdateToDatabase(ObjectContext.PartsRetailGuidePriceHistories, (PartsRetailGuidePriceHistory)entity);
                    break;
                case "PartsRetailOrder":
                    UpdateToDatabase(ObjectContext.PartsRetailOrders, (PartsRetailOrder)entity);
                    break;
                case "PartsRetailOrderDetail":
                    UpdateToDatabase(ObjectContext.PartsRetailOrderDetails, (PartsRetailOrderDetail)entity);
                    break;
                case "PartsRetailReturnBill":
                    UpdateToDatabase(ObjectContext.PartsRetailReturnBills, (PartsRetailReturnBill)entity);
                    break;
                case "PartsRetailReturnBillDetail":
                    UpdateToDatabase(ObjectContext.PartsRetailReturnBillDetails, (PartsRetailReturnBillDetail)entity);
                    break;
                case "PartsSalePriceIncreaseRate":
                    UpdateToDatabase(ObjectContext.PartsSalePriceIncreaseRates, (PartsSalePriceIncreaseRate)entity);
                    break;
                case "PartsSalesCategory":
                    UpdateToDatabase(ObjectContext.PartsSalesCategories, (PartsSalesCategory)entity);
                    break;
                case "PartsSalesCategoryView":
                    UpdateToDatabase(ObjectContext.PartsSalesCategoryViews, (PartsSalesCategoryView)entity);
                    break;
                case "PartsSalesOrder":
                    UpdateToDatabase(ObjectContext.PartsSalesOrders, (PartsSalesOrder)entity);
                    break;
                case "PartsSalesOrderASAP":
                    UpdateToDatabase(ObjectContext.PartsSalesOrderASAPs, (PartsSalesOrderASAP)entity);
                    break;
                case "PartsSalesOrderDetail":
                    UpdateToDatabase(ObjectContext.PartsSalesOrderDetails, (PartsSalesOrderDetail)entity);
                    break;
                case "PartsSalesOrderDetailASAP":
                    UpdateToDatabase(ObjectContext.PartsSalesOrderDetailASAPs, (PartsSalesOrderDetailASAP)entity);
                    break;
                case "PartsSalesOrderProcess":
                    UpdateToDatabase(ObjectContext.PartsSalesOrderProcesses, (PartsSalesOrderProcess)entity);
                    break;
                case "PartsSalesOrderProcessDetail":
                    UpdateToDatabase(ObjectContext.PartsSalesOrderProcessDetails, (PartsSalesOrderProcessDetail)entity);
                    break;
                case "PartsSalesOrderType":
                    UpdateToDatabase(ObjectContext.PartsSalesOrderTypes, (PartsSalesOrderType)entity);
                    break;
                case "PartsSalesPrice":
                    UpdateToDatabase(ObjectContext.PartsSalesPrices, (PartsSalesPrice)entity);
                    break;
                case "PartsSalesPriceChange":
                    UpdateToDatabase(ObjectContext.PartsSalesPriceChanges, (PartsSalesPriceChange)entity);
                    break;
                case "PartsSalesPriceChangeDetail":
                    UpdateToDatabase(ObjectContext.PartsSalesPriceChangeDetails, (PartsSalesPriceChangeDetail)entity);
                    break;
                case "PartsSalesPriceHistory":
                    UpdateToDatabase(ObjectContext.PartsSalesPriceHistories, (PartsSalesPriceHistory)entity);
                    break;
                case "PartsSalesReturnBill":
                    UpdateToDatabase(ObjectContext.PartsSalesReturnBills, (PartsSalesReturnBill)entity);
                    break;
                case "PartsSalesReturnBillDetail":
                    UpdateToDatabase(ObjectContext.PartsSalesReturnBillDetails, (PartsSalesReturnBillDetail)entity);
                    break;
                case "PartsSalesRtnSettlement":
                    UpdateToDatabase(ObjectContext.PartsSalesRtnSettlements, (PartsSalesRtnSettlement)entity);
                    break;
                case "PartsSalesRtnSettlementDetail":
                    UpdateToDatabase(ObjectContext.PartsSalesRtnSettlementDetails, (PartsSalesRtnSettlementDetail)entity);
                    break;
                case "PartsSalesRtnSettlementRef":
                    UpdateToDatabase(ObjectContext.PartsSalesRtnSettlementRefs, (PartsSalesRtnSettlementRef)entity);
                    break;
                case "PartsSalesSettlement":
                    UpdateToDatabase(ObjectContext.PartsSalesSettlements, (PartsSalesSettlement)entity);
                    break;
                case "PartsSalesSettlementDetail":
                    UpdateToDatabase(ObjectContext.PartsSalesSettlementDetails, (PartsSalesSettlementDetail)entity);
                    break;
                case "PartsSalesSettlementRef":
                    UpdateToDatabase(ObjectContext.PartsSalesSettlementRefs, (PartsSalesSettlementRef)entity);
                    break;
                case "PartsSalesWeeklyBase":
                    UpdateToDatabase(ObjectContext.PartsSalesWeeklyBases, (PartsSalesWeeklyBase)entity);
                    break;
                case "PartsServiceLevel":
                    UpdateToDatabase(ObjectContext.PartsServiceLevels, (PartsServiceLevel)entity);
                    break;
                case "PartsShelvesTask":
                    UpdateToDatabase(ObjectContext.PartsShelvesTasks, (PartsShelvesTask)entity);
                    break;
                case "PartsShiftOrder":
                    UpdateToDatabase(ObjectContext.PartsShiftOrders, (PartsShiftOrder)entity);
                    break;
                case "PartsShiftOrderDetail":
                    UpdateToDatabase(ObjectContext.PartsShiftOrderDetails, (PartsShiftOrderDetail)entity);
                    break;
                case "PartsShiftSIHDetail":
                    UpdateToDatabase(ObjectContext.PartsShiftSIHDetails, (PartsShiftSIHDetail)entity);
                    break;
                case "PartsShippingOrder":
                    UpdateToDatabase(ObjectContext.PartsShippingOrders, (PartsShippingOrder)entity);
                    break;
                case "PartsShippingOrderDetail":
                    UpdateToDatabase(ObjectContext.PartsShippingOrderDetails, (PartsShippingOrderDetail)entity);
                    break;
                case "PartsShippingOrderRef":
                    UpdateToDatabase(ObjectContext.PartsShippingOrderRefs, (PartsShippingOrderRef)entity);
                    break;
                case "PartsSpecialPriceHistory":
                    UpdateToDatabase(ObjectContext.PartsSpecialPriceHistories, (PartsSpecialPriceHistory)entity);
                    break;
                case "PartsSpecialTreatyPrice":
                    UpdateToDatabase(ObjectContext.PartsSpecialTreatyPrices, (PartsSpecialTreatyPrice)entity);
                    break;
                case "PartsStandardStockBill":
                    UpdateToDatabase(ObjectContext.PartsStandardStockBills, (PartsStandardStockBill)entity);
                    break;
                case "PartsStock":
                    UpdateToDatabase(ObjectContext.PartsStocks, (PartsStock)entity);
                    break;
                case "PartsStockBatchDetail":
                    UpdateToDatabase(ObjectContext.PartsStockBatchDetails, (PartsStockBatchDetail)entity);
                    break;
                case "PartsStockCoefficient":
                    UpdateToDatabase(ObjectContext.PartsStockCoefficients, (PartsStockCoefficient)entity);
                    break;
                case "PartsStockCoefficientPrice":
                    UpdateToDatabase(ObjectContext.PartsStockCoefficientPrices, (PartsStockCoefficientPrice)entity);
                    break;
                case "PartsStockHistory":
                    UpdateToDatabase(ObjectContext.PartsStockHistories, (PartsStockHistory)entity);
                    break;
                case "PartsSupplier":
                    UpdateToDatabase(ObjectContext.PartsSuppliers, (PartsSupplier)entity);
                    break;
                case "PartsSupplierCollection":
                    UpdateToDatabase(ObjectContext.PartsSupplierCollections, (PartsSupplierCollection)entity);
                    break;
                case "PartsSupplierRelation":
                    UpdateToDatabase(ObjectContext.PartsSupplierRelations, (PartsSupplierRelation)entity);
                    break;
                case "PartsSupplierRelationHistory":
                    UpdateToDatabase(ObjectContext.PartsSupplierRelationHistories, (PartsSupplierRelationHistory)entity);
                    break;
                case "PartsSupplierStock":
                    UpdateToDatabase(ObjectContext.PartsSupplierStocks, (PartsSupplierStock)entity);
                    break;
                case "PartsSupplierStockHistory":
                    UpdateToDatabase(ObjectContext.PartsSupplierStockHistories, (PartsSupplierStockHistory)entity);
                    break;
                case "PartsTransferOrder":
                    UpdateToDatabase(ObjectContext.PartsTransferOrders, (PartsTransferOrder)entity);
                    break;
                case "PartsTransferOrderDetail":
                    UpdateToDatabase(ObjectContext.PartsTransferOrderDetails, (PartsTransferOrderDetail)entity);
                    break;
                case "PartsWarehousePendingCost":
                    UpdateToDatabase(ObjectContext.PartsWarehousePendingCosts, (PartsWarehousePendingCost)entity);
                    break;
                case "PaymentBeneficiaryList":
                    UpdateToDatabase(ObjectContext.PaymentBeneficiaryLists, (PaymentBeneficiaryList)entity);
                    break;
                case "PaymentBill":
                    UpdateToDatabase(ObjectContext.PaymentBills, (PaymentBill)entity);
                    break;
                case "PaymentBill_Sync":
                    UpdateToDatabase(ObjectContext.PaymentBill_Sync, (PaymentBill_Sync)entity);
                    break;
                case "PayOutBill":
                    UpdateToDatabase(ObjectContext.PayOutBills, (PayOutBill)entity);
                    break;
                case "Personnel":
                    UpdateToDatabase(ObjectContext.Personnels, (Personnel)entity);
                    break;
                case "PersonnelSupplierRelation":
                    UpdateToDatabase(ObjectContext.PersonnelSupplierRelations, (PersonnelSupplierRelation)entity);
                    break;
                case "PersonSalesCenterLink":
                    UpdateToDatabase(ObjectContext.PersonSalesCenterLinks, (PersonSalesCenterLink)entity);
                    break;
                case "PersonSubDealer":
                    UpdateToDatabase(ObjectContext.PersonSubDealers, (PersonSubDealer)entity);
                    break;
                case "PickingTask":
                    UpdateToDatabase(ObjectContext.PickingTasks, (PickingTask)entity);
                    break;
                case "PickingTaskBatchDetail":
                    UpdateToDatabase(ObjectContext.PickingTaskBatchDetails, (PickingTaskBatchDetail)entity);
                    break;
                case "PickingTaskDetail":
                    UpdateToDatabase(ObjectContext.PickingTaskDetails, (PickingTaskDetail)entity);
                    break;
                case "PlannedPriceApp":
                    UpdateToDatabase(ObjectContext.PlannedPriceApps, (PlannedPriceApp)entity);
                    break;
                case "PlannedPriceAppDetail":
                    UpdateToDatabase(ObjectContext.PlannedPriceAppDetails, (PlannedPriceAppDetail)entity);
                    break;
                case "PlanPriceCategory":
                    UpdateToDatabase(ObjectContext.PlanPriceCategories, (PlanPriceCategory)entity);
                    break;
                case "PostSaleClaims_Tmp":
                    UpdateToDatabase(ObjectContext.PostSaleClaims_Tmp, (PostSaleClaims_Tmp)entity);
                    break;
                case "PQIReport":
                    UpdateToDatabase(ObjectContext.PQIReports, (PQIReport)entity);
                    break;
                case "PreInspectionOrder":
                    UpdateToDatabase(ObjectContext.PreInspectionOrders, (PreInspectionOrder)entity);
                    break;
                case "PreOrder":
                    UpdateToDatabase(ObjectContext.PreOrders, (PreOrder)entity);
                    break;
                case "PreSaleCheckOrder":
                    UpdateToDatabase(ObjectContext.PreSaleCheckOrders, (PreSaleCheckOrder)entity);
                    break;
                case "PreSaleItem":
                    UpdateToDatabase(ObjectContext.PreSaleItems, (PreSaleItem)entity);
                    break;
                case "PreSalesCheckDetail":
                    UpdateToDatabase(ObjectContext.PreSalesCheckDetails, (PreSalesCheckDetail)entity);
                    break;
                case "PreSalesCheckFeeStandard":
                    UpdateToDatabase(ObjectContext.PreSalesCheckFeeStandards, (PreSalesCheckFeeStandard)entity);
                    break;
                case "ProductCategoryView":
                    UpdateToDatabase(ObjectContext.ProductCategoryViews, (ProductCategoryView)entity);
                    break;
                case "ProductView":
                    UpdateToDatabase(ObjectContext.ProductViews, (ProductView)entity);
                    break;
                case "PurchaseNoSettleTable":
                    UpdateToDatabase(ObjectContext.PurchaseNoSettleTables, (PurchaseNoSettleTable)entity);
                    break;
                case "PurchaseOrderFinishedDetail":
                    UpdateToDatabase(ObjectContext.PurchaseOrderFinishedDetails, (PurchaseOrderFinishedDetail)entity);
                    break;
                case "PurchaseOrderUnFinishDetail":
                    UpdateToDatabase(ObjectContext.PurchaseOrderUnFinishDetails, (PurchaseOrderUnFinishDetail)entity);
                    break;
                case "PurchasePersonSupplierLink":
                    UpdateToDatabase(ObjectContext.PurchasePersonSupplierLinks, (PurchasePersonSupplierLink)entity);
                    break;
                case "PurchasePricingChangeHi":
                    UpdateToDatabase(ObjectContext.PurchasePricingChangeHis, (PurchasePricingChangeHi)entity);
                    break;
                case "PurchaseRtnSettleInvoiceRel":
                    UpdateToDatabase(ObjectContext.PurchaseRtnSettleInvoiceRels, (PurchaseRtnSettleInvoiceRel)entity);
                    break;
                case "PurchaseSettleInvoiceRel":
                    UpdateToDatabase(ObjectContext.PurchaseSettleInvoiceRels, (PurchaseSettleInvoiceRel)entity);
                    break;
                case "PurchaseSettleInvoiceResource":
                    UpdateToDatabase(ObjectContext.PurchaseSettleInvoiceResources, (PurchaseSettleInvoiceResource)entity);
                    break;
                case "PurDistributionDealerLog":
                    UpdateToDatabase(ObjectContext.PurDistributionDealerLogs, (PurDistributionDealerLog)entity);
                    break;
                case "QTSDataQuery":
                    UpdateToDatabase(ObjectContext.QTSDataQueries, (QTSDataQuery)entity);
                    break;
                case "QTSDataQueryForDGN":
                    UpdateToDatabase(ObjectContext.QTSDataQueryForDGNs, (QTSDataQueryForDGN)entity);
                    break;
                case "QTSDataQueryForFD":
                    UpdateToDatabase(ObjectContext.QTSDataQueryForFDs, (QTSDataQueryForFD)entity);
                    break;
                case "QTSDataQueryForNFGC":
                    UpdateToDatabase(ObjectContext.QTSDataQueryForNFGCs, (QTSDataQueryForNFGC)entity);
                    break;
                case "QTSDataQueryForQXC":
                    UpdateToDatabase(ObjectContext.QTSDataQueryForQXCs, (QTSDataQueryForQXC)entity);
                    break;
                case "QTSDataQueryForSD":
                    UpdateToDatabase(ObjectContext.QTSDataQueryForSDs, (QTSDataQueryForSD)entity);
                    break;
                case "QTSDataQueryForSP":
                    UpdateToDatabase(ObjectContext.QTSDataQueryForSPs, (QTSDataQueryForSP)entity);
                    break;
                case "Region":
                    UpdateToDatabase(ObjectContext.Regions, (Region)entity);
                    break;
                case "RegionMarketDptRelation":
                    UpdateToDatabase(ObjectContext.RegionMarketDptRelations, (RegionMarketDptRelation)entity);
                    break;
                case "RegionPersonnelRelation":
                    UpdateToDatabase(ObjectContext.RegionPersonnelRelations, (RegionPersonnelRelation)entity);
                    break;
                case "RepairClaimBillQuery":
                    UpdateToDatabase(ObjectContext.RepairClaimBillQueries, (RepairClaimBillQuery)entity);
                    break;
                case "RepairClaimBillWithSupplier":
                    UpdateToDatabase(ObjectContext.RepairClaimBillWithSuppliers, (RepairClaimBillWithSupplier)entity);
                    break;
                case "RepairItemLaborHourView":
                    UpdateToDatabase(ObjectContext.RepairItemLaborHourViews, (RepairItemLaborHourView)entity);
                    break;
                case "RepairItemView":
                    UpdateToDatabase(ObjectContext.RepairItemViews, (RepairItemView)entity);
                    break;
                case "RepairOrderQuery":
                    UpdateToDatabase(ObjectContext.RepairOrderQueries, (RepairOrderQuery)entity);
                    break;
                case "ReportDownloadMsg":
                    UpdateToDatabase(ObjectContext.ReportDownloadMsgs, (ReportDownloadMsg)entity);
                    break;
                case "ReserveFactorMasterOrder":
                    UpdateToDatabase(ObjectContext.ReserveFactorMasterOrders, (ReserveFactorMasterOrder)entity);
                    break;
                case "ReserveFactorOrderDetail":
                    UpdateToDatabase(ObjectContext.ReserveFactorOrderDetails, (ReserveFactorOrderDetail)entity);
                    break;
                case "ResponsibleDetail":
                    UpdateToDatabase(ObjectContext.ResponsibleDetails, (ResponsibleDetail)entity);
                    break;
                case "ResponsibleMember":
                    UpdateToDatabase(ObjectContext.ResponsibleMembers, (ResponsibleMember)entity);
                    break;
                case "ResponsibleUnit":
                    UpdateToDatabase(ObjectContext.ResponsibleUnits, (ResponsibleUnit)entity);
                    break;
                case "ResponsibleUnitAffiProduct":
                    UpdateToDatabase(ObjectContext.ResponsibleUnitAffiProducts, (ResponsibleUnitAffiProduct)entity);
                    break;
                case "ResponsibleUnitBranch":
                    UpdateToDatabase(ObjectContext.ResponsibleUnitBranches, (ResponsibleUnitBranch)entity);
                    break;
                case "ResponsibleUnitProductDetail":
                    UpdateToDatabase(ObjectContext.ResponsibleUnitProductDetails, (ResponsibleUnitProductDetail)entity);
                    break;
                case "ResRelationship":
                    UpdateToDatabase(ObjectContext.ResRelationships, (ResRelationship)entity);
                    break;
                case "Retailer_Delivery":
                    UpdateToDatabase(ObjectContext.Retailer_Delivery, (Retailer_Delivery)entity);
                    break;
                case "Retailer_DeliveryDetail":
                    UpdateToDatabase(ObjectContext.Retailer_DeliveryDetail, (Retailer_DeliveryDetail)entity);
                    break;
                case "Retailer_ServiceApply":
                    UpdateToDatabase(ObjectContext.Retailer_ServiceApply, (Retailer_ServiceApply)entity);
                    break;
                case "Retailer_ServiceApplyDetail":
                    UpdateToDatabase(ObjectContext.Retailer_ServiceApplyDetail, (Retailer_ServiceApplyDetail)entity);
                    break;
                case "RetailOrderCustomer":
                    UpdateToDatabase(ObjectContext.RetailOrderCustomers, (RetailOrderCustomer)entity);
                    break;
                case "RetainedCustomer":
                    UpdateToDatabase(ObjectContext.RetainedCustomers, (RetainedCustomer)entity);
                    break;
                case "RetainedCustomerExtended":
                    UpdateToDatabase(ObjectContext.RetainedCustomerExtendeds, (RetainedCustomerExtended)entity);
                    break;
                case "ReturnFreightDisposal":
                    UpdateToDatabase(ObjectContext.ReturnFreightDisposals, (ReturnFreightDisposal)entity);
                    break;
                case "ReturnVisitQuest":
                    UpdateToDatabase(ObjectContext.ReturnVisitQuests, (ReturnVisitQuest)entity);
                    break;
                case "ReturnVisitQuestDetail":
                    UpdateToDatabase(ObjectContext.ReturnVisitQuestDetails, (ReturnVisitQuestDetail)entity);
                    break;
                case "SaleNoSettleTable":
                    UpdateToDatabase(ObjectContext.SaleNoSettleTables, (SaleNoSettleTable)entity);
                    break;
                case "SalesCenterstrategy":
                    UpdateToDatabase(ObjectContext.SalesCenterstrategies, (SalesCenterstrategy)entity);
                    break;
                case "SalesInvoice_tmp":
                    UpdateToDatabase(ObjectContext.SalesInvoice_tmp, (SalesInvoice_tmp)entity);
                    break;
                case "SalesOrderDailyAvg":
                    UpdateToDatabase(ObjectContext.SalesOrderDailyAvgs, (SalesOrderDailyAvg)entity);
                    break;
                case "SalesOrderRecommendForce":
                    UpdateToDatabase(ObjectContext.SalesOrderRecommendForces, (SalesOrderRecommendForce)entity);
                    break;
                case "SalesOrderRecommendWeekly":
                    UpdateToDatabase(ObjectContext.SalesOrderRecommendWeeklies, (SalesOrderRecommendWeekly)entity);
                    break;
                case "SalesRegion":
                    UpdateToDatabase(ObjectContext.SalesRegions, (SalesRegion)entity);
                    break;
                case "SalesRtnSettleInvoiceRel":
                    UpdateToDatabase(ObjectContext.SalesRtnSettleInvoiceRels, (SalesRtnSettleInvoiceRel)entity);
                    break;
                case "SalesSettleInvoiceResource":
                    UpdateToDatabase(ObjectContext.SalesSettleInvoiceResources, (SalesSettleInvoiceResource)entity);
                    break;
                case "SalesUnit":
                    UpdateToDatabase(ObjectContext.SalesUnits, (SalesUnit)entity);
                    break;
                case "SalesUnitAffiPersonnel":
                    UpdateToDatabase(ObjectContext.SalesUnitAffiPersonnels, (SalesUnitAffiPersonnel)entity);
                    break;
                case "SalesUnitAffiWarehouse":
                    UpdateToDatabase(ObjectContext.SalesUnitAffiWarehouses, (SalesUnitAffiWarehouse)entity);
                    break;
                case "SalesUnitAffiWhouseHistory":
                    UpdateToDatabase(ObjectContext.SalesUnitAffiWhouseHistories, (SalesUnitAffiWhouseHistory)entity);
                    break;
                case "SAP_YX_InvoiceverAccountInfo":
                    UpdateToDatabase(ObjectContext.SAP_YX_InvoiceverAccountInfo, (SAP_YX_InvoiceverAccountInfo)entity);
                    break;
                case "SAPInvoiceInfo":
                    UpdateToDatabase(ObjectContext.SAPInvoiceInfoes, (SAPInvoiceInfo)entity);
                    break;
                case "SAPInvoiceInfo_FD":
                    UpdateToDatabase(ObjectContext.SAPInvoiceInfo_FD, (SAPInvoiceInfo_FD)entity);
                    break;
                case "ScheduleExportState":
                    UpdateToDatabase(ObjectContext.ScheduleExportStates, (ScheduleExportState)entity);
                    break;
                case "SecondClassStationPlan":
                    UpdateToDatabase(ObjectContext.SecondClassStationPlans, (SecondClassStationPlan)entity);
                    break;
                case "SecondClassStationPlanDetail":
                    UpdateToDatabase(ObjectContext.SecondClassStationPlanDetails, (SecondClassStationPlanDetail)entity);
                    break;
                case "SendWaybill":
                    UpdateToDatabase(ObjectContext.SendWaybills, (SendWaybill)entity);
                    break;
                case "ServiceDealerRebate":
                    UpdateToDatabase(ObjectContext.ServiceDealerRebates, (ServiceDealerRebate)entity);
                    break;
                case "ServiceLevelBySafeCf":
                    UpdateToDatabase(ObjectContext.ServiceLevelBySafeCfs, (ServiceLevelBySafeCf)entity);
                    break;
                case "ServiceProductLineView":
                    UpdateToDatabase(ObjectContext.ServiceProductLineViews, (ServiceProductLineView)entity);
                    break;
                case "ServiceProductLineViewAllDB":
                    UpdateToDatabase(ObjectContext.ServiceProductLineViewAllDBs, (ServiceProductLineViewAllDB)entity);
                    break;
                case "ServProdLineAffiProduct":
                    UpdateToDatabase(ObjectContext.ServProdLineAffiProducts, (ServProdLineAffiProduct)entity);
                    break;
                case "SettleBillInvoiceLink":
                    UpdateToDatabase(ObjectContext.SettleBillInvoiceLinks, (SettleBillInvoiceLink)entity);
                    break;
                case "SettlementAutomaticTaskSet":
                    UpdateToDatabase(ObjectContext.SettlementAutomaticTaskSets, (SettlementAutomaticTaskSet)entity);
                    break;
                case "SIHCenterPer":
                    UpdateToDatabase(ObjectContext.SIHCenterPers, (SIHCenterPer)entity);
                    break;
                case "SIHCreditInfo":
                    UpdateToDatabase(ObjectContext.SIHCreditInfoes, (SIHCreditInfo)entity);
                    break;
                case "SIHCreditInfoDetail":
                    UpdateToDatabase(ObjectContext.SIHCreditInfoDetails, (SIHCreditInfoDetail)entity);
                    break;
                case "SIHDailySalesAverage":
                    UpdateToDatabase(ObjectContext.SIHDailySalesAverages, (SIHDailySalesAverage)entity);
                    break;
                case "SIHRecommendPlan":
                    UpdateToDatabase(ObjectContext.SIHRecommendPlans, (SIHRecommendPlan)entity);
                    break;
                case "SIHSmartOrderBase":
                    UpdateToDatabase(ObjectContext.SIHSmartOrderBases, (SIHSmartOrderBase)entity);
                    break;
                case "SmartCompany":
                    UpdateToDatabase(ObjectContext.SmartCompanies, (SmartCompany)entity);
                    break;
                case "SmartOrderCalendar":
                    UpdateToDatabase(ObjectContext.SmartOrderCalendars, (SmartOrderCalendar)entity);
                    break;
                case "SmartProcessMethod":
                    UpdateToDatabase(ObjectContext.SmartProcessMethods, (SmartProcessMethod)entity);
                    break;
                case "SparePart":
                    UpdateToDatabase(ObjectContext.SpareParts, (SparePart)entity);
                    break;
                case "SparePartHistory":
                    UpdateToDatabase(ObjectContext.SparePartHistories, (SparePartHistory)entity);
                    break;
                case "SpecialPriceChangeList":
                    UpdateToDatabase(ObjectContext.SpecialPriceChangeLists, (SpecialPriceChangeList)entity);
                    break;
                case "SpecialTreatyPriceChange":
                    UpdateToDatabase(ObjectContext.SpecialTreatyPriceChanges, (SpecialTreatyPriceChange)entity);
                    break;
                case "SsUsedPartsDisposalBill":
                    UpdateToDatabase(ObjectContext.SsUsedPartsDisposalBills, (SsUsedPartsDisposalBill)entity);
                    break;
                case "SsUsedPartsDisposalDetail":
                    UpdateToDatabase(ObjectContext.SsUsedPartsDisposalDetails, (SsUsedPartsDisposalDetail)entity);
                    break;
                case "SsUsedPartsStorage":
                    UpdateToDatabase(ObjectContext.SsUsedPartsStorages, (SsUsedPartsStorage)entity);
                    break;
                case "SubChannelRelation":
                    UpdateToDatabase(ObjectContext.SubChannelRelations, (SubChannelRelation)entity);
                    break;
                case "SubDealer":
                    UpdateToDatabase(ObjectContext.SubDealers, (SubDealer)entity);
                    break;
                case "Summary":
                    UpdateToDatabase(ObjectContext.Summaries, (Summary)entity);
                    break;
                case "SupplierAccount":
                    UpdateToDatabase(ObjectContext.SupplierAccounts, (SupplierAccount)entity);
                    break;
                case "SupplierCollectionDetail":
                    UpdateToDatabase(ObjectContext.SupplierCollectionDetails, (SupplierCollectionDetail)entity);
                    break;
                case "SupplierExpenseAdjustBill":
                    UpdateToDatabase(ObjectContext.SupplierExpenseAdjustBills, (SupplierExpenseAdjustBill)entity);
                    break;
                case "SupplierInformation":
                    UpdateToDatabase(ObjectContext.SupplierInformations, (SupplierInformation)entity);
                    break;
                case "SupplierOpenAccountApp":
                    UpdateToDatabase(ObjectContext.SupplierOpenAccountApps, (SupplierOpenAccountApp)entity);
                    break;
                case "SupplierPlanArrear":
                    UpdateToDatabase(ObjectContext.SupplierPlanArrears, (SupplierPlanArrear)entity);
                    break;
                case "SupplierPreAppLoanDetail":
                    UpdateToDatabase(ObjectContext.SupplierPreAppLoanDetails, (SupplierPreAppLoanDetail)entity);
                    break;
                case "SupplierPreApprovedLoan":
                    UpdateToDatabase(ObjectContext.SupplierPreApprovedLoans, (SupplierPreApprovedLoan)entity);
                    break;
                case "SupplierShippingDetail":
                    UpdateToDatabase(ObjectContext.SupplierShippingDetails, (SupplierShippingDetail)entity);
                    break;
                case "SupplierShippingOrder":
                    UpdateToDatabase(ObjectContext.SupplierShippingOrders, (SupplierShippingOrder)entity);
                    break;
                case "SupplierSpareOutBoud":
                    UpdateToDatabase(ObjectContext.SupplierSpareOutBouds, (SupplierSpareOutBoud)entity);
                    break;
                case "SupplierStoreInboundDetail":
                    UpdateToDatabase(ObjectContext.SupplierStoreInboundDetails, (SupplierStoreInboundDetail)entity);
                    break;
                case "SupplierStorePlanDetail":
                    UpdateToDatabase(ObjectContext.SupplierStorePlanDetails, (SupplierStorePlanDetail)entity);
                    break;
                case "SupplierStorePlanOrder":
                    UpdateToDatabase(ObjectContext.SupplierStorePlanOrders, (SupplierStorePlanOrder)entity);
                    break;
                case "SupplierTraceCode":
                    UpdateToDatabase(ObjectContext.SupplierTraceCodes, (SupplierTraceCode)entity);
                    break;
                case "SupplierTraceCodeDetail":
                    UpdateToDatabase(ObjectContext.SupplierTraceCodeDetails, (SupplierTraceCodeDetail)entity);
                    break;
                case "SupplierTraceDetail":
                    UpdateToDatabase(ObjectContext.SupplierTraceDetails, (SupplierTraceDetail)entity);
                    break;
                case "SupplierTransferBill":
                    UpdateToDatabase(ObjectContext.SupplierTransferBills, (SupplierTransferBill)entity);
                    break;
                case "TemPurchaseOrder":
                    UpdateToDatabase(ObjectContext.TemPurchaseOrders, (TemPurchaseOrder)entity);
                    break;
                case "TemPurchaseOrderDetail":
                    UpdateToDatabase(ObjectContext.TemPurchaseOrderDetails, (TemPurchaseOrderDetail)entity);
                    break;
                case "TemPurchasePlanOrder":
                    UpdateToDatabase(ObjectContext.TemPurchasePlanOrders, (TemPurchasePlanOrder)entity);
                    break;
                case "TemPurchasePlanOrderDetail":
                    UpdateToDatabase(ObjectContext.TemPurchasePlanOrderDetails, (TemPurchasePlanOrderDetail)entity);
                    break;
                case "TemShippingOrderDetail":
                    UpdateToDatabase(ObjectContext.TemShippingOrderDetails, (TemShippingOrderDetail)entity);
                    break;
                case "TemSupplierShippingOrder":
                    UpdateToDatabase(ObjectContext.TemSupplierShippingOrders, (TemSupplierShippingOrder)entity);
                    break;
                case "TiledRegion":
                    UpdateToDatabase(ObjectContext.TiledRegions, (TiledRegion)entity);
                    break;
                case "TraceTempType":
                    UpdateToDatabase(ObjectContext.TraceTempTypes, (TraceTempType)entity);
                    break;
                case "TransactionInterfaceList":
                    UpdateToDatabase(ObjectContext.TransactionInterfaceLists, (TransactionInterfaceList)entity);
                    break;
                case "TransactionInterfaceLog":
                    UpdateToDatabase(ObjectContext.TransactionInterfaceLogs, (TransactionInterfaceLog)entity);
                    break;
                case "UsedPartsDisposalBill":
                    UpdateToDatabase(ObjectContext.UsedPartsDisposalBills, (UsedPartsDisposalBill)entity);
                    break;
                case "UsedPartsDisposalDetail":
                    UpdateToDatabase(ObjectContext.UsedPartsDisposalDetails, (UsedPartsDisposalDetail)entity);
                    break;
                case "UsedPartsDistanceInfor":
                    UpdateToDatabase(ObjectContext.UsedPartsDistanceInfors, (UsedPartsDistanceInfor)entity);
                    break;
                case "UsedPartsInboundDetail":
                    UpdateToDatabase(ObjectContext.UsedPartsInboundDetails, (UsedPartsInboundDetail)entity);
                    break;
                case "UsedPartsInboundOrder":
                    UpdateToDatabase(ObjectContext.UsedPartsInboundOrders, (UsedPartsInboundOrder)entity);
                    break;
                case "UsedPartsLoanBill":
                    UpdateToDatabase(ObjectContext.UsedPartsLoanBills, (UsedPartsLoanBill)entity);
                    break;
                case "UsedPartsLoanDetail":
                    UpdateToDatabase(ObjectContext.UsedPartsLoanDetails, (UsedPartsLoanDetail)entity);
                    break;
                case "UsedPartsLogisticLossBill":
                    UpdateToDatabase(ObjectContext.UsedPartsLogisticLossBills, (UsedPartsLogisticLossBill)entity);
                    break;
                case "UsedPartsLogisticLossDetail":
                    UpdateToDatabase(ObjectContext.UsedPartsLogisticLossDetails, (UsedPartsLogisticLossDetail)entity);
                    break;
                case "UsedPartsOutboundDetail":
                    UpdateToDatabase(ObjectContext.UsedPartsOutboundDetails, (UsedPartsOutboundDetail)entity);
                    break;
                case "UsedPartsOutboundOrder":
                    UpdateToDatabase(ObjectContext.UsedPartsOutboundOrders, (UsedPartsOutboundOrder)entity);
                    break;
                case "UsedPartsRefitBill":
                    UpdateToDatabase(ObjectContext.UsedPartsRefitBills, (UsedPartsRefitBill)entity);
                    break;
                case "UsedPartsRefitReqDetail":
                    UpdateToDatabase(ObjectContext.UsedPartsRefitReqDetails, (UsedPartsRefitReqDetail)entity);
                    break;
                case "UsedPartsRefitReturnDetail":
                    UpdateToDatabase(ObjectContext.UsedPartsRefitReturnDetails, (UsedPartsRefitReturnDetail)entity);
                    break;
                case "UsedPartsReturnDetail":
                    UpdateToDatabase(ObjectContext.UsedPartsReturnDetails, (UsedPartsReturnDetail)entity);
                    break;
                case "UsedPartsReturnOrder":
                    UpdateToDatabase(ObjectContext.UsedPartsReturnOrders, (UsedPartsReturnOrder)entity);
                    break;
                case "UsedPartsReturnPolicyHistory":
                    UpdateToDatabase(ObjectContext.UsedPartsReturnPolicyHistories, (UsedPartsReturnPolicyHistory)entity);
                    break;
                case "UsedPartsShiftDetail":
                    UpdateToDatabase(ObjectContext.UsedPartsShiftDetails, (UsedPartsShiftDetail)entity);
                    break;
                case "UsedPartsShiftOrder":
                    UpdateToDatabase(ObjectContext.UsedPartsShiftOrders, (UsedPartsShiftOrder)entity);
                    break;
                case "UsedPartsShippingDetail":
                    UpdateToDatabase(ObjectContext.UsedPartsShippingDetails, (UsedPartsShippingDetail)entity);
                    break;
                case "UsedPartsShippingOrder":
                    UpdateToDatabase(ObjectContext.UsedPartsShippingOrders, (UsedPartsShippingOrder)entity);
                    break;
                case "UsedPartsStock":
                    UpdateToDatabase(ObjectContext.UsedPartsStocks, (UsedPartsStock)entity);
                    break;
                case "UsedPartsStockView":
                    UpdateToDatabase(ObjectContext.UsedPartsStockViews, (UsedPartsStockView)entity);
                    break;
                case "UsedPartsTransferDetail":
                    UpdateToDatabase(ObjectContext.UsedPartsTransferDetails, (UsedPartsTransferDetail)entity);
                    break;
                case "UsedPartsTransferOrder":
                    UpdateToDatabase(ObjectContext.UsedPartsTransferOrders, (UsedPartsTransferOrder)entity);
                    break;
                case "UsedPartsWarehouse":
                    UpdateToDatabase(ObjectContext.UsedPartsWarehouses, (UsedPartsWarehouse)entity);
                    break;
                case "UsedPartsWarehouseArea":
                    UpdateToDatabase(ObjectContext.UsedPartsWarehouseAreas, (UsedPartsWarehouseArea)entity);
                    break;
                case "UsedPartsWarehouseManager":
                    UpdateToDatabase(ObjectContext.UsedPartsWarehouseManagers, (UsedPartsWarehouseManager)entity);
                    break;
                case "UsedPartsWarehouseStaff":
                    UpdateToDatabase(ObjectContext.UsedPartsWarehouseStaffs, (UsedPartsWarehouseStaff)entity);
                    break;
                case "VehicleCategoryAffiProduct":
                    UpdateToDatabase(ObjectContext.VehicleCategoryAffiProducts, (VehicleCategoryAffiProduct)entity);
                    break;
                case "VehicleModelAffiProduct":
                    UpdateToDatabase(ObjectContext.VehicleModelAffiProducts, (VehicleModelAffiProduct)entity);
                    break;
                case "VeriCodeEffectTime":
                    UpdateToDatabase(ObjectContext.VeriCodeEffectTimes, (VeriCodeEffectTime)entity);
                    break;
                case "VersionInfo":
                    UpdateToDatabase(ObjectContext.VersionInfoes, (VersionInfo)entity);
                    break;
                case "View_ErrorTable_SAP_YX":
                    UpdateToDatabase(ObjectContext.View_ErrorTable_SAP_YX, (View_ErrorTable_SAP_YX)entity);
                    break;
                case "ViewSYNCWMSINOUTLOGINFO":
                    UpdateToDatabase(ObjectContext.ViewSYNCWMSINOUTLOGINFOes, (ViewSYNCWMSINOUTLOGINFO)entity);
                    break;
                case "Warehouse":
                    UpdateToDatabase(ObjectContext.Warehouses, (Warehouse)entity);
                    break;
                case "WarehouseArea":
                    UpdateToDatabase(ObjectContext.WarehouseAreas, (WarehouseArea)entity);
                    break;
                case "WarehouseAreaCategory":
                    UpdateToDatabase(ObjectContext.WarehouseAreaCategories, (WarehouseAreaCategory)entity);
                    break;
                case "WarehouseAreaHistory":
                    UpdateToDatabase(ObjectContext.WarehouseAreaHistories, (WarehouseAreaHistory)entity);
                    break;
                case "WarehouseAreaManager":
                    UpdateToDatabase(ObjectContext.WarehouseAreaManagers, (WarehouseAreaManager)entity);
                    break;
                case "WarehouseAreaWithPartsStock":
                    UpdateToDatabase(ObjectContext.WarehouseAreaWithPartsStocks, (WarehouseAreaWithPartsStock)entity);
                    break;
                case "WarehouseCostChangeBill":
                    UpdateToDatabase(ObjectContext.WarehouseCostChangeBills, (WarehouseCostChangeBill)entity);
                    break;
                case "WarehouseCostChangeDetail":
                    UpdateToDatabase(ObjectContext.WarehouseCostChangeDetails, (WarehouseCostChangeDetail)entity);
                    break;
                case "WarehouseOperator":
                    UpdateToDatabase(ObjectContext.WarehouseOperators, (WarehouseOperator)entity);
                    break;
                case "WarehouseSequence":
                    UpdateToDatabase(ObjectContext.WarehouseSequences, (WarehouseSequence)entity);
                    break;
                case "WarehousingInspection":
                    UpdateToDatabase(ObjectContext.WarehousingInspections, (WarehousingInspection)entity);
                    break;
                case "WmsCongelationStockView":
                    UpdateToDatabase(ObjectContext.WmsCongelationStockViews, (WmsCongelationStockView)entity);
                    break;
                case "WorkGruopPersonNumber":
                    UpdateToDatabase(ObjectContext.WorkGruopPersonNumbers, (WorkGruopPersonNumber)entity);
                    break;
            }
        }

        internal void DeleteFromDatabase<T>(ObjectSet<T> objectSet, T entity) where T : class {
            objectSet.Attach(entity);
            objectSet.DeleteObject(entity);
        }

        internal void DeleteFromDatabase(EntityObject entity) {
            if(entity.EntityState != EntityState.Detached)
                ObjectContext.ObjectStateManager.ChangeObjectState(entity, EntityState.Deleted);
            else {
                switch(entity.GetType().Name) {
                    case "ABCSetting":
                        DeleteFromDatabase(ObjectContext.ABCSettings, (ABCSetting)entity);
                        break;
                    case "ABCStrategy":
                        DeleteFromDatabase(ObjectContext.ABCStrategies, (ABCStrategy)entity);
                        break;
                    case "ABCTypeSafeDay":
                        DeleteFromDatabase(ObjectContext.ABCTypeSafeDays, (ABCTypeSafeDay)entity);
                        break;
                    case "ACCESSINFO_TMP":
                        DeleteFromDatabase(ObjectContext.ACCESSINFO_TMP, (ACCESSINFO_TMP)entity);
                        break;
                    case "AccountGroup":
                        DeleteFromDatabase(ObjectContext.AccountGroups, (AccountGroup)entity);
                        break;
                    case "AccountPayableHistoryDetail":
                        DeleteFromDatabase(ObjectContext.AccountPayableHistoryDetails, (AccountPayableHistoryDetail)entity);
                        break;
                    case "AccountPeriod":
                        DeleteFromDatabase(ObjectContext.AccountPeriods, (AccountPeriod)entity);
                        break;
                    case "AccurateTrace":
                        DeleteFromDatabase(ObjectContext.AccurateTraces, (AccurateTrace)entity);
                        break;
                    case "Agency":
                        DeleteFromDatabase(ObjectContext.Agencies, (Agency)entity);
                        break;
                    case "AgencyAffiBranch":
                        DeleteFromDatabase(ObjectContext.AgencyAffiBranches, (AgencyAffiBranch)entity);
                        break;
                    case "AgencyDealerRelation":
                        DeleteFromDatabase(ObjectContext.AgencyDealerRelations, (AgencyDealerRelation)entity);
                        break;
                    case "AgencyDealerRelationHistory":
                        DeleteFromDatabase(ObjectContext.AgencyDealerRelationHistories, (AgencyDealerRelationHistory)entity);
                        break;
                    case "AgencyDifferenceBackBill":
                        DeleteFromDatabase(ObjectContext.AgencyDifferenceBackBills, (AgencyDifferenceBackBill)entity);
                        break;
                    case "AgencyLogisticCompany":
                        DeleteFromDatabase(ObjectContext.AgencyLogisticCompanies, (AgencyLogisticCompany)entity);
                        break;
                    case "AgencyPartsOutboundBill":
                        DeleteFromDatabase(ObjectContext.AgencyPartsOutboundBills, (AgencyPartsOutboundBill)entity);
                        break;
                    case "AgencyPartsOutboundPlan":
                        DeleteFromDatabase(ObjectContext.AgencyPartsOutboundPlans, (AgencyPartsOutboundPlan)entity);
                        break;
                    case "AgencyPartsShippingOrder":
                        DeleteFromDatabase(ObjectContext.AgencyPartsShippingOrders, (AgencyPartsShippingOrder)entity);
                        break;
                    case "AgencyPartsShippingOrderRef":
                        DeleteFromDatabase(ObjectContext.AgencyPartsShippingOrderRefs, (AgencyPartsShippingOrderRef)entity);
                        break;
                    case "AgencyRetailerList":
                        DeleteFromDatabase(ObjectContext.AgencyRetailerLists, (AgencyRetailerList)entity);
                        break;
                    case "AgencyRetailerOrder":
                        DeleteFromDatabase(ObjectContext.AgencyRetailerOrders, (AgencyRetailerOrder)entity);
                        break;
                    case "AgencyStockQryFrmOrder":
                        DeleteFromDatabase(ObjectContext.AgencyStockQryFrmOrders, (AgencyStockQryFrmOrder)entity);
                        break;
                    case "APartsOutboundBillDetail":
                        DeleteFromDatabase(ObjectContext.APartsOutboundBillDetails, (APartsOutboundBillDetail)entity);
                        break;
                    case "APartsOutboundPlanDetail":
                        DeleteFromDatabase(ObjectContext.APartsOutboundPlanDetails, (APartsOutboundPlanDetail)entity);
                        break;
                    case "APartsShippingOrderDetail":
                        DeleteFromDatabase(ObjectContext.APartsShippingOrderDetails, (APartsShippingOrderDetail)entity);
                        break;
                    case "ApplicationType":
                        DeleteFromDatabase(ObjectContext.ApplicationTypes, (ApplicationType)entity);
                        break;
                    case "AppointDealerDtl":
                        DeleteFromDatabase(ObjectContext.AppointDealerDtls, (AppointDealerDtl)entity);
                        break;
                    case "AppointFaultReasonDtl":
                        DeleteFromDatabase(ObjectContext.AppointFaultReasonDtls, (AppointFaultReasonDtl)entity);
                        break;
                    case "AppointSupplierDtl":
                        DeleteFromDatabase(ObjectContext.AppointSupplierDtls, (AppointSupplierDtl)entity);
                        break;
                    case "AssemblyPartRequisitionLink":
                        DeleteFromDatabase(ObjectContext.AssemblyPartRequisitionLinks, (AssemblyPartRequisitionLink)entity);
                        break;
                    case "AuthenticationType":
                        DeleteFromDatabase(ObjectContext.AuthenticationTypes, (AuthenticationType)entity);
                        break;
                    case "AutoSalesOrderProcess":
                        DeleteFromDatabase(ObjectContext.AutoSalesOrderProcesses, (AutoSalesOrderProcess)entity);
                        break;
                    case "AutoTaskExecutResult":
                        DeleteFromDatabase(ObjectContext.AutoTaskExecutResults, (AutoTaskExecutResult)entity);
                        break;
                    case "BankAccount":
                        DeleteFromDatabase(ObjectContext.BankAccounts, (BankAccount)entity);
                        break;
                    case "BonusPointsOrder":
                        DeleteFromDatabase(ObjectContext.BonusPointsOrders, (BonusPointsOrder)entity);
                        break;
                    case "BonusPointsOrderList":
                        DeleteFromDatabase(ObjectContext.BonusPointsOrderLists, (BonusPointsOrderList)entity);
                        break;
                    case "BonusPointsSummary":
                        DeleteFromDatabase(ObjectContext.BonusPointsSummaries, (BonusPointsSummary)entity);
                        break;
                    case "BonusPointsSummaryList":
                        DeleteFromDatabase(ObjectContext.BonusPointsSummaryLists, (BonusPointsSummaryList)entity);
                        break;
                    case "BorrowBill":
                        DeleteFromDatabase(ObjectContext.BorrowBills, (BorrowBill)entity);
                        break;
                    case "BorrowBillDetail":
                        DeleteFromDatabase(ObjectContext.BorrowBillDetails, (BorrowBillDetail)entity);
                        break;
                    case "BottomStock":
                        DeleteFromDatabase(ObjectContext.BottomStocks, (BottomStock)entity);
                        break;
                    case "BottomStockColVersion":
                        DeleteFromDatabase(ObjectContext.BottomStockColVersions, (BottomStockColVersion)entity);
                        break;
                    case "BottomStockForceReserveSub":
                        DeleteFromDatabase(ObjectContext.BottomStockForceReserveSubs, (BottomStockForceReserveSub)entity);
                        break;
                    case "BottomStockForceReserveType":
                        DeleteFromDatabase(ObjectContext.BottomStockForceReserveTypes, (BottomStockForceReserveType)entity);
                        break;
                    case "BottomStockSettleTable":
                        DeleteFromDatabase(ObjectContext.BottomStockSettleTables, (BottomStockSettleTable)entity);
                        break;
                    case "BottomStockSubVersion":
                        DeleteFromDatabase(ObjectContext.BottomStockSubVersions, (BottomStockSubVersion)entity);
                        break;
                    case "BoxUpTask":
                        DeleteFromDatabase(ObjectContext.BoxUpTasks, (BoxUpTask)entity);
                        break;
                    case "BoxUpTaskDetail":
                        DeleteFromDatabase(ObjectContext.BoxUpTaskDetails, (BoxUpTaskDetail)entity);
                        break;
                    case "Branch":
                        DeleteFromDatabase(ObjectContext.Branches, (Branch)entity);
                        break;
                    case "BranchDateBaseRel":
                        DeleteFromDatabase(ObjectContext.BranchDateBaseRels, (BranchDateBaseRel)entity);
                        break;
                    case "Branchstrategy":
                        DeleteFromDatabase(ObjectContext.Branchstrategies, (Branchstrategy)entity);
                        break;
                    case "BranchSupplierRelation":
                        DeleteFromDatabase(ObjectContext.BranchSupplierRelations, (BranchSupplierRelation)entity);
                        break;
                    case "BrandGradeMessage":
                        DeleteFromDatabase(ObjectContext.BrandGradeMessages, (BrandGradeMessage)entity);
                        break;
                    case "CAReconciliation":
                        DeleteFromDatabase(ObjectContext.CAReconciliations, (CAReconciliation)entity);
                        break;
                    case "CAReconciliationList":
                        DeleteFromDatabase(ObjectContext.CAReconciliationLists, (CAReconciliationList)entity);
                        break;
                    case "CenterABCBasi":
                        DeleteFromDatabase(ObjectContext.CenterABCBasis, (CenterABCBasi)entity);
                        break;
                    case "CenterAccumulateDailyExp":
                        DeleteFromDatabase(ObjectContext.CenterAccumulateDailyExps, (CenterAccumulateDailyExp)entity);
                        break;
                    case "CenterApproveBill":
                        DeleteFromDatabase(ObjectContext.CenterApproveBills, (CenterApproveBill)entity);
                        break;
                    case "CenterAuthorizedProvince":
                        DeleteFromDatabase(ObjectContext.CenterAuthorizedProvinces, (CenterAuthorizedProvince)entity);
                        break;
                    case "CenterQuarterRebate":
                        DeleteFromDatabase(ObjectContext.CenterQuarterRebates, (CenterQuarterRebate)entity);
                        break;
                    case "CenterRebateSummary":
                        DeleteFromDatabase(ObjectContext.CenterRebateSummaries, (CenterRebateSummary)entity);
                        break;
                    case "CenterSaleReturnStrategy":
                        DeleteFromDatabase(ObjectContext.CenterSaleReturnStrategies, (CenterSaleReturnStrategy)entity);
                        break;
                    case "CenterSaleSettle":
                        DeleteFromDatabase(ObjectContext.CenterSaleSettles, (CenterSaleSettle)entity);
                        break;
                    case "ChannelCapability":
                        DeleteFromDatabase(ObjectContext.ChannelCapabilities, (ChannelCapability)entity);
                        break;
                    case "ChannelPlan":
                        DeleteFromDatabase(ObjectContext.ChannelPlans, (ChannelPlan)entity);
                        break;
                    case "ChannelPlanDetail":
                        DeleteFromDatabase(ObjectContext.ChannelPlanDetails, (ChannelPlanDetail)entity);
                        break;
                    case "ChannelPlanHistory":
                        DeleteFromDatabase(ObjectContext.ChannelPlanHistories, (ChannelPlanHistory)entity);
                        break;
                    case "ChannelPlanHistoryDetail":
                        DeleteFromDatabase(ObjectContext.ChannelPlanHistoryDetails, (ChannelPlanHistoryDetail)entity);
                        break;
                    case "ChassisInformation":
                        DeleteFromDatabase(ObjectContext.ChassisInformations, (ChassisInformation)entity);
                        break;
                    case "ClaimApproverStrategy":
                        DeleteFromDatabase(ObjectContext.ClaimApproverStrategies, (ClaimApproverStrategy)entity);
                        break;
                    case "ClaimSettleBillDealerInvoice":
                        DeleteFromDatabase(ObjectContext.ClaimSettleBillDealerInvoices, (ClaimSettleBillDealerInvoice)entity);
                        break;
                    case "CodeTemplate":
                        DeleteFromDatabase(ObjectContext.CodeTemplates, (CodeTemplate)entity);
                        break;
                    case "CodeTemplateSerial":
                        DeleteFromDatabase(ObjectContext.CodeTemplateSerials, (CodeTemplateSerial)entity);
                        break;
                    case "CombinedPart":
                        DeleteFromDatabase(ObjectContext.CombinedParts, (CombinedPart)entity);
                        break;
                    case "Company":
                        DeleteFromDatabase(ObjectContext.Companies, (Company)entity);
                        break;
                    case "company_log":
                        DeleteFromDatabase(ObjectContext.company_log, (company_log)entity);
                        break;
                    case "CompanyAddress":
                        DeleteFromDatabase(ObjectContext.CompanyAddresses, (CompanyAddress)entity);
                        break;
                    case "CompanyDetail":
                        DeleteFromDatabase(ObjectContext.CompanyDetails, (CompanyDetail)entity);
                        break;
                    case "CompanyInvoiceInfo":
                        DeleteFromDatabase(ObjectContext.CompanyInvoiceInfoes, (CompanyInvoiceInfo)entity);
                        break;
                    case "CompanyLoginPicture":
                        DeleteFromDatabase(ObjectContext.CompanyLoginPictures, (CompanyLoginPicture)entity);
                        break;
                    case "CompanyTransferOrder":
                        DeleteFromDatabase(ObjectContext.CompanyTransferOrders, (CompanyTransferOrder)entity);
                        break;
                    case "CompanyTransferOrderDetail":
                        DeleteFromDatabase(ObjectContext.CompanyTransferOrderDetails, (CompanyTransferOrderDetail)entity);
                        break;
                    case "Countersignature":
                        DeleteFromDatabase(ObjectContext.Countersignatures, (Countersignature)entity);
                        break;
                    case "CountersignatureDetail":
                        DeleteFromDatabase(ObjectContext.CountersignatureDetails, (CountersignatureDetail)entity);
                        break;
                    case "CredenceApplication":
                        DeleteFromDatabase(ObjectContext.CredenceApplications, (CredenceApplication)entity);
                        break;
                    case "CrossSalesOrder":
                        DeleteFromDatabase(ObjectContext.CrossSalesOrders, (CrossSalesOrder)entity);
                        break;
                    case "CrossSalesOrderDetail":
                        DeleteFromDatabase(ObjectContext.CrossSalesOrderDetails, (CrossSalesOrderDetail)entity);
                        break;
                    case "Customer":
                        DeleteFromDatabase(ObjectContext.Customers, (Customer)entity);
                        break;
                    case "CustomerAccount":
                        DeleteFromDatabase(ObjectContext.CustomerAccounts, (CustomerAccount)entity);
                        break;
                    case "CustomerAccountHisDetail":
                        DeleteFromDatabase(ObjectContext.CustomerAccountHisDetails, (CustomerAccountHisDetail)entity);
                        break;
                    case "CustomerAccountHistory":
                        DeleteFromDatabase(ObjectContext.CustomerAccountHistories, (CustomerAccountHistory)entity);
                        break;
                    case "CustomerDirectSpareList":
                        DeleteFromDatabase(ObjectContext.CustomerDirectSpareLists, (CustomerDirectSpareList)entity);
                        break;
                    case "CustomerInformation":
                        DeleteFromDatabase(ObjectContext.CustomerInformations, (CustomerInformation)entity);
                        break;
                    case "CustomerOpenAccountApp":
                        DeleteFromDatabase(ObjectContext.CustomerOpenAccountApps, (CustomerOpenAccountApp)entity);
                        break;
                    case "CustomerOrderPriceGrade":
                        DeleteFromDatabase(ObjectContext.CustomerOrderPriceGrades, (CustomerOrderPriceGrade)entity);
                        break;
                    case "CustomerOrderPriceGradeHistory":
                        DeleteFromDatabase(ObjectContext.CustomerOrderPriceGradeHistories, (CustomerOrderPriceGradeHistory)entity);
                        break;
                    case "CustomerSupplyInitialFeeSet":
                        DeleteFromDatabase(ObjectContext.CustomerSupplyInitialFeeSets, (CustomerSupplyInitialFeeSet)entity);
                        break;
                    case "CustomerTransferBill":
                        DeleteFromDatabase(ObjectContext.CustomerTransferBills, (CustomerTransferBill)entity);
                        break;
                    case "CustomerTransferBill_Sync":
                        DeleteFromDatabase(ObjectContext.CustomerTransferBill_Sync, (CustomerTransferBill_Sync)entity);
                        break;
                    case "DailySalesWeight":
                        DeleteFromDatabase(ObjectContext.DailySalesWeights, (DailySalesWeight)entity);
                        break;
                    case "DataBaseLink":
                        DeleteFromDatabase(ObjectContext.DataBaseLinks, (DataBaseLink)entity);
                        break;
                    case "DataBaseLinkBranch":
                        DeleteFromDatabase(ObjectContext.DataBaseLinkBranches, (DataBaseLinkBranch)entity);
                        break;
                    case "Dealer":
                        DeleteFromDatabase(ObjectContext.Dealers, (Dealer)entity);
                        break;
                    case "DealerBusinessPermit":
                        DeleteFromDatabase(ObjectContext.DealerBusinessPermits, (DealerBusinessPermit)entity);
                        break;
                    case "DealerBusinessPermitHistory":
                        DeleteFromDatabase(ObjectContext.DealerBusinessPermitHistories, (DealerBusinessPermitHistory)entity);
                        break;
                    case "DealerClaimSparePart":
                        DeleteFromDatabase(ObjectContext.DealerClaimSpareParts, (DealerClaimSparePart)entity);
                        break;
                    case "DealerFormat":
                        DeleteFromDatabase(ObjectContext.DealerFormats, (DealerFormat)entity);
                        break;
                    case "DealerGradeInfo":
                        DeleteFromDatabase(ObjectContext.DealerGradeInfoes, (DealerGradeInfo)entity);
                        break;
                    case "DealerHistory":
                        DeleteFromDatabase(ObjectContext.DealerHistories, (DealerHistory)entity);
                        break;
                    case "DealerInvoice":
                        DeleteFromDatabase(ObjectContext.DealerInvoices, (DealerInvoice)entity);
                        break;
                    case "DealerKeyEmployee":
                        DeleteFromDatabase(ObjectContext.DealerKeyEmployees, (DealerKeyEmployee)entity);
                        break;
                    case "DealerKeyEmployeeHistory":
                        DeleteFromDatabase(ObjectContext.DealerKeyEmployeeHistories, (DealerKeyEmployeeHistory)entity);
                        break;
                    case "DealerKeyPosition":
                        DeleteFromDatabase(ObjectContext.DealerKeyPositions, (DealerKeyPosition)entity);
                        break;
                    case "DealerMarketDptRelation":
                        DeleteFromDatabase(ObjectContext.DealerMarketDptRelations, (DealerMarketDptRelation)entity);
                        break;
                    case "DealerMobileNumberHistory":
                        DeleteFromDatabase(ObjectContext.DealerMobileNumberHistories, (DealerMobileNumberHistory)entity);
                        break;
                    case "DealerMobileNumberList":
                        DeleteFromDatabase(ObjectContext.DealerMobileNumberLists, (DealerMobileNumberList)entity);
                        break;
                    case "DealerPartsInventoryBill":
                        DeleteFromDatabase(ObjectContext.DealerPartsInventoryBills, (DealerPartsInventoryBill)entity);
                        break;
                    case "DealerPartsInventoryDetail":
                        DeleteFromDatabase(ObjectContext.DealerPartsInventoryDetails, (DealerPartsInventoryDetail)entity);
                        break;
                    case "DealerPartsRetailOrder":
                        DeleteFromDatabase(ObjectContext.DealerPartsRetailOrders, (DealerPartsRetailOrder)entity);
                        break;
                    case "DealerPartsSalesPrice":
                        DeleteFromDatabase(ObjectContext.DealerPartsSalesPrices, (DealerPartsSalesPrice)entity);
                        break;
                    case "DealerPartsSalesPriceHistory":
                        DeleteFromDatabase(ObjectContext.DealerPartsSalesPriceHistories, (DealerPartsSalesPriceHistory)entity);
                        break;
                    case "DealerPartsSalesReturnBill":
                        DeleteFromDatabase(ObjectContext.DealerPartsSalesReturnBills, (DealerPartsSalesReturnBill)entity);
                        break;
                    case "DealerPartsStock":
                        DeleteFromDatabase(ObjectContext.DealerPartsStocks, (DealerPartsStock)entity);
                        break;
                    case "DealerPartsStockOutInRecord":
                        DeleteFromDatabase(ObjectContext.DealerPartsStockOutInRecords, (DealerPartsStockOutInRecord)entity);
                        break;
                    case "DealerPartsStockQueryView":
                        DeleteFromDatabase(ObjectContext.DealerPartsStockQueryViews, (DealerPartsStockQueryView)entity);
                        break;
                    case "DealerPartsTransferOrder":
                        DeleteFromDatabase(ObjectContext.DealerPartsTransferOrders, (DealerPartsTransferOrder)entity);
                        break;
                    case "DealerPartsTransOrderDetail":
                        DeleteFromDatabase(ObjectContext.DealerPartsTransOrderDetails, (DealerPartsTransOrderDetail)entity);
                        break;
                    case "DealerPerTrainAut":
                        DeleteFromDatabase(ObjectContext.DealerPerTrainAuts, (DealerPerTrainAut)entity);
                        break;
                    case "DealerRetailOrderDetail":
                        DeleteFromDatabase(ObjectContext.DealerRetailOrderDetails, (DealerRetailOrderDetail)entity);
                        break;
                    case "DealerRetailReturnBillDetail":
                        DeleteFromDatabase(ObjectContext.DealerRetailReturnBillDetails, (DealerRetailReturnBillDetail)entity);
                        break;
                    case "DealerServiceExt":
                        DeleteFromDatabase(ObjectContext.DealerServiceExts, (DealerServiceExt)entity);
                        break;
                    case "DealerServiceExtHistory":
                        DeleteFromDatabase(ObjectContext.DealerServiceExtHistories, (DealerServiceExtHistory)entity);
                        break;
                    case "DealerServiceInfo":
                        DeleteFromDatabase(ObjectContext.DealerServiceInfoes, (DealerServiceInfo)entity);
                        break;
                    case "DealerServiceInfoHistory":
                        DeleteFromDatabase(ObjectContext.DealerServiceInfoHistories, (DealerServiceInfoHistory)entity);
                        break;
                    case "DeferredDiscountType":
                        DeleteFromDatabase(ObjectContext.DeferredDiscountTypes, (DeferredDiscountType)entity);
                        break;
                    case "DepartmentBrandRelation":
                        DeleteFromDatabase(ObjectContext.DepartmentBrandRelations, (DepartmentBrandRelation)entity);
                        break;
                    case "DepartmentInformation":
                        DeleteFromDatabase(ObjectContext.DepartmentInformations, (DepartmentInformation)entity);
                        break;
                    case "EcommerceFreightDisposal":
                        DeleteFromDatabase(ObjectContext.EcommerceFreightDisposals, (EcommerceFreightDisposal)entity);
                        break;
                    case "EngineModelview":
                        DeleteFromDatabase(ObjectContext.EngineModelviews, (EngineModelview)entity);
                        break;
                    case "EnterprisePartsCost":
                        DeleteFromDatabase(ObjectContext.EnterprisePartsCosts, (EnterprisePartsCost)entity);
                        break;
                    case "ExportCustomerInfo":
                        DeleteFromDatabase(ObjectContext.ExportCustomerInfoes, (ExportCustomerInfo)entity);
                        break;
                    case "Express":
                        DeleteFromDatabase(ObjectContext.Expresses, (Express)entity);
                        break;
                    case "ExpressToLogistic":
                        DeleteFromDatabase(ObjectContext.ExpressToLogistics, (ExpressToLogistic)entity);
                        break;
                    case "ExtendedInfoTemplate":
                        DeleteFromDatabase(ObjectContext.ExtendedInfoTemplates, (ExtendedInfoTemplate)entity);
                        break;
                    case "ExtendedSparePartList":
                        DeleteFromDatabase(ObjectContext.ExtendedSparePartLists, (ExtendedSparePartList)entity);
                        break;
                    case "ExtendedWarrantyOrder":
                        DeleteFromDatabase(ObjectContext.ExtendedWarrantyOrders, (ExtendedWarrantyOrder)entity);
                        break;
                    case "ExtendedWarrantyOrderList":
                        DeleteFromDatabase(ObjectContext.ExtendedWarrantyOrderLists, (ExtendedWarrantyOrderList)entity);
                        break;
                    case "ExtendedWarrantyProduct":
                        DeleteFromDatabase(ObjectContext.ExtendedWarrantyProducts, (ExtendedWarrantyProduct)entity);
                        break;
                    case "FactoryPurchacePrice_Tmp":
                        DeleteFromDatabase(ObjectContext.FactoryPurchacePrice_Tmp, (FactoryPurchacePrice_Tmp)entity);
                        break;
                    case "FaultyPartsSupplierAssembly":
                        DeleteFromDatabase(ObjectContext.FaultyPartsSupplierAssemblies, (FaultyPartsSupplierAssembly)entity);
                        break;
                    case "FDWarrantyMessage":
                        DeleteFromDatabase(ObjectContext.FDWarrantyMessages, (FDWarrantyMessage)entity);
                        break;
                    case "FinancialSnapshotSet":
                        DeleteFromDatabase(ObjectContext.FinancialSnapshotSets, (FinancialSnapshotSet)entity);
                        break;
                    case "ForceReserveBill":
                        DeleteFromDatabase(ObjectContext.ForceReserveBills, (ForceReserveBill)entity);
                        break;
                    case "ForceReserveBillDetail":
                        DeleteFromDatabase(ObjectContext.ForceReserveBillDetails, (ForceReserveBillDetail)entity);
                        break;
                    case "FullStockTask":
                        DeleteFromDatabase(ObjectContext.FullStockTasks, (FullStockTask)entity);
                        break;
                    case "FundMonthlySettleBill":
                        DeleteFromDatabase(ObjectContext.FundMonthlySettleBills, (FundMonthlySettleBill)entity);
                        break;
                    case "FundMonthlySettleBillDetail":
                        DeleteFromDatabase(ObjectContext.FundMonthlySettleBillDetails, (FundMonthlySettleBillDetail)entity);
                        break;
                    case "GetAllPartsPlannedPrice":
                        DeleteFromDatabase(ObjectContext.GetAllPartsPlannedPrices, (GetAllPartsPlannedPrice)entity);
                        break;
                    case "GetAllPartsPurchasePricing":
                        DeleteFromDatabase(ObjectContext.GetAllPartsPurchasePricings, (GetAllPartsPurchasePricing)entity);
                        break;
                    case "GetAllPartsSalesPrice":
                        DeleteFromDatabase(ObjectContext.GetAllPartsSalesPrices, (GetAllPartsSalesPrice)entity);
                        break;
                    case "GoldenTaxClassifyMsg":
                        DeleteFromDatabase(ObjectContext.GoldenTaxClassifyMsgs, (GoldenTaxClassifyMsg)entity);
                        break;
                    case "GradeCoefficient":
                        DeleteFromDatabase(ObjectContext.GradeCoefficients, (GradeCoefficient)entity);
                        break;
                    case "GradeCoefficientView":
                        DeleteFromDatabase(ObjectContext.GradeCoefficientViews, (GradeCoefficientView)entity);
                        break;
                    case "HauDistanceInfor":
                        DeleteFromDatabase(ObjectContext.HauDistanceInfors, (HauDistanceInfor)entity);
                        break;
                    case "InOutSettleSummary":
                        DeleteFromDatabase(ObjectContext.InOutSettleSummaries, (InOutSettleSummary)entity);
                        break;
                    case "InOutWorkPerHour":
                        DeleteFromDatabase(ObjectContext.InOutWorkPerHours, (InOutWorkPerHour)entity);
                        break;
                    case "IntegralSettleDictate":
                        DeleteFromDatabase(ObjectContext.IntegralSettleDictates, (IntegralSettleDictate)entity);
                        break;
                    case "IntelligentOrderMonthly":
                        DeleteFromDatabase(ObjectContext.IntelligentOrderMonthlies, (IntelligentOrderMonthly)entity);
                        break;
                    case "IntelligentOrderMonthlyBase":
                        DeleteFromDatabase(ObjectContext.IntelligentOrderMonthlyBases, (IntelligentOrderMonthlyBase)entity);
                        break;
                    case "IntelligentOrderWeekly":
                        DeleteFromDatabase(ObjectContext.IntelligentOrderWeeklies, (IntelligentOrderWeekly)entity);
                        break;
                    case "IntelligentOrderWeeklyBase":
                        DeleteFromDatabase(ObjectContext.IntelligentOrderWeeklyBases, (IntelligentOrderWeeklyBase)entity);
                        break;
                    case "InternalAcquisitionBill":
                        DeleteFromDatabase(ObjectContext.InternalAcquisitionBills, (InternalAcquisitionBill)entity);
                        break;
                    case "InternalAcquisitionDetail":
                        DeleteFromDatabase(ObjectContext.InternalAcquisitionDetails, (InternalAcquisitionDetail)entity);
                        break;
                    case "InternalAllocationBill":
                        DeleteFromDatabase(ObjectContext.InternalAllocationBills, (InternalAllocationBill)entity);
                        break;
                    case "InternalAllocationDetail":
                        DeleteFromDatabase(ObjectContext.InternalAllocationDetails, (InternalAllocationDetail)entity);
                        break;
                    case "InteSettleBillInvoiceLink":
                        DeleteFromDatabase(ObjectContext.InteSettleBillInvoiceLinks, (InteSettleBillInvoiceLink)entity);
                        break;
                    case "InvoiceInformation":
                        DeleteFromDatabase(ObjectContext.InvoiceInformations, (InvoiceInformation)entity);
                        break;
                    case "IvecoPriceChangeApp":
                        DeleteFromDatabase(ObjectContext.IvecoPriceChangeApps, (IvecoPriceChangeApp)entity);
                        break;
                    case "IvecoPriceChangeAppDetail":
                        DeleteFromDatabase(ObjectContext.IvecoPriceChangeAppDetails, (IvecoPriceChangeAppDetail)entity);
                        break;
                    case "IvecoSalesPrice":
                        DeleteFromDatabase(ObjectContext.IvecoSalesPrices, (IvecoSalesPrice)entity);
                        break;
                    case "KeyValueItem":
                        DeleteFromDatabase(ObjectContext.KeyValueItems, (KeyValueItem)entity);
                        break;
                    case "LayerNodeType":
                        DeleteFromDatabase(ObjectContext.LayerNodeTypes, (LayerNodeType)entity);
                        break;
                    case "LayerNodeTypeAffiliation":
                        DeleteFromDatabase(ObjectContext.LayerNodeTypeAffiliations, (LayerNodeTypeAffiliation)entity);
                        break;
                    case "LayerStructure":
                        DeleteFromDatabase(ObjectContext.LayerStructures, (LayerStructure)entity);
                        break;
                    case "LayerStructurePurpose":
                        DeleteFromDatabase(ObjectContext.LayerStructurePurposes, (LayerStructurePurpose)entity);
                        break;
                    case "LinkAllocation":
                        DeleteFromDatabase(ObjectContext.LinkAllocations, (LinkAllocation)entity);
                        break;
                    case "LoadingDetail":
                        DeleteFromDatabase(ObjectContext.LoadingDetails, (LoadingDetail)entity);
                        break;
                    case "LocketUnitQuery":
                        DeleteFromDatabase(ObjectContext.LocketUnitQueries, (LocketUnitQuery)entity);
                        break;
                    case "Logistic":
                        DeleteFromDatabase(ObjectContext.Logistics, (Logistic)entity);
                        break;
                    case "LogisticCompany":
                        DeleteFromDatabase(ObjectContext.LogisticCompanies, (LogisticCompany)entity);
                        break;
                    case "LogisticCompanyServiceRange":
                        DeleteFromDatabase(ObjectContext.LogisticCompanyServiceRanges, (LogisticCompanyServiceRange)entity);
                        break;
                    case "LogisticsDetail":
                        DeleteFromDatabase(ObjectContext.LogisticsDetails, (LogisticsDetail)entity);
                        break;
                    case "MaintainRedProductMap":
                        DeleteFromDatabase(ObjectContext.MaintainRedProductMaps, (MaintainRedProductMap)entity);
                        break;
                    case "MalfunctionView":
                        DeleteFromDatabase(ObjectContext.MalfunctionViews, (MalfunctionView)entity);
                        break;
                    case "ManualScheduling":
                        DeleteFromDatabase(ObjectContext.ManualSchedulings, (ManualScheduling)entity);
                        break;
                    case "MarketABQualityInformation":
                        DeleteFromDatabase(ObjectContext.MarketABQualityInformations, (MarketABQualityInformation)entity);
                        break;
                    case "MarketDptPersonnelRelation":
                        DeleteFromDatabase(ObjectContext.MarketDptPersonnelRelations, (MarketDptPersonnelRelation)entity);
                        break;
                    case "MarketingDepartment":
                        DeleteFromDatabase(ObjectContext.MarketingDepartments, (MarketingDepartment)entity);
                        break;
                    case "MultiLevelApproveConfig":
                        DeleteFromDatabase(ObjectContext.MultiLevelApproveConfigs, (MultiLevelApproveConfig)entity);
                        break;
                    case "Notification":
                        DeleteFromDatabase(ObjectContext.Notifications, (Notification)entity);
                        break;
                    case "NotificationLimit":
                        DeleteFromDatabase(ObjectContext.NotificationLimits, (NotificationLimit)entity);
                        break;
                    case "NotificationOpLog":
                        DeleteFromDatabase(ObjectContext.NotificationOpLogs, (NotificationOpLog)entity);
                        break;
                    case "NotificationReply":
                        DeleteFromDatabase(ObjectContext.NotificationReplies, (NotificationReply)entity);
                        break;
                    case "OrderApproveWeekday":
                        DeleteFromDatabase(ObjectContext.OrderApproveWeekdays, (OrderApproveWeekday)entity);
                        break;
                    case "OutboundOrder":
                        DeleteFromDatabase(ObjectContext.OutboundOrders, (OutboundOrder)entity);
                        break;
                    case "OutofWarrantyPayment":
                        DeleteFromDatabase(ObjectContext.OutofWarrantyPayments, (OutofWarrantyPayment)entity);
                        break;
                    case "OverstockPartsAdjustBill":
                        DeleteFromDatabase(ObjectContext.OverstockPartsAdjustBills, (OverstockPartsAdjustBill)entity);
                        break;
                    case "OverstockPartsAdjustDetail":
                        DeleteFromDatabase(ObjectContext.OverstockPartsAdjustDetails, (OverstockPartsAdjustDetail)entity);
                        break;
                    case "OverstockPartsApp":
                        DeleteFromDatabase(ObjectContext.OverstockPartsApps, (OverstockPartsApp)entity);
                        break;
                    case "OverstockPartsAppDetail":
                        DeleteFromDatabase(ObjectContext.OverstockPartsAppDetails, (OverstockPartsAppDetail)entity);
                        break;
                    case "OverstockPartsInformation":
                        DeleteFromDatabase(ObjectContext.OverstockPartsInformations, (OverstockPartsInformation)entity);
                        break;
                    case "OverstockPartsPlatFormBill":
                        DeleteFromDatabase(ObjectContext.OverstockPartsPlatFormBills, (OverstockPartsPlatFormBill)entity);
                        break;
                    case "OverstockPartsRecommendBill":
                        DeleteFromDatabase(ObjectContext.OverstockPartsRecommendBills, (OverstockPartsRecommendBill)entity);
                        break;
                    case "OverstockTransferOrder":
                        DeleteFromDatabase(ObjectContext.OverstockTransferOrders, (OverstockTransferOrder)entity);
                        break;
                    case "OverstockTransferOrderDetail":
                        DeleteFromDatabase(ObjectContext.OverstockTransferOrderDetails, (OverstockTransferOrderDetail)entity);
                        break;
                    case "PackingTask":
                        DeleteFromDatabase(ObjectContext.PackingTasks, (PackingTask)entity);
                        break;
                    case "PackingTaskDetail":
                        DeleteFromDatabase(ObjectContext.PackingTaskDetails, (PackingTaskDetail)entity);
                        break;
                    case "PartABCDetail":
                        DeleteFromDatabase(ObjectContext.PartABCDetails, (PartABCDetail)entity);
                        break;
                    case "PartConPurchasePlan":
                        DeleteFromDatabase(ObjectContext.PartConPurchasePlans, (PartConPurchasePlan)entity);
                        break;
                    case "PartDeleaveHistory":
                        DeleteFromDatabase(ObjectContext.PartDeleaveHistories, (PartDeleaveHistory)entity);
                        break;
                    case "PartDeleaveInformation":
                        DeleteFromDatabase(ObjectContext.PartDeleaveInformations, (PartDeleaveInformation)entity);
                        break;
                    case "PartNotConPurchasePlan":
                        DeleteFromDatabase(ObjectContext.PartNotConPurchasePlans, (PartNotConPurchasePlan)entity);
                        break;
                    case "PartRequisitionReturnBill":
                        DeleteFromDatabase(ObjectContext.PartRequisitionReturnBills, (PartRequisitionReturnBill)entity);
                        break;
                    case "PartSafeStock":
                        DeleteFromDatabase(ObjectContext.PartSafeStocks, (PartSafeStock)entity);
                        break;
                    case "PartsBatchRecord":
                        DeleteFromDatabase(ObjectContext.PartsBatchRecords, (PartsBatchRecord)entity);
                        break;
                    case "PartsBranch":
                        DeleteFromDatabase(ObjectContext.PartsBranches, (PartsBranch)entity);
                        break;
                    case "PartsBranchHistory":
                        DeleteFromDatabase(ObjectContext.PartsBranchHistories, (PartsBranchHistory)entity);
                        break;
                    case "PartsBranchPackingProp":
                        DeleteFromDatabase(ObjectContext.PartsBranchPackingProps, (PartsBranchPackingProp)entity);
                        break;
                    case "PartsBranchView":
                        DeleteFromDatabase(ObjectContext.PartsBranchViews, (PartsBranchView)entity);
                        break;
                    case "PartsClaimOrderNew":
                        DeleteFromDatabase(ObjectContext.PartsClaimOrderNews, (PartsClaimOrderNew)entity);
                        break;
                    case "PartsClaimPrice":
                        DeleteFromDatabase(ObjectContext.PartsClaimPrices, (PartsClaimPrice)entity);
                        break;
                    case "PartsClaimPriceForbw":
                        DeleteFromDatabase(ObjectContext.PartsClaimPriceForbws, (PartsClaimPriceForbw)entity);
                        break;
                    case "PartsCostTransferInTransit":
                        DeleteFromDatabase(ObjectContext.PartsCostTransferInTransits, (PartsCostTransferInTransit)entity);
                        break;
                    case "PartsDifferenceBackBill":
                        DeleteFromDatabase(ObjectContext.PartsDifferenceBackBills, (PartsDifferenceBackBill)entity);
                        break;
                    case "PartsDifferenceBackBillDtl":
                        DeleteFromDatabase(ObjectContext.PartsDifferenceBackBillDtls, (PartsDifferenceBackBillDtl)entity);
                        break;
                    case "PartsExchange":
                        DeleteFromDatabase(ObjectContext.PartsExchanges, (PartsExchange)entity);
                        break;
                    case "PartsExchangeGroup":
                        DeleteFromDatabase(ObjectContext.PartsExchangeGroups, (PartsExchangeGroup)entity);
                        break;
                    case "PartsExchangeHistory":
                        DeleteFromDatabase(ObjectContext.PartsExchangeHistories, (PartsExchangeHistory)entity);
                        break;
                    case "PartsHistoryStock":
                        DeleteFromDatabase(ObjectContext.PartsHistoryStocks, (PartsHistoryStock)entity);
                        break;
                    case "PartsInboundCheckBill":
                        DeleteFromDatabase(ObjectContext.PartsInboundCheckBills, (PartsInboundCheckBill)entity);
                        break;
                    case "PartsInboundCheckBill_Sync":
                        DeleteFromDatabase(ObjectContext.PartsInboundCheckBill_Sync, (PartsInboundCheckBill_Sync)entity);
                        break;
                    case "PartsInboundCheckBillDetail":
                        DeleteFromDatabase(ObjectContext.PartsInboundCheckBillDetails, (PartsInboundCheckBillDetail)entity);
                        break;
                    case "PartsInboundPackingDetail":
                        DeleteFromDatabase(ObjectContext.PartsInboundPackingDetails, (PartsInboundPackingDetail)entity);
                        break;
                    case "PartsInboundPerformance":
                        DeleteFromDatabase(ObjectContext.PartsInboundPerformances, (PartsInboundPerformance)entity);
                        break;
                    case "PartsInboundPlan":
                        DeleteFromDatabase(ObjectContext.PartsInboundPlans, (PartsInboundPlan)entity);
                        break;
                    case "PartsInboundPlanDetail":
                        DeleteFromDatabase(ObjectContext.PartsInboundPlanDetails, (PartsInboundPlanDetail)entity);
                        break;
                    case "PartsInventoryBill":
                        DeleteFromDatabase(ObjectContext.PartsInventoryBills, (PartsInventoryBill)entity);
                        break;
                    case "PartsInventoryDetail":
                        DeleteFromDatabase(ObjectContext.PartsInventoryDetails, (PartsInventoryDetail)entity);
                        break;
                    case "PartsLockedStock":
                        DeleteFromDatabase(ObjectContext.PartsLockedStocks, (PartsLockedStock)entity);
                        break;
                    case "PartsLogisticBatch":
                        DeleteFromDatabase(ObjectContext.PartsLogisticBatches, (PartsLogisticBatch)entity);
                        break;
                    case "PartsLogisticBatchBillDetail":
                        DeleteFromDatabase(ObjectContext.PartsLogisticBatchBillDetails, (PartsLogisticBatchBillDetail)entity);
                        break;
                    case "PartsLogisticBatchItemDetail":
                        DeleteFromDatabase(ObjectContext.PartsLogisticBatchItemDetails, (PartsLogisticBatchItemDetail)entity);
                        break;
                    case "PartsManagementCostGrade":
                        DeleteFromDatabase(ObjectContext.PartsManagementCostGrades, (PartsManagementCostGrade)entity);
                        break;
                    case "PartsManagementCostRate":
                        DeleteFromDatabase(ObjectContext.PartsManagementCostRates, (PartsManagementCostRate)entity);
                        break;
                    case "PartsOutboundBill":
                        DeleteFromDatabase(ObjectContext.PartsOutboundBills, (PartsOutboundBill)entity);
                        break;
                    case "PartsOutboundBillDetail":
                        DeleteFromDatabase(ObjectContext.PartsOutboundBillDetails, (PartsOutboundBillDetail)entity);
                        break;
                    case "PartsOutboundPerformance":
                        DeleteFromDatabase(ObjectContext.PartsOutboundPerformances, (PartsOutboundPerformance)entity);
                        break;
                    case "PartsOutboundPlan":
                        DeleteFromDatabase(ObjectContext.PartsOutboundPlans, (PartsOutboundPlan)entity);
                        break;
                    case "PartsOutboundPlanDetail":
                        DeleteFromDatabase(ObjectContext.PartsOutboundPlanDetails, (PartsOutboundPlanDetail)entity);
                        break;
                    case "PartsOuterPurchaseChange":
                        DeleteFromDatabase(ObjectContext.PartsOuterPurchaseChanges, (PartsOuterPurchaseChange)entity);
                        break;
                    case "PartsOuterPurchaselist":
                        DeleteFromDatabase(ObjectContext.PartsOuterPurchaselists, (PartsOuterPurchaselist)entity);
                        break;
                    case "PartsPacking":
                        DeleteFromDatabase(ObjectContext.PartsPackings, (PartsPacking)entity);
                        break;
                    case "PartsPackingPropAppDetail":
                        DeleteFromDatabase(ObjectContext.PartsPackingPropAppDetails, (PartsPackingPropAppDetail)entity);
                        break;
                    case "PartsPackingPropertyApp":
                        DeleteFromDatabase(ObjectContext.PartsPackingPropertyApps, (PartsPackingPropertyApp)entity);
                        break;
                    case "PartsPlannedPrice":
                        DeleteFromDatabase(ObjectContext.PartsPlannedPrices, (PartsPlannedPrice)entity);
                        break;
                    case "PartsPriceIncreaseRate":
                        DeleteFromDatabase(ObjectContext.PartsPriceIncreaseRates, (PartsPriceIncreaseRate)entity);
                        break;
                    case "PartsPriceTimeLimit":
                        DeleteFromDatabase(ObjectContext.PartsPriceTimeLimits, (PartsPriceTimeLimit)entity);
                        break;
                    case "PartsPriorityBill":
                        DeleteFromDatabase(ObjectContext.PartsPriorityBills, (PartsPriorityBill)entity);
                        break;
                    case "PartsPurchaseInfoSummary":
                        DeleteFromDatabase(ObjectContext.PartsPurchaseInfoSummaries, (PartsPurchaseInfoSummary)entity);
                        break;
                    case "PartsPurchaseOrder":
                        DeleteFromDatabase(ObjectContext.PartsPurchaseOrders, (PartsPurchaseOrder)entity);
                        break;
                    case "PartsPurchaseOrder_Sync":
                        DeleteFromDatabase(ObjectContext.PartsPurchaseOrder_Sync, (PartsPurchaseOrder_Sync)entity);
                        break;
                    case "PartsPurchaseOrderDetail":
                        DeleteFromDatabase(ObjectContext.PartsPurchaseOrderDetails, (PartsPurchaseOrderDetail)entity);
                        break;
                    case "PartsPurchaseOrderType":
                        DeleteFromDatabase(ObjectContext.PartsPurchaseOrderTypes, (PartsPurchaseOrderType)entity);
                        break;
                    case "PartsPurchasePlan":
                        DeleteFromDatabase(ObjectContext.PartsPurchasePlans, (PartsPurchasePlan)entity);
                        break;
                    case "PartsPurchasePlan_HW":
                        DeleteFromDatabase(ObjectContext.PartsPurchasePlan_HW, (PartsPurchasePlan_HW)entity);
                        break;
                    case "PartsPurchasePlanDetail":
                        DeleteFromDatabase(ObjectContext.PartsPurchasePlanDetails, (PartsPurchasePlanDetail)entity);
                        break;
                    case "PartsPurchasePlanDetail_HW":
                        DeleteFromDatabase(ObjectContext.PartsPurchasePlanDetail_HW, (PartsPurchasePlanDetail_HW)entity);
                        break;
                    case "PartsPurchasePlanTemp":
                        DeleteFromDatabase(ObjectContext.PartsPurchasePlanTemps, (PartsPurchasePlanTemp)entity);
                        break;
                    case "PartsPurchasePricing":
                        DeleteFromDatabase(ObjectContext.PartsPurchasePricings, (PartsPurchasePricing)entity);
                        break;
                    case "PartsPurchasePricingChange":
                        DeleteFromDatabase(ObjectContext.PartsPurchasePricingChanges, (PartsPurchasePricingChange)entity);
                        break;
                    case "PartsPurchasePricingDetail":
                        DeleteFromDatabase(ObjectContext.PartsPurchasePricingDetails, (PartsPurchasePricingDetail)entity);
                        break;
                    case "PartsPurchaseRtnSettleBill":
                        DeleteFromDatabase(ObjectContext.PartsPurchaseRtnSettleBills, (PartsPurchaseRtnSettleBill)entity);
                        break;
                    case "PartsPurchaseRtnSettleDetail":
                        DeleteFromDatabase(ObjectContext.PartsPurchaseRtnSettleDetails, (PartsPurchaseRtnSettleDetail)entity);
                        break;
                    case "PartsPurchaseRtnSettleRef":
                        DeleteFromDatabase(ObjectContext.PartsPurchaseRtnSettleRefs, (PartsPurchaseRtnSettleRef)entity);
                        break;
                    case "PartsPurchaseSettleBill":
                        DeleteFromDatabase(ObjectContext.PartsPurchaseSettleBills, (PartsPurchaseSettleBill)entity);
                        break;
                    case "PartsPurchaseSettleDetail":
                        DeleteFromDatabase(ObjectContext.PartsPurchaseSettleDetails, (PartsPurchaseSettleDetail)entity);
                        break;
                    case "PartsPurchaseSettleRef":
                        DeleteFromDatabase(ObjectContext.PartsPurchaseSettleRefs, (PartsPurchaseSettleRef)entity);
                        break;
                    case "PartsPurReturnOrder":
                        DeleteFromDatabase(ObjectContext.PartsPurReturnOrders, (PartsPurReturnOrder)entity);
                        break;
                    case "PartsPurReturnOrderDetail":
                        DeleteFromDatabase(ObjectContext.PartsPurReturnOrderDetails, (PartsPurReturnOrderDetail)entity);
                        break;
                    case "PartsRebateAccount":
                        DeleteFromDatabase(ObjectContext.PartsRebateAccounts, (PartsRebateAccount)entity);
                        break;
                    case "PartsRebateApplication":
                        DeleteFromDatabase(ObjectContext.PartsRebateApplications, (PartsRebateApplication)entity);
                        break;
                    case "PartsRebateChangeDetail":
                        DeleteFromDatabase(ObjectContext.PartsRebateChangeDetails, (PartsRebateChangeDetail)entity);
                        break;
                    case "PartsRebateType":
                        DeleteFromDatabase(ObjectContext.PartsRebateTypes, (PartsRebateType)entity);
                        break;
                    case "PartsReplacement":
                        DeleteFromDatabase(ObjectContext.PartsReplacements, (PartsReplacement)entity);
                        break;
                    case "PartsReplacementHistory":
                        DeleteFromDatabase(ObjectContext.PartsReplacementHistories, (PartsReplacementHistory)entity);
                        break;
                    case "PartsRequisitionSettleBill":
                        DeleteFromDatabase(ObjectContext.PartsRequisitionSettleBills, (PartsRequisitionSettleBill)entity);
                        break;
                    case "PartsRequisitionSettleDetail":
                        DeleteFromDatabase(ObjectContext.PartsRequisitionSettleDetails, (PartsRequisitionSettleDetail)entity);
                        break;
                    case "PartsRequisitionSettleRef":
                        DeleteFromDatabase(ObjectContext.PartsRequisitionSettleRefs, (PartsRequisitionSettleRef)entity);
                        break;
                    case "PartsRetailGuidePrice":
                        DeleteFromDatabase(ObjectContext.PartsRetailGuidePrices, (PartsRetailGuidePrice)entity);
                        break;
                    case "PartsRetailGuidePriceHistory":
                        DeleteFromDatabase(ObjectContext.PartsRetailGuidePriceHistories, (PartsRetailGuidePriceHistory)entity);
                        break;
                    case "PartsRetailOrder":
                        DeleteFromDatabase(ObjectContext.PartsRetailOrders, (PartsRetailOrder)entity);
                        break;
                    case "PartsRetailOrderDetail":
                        DeleteFromDatabase(ObjectContext.PartsRetailOrderDetails, (PartsRetailOrderDetail)entity);
                        break;
                    case "PartsRetailReturnBill":
                        DeleteFromDatabase(ObjectContext.PartsRetailReturnBills, (PartsRetailReturnBill)entity);
                        break;
                    case "PartsRetailReturnBillDetail":
                        DeleteFromDatabase(ObjectContext.PartsRetailReturnBillDetails, (PartsRetailReturnBillDetail)entity);
                        break;
                    case "PartsSalePriceIncreaseRate":
                        DeleteFromDatabase(ObjectContext.PartsSalePriceIncreaseRates, (PartsSalePriceIncreaseRate)entity);
                        break;
                    case "PartsSalesCategory":
                        DeleteFromDatabase(ObjectContext.PartsSalesCategories, (PartsSalesCategory)entity);
                        break;
                    case "PartsSalesCategoryView":
                        DeleteFromDatabase(ObjectContext.PartsSalesCategoryViews, (PartsSalesCategoryView)entity);
                        break;
                    case "PartsSalesOrder":
                        DeleteFromDatabase(ObjectContext.PartsSalesOrders, (PartsSalesOrder)entity);
                        break;
                    case "PartsSalesOrderASAP":
                        DeleteFromDatabase(ObjectContext.PartsSalesOrderASAPs, (PartsSalesOrderASAP)entity);
                        break;
                    case "PartsSalesOrderDetail":
                        DeleteFromDatabase(ObjectContext.PartsSalesOrderDetails, (PartsSalesOrderDetail)entity);
                        break;
                    case "PartsSalesOrderDetailASAP":
                        DeleteFromDatabase(ObjectContext.PartsSalesOrderDetailASAPs, (PartsSalesOrderDetailASAP)entity);
                        break;
                    case "PartsSalesOrderProcess":
                        DeleteFromDatabase(ObjectContext.PartsSalesOrderProcesses, (PartsSalesOrderProcess)entity);
                        break;
                    case "PartsSalesOrderProcessDetail":
                        DeleteFromDatabase(ObjectContext.PartsSalesOrderProcessDetails, (PartsSalesOrderProcessDetail)entity);
                        break;
                    case "PartsSalesOrderType":
                        DeleteFromDatabase(ObjectContext.PartsSalesOrderTypes, (PartsSalesOrderType)entity);
                        break;
                    case "PartsSalesPrice":
                        DeleteFromDatabase(ObjectContext.PartsSalesPrices, (PartsSalesPrice)entity);
                        break;
                    case "PartsSalesPriceChange":
                        DeleteFromDatabase(ObjectContext.PartsSalesPriceChanges, (PartsSalesPriceChange)entity);
                        break;
                    case "PartsSalesPriceChangeDetail":
                        DeleteFromDatabase(ObjectContext.PartsSalesPriceChangeDetails, (PartsSalesPriceChangeDetail)entity);
                        break;
                    case "PartsSalesPriceHistory":
                        DeleteFromDatabase(ObjectContext.PartsSalesPriceHistories, (PartsSalesPriceHistory)entity);
                        break;
                    case "PartsSalesReturnBill":
                        DeleteFromDatabase(ObjectContext.PartsSalesReturnBills, (PartsSalesReturnBill)entity);
                        break;
                    case "PartsSalesReturnBillDetail":
                        DeleteFromDatabase(ObjectContext.PartsSalesReturnBillDetails, (PartsSalesReturnBillDetail)entity);
                        break;
                    case "PartsSalesRtnSettlement":
                        DeleteFromDatabase(ObjectContext.PartsSalesRtnSettlements, (PartsSalesRtnSettlement)entity);
                        break;
                    case "PartsSalesRtnSettlementDetail":
                        DeleteFromDatabase(ObjectContext.PartsSalesRtnSettlementDetails, (PartsSalesRtnSettlementDetail)entity);
                        break;
                    case "PartsSalesRtnSettlementRef":
                        DeleteFromDatabase(ObjectContext.PartsSalesRtnSettlementRefs, (PartsSalesRtnSettlementRef)entity);
                        break;
                    case "PartsSalesSettlement":
                        DeleteFromDatabase(ObjectContext.PartsSalesSettlements, (PartsSalesSettlement)entity);
                        break;
                    case "PartsSalesSettlementDetail":
                        DeleteFromDatabase(ObjectContext.PartsSalesSettlementDetails, (PartsSalesSettlementDetail)entity);
                        break;
                    case "PartsSalesSettlementRef":
                        DeleteFromDatabase(ObjectContext.PartsSalesSettlementRefs, (PartsSalesSettlementRef)entity);
                        break;
                    case "PartsSalesWeeklyBase":
                        DeleteFromDatabase(ObjectContext.PartsSalesWeeklyBases, (PartsSalesWeeklyBase)entity);
                        break;
                    case "PartsServiceLevel":
                        DeleteFromDatabase(ObjectContext.PartsServiceLevels, (PartsServiceLevel)entity);
                        break;
                    case "PartsShelvesTask":
                        DeleteFromDatabase(ObjectContext.PartsShelvesTasks, (PartsShelvesTask)entity);
                        break;
                    case "PartsShiftOrder":
                        DeleteFromDatabase(ObjectContext.PartsShiftOrders, (PartsShiftOrder)entity);
                        break;
                    case "PartsShiftOrderDetail":
                        DeleteFromDatabase(ObjectContext.PartsShiftOrderDetails, (PartsShiftOrderDetail)entity);
                        break;
                    case "PartsShiftSIHDetail":
                        DeleteFromDatabase(ObjectContext.PartsShiftSIHDetails, (PartsShiftSIHDetail)entity);
                        break;
                    case "PartsShippingOrder":
                        DeleteFromDatabase(ObjectContext.PartsShippingOrders, (PartsShippingOrder)entity);
                        break;
                    case "PartsShippingOrderDetail":
                        DeleteFromDatabase(ObjectContext.PartsShippingOrderDetails, (PartsShippingOrderDetail)entity);
                        break;
                    case "PartsShippingOrderRef":
                        DeleteFromDatabase(ObjectContext.PartsShippingOrderRefs, (PartsShippingOrderRef)entity);
                        break;
                    case "PartsSpecialPriceHistory":
                        DeleteFromDatabase(ObjectContext.PartsSpecialPriceHistories, (PartsSpecialPriceHistory)entity);
                        break;
                    case "PartsSpecialTreatyPrice":
                        DeleteFromDatabase(ObjectContext.PartsSpecialTreatyPrices, (PartsSpecialTreatyPrice)entity);
                        break;
                    case "PartsStandardStockBill":
                        DeleteFromDatabase(ObjectContext.PartsStandardStockBills, (PartsStandardStockBill)entity);
                        break;
                    case "PartsStock":
                        DeleteFromDatabase(ObjectContext.PartsStocks, (PartsStock)entity);
                        break;
                    case "PartsStockBatchDetail":
                        DeleteFromDatabase(ObjectContext.PartsStockBatchDetails, (PartsStockBatchDetail)entity);
                        break;
                    case "PartsStockCoefficient":
                        DeleteFromDatabase(ObjectContext.PartsStockCoefficients, (PartsStockCoefficient)entity);
                        break;
                    case "PartsStockCoefficientPrice":
                        DeleteFromDatabase(ObjectContext.PartsStockCoefficientPrices, (PartsStockCoefficientPrice)entity);
                        break;
                    case "PartsStockHistory":
                        DeleteFromDatabase(ObjectContext.PartsStockHistories, (PartsStockHistory)entity);
                        break;
                    case "PartsSupplier":
                        DeleteFromDatabase(ObjectContext.PartsSuppliers, (PartsSupplier)entity);
                        break;
                    case "PartsSupplierCollection":
                        DeleteFromDatabase(ObjectContext.PartsSupplierCollections, (PartsSupplierCollection)entity);
                        break;
                    case "PartsSupplierRelation":
                        DeleteFromDatabase(ObjectContext.PartsSupplierRelations, (PartsSupplierRelation)entity);
                        break;
                    case "PartsSupplierRelationHistory":
                        DeleteFromDatabase(ObjectContext.PartsSupplierRelationHistories, (PartsSupplierRelationHistory)entity);
                        break;
                    case "PartsSupplierStock":
                        DeleteFromDatabase(ObjectContext.PartsSupplierStocks, (PartsSupplierStock)entity);
                        break;
                    case "PartsSupplierStockHistory":
                        DeleteFromDatabase(ObjectContext.PartsSupplierStockHistories, (PartsSupplierStockHistory)entity);
                        break;
                    case "PartsTransferOrder":
                        DeleteFromDatabase(ObjectContext.PartsTransferOrders, (PartsTransferOrder)entity);
                        break;
                    case "PartsTransferOrderDetail":
                        DeleteFromDatabase(ObjectContext.PartsTransferOrderDetails, (PartsTransferOrderDetail)entity);
                        break;
                    case "PartsWarehousePendingCost":
                        DeleteFromDatabase(ObjectContext.PartsWarehousePendingCosts, (PartsWarehousePendingCost)entity);
                        break;
                    case "PaymentBeneficiaryList":
                        DeleteFromDatabase(ObjectContext.PaymentBeneficiaryLists, (PaymentBeneficiaryList)entity);
                        break;
                    case "PaymentBill":
                        DeleteFromDatabase(ObjectContext.PaymentBills, (PaymentBill)entity);
                        break;
                    case "PaymentBill_Sync":
                        DeleteFromDatabase(ObjectContext.PaymentBill_Sync, (PaymentBill_Sync)entity);
                        break;
                    case "PayOutBill":
                        DeleteFromDatabase(ObjectContext.PayOutBills, (PayOutBill)entity);
                        break;
                    case "Personnel":
                        DeleteFromDatabase(ObjectContext.Personnels, (Personnel)entity);
                        break;
                    case "PersonnelSupplierRelation":
                        DeleteFromDatabase(ObjectContext.PersonnelSupplierRelations, (PersonnelSupplierRelation)entity);
                        break;
                    case "PersonSalesCenterLink":
                        DeleteFromDatabase(ObjectContext.PersonSalesCenterLinks, (PersonSalesCenterLink)entity);
                        break;
                    case "PersonSubDealer":
                        DeleteFromDatabase(ObjectContext.PersonSubDealers, (PersonSubDealer)entity);
                        break;
                    case "PickingTask":
                        DeleteFromDatabase(ObjectContext.PickingTasks, (PickingTask)entity);
                        break;
                    case "PickingTaskBatchDetail":
                        DeleteFromDatabase(ObjectContext.PickingTaskBatchDetails, (PickingTaskBatchDetail)entity);
                        break;
                    case "PickingTaskDetail":
                        DeleteFromDatabase(ObjectContext.PickingTaskDetails, (PickingTaskDetail)entity);
                        break;
                    case "PlannedPriceApp":
                        DeleteFromDatabase(ObjectContext.PlannedPriceApps, (PlannedPriceApp)entity);
                        break;
                    case "PlannedPriceAppDetail":
                        DeleteFromDatabase(ObjectContext.PlannedPriceAppDetails, (PlannedPriceAppDetail)entity);
                        break;
                    case "PlanPriceCategory":
                        DeleteFromDatabase(ObjectContext.PlanPriceCategories, (PlanPriceCategory)entity);
                        break;
                    case "PostSaleClaims_Tmp":
                        DeleteFromDatabase(ObjectContext.PostSaleClaims_Tmp, (PostSaleClaims_Tmp)entity);
                        break;
                    case "PQIReport":
                        DeleteFromDatabase(ObjectContext.PQIReports, (PQIReport)entity);
                        break;
                    case "PreInspectionOrder":
                        DeleteFromDatabase(ObjectContext.PreInspectionOrders, (PreInspectionOrder)entity);
                        break;
                    case "PreOrder":
                        DeleteFromDatabase(ObjectContext.PreOrders, (PreOrder)entity);
                        break;
                    case "PreSaleCheckOrder":
                        DeleteFromDatabase(ObjectContext.PreSaleCheckOrders, (PreSaleCheckOrder)entity);
                        break;
                    case "PreSaleItem":
                        DeleteFromDatabase(ObjectContext.PreSaleItems, (PreSaleItem)entity);
                        break;
                    case "PreSalesCheckDetail":
                        DeleteFromDatabase(ObjectContext.PreSalesCheckDetails, (PreSalesCheckDetail)entity);
                        break;
                    case "PreSalesCheckFeeStandard":
                        DeleteFromDatabase(ObjectContext.PreSalesCheckFeeStandards, (PreSalesCheckFeeStandard)entity);
                        break;
                    case "ProductCategoryView":
                        DeleteFromDatabase(ObjectContext.ProductCategoryViews, (ProductCategoryView)entity);
                        break;
                    case "ProductView":
                        DeleteFromDatabase(ObjectContext.ProductViews, (ProductView)entity);
                        break;
                    case "PurchaseNoSettleTable":
                        DeleteFromDatabase(ObjectContext.PurchaseNoSettleTables, (PurchaseNoSettleTable)entity);
                        break;
                    case "PurchaseOrderFinishedDetail":
                        DeleteFromDatabase(ObjectContext.PurchaseOrderFinishedDetails, (PurchaseOrderFinishedDetail)entity);
                        break;
                    case "PurchaseOrderUnFinishDetail":
                        DeleteFromDatabase(ObjectContext.PurchaseOrderUnFinishDetails, (PurchaseOrderUnFinishDetail)entity);
                        break;
                    case "PurchasePersonSupplierLink":
                        DeleteFromDatabase(ObjectContext.PurchasePersonSupplierLinks, (PurchasePersonSupplierLink)entity);
                        break;
                    case "PurchasePricingChangeHi":
                        DeleteFromDatabase(ObjectContext.PurchasePricingChangeHis, (PurchasePricingChangeHi)entity);
                        break;
                    case "PurchaseRtnSettleInvoiceRel":
                        DeleteFromDatabase(ObjectContext.PurchaseRtnSettleInvoiceRels, (PurchaseRtnSettleInvoiceRel)entity);
                        break;
                    case "PurchaseSettleInvoiceRel":
                        DeleteFromDatabase(ObjectContext.PurchaseSettleInvoiceRels, (PurchaseSettleInvoiceRel)entity);
                        break;
                    case "PurchaseSettleInvoiceResource":
                        DeleteFromDatabase(ObjectContext.PurchaseSettleInvoiceResources, (PurchaseSettleInvoiceResource)entity);
                        break;
                    case "PurDistributionDealerLog":
                        DeleteFromDatabase(ObjectContext.PurDistributionDealerLogs, (PurDistributionDealerLog)entity);
                        break;
                    case "QTSDataQuery":
                        DeleteFromDatabase(ObjectContext.QTSDataQueries, (QTSDataQuery)entity);
                        break;
                    case "QTSDataQueryForDGN":
                        DeleteFromDatabase(ObjectContext.QTSDataQueryForDGNs, (QTSDataQueryForDGN)entity);
                        break;
                    case "QTSDataQueryForFD":
                        DeleteFromDatabase(ObjectContext.QTSDataQueryForFDs, (QTSDataQueryForFD)entity);
                        break;
                    case "QTSDataQueryForNFGC":
                        DeleteFromDatabase(ObjectContext.QTSDataQueryForNFGCs, (QTSDataQueryForNFGC)entity);
                        break;
                    case "QTSDataQueryForQXC":
                        DeleteFromDatabase(ObjectContext.QTSDataQueryForQXCs, (QTSDataQueryForQXC)entity);
                        break;
                    case "QTSDataQueryForSD":
                        DeleteFromDatabase(ObjectContext.QTSDataQueryForSDs, (QTSDataQueryForSD)entity);
                        break;
                    case "QTSDataQueryForSP":
                        DeleteFromDatabase(ObjectContext.QTSDataQueryForSPs, (QTSDataQueryForSP)entity);
                        break;
                    case "Region":
                        DeleteFromDatabase(ObjectContext.Regions, (Region)entity);
                        break;
                    case "RegionMarketDptRelation":
                        DeleteFromDatabase(ObjectContext.RegionMarketDptRelations, (RegionMarketDptRelation)entity);
                        break;
                    case "RegionPersonnelRelation":
                        DeleteFromDatabase(ObjectContext.RegionPersonnelRelations, (RegionPersonnelRelation)entity);
                        break;
                    case "RepairClaimBillQuery":
                        DeleteFromDatabase(ObjectContext.RepairClaimBillQueries, (RepairClaimBillQuery)entity);
                        break;
                    case "RepairClaimBillWithSupplier":
                        DeleteFromDatabase(ObjectContext.RepairClaimBillWithSuppliers, (RepairClaimBillWithSupplier)entity);
                        break;
                    case "RepairItemLaborHourView":
                        DeleteFromDatabase(ObjectContext.RepairItemLaborHourViews, (RepairItemLaborHourView)entity);
                        break;
                    case "RepairItemView":
                        DeleteFromDatabase(ObjectContext.RepairItemViews, (RepairItemView)entity);
                        break;
                    case "RepairOrderQuery":
                        DeleteFromDatabase(ObjectContext.RepairOrderQueries, (RepairOrderQuery)entity);
                        break;
                    case "ReportDownloadMsg":
                        DeleteFromDatabase(ObjectContext.ReportDownloadMsgs, (ReportDownloadMsg)entity);
                        break;
                    case "ReserveFactorMasterOrder":
                        DeleteFromDatabase(ObjectContext.ReserveFactorMasterOrders, (ReserveFactorMasterOrder)entity);
                        break;
                    case "ReserveFactorOrderDetail":
                        DeleteFromDatabase(ObjectContext.ReserveFactorOrderDetails, (ReserveFactorOrderDetail)entity);
                        break;
                    case "ResponsibleDetail":
                        DeleteFromDatabase(ObjectContext.ResponsibleDetails, (ResponsibleDetail)entity);
                        break;
                    case "ResponsibleMember":
                        DeleteFromDatabase(ObjectContext.ResponsibleMembers, (ResponsibleMember)entity);
                        break;
                    case "ResponsibleUnit":
                        DeleteFromDatabase(ObjectContext.ResponsibleUnits, (ResponsibleUnit)entity);
                        break;
                    case "ResponsibleUnitAffiProduct":
                        DeleteFromDatabase(ObjectContext.ResponsibleUnitAffiProducts, (ResponsibleUnitAffiProduct)entity);
                        break;
                    case "ResponsibleUnitBranch":
                        DeleteFromDatabase(ObjectContext.ResponsibleUnitBranches, (ResponsibleUnitBranch)entity);
                        break;
                    case "ResponsibleUnitProductDetail":
                        DeleteFromDatabase(ObjectContext.ResponsibleUnitProductDetails, (ResponsibleUnitProductDetail)entity);
                        break;
                    case "ResRelationship":
                        DeleteFromDatabase(ObjectContext.ResRelationships, (ResRelationship)entity);
                        break;
                    case "Retailer_Delivery":
                        DeleteFromDatabase(ObjectContext.Retailer_Delivery, (Retailer_Delivery)entity);
                        break;
                    case "Retailer_DeliveryDetail":
                        DeleteFromDatabase(ObjectContext.Retailer_DeliveryDetail, (Retailer_DeliveryDetail)entity);
                        break;
                    case "Retailer_ServiceApply":
                        DeleteFromDatabase(ObjectContext.Retailer_ServiceApply, (Retailer_ServiceApply)entity);
                        break;
                    case "Retailer_ServiceApplyDetail":
                        DeleteFromDatabase(ObjectContext.Retailer_ServiceApplyDetail, (Retailer_ServiceApplyDetail)entity);
                        break;
                    case "RetailOrderCustomer":
                        DeleteFromDatabase(ObjectContext.RetailOrderCustomers, (RetailOrderCustomer)entity);
                        break;
                    case "RetainedCustomer":
                        DeleteFromDatabase(ObjectContext.RetainedCustomers, (RetainedCustomer)entity);
                        break;
                    case "RetainedCustomerExtended":
                        DeleteFromDatabase(ObjectContext.RetainedCustomerExtendeds, (RetainedCustomerExtended)entity);
                        break;
                    case "ReturnFreightDisposal":
                        DeleteFromDatabase(ObjectContext.ReturnFreightDisposals, (ReturnFreightDisposal)entity);
                        break;
                    case "ReturnVisitQuest":
                        DeleteFromDatabase(ObjectContext.ReturnVisitQuests, (ReturnVisitQuest)entity);
                        break;
                    case "ReturnVisitQuestDetail":
                        DeleteFromDatabase(ObjectContext.ReturnVisitQuestDetails, (ReturnVisitQuestDetail)entity);
                        break;
                    case "SaleNoSettleTable":
                        DeleteFromDatabase(ObjectContext.SaleNoSettleTables, (SaleNoSettleTable)entity);
                        break;
                    case "SalesCenterstrategy":
                        DeleteFromDatabase(ObjectContext.SalesCenterstrategies, (SalesCenterstrategy)entity);
                        break;
                    case "SalesInvoice_tmp":
                        DeleteFromDatabase(ObjectContext.SalesInvoice_tmp, (SalesInvoice_tmp)entity);
                        break;
                    case "SalesOrderDailyAvg":
                        DeleteFromDatabase(ObjectContext.SalesOrderDailyAvgs, (SalesOrderDailyAvg)entity);
                        break;
                    case "SalesOrderRecommendForce":
                        DeleteFromDatabase(ObjectContext.SalesOrderRecommendForces, (SalesOrderRecommendForce)entity);
                        break;
                    case "SalesOrderRecommendWeekly":
                        DeleteFromDatabase(ObjectContext.SalesOrderRecommendWeeklies, (SalesOrderRecommendWeekly)entity);
                        break;
                    case "SalesRegion":
                        DeleteFromDatabase(ObjectContext.SalesRegions, (SalesRegion)entity);
                        break;
                    case "SalesRtnSettleInvoiceRel":
                        DeleteFromDatabase(ObjectContext.SalesRtnSettleInvoiceRels, (SalesRtnSettleInvoiceRel)entity);
                        break;
                    case "SalesSettleInvoiceResource":
                        DeleteFromDatabase(ObjectContext.SalesSettleInvoiceResources, (SalesSettleInvoiceResource)entity);
                        break;
                    case "SalesUnit":
                        DeleteFromDatabase(ObjectContext.SalesUnits, (SalesUnit)entity);
                        break;
                    case "SalesUnitAffiPersonnel":
                        DeleteFromDatabase(ObjectContext.SalesUnitAffiPersonnels, (SalesUnitAffiPersonnel)entity);
                        break;
                    case "SalesUnitAffiWarehouse":
                        DeleteFromDatabase(ObjectContext.SalesUnitAffiWarehouses, (SalesUnitAffiWarehouse)entity);
                        break;
                    case "SalesUnitAffiWhouseHistory":
                        DeleteFromDatabase(ObjectContext.SalesUnitAffiWhouseHistories, (SalesUnitAffiWhouseHistory)entity);
                        break;
                    case "SAP_YX_InvoiceverAccountInfo":
                        DeleteFromDatabase(ObjectContext.SAP_YX_InvoiceverAccountInfo, (SAP_YX_InvoiceverAccountInfo)entity);
                        break;
                    case "SAPInvoiceInfo":
                        DeleteFromDatabase(ObjectContext.SAPInvoiceInfoes, (SAPInvoiceInfo)entity);
                        break;
                    case "SAPInvoiceInfo_FD":
                        DeleteFromDatabase(ObjectContext.SAPInvoiceInfo_FD, (SAPInvoiceInfo_FD)entity);
                        break;
                    case "ScheduleExportState":
                        DeleteFromDatabase(ObjectContext.ScheduleExportStates, (ScheduleExportState)entity);
                        break;
                    case "SecondClassStationPlan":
                        DeleteFromDatabase(ObjectContext.SecondClassStationPlans, (SecondClassStationPlan)entity);
                        break;
                    case "SecondClassStationPlanDetail":
                        DeleteFromDatabase(ObjectContext.SecondClassStationPlanDetails, (SecondClassStationPlanDetail)entity);
                        break;
                    case "SendWaybill":
                        DeleteFromDatabase(ObjectContext.SendWaybills, (SendWaybill)entity);
                        break;
                    case "ServiceDealerRebate":
                        DeleteFromDatabase(ObjectContext.ServiceDealerRebates, (ServiceDealerRebate)entity);
                        break;
                    case "ServiceLevelBySafeCf":
                        DeleteFromDatabase(ObjectContext.ServiceLevelBySafeCfs, (ServiceLevelBySafeCf)entity);
                        break;
                    case "ServiceProductLineView":
                        DeleteFromDatabase(ObjectContext.ServiceProductLineViews, (ServiceProductLineView)entity);
                        break;
                    case "ServiceProductLineViewAllDB":
                        DeleteFromDatabase(ObjectContext.ServiceProductLineViewAllDBs, (ServiceProductLineViewAllDB)entity);
                        break;
                    case "ServProdLineAffiProduct":
                        DeleteFromDatabase(ObjectContext.ServProdLineAffiProducts, (ServProdLineAffiProduct)entity);
                        break;
                    case "SettleBillInvoiceLink":
                        DeleteFromDatabase(ObjectContext.SettleBillInvoiceLinks, (SettleBillInvoiceLink)entity);
                        break;
                    case "SettlementAutomaticTaskSet":
                        DeleteFromDatabase(ObjectContext.SettlementAutomaticTaskSets, (SettlementAutomaticTaskSet)entity);
                        break;
                    case "SIHCenterPer":
                        DeleteFromDatabase(ObjectContext.SIHCenterPers, (SIHCenterPer)entity);
                        break;
                    case "SIHCreditInfo":
                        DeleteFromDatabase(ObjectContext.SIHCreditInfoes, (SIHCreditInfo)entity);
                        break;
                    case "SIHCreditInfoDetail":
                        DeleteFromDatabase(ObjectContext.SIHCreditInfoDetails, (SIHCreditInfoDetail)entity);
                        break;
                    case "SIHDailySalesAverage":
                        DeleteFromDatabase(ObjectContext.SIHDailySalesAverages, (SIHDailySalesAverage)entity);
                        break;
                    case "SIHRecommendPlan":
                        DeleteFromDatabase(ObjectContext.SIHRecommendPlans, (SIHRecommendPlan)entity);
                        break;
                    case "SIHSmartOrderBase":
                        DeleteFromDatabase(ObjectContext.SIHSmartOrderBases, (SIHSmartOrderBase)entity);
                        break;
                    case "SmartCompany":
                        DeleteFromDatabase(ObjectContext.SmartCompanies, (SmartCompany)entity);
                        break;
                    case "SmartOrderCalendar":
                        DeleteFromDatabase(ObjectContext.SmartOrderCalendars, (SmartOrderCalendar)entity);
                        break;
                    case "SmartProcessMethod":
                        DeleteFromDatabase(ObjectContext.SmartProcessMethods, (SmartProcessMethod)entity);
                        break;
                    case "SparePart":
                        DeleteFromDatabase(ObjectContext.SpareParts, (SparePart)entity);
                        break;
                    case "SparePartHistory":
                        DeleteFromDatabase(ObjectContext.SparePartHistories, (SparePartHistory)entity);
                        break;
                    case "SpecialPriceChangeList":
                        DeleteFromDatabase(ObjectContext.SpecialPriceChangeLists, (SpecialPriceChangeList)entity);
                        break;
                    case "SpecialTreatyPriceChange":
                        DeleteFromDatabase(ObjectContext.SpecialTreatyPriceChanges, (SpecialTreatyPriceChange)entity);
                        break;
                    case "SsUsedPartsDisposalBill":
                        DeleteFromDatabase(ObjectContext.SsUsedPartsDisposalBills, (SsUsedPartsDisposalBill)entity);
                        break;
                    case "SsUsedPartsDisposalDetail":
                        DeleteFromDatabase(ObjectContext.SsUsedPartsDisposalDetails, (SsUsedPartsDisposalDetail)entity);
                        break;
                    case "SsUsedPartsStorage":
                        DeleteFromDatabase(ObjectContext.SsUsedPartsStorages, (SsUsedPartsStorage)entity);
                        break;
                    case "SubChannelRelation":
                        DeleteFromDatabase(ObjectContext.SubChannelRelations, (SubChannelRelation)entity);
                        break;
                    case "SubDealer":
                        DeleteFromDatabase(ObjectContext.SubDealers, (SubDealer)entity);
                        break;
                    case "Summary":
                        DeleteFromDatabase(ObjectContext.Summaries, (Summary)entity);
                        break;
                    case "SupplierAccount":
                        DeleteFromDatabase(ObjectContext.SupplierAccounts, (SupplierAccount)entity);
                        break;
                    case "SupplierCollectionDetail":
                        DeleteFromDatabase(ObjectContext.SupplierCollectionDetails, (SupplierCollectionDetail)entity);
                        break;
                    case "SupplierExpenseAdjustBill":
                        DeleteFromDatabase(ObjectContext.SupplierExpenseAdjustBills, (SupplierExpenseAdjustBill)entity);
                        break;
                    case "SupplierInformation":
                        DeleteFromDatabase(ObjectContext.SupplierInformations, (SupplierInformation)entity);
                        break;
                    case "SupplierOpenAccountApp":
                        DeleteFromDatabase(ObjectContext.SupplierOpenAccountApps, (SupplierOpenAccountApp)entity);
                        break;
                    case "SupplierPlanArrear":
                        DeleteFromDatabase(ObjectContext.SupplierPlanArrears, (SupplierPlanArrear)entity);
                        break;
                    case "SupplierPreAppLoanDetail":
                        DeleteFromDatabase(ObjectContext.SupplierPreAppLoanDetails, (SupplierPreAppLoanDetail)entity);
                        break;
                    case "SupplierPreApprovedLoan":
                        DeleteFromDatabase(ObjectContext.SupplierPreApprovedLoans, (SupplierPreApprovedLoan)entity);
                        break;
                    case "SupplierShippingDetail":
                        DeleteFromDatabase(ObjectContext.SupplierShippingDetails, (SupplierShippingDetail)entity);
                        break;
                    case "SupplierShippingOrder":
                        DeleteFromDatabase(ObjectContext.SupplierShippingOrders, (SupplierShippingOrder)entity);
                        break;
                    case "SupplierSpareOutBoud":
                        DeleteFromDatabase(ObjectContext.SupplierSpareOutBouds, (SupplierSpareOutBoud)entity);
                        break;
                    case "SupplierStoreInboundDetail":
                        DeleteFromDatabase(ObjectContext.SupplierStoreInboundDetails, (SupplierStoreInboundDetail)entity);
                        break;
                    case "SupplierStorePlanDetail":
                        DeleteFromDatabase(ObjectContext.SupplierStorePlanDetails, (SupplierStorePlanDetail)entity);
                        break;
                    case "SupplierStorePlanOrder":
                        DeleteFromDatabase(ObjectContext.SupplierStorePlanOrders, (SupplierStorePlanOrder)entity);
                        break;
                    case "SupplierTraceCode":
                        DeleteFromDatabase(ObjectContext.SupplierTraceCodes, (SupplierTraceCode)entity);
                        break;
                    case "SupplierTraceCodeDetail":
                        DeleteFromDatabase(ObjectContext.SupplierTraceCodeDetails, (SupplierTraceCodeDetail)entity);
                        break;
                    case "SupplierTraceDetail":
                        DeleteFromDatabase(ObjectContext.SupplierTraceDetails, (SupplierTraceDetail)entity);
                        break;
                    case "SupplierTransferBill":
                        DeleteFromDatabase(ObjectContext.SupplierTransferBills, (SupplierTransferBill)entity);
                        break;
                    case "TemPurchaseOrder":
                        DeleteFromDatabase(ObjectContext.TemPurchaseOrders, (TemPurchaseOrder)entity);
                        break;
                    case "TemPurchaseOrderDetail":
                        DeleteFromDatabase(ObjectContext.TemPurchaseOrderDetails, (TemPurchaseOrderDetail)entity);
                        break;
                    case "TemPurchasePlanOrder":
                        DeleteFromDatabase(ObjectContext.TemPurchasePlanOrders, (TemPurchasePlanOrder)entity);
                        break;
                    case "TemPurchasePlanOrderDetail":
                        DeleteFromDatabase(ObjectContext.TemPurchasePlanOrderDetails, (TemPurchasePlanOrderDetail)entity);
                        break;
                    case "TemShippingOrderDetail":
                        DeleteFromDatabase(ObjectContext.TemShippingOrderDetails, (TemShippingOrderDetail)entity);
                        break;
                    case "TemSupplierShippingOrder":
                        DeleteFromDatabase(ObjectContext.TemSupplierShippingOrders, (TemSupplierShippingOrder)entity);
                        break;
                    case "TiledRegion":
                        DeleteFromDatabase(ObjectContext.TiledRegions, (TiledRegion)entity);
                        break;
                    case "TraceTempType":
                        DeleteFromDatabase(ObjectContext.TraceTempTypes, (TraceTempType)entity);
                        break;
                    case "TransactionInterfaceList":
                        DeleteFromDatabase(ObjectContext.TransactionInterfaceLists, (TransactionInterfaceList)entity);
                        break;
                    case "TransactionInterfaceLog":
                        DeleteFromDatabase(ObjectContext.TransactionInterfaceLogs, (TransactionInterfaceLog)entity);
                        break;
                    case "UsedPartsDisposalBill":
                        DeleteFromDatabase(ObjectContext.UsedPartsDisposalBills, (UsedPartsDisposalBill)entity);
                        break;
                    case "UsedPartsDisposalDetail":
                        DeleteFromDatabase(ObjectContext.UsedPartsDisposalDetails, (UsedPartsDisposalDetail)entity);
                        break;
                    case "UsedPartsDistanceInfor":
                        DeleteFromDatabase(ObjectContext.UsedPartsDistanceInfors, (UsedPartsDistanceInfor)entity);
                        break;
                    case "UsedPartsInboundDetail":
                        DeleteFromDatabase(ObjectContext.UsedPartsInboundDetails, (UsedPartsInboundDetail)entity);
                        break;
                    case "UsedPartsInboundOrder":
                        DeleteFromDatabase(ObjectContext.UsedPartsInboundOrders, (UsedPartsInboundOrder)entity);
                        break;
                    case "UsedPartsLoanBill":
                        DeleteFromDatabase(ObjectContext.UsedPartsLoanBills, (UsedPartsLoanBill)entity);
                        break;
                    case "UsedPartsLoanDetail":
                        DeleteFromDatabase(ObjectContext.UsedPartsLoanDetails, (UsedPartsLoanDetail)entity);
                        break;
                    case "UsedPartsLogisticLossBill":
                        DeleteFromDatabase(ObjectContext.UsedPartsLogisticLossBills, (UsedPartsLogisticLossBill)entity);
                        break;
                    case "UsedPartsLogisticLossDetail":
                        DeleteFromDatabase(ObjectContext.UsedPartsLogisticLossDetails, (UsedPartsLogisticLossDetail)entity);
                        break;
                    case "UsedPartsOutboundDetail":
                        DeleteFromDatabase(ObjectContext.UsedPartsOutboundDetails, (UsedPartsOutboundDetail)entity);
                        break;
                    case "UsedPartsOutboundOrder":
                        DeleteFromDatabase(ObjectContext.UsedPartsOutboundOrders, (UsedPartsOutboundOrder)entity);
                        break;
                    case "UsedPartsRefitBill":
                        DeleteFromDatabase(ObjectContext.UsedPartsRefitBills, (UsedPartsRefitBill)entity);
                        break;
                    case "UsedPartsRefitReqDetail":
                        DeleteFromDatabase(ObjectContext.UsedPartsRefitReqDetails, (UsedPartsRefitReqDetail)entity);
                        break;
                    case "UsedPartsRefitReturnDetail":
                        DeleteFromDatabase(ObjectContext.UsedPartsRefitReturnDetails, (UsedPartsRefitReturnDetail)entity);
                        break;
                    case "UsedPartsReturnDetail":
                        DeleteFromDatabase(ObjectContext.UsedPartsReturnDetails, (UsedPartsReturnDetail)entity);
                        break;
                    case "UsedPartsReturnOrder":
                        DeleteFromDatabase(ObjectContext.UsedPartsReturnOrders, (UsedPartsReturnOrder)entity);
                        break;
                    case "UsedPartsReturnPolicyHistory":
                        DeleteFromDatabase(ObjectContext.UsedPartsReturnPolicyHistories, (UsedPartsReturnPolicyHistory)entity);
                        break;
                    case "UsedPartsShiftDetail":
                        DeleteFromDatabase(ObjectContext.UsedPartsShiftDetails, (UsedPartsShiftDetail)entity);
                        break;
                    case "UsedPartsShiftOrder":
                        DeleteFromDatabase(ObjectContext.UsedPartsShiftOrders, (UsedPartsShiftOrder)entity);
                        break;
                    case "UsedPartsShippingDetail":
                        DeleteFromDatabase(ObjectContext.UsedPartsShippingDetails, (UsedPartsShippingDetail)entity);
                        break;
                    case "UsedPartsShippingOrder":
                        DeleteFromDatabase(ObjectContext.UsedPartsShippingOrders, (UsedPartsShippingOrder)entity);
                        break;
                    case "UsedPartsStock":
                        DeleteFromDatabase(ObjectContext.UsedPartsStocks, (UsedPartsStock)entity);
                        break;
                    case "UsedPartsStockView":
                        DeleteFromDatabase(ObjectContext.UsedPartsStockViews, (UsedPartsStockView)entity);
                        break;
                    case "UsedPartsTransferDetail":
                        DeleteFromDatabase(ObjectContext.UsedPartsTransferDetails, (UsedPartsTransferDetail)entity);
                        break;
                    case "UsedPartsTransferOrder":
                        DeleteFromDatabase(ObjectContext.UsedPartsTransferOrders, (UsedPartsTransferOrder)entity);
                        break;
                    case "UsedPartsWarehouse":
                        DeleteFromDatabase(ObjectContext.UsedPartsWarehouses, (UsedPartsWarehouse)entity);
                        break;
                    case "UsedPartsWarehouseArea":
                        DeleteFromDatabase(ObjectContext.UsedPartsWarehouseAreas, (UsedPartsWarehouseArea)entity);
                        break;
                    case "UsedPartsWarehouseManager":
                        DeleteFromDatabase(ObjectContext.UsedPartsWarehouseManagers, (UsedPartsWarehouseManager)entity);
                        break;
                    case "UsedPartsWarehouseStaff":
                        DeleteFromDatabase(ObjectContext.UsedPartsWarehouseStaffs, (UsedPartsWarehouseStaff)entity);
                        break;
                    case "VehicleCategoryAffiProduct":
                        DeleteFromDatabase(ObjectContext.VehicleCategoryAffiProducts, (VehicleCategoryAffiProduct)entity);
                        break;
                    case "VehicleModelAffiProduct":
                        DeleteFromDatabase(ObjectContext.VehicleModelAffiProducts, (VehicleModelAffiProduct)entity);
                        break;
                    case "VeriCodeEffectTime":
                        DeleteFromDatabase(ObjectContext.VeriCodeEffectTimes, (VeriCodeEffectTime)entity);
                        break;
                    case "VersionInfo":
                        DeleteFromDatabase(ObjectContext.VersionInfoes, (VersionInfo)entity);
                        break;
                    case "View_ErrorTable_SAP_YX":
                        DeleteFromDatabase(ObjectContext.View_ErrorTable_SAP_YX, (View_ErrorTable_SAP_YX)entity);
                        break;
                    case "ViewSYNCWMSINOUTLOGINFO":
                        DeleteFromDatabase(ObjectContext.ViewSYNCWMSINOUTLOGINFOes, (ViewSYNCWMSINOUTLOGINFO)entity);
                        break;
                    case "Warehouse":
                        DeleteFromDatabase(ObjectContext.Warehouses, (Warehouse)entity);
                        break;
                    case "WarehouseArea":
                        DeleteFromDatabase(ObjectContext.WarehouseAreas, (WarehouseArea)entity);
                        break;
                    case "WarehouseAreaCategory":
                        DeleteFromDatabase(ObjectContext.WarehouseAreaCategories, (WarehouseAreaCategory)entity);
                        break;
                    case "WarehouseAreaHistory":
                        DeleteFromDatabase(ObjectContext.WarehouseAreaHistories, (WarehouseAreaHistory)entity);
                        break;
                    case "WarehouseAreaManager":
                        DeleteFromDatabase(ObjectContext.WarehouseAreaManagers, (WarehouseAreaManager)entity);
                        break;
                    case "WarehouseAreaWithPartsStock":
                        DeleteFromDatabase(ObjectContext.WarehouseAreaWithPartsStocks, (WarehouseAreaWithPartsStock)entity);
                        break;
                    case "WarehouseCostChangeBill":
                        DeleteFromDatabase(ObjectContext.WarehouseCostChangeBills, (WarehouseCostChangeBill)entity);
                        break;
                    case "WarehouseCostChangeDetail":
                        DeleteFromDatabase(ObjectContext.WarehouseCostChangeDetails, (WarehouseCostChangeDetail)entity);
                        break;
                    case "WarehouseOperator":
                        DeleteFromDatabase(ObjectContext.WarehouseOperators, (WarehouseOperator)entity);
                        break;
                    case "WarehouseSequence":
                        DeleteFromDatabase(ObjectContext.WarehouseSequences, (WarehouseSequence)entity);
                        break;
                    case "WarehousingInspection":
                        DeleteFromDatabase(ObjectContext.WarehousingInspections, (WarehousingInspection)entity);
                        break;
                    case "WmsCongelationStockView":
                        DeleteFromDatabase(ObjectContext.WmsCongelationStockViews, (WmsCongelationStockView)entity);
                        break;
                    case "WorkGruopPersonNumber":
                        DeleteFromDatabase(ObjectContext.WorkGruopPersonNumbers, (WorkGruopPersonNumber)entity);
                        break;
                }
            }
        }
        #region
        public IQueryable<ABCSetting> GetABCSettings() {
            return ObjectContext.ABCSettings.OrderBy(e => e.Id);
        }

        public IQueryable<ABCStrategy> GetABCStrategies() {
            return ObjectContext.ABCStrategies.OrderBy(e => e.Id);
        }

        public IQueryable<ABCTypeSafeDay> GetABCTypeSafeDays() {
            return ObjectContext.ABCTypeSafeDays.OrderBy(e => e.Id);
        }

        public IQueryable<ACCESSINFO_TMP> GetACCESSINFO_TMP() {
            return ObjectContext.ACCESSINFO_TMP.OrderBy(e => e.Id);
        }

        public IQueryable<AccountGroup> GetAccountGroups() {
            return ObjectContext.AccountGroups.OrderBy(e => e.Id);
        }

        public IQueryable<AccountPayableHistoryDetail> GetAccountPayableHistoryDetails() {
            return ObjectContext.AccountPayableHistoryDetails.OrderBy(e => e.Id);
        }

        public IQueryable<AccountPeriod> GetAccountPeriods() {
            return ObjectContext.AccountPeriods.OrderBy(e => e.Id);
        }

        public IQueryable<AccurateTrace> GetAccurateTraces() {
            return ObjectContext.AccurateTraces.OrderBy(e => e.Id);
        }

        public IQueryable<Agency> GetAgencies() {
            return ObjectContext.Agencies.OrderBy(e => e.Id);
        }

        public IQueryable<AgencyAffiBranch> GetAgencyAffiBranches() {
            return ObjectContext.AgencyAffiBranches.OrderBy(e => e.Id);
        }

        public IQueryable<AgencyDealerRelation> GetAgencyDealerRelations() {
            return ObjectContext.AgencyDealerRelations.OrderBy(e => e.Id);
        }

        public IQueryable<AgencyDealerRelationHistory> GetAgencyDealerRelationHistories() {
            return ObjectContext.AgencyDealerRelationHistories.OrderBy(e => e.Id);
        }

        public IQueryable<AgencyDifferenceBackBill> GetAgencyDifferenceBackBills() {
            return ObjectContext.AgencyDifferenceBackBills.OrderBy(e => e.Id);
        }

        public IQueryable<AgencyLogisticCompany> GetAgencyLogisticCompanies() {
            return ObjectContext.AgencyLogisticCompanies.OrderBy(e => e.Id);
        }

        public IQueryable<AgencyPartsOutboundBill> GetAgencyPartsOutboundBills() {
            return ObjectContext.AgencyPartsOutboundBills.OrderBy(e => e.Id);
        }

        public IQueryable<AgencyPartsOutboundPlan> GetAgencyPartsOutboundPlans() {
            return ObjectContext.AgencyPartsOutboundPlans.OrderBy(e => e.Id);
        }

        public IQueryable<AgencyPartsShippingOrder> GetAgencyPartsShippingOrders() {
            return ObjectContext.AgencyPartsShippingOrders.OrderBy(e => e.Id);
        }

        public IQueryable<AgencyPartsShippingOrderRef> GetAgencyPartsShippingOrderRefs() {
            return ObjectContext.AgencyPartsShippingOrderRefs.OrderBy(e => e.Id);
        }

        public IQueryable<AgencyRetailerList> GetAgencyRetailerLists() {
            return ObjectContext.AgencyRetailerLists.OrderBy(e => e.Id);
        }

        public IQueryable<AgencyRetailerOrder> GetAgencyRetailerOrders() {
            return ObjectContext.AgencyRetailerOrders.OrderBy(e => e.Id);
        }

        public IQueryable<AgencyStockQryFrmOrder> GetAgencyStockQryFrmOrders() {
            return ObjectContext.AgencyStockQryFrmOrders.OrderBy(e => e.AgencyCode);
        }

        public IQueryable<APartsOutboundBillDetail> GetAPartsOutboundBillDetails() {
            return ObjectContext.APartsOutboundBillDetails.OrderBy(e => e.Id);
        }

        public IQueryable<APartsOutboundPlanDetail> GetAPartsOutboundPlanDetails() {
            return ObjectContext.APartsOutboundPlanDetails.OrderBy(e => e.Id);
        }

        public IQueryable<APartsShippingOrderDetail> GetAPartsShippingOrderDetails() {
            return ObjectContext.APartsShippingOrderDetails.OrderBy(e => e.Id);
        }

        public IQueryable<ApplicationType> GetApplicationTypes() {
            return ObjectContext.ApplicationTypes.OrderBy(e => e.Id);
        }

        public IQueryable<AppointDealerDtl> GetAppointDealerDtls() {
            return ObjectContext.AppointDealerDtls.OrderBy(e => e.Id);
        }

        public IQueryable<AppointFaultReasonDtl> GetAppointFaultReasonDtls() {
            return ObjectContext.AppointFaultReasonDtls.OrderBy(e => e.Id);
        }

        public IQueryable<AppointSupplierDtl> GetAppointSupplierDtls() {
            return ObjectContext.AppointSupplierDtls.OrderBy(e => e.Id);
        }

        public IQueryable<AssemblyPartRequisitionLink> GetAssemblyPartRequisitionLinks() {
            return ObjectContext.AssemblyPartRequisitionLinks.OrderBy(e => e.Id);
        }

        public IQueryable<AuthenticationType> GetAuthenticationTypes() {
            return ObjectContext.AuthenticationTypes.OrderBy(e => e.Id);
        }

        public IQueryable<AutoSalesOrderProcess> GetAutoSalesOrderProcesses() {
            return ObjectContext.AutoSalesOrderProcesses.OrderBy(e => e.Id);
        }

        public IQueryable<AutoTaskExecutResult> GetAutoTaskExecutResults() {
            return ObjectContext.AutoTaskExecutResults.OrderBy(e => e.Id);
        }

        public IQueryable<BankAccount> GetBankAccounts() {
            return ObjectContext.BankAccounts.OrderBy(e => e.Id);
        }

        public IQueryable<BonusPointsOrder> GetBonusPointsOrders() {
            return ObjectContext.BonusPointsOrders.OrderBy(e => e.Id);
        }

        public IQueryable<BonusPointsOrderList> GetBonusPointsOrderLists() {
            return ObjectContext.BonusPointsOrderLists.OrderBy(e => e.Id);
        }

        public IQueryable<BonusPointsSummary> GetBonusPointsSummaries() {
            return ObjectContext.BonusPointsSummaries.OrderBy(e => e.Id);
        }

        public IQueryable<BonusPointsSummaryList> GetBonusPointsSummaryLists() {
            return ObjectContext.BonusPointsSummaryLists.OrderBy(e => e.Id);
        }

        public IQueryable<BorrowBill> GetBorrowBills() {
            return ObjectContext.BorrowBills.OrderBy(e => e.Id);
        }

        public IQueryable<BorrowBillDetail> GetBorrowBillDetails() {
            return ObjectContext.BorrowBillDetails.OrderBy(e => e.Id);
        }

        public IQueryable<BottomStock> GetBottomStocks() {
            return ObjectContext.BottomStocks.OrderBy(e => e.Id);
        }

        public IQueryable<BottomStockColVersion> GetBottomStockColVersions() {
            return ObjectContext.BottomStockColVersions.OrderBy(e => e.Id);
        }

        public IQueryable<BottomStockForceReserveSub> GetBottomStockForceReserveSubs() {
            return ObjectContext.BottomStockForceReserveSubs.OrderBy(e => e.Id);
        }

        public IQueryable<BottomStockForceReserveType> GetBottomStockForceReserveTypes() {
            return ObjectContext.BottomStockForceReserveTypes.OrderBy(e => e.Id);
        }

        public IQueryable<BottomStockSettleTable> GetBottomStockSettleTables() {
            return ObjectContext.BottomStockSettleTables.OrderBy(e => e.Id);
        }

        public IQueryable<BottomStockSubVersion> GetBottomStockSubVersions() {
            return ObjectContext.BottomStockSubVersions.OrderBy(e => e.Id);
        }

        public IQueryable<BoxUpTask> GetBoxUpTasks() {
            return ObjectContext.BoxUpTasks.OrderBy(e => e.Id);
        }

        public IQueryable<BoxUpTaskDetail> GetBoxUpTaskDetails() {
            return ObjectContext.BoxUpTaskDetails.OrderBy(e => e.Id);
        }

        public IQueryable<Branch> GetBranches() {
            return ObjectContext.Branches.OrderBy(e => e.Id);
        }

        public IQueryable<BranchDateBaseRel> GetBranchDateBaseRels() {
            return ObjectContext.BranchDateBaseRels.OrderBy(e => e.Id);
        }

        public IQueryable<Branchstrategy> GetBranchstrategies() {
            return ObjectContext.Branchstrategies.OrderBy(e => e.Id);
        }

        public IQueryable<BranchSupplierRelation> GetBranchSupplierRelations() {
            return ObjectContext.BranchSupplierRelations.OrderBy(e => e.Id);
        }

        public IQueryable<BrandGradeMessage> GetBrandGradeMessages() {
            return ObjectContext.BrandGradeMessages.OrderBy(e => e.Id);
        }

        public IQueryable<CAReconciliation> GetCAReconciliations() {
            return ObjectContext.CAReconciliations.OrderBy(e => e.Id);
        }

        public IQueryable<CAReconciliationList> GetCAReconciliationLists() {
            return ObjectContext.CAReconciliationLists.OrderBy(e => e.Id);
        }

        public IQueryable<CenterABCBasi> GetCenterABCBasis() {
            return ObjectContext.CenterABCBasis.OrderBy(e => e.Id);
        }

        public IQueryable<CenterAccumulateDailyExp> GetCenterAccumulateDailyExps() {
            return ObjectContext.CenterAccumulateDailyExps.OrderBy(e => e.Id);
        }

        public IQueryable<CenterApproveBill> GetCenterApproveBills() {
            return ObjectContext.CenterApproveBills.OrderBy(e => e.Id);
        }

        public IQueryable<CenterAuthorizedProvince> GetCenterAuthorizedProvinces() {
            return ObjectContext.CenterAuthorizedProvinces.OrderBy(e => e.Id);
        }

        public IQueryable<CenterQuarterRebate> GetCenterQuarterRebates() {
            return ObjectContext.CenterQuarterRebates.OrderBy(e => e.Id);
        }

        public IQueryable<CenterRebateSummary> GetCenterRebateSummaries() {
            return ObjectContext.CenterRebateSummaries.OrderBy(e => e.Id);
        }

        public IQueryable<CenterSaleReturnStrategy> GetCenterSaleReturnStrategies() {
            return ObjectContext.CenterSaleReturnStrategies.OrderBy(e => e.Id);
        }

        public IQueryable<CenterSaleSettle> GetCenterSaleSettles() {
            return ObjectContext.CenterSaleSettles.OrderBy(e => e.Id);
        }

        public IQueryable<ChannelCapability> GetChannelCapabilities() {
            return ObjectContext.ChannelCapabilities.OrderBy(e => e.Id);
        }

        public IQueryable<ChannelPlan> GetChannelPlans() {
            return ObjectContext.ChannelPlans.OrderBy(e => e.Id);
        }

        public IQueryable<ChannelPlanDetail> GetChannelPlanDetails() {
            return ObjectContext.ChannelPlanDetails.OrderBy(e => e.Id);
        }

        public IQueryable<ChannelPlanHistory> GetChannelPlanHistories() {
            return ObjectContext.ChannelPlanHistories.OrderBy(e => e.Id);
        }

        public IQueryable<ChannelPlanHistoryDetail> GetChannelPlanHistoryDetails() {
            return ObjectContext.ChannelPlanHistoryDetails.OrderBy(e => e.Id);
        }

        public IQueryable<ChassisInformation> GetChassisInformations() {
            return ObjectContext.ChassisInformations.OrderBy(e => e.Id);
        }

        public IQueryable<ClaimApproverStrategy> GetClaimApproverStrategies() {
            return ObjectContext.ClaimApproverStrategies.OrderBy(e => e.Id);
        }

        public IQueryable<ClaimSettleBillDealerInvoice> GetClaimSettleBillDealerInvoices() {
            return ObjectContext.ClaimSettleBillDealerInvoices.OrderBy(e => e.Id);
        }

        public IQueryable<CodeTemplateSerial> GetCodeTemplateSerials() {
            return ObjectContext.CodeTemplateSerials.OrderBy(e => e.Id);
        }

        public IQueryable<CombinedPart> GetCombinedParts() {
            return ObjectContext.CombinedParts.OrderBy(e => e.Id);
        }

        public IQueryable<Company> GetCompanies() {
            return ObjectContext.Companies.OrderBy(e => e.Id);
        }

        public IQueryable<company_log> Getcompany_log() {
            return ObjectContext.company_log.OrderBy(e => e.id);
        }

        public IQueryable<CompanyAddress> GetCompanyAddresses() {
            return ObjectContext.CompanyAddresses.OrderBy(e => e.Id);
        }

        public IQueryable<CompanyDetail> GetCompanyDetails() {
            return ObjectContext.CompanyDetails.OrderBy(e => e.Id);
        }

        public IQueryable<CompanyInvoiceInfo> GetCompanyInvoiceInfoes() {
            return ObjectContext.CompanyInvoiceInfoes.OrderBy(e => e.Id);
        }

        public IQueryable<CompanyLoginPicture> GetCompanyLoginPictures() {
            return ObjectContext.CompanyLoginPictures.OrderBy(e => e.Id);
        }

        public IQueryable<CompanyTransferOrder> GetCompanyTransferOrders() {
            return ObjectContext.CompanyTransferOrders.OrderBy(e => e.Id);
        }

        public IQueryable<CompanyTransferOrderDetail> GetCompanyTransferOrderDetails() {
            return ObjectContext.CompanyTransferOrderDetails.OrderBy(e => e.Id);
        }

        public IQueryable<Countersignature> GetCountersignatures() {
            return ObjectContext.Countersignatures.OrderBy(e => e.Id);
        }

        public IQueryable<CountersignatureDetail> GetCountersignatureDetails() {
            return ObjectContext.CountersignatureDetails.OrderBy(e => e.Id);
        }

        public IQueryable<CredenceApplication> GetCredenceApplications() {
            return ObjectContext.CredenceApplications.OrderBy(e => e.Id);
        }

        public IQueryable<CrossSalesOrder> GetCrossSalesOrders() {
            return ObjectContext.CrossSalesOrders.OrderBy(e => e.Id);
        }

        public IQueryable<CrossSalesOrderDetail> GetCrossSalesOrderDetails() {
            return ObjectContext.CrossSalesOrderDetails.OrderBy(e => e.Id);
        }

        public IQueryable<Customer> GetCustomers() {
            return ObjectContext.Customers.OrderBy(e => e.Id);
        }

        public IQueryable<CustomerAccount> GetCustomerAccounts() {
            return ObjectContext.CustomerAccounts.OrderBy(e => e.Id);
        }

        public IQueryable<CustomerAccountHisDetail> GetCustomerAccountHisDetails() {
            return ObjectContext.CustomerAccountHisDetails.OrderBy(e => e.Id);
        }

        public IQueryable<CustomerAccountHistory> GetCustomerAccountHistories() {
            return ObjectContext.CustomerAccountHistories.OrderBy(e => e.Id);
        }

        public IQueryable<CustomerDirectSpareList> GetCustomerDirectSpareLists() {
            return ObjectContext.CustomerDirectSpareLists.OrderBy(e => e.Id);
        }

        public IQueryable<CustomerInformation> GetCustomerInformations() {
            return ObjectContext.CustomerInformations.OrderBy(e => e.Id);
        }

        public IQueryable<CustomerOpenAccountApp> GetCustomerOpenAccountApps() {
            return ObjectContext.CustomerOpenAccountApps.OrderBy(e => e.Id);
        }

        public IQueryable<CustomerOrderPriceGrade> GetCustomerOrderPriceGrades() {
            return ObjectContext.CustomerOrderPriceGrades.OrderBy(e => e.Id);
        }

        public IQueryable<CustomerOrderPriceGradeHistory> GetCustomerOrderPriceGradeHistories() {
            return ObjectContext.CustomerOrderPriceGradeHistories.OrderBy(e => e.Id);
        }

        public IQueryable<CustomerSupplyInitialFeeSet> GetCustomerSupplyInitialFeeSets() {
            return ObjectContext.CustomerSupplyInitialFeeSets.OrderBy(e => e.Id);
        }

        public IQueryable<CustomerTransferBill> GetCustomerTransferBills() {
            return ObjectContext.CustomerTransferBills.OrderBy(e => e.Id);
        }

        public IQueryable<CustomerTransferBill_Sync> GetCustomerTransferBill_Sync() {
            return ObjectContext.CustomerTransferBill_Sync.OrderBy(e => e.Id);
        }

        public IQueryable<DailySalesWeight> GetDailySalesWeights() {
            return ObjectContext.DailySalesWeights.OrderBy(e => e.Id);
        }

        public IQueryable<DataBaseLink> GetDataBaseLinks() {
            return ObjectContext.DataBaseLinks.OrderBy(e => e.Id);
        }

        public IQueryable<DataBaseLinkBranch> GetDataBaseLinkBranches() {
            return ObjectContext.DataBaseLinkBranches.OrderBy(e => e.Id);
        }

        public IQueryable<Dealer> GetDealers() {
            return ObjectContext.Dealers.OrderBy(e => e.Id);
        }

        public IQueryable<DealerBusinessPermit> GetDealerBusinessPermits() {
            return ObjectContext.DealerBusinessPermits.OrderBy(e => e.Id);
        }

        public IQueryable<DealerBusinessPermitHistory> GetDealerBusinessPermitHistories() {
            return ObjectContext.DealerBusinessPermitHistories.OrderBy(e => e.Id);
        }

        public IQueryable<DealerClaimSparePart> GetDealerClaimSpareParts() {
            return ObjectContext.DealerClaimSpareParts.OrderBy(e => e.Id);
        }

        public IQueryable<DealerFormat> GetDealerFormats() {
            return ObjectContext.DealerFormats.OrderBy(e => e.Id);
        }

        public IQueryable<DealerGradeInfo> GetDealerGradeInfoes() {
            return ObjectContext.DealerGradeInfoes.OrderBy(e => e.Id);
        }

        public IQueryable<DealerHistory> GetDealerHistories() {
            return ObjectContext.DealerHistories.OrderBy(e => e.Id);
        }

        public IQueryable<DealerInvoice> GetDealerInvoices() {
            return ObjectContext.DealerInvoices.OrderBy(e => e.Id);
        }

        public IQueryable<DealerKeyEmployee> GetDealerKeyEmployees() {
            return ObjectContext.DealerKeyEmployees.OrderBy(e => e.Id);
        }

        public IQueryable<DealerKeyEmployeeHistory> GetDealerKeyEmployeeHistories() {
            return ObjectContext.DealerKeyEmployeeHistories.OrderBy(e => e.Id);
        }

        public IQueryable<DealerKeyPosition> GetDealerKeyPositions() {
            return ObjectContext.DealerKeyPositions.OrderBy(e => e.Id);
        }

        public IQueryable<DealerMarketDptRelation> GetDealerMarketDptRelations() {
            return ObjectContext.DealerMarketDptRelations.OrderBy(e => e.Id);
        }

        public IQueryable<DealerMobileNumberHistory> GetDealerMobileNumberHistories() {
            return ObjectContext.DealerMobileNumberHistories.OrderBy(e => e.Id);
        }

        public IQueryable<DealerMobileNumberList> GetDealerMobileNumberLists() {
            return ObjectContext.DealerMobileNumberLists.OrderBy(e => e.Id);
        }

        public IQueryable<DealerPartsInventoryBill> GetDealerPartsInventoryBills() {
            return ObjectContext.DealerPartsInventoryBills.OrderBy(e => e.Id);
        }

        public IQueryable<DealerPartsInventoryDetail> GetDealerPartsInventoryDetails() {
            return ObjectContext.DealerPartsInventoryDetails.OrderBy(e => e.Id);
        }

        public IQueryable<DealerPartsRetailOrder> GetDealerPartsRetailOrders() {
            return ObjectContext.DealerPartsRetailOrders.OrderBy(e => e.Id);
        }

        public IQueryable<DealerPartsSalesPrice> GetDealerPartsSalesPrices() {
            return ObjectContext.DealerPartsSalesPrices.OrderBy(e => e.Id);
        }

        public IQueryable<DealerPartsSalesPriceHistory> GetDealerPartsSalesPriceHistories() {
            return ObjectContext.DealerPartsSalesPriceHistories.OrderBy(e => e.Id);
        }

        public IQueryable<DealerPartsSalesReturnBill> GetDealerPartsSalesReturnBills() {
            return ObjectContext.DealerPartsSalesReturnBills.OrderBy(e => e.Id);
        }

        public IQueryable<DealerPartsStock> GetDealerPartsStocks() {
            return ObjectContext.DealerPartsStocks.OrderBy(e => e.Id);
        }

        public IQueryable<DealerPartsStockOutInRecord> GetDealerPartsStockOutInRecords() {
            return ObjectContext.DealerPartsStockOutInRecords.OrderBy(e => e.PartsCode);
        }

        public IQueryable<DealerPartsStockQueryView> GetDealerPartsStockQueryViews() {
            return ObjectContext.DealerPartsStockQueryViews.OrderBy(e => e.BranchCode);
        }

        public IQueryable<DealerPartsTransferOrder> GetDealerPartsTransferOrders() {
            return ObjectContext.DealerPartsTransferOrders.OrderBy(e => e.Id);
        }

        public IQueryable<DealerPartsTransOrderDetail> GetDealerPartsTransOrderDetails() {
            return ObjectContext.DealerPartsTransOrderDetails.OrderBy(e => e.Id);
        }

        public IQueryable<DealerPerTrainAut> GetDealerPerTrainAuts() {
            return ObjectContext.DealerPerTrainAuts.OrderBy(e => e.Id);
        }

        public IQueryable<DealerRetailOrderDetail> GetDealerRetailOrderDetails() {
            return ObjectContext.DealerRetailOrderDetails.OrderBy(e => e.id);
        }

        public IQueryable<DealerRetailReturnBillDetail> GetDealerRetailReturnBillDetails() {
            return ObjectContext.DealerRetailReturnBillDetails.OrderBy(e => e.id);
        }

        public IQueryable<DealerServiceExt> GetDealerServiceExts() {
            return ObjectContext.DealerServiceExts.OrderBy(e => e.Id);
        }

        public IQueryable<DealerServiceExtHistory> GetDealerServiceExtHistories() {
            return ObjectContext.DealerServiceExtHistories.OrderBy(e => e.Id);
        }

        public IQueryable<DealerServiceInfo> GetDealerServiceInfoes() {
            return ObjectContext.DealerServiceInfoes.OrderBy(e => e.Id);
        }

        public IQueryable<DealerServiceInfoHistory> GetDealerServiceInfoHistories() {
            return ObjectContext.DealerServiceInfoHistories.OrderBy(e => e.Id);
        }

        public IQueryable<DeferredDiscountType> GetDeferredDiscountTypes() {
            return ObjectContext.DeferredDiscountTypes.OrderBy(e => e.Id);
        }

        public IQueryable<DepartmentBrandRelation> GetDepartmentBrandRelations() {
            return ObjectContext.DepartmentBrandRelations.OrderBy(e => e.Id);
        }

        public IQueryable<DepartmentInformation> GetDepartmentInformations() {
            return ObjectContext.DepartmentInformations.OrderBy(e => e.Id);
        }

        public IQueryable<EcommerceFreightDisposal> GetEcommerceFreightDisposals() {
            return ObjectContext.EcommerceFreightDisposals.OrderBy(e => e.Id);
        }

        public IQueryable<EngineModelview> GetEngineModelviews() {
            return ObjectContext.EngineModelviews.OrderBy(e => e.BranchId);
        }

        public IQueryable<EnterprisePartsCost> GetEnterprisePartsCosts() {
            return ObjectContext.EnterprisePartsCosts.OrderBy(e => e.Id);
        }

        public IQueryable<ExportCustomerInfo> GetExportCustomerInfoes() {
            return ObjectContext.ExportCustomerInfoes.OrderBy(e => e.Id);
        }

        public IQueryable<Express> GetExpresses() {
            return ObjectContext.Expresses.OrderBy(e => e.Id);
        }

        public IQueryable<ExpressToLogistic> GetExpressToLogistics() {
            return ObjectContext.ExpressToLogistics.OrderBy(e => e.Id);
        }

        public IQueryable<ExtendedInfoTemplate> GetExtendedInfoTemplates() {
            return ObjectContext.ExtendedInfoTemplates.OrderBy(e => e.Id);
        }

        public IQueryable<ExtendedSparePartList> GetExtendedSparePartLists() {
            return ObjectContext.ExtendedSparePartLists.OrderBy(e => e.Id);
        }

        public IQueryable<ExtendedWarrantyOrder> GetExtendedWarrantyOrders() {
            return ObjectContext.ExtendedWarrantyOrders.OrderBy(e => e.Id);
        }

        public IQueryable<ExtendedWarrantyOrderList> GetExtendedWarrantyOrderLists() {
            return ObjectContext.ExtendedWarrantyOrderLists.OrderBy(e => e.Id);
        }

        public IQueryable<ExtendedWarrantyProduct> GetExtendedWarrantyProducts() {
            return ObjectContext.ExtendedWarrantyProducts.OrderBy(e => e.Id);
        }

        public IQueryable<FactoryPurchacePrice_Tmp> GetFactoryPurchacePrice_Tmp() {
            return ObjectContext.FactoryPurchacePrice_Tmp.OrderBy(e => e.Id);
        }

        public IQueryable<FaultyPartsSupplierAssembly> GetFaultyPartsSupplierAssemblies() {
            return ObjectContext.FaultyPartsSupplierAssemblies.OrderBy(e => e.Id);
        }

        public IQueryable<FDWarrantyMessage> GetFDWarrantyMessages() {
            return ObjectContext.FDWarrantyMessages.OrderBy(e => e.Id);
        }

        public IQueryable<FinancialSnapshotSet> GetFinancialSnapshotSets() {
            return ObjectContext.FinancialSnapshotSets.OrderBy(e => e.Id);
        }

        public IQueryable<ForceReserveBill> GetForceReserveBills() {
            return ObjectContext.ForceReserveBills.OrderBy(e => e.Id);
        }

        public IQueryable<ForceReserveBillDetail> GetForceReserveBillDetails() {
            return ObjectContext.ForceReserveBillDetails.OrderBy(e => e.Id);
        }

        public IQueryable<FullStockTask> GetFullStockTasks() {
            return ObjectContext.FullStockTasks.OrderBy(e => e.Id);
        }

        public IQueryable<FundMonthlySettleBill> GetFundMonthlySettleBills() {
            return ObjectContext.FundMonthlySettleBills.OrderBy(e => e.Id);
        }

        public IQueryable<FundMonthlySettleBillDetail> GetFundMonthlySettleBillDetails() {
            return ObjectContext.FundMonthlySettleBillDetails.OrderBy(e => e.Id);
        }

        public IQueryable<GetAllPartsPlannedPrice> GetGetAllPartsPlannedPrices() {
            return ObjectContext.GetAllPartsPlannedPrices.OrderBy(e => e.PartsSalesCategoryName);
        }

        public IQueryable<GetAllPartsPurchasePricing> GetGetAllPartsPurchasePricings() {
            return ObjectContext.GetAllPartsPurchasePricings.OrderBy(e => e.PartsSalesCategoryName);
        }

        public IQueryable<GetAllPartsSalesPrice> GetGetAllPartsSalesPrices() {
            return ObjectContext.GetAllPartsSalesPrices.OrderBy(e => e.PartsSalesCategoryName);
        }

        public IQueryable<GoldenTaxClassifyMsg> GetGoldenTaxClassifyMsgs() {
            return ObjectContext.GoldenTaxClassifyMsgs.OrderBy(e => e.Id);
        }

        public IQueryable<GradeCoefficient> GetGradeCoefficients() {
            return ObjectContext.GradeCoefficients.OrderBy(e => e.Id);
        }

        public IQueryable<GradeCoefficientView> GetGradeCoefficientViews() {
            return ObjectContext.GradeCoefficientViews.OrderBy(e => e.Grade);
        }

        public IQueryable<HauDistanceInfor> GetHauDistanceInfors() {
            return ObjectContext.HauDistanceInfors.OrderBy(e => e.Id);
        }

        public IQueryable<InOutSettleSummary> GetInOutSettleSummaries() {
            return ObjectContext.InOutSettleSummaries.OrderBy(e => e.Id);
        }

        public IQueryable<InOutWorkPerHour> GetInOutWorkPerHours() {
            return ObjectContext.InOutWorkPerHours.OrderBy(e => e.Id);
        }

        public IQueryable<IntegralSettleDictate> GetIntegralSettleDictates() {
            return ObjectContext.IntegralSettleDictates.OrderBy(e => e.Id);
        }

        public IQueryable<IntelligentOrderMonthly> GetIntelligentOrderMonthlies() {
            return ObjectContext.IntelligentOrderMonthlies.OrderBy(e => e.Id);
        }

        public IQueryable<IntelligentOrderMonthlyBase> GetIntelligentOrderMonthlyBases() {
            return ObjectContext.IntelligentOrderMonthlyBases.OrderBy(e => e.Id);
        }

        public IQueryable<IntelligentOrderWeekly> GetIntelligentOrderWeeklies() {
            return ObjectContext.IntelligentOrderWeeklies.OrderBy(e => e.Id);
        }

        public IQueryable<IntelligentOrderWeeklyBase> GetIntelligentOrderWeeklyBases() {
            return ObjectContext.IntelligentOrderWeeklyBases.OrderBy(e => e.Id);
        }

        public IQueryable<InternalAcquisitionBill> GetInternalAcquisitionBills() {
            return ObjectContext.InternalAcquisitionBills.OrderBy(e => e.Id);
        }

        public IQueryable<InternalAcquisitionDetail> GetInternalAcquisitionDetails() {
            return ObjectContext.InternalAcquisitionDetails.OrderBy(e => e.Id);
        }

        public IQueryable<InternalAllocationBill> GetInternalAllocationBills() {
            return ObjectContext.InternalAllocationBills.OrderBy(e => e.Id);
        }

        public IQueryable<InternalAllocationDetail> GetInternalAllocationDetails() {
            return ObjectContext.InternalAllocationDetails.OrderBy(e => e.Id);
        }

        public IQueryable<InteSettleBillInvoiceLink> GetInteSettleBillInvoiceLinks() {
            return ObjectContext.InteSettleBillInvoiceLinks.OrderBy(e => e.Id);
        }

        public IQueryable<InvoiceInformation> GetInvoiceInformations() {
            return ObjectContext.InvoiceInformations.OrderBy(e => e.Id);
        }

        public IQueryable<IvecoPriceChangeApp> GetIvecoPriceChangeApps() {
            return ObjectContext.IvecoPriceChangeApps.OrderBy(e => e.Id);
        }

        public IQueryable<IvecoPriceChangeAppDetail> GetIvecoPriceChangeAppDetails() {
            return ObjectContext.IvecoPriceChangeAppDetails.OrderBy(e => e.Id);
        }

        public IQueryable<IvecoSalesPrice> GetIvecoSalesPrices() {
            return ObjectContext.IvecoSalesPrices.OrderBy(e => e.Id);
        }

        public IQueryable<KeyValueItem> GetKeyValueItems() {
            return ObjectContext.KeyValueItems.OrderBy(e => e.Id);
        }

        public IQueryable<LayerNodeType> GetLayerNodeTypes() {
            return ObjectContext.LayerNodeTypes.OrderBy(e => e.Id);
        }

        public IQueryable<LayerNodeTypeAffiliation> GetLayerNodeTypeAffiliations() {
            return ObjectContext.LayerNodeTypeAffiliations.OrderBy(e => e.Id);
        }

        public IQueryable<LayerStructure> GetLayerStructures() {
            return ObjectContext.LayerStructures.OrderBy(e => e.Id);
        }

        public IQueryable<LayerStructurePurpose> GetLayerStructurePurposes() {
            return ObjectContext.LayerStructurePurposes.OrderBy(e => e.Id);
        }

        public IQueryable<LinkAllocation> GetLinkAllocations() {
            return ObjectContext.LinkAllocations.OrderBy(e => e.Id);
        }

        public IQueryable<LoadingDetail> GetLoadingDetails() {
            return ObjectContext.LoadingDetails.OrderBy(e => e.Id);
        }

        public IQueryable<LocketUnitQuery> GetLocketUnitQueries() {
            return ObjectContext.LocketUnitQueries.OrderBy(e => e.Code);
        }

        public IQueryable<Logistic> GetLogistics() {
            return ObjectContext.Logistics.OrderBy(e => e.Id);
        }

        public IQueryable<LogisticCompany> GetLogisticCompanies() {
            return ObjectContext.LogisticCompanies.OrderBy(e => e.Id);
        }

        public IQueryable<LogisticCompanyServiceRange> GetLogisticCompanyServiceRanges() {
            return ObjectContext.LogisticCompanyServiceRanges.OrderBy(e => e.Id);
        }

        public IQueryable<LogisticsDetail> GetLogisticsDetails() {
            return ObjectContext.LogisticsDetails.OrderBy(e => e.Id);
        }

        public IQueryable<MaintainRedProductMap> GetMaintainRedProductMaps() {
            return ObjectContext.MaintainRedProductMaps.OrderBy(e => e.Id);
        }

        public IQueryable<MalfunctionView> GetMalfunctionViews() {
            return ObjectContext.MalfunctionViews.OrderBy(e => e.Code);
        }

        public IQueryable<ManualScheduling> GetManualSchedulings() {
            return ObjectContext.ManualSchedulings.OrderBy(e => e.Id);
        }

        public IQueryable<MarketABQualityInformation> GetMarketABQualityInformations() {
            return ObjectContext.MarketABQualityInformations.OrderBy(e => e.Id);
        }

        public IQueryable<MarketDptPersonnelRelation> GetMarketDptPersonnelRelations() {
            return ObjectContext.MarketDptPersonnelRelations.OrderBy(e => e.Id);
        }

        public IQueryable<MarketingDepartment> GetMarketingDepartments() {
            return ObjectContext.MarketingDepartments.OrderBy(e => e.Id);
        }

        public IQueryable<MultiLevelApproveConfig> GetMultiLevelApproveConfigs() {
            return ObjectContext.MultiLevelApproveConfigs.OrderBy(e => e.Id);
        }

        public IQueryable<Notification> GetNotifications() {
            return ObjectContext.Notifications.OrderBy(e => e.Id);
        }

        public IQueryable<NotificationLimit> GetNotificationLimits() {
            return ObjectContext.NotificationLimits.OrderBy(e => e.Id);
        }

        public IQueryable<NotificationOpLog> GetNotificationOpLogs() {
            return ObjectContext.NotificationOpLogs.OrderBy(e => e.Id);
        }

        public IQueryable<NotificationReply> GetNotificationReplies() {
            return ObjectContext.NotificationReplies.OrderBy(e => e.id);
        }

        public IQueryable<OrderApproveWeekday> GetOrderApproveWeekdays() {
            return ObjectContext.OrderApproveWeekdays.OrderBy(e => e.Id);
        }

        public IQueryable<OutboundOrder> GetOutboundOrders() {
            return ObjectContext.OutboundOrders.OrderBy(e => e.BranchName);
        }

        public IQueryable<OutofWarrantyPayment> GetOutofWarrantyPayments() {
            return ObjectContext.OutofWarrantyPayments.OrderBy(e => e.Id);
        }

        public IQueryable<OverstockPartsAdjustBill> GetOverstockPartsAdjustBills() {
            return ObjectContext.OverstockPartsAdjustBills.OrderBy(e => e.Id);
        }

        public IQueryable<OverstockPartsAdjustDetail> GetOverstockPartsAdjustDetails() {
            return ObjectContext.OverstockPartsAdjustDetails.OrderBy(e => e.Id);
        }

        public IQueryable<OverstockPartsApp> GetOverstockPartsApps() {
            return ObjectContext.OverstockPartsApps.OrderBy(e => e.Id);
        }

        public IQueryable<OverstockPartsAppDetail> GetOverstockPartsAppDetails() {
            return ObjectContext.OverstockPartsAppDetails.OrderBy(e => e.Id);
        }

        public IQueryable<OverstockPartsInformation> GetOverstockPartsInformations() {
            return ObjectContext.OverstockPartsInformations.OrderBy(e => e.Id);
        }

        public IQueryable<OverstockPartsPlatFormBill> GetOverstockPartsPlatFormBills() {
            return ObjectContext.OverstockPartsPlatFormBills.OrderBy(e => e.Id);
        }

        public IQueryable<OverstockPartsRecommendBill> GetOverstockPartsRecommendBills() {
            return ObjectContext.OverstockPartsRecommendBills.OrderBy(e => e.Id);
        }

        public IQueryable<OverstockTransferOrder> GetOverstockTransferOrders() {
            return ObjectContext.OverstockTransferOrders.OrderBy(e => e.Id);
        }

        public IQueryable<OverstockTransferOrderDetail> GetOverstockTransferOrderDetails() {
            return ObjectContext.OverstockTransferOrderDetails.OrderBy(e => e.Id);
        }

        public IQueryable<PackingTask> GetPackingTasks() {
            return ObjectContext.PackingTasks.OrderBy(e => e.Id);
        }

        public IQueryable<PackingTaskDetail> GetPackingTaskDetails() {
            return ObjectContext.PackingTaskDetails.OrderBy(e => e.Id);
        }

        public IQueryable<PartABCDetail> GetPartABCDetails() {
            return ObjectContext.PartABCDetails.OrderBy(e => e.Id);
        }

        public IQueryable<PartConPurchasePlan> GetPartConPurchasePlans() {
            return ObjectContext.PartConPurchasePlans.OrderBy(e => e.Id);
        }

        public IQueryable<PartDeleaveHistory> GetPartDeleaveHistories() {
            return ObjectContext.PartDeleaveHistories.OrderBy(e => e.Id);
        }

        public IQueryable<PartDeleaveInformation> GetPartDeleaveInformations() {
            return ObjectContext.PartDeleaveInformations.OrderBy(e => e.Id);
        }

        public IQueryable<PartNotConPurchasePlan> GetPartNotConPurchasePlans() {
            return ObjectContext.PartNotConPurchasePlans.OrderBy(e => e.BranchId);
        }

        public IQueryable<PartRequisitionReturnBill> GetPartRequisitionReturnBills() {
            return ObjectContext.PartRequisitionReturnBills.OrderBy(e => e.Id);
        }

        public IQueryable<PartSafeStock> GetPartSafeStocks() {
            return ObjectContext.PartSafeStocks.OrderBy(e => e.Id);
        }

        public IQueryable<PartsBatchRecord> GetPartsBatchRecords() {
            return ObjectContext.PartsBatchRecords.OrderBy(e => e.Id);
        }

        public IQueryable<PartsBranch> GetPartsBranches() {
            return ObjectContext.PartsBranches.OrderBy(e => e.Id);
        }

        public IQueryable<PartsBranchHistory> GetPartsBranchHistories() {
            return ObjectContext.PartsBranchHistories.OrderBy(e => e.Id);
        }

        public IQueryable<PartsBranchPackingProp> GetPartsBranchPackingProps() {
            return ObjectContext.PartsBranchPackingProps.OrderBy(e => e.Id);
        }

        public IQueryable<PartsBranchView> GetPartsBranchViews() {
            return ObjectContext.PartsBranchViews.OrderBy(e => e.PartCode);
        }

        public IQueryable<PartsClaimOrderNew> GetPartsClaimOrderNews() {
            return ObjectContext.PartsClaimOrderNews.OrderBy(e => e.Id);
        }

        public IQueryable<PartsClaimPrice> GetPartsClaimPrices() {
            return ObjectContext.PartsClaimPrices.OrderBy(e => e.BranchId);
        }

        public IQueryable<PartsClaimPriceForbw> GetPartsClaimPriceForbws() {
            return ObjectContext.PartsClaimPriceForbws.OrderBy(e => e.BranchId);
        }

        public IQueryable<PartsCostTransferInTransit> GetPartsCostTransferInTransits() {
            return ObjectContext.PartsCostTransferInTransits.OrderBy(e => e.Id);
        }

        public IQueryable<PartsDifferenceBackBill> GetPartsDifferenceBackBills() {
            return ObjectContext.PartsDifferenceBackBills.OrderBy(e => e.Id);
        }

        public IQueryable<PartsDifferenceBackBillDtl> GetPartsDifferenceBackBillDtls() {
            return ObjectContext.PartsDifferenceBackBillDtls.OrderBy(e => e.Id);
        }

        public IQueryable<PartsExchange> GetPartsExchanges() {
            return ObjectContext.PartsExchanges.OrderBy(e => e.Id);
        }

        public IQueryable<PartsExchangeGroup> GetPartsExchangeGroups() {
            return ObjectContext.PartsExchangeGroups.OrderBy(e => e.Id);
        }

        public IQueryable<PartsExchangeHistory> GetPartsExchangeHistories() {
            return ObjectContext.PartsExchangeHistories.OrderBy(e => e.Id);
        }

        public IQueryable<PartsHistoryStock> GetPartsHistoryStocks() {
            return ObjectContext.PartsHistoryStocks.OrderBy(e => e.Id);
        }

        public IQueryable<PartsInboundCheckBill> GetPartsInboundCheckBills() {
            return ObjectContext.PartsInboundCheckBills.OrderBy(e => e.Id);
        }

        public IQueryable<PartsInboundCheckBill_Sync> GetPartsInboundCheckBill_Sync() {
            return ObjectContext.PartsInboundCheckBill_Sync.OrderBy(e => e.Id);
        }

        public IQueryable<PartsInboundCheckBillDetail> GetPartsInboundCheckBillDetails() {
            return ObjectContext.PartsInboundCheckBillDetails.OrderBy(e => e.Id);
        }

        public IQueryable<PartsInboundPackingDetail> GetPartsInboundPackingDetails() {
            return ObjectContext.PartsInboundPackingDetails.OrderBy(e => e.Id);
        }

        public IQueryable<PartsInboundPerformance> GetPartsInboundPerformances() {
            return ObjectContext.PartsInboundPerformances.OrderBy(e => e.Id);
        }

        public IQueryable<PartsInboundPlan> GetPartsInboundPlans() {
            return ObjectContext.PartsInboundPlans.OrderBy(e => e.Id);
        }

        public IQueryable<PartsInboundPlanDetail> GetPartsInboundPlanDetails() {
            return ObjectContext.PartsInboundPlanDetails.OrderBy(e => e.Id);
        }

        public IQueryable<PartsInventoryBill> GetPartsInventoryBills() {
            return ObjectContext.PartsInventoryBills.OrderBy(e => e.Id);
        }

        public IQueryable<PartsInventoryDetail> GetPartsInventoryDetails() {
            return ObjectContext.PartsInventoryDetails.OrderBy(e => e.Id);
        }

        public IQueryable<PartsLockedStock> GetPartsLockedStocks() {
            return ObjectContext.PartsLockedStocks.OrderBy(e => e.Id);
        }

        public IQueryable<PartsLogisticBatch> GetPartsLogisticBatches() {
            return ObjectContext.PartsLogisticBatches.OrderBy(e => e.Id);
        }

        public IQueryable<PartsLogisticBatchBillDetail> GetPartsLogisticBatchBillDetails() {
            return ObjectContext.PartsLogisticBatchBillDetails.OrderBy(e => e.Id);
        }

        public IQueryable<PartsLogisticBatchItemDetail> GetPartsLogisticBatchItemDetails() {
            return ObjectContext.PartsLogisticBatchItemDetails.OrderBy(e => e.Id);
        }

        public IQueryable<PartsManagementCostGrade> GetPartsManagementCostGrades() {
            return ObjectContext.PartsManagementCostGrades.OrderBy(e => e.Id);
        }

        public IQueryable<PartsManagementCostRate> GetPartsManagementCostRates() {
            return ObjectContext.PartsManagementCostRates.OrderBy(e => e.Id);
        }

        public IQueryable<PartsOutboundBill> GetPartsOutboundBills() {
            return ObjectContext.PartsOutboundBills.OrderBy(e => e.Id);
        }

        public IQueryable<PartsOutboundBillDetail> GetPartsOutboundBillDetails() {
            return ObjectContext.PartsOutboundBillDetails.OrderBy(e => e.Id);
        }

        public IQueryable<PartsOutboundPerformance> GetPartsOutboundPerformances() {
            return ObjectContext.PartsOutboundPerformances.OrderBy(e => e.Id);
        }

        public IQueryable<PartsOutboundPlan> GetPartsOutboundPlans() {
            return ObjectContext.PartsOutboundPlans.OrderBy(e => e.Id);
        }

        public IQueryable<PartsOutboundPlanDetail> GetPartsOutboundPlanDetails() {
            return ObjectContext.PartsOutboundPlanDetails.OrderBy(e => e.Id);
        }

        public IQueryable<PartsOuterPurchaseChange> GetPartsOuterPurchaseChanges() {
            return ObjectContext.PartsOuterPurchaseChanges.OrderBy(e => e.Id);
        }

        public IQueryable<PartsOuterPurchaselist> GetPartsOuterPurchaselists() {
            return ObjectContext.PartsOuterPurchaselists.OrderBy(e => e.id);
        }

        public IQueryable<PartsPacking> GetPartsPackings() {
            return ObjectContext.PartsPackings.OrderBy(e => e.Id);
        }

        public IQueryable<PartsPackingPropAppDetail> GetPartsPackingPropAppDetails() {
            return ObjectContext.PartsPackingPropAppDetails.OrderBy(e => e.Id);
        }

        public IQueryable<PartsPackingPropertyApp> GetPartsPackingPropertyApps() {
            return ObjectContext.PartsPackingPropertyApps.OrderBy(e => e.Id);
        }

        public IQueryable<PartsPlannedPrice> GetPartsPlannedPrices() {
            return ObjectContext.PartsPlannedPrices.OrderBy(e => e.Id);
        }

        public IQueryable<PartsPriceIncreaseRate> GetPartsPriceIncreaseRates() {
            return ObjectContext.PartsPriceIncreaseRates.OrderBy(e => e.Id);
        }

        public IQueryable<PartsPriceTimeLimit> GetPartsPriceTimeLimits() {
            return ObjectContext.PartsPriceTimeLimits.OrderBy(e => e.Id);
        }

        public IQueryable<PartsPriorityBill> GetPartsPriorityBills() {
            return ObjectContext.PartsPriorityBills.OrderBy(e => e.Id);
        }

        public IQueryable<PartsPurchaseInfoSummary> GetPartsPurchaseInfoSummaries() {
            return ObjectContext.PartsPurchaseInfoSummaries.OrderBy(e => e.Id);
        }

        public IQueryable<PartsPurchaseOrder> GetPartsPurchaseOrders() {
            return ObjectContext.PartsPurchaseOrders.OrderBy(e => e.Id);
        }

        public IQueryable<PartsPurchaseOrder_Sync> GetPartsPurchaseOrder_Sync() {
            return ObjectContext.PartsPurchaseOrder_Sync.OrderBy(e => e.Id);
        }

        public IQueryable<PartsPurchaseOrderDetail> GetPartsPurchaseOrderDetails() {
            return ObjectContext.PartsPurchaseOrderDetails.OrderBy(e => e.Id);
        }

        public IQueryable<PartsPurchaseOrderType> GetPartsPurchaseOrderTypes() {
            return ObjectContext.PartsPurchaseOrderTypes.OrderBy(e => e.Id);
        }

        public IQueryable<PartsPurchasePlan> GetPartsPurchasePlans() {
            return ObjectContext.PartsPurchasePlans.OrderBy(e => e.Id);
        }

        public IQueryable<PartsPurchasePlan_HW> GetPartsPurchasePlan_HW() {
            return ObjectContext.PartsPurchasePlan_HW.OrderBy(e => e.Id);
        }

        public IQueryable<PartsPurchasePlanDetail> GetPartsPurchasePlanDetails() {
            return ObjectContext.PartsPurchasePlanDetails.OrderBy(e => e.id);
        }

        public IQueryable<PartsPurchasePlanDetail_HW> GetPartsPurchasePlanDetail_HW() {
            return ObjectContext.PartsPurchasePlanDetail_HW.OrderBy(e => e.Id);
        }

        public IQueryable<PartsPurchasePlanTemp> GetPartsPurchasePlanTemps() {
            return ObjectContext.PartsPurchasePlanTemps.OrderBy(e => e.Id);
        }

        public IQueryable<PartsPurchasePricing> GetPartsPurchasePricings() {
            return ObjectContext.PartsPurchasePricings.OrderBy(e => e.Id);
        }

        public IQueryable<PartsPurchasePricingChange> GetPartsPurchasePricingChanges() {
            return ObjectContext.PartsPurchasePricingChanges.OrderBy(e => e.Id);
        }

        public IQueryable<PartsPurchasePricingDetail> GetPartsPurchasePricingDetails() {
            return ObjectContext.PartsPurchasePricingDetails.OrderBy(e => e.Id);
        }

        public IQueryable<PartsPurchaseRtnSettleBill> GetPartsPurchaseRtnSettleBills() {
            return ObjectContext.PartsPurchaseRtnSettleBills.OrderBy(e => e.Id);
        }

        public IQueryable<PartsPurchaseRtnSettleDetail> GetPartsPurchaseRtnSettleDetails() {
            return ObjectContext.PartsPurchaseRtnSettleDetails.OrderBy(e => e.Id);
        }

        public IQueryable<PartsPurchaseRtnSettleRef> GetPartsPurchaseRtnSettleRefs() {
            return ObjectContext.PartsPurchaseRtnSettleRefs.OrderBy(e => e.Id);
        }

        public IQueryable<PartsPurchaseSettleBill> GetPartsPurchaseSettleBills() {
            return ObjectContext.PartsPurchaseSettleBills.OrderBy(e => e.Id);
        }

        public IQueryable<PartsPurchaseSettleDetail> GetPartsPurchaseSettleDetails() {
            return ObjectContext.PartsPurchaseSettleDetails.OrderBy(e => e.Id);
        }

        public IQueryable<PartsPurchaseSettleRef> GetPartsPurchaseSettleRefs() {
            return ObjectContext.PartsPurchaseSettleRefs.OrderBy(e => e.Id);
        }

        public IQueryable<PartsPurReturnOrder> GetPartsPurReturnOrders() {
            return ObjectContext.PartsPurReturnOrders.OrderBy(e => e.Id);
        }

        public IQueryable<PartsPurReturnOrderDetail> GetPartsPurReturnOrderDetails() {
            return ObjectContext.PartsPurReturnOrderDetails.OrderBy(e => e.Id);
        }

        public IQueryable<PartsRebateAccount> GetPartsRebateAccounts() {
            return ObjectContext.PartsRebateAccounts.OrderBy(e => e.Id);
        }

        public IQueryable<PartsRebateApplication> GetPartsRebateApplications() {
            return ObjectContext.PartsRebateApplications.OrderBy(e => e.Id);
        }

        public IQueryable<PartsRebateChangeDetail> GetPartsRebateChangeDetails() {
            return ObjectContext.PartsRebateChangeDetails.OrderBy(e => e.Id);
        }

        public IQueryable<PartsRebateType> GetPartsRebateTypes() {
            return ObjectContext.PartsRebateTypes.OrderBy(e => e.Id);
        }

        public IQueryable<PartsReplacement> GetPartsReplacements() {
            return ObjectContext.PartsReplacements.OrderBy(e => e.Id);
        }

        public IQueryable<PartsReplacementHistory> GetPartsReplacementHistories() {
            return ObjectContext.PartsReplacementHistories.OrderBy(e => e.Id);
        }

        public IQueryable<PartsRequisitionSettleBill> GetPartsRequisitionSettleBills() {
            return ObjectContext.PartsRequisitionSettleBills.OrderBy(e => e.Id);
        }

        public IQueryable<PartsRequisitionSettleDetail> GetPartsRequisitionSettleDetails() {
            return ObjectContext.PartsRequisitionSettleDetails.OrderBy(e => e.Id);
        }

        public IQueryable<PartsRequisitionSettleRef> GetPartsRequisitionSettleRefs() {
            return ObjectContext.PartsRequisitionSettleRefs.OrderBy(e => e.Id);
        }

        public IQueryable<PartsRetailGuidePrice> GetPartsRetailGuidePrices() {
            return ObjectContext.PartsRetailGuidePrices.OrderBy(e => e.Id);
        }

        public IQueryable<PartsRetailGuidePriceHistory> GetPartsRetailGuidePriceHistories() {
            return ObjectContext.PartsRetailGuidePriceHistories.OrderBy(e => e.Id);
        }

        public IQueryable<PartsRetailOrder> GetPartsRetailOrders() {
            return ObjectContext.PartsRetailOrders.OrderBy(e => e.Id);
        }

        public IQueryable<PartsRetailOrderDetail> GetPartsRetailOrderDetails() {
            return ObjectContext.PartsRetailOrderDetails.OrderBy(e => e.Id);
        }

        public IQueryable<PartsRetailReturnBill> GetPartsRetailReturnBills() {
            return ObjectContext.PartsRetailReturnBills.OrderBy(e => e.Id);
        }

        public IQueryable<PartsRetailReturnBillDetail> GetPartsRetailReturnBillDetails() {
            return ObjectContext.PartsRetailReturnBillDetails.OrderBy(e => e.Id);
        }

        public IQueryable<PartsSalePriceIncreaseRate> GetPartsSalePriceIncreaseRates() {
            return ObjectContext.PartsSalePriceIncreaseRates.OrderBy(e => e.Id);
        }

        public IQueryable<PartsSalesCategory> GetPartsSalesCategories() {
            return ObjectContext.PartsSalesCategories.OrderBy(e => e.Id);
        }

        public IQueryable<PartsSalesCategoryView> GetPartsSalesCategoryViews() {
            return ObjectContext.PartsSalesCategoryViews.OrderBy(e => e.BranchCode);
        }

        public IQueryable<PartsSalesOrder> GetPartsSalesOrders() {
            return ObjectContext.PartsSalesOrders.OrderBy(e => e.Id);
        }

        public IQueryable<PartsSalesOrderASAP> GetPartsSalesOrderASAPs() {
            return ObjectContext.PartsSalesOrderASAPs.OrderBy(e => e.Id);
        }

        public IQueryable<PartsSalesOrderDetail> GetPartsSalesOrderDetails() {
            return ObjectContext.PartsSalesOrderDetails.OrderBy(e => e.Id);
        }

        public IQueryable<PartsSalesOrderDetailASAP> GetPartsSalesOrderDetailASAPs() {
            return ObjectContext.PartsSalesOrderDetailASAPs.OrderBy(e => e.Id);
        }

        public IQueryable<PartsSalesOrderProcess> GetPartsSalesOrderProcesses() {
            return ObjectContext.PartsSalesOrderProcesses.OrderBy(e => e.Id);
        }

        public IQueryable<PartsSalesOrderProcessDetail> GetPartsSalesOrderProcessDetails() {
            return ObjectContext.PartsSalesOrderProcessDetails.OrderBy(e => e.Id);
        }

        public IQueryable<PartsSalesOrderType> GetPartsSalesOrderTypes() {
            return ObjectContext.PartsSalesOrderTypes.OrderBy(e => e.Id);
        }

        public IQueryable<PartsSalesPrice> GetPartsSalesPrices() {
            return ObjectContext.PartsSalesPrices.OrderBy(e => e.Id);
        }

        public IQueryable<PartsSalesPriceChange> GetPartsSalesPriceChanges() {
            return ObjectContext.PartsSalesPriceChanges.OrderBy(e => e.Id);
        }

        public IQueryable<PartsSalesPriceChangeDetail> GetPartsSalesPriceChangeDetails() {
            return ObjectContext.PartsSalesPriceChangeDetails.OrderBy(e => e.Id);
        }

        public IQueryable<PartsSalesPriceHistory> GetPartsSalesPriceHistories() {
            return ObjectContext.PartsSalesPriceHistories.OrderBy(e => e.Id);
        }

        public IQueryable<PartsSalesReturnBill> GetPartsSalesReturnBills() {
            return ObjectContext.PartsSalesReturnBills.OrderBy(e => e.Id);
        }

        public IQueryable<PartsSalesReturnBillDetail> GetPartsSalesReturnBillDetails() {
            return ObjectContext.PartsSalesReturnBillDetails.OrderBy(e => e.Id);
        }

        public IQueryable<PartsSalesRtnSettlement> GetPartsSalesRtnSettlements() {
            return ObjectContext.PartsSalesRtnSettlements.OrderBy(e => e.Id);
        }

        public IQueryable<PartsSalesRtnSettlementDetail> GetPartsSalesRtnSettlementDetails() {
            return ObjectContext.PartsSalesRtnSettlementDetails.OrderBy(e => e.Id);
        }

        public IQueryable<PartsSalesRtnSettlementRef> GetPartsSalesRtnSettlementRefs() {
            return ObjectContext.PartsSalesRtnSettlementRefs.OrderBy(e => e.Id);
        }

        public IQueryable<PartsSalesSettlement> GetPartsSalesSettlements() {
            return ObjectContext.PartsSalesSettlements.OrderBy(e => e.Id);
        }

        public IQueryable<PartsSalesSettlementDetail> GetPartsSalesSettlementDetails() {
            return ObjectContext.PartsSalesSettlementDetails.OrderBy(e => e.Id);
        }

        public IQueryable<PartsSalesSettlementRef> GetPartsSalesSettlementRefs() {
            return ObjectContext.PartsSalesSettlementRefs.OrderBy(e => e.Id);
        }

        public IQueryable<PartsSalesWeeklyBase> GetPartsSalesWeeklyBases() {
            return ObjectContext.PartsSalesWeeklyBases.OrderBy(e => e.Id);
        }

        public IQueryable<PartsServiceLevel> GetPartsServiceLevels() {
            return ObjectContext.PartsServiceLevels.OrderBy(e => e.Id);
        }

        public IQueryable<PartsShelvesTask> GetPartsShelvesTasks() {
            return ObjectContext.PartsShelvesTasks.OrderBy(e => e.Id);
        }

        public IQueryable<PartsShiftOrder> GetPartsShiftOrders() {
            return ObjectContext.PartsShiftOrders.OrderBy(e => e.Id);
        }

        public IQueryable<PartsShiftOrderDetail> GetPartsShiftOrderDetails() {
            return ObjectContext.PartsShiftOrderDetails.OrderBy(e => e.Id);
        }

        public IQueryable<PartsShiftSIHDetail> GetPartsShiftSIHDetails() {
            return ObjectContext.PartsShiftSIHDetails.OrderBy(e => e.Id);
        }

        public IQueryable<PartsShippingOrder> GetPartsShippingOrders() {
            return ObjectContext.PartsShippingOrders.OrderBy(e => e.Id);
        }

        public IQueryable<PartsShippingOrderDetail> GetPartsShippingOrderDetails() {
            return ObjectContext.PartsShippingOrderDetails.OrderBy(e => e.Id);
        }

        public IQueryable<PartsShippingOrderRef> GetPartsShippingOrderRefs() {
            return ObjectContext.PartsShippingOrderRefs.OrderBy(e => e.Id);
        }

        public IQueryable<PartsSpecialPriceHistory> GetPartsSpecialPriceHistories() {
            return ObjectContext.PartsSpecialPriceHistories.OrderBy(e => e.Id);
        }

        public IQueryable<PartsSpecialTreatyPrice> GetPartsSpecialTreatyPrices() {
            return ObjectContext.PartsSpecialTreatyPrices.OrderBy(e => e.Id);
        }

        public IQueryable<PartsStandardStockBill> GetPartsStandardStockBills() {
            return ObjectContext.PartsStandardStockBills.OrderBy(e => e.Id);
        }

        public IQueryable<PartsStock> GetPartsStocks() {
            return ObjectContext.PartsStocks.OrderBy(e => e.Id);
        }

        public IQueryable<PartsStockBatchDetail> GetPartsStockBatchDetails() {
            return ObjectContext.PartsStockBatchDetails.OrderBy(e => e.Id);
        }

        public IQueryable<PartsStockCoefficient> GetPartsStockCoefficients() {
            return ObjectContext.PartsStockCoefficients.OrderBy(e => e.Id);
        }

        public IQueryable<PartsStockCoefficientPrice> GetPartsStockCoefficientPrices() {
            return ObjectContext.PartsStockCoefficientPrices.OrderBy(e => e.Id);
        }

        public IQueryable<PartsStockHistory> GetPartsStockHistories() {
            return ObjectContext.PartsStockHistories.OrderBy(e => e.Id);
        }

        public IQueryable<PartsSupplier> GetPartsSuppliers() {
            return ObjectContext.PartsSuppliers.OrderBy(e => e.Id);
        }

        public IQueryable<PartsSupplierCollection> GetPartsSupplierCollections() {
            return ObjectContext.PartsSupplierCollections.OrderBy(e => e.Id);
        }

        public IQueryable<PartsSupplierRelation> GetPartsSupplierRelations() {
            return ObjectContext.PartsSupplierRelations.OrderBy(e => e.Id);
        }

        public IQueryable<PartsSupplierRelationHistory> GetPartsSupplierRelationHistories() {
            return ObjectContext.PartsSupplierRelationHistories.OrderBy(e => e.Id);
        }

        public IQueryable<PartsSupplierStock> GetPartsSupplierStocks() {
            return ObjectContext.PartsSupplierStocks.OrderBy(e => e.Id);
        }

        public IQueryable<PartsSupplierStockHistory> GetPartsSupplierStockHistories() {
            return ObjectContext.PartsSupplierStockHistories.OrderBy(e => e.Id);
        }

        public IQueryable<PartsTransferOrder> GetPartsTransferOrders() {
            return ObjectContext.PartsTransferOrders.OrderBy(e => e.Id);
        }

        public IQueryable<PartsTransferOrderDetail> GetPartsTransferOrderDetails() {
            return ObjectContext.PartsTransferOrderDetails.OrderBy(e => e.Id);
        }

        public IQueryable<PartsWarehousePendingCost> GetPartsWarehousePendingCosts() {
            return ObjectContext.PartsWarehousePendingCosts.OrderBy(e => e.Id);
        }

        public IQueryable<PaymentBeneficiaryList> GetPaymentBeneficiaryLists() {
            return ObjectContext.PaymentBeneficiaryLists.OrderBy(e => e.Id);
        }

        public IQueryable<PaymentBill> GetPaymentBills() {
            return ObjectContext.PaymentBills.OrderBy(e => e.Id);
        }

        public IQueryable<PaymentBill_Sync> GetPaymentBill_Sync() {
            return ObjectContext.PaymentBill_Sync.OrderBy(e => e.Id);
        }

        public IQueryable<PayOutBill> GetPayOutBills() {
            return ObjectContext.PayOutBills.OrderBy(e => e.Id);
        }

        public IQueryable<Personnel> GetPersonnels() {
            return ObjectContext.Personnels.OrderBy(e => e.Id);
        }

        public IQueryable<PersonnelSupplierRelation> GetPersonnelSupplierRelations() {
            return ObjectContext.PersonnelSupplierRelations.OrderBy(e => e.Id);
        }

        public IQueryable<PersonSalesCenterLink> GetPersonSalesCenterLinks() {
            return ObjectContext.PersonSalesCenterLinks.OrderBy(e => e.Id);
        }

        public IQueryable<PersonSubDealer> GetPersonSubDealers() {
            return ObjectContext.PersonSubDealers.OrderBy(e => e.Id);
        }

        public IQueryable<PickingTask> GetPickingTasks() {
            return ObjectContext.PickingTasks.OrderBy(e => e.Id);
        }

        public IQueryable<PickingTaskBatchDetail> GetPickingTaskBatchDetails() {
            return ObjectContext.PickingTaskBatchDetails.OrderBy(e => e.Id);
        }

        public IQueryable<PickingTaskDetail> GetPickingTaskDetails() {
            return ObjectContext.PickingTaskDetails.OrderBy(e => e.Id);
        }

        public IQueryable<PlannedPriceApp> GetPlannedPriceApps() {
            return ObjectContext.PlannedPriceApps.OrderBy(e => e.Id);
        }

        public IQueryable<PlannedPriceAppDetail> GetPlannedPriceAppDetails() {
            return ObjectContext.PlannedPriceAppDetails.OrderBy(e => e.Id);
        }

        public IQueryable<PlanPriceCategory> GetPlanPriceCategories() {
            return ObjectContext.PlanPriceCategories.OrderBy(e => e.Id);
        }

        public IQueryable<PostSaleClaims_Tmp> GetPostSaleClaims_Tmp() {
            return ObjectContext.PostSaleClaims_Tmp.OrderBy(e => e.Id);
        }

        public IQueryable<PQIReport> GetPQIReports() {
            return ObjectContext.PQIReports.OrderBy(e => e.Id);
        }

        public IQueryable<PreInspectionOrder> GetPreInspectionOrders() {
            return ObjectContext.PreInspectionOrders.OrderBy(e => e.Id);
        }

        public IQueryable<PreOrder> GetPreOrders() {
            return ObjectContext.PreOrders.OrderBy(e => e.Id);
        }

        public IQueryable<PreSaleCheckOrder> GetPreSaleCheckOrders() {
            return ObjectContext.PreSaleCheckOrders.OrderBy(e => e.Id);
        }

        public IQueryable<PreSaleItem> GetPreSaleItems() {
            return ObjectContext.PreSaleItems.OrderBy(e => e.Id);
        }

        public IQueryable<PreSalesCheckDetail> GetPreSalesCheckDetails() {
            return ObjectContext.PreSalesCheckDetails.OrderBy(e => e.Id);
        }

        public IQueryable<PreSalesCheckFeeStandard> GetPreSalesCheckFeeStandards() {
            return ObjectContext.PreSalesCheckFeeStandards.OrderBy(e => e.Id);
        }

        public IQueryable<ProductCategoryView> GetProductCategoryViews() {
            return ObjectContext.ProductCategoryViews.OrderBy(e => e.ProductCategoryCode);
        }

        public IQueryable<ProductView> GetProductViews() {
            return ObjectContext.ProductViews.OrderBy(e => e.BrandId);
        }

        public IQueryable<PurchaseNoSettleTable> GetPurchaseNoSettleTables() {
            return ObjectContext.PurchaseNoSettleTables.OrderBy(e => e.Id);
        }

        public IQueryable<PurchaseOrderFinishedDetail> GetPurchaseOrderFinishedDetails() {
            return ObjectContext.PurchaseOrderFinishedDetails.OrderBy(e => e.Id);
        }

        public IQueryable<PurchaseOrderUnFinishDetail> GetPurchaseOrderUnFinishDetails() {
            return ObjectContext.PurchaseOrderUnFinishDetails.OrderBy(e => e.Id);
        }

        public IQueryable<PurchasePersonSupplierLink> GetPurchasePersonSupplierLinks() {
            return ObjectContext.PurchasePersonSupplierLinks.OrderBy(e => e.Id);
        }

        public IQueryable<PurchasePricingChangeHi> GetPurchasePricingChangeHis() {
            return ObjectContext.PurchasePricingChangeHis.OrderBy(e => e.Id);
        }

        public IQueryable<PurchaseRtnSettleInvoiceRel> GetPurchaseRtnSettleInvoiceRels() {
            return ObjectContext.PurchaseRtnSettleInvoiceRels.OrderBy(e => e.Id);
        }

        public IQueryable<PurchaseSettleInvoiceRel> GetPurchaseSettleInvoiceRels() {
            return ObjectContext.PurchaseSettleInvoiceRels.OrderBy(e => e.Id);
        }

        public IQueryable<PurchaseSettleInvoiceResource> GetPurchaseSettleInvoiceResources() {
            return ObjectContext.PurchaseSettleInvoiceResources.OrderBy(e => e.InvoiceId);
        }

        public IQueryable<PurDistributionDealerLog> GetPurDistributionDealerLogs() {
            return ObjectContext.PurDistributionDealerLogs.OrderBy(e => e.Id);
        }

        public IQueryable<QTSDataQuery> GetQTSDataQueries() {
            return ObjectContext.QTSDataQueries.OrderBy(e => e.Patch);
        }

        public IQueryable<QTSDataQueryForDGN> GetQTSDataQueryForDGNs() {
            return ObjectContext.QTSDataQueryForDGNs.OrderBy(e => e.VinCode);
        }

        public IQueryable<QTSDataQueryForFD> GetQTSDataQueryForFDs() {
            return ObjectContext.QTSDataQueryForFDs.OrderBy(e => e.Patch);
        }

        public IQueryable<QTSDataQueryForNFGC> GetQTSDataQueryForNFGCs() {
            return ObjectContext.QTSDataQueryForNFGCs.OrderBy(e => e.Mapid);
        }

        public IQueryable<QTSDataQueryForQXC> GetQTSDataQueryForQXCs() {
            return ObjectContext.QTSDataQueryForQXCs.OrderBy(e => e.Bind_Date);
        }

        public IQueryable<QTSDataQueryForSD> GetQTSDataQueryForSDs() {
            return ObjectContext.QTSDataQueryForSDs.OrderBy(e => e.DetailId);
        }

        public IQueryable<QTSDataQueryForSP> GetQTSDataQueryForSPs() {
            return ObjectContext.QTSDataQueryForSPs.OrderBy(e => e.VinCode);
        }

        public IQueryable<Region> GetRegions() {
            return ObjectContext.Regions.OrderBy(e => e.Id);
        }

        public IQueryable<RegionMarketDptRelation> GetRegionMarketDptRelations() {
            return ObjectContext.RegionMarketDptRelations.OrderBy(e => e.Id);
        }

        public IQueryable<RegionPersonnelRelation> GetRegionPersonnelRelations() {
            return ObjectContext.RegionPersonnelRelations.OrderBy(e => e.Id);
        }

        public IQueryable<RepairClaimBillQuery> GetRepairClaimBillQueries() {
            return ObjectContext.RepairClaimBillQueries.OrderBy(e => e.报修时间);
        }

        public IQueryable<RepairClaimBillWithSupplier> GetRepairClaimBillWithSuppliers() {
            return ObjectContext.RepairClaimBillWithSuppliers.OrderBy(e => e.BranchId);
        }

        public IQueryable<RepairItemLaborHourView> GetRepairItemLaborHourViews() {
            return ObjectContext.RepairItemLaborHourViews.OrderBy(e => e.Code);
        }

        public IQueryable<RepairItemView> GetRepairItemViews() {
            return ObjectContext.RepairItemViews.OrderBy(e => e.RepairItemId);
        }

        public IQueryable<RepairOrderQuery> GetRepairOrderQueries() {
            return ObjectContext.RepairOrderQueries.OrderBy(e => e.报修时间);
        }

        public IQueryable<ReportDownloadMsg> GetReportDownloadMsgs() {
            return ObjectContext.ReportDownloadMsgs.OrderBy(e => e.Id);
        }

        public IQueryable<ReserveFactorMasterOrder> GetReserveFactorMasterOrders() {
            return ObjectContext.ReserveFactorMasterOrders.OrderBy(e => e.Id);
        }

        public IQueryable<ReserveFactorOrderDetail> GetReserveFactorOrderDetails() {
            return ObjectContext.ReserveFactorOrderDetails.OrderBy(e => e.Id);
        }

        public IQueryable<ResponsibleDetail> GetResponsibleDetails() {
            return ObjectContext.ResponsibleDetails.OrderBy(e => e.Id);
        }

        public IQueryable<ResponsibleMember> GetResponsibleMembers() {
            return ObjectContext.ResponsibleMembers.OrderBy(e => e.Id);
        }

        public IQueryable<ResponsibleUnit> GetResponsibleUnits() {
            return ObjectContext.ResponsibleUnits.OrderBy(e => e.Id);
        }

        public IQueryable<ResponsibleUnitAffiProduct> GetResponsibleUnitAffiProducts() {
            return ObjectContext.ResponsibleUnitAffiProducts.OrderBy(e => e.ProductId);
        }

        public IQueryable<ResponsibleUnitBranch> GetResponsibleUnitBranches() {
            return ObjectContext.ResponsibleUnitBranches.OrderBy(e => e.Id);
        }

        public IQueryable<ResponsibleUnitProductDetail> GetResponsibleUnitProductDetails() {
            return ObjectContext.ResponsibleUnitProductDetails.OrderBy(e => e.Id);
        }

        public IQueryable<ResRelationship> GetResRelationships() {
            return ObjectContext.ResRelationships.OrderBy(e => e.Id);
        }

        public IQueryable<Retailer_Delivery> GetRetailer_Delivery() {
            return ObjectContext.Retailer_Delivery.OrderBy(e => e.Id);
        }

        public IQueryable<Retailer_DeliveryDetail> GetRetailer_DeliveryDetail() {
            return ObjectContext.Retailer_DeliveryDetail.OrderBy(e => e.Id);
        }

        public IQueryable<Retailer_ServiceApply> GetRetailer_ServiceApply() {
            return ObjectContext.Retailer_ServiceApply.OrderBy(e => e.Id);
        }

        public IQueryable<Retailer_ServiceApplyDetail> GetRetailer_ServiceApplyDetail() {
            return ObjectContext.Retailer_ServiceApplyDetail.OrderBy(e => e.Id);
        }

        public IQueryable<RetailOrderCustomer> GetRetailOrderCustomers() {
            return ObjectContext.RetailOrderCustomers.OrderBy(e => e.Id);
        }

        public IQueryable<RetainedCustomer> GetRetainedCustomers() {
            return ObjectContext.RetainedCustomers.OrderBy(e => e.Id);
        }

        public IQueryable<RetainedCustomerExtended> GetRetainedCustomerExtendeds() {
            return ObjectContext.RetainedCustomerExtendeds.OrderBy(e => e.Id);
        }

        public IQueryable<ReturnFreightDisposal> GetReturnFreightDisposals() {
            return ObjectContext.ReturnFreightDisposals.OrderBy(e => e.Id);
        }

        public IQueryable<ReturnVisitQuest> GetReturnVisitQuests() {
            return ObjectContext.ReturnVisitQuests.OrderBy(e => e.id);
        }

        public IQueryable<ReturnVisitQuestDetail> GetReturnVisitQuestDetails() {
            return ObjectContext.ReturnVisitQuestDetails.OrderBy(e => e.Id);
        }

        public IQueryable<SaleNoSettleTable> GetSaleNoSettleTables() {
            return ObjectContext.SaleNoSettleTables.OrderBy(e => e.Id);
        }

        public IQueryable<SalesCenterstrategy> GetSalesCenterstrategies() {
            return ObjectContext.SalesCenterstrategies.OrderBy(e => e.Id);
        }

        public IQueryable<SalesInvoice_tmp> GetSalesInvoice_tmp() {
            return ObjectContext.SalesInvoice_tmp.OrderBy(e => e.Id);
        }

        public IQueryable<SalesOrderDailyAvg> GetSalesOrderDailyAvgs() {
            return ObjectContext.SalesOrderDailyAvgs.OrderBy(e => e.Id);
        }

        public IQueryable<SalesOrderRecommendForce> GetSalesOrderRecommendForces() {
            return ObjectContext.SalesOrderRecommendForces.OrderBy(e => e.Id);
        }

        public IQueryable<SalesOrderRecommendWeekly> GetSalesOrderRecommendWeeklies() {
            return ObjectContext.SalesOrderRecommendWeeklies.OrderBy(e => e.Id);
        }

        public IQueryable<SalesRegion> GetSalesRegions() {
            return ObjectContext.SalesRegions.OrderBy(e => e.Id);
        }

        public IQueryable<SalesRtnSettleInvoiceRel> GetSalesRtnSettleInvoiceRels() {
            return ObjectContext.SalesRtnSettleInvoiceRels.OrderBy(e => e.Id);
        }

        public IQueryable<SalesSettleInvoiceResource> GetSalesSettleInvoiceResources() {
            return ObjectContext.SalesSettleInvoiceResources.OrderBy(e => e.InvoiceId);
        }

        public IQueryable<SalesUnit> GetSalesUnits() {
            return ObjectContext.SalesUnits.OrderBy(e => e.Id);
        }

        public IQueryable<SalesUnitAffiPersonnel> GetSalesUnitAffiPersonnels() {
            return ObjectContext.SalesUnitAffiPersonnels.OrderBy(e => e.Id);
        }

        public IQueryable<SalesUnitAffiWarehouse> GetSalesUnitAffiWarehouses() {
            return ObjectContext.SalesUnitAffiWarehouses.OrderBy(e => e.Id);
        }

        public IQueryable<SalesUnitAffiWhouseHistory> GetSalesUnitAffiWhouseHistories() {
            return ObjectContext.SalesUnitAffiWhouseHistories.OrderBy(e => e.Id);
        }

        public IQueryable<SAP_YX_InvoiceverAccountInfo> GetSAP_YX_InvoiceverAccountInfo() {
            return ObjectContext.SAP_YX_InvoiceverAccountInfo.OrderBy(e => e.Id);
        }

        public IQueryable<SAPInvoiceInfo> GetSAPInvoiceInfoes() {
            return ObjectContext.SAPInvoiceInfoes.OrderBy(e => e.Id);
        }

        public IQueryable<SAPInvoiceInfo_FD> GetSAPInvoiceInfo_FD() {
            return ObjectContext.SAPInvoiceInfo_FD.OrderBy(e => e.Id);
        }

        public IQueryable<ScheduleExportState> GetScheduleExportStates() {
            return ObjectContext.ScheduleExportStates.OrderBy(e => e.Id);
        }

        public IQueryable<SecondClassStationPlan> GetSecondClassStationPlans() {
            return ObjectContext.SecondClassStationPlans.OrderBy(e => e.Id);
        }

        public IQueryable<SecondClassStationPlanDetail> GetSecondClassStationPlanDetails() {
            return ObjectContext.SecondClassStationPlanDetails.OrderBy(e => e.Id);
        }

        public IQueryable<SendWaybill> GetSendWaybills() {
            return ObjectContext.SendWaybills.OrderBy(e => e.Code);
        }

        public IQueryable<ServiceDealerRebate> GetServiceDealerRebates() {
            return ObjectContext.ServiceDealerRebates.OrderBy(e => e.Id);
        }

        public IQueryable<ServiceLevelBySafeCf> GetServiceLevelBySafeCfs() {
            return ObjectContext.ServiceLevelBySafeCfs.OrderBy(e => e.Id);
        }

        public IQueryable<ServiceProductLineView> GetServiceProductLineViews() {
            return ObjectContext.ServiceProductLineViews.OrderBy(e => e.ProductLineId);
        }

        public IQueryable<ServiceProductLineViewAllDB> GetServiceProductLineViewAllDBs() {
            return ObjectContext.ServiceProductLineViewAllDBs.OrderBy(e => e.Id);
        }

        public IQueryable<ServProdLineAffiProduct> GetServProdLineAffiProducts() {
            return ObjectContext.ServProdLineAffiProducts.OrderBy(e => e.ProductId);
        }

        public IQueryable<SettleBillInvoiceLink> GetSettleBillInvoiceLinks() {
            return ObjectContext.SettleBillInvoiceLinks.OrderBy(e => e.Id);
        }

        public IQueryable<SettlementAutomaticTaskSet> GetSettlementAutomaticTaskSets() {
            return ObjectContext.SettlementAutomaticTaskSets.OrderBy(e => e.Id);
        }

        public IQueryable<SIHCenterPer> GetSIHCenterPers() {
            return ObjectContext.SIHCenterPers.OrderBy(e => e.Id);
        }

        public IQueryable<SIHCreditInfo> GetSIHCreditInfoes() {
            return ObjectContext.SIHCreditInfoes.OrderBy(e => e.Id);
        }

        public IQueryable<SIHCreditInfoDetail> GetSIHCreditInfoDetails() {
            return ObjectContext.SIHCreditInfoDetails.OrderBy(e => e.Id);
        }

        public IQueryable<SIHDailySalesAverage> GetSIHDailySalesAverages() {
            return ObjectContext.SIHDailySalesAverages.OrderBy(e => e.Id);
        }

        public IQueryable<SIHRecommendPlan> GetSIHRecommendPlans() {
            return ObjectContext.SIHRecommendPlans.OrderBy(e => e.Id);
        }

        public IQueryable<SIHSmartOrderBase> GetSIHSmartOrderBases() {
            return ObjectContext.SIHSmartOrderBases.OrderBy(e => e.Id);
        }

        public IQueryable<SmartCompany> GetSmartCompanies() {
            return ObjectContext.SmartCompanies.OrderBy(e => e.Id);
        }

        public IQueryable<SmartOrderCalendar> GetSmartOrderCalendars() {
            return ObjectContext.SmartOrderCalendars.OrderBy(e => e.Id);
        }

        public IQueryable<SmartProcessMethod> GetSmartProcessMethods() {
            return ObjectContext.SmartProcessMethods.OrderBy(e => e.Id);
        }

        public IQueryable<SparePart> GetSpareParts() {
            return ObjectContext.SpareParts.OrderBy(e => e.Id);
        }

        public IQueryable<SparePartHistory> GetSparePartHistories() {
            return ObjectContext.SparePartHistories.OrderBy(e => e.Id);
        }

        public IQueryable<SpecialPriceChangeList> GetSpecialPriceChangeLists() {
            return ObjectContext.SpecialPriceChangeLists.OrderBy(e => e.Id);
        }

        public IQueryable<SpecialTreatyPriceChange> GetSpecialTreatyPriceChanges() {
            return ObjectContext.SpecialTreatyPriceChanges.OrderBy(e => e.Id);
        }

        public IQueryable<SsUsedPartsDisposalBill> GetSsUsedPartsDisposalBills() {
            return ObjectContext.SsUsedPartsDisposalBills.OrderBy(e => e.Id);
        }

        public IQueryable<SsUsedPartsDisposalDetail> GetSsUsedPartsDisposalDetails() {
            return ObjectContext.SsUsedPartsDisposalDetails.OrderBy(e => e.Id);
        }

        public IQueryable<SsUsedPartsStorage> GetSsUsedPartsStorages() {
            return ObjectContext.SsUsedPartsStorages.OrderBy(e => e.Id);
        }

        public IQueryable<SubChannelRelation> GetSubChannelRelations() {
            return ObjectContext.SubChannelRelations.OrderBy(e => e.Id);
        }

        public IQueryable<SubDealer> GetSubDealers() {
            return ObjectContext.SubDealers.OrderBy(e => e.Id);
        }

        public IQueryable<Summary> GetSummaries() {
            return ObjectContext.Summaries.OrderBy(e => e.Id);
        }

        public IQueryable<SupplierAccount> GetSupplierAccounts() {
            return ObjectContext.SupplierAccounts.OrderBy(e => e.Id);
        }

        public IQueryable<SupplierCollectionDetail> GetSupplierCollectionDetails() {
            return ObjectContext.SupplierCollectionDetails.OrderBy(e => e.Id);
        }

        public IQueryable<SupplierExpenseAdjustBill> GetSupplierExpenseAdjustBills() {
            return ObjectContext.SupplierExpenseAdjustBills.OrderBy(e => e.Id);
        }

        public IQueryable<SupplierInformation> GetSupplierInformations() {
            return ObjectContext.SupplierInformations.OrderBy(e => e.Id);
        }

        public IQueryable<SupplierOpenAccountApp> GetSupplierOpenAccountApps() {
            return ObjectContext.SupplierOpenAccountApps.OrderBy(e => e.Id);
        }

        public IQueryable<SupplierPlanArrear> GetSupplierPlanArrears() {
            return ObjectContext.SupplierPlanArrears.OrderBy(e => e.Id);
        }

        public IQueryable<SupplierPreAppLoanDetail> GetSupplierPreAppLoanDetails() {
            return ObjectContext.SupplierPreAppLoanDetails.OrderBy(e => e.Id);
        }

        public IQueryable<SupplierPreApprovedLoan> GetSupplierPreApprovedLoans() {
            return ObjectContext.SupplierPreApprovedLoans.OrderBy(e => e.Id);
        }

        public IQueryable<SupplierShippingDetail> GetSupplierShippingDetails() {
            return ObjectContext.SupplierShippingDetails.OrderBy(e => e.Id);
        }

        public IQueryable<SupplierShippingOrder> GetSupplierShippingOrders() {
            return ObjectContext.SupplierShippingOrders.OrderBy(e => e.Id);
        }

        public IQueryable<SupplierSpareOutBoud> GetSupplierSpareOutBouds() {
            return ObjectContext.SupplierSpareOutBouds.OrderBy(e => e.Id);
        }

        public IQueryable<SupplierStoreInboundDetail> GetSupplierStoreInboundDetails() {
            return ObjectContext.SupplierStoreInboundDetails.OrderBy(e => e.Id);
        }

        public IQueryable<SupplierStorePlanDetail> GetSupplierStorePlanDetails() {
            return ObjectContext.SupplierStorePlanDetails.OrderBy(e => e.Id);
        }

        public IQueryable<SupplierStorePlanOrder> GetSupplierStorePlanOrders() {
            return ObjectContext.SupplierStorePlanOrders.OrderBy(e => e.Id);
        }

        public IQueryable<SupplierTraceCode> GetSupplierTraceCodes() {
            return ObjectContext.SupplierTraceCodes.OrderBy(e => e.Id);
        }

        public IQueryable<SupplierTraceCodeDetail> GetSupplierTraceCodeDetails() {
            return ObjectContext.SupplierTraceCodeDetails.OrderBy(e => e.Id);
        }

        public IQueryable<SupplierTraceDetail> GetSupplierTraceDetails() {
            return ObjectContext.SupplierTraceDetails.OrderBy(e => e.Id);
        }

        public IQueryable<SupplierTransferBill> GetSupplierTransferBills() {
            return ObjectContext.SupplierTransferBills.OrderBy(e => e.Id);
        }

        public IQueryable<TemPurchaseOrder> GetTemPurchaseOrders() {
            return ObjectContext.TemPurchaseOrders.OrderBy(e => e.Id);
        }

        public IQueryable<TemPurchaseOrderDetail> GetTemPurchaseOrderDetails() {
            return ObjectContext.TemPurchaseOrderDetails.OrderBy(e => e.Id);
        }

        public IQueryable<TemPurchasePlanOrder> GetTemPurchasePlanOrders() {
            return ObjectContext.TemPurchasePlanOrders.OrderBy(e => e.Id);
        }

        public IQueryable<TemPurchasePlanOrderDetail> GetTemPurchasePlanOrderDetails() {
            return ObjectContext.TemPurchasePlanOrderDetails.OrderBy(e => e.Id);
        }

        public IQueryable<TemShippingOrderDetail> GetTemShippingOrderDetails() {
            return ObjectContext.TemShippingOrderDetails.OrderBy(e => e.Id);
        }

        public IQueryable<TemSupplierShippingOrder> GetTemSupplierShippingOrders() {
            return ObjectContext.TemSupplierShippingOrders.OrderBy(e => e.Id);
        }

        public IQueryable<TiledRegion> GetTiledRegions() {
            return ObjectContext.TiledRegions.OrderBy(e => e.Id);
        }

        public IQueryable<TraceTempType> GetTraceTempTypes() {
            return ObjectContext.TraceTempTypes.OrderBy(e => e.Id);
        }

        public IQueryable<TransactionInterfaceList> GetTransactionInterfaceLists() {
            return ObjectContext.TransactionInterfaceLists.OrderBy(e => e.Id);
        }

        public IQueryable<TransactionInterfaceLog> GetTransactionInterfaceLogs() {
            return ObjectContext.TransactionInterfaceLogs.OrderBy(e => e.Id);
        }

        public IQueryable<UsedPartsDisposalBill> GetUsedPartsDisposalBills() {
            return ObjectContext.UsedPartsDisposalBills.OrderBy(e => e.Id);
        }

        public IQueryable<UsedPartsDisposalDetail> GetUsedPartsDisposalDetails() {
            return ObjectContext.UsedPartsDisposalDetails.OrderBy(e => e.Id);
        }

        public IQueryable<UsedPartsDistanceInfor> GetUsedPartsDistanceInfors() {
            return ObjectContext.UsedPartsDistanceInfors.OrderBy(e => e.Id);
        }

        public IQueryable<UsedPartsInboundDetail> GetUsedPartsInboundDetails() {
            return ObjectContext.UsedPartsInboundDetails.OrderBy(e => e.Id);
        }

        public IQueryable<UsedPartsInboundOrder> GetUsedPartsInboundOrders() {
            return ObjectContext.UsedPartsInboundOrders.OrderBy(e => e.Id);
        }

        public IQueryable<UsedPartsLoanBill> GetUsedPartsLoanBills() {
            return ObjectContext.UsedPartsLoanBills.OrderBy(e => e.Id);
        }

        public IQueryable<UsedPartsLoanDetail> GetUsedPartsLoanDetails() {
            return ObjectContext.UsedPartsLoanDetails.OrderBy(e => e.Id);
        }

        public IQueryable<UsedPartsLogisticLossBill> GetUsedPartsLogisticLossBills() {
            return ObjectContext.UsedPartsLogisticLossBills.OrderBy(e => e.Id);
        }

        public IQueryable<UsedPartsLogisticLossDetail> GetUsedPartsLogisticLossDetails() {
            return ObjectContext.UsedPartsLogisticLossDetails.OrderBy(e => e.Id);
        }

        public IQueryable<UsedPartsOutboundDetail> GetUsedPartsOutboundDetails() {
            return ObjectContext.UsedPartsOutboundDetails.OrderBy(e => e.Id);
        }

        public IQueryable<UsedPartsOutboundOrder> GetUsedPartsOutboundOrders() {
            return ObjectContext.UsedPartsOutboundOrders.OrderBy(e => e.Id);
        }

        public IQueryable<UsedPartsRefitBill> GetUsedPartsRefitBills() {
            return ObjectContext.UsedPartsRefitBills.OrderBy(e => e.Id);
        }

        public IQueryable<UsedPartsRefitReqDetail> GetUsedPartsRefitReqDetails() {
            return ObjectContext.UsedPartsRefitReqDetails.OrderBy(e => e.Id);
        }

        public IQueryable<UsedPartsRefitReturnDetail> GetUsedPartsRefitReturnDetails() {
            return ObjectContext.UsedPartsRefitReturnDetails.OrderBy(e => e.Id);
        }

        public IQueryable<UsedPartsReturnDetail> GetUsedPartsReturnDetails() {
            return ObjectContext.UsedPartsReturnDetails.OrderBy(e => e.Id);
        }

        public IQueryable<UsedPartsReturnOrder> GetUsedPartsReturnOrders() {
            return ObjectContext.UsedPartsReturnOrders.OrderBy(e => e.Id);
        }

        public IQueryable<UsedPartsReturnPolicyHistory> GetUsedPartsReturnPolicyHistories() {
            return ObjectContext.UsedPartsReturnPolicyHistories.OrderBy(e => e.Id);
        }

        public IQueryable<UsedPartsShiftDetail> GetUsedPartsShiftDetails() {
            return ObjectContext.UsedPartsShiftDetails.OrderBy(e => e.Id);
        }

        public IQueryable<UsedPartsShiftOrder> GetUsedPartsShiftOrders() {
            return ObjectContext.UsedPartsShiftOrders.OrderBy(e => e.Id);
        }

        public IQueryable<UsedPartsShippingDetail> GetUsedPartsShippingDetails() {
            return ObjectContext.UsedPartsShippingDetails.OrderBy(e => e.Id);
        }

        public IQueryable<UsedPartsShippingOrder> GetUsedPartsShippingOrders() {
            return ObjectContext.UsedPartsShippingOrders.OrderBy(e => e.Id);
        }

        public IQueryable<UsedPartsStock> GetUsedPartsStocks() {
            return ObjectContext.UsedPartsStocks.OrderBy(e => e.Id);
        }

        public IQueryable<UsedPartsStockView> GetUsedPartsStockViews() {
            return ObjectContext.UsedPartsStockViews.OrderBy(e => e.Id);
        }

        public IQueryable<UsedPartsTransferDetail> GetUsedPartsTransferDetails() {
            return ObjectContext.UsedPartsTransferDetails.OrderBy(e => e.Id);
        }

        public IQueryable<UsedPartsTransferOrder> GetUsedPartsTransferOrders() {
            return ObjectContext.UsedPartsTransferOrders.OrderBy(e => e.Id);
        }

        public IQueryable<UsedPartsWarehouse> GetUsedPartsWarehouses() {
            return ObjectContext.UsedPartsWarehouses.OrderBy(e => e.Id);
        }

        public IQueryable<UsedPartsWarehouseArea> GetUsedPartsWarehouseAreas() {
            return ObjectContext.UsedPartsWarehouseAreas.OrderBy(e => e.Id);
        }

        public IQueryable<UsedPartsWarehouseManager> GetUsedPartsWarehouseManagers() {
            return ObjectContext.UsedPartsWarehouseManagers.OrderBy(e => e.Id);
        }

        public IQueryable<UsedPartsWarehouseStaff> GetUsedPartsWarehouseStaffs() {
            return ObjectContext.UsedPartsWarehouseStaffs.OrderBy(e => e.Id);
        }

        public IQueryable<VehicleCategoryAffiProduct> GetVehicleCategoryAffiProducts() {
            return ObjectContext.VehicleCategoryAffiProducts.OrderBy(e => e.ProductId);
        }

        public IQueryable<VehicleModelAffiProduct> GetVehicleModelAffiProducts() {
            return ObjectContext.VehicleModelAffiProducts.OrderBy(e => e.ProductCategoryId);
        }

        public IQueryable<VeriCodeEffectTime> GetVeriCodeEffectTimes() {
            return ObjectContext.VeriCodeEffectTimes.OrderBy(e => e.Id);
        }

        public IQueryable<VersionInfo> GetVersionInfoes() {
            return ObjectContext.VersionInfoes.OrderBy(e => e.Id);
        }

        public IQueryable<View_ErrorTable_SAP_YX> GetView_ErrorTable_SAP_YX() {
            return ObjectContext.View_ErrorTable_SAP_YX.OrderBy(e => e.BrandName);
        }

        public IQueryable<ViewSYNCWMSINOUTLOGINFO> GetViewSYNCWMSINOUTLOGINFOes() {
            return ObjectContext.ViewSYNCWMSINOUTLOGINFOes.OrderBy(e => e.Code);
        }

        public IQueryable<Warehouse> GetWarehouses() {
            return ObjectContext.Warehouses.OrderBy(e => e.Id);
        }

        public IQueryable<WarehouseArea> GetWarehouseAreas() {
            return ObjectContext.WarehouseAreas.OrderBy(e => e.Id);
        }

        public IQueryable<WarehouseAreaCategory> GetWarehouseAreaCategories() {
            return ObjectContext.WarehouseAreaCategories.OrderBy(e => e.Id);
        }

        public IQueryable<WarehouseAreaHistory> GetWarehouseAreaHistories() {
            return ObjectContext.WarehouseAreaHistories.OrderBy(e => e.Id);
        }

        public IQueryable<WarehouseAreaManager> GetWarehouseAreaManagers() {
            return ObjectContext.WarehouseAreaManagers.OrderBy(e => e.Id);
        }

        public IQueryable<WarehouseAreaWithPartsStock> GetWarehouseAreaWithPartsStocks() {
            return ObjectContext.WarehouseAreaWithPartsStocks.OrderBy(e => e.WarehouseAreaId);
        }

        public IQueryable<WarehouseCostChangeBill> GetWarehouseCostChangeBills() {
            return ObjectContext.WarehouseCostChangeBills.OrderBy(e => e.Id);
        }

        public IQueryable<WarehouseCostChangeDetail> GetWarehouseCostChangeDetails() {
            return ObjectContext.WarehouseCostChangeDetails.OrderBy(e => e.Id);
        }

        public IQueryable<WarehouseOperator> GetWarehouseOperators() {
            return ObjectContext.WarehouseOperators.OrderBy(e => e.Id);
        }

        public IQueryable<WarehouseSequence> GetWarehouseSequences() {
            return ObjectContext.WarehouseSequences.OrderBy(e => e.Id);
        }

        public IQueryable<WarehousingInspection> GetWarehousingInspections() {
            return ObjectContext.WarehousingInspections.OrderBy(e => e.BranchName);
        }

        public IQueryable<WmsCongelationStockView> GetWmsCongelationStockViews() {
            return ObjectContext.WmsCongelationStockViews.OrderBy(e => e.BranchCode);
        }

        public IQueryable<WorkGruopPersonNumber> GetWorkGruopPersonNumbers() {
            return ObjectContext.WorkGruopPersonNumbers.OrderBy(e => e.Id);
        }

        #endregion
    }
}

