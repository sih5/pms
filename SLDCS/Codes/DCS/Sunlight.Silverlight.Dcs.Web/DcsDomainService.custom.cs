﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Data;
using System.Data.EntityClient;
using System.Data.Metadata.Edm;
using System.Data.Objects.DataClasses;
using System.Linq;
using System.Linq.Expressions;
using System.ServiceModel.DomainServices.Server;
using System.Transactions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Log.Web;
using IsolationLevel = System.Transactions.IsolationLevel;
using Utils = Sunlight.Silverlight.Web.Utils;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {

        const string AttachmentFolder = "";
        private static readonly string connectionStr;

        static DcsDomainService() {
            var entityConnStr = ConfigurationManager.ConnectionStrings["DcsEntities"].ConnectionString;
            var entityConnection = new EntityConnectionStringBuilder(entityConnStr);
            connectionStr = entityConnection.ProviderConnectionString;
        }


        internal IQueryable ParamQueryable = null;

        private bool ValidateUniqueValue<T>(Func<T, object> getValue, Func<T, bool> filter, string[] propertyName) where T : EntityObject {
            var entries = ChangeSet.ChangeSetEntries.Where(e => e.Operation == DomainOperation.Insert || e.Operation == DomainOperation.Update).ToArray();
            var entities = entries.Select(e => e.Entity).OfType<T>().ToArray();
            if(filter != null)
                entities = entities.Where(filter).ToArray();
            if(entities.Length == 0)
                return true;
            var values = entities.Select(getValue).ToArray();
            var result = values.Length == values.Distinct().Count();
            if(!result)
                foreach(var changeSetEntry in entries)
                    changeSetEntry.ValidationErrors = new[] { new ValidationResultInfo(ErrorStrings.WholesaleRewardApp_属性值不允许重复, propertyName) };
            return result;
        }

        protected override bool ValidateChangeSet() {
            if(!base.ValidateChangeSet())
                return false;

            if(ChangeSet != null) {
                //公共业务
                if(!ValidateUniqueValue<SparePart>(e => e.Code.ToLower(), f => f.Status != (int)DcsMasterDataStatus.作废, new[] { "Code" }))
                    return false;
                if(!ValidateUniqueValue<PartsBranch>(e => new {
                    e.BranchId,
                    e.PartId
                }, f => f.Status != (int)DcsBaseDataStatus.作废, new[] { "BranchId", "PartId" }))
                    return false;
                if(!ValidateUniqueValue<PartsReplacement>(e => new {
                    e.OldPartId,
                    e.NewPartId
                }, f => f.Status != (int)DcsBaseDataStatus.作废, new[] { "OldPartId", "NewPartId" }))
                    return false;
                if(!ValidateUniqueValue<ABCStrategy>(e => new {
                    e.BranchId,
                    e.Category
                }, f => f.Status != (int)DcsBaseDataStatus.作废, new[] { "BranchId", "PartId" }))
                    return false;
                if(!ValidateUniqueValue<PartsSupplier>(e => e.Code.ToLower(), f => f.Status != (int)DcsMasterDataStatus.作废, new[] { "Code" }))
                    return false;
                if(!ValidateUniqueValue<PartsSupplierRelation>(e => new {
                    e.PartId,
                    e.SupplierId
                }, f => f.Status != (int)DcsBaseDataStatus.作废, new[] { "PartId", "SupplierId" }))
                    return false;
                //网络渠道管理
                if(!ValidateUniqueValue<Dealer>(e => e.Code.ToLower(), f => f.Status != (int)DcsMasterDataStatus.作废, new[] { "Code" }))
                    return false;
                if(!ValidateUniqueValue<DealerServiceInfo>(e => new {
                    e.BranchId,
                    e.DealerId
                }, f => f.Status != (int)DcsMasterDataStatus.作废, new[] { "BranchId", "DealerId" }))
                    return false;
                if(!ValidateUniqueValue<DealerMarketDptRelation>(e => new {
                    e.MarketId,
                    e.DealerId
                }, null, new[] { "MarketId", "DealerId" }))
                    return false;
                if(!ValidateUniqueValue<DealerKeyPosition>(e => new {
                    e.BranchId,
                    PositionName = e.PositionName.ToLower()
                }, f => f.Status != (int)DcsMasterDataStatus.作废, new[] { "BranchId", "PositionName" }))
                    return false;
            }
            return true;
        }

        internal LogInfo logInfo;

        /// <summary>
        /// 获取指定实体结构
        /// </summary>
        /// <param name="entitySet"></param>
        /// <param name="notNullableFields">不允许为空的字段名称（大写）</param>
        /// <param name="fieldLenght">string类型字段长度，字段名称（大写）、长度</param>
        internal void GetEntitySchema(EntitySet entitySet, out List<string> notNullableFields, out Dictionary<string, int> fieldLenght) {
            notNullableFields = entitySet.ElementType.Properties.Where(v => !v.Nullable).Select(v => v.Name.ToUpper()).ToList();
            fieldLenght = entitySet.ElementType.Properties.Where(v => v.TypeUsage.EdmType.Name == "String").ToDictionary(v => v.Name.ToUpper(), v => int.Parse(v.TypeUsage.Facets["MaxLength"].Value.ToString()));
        }

        internal string[] GetFullExceptionMessage(Exception ex) {
            var exception = ex;
            var errorMessages = new List<string> {
                exception.Message
            };
            while(exception.InnerException != null) {
                exception = exception.InnerException;
                errorMessages.Add(exception.Message);
            }
            return errorMessages.ToArray();
        }

        private void CheckEntityState(EntityObject entity) {
            if(ChangeSet.GetChangeOperation(entity) == ChangeOperation.Update && entity.EntityState == EntityState.Added) {
                ObjectContext.ObjectStateManager.ChangeObjectState(entity, EntityState.Unchanged);
            }
        }

        private void SetCurrentUICulture() {
            var cultureName = System.Web.HttpContext.Current.Request.Headers[GlobalVar.CULTURE_INFO_HEAD_LOCAL_NAME];
            if(!string.IsNullOrEmpty(cultureName))
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(cultureName);
        }

        public override bool Submit(ChangeSet changeSet) {
            try {
                var userInfo = Sunlight.Silverlight.Web.Utils.GetCurrentUserInfo();
                logInfo = LogManager.logManager.NewLogInfo("DCS", userInfo.LoginId, userInfo.Name, userInfo.EnterpriseCode, userInfo.EnterpriseName, OperatingType.Submit, changeSet);

                bool result;
                SetCurrentUICulture();
                using(var tx = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions {
                    IsolationLevel = IsolationLevel.ReadCommitted
                })) {
                    //TODO:如数据库错误，则信息无法抛出至客户端
                    result = base.Submit(changeSet);
                    if(!ChangeSet.HasError)
                        tx.Complete();
                    else
                        logInfo.SetErrorLog(changeSet, ErrorStrings.WholesaleRewardApp_变更内容提交出错);
                }

                return result;
            } catch(Exception ex) {
                var errorMessages = GetFullExceptionMessage(ex);
                logInfo.SetErrorLog(null, errorMessages);
                //异常信息作为实体对象本身的错误信息返回
                changeSet.ChangeSetEntries.First(e => e.Operation == DomainOperation.Insert || e.Operation == DomainOperation.Update).ValidationErrors = new[] { new ValidationResultInfo(string.Format(EntityStrings._Common_提交出错 + "：{0}", string.Join("\r\n", errorMessages)), new string[0]) };
                return false;
            } finally {
                LogManager.logManager.SetLogInfoComplete(logInfo);
            }
        }

        public override IEnumerable Query(QueryDescription queryDescription, out IEnumerable<ValidationResult> validationErrors, out int totalCount) {
            validationErrors = null;
            try {
                this.ParamQueryable = queryDescription.Query;
                var userInfo = Sunlight.Silverlight.Web.Utils.GetCurrentUserInfo();
                logInfo = LogManager.logManager.NewLogInfo("DCS", userInfo.LoginId, userInfo.Name, userInfo.EnterpriseCode, userInfo.EnterpriseName, OperatingType.Query, queryDescription);
                SetCurrentUICulture();
                var result = base.Query(queryDescription, out validationErrors, out totalCount);
                // 如果手动设置过查询结果，则返回手动设置的查询结果
                if(TotalCount.HasValue) {
                    result = QueryResult;
                    totalCount = TotalCount.Value;
                }
                logInfo.SetQueryResult(result, totalCount);
                return result;
            } catch(Exception ex) {
                logInfo.SetErrorLog(validationErrors, ex);
                throw;
            } finally {
                LogManager.logManager.SetLogInfoComplete(logInfo);
            }
        }

        /// <summary>
        /// 手动设置查询的结果
        /// </summary>
        /// <param name="result">结果集</param>
        /// <param name="totalCount">分页前的总数</param> 
        /// <code>
        /// // 示例代码
        /// var source = this.ObjectContext.ABCs; //查询表达式
        /// var result = source.ComposeAllFilter(t => t, this.ParamQueryable); //结果集或者结果表达式
        /// var totalCount = source.ComposeWithoutPaging(t => t, this.ParamQueryable).Count();
        /// DomainService.SetQueryResultManually(result, totalCount);
        /// </code>
        internal void SetQueryResultManually(IEnumerable result, int totalCount) {
            QueryResult = result;
            TotalCount = totalCount;
        }

        internal IEnumerable QueryResult {
            get; private set;
        }

        internal int? TotalCount {
            get; private set;
        }

        /// <summary>
        /// 捕捉自定义Oracle报错,两个重载方法
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="strErrorCode"></param>
        internal static void GetOracleException(Exception ex, string strErrorCode = "ORA-20001") {
            //System.Data.EntityCommandExecutionException: 执行命令定义时出错。有关详细信息，请参阅内部异常。 ---> Devart.Data.Oracle.OracleException: ORA-20001: 该经销商账户可用余额不足！
            string strSource = ex.ToString();
            if(strSource.Contains(strErrorCode)) {
                string errMsg = "";
                try {
                    int indexErrorMsgBegin = strSource.IndexOf(strErrorCode);
                    //获取截取字符串之后，第一个"\n"的相对位置
                    int errorMessageLenght = strSource.Substring(indexErrorMsgBegin).IndexOf("\n");
                    errMsg = strSource.Substring(indexErrorMsgBegin, errorMessageLenght);
                } catch(Exception subStringEx) {
                    throw new Exception(subStringEx.ToString() + " 处理前Exception:" + ex.ToString() + 1);
                }
                throw new ValidationException(errMsg);
            } else throw new Exception(strSource);
        }

        /// <summary>
        /// Copy System.ServiceModel.DomainServices.Server.QueryComposer
        /// 程序集 System.ServiceModel.DomainServices.Server.dll, v4.0.30319
        /// 仅公开方法Compose，并增加source是否为空的判断
        /// </summary>
        internal static class QueryComposer {
            // Methods
            /// <summary>
            /// 返回合并后查询约束的IQueryable
            /// </summary>
            /// <param name="source">数据源</param>
            /// <param name="query">待合并查询约束</param>
            /// <returns>
            /// 如果query不为空，返回合并后查询约束的IQueryable
            /// 如果query为空，返回source
            /// </returns>
            internal static IQueryable Compose(IQueryable source, IQueryable query) {
                return query == null ? source : QueryRebaser.Rebase(source, query);
            }

            private static bool TryComposeWithLimit(IEnumerable results, DomainOperationEntry queryOperation, out IEnumerable limitedResults) {
                int resultLimit = ((QueryAttribute)queryOperation.OperationAttribute).ResultLimit;
                if(resultLimit > 0) {
                    IQueryable source = results.AsQueryable();
                    IQueryable query = Array.CreateInstance(queryOperation.AssociatedType, 0).AsQueryable();
                    query = query.Provider.CreateQuery(Expression.Call(typeof(Queryable), "Take", new Type[] {
                        query.ElementType
                    }, new Expression[] {
                        query.Expression, Expression.Constant(resultLimit)
                    }));
                    limitedResults = Compose(source, query);
                    return true;
                }
                limitedResults = null;
                return false;
            }

            private static bool TryComposeWithoutPaging(IQueryable query, out IQueryable countQuery) {
                MethodCallExpression expression = query.Expression as MethodCallExpression;
                Expression expression2 = null;
                if(((expression != null) && (expression.Method.DeclaringType == typeof(Queryable))) && expression.Method.Name.Equals("take", StringComparison.OrdinalIgnoreCase)) {
                    expression2 = expression.Arguments[0];
                    expression = expression2 as MethodCallExpression;
                    if(((expression != null) && (expression.Method.DeclaringType == typeof(Queryable))) && expression.Method.Name.Equals("skip", StringComparison.OrdinalIgnoreCase)) {
                        expression2 = expression.Arguments[0];
                    }
                }
                countQuery = null;
                if(expression2 != null) {
                    countQuery = query.Provider.CreateQuery(expression2);
                    return true;
                }
                return false;
            }

            /// <summary>
            /// 扩展方法，判断查询树是否包含除take、skip、order之外的条件
            /// </summary>
            /// <param name="query">待校验查询树</param>
            /// <returns>是包含条件</returns>
            internal static bool HaveFilterWithoutPagingAndOrder(IQueryable query) {
                if(query == null)
                    return false;
                MethodCallExpression expression = query.Expression as MethodCallExpression;
                Expression expression2 = null;
                var notExist = true;
                while(notExist && (expression != null && expression.Method.DeclaringType == typeof(Queryable))) {
                    notExist = expression.Method.Name.Equals("take", StringComparison.OrdinalIgnoreCase)
                        || expression.Method.Name.Equals("skip", StringComparison.OrdinalIgnoreCase)
                        || expression.Method.Name.Equals("orderby", StringComparison.OrdinalIgnoreCase);
                    if(notExist) {
                        expression2 = expression.Arguments[0];
                        expression = expression2 as MethodCallExpression;
                    }
                }
                return !notExist;
            }

            // Nested Types
            private class QueryRebaser : ExpressionVisitor {
                // Methods
                public static IQueryable Rebase(IQueryable source, IQueryable query) {
                    Expression expression = new Visitor(source.Expression).Visit(query.Expression);
                    return source.Provider.CreateQuery(expression);
                }

                // Nested Types
                private class Visitor : ExpressionVisitor {
                    // Fields
                    private Expression _root;

                    // Methods
                    public Visitor(Expression root) {
                        _root = root;
                    }

                    protected override Expression VisitMethodCall(MethodCallExpression m) {
                        if(((m.Arguments.Count > 0) && (m.Arguments[0].NodeType == ExpressionType.Constant)) && ((((ConstantExpression)m.Arguments[0]).Value != null) && (((ConstantExpression)m.Arguments[0]).Value is IQueryable))) {
                            List<Expression> list = new List<Expression>();
                            list.Add(_root);
                            list.AddRange(m.Arguments.Skip<Expression>(1));
                            return Expression.Call(m.Method, list.ToArray());
                        }
                        return base.VisitMethodCall(m);
                    }
                }
            }
        }

        /// <summary>
        /// 赋与实体更新时必要的信息
        /// </summary>
        /// <typeparam name="T">实体类型</typeparam>
        /// <param name="entity">实体</param>
        private void SetMofifyInfo<T>(T entity) where T : EntityObject {
            var userInfo = Utils.GetCurrentUserInfo();
            SetMofifyInfo(entity, userInfo);
        }

        /// <summary>
        /// 赋与实体更新时必要的信息
        /// </summary>
        /// <typeparam name="T">实体类型</typeparam>
        /// <param name="entity">实体</param>
        /// <param name="userInfo">用户信息</param>
        private void SetMofifyInfo<T>(T entity, Utils.UserInfo userInfo) where T : EntityObject {
            foreach(var pro in entity.GetType().GetProperties()) {
                switch(pro.Name.ToLower()) {
                    case "modifierid":
                        pro.SetValue(entity, userInfo.Id, null);
                        break;
                    case "modifiername":
                        pro.SetValue(entity, userInfo.Name, null);
                        break;
                    case "modifytime":
                        pro.SetValue(entity, DateTime.Now, null);
                        break;
                }
            }
        }

        /// <summary>
        /// 赋与实体创建时必要的信息
        /// </summary>
        /// <typeparam name="T">实体类型</typeparam>
        /// <param name="entity">实体</param>
        private void SetCreateInfo<T>(T entity) where T : EntityObject {
            var userInfo = Utils.GetCurrentUserInfo();
            SetCreateInfo(entity, userInfo);
        }

        /// <summary>
        /// 赋与实体创建时必要的信息
        /// </summary>
        /// <typeparam name="T">实体类型</typeparam>
        /// <param name="entity">实体</param>
        /// <param name="userInfo">用户信息</param>
        private void SetCreateInfo<T>(T entity, Utils.UserInfo userInfo) where T : EntityObject {
            foreach(var pro in entity.GetType().GetProperties()) {
                switch(pro.Name.ToLower()) {
                    case "creatorid":
                        pro.SetValue(entity, userInfo.Id, null);
                        break;
                    case "creatorname":
                        pro.SetValue(entity, userInfo.Name, null);
                        break;
                    case "createtime":
                        pro.SetValue(entity, DateTime.Now, null);
                        break;
                }
            }
        }

        /// <summary>
        /// 赋与实体作废时必要的信息
        /// </summary>
        /// <typeparam name="T">实体类型</typeparam>
        /// <param name="entity">实体</param>
        private void SetAbandonInfo<T>(T entity) where T : EntityObject {
            var userInfo = Utils.GetCurrentUserInfo();
            SetAbandonInfo(entity, userInfo);
        }

        /// <summary>
        /// 赋与实体作废时必要的信息
        /// </summary>
        /// <typeparam name="T">实体类型</typeparam>
        /// <param name="entity">实体</param>
        /// <param name="userInfo">用户信息</param>
        private void SetAbandonInfo<T>(T entity, Utils.UserInfo userInfo) where T : EntityObject {
            foreach(var pro in entity.GetType().GetProperties()) {
                switch(pro.Name.ToLower()) {
                    case "abandonerid":
                        pro.SetValue(entity, userInfo.Id, null);
                        break;
                    case "abandonername":
                        pro.SetValue(entity, userInfo.Name, null);
                        break;
                    case "abandontime":
                        pro.SetValue(entity, DateTime.Now, null);
                        break;
                }
            }
        }

        private void SetApproveInfo<T>(T entity) where T : EntityObject {
            var userInfo = Utils.GetCurrentUserInfo();
            SetApproveInfo(entity, userInfo);
        }

        /// <summary>
        /// 赋与实体审批时必要的信息
        /// </summary>
        /// <typeparam name="T">实体类型</typeparam>
        /// <param name="entity">实体</param>
        /// <param name="userInfo">用户信息</param>
        private void SetApproveInfo<T>(T entity, Utils.UserInfo userInfo) where T : EntityObject {
            foreach(var pro in entity.GetType().GetProperties()) {
                switch(pro.Name.ToLower()) {
                    case "approverid":
                        pro.SetValue(entity, userInfo.Id, null);
                        break;
                    case "approvername":
                        pro.SetValue(entity, userInfo.Name, null);
                        break;
                    case "approvetime":
                        pro.SetValue(entity, DateTime.Now, null);
                        break;
                }
            }
        }

        internal DcsEntities GetObjectContext() {
            return this.ObjectContext;
        }

        internal ChangeSet GetChangeSet() {
            return this.ChangeSet;
        }
    }
}
