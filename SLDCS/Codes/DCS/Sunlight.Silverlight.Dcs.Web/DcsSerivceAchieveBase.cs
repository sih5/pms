﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.Objects.DataClasses;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.ServiceModel.DomainServices.Server;
using System.Text;
using System.Text.RegularExpressions;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {


    class DcsSerivceAchieveBase {
        protected readonly DcsEntities ObjectContext;
        protected IQueryable ParamQueryable = null;
        protected Utils.UserInfo UserInfo;

        protected DcsSerivceAchieveBase(DcsDomainService service) {
            DomainService = service;
            ChangeSet = service.GetChangeSet();
            ObjectContext = service.GetObjectContext();
            UserInfo = Utils.GetCurrentUserInfo();
            this.ParamQueryable = service.ParamQueryable;
        }

        public IEnumerable<KeyValueItem> GetKeyValueItems(string category, string name, bool? isBuiltIn) {
            if(category == null)
                throw new ArgumentNullException("category");
            if(name == null)
                throw new ArgumentNullException("name");

            var result = this.ObjectContext.KeyValueItems.Where(kv => kv.Status != (int)DcsBaseDataStatus.作废 && kv.Category == category && kv.Name == name);
            if(isBuiltIn != null)
                result = result.Where(kv => kv.IsBuiltIn == isBuiltIn.Value);
            return result;
        }
        protected void CheckEntityState(EntityObject entity) {
            if(ChangeSet.GetChangeOperation(entity) == ChangeOperation.Update && entity.EntityState == EntityState.Added) {
                ObjectContext.ObjectStateManager.ChangeObjectState(entity, EntityState.Unchanged);
            }
        }

        protected DcsDomainService DomainService;

        protected ChangeSet ChangeSet;

        protected void InsertToDatabase(EntityObject entity) {
            DomainService.InsertToDatabase(entity);
        }
        public string GetErrorFilePath(string fileName) {
            return Path.Combine(Path.GetDirectoryName(fileName) ?? "", string.Format("{0}_ErrorData{1}", Path.GetFileNameWithoutExtension(fileName), Path.GetExtension(fileName)));
        }

        public string GetExportFilePath(string fileName) {
            var enterpriseCode = Utils.GetCurrentUserInfo().EnterpriseCode;
            return Path.Combine(GlobalVar.DOWNLOAD_EXPORTFILE_DIR, enterpriseCode, string.Format("{0}_{1:yyMMdd_HHmmss_fff}{2}", Path.GetFileNameWithoutExtension(fileName), DateTime.Now, Path.GetExtension(fileName)));
        }

        public string GetImportFilePath(string fileName) {
            return fileName;
        }
        protected void UpdateToDatabase(EntityObject entity) {
            DomainService.UpdateToDatabase(entity);
        }

        protected void DeleteFromDatabase(EntityObject entity) {
            DomainService.DeleteFromDatabase(entity);
        }

        /// <summary>
        /// 赋与实体更新时必要的信息
        /// </summary>
        /// <typeparam name="T">实体类型</typeparam>
        /// <param name="entity">实体</param>
        protected void SetMofifyInfo<T>(T entity) where T : EntityObject {
            SetMofifyInfo(entity, UserInfo);
        }

        /// <summary>
        /// 赋与实体更新时必要的信息
        /// </summary>
        /// <typeparam name="T">实体类型</typeparam>
        /// <param name="entity">实体</param>
        /// <param name="userInfo">用户信息</param>
        protected void SetMofifyInfo<T>(T entity, Utils.UserInfo userInfo) where T : EntityObject {
            foreach(var pro in entity.GetType().GetProperties()) {
                switch(pro.Name.ToLower()) {
                    case "modifierid":
                        pro.SetValue(entity, userInfo.Id, null);
                        break;
                    case "modifiername":
                        pro.SetValue(entity, userInfo.Name, null);
                        break;
                    case "modifytime":
                        pro.SetValue(entity, DateTime.Now, null);
                        break;
                }
            }
        }

        /// <summary>
        /// 赋与实体创建时必要的信息
        /// </summary>
        /// <typeparam name="T">实体类型</typeparam>
        /// <param name="entity">实体</param>
        protected void SetCreateInfo<T>(T entity) where T : EntityObject {
            SetCreateInfo(entity, UserInfo);
        }

        /// <summary>
        /// 捕捉自定义Oracle报错，启用分布式事务后，在RAC类型的Oracle数据上抛自定义错误，错误内容无法抛出，所以暂时用此方法来捕获错误
        /// </summary>
        /// <param name="ex"></param>
        protected void ThrowOracleException(Exception ex) {
            //System.Data.EntityCommandExecutionException: 执行命令定义时出错。有关详细信息，请参阅内部异常。 ---> Devart.Data.Oracle.OracleException: ORA-20001: 该经销商账户可用余额不足！
            var strSource = ex.ToString();
            if(Regex.Match(strSource, @"ORA-20\d{3}").Success) {
                throw new ValidationException(ObjectContext.ExecuteStoreQuery<string>(@"select 'ORA' || errorId || ': ' || errormsg from tmp_error
union all
select 'ORA' || errorId || ': ' || errormsg from SecurityTmpError").FirstOrDefault(), ex);
            }
            throw new Exception(strSource);
        }

        /// <summary>
        /// 赋与实体创建时必要的信息
        /// </summary>
        /// <typeparam name="T">实体类型</typeparam>
        /// <param name="entity">实体</param>
        /// <param name="userInfo">用户信息</param>
        protected void SetCreateInfo<T>(T entity, Utils.UserInfo userInfo) where T : EntityObject {
            foreach(var pro in entity.GetType().GetProperties()) {
                switch(pro.Name.ToLower()) {
                    case "creatorid":
                        pro.SetValue(entity, userInfo.Id, null);
                        break;
                    case "creatorname":
                        pro.SetValue(entity, userInfo.Name, null);
                        break;
                    case "createtime":
                        pro.SetValue(entity, DateTime.Now, null);
                        break;
                }
            }
        }

        /// <summary>
        /// 赋与实体作废时必要的信息
        /// </summary>
        /// <typeparam name="T">实体类型</typeparam>
        /// <param name="entity">实体</param>
        protected void SetAbandonInfo<T>(T entity) where T : EntityObject {
            SetAbandonInfo(entity, UserInfo);
        }

        /// <summary>
        /// 赋与实体作废时必要的信息
        /// </summary>
        /// <typeparam name="T">实体类型</typeparam>
        /// <param name="entity">实体</param>
        /// <param name="userInfo">用户信息</param>
        protected void SetAbandonInfo<T>(T entity, Utils.UserInfo userInfo) where T : EntityObject {
            foreach(var pro in entity.GetType().GetProperties()) {
                switch(pro.Name.ToLower()) {
                    case "abandonerid":
                        pro.SetValue(entity, userInfo.Id, null);
                        break;
                    case "abandonername":
                        pro.SetValue(entity, userInfo.Name, null);
                        break;
                    case "abandontime":
                        pro.SetValue(entity, DateTime.Now, null);
                        break;
                }
            }
        }

        protected void SetApproveInfo<T>(T entity) where T : EntityObject {
            SetApproveInfo(entity, UserInfo);
        }

        /// <summary>
        /// 赋与实体审批时必要的信息
        /// </summary>
        /// <typeparam name="T">实体类型</typeparam>
        /// <param name="entity">实体</param>
        /// <param name="userInfo">用户信息</param>
        protected void SetApproveInfo<T>(T entity, Utils.UserInfo userInfo) where T : EntityObject {
            foreach(var pro in entity.GetType().GetProperties()) {
                switch(pro.Name.ToLower()) {
                    case "approverid":
                        pro.SetValue(entity, userInfo.Id, null);
                        break;
                    case "approvername":
                        pro.SetValue(entity, userInfo.Name, null);
                        break;
                    case "approvetime":
                        pro.SetValue(entity, DateTime.Now, null);
                        break;
                }
            }
        }

        /// <summary>
        /// 撤销所有服务端的修改
        /// </summary>
        protected void RejectChanges() {
            var add = ObjectContext.ObjectStateManager.GetObjectStateEntries(EntityState.Added);
            foreach(var a in add)
                ObjectContext.Detach(a.Entity);

            var modify = ObjectContext.ObjectStateManager.GetObjectStateEntries(EntityState.Modified);
            foreach(var m in modify)
                for(int i = 0; i < m.OriginalValues.FieldCount; i++)
                    m.CurrentValues.SetValue(i, m.OriginalValues[i]);

            var delete = ObjectContext.ObjectStateManager.GetObjectStateEntries(EntityState.Deleted);
            foreach(var d in delete) {
                var entity = d.Entity;
                var entitySetName = d.EntitySet.Name;
                ObjectContext.Detach(entity);
                ObjectContext.AttachTo(entitySetName, entity);
            }
            ObjectContext.AcceptAllChanges();
        }

        /// <summary>
        /// 手动设置查询的结果
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="totalCount">未分页的数据总数，没有传值会自动计算</param>
        protected IQueryable<T> SetQueryResultManually<T>(IQueryable<T> source, int? totalCount = null) where T : EntityObject {
            var result = source.ComposeAllFilter(t => t, this.ParamQueryable);
            if(totalCount == null)
                totalCount = source.ComposeWithoutPaging(t => t, this.ParamQueryable).Count();
            DomainService.SetQueryResultManually(result, totalCount.Value);
            // 手动设置查询结果后，就不能再把表达式传给RIA框架，否则会重复查询
            return null;
        }

        private const string REDIS_KEY_TEMPALTE = "{0}:{1}";
        private const string WITHOUT_PAGING_PATTERN = @"^.*?((?=\.Skip\()|(?=\.Take\())";

        /// <summary>
        /// 生成Redis的Key
        /// </summary>
        /// <param name="prefix"></param>
        /// <param name="queryParams"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        protected static string GenerateRedisKey(string prefix, IQueryable queryParams, Dictionary<string, object> parameters = null) {
            var result = prefix;
            if(parameters != null)
                result = string.Format(REDIS_KEY_TEMPALTE, prefix, string.Join(":", parameters.Select(p => string.Format(REDIS_KEY_TEMPALTE, p.Key, p.Value))));
            return string.Format(REDIS_KEY_TEMPALTE, result, queryParams);
        }

        /// <summary>
        /// 生成总数的Key，因为总数不包含分页数据
        /// </summary>
        /// <param name="prefix"></param>
        /// <param name="queryParams"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        protected static string GenerateRedisTotalCountKey(string prefix, IQueryable queryParams, Dictionary<string, object> parameters = null) {
            return Regex.Match(GenerateRedisKey(prefix, queryParams, parameters), WITHOUT_PAGING_PATTERN).Value;
        }
    }
}
