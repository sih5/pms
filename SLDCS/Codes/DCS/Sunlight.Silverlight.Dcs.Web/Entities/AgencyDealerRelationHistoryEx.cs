﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 代理库与经销商隶属关系
    /// </summary>
    [DataContract(IsReference = true)]
    public class AgencyDealerRelationHistoryEx : EntityObject {
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }
        [DataMemberAttribute]
        public int BranchId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string BranchCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string BranchName {
            get;
            set;
        }


        [DataMemberAttribute]
        public int PartsSalesCategoryId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartsSalesCategoryCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartsSalesCategoryName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int AgencyId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string AgencyCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string AgencyName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int DealerId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string DealerCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string DealerName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int Status {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Remark {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? CreatorId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CreatorName {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? CreateTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? AgencyDealerRelationId {
            get;
            set;
        }

    }
}
