﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class AgencyDifferenceBackBillForSparepart : EntityObject {
        [Key]
        [DataMemberAttribute]
        public int Rownum {
            get;
            set;
        }
        [DataMemberAttribute]
        public int StorageCompanyId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string StorageCompanyCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string StorageCompanyName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int SparePartId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SparePartCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SparePartName {
            get;
            set;
        }
        [DataMemberAttribute]
        public string MeasureUnit {
            get;
            set;
        }
        [DataMemberAttribute]
        public int PartsInboundPlanId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartsInboundPlanCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public int InWarehouseId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string InWarehouseCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string InWarehouseName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int PartsShippingId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartsShippingCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public int PartsSalesOrderId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartsSalesOrderCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public int InboundQty {
            get;
            set;
        }
        [DataMemberAttribute]
        public int CanDIffer {
            get;
            set;
        }
         [DataMemberAttribute]
        public decimal OrderPrice {
            get;
            set;
        }
         [DataMemberAttribute]
         public DateTime CreateTime {
             get;
             set;
         }
    }
}
