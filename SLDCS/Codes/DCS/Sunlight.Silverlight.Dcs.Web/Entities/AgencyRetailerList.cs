﻿using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 代理库电商订单清单
    /// </summary>
    public partial class AgencyRetailerList {
        /// <summary>
        /// 本次出库数量 
        /// </summary>
        [DataMemberAttribute]
        public int CurrentOutQuantity {
            get;
            set;
        }
    }
}
