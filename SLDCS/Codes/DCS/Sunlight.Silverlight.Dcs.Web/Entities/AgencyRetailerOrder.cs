﻿using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 代理库电商订单
    /// </summary>
    public partial class AgencyRetailerOrder {
        /// <summary>
        /// 物流公司Id 
        /// </summary>
        [DataMemberAttribute]
        public int LogisticCompanyId {
            get;
            set;
        }
        /// <summary>
        /// 物流公司编号
        /// </summary>
        [DataMemberAttribute]
        public string LogisticCompanyCode {
            get;
            set;
        }

        /// <summary>
        /// 物流公司名称
        /// </summary>
        [DataMemberAttribute]
        public string LogisticCompanyName {
            get;
            set;
        }
        /// <summary>
        /// 快递单号
        /// </summary>
        [DataMemberAttribute]
        public string DeliveryBillNumber {
            get;
            set;
        }

    }
}
