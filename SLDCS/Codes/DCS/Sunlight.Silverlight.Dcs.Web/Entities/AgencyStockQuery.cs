﻿using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 代理库库存查询
    /// </summary>
    [DataContract(IsReference = true)]
    public class AgencyStockQuery : EntityObject {
        /// <summary>
        /// 分公司id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int BranchId {
            get;
            set;
        }

        /// <summary>
        /// 分公司名称
        /// </summary>
        [DataMemberAttribute]
        public string BranchCode {
            get;
            set;
        }

        /// <summary>
        /// 代理库编号
        /// </summary>
        [DataMemberAttribute]
        public string AgencyCode {
            get;
            set;
        }


        /// <summary>
        /// 代理库名称
        /// </summary>
        [DataMemberAttribute]
        public string AgencyName {
            get;
            set;
        }

        /// <summary>
        /// 仓库id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int WarehouseId {
            get;
            set;
        }

        /// <summary>
        /// 仓库名称
        /// </summary>
        [DataMemberAttribute]
        public string WarehouseName {
            get;
            set;
        }

        /// <summary>
        /// 配件Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int PartId {
            get;
            set;
        }

        /// <summary>
        /// 配件编号
        /// </summary>
        [DataMemberAttribute]
        public string PartCode {
            get;
            set;
        }

        /// <summary>
        /// 配件名称
        /// </summary>
        [DataMemberAttribute]
        public string PartName {
            get;
            set;
        }

        /// <summary>
        /// 库存
        /// </summary>
        [DataMemberAttribute]
        public int Qty {
            get;
            set;
        }

        /// <summary>
        /// 可用库存
        /// </summary>
        [DataMemberAttribute]
        public int UsableQty {
            get;
            set;
        }

        /// <summary>
        /// 批发价
        /// </summary>
        [DataMemberAttribute]
        public decimal Saleprice {
            get;
            set;
        }

        /// <summary>
        /// 批发金额
        /// </summary>
        [DataMemberAttribute]
        public decimal Totalprice {
            get;
            set;
        }

        /// <summary>
        /// 配件销售类型Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int PartsSalesCategoryId {
            get;
            set;
        }


        /// <summary>
        /// 配件销售类型名称
        /// </summary>
        [DataMemberAttribute]
        public string PartsSalesCategoryName {
            get;
            set;
        }

        /// <summary>
        /// 配件所属分类
        /// </summary>
        [DataMemberAttribute]
        public string PartsAttribution {
            get;
            set;
        }
    }
}
