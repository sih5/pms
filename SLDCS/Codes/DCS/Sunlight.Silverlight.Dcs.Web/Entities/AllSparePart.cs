﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web
{
    [DataContract(IsReference = true)]
    public class AllSparePart : EntityObject
    {
        [Key]
        [DataMemberAttribute]
        public int Id
        {
            get;
            set;
        }
        
        [DataMemberAttribute]
        public string Code
        {
            get;
            set;
        }
        //物料名称
        [DataMemberAttribute]
        public string Name
        {
            get;
            set;
        }
        //红岩号
        [DataMemberAttribute]
        public string ReferenceCode
        {
            get;
            set;
        }
        //供应商图号
        [DataMemberAttribute]
        public string SupplierPartCode
        {
            get;
            set;
        }
        //英文名称
        [DataMemberAttribute]
        public string EnglishName
        {
            get;
            set;
        }
        //合同价(PartsPurchasePricing.PurchasePrice)
        [DataMemberAttribute]
        public decimal PurchasePrice
        {
            get;
            set;
        }
        //单位
        [DataMemberAttribute]
        public string MeasureUnit
        {
            get;
            set;
        }
        //供应商编号PartsSupplier.Code
        [DataMemberAttribute]
        public string PartsSupplierCode
        {
            get;
            set;
        }
        //供应商名称
        [DataMemberAttribute]
        public string PartsSupplierName
        {
            get;
            set;
        }
        //是否首选供应商
        [DataMemberAttribute]
        public bool IsPrimary
        {
            get;
            set;
        }
        //合同有效期PartsPurchasePricing.ValidFrom-PartsPurchasePricing.ValidTo
        [DataMemberAttribute]
        public string Htyxq
        {
            get;
            set;
        }
        //是否暂估PartsPurchasePricing.PriceType 为临时价格时，true
        [DataMemberAttribute]
        public bool IsZg
        {
            get;
            set;
        }
        //断点时间
        [DataMemberAttribute]
        public DateTime? BreakTime
        {
            get;
            set;
        }
        //是否授权包装
        [DataMemberAttribute]
        public bool IsAccreditPack
        {
            get;
            set;
        }
        //最小订购量（最小包装数量（最小包装包装数量））
        [DataMemberAttribute]
        public int PackingCoefficient
        {
            get;
            set;
        }
        //最小销售数量
        [DataMemberAttribute]
        public int MInPackingAmount
        {
            get;
            set;
        }
        //供应商发货周期
        [DataMemberAttribute]
        public int OrderCycle
        {
            get;
            set;
        }
        //供应商物流周期 BranchSupplierRelation.ArrivalCycle
        [DataMemberAttribute]
        public int ArrivalCycle
        {
            get;
            set;
        }
        //建议售价
        [DataMemberAttribute]
        public decimal RetailGuidePrice
        {
            get;
            set;
        }
        //中心库价
        [DataMemberAttribute]
        public decimal CenterPrice
        {
            get;
            set;
        }
        //经销价 PartsSalesPrice.SalesPrice
        [DataMemberAttribute]
        public decimal SalesPrice
        {
            get;
            set;
        }
        //配件属性
        [DataMemberAttribute]
        public int PartABC
        {
            get;
            set;
        }
        //是否可销
        [DataMemberAttribute]
        public bool IsSalable
        {
            get;
            set;
        }
        //是否可采
        [DataMemberAttribute]
        public bool IsOrderable
        {
            get;
            set;
        }
        //价格属性配件分品品牌信息.加价率分组ID关联加价率 PartsBranch.PartsSalePriceIncreaseRate.GroupName
        [DataMemberAttribute]
        public string GroupName
        {
            get;
            set;
        }
        //价格属性配件分品品牌信息.加价率分组ID关联加价率 PartsBranch.PartsSalePriceIncreaseRate.GroupName
        [DataMemberAttribute]
        public int IncreaseRateGroupId
        {
            get;
            set;
        }
        //配件种类 配件信息.配件类型
        [DataMemberAttribute]
        public int PartType
        {
            get;
            set;
        }
        //配件类别配件分品牌信息.采购路线
        [DataMemberAttribute]
        public int PurchaseRoute
        {
            get;
            set;
        }
        //三包期限
        [DataMemberAttribute]
        public int PartsWarrantyLong
        {
            get;
            set;
        }
        //合同有效期限
        [DataMemberAttribute]
        public DateTime ValidFrom
        {
            get;
            set;
        }
        //合同有效期限
        [DataMemberAttribute]
        public DateTime ValidTo
        {
            get;
            set;
        }

        [Key]
        [DataMemberAttribute]
        public int PartsBranchId
        {
            get;
            set;
        }
        [Key]
        [DataMemberAttribute]
        public int PartsSupplierRelationId
        {
            get;
            set;
        }
        [Key]
        [DataMemberAttribute]
        public int PartsPurchasePricingId
        {
            get;
            set;
        }
        [Key]
        [DataMemberAttribute]
        public int PartsSupplierId
        {
            get;
            set;
        }
        [Key]
        [DataMemberAttribute]
        public int BranchSupplierRelationId
        {
            get;
            set;
        }
        [Key]
        [DataMemberAttribute]
        public int PartsRetailGuidePriceId
        {
            get;
            set;
        }
        [Key]
        [DataMemberAttribute]
        public int PartsSalesPriceId
        {
            get;
            set;
        }
        [Key]
        [DataMemberAttribute]
        public int PartsSalePriceIncreaseRateId
        {
            get;
            set;
        }
        //是否合同有效期
        [DataMemberAttribute]
        public bool IsEffective {
            get;
            set;
        }
        //是否首选供应商
        [DataMemberAttribute]
        public bool IsSupplierPutIn {
            get;
            set;
        }
         [DataMemberAttribute]
        public int? OrderGoodsCycle {
            get;
            set;
        }
    }

}
