﻿
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 经销商区域经理关系（项目）
    /// </summary>
    public partial class AppointSupplierDtl {

        [DataMember]
        public string BusinessCode {
            get;
            set;
        }
        [DataMember]
        public string BusinessName {
            get;
            set;
        }
    }
}
