﻿
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;
namespace Sunlight.Silverlight.Dcs.Web {
    public partial class BorrowBill : EntityObject {
        /// <summary>
        /// 是否上传附件
        /// </summary>
        [DataMemberAttribute]
        public bool? IsUploadFile {
            get;
            set;
        }

    }
}
