﻿using System.Runtime.Serialization;
namespace Sunlight.Silverlight.Dcs.Web {
    public partial class BorrowBillDetail {
        /// <summary>
        /// 本次归还数量
        /// </summary>
        [DataMember]
        public int? BeforeQty {
            get;
            set;
        }

        /// <summary>
        /// 确认归还数量
        /// </summary>
        [DataMember]
        public int? ConfirmBeforeQty {
            get;
            set;
        }
    }
}
