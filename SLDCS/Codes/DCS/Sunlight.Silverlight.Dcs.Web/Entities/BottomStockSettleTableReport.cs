﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class BottomStockSettleTableReport : EntityObject {
        [Key]
        [DataMemberAttribute]
        public int Rownum {
            get;
            set;
        }
        [DataMemberAttribute]
        public string DistributionCenterName {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CenterName {
            get;
            set;
        }
        [DataMemberAttribute]
        public string DealerCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string DealerName {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SparePartCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SparePartName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? ActualAvailableStock {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? OnwayQty {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? StockQty {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? DifferenceQty {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? Type {
            get;
            set;
        }
        [DataMemberAttribute]
        public Decimal? AccountSpeciesNum {
            get;
            set;
        }
        [DataMemberAttribute]
        public Decimal? ActualSpeciesNum {
            get;
            set;
        }
        [DataMemberAttribute]
        public Decimal? AccountFee {
            get;
            set;
        }
        [DataMemberAttribute]
        public Decimal? DifferSpeciesNum {
            get;
            set;
        }
        [DataMemberAttribute]
        public Decimal? BottomCoverage {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Year {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Month {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Week {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? CreateTime {
            get;
            set;
        }
        //差异金额
         [DataMemberAttribute]
        public Decimal? DifferenceMoney {
            get;
            set;
        }
        //差异数是否大于0
         [DataMemberAttribute]
        public bool?  OverZero {
            get;
            set;
        }
         [DataMemberAttribute]
         public int? ReserveTypeId {
             get;
             set;
         }
         [DataMemberAttribute]
         public string ReserveType {
             get;
             set;
         }
         [DataMemberAttribute]
         public string ReserveTypeSubItem {
             get;
             set;
         }
         [DataMemberAttribute]
         public int? ActualFee
         {
             get;
             set;
         }
    }
}
