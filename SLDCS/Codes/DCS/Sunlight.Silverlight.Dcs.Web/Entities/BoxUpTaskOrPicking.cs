﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class BoxUpTaskOrPicking : EntityObject {
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }
        [Key]
        [DataMemberAttribute]
        public int TaskId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string TaskCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string WarehouseCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string WarehouseName {
            get;
            set;
        }

        [DataMemberAttribute]
        public int OutboundType {
            get;
            set;
        }

        [DataMemberAttribute]
        public string OrderTypeName {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? OutboundTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? ShippingMethod {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Remark {
            get;
            set;
        }
        [Key]
        [DataMemberAttribute]
        public int SparePartId {
            get;
            set;
        }

        [DataMemberAttribute]
        public string SparePartName {
            get;
            set;
        }

        [DataMemberAttribute]
        public string SparePartCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public int ShippingAmount {
            get;
            set;
        }
        [Key]
        [DataMemberAttribute]
        public decimal SettlementPrice {
            get;
            set;
        }

        [DataMemberAttribute]
        public decimal TotalAmount {
            get {
                return this.SettlementPrice * this.ShippingAmount;

            }
            set {
                this.TotalAmount = value;
            }
        }

        [DataMemberAttribute]
        public int Type {
            get;
            set;
        }

        [DataMemberAttribute]
        public int PartsOutboundPlanId {
            get;
            set;
        }

        [DataMemberAttribute]
        public string PartsOutboundPlanCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string ContainerNumber {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? TraceProperty {
            get;
            set;
        }
        [DataMemberAttribute]
        public string TraceCode {
            get;
            set;
        }
    }
}
