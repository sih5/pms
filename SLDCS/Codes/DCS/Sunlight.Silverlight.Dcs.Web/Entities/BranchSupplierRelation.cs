﻿using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 分公司与供应商关系
    /// </summary>
    public partial class BranchSupplierRelation {
        /// <summary>
        /// 配件管理费率
        /// </summary>
        [DataMemberAttribute]
        public double Rate {
            get;
            set;
        }
    }
}
