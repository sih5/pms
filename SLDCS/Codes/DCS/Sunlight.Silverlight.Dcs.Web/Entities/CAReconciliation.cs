﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class CAReconciliation {
        [DataMember]
        public int CompanyType {
            get;
            set;
        }

        [DataMember]
        public string LeftMess {
            get;
            set;
        }

        [DataMember]
        public string RightMess {
            get;
            set;
        }
    }
}
