﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
namespace Sunlight.Silverlight.Dcs.Web {
    public partial class CAReconciliationList {
        /// <summary>
        /// 时间
        /// </summary>
        [DataMember]
        public DateTime? Time1 {
            get;
            set;
        }


        /// <summary>
        /// 金额
        /// </summary>
        [DataMember]
        public decimal? Money1 {
            get;
            set;
        }
    }
}
