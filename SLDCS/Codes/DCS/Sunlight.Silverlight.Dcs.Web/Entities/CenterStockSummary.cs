﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class CenterStockSummary : EntityObject {
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }
        [DataMemberAttribute]
        public string YearMonth {
            get;
            set;
        }
        //分销中心
        [DataMemberAttribute]
        public string MarketingDepartmentName {
            get;
            set;
        }
        //中心库名称
        [DataMemberAttribute]
        public string CenterName {
            get;
            set;
        }
        //服务站名称
        [DataMemberAttribute]
        public string DealerName {
            get;
            set;
        }
        //配件属性
        [DataMemberAttribute]
        public int? NewType {
            get;
            set;
        }
        //品种
        [DataMemberAttribute]
        public int? Entries {
            get;
            set;
        }
        //数量
        [DataMemberAttribute]
        public int? ActualStock {
            get;
            set;
        }
        //金额
        [DataMemberAttribute]
        public int? SalesTotal {
            get;
            set;
        }
         [DataMemberAttribute]
        public DateTime? CreateTime {
            get;
            set;
        }
    }
}
