﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;


namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class CentralPartsSalesOrder : EntityObject {
        [Key]
        [DataMemberAttribute]
        public int RowNum {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Year {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Month {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Day {
            get;
            set;
        }
        [DataMemberAttribute]
        public int OrderAmount {
            get;
            set;
        }
        [DataMemberAttribute]
        public int Type {
            get;
            set;
        }
        [DataMemberAttribute]
        public int SatisfactionAmount {
            get;
            set;
        }
        [DataMemberAttribute]
        public int UnSatisfactionAmount {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal? OneTimeRate {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal? TotalOrderAmount {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal? ApproveTotalOrderAmount {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? ApproveTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CenterName {
            get;
            set;
        }
        [DataMemberAttribute]
        public string DealerName {
            get;
            set;
        }
    }
}
