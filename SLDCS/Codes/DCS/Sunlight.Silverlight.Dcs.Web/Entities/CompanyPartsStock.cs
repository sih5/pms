﻿using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 配件企业库存
    /// </summary>
    [DataContract(IsReference = true)]
    public class CompanyPartsStock : EntityObject {
        /// <summary>
        /// 企业Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int CompanyId {
            get;
            set;
        }

        /// <summary>
        /// 配件Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int SparePartId {
            get;
            set;
        }

        /// <summary>
        /// 企业类型
        /// </summary>
        [DataMemberAttribute]
        public int CompanyType {
            get;
            set;
        }

        /// <summary>
        /// 企业编号
        /// </summary>
        [DataMemberAttribute]
        public string CompanyCode {
            get;
            set;
        }

        /// <summary>
        /// 企业名称
        /// </summary>
        [DataMemberAttribute]
        public string CompanyName {
            get;
            set;
        }

        /// <summary>
        /// 营销分公司Id
        /// </summary>
        [DataMemberAttribute]
        public int BranchId {
            get;
            set;
        }

        /// <summary>
        ///  营销分公司编号
        /// </summary>
        [DataMemberAttribute]
        public string BranchCode {
            get;
            set;
        }

        /// <summary>
        ///  营销分公司名称
        /// </summary>
        [DataMemberAttribute]
        public string BranchName {
            get;
            set;
        }

        /// <summary>
        ///  配件编号
        /// </summary>
        [DataMemberAttribute]
        public string SparePartCode {
            get;
            set;
        }

        /// <summary>
        ///  配件名称
        /// </summary>
        [DataMemberAttribute]
        public string SparePartName {
            get;
            set;
        }

        /// <summary>
        ///  订货仓库Id（销售订单提报校验库存使用）
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int WarehouseId {
            get;
            set;
        }







        /// <summary>
        ///  订货仓库编号（销售订单提报校验库存使用）
        /// </summary>
        [DataMemberAttribute]
        public string WarehouseCode {
            get;
            set;
        }

        /// <summary>
        ///  订货仓库名称（销售订单提报校验库存使用）
        /// </summary>
        [DataMemberAttribute]
        public string WarehouseName {
            get;
            set;
        }

        /// <summary>
        ///  实际库存
        /// </summary>
        [DataMemberAttribute]
        public int Quantity {
            get;
            set;
        }

        /// <summary>
        ///  可用库存
        /// </summary>
        [DataMemberAttribute]
        public int UsableQuantity {
            get;
            set;
        }

        /// <summary>
        ///  省
        /// </summary>
        [DataMemberAttribute]
        public string ProvinceName {
            get;
            set;
        }

        /// <summary>
        ///  市
        /// </summary>
        [DataMemberAttribute]
        public string CityName {
            get;
            set;
        }

        /// <summary>
        ///  县
        /// </summary>
        [DataMemberAttribute]
        public string CountyName {
            get;
            set;
        }

        /// <summary>
        /// 配件销售价
        /// </summary>
        [DataMemberAttribute]
        public decimal PartsSalesPrice {
            get;
            set;
        }

        /// <summary>
        /// 配件计量单位
        /// </summary>
        [DataMemberAttribute]
        public string MeasureUnit {
            get;
            set;
        }

        /// <summary>
        /// 品牌Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int PartsSalesCategoryId {
            get;
            set;
        }
        /// <summary>
        /// 品牌编号
        /// </summary>
        [DataMemberAttribute]
        public string PartsSalesCategoryCode {
            get;
            set;
        }

        /// <summary>
        /// 品牌名称
        /// </summary>
        [DataMemberAttribute]
        public string PartsSalesCategoryName {
            get;
            set;
        }

    }
}
