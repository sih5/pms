﻿using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 客户账户
    /// </summary>
    public partial class CustomerAccount {

        /// <summary>
        /// 可用额度
        /// </summary>
        [DataMemberAttribute]
        public decimal UseablePosition {
            get;
            set;
        }

        /// <summary>
        /// 是否欠款计算管理费
        /// </summary>
        [DataMemberAttribute]
        public bool? IsDebtManage {
            get;
            set;
        }
        [DataMemberAttribute]
        public bool? AllowCustomerCredit {
            get;
            set;
        }
        /// <summary>
        /// 订单可用额度
        /// 订单可用余额 = 账户金额 + 信用金额+ 返利金额- 锁定金额 - 未审核订单总金额
        /// </summary>
        [DataMemberAttribute]
        public decimal OrderUseablePosition {
            set;
            get;
        }
    }
}
