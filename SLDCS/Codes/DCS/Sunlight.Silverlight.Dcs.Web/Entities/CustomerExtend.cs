﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class CustomerExtend : EntityObject {

        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }

        [DataMemberAttribute]
        public string Name {
            get;
            set;
        }

        [DataMemberAttribute]
        public int Gender {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? IdDocumentType {
            get;
            set;
        }

        [DataMemberAttribute]
        public string IdDocumentNumber {
            get;
            set;
        }

        [DataMemberAttribute]
        public string CellPhoneNumber {
            get;
            set;
        }

        [DataMemberAttribute]
        public string HomePhoneNumber {
            get;
            set;
        }

        [DataMemberAttribute]
        public int CustomerType {
            get;
            set;
        }

        [DataMemberAttribute]
        public string ProvinceName {
            get;
            set;
        }

        [DataMemberAttribute]
        public string CityName {
            get;
            set;
        }

        [DataMemberAttribute]
        public string CountyName {
            get;
            set;
        }

        /// <summary>
        /// 行驶里程
        /// </summary>
        [DataMemberAttribute]
        public int? RetainedCustomerVehicleListMileage {
            get;
            set;
        }

        /// <summary>
        /// 变更时间
        /// </summary>
        [DataMemberAttribute]
        public DateTime? RetainedCustomerVehicleListChangeTime {
            get;
            set;
        }

        /// <summary>
        /// 变更操作人
        /// </summary>
        [DataMemberAttribute]
        public string RetainedCustomerVehicleListChangeOperation {
            get;
            set;
        }
    }
}
