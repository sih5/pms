﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {

    [DataContract(IsReference = true)]
    public class CustomerForBussiness : EntityObject {
        ///<summary>
        ///VIN码
        ///</summary>
        [DataMemberAttribute]
        [Key]
        public string Vin {
            get;
            set;
        }

        ///<summary>
        ///车辆Id
        ///</summary>
        [Key]
        [DataMemberAttribute]
        public int VehicleId {
            get;
            set;
        }

        ///<summary>
        ///车牌号
        ///</summary>
        [DataMemberAttribute]
        public string VehicleLicensePlate {
            get;
            set;
        }
        ///<summary>
        ///销售日期
        ///</summary>
        [DataMemberAttribute]
        public DateTime? SalesDate {
            get;
            set;
        }

        ///<summary>
        ///出厂日期
        ///</summary>
        [DataMemberAttribute]
        public DateTime? OutOfFactoryDate {
            get;
            set;
        }
        ///<summary>
        ///产品品牌
        ///</summary>
        [DataMemberAttribute]
        public string BrandName {
            get;
            set;
        }
        ///<summary>
        ///售出状态
        ///</summary>
        [DataMemberAttribute]
        public bool? IsWorkOff {
            get;
            set;
        }
        ///<summary>
        ///创建人Id
        ///</summary>
        [DataMemberAttribute]
        public int? CreatorId {
            get;
            set;
        }
        ///<summary>
        ///创建人姓名
        ///</summary>
        [DataMemberAttribute]
        public string CreatorName {
            get;
            set;
        }
        ///<summary>
        ///创建时间
        ///</summary>
        [DataMemberAttribute]
        public DateTime? CreateTime {
            get;
            set;
        }
        ///<summary>
        ///修改人Id
        ///</summary>
        [DataMemberAttribute]
        public int? ModifierId {
            get;
            set;
        }
        ///<summary>
        ///修改人名称
        ///</summary>
        [DataMemberAttribute]
        public string ModifierName {
            get;
            set;
        }
        ///<summary>
        ///修改时间
        ///</summary>
        [DataMemberAttribute]
        public DateTime? ModifyTime {
            get;
            set;
        }

        ///<summary>
        ///车辆种类Id
        ///</summary>
        [DataMemberAttribute]
        public int VehicleCategoryId {
            get;
            set;
        }
        ///<summary>
        ///车辆种类
        ///</summary>
        [DataMemberAttribute]
        public string VehicleCategoryName {
            get;
            set;
        }
        ///<summary>
        ///产品编号Id
        ///</summary>
        [DataMemberAttribute]
        public int ProductId {
            get;
            set;
        }
        ///<summary>
        ///产品编号
        ///</summary>
        [DataMemberAttribute]
        public string ProductCode {
            get;
            set;
        }
        ///<summary>
        ///保有客户Id
        ///</summary>
        [Key]
        [DataMemberAttribute]
        public int RetainedCustomerId {
            get;
            set;
        }

        ///<summary>
        ///基础客户Id
        ///</summary>
        [Key]
        [DataMemberAttribute]
        public int CustomerId {
            get;
            set;
        }

        ///<summary>
        ///基础客户名称
        ///</summary>

        [DataMemberAttribute]
        public string CustomerName {
            get;
            set;
        }

        ///<summary>
        ///基础客户手机号
        ///</summary>
        [DataMemberAttribute]
        public string CustomerCellPhoneNumber {
            get;
            set;
        }
        ///<summary>
        ///基础客户性别
        ///</summary>
        [DataMemberAttribute]
        public int CustomerGender {
            get;
            set;
        }
        ///<summary>
        ///基础客户证件类型
        ///</summary>
        [DataMemberAttribute]
        public int? CustomerIdDocumentType {
            get;
            set;
        }
        ///<summary>
        ///基础客户证件号码
        ///</summary>
        [DataMemberAttribute]
        public string CustomerIdDocumentNumber {
            get;
            set;
        }
        ///<summary>
        ///服务产品线名称
        ///</summary>
        [DataMemberAttribute]
        public string ServiceProductLineName {
            get;
            set;
        }
        ///<summary>
        ///服务产品线Id
        ///</summary>
        [DataMemberAttribute]
        [Key]
        public int ServiceProductLineId {
            get;
            set;
        }

        [DataMemberAttribute]
        public int PartsSalesCategoryId {
            get;
            set;
        }

        [DataMemberAttribute]
        public string PartsSalesCategoryName {
            get;
            set;
        }

    }
}
