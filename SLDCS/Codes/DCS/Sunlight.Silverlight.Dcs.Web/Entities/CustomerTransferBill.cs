﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class CustomerTransferBill  {
        /// <summary>
        /// 凭单号
        /// </summary>
        [DataMemberAttribute]
        public string Message {
            get;
            set;
        }
    }
}
