﻿using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {

    [DataContract(IsReference = true)]
    public class DTMRepairContractItemWithView : EntityObject {
        [Key]
        [DataMemberAttribute]
        public int View_RepairItemId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string View_Code {
            get;
            set;
        }

        [DataMemberAttribute]
        public string View_Name {
            get;
            set;
        }

        [Key]
        [DataMemberAttribute]
        public int View_RepairItemCategoryId {
            get;
            set;
        }


        [DataMemberAttribute]
        public int View_RepairItemStatus {
            get;
            set;
        }


        [DataMemberAttribute]
        public string View_RepairItemCategoryCode {
            get;
            set;
        }


        [DataMemberAttribute]
        public string View_RepairItemCategoryName {
            get;
            set;
        }

        [Key]
        [DataMemberAttribute]
        public int? View_ParentId {
            get;
            set;
        }


        [DataMemberAttribute]
        public string View_ParentCode {
            get;
            set;
        }

        [Key]
        [DataMemberAttribute]
        public int View_MalfunctionBrandRelationId {
            get;
            set;
        }


        [DataMemberAttribute]
        public string View_ParentName {
            get;
            set;
        }

        [Key]
        [DataMemberAttribute]
        public int View_PartsSalesCategoryId {
            get;
            set;
        }


        [DataMemberAttribute]
        public string View_PartsSalesCategoryName {
            get;
            set;
        }

        [Key]
        [DataMemberAttribute]
        public int View_ServiceProductLineId {
            get;
            set;
        }


        [DataMemberAttribute]
        public string View_ServiceProductLineName {
            get;
            set;
        }



        [DataMemberAttribute]
        public double View_DefaultLaborHour {
            get;
            set;
        }

        [DataMemberAttribute]
        public int View_RepairQualificationGrade {
            get;
            set;
        }

        [DataMemberAttribute]
        public int View_JobType {
            get;
            set;
        }


        [Key]
        [DataMemberAttribute]
        public string Entity_DTMObjid {
            get;
            set;
        }



        [Key]
        [DataMemberAttribute]
        public string Entity_DTMRepairContractItemObjid {
            get;
            set;
        }

        [Key]
        [DataMemberAttribute]
        public string Entity_RepairItemCode {
            get;
            set;
        }


        [DataMemberAttribute]
        public string Entity_RepairItemName {
            get;
            set;
        }


        [DataMemberAttribute]
        public int? Entity_AccountingProperty {
            get;
            set;
        }


        [DataMemberAttribute]
        public double? Entity_Hours {
            get;
            set;
        }


        [DataMemberAttribute]
        public double? Entity_BWorkHour {
            get;
            set;
        }


        [DataMemberAttribute]
        public decimal? Entity_Price {
            get;
            set;
        }


        [DataMemberAttribute]
        public decimal? Entity_WorkingFee {
            get;
            set;
        }


        [DataMemberAttribute]
        public decimal? Entity_WorkingFeeDiscount {
            get;
            set;
        }


        [DataMemberAttribute]
        public string Entity_Repairman {
            get;
            set;
        }


        [DataMemberAttribute]
        public string Entity_RepairReason {
            get;
            set;
        }


        [DataMemberAttribute]
        public int? Entity_ClaimStatus {
            get;
            set;
        }


        [DataMemberAttribute]
        public string Entity_Remark {
            get;
            set;
        }


    }
}
