﻿using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class DTMRepairContractMaterialWithView : EntityObject {

        [DataMemberAttribute]
        public int View_BranchId {
            get;
            set;
        }

        [DataMemberAttribute]
        public string View_SparePartFeature {
            get;
            set;
        }
        [DataMemberAttribute]
        public int View_SparePartPartType {
            get;
            set;
        }
        [DataMemberAttribute]
        public string View_SparePartMeasureUnit {
            get;
            set;
        }
        [DataMemberAttribute]
        public string View_SparePartSpecification {
            get;
            set;
        }
        [Key]
        [DataMemberAttribute]
        public int View_UsedPartsId {
            get;
            set;
        }
        [DataMemberAttribute]
        public int View_SelectNumber {
            get;
            set;
        }

        [DataMemberAttribute]
        public string View_UsedPartsCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string View_PartsSalesCategoryName {
            get;
            set;
        }
        [DataMemberAttribute]
        public string View_UsedPartsName {
            get;
            set;
        }


        [DataMemberAttribute]
        public int? View_PartsWarrantyCategoryId {
            get;
            set;
        }


        [DataMemberAttribute]
        public string View_PartsWarrantyCategoryName {
            get;
            set;
        }


        [DataMemberAttribute]
        public int? View_PartsSalesCategoryId {
            get;
            set;
        }


        [DataMemberAttribute]
        public int? View_PartsSupplierId {
            get;
            set;
        }


        [DataMemberAttribute]
        public string View_PartsSupplierCode {
            get;
            set;
        }


        [DataMemberAttribute]
        public string View_PartsSupplierName {
            get;
            set;
        }


        [DataMemberAttribute]
        public decimal View_SalesPrice {
            get;
            set;
        }


        [DataMemberAttribute]
        public int View_UsedPartsReturnPolicy {
            get;
            set;
        }


        [DataMemberAttribute]
        public string View_MeasureUnit {
            get;
            set;
        }


        [DataMemberAttribute]
        public string View_Specification {
            get;
            set;
        }


        [DataMemberAttribute]
        public string View_Feature {
            get;
            set;
        }


        [DataMemberAttribute]

        public int? View_PartsWarrantyType {
            get;
            set;
        }


        [DataMemberAttribute]
        public int? View_PartType {
            get;
            set;
        }


        [DataMemberAttribute]
        public int? View_WarrantyPolicyCategory {
            get;
            set;
        }


        [DataMemberAttribute]
        public int? View_DeleaveAmount {
            get;
            set;
        }


        [DataMemberAttribute]
        public string View_DeleavePartCode {
            get;
            set;
        }


        [DataMemberAttribute]
        public int? View_DealerId {
            get;
            set;
        }


        [DataMemberAttribute]
        public int? View_SubDealerId {
            get;
            set;
        }


        [DataMemberAttribute]
        public decimal? View_Quantity {
            get;
            set;
        }







        [DataMemberAttribute]
        public string Entity_DTMRepairContractMaterialObjid {
            get;
            set;
        }


        [DataMemberAttribute]
        public string Entity_DTMRepairContractItemObjid {
            get;
            set;
        }


        [DataMemberAttribute]
        public string Entity_DTMRepairContractObjid {
            get;
            set;
        }


        [DataMemberAttribute]
        public string Entity_SparePartCode {
            get;
            set;
        }


        [DataMemberAttribute]

        public string Entity_SparePartName {
            get;
            set;
        }


        [DataMemberAttribute]
        public int? Entity_AccountingProperty {
            get;
            set;
        }


        [DataMemberAttribute]
        public string Entity_MeasureUnitName {
            get;
            set;
        }


        [DataMemberAttribute]

        public int? Entity_Amount {
            get;
            set;
        }


        [DataMemberAttribute]
        public decimal? Entity_Price {
            get;
            set;
        }


        [DataMemberAttribute]
        public decimal? Entity_GuidePrice {
            get;
            set;
        }


        [DataMemberAttribute]
        public decimal? Entity_MaterialFee {
            get;
            set;
        }


        [DataMemberAttribute]
        public decimal? Entity_MaterialFeeDiscount {
            get;
            set;
        }


        [DataMemberAttribute]
        public int? Entity_ClaimStatus {
            get;
            set;
        }


        [DataMember]
        public string Entity_Remark {
            get;
            set;
        }


        [DataMemberAttribute]
        public string Entity_OldSparePartCode {
            get;
            set;
        }


        [DataMemberAttribute]
        public string Entity_OldSparePartName {
            get;
            set;
        }


        [DataMemberAttribute]
        public string Entity_OldSparePartSupplierCode {
            get;
            set;
        }


        [DataMemberAttribute]
        public string Entity_OldSparePartSupplierName {
            get;
            set;
        }


        [DataMemberAttribute]
        public string Entity_UsedPartsBarCode {
            get;
            set;
        }

    }
}
