﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class DealerPartsInboundBill : EntityObject {
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Code {
            get;
            set;
        }
        [Key]
        [DataMemberAttribute]
        public string InboundType {
            get;
            set;
        }
        [DataMemberAttribute]
        public int Quantity {
            get;
            set;
        }
        // 销售价格
        [DataMemberAttribute]
        public decimal SettlementPrice {
            get;
            set;
        }
        //建议售价
        [DataMemberAttribute]
        public decimal OriginalPrice {
            get;
            set;
        }
        //经销价
        [DataMemberAttribute]
        public decimal CostPrice {
            get;
            set;
        }
        //价格属性
        [DataMemberAttribute]
        public string GroupName {
            get;
            set;
        }
         [DataMemberAttribute]
        public string MarketingDepartmentName {
            get;
            set;
        }
         [DataMemberAttribute]
         public string StorageCompanyName {
             get;
             set;
         }
         [DataMemberAttribute]
         public string ReceivingCompanyCode {
             get;
             set;
         }
         [DataMemberAttribute]
         public string ReceivingCompanyName {
             get;
             set;
         }
         [DataMemberAttribute]
         public DateTime? ConfirmedReceptionTime {
             get;
             set;
         }
         [DataMemberAttribute]
         public string PartsCode {
             get;
             set;
         }
         [DataMemberAttribute]
         public string PartsName {
             get;
             set;
         }
    }
}
