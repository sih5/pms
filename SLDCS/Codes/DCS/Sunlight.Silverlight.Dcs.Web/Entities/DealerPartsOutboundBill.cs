﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    //服务站出库单统计
    [DataContract(IsReference = true)]
    public class DealerPartsOutboundBill : EntityObject {

        [Key]
        [DataMemberAttribute]
        public int Rownum {
            get;
            set;
        }
        [DataMemberAttribute]
        public int DealerId {
            get;
            set;
        }
       
        [DataMemberAttribute]
        public int PartsId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Code {
            get;
            set;
        }
        [DataMemberAttribute]
        public string OutInBandtype {
            get;
            set;
        }
        [DataMemberAttribute]
        public int Quantity {
            get;
            set;
        }
        //建议售价
        [DataMemberAttribute]
        public decimal ReturnPrice {
            get;
            set;
        }
        //销售价格
        [DataMemberAttribute]
        public decimal OriginalPrice {
            get;
            set;
        }
        //经销价
        [DataMemberAttribute]
        public decimal CostPrice {
            get;
            set;
        }
        //价格属性
        [DataMemberAttribute]
        public string GroupName {
            get;
            set;
        }
        //红岩号
        [DataMemberAttribute]
        public string ReferenceCode {
            get;
            set;
        }
        //配件编号
        [DataMemberAttribute]
        public string PartsCode {
            get;
            set;
        }
        //配件名称
        [DataMemberAttribute]
        public string PartsName {
            get;
            set;
        }
        //服务站名称
        [DataMemberAttribute]
        public string DealerName {
            get;
            set;
        }
        //服务站编号
        [DataMemberAttribute]
        public string DealerCode {
            get;
            set;
        }
        //分销中心
        [DataMemberAttribute]
        public string MarketingDepartmentName {
            get;
            set;
        }
        //所属中心库名称
        [DataMemberAttribute]
        public string CenterName {
            get;
            set;
        }
        //所属中心库编号
        [DataMemberAttribute]
        public string CenterCode {
            get;
            set;
        }
        //客户类型
        [DataMemberAttribute]
        public string CustomerType {
            get;
            set;
        }
        //客户名称
        [DataMemberAttribute]
        public string Customer {
            get;
            set;
        }
        //出库时间       
        [DataMemberAttribute]
        public DateTime? OutBoundTime {
            get;
            set;
        }
    }
}
