﻿using System;
using System.Data.Objects.DataClasses;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class DealerPartsRetailOrderEx : EntityObject {

        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Code {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? RetailOrderType {
            get;
            set;
        }
        [DataMemberAttribute]
        public int BranchId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string BranchName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int DealerId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string DealerCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CustomerUnit {
            get;
            set;
        }
        [DataMemberAttribute]
        public string DealerName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int SubDealerId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SubDealerCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SubDealerName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? SubDealerlOrderType {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ApprovalComment {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Customer {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CustomerPhone {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CustomerCellPhone {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Address {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SalesCategoryName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int PartsSalesCategoryId {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal TotalAmount {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Remark {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CreatorName {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? CreateTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ModifierName {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? ModifyTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ApproverName {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? ApproveTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public string AbandonerName {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? AbandonTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? Status {
            get;
            set;
        }
        [DataMemberAttribute]
        public int SerialNumber {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartsCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartsName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int Quantity {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal Price {
            get;
            set;
        }
        [DataMemberAttribute]
        public string DetailRemark {
            get;
            set;
        }
        /// <summary>
        /// 标签码
        /// </summary>
        public string SIHLabelCode {
            get;
            set;
        }
        /// <summary>
        /// 追溯属性
        /// </summary>
        public int? TraceProperty {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal? SalesPrice {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal? RetailGuidePrice {
            get;
            set;
        }
        [DataMemberAttribute]
        public string GroupCode {
            get;
            set;
        }
    }
}
