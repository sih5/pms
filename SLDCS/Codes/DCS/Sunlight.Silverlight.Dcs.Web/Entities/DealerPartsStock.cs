﻿using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 经销商配件库存
    /// </summary>
    public partial class DealerPartsStock {
        /// <summary>
        /// 零售指导价 
        /// </summary>
        [DataMemberAttribute]
        public decimal RetailGuidePrice {
            get;
            set;
        }
        /// <summary>
        /// 基准销售价
        /// </summary>
        [DataMemberAttribute]
        public decimal BasicSalePrice {
            get;
            set;
        }
    }
}
