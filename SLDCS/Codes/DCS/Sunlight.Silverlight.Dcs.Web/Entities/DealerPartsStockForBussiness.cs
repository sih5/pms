﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {

    [DataContract(IsReference = true)]
    public class DealerPartsStockForBussiness : EntityObject {
        /// <summary>
        /// Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }


        /// <summary>
        /// 经销商Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int DealerId {
            get;
            set;
        }


        /// <summary>
        /// 经销商编号
        /// </summary>
        [DataMemberAttribute]
        public string DealerCode {
            get;
            set;
        }

        /// <summary>
        /// 经销商名称
        /// </summary>
        [DataMemberAttribute]
        public string DealerName {
            get;
            set;
        }

        /// <summary>
        /// 二级站Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int? SubDealerId {
            get;
            set;
        }

        /// <summary>
        /// 营销分公司Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int BranchId {
            get;
            set;
        }

        /// <summary>
        /// 销售类型Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int SalesCategoryId {
            get;
            set;
        }


        /// <summary>
        /// 销售类型名称
        /// </summary>
        [DataMemberAttribute]
        public string SalesCategoryName {
            get;
            set;
        }

        /// <summary>
        /// 配件Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int SparePartId {
            get;
            set;
        }

        /// <summary>
        /// 配件编号
        /// </summary>
        [DataMemberAttribute]
        public string SparePartCode {
            get;
            set;
        }


        /// <summary>
        /// 配件名称
        /// </summary>
        [DataMemberAttribute]
        public string SparePartName {
            get;
            set;
        }


        /// <summary>
        /// 数量
        /// </summary>
        [DataMemberAttribute]
        public int Quantity {
            get;
            set;
        }


        /// <summary>
        /// 创建人Id
        /// </summary>
        [DataMemberAttribute]
        public int? CreatorId {
            get;
            set;
        }

        /// <summary>
        /// 创建人
        /// </summary>
        [DataMemberAttribute]
        public string CreatorName {
            get;
            set;
        }


        /// <summary>
        /// 创建时间
        /// </summary>
        [DataMemberAttribute]
        public DateTime? CreateTime {
            get;
            set;
        }


        /// <summary>
        /// 修改人Id
        /// </summary>
        [DataMemberAttribute]
        public int? ModifierId {
            get;
            set;
        }

        /// <summary>
        /// 修改人
        /// </summary>
        [DataMemberAttribute]
        public string ModifierName {
            get;
            set;
        }


        /// <summary>
        /// 修改时间
        /// </summary>
        [DataMemberAttribute]
        public DateTime? ModifyTime {
            get;
            set;
        }

        /// <summary>
        /// 零售指导价 
        /// </summary>
        [DataMemberAttribute]
        public decimal RetailGuidePrice {
            get;
            set;
        }
        /// <summary>
        /// 基准销售价
        /// </summary>
        [DataMemberAttribute]
        public decimal BasicSalePrice {
            get;
            set;
        }
        /// <summary>
        /// 追溯属性
        /// </summary>
        [DataMemberAttribute]
        public int? TraceProperty {
            get;
            set;
        }
        /// <summary>
        /// 经销价
        /// </summary>
        [DataMemberAttribute]
        public decimal? SalesPrice {
            get;
            set;
        }
       
        /// <summary>
        /// 价格属性
        /// </summary>
        [DataMemberAttribute]
        public string  GroupCode {
            get;
            set;
        }
    }
}
