﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
namespace Sunlight.Silverlight.Dcs.Web {
    public partial class DealerPartsTransOrderDetail {
        /// <summary>
        /// 可用库存
        /// </summary>
        [DataMember]
        public int Stock {
            get;
            set;
        }


        /// <summary>
        /// 待件数量
        /// </summary>
        [DataMember]
        public int Quantity {
            get;
            set;
        }
    }
}
