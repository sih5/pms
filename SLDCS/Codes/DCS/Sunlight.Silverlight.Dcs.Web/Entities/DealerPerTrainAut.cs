﻿using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 服务站人员培训及认证
    /// </summary>
    public partial class DealerPerTrainAut {

        /// <summary>
        /// 证书有效期
        /// </summary>
        [DataMemberAttribute]
        public System.DateTime? AuthenticationValDate {
            get {
                if(!AuthenticationTime.HasValue)
                    return null;
                return AuthenticationTime.Value.AddYears(2);
            }
        }
    }
}
