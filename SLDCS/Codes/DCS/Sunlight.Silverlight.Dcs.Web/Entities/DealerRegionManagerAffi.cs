﻿using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 经销商区域经理关系（项目）
    /// </summary>
    public partial class DealerRegionManagerAffi {

        /// <summary>
        /// ErrorMsg
        /// </summary>
        [DataMemberAttribute]
        public string ErrorMsg {
            get;
            set;
        }
    }
}
