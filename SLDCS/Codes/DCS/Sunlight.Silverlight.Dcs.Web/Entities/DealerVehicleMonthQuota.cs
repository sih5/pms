﻿using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class DealerVehicleMonthQuota {
        /// <summary>
        /// 导入的计划年
        /// </summary>
        [DataMemberAttribute]
        public string YearOfPlanStr {
            get;
            set;
        }

        /// <summary>
        /// 导入的计划月
        /// </summary>
        [DataMemberAttribute]
        public string MonthOfPlanStr {
            get;
            set;
        }

        /// <summary>
        /// 导入的批发目标
        /// </summary>
        [DataMemberAttribute]
        public string WholesaleQuotaQuantityStr {
            get;
            set;
        }

        /// <summary>
        /// 导入的销售目标
        /// </summary>
        [DataMemberAttribute]
        public string RetailQuotaQuantityStr {
            get;
            set;
        }

        /// <summary>
        /// 错误信息
        /// </summary>
        [DataMemberAttribute]
        public string ErrorMsgs {
            get;
            set;
        }
    }
}