﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {

    [DataContract(IsReference = true)]
    public class Dtp_VehicleInformationEx : EntityObject {
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }
        public string DmsId {
            get;
            set;
        }

        [DataMemberAttribute]
        public string Vin {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ProductCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public bool? IsWorkOff {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? OutOfFactoryDate {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SysCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? TheDate {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? ReceiveTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? HandleTime {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? HandleStatus {
            get;
            set;
        }

        [DataMemberAttribute]
        public string HandleMessage {
            get;
            set;
        }

        [DataMemberAttribute]
        public string OutFactoryCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string CustomerCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public DateTime? SalesDate {
            get;
            set;
        }

    }
}
