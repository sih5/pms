﻿using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class ERPDeliveryDetailVirtual : EntityObject {
        /// <summary>
        /// Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Sku_Code {
            get;
            set;
        }
        [DataMemberAttribute]
        public int DeliveryId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Item_Name {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ErrorMessage {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? Qty {
            get;
            set;
        }
    }
}
