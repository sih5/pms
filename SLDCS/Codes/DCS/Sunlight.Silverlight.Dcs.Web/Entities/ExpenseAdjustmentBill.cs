﻿using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class ExpenseAdjustmentBill {
        /// <summary>
        /// 市场部名称
        /// </summary>
        [DataMember]
        public string MarketingDepartmentName {
            get;
            set;
        }
    }
}
