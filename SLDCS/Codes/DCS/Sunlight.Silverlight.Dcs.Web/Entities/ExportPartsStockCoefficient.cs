﻿using System;
using System.Data.Objects.DataClasses;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class ExportPartsStockCoefficient : EntityObject {
         [DataMemberAttribute]
        public int? Type {
            get;
            set;
        }
         [DataMemberAttribute]
         public int? ProductType {
             get;
             set;
         }
         [DataMemberAttribute]
         public int? Status {
             get;
             set;
         }
         [DataMemberAttribute]
         public string CreatorName {
             get;
             set;
         }
         [DataMemberAttribute]
         public DateTime? CreateTime {
             get;
             set;
         }
         [DataMemberAttribute]
         public string ModifierName {
             get;
             set;
         }
         [DataMemberAttribute]
         public DateTime? ModifyTime {
             get;
             set;
         }
         [DataMemberAttribute]
         public Decimal? MinPrice {
             get;
             set;
         }
         [DataMemberAttribute]
         public Decimal? MaxPrice {
             get;
             set;
         }
         [DataMemberAttribute]
         public Decimal? Coefficient {
             get;
             set;
         }
    }
}
