﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class ExtendedWarrantyOrder {

        public string BusinessAddress {
            get;
            set;
        }

        public string VehicleChangeCustomerName {
            get;
            set;
        }

        public string ContactPhone {
            get;
            set;
        }

        public string MailingAddress {
            get;
            set;
        }

        public double PayStartTimeCondition {
            get;
            set;
        }

        public double PayEndTimeCondition {
            get;
            set;
        }

        public int PayStartTimeMileage {
            get;
            set;
        }
        public int PayEndTimeMileage {
            get;
            set;
        }

        public string Codes {
            get;
            set;
        }

    }
}
