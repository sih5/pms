﻿using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {

    [DataContract(IsReference = true)]
    public class FaultyPart : EntityObject {

        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }

        [DataMemberAttribute]
        public string Code {
            get;
            set;
        }

        [DataMemberAttribute]
        public string Name {
            get;
            set;
        }

        [DataMemberAttribute]
        public string ReferenceName {
            get;
            set;
        }

        [DataMemberAttribute]
        public string ReferenceCode {
            get;
            set;
        }
        /// <summary>
        /// 单位
        /// </summary>
        [DataMemberAttribute]
        public string MeasureUnit {
            get;
            set;
        }

        [DataMemberAttribute]
        public string Feature {
            get;
            set;
        }

        /// <summary>
        /// 供应商编号
        /// </summary>
        [DataMemberAttribute]
        public string SupplierCode {
            get;
            set;
        }
        /// <summary>
        /// 供应商名称
        /// </summary>
        [DataMemberAttribute]
        public string SupplierName {
            get;
            set;
        }
        /// <summary>
        /// 状态
        /// </summary>
        [DataMemberAttribute]
        public int Status {
            get;
            set;
        }
        /// <summary>
        /// 配件销售类型Id
        /// </summary>
        [DataMemberAttribute]
        public int PartsSalecategoryId {
            get;
            set;
        }
        /// <summary>
        /// 供应商Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int SupplierId {
            get;
            set;
        }

        /// <summary>
        /// 配件保修分类Id
        /// </summary>
        [DataMemberAttribute]
        public int? PartsWarrantyCategoryId {
            get;
            set;
        }

    }
}
