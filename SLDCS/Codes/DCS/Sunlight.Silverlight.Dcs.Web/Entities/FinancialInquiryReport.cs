﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web.Entities {
    [DataContract(IsReference = true)]
    public class FinancialInquiryReport : EntityObject {

        [Key]
        [DataMemberAttribute]
        public int Rownum {
            get;
            set;
        }
        //仓库ID
        [DataMemberAttribute]
        public int? WarehouseId {
            get;
            set;
        }
        //仓库名称
        [DataMemberAttribute]
        public string WarehouseName {
            get;
            set;
        }
        //仓库编号
        [DataMemberAttribute]
        public string WarehouseCode {
            get;
            set;
        }
        //销售出库
        [DataMemberAttribute]
        public int SaleOutBound {
            get;
            set;
        }
        //采购退货
        [DataMemberAttribute]
        public int PartsPurchaseReturn {
            get;
            set;
        }
        //内部领出
        [DataMemberAttribute]
        public int Internal {
            get;
            set;
        }
        //采购入库
        [DataMemberAttribute]
        public int PartsPurchaseInBound {
            get;
            set;
        }
        //销售退货
        [DataMemberAttribute]
        public int SaleReturn {
            get;
            set;
        }
    }
}
