﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;


namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class ForceReserveBillDealerMasterExport : EntityObject
    {
        [DataMemberAttribute]
        public int? Status {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CompanyCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CompanyName {
            get;
            set;
        }        
        [DataMemberAttribute]
        public string SubVersionCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ColVersionCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ReserveType {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ReserveTypeSubItem {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SubmitterName {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? SubmitTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CheckerName {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? CheckTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ApproverName {
            get;
            set;

        }
        [DataMemberAttribute]
        public DateTime? ApproveTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public string RejecterName {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? RejectTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public string AbandonerName {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? AbandonTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CreatorName {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? CreateTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ModifierName {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? ModifyTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? ValidateFrom {
            get;
            set;
        }
    }
}
