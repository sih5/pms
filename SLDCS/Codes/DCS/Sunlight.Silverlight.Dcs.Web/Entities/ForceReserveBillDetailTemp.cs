﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Sunlight.Silverlight.Dcs.Web {
      [DataContract(IsReference = true)]
    public partial class ForceReserveBillDetailTemp : EntityObject {      
        [DataMemberAttribute]
        public string SparePartCode {
            get;
            set;
        }

        /// <summary>
        /// 配件编号
        /// </summary>
        [DataMemberAttribute]
        public string SparePartName {
            get;
            set;
        }
        /// <summary>
        /// 企业编号
        /// </summary>
        /// 
        [DataMemberAttribute]
        public string CompanyCode {
            get;
            set;
        }
        /// <summary>
        /// 企业编号
        /// </summary>
        [DataMemberAttribute]
        public string CompanyName {
            get;
            set;
        }
        /// <summary>
        /// 储备数量
        /// </summary>
        [DataMemberAttribute]
        public int ForceReserveQty {
            get;
            set;
        }
        [DataMemberAttribute]
        public int SparePartId {
            get;
            set;
        }
        [DataMemberAttribute]
        public int CompanyId {
            get;
            set;
        }
        [DataMemberAttribute]
        public Decimal? CenterPrice {
            get;
            set;
        }
        [DataMemberAttribute]
        public Decimal? ReserveFee {
            get;
            set;
        }
        [DataMemberAttribute]
        public int CenterPartProperty {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? SuggestForceReserveQty {
            get;
            set;
        }
    }
}
