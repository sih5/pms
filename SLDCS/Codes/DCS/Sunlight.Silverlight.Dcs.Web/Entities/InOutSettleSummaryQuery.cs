﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class InOutSettleSummaryQuery : EntityObject {
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? InPurchaseListAll {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? InPurchaseQtyAll {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal? InPurchaseFeeAll {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? WaitInPurchaseListAll {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal? WaitInArriveFeeAll {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? PackingPurchaseListAll {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? PackingPurchaseQtyAll {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal? PackingPurchaseFeeAll {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? WaitPackPurchaseListAll {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal? WaitPackPurchaseFeeAll {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? ShelvesPurchaseListAll {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? ShelvesPurchaseQtyAll {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal? ShelvesPurchaseFeeAll {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? WaitShelvesPurchaseLisAll {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal? WaitShelvesPurchaseFeeAll {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? WarehouseId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string RecordTimes {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? RecordTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? InPurchaseList {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? InReturnList {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? InPurchaseQty {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? InReturnQty {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal? InPurchaseFee {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal? InReturnFee {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? WaitInPurchaseList {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? WaitInLineList {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal? WaitInArriveFee {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal? WaitInLineFee {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? PackingPurchaseList {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? PackingReturnList {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? PackingPurchaseQty {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? PackingReturnQty {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal? PackingPurchaseFee {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal? PackingReturnFee {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? WaitPackPurchaseList {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? WaitPackReturnList {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal? WaitPackPurchaseFee {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal? WaitPackReturnFee {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? ShelvesPurchaseList {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? ShelvesReturnList {
            get;
            set;

        }
        [DataMemberAttribute]
        public int? ShelvesPurchaseQty {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? ShelvesReturnQty {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal? ShelvesPurchaseFee {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal? ShelvesReturnFee {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? WaitShelvesPurchaseLis {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? WaitShelvesReturnList {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal? WaitShelvesPurchaseFee {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal? WaitShelvesReturnFee {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? SaleApproveList {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? SaleApproveQty {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal? SaleApproveFee {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? PickingList {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? PickingQty {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal? PickingFee {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? OutList {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? OutQty {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal? OutFee {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? WaitOutList {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? WaitOutQty {
            get;
            set;

        }
        [DataMemberAttribute]
        public int? WaitOutFee {
            get;
            set;

        }
        [DataMemberAttribute]
        public decimal? ShippingFee {
            get;
            set;

        }
        [DataMemberAttribute]
        public decimal? ShippingWeight {
            get;
            set;

        }
        [DataMemberAttribute]
        public int InPurchaseInReturnItem {
            get;
            set;
        }
        [DataMemberAttribute]
        public int WaitInPurchaseInLineItem {
            get;
            set;
        }
        [DataMemberAttribute]
        public int PackingPurchaseReturnItem {
            get;
            set;
        }
        [DataMemberAttribute]
        public int WaitPackPurchaseReturnItem {
            get;
            set;
        }
        [DataMemberAttribute]
        public int ShelvesPurchasesReturnItem {
            get;
            set;
        }
        [DataMemberAttribute]
        public int WaitShelvesPurchaseReturnItem {
            get;
            set;
        }
        [DataMemberAttribute]
        public int SaleApproveItem {
            get;
            set;
        }
        [DataMemberAttribute]
        public int PickingItem {
            get;
            set;
        }
        [DataMemberAttribute]
        public int OutItem {
            get;
            set;
        }
        [DataMemberAttribute]
        public int WaitOutItem {
            get;
            set;
        }
    }
}
