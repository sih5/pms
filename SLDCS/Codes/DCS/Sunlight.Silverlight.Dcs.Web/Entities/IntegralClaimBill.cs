﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Sunlight.Silverlight.Dcs.Web {
    //积分索赔单
    public partial class IntegralClaimBill {

        
        [DataMemberAttribute]
        public decimal? MemberValue {
            get;
            set;
        }

        [DataMemberAttribute]
        public int MaxMemberValue {
            get;
            set;
        }

        [DataMemberAttribute]
        public decimal? MaterialCost {
            get;
            set;
        }

        [DataMemberAttribute]
        public decimal? LaborCost {
            get;
            set;
        }

        [DataMemberAttribute]
        public decimal? FieldServiceCharge {
            get;
            set;
        }

        [DataMemberAttribute]
        public decimal? PartsManagementCost {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? OutRange {
            get;
            set;
        }

        [DataMemberAttribute]
        public decimal? FieldTotalAmount {
            get;
            set;
        }
        
        [DataMemberAttribute]
        public decimal? TrimCost {
            get;
            set;
        }

        [DataMemberAttribute]
        public decimal? OtherCost {
            get;
            set;
        }

        [DataMemberAttribute]
        public decimal? TotalAmount {
            get;
            set;
        }
        
        [DataMemberAttribute]
        public decimal? DeductionCost {
            get;
            set;
        }

        [DataMemberAttribute]
        public int RepairType {
            get;
            set;
        }

        //[DataMemberAttribute]
        //public decimal? RightsCost {
        //    get;
        //    set;
        //}

        [DataMemberAttribute]
        public string RedPacketsTitle {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? RepairWorkOrderId {
            get;
            set;
        }

        [DataMemberAttribute]
        public int ServiceProductLineId {
            get;
            set;
        }

        [DataMemberAttribute]
        public string RepairWorkOrder {
            get;
            set;
        }

        [DataMemberAttribute]
        public string VIN {
            get;
            set;
        }

        [DataMemberAttribute]
        public string VehicleLicensePlate {
            get;
            set;
        }
    }
}
