﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web
{
   
    public partial class IntegralClaimBillSettle : EntityObject
    {
        /// <summary>
        /// 业务编号
        /// </summary>
        [DataMemberAttribute]
        public string BusinessCode
        {
            get;
            set;
        }

        /// <summary>
        /// 分公司名称
        /// </summary>
        [DataMemberAttribute]
        public string PartsSalesCategoryName
        {
            get;
            set;
        }
    }
}
