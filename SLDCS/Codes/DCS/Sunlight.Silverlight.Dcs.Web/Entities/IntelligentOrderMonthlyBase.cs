﻿using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 智能订货月度基础表
    /// </summary>
    public partial class IntelligentOrderMonthlyBase {
        /// <summary>
        /// 当月频次合计
        /// </summary>
        [DataMemberAttribute]
        public int? StartTotalMonthlyFrequency {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? EndTotalMonthlyFrequency {
            get;
            set;
        }
    }
}
