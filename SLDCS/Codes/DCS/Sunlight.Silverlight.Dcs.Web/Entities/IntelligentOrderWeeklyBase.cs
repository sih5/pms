﻿using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 智能订货周度基础表
    /// </summary>
   public partial class IntelligentOrderWeeklyBase {
       /// <summary>
       /// 周度
       /// </summary>
       
       [DataMemberAttribute]
       public int? StartWeeklyD {
           get;
           set;
       }

       [DataMemberAttribute]
       public int? EndWeeklyD {
           get;
           set;
       }
    }
}
