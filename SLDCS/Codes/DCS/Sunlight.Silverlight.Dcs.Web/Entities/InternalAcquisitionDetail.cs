﻿
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class InternalAcquisitionDetail {
        [DataMemberAttribute]
        public int IsOrderable {
            get;
            set;
        }
    }
}
