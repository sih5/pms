﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;


namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
   public class InternalAllocationBillReport {
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }
        [Key]
        [DataMemberAttribute]
        public int PartsOutboundBillId {
            get;
            set;
        }
        //仓库ID
        [DataMemberAttribute]
        public int? WarehouseId {
            get;
            set;
        }
        //仓库名称
        [DataMemberAttribute]
        public string WarehouseName {
            get;
            set;
        }
        //仓库编号
        [DataMemberAttribute]
        public string WarehouseCode {
            get;
            set;
        }
        //出库时间
        [DataMemberAttribute]
        public DateTime? PartsOutboundBillTime {
            get;
            set;
        }
        //订单类型（内部领用）
        [DataMemberAttribute]
        public string OrderType {
            get;
            set;
        }
        //领用类型
        [DataMemberAttribute]
        public int? Type {
            get;
            set;
        }
        //配件编号
        [DataMemberAttribute]
        public string SparePartCode {
            get;
            set;
        }
        //配件名称
        [DataMemberAttribute]
        public string SparePartName {
            get;
            set;
        }
        //出库数量 配件出库单清单.出库数量
        [DataMemberAttribute]
        public int? OutboundAmount {
            get;
            set;
        }
        //库存成本EnterprisePartsCost.CostPrice
        [DataMemberAttribute]
        public decimal CostPrice {
            get;
            set;
        }
        //库存成本总额
        [DataMemberAttribute]
        public decimal CostPriceAll {
            get;
            set;
        }
         [DataMemberAttribute]
        public string PartsRequisitionSettleBillCode {
            get;
            set;
        }
         [DataMemberAttribute]
         public DateTime? PartsRequisitionSettleBillDate {
             get;
             set;
         }
    }
}
