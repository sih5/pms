﻿
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class LogisticCompanyServiceRange
    {
        [DataMember]
        public string BranchName
        {
            get;
            set;
        }
    }
}
