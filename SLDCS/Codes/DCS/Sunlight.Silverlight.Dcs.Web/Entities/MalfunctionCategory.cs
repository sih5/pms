﻿using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class MalfunctionCategory {
        /// <summary>
        /// 是否有子节点
        /// </summary>
        [DataMember]
        public bool HasChildrenNode {
            get;
            set;
        }
    }
}
