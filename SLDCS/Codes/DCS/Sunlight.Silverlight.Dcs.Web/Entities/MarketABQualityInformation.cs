﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class MarketABQualityInformation {
        /// <summary>
        /// 会签人
        /// </summary>
        [DataMemberAttribute]
        public string CountersignatureCreatorName {
            get;
            set;
        }
        /// <summary>
        /// 会签时间
        /// </summary>
        [DataMemberAttribute]
        public DateTime? CountersignatureCreateTime {
            get;
            set;
        }
    }
}
