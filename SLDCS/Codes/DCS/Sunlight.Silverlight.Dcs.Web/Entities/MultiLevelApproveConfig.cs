﻿using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class MultiLevelApproveConfig {
        [DataMember]
        public int? ApproverIds {
            get;
            set;
        }
         [DataMember]
        public string ApproverNames {
            get;
            set;
        }
    }
}
