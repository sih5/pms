﻿using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 代理库电商订单清单
    /// </summary>
    public partial class Notification {
        /// <summary>
        /// 回复内容
        /// </summary>
        [DataMemberAttribute]
        public string ReplyContent {
            get;
            set;
        }

        /// <summary>
        /// 回复路径
        /// </summary>
        [DataMemberAttribute]
        public string ReplyFilePath {
            get;
            set;
        }
    }
}
