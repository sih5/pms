﻿using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class NotificationReply {
        [DataMemberAttribute]
        public int SerialNumber {
            get;
            set;
        }
    }
}
