﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 配件出入库单
    /// </summary>
    [DataContract(IsReference = true)]
    public class OutboundAndInboundBill : EntityObject {
        /// <summary>
        /// 单据Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int BillId {
            get;
            set;
        }

        /// <summary>
        /// 单据类型
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int BillType {
            get;
            set;
        }
        [DataMemberAttribute]
        public int PartsSalesCategoryId {
            get;
            set;
        }
        /// <summary>
        /// 单据编号
        /// </summary>
        [DataMemberAttribute]
        public string BillCode {
            get;
            set;
        }

        /// <summary>
        ///  结算金额
        /// </summary>
        [DataMemberAttribute]
        public Decimal SettlementAmount {
            get;
            set;
        }

        /// <summary>
        ///  成本金额
        /// </summary>
        [DataMemberAttribute]
        public Decimal CostAmount {
            get;
            set;
        }

        /// <summary>
        ///  创建时间
        /// </summary>
        [DataMemberAttribute]
        public DateTime BillCreateTime {
            get;
            set;
        }

        /// <summary>
        /// 企业Id
        /// </summary>
        [DataMemberAttribute]
        public int CompanyId {
            get;
            set;
        }

        /// <summary>
        /// 对方单位Id
        /// </summary>
        [DataMemberAttribute]
        public int CounterpartCompanyId {
            get;
            set;
        }

        /// <summary>
        /// 对方单位编号
        /// </summary>
        [DataMemberAttribute]
        public string CounterpartCompanyCode {
            get;
            set;
        }

        /// <summary>
        /// 对方单位名称
        /// </summary>
        [DataMemberAttribute]
        public string CounterpartCompanyName {
            get;
            set;
        }

        /// <summary>
        /// 客户账户Id
        /// </summary>
        [DataMemberAttribute]
        public int? CustomerAccountId {
            get;
            set;
        }

        /// <summary>
        ///开票类型
        /// </summary>
        [DataMemberAttribute]
        public int? InvoiceType {
            get;
            set;
        }

        /// <summary>
        /// 出入库类型
        /// </summary>
        [DataMemberAttribute]
        public int BillBusinessType {
            get;
            set;
        }

        /// <summary>
        ///  配件仓库Id
        /// </summary>
        [DataMemberAttribute]
        public int WarehouseId {
            get;
            set;
        }

        /// <summary>
        ///  退货类型
        /// </summary>
        [DataMemberAttribute]
        public int ReturnType {
            get;
            set;
        }

        /// <summary>
        ///  退货原因
        /// </summary>
        [DataMemberAttribute]
        public string ReturnReason {
            get;
            set;
        }

        /// <summary>
        ///  销售退货单备注
        /// </summary>
        [DataMemberAttribute]
        public string PartsSalesReturnBillRemark {
            get;
            set;
        }

        /// <summary>
        ///  配件仓库名称
        /// </summary>
        [DataMemberAttribute]
        public string WarehouseName {
            get;
            set;
        }
        /// <summary>
        ///  平台单号
        /// </summary>
        [DataMemberAttribute]
        public string ERPOrderCode {
            get;
            set;
        }

        /// <summary>
        /// 单位名称
        /// </summary>
        [DataMemberAttribute]
        public string InvoiceCustomerName {
            get;
            set;
        }

        /// <summary>
        ///  金税编码
        /// </summary>
        [DataMemberAttribute]
        public string GoldenTaxClassifyCode {
            get;
            set;
        }

        /// <summary>
        ///  配件图号
        /// </summary>
        [DataMemberAttribute]
        public string SparePartCode {
            get;
            set;
        }

        /// <summary>
        ///  配件名称
        /// </summary>
        [DataMemberAttribute]
        public string SparePartName {
            get;
            set;
        }

        /// <summary>
        ///  纳税人识别号
        /// </summary>
        [DataMemberAttribute]
        public string Taxpayeridentification {
            get;
            set;
        }

        /// <summary>
        ///  品牌名称
        /// </summary>
        [DataMemberAttribute]
        public string PartsSalesCategoryName {
            get;
            set;
        }

        /// <summary>
        ///  原始需求单据Id
        /// </summary>
        [DataMemberAttribute]
        public int OriginalRequirementBillId {
            get;
            set;
        }

        /// <summary>
        ///  原始需求单据类型
        /// </summary>
        [DataMemberAttribute]
        public int OriginalRequirementBillType {
            get;
            set;
        }

        /// <summary>
        ///  原始采购结算单号
        /// </summary>
        [DataMemberAttribute]
        public string PartsPurchaseSettleCode
        {
            get;
            set;
        }


        /// <summary>
        ///  转采原始采购订单编号
        /// </summary>
        [DataMemberAttribute]
        public string CPPartsPurchaseOrderCode
        {
            get;
            set;
        }

        /// <summary>
        ///  转采原始入库单编号
        /// </summary>
        [DataMemberAttribute]
        public string CPPartsInboundCheckCode
        {
            get;
            set;
        }
        /// <summary>
        ///  原始需求单据编号
        /// </summary>
        [DataMemberAttribute]
        public string OriginalRequirementBillCode {
            get;
            set;
        }
        /// <summary>
        ///  订单类型
        /// </summary>
        [DataMemberAttribute]
        public string PartsSalesOrderTypeName {
            get;
            set;
        }
    }


    /// <summary>
    /// 带采购价的配件出入库清单
    /// 出入库单据编号、仓库名称、配件图号、配件名称、采购价格
    /// </summary>
    [DataContract(IsReference = true)]
    public class OutAndInDetailWithPurchasePrice : EntityObject {
        /// <summary>
        /// 出入库清单Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int BillId {
            get;
            set;
        }

        /// <summary>
        /// 出入库单据编号
        /// </summary>
        [DataMemberAttribute]
        public string BillCode {
            get;
            set;
        }

        /// <summary>
        ///  仓库名称
        /// </summary>
        [DataMemberAttribute]
        public string WarehouseName {
            get;
            set;
        }

        /// <summary>
        ///  配件图号
        /// </summary>
        [DataMemberAttribute]
        public string SparePartCode {
            get;
            set;
        }

        /// <summary>
        ///  配件名称
        /// </summary>
        [DataMemberAttribute]
        public string SparePartName {
            get;
            set;
        }

        /// <summary>
        ///  采购价格
        /// </summary>
        [DataMemberAttribute]
        public Decimal PurchasePrice {
            get;
            set;
        }
    
       
    }
}
