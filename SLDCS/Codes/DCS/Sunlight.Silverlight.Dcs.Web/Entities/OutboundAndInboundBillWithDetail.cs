﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class OutboundAndInboundBillWithDetail : EntityObject {
        /// <summary>
        /// 单据Id
        /// </summary>
        
        [DataMemberAttribute]
        public int BillId {
            get;
            set;
        }

        /// <summary>
        /// 单据编号
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public string BillCode {
            get;
            set;
        }

        /// <summary>
        /// 配件图号
        /// </summary>

        [DataMemberAttribute]
        public string  SparePartCode {
            get;
            set;
        }

        /// <summary>
        /// 配件名称
        /// </summary>
        [DataMemberAttribute]
        public string SparePartName {
            get;
            set;
        }
    }
}
