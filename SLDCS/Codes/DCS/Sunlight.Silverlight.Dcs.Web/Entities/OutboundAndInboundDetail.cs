﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 配件出入库单明细
    /// </summary>
    [DataContract(IsReference = true)]
    public class OutboundAndInboundDetail : EntityObject {
        /// <summary>
        /// 单据Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int BillId {
            get;
            set;
        }

        /// <summary>
        /// 单据类型
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int BillType {
            get;
            set;
        }

        /// <summary>
        /// 配件Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int SparePartId {
            get;
            set;
        }

        /// <summary>
        ///  配件编号
        /// </summary>
        [DataMemberAttribute]
        public string SparePartCode {
            get;
            set;
        }

        /// <summary>
        ///  配件名称
        /// </summary>
        [DataMemberAttribute]
        public string SparePartName {
            get;
            set;
        }

        /// <summary>
        ///  结算价
        /// </summary>
        [DataMemberAttribute]
        public Decimal SettlementPrice {
            get;
            set;
        }

        /// <summary>
        ///  成本价
        /// </summary>
        [DataMemberAttribute]
        public Decimal CostPrice {
            get;
            set;
        }


        /// <summary>
        ///  数量
        /// </summary>
        [DataMemberAttribute]
        public int Quantity {
            get;
            set;
        }

        /// <summary>
        ///  结算金额
        /// </summary>
        [DataMemberAttribute]
        public Decimal SettlementAmount {
            get;
            set;
        }

        /// <summary>
        /// 配件销售类型Id
        /// </summary>
        [DataMemberAttribute]
        public int PartsSaleCategoryId {
            get;
            set;
        }

        public decimal PurchasePrice {
            get;
            set;
        }

        /// <summary>
        ///  计量单位
        /// </summary>
        [DataMemberAttribute]
        public string MeasureUnit {
            get;
            set;
        }

        /// <summary>
        ///  备注
        /// </summary>
        [DataMemberAttribute]
        public string Remark {
            get;
            set;
        }

        [DataMemberAttribute]
        public string GoldenTaxClassifyCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string BillCode {
            get;
            set;
        }

    }
}
