﻿
using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class OutboundAndInboundWithDetail : EntityObject {

        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }
        [Key]
        [DataMemberAttribute]
        public int WarehouseId {
            get;
            set;
        }


        [DataMemberAttribute]
        public string WarehouseCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string WarehouseName {
            get;
            set;
        }

        [Key]
        [DataMemberAttribute]
        public int BranchId {
            get;
            set;
        }

        [DataMemberAttribute]
        public string BranchCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string BranchName {
            get;
            set;
        }

        [DataMemberAttribute]
        public string MainbillCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public int InboundType {
            get;
            set;
        }

        [DataMemberAttribute]
        public int OutboundType {
            get;
            set;
        }

        [Key]
        [DataMemberAttribute]
        public int CounterpartCompanyId {
            get;
            set;
        }

        [DataMemberAttribute]
        public string CounterpartCompanyName {
            get;
            set;
        }

        [DataMemberAttribute]
        public string CounterpartCompanyCode {
            get;
            set;
        }

        [Key]
        [DataMemberAttribute]
        public int SparePartId {
            get;
            set;
        }

        [DataMemberAttribute]
        public string SparePartCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string SparePartName {
            get;
            set;
        }

        [DataMemberAttribute]
        public int Quantity {
            get;
            set;
        }

        [DataMemberAttribute]
        public decimal Price {
            get;
            set;
        }

        [DataMemberAttribute]
        public DateTime CreateTime {
            get;
            set;
        }
    }



}
