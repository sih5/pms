﻿using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class OverstockPartsRecommendBill : EntityObject {
        [DataMember]
        public decimal Price {
            get;
            set;
        }
        [DataMember]
        public int OverstockPartsAmount {
            get;
            set;
        }

        [DataMember]
        public double? DiscountRate {
            get;
            set;
        }
    }
}
