﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 虚拟积压件库存
    /// </summary>
    [DataContract(IsReference = true)]
    public class OverstockPartsStock : EntityObject {
        /// <summary>
        /// 积压件信息Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int OverstockPartsInformationId {
            get;
            set;
        }

        /// <summary>
        /// 仓储企业Id
        /// </summary>
        [DataMemberAttribute]
        public int StorageCompanyId {
            get;
            set;
        }

        /// <summary>
        /// 配件Id
        /// </summary>
        [DataMemberAttribute]
        public int SparePartId {
            get;
            set;
        }

        /// <summary>
        /// 企业类型
        /// </summary>
        [DataMemberAttribute]
        [Display(Name = "OverstockPartsStock_StorageCompanyType", ResourceType = typeof(Resources.EntityStrings))]
        public int StorageCompanyType {
            get;
            set;
        }

        /// <summary>
        /// 企业编号
        /// </summary>
        [DataMemberAttribute]
        [Display(Name = "OverstockPartsStock_StorageCompanyCode", ResourceType = typeof(Resources.EntityStrings))]
        public string StorageCompanyCode {
            get;
            set;
        }

        /// <summary>
        /// 企业名称
        /// </summary>
        [DataMemberAttribute]
        [Display(Name = "OverstockPartsStock_StorageCompanyName", ResourceType = typeof(Resources.EntityStrings))]
        public string StorageCompanyName {
            get;
            set;
        }

        /// <summary>
        /// 省
        /// </summary>
        [DataMemberAttribute]
        [Display(Name = "OverstockPartsStock_ProvinceName", ResourceType = typeof(Resources.EntityStrings))]
        public string ProvinceName {
            get;
            set;
        }

        /// <summary>
        /// 市
        /// </summary>
        [DataMemberAttribute]
        [Display(Name = "OverstockPartsStock_CityName", ResourceType = typeof(Resources.EntityStrings))]
        public string CityName {
            get;
            set;
        }

        /// <summary>
        /// 县
        /// </summary>
        [DataMemberAttribute]
        public string CountyName {
            get;
            set;
        }

        /// <summary>
        /// 营销分公司Id
        /// </summary>
        [DataMemberAttribute]
        public int BranchId {
            get;
            set;
        }

        /// <summary>
        /// 营销分公司编号
        /// </summary>
        [DataMemberAttribute]
        public string BranchCode {
            get;
            set;
        }

        /// <summary>
        /// 营销分公司名称
        /// </summary>
        [DataMemberAttribute]
        public string BranchName {
            get;
            set;
        }

        /// <summary>
        ///  配件编号
        /// </summary>
        [DataMemberAttribute]
        [Display(Name = "OverstockPartsStock_SparePartCode", ResourceType = typeof(Resources.EntityStrings))]
        public string SparePartCode {
            get;
            set;
        }

        /// <summary>
        ///  配件名称
        /// </summary>
        [DataMemberAttribute]
        [Display(Name = "OverstockPartsStock_SparePartName", ResourceType = typeof(Resources.EntityStrings))]
        public string SparePartName {
            get;
            set;
        }

        /// <summary>
        ///  可用库存
        /// </summary>
        [DataMemberAttribute]
        [Display(Name = "OverstockPartsStock_UsableQuantity", ResourceType = typeof(Resources.EntityStrings))]
        public int UsableQuantity {
            get;
            set;
        }

        /// <summary>
        ///  创建时间
        /// </summary>
        [DataMemberAttribute]
        [Display(Name = "_Common_CreateTime", ResourceType = typeof(Resources.EntityStrings))]
        public DateTime? CreateTime {
            get;
            set;
        }

        /// <summary>
        ///  处理价格
        /// </summary>
        [DataMemberAttribute]
        public decimal Dealprice {
            get;
            set;
        }

        /// <summary>
        ///  批发价
        /// </summary>
        [DataMemberAttribute]
        public decimal? SalePrice {
            get;
            set;
        }

        /// <summary>
        ///  处理价金额
        /// </summary>
        [DataMemberAttribute]
        public decimal DealAmount {
            get;
            set;
        }

        /// <summary>
        ///  配件销售类型Id
        /// </summary>
        [DataMemberAttribute]
        public int? PartsSalesCategoryId {
            get;
            set;
        }

        /// <summary>
        ///  配件销售类型名称
        /// </summary>
        [DataMemberAttribute]
        public string PartsSalesCategoryName {
            get;
            set;
        }

        /// <summary>
        /// 冻结库存
        /// </summary>
        [DataMemberAttribute]
        public int CongelationStockQty {
            get;
            set;
        }

        /// <summary>
        /// 不可用库存
        /// </summary>
        [DataMemberAttribute]
        int DisabledStock {
            get;
            set;
        }

        ///<summary>
        /// 联系人
        ///</summary>
        [DataMemberAttribute]
        public string LinkMan {
            get;
            set;
        }

        ///<summary>
        /// 联系人电话
        ///</summary>
        [DataMemberAttribute]
        public string LinkPhone {
            get;
            set;
        }


    }
}
