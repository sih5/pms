﻿using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;


namespace Sunlight.Silverlight.Dcs.Web
{
    public partial class PackingTask 
    {
        [DataMember]
        public int ScanNumber
        {
            get;
            set;
        }
        [DataMember]
        public string FirstPackingMaterial
        {
            get;
            set;
        }
        [DataMember]
        public string FirstPackingMaterialSj
        {
            get;
            set;
        }
        [DataMember]
        public int FirstNumber
        {
            get;
            set;
        }
        [DataMember]
        public int FirstNumberSj
        {
            get;
            set;
        }
        [DataMember]
        public string SecPackingMaterial
        {
            get;
            set;
        }
        [DataMember]
        public int SecNumber
        {
            get;
            set;
        }
        [DataMember]
        public string SecPackingMaterialSj
        {
            get;
            set;
        }
        [DataMember]
        public int SecNumberSj
        {
            get;
            set;
        }
          [DataMember]
        public string ThidPackingMaterial
        {
            get;
            set;
        }
        [DataMember]
        public int ThidNumber
        {
            get;
            set;
        }
        [DataMember]
        public string ThidPackingMaterialSj
        {
            get;
            set;
        }
        [DataMember]
        public int ThidNumberSj
        {
            get;
            set;
        }
        [DataMember]
        public int PackingNumber
        {
            get;
            set;
        }
         [DataMember]
        public string ProductCode
        {
            get;
            set;
        }
         [DataMember]
         public string ReferenceCode {
             get;
             set;
         }
        [DataMemberAttribute]
         public string TraceProperty {
             get;
             set;
         }
         [DataMemberAttribute]
        public string TraceCode {
            get;
            set;
        }
         [DataMemberAttribute]
         public int? TracePropertyInt {
             get;
             set;
         }
         [DataMemberAttribute]
         public string SIHCode {
             get;
             set;
         }
        [DataMemberAttribute]
         public string BoxCode {
             get;
             set;
         }
    }
}
