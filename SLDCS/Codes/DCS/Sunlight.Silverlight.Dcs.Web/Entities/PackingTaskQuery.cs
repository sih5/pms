﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Sunlight.Silverlight.Dcs.Web
{
    [DataContract(IsReference = true)]
    public class PackingTaskQuery : EntityObject
    {
        [Key]
        [DataMemberAttribute]
        public int Id
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public String Code
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public String PartsInboundPlanCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public String PartsInboundCheckBillCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public String SparePartCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public String SparePartName
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int PlanQty
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int PackingQty
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public String SourceCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int Status
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public String CounterpartCompanyCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public String CounterpartCompanyName
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public String CreatorName
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? CreateTime
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public String WarehouseCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public String WarehouseName
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? PartsInboundPlanId
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? PartsInboundCheckBillId
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? SparePartId
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string BatchNumber
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ModifierName
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? ModifyTime
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PackingMaterial
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ReferenceCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? PackNum
        {
            get;
            set;
        }
         [DataMemberAttribute]
        public int? InboundType {
            get;
            set;
        }
        [DataMemberAttribute]
         public string WarehouseAreaCode {
             get;
             set;
         }
        [DataMemberAttribute]
        public int? TraceProperty {
            get;
            set;
        }
        [DataMemberAttribute] 
        public bool HasDifference { 
            get; 
            set; 
        } 
    }
}
