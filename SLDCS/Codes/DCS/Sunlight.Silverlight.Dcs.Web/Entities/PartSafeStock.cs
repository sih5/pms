﻿using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 配件安全库存
    /// </summary>
    public partial class PartSafeStock {

        /// <summary>
        /// 近12月出库频次
        /// </summary>

        [DataMemberAttribute]
        public int? StartOrderQuantityFor12M {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? EndOrderQuantityFor12M {
            get;
            set;
        }
    }
}
