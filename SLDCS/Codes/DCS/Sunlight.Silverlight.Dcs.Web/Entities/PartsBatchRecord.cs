﻿using System;
using System.ServiceModel.DomainServices.Server;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class PartsBatchRecord {
        [Exclude]
        public string BatchNumberDate {
            get {
                var today = DateTime.Today;
                var year = "";
                switch(today.Year) {
                    case 2012:
                        year = "B";
                        break;
                    case 2013:
                        year = "C";
                        break;
                    case 2014:
                        year = "D";
                        break;
                    case 2015:
                        year = "E";
                        break;
                    case 2016:
                        year = "F";
                        break;
                    case 2017:
                        year = "G";
                        break;
                    case 2018:
                        year = "H";
                        break;
                    case 2019:
                        year = "I";
                        break;
                    case 2020:
                        year = "J";
                        break;
                    case 2021:
                        year = "K";
                        break;
                    case 2022:
                        year = "L";
                        break;
                    case 2023:
                        year = "M";
                        break;
                    case 2024:
                        year = "N";
                        break;
                    case 2025:
                        year = "O";
                        break;
                    case 2026:
                        year = "P";
                        break;
                    case 2027:
                        year = "Q";
                        break;
                    case 2028:
                        year = "R";
                        break;
                    case 2029:
                        year = "S";
                        break;
                    case 2030:
                        year = "T";
                        break;
                }
                var month = "";
                switch(today.Month) {
                    case 1:
                        year = "A";
                        break;
                    case 2:
                        year = "B";
                        break;
                    case 3:
                        year = "C";
                        break;
                    case 4:
                        year = "D";
                        break;
                    case 5:
                        year = "E";
                        break;
                    case 6:
                        year = "F";
                        break;
                    case 7:
                        year = "G";
                        break;
                    case 8:
                        year = "H";
                        break;
                    case 9:
                        year = "I";
                        break;
                    case 10:
                        year = "J";
                        break;
                    case 11:
                        year = "K";
                        break;
                    case 12:
                        year = "L";
                        break;
                }
                return string.Concat(year, month, today.ToString("dd"));
            }
        }
    }
}
