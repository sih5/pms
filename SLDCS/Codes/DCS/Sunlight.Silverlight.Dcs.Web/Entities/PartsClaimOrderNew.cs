﻿
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class PartsClaimOrderNew {
        /// <summary>
        /// RDC入库计划单号
        /// </summary>
        [DataMember]
        public string RDCPartsInboundPlanCode {
            get;
            set;
        }

        [DataMember]
        public int RDCPartsInboundPlanStauts {
            get;
            set;
        }

        [DataMember]
        public string AgencyOutWarehouseName {
            get;
            set;
        }
        /// <summary>
        /// 退货企业名称
        /// </summary>
        [DataMember]
        public string ReturnCompanyName {
            get;
            set;
        }
        /// <summary>
        /// Branch编号
        /// </summary>
        [DataMember]
        public string BranchCode
        {
            get;
            set;
        }
        /// <summary>
        /// Branch名称
        /// </summary>
        [DataMember]
        public string BranchName
        {
            get;
            set;
        }
        /// <summary>
        /// 供货单位名称
        /// </summary>
         [DataMember]
        public string RWarehouseCompanyName 
        {
            get;
            set;
        }

          [DataMember]
         public string FaultyPartsReferenceCode 
        {
            get;
            set;
        }

        [DataMember]
        public int? ApprovalId
        {
            get;
            set;
        }
    }
}
