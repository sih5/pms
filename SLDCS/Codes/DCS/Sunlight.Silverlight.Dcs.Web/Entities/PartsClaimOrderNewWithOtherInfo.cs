﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {

    [DataContract(IsReference = true)]
    public class PartsClaimOrderNewWithOtherInfo : EntityObject {
        [Key]
        [DataMemberAttribute()]
        public System.Int32 Id {
            get;
            set;
        }

        [Key]
        [DataMemberAttribute()]
        public System.Int32 CDCPartsInboundPlanId {
            get;
            set;
        }
        [Key]
        [DataMemberAttribute()]
        public System.Int32 RDCPartsInboundPlanId {
            get;
            set;
        }
        [Key]
        [DataMemberAttribute()]
        public System.Int32 IdForGetBussinessInfo {
            get;
            set;
        }
        [DataMemberAttribute()]
        public System.String CDCPartsInboundPlanCode {
            get;
            set;
        }
        [DataMemberAttribute()]
        public System.Int32 CDCPartsInboundPlanStauts {
            get;
            set;
        }
        [DataMemberAttribute()]
        public System.Int32? OverWarrantyDate {
            get;
            set;
        }
        [DataMemberAttribute()]
        public System.String RDCPartsInboundPlanCode {
            get;
            set;
        }
        [DataMemberAttribute()]
        public System.Int32 RDCPartsInboundPlanStauts {
            get;
            set;
        }

        [DataMemberAttribute()]
        public System.Int32 ReturnCompanyType {
            get;
            set;
        }

        [DataMemberAttribute()]
        public Nullable<System.Boolean> IsOutWarranty {
            get;
            set;
        }

        [DataMemberAttribute()]
        public Nullable<System.DateTime> AbandonTime {
            get;
            set;
        }
        [DataMemberAttribute()]
        public Nullable<System.DateTime> ApproveTime {
            get;
            set;
        }
        [DataMemberAttribute()]
        public Nullable<System.DateTime> CancelTime {
            get;
            set;
        }
        [DataMemberAttribute()]
        public Nullable<System.DateTime> CreateTime {
            get;
            set;
        }
        [DataMemberAttribute()]
        public Nullable<System.DateTime> ModifyTime {
            get;
            set;
        }
        [DataMemberAttribute()]
        public Nullable<System.DateTime> PartsSaleTime {
            get;
            set;
        }
        [DataMemberAttribute()]
        public Nullable<System.DateTime> RejectTime {
            get;
            set;
        }
        [DataMemberAttribute()]
        public Nullable<System.DateTime> RepairRequestTime {
            get;
            set;
        }
        [DataMemberAttribute()]
        public Nullable<System.Decimal> LaborCost {
            get;
            set;
        }
        [DataMemberAttribute()]
        public Nullable<System.Decimal> MaterialCost {
            get;
            set;
        }
        [DataMemberAttribute()]
        public Nullable<System.Decimal> MaterialManagementCost {
            get;
            set;
        }
        [DataMemberAttribute()]
        public Nullable<System.Decimal> OtherCost {
            get;
            set;
        }
        [DataMemberAttribute()]
        public Nullable<System.Decimal> TotalAmount {
            get;
            set;
        }
        [DataMemberAttribute()]
        public Nullable<System.Int32> AbandonerId {
            get;
            set;
        }
        [DataMemberAttribute()]
        public Nullable<System.Int32> AgencyOutWarehouseId {
            get;
            set;
        }
        [DataMemberAttribute()]
        public Nullable<System.Int32> ApproverId {
            get;
            set;
        }
        [DataMemberAttribute()]
        public Nullable<System.Int32> CancelId {
            get;
            set;
        }
        [DataMemberAttribute()]
        public Nullable<System.Int32> CDCReturnWarehouseId {
            get;
            set;
        }
        [DataMemberAttribute()]
        public Nullable<System.Int32> CDCRWCompanyId {
            get;
            set;
        }
        [DataMemberAttribute()]
        public Nullable<System.Int32> CDCRWCompanyType {
            get;
            set;
        }
        [DataMemberAttribute()]
        public Nullable<System.Int32> CreatorId {
            get;
            set;
        }
        [DataMemberAttribute()]
        public Nullable<System.Int32> FaultyPartsSupplierId {
            get;
            set;
        }
        [DataMemberAttribute()]
        public Nullable<System.Int32> ModifierId {
            get;
            set;
        }
        [DataMemberAttribute()]
        public Nullable<System.Int32> PartsRetailOrderId {
            get;
            set;
        }
        [DataMemberAttribute()]
        public Nullable<System.Int32> PartsSaleLong {
            get;
            set;
        }
        [DataMemberAttribute()]
        public Nullable<System.Int32> PartsSalesOrderId {
            get;
            set;
        }
        [DataMemberAttribute()]
        public Nullable<System.Int32> PartsWarrantyLong {
            get;
            set;
        }
        [DataMemberAttribute()]
        public Nullable<System.Int32> Quantity {
            get;
            set;
        }
        [DataMemberAttribute()]
        public Nullable<System.Int32> RejectId {
            get;
            set;
        }
        [DataMemberAttribute()]
        public Nullable<System.Int32> RejectQty {
            get;
            set;
        }
        [DataMemberAttribute()]
        public Nullable<System.Int32> RejectStatus {
            get;
            set;
        }
        [DataMemberAttribute()]
        public Nullable<System.Int32> RepairOrderId {
            get;
            set;
        }
        [DataMemberAttribute()]
        public Nullable<System.Int32> RWarehouseCompanyId {
            get;
            set;
        }
        [DataMemberAttribute()]
        public Nullable<System.Int32> RWarehouseCompanyType {
            get;
            set;
        }
        [DataMemberAttribute()]
        public Nullable<System.Int32> SalesWarehouseId {
            get;
            set;
        }
        [DataMemberAttribute()]
        public Nullable<System.Int32> SubmitterId {
            get;
            set;
        }
        [DataMemberAttribute()]
        public Nullable<System.Int32> SubmitterName {
            get;
            set;
        }
        [DataMemberAttribute()]
        public Nullable<System.Int32> SubmitTime {
            get;
            set;
        }
        [DataMemberAttribute()]
        public System.Int32 BranchId {
            get;
            set;
        }
        [DataMemberAttribute()]
        public System.Int32 FaultyPartsId {
            get;
            set;
        }

        [DataMemberAttribute()]
        public System.Int32 PartsSalesCategoryId {
            get;
            set;
        }
        [DataMemberAttribute()]
        public System.Int32 ReturnCompanyId {
            get;
            set;
        }
        [DataMemberAttribute()]
        public System.Int32 ReturnWarehouseId {
            get;
            set;
        }
        [DataMemberAttribute()]
        public System.Int32 SettlementStatus {
            get;
            set;
        }
        [DataMemberAttribute()]
        public System.Int32 Status {
            get;
            set;
        }
        [DataMemberAttribute()]
        public System.String AbandonerName {
            get;
            set;
        }
        [DataMemberAttribute()]
        public System.String ApproveComment {
            get;
            set;
        }
        [DataMemberAttribute]
        public System.String PartsSalesCategoryName {
            get;
            set;
        }
        [DataMemberAttribute]
        public System.String ReturnCompanyName {
            get;
            set;
        }
        [DataMemberAttribute]
        public System.String ReturnCompanyCode {
            get;
            set;
        }
        [DataMemberAttribute()]
        public System.String ApproveCommentHistory {
            get;
            set;
        }
        [DataMemberAttribute()]
        public System.String ApproverName {
            get;
            set;
        }
        [DataMemberAttribute()]
        public System.String CancelName {
            get;
            set;
        }
        [DataMemberAttribute()]
        public System.String CDCReturnWarehouseCode {
            get;
            set;
        }
        [DataMemberAttribute()]
        public System.String CDCReturnWarehouseName {
            get;
            set;
        }
        [DataMemberAttribute()]
        public System.String CDCRWCompanyCode {
            get;
            set;
        }
        [DataMemberAttribute()]
        public System.String Code {
            get;
            set;
        }
        [DataMemberAttribute()]
        public System.String CreatorName {
            get;
            set;
        }
        [DataMemberAttribute()]
        public System.String CustomerContactPerson {
            get;
            set;
        }
        [DataMemberAttribute()]
        public System.String CustomerContactPhone {
            get;
            set;
        }
        [DataMemberAttribute()]
        public System.String FaultyPartsCode {
            get;
            set;
        }
        [DataMemberAttribute()]
        public System.String FaultyPartsName {
            get;
            set;
        }
        [DataMemberAttribute()]
        public System.String FaultyPartsSupplierCode {
            get;
            set;
        }
        [DataMemberAttribute()]
        public System.String FaultyPartsSupplierName {
            get;
            set;
        }
        [DataMemberAttribute()]
        public System.String MalfunctionDescription {
            get;
            set;
        }
        [DataMemberAttribute()]
        public System.String ModifierName {
            get;
            set;
        }
        [DataMemberAttribute()]
        public System.String PartsRetailOrderCode {
            get;
            set;
        }
        [DataMemberAttribute()]
        public System.String PartsSalesOrderCode {
            get;
            set;
        }
        [DataMemberAttribute()]
        public System.String Path {
            get;
            set;
        }
        [DataMemberAttribute()]
        public System.String RDCReceiptSituation {
            get;
            set;
        }
        [DataMemberAttribute()]
        public System.String RejectName {
            get;
            set;
        }
        [DataMemberAttribute()]
        public System.String RejectReason {
            get;
            set;
        }
        [DataMemberAttribute()]
        public System.String Remark {
            get;
            set;
        }
        [DataMemberAttribute()]
        public System.String RepairOrderCode {
            get;
            set;
        }
        [DataMemberAttribute()]
        public System.String ReturnWarehouseCode {
            get;
            set;
        }
        [DataMemberAttribute()]
        public System.String ReturnWarehouseName {
            get;
            set;
        }
        [DataMemberAttribute()]
        public System.String RWarehouseCompanyCode {
            get;
            set;
        }
        [DataMemberAttribute()]
        public System.String SalesWarehouseCode {
            get;
            set;
        }
        [DataMemberAttribute()]
        public System.String SalesWarehouseName {
            get;
            set;
        }
        [DataMemberAttribute()]
        public System.String RWarehouseCompanyName {
            get;
            set;
        }
        [DataMemberAttribute()]
        public System.String ReturnReason {
            get;
            set;
        }
        [DataMemberAttribute()]
        public System.String AbandonerReason {
            get;
            set;
        }
        [DataMemberAttribute()]
        public System.String StopReason {
            get;
            set;
        }
        [DataMemberAttribute()]
        public System.String ShippingCode {
            get;
            set;
        }
        [DataMemberAttribute()]
        public System.String CenterPartsSalesOrderCode {
            get;
            set;
        }

        [DataMemberAttribute()]
        public System.Boolean IsUplodFile {
            get;
            set;
        }
    }
}
