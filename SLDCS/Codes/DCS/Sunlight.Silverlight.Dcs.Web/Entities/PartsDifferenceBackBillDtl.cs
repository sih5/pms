﻿using System;
using System.Collections.Generic;
using System.Data.Objects.DataClasses;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class PartsDifferenceBackBillDtl : EntityObject {
        [DataMember]
        public int OnlyDiffQuantity {
            get;
            set;
        }
    }
}
