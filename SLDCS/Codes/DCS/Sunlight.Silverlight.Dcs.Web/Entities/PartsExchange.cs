﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Sunlight.Silverlight.Dcs.Web {
    
    public partial class PartsExchange {
        /// <summary>
        /// 互换分组号
        /// </summary>
        [DataMemberAttribute]
        public string ExGroupCode {
            get;
            set;
        }
    }
}
