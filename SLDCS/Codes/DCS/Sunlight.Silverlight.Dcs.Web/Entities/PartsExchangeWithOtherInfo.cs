﻿using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class PartsExchangeWithOtherInfo : EntityObject {
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }

        [Key]
        [DataMemberAttribute]
        public int PartId {
            get;
            set;
        }

        [Key]
        [DataMemberAttribute]
        public int SparePartId {
            get;
            set;
        }

        [Key]
        [DataMemberAttribute]
        public int PartsSalesCategoryId {
            get;
            set;
        }

        [Key]
        [DataMemberAttribute, Display(ResourceType = typeof(EntityStrings), Name = "SparePart_Code")]
        public string Code {
            get;
            set;
        }

        [Key]
        [DataMemberAttribute, Display(ResourceType = typeof(EntityStrings), Name = "SparePart_Name")]
        public string Name {
            get;
            set;
        }

        [DataMemberAttribute, Display(ResourceType = typeof(EntityStrings), Name = "PartsExchange_ExchangeCode")]
        public string ExchangeCode {
            get;
            set;
        }

        [DataMemberAttribute, Display(ResourceType = typeof(EntityStrings), Name = "PartsSalesCategory_Name")]
        public string PartsSalesCategoryName {
            get;
            set;
        }

        [DataMemberAttribute, Display(ResourceType = typeof(EntityStrings), Name = "PartsBranch_IsOrderable")]
        public bool IsOrderable {
            get;
            set;
        }

        [DataMemberAttribute, Display(ResourceType = typeof(EntityStrings), Name = "PartsBranch_IsSalable")]
        public bool? IsSalable {
            get;
            set;
        }

        [DataMemberAttribute, Display(ResourceType = typeof(EntityStrings), Name = "PartsBranch_IsDirectSupply")]
        public bool IsDirectSupply {
            get;
            set;
        }

        [DataMemberAttribute, Display(ResourceType = typeof(EntityStrings), Name = "PartsBranch_PartsReturnPolicy")]
        public int PartsReturnPolicy {
            get;
            set;
        }


        [DataMemberAttribute, Display(ResourceType = typeof(EntityStrings), Name = "SparePart_Feature")]
        public string Feature {
            get;
            set;
        }
    }
}
