﻿using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 配件历史库存
    /// </summary>
    public partial class PartsHistoryStock {
        /// <summary>
        /// 库位号
        /// </summary>
        [DataMemberAttribute]
        public string WarehouseAreaCode {
            get;
            set;
        }

    }
}
