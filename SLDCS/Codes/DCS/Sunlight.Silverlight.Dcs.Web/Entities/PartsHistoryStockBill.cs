﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    public class PartsHistoryStockBill {

        /// <summary>
        /// key
        /// </summary>
        [Key]
        public int Id {
            get;
            set;
        }
        /// <summary>
        /// 分公司
        /// </summary>
        [DataMember]
        public string BranchName {
            get;
            set;
        }
        /// <summary>
        /// 仓库名称
        /// </summary>
        [DataMember]
        public string WarehouseName {
            get;
            set;
        }
        /// <summary>
        /// 仓库编号
        /// </summary>
        [DataMember]
        public string WarehouseCode {
            get;
            set;
        }
        /// <summary>
        /// 品牌
        /// </summary>
        [DataMember]
        public string PartsSalesCategoryName {
            get;
            set;
        }
        /// <summary>
        /// 仓储企业类型
        /// </summary>
        [DataMember]
        public int StorageCompanyType {
            get;
            set;
        }
        /// <summary>
        /// 仓储企业名称
        /// </summary>
        [DataMember]
        public string CompanyName {
            get;
            set;
        }

        /// <summary>
        /// 仓储企业编号
        /// </summary>
        [DataMember]
        public string CompanyCode {
            get;
            set;
        }
        /// <summary>
        /// 计划价金额
        /// </summary>
        [DataMember]
        public decimal? PlannedPriceAmount {
            get;
            set;
        }
        /// <summary>
        /// 创建时间
        /// </summary>
        [DataMember]
        public DateTime? CreateTime {
            get;
            set;
        }

        /// <summary>
        /// 数量
        /// </summary>
        [DataMember]
        public int Quantity {
            get;
            set;
        }
        /// <summary>
        /// 仓库Id
        /// </summary>
        [DataMember]
        public int WarehouseId {
            get;
            set;
        }

        /// <summary>
        /// 快照日期
        /// </summary>
        [DataMember]
        public DateTime? SnapshoTime {
            get;
            set;
        }

        /// <summary>
        /// 配件销售类型id
        /// </summary>
        [DataMember]
        public int? PartsSalesCategoryId {
            get;
            set;
        }

    }
}
