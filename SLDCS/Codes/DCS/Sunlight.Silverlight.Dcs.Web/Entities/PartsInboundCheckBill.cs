﻿using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class PartsInboundCheckBill {
        /// <summary>
        /// 采购订单类型
        /// </summary>
        [DataMemberAttribute]
        public string PartsPurchaseOrderType {
            get;
            set;
        }

        /// <summary>
        /// 库区
        /// </summary>
        [DataMemberAttribute]
        public string WarehouseAreaCode {
            get;
            set;
        }

        /// <summary>
        /// 打印审核人
        /// </summary>
        [DataMemberAttribute]
        public string PrintApproverName {
            get;
            set;
        }


        /// <summary>
        /// 计划来源
        /// </summary>
        [DataMemberAttribute]
        public string PlanSource {
            get;
            set;
        }

        /// <summary>
        /// 采购订单类型名称
        /// </summary>
        [DataMemberAttribute]
        public string PartsPurchaseOrderTypeName {
            get;
            set;
        }
    }
}
