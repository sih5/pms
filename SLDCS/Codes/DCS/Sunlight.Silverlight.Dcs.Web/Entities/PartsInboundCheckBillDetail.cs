﻿using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class PartsInboundCheckBillDetail {

        /// <summary>
        /// 打印次数
        /// </summary>
        [DataMemberAttribute]
        public int? PrintNumber {
            get;
            set;
        }


        /// <summary>
        /// 计量单位
        /// </summary>
        [DataMemberAttribute]
        public string Unit {
            get;
            set;
        }

        /// <summary>
        /// 目标库位
        /// </summary>
        [DataMemberAttribute]
        public string DestWarehouseAreaCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string SupplierPartCode {
            get;
            set;
        }

        /// <summary>
        /// 本次收货量
        /// </summary>
        [DataMemberAttribute]
        [Display(Name = "PartsInboundCheckBillDetail_ThisInspectedQuantity", ResourceType = typeof(Resources.EntityStrings))]
        public int? ThisInspectedQuantity {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? TraceProperty {
            get;
            set;
        }
        [DataMemberAttribute]
        public string TraceCode {
            get;
            set;
        }
    }
}
