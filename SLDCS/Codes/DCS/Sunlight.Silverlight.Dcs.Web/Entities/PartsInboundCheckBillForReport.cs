﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    //入库检验单
    [DataContract(IsReference = true)]
    public class PartsInboundCheckBillForReport : EntityObject {
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }
        //入库单号
        [DataMemberAttribute]
        public string Code {
            get;
            set;
        }
        //供应商名称
        [DataMemberAttribute]
        public string PartsSupplierName {
            get;
            set;
        }
        //供应商编码
        [DataMemberAttribute]
        public string PartsSupplierCode {
            get;
            set;
        }
        //供应商图号
        [DataMemberAttribute]
        public string SupplierPartCode {
            get;
            set;
        }
        //配件编号
        [DataMemberAttribute]
        public string SparePartCode {
            get;
            set;
        }
        //配件名称
        [DataMemberAttribute]
        public string SparePartName {
            get;
            set;
        }
        //红岩号
        [DataMemberAttribute]
        public string ReferenceCode {
            get;
            set;
        }
        //备件属性
        [DataMemberAttribute]
        public int PartABC {
            get;
            set;
        }
        //入库数量
        [DataMemberAttribute]
        public int InspectedQuantity {
            get;
            set;
        }
        //入库时间
        [DataMemberAttribute]
        public DateTime? CreateTime {
            get;
            set;
        }
        //开票价格
        [DataMemberAttribute]
        public decimal? SettlementPrice {
            get;
            set;
        }
        //采购订单编号
        [DataMemberAttribute]
        public string PartsPurchaseOrderCode {
            get;
            set;
        }
        //采购订单生成时间
        [DataMemberAttribute]
        public DateTime? PartsPurchaseOrderDate {
            get;
            set;
        }
        //订单类型采购订单类型
        [DataMemberAttribute]
        public string PartsPurchaseOrderType {
            get;
            set;
        }
        //结算单号PartsPurchaseSettle.BillCode
        [DataMemberAttribute]
        public string SettleBillCode {
            get;
            set;
        }
        //结算单状态PartsPurchaseSettleBill.Status
        [DataMemberAttribute]
        public int? SettleBillStatus {
            get;
            set;
        }
        //Sap凭证编号
        [DataMemberAttribute]
        public string SapCode {
            get;
            set;
        }
        /// <summary>
        /// 合同价格
        /// </summary>
        [DataMember]
        public decimal PurchasePrice {
            get;
            set;
        }
        /// <summary>
        /// 价差
        /// </summary>
        [DataMember]
        public decimal DifferPurchasePrice {
            get;
            set;
        }
        /// <summary>
        /// 合同价格
        /// </summary>
        [DataMember]
        public decimal DifferPurchasePriceAll {
            get;
            set;
        }
        /// <summary>
        /// 入库单备注
        /// </summary>
        [DataMember]
        public string PartsInboundCheckBillRemark {
            get;
            set;
        }
        /// <summary>
        /// 采购单备注
        /// </summary>
        [DataMember]
        public string PartsPurchaseOrderRemark {
            get;
            set;
        }
        //金额
        [DataMemberAttribute]
        public decimal? SettlementPriceSum {
            get;
            set;
        }
        /// <summary>
        /// 发票
        /// </summary>
        [DataMember]
        public string Invoice {
            get;
            set;
        }
        //分销中心
        [DataMember]
        public string MarketingDepartmentName {
            get;
            set;
        }
        //仓库名称
        [DataMemberAttribute]
        public string WarehouseName {
            get;
            set;
        }
        //仓库名称
        [DataMemberAttribute]
        public string WarehouseCode {
            get;
            set;
        }
        //仓库Id
        [DataMemberAttribute]
        public int WarehouseId {
            get;
            set;
        }
        //省份
        [DataMemberAttribute]
        public string Province {
            get;
            set;
        }
        //客户销售订单号
         [DataMemberAttribute]
        public string PartsSalesOrderCode {
            get;
            set;
        }
        //处理单编号
        [DataMemberAttribute]
        public string PartsSalesOrderProcessCode {
            get;
            set;
        }
        //订单类型
        [DataMemberAttribute]
        public string PartsSalesOrderTypeName {
            get;
            set;
        }
        //单据类型
        [DataMemberAttribute]
        public int? OriginalRequirementBillType {
            get;
            set;
        }     
        //建议售价
        [DataMemberAttribute]
        public decimal OriginalPrice {
            get;
            set;
        }
        //建议售价总价
        [DataMemberAttribute]
        public decimal OriginalPriceSum {
            get;
            set;
        }
        //价格属性
        [DataMemberAttribute]
        public string GroupName {
            get;
            set;
        }
        //价格属性
        [DataMemberAttribute]
        public int? InboundType {
            get;
            set;
        }
         [DataMemberAttribute]
        public decimal CostPrice {
            get;
            set;
        }
         [DataMemberAttribute]
         public decimal CostPriceAll {
             get;
             set;
         }
         [DataMemberAttribute]
         public string CounterpartCompanyName {
             get;
             set;
         }
    }
}
