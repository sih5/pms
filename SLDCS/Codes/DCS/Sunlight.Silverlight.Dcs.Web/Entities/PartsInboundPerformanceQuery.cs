﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class PartsInboundPerformanceQuery : EntityObject {
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }
        /// <summary>
        /// 上架单号
        /// </summary>
        [DataMemberAttribute]
        public String Code {
            get;
            set;
        }
        /// <summary>
        /// 库位
        /// </summary>
        [DataMemberAttribute]
        public String WarehouseAreaCode {
            get;
            set;
        }
        /// <summary>
        /// 库区
        /// </summary>
        [DataMemberAttribute]
        public String ParentCode {
            get;
            set;
        }
        /// <summary>
        /// 数量
        /// </summary>
        [DataMemberAttribute]
        public int? Qty {
            get;
            set;
        }
        /// <summary>
        /// 操作人名称
        /// </summary>

        [DataMemberAttribute]
        public string OperaterName {
            get;
            set;
        }
        /// <summary>
        /// 操作时间
        /// </summary>        
        [DataMemberAttribute]
        public DateTime? OperaterEndTime {
            get;
            set;
        }
        /// <summary>
        /// 操作设备
        /// </summary>
        [DataMemberAttribute]
        public int? Equipment {
            get;
            set;
        }
        /// <summary>
        /// 配件Id
        /// </summary>
        [DataMemberAttribute]
        public int? SparePartId {
            get;
            set;
        }
        /// <summary>
        /// 配件编号
        /// </summary>
        [DataMemberAttribute]
        public string SpareCode {
            get;
            set;
        }
        /// <summary>
        /// 配件编号
        /// </summary>
        [DataMemberAttribute]
        public string SpareName {
            get;
            set;
        }
    }
}
