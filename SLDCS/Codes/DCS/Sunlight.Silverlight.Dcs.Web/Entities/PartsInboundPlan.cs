﻿using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class PartsInboundPlan {

        /// <summary>
        /// 采购订单类型
        /// </summary>
        [DataMemberAttribute]
        public string PartsPurchaseOrderType {
            get;
            set;
        }

        /// <summary>
        /// 品牌
        /// </summary>
        [DataMemberAttribute]
        public string PartsSalesCategorieName {
            get;
            set;
        }


        /// <summary>
        /// 计划来源
        /// </summary>
        [DataMemberAttribute]
        public string PlanSource {
            get;
            set;
        }

        /// <summary>
        /// 采购订单类型名称
        /// </summary>
        [DataMemberAttribute]
        public string PartsPurchaseOrderTypeName {
            get;
            set;
        }

        /// <summary>
        /// 退货类型
        /// </summary>
        [DataMemberAttribute]
        public int ReturnType {
            get;
            set;
        }


        /// <summary>
        /// 销售订单编号
        /// </summary>
        [DataMemberAttribute]
        public string PartsSalesOrderCode {
            get;
            set;
        }

        /// <summary>
        /// 配件编号 
        /// </summary>
        [DataMemberAttribute]
        public string SparePartCode {
            get;
            set;
        }

        /// <summary>
        /// 配件编号 
        /// </summary>
        [DataMemberAttribute]
        public string SparePartName {
            get;
            set;
        }
         [DataMemberAttribute]
        public decimal TotalAmountForWarehouse
        {
            get;
            set;
        }
        /// <summary>
        /// 是否有差异单
        /// </summary>
         [DataMemberAttribute]
        public bool HasDifference
        {
            get;
            set;
        }
    }
}
