﻿using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class PartsInboundPlanDetail {

        /// <summary>
        /// 打印次数
        /// </summary>
        [DataMemberAttribute]
        public int? PrintNumber {
            get;
            set;
        }
        /// <summary>
        /// 供应商图号
        /// </summary>
        [DataMemberAttribute]
        public string SupplierPartCode {
            get;
            set;
        }

        /// <summary>
        /// 计量单位
        /// </summary>
        [DataMemberAttribute]
        public string Unit {
            get;
            set;
        }

        /// <summary>
        /// 货位号
        /// </summary>
        [DataMemberAttribute]
        public string WarehouseAreaCode {
            get;
            set;
        }

        /// <summary>
        /// 序列号
        /// </summary>
        [DataMember]
        public string NumCode {
            get;
            set;
        }

        /// <summary>
        /// 英文名称
        /// </summary>
        [DataMember]
        public string EnglishName {
            get;
            set;
        }

        /// <summary>
        /// 规格型号
        /// </summary>
        [DataMember]
        public string Specification {
            get;
            set;
        }

        /// <summary>
        /// 产品执行标准代码
        /// </summary>
        [DataMember]
        public string StandardCode {
            get;
            set;
        }

        /// <summary>
        /// 产品执行标准名称
        /// </summary>
        [DataMember]
        public string StandardName {
            get;
            set;
        }

        /// <summary>
        /// 最小包装数量
        /// </summary>
        [DataMember]
        public int? MInPackingAmount {
            get;
            set;
        }
        /// <summary>
        /// 最小包装数量
        /// </summary>
        [DataMember]
        public int? TraceProperty {
            get;
            set;
        }
    }
}
