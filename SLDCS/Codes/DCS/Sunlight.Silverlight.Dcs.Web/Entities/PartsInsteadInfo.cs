﻿using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {

    [DataContract(IsReference = true)]
    public class PartsInsteadInfo : EntityObject {
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }

        /// <summary>
        /// 替互换配件编号
        /// </summary>
        [DataMemberAttribute]
        public string SparePartCode {
            get;
            set;
        }

        /// <summary>
        /// 替互换配件名称
        /// </summary>
        [DataMemberAttribute]
        public string SparePartName {
            get;
            set;
        }

        /// <summary>
        /// 替换类型
        /// </summary>
        [DataMemberAttribute]
        public int InsteadType {
            get;
            set;
        }

        /// <summary>
        /// 原件Id
        /// </summary>
        [DataMemberAttribute]
        public int OriginalSparePartId {
            get;
            set;
        }

        /// <summary>
        /// 供应商Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int SupplierId {
            get;
            set;
        }

        /// <summary>
        /// 供应商编号
        /// </summary>
        [DataMemberAttribute]
        public string SupplierCode {
            get;
            set;
        }


        /// <summary>
        /// 供应商名称
        /// </summary>
        [DataMemberAttribute]
        public string SupplierName {
            get;
            set;
        }
        /// <summary>
        /// 原配件数量
        /// </summary>
        [DataMemberAttribute]
        public string FormerPartsNumber {
            get;
            set;
        }
        /// <summary>
        /// 替互换配件数量
        /// </summary>
        [DataMemberAttribute]
        public string AlterPartsNumber {
            get;
            set;
        }

        /// <summary>
        /// 采购价
        /// </summary>
        [DataMemberAttribute]
        public decimal PurchasePrice {
            get;
            set;
        }

        /// <summary>
        /// 经销商库存数量
        /// </summary>
        [DataMemberAttribute]
        public int DealerPartStockQuantity {
            get;
            set;
        } 
    }
}
