﻿
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class PartsInventoryBill
    {

        [DataMemberAttribute]
        public int? ApprovalId
        {
            get;
            set;
        }

        /// <summary>
        /// 是否上传附件
        /// </summary>
        [DataMemberAttribute]
        public bool? IsUplodFile {
            get;
            set;
        }
      
    }
}
