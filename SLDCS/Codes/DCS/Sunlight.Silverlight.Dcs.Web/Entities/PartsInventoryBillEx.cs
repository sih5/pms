﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    [DataContractAttribute(IsReference = true)]
    public class PartsInventoryBillEx : EntityObject {
        [Key]
        [DataMemberAttribute]
        public Int32 Id {
            get;
            set;
        }



        [DataMemberAttribute]
        public String Code {
            get;
            set;
        }

        [DataMemberAttribute]
        public String RejectComment {
            get;
            set;
        }



        [DataMemberAttribute]
        public Int32 BranchId {
            get;
            set;
        }


        [DataMemberAttribute]
        public Int32 WarehouseAreaCategory {
            get;
            set;
        }


        [DataMemberAttribute]
        public Int32 WarehouseId {
            get;
            set;
        }




        [DataMemberAttribute]
        public String WarehouseCode {
            get;
            set;
        }



        [DataMemberAttribute]
        public String WarehouseName {
            get;
            set;
        }



        [DataMemberAttribute]
        public Int32 StorageCompanyId {
            get;
            set;
        }



        [DataMemberAttribute]
        public String StorageCompanyCode {
            get;
            set;
        }



        [DataMemberAttribute]
        public String StorageCompanyName {
            get;
            set;
        }




        [DataMemberAttribute]
        public Int32 StorageCompanyType {
            get;
            set;
        }




        [DataMemberAttribute]
        public String InitiatorName {
            get;
            set;
        }




        [DataMemberAttribute]
        public String InventoryReason {
            get;
            set;
        }




        [DataMemberAttribute]
        public Int32 Status {
            get;
            set;
        }




        [DataMemberAttribute]
        public String Remark {
            get;
            set;
        }




        [DataMemberAttribute]
        public int? CreatorId {
            get;
            set;
        }




        [DataMemberAttribute]
        public String CreatorName {
            get;
            set;
        }



        [DataMemberAttribute]
        public DateTime? CreateTime {
            get;
            set;
        }



        [DataMemberAttribute]
        public int? ModifierId {
            get;
            set;
        }




        [DataMemberAttribute]
        public String ModifierName {
            get;
            set;
        }




        [DataMemberAttribute]
        public DateTime? ModifyTime {
            get;
            set;
        }




        [DataMemberAttribute]
        public int? ApproverId {
            get;
            set;
        }




        [DataMemberAttribute]
        public String ApproverName {
            get;
            set;
        }




        [DataMemberAttribute]
        public DateTime? ApproveTime {
            get;
            set;
        }




        [DataMemberAttribute]
        public int? AbandonerId {
            get;
            set;
        }




        [DataMemberAttribute]
        public String AbandonerName {
            get;
            set;
        }




        [DataMemberAttribute]
        public DateTime? AbandonTime {
            get;
            set;
        }



        [DataMemberAttribute]
        public DateTime? RowVersion {
            get;
            set;
        }



        [DataMemberAttribute]
        public int? ResultInputOperatorId {
            get;
            set;
        }


        [DataMemberAttribute]
        public String ResultInputOperatorName {
            get;
            set;
        }



        [DataMemberAttribute]
        public DateTime? ResultInputTime {
            get;
            set;
        }


        [DataMemberAttribute]
        public int? InventoryRecordOperatorId {
            get;
            set;
        }



        [DataMemberAttribute]
        public String InventoryRecordOperatorName {
            get;
            set;
        }



        [DataMemberAttribute]
        public DateTime? InventoryRecordTime {
            get;
            set;

        }



        [DataMemberAttribute]
        public decimal? AmountDifference {
            get;
            set;
        }


        [DataMemberAttribute]
        public int? AccountingStatus {
            get;
            set;
        }

        /// <summary>
        /// 盘点前成本金额
        /// </summary>
        [DataMemberAttribute]
        public decimal SumCostBeforeInventory {
            get;
            set;
        }

        /// <summary>
        /// 盘点后成本金额
        /// </summary>
        [DataMemberAttribute]
        public decimal SumCostAfterInventory {
            get;
            set;
        }

        /// <summary>
        /// 盘点前后总成本差异总金额
        /// </summary>
        [DataMemberAttribute]
        public decimal SumCostDifference {
            get;
            set;
        }
        [DataMemberAttribute]
        public String CheckerName {
            get;
            set;
        }



        [DataMemberAttribute]
        public DateTime? CheckTime {
            get;
            set;
        }

        /// <summary>
        /// 是否上传附件
        /// </summary>
        [DataMemberAttribute]
        public bool? IsUplodFile {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? RejecterId {
            get;
            set;
        }

        [DataMemberAttribute]
        public String RejecterName {
            get;
            set;
        }

        [DataMemberAttribute]
        public DateTime? RejectTime {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? InitialApproverId {
            get;
            set;
        }

        [DataMemberAttribute]
        public String InitialApproverName {
            get;
            set;
        }

        [DataMemberAttribute]
        public DateTime? InitialApproveTime {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? AmountDifferenceRange {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? AdvancedAuditID {
            get;
            set;
        }

        [DataMemberAttribute]
        public String AdvancedAuditName {
            get;
            set;
        }

        [DataMemberAttribute]
        public DateTime? AdvancedAuditTime {
            get;
            set;
        }
    }
}
