﻿using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;


namespace Sunlight.Silverlight.Dcs.Web{
   public partial class PartsInventoryDetail {
       [DataMember]
       public int? StorageAfterInventoryNew {
           get;
           set;
       }
    }
}
