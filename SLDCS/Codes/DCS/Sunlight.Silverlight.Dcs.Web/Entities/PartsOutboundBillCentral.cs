﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;
namespace Sunlight.Silverlight.Dcs.Web {
    //销售订单完成情况统计
    [DataContract(IsReference = true)]

    public class PartsOutboundBillCentral : EntityObject {
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }
        [Key]
        [DataMemberAttribute]
        public int PartsOutboundBillId {
            get;
            set;
        }
        //分销中心
        [DataMemberAttribute]
        public string MarketingDepartmentName {
            get;
            set;
        }
        //中心库编号
        [DataMemberAttribute]
        public string CompanyCode {
            get;
            set;
        }
        //中心库名称
        [DataMemberAttribute]
        public string CompanyName {
            get;
            set;
        }


        //仓库名称
        [DataMemberAttribute]
        public string WarehouseName {
            get;
            set;
        }
        //仓库名称
        [DataMemberAttribute]
        public string WarehouseCode {
            get;
            set;
        }

        //省份
        [DataMemberAttribute]
        public string Province {
            get;
            set;
        }
        //客户编号
        [DataMemberAttribute]
        public string SubmitCompanyCode {
            get;
            set;
        }
        //客户名称
        [DataMemberAttribute]
        public string SubmitCompanyName {
            get;
            set;
        }
        //客户类型
        [DataMemberAttribute]
        public int CustomerTypeInt {
            get;
            set;
        }
        //客户类型
        [DataMemberAttribute]
        public string CustomerType {
            get;
            set;
        }
       
        //销售单号
        [DataMemberAttribute]
        public string PartsSalesOrderCode {
            get;
            set;
        } //处理单编号
        [DataMemberAttribute]
        public string PartsSalesOrderProcessCode {
            get;
            set;
        } //出库单号
        [DataMemberAttribute]
        public string Code {
            get;
            set;
        }
        //红岩号
        [DataMemberAttribute]
        public string ReferenceCode {
            get;
            set;
        }
        //配件编号
        [DataMemberAttribute]
        public string SparePartCode {
            get;
            set;
        }
        //配件名称
        [DataMemberAttribute]
        public string SparePartName {
            get;
            set;
        }
        //配件属性
        [DataMemberAttribute]
        public int? PartABC {
            get;
            set;
        }
        //订单类型
        [DataMemberAttribute]
        public string PartsSalesOrderTypeName {
            get;
            set;
        }
        //价格属性
        [DataMemberAttribute]
        public string GroupName {
            get;
            set;
        }
        //已出库数量 配件出库单清单.出库数量
        [DataMemberAttribute]
        public int? OutboundAmount {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal CostPrice {
            get;
            set;
        }
        //库存成本总额
        [DataMemberAttribute]
        public decimal CostPriceAmount {
            get;
            set;
        }
        //建议售价
        [DataMemberAttribute]
        public decimal OriginalPrice {
            get;
            set;
        }
        //建议售价总额
        [DataMemberAttribute]
        public decimal OriginalPriceAmount {
            get;
            set;
        }

        //折让金额 就是 原始价格 - 销售价
        [DataMemberAttribute]
        public decimal ConcessionPrice {
            get;
            set;
        }
        //折让比率=（原始金额 - 销售金额）/销售金额
        [DataMemberAttribute]
        public decimal ConcessionRate {
            get;
            set;
        }
        //经销价-结算价
        [DataMemberAttribute]
        public decimal SettlementPrice {
            get;
            set;
        }
        //经销价总额-结算价
        [DataMemberAttribute]
        public decimal SettlementPriceAmount {
            get;
            set;
        }
        //发运时间
        [DataMemberAttribute]
        public DateTime? ShippingDate {
            get;
            set;
        }
        //结算数量
        [DataMemberAttribute]
        public int QuantityToSettle {
            get;
            set;
        }
        //备注
        [DataMemberAttribute]
        public string Remark {
            get;
            set;
        }

        //结算状态
        [DataMemberAttribute]
        public int? SettlementStatus {
            get;
            set;
        }
        //供应商编号
        [DataMemberAttribute]
        public string PartsSupplierCode {
            get;
            set;
        }
        //供应商名称
        [DataMemberAttribute]
        public string PartsSupplierName {
            get;
            set;
        }
        //采购退货单编号
        [DataMemberAttribute]
        public string PartsPurReturnOrderCode {
            get;
            set;
        }
        //结算状态：是否合并结算
        [DataMemberAttribute]
        public string SettlementStatusReturn {
            get;
            set;
        }
        //出库时间
        [DataMemberAttribute]
        public DateTime? OutBoundTime {
            get;
            set;
        }
        //凭证号
        [DataMemberAttribute]
        public string VoucherNo {
            get;
            set;
        }
        //结算单号
        [DataMemberAttribute]
        public string SettlementNo {
            get;
            set;
        }
        public int? CustomerTypeTy
        {
            get;
            set;

        }
    }
}
