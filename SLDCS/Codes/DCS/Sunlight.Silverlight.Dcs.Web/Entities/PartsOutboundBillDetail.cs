﻿using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class PartsOutboundBillDetail {
        /// <summary>
        /// 可退货数量
        /// </summary>
        [DataMemberAttribute]
        public int CanReturnAmount {
            get;
            set;
        }

        /// <summary>
        /// 配件仓库管理粒度 
        /// </summary>
        [DataMemberAttribute]
        public int? PartsWarhouseManageGranularity {
            get;
            set;
        }

        /// <summary>
        /// 计量单位
        /// </summary>
        [DataMemberAttribute]
        public string Unit {
            get;
            set;
        }
    }
}
