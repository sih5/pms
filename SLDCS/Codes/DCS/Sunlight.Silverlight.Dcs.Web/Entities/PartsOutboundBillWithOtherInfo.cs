﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {

    [DataContract(IsReference = true)]
    public class PartsOutboundBillWithOtherInfo : EntityObject {
        [Key]
        [DataMemberAttribute]
        public int PartsOutboundBillId {
            get;
            set;
        }
        [Key]
        [DataMemberAttribute]
        public int PartsOutboundPlanId {
            get;
            set;
        }

        [Key]
        [DataMemberAttribute]
        public int PartsSalesCategoryId {
            get;
            set;
        }

        [DataMemberAttribute]
        public int CompanyId {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? PrintTimes {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? InvoiceType {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ERPSourceOrderCode {
            get;
            set;
        }


        [DataMemberAttribute]
        public decimal TotalAmount {
            get;
            set;
        }

        [DataMemberAttribute]
        public decimal? EcommerceMoney {
            get;
            set;
        }

        [DataMemberAttribute]
        public string Code {
            get;
            set;
        }

        [DataMemberAttribute]
        public string WarehouseCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string WarehouseName {
            get;
            set;
        }

        [DataMemberAttribute]
        public int WarehouseId {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? OutboundType {
            get;
            set;
        }

        [DataMemberAttribute]
        public string OriginalRequirementBillCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string PartsSalesOrderTypeName {
            get;
            set;
        }

        [DataMemberAttribute]
        public string PartsOutboundPlanCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string PartsSalesCategoryName {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? BranchId {
            get;
            set;
        }

        [DataMemberAttribute]

        public string BranchCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string BranchName {
            get;
            set;
        }

        [DataMemberAttribute]
        public int CounterPartCompanyId {
            get;
            set;
        }

        [DataMemberAttribute]
        public string CounterpartCompanyCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string CounterpartCompanyName {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? CompanyType {
            get;
            set;
        }

        [DataMemberAttribute]
        public int ReceivingCompanyId {
            get;
            set;
        }

        [DataMemberAttribute]
        public int StorageCompanyId {
            get;
            set;
        }

        [DataMemberAttribute]
        public string ReceivingWarehouseCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string ReceivingWarehouseName {
            get;
            set;
        }

        [DataMemberAttribute]
        public string OutboundPackPlanCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string ContractCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string GPMSPurOrderCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string InterfaceRecordId {
            get;
            set;
        }

        [DataMemberAttribute]
        public int SettlementStatus {
            get;
            set;
        }

        [DataMemberAttribute]
        public string Remark {
            get;
            set;
        }

        [DataMemberAttribute]
        public string OrderApproveComment {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? CreatorId {
            get;
            set;
        }

        [DataMemberAttribute]
        public string CreatorName {
            get;
            set;
        }

        [DataMemberAttribute]
        public DateTime? CreateTime {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? ModifierId {
            get;
            set;
        }

        [DataMemberAttribute]
        public string ModifierName {
            get;
            set;
        }

        [DataMemberAttribute]
        public DateTime? ModifyTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ProvinceName {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SAPPurchasePlanCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ZpNumber {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PWMSOutboundCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CPPartsPurchaseOrderCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CPPartsInboundCheckCode
        {
            get;
            set;
        }
        //供应商是否已结算
        [DataMemberAttribute]
        public bool IsSupplySettlement
        {
            get;
            set;
        }
        //货箱号
        [DataMemberAttribute]
        public string ContainerNumber
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SparePartCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SparePartName
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal TotalAmountForWarehouse
        {
            get;
            set;
        }
    }
}
