﻿using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;
namespace Sunlight.Silverlight.Dcs.Web {
    public partial class PartsOutboundPlan {
        [DataMember]
        public string SparePartCode
        {
            get;
            set;
        }     
        [DataMember]
        public string SparePartName
        {
            get;
            set;
        }
        [DataMember]
        public string PartsShippingOrderCode
        {
            get;
            set;
        }
        [DataMember]
        public bool? IsInternalAllocationBill {
            get;
            set;
        }
    }
}
