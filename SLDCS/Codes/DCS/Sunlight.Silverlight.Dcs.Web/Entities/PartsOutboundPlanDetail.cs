﻿
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class PartsOutboundPlanDetail {
        /// <summary>
        /// 库区库位编号
        /// </summary>
        [DataMember]
        public string WarehouseAreaCode {
            get;
            set;
        }

		/// <summary>
        /// 计量单位
        /// </summary>
        [DataMemberAttribute]
        public string Unit {
            get;
            set;
        }

    }
}
