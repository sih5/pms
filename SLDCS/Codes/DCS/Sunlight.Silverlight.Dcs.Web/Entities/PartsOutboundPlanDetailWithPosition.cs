﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    [DataContractAttribute(IsReference = true)]
    public class PartsOutboundPlanDetailWithPosition : EntityObject {
        ///<Summary>
        ///计划单Id
        ///</Summary>
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartsSalesCategoryName {
            get;
            set;
        }

        ///<Summary>
        ///配件销售订单类型名称
        ///</Summary>
        [DataMemberAttribute]
        public string PartsSalesOrderTypeName {
            get;
            set;
        }

        ///<Summary>
        ///收货仓库名称
        ///</Summary>
        [DataMemberAttribute]
        public string ReceivingWarehouseName {
            get;
            set;
        }
        ///<Summary>
        ///计划单编号
        ///</Summary>
        [DataMemberAttribute]
        public string Code {
            get;
            set;
        }

        ///<Summary>
        ///仓库编号
        ///</Summary>
        public string WarehouseCode {
            get;
            set;
        }

        ///<Summary>
        ///分公司名称
        ///</Summary>
        [DataMember]
        public string BranchName {
            get;
            set;
        }

        ///<Summary>
        ///对方单位名称
        ///</Summary>
        [DataMemberAttribute]
        public string CounterpartCompanyName {
            get;
            set;
        }

        ///<Summary>
        ///出库类型
        ///</Summary>
        [DataMemberAttribute]
        public int OutboundType {
            get;
            set;
        }

        ///<Summary>
        ///发运方式
        ///</Summary>
        [DataMemberAttribute]
        public int? ShippingMethod {
            get;
            set;
        }

        ///<Summary>
        ///源单据编号
        ///</Summary>
        [DataMemberAttribute]
        public string SourceCode {
            get;
            set;
        }

        ///<Summary>
        ///顶层库区
        ///</Summary>
        [DataMemberAttribute]
        public int TopLevelAreaId {
            get;
            set;
        }

        ///<Summary>
        ///顶层库区编号
        ///</Summary>
        [DataMemberAttribute]
        public string TopLevelAreaCode {
            get;
            set;
        }

        ///<Summary>
        ///仓储企业名称
        ///</Summary>
        [DataMemberAttribute]
        public string StorageCompanyName {
            get;
            set;
        }

        ///<Summary>
        ///创建人Id
        ///</Summary>
        [DataMemberAttribute]
        public int? CreatorId {
            get;
            set;
        }

        ///<Summary>
        ///创建人姓名
        ///</Summary>
        [DataMemberAttribute]
        public string CreatorName {
            get;
            set;
        }

        ///<Summary>
        ///创建日期
        ///</Summary>
        [DataMemberAttribute]
        public DateTime? CreateTime {
            get;
            set;
        }

        ///<Summary>
        ///配件Id
        ///</Summary>
        [DataMemberAttribute]
        public int SparePartId {
            get;
            set;
        }

        ///<Summary>
        ///库位Id
        ///</Summary>
        [DataMemberAttribute]
        public int PositionId {
            get;
            set;
        }

        ///<Summary>
        ///库位编号
        ///</Summary>
        [DataMemberAttribute]
        public string PositionCode {
            get;
            set;
        }

        ///<Summary>
        ///配件编号
        ///</Summary>
        [DataMemberAttribute]
        public string SparePartCode {
            get;
            set;
        }

        ///<Summary>
        ///配件名称
        ///</Summary>
        [DataMemberAttribute]
        public string SparePartName {
            get;
            set;
        }

        ///<Summary>
        ///出库满足数量
        ///</Summary>
        [DataMemberAttribute]
        public int? OutboundFulfillment {
            get;
            set;
        }

        ///<Summary>
        ///计划量
        ///</Summary>
        [DataMemberAttribute]
        public int? PlannedAmount {
            get;
            set;
        }

        ///<Summary>
        ///单位
        ///</Summary>
        [DataMemberAttribute]
        public string MeasureUnit {
            get;
            set;
        }

        ///<Summary>
        ///备注
        ///</Summary>
        [DataMemberAttribute]
        public string Remark {
            get;
            set;
        }

    }
}
