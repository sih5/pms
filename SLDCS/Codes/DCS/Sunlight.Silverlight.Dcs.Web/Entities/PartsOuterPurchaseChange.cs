﻿
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class PartsOuterPurchaseChange {
        [DataMember]
        public int? MarketDepartmentId {
            get;
            set;
        }

        [DataMember]
        public int? ApprovalId {
            get;
            set;
        }
    }
}
