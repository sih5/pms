﻿using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsOuterPurchaselist : EntityObject {
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int Number {
            get;
            set;
        }
    }
}
