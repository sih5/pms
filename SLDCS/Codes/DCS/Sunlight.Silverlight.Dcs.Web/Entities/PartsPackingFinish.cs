﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web
{
    [DataContract(IsReference = true)]

    public class PartsPackingFinish : EntityObject
    {
        [Key]
        [DataMemberAttribute]
        public int Id
        {
            get;
            set;
        }
        //采购订单编号
        [DataMemberAttribute]
        public string PartsPurchaseOrderCode
        {
            get;
            set;
        }
        //入库单号
        [DataMemberAttribute]
        public string PartsInboundCheckBillCode
        {
            get;
            set;
        }
        //仓库ID
        [DataMemberAttribute]
        public int? WarehouseId
        {
            get;
            set;
        }
        //仓库名称
        [DataMemberAttribute]
        public string WarehouseName
        {
            get;
            set;
        }
        //红岩号
        [DataMemberAttribute]
        public string ReferenceCode
        {
            get;
            set;
        }
        //配件编号
        [DataMemberAttribute]
        public string SparePartCode
        {
            get;
            set;
        }
        //供应商图号
        [DataMemberAttribute]
        public string SupplierPartCode
        {
            get;
            set;
        }
        //配件名称
        [DataMemberAttribute]
        public string SparePartName
        {
            get;
            set;
        }
        //配件属性
        [DataMemberAttribute]
        public int? PartABC
        {
            get;
            set;
        }
        //入库数量
        [DataMemberAttribute]
        public int? InspectedQuantity
        {
            get;
            set;
        }
        //已包装数量
        [DataMemberAttribute]
        public int? PackingQty
        {
            get;
            set;
        }
        //入库完成时间（检验单生成时间）
        [DataMemberAttribute]
        public DateTime? CheckBillCreateTime
        {
            get;
            set;
        }
        // 包装生成时间
        [DataMemberAttribute]
        public DateTime? CreateTime
        {
            get;
            set;
        }
        // 包装生成时间
        [DataMemberAttribute]
        public DateTime? ModifyTime
        {
            get;
            set;
        }
       
        // 建议售价
        [DataMemberAttribute]
        public decimal RetailGuidePrice
        {
            get;
            set;
        }
    }
}
