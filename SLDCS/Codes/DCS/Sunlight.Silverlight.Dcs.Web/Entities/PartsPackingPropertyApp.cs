﻿using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web
{
    public partial class PartsPackingPropertyApp : EntityObject
    {
        [DataMember]
        public int? ApprovalId
        {
            get;
            set;
        }
        [DataMember]
        public string PartsSalesCategoryName
        {
            get;
              set;
        }
        [DataMember]
        public int? FirId
        {
            get;
            set;
        }
        [DataMember]
        public int? SecId
        {
            get;
            set;
        }
        [DataMember]
        public int? ThidId
        {
            get;
            set;
        }    
        [DataMember]
        public string FirPackingCode
        {
            get;
            set;
        }
        [DataMember]
        public int? FirPackingType
        {
            get;
            set;
        }
        [DataMember]
        public int? SecPackingType
        {
            get;
            set;
        }
        [DataMember]
        public int? ThidPackingType
        {
            get;
            set;
        }
        [DataMember]
        public string FirMeasureUnit
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? FirPackingCoefficient
        {
            get;
            set;
        }
        [DataMember]
        public string FirPackingMaterial
        {
            get;
            set;
        }
        [DataMember]
        public double? FirVolume
        {
            get;
            set;
        }
        [DataMember]
        public double? FirWeight
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public double? FirLength
        {
            get;
            set;
        }
        [DataMember]
        public double? FirWidth
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public double? FirHeight
        {
            get;
            set;
        }
        [DataMember]
        public string ThidPackingCode
        {
            get;
            set;
        }
        [DataMember]
        public string ThidMeasureUnit
        {
            get;
            set;
        }
        [DataMember]
        public int? ThidPackingCoefficient
        {
            get;
            set;
        }
        [DataMember]
        public string ThidPackingMaterial
        {
            get;
            set;
        }
        [DataMember]
        public double? ThidVolume
        {
            get;
            set;
        }
        [DataMember]
        public double? ThidWeight
        {
            get;
            set;
        }
        [DataMember]
        public double? ThidLength
        {
            get;
            set;
        }
        [DataMember]
        public double? ThidWidth
        {
            get;
            set;
        }
        [DataMember]
        public double? ThidHeight
        {
            get;
            set;
        }
        [DataMember]
        public string SecPackingCode
        {
            get;
            set;
        }
        [DataMember]
        public string SecMeasureUnit
        {
            get;
            set;
        }
        [DataMember]
        public int? SecPackingCoefficient
        {
            get;
            set;
        }
        [DataMember]
        public string SecPackingMaterial
        {
            get;
            set;
        }
        [DataMember]
        public double? SecVolume
        {
            get;
            set;
        }
        [DataMember]
        public double? SecWeight
        {
            get;
            set;
        }
        [DataMember]
        public double? SecLength
        {
            get;
            set;
        }
        [DataMember]
        public double? SecWidth
        {
            get;
            set;
        }
        [DataMember]
        public double? SecHeight
        {
            get;
            set;
        }
         [DataMember]
        public string FirPackingMaterialName
        {
            get;
            set;
         }
         [DataMember]
        public string SecPackingMaterialName
        {
            get;
            set;
        }
         [DataMember]
        public string ThidPackingMaterialName
        {
            get;
            set;
        }
         [DataMember]
         public int? FirIsPrintPartStandard
         {
             get;
             set;
         }
         [DataMember]
         public int? SecIsPrintPartStandard
         {
             get;
             set;
         }
         [DataMember]
         public int? ThidIsPrintPartStandard
         {
             get;
             set;
         }
         [DataMember]
         public int? FirIsBoxStandardPrint
         {
             get;
             set;
         }
         [DataMember]
         public int? SecIsBoxStandardPrint
         {
             get;
             set;
         }
         [DataMember]
         public int? ThidIsBoxStandardPrint
         {
             get;
             set;
         }
    }
}
