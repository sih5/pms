﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Sunlight.Silverlight.Dcs.Web
{
    [DataContract(IsReference = true)]
    public class PartsPackingPropertyAppQuery : EntityObject
    {
        [Key]
        [DataMemberAttribute]
        public int Id
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int PartsSalesCategoryId
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int SparePartId
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SpareCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SihCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SpareName
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int Status
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ApprovalComment
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int MainPackingType
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int MInSalesAmount
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Remark
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ModifierName
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? ModifyTime
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public String CreatorName
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? CreateTime
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public String ApproverName
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? ApproveTime
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int FirPackingCoefficient
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int ThidPackingCoefficient
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int SecPackingCoefficient
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public String Code
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int OldMInPackingAmount {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal? RetailGuidePrice {
            get;
            set;
        }
        
    }
}
