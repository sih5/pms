﻿using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {

    [DataContract(IsReference = true)]
    public class PartsPlannedPriceWithStockQuantity : EntityObject {

        [Key]
        [DataMemberAttribute]
        public int SparePartId {
            get;
            set;
        }

        [DataMemberAttribute]
        public int TotalQuantity {
            get;
            set;
        }

        [DataMemberAttribute]
        public decimal PlannedPrice {
            get;
            set;
        }

    }
}
