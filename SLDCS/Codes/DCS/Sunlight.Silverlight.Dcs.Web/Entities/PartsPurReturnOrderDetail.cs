﻿using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class PartsPurReturnOrderDetail {
        //需求:根据分公司策略是否允许修改退货价格，用此虚拟字段实现校验退货价格是否修改
        /// <summary>
        ///校验退货价格
        /// </summary>
        [DataMemberAttribute]
        public decimal CheckUnitPrice {
            get;
            set;
        }
    }
}
