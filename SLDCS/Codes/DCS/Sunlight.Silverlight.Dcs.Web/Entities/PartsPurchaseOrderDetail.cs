﻿using System;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class PartsPurchaseOrderDetail {
        /// <summary>
        /// 配件采购清单
        /// </summary>
        [DataMemberAttribute]
        public int ShippingQuantity {
            get;
            set;
        }

        [DataMemberAttribute]
        public int InspectedQuantity {
            get;
            set;
        }
        [DataMemberAttribute]
        public int ReceiptAmount {
            get;
            set;
        }
        /// <summary>
        /// 原始配件Id(用于渠道变更操作时记录替换件的原始配件Id)
        /// </summary>
        [DataMemberAttribute]
        public int OriginalSparePartId {
            get;
            set;
        }
        /// <summary>
        /// 原始配件编号(用于渠道变更操作时记录替换件的原始配件Code)
        /// </summary>
        [DataMemberAttribute]
        public string OriginalSparePartCode {
            get;
            set;
        }
        /// <summary>
        /// 变更后配件编号
        /// </summary>
        [DataMemberAttribute]
        public string ChangedSparePartCode {
            get;
            set;
        }

        /// <summary>
        /// 最大订货量
        /// </summary>
        [DataMemberAttribute]
        public int? MaxOrderAmount {
            get;
            set;
        }

        [DataMemberAttribute]
        public string PartsSupplierCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartsSupplierName {
            get;
            set;
        }
        [DataMemberAttribute]
        public System.DateTime? RequestedDeliveryTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? PartsSupplierId {
            get;
            set;
        }

        /// <summary>
        /// 采购订单编号
        /// </summary>
        [DataMemberAttribute]
        public string PartsPurchaseOrderCode
        {
            get;
            set;
        }
        /// <summary>
        /// 采购订单类型
        /// </summary>
        [DataMemberAttribute]
        public int PartsPurchaseOrderTypeId
        {
            get;
            set;
        }
        /// <summary>
        /// 创建人
        /// </summary>
        [DataMemberAttribute]
        public string CreatorName
        {
            get;
            set;
        }
        /// <summary>
        /// 创建时间
        /// </summary>
        [DataMemberAttribute]
        public DateTime? CreateTime
        {
            get;
            set;
        }

        /// <summary>
        ///  本次发运数量
        /// </summary>
        [DataMemberAttribute]
        public int UnShippingAmount
        {
            get;
            set;
        }

        /// <summary>
        ///  是否直供
        /// </summary>
        [DataMemberAttribute]
        public bool IfDirectProvision
        {
            get;
            set;
        }
        //1.品牌
        public int? PartsSalesCategoryId
        {
            get;
            set;
        }
        public string PartsSalesCategoryName
        {
            get;
            set;
        }

        //2.发运方式
        public int? ShippingMethod
        {
            get;
            set;
        }
        //3.司机
        public string Driver
        {
            get;
            set;
        }
        //4.送货单号
        public string DeliveryBillNumber
        {
            get;
            set;
        }
        //5.电话
        public string Phone
        {
            get;
            set;
        }
        //6.物流公司
        public string LogisticCompany
        {
            get;
            set;
        }
        //7.车牌号
        public string VehicleLicensePlate
        {
            get;
            set;
        }
        //8.备注
        public string ShippingRemark
        {
            get;
            set;
        }
        //9.要求到货时间
        public DateTime? ShippingRequestedDeliveryTime
        {
            get;
            set;
        }
        //10.预计到货时间
        public DateTime? PlanDeliveryTime
        {
            get;
            set;
        }
        //订单类型名称
         public string PartsPurchaseOrderTypeName
        {
            get;
            set;
        }
        //发运方式名称
         public string ShippingMethodName
         {
             get;
             set;
         }
         [DataMemberAttribute]
         public string TraceCode {
             get;
             set;
         }
    }
}