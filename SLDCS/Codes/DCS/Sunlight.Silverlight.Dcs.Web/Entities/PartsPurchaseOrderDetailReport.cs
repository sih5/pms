﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class PartsPurchaseOrderDetailReport : EntityObject {
        [Key]
        [DataMemberAttribute]
        public int Rownum {
            get;
            set;
        }
         [DataMemberAttribute]
        public DateTime PriorityCreateTime {
            get;
            set;
        }
         [DataMemberAttribute]
        public string PartsPurchaseOrderCode {
            get;
            set;
        }
         [DataMemberAttribute]
         public string InboundPlanCode {
             get;
             set;
         }
         [DataMemberAttribute]
         public string SparePartCode {
             get;
             set;
         }
         [DataMemberAttribute]
         public string SparePartName {
             get;
             set;
         }
        [DataMemberAttribute]
         public int Priority {
             get;
             set;
         }
         [DataMemberAttribute]
        public string SupplierPartCode {
            get;
            set;
        }
         [DataMemberAttribute]
         public int ShippingAmount {
             get;
             set;
         }
         [DataMemberAttribute]
         public int InspectedQuantity {
             get;
             set;
         }
         [DataMemberAttribute]
         public int ShallInBoundQty {
             get;
             set;
         }
         [DataMemberAttribute]
         public int ShallInboundEntries {
             get;
             set;
         }
         [DataMemberAttribute]
         public int ActualInboundEntries {
             get;
             set;
         }
         [DataMemberAttribute]
         public DateTime? ShippingDate {
             get;
             set;
         }
         [DataMemberAttribute]
         public DateTime? PlanDeliveryTime {
             get;
             set;
         }
         [DataMemberAttribute]
         public DateTime? InboundFinishTime {
             get;
             set;
         }
         [DataMemberAttribute]
         public int? InboundFinishStatus {
             get;
             set;
         }
        [DataMemberAttribute]
         public string SupplierName {
             get;
             set;
         }
        [DataMemberAttribute]
        public DateTime? Createtime {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ReferenceCode {
            get;
            set;
        }
    }
}
