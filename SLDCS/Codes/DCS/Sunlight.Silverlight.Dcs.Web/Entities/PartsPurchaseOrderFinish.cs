﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class PartsPurchaseOrderFinish : EntityObject {
        [Key]
        [DataMemberAttribute]
        public int Rownum {
            get;
            set;
        }
        //月份
        [DataMemberAttribute]
        public string Month {
            get;
            set;
        }
        //日期
        [DataMemberAttribute]
        public string Day {
            get;
            set;
        }
        //总条目数
        [DataMemberAttribute]
        public int AllItem {
            get;
            set;
        }
        //采购订单到期总条目数
        [DataMemberAttribute]
        public int ArriveItem {
            get;
            set;
        }
        //采购订单按期完成条目数
        [DataMemberAttribute]
        public int OnTime {
            get;
            set;
        }
        //采购订单超期完成条目数
        [DataMemberAttribute]
        public int OutTime {
            get;
            set;
        }
        //采购订单超期7天内未完成条目数
        [DataMemberAttribute]
        public int SevenUn {
            get;
            set;
        }
        //采购订单超期14天内未完成条目数
        [DataMemberAttribute]
        public int EightUn {
            get;
            set;
        }
        //采购订单超期21天内未完成条目数
        [DataMemberAttribute]
        public int TwentyUn {
            get;
            set;
        }
        //采购订单超期21天以上未完成条目数
        [DataMemberAttribute]
        public int OverTwentyUn {
            get;
            set;
        }
        //采购订单未到期未完成条目数
        [DataMemberAttribute]
        public int Undue {
            get;
            set;
        }
        //采购订单未到期未完成条目数
        [DataMemberAttribute]
        public double Mzl {
            get;
            set;
        }
        //采购计划生成时间
        [DataMemberAttribute]
        public DateTime? PurchasePlanTime {
            get;
            set;
        }
        //理论到货时间
        [DataMemberAttribute]
        public DateTime? TheoryDeliveryTime {
            get;
            set;
        }
        //供应商名称
        [DataMemberAttribute]
        public string SupplierName {
            get;
            set;
        }
        //采购单号
        [DataMemberAttribute]
        public string PurchaseCode {
            get;
            set;
        }
        //采购计划单号
        [DataMemberAttribute]
        public string PurchasePlanCode {
            get;
            set;
        }
        //供应商 图号
        [DataMemberAttribute]
        public string SupplierPartCode {
            get;
            set;
        }
        //配件编号
        [DataMemberAttribute]
        public string SpareCode {
            get;
            set;
        }
        //红岩号
        [DataMemberAttribute]
        public string SihCode {
            get;
            set;
        }
        //物料号
        [DataMemberAttribute]
        public string MapName {
            get;
            set;
        }
        //配件属性
        [DataMemberAttribute]
        public int? PartABC {
            get;
            set;
        }
        //采购计划数量
        [DataMemberAttribute]
        public int PurchasePlanQty {
            get;
            set;
        }
        //供应商确认数量
        [DataMemberAttribute]
        public int SupplyConfirmQty {
            get;
            set;
        }
        //供应商确认原因
        [DataMemberAttribute]
        public string SupplyConfirmReason {
            get;
            set;
        }
        //供应商确认数量
        [DataMemberAttribute]
        public int SupplyShippingQty {
            get;
            set;
        }
        //入库数量
        [DataMemberAttribute]
        public int InboundQty {
            get;
            set;
        }
        //入库强制完成数量
        [DataMemberAttribute]
        public int InboundForceQty {
            get;
            set;
        }
        //在途数量
        [DataMemberAttribute]
        public int OnLineQty {
            get;
            set;
        }
        //采购订单生成时间
        [DataMemberAttribute]
        public DateTime? PurchaseCreateTime {
            get;
            set;
        }
        //供应商确认时间
        [DataMemberAttribute]
        public DateTime? SupplyConfirmTime {
            get;
            set;
        }
        //供应商发运时间
        [DataMemberAttribute]
        public DateTime? SupplyShippingTime {
            get;
            set;
        }
        //预计到货时间
        [DataMemberAttribute]
        public DateTime? ExpectDeliveryTime {
            get;
            set;
        }
        //入库时间
        [DataMemberAttribute]
        public DateTime? InboundTime {
            get;
            set;
        }
        //计划类型
        [DataMemberAttribute]
        public string PlanType {
            get;
            set;
        }
        //仓库名称
        [DataMemberAttribute]
        public string WarehouseName {
            get;
            set;
        }
        //供应商发货周期
        [DataMemberAttribute]
        public int SupplyShippingCycle {
            get;
            set;
        }
        //供应商物流周期
        [DataMemberAttribute]
        public int SupplyArrivalCycle {
            get;
            set;
        }


        //采购计划备注
        [DataMemberAttribute]
        public string PurchasePlanRemark {
            get;
            set;
        }
        //超期时限
        [DataMemberAttribute]
        public string OverTime {
            get;
            set;
        }
        //未到期预警状态
        [DataMemberAttribute]
        public string UndueTime {
            get;
            set;
        }
        //采购订单完成情况
        [DataMemberAttribute]
        public string FinishDescribe {
            get;
            set;
        }
        //欠货数量
        [DataMemberAttribute]
        public int OwningQty {
            get;
            set;
        }
        [DataMemberAttribute]
        public string KvFinishi {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? KvOverTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public string KvUndueTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public double Weight {
            get;
            set;
        }
        [DataMemberAttribute]
        public double Volume {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal Salesprice {
            get;
            set;
        }
        [DataMemberAttribute]
        public int StopAmount {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PersonName {
            get;
            set;
        }
    }
}
