﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class PartsPurchaseOrderFinishReport : EntityObject {
        [Key]
        [DataMemberAttribute]
        public int Rownum {
            get;
            set;
        }
        [DataMemberAttribute]
        public int PartId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? SupplyOnLineQty {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? OverdueOnLineQty {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? WaitInQty {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? WaitPackQty {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? WaitShelvesQty {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? OweOrderNum {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? OwePartNum {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? CreateTime {
            get;
            set;
        }
        //供应商图号
        [DataMemberAttribute]
        public string SupplierPartCode {
            get;
            set;
        }
        //红岩号
        [DataMemberAttribute]
        public string ReferenceCode {
            get;
            set;
        }
        //首选供应商名称
        [DataMemberAttribute]
        public string SupplierName {
            get;
            set;
        }
        //配件属性
        [DataMemberAttribute]
        public int? PartABC {
            get;
            set;
        }
        //日均销量
        [DataMemberAttribute]
        public Decimal? DailySaleNum {
            get;
            set;
        }
        //标准库存下限
        [DataMemberAttribute]
        public Decimal? QtyLowerLimit {
            get;
            set;
        }
        //双桥库实际可用库存
        [DataMemberAttribute]
        public int? ActualUseableQty {
            get;
            set;
        }
        //双桥库保底库存（标准）
        [DataMemberAttribute]
        public int? StockQty {
            get;
            set;
        }
        //优先级
        [DataMemberAttribute]
        public int? Priority {
            get;
            set;
        }
        //优先级生成时间
        [DataMemberAttribute]
        public DateTime? PriorityCreateTime {
            get;
            set;
        }
        //持续天数
        [DataMemberAttribute]
        public int? DurationDay {
            get;
            set;
        }
        //持续天数
        [DataMemberAttribute]
        public int? TotalDurativeDayNum {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? InboundFinishStatus {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SparePartCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? PlanDeliveryTime {
            get;
            set;
        }
       
         [DataMemberAttribute]
         public bool? IsOrderable {
             get;
             set;
         }
         [DataMemberAttribute]
         public bool? IsSalable {
             get;
             set;
         }
    }
}
