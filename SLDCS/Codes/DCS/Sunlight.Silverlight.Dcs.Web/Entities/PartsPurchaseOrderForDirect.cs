﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class PartsPurchaseOrderForDirect : EntityObject {
        [Key]
        [DataMemberAttribute]
        public int? Rownum {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SparePartCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SparePartName {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartsSupplierCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartsSupplierName {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Code {
            get;
            set;
        }
        //采购计划单据状态
        [DataMemberAttribute]
        public int? Status {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartsSalesOrderTypeName {
            get;
            set;
        }
        //采购计划备注
        [DataMemberAttribute]
        public string Remark {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PersonName {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CreatorName {
            get;
            set;
        }
        [DataMemberAttribute]
        public string OriginalRequirementBillCode {
            get;
            set;
        }
        //供应商图号
        [DataMemberAttribute]
        public string SupplierPartCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ABCStrategy {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? OrderAmount {
            get;
            set;
        }
        //供应商确认数量  采购订单清单.确认量
        [DataMemberAttribute]
        public int? ConfirmedAmount {
            get;
            set;
        }
        //供应商确认原因 采购订单清单.短供原因
        [DataMemberAttribute]
        public int? ShortSupReason {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? Quantity {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? ShipCon {
            get;
            set;
        }
        //入库数量 配件入库检验单清单.检验量
        [DataMemberAttribute]
        public int? InspectedQuantity {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? OrderShippingTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CloserName {
            get;
            set;
        }
        [DataMemberAttribute]
        public bool? IsTransSap {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? SubmitTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? ApproveTime {
            get;
            set;
        }
        //供应商发运时间  如果多次发运，取最后一次发运更新时间  供应商发运单最大时间
        [DataMemberAttribute]
        public DateTime? ShippingDate {
            get;
            set;
        }
        //预计到货时间 供应商发货时间+物流周期
        [DataMemberAttribute]
        public DateTime? ExpectedArrivalDate {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? ConfirmationTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? InStatus {
            get;
            set;
        }
        [DataMemberAttribute]
        public string LogisticCompany {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? ShippingMethod {
            get;
            set;
        }
        /// <summary>
        /// 发运单号
        /// </summary>
        [DataMemberAttribute]
        public string DeliveryBillNumber {
            get;
            set;
        }
        /// <summary>
        /// 配件公司入库时间
        /// </summary>
        [DataMemberAttribute]
        public DateTime? SIHInboundDate {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ReceivingCompanyName {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ReceivingAddress {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ContactPerson {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ContactPhone {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ShipRemark {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? FinishStatus {
            get;
            set;
        }
    }
}
