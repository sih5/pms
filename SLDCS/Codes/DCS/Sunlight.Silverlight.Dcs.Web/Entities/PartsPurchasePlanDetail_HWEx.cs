﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class PartsPurchasePlanDetail_HWEx : EntityObject {
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? PartsPurchasePlanId {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? PartId
        {
            get;
            set;
        }

        [DataMemberAttribute]
        public string PartCode
        {
            get;
            set;
        }

        [DataMemberAttribute]
        public string PartName {
            get;
            set;
        }

        [DataMemberAttribute]
        public string GPMSPartCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? PartsPurchasePlanQty {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? UntFulfilledQty {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? PurchaseQty {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? PurchaseConfimQty {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? TransferQty {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? TransferConfimQty {
            get;
            set;
        }

        [DataMemberAttribute]
        public decimal? UnitPrice {
            get;
            set;
        }

        [DataMemberAttribute]
        public string MeasureUnit {
            get;
            set;
        }

        [DataMemberAttribute]
        public string GPMSRowNum {
            get;
            set;
        }

        [DataMemberAttribute]
        public string OverseasSuplierCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? PartsSupplierId {
            get;
            set;
        }

        [DataMemberAttribute]
        public string PartsSupplierCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string PartsSupplierName {
            get;
            set;
        }

        [DataMemberAttribute]
        public string FaultReason {
            get;
            set;
        }

        [DataMemberAttribute]
        public string Remark {
            get;
            set;
        }

        [DataMemberAttribute]
        public string POCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string SuplierCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public DateTime? RequestedDeliveryTime {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? BJCDCStock {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? SDCDCStock {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? GDCDCStock {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? WarehouseId {
            get;
            set;
        }

        [DataMemberAttribute]
        public string WarehouseCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string WarehouseName {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? CanAnalyzeQty {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? EndAmount {
            get;
            set;
        }
    }
}
