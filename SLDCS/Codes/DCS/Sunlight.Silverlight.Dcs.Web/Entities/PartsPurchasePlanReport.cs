﻿using System;
using System.Data.Objects.DataClasses;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web
{
    [DataContract(IsReference = true)]
    public class PartsPurchasePlanReport : EntityObject
    {
        //采购计划单Id
        [Key]
        [DataMemberAttribute]
        public int? Id
        {
            get;
            set;
        }
        //采购单Id
        [Key]
        [DataMemberAttribute]
        public int? PartsPurchaseOrderId
        {
            get;
            set;
        }
        //配件Id
        [Key]
        [DataMemberAttribute]
        public int? SparePartId
        {
            get;
            set;
        }
        //采购计划编号
        [DataMemberAttribute]
        public string Code
        {
            get;
            set;
        }
        //采购订单编号
        [DataMemberAttribute]
        public string PartsPurchaseOrderCode
        {
            get;
            set;
        }
        //供应商图号
        [DataMemberAttribute]
        public string SupplierPartCode
        {
            get;
            set;
        }
        //配件编号
        [DataMemberAttribute]
        public string SparePartCode
        {
            get;
            set;
        }
        //红岩号
        [DataMemberAttribute]
        public string ReferenceCode
        {
            get;
            set;
        }
        //物料名称
        [DataMemberAttribute]
        public string SparePartName
        {
            get;
            set;
        }
        //配件属性
        [DataMemberAttribute]
        public int PartABC
        {
            get;
            set;
        }
        //采购计划数量
         [DataMemberAttribute]
        public int PlanAmount
        {
            get;
            set;
        }
         //供应商确认数量  采购订单清单.确认量
         [DataMemberAttribute]
         public int ConfirmedAmount
         {
             get;
             set;
         }
         //供应商确认原因 采购订单清单.短供原因
         [DataMemberAttribute]
         public int ShortSupReason
         {
             get;
             set;
         }
         //供应商发运数 采购订单清单.发运量
         [DataMemberAttribute]
         public int ShippingAmount
         {
             get;
             set;
         }
         //入库数量 配件入库检验单清单.检验量
         [DataMemberAttribute]
         public int InspectedQuantity
         {
             get;
             set;
         }
         //入库强制完成数量 入库计划清单.计划量-检验量  计划单状态为终止
         [DataMemberAttribute]
         public int CompletionQuantity
         {
             get;
             set;
         }
         //在途数量 供应商确认数量-入库数量-入库强制完成数量
         [DataMemberAttribute]
         public int OnWayNumber
         {
             get;
             set;
         }
        //采购订单生成时间
         [DataMemberAttribute]
         public DateTime? PartsPurchaseOrderDate
         {
             get;
             set;
         }
         //供应商确认时间 采购订单。确认时间 //暂无
         [DataMemberAttribute]
         public DateTime? ConfirmationDate
         {
             get;
             set;
         }
         //供应商发运时间  如果多次发运，取最后一次发运更新时间  供应商发运单最大时间
         [DataMemberAttribute]
         public DateTime? ShippingDate
         {
             get;
             set;
         }
         //预计到货时间 供应商发货时间+物流周期
         [DataMemberAttribute]
         public string ExpectedArrivalDate
         {
             get;
             set;
         }
         //入库检验单最大时间
         [DataMemberAttribute]
         public DateTime? PartsInboundCheckBillDate
         {
             get;
             set;
         }
         //计划类型 PartsPurchasePlan。PartsPlanTypeId
         [DataMemberAttribute]
         public int? PartsPlanTypeId
         {
             get;
             set;
         }
         //计划类型 PartsPurchasePlan。PartsPlanTypeId
         [DataMemberAttribute]
         public string PartsPlanTypeName
         {
             get;
             set;
         }
         //采购计划单据状态
         [DataMemberAttribute]
         public int? Status
         {
             get;
             set;
         }
         //采购订单单据状态
         [DataMemberAttribute]
         public int? PartsPurchaseOrderStatus
         {
             get;
             set;
         }
         //仓库
         [DataMemberAttribute]
         public string WarehouseName
         {
             get;
             set;
         }
         //计划下达人员
         [DataMemberAttribute]
         public string SubmitterName
         {
             get;
             set;
         }
         //供应商名称
         [DataMemberAttribute]
         public string PartsSupplierName
         {
             get;
             set;
         }
         //供应商发货日期
         [DataMemberAttribute]
         public int OrderCycle
         {
             get;
             set;
         }
         //供应商物流周期  BranchSupplierRelation.ArrivalCycle
         [DataMemberAttribute]
         public int ArrivalCycle
         {
             get;
             set;
         }
         //理论到货日期 采购订单生成时间+供应商发货周期+供应商物流周期
         [DataMemberAttribute]
         public string TheoreticalArrivalDate
         {
             get;
             set;
         }
         //入库状态  PartsPurchaseOrder.InStatus
         [DataMemberAttribute]
         public int InStatus
         {
             get;
             set;
         }
         //供应商物流状态 (供应商确认量与发运量比较：相同则供应商发运完成，否则供应商部分发运)
         [DataMemberAttribute]
         public string ShippingStatus
         {
             get;
             set;
         }
         //包材红岩号（最大层级）
         [DataMemberAttribute]
         public string PackingMaterialReF
         {
             get;
             set;
         }
         //包材编号（最大层级）
         [DataMemberAttribute]
         public string PackingMaterial
         {
             get;
             set;
         }
         //包材转换里
         [DataMemberAttribute]
         public int PackingCoefficient
         {
             get;
             set;
         }
         //采购计划备注
         [DataMemberAttribute]
         public string Remark
         {
             get;
             set;
         }
         //采购订单完成情况
         [DataMemberAttribute]
         public string CompletionSituation
         {
             get;
             set;
         }
         //采购计划生成时间
         [DataMemberAttribute]
         public DateTime? PartsPurchasePlanTime
         {
             get;
             set;
         }
         //在途数量是否为0
         [DataMemberAttribute]
         public bool IsOnWayNumber
         {
             get;
             set;
         }
         [DataMemberAttribute]
         public string PersonName {
             get;
             set;
         }
        [DataMemberAttribute]
         public DateTime? SubmitTime {
             get;
             set;
         }
    }
}
