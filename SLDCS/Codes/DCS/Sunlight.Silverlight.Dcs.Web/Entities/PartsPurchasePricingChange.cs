﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class PartsPurchasePricingChange
    {

        [DataMemberAttribute]
        public int? ApprovalId
        {
            get;
            set;
        }

        /// <summary>
        /// 新配件信息
        /// </summary>
        [DataMemberAttribute]
        public bool? IsNewPart
        {
            get;
            set;
        }
        /// <summary>
        /// 是首选供应商、不是最低采购价
        /// </summary>
        [DataMemberAttribute]
        public bool? IsPrimaryNotMinPurchasePrice
        {
            get;
            set;
        }
        /// <summary>
        /// 不是首选供应商、是最低采购价
        /// </summary>
        [DataMemberAttribute]
        public bool? NotPrimaryIsMinPurchasePrice
        {
            get;
            set;
        }
        /// <summary>
        /// 是首选供应商、是最低采购价
        /// </summary>
        [DataMemberAttribute]
        public bool? IsPrimaryIsMinPurchasePrice
        {
            get;
            set;
        }
        /// <summary>
        /// 不是首选供应商、不是最低采购价
        /// </summary>
        [DataMemberAttribute]
        public bool? NotPrimaryNotMinPurchasePrice
        {
            get;
            set;
        }

        
    }
}
