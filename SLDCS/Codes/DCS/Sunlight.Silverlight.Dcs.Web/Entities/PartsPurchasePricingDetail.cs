﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class PartsPurchasePricingDetail {
        /// <summary>
        /// 品牌
        /// </summary>
        [DataMemberAttribute]
        public string PartsSalesCategoryName {
            get;
            set;
        }

        /// <summary>
        /// 品牌ID
        /// </summary>
        [DataMemberAttribute]
        public int PartsSalesCategoryId {
            get;
            set;
        }

        /// <summary>
        /// 是否新配件
        /// </summary>
        [DataMemberAttribute]
        public bool? IsNewPart
        {
            get;
            set;
        }
    }
}
