﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web
{
    [DataContract(IsReference = true)]
    public class PartsPurchasePricingForQuery : EntityObject
    {

        [Key]
        [DataMemberAttribute]
        public int Id
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Code
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Name
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CompanyCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CompanyName
        {
            get;
            set;
        }

        [DataMemberAttribute]
        public decimal Purchaseprice
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? Createtime
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public bool Isprice
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public bool Isprimary
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? ValidTo
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? ModifyTime
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CreatorName
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ModifierName
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? PartCreateTime
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ReferenceCode
        {
            get;
            set;
        }
    }
}
