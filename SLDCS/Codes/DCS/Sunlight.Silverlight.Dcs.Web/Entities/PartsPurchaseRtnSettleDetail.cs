﻿
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class PartsPurchaseRtnSettleDetail {
        [DataMember]
        public string SupplierPartCode {
            get;
            set;
        }
    }
}
