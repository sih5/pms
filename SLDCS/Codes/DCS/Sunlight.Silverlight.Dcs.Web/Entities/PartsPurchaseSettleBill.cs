﻿using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class PartsPurchaseSettleBill {
        [DataMemberAttribute]
        public int? IsPassed {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal SumSettlementAmount {
            get;
            set;
        }
    }
}
