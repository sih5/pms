﻿
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 配件采购结算单关联单
    /// </summary>
    public partial class PartsPurchaseSettleRef {
        /// <summary>
        /// 计划金额
        /// </summary>
        public decimal PlannedAmount {
            get;
            set;
        }

        /// <summary>
        ///  采购退货单退货原因
        /// </summary>
        [DataMember]
        public string ReturnReason {
            get;
            set;
        }
        /// <summary>
        ///  原始采购订单编号
        /// </summary>
        [DataMemberAttribute]
        public string CPPartsPurchaseOrderCode
        {
            get;
            set;
        }
        /// <summary>
        ///  原始入库单编号
        /// </summary>
        [DataMemberAttribute]
        public string CPPartsInboundCheckCode
        {
            get;
            set;
        }
    }
}
