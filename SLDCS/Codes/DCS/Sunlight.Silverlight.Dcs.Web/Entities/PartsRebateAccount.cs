﻿using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 配件返利账户
    /// </summary>
    public partial class PartsRebateAccount {
        /// <summary>
        /// 可用额度
        /// </summary>
        [DataMemberAttribute]
        public decimal UseablePosition {
            get;
            set;
        }
    }
}