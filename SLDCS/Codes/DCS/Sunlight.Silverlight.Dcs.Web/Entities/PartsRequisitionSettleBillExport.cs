﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    [DataContractAttribute(IsReference = true)]
    public class PartsRequisitionSettleBillExport : EntityObject {
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }
        //结算单编号
        [DataMemberAttribute]
        public string Code {
            get;
            set;
        }
        //营销分公司
        [DataMemberAttribute]
        public int BranchId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string BranchName {
            get;
            set;
        }
        //品牌
        [DataMemberAttribute]
        public int PartsSalesCategoryId {
            get;
            set;
        }

        [DataMemberAttribute]
        public string PartsSalesCategoryName {
            get;
            set;
        }
        //部门

        [DataMemberAttribute]
        public int DepartmentId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string DepartmentName {
            get;
            set;
        }
        //结算类型
        [DataMemberAttribute, Display(ResourceType = typeof(EntityStrings), Name = "SsClaimSettlementBill_SettleType")]
        public int? SettleType {
            get;
            set;
        }
        //结算总金额
        [DataMemberAttribute, Display(ResourceType = typeof(EntityStrings), Name = "PartsPurchaseSettleBill_TotalSettlementAmount")]
        public decimal TotalSettlementAmount {
            get;
            set;
        }
        //状态
        [DataMemberAttribute, Display(ResourceType = typeof(EntityStrings), Name = "_Common_Status")]
        public int Status {
            get;
            set;
        }
        //备注
        [DataMemberAttribute, Display(ResourceType = typeof(EntityStrings), Name = "_Common_Remark")]
        public string Remark {
            get;
            set;
        }
        //创建人
        [DataMemberAttribute, Display(ResourceType = typeof(EntityStrings), Name = "_Common_CreatorName")]
        public string CreatorName {
            get;
            set;
        }
        //创建时间
        [DataMemberAttribute, Display(ResourceType = typeof(EntityStrings), Name = "_Common_CreateTime")]
        public DateTime? CreateTime {
            get;
            set;
        }
        //修改人
        [DataMemberAttribute, Display(ResourceType = typeof(EntityStrings), Name = "_Common_ModifierName")]
        public string ModifierName {
            get;
            set;
        }
        //修改时间
        [DataMemberAttribute, Display(ResourceType = typeof(EntityStrings), Name = "_Common_ModifyTime")]
        public DateTime? ModifyTime {
            get;
            set;
        }
        //审批人
        [DataMemberAttribute, Display(ResourceType = typeof(EntityStrings), Name = "_Common_ApproverName")]
        public string ApproverName {
            get;
            set;
        }
        //审批时间
        [DataMemberAttribute, Display(ResourceType = typeof(EntityStrings), Name = "_Common_ApproveTime")]
        public DateTime? ApproveTime {
            get;
            set;
        }
        //作废人
        [DataMemberAttribute, Display(ResourceType = typeof(EntityStrings), Name = "_Common_AbandonerName")]
        public string AbandonerName {
            get;
            set;
        }
        //作废时间
        [DataMemberAttribute, Display(ResourceType = typeof(EntityStrings), Name = "_Common_AbandonTime")]
        public DateTime? AbandonTime {
            get;
            set;
        }
        //配件领用结算关联单.源单据编号 as 关联单编号
        [DataMemberAttribute]
        public string SourceCode {
            get;
            set;
        }
        //配件领用结算关联单.源单据类型 as 单据类型
        [DataMemberAttribute]
        public int SourceType {
            get;
            set;
        }
        //出库单清单.配件编号
        [DataMemberAttribute]
        public string SparePartCode {
            get;
            set;
        }
        //出库单清单.配件名称
        [DataMemberAttribute]
        public string SparePartName {
            get;
            set;
        }
        //出库单清单.结算价 
        [DataMemberAttribute]
        public decimal SettlementPrice {
            get;
            set;
        }
        //出库单清单.出库数量 as 结算数量
        [DataMemberAttribute]
        public int OutboundAmount {
            get;
            set;
        }
        //出库单清单.结算价 *数量 as 结算金额
        [DataMemberAttribute]
        public decimal SettlementAmount {
            get;
            set;
        }

        //计划价合计
        [DataMemberAttribute,]
        public decimal PlannedPriceTotalAmount {
            get;
            set;
        }

        //计划价税额
        [DataMemberAttribute,]
        public decimal TotalTaxAmount {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? InvoiceDate
        {
            get;
            set;
        }
       [DataMemberAttribute]
        public int? Type {
            get;
            set;
        }
    }
}
