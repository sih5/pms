﻿
using System.Runtime.Serialization;
namespace Sunlight.Silverlight.Dcs.Web {
    public partial class PartsRequisitionSettleDetail {
        /// <summary>
        /// 结算金额
        /// </summary>
        [DataMemberAttribute]
        public decimal SettlementAmount {
            get;
            set;
        }
    }
}
