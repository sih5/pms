﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
     [DataContract(IsReference = true)]
    public class PartsSaleRtnInboundCheck : EntityObject {
         [Key]
         [DataMemberAttribute]
         public int Rownum {
             get;
             set;
         }
         //仓库ID：入库仓库
         [DataMemberAttribute]
         public int? WarehouseId {
             get;
             set;
         }
         //仓库名称
         [DataMemberAttribute]
         public string WarehouseName {
             get;
             set;
         }
         //仓库编号
         [DataMemberAttribute]
         public string WarehouseCode {
             get;
             set;
         }
         //省份
         [DataMemberAttribute]
         public string Province {
             get;
             set;
         }
         //客户编号
         [DataMemberAttribute]
         public string CounterpartCompanyCode {
             get;
             set;
         }
         //销售退货单
         [DataMemberAttribute]
         public string PartsSalesReturnBillCode {
             get;
             set;
         }
         ////销售退货单
         //[DataMemberAttribute]
         //public string PartsSalesReturnBillCode {
         //    get;
         //    set;
         //}
         //客户单位名称
         [DataMemberAttribute]
         public string CounterpartCompanyName {
             get;
             set;
         }
         //客户出库仓库
         [DataMemberAttribute]
         public string ReturnWarehouseCode {
             get;
             set;
         }
         //销售订单编号
         [DataMemberAttribute]
         public string PartsSalesOrderCode {
             get;
             set;
         }
         //入库单号
         [DataMemberAttribute]
         public string Code {
             get;
             set;
         }
         //配件编号
         [DataMemberAttribute]
         public string SparePartCode {
             get;
             set;
         }
         //配件名称
         [DataMemberAttribute]
         public string SparePartName {
             get;
             set;
         }
         //配件属性
         [DataMemberAttribute]
         public int? PartABC {
             get;
             set;
         }
         //结算状态
         [DataMemberAttribute]
         public int? SettlementStatus {
             get;
             set;
         }
         //折价前单价
         [DataMemberAttribute]
         public decimal OriginalPrice {
             get;
             set;
         }
         //折价前金额
         [DataMemberAttribute]
         public decimal OriginalPriceAll {
             get;
             set;
         }
         //折价后单价
         [DataMemberAttribute]
         public decimal SettlementPrice {
             get;
             set;
         }
         //折价后金额
         [DataMemberAttribute]
         public decimal SettlementPriceAll {
             get;
             set;
         }
         //折价系数
         [DataMemberAttribute]
         public decimal Discount {
             get;
             set;
         }
         //是否折价
         [DataMemberAttribute]
         public bool IsDiscount {
             get;
             set;
         }
         //价格类型
         [DataMemberAttribute]
         public string PriceType {
             get;
             set;
         }
         //入库成本单价
         [DataMemberAttribute]
         public decimal CostPrice {
             get;
             set;
         }
         //入库成本金额
         [DataMemberAttribute]
         public decimal CostPriceAll {
             get;
             set;
         }
         //结算单号
         [DataMemberAttribute]
         public string SettlementNo {
             get;
             set;
         }
         //SAP凭证号
         [DataMemberAttribute]
         public string SAPSysInvoiceNumber {
             get;
             set;
         }
         //发票号
         [DataMemberAttribute]
         public string InvoiceNumber {
             get;
             set;
         }
         //入库时间
         [DataMemberAttribute]
         public DateTime? CreateTime {
             get;
             set;
         }
         //创建人
         [DataMemberAttribute]
         public string CreatorName {
             get;
             set;
         }
         //是否开红票
         [DataMemberAttribute]
         public bool IsRed {
             get;
             set;
         }
         //分销中心
         [DataMemberAttribute]
         public string MarketingDepartmentName {
             get;
             set;
         }
          [DataMemberAttribute]
         public int InspectedQuantity {
             get;
             set;
         }
          [DataMemberAttribute]
          public string ReturnReason {
              get;
              set;
          }
    }
}
