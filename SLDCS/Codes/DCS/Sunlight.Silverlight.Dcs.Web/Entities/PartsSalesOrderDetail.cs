﻿using System.Runtime.Serialization;



namespace Sunlight.Silverlight.Dcs.Web {
    public partial class PartsSalesOrderDetail {
        /// <summary>
        /// 未满足数量
        /// </summary>
        [DataMemberAttribute]
        public int UnfulfilledQuantity {
            get;
            set;
        }

    }
}
