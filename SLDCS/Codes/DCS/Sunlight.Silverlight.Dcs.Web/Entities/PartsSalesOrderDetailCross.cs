﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
     [DataContract(IsReference = true)]
    public class PartsSalesOrderDetailCross : EntityObject {
        
        /// <summary>
        /// Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }
        [DataMemberAttribute]
        public int SparePartId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SparePartCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SparePartName {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartsSalesOrderCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public int OrderedQuantity {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? ApproveQuantity {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal OrderPrice {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal? SalesPrice {
            get;
            set;
        }
    }
}
