﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;
namespace Sunlight.Silverlight.Dcs.Web
{
    //销售订单完成情况统计
    [DataContract(IsReference = true)]
    public class PartsSalesOrderFinishReport : EntityObject
    {
        
        [DataMemberAttribute]
        public int Id
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int PartsSalesOrderId
        {
            get;
            set;
        }
        //分销中心
        [DataMemberAttribute]
        public string MarketingDepartmentName
        {
            get;
            set;
        }
        //仓库ID
        [DataMemberAttribute]
        public int? WarehouseId
        {
            get;
            set;
        }
        //仓库名称
        [DataMemberAttribute]
        public string WarehouseName
        {
            get;
            set;
        }
        //仓库编号
        [DataMemberAttribute]
        public string WarehouseCode {
            get;
            set;
        }
        //省份
        [DataMemberAttribute]
        public string Province
        {
            get;
            set;
        }
        //客户编号
        [DataMemberAttribute]
        public string SubmitCompanyCode
        {
            get;
            set;
        }
        //客户名称
        [DataMemberAttribute]
        public string SubmitCompanyName
        {
            get;
            set;
        }
        //客户类型
        [DataMemberAttribute]
        public int? SubmitCompanyType {
            get;
            set;
        }
        //红岩号
        [DataMemberAttribute]
        public string ReferenceCode
        {
            get;
            set;
        }
        //配件编号
        [DataMemberAttribute]
        public string SparePartCode
        {
            get;
            set;
        }
        //配件名称
        [DataMemberAttribute]
        public string SparePartName
        {
            get;
            set;
        }
        //英文名称
        [DataMemberAttribute]
        public string EnglishName
        {
            get;
            set;
        }
        //配件属性
        [DataMemberAttribute]
        public int? PartABC
        {
            get;
            set;
        }
        //配件类别
        [DataMemberAttribute]
        public int? PurchaseRoute
        {
            get;
            set;
        }
        //订货数量
        [DataMemberAttribute]
        public int? OrderedQuantity
        {
            get;
            set;
        }
        //本次满足率
        [DataMemberAttribute]
        public int? ApproveQuantity
        {
            get;
            set;
        }
        //未审核通过量(OrderedQuantity-ApproveQuantity)
        [DataMemberAttribute]
        public int? UnApproveQuantity
        {
            get;
            set;
        }
        //已拣货量
        [DataMemberAttribute]
        public int? PickingQty
        {
            get;
            set;
        }
        //已出库数量 配件出库单清单.出库数量
        [DataMemberAttribute]
        public int? OutboundAmount
        {
            get;
            set;
        }
        //本次满足量与出库量差异
        [DataMemberAttribute]
        public int? DifferenceQuantity
        {
            get;
            set;
        }
        //价格属性
        [DataMemberAttribute]
        public string GroupName
        {
            get;
            set;
        }
        //价格属性
        [DataMemberAttribute]
        public int? GroupNameId {
            get;
            set;
        }
        //单价（含税）
        [DataMemberAttribute]
        public decimal OrderPrice
        {
            get;
            set;
        }
        //金额（含税）
        [DataMemberAttribute]
        public decimal OrderSum
        {
            get;
            set;
        }
        //建议售价
        [DataMemberAttribute]
        public decimal OriginalPrice
        {
            get;
            set;
        }
        //建议售价总金额
        [DataMemberAttribute]
        public decimal OriginalPriceSum
        {
            get;
            set;
        }
        //重量
        [DataMemberAttribute]
        public float Weight
        {
            get;
            set;
        }
        //体积
        [DataMemberAttribute]
        public float Volume
        {
            get;
            set;
        }
        //销售订单编号
        [DataMemberAttribute]
        public string Code
        {
            get;
            set;
        }

        //处理单编号
        [DataMemberAttribute]
        public string PartsSalesOrderProcessCode
        {
            get;
            set;
        }
        //出库单编号
        [DataMemberAttribute]
        public string PartsOutboundBillCode
        {
            get;
            set;
        }
        //销售单状态
        [DataMemberAttribute]
        public int? Status
        {
            get;
            set;
        }
        //出库计划单状态
        [DataMemberAttribute]
        public int? PartsOutboundPlanStatus
        {
            get;
            set;
        }
        //销售订单审核时间
        [DataMemberAttribute]
        public DateTime? ApproveTime
        {
            get;
            set;
        }
        //销售订单备注
        [DataMemberAttribute]
        public string Remark
        {
            get;
            set;
        }
        //拣货时间（拣货完成时间）
        [DataMemberAttribute]
        public DateTime? PickingFinishTime
        {
            get;
            set;
        }
        //发运时间  配件发运单.发运日期
        [DataMemberAttribute]
        public DateTime? ShippingDate
        {
            get;
            set;
        }
        //订单类型
        [DataMemberAttribute]
        public string PartsSalesOrderTypeName
        {
            get;
            set;
        }
        //订单类型
        [DataMemberAttribute]
        public int PartsSalesOrderTypeId {
            get;
            set;
        }
        //合同号
        [DataMemberAttribute]
        public string ContractCode
        {
            get;
            set;
        }
        //订单创建时间
        [DataMemberAttribute]
        public DateTime? CreateTime
        {
            get;
            set;
        }
        //采购订单编号
        [DataMemberAttribute]
        public string PartsPurchaseOrderCode
        {
            get;
            set;
        }
        //中心库编号
        [DataMemberAttribute]
        public string CompanyCode {
            get;
            set;
        }
        //中心库名称
        [DataMemberAttribute]
        public string CompanyName {
            get;
            set;
        }
        //出库时间
         [DataMemberAttribute]
        public DateTime? PartsOutboundBillTime {
            get;
            set;
        }
         //关联中心库，非质量件，虚拟库的保管区库存
         [DataMemberAttribute]
         public int Actualqty {
             get;
             set;
         }
         //是否属欠货
        [DataMemberAttribute]
         public bool? Owning {
             get;
             set;
         }         
         //是否替换
        [DataMemberAttribute]
         public bool? IsReplace {
             get;
             set;
         }
        //库存成本EnterprisePartsCost.CostPrice
         [DataMemberAttribute]
        public decimal CostPrice {
            get;
            set;
        }
         //库存成本总额
         [DataMemberAttribute]
         public decimal CostPriceAll {
             get;
             set;
         }
         //客户类型
         [DataMemberAttribute]
         public int? CustomerType {
             get;
             set;
         }
         //客户类型
         [DataMemberAttribute]
         public string CustomerTypeStr {
             get;
             set;
         }
         /// <summary>
         /// 经销价
         /// </summary>
         [DataMemberAttribute]
         public Decimal SalesPrice {
             get;
             set;
         }
         /// <summary>
         /// 经销价
         /// </summary>
         [DataMemberAttribute]
         public Decimal SalesPriceAll {
             get;
             set;
         }
         /// <summary>
         /// 中心库价
         /// </summary>
         [DataMemberAttribute]
         public Decimal? Settlementprice {
             get;
             set;
         }
         /// <summary>
         /// 中心库价
         /// </summary>
         [DataMemberAttribute]
         public Decimal? SettlementpriceAll {
             get;
             set;
         }
        //已结算数量
         [DataMemberAttribute]
         public int QuantityToSettle {
             get;
             set;
         }
        //结算状态
         [DataMemberAttribute]
         public int? SettlementStatus {
             get;
             set;
         }
        //折让金额
         [DataMemberAttribute]
         public decimal DiscountAmount {
             get;
             set;
         }
         //折让比率
         [DataMemberAttribute]
         public decimal DiscountAmountRate {
             get;
             set;
         }
        //SAP凭证号
        [DataMemberAttribute]
         public string SapCode {
             get;
             set;
         }
        //发票号
        [DataMemberAttribute]
        public string Invoice {
            get;
            set;
        }
        //替换件SAP物料号
        [DataMemberAttribute]
        public string ReplaceCode {
            get;
            set;
        }
        //是否欠货补发
        [DataMemberAttribute]
        public bool? IsReplenishment {
            get;
            set;
        }
        //要求到货时间
        [DataMemberAttribute]
        public DateTime? RequestedDeliveryTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ShippingCode {
            get;
            set;
        }
        [Key]
        [DataMemberAttribute]
        public int Rownum {
            get;
            set;
        }
        //是否直供
        [DataMemberAttribute]
        public bool IfDirectProvision {
            get;
            set;
        }
        //处理方式
        [DataMemberAttribute]
        public int? OrderProcessMethod {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SettlementCode {
            get;
            set;
        }
         [DataMemberAttribute]
        public int Time {
            get;
            set;
        }
         [DataMemberAttribute]
         public DateTime? FirstApproveTime {
             get;
             set;
         }
         [DataMemberAttribute]
         public string StatusStr {
             get;
             set;
         }
         [DataMemberAttribute]
         public string PartsOutboundPlanStatusStr {
             get;
             set;
         }
        [DataMemberAttribute]
         public string ReceivingCompanyName {
             get;
             set;
         }
        [DataMemberAttribute]
        public DateTime? ShippingCreatTime {
            get;
            set;
        }
         [DataMemberAttribute]
        public int? RetailStatus {
            get;
            set;
        }
         [DataMemberAttribute]
         public string SalesUnitOwnerCompanyName {
             get;
             set;
         }
          [DataMemberAttribute]
         public DateTime? SubmitTime {
             get;
             set;
         }
    }
}
