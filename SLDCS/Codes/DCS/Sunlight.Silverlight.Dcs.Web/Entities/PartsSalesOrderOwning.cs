﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;
namespace Sunlight.Silverlight.Dcs.Web {
    //中心库欠货台账
    [DataContract(IsReference = true)]
    public class PartsSalesOrderOwning : EntityObject {
        //分销中心
        [Key]
        [DataMemberAttribute]
        public string MarketingDepartmentName {
            get;
            set;
        }
        //客户编号
        [Key]
        [DataMemberAttribute]
        public string SubmitCompanyCode {
            get;
            set;
        }
        //客户名称
        [Key]
        [DataMemberAttribute]
        public string SubmitCompanyName {
            get;
            set;
        }
        //红岩号
        [Key]
        [DataMemberAttribute]
        public string ReferenceCode {
            get;
            set;
        }
        //配件编号
        [Key]
        [DataMemberAttribute]
        public string SparePartCode {
            get;
            set;
        }
        //配件名称
        [Key]
        [DataMemberAttribute]
        public string SparePartName {
            get;
            set;
        }
        //中心库名称
        [Key]
        [DataMemberAttribute]
        public string CompanyName {
            get;
            set;
        }
        //未审核通过量(OrderedQuantity-ApproveQuantity)
        [DataMemberAttribute]
        public int? UnApproveQuantity {
            get;
            set;
        }
    }
}
