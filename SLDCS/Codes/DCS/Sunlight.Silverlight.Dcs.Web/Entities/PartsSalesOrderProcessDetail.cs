﻿using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {

    public partial class PartsSalesOrderProcessDetail {

        /// <summary>
        /// 调出价格
        /// </summary>
        [DataMemberAttribute]
        public decimal TransferPrice {
            get;
            set;
        }
    }
}
