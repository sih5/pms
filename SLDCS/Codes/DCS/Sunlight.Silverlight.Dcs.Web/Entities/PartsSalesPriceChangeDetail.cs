﻿using System;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 配件企业库存
    /// </summary>
    public partial class PartsSalesPriceChangeDetail : EntityObject {
        /// <summary>
        /// 参考价
        /// </summary>
        [DataMemberAttribute]
        public Decimal ReferencePrice {
            get;
            set;
        }

        /// <summary>
        /// 采购价
        /// </summary>
        [DataMemberAttribute]
        public Decimal PurchasePrice {
            get;
            set;
        }

        /// <summary>
        /// 销售价加价率
        /// </summary>
        [DataMemberAttribute]
        public Decimal IncreaseRateSale {
            get;
            set;
        }
        /// <summary>
        /// 品牌Id
        /// </summary>
        [DataMemberAttribute]
        public int PartsSalesCategoryId {
            get;
            set;
        }

        [DataMemberAttribute]
        public string PartsSalesCategoryCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartsSalesCategoryName {
            get;
            set;
        }




        /// <summary>
        /// 零售价加价率
        /// </summary>
        [DataMemberAttribute]
        public Decimal IncreaseRateRetail {
            get;
            set;
        }

        /// <summary>
        /// 批发价加价率
        /// </summary>
        [DataMemberAttribute]
        public Decimal IncreaseRateDealerSalesPrice {
            get;
            set;
        }

        /// <summary>
        /// 原销售价格（配件销售价表中的销售价格）
        /// </summary>
        [DataMemberAttribute]
        public decimal PartsSalesPrice_SalePrice {
            get;
            set;
        }
        /// <summary>
        ///原零售指导价（配件零售指导价表中的 零售指导价）
        /// </summary>
        [DataMemberAttribute]
        public decimal PartsRetailGuidePrice_RetailGuidePrice {
            get;
            set;
        }

    }
}
