﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 配件销售价和零售指导价
    /// </summary>
    [DataContract(IsReference = true)]
    public class PartsSalesPriceWithRetailGuidePrice : EntityObject {
        /// <summary>
        /// Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }

        /// <summary>
        /// 营销分公司Id
        /// </summary>
        [DataMemberAttribute]
        public int BranchId {
            get;
            set;
        }

        /// <summary>
        /// 配件销售类型Id
        /// </summary>
        [DataMemberAttribute]
        public int PartsSalesCategoryId {
            get;
            set;
        }

        /// <summary>
        /// 销售类型编号
        /// </summary>
        [DataMemberAttribute]
        public string PartsSalesCategoryCode {
            get;
            set;
        }

        /// <summary>
        /// 销售类型名称
        /// </summary>
        [DataMemberAttribute]
        public string PartsSalesCategoryName {
            get;
            set;
        }

        /// <summary>
        /// 配件Id
        /// </summary>
        [DataMemberAttribute]
        public int SparePartId {
            get;
            set;
        }

        /// <summary>
        /// 配件编号
        /// </summary>
        [DataMemberAttribute]
        public string SparePartCode {
            get;
            set;
        }

        /// <summary>
        /// 配件名称
        /// </summary>
        [DataMemberAttribute]
        public string SparePartName {
            get;
            set;
        }

        /// <summary>
        /// 是否用于索赔
        /// </summary>
        [DataMemberAttribute]
        public bool? IfClaim {
            get;
            set;
        }

        /// <summary>
        /// 经销价
        /// </summary>
        [DataMemberAttribute]
        public Decimal SalesPrice {
            get;
            set;
        }
        /// <summary>
        /// 中心库价
        /// </summary>
        [DataMemberAttribute]
        public Decimal? CenterPrice {
            get;
            set;
        }
        /// <summary>
        /// 零售指导价
        /// </summary>
        [DataMemberAttribute]
        public Decimal? RetailGuidePrice {
            get;
            set;
        }

        /// <summary>
        /// 价格类型
        /// </summary>
        [DataMemberAttribute]
        public int PriceType {
            get;
            set;
        }

        /// <summary>
        /// 状态
        /// </summary>
        [DataMemberAttribute]
        public int Status {
            get;
            set;
        }

        /// <summary>
        /// 生效时间
        /// </summary>
        [DataMemberAttribute]
        public DateTime? ValidationTime {
            get;
            set;
        }

        /// <summary>
        /// 失效时间
        /// </summary>
        [DataMemberAttribute]
        public DateTime? ExpireTime {
            get;
            set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        [DataMemberAttribute]
        public string Remark {
            get;
            set;
        }

        /// <summary>
        /// 创建人Id
        /// </summary>
        [DataMemberAttribute]
        public int? CreatorId {
            get;
            set;
        }

        /// <summary>
        /// 创建人
        /// </summary>
        [DataMemberAttribute]
        public string CreatorName {
            get;
            set;
        }

        /// <summary>
        /// 创建时间
        /// </summary>
        [DataMemberAttribute]
        public DateTime? CreateTime {
            get;
            set;
        }

        /// <summary>
        /// 修改人Id
        /// </summary>
        [DataMemberAttribute]
        public int? ModifierId {
            get;
            set;
        }

        /// <summary>
        /// 修改人
        /// </summary>
        [DataMemberAttribute]
        public string ModifierName {
            get;
            set;
        }

        /// <summary>
        /// 修改时间
        /// </summary>
        [DataMemberAttribute]
        public DateTime? ModifyTime {
            get;
            set;
        }

        /// <summary>
        /// 审批人Id
        /// </summary>
        [DataMemberAttribute]
        public int? ApproverId {
            get;
            set;
        }

        /// <summary>
        /// 审批人
        /// </summary>
        [DataMemberAttribute]
        public string ApproverName {
            get;
            set;
        }

        /// <summary>
        /// 审批时间
        /// </summary>
        [DataMemberAttribute]
        public DateTime? ApproveTime {
            get;
            set;
        }

        /// <summary>
        /// 作废人Id
        /// </summary>
        [DataMemberAttribute]
        public int? AbandonerId {
            get;
            set;
        }

        /// <summary>
        /// 作废人
        /// </summary>
        [DataMemberAttribute]
        public string AbandonerName {
            get;
            set;
        }

        /// <summary>
        /// 作废时间
        /// </summary>
        [DataMemberAttribute]
        public DateTime? AbandonTime {
            get;
            set;
        }
    }
}
