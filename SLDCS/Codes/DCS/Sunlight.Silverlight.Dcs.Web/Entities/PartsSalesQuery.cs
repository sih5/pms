﻿using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web.Entities {
    /// <summary>
    /// 配件销售在途查询
    /// </summary>
    [DataContract(IsReference = true)]
    public class PartsSalesQuery : EntityObject {
        /// <summary>
        /// 配件发运单id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }

        /// <summary>
        /// 分公司Id
        /// </summary>
        [DataMemberAttribute]
        public int BranchId {
            get;
            set;
        }

        /// <summary>
        /// 分公司编号
        /// </summary>
        [DataMemberAttribute]
        public string BranchCode {
            get;
            set;
        }


        /// <summary>
        /// 仓库id
        /// </summary>
        [DataMemberAttribute]
        public int? WarehouseId {
            get;
            set;
        }

        /// <summary>
        /// 仓库名称
        /// </summary>
        [DataMemberAttribute]
        public string WarehouseName {
            get;
            set;
        }

        /// <summary>
        /// 仓库编号
        /// </summary>
        [DataMemberAttribute]
        public string WarehouseCode {
            get;
            set;
        }

        /// <summary>
        /// 收货单位Id
        /// </summary>
        [DataMemberAttribute]
        public int ReceivingCompanyId {
            get;
            set;
        }

        /// <summary>
        /// 收货单位名称
        /// </summary>
        [DataMemberAttribute]
        public string ReceivingCompanyName {
            get;
            set;
        }

        /// <summary>
        /// 收货单位编号
        /// </summary>
        [DataMemberAttribute]
        public string ReceivingCompanyCode {
            get;
            set;
        }


        /// <summary>
        /// 配件id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int SparePartId {
            get;
            set;
        }
        /// <summary>
        /// 配件图号
        /// </summary>
        [DataMemberAttribute]
        public string SparePartCode {
            get;
            set;
        }
        /// <summary>
        /// 配件名称
        /// </summary>
        [DataMemberAttribute]
        public string SparePartName {
            get;
            set;
        }

        /// <summary>
        /// 批发价
        /// </summary>
        [DataMemberAttribute]
        public decimal SettlementPrice {
            get;
            set;
        }

        /// <summary>
        /// 数量
        /// </summary>
        [DataMemberAttribute]
        public int Qty {
            get;
            set;
        }

        /// <summary>
        /// 批发金额
        /// </summary>
        [DataMemberAttribute]
        public decimal TotalPrice {
            get;
            set;
        }
    }
}
