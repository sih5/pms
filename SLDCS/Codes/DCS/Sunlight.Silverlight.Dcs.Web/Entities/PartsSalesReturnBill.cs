﻿
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;
namespace Sunlight.Silverlight.Dcs.Web {
    public partial class PartsSalesReturnBill : EntityObject {
        /// <summary>
        /// 企业类型Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int ReturnCompanyType {
            get;
            set;
        }
        [DataMember]
        public int? ApprovalId
        {
            get;
            set;
        }

       

    }
}
