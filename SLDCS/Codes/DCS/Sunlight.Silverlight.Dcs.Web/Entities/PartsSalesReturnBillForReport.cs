﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;
namespace Sunlight.Silverlight.Dcs.Web {
    //销售退货完成情况统计
    [DataContract(IsReference = true)]
    class PartsSalesReturnBillForReport : EntityObject {
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }
        [Key]
        [DataMemberAttribute]
        public int PartsSalesReturnBillId {
            get;
            set;
        }
        //分销中心
        [DataMemberAttribute]
        public string MarketingDepartmentName {
            get;
            set;
        }
        //省份
        [DataMemberAttribute]
        public string Province {
            get;
            set;
        }
        //中心库名称
        [DataMemberAttribute]
        public string CompanyName {
            get;
            set;
        }
        //客户名称
        [DataMemberAttribute]
        public string SubmitCompanyName {
            get;
            set;
        }
        //配件编号
        [DataMemberAttribute]
        public string SparePartCode {
            get;
            set;
        }
        //配件名称
        [DataMemberAttribute]
        public string SparePartName {
            get;
            set;
        }
        //配件属性
        [DataMemberAttribute]
        public int? PartABC {
            get;
            set;
        }
        //状态
        [DataMemberAttribute]
        public int? Status {
            get;
            set;
        }
        //计划量
         [DataMemberAttribute]
        public int ReturnedQuantity {
            get;
            set;
        }
         //下达量
         [DataMemberAttribute]
         public int ApproveQuantity {
             get;
             set;
         }
         //检验数量
         [DataMemberAttribute]
         public int InspectedQuantity {
             get;
             set;
         }
         //入库包装数量
         [DataMemberAttribute]
         public int PackingQty {
             get;
             set;
         }
         //上架数量
         [DataMemberAttribute]
         public int ShelvesAmount {
             get;
             set;
         }
         //销售退货审核时间
         [DataMemberAttribute]
         public DateTime? ApproveTime {
             get;
             set;
         }
         //计划量与下达量差异
         [DataMemberAttribute]
         public int ApproveDifference {
             get;
             set;
         }
         //下达量与入库量差异
         [DataMemberAttribute]
         public int InspectedDifference {
             get;
             set;
         }
         //单价
         [DataMemberAttribute]
         public decimal? ReturnPrice {
             get;
             set;
         }
         //金额
         [DataMemberAttribute]
         public decimal? ReturnPriceAmount {
             get;
             set;
         }
         //入库计划单编号
         [DataMemberAttribute]
         public string PartsInboundPlanCode {
             get;
             set;
         }
         //入库时间
         [DataMemberAttribute]
         public DateTime? PartsInboundCheckBillDate {
             get;
             set;
         }
         //销售退货单编号
         [DataMemberAttribute]
         public string Code {
             get;
             set;
         }
         //销售订单编号
         [DataMemberAttribute]
         public string PartsSalesOrderCode {
             get;
             set;
         }
         //退货原因
         [DataMemberAttribute]
         public string ReturnReason {
             get;
             set;
         }
         //备注
         [DataMemberAttribute]
         public string Remark {
             get;
             set;
         }
    }
}
