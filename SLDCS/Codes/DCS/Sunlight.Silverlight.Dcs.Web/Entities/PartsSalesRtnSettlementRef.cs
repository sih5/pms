﻿
using System.Runtime.Serialization;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsSalesRtnSettlementRef {
        /// <summary>
        ///  销售退货单退货原因
        /// </summary>
        [DataMemberAttribute]
        public string ReturnReason {
            get;
            set;
        }

        /// <summary>
        ///  销售退货单备注
        /// </summary>
        [DataMemberAttribute]
        public string PartsSalesReturnBillRemark {
            get;
            set;
        }

        /// <summary>
        /// 退货类型
        /// </summary>
        [DataMemberAttribute]
        public int ReturnType {
            get;
            set;
        }
    }
}
