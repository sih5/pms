﻿using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class PartsSalesSettlementDetail {
        /// <summary>
        /// 计划来源
        /// </summary>
        [DataMemberAttribute]
        public string BillCode {
            get;
            set;
        }

    }
}
