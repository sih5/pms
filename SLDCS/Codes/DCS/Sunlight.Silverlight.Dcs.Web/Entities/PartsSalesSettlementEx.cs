﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class PartsSalesSettlementEx : EntityObject {
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Code {
            get;
            set;
        }
        [DataMemberAttribute]
        public int SettlementPath {
            get;
            set;
        }
        [DataMemberAttribute]
        public int SalesCompanyId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SalesCompanyCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SalesCompanyName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? WarehouseId {
            get;
            set;
        }
        [DataMemberAttribute]
        public int PartsSalesCategoryId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartsSalesCategoryName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int AccountGroupId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string AccountGroupCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string AccountGroupName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int CustomerCompanyId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CustomerCompanyCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CustomerCompanyName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int CustomerAccountId {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal TotalSettlementAmount {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? RebateMethod {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal? RebateAmount {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal InvoiceAmountDifference {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? OffsettedSettlementBillId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string OffsettedSettlementBillCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public double TaxRate {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal Tax {
            get;
            set;
        }
        [DataMemberAttribute]
        public int Status {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Remark {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? CreatorId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CreatorName {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? CreateTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? ModifierId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ModifierName {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? ModifyTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? AbandonerId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string AbandonerName {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? AbandonTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? InvoiceRegistrationOperatorId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string InvoiceRegistrationOperator {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? InvoiceRegistrationTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? ApproverId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ApproverName {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? ApproveTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal PlannedPriceTotalAmount {
            get;
            set;
        }
        [DataMemberAttribute]
        public string BusinessCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public bool? IsUnifiedSettle
        {
            get;
            set;
        }
        
        [DataMemberAttribute]
        public DateTime? InvoiceDate
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? SettleType
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? BusinessType
        {
            get;
            set;
        }

        //不含税金额
        [DataMemberAttribute]
        public decimal NoTaxAmount {
            get {
                return this.TotalSettlementAmount - this.Tax;

            }
        }
         [DataMemberAttribute]
        public string InvoiceCode {
            get;
            set;
        }
    }
}
