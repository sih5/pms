﻿
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 配件采购结算单关联单
    /// </summary>
    public partial class PartsSalesSettlementRef {
        /// <summary>
        ///  开票类型
        /// </summary>
        [DataMember]
        public int? InvoiceType {
            get;
            set;
        }
        /// <summary>
        /// 平台单号
        /// </summary>
        [DataMemberAttribute]
        public string ERPSourceOrderCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string InvoiceCustomerName {
            get;
            set;
        }

        /// <summary>
        /// 纳税人识别号
        /// </summary>
        [DataMemberAttribute]
        public string Taxpayeridentification {
            get;
            set;
        }

        /// <summary>
        /// 退货类型
        /// </summary>
        [DataMemberAttribute]
        public int ReturnType {
            get;
            set;
        }

        /// <summary>
        /// 经销商是否已结算
        /// </summary>
        [DataMemberAttribute]
        public string DealerIsSettlement
        {
            get;
            set;
        }

        /// <summary>
        /// 原始采购结算单号
        /// </summary>
        [DataMemberAttribute]
        public string PartsPurchaseSettleCode
        {
            get;
            set;
        }
         [DataMemberAttribute]
        public bool IsSett {
            get;
            set;
        }
    }
}
