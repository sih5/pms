﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class PartsShiftOrderDetail {
        [DataMemberAttribute]
        public  string  CourrentSIHCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CourrentDownSIHCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? CourrentUpShelfQty {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? CourrentDownShelfQty {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? TraceProperty {
            get;
            set;
        }
        //源库区
        [DataMemberAttribute]
        public string OrWarehouseAreaCode {
            get;
            set;
        }
        //目标库区
        [DataMemberAttribute]
        public string DeWarehouseAreaCode {
            get;
            set;
        }
    }
}
