﻿using System.Runtime.Serialization;
namespace Sunlight.Silverlight.Dcs.Web {
    public partial class PartsShippingOrder {
        public string ContainerNumber {
            get;
            set;
        }

        [DataMember]
        public string ContactPerson {
            get;
            set;
        }

        [DataMember]
        public string ContactPhone {
            get;
            set;
        }

        //[DataMember]
        //public string PartsSalesOrderTypeName {
        //    get;
        //    set;
        //}
    }
}
