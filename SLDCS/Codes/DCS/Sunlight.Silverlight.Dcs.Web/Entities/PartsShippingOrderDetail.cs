﻿
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class PartsShippingOrderDetail {
        /// <summary>
        ///  PO单号
        /// </summary>
        [DataMemberAttribute]
        public string POCode {
            get;
            set;
        }
    }
}
