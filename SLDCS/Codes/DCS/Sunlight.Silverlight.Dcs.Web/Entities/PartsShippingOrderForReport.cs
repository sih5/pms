﻿using System;
using System.Data.Objects.DataClasses;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;


namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class PartsShippingOrderForReport : EntityObject {
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }
        //发运日期
        [DataMemberAttribute]
        public DateTime? ShippingDate {
            get;
            set;
        }
        //出库仓库
        [DataMemberAttribute]
        public string WarehouseName {
            get;
            set;
        }
        //订单类型
        [DataMemberAttribute]
        public string PartsSalesOrderTypeName {
            get;
            set;
        }
        //发运单号
        [DataMemberAttribute]
        public string Code {
            get;
            set;
        }
        //发运方式
        [DataMemberAttribute]
        public int? ShippingMethod {
            get;
            set;
        }
        //收货单位
        [DataMemberAttribute]
        public string ReceivingCompanyName {
            get;
            set;
        }
        //收货地址
        [DataMemberAttribute]
        public string ReceivingAddress {
            get;
            set;
        }
        //收货人
        [DataMemberAttribute]
        public string LinkName {
            get;
            set;
        }
        //重量
        [DataMemberAttribute]
        public double? Weight {
            get;
            set;
        }
        //体积
        [DataMemberAttribute]
        public double? Volume {
            get;
            set;
        }
       
        //审单员
        [DataMemberAttribute]
        public string AppraiserName {
            get;
            set;
        }
        //销售单号
        [DataMemberAttribute]
        public string PartsSalesOrderCode {
            get;
            set;
        }
        //备注
        [DataMemberAttribute]
        public string Remark {
            get;
            set;
        }
         [DataMemberAttribute]
        public decimal SettlementPriceAll {
            get;
            set;
        }
        [DataMemberAttribute]
         public string LogisticCompanyName {
             get;
             set;
         }
        //原重量
        [DataMemberAttribute]
        public double? OldWeight {
            get;
            set;
        }
        //原体积
        [DataMemberAttribute]
        public double? OldVolume {
            get;
            set;
        }
        //送货单号
         [DataMemberAttribute]
        public string DeliveryBillNumber {
            get;
            set;
        }
    }
}
