﻿using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class PartsShippingOrderRef {
        /// <summary>
        /// 出库金额
        /// </summary>
        [DataMemberAttribute]
        public decimal TotalAmount {
            get;
            set;
        }
    }
}
