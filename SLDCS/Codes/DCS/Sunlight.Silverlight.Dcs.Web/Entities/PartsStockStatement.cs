﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web
{
    [DataContract(IsReference = true)]
    public class PartsStockStatement : EntityObject
    {
        [Key]
        [DataMemberAttribute]
        public int Id
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? WarehouseId
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string WarehouseCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string WarehouseName
        {
            get;
            set;
        }
        //配件编号
        [DataMemberAttribute]
        public string SparePartCode
        {
            get;
            set;
        }
        //红岩号
        [DataMemberAttribute]
        public string ReferenceCode
        {
            get;
            set;
        }
        //配件名称
        [DataMemberAttribute]
        public string SparePartName
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? OutQuantity
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? InQuantity
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? InventoryQuantity
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? BeginQuantity
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? EndQuantity
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? BeginTime
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? EndTime
        {
            get;
            set;
        }
    }
}

