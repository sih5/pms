﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 代理库与经销商隶属关系
    /// </summary>
    [DataContract(IsReference = true)]
    public class PartsSupplierRelationWithPurchasePrice : EntityObject {
        [Key]
        [DataMemberAttribute]
        public int PartsSupplierRelationId {
            get;
            set;
        }
        [Key]
        [DataMemberAttribute]
        public int SparePartId {
            get;
            set;
        }
        [Key]
        [DataMemberAttribute]
        public int PartsSalesCategoryId {
            get;
            set;
        }
        [Key]
        [DataMemberAttribute]
        public int PartsSupplierId {
            get;
            set;
        }
        [Key]
        [DataMemberAttribute]
        public int PartsPurchasePricingId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartsSalesCategoryCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartsSalesCategoryName {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SparePartCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SparePartName {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ReferenceCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ReferenceName {
            get;
            set;
        }


        [DataMemberAttribute]
        public string SupplierCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SupplierName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? PriceType {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal? PurchasePrice {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? ValidFrom {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? ValidTo {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? CreatorId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CreatorName {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? CreateTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SupplierPartCode {
            get;
            set;
        }

    }
}



