﻿
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class PartsTransferOrder {
        [DataMember]
        public int? ApprovalId {
            get;
            set;
        }

        [DataMember]
        public bool IsUploadFile {
            get;
            set;
        }
    }
}
