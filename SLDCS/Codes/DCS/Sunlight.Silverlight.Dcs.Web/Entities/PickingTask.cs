﻿using System;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web
{
    public partial class PickingTask : EntityObject
    {
        [DataMember]
        public string PartsOutboundPlanCode
        {
            get;
            set;
        }
        [DataMember]
        public string SourceCode
        {
            get;
            set;
        }
        [DataMember]
        public string SparePartCode
        {
            get;
            set;
        }
        [DataMember]
        public string SparePartName
        {
            get;
            set;
        }

    }
}
