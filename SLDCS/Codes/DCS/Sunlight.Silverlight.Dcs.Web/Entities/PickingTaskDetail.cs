﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Sunlight.Silverlight.Dcs.Web
{
    public partial class PickingTaskDetail
    {
        [DataMemberAttribute]
        public int? CourrentPicking
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public String ReferenceCode
        {
            get;
            set;
        }
         [DataMemberAttribute]
        public String BatchNumber
        {
            get;
            set;
        }

    }
}
