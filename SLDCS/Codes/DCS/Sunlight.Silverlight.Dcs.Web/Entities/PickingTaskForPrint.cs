﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class PickingTaskForPrint {
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }
        [DataMemberAttribute]
        public String Code {
            get;
            set;
        }
        //拣货数量
        [DataMemberAttribute]
        public int PickingQty {
            get;
            set;
        }
        //库区
         [Key]
        [DataMemberAttribute]
        public int WarehouseArea {
            get;
            set;
        }
        //库区
        [DataMemberAttribute]
        public String WarehouseAreaName {
            get;
            set;
        }
        //品种
        [DataMemberAttribute]
        public int Varieties {
            get;
            set;
        }
    }
}
