﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Sunlight.Silverlight.Dcs.Web
{
    [DataContract(IsReference = true)]
    public class PickingTaskQuery
    {
        [Key]
        [DataMemberAttribute]
        public int Id
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public String Code
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public String WarehouseCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public String WarehouseName
        {
            get;
            set;
        }
         [DataMemberAttribute]
        public String CounterpartCompanyCode
        {
            get;
            set;
        }
         [DataMemberAttribute]
         public String CounterpartCompanyName
        {
            get;
            set;
        }
         [DataMemberAttribute]
         public int Status
         {
             get;
             set;
         }
         [DataMemberAttribute]
         public String CreatorName
         {
             get;
             set;
         }
         [DataMemberAttribute]
         public DateTime? CreateTime
         {
             get;
             set;
         }
         [DataMemberAttribute]
         public String ModifierName
         {
             get;
             set;
         }
         [DataMemberAttribute]
         public DateTime? ModifyTime
         {
             get;
             set;
         }
         [DataMemberAttribute]
         public DateTime? PickingFinishTime
         {
             get;
             set;
         }
         [DataMemberAttribute]
         public String PartsSalesCategoryName
         {
             get;
             set;
         }
         [DataMemberAttribute]
         public String ResponsiblePersonName
         {
             get;
             set;
         }
         [DataMemberAttribute]
         public string OrderTypeName
         {
             get;
             set;
         }
         [DataMemberAttribute]
         public string Remark {
             get;
             set;
         }
    }
}
