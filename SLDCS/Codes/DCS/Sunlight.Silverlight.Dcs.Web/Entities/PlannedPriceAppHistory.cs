﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;
namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class PlannedPriceAppHistory : EntityObject {
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Code {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SparePartCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SparePartName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int PartsSalesCategoryId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartsSalesCategoryName {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime PlannedExecutionTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? ActualExecutionTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal RequestedPrice {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal? PriceBeforeChange {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? Quantity {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal? AmountDifference {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CreatorName {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? CreateTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ModifierName {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? ModifyTime {
            get;
            set;
        }
    }
}
