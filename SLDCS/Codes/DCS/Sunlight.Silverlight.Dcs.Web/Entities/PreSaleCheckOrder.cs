﻿using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class PreSaleCheckOrder {

        /// <summary>
        /// 已检查次数
        /// </summary>
        [DataMemberAttribute]
        public int? CheckedCount {
            get;
            set;
        }
        /// <summary>
        /// 审批状态
        /// </summary>
        [DataMemberAttribute]
        public int IsPassed {
            get;
            set;
        }

    }
}
