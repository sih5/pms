﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;
namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class PriorityPackingShelvesReport : EntityObject {
        [Key]
        [DataMemberAttribute]
        public int Rownum {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SparePartCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SparePartName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int Priority {
            get;
            set;
        }
        [DataMemberAttribute]
        public string InboundCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartsPurchaseOrderCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ReferenceCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public int InspectedQuantity {
            get;
            set;
        }
        [DataMemberAttribute]
        public int PackingQty {
            get;
            set;
        }
        [DataMemberAttribute]
        public int ShallPackingEntries {
            get;
            set;
        }
        [DataMemberAttribute]
        public int ActualPackingEntries {
            get;
            set;
        }
        [DataMemberAttribute]
        public int ShelvesAmount {
            get;
            set;
        }
        [DataMemberAttribute]
        public int ShallShelvesEntries {
            get;
            set;
        }
        [DataMemberAttribute]
        public int ActualShelvesEntries {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? InBoundFinishTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? PackingFinishTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? PackingFinishStatus {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? PriorityPackingTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? ShelvesFinishTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? ShelvesFinishStatus {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? PriorityShelvesTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? CreateTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Month {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Dates {
            get;
            set;
        }
        //应入库行数
        [DataMemberAttribute]
        public int? StorageNumber {
            get;
            set;
        }
        //实入库行数
        [DataMemberAttribute]
        public int? ActStorageNumber {
            get;
            set;
        }
        //应入库条目数
        [DataMemberAttribute]
        public int? StorageNumberItem {
            get;
            set;
        }
        //实入库条目数
        [DataMemberAttribute]
        public int? ActStorageNumberItem {
            get;
            set;
        }
        //入库行数完成率
        [DataMemberAttribute]
        public double? CompletionRate {
            get;
            set;
        }
        //入库条目数完成率
        [DataMemberAttribute]
        public double? CompletionEntryRate {
            get;
            set;
        }
        //应包装行数
        [DataMemberAttribute]
        public int? PackingNumber {
            get;
            set;
        }
        //实包装行数
        [DataMemberAttribute]
        public int? PackedNumber {
            get;
            set;
        }
        //应包装条目数
        [DataMemberAttribute]
        public int? PackingNumberItem {
            get;
            set;
        }
        //实包装条目数
        [DataMemberAttribute]
        public int? PackedNumberItem {
            get;
            set;
        }
        //包装行数完成率
        [DataMemberAttribute]
        public double? PackRate {
            get;
            set;
        }
        //包装条目完成率
        [DataMemberAttribute]
        public double? PackItemRate {
            get;
            set;
        }
        //	应上架行数
        [DataMemberAttribute]
        public int? ShelveNumber {
            get;
            set;
        }
        //实上架行数
        [DataMemberAttribute]
        public int? ShelvedNumber {
            get;
            set;
        }
        //应上架条目数
        [DataMemberAttribute]
        public int? ShelveNumberItem {
            get;
            set;
        }
        //实上架条目数
        [DataMemberAttribute]
        public int? ShelvedNumberItem {
            get;
            set;
        }
        //上架行数完成率
        [DataMemberAttribute]
        public double? ShelveRate {
            get;
            set;
        }
        //上架条目数完成率
        [DataMemberAttribute]
        public double? ShelveItemRate {
            get;
            set;
        }
        //类型
        [DataMemberAttribute]
        public int? Type {
            get;
            set;
        }
         [DataMemberAttribute]
        public DateTime? PriorityCreateTime {
            get;
            set;
        }
    }
}
