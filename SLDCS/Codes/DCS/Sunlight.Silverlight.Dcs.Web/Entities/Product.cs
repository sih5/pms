﻿using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 产品
    /// </summary>
    public partial class Product {
        /// <summary>
        /// 产品价格
        /// </summary>
        [DataMemberAttribute]
        public decimal ProductPrice {
            get;
            set;
        }
    }
}