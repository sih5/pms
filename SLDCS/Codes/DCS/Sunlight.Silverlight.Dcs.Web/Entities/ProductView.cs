﻿using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class ProductView {
        /// <summary>
        /// 服务产品线名称
        /// </summary>
        [DataMember]
        public string ServiceProductLineName {
            get;
            set;
        }
    }
}
