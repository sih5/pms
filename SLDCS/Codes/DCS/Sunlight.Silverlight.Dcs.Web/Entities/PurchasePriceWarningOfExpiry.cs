﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Sunlight.Silverlight.Dcs.Web{
    [DataContract(IsReference = true)]
    public class PurchasePriceWarningOfExpiry : EntityObject {
        [Key]
        [DataMemberAttribute]
        public int Rownum {
            get;
            set;
        }

        [DataMemberAttribute]
        public String SparepartCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public String SparepartName {
            get;
            set;
        }
        [DataMemberAttribute]
        public String PartsSupplierCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public String PartsSupplierName {
            get;
            set;
        }
        [DataMemberAttribute]
        public Boolean? IsPrimary {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? PriceType {
            get;
            set;
        }
        [DataMemberAttribute]
        public Decimal? PurchasePrice {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? ValidFrom {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? ValidTo {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? DaysRemaining {
            get;
            set;
        }
         [DataMemberAttribute]
        public string SupplierPartCode {
            get;
            set;
        }
    }
}
