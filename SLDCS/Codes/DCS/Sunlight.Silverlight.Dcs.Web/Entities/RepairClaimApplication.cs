﻿using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    //为打印维修保养索赔单提供几个虚拟字段
    public partial class RepairClaimApplication {
        /// <summary>
        /// 车主联系电话(车辆联系人.联系电话且联系人类型=1)
        /// </summary>
        [DataMember]
        public string RetainedCustomerPhone {
            get;
            set;
        }


        /// <summary>
        /// 服务站联系人
        /// </summary>
        [DataMember]
        public string CompanyContactPerson {
            get;
            set;
        }

        /// <summary>
        /// 联系电话
        /// </summary>
        [DataMember]
        public string CompanyContactMobile {
            get;
            set;
        }

        /// <summary>
        /// 出厂编号
        /// </summary>
        [DataMember]
        public string VehicleSerialNumber {
            get;
            set;
        }

        /// <summary>
        /// 车型代码
        /// </summary>
        [DataMember]
        public string ProductCategoryCode {
            get;
            set;
        }

        /// <summary>
        /// 发动机型号
        /// </summary>
        [DataMember]
        public string EngineModel {
            get;
            set;
        }

        /// <summary>
        /// 发动机编号
        /// </summary>
        [DataMember]
        public string EngineSerialNumber {
            get;
            set;
        }

        /// <summary>
        /// 整车编号
        /// </summary>
        [DataMember]
        public string ProductCode {
            get;
            set;
        }
    }
}
