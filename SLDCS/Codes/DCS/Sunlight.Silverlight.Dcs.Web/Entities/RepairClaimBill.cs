﻿
using System.Runtime.Serialization;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class RepairClaimBill {

        //   供应商确认，复核 专用价格字段 

        [DataMember]
        public decimal Supplier_LaborCost {
            get;
            set;
        }
        /// <summary>
        /// 服务站业务编码
        /// </summary>
        [DataMember]
        public string CustomerCode {
            get;
            set;
        }
        /// <summary>
        /// 服务站业务编码
        /// </summary>
        [DataMember]
        public int? IsAgreement {
            get;
            set;
        }

        /// <summary>
        /// 审核意见审核意见
        /// </summary>
        [DataMember]
        public string RepairClaimApplicationCheckComment {
            get;
            set;
        }

        [DataMember]
        public decimal Supplier_MaterialCost {
            get;
            set;
        }

        [DataMember]
        public decimal? Supplier_PartsManagementCost {
            get;
            set;
        }

        [DataMember]
        public decimal Supplier_TotalAmount {
            get;
            set;
        }
        //   供应商确认，复核 专用价格字段 
        [DataMember]
        public string CageCategory {
            get;
            set;
        }

        [DataMember]
        public string VehicleSeries {
            get;
            set;
        }

        [DataMember]
        public string VehicleFunction {
            get;
            set;
        }

        [DataMember]
        public string DriveModeCode {
            get;
            set;
        }

        [DataMember]
        public string ServiceTripClaimApplicationRemark {
            get;
            set;
        }

        [DataMember]
        public double? LaborCoefficient {
            get;
            set;
        }

        [DataMember]
        public string ServiceActivityName {
            get;
            set;
        }


        [DataMember]
        public double? PartsClaimCoefficient {
            get;
            set;
        }

        [DataMember]
        public double Rate {
            get;
            set;
        }

        [DataMember]
        public string SupplierCode {
            get;
            set;
        }

        [DataMember]
        public int? ServiceTripDistance {
            get;
            set;
        }

        [DataMember]
        public string BehindAxleTypTonneLevel {
            get;
            set;
        }

        [DataMember]
        public string SerialNumber {
            get;
            set;
        }

        [DataMember]
        public string ServiceProductLineCode {
            get;
            set;
        }

        [DataMember]
        public string ServiceProductLineName {
            get;
            set;
        }

        [DataMember]
        public string VehicleType {
            get;
            set;
        }

        [DataMember]
        public string ApplicationTypeName {
            get;
            set;
        }

        [DataMember]
        public string responsibleUnitCode {
            get;
            set;
        }

        [DataMember]
        public string responsibleUnitName {
            get;
            set;
        }

        [DataMember]
        public string marketingDepartmentCode {
            get;
            set;
        }

        [DataMember]
        public string marketingDepartmentName {
            get;
            set;
        }

        [DataMember]
        public int? BridgeType {
            get;
            set;
        }

        [DataMember]
        public string PartsSalesCategoryName {
            get;
            set;
        }

        [DataMember]
        public string CheckComment {
            get;
            set;
        }

        [DataMember]
        public string VehicleCategoryName {
            get;
            set;
        }

        [DataMember]
        public int CompanyType {
            get;
            set;
        }

        [DataMember]
        public int? FirstStoppageMileage {
            get;
            set;
        }

        /// <summary>
        /// 补款金额
        /// </summary>
        [DataMemberAttribute]
        public decimal? ReplenishmentAmount {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? PartSource {
            get;
            set;
        }

        [DataMemberAttribute]
        public string SysCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string EngineCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string HavingPurchase {
            get {
                return PartSource == 3 ? EntityStrings.Export_Title_Yes : EntityStrings.Export_Title_No;
            }
        }

        /// <summary>
        /// 改装类型
        /// </summary>
        [DataMemberAttribute]
        public int? AdaptType {
            get;
            set;
        }

        /// <summary>
        /// 客户分类
        /// </summary>
        [DataMemberAttribute]
        public int? CustomerCategory {
            get;
            set;

        }

        [DataMember]
        public string RepairTarget {
            get;
            set;
        }
        [DataMember]
        public string DealerGradeName
        {
            get;
            set;
        }
    }
}
