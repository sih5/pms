﻿using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 维修保养索赔单供应商索赔清单
    /// </summary>
    public partial class RepairClaimSupplierDetail {
        /// <summary>
        /// 供应商编号 
        /// </summary>
        [DataMemberAttribute]
        public string PartsSupplierCode {
            get;
            set;
        }
        /// <summary>
        /// 供应商名称
        /// </summary>
        [DataMemberAttribute]
        public string PartsSupplierName {
            get;
            set;
        }
    }
}
