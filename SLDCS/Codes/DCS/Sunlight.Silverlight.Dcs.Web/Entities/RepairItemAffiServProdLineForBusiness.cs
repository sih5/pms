﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class RepairItemAffiServProdLineForBusiness : EntityObject {

        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }

        ///<summary>
        /// 分公司Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int BranchId {
            get;
            set;
        }

        ///<summary>
        /// 分公司名称
        /// </summary>
        [DataMemberAttribute]
        public string BranchName {
            get;
            set;
        }

        ///<summary>
        /// 配件销售类型Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int PartsSalesCategoryId {
            get;
            set;
        }

        ///<summary>
        /// 服务产品线Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int ServiceProductLineId {
            get;
            set;
        }

        ///<summary>
        /// 服务产品线编号
        /// </summary>
        [DataMemberAttribute]
        public string ProductLineCode {
            get;
            set;
        }

        ///<summary>
        /// 服务产品线名称
        /// </summary>
        [DataMemberAttribute]
        public string ProductLineName {
            get;
            set;
        }

        ///<summary>
        /// 产品线类型
        /// </summary>
        [DataMemberAttribute]
        public int ProductLineType {
            get;
            set;
        }

        ///<summary>
        /// 维修项目Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int RepairItemId {
            get;
            set;
        }

        /// <summary>
        /// 发动机维修权限
        /// </summary>
        [DataMemberAttribute]
        public int? EngineRepairPower {
            get;
            set;
        }

        ///<summary>
        /// 维修项目编号
        /// </summary>
        [DataMemberAttribute]
        public string RepairItemCode {
            get;
            set;
        }

        ///<summary>
        /// 维修项目名称
        /// </summary>
        [DataMemberAttribute]
        public string RepairItemName {
            get;
            set;
        }

        ///<summary>
        /// 工时定额
        /// </summary>
        [DataMemberAttribute]
        public double DefaultLaborHour {
            get;
            set;
        }

        ///<summary>
        /// 状态
        /// </summary>
        [DataMemberAttribute]
        public int Status {
            get;
            set;
        }

        ///<summary>
        /// 创建人Id
        /// </summary>
        [DataMemberAttribute]
        public int? CreatorId {
            get;
            set;
        }

        ///<summary>
        /// 创建人名称
        /// </summary>
        [DataMemberAttribute]
        public string CreatorName {
            get;
            set;
        }

        ///<summary>
        /// 创建时间
        /// </summary>
        [DataMemberAttribute]
        public DateTime? CreateTime {
            get;
            set;
        }

        ///<summary>
        /// 修改人Id
        /// </summary>
        [DataMemberAttribute]
        public int? ModifierId {
            get;
            set;
        }

        ///<summary>
        /// 修改人名称
        /// </summary>
        [DataMemberAttribute]
        public string ModifierName {
            get;
            set;
        }

        ///<summary>
        /// 修改时间
        /// </summary>
        [DataMemberAttribute]
        public DateTime? ModifyTime {
            get;
            set;
        }

        ///<summary>
        /// 作废人Id
        /// </summary>
        [DataMemberAttribute]
        public int? AbandonerId {
            get;
            set;
        }

        ///<summary>
        /// 作废人名称
        /// </summary>
        [DataMemberAttribute]
        public string AbandonerName {
            get;
            set;
        }

        ///<summary>
        /// 作废时间
        /// </summary>
        [DataMemberAttribute]
        public DateTime? AbandonTime {
            get;
            set;
        }

        ///<summary>
        /// 备注
        /// </summary>
        [DataMemberAttribute]
        public string Remark {
            get;
            set;
        }
    }
}
