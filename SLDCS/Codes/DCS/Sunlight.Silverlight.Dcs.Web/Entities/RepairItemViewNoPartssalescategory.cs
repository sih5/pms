﻿using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web.Entities {
    /// <summary>
    /// 代理库与经销商隶属关系
    /// </summary>
    [DataContract(IsReference = true)]
    public class RepairItemViewNoPartssalescategory : EntityObject {
        [Key]
        [DataMemberAttribute]
        public int RepairItemId {
            get;
            set;
        }

        [DataMemberAttribute]
        public string RepairItemName {
            get;
            set;
        }
        [DataMemberAttribute]
        public string GrandParentCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string GrandParentName {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ParentCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ParentName {
            get;
            set;
        }
        [DataMemberAttribute]
        public string RepairItemCategoryCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string RepairItemCategoryName {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Code {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Name {
            get;
            set;
        }
        [DataMemberAttribute]
        public string QuickCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? RepairQualificationGrade {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Remark {
            get;
            set;
        }
        [DataMemberAttribute]
        public int Status {
            get;
            set;
        }
    }
}
