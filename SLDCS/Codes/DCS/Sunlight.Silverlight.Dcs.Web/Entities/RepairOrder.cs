﻿

using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class RepairOrder {
        [DataMemberAttribute]
        public string DTMROBJECTID {
            get;
            set;
        }
        /// <summary>
        /// 打印使用
        /// </summary>
        public string DealerServiceInfoFix {
            get;
            set;
        }

        /// <summary>
        /// 打印使用 本次使用积分
        /// </summary>
        public decimal MemberUsedIntegral {
            get;
            set;
        }

        /// <summary>
        /// 打印使用 会员使用金额
        /// </summary>
        public decimal MemberUsedPrice {
            get;
            set;
        }

        /// <summary>
        /// 打印使用 权益抵扣费用
        /// </summary>
        public decimal MemberRightsCost {
            get;
            set;
        }

        /// <summary>
        /// 打印使用 实际结算总计
        /// </summary>
        public decimal? DeductionTotalAmount {
            get;
            set;
        }



        /// <summary>
        /// 打印使用 现有积分
        /// </summary>
        public decimal? ExistingIntegral {
            get;
            set;
        }

        /// <summary>
        /// 打印使用 红包使用金额
        /// </summary>
        public decimal? RedPacketsUsedPrice {
            get;
            set;
        }

        /// <summary>
        /// 打印使用 本次使用权益
        /// </summary>
        public string RightsItem {
            get;
            set;
        }

        /// <summary>
        /// 打印使用 工时抵扣
        /// </summary>
        public decimal CostDiscount {
            get;
            set;
        }


        [DataMemberAttribute]
        public int ProvinceId {
            get;
            set;
        }
        //[DataMemberAttribute]
        //public decimal LaborHourUnitPriceRatePrice {
        //    get;
        //    set;
        //}

        /// <summary>
        /// 保养是否可修改
        /// </summary>
        [DataMemberAttribute]
        public bool IsMaintenanceChange {
            get;
            set;
        }
        /// <summary>
        /// 外出费用合计
        /// </summary>
        [DataMemberAttribute]
        public decimal FieldTotalAmount {
            get;
            set;
        }

        [DataMemberAttribute]
        public int CityId {
            get;
            set;
        }
        [DataMemberAttribute]
        public int CountyId {
            get;
            set;
        }
        /// <summary>
        /// 外出服务半径
        /// </summary>
        [DataMemberAttribute]
        public int OutServiceradii {
            get;
            set;
        }
        /// <summary>
        /// 外出服务费等级
        /// </summary>
        [DataMemberAttribute]
        public int? OutFeegradeId {
            get;
            set;
        }


        ///// <summary>
        /////产品系列
        ///// </summary>
        //[DataMemberAttribute]
        //public string TerraceCode {
        //    get;
        //    set;
        //}
        /// <summary>
        ///品牌
        /// </summary>
        [DataMemberAttribute]
        public string PartsSalesCategoryName {
            get;
            set;
        }
        /// <summary>
        ///事业部名称
        /// </summary>
        [DataMemberAttribute]
        public string ResponsibleUnitName {
            get;
            set;
        }

        [DataMemberAttribute]
        public string SerialNumber {
            get;
            set;
        }


        [DataMemberAttribute]
        public string ServiceActivityVisibility {
            get;
            set;
        }

        //是否查询出外出费用信息

        [DataMemberAttribute]
        public bool IsServiceTrip {
            get;
            set;
        }

        /// <summary>
        /// DTM维修合同编号
        /// </summary>
        [DataMemberAttribute]
        public string DTMRepairContractCode {
            get;
            set;
        }
    }
}
