﻿
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class RepairOrderFaultReason {
        [DataMemberAttribute]
        public int TabItemTag {
            get;
            set;
        }

        [DataMemberAttribute]
        public bool BtnIsEnabled {
            get;
            set;
        }

        [DataMemberAttribute]
        public string AmendmentsIsVisibility {
            get;
            set;
        }


        [DataMemberAttribute]
        public string RejectReason {
            get;
            set;
        }
        [DataMemberAttribute]
        public string RejectReasonIsVisibility {
            get;
            set;
        }

        /// <summary>
        /// 强制保养 固定金额 时在故障清单内根据此值判断是否汇总材料费
        /// </summary>
        [DataMemberAttribute]
        public bool VehicleMainteDetail {
            get;
            set;
        }
    }
}
