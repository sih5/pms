﻿using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class RepairOrderItemDetail {

        /// <summary>
        /// 实收费用合计
        /// </summary>
        [DataMemberAttribute]
        public decimal ActuraCost {
            get;
            set;
        }
        /// <summary>
        /// DTM维修项目ID
        /// </summary>
        [DataMemberAttribute]
        public string DTMRepairContractItemObjid {
            get;
            set;
        }
        /// <summary>
        /// 配件管理费
        /// </summary>
        [DataMemberAttribute]
        public decimal PartsManagementCost {
            get;
            set;
        }

        /// <summary>
        /// 标准费用合计
        /// </summary>
        [DataMemberAttribute]
        public decimal Cost {
            get;
            set;

        }


        [DataMemberAttribute]
        public int Number {
            get;
            set;
        }

        [DataMemberAttribute]
        public bool IsDTMRepairOrderItem {
            get;
            set;
        }
        [DataMemberAttribute]
        public bool ActualLaborCostIsReadOnly {
            get;
            set;
        }
    }
}
