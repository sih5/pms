﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {

    [DataContract(IsReference = true)]
    public class RepairOrderOutOfWarranty : EntityObject {
        ///<Summary>
        ///保外维修单号 
        ///</Summary> 
        [DataMemberAttribute]
        public string RepairOrderCode {
            get;
            set;
        }

        ///<Summary>
        ///状态 
        ///</Summary> 
        [DataMemberAttribute]
        public int RepairOrderStatus {
            get;
            set;
        }

        ///<Summary>
        ///服务站编号 
        ///</Summary> 
        [DataMemberAttribute]
        public string DealerCode {
            get;
            set;
        }

        ///<Summary>
        ///服务站业务编码   
        ///</Summary> 
        [DataMemberAttribute]
        public string DealerBussinessCode {
            get;
            set;
        }

        ///<Summary>
        ///服务站名称 
        ///</Summary> 
        [DataMemberAttribute]
        public string DealerName {
            get;
            set;
        }

        ///<Summary>
        ///驳回状态 
        ///</Summary> 
        [DataMemberAttribute]
        public int? RejectStatus {
            get;
            set;
        }


        ///<Summary>
        ///保内保外属性 
        ///</Summary> 
        [DataMemberAttribute]
        public int? WarrantyStatus {
            get;
            set;
        }


        ///<Summary>
        ///维修类型 
        ///</Summary> 
        [DataMemberAttribute]
        public int RepairType {
            get;
            set;
        }

        ///<Summary>
        ///品牌 
        ///</Summary> 
        [DataMemberAttribute]
        public string PartsSalesCategoryName {
            get;
            set;
        }

        ///<Summary>
        ///车牌号 
        ///</Summary> 
        [DataMemberAttribute]
        public string VehicleLicensePlate {
            get;
            set;
        }

        ///<Summary>
        ///Vin码 
        ///</Summary> 
        [DataMemberAttribute]
        public string VIN {
            get;
            set;
        }

        ///<Summary>
        ///分公司 
        ///</Summary> 
        [DataMemberAttribute]
        public string BranchName {
            get;
            set;
        }

        ///<Summary>
        ///服务产品线 
        ///</Summary> 
        [DataMemberAttribute]
        public string ServiceProductLineName {
            get;
            set;
        }

        ///<Summary>
        ///费用合计 
        ///</Summary> 
        [DataMemberAttribute]
        public decimal TotalAmount {
            get;
            set;
        }

        ///<Summary>
        ///材料费 
        ///</Summary> 
        [DataMemberAttribute]
        public decimal MaterialCost {
            get;
            set;
        }
        [DataMemberAttribute]
        public int MarketDepartmentId {
            get;
            set;
        }

        ///<Summary>
        ///其他费用 
        ///</Summary> 
        [DataMemberAttribute]
        public decimal OtherCost {
            get;
            set;
        }

        ///<Summary>
        ///外出费用 
        ///</Summary> 
        [DataMemberAttribute]
        public decimal FieldServiceCharge {
            get;
            set;
        }

        ///<Summary>
        ///拖车费用 
        ///</Summary> 
        [DataMemberAttribute]
        public decimal TowCharge {
            get;
            set;
        }

        ///<Summary>
        ///视频采集手机号 
        ///</Summary> 
        [DataMemberAttribute]
        public string VideoMobileNumber {
            get;
            set;
        }

        ///<Summary>
        ///MVS外出里程 
        ///</Summary> 
        [DataMemberAttribute]
        public int? MVSOutRange {
            get;
            set;
        }

        ///<Summary>
        ///MVS外出时长 
        ///</Summary> 
        [DataMemberAttribute]
        public int? MVSOutTime {
            get;
            set;
        }

        ///<Summary>
        ///MVS外出直线距离 
        ///</Summary> 
        [DataMemberAttribute]
        public int? MVSOutDistance {
            get;
            set;
        }

        ///<Summary>
        ///外出地点经纬度 
        ///</Summary> 
        [DataMemberAttribute]
        public string MVSOutCoordinate {
            get;
            set;
        }

        ///<Summary>
        ///创建人Id 
        ///</Summary> 
        [DataMemberAttribute]
        public int? CreatorId {
            get;
            set;
        }


        ///<Summary>
        ///创建人 
        ///</Summary> 
        [DataMemberAttribute]
        public string CreatorName {
            get;
            set;
        }

        ///<Summary>
        ///创建时间 
        ///</Summary> 
        [DataMemberAttribute]
        public DateTime? CreateTime {
            get;
            set;
        }

        ///<Summary>
        ///修改人Id 
        ///</Summary> 
        [DataMemberAttribute]
        public int? ModifierId {
            get;
            set;
        }

        ///<Summary>
        ///修改人 
        ///</Summary> 
        [DataMemberAttribute]
        public string ModifierName {
            get;
            set;
        }

        ///<Summary>
        ///修改时间 
        ///</Summary> 
        [DataMemberAttribute]
        public DateTime? ModifyTime {
            get;
            set;
        }

        ///<Summary>
        ///市场部 
        ///</Summary> 
        [DataMemberAttribute]
        public string MarketingDepartmentName {
            get;
            set;
        }

        ///<Summary>
        ///维修单Id 
        ///</Summary> 
        [Key]
        [DataMemberAttribute]
        public int RepairOrderId {
            get;
            set;
        }

        ///<Summary>
        ///经销商Id 
        ///</Summary> 
        [Key]
        [DataMemberAttribute]
        public int DealerId {
            get;
            set;
        }

        ///<Summary>
        ///经销商分公司信息Id 
        ///</Summary> 
        [Key]
        [DataMemberAttribute]
        public int DealerServiceInfoId {
            get;
            set;
        }

        ///<Summary>
        ///市场部Id 
        ///</Summary> 
        [Key]
        [DataMemberAttribute]
        public int MarketingDepartmentId {
            get;
            set;
        }

        ///<Summary>
        ///品怕Id 
        ///</Summary> 
        [Key]
        [DataMemberAttribute]
        public int PartsSalesCategoryId {
            get;
            set;
        }

        ///<Summary>
        ///分公司Id 
        ///</Summary> 
        [Key]
        [DataMemberAttribute]
        public int BranchId {
            get;
            set;
        }

        ///<Summary>
        ///服务产品线Id 
        ///</Summary> 
        [Key]
        [DataMemberAttribute]
        public int ServiceProductLineId {
            get;
            set;
        }

        ///<Summary>
        ///车辆Id 
        ///</Summary> 
        [Key]
        [DataMemberAttribute]
        public int VehicleInformationId {
            get;
            set;
        }


        [DataMemberAttribute]
        public string SysCode {
            get;
            set;
        }

        /// <summary>
        /// 会员号
        /// </summary>
        [DataMemberAttribute]
        public string MemberCode {
            get;
            set;
        }
        /// <summary>
        /// 会员姓名
        /// </summary>
        [DataMemberAttribute]
        public string MemberName {
            set;
            get;
        }
        /// <summary>
        /// 会员手机号码
        /// </summary>
        [DataMemberAttribute]
        public string MemberPhone {
            get;
            set;
        }

        /// <summary>
        /// 证件类型
        /// </summary>
        [DataMemberAttribute]
        public string IDType {
            get;
            set;
        }

        /// <summary>
        /// 证件号码
        /// </summary>
        [DataMemberAttribute]
        public string IDNumer {
            set;
            get;
        }

        /// <summary>
        /// 会员等级
        /// </summary>
        [DataMemberAttribute]
        public string MemberRank {
            set;
            get;
        }

        /// <summary>
        /// 实际结算总计
        /// </summary>
        [DataMemberAttribute]
        public decimal? DeductionTotalAmount {
            set;
            get;
        }

        /// <summary>
        /// 红包使用金额
        /// </summary>
        [DataMemberAttribute]
        public decimal? RedPacketsUsedPrice {
            set;
            get;
        }

        /// <summary>
        /// 会员使用金额
        /// </summary>
        [DataMemberAttribute]
        public decimal? MemberUsedPrice {
            set;
            get;
        }
        /// <summary>
        /// 是否包含APP照片
        /// </summary>
        [DataMemberAttribute]
        public int? IsContainPhoto {
            get;
            set;
        }

        /// <summary>
        /// 会员折扣率
        /// </summary>
        [DataMemberAttribute]
        public decimal? CostDiscount {
            set;
            get;
        }

        /// <summary>
        /// 会员折扣率
        /// </summary>
        [DataMemberAttribute]
        public decimal? MemberRightsCost {
            set;
            get;
        }

        /// <summary>
        /// 会员折扣率
        /// </summary>
        [DataMemberAttribute]
        public decimal? Discount{
            set;
            get;
        }

        /// <summary>
        /// 会员使用金额
        /// </summary>
        [DataMemberAttribute]
        public decimal? UsedPrice {
            set;
            get;
        }
    }
}
