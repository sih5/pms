﻿
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class RepairOrderSupplierDetail {
        [DataMemberAttribute]
        public string PartsSupplierCode {
            get;
            set;
        }

        [DataMember]
        public string PartsSupplierName {
            get;
            set;
        }
    }
}
