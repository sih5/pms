﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class Retailer_ServiceApply {
        [DataMember]
        public string WarehouseName {
            get;
            set;
        }

        [DataMember]
        public string CustomerCode {
            get;
            set;
        }

        [DataMember]
        public string CustomerName {
            get;
            set;
        }
    }
}
