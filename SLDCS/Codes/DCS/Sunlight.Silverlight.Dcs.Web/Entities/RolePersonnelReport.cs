﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;
namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class RolePersonnelReport : EntityObject {
        [Key]
        [DataMemberAttribute]
        public string Rownum {
            get;
            set;
        }
        [DataMemberAttribute]
        public string RoleName {
            get;
            set;
        }
        [DataMemberAttribute]
        public string LoginId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Name {
            get;
            set;
        }
        [DataMemberAttribute]
        public string LV1 {
            get;
            set;
        }
        [DataMemberAttribute]
        public string LV2 {
            get;
            set;
        }
        [DataMemberAttribute]
        public string LV3 {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ActionName {
            get;
            set;
        }
        //KD&配件中心总监
        [DataMemberAttribute]
        public string AccessoriesCenter {
            get;
            set;
        }
        //配件业务处经理
        [DataMemberAttribute]
        public string BussinessMananger {
            get;
            set;
        }
        //销售服务组主管
        [DataMemberAttribute]
        public string SalesMananger {
            get;
            set;
        }
        //销售片管
        [DataMemberAttribute]
        public string SalesFilmMananger {
            get;
            set;
        }
        //出口专员
        [DataMemberAttribute]
        public string OutSalesMananger {
            get;
            set;
        }
        //特急专员
        [DataMemberAttribute]
        public string ExtraMananger {
            get;
            set;
        }
        //服务商管理员
        [DataMemberAttribute]
        public string ServiceMananger {
            get;
            set;
        }
        //三包、大客户、销售员
        [DataMemberAttribute]
        public string BigMananger {
            get;
            set;
        }
        //采购计划组主管
        [DataMemberAttribute]
        public string PurchaseMananger {
            get;
            set;
        }
        //计划专员
        [DataMemberAttribute]
        public string PlanMananger {
            get;
            set;
        }
        //调拨专员
        [DataMemberAttribute]
        public string AllocationMananger {
            get;
            set;
        }
        //采购专员
        [DataMemberAttribute]
        public string Buyer {
            get;
            set;
        }
        //市场投放组主管
        [DataMemberAttribute]
        public string MarketMananger {
            get;
            set;
        }
        //价格管理
        [DataMemberAttribute]
        public string PriceMananger {
            get;
            set;
        }
        //商务专员
        [DataMemberAttribute]
        public string CommercialMananger {
            get;
            set;
        }
        //网络销售员
        [DataMemberAttribute]
        public string NetMananger {
            get;
            set;
        }
        //市场投放专员
        [DataMemberAttribute]
        public string MarketCommissioner {
            get;
            set;
        }
        //采购结算专员
        [DataMemberAttribute]
        public string PurchaseCommissioner {
            get;
            set;
        }
        //综合专员
        [DataMemberAttribute]
        public string GeneralCommissioner {
            get;
            set;
        }
        //配件技术员
        [DataMemberAttribute]
        public string AccessoryTechnician {
            get;
            set;
        }
        //物流管理处经理
        [DataMemberAttribute]
        public string LogisticsManager {
            get;
            set;
        }
        //物流管理处副经理
        [DataMemberAttribute]
        public string LogisticsFuManager {
            get;
            set;
        }
        //入库组主管
        [DataMemberAttribute]
        public string InBoundManager {
            get;
            set;
        }
        //入库组组长
        [DataMemberAttribute]
        public string InBoundHead {
            get;
            set;
        }
        //入库组组员
        [DataMemberAttribute]
        public string InBoundMember {
            get;
            set;
        }
        //包装组主管
        [DataMemberAttribute]
        public string PackingManager {
            get;
            set;
        }
        //包装组组长
        [DataMemberAttribute]
        public string PackingHead {
            get;
            set;
        }
        //包装组组员
        [DataMemberAttribute]
        public string PackingMember {
            get;
            set;
        }
        //包装工程师
        [DataMemberAttribute]
        public string PackingEngineer {
            get;
            set;
        }
        //销售退货专员
        [DataMemberAttribute]
        public string ReturnSales {
            get;
            set;
        }
        //库管组主管
        [DataMemberAttribute]
        public string WarehouseManagement {
            get;
            set;
        }
        //库管组组长
        [DataMemberAttribute]
        public string WarehouseHead {
            get;
            set;
        }
        //库管组组员
        [DataMemberAttribute]
        public string WarehouseMember {
            get;
            set;
        }
        //仓库管理员(马钢库、重庆库)
        [DataMemberAttribute]
        public string WarehouseManager {
            get;
            set;
        }
        //物流组主管
        [DataMemberAttribute]
        public string SupervisorLogistics {
            get;
            set;
        }
        //物流管理工
        [DataMemberAttribute]
        public string LogistManager {
            get;
            set;
        }
        //清点组组长
        [DataMemberAttribute]
        public string InventoryHead {
            get;
            set;
        }
        //清点组组员
        [DataMemberAttribute]
        public string InventoryMember {
            get;
            set;
        }
        //库内物流工程师
        [DataMemberAttribute]
        public string LogisticsWarehouse {
            get;
            set;
        }
        //财务主管
        [DataMemberAttribute]
        public string Treasurer {
            get;
            set;
        }
        //财务人员
        [DataMemberAttribute]
        public string TreasurerMember {
            get;
            set;
        }
        //服务经理
        [DataMemberAttribute]
        public string ServiceManager {
            get;
            set;
        }
        //总监助理
        [DataMemberAttribute]
        public string AssistantDirector {
            get;
            set;
        }
        //系统管理员助理
        [DataMemberAttribute]
        public string AssistantSystem {
            get;
            set;
        }
        //系统管理员
        [DataMemberAttribute]
        public string SystemAdministrator {
            get;
            set;
        }
        //综合组主管
        [DataMemberAttribute]
        public string IntegratedHead {
            get;
            set;
        }
    }
}
