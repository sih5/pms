﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class SIHCenterSalesPer {
        [Key]
        [DataMemberAttribute]
        public int Rownum {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SparePartCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SparePartName {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SubmitCompanyCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SubmitCompanyName {
            get;
            set;
        }
        public int MaxOrderedquantity {
            get;
            set;
        }
        [DataMemberAttribute]
        public int SumQty {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal PerQty {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? WarehouseId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string WarehouseName {
            get;
            set;
        }
        [DataMemberAttribute]
        public string WarehouseCode {
            get;
            set;
        }
         [DataMemberAttribute]
        public DateTime? Createtime {
            get;
            set;
        }
        [DataMemberAttribute]
         public string ReferenceCode {
             get;
             set;
         }
    }
}
