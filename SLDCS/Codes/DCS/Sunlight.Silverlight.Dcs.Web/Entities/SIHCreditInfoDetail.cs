﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class SIHCreditInfoDetail : EntityObject {
        /// <summary>
        /// 本次SIH授信额度（服务商）
        /// </summary>
        [DataMember]
        public decimal? ThisSIHCredit {
            get;
            set;
        }

        /// <summary>
        /// SIH授信额度（服务商）
        /// </summary>
        [DataMember]
        public decimal? SIHCredit {
            get;
            set;
        }

        /// <summary>
        /// SIH授信额度（服务商）
        /// </summary>
        [DataMember]
        public decimal? SIHCreditZB {
            get;
            set;
        }
    }
}
