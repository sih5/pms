﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web
{
    [DataContract(IsReference = true)]

    public class SaleAdjustmentReport : EntityObject
    {
        [Key]
        [DataMemberAttribute]
        public int Id
        {
            get;
            set;
        }
        //客户类型
        [DataMemberAttribute]
        public int CustomerType
        {
            get;
            set;
        }
        //中心库名称
        [DataMemberAttribute]
        public String AgencyName
        {
            get;
            set;
        }
        //客户名称
        [DataMemberAttribute]
        public string CustomerName
        {
            get;
            set;
        }
        //客户代码
        [DataMemberAttribute]
        public string CustomerCode
        {
            get;
            set;
        }
        //配件编号
        [DataMemberAttribute]
        public string SparePartCode
        {
            get;
            set;
        }
        //配件名称
        [DataMemberAttribute]
        public string SparePartName
        {
            get;
            set;
        }
        //调价前价格
        [DataMemberAttribute]
        public decimal OldPrice
        {
            get;
            set;
        }
        //调价后价格
        [DataMemberAttribute]
        public decimal Price
        {
            get;
            set;
        }
        //调价金额
        [DataMemberAttribute]
        public decimal Difference
        {
            get;
            set;
        }
        //调价幅度
        [DataMemberAttribute]
        public decimal DifferenceRate
        {
            get;
            set;
        }
        // 过去三月采购量
        [DataMemberAttribute]
        public int PastThreeMonthsPruchase
        {
            get;
            set;
        }
        // 实际可用库存
        [DataMemberAttribute]
        public int UsableQty
        {
            get;
            set;
        }
       
        // 在途数量
        [DataMemberAttribute]
        public int OnLineQty
        {
            get;
            set;
        }
        // 补差数量
        [DataMemberAttribute]
        public int AdjustQty
        {
            get;
            set;
        }
        // 补差金额
        [DataMemberAttribute]
        public decimal AdjustAmount
        {
            get;
            set;
        }
        // 调价时间
        [DataMemberAttribute]
        public DateTime AdjustTime
        {
            get;
            set;
        }
        // 补差批次
        [DataMemberAttribute]
        public string BatchNumber
        {
            get;
            set;
        }
        //销售在途
         [DataMemberAttribute]
        public int? OnwaySales {
            get;
            set;
        }
         //欠货在途
         [DataMemberAttribute]
         public int? OutBoundAmount {
             get;
             set;
         }
         //常规库存
         [DataMemberAttribute]
         public int? NormalStock {
             get;
             set;
         }
         //保底库存
         [DataMemberAttribute]
         public int? StockQty {
             get;
             set;
         }
         //保底库存补差金额
         [DataMemberAttribute]
         public decimal? StockQtyFee {
             get;
             set;
         }
    }
}
