﻿using System;
using System.Data.Objects.DataClasses;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class SaleOrderTerminalSatisfactionRate {
        [Key]
        [DataMemberAttribute]
        public int Rownum {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? Month {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? Year {
            get;
            set;
        }
        //分销中心
        [DataMemberAttribute]
        public string MarketingDepartmentName {
            get;
            set;
        }
        //中心库名称
        [DataMemberAttribute]
        public string CenterName {
            get;
            set;
        }
        //服务站名称
        [DataMemberAttribute]
        public string DealerName {
            get;
            set;
        }
        //紧急订单行数
        [DataMemberAttribute]
        public int? EmergencyOrderNum {
            get;
            set;
        }
        //特紧订单行数
        [DataMemberAttribute]
        public int? ExEmergencyOrderNum {
            get;
            set;
        }
        //AB类行数
        [DataMemberAttribute]
        public int? AbNum {
            get;
            set;
        }
        //订单总行数
          [DataMemberAttribute]
        public int? OrderNum {
            get;
            set;
        }
        //急件比例
        [DataMemberAttribute]
        public Decimal? EmergencyRate {
            get;
            set;
        }
        //急件比例
        [DataMemberAttribute]
        public Decimal? AbRate {
            get;
            set;
        }
        //终端比例
        [DataMemberAttribute]
        public Decimal? TerminalRate {
            get;
            set;
        }
        //渠道销售订单编号
        [DataMemberAttribute]
        public string Code {
            get;
            set;
        }
        //销售订单提交时间
         [DataMemberAttribute]
        public DateTime? SubmitTime {
            get;
            set;
         }
        //订单类型
        [DataMemberAttribute]
         public string PartsSalesOrderTypeName {
             get;
             set;
         }
        [DataMemberAttribute]
        public string SparePartCode {
            get;
            set;
        }
         [DataMemberAttribute]
        public int? OrderedQuantity {
            get;
            set;
        }
        [DataMemberAttribute]
         public int? Unit {
             get;
             set;
         }
    }
}
