﻿using System;
using System.Data.Objects.DataClasses;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class SalesSatisfactionRate : EntityObject {
        [DataMemberAttribute]
        public string Code {
            get;
            set;
        }
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }
        [DataMemberAttribute]
        public int CompanyId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Name {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Type {
            get;
            set;
        }
        //满足条目数（品种）
        [DataMemberAttribute]
        public int Ycpzs {
            get;
            set;
        }
        //总条目数（品种）
        [DataMemberAttribute]
        public int Totalpz {
            get;
            set;
        }
        //条目数满足率
        [DataMemberAttribute]
        public decimal Tmmzl {
            get;
            set;
        }
        //满足条目建议售价金额
        [DataMemberAttribute]
        public decimal Ycfee {
            get;
            set;
        }
        //总条目建议售价总金额
        [DataMemberAttribute]
        public decimal Totalfee {
            get;
            set;
        }
        //金额满足率
        [DataMemberAttribute]
        public decimal Jemzl {
            get;
            set;
        }
        //销售订单审核时间
        [DataMemberAttribute]
        public DateTime ApproveTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? UnYcpzs {
            get;
            set;
        }
    }
}
