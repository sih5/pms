﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class ServiceActivity {
        /// <summary>
        /// 错误信息
        /// </summary>
        [DataMemberAttribute]
        public string ErrorMsg {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ResponsibleUnitName {
            get;
            set;
        }
        /// <summary>
        /// 服务产品线组
        /// </summary>
        [DataMemberAttribute]
        public List<int> ServiceProductLineIds{
            get;
            set;
        }
        /// <summary>
        /// 名称（原始）
        /// </summary>
        [DataMemberAttribute]
        public string NameOld{
            get;
            set;
        }
    }
}
