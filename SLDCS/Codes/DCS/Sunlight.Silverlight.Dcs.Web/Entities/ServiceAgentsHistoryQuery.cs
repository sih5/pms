﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web.Entities {
    /// <summary>
    /// 服务站向代理库提报订单查询
    /// </summary>
    [DataContract(IsReference = true)]
    public class ServiceAgentsHistoryQuery : EntityObject {
        /// <summary>
        /// 配件销售订单id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }

        /// <summary>
        /// 代理库id
        /// </summary>
        [DataMemberAttribute]
        public int SalesUnitOwnerCompanyId {
            get;
            set;
        }

        /// <summary>
        /// 代理库名称
        /// </summary>
        [DataMemberAttribute]
        public string SalesUnitOwnerCompanyName {
            get;
            set;
        }

        /// <summary>
        /// 代理库编号
        /// </summary>
        [DataMemberAttribute]
        public string SalesUnitOwnerCompanyCode {
            get;
            set;
        }


        /// <summary>
        /// 服务站id
        /// </summary>
        [DataMemberAttribute]
        public int SubmitCompanyId {
            get;
            set;
        }

        /// <summary>
        /// 服务站名称
        /// </summary>
        [DataMemberAttribute]
        public string SubmitCompanyName {
            get;
            set;
        }

        /// <summary>
        /// 服务站编号
        /// </summary>
        [DataMemberAttribute]
        public string SubmitCompanyCode {
            get;
            set;
        }

        /// <summary>
        /// 品牌id
        /// </summary>
        [DataMemberAttribute]
        public int SalesCategoryId {
            get;
            set;
        }

        /// <summary>
        /// 品牌名称
        /// </summary>
        [DataMemberAttribute]
        public string SalesCategoryName {
            get;
            set;
        }

        /// <summary>
        /// 订单编号
        /// </summary>
        [DataMemberAttribute]
        public string Code {
            get;
            set;
        }

        /// <summary>
        /// 配件销售订单类型id
        /// </summary>
        [DataMemberAttribute]
        public int PartsSalesOrderTypeId {
            get;
            set;
        }

        /// <summary>
        /// 配件销售订单类型名称
        /// </summary>
        [DataMemberAttribute]
        public string PartsSalesOrderTypeName {
            get;
            set;
        }

        /// <summary>
        /// 配件id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int SparePartId {
            get;
            set;
        }
        /// <summary>
        /// 配件图号
        /// </summary>
        [DataMemberAttribute]
        public string SparePartCode {
            get;
            set;
        }
        /// <summary>
        /// 配件名称
        /// </summary>
        [DataMemberAttribute]
        public string SparePartName {
            get;
            set;
        }

        /// <summary>
        /// 订货数量
        /// </summary>
        [DataMemberAttribute]
        public int OrderedQuantity {
            get;
            set;
        }

        /// <summary>
        /// 满足数量
        /// </summary>
        [DataMemberAttribute]
        public int? ApproveQuantity {
            get;
            set;
        }

        /// <summary>
        /// 单价
        /// </summary>
        [DataMemberAttribute]
        public decimal OrderPrice {
            get;
            set;
        }

        /// <summary>
        /// 订货金额
        /// </summary>
        [DataMemberAttribute]
        public decimal OrderSum {
            get;
            set;
        }

        /// <summary>
        /// 订货仓库id
        /// </summary>
        [DataMemberAttribute]
        public int? WarehouseId {
            get;
            set;
        }

        /// <summary>
        /// 订货仓库名称
        /// </summary>
        [DataMemberAttribute]
        public string WarehouseName {
            get;
            set;
        }

        /// <summary>
        /// 订货仓库编号
        /// </summary>
        [DataMemberAttribute]
        public string WarehouseCode {
            get;
            set;
        }


        /// <summary>
        /// 是否直供
        /// </summary>
        [DataMemberAttribute]
        public bool IfDirectProvision {
            get;
            set;
        }

        /// <summary>
        /// 状态
        /// </summary>
        [DataMemberAttribute]
        public int Status {
            get;
            set;
        }

        /// <summary>
        /// 是否欠款
        /// </summary>
        [DataMemberAttribute]
        public bool IsDebt {
            get;
            set;
        }

        /// <summary>
        /// 订单总金额
        /// </summary>
        [DataMemberAttribute]
        public decimal TotalAmount {
            get;
            set;
        }


        /// <summary>
        /// 备注
        /// </summary>
        [DataMemberAttribute]
        public string Remark {
            get;
            set;
        }
        /// <summary>
        /// 发运方式
        /// </summary>
        [DataMemberAttribute]
        public int ShippingMethod {
            get;
            set;
        }

        /// <summary>
        /// 收货地址
        /// </summary>
        [DataMemberAttribute]
        public string ReceivingAddress {
            get;
            set;
        }

        /// <summary>
        /// 要求到货时间
        /// </summary>
        [DataMemberAttribute]
        public DateTime? RequestedDeliveryTime {
            get;
            set;
        }

        /// <summary>
        /// 终止原因
        /// </summary>
        [DataMemberAttribute]
        public string StopComment {
            get;
            set;
        }

        /// <summary>
        /// 创建人id
        /// </summary>
        [DataMemberAttribute]
        public int? CreatorId {
            get;
            set;
        }

        /// <summary>
        /// 创建人
        /// </summary>
        [DataMemberAttribute]
        public string CreatorName {
            get;
            set;
        }

        /// <summary>
        /// 创建时间
        /// </summary>
        [DataMemberAttribute]
        public DateTime? CreateTime {
            get;
            set;
        }

        /// <summary>
        /// 修改人id
        /// </summary>
        [DataMemberAttribute]
        public int? ModifierId {
            get;
            set;
        }

        /// <summary>
        /// 修改人
        /// </summary>
        [DataMemberAttribute]
        public string ModifierName {
            get;
            set;
        }

        /// <summary>
        /// 修改时间
        /// </summary>
        [DataMemberAttribute]
        public DateTime? ModifyTime {
            get;
            set;
        }

        /// <summary>
        /// 提交人id
        /// </summary>
        [DataMemberAttribute]
        public int? SubmitterId {
            get;
            set;
        }

        /// <summary>
        /// 提交人
        /// </summary>
        [DataMemberAttribute]
        public string SubmitterName {
            get;
            set;
        }

        /// <summary>
        /// 提交时间
        /// </summary>
        [DataMemberAttribute]
        public DateTime? SubmitTime {
            get;
            set;
        }

        /// <summary>
        /// 审批人Id
        /// </summary>
        [DataMemberAttribute]
        public int? ApproverId {
            get;
            set;
        }

        /// <summary>
        /// 审批人
        /// </summary>
        [DataMemberAttribute]
        public string ApproverName {
            get;
            set;
        }

        /// <summary>
        /// 审批时间
        /// </summary>
        [DataMemberAttribute]
        public DateTime? ApproveTime {
            get;
            set;
        }

        /// <summary>
        /// 初次审批时间
        /// </summary>
        [DataMemberAttribute]
        public DateTime? FirstApproveTime {
            get;
            set;
        }

        /// <summary>
        /// 作废人Id
        /// </summary>
        [DataMemberAttribute]
        public int? AbandonerId {
            get;
            set;
        }

        /// <summary>
        /// 作废人
        /// </summary>
        [DataMemberAttribute]
        public string AbandonerName {
            get;
            set;
        }

        /// <summary>
        /// 作废时间
        /// </summary>
        [DataMemberAttribute]
        public DateTime? AbandonTime {
            get;
            set;
        }

        /// <summary>
        /// 分公司Id
        /// </summary>
        [DataMemberAttribute]
        public int BranchId {
            get;
            set;
        }

        /// <summary>
        /// 分公司编号
        /// </summary>
        [DataMemberAttribute]
        public string BranchCode {
            get;
            set;
        }

        /// <summary>
        /// 分公司名称
        /// </summary>
        [DataMemberAttribute]
        public string BranchName {
            get;
            set;
        }

    }
}
