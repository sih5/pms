﻿
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class ServiceTripClaimApplication {
        [DataMember]
        public string Dealer_ContactAddress {
            get;
            set;
        }
    }
}
