﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 外出服务索赔单
    /// </summary>
    public partial class ServiceTripClaimBill {
        /// <summary>
        /// 外出人数
        /// </summary>
        [DataMember]
        public int? SettlePerson {
            get;
            set;
        }

        /// <summary>
        /// 外出天数
        /// </summary>
        [DataMember]
        public int? SettleDuration {
            get;
            set;
        }

        /// <summary>
        /// 故障原因Id
        /// </summary>
        public int? RepairOrderFalultReasonId {
            get;
            set;
        }

        /// <summary>
        /// 服务站业务编码
        /// </summary>
        [DataMember]
        public string BusinessCode {
            get;
            set;
        }

        /// <summary>
        /// 维修索赔单编号
        /// </summary>
        [DataMember]
        public string ClaimBillCode {
            get;
            set;
        }

        /// <summary>
        /// 品牌
        /// </summary>
        [DataMember]
        public string PartsSalesCategoryName {
            get;
            set;
        }

        /// <summary>
        /// 维修索赔单状态
        /// </summary>
        [DataMember]
        public int RepairClaimBillStatus {
            get;
            set;
        }

        /// <summary>
        /// 出厂编号
        /// </summary>
        [DataMember]
        public string SerialNumber {
            get;
            set;
        }
        /// <summary>
        /// 工时费
        /// </summary>
        [DataMember]
        public decimal LaborCost {
            get;
            set;
        }
        /// <summary>
        /// 索赔单备注
        /// </summary>
        [DataMember]
        public string RepairClaimBillRemark {
            get;
            set;
        }
        /// <summary>
        /// 市场部名称
        /// </summary>
        [DataMember]
        public string MarketDepartmentName {
            get;
            set;
        }
        /// <summary>
        /// 故障原因
        /// </summary>
        [DataMember]
        public string MalfunctionDescription {
            get;
            set;
        }
        /// <summary>
        /// 祸首件
        /// </summary>
        [DataMember]
        public int? FaultyPartsId {
            get;
            set;
        }
        /// <summary>
        /// 祸首件编号
        /// </summary>
        [DataMember]
        public string FaultyPartsCode {
            get;
            set;
        }
        /// <summary>
        /// 祸首件名称
        ///  </summary>
        [DataMember]
        public string FaultyPartsName {
            get;
            set;
        }

        /// <summary>
        /// 产品线名称
        ///  </summary>
        [DataMember]
        public string ProductLineName {
            get;
            set;
        }
        /// <summary>
        /// 品牌名称
        ///  </summary>
        [DataMember]
        public string ProductCategoryName {
            get;
            set;
        }
        /// <summary>
        /// 发动机型号
        ///  </summary>
        [DataMember]
        public string EngineModel {
            get;
            set;
        }
        /// <summary>
        /// 维修单编号
        ///  </summary>
        [DataMember]
        public string RepairOrderCode {
            get;
            set;
        }
        /// <summary>
        /// 故障现象描述
        ///  </summary>
        [DataMember]
        public string MalfunctionReason {
            get;
            set;
        }

        /// <summary>
        /// 旧件处理状态
        ///  </summary>
        [DataMember]
        public int UsedPartsDisposalStatus {
            get;
            set;
        }
        /// <summary>
        /// 外出申请单审核意见
        /// </summary>
        [DataMember]
        public string ApproveComment {
            get;
            set;
        }
        /// <summary>
        /// 客户名称
        /// </summary>
         [DataMember]
        public string RetainedCustomer {
            get;
            set;
        }
         /// <summary>
         /// 手机号码
         /// </summary>
         [DataMember]
         public string RetainedCustomerPhone {
             get;
             set;
         }
        /// <summary>
        /// 服务站分级名称
        /// </summary>
        [DataMember]
        public string DealerGradeName
        {
            get;
            set;
        }

        [DataMember]
        public string BranchCode {
            get;
            set;
        }
    }
}
