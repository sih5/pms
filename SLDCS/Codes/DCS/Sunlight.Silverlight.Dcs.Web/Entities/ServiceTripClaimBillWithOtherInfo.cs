﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {

    [DataContract(IsReference = true)]
    public class ServiceTripClaimBillWithOtherInfo : EntityObject {
        ///<Summary>
        ///外出索赔单Id
        ///</Summary>
        [Key]
        [DataMemberAttribute]
        public int ServiceTripClaimBillId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string OtherCostReason {
            get;
            set;
        }
        ///<Summary>
        ///索赔供应商
        ///</Summary>
        [DataMemberAttribute]
        public int? ClaimSupplierId {
            get;
            set;
        }
        ///<Summary>
        ///外出索赔单编号
        ///</Summary>
        [DataMemberAttribute]
        public string ServiceTripClaimBillCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public decimal? OtherCost {
            get;
            set;
        }

        ///<Summary>
        ///维修索赔单Id
        ///</Summary>
        [Key]
        [DataMemberAttribute]
        public int ClaimBillId {
            get;
            set;
        }

        ///<Summary>
        ///维修索赔单编号
        ///</Summary>
        [DataMemberAttribute]
        public string ClaimBillCode {
            get;
            set;
        }

        ///<Summary>
        ///品牌id
        ///</Summary>
        [Key]
        [DataMemberAttribute]
        public int PartsSalesCategoryId {
            get;
            set;
        }

        ///<Summary>
        ///品牌
        ///</Summary>
        [DataMemberAttribute]
        public string PartsSalesCategoryName {
            get;
            set;
        }

        ///<Summary>
        ///状态
        ///</Summary>
        [DataMemberAttribute]
        public int Status {
            get;
            set;
        }

        ///<Summary>
        ///驳回次数
        ///</Summary>
        [DataMemberAttribute]
        public int? RejectQty {
            get;
            set;
        }

        ///<Summary>
        ///维修索赔单单据状态 
        ///</Summary>
        [DataMemberAttribute]
        public int RepairClaimBillStatus {
            get;
            set;
        }

        ///<Summary>
        ///外出类型
        ///</Summary>
        [DataMemberAttribute]
        public int ServiceTripType {
            get;
            set;
        }

        ///<Summary>
        ///服务站编号
        ///</Summary>
        [Key]
        [DataMemberAttribute]
        public int DealerId {
            get;
            set;
        }

        ///<Summary>
        ///服务站编号
        ///</Summary>
        [DataMemberAttribute]
        public string DealerCode {
            get;
            set;
        }

        ///<Summary>
        ///服务站业务编码
        ///</Summary>
        [DataMemberAttribute]
        public string BusinessCode {
            get;
            set;
        }

        ///<Summary>
        ///服务站名称
        ///</Summary>
        [DataMemberAttribute]
        public string DealerName {
            get;
            set;
        }

        ///<Summary>
        ///出厂编号
        ///</Summary>
        [DataMemberAttribute]
        public string SerialNumber {
            get;
            set;
        }
        ///<Summary>
        ///作废人
        ///</Summary>
        [DataMemberAttribute]
        public string AbandonerName {
            get;
            set;
        }

        ///<Summary>
        ///是否向责任单位索赔
        ///</Summary>
        [DataMemberAttribute]
        public bool IfClaimToSupplier {
            get;
            set;
        }

        ///<Summary>
        ///是否使用自备车
        ///</Summary>
        [DataMemberAttribute]
        public bool? IfUseOwnVehicle {
            get;
            set;
        }

        ///<Summary>
        ///外出车牌号
        ///</Summary>
        [DataMemberAttribute]
        public string OutVehicleLicensePlate {
            get;
            set;
        }

        ///<Summary>
        ///人员补助单价
        ///</Summary>
        [DataMemberAttribute]
        public decimal? OutSubsidyPrice {
            get;
            set;
        }

        ///<Summary>
        ///车辆补助单价
        ///</Summary>
        [DataMemberAttribute]
        public decimal? OutServiceCarUnitPrice {
            get;
            set;
        }

        ///<Summary>
        ///结算里程
        ///</Summary>
        [DataMemberAttribute]
        public int SettleDistance {
            get;
            set;
        }

        ///<Summary>
        ///外出里程
        ///</Summary>
        [DataMemberAttribute]
        public int ServiceTripDistance {
            get;
            set;
        }

        ///<Summary>
        ///高速公路拖车费
        ///</Summary>
        [DataMemberAttribute]
        public decimal? TowCharge {
            get;
            set;
        }

        ///<Summary>
        ///外出人数
        ///</Summary>
        [DataMemberAttribute]
        public int? ServiceTripPerson {
            get;
            set;
        }

        ///<Summary>
        ///结算人数
        ///</Summary>
        [DataMemberAttribute]
        public int? SettlePerson {
            get;
            set;
        }

        ///<Summary>
        ///外出天数
        ///</Summary>
        [DataMemberAttribute]
        public int? ServiceTripDuration {
            get;
            set;
        }

        ///<Summary>
        ///结算天数
        ///</Summary>
        [DataMemberAttribute]
        public int? SettleDuration {
            get;
            set;
        }

        ///<Summary>
        ///费用合计
        ///</Summary>
        [DataMemberAttribute]
        public decimal? TotalAmount {
            get;
            set;
        }

        ///<Summary>
        ///工时费
        ///</Summary>
        [DataMemberAttribute]
        public decimal LaborCost {
            get;
            set;
        }

        ///<Summary>
        ///外出救援地
        ///</Summary>
        [DataMemberAttribute]
        public string DetailedAddress {
            get;
            set;
        }

        ///<Summary>
        ///外出原因
        ///</Summary>
        [DataMemberAttribute]
        public string ServiceTripReason {
            get;
            set;
        }

        ///<Summary>
        ///外出时间
        ///</Summary>
        [DataMemberAttribute]
        public DateTime? ServiceTripTime {
            get;
            set;
        }

        ///<Summary>
        ///外出索赔申请单编号
        ///</Summary>
        [DataMemberAttribute]
        public string ServiceTripClaimAppCode {
            get;
            set;
        }

        ///<Summary>
        ///外出备注
        ///</Summary>
        [DataMemberAttribute]
        public string OutRemark {
            get;
            set;
        }

        ///<Summary>
        ///备注
        ///</Summary>
        [DataMemberAttribute]
        public string Remark {
            get;
            set;
        }

        ///<Summary>
        ///供应商审批意见
        ///</Summary>
        [DataMemberAttribute]
        public string SupplierApproveComment {
            get;
            set;
        }
        ///<Summary>
        ///外出申请单审核意见
        ///</Summary>
        [DataMemberAttribute]
        public string ApproveComment {
            get;
            set;
        }
        ///<Summary>
        ///车主
        ///</Summary>
        [DataMemberAttribute]
        public string RetainedCustomer {
            get;
            set;
        }
        ///<Summary>
        ///旧件处理状态
        ///</Summary>
        [DataMemberAttribute]
        public int? UsedPartsDisposalStatus {
            get;
            set;
        }
        ///<Summary>
        ///车主电话
        ///</Summary>
        [DataMemberAttribute]
        public string RetainedCustomerPhone {
            get;
            set;
        }

        ///<Summary>
        ///市场部Id
        ///</Summary>
        [Key]
        [DataMemberAttribute]
        public int MarketDepartmentId {
            get;
            set;
        }

        ///<Summary>
        ///市场部名称
        ///</Summary>
        [DataMemberAttribute]
        public string MarketDepartmentName {
            get;
            set;
        }

        ///<Summary>
        ///故障原因名称
        ///</Summary>
        [DataMemberAttribute]
        public string MalfunctionDescription {
            get;
            set;
        }

        ///<Summary>
        ///祸首件编号
        ///</Summary>
        [Key]
        [DataMemberAttribute]
        public int? FaultyPartsId {
            get;
            set;
        }

        ///<Summary>
        ///祸首件编号
        ///</Summary>
        [DataMemberAttribute]
        public string FaultyPartsCode {
            get;
            set;
        }

        ///<Summary>
        ///祸首件名称
        ///</Summary>
        [DataMemberAttribute]
        public string FaultyPartsName {
            get;
            set;
        }

        ///<Summary>
        ///车辆Id
        ///</Summary>
        [Key]
        [DataMemberAttribute]
        public int VehicleInformationId {
            get;
            set;
        }

        ///<Summary>
        ///VIN
        ///</Summary>
        [DataMemberAttribute]
        public string VIN {
            get;
            set;
        }

        ///<Summary>
        ///服务产品线Id
        ///</Summary>
        [Key]
        [DataMemberAttribute]
        public int ProductLineId {
            get;
            set;
        }


        ///<Summary>
        ///服务产品线
        ///</Summary>
        [DataMemberAttribute]
        public string ProductLineName {
            get;
            set;
        }

        ///<Summary>
        ///车型
        ///</Summary>
        [DataMemberAttribute]
        public string ProductCategoryName {
            get;
            set;
        }

        ///<Summary>
        ///发动机型号
        ///</Summary>
        [DataMemberAttribute]
        public string EngineModel {
            get;
            set;
        }

        ///<Summary>
        ///维修单Id
        ///</Summary>
        [Key]
        [DataMemberAttribute]
        public int RepairOrderId {
            get;
            set;
        }

        ///<Summary>
        ///维修单编号
        ///</Summary>
        [DataMemberAttribute]
        public string RepairOrderCode {
            get;
            set;
        }

        ///<Summary>
        ///故障现象描述
        ///</Summary>
        [DataMemberAttribute]
        public string MalfunctionReason {
            get;
            set;
        }

        ///<Summary>
        ///责任单位Id
        ///</Summary>
        [Key]
        [DataMemberAttribute]
        public int ResponsibleUnitId {
            get;
            set;
        }


        ///<Summary>
        ///责任单位编号
        ///</Summary>
        [DataMemberAttribute]
        public string ResponsibleUnitCode {
            get;
            set;
        }

        ///<Summary>
        ///责任单位名称
        ///</Summary>
        [DataMemberAttribute]
        public string ResponsibleUnitName {
            get;
            set;
        }

        ///<Summary>
        ///供应商确认状态
        ///</Summary>
        [DataMemberAttribute]
        public int? SupplierApproveStatus {
            get;
            set;
        }

        ///<Summary>
        ///终审意见
        ///</Summary>
        [DataMemberAttribute]
        public string ApprovalComment {
            get;
            set;
        }

        ///<Summary>
        ///修改人Id
        ///</Summary>
        [DataMemberAttribute]
        public int? ModifierId {
            get;
            set;
        }


        ///<Summary>
        ///修改人名称
        ///</Summary>
        [DataMemberAttribute]
        public string ModifierName {
            get;
            set;
        }


        ///<Summary>
        ///修改时间
        ///</Summary>
        [DataMemberAttribute]
        public DateTime? ModifyTime {
            get;
            set;
        }

        ///<Summary>
        ///创建人名称
        ///</Summary>
        [DataMemberAttribute]
        public int? CreatorId {
            get;
            set;
        }


        ///<Summary>
        ///创建人名称
        ///</Summary>
        [DataMemberAttribute]
        public string CreatorName {
            get;
            set;
        }


        ///<Summary>
        ///创建时间
        ///</Summary>
        [DataMemberAttribute]
        public DateTime? CreateTime {
            get;
            set;
        }

        ///<Summary>
        ///审批人Id
        ///</Summary>
        [DataMemberAttribute]
        public int? ApproverId {
            get;
            set;
        }


        ///<Summary>
        ///审批人名称
        ///</Summary>
        [DataMemberAttribute]
        public string ApproverName {
            get;
            set;
        }


        ///<Summary>
        ///审批时间
        ///</Summary>
        [DataMemberAttribute]
        public DateTime? ApproveTime {
            get;
            set;
        }


        ///<Summary>
        ///故障原因Id
        ///</Summary>
        [Key]
        [DataMemberAttribute]
        public int RepairOrderFalultReasonId {
            get;
            set;
        }

        ///<Summary>
        ///故障原因Id
        ///</Summary>
        [Key]
        [DataMemberAttribute]
        public int DealerServiceInfoId {
            get;
            set;
        }

        /// <summary>
        /// 结算状态
        /// </summary>
        [DataMemberAttribute]
        public int SettlementStatus {
            get;
            set;
        }
        /// <summary>
        /// 供应商确认状态
        /// </summary>
        [DataMemberAttribute]
        public int? SupplierCheckStatus {
            get;
            set;
        }

        /// <summary>
        /// 分工司Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int BranchId {
            get;
            set;
        }
        /// <summary>
        /// 分工司Code
        /// </summary>
        [DataMemberAttribute]
        public string BranchCode {
            get;
            set;
        }
        /// <summary>
        /// 索赔供应商名称
        /// </summary>
        [DataMemberAttribute]
        public string ClaimSupplierName {
            get;
            set;
        }
        /// <summary>
        /// 索赔供应商编号
        /// </summary>
        [DataMemberAttribute]
        public string ClaimSupplierCode {
            get;
            set;
        }

        /// <summary>
        /// 索赔供应商编号
        /// </summary>
        [DataMemberAttribute]
        public int MarketingDepartmentId {
            get;
            set;
        }

        [DataMemberAttribute]
        public decimal? OfferOtherCost {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal? OffertrailerCost {
            get;
            set;
        }

        [DataMemberAttribute]
        public string SysCode {
            get;
            set;
        }
    }
}
