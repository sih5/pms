﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;
namespace Sunlight.Silverlight.Dcs.Web {
    //结算单与出入库单关联发票查询
    [DataContract(IsReference = true)]
    public class SettlementInvoice : EntityObject {
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }
        [Key]
        [DataMemberAttribute]
        public int PartsOutboundBillId {
            get;
            set;
        }
        //销售订单编号
        [DataMemberAttribute]
        public string PartsSalesOrderCode {
            get;
            set;
        }
        //分销中心
        [DataMemberAttribute]
        public string MarketingDepartmentName {
            get;
            set;
        }
     
        //仓库名称
        [DataMemberAttribute]
        public string WarehouseName {
            get;
            set;
        }
       
        //省份
        [DataMemberAttribute]
        public string Province {
            get;
            set;
        }
        //客户名称
        [DataMemberAttribute]
        public string SubmitCompanyName {
            get;
            set;
        }
        //出库单
        [DataMemberAttribute]
        public string PartsOutboundBillCode {
            get;
            set;
        }
        //红岩号
        [DataMemberAttribute]
        public string ReferenceCode {
            get;
            set;
        }
        //配件编号
        [DataMemberAttribute]
        public string SparePartCode {
            get;
            set;
        }
        //配件名称
        [DataMemberAttribute]
        public string SparePartName {
            get;
            set;
        }
        //采购或调拨
        [DataMemberAttribute]
        public int? PurchaseRoute {
            get;
            set;
        }
        //产品类别
        [DataMemberAttribute]
        public int? PartABC {
            get;
            set;
        }
        //出库数量
        [DataMemberAttribute]
        public int OutboundAmount {
            get;
            set;
        }
        //出库成本单价
        [DataMemberAttribute]
        public decimal? CostPrice {
            get;
            set;
        }
        //出库成本金额
        [DataMemberAttribute]
        public decimal? CostPriceAmount {
            get;
            set;
        }
        //折让后单价
        [DataMemberAttribute]
        public decimal? SettlementPrice {
            get;
            set;
        }
        //折让后金额
        [DataMemberAttribute]
        public decimal? SettlementPriceAmount {
            get;
            set;
        }
        //出库时间
        [DataMemberAttribute]
        public DateTime? PartsOutboundBillCreateTime {
            get;
            set;
        }
        //结算单生成时间
        [DataMemberAttribute]
        public DateTime? CreateTime {
            get;
            set;
        }
        //销售结算单状态
        [DataMemberAttribute]
        public int? Status {
            get;
            set;
        }
        //销售结算单业务分类
        [DataMemberAttribute]
        public int? BusinessType {
            get;
            set;
        }
        //备注
        [DataMemberAttribute]
        public string Remark {
            get;
            set;
        }
        //结算单号
        [DataMemberAttribute]
        public string Code {
            get;
            set;
        }
        //发票号
        [DataMemberAttribute]
        public string InvoiceNumber {
            get;
            set;
        }
    }
}
