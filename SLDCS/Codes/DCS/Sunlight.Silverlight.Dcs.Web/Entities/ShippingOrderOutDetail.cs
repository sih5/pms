﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 发运单关联单虚拟bo
    /// </summary>
    [DataContract(IsReference = true)]
    public class ShippingOrderOutDetail : EntityObject {
        [Key]
        [DataMemberAttribute]
        public int PartsShippingOrderRefId {
            get;
            set;
        }
        [Key]
        [DataMemberAttribute]
        public int PartsShippingOrderId {
            get;
            set;
        }
        [Key]
        [DataMemberAttribute]
        public int PartsOutboundBillId {
            get;
            set;
        }
        [Key]
        [DataMemberAttribute]
        public int PartsOutboundBillDetailId {
            get;
            set;
        }
        [Key]
        [DataMemberAttribute]
        public int PartsShippingOrderDetailId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartsOutboundBillCode {
            get;
            set;
        }
        [Key]
        [DataMemberAttribute]
        public int WarehouseId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string WarehouseCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string WarehouseName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int OutboundType {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartsSalesOrderTypeName {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? CreateTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? ShippingMethod {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Remark {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal TotalAmount {
            get;
            set;
        }

        [DataMemberAttribute]
        public string OrderApproveComment {
            get;
            set;
        }


    }
}
