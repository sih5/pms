﻿using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class ShouldTaxGoods {
        /// <summary>
        /// 货物(劳务)名称
        /// </summary>
        [DataMemberAttribute]
        public string GoodsName {
            get;
            set;
        }

        /// <summary>
        /// 规格型号
        /// </summary>
        [DataMemberAttribute]
        public string Spec {
            get;
            set;
        }

        /// <summary>
        /// 数量
        /// </summary>
        [DataMemberAttribute]
        public int? Quantity {
            get;
            set;
        }

        /// <summary>
        /// 单价
        /// </summary>
        [DataMemberAttribute]
        public decimal? Price {
            get;
            set;
        }

        /// <summary>
        /// 金额
        /// </summary>
        [DataMemberAttribute]
        public decimal Amount {
            get;
            set;
        }

        /// <summary>
        /// 税额
        /// </summary>
        [DataMemberAttribute]
        public decimal TaxAmount {
            get;
            set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        [DataMemberAttribute]
        public string Remark {
            get;
            set;
        }

        /// <summary>
        /// 显示顺序
        /// </summary>
        [DataMemberAttribute]
        public string UsedPartsBarCode {
            get;
            set;
        }

        /// <summary>
        /// 显示顺序
        /// </summary>
        [DataMemberAttribute]
        public int SN {
            get;
            set;
        }
        /// <summary>
        /// 税额 (主单)
        /// </summary>
        [DataMemberAttribute]
        public decimal MainTaxAmounts {
            get;
            set;
        }
        /// <summary>
        /// 金额 (主单)
        /// </summary>
        [DataMemberAttribute]
        public decimal MainAmount {
            get;
            set;
        }


    }
}
