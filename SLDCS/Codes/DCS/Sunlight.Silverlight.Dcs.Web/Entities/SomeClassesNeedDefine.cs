﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    public class PropertyChangeInfo {
        public string PropertyName {
            get;
            set;
        }

        public object BeforeChangeValue {
            get;
            set;
        }

        public object AfterChangeValue {
            get;
            set;
        }
        public PropertyChangeInfo() {

        }
        public PropertyChangeInfo(string propertyName, object beforeChangeValue, object afterChangeValue) {
            PropertyName = propertyName;
            BeforeChangeValue = beforeChangeValue;
            AfterChangeValue = afterChangeValue;
        }

        
    }
}
