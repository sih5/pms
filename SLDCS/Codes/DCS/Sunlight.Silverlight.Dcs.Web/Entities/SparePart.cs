﻿using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class SparePart {
        /// <summary>
        /// 可用额度
        /// </summary>
        [DataMemberAttribute]
        public decimal PartsSalePrice {
            get;
            set;
        }

        [DataMemberAttribute]
        public string SupplierName {
            get;
            set;
        }

        [DataMemberAttribute]
        public string SupplierCode {
            get;
            set;
        }
        /// <summary>
        /// 打印次数
        /// </summary>
        [DataMember]
        public int? PrintNumber {
            get;
            set;
        }

        /// <summary>
        /// 打印数量
        /// </summary>
        [DataMember]
        public int? PrintNums {
            get;
            set;
        }

        /// <summary>
        /// 序列号
        /// </summary>
        [DataMember]
        public string NumCode {
            get;
            set;
        }

        /// <summary>
        /// 互换分组号
        /// </summary>
        [DataMember]
        public string ExGroupCode {
            get;
            set;
        }
        [DataMember]
        public bool Isprice
        {
            get;
            set;
        }
        //是否可销售.
        [DataMember]
        public bool? IsSalable
        {
            get;
            set;
        }
        //是否可采购
        [DataMember]
        public bool? IsOrderable
        {
            get;
            set;
        }
    }
}
