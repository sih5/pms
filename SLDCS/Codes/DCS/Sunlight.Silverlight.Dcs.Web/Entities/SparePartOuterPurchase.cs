﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class SparePartOuterPurchase : EntityObject {
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }

        [Key]
        [DataMemberAttribute]
        public int PartId {
            get;
            set;
        }

        [DataMemberAttribute]
        public string Code {
            get;
            set;
        }

        [DataMemberAttribute, Display(ResourceType = typeof(EntityStrings), Name = "SparePart_Name")]
        public string Name {
            get;
            set;
        }

        [DataMemberAttribute]
        public decimal? PartsSalesPrice {
            get;
            set;
        }

        [DataMemberAttribute, Display(ResourceType = typeof(EntityStrings), Name = "_Common_CreateTime")]
        public DateTime? CreateTime {
            get;
            set;
        }

        [DataMemberAttribute, Display(ResourceType = typeof(EntityStrings), Name = "_Common_ModifyTime")]
        public DateTime? ModifyTime {
            get;
            set;
        }

        [DataMemberAttribute, Display(ResourceType = typeof(EntityStrings), Name = "_Common_Status")]
        public int? Status {
            get;
            set;
        }


        [DataMemberAttribute]
        public string ReferenceCode {
            get;
            set;
        }

    }
}
