﻿using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {

    [DataContract(IsReference = true)]
    public class SparePartWithQuantity : EntityObject {
        [Key]
        [DataMemberAttribute]
        public int SparePartId {
            get;
            set;
        }
        [DataMemberAttribute]
        [Display(ResourceType = typeof(EntityStrings), Name = "DealerPartsInventoryDetail_SparePartCode")]
        public string SparePartCode {
            get;
            set;
        }
        [DataMemberAttribute]
        [Display(ResourceType = typeof(EntityStrings), Name = "DealerPartsInventoryDetail_SparePartName")]
        public string SparePartName {
            get;
            set;
        }
        [DataMemberAttribute]
        [Display(ResourceType = typeof(EntityStrings), Name = "DealerPartsStock_Quantity")]
        public int Quantity {
            get;
            set;
        }
        [DataMemberAttribute]
        [Display(ResourceType = typeof(EntityStrings), Name = "VirtualUsedPartsInboundPlanDetail_InboundAmount")]
        public int InspectedQuantity {
            get;
            set;
        }

        [DataMemberAttribute]
        [Display(ResourceType = typeof(EntityStrings), Name = "_Common_SerialNumber")]
        public int SerialNumber {
            get;
            set;
        }



    }
}
