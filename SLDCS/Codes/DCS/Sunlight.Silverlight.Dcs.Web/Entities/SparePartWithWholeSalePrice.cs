﻿
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class SparePartWithWholeSalePrice : EntityObject {

        /// <summary>
        /// 配件Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int SparePartId {
            get;
            set;
        }
        /// <summary>
        /// 配件编号
        /// </summary>
        [DataMemberAttribute]
        public string SparePartCode {
            get;
            set;
        }
        /// <summary>
        /// 配件名称
        /// </summary>
        [DataMemberAttribute]
        public string SparePartName {
            get;
            set;
        }
        /// <summary>
        /// 批发价
        /// </summary>
        [DataMemberAttribute]
        public decimal WholesalePrice {
            get;
            set;
        }
        /// <summary>
        /// 配件状态
        /// </summary>
        [DataMemberAttribute]
        public int Status {
            get;
            set;
        }
    }
}
