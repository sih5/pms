﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class SpecialPriceChangeList {
        [DataMemberAttribute]
        public decimal? PurchasePrice {
            get;
            set;
        }

        [DataMemberAttribute]
        public decimal? SalesPrice {
            get;
            set;
        }

        [DataMemberAttribute]
        public decimal? CenterPrice {
            get;
            set;
        }
        //成本价
        [DataMemberAttribute]
        public decimal? CostPrice {
            get;
            set;
        }
    }
}
