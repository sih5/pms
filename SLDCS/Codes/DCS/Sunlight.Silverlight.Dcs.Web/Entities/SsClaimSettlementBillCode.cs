﻿using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 服务站索赔结算单编号
    /// </summary>
    [DataContract(IsReference = true)]
    public class SsClaimSettlementBillCode : EntityObject {
        /// <summary>
        /// 服务站索赔结算单Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }
        [DataMemberAttribute]
        public int DealerInvoiceInformationId {
            get;
            set;
        }

        /// <summary>
        /// 服务站索赔结算单编号
        /// </summary>
        [DataMemberAttribute]
        public string Code {
            get;
            set;
        }

    }
}
