﻿using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 服务站旧件库存
    /// </summary>
    public partial class SsUsedPartsStorage {

        /// <summary>
        /// 故障现象描述(条码标签打印)
        /// </summary>
        [DataMemberAttribute]
        public string MalfunctionReason {
            get;
            set;
        }
    }
}
