﻿using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;
//库龄报表统计
namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 配件企业库存
    /// </summary>
    [DataContract(IsReference = true)]
    public class StorehouseCensus : EntityObject {
        [Key]
        [DataMemberAttribute]
        public int Rownum {
            get;
            set;
        }
        [DataMemberAttribute]
        public int SparePartId {
            get;
            set;
        }
        [DataMemberAttribute]
        public int StorageCompanyId {
            get;
            set;
        }
        //配件编号
        [DataMemberAttribute]
        public string SparePartCode {
            get;
            set;
        }
        //配件名称
        [DataMemberAttribute]
        public string SparePartName {
            get;
            set;
        }
        //红岩号
        [DataMemberAttribute]
        public string ReferenceCode {
            get;
            set;
        }
        //配件属性
        [DataMemberAttribute]
        public int? PartABC {
            get;
            set;
        }
        //单价
        [DataMemberAttribute]
        public decimal CostPrice {
            get;
            set;
        }
        //库存金额（等于单价*企业库存）
        [DataMemberAttribute]
        public decimal CostAmount {
            get;
            set;
        }
        //企业库存
        [DataMemberAttribute]
        public int Quantity {
            get;
            set;
        }
        //90天内库存
        [DataMemberAttribute]
        public int QuantityOne {
            get;
            set;
        }
        //90天内库存总金额
        [DataMemberAttribute]
        public decimal QuantityOneAmount {
            get;
            set;
        }
        //90-180天内库存
        [DataMemberAttribute]
        public int QuantityTwo {
            get;
            set;
        }
        //90-180天内库存总金额
        [DataMemberAttribute]
        public decimal QuantityTwoAmount {
            get;
            set;
        }
        //181-360天内库存
        [DataMemberAttribute]
        public int QuantityThree {
            get;
            set;
        }
        //181-360天内库存总金额
        [DataMemberAttribute]
        public decimal QuantityThreeAmount {
            get;
            set;
        }
        //361-720天内库存
        [DataMemberAttribute]
        public int QuantityFour {
            get;
            set;
        }
        //361-720天内库存总金额
        [DataMemberAttribute]
        public decimal QuantityFourAmount {
            get;
            set;
        }
        //721-1080天内库存
        [DataMemberAttribute]
        public int QuantityFive {
            get;
            set;
        }
        //721-1080天内库存总金额
        [DataMemberAttribute]
        public decimal QuantityFiveAmount {
            get;
            set;
        }
        //7更早库存
        [DataMemberAttribute]
        public int QuantityMore {
            get;
            set;
        }
        //更早库存总金额
        [DataMemberAttribute]
        public decimal QuantityMoreAmount {
            get;
            set;
        }      
        [DataMemberAttribute]
        public string CenterCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CenterName {
            get;
            set;
        }
    }
}
