﻿using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 配件企业库存
    /// </summary>
    [DataContract(IsReference = true)]
    public class SubsidyInfo : EntityObject {
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }
        /// <summary>
        /// 外出人员补助单价(天)
        /// </summary>
        [DataMemberAttribute]
        public decimal OutSubsidyPrice {
            get;
            set;
        }
        /// <summary>
        /// 外出人员补助
        /// </summary>
        [DataMemberAttribute]
        public decimal OutSubsidyCost {
            get;
            set;
        }

        /// <summary>
        /// 外出服务单价(公里)
        /// </summary>
        [DataMemberAttribute]
        public decimal OutServiceCarUnitPrice {
            get;
            set;
        }

        /// <summary>
        /// 外出服务车费
        /// </summary>
        [DataMemberAttribute]
        public decimal OutServiceCarCost {
            get;
            set;
        }

        /// <summary>
        /// 外出费
        /// </summary>
        [DataMemberAttribute]
        public decimal AllOutCost {
            get;
            set;
        }

        /// <summary>
        /// 是否存在标准
        /// </summary>
        [DataMemberAttribute]
        public bool HaveRate {
            get;
            set;
        }
    }
}
