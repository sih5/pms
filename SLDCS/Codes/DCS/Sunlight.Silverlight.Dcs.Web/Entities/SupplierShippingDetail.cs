﻿using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class SupplierShippingDetail {
        /// <summary>
        /// 打印次数
        /// </summary>
        [DataMember]
        public int? PrintNumber {
            get;
            set;
        }

        /// <summary>
        /// 序列号
        /// </summary>
        [DataMember]
        public string NumCode {
            get;
            set;
        }

        /// <summary>
        /// 英文名称
        /// </summary>
        [DataMember]
        public string EnglishName {
            get;
            set;
        }

        /// <summary>
        /// 规格型号
        /// </summary>
        [DataMember]
        public string Specification {
            get;
            set;
        }
        /// <summary>
        /// 追溯属性
        /// </summary>
        [DataMember]
        public int? TraceProperty {
            get;
            set;
        }
        /// <summary>
        /// 追溯码
        /// </summary>
        [DataMember]
        public string TraceCode {
            get;
            set;
        }
    }
}
