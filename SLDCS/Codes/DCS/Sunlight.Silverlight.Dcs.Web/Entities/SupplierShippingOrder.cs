﻿using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web
{
    public partial class SupplierShippingOrder
    {
        [DataMemberAttribute]
        public string ContactPerson
        {
            get;
            set;
        }

        [DataMemberAttribute]
        public string ContactPhone
        {
            get;
            set;
        }
         [DataMemberAttribute]
        public decimal TotalAmount {
            get;
            set;
        }
         [DataMemberAttribute]
         public bool IsMust {
             get;
             set;
         }
    }
}
