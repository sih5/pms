﻿using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;
namespace Sunlight.Silverlight.Dcs.Web {
   public partial class SupplierTraceCode {
         [DataMember]
       public string SparePartCode {
           get;
           set;
       }
         [DataMember]
         public string PartsSupplierCode {
             get;
             set;
         }
         [DataMember]
         public string PartsSupplierName {
             get;
             set;
         }
         [DataMember]
         public string PartsPurchaseOrderCode {
             get;
             set;
         }
         [DataMember]
         public string ShippingCode {
             get;
             set;
         }
         [DataMember]
         public string OldTraceCode {
             get;
             set;
         }
         [DataMember]
         public string NewTraceCode {
             get;
             set;
         }       
    }
}
