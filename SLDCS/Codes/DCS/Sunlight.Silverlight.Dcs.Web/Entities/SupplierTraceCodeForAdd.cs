﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;


namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class SupplierTraceCodeForAdd : EntityObject {
        /// <summary>
        ///包装任务单id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }
        /// <summary>
        ///原追溯码
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public string OldTraceCode {
            get;
            set;
        }
        /// <summary>
        ///包装任务单编号
        /// </summary>
        [DataMemberAttribute]
        public string PackingCode {
            get;
            set;
        }
        /// <summary>
        /// 配件Id
        /// </summary>
        [DataMemberAttribute]
        public int? SparePartId {
            get;
            set;
        }
        /// <summary>
        /// 配件编号
        /// </summary>
         [DataMemberAttribute]
        public string SparePartCode {
            get;
            set;
        }
         /// <summary>
         /// 配件名称
         /// </summary>
         [DataMemberAttribute]
         public string SparePartName {
             get;
             set;
         }
         /// <summary>
         /// 采购订单Id
         /// </summary>
         [DataMemberAttribute]
         public int? PartsPurchaseOrderId {
             get;
             set;
         }
         /// <summary>
         /// 采购订单编号
         /// </summary>
         [DataMemberAttribute]
         public string PartsPurchaseOrderCode {
             get;
             set;
         }
         /// <summary>
         /// 供应商ID
         /// </summary>
         [DataMemberAttribute]
         public int? PartsSupplierId {
             get;
             set;
         }
         /// <summary>
         /// 供应商编号
         /// </summary>
         [DataMemberAttribute]
         public string PartsSupplierCode {
             get;
             set;
         }
         /// <summary>
         /// 供应商名称
         /// </summary>
         [DataMemberAttribute]
         public string PartsSupplierName {
             get;
             set;
         }
         /// <summary>
         /// 发运单id
         /// </summary>
         [DataMemberAttribute]
         public int? ShippingId {
             get;
             set;
         }
         /// <summary>
         /// 发运单编号
         /// </summary>
         [DataMemberAttribute]
         public string ShippingCode {
             get;
             set;
         }
        [DataMemberAttribute]
         public int? TraceProperty {
             get;
             set;
         }
        [DataMemberAttribute]
        public string NewTraceCode {
            get;
            set;
        }
         
    }
}
