﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;


namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class TaskListDetail : EntityObject {
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }

        [DataMemberAttribute]
        public string PartsOutboundPlanCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public int SparePartId {
            get;
            set;
        }

        [DataMemberAttribute]
        public string SparePartCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string SparePartName {
            get;
            set;
        }

        [DataMemberAttribute]
        public string SihCode {
            get;
            set;
        }
        
        [DataMemberAttribute]
        public string SourceCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public int TaskQty {
            get;
            set;
        }

        [DataMemberAttribute]
        public string ContainerNumber {
            get;
            set;
        }
    }
}
