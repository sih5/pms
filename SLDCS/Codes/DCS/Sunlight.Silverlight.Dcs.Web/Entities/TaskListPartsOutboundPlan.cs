﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;


namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class TaskListPartsOutboundPlan : EntityObject {
        /// <summary>
        /// 任务Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int TaskId {
            get;
            set;
        }
        /// <summary>
        /// 任务单号
        /// </summary>
        [DataMemberAttribute]
        public string TaskCode {
            get;
            set;
        }

        /// <summary>
        /// 任务单状态
        /// </summary>
        [DataMemberAttribute]
        public string TaskStatus {
            get;
            set;
        }

        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }

        [DataMemberAttribute]
        public string Code {
            get;
            set;
        }

        [DataMemberAttribute]
        public int WarehouseId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string WarehouseCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string WarehouseName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int StorageCompanyId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string StorageCompanyCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string StorageCompanyName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int StorageCompanyType {
            get;
            set;
        }
        [DataMemberAttribute]
        public int CompanyAddressId {
            get;
            set;
        }
        [DataMemberAttribute]
        public int BranchId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string BranchCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string BranchName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int PartsSalesCategoryId {
            get;
            set;
        }
        [DataMemberAttribute]
        public int PartsSalesOrderTypeId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartsSalesOrderTypeName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int CounterpartCompanyId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CounterpartCompanyCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CounterpartCompanyName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int ReceivingCompanyId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ReceivingCompanyCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ReceivingCompanyName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? ReceivingWarehouseId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ReceivingWarehouseCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ReceivingWarehouseName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int SourceId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SourceCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public int OutboundType {
            get;
            set;
        }
        [DataMemberAttribute]
        public int ShippingMethod {
            get;
            set;
        }
        [DataMemberAttribute]
        public int CustomerAccountId {
            get;
            set;
        }
        [DataMemberAttribute]
        public int OriginalRequirementBillId {
            get;
            set;
        }
        [DataMemberAttribute]
        public int OriginalRequirementBillType {
            get;
            set;
        }
        [DataMemberAttribute]
        public string OriginalRequirementBillCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public int Status {
            get;
            set;
        }
        [DataMemberAttribute]
        public string StopComment {
            get;
            set;
        }
        [DataMemberAttribute]
        public bool? IfWmsInterface {
            get;
            set;
        }
        [DataMemberAttribute]
        public string OrderApproveComment {
            get;
            set;
        }
        [DataMemberAttribute]
        public int StoperId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Stoper {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? StopTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public string GPMSPurOrderCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public bool? IsPurDistribution {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SPRAS {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PackingDescription {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Mark {
            get;
            set;
        }
        [DataMemberAttribute]
        public string OtherPackingRequirements {
            get;
            set;
        }
        [DataMemberAttribute]
        public string IsSpecialInspection {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ZPNUMBER {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SAPPurchasePlanCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ERPSourceOrderCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public int OrderTypeId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string OrderTypeName {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ReceivingAddress {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Remark {
            get;
            set;
        }

        [DataMemberAttribute]
        public string CreatorName {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? CreateTime {
            get;
            set;
        }

        [DataMemberAttribute]
        public string ModifierName {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? ModifyTime {
            get;
            set;
        }

        [DataMemberAttribute]
        public string ContactPerson {
            get;
            set;
        }

        [DataMemberAttribute]
        public string ContactPhone {
            get;
            set;
        }

        [DataMemberAttribute]
        public string ApproverName {
            get;
            set;
        }
        [DataMemberAttribute]
        public bool? IsTurn {
            get;
            set;
        }
    }
}
