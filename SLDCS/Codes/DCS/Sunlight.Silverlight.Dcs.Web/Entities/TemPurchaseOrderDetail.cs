﻿using System;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class TemPurchaseOrderDetail {
        [DataMemberAttribute]
        public int ReceiptAmount {
            get;
            set;
        }
        [DataMemberAttribute]
        public string TemPurchaseOrderCode {
            get;
            set;
        }
       
        /// <summary>
        ///  本次发运数量
        /// </summary>
        [DataMemberAttribute]
        public int UnShippingAmount {
            get;
            set;
        }
    }
}
