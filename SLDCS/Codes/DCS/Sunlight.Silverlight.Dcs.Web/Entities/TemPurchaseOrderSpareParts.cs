﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract(IsReference = true)]
    public class TemPurchaseOrderSpareParts : EntityObject {
        [Key]
        [DataMemberAttribute]
        public int Id
        {
            get;
            set;
        }
        /// <summary>
        /// 临时订单编号
        /// </summary>
        [DataMemberAttribute]
        public string Code
        {
            get;
            set;
        }
        /// <summary>
        ///  配件编号
        /// </summary>
        [DataMemberAttribute]
        public string SparePartCode
        {
            get;
            set;
        }

        /// <summary>
        ///  配件名称
        /// </summary>
        [DataMemberAttribute]
        public string SparePartName
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int SupplierId
        {
            get;
            set;
        }
        /// <summary>
        /// 供应商编号
        /// </summary>
        [DataMemberAttribute]
        public string SuplierCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SuplierName
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal? PurchasePrice
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal? SalesPrice
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal? CenterPrice
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public bool? IsPurchase
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public bool? IsSales
        {
            get;
            set;
        }
    }
}
