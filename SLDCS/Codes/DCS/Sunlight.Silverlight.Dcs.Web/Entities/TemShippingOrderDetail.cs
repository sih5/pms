﻿using System;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class TemShippingOrderDetail {
        /// <summary>
        ///  本次确认量
        /// </summary>
        [DataMemberAttribute]
        public int UnConfirmedAmount {
            get;
            set;
        }
    }
}
