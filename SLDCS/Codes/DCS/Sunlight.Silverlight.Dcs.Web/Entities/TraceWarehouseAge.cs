﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class TraceWarehouseAge : EntityObject {
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SparePartCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SparePartName {
            get;
            set;
        }
        /// <summary>
        ///  仓库Id
        /// </summary>
        [DataMemberAttribute]
        public int WarehouseId {
            get;
            set;
        }
        /// <summary>
        ///  仓库名称
        /// </summary>
        [DataMemberAttribute]
        public string WarehouseName {
            get;
            set;
        }
		/// <summary>
        ///  仓库编号
        /// </summary>
        [DataMemberAttribute]
        public string WarehouseCode {
            get;
            set;
        }
        /// <summary>
        /// 库区编号
        /// </summary>
        [DataMemberAttribute]
        public string WarehouseAreaCode {
            get;
            set;
        }
        /// <summary>
        /// 库位编号
        /// </summary>
        [DataMemberAttribute]
        public string StorehouseAreaCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? TraceProperty {
            get;
            set;
        }
        [DataMemberAttribute]
        public string TracePropertyString {
            get;
            set;
        }
        [DataMemberAttribute]
        public int Quantity {
            get;
            set;
        }
        [DataMemberAttribute]
        public int LockedQty {
            get;
            set;
        }
        /// <summary>
        ///经销价（服务站价）
        /// </summary>
        [DataMemberAttribute]
        public Decimal? SalesPrice {
            get;
            set;
        }
        [DataMemberAttribute]
        public string TraceCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SIHLabelCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? InBoundDate {
            get;
            set;
        }
        [DataMemberAttribute]
        public int Age {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? KvAge {
            get;
            set;
        }
        [DataMemberAttribute]
        public bool? GreaterThanZero {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CounterpartCompanyCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CounterpartCompanyName {
            get;
            set;
        }
         [DataMemberAttribute]
        public string OriginalRequirementBillCode {
            get;
            set;
        }
         [DataMemberAttribute]
         public string PackingTaskCode {
             get;
             set;
         }
        [DataMemberAttribute]
         public int? InboundType {
             get;
             set;
         }
         [DataMemberAttribute]
        public string SuplierCode {
            get;
            set;
        }
         [DataMemberAttribute]
         public string SuplierName {
             get;
             set;
         }
        [DataMemberAttribute]
         public string SourceCode {
             get;
             set;
         }
		 [DataMemberAttribute]
		 public int? Status{
			 get;
             set;
		 }
		 [DataMemberAttribute]
		 public string InBoundCode{
			 get;
             set;
		 }
        [DataMemberAttribute]
         public string BoxCode {
             get;
             set;
         }
        [DataMemberAttribute]
        public int? InQty {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? OutQty {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CompanyCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CompanyName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? DownQty {
            get;
            set;
        }
    }
}
