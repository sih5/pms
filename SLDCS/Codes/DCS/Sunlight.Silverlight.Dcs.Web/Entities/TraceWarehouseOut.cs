﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class TraceWarehouseOut : EntityObject {
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SparePartCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SparePartName {
            get;
            set;
        }
        /// <summary>
        ///  仓库Id
        /// </summary>
        [DataMemberAttribute]
        public int WarehouseId {
            get;
            set;
        }
        /// <summary>
        ///  仓库名称
        /// </summary>
        [DataMemberAttribute]
        public string WarehouseName {
            get;
            set;
        }
        /// <summary>
        ///  仓库编号
        /// </summary>
        [DataMemberAttribute]
        public string WarehouseCode {
            get;
            set;
        }
        /// <summary>
        /// 出库单号
        /// </summary>
        [DataMemberAttribute]
        public string OutCode {
            get;
            set;
        }
        /// <summary>
        ///  出库类型
        /// </summary>
        [DataMemberAttribute]
        public int? OutboundType {
            get;
            set;
        }
        
        [DataMemberAttribute]
        public string TraceCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SIHLabelCode {
            get;
            set;
        }
        /// <summary>
        /// 出库时间
        /// </summary>
        [DataMemberAttribute]
        public DateTime? OutTime {
            get;
            set;
        }              
        [DataMemberAttribute]
        public string CounterpartCompanyCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CounterpartCompanyName {
            get;
            set;
        }
        [DataMemberAttribute]
        public string OriginalRequirementBillCode {
            get;
            set;
        }        
        [DataMemberAttribute]
        public int? Status {
            get;
            set;
        }
		[DataMemberAttribute]
		public string ShippingCode{
			get;
            set;
		}
    }
}
