﻿
namespace Sunlight.Silverlight.Dcs.Web {
    public partial class UsedPartsInboundOrder {
        /// <summary>
        /// 验收备注
        /// </summary>
        public string ReceptionRemark {
            get;
            set;
        }
        /// <summary>
        /// 制单单位 取分公司名称
        /// </summary>
        public string BranchName {
            get;
            set;
        }
    }
}
