﻿using System.Runtime.Serialization;
namespace Sunlight.Silverlight.Dcs.Web {
    public partial class UsedPartsOutboundOrder
    {
        /// <summary>
        /// 业务编号
        /// </summary>
        [DataMemberAttribute]
        public string BusinessCode
        {
            get;
            set;
        }
    }
}
