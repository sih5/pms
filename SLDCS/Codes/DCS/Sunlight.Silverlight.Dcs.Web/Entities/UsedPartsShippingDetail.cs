﻿using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 旧件发运单
    /// </summary>
    public partial class UsedPartsShippingDetail {

        /// <summary>
        /// 故障现象描述(条码标签打印)
        /// </summary>
        [DataMemberAttribute]
        public string MalfunctionReason {
            get;
            set;
        }


        /// <summary>
        ///  申请单
        /// </summary>
        [DataMemberAttribute]
        public string RepairClaimApplication {
            get;
            set;
        }

        /// <summary>
        ///  出厂编号
        /// </summary>
        [DataMemberAttribute]
        public string VehicleSerialNumber {
            get;
            set;
        }


    }
}
