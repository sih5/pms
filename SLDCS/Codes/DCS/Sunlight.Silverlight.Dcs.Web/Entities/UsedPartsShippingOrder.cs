﻿
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class UsedPartsShippingOrder {
        /// <summary>
        /// 业务编码
        /// </summary>
        [DataMember]
        public string BusinessCode {
            get;
            set;
        }
    }
}
