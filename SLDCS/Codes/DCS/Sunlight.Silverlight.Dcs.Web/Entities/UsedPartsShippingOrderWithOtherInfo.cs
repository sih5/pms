﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {

    [DataContract(IsReference = true)]
    public class UsedPartsShippingOrderWithOtherInfo : EntityObject {
        ///<Summary>
        ///旧件发运单Id
        ///</Summary>
        [Key]
        [DataMemberAttribute]
        public int UsedPartsShippingOrderId {
            get;
            set;
        }
        ///<Summary>
        ///旧件仓库Id
        ///</Summary>
        [Key]
        [DataMemberAttribute]
        public int UsedPartsWarehouseId {
            get;
            set;
        }

        [DataMemberAttribute]
        public string LogisticCompanyCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string LogisticCompanyName {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Operator {
            get;
            set;
        }

        [DataMemberAttribute]
        public string TransportationVehicle {
            get;
            set;
        }


        [DataMemberAttribute]
        public string OperatorPhone {
            get;
            set;
        }
        ///<Summary>
        ///品牌Id
        ///</Summary>
        [Key]
        [DataMemberAttribute]
        public int PartsSalesCategoryId {
            get;
            set;
        }
        ///<Summary>
        ///服务站Id
        ///</Summary>
        [Key]
        [DataMemberAttribute]
        public int DealerId {
            get;
            set;
        }
        ///<Summary>
        ///服务站编号
        ///</Summary>
        [DataMemberAttribute]
        public string DealerCode {
            get;
            set;
        }
        ///<Summary>
        /// 服务站名称
        ///</Summary>
        [DataMemberAttribute]
        public string DealerName {
            get;
            set;
        }

        ///<Summary>
        /// 经销商分公司信息Id
        ///</Summary>
        [Key]
        [DataMemberAttribute]
        public int DealerServiceInfoId {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? ActualArrivalDate {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? ModifyTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? ConfirmationTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ConfirmorName {
            get;
            set;
        }

        [DataMemberAttribute]
        public string ModifierName {
            get;
            set;
        }

        [DataMemberAttribute]
        public DateTime? ShippingDate {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? RequestedArrivalDate {
            get;
            set;
        }


        ///<Summary>
        ///业务编码
        ///</Summary>
        [DataMemberAttribute]
        public string BussinessCode {
            get;
            set;
        }
        ///<Summary>
        ///旧件发运单编号
        ///</Summary>
        [DataMemberAttribute]
        public string UsedPartsShippingOrderCode {
            get;
            set;
        }
        ///<Summary>
        ///分公司Id
        ///</Summary>
        [DataMemberAttribute]
        public int BranchId {
            get;
            set;
        }
        ///<Summary>
        ///分公司编号
        ///</Summary>
        [DataMemberAttribute]
        public string BranchCode {
            get;
            set;
        }

        ///<Summary>
        ///分公司名称
        ///</Summary>
        [DataMemberAttribute]
        public string BranchName {
            get;
            set;
        }
        ///<Summary>
        ///品牌名称
        ///</Summary>
        [DataMemberAttribute]
        public string PartsSalesCategoryName {
            get;
            set;
        }
        ///<Summary>
        ///旧件仓库名称
        ///</Summary>
        [DataMemberAttribute]
        public string UsedPartsWarehouseName {
            get;
            set;
        }
        ///<Summary>
        ///发运方式
        ///</Summary>
        [DataMemberAttribute]
        public int? ShippingMethod {
            get;
            set;
        }
        ///<Summary>
        ///发运方
        ///</Summary>
        [DataMemberAttribute]
        public int Dispatcher {
            get;
            set;
        }
        ///<Summary>
        ///仓库联系人
        ///</Summary>
        [DataMemberAttribute]
        public string WarehouseContact {
            get;
            set;
        }
        ///<Summary>
        ///仓库电话 
        ///</Summary>
        [DataMemberAttribute]
        public string WarehousePhone {
            get;
            set;
        }
        ///<Summary>
        ///收货地址 
        ///</Summary>
        [DataMemberAttribute]
        public string DestinationAddress {
            get;
            set;
        }
        ///<Summary>
        ///承运司机
        ///</Summary>
        [DataMemberAttribute]
        public string TransportDriver {
            get;
            set;
        }
        ///<Summary>
        ///司机电话 
        ///</Summary>
        [DataMemberAttribute]
        public string DriverPhone {
            get;
            set;
        }
        ///<Summary>
        ///督办人 
        ///</Summary>
        [DataMemberAttribute]
        public string Supervisor {
            get;
            set;
        }
        ///<Summary>
        ///督办人电话 
        ///</Summary>
        [DataMemberAttribute]
        public string SupervisorPhone {
            get;
            set;
        }
        ///<Summary>
        ///总金额 
        ///</Summary>
        [DataMemberAttribute]
        public decimal SumUnitPrice {
            get;
            set;
        }
        ///<Summary>
        ///品种数量  
        ///</Summary>
        [DataMemberAttribute]
        public int DetailCount {
            get;
            set;
        }
        ///<Summary>
        ///状态
        ///</Summary>
        [DataMemberAttribute]
        public int Status {
            get;
            set;
        }
        ///<Summary>
        ///备注 
        ///</Summary>
        [DataMemberAttribute]
        public string Remark {
            get;
            set;
        }
        ///<Summary>
        ///验收备注 
        ///</Summary>
        [DataMemberAttribute]
        public string ReceptionRemark {
            get;
            set;
        }
        ///<Summary>
        ///创建人Id 
        ///</Summary>
        [DataMemberAttribute]
        public int? CreatorId {
            get;
            set;
        }
        ///<Summary>
        ///创建人姓名
        ///</Summary>
        [DataMemberAttribute]
        public string CreatorName {
            get;
            set;
        }
        ///<Summary>
        ///创建时间 
        ///</Summary>
        [DataMemberAttribute]
        public DateTime? CreateTime {
            get;
            set;
        }

    }
}
