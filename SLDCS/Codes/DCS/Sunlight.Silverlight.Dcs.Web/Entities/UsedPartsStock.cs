﻿using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 旧件库存
    /// </summary>
    public partial class UsedPartsStock {
        /// <summary>
        /// 故障现象描述(旧件条码标签打印)
        /// </summary>
        [DataMember]
        public string MalfunctionReason {
            get;
            set;
        }
    }
}
