﻿
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class UsedPartsTransferDetail {
        /// <summary>
        /// 序号。 打印使用
        /// </summary>
        [DataMember]
        public int Number {
            get;
            set;
        }

        /// <summary>
        /// 序号。 打印使用
        /// </summary>
        [DataMember]
        public string MalfunctionReason {
            get;
            set;
        }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public string PartsSalesCategoryName {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public string RepairClaimBillVIN {
            get;
            set;
        }

    }
}
