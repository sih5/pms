﻿
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class UsedPartsTransferOrder {

        /// <summary>
        /// 旧件仓库联系电话 打印用
        /// </summary>
        [DataMember]
        public string ContactPhone {
            get;
            set;
        }

        /// <summary>
        /// 旧件仓库联系人 打印用
        /// </summary>
        [DataMember]
        public string Contact {
            get;
            set;
        }

        /// <summary>
        /// 源旧件仓库所属企业编号 打印用
        /// </summary>
        [DataMember]
        public string OriginWarehouseCompanyCode {
            get;
            set;

        }

        /// <summary>
        /// 源旧件仓库所属企业名称 打印用
        /// </summary>
        [DataMember]
        public string OriginWarehouseCompanyName {
            get;
            set;
        }



        /// <summary>
        /// 目的旧件仓库所属企业编号 打印用
        /// </summary>
        [DataMember]
        public string DestinationWarehouseCompanyCode {
            get;
            set;

        }

        /// <summary>
        /// 目的旧件仓库所属企业名称 打印用
        /// </summary>
        [DataMember]
        public string DestinationWarehouseCompanyName {
            get;
            set;
        }
    }
}
