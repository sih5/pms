﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 虚拟代理库入库记录
    /// </summary>
    [DataContract(IsReference = true)]
    public class VehicleAgentsHistoryIn : EntityObject {


        /// <summary>
        ///分公司
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int BranchId {
            get;
            set;
        }
        /// <summary>
        /// 分公司编号
        /// </summary>
        [DataMemberAttribute]
        public string BranchCode {
            get;
            set;
        }
        /// <summary>
        ///分公司名称
        /// </summary>
        [DataMemberAttribute]
        public string BranchName {
            get;
            set;
        }
        /// <summary>
        ///品牌
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int PartsSalesCategoryId {
            get;
            set;
        }
        /// <summary>
        ///品牌
        /// </summary>
        [DataMemberAttribute]
        public string PartsSalesCategoryName
        {
            get;
            set;
        }  
        /// <summary>
        /// 仓库ID
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int WarehouseId {
            get;
            set;
        }
        /// <summary>
        /// 仓库编号
        /// </summary>
        [DataMemberAttribute]
        public string WarehouseCode {
            get;
            set;
        }
    
        /// <summary>
        /// 仓库名称
        /// </summary>
        [DataMemberAttribute]
        public string WarehouseName {
            get;
            set;
        }
        ///// <summary>
        ///// 入库类型
        ///// </summary>
        //[DataMemberAttribute]
        //public string InboundType {
        //    get;
        //    set;
        //}
        /// <summary>
        /// 入库类型
        /// </summary>
        [DataMemberAttribute]
        public int InboundType {
            get;
            set;
        }
        /// <summary>
        /// 代理库编号
        /// </summary>
        [DataMemberAttribute]
        public string AgencyCode {
            get;
            set;
        }
        /// <summary>
        /// 代理库名称
        /// </summary>
        [DataMemberAttribute]
        public string AgencyName {
            get;
            set;
        }
        /// <summary>
        ///// 配件图号
        ///// </summary>
        //[DataMemberAttribute]
        //public string PartCode {
        //    get;
        //    set;
        //}
        /// <summary>
        /// 配件编号
        /// </summary>
        [DataMemberAttribute]
        public string PartCode {
            get;
            set;
        }

        /// <summary>
        /// 配件Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int PartId {
            get;
            set;
        }
        /// <summary>
        /// 配件名称
        /// </summary>
        [DataMemberAttribute]
        public string PartName {
            get;
            set;
        }
    
        ///// <summary>
        ///// 查询时间
        ///// </summary>   
        //[DataMemberAttribute]
        //[Display(ResourceType = typeof(EntityStrings), Name = "_Common_CreateTime")]
        //public DateTime? CreateTime {
        //    get;
        //    set;
        //}
        /// <summary>
        /// 仓储企业编号
        /// </summary>
        [DataMemberAttribute]
        public string StorageCompanyCode {
            get;
            set;
        }
        /// <summary>
        /// 仓储企业名称
        /// </summary>
        [DataMemberAttribute]
        public string StorageCompanyName {
            get;
            set;
        }
        /// <summary>
        /// 省份
        /// </summary>
        [DataMemberAttribute]
        public string ProvinceName {
            get;
            set;
        }
        /// <summary>
        /// 入库单编号
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public string PartsInboundPlanCode {
            get;
            set;
        }
        /// <summary>
        /// 入库时间
        /// </summary>   
        [DataMemberAttribute]       
        public DateTime? PartsInboundPlanTime {
            get;
            set;
        }
        /// <summary>
        /// 原始需求单据编号
        /// </summary>   
        [DataMemberAttribute]
        public string OriginalRequirementBillCode {
            get;
            set;
        }
        /// <summary>
        /// 原始需求单据类型
        /// </summary>   
        [DataMemberAttribute]
        public int OriginalRequirementBillType {
            get;
            set;
        } 
        /// <summary>
        /// 库位
        /// </summary>   
        [DataMemberAttribute]
        public string WarehouseAreaCode {
            get;
            set;
        }
        /// <summary>
        /// 入库数量
        /// </summary>
        [DataMemberAttribute]
        public int InspectedQuantity {
            get;
            set;
        }
        /// <summary>
        /// 代理库批发价
        /// </summary>
        [DataMemberAttribute]
        public decimal SettlementPrice {
            get;
            set;
        }

        /// <summary>
        /// 入库金额
        /// </summary>
        [DataMemberAttribute]
        public decimal Price {
            get;
            set;
        }
        /// <summary>
        /// 结算状态
        /// </summary>
        [DataMemberAttribute]
        public int SettlementStatus{
            get;
            set;
        }
        ///// <summary>
        ///// 结算状态
        ///// </summary>
        //[DataMemberAttribute]
        //public string SettlementStatus {
        //    get;
        //    set;
        //}
        /// <summary>
        /// 对方单位编号
        /// </summary>   
        [DataMemberAttribute]
        public string CounterPartCompanyCode {
            get;
            set;
        }
        /// <summary>
        /// 对方单位名称
        /// </summary>   
        [DataMemberAttribute]
        public string CounterPartCompanyName {
            get;
            set;
        }
    }
}
