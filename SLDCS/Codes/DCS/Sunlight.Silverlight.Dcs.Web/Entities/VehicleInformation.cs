﻿using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class VehicleInformation {
        /// <summary>
        /// 售出状态
        /// </summary>
        [DataMemberAttribute]
        public int SalesStatus {
            get;
            set;
        }

        /// <summary>
        /// 是否有单车服务卡
        /// </summary>
        [DataMemberAttribute]
        public bool HasVehicleWarrantyCard {
            get;
            set;
        }
         /// <summary>
        /// 是否有单车服务卡
        /// </summary>
        [DataMemberAttribute]
        public string ServicePartsSalesCategoryName {
            get;
            set;
        }

        

        /// <summary>
        /// 导入的下线时间
        /// </summary>
        [DataMemberAttribute]
        public string RolloutDateStr {
            get;
            set;
        }

        /// <summary>
        /// 导入的经销商编号
        /// </summary>
        [DataMemberAttribute]
        public string DealerCode {
            get;
            set;
        }

        /// <summary>
        /// 仓库名称
        /// </summary>
        [DataMemberAttribute]
        public string VehicleWarehouseName {
            get;
            set;
        }

        /// <summary>
        /// 服务产品线名称名称
        /// </summary>
        [DataMemberAttribute]
        public string ServiceProductLineName {
            get;
            set;
        }
        /// <summary>
        /// 客户名称
        /// </summary>
        [DataMemberAttribute]
        public string CustomerName {
            get;
            set;
        }
        /// <summary>
        /// 客户类型
        /// </summary>
        [DataMemberAttribute]
        public int? CustomerType {
            get;
            set;
        }
        /// <summary>
        /// 证件类型
        /// </summary>
        [DataMemberAttribute]
        public int? IdDocumentType {
            get;
            set;
        }
        /// <summary>
        /// 手机号码
        /// </summary>
        [DataMemberAttribute]
        public string CustomerCellPhoneNumber {
            get;
            set;
        }

    
    }
}
