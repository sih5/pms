﻿using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web.Entities {
    /// <summary>
    /// 虚拟采购退货结算单
    /// </summary>
    [DataContract(IsReference = true)]
    public class VehiclePartsPurchaseRtnSettleDetail : EntityObject {
        /// <summary>
        /// 出库单编号
        /// </summary>
        [DataMemberAttribute]
        public string SourceCode {
            get;
            set;
        }

        /// <summary>
        /// 计划价
        /// </summary>
        [DataMemberAttribute]
        public decimal CostPrice {
            get;
            set;
        }

        /// <summary>
        /// 品牌
        /// </summary>
        [DataMemberAttribute]
        public string PartsSalesCategoryName {
            get;
            set;
        }

        /// <summary>
        /// 单位（供应商名称）
        /// </summary>
        [DataMemberAttribute]
        public string PartsSupplierName {
            get;
            set;
        }

        /// <summary>
        /// 结算单号
        /// </summary>
        [DataMemberAttribute]
        public string Code {
            get;
            set;
        }

        /// <summary>
        /// 结算金额
        /// </summary>
        [DataMemberAttribute]
        public decimal TotalSettlementAmount {
            get;
            set;
        }

        /// <summary>
        /// 图号
        /// </summary>
        [DataMemberAttribute]
        public string SparePartCode {
            get;
            set;
        }

        /// <summary>
        /// 配件名称
        /// </summary>
        [DataMemberAttribute]
        public string SparePartName {
            get;
            set;
        }

        /// <summary>
        /// 数量
        /// </summary>
        [DataMemberAttribute]
        public int QuantityToSettle {
            get;
            set;
        }

        /// <summary>
        /// 价格
        /// </summary>
        [DataMemberAttribute]
        public decimal SettlementAmount {
            get;
            set;
        }

        /// <summary>
        /// 制单单位
        /// </summary>
        [DataMemberAttribute]
        public string BranchName {
            get;
            set;
        }

        /// <summary>
        /// 制单人
        /// </summary>
        [DataMemberAttribute]
        public string CreatorName {
            get;
            set;
        }

        /// <summary>
        /// 制单时间
        /// </summary>
        [DataMemberAttribute]
        public decimal CreatorTime {
            get;
            set;
        }
    }
}
