﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 虚拟配件销售订单
    /// </summary>
    [DataContract(IsReference = true)]
    public class VehiclePartsSalesOrdercs : EntityObject {
        /// <summary>
        /// 配件销售订单Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartsOutboundPlanCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? PartsOutboundPlanStatus {
            get;
            set;
        }
        [DataMemberAttribute]
        public int Status {
            get;
            set;
        }

        [DataMemberAttribute]
        public int BranchId {
            get;
            set;
        }
        /// <summary>
        /// 配件销售订单清单Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int DetailId {
            get;
            set;
        }
        /// <summary>
        /// 配件销售订单编号
        /// </summary>
        [DataMemberAttribute, Display(ResourceType = typeof(EntityStrings), Name = "PartsSalesOrder_Code")]
        public string PartsSalesOrderCode {
            get;
            set;
        }
        /// <summary>
        /// 收货企业Id
        /// </summary>
        [DataMemberAttribute]
        public int ReceivingCompanyId {
            get;
            set;
        }
        /// <summary>
        /// 收货企业编号
        /// </summary>
        [DataMemberAttribute]
        public string ReceivingCompanyCode {
            get;
            set;
        }
        /// <summary>
        /// 收货企业名称
        /// </summary>
        [DataMemberAttribute]
        public string ReceivingCompanyName {
            get;
            set;
        }
        /// <summary>
        /// 销售企业Id
        /// </summary>
        [DataMemberAttribute]
        public int SalesUnitOwnerCompanyId {
            get;
            set;
        }
        /// <summary>
        /// 销售企业编号
        /// </summary>
        [DataMemberAttribute]
        public string SalesUnitOwnerCompanyCode {
            get;
            set;
        }
        /// <summary>
        /// 销售企业名称
        /// </summary>
        [DataMemberAttribute]
        public string SalesUnitOwnerCompanyName {
            get;
            set;
        }
        /// <summary>
        /// 品牌
        /// </summary>
        [DataMemberAttribute, Display(ResourceType = typeof(EntityStrings), Name = "PartsSalesCategory_Name")]
        public string SalesCategoryName {
            get;
            set;
        }
        /// <summary>
        /// 配件Id
        /// </summary>
        [DataMemberAttribute]
        public int SparePartId {
            get;
            set;
        }
        /// <summary>
        /// 配件编号
        /// </summary>
        [DataMemberAttribute, Display(ResourceType = typeof(EntityStrings), Name = "SparePart_Code")]
        public string SparePartCode {
            get;
            set;
        }
        /// <summary>
        /// 配件名称
        /// </summary>
        [DataMemberAttribute, Display(ResourceType = typeof(EntityStrings), Name = "SparePart_Name")]
        public string SparePartName {
            get;
            set;
        }
        /// <summary>
        /// 订货数量
        /// </summary>
        [DataMemberAttribute]
        public int OrderedQuantity {
            get;
            set;
        }

        /// <summary>
        /// 审核数量
        /// </summary>
        [DataMemberAttribute]
        public int ApproveQuantity {
            get;
            set;
        }

        /// <summary>
        /// 入库数量(配件入库计划清单.计划量)
        /// </summary>
        [DataMemberAttribute]
        [Key]
        public int InboundQuantity {
            get;
            set;
        }

        /// <summary>
        /// 出库数量(配件出库单清单.出库数量)
        /// </summary>
        [DataMemberAttribute]
        [Key]
        public int OutboundQuantity {
            get;
            set;
        }

        /// <summary>
        /// 发运数量(配件发运单清单.发运量 )
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int ShippingQuantity {
            get;
            set;
        }

        /// <summary>
        /// 确认数量(配件发运单清单.确认量)
        /// </summary>
        [DataMemberAttribute]
        public int ConfirmQuantity {
            get;
            set;
        }

        /// <summary>
        /// 订货单价
        /// </summary>
        [DataMemberAttribute]
        public decimal OrderPrice {
            get;
            set;
        }
        /// <summary>
        /// 创建时间
        /// </summary>
        [DataMemberAttribute]
        public DateTime? CreateTime {
            get;
            set;
        }

        /// <summary>
        /// 发运方式
        /// </summary>
        [DataMemberAttribute, Display(ResourceType = typeof(EntityStrings), Name = "PartsOutboundPlan_ShippingMethod")]
        public int? ShippingMethod {
            get;
            set;
        }


        [DataMemberAttribute]
        public int? WarehouseId {
            get;
            set;
        }

        [Key]
        [DataMemberAttribute]
        public int SalesCategoryId {
            get;
            set;
        }

        /// <summary>
        /// 仓库编号
        /// </summary>
        [DataMemberAttribute, Display(ResourceType = typeof(EntityStrings), Name = "Warehouse_Code")]
        public string WarehouseCode {
            get;
            set;
        }

        /// <summary>
        /// 仓库名称
        /// </summary>
        [DataMemberAttribute, Display(ResourceType = typeof(EntityStrings), Name = "Warehouse_Name")]
        public string WarehouseName {
            get;
            set;
        }

        [Key]
        [DataMemberAttribute]
        public int SubmitCompanyId {
            get;
            set;
        }

        /// <summary>
        /// 提报单位编号
        /// </summary>
        [DataMemberAttribute]
        public string SubmitCompanyCode {
            get;
            set;
        }

        /// <summary>
        /// 提报单位名称
        /// </summary>
        [DataMemberAttribute, Display(ResourceType = typeof(EntityStrings), Name = "PartsSalesOrder_SubmitCompanyName")]
        public string SubmitCompanyName {
            get;
            set;
        }

        /// <summary>
        /// 客户类型
        /// </summary>
        [DataMemberAttribute, Display(ResourceType = typeof(EntityStrings), Name = "PartsSalesOrder_CustomerType")]
        public int CustomerType {
            get;
            set;
        }

        /// <summary>
        /// 销售订单类型
        /// </summary>
        [DataMemberAttribute, Display(ResourceType = typeof(EntityStrings), Name = "PartsSalesOrderType_Name")]
        public string PartsSalesOrderTypeName {
            get;
            set;
        }

        /// <summary>
        /// 提交时间
        /// </summary>
        [DataMemberAttribute, Display(ResourceType = typeof(EntityStrings), Name = "PreSaleCheckOrder_SubmitTime")]
        public DateTime? SubmitTime {
            get;
            set;
        }

        /// <summary>
        /// 审批时间
        /// </summary>
        [DataMemberAttribute, Display(ResourceType = typeof(EntityStrings), Name = "_Common_ApproveTime")]
        public DateTime? ApproveTime {
            get;
            set;
        }

        /// <summary>
        /// 发运单号
        /// </summary>
        [Key]
        [DataMemberAttribute, Display(ResourceType = typeof(EntityStrings), Name = "UsedPartsShippingOrder_Code")]
        public string PartsShippingOrderCode {
            get;
            set;
        }

        /// <summary>
        /// 发运时间(发运单发运日期)
        /// </summary>
        [DataMemberAttribute, Display(ResourceType = typeof(EntityStrings), Name = "UsedPartsShippingOrder_ShippingDate")]
        public DateTime? ShippingDate {
            get;
            set;
        }
        /// <summary>
        /// 发运状态
        /// </summary>
        [DataMemberAttribute]
        public int? ShippingStatus {
            get;
            set;
        }

        /// <summary>
        /// 收货确认人
        /// </summary>
        [DataMemberAttribute, Display(ResourceType = typeof(EntityStrings), Name = "PartsShippingOrder_ConsigneeName")]
        public string PSOrderConsigneeName {
            get;
            set;
        }

        /// <summary>
        /// 收货确认时间
        /// </summary>
        [DataMemberAttribute, Display(ResourceType = typeof(EntityStrings), Name = "PartsShippingOrder_ConfirmedReceptionTime")]
        public DateTime? ConfirmedReceptionTime {
            get;
            set;
        }

        /// <summary>
        /// 代理库入库计划单号(配件入库计划编号)
        /// </summary>
        [DataMemberAttribute]
        [Key]
        public string PartsInboundPlanCode {
            get;
            set;
        }

        /// <summary>
        /// 代理库入库计划状态(配件入库计划状态)
        /// </summary>
        [DataMemberAttribute]
        public int PartsInboundPlanStatus {
            get;
            set;
        }

        /// <summary>
        /// 代理库入库单号(配件入库检验单编号)
        /// </summary>
        [DataMemberAttribute]
        public string PartsInboundCheckBillCode {
            get;
            set;
        }
        /// <summary>
        /// ERP平台单号(ERP原始订单编号)
        /// </summary>
        [DataMemberAttribute]
        public string ERPSourceOrderCode {
            get;
            set;
        }
    }
}
