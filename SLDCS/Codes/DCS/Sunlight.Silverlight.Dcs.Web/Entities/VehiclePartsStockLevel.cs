﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using Telerik.Reporting;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class VehiclePartsStockLevel {
        /// <summary>
        /// 可用库存
        /// </summary>
        [DataMember]
        public int? UsableQuantity {
            get;
            set;
        }

        /// <summary>
        /// 销售价
        /// </summary>
        [DataMember]
        public decimal? PartsSalesPrice {
            get;
            set;
        }

        [DataMember]
        public List<VehiclePartsStockLevel> list {
            get;
            set;
        }
    }
}
