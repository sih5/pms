﻿
using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class VietualPartsSalesRtnSettlementRef : EntityObject {
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }

        [DataMemberAttribute]
        public string SourceCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string WarehouseName {
            get;
            set;
        }

        [DataMemberAttribute]
        public DateTime SourceBillCreateTime {
            get;
            set;
        }

        [DataMemberAttribute]
        public decimal SettlementAmount {
            get;
            set;
        }

        [DataMemberAttribute]
        public string ReturnReason {
            get;
            set;
        }

        /// <summary>
        ///  销售退货单备注
        /// </summary>
        [DataMemberAttribute]
        public string PartsSalesReturnBillRemark {
            get;
            set;
        }

        [DataMemberAttribute]
        public int PartsSalesRtnSettlementId {
            get;
            set;
        }
    }
}
