﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class VirPartsInventoryDetail : EntityObject {
         [DataMemberAttribute]
        public string SparePartCode {
            get;
            set;        
        }
         [DataMemberAttribute]
         public string SparePartName {
             get;
             set;
         }
         [DataMemberAttribute]
         public string WarehouseAreaCode {
             get;
             set;
         }
         [DataMemberAttribute]
         public string ReferenceCode {
             get;
             set;
         }
         [DataMemberAttribute]
         public int? CurrentStorage {
             get;
             set;
         }
         [DataMemberAttribute]
         public int? StorageAfterInventory {
             get;
             set;
         }
         [DataMemberAttribute]
         public string TopCode {
             get;
             set;
         }
         [DataMemberAttribute]
         public string Remark {
             get;
             set;
         }
    }
}
