﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class VirPartsShiftOrderDetail : EntityObject {
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }
        [DataMemberAttribute]
        public int PartsShiftOrderId {
            get;
            set;
        }
        [DataMemberAttribute]
        public int SparePartId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SparePartCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SparePartName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int OriginalWarehouseAreaId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string OriginalWarehouseAreaCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public int OriginalWarehouseAreaCategory {
            get;
            set;
        }
        [DataMemberAttribute]
        public int DestWarehouseAreaId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string DestWarehouseAreaCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public int DestWarehouseAreaCategory {
            get;
            set;
        }
        [DataMemberAttribute]
        public string BatchNumber {
            get;
            set;
        }
        [DataMemberAttribute]
        public int Quantity {
            get;
            set;
        }
        [DataMemberAttribute]
        public string OrWarehouseAreaCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string DeWarehouseAreaCode {
            get;
            set;
        }
    }
}
