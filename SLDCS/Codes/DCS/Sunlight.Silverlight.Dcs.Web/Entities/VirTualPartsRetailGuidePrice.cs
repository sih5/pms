﻿using System.Data.Objects.DataClasses;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System;

namespace Sunlight.Silverlight.Dcs.Web
{
    [DataContract(IsReference = true)]
    public class VirTualPartsRetailGuidePrice : EntityObject
    {
        /// <summary>
        /// Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int Id
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal SalesPrice
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SparePartName
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int BranchId
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int PartsSalesCategoryId
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartsSalesCategoryCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartsSalesCategoryName
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int SparePartId
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SparePartCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal RetailGuidePrice
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? ValidationTime
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? ExpireTime
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int Status
        {
            get;
            set;
        }
    }
}
