﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 虚拟服务站客户账户
    /// </summary>
    [DataContract(IsReference = true)]
    public class VirtualAgencyCustomerAccount : EntityObject {
        /// <summary>
        /// 客户账户Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }
        /// <summary>
        /// 经销商基本信息编号
        /// </summary>
        [DataMemberAttribute]
        public string DealerCode {
            get;
            set;
        }
        /// <summary>
        /// 经销商基本信息名称
        /// </summary>
        [DataMemberAttribute]
        public string DealerName {
            get;
            set;
        }
        /// <summary>
        /// 代理库编号
        /// </summary>
        [DataMemberAttribute]
        public string AgencyCode {
            get;
            set;
        }
        /// <summary>
        /// 代理库名称
        /// </summary>
        [DataMemberAttribute]
        public string AgencyName {
            get;
            set;
        }
        /// <summary>
        ///  客户账户变更明细.发生日期
        /// </summary>
        [DataMemberAttribute]
        public DateTime? ProcessDate {
            get;
            set;
        }
        /// <summary>
        /// 客户账户变更明细.变更金额
        /// </summary>
        [DataMemberAttribute]
        public decimal? ChangeAmount {
            get;
            set;
        }
        /// <summary>
        /// 客户账户变更明细.业务类型
        /// </summary>
        [DataMemberAttribute]
        public int? BusinessType {
            get;
            set;
        }
        /// <summary>
        /// 客户账户变更明细.账户变更单据编号
        /// </summary>
        [DataMemberAttribute]
        public string SourceCode {
            get;
            set;
        }
        /// <summary>
        ///客户账户变更明细.账户变更摘要
        /// </summary>
        [DataMemberAttribute]
        public string Summary {
            get;
            set;
        }
        /// <summary>
        /// 客户账户变更明细.发生前金额
        /// </summary>
        [DataMemberAttribute]
        public decimal? BeforeChangeAmount {
            get;
            set;
        }
        /// <summary>
        /// 客户账户变更明细.发生后金额
        /// </summary>
        [DataMemberAttribute]
        public decimal? AfterChangeAmount {
            get;
            set;
        }
        /// <summary>
        /// 客户账户变更明细.借方
        /// </summary>
        [DataMemberAttribute]
        public decimal? Debit {
            get;
            set;
        }
        /// <summary>
        ///  客户账户变更明细.贷方   
        /// </summary>
        [DataMemberAttribute]
        public decimal? Credit {
            get;
            set;
        }
        /// <summary>
        ///账户组.名称
        /// </summary>
        [DataMemberAttribute]
        public string AccountGroupName {
            get;
            set;
        }
      
    }
}
