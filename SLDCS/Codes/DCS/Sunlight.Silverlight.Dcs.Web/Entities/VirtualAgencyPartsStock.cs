﻿using System.Data.Objects.DataClasses;
using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web
{
    [DataContract(IsReference = true)]
    public class VirtualAgencyPartsStock : EntityObject
    {

        [Key]
        [DataMemberAttribute]
        public int Id
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CompanyCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CompanyName
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string WarehouseCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string WarehouseName
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string WarehouseAreaCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string WarehouseAreaPositionCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SparePartCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SparePartName
        {
            get;
            set;
        }

        [DataMemberAttribute]
        public int Quantity
        {
            get;
            set;
        }
       
        [DataMemberAttribute]
        public int WarehouseAreaCategory
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int ActualAvailableStock
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? PartABC
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal? CenterPrice
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal QuantityAll {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal ActualAll {
            get;
            set;
        }
    }
}
