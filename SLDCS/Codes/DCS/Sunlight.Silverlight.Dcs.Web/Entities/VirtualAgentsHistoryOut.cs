﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 虚拟代理库出库记录
    /// </summary>
    [DataContract(IsReference = true)]
    public class VirtualAgentsHistoryOut : EntityObject {


        /// <summary>
        ///分公司
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int BranchId {
            get;
            set;
        }
        /// <summary>
        /// 分公司编号
        /// </summary>
        [DataMemberAttribute]
        public string BranchCode {
            get;
            set;
        }
        /// <summary>
        ///分公司名称
        /// </summary>
        [DataMemberAttribute]
        public string BranchName {
            get;
            set;
        }
        /// <summary>
        ///品牌ID
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int BrandId {
            get;
            set;
        }
        /// <summary>
        ///品牌
        /// </summary>
        [DataMemberAttribute]
        public string BrandName {
            get;
            set;
        }
        /// <summary>
        /// 仓库ID
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int WarehouseId {
            get;
            set;
        }
        /// <summary>
        /// 仓库编号
        /// </summary>
        [DataMemberAttribute]
        public string WarehouseCode {
            get;
            set;
        }

        /// <summary>
        /// 仓库名称
        /// </summary>
        [DataMemberAttribute]
        public string WarehouseName {
            get;
            set;
        }
        ///// <summary>
        ///// 入库类型
        ///// </summary>
        //[DataMemberAttribute]
        //public string InboundType {
        //    get;
        //    set;
        //}
        /// <summary>
        /// 出库类型
        /// </summary>
        [DataMemberAttribute]
        public int OutboundType {
            get;
            set;
        }
        /// <summary>
        /// 出库单号
        /// </summary>
        [DataMemberAttribute]
        public string OutboundCode {
            get;
            set;
        }
        /// <summary>
        /// 出库时间
        /// </summary>
        [DataMemberAttribute]
        public DateTime? OutboundTime {
            get;
            set;
        }

        /// <summary>
        /// 配件编号
        /// </summary>
        [DataMemberAttribute]
        public string PartCode {
            get;
            set;
        }

        /// <summary>
        /// 配件Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int PartId {
            get;
            set;
        }
        /// <summary>
        /// 配件名称
        /// </summary>
        [DataMemberAttribute]
        public string PartName {
            get;
            set;
        }

        /// </summary>
        /// 业务编码
        /// </summary>
        [DataMemberAttribute]
        public string Businessode {
            get;
            set;
        }

        /// <summary>
        /// 仓储企业编号
        /// </summary>
        [DataMemberAttribute]
        public string StorageCompanyCode {
            get;
            set;
        }
        /// <summary>
        /// 仓储企业名称
        /// </summary>
        [DataMemberAttribute]
        public string StorageCompanyName {
            get;
            set;
        }
        /// <summary>
        /// 省份
        /// </summary>
        [DataMemberAttribute]
        public string ProvinceName {
            get;
            set;
        }

        /// <summary>
        /// 原始需求单据编号
        /// </summary>   
        [DataMemberAttribute]
        public string OriginalRequirementBillCode {
            get;
            set;
        }
        /// <summary>
        /// 原始需求单据类型
        /// </summary>   
        [DataMemberAttribute]
        public int? OriginalRequirementBillType {
            get;
            set;
        }
        /// <summary>
        /// 库位
        /// </summary>   
        [DataMemberAttribute]
        public string WarehouseAreaCode {
            get;
            set;
        }
        /// <summary>
        /// 出库数量
        /// </summary>
        [DataMemberAttribute]
        public int OutboundAmount {
            get;
            set;
        }

        /// <summary>
        /// 代理库批发价
        /// </summary>
        [DataMemberAttribute]
        public decimal SettlementPrice {
            get;
            set;
        }
        /// <summary>
        /// 出库金额
        /// </summary>
        [DataMemberAttribute]
        public decimal Price {
            get;
            set;
        }
        /// <summary>
        /// 结算状态
        /// </summary>
        [DataMemberAttribute]
        public int SettlementStatus {
            get;
            set;
        }

        /// <summary>
        /// 收货单位编号
        /// </summary>   
        [DataMemberAttribute]
        public string ReceivingCompanyCode {
            get;
            set;
        }
        /// <summary>
        /// 收货单位名称
        /// </summary>   
        [DataMemberAttribute]
        public string ReceivingCompanyName {
            get;
            set;
        }
        /// <summary>
        /// 收货仓库编号
        /// </summary>   
        [DataMemberAttribute]
        public string ReceivingWarehouseCode {
            get;
            set;
        }
        /// <summary>
        /// 收货仓库名称
        /// </summary>   
        [DataMemberAttribute]
        public string ReceivingWarehouseName {
            get;
            set;
        }
    }
}
