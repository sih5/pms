﻿using System;
using System.Data.Objects.DataClasses;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;


namespace Sunlight.Silverlight.Dcs.Web
{
     [DataContract(IsReference = true)]
    public class VirtualBoxUpTask : EntityObject
    {
        [Key]
        [DataMemberAttribute]
        public int Id

        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Code
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int PartsSalesCategoryId
        {
            get;
            set;
        }
       [DataMemberAttribute]
        public string PartsSalesCategoryName
        {
            get;
            set;
        }
       [DataMemberAttribute]
       public int WarehouseId
       {
           get;
           set;
       }
       [DataMemberAttribute]
       public string WarehouseCode
       {
           get;
           set;
       }
       [DataMemberAttribute]
       public string WarehouseName
       {
           get;
           set;
       }
         
       [DataMemberAttribute]
       public int CounterpartCompanyId
       {
           get;
           set;
       }
       [DataMemberAttribute]
       public string CounterpartCompanyCode
       {
           get;
           set;
       }
       [DataMemberAttribute]
       public string CounterpartCompanyName
       {
           get;
           set;
       }
       [DataMemberAttribute]
       public int Status
       {
           get;
           set;
       }
       [DataMemberAttribute]
       public bool IsExistShippingOrder
       {
           get;
           set;
       }
       [DataMemberAttribute]
       public string ContainerNumber
       {
           get;
           set;
       }
       [DataMemberAttribute]
       public int CreatorId
       {
           get;
           set;
       }
       [DataMemberAttribute]
       public string CreatorName
       {
           get;
           set;
       }
       [DataMemberAttribute]
       public DateTime? CreateTime
       {
           get;
           set;
       }
       [DataMemberAttribute]
       public int? ModifierId
       {
           get;
           set;
       }
       [DataMemberAttribute]
       public string ModifierName
       {
           get;
           set;
       }
       [DataMemberAttribute]
       public DateTime? ModifyTime
       {
           get;
           set;
       }
       [DataMemberAttribute]
       public DateTime? BoxUpFinishTime
       {
           get;
           set;
       }
       [DataMemberAttribute]
       public string Remark
       {
           get;
           set;
       }
       [DataMemberAttribute]
       public string BranchName
       {
           get;
           set;
       }
       [DataMemberAttribute]
       public string OrderTypeName
       {
           get;
           set;
       }
       /**
   * 入库仓库名称
   **/
       [DataMemberAttribute]
       public string InWarehouseName {
           get;
           set;
       }
    }
}
