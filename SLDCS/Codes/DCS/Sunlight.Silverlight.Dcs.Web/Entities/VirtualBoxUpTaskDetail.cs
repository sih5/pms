﻿using System.Data.Objects.DataClasses;
using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web
{
    [DataContract(IsReference = true)]
    public class VirtualBoxUpTaskDetail : EntityObject
    {

        [Key]
        [DataMemberAttribute]
        public int Id
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int BoxUpTaskId
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string BoxUpTaskCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int PartsOutboundPlanId
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartsOutboundPlanCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int PickingTaskId
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PickingTaskCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SourceCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int SparePartId
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SparePartCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SparePartName
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SihCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int PlanQty
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? BoxUpQty
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? NowBoxUpQuantity
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string MeasureUnit
        {
            get;
            set;
        }

       [DataMemberAttribute]
        public int? PickingTaskDetailId
        {
            get;
            set;
        } 
        [DataMemberAttribute]
       public string ContainerNumber
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string OriginalRequirementBillCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string WarehouseAreaCode
        {
            get;
            set;
        }
        /// <summary>
        /// 重量
        /// </summary>
        [DataMemberAttribute]
        public double? Weight
        {
            get;
            set;
        }
        /// <summary>
        /// 体积
        /// </summary>
        [DataMemberAttribute]
        public double? Volume
        {
            get;
            set;
        }
         [DataMemberAttribute]
        public int? TopLevelWarehouseAreaId {
            get;
            set;
        }
         [DataMemberAttribute]
         public string TopLevelWarehouseAreaCode {
             get;
             set;
         }
    }
}
