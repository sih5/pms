﻿using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class VirtualCompanySalescenterStock : EntityObject {
        /// <summary>
        /// 企业Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int CompanyId {
            get;
            set;
        }

        /// <summary>
        /// 企业类型
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int CompanyType {
            get;
            set;
        }

        /// <summary>
        /// 企业编号
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public string CompanyCode {
            get;
            set;
        }

        /// <summary>
        /// 企业名称
        /// </summary>
        [DataMemberAttribute]
        public string CompanyName {
            get;
            set;
        }

        /// <summary>
        /// 配件库存Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int PartsStockId {
            get;
            set;
        }

        /// <summary>
        ///  销售价Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int PartsSalesPriceId {
            get;
            set;
        }





        /// <summary>
        /// 营销分公司Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int BranchId {
            get;
            set;
        }

        /// <summary>
        /// 营销分公司编号
        /// </summary>
        [DataMemberAttribute]
        public string BranchCode {
            get;
            set;
        }

        /// <summary>
        /// 营销分公司名称
        /// </summary>
        [DataMemberAttribute]
        public string BranchName {
            get;
            set;
        }

        /// <summary>
        /// 配件Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int SparePartsId {
            get;
            set;
        }

        /// <summary>
        /// 配件编号
        /// </summary>
        [DataMemberAttribute]
        public string SparePartsCode {
            get;
            set;
        }

        /// <summary>
        /// 配件名称
        /// </summary>
        [DataMemberAttribute]
        public string SparePartsName {
            get;
            set;
        }

        /// <summary>
        /// 实际数量
        /// </summary>
        [DataMemberAttribute]
        public int FactAmount {
            get;
            set;
        }

        /// <summary>
        /// 可用数量
        /// </summary>
        [DataMemberAttribute]
        public int UsableAmount {
            get;
            set;
        }

        /// <summary>
        /// 省
        /// </summary>
        [DataMemberAttribute]
        public string ProvinceName {
            get;
            set;
        }

        /// <summary>
        /// 市
        /// </summary>
        [DataMemberAttribute]
        public string CityName {
            get;
            set;
        }

        /// <summary>
        /// 县
        /// </summary>
        [DataMemberAttribute]
        public string CountyName {
            get;
            set;
        }

        /// <summary>
        /// 配件销售价
        /// </summary>
        [DataMemberAttribute]
        public decimal PartsSalesPrice {
            get;
            set;
        }

        /// <summary>
        /// 配件销售类型Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int PartsSalesCategoryId {
            get;
            set;
        }

        /// <summary>
        /// 配件销售类型名称
        /// </summary>
        [DataMemberAttribute]
        public string PartsSalesCategoryName {
            get;
            set;
        }
    }
}
