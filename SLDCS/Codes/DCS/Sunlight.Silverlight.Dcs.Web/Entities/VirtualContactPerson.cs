﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web.Entities
{
    [DataContract(IsReference = true)]
    public class VirtualContactPerson : EntityObject 
    {
        [Key]
        [DataMemberAttribute]
        public int Id
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ContactPerson
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ContactPhone
        {
            get;
            set;
        }
    }
}
