﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 虚拟信用申请单
    /// </summary>
    [DataContract(IsReference = true)]
    public class VirtualCredenceApplication : EntityObject {
        /// <summary>
        /// 信用申请单Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }
        /// <summary>
        /// 营销分公司Id
        /// </summary>
        
        [DataMemberAttribute]
        public int BranchId {
            get;
            set;
        }
        /// <summary>
        /// 销售企业Id
        /// </summary>
        [DataMemberAttribute]
        public int SalesCompanyId {
            get;
            set;
        }
        /// <summary>
        /// 担保企业Id
        /// </summary>
  
        [DataMemberAttribute]
        public int GuarantorCompanyId {
            get;
            set;
        }
        /// <summary>
        /// 状态
        /// </summary>
       
        [DataMemberAttribute]
        public int Status {
            get;
            set;
        }
        /// <summary>
        /// 使用条件
        /// </summary>
        
        [DataMemberAttribute]
        public int ApplyCondition {
            get;
            set;
        }
        /// <summary>
        /// 营销分公司编号
        /// </summary>
        [DataMemberAttribute]
        public string BranchName {
            get;
            set;
        }
        /// <summary>
        /// 信用额度
        /// </summary>
        [DataMemberAttribute]
        public decimal CredenceLimit {
            get;
            set;
        }
        /// <summary>
        /// 账户组名称
        /// </summary>
        [DataMemberAttribute]
        public string AccountGroupName {
            get;
            set;
        }
        /// <summary>
        /// 源单据编号
        /// </summary>
        [DataMemberAttribute]
        public string SourceCode {
            get;
            set;
        }
        /// <summary>
        /// 客户名称
        /// </summary>
        [DataMemberAttribute]
        public string CompanyName {
            get;
            set;
        }
        /// <summary>
        /// 担保企业名称
        /// </summary>
        [DataMemberAttribute]
        public string GuarantorCompanyName {
            get;
            set;
        }
        /// <summary>
        /// 客户编号
        /// </summary>
        [DataMemberAttribute]
        public string CompanyCode {
            get;
            set;
        }
        /// <summary>
        /// 已使用金额(H)
        /// </summary>
        [DataMemberAttribute]
        public decimal ConsumedAmount {
            get;
            set;
        }
        /// <summary>
        /// 账户组Id
        /// </summary>
        [DataMemberAttribute]
        public int? AccountGroupId {
            get;
            set;
        }
       
        [DataMemberAttribute]
        public int? CustomerCompanyId {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? ApproverId {
            get;
            set;
        }
        /// <summary>
        /// 源单据类型
        /// </summary>
        [DataMemberAttribute]
        public int? SourceType {
            get;
            set;
        }
        /// <summary>
        /// 业务编码
        /// </summary>
        [DataMemberAttribute]
        public string BusinessCode {
            get;
            set;
        }
        /// <summary>
        ///  创建人
        /// </summary>
        [DataMemberAttribute]
        [Display(ResourceType = typeof(EntityStrings), Name = "_Common_CreatorName")]
        public string CreatorName {
            get;
            set;
        }
        /// <summary>
        ///  创建时间
        /// </summary>
        [DataMemberAttribute]
        [Display(ResourceType = typeof(EntityStrings), Name = "_Common_CreateTime")]
        public DateTime? CreateTime {
            get;
            set;
        }
        /// <summary>
        ///  修改人
        /// </summary>
        [DataMemberAttribute]
        [Display(ResourceType = typeof(EntityStrings), Name = "_Common_ModifierName")]
        public string ModifierName {
            get;
            set;
        }
        /// <summary>
        ///  修改时间
        /// </summary>
        [DataMemberAttribute]
        [Display(ResourceType = typeof(EntityStrings), Name = "_Common_ModifyTime")]
        public DateTime? ModifyTime {
            get;
            set;
        }
        /// <summary>
        ///  审核时间
        /// </summary>
        [DataMemberAttribute]
        public DateTime? ApproveTime {
            get;
            set;
        }
        /// <summary>
        ///  审核人
        /// </summary>
        [DataMemberAttribute]
        public string ApproverName {
            get;
            set;
        }
        /// <summary>
        ///  生效时间
        /// </summary>
        [DataMemberAttribute]
        public DateTime? ValidationDate {
            get;
            set;
        }
        /// <summary>
        ///  失效时间
        /// </summary>
        [DataMemberAttribute]
        public DateTime? ExpireDate {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? CreditType {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? CompanyType {
            get;
            set;
        }

        /// <summary>
        /// 是否上传附件
        /// </summary>
        [DataMemberAttribute]
        public bool? IsUplodFile {
            get;
            set;
        }      
         [DataMemberAttribute]
        public string InitialApproverName {
            get;
            set;
        }
         [DataMemberAttribute]
         public DateTime? InitialApproveTime {
             get;
             set;
         }
         [DataMemberAttribute]
         public string CheckerName {
             get;
             set;
         }
         [DataMemberAttribute]
         public DateTime? CheckTime {
             get;
             set;
         }
         [DataMemberAttribute]
         public string UpperCheckerName {
             get;
             set;
         }
         [DataMemberAttribute]
         public DateTime? UpperCheckTime {
             get;
             set;
         }
         [DataMemberAttribute]
         public string UpperApproverName {
             get;
             set;
         }
         [DataMemberAttribute]
         public DateTime? UpperApproveTime {
             get;
             set;
         }
         //SIH授信（服务商）
         [DataMemberAttribute]
         public decimal? SIHCredit {
             get;
             set;
         }
         //正常授信 有效
         [DataMemberAttribute]
         public decimal? ZcLimit {
             get;
             set;
         }
         //临时授信 有效
         [DataMemberAttribute]
         public decimal? LsLimit {
             get;
             set;
         }
         //合计 有效
         [DataMemberAttribute]
         public decimal? HjLimit {
             get;
             set;
         }
         //本次变更 正常
         [DataMemberAttribute]
         public decimal? ZsNow {
             get;
             set;
         }
         //本次变更 临时
         [DataMemberAttribute]
         public decimal? LsNow {
             get;
             set;
         }
         //本次变更
         [DataMemberAttribute]
         public decimal? HjNow {
             get;
             set;
         }
         //变更后正常
         [DataMemberAttribute]
         public decimal? DifZc {
             get;
             set;
         }
         //变更后临时
         [DataMemberAttribute]
         public decimal? DifLs {
             get;
             set;
         }
         //变更后合集
         [DataMemberAttribute]
         public decimal? HjDif {
             get;
             set;
         }
    }
}
