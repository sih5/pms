﻿using System;
using System.Data.Objects.DataClasses;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web
{
    [DataContract(IsReference = true)]
    public class VirtualCsbomParts : EntityObject
    {
        [Key]
        [DataMemberAttribute]
        public int Id
        {
            get;
            set;
        }

        [DataMemberAttribute]
        public string Code
        {
            get;
            set;
        }
       
        [DataMemberAttribute]
        public string Name
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ReferenceCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SPMReferenceCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string EnglishName
        {
            get;
            set;
        }
        
        [DataMemberAttribute]
        public string SPMName
        {
            get;
            set;
        }

        [DataMemberAttribute]
        public string SPMEnglishName
        {
            get;
            set;
        }

        [DataMemberAttribute]
        public DateTime? CreateTime
        {
            get;
            set;
        }

        [DataMemberAttribute]
        public string IsUpdate
        {
            get;
            set;
        }
    }
}
