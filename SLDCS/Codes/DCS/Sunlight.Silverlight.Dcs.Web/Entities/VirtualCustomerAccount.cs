﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web.Entities {
    /// <summary>
    /// 虚拟客户账户
    /// </summary>
    [DataContract(IsReference = true)]
    public class VirtualCustomerAccount : EntityObject {
        /// <summary>
        /// 客户账户Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }

        /// <summary>
        /// 账户组Id
        /// </summary>
        [DataMemberAttribute]
        public int AccountGroupId {
            get;
            set;
        }
        /// <summary>
        /// 账户组编号
        /// </summary>
        [DataMemberAttribute]
        public string AccountGroupCode {
            get;
            set;
        }
        /// <summary>
        /// 账户组名称
        /// </summary>
        [DataMemberAttribute]
        public string AccountGroupName {
            get;
            set;
        }
        /// <summary>
        /// 客户企业Id
        /// </summary>
        [DataMemberAttribute]
        public int CustomerCompanyId {
            get;
            set;
        }
        /// <summary>
        /// 返利台账(R)
        /// </summary>
        [DataMemberAttribute]
        public decimal RebateAccount {
            get;
            set;
        }
        /// <summary>
        /// 未审核订单金额(H)
        /// </summary>
        [DataMemberAttribute]
        public decimal UnApproveAmount {
            get;
            set;
        }
        /// <summary>
        /// 账户金额(A)
        /// </summary>
        [DataMemberAttribute]
        public decimal AccountBalance {
            get;
            set;
        }

        /// <summary>
        /// 余额为零
        /// </summary>
        [DataMemberAttribute]
        public bool? IsZeroForAccountBalance {
            get;
            set;
        }
        /// <summary>
        /// 发出商品金额(S)
        /// </summary>
        [DataMemberAttribute]
        public decimal ShippedProductValue {
            get;
            set;
        }
        /// <summary>
        /// 审批代发金额(P)
        /// </summary>
        [DataMemberAttribute]
        public decimal PendingAmount {
            get;
            set;
        }
        /// <summary>
        /// 客户信用总额(C)
        /// </summary>
        [DataMemberAttribute]
        public decimal CustomerCredenceAmount {
            get;
            set;
        }
        /// <summary>
        /// 临时信用总额(D)
        /// </summary>
        [DataMemberAttribute]
        public decimal? TempCreditTotalFee {
            get;
            set;
        }
        /// <summary>
        /// SIH授信（服务商）(B)
        /// </summary>
        [DataMemberAttribute]
        public decimal? SIHCredit {
            get;
            set;
        }
        /// <summary>
        /// 可用金额=A+C+B+D+R-S-P-H
        /// </summary>
        [DataMemberAttribute]
        public decimal UsableBalance {
            get;
            set;
        }
        /// <summary>
        /// 账户余额=账户金额-发出商品金额
        /// </summary>
        [DataMemberAttribute]
        public decimal Balance {
            get;
            set;
        }
        /// <summary>
        /// 锁定金额=发出商品金额+审批代发金额
        /// </summary>
        [DataMemberAttribute]
        public decimal LockBalance {
            get;
            set;
        }
        /// <summary>
        /// 客户企业编号
        /// </summary>
        [DataMemberAttribute]
        public string CompanyCode {
            get;
            set;
        }
        /// <summary>
        /// 客户企业名称
        /// </summary>
        [DataMemberAttribute]
        public string CompanyName {
            get;
            set;
        }
        /// <summary>
        /// 业务编码
        /// </summary>
        [DataMemberAttribute]
        public string BusinessCode {
            get;
            set;
        }
        /// <summary>
        /// 客户企业类型
        /// </summary>
        [DataMemberAttribute]
        public int CompanyType {
            get;
            set;
        }
        /// <summary>
        ///  状态
        /// </summary>
        [DataMemberAttribute]
        [Display(ResourceType = typeof(EntityStrings), Name = "_Common_Status")]
        public int Status {
            get;
            set;
        }
        /// <summary>
        ///  创建人
        /// </summary>
        [DataMemberAttribute]
        [Display(ResourceType = typeof(EntityStrings), Name = "_Common_CreatorName")]
        public string CreatorName {
            get;
            set;
        }
        /// <summary>
        ///  创建时间
        /// </summary>
        [DataMemberAttribute]
        [Display(ResourceType = typeof(EntityStrings), Name = "_Common_CreateTime")]
        public DateTime? CreateTime {
            get;
            set;
        }
        /// <summary>
        ///  修改人
        /// </summary>
        [DataMemberAttribute]
        [Display(ResourceType = typeof(EntityStrings), Name = "_Common_ModifierName")]
        public string ModifierName {
            get;
            set;
        }
        /// <summary>
        ///  修改时间
        /// </summary>
        [DataMemberAttribute]
        [Display(ResourceType = typeof(EntityStrings), Name = "_Common_ModifyTime")]
        public DateTime? ModifyTime {
            get;
            set;
        }
    }
}
