﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class VirtualCustomerAccountHisDetails : EntityObject {
        [Key]
        [DataMemberAttribute]
        public int ID {
            get;
            set;
        }

        [DataMemberAttribute]
        public string CompanyCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string BusinessCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CompanyName {
            get;
            set;
        }

        [Key]
        [DataMemberAttribute]
        public int AccountGroupId {
            get;
            set;
        }

        [DataMemberAttribute]
        public string AccountGroupName {
            get;
            set;
        }

        [DataMemberAttribute]
        public decimal? ChangeAmount {
            get;
            set;
        }

        [DataMemberAttribute]
        public DateTime? ProcessDate {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? BusinessType {
            get;
            set;
        }

        [DataMemberAttribute]
        public string SourceCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string Summary {
            get;
            set;
        }

        [DataMemberAttribute]
        public decimal? BeforeChangeAmount {
            get;
            set;
        }

        [DataMemberAttribute]
        public decimal? AfterChangeAmount {
            get;
            set;
        }

        [DataMemberAttribute]
        public decimal? Debit {
            get;
            set;
        }

        [DataMemberAttribute]
        public decimal? Credit {
            get;
            set;
        }

        [DataMemberAttribute]
        public string CreatorName {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? InvoiceDate
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? SerialType
        {
            get;
            set;
        }
    }
}
