﻿
using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {

    [DataContract(IsReference = true)]
    public class VirtualCustomerAccountHistory : EntityObject {
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }

        [DataMemberAttribute]
        public string Code {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Name {
            get;
            set;
        }
        [DataMemberAttribute]
        public int Type {
            get;
            set;
        }
        [DataMemberAttribute]
        public string AccountGroupCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string AccountGroupName {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal AccountBalance {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal ShippedProductValue {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal PendingAmount {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal CustomerCredenceAmount {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal? FlBalance {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal? WshBalance {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal? SdBalance {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal? YeBalance {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal? KyBalance {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? TheDate {
            get;
            set;
        }
        [DataMemberAttribute]
        public int AccountGroupId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string AgencyCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string AgencyName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int SalesCompanyId {
            get;
            set;
        }

        /// <summary>
        /// 临时信用总额(D)
        /// </summary>
        [DataMemberAttribute]
        public decimal? TempCreditTotalFee {
            get;
            set;
        }
        /// <summary>
        /// SIH授信（服务商）(B)
        /// </summary>
        [DataMemberAttribute]
        public decimal? SIHCredit {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ChannelCapabilityName {
            get;
            set;
        }
    }
}
