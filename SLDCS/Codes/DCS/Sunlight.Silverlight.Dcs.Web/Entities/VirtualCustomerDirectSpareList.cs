﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web
{
    [DataContract(IsReference = true)]
    public class VirtualCustomerDirectSpareList : EntityObject
    {
        [Key]
        [DataMemberAttribute]
        public int Id
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int PartsSalesCategoryId
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartsSalesCategoryName
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int CustomerId
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CustomerCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CustomerName
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int SparePartId
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SparePartCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SparePartName
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string MeasureUnit
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? IfDirectProvision
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? Status
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? CreatorId
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CreatorName
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? CreateTime
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? ModifierId
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ModifierName
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? ModifyTime
        {
            get;
            set;
        }
        [Key]
        [DataMemberAttribute]
        public string PartsSupplierCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartsSupplierName
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public bool? IsPrimary
        {
            get;
            set;
        }
        //首选供应商
        [DataMemberAttribute]
        public string PrimaryPartsSupplierName
        {
            get;
            set;
        }
    }
}
