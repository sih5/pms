﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class VirtualCustomerInformation {
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }

        [DataMemberAttribute]
        public string Code {
            get;
            set;
        }

        [DataMemberAttribute]
        public string Name {
            get;
            set;
        }

        [DataMemberAttribute]
        public int CustomerId {
            get;
            set;
        }

        [DataMemberAttribute]
        public int Customeraccountid {
            get;
            set;
        }

        [DataMember]
        public int CustomerCompanyId {
            get;
            set;
        }

        [DataMember]
        public string CustomerCompanyName {
            get;
            set;
        }

        [DataMember]
        public string CustomerCompanyCode {
            get;
            set;
        }
    }
}