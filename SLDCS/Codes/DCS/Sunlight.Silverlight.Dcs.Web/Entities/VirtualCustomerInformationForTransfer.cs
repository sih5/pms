﻿using System;
using System.Data.Objects.DataClasses;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class VirtualCustomerInformationForTransfer: EntityObject {
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? SalesCompanyId {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? CustomerCompanyId {
            get;
            set;
        }
       [DataMemberAttribute]
        public string ContactPerson {
            get;
            set;
        }

        [DataMemberAttribute]
        public string ContactPhone {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? Status {
            get;
            set;
        }

        [DataMemberAttribute]
        public string CreatorName {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? CreateTime {
            get;
            set;
        }
       [DataMemberAttribute]
       public string ModifierName
       {
           get;
           set;
       }
       [DataMemberAttribute]
       public DateTime? ModifyTime
       {
           get;
           set;
       }

        [DataMemberAttribute]
        public string CompanyCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string CompanyName {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? CompanyType {
            get;
            set;
        }
        
    }
}