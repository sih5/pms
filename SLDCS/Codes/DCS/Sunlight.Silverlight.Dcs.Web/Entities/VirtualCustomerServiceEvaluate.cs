﻿
using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class VirtualCustomerServiceEvaluate : EntityObject {
        [Key]
        [DataMember]
        public int Id {
            get;
            set;
        }

        [Key]
        [DataMember]
        public int DealerId {
            get;
            set;
        }
        [DataMember]
        public DateTime? RepairClaimBillCreateTime {
            get;
            set;
        }

        [DataMember]
        public int RepairClaimBillId {
            get;
            set;
        }

        [DataMember]
        public string RepairClaimBillCode {
            get;
            set;
        }

        [DataMember]
        public string EvaluateSource {
            get;
            set;
        }

        [DataMember]
        public int EvaluateGrade {
            get;
            set;
        }

        [DataMember]
        public string isContentFactor {
            get;
            set;
        }

        [DataMember]
        public DateTime? EvaluateTime {
            get;
            set;
        }

        [DataMember]
        public int? CreatorId {
            get;
            set;
        }

        [DataMember]
        public string CreatorName {
            get;
            set;
        }

        [DataMember]
        public DateTime? CreateTime {
            get;
            set;
        }

        [DataMember]
        public string VIN {
            get;
            set;
        }
        [Key]
        [DataMember]
        public int PartsSalesCategoryId {
            get;
            set;
        }

        [DataMember]
        public string PartsSalesCategoryName {
            get;
            set;
        }
        [Key]
        [DataMember]
        public int MarketingDepartmentId {
            get;
            set;
        }

        [DataMember]
        public string MarketingDepartmentName {
            get;
            set;
        }

        [DataMember]
        public string DealerName {
            get;
            set;
        }

        [DataMember]
        public string DealerCode {
            get;
            set;
        }
        [DataMember]
        public int? RepairType {
            get;
            set;
        }
        [DataMember]
        public int? DisContentFactor {
            get;
            set;
        }

        [DataMember]
        public string VehicleContactPerson {
            get;
            set;
        }

        [DataMember]
        public string ContactPhone {
            get;
            set;
        }
        [DataMember]
        public string VehicleLicensePlate {
            get;
            set;
        }
        [DataMember]
        public DateTime? RepairRequestTime {
            get;
            set;
        }
        [DataMember]
        public int EvaluationResults {
            get;
            set;
        }
        [DataMember]
        public string CustomOpinion {
            get;
            set;
        }
        /// <summary>
        /// 评价数量
        /// </summary>
        [DataMember]
        public double EvaluationQuantity {
            get;
            set;
        }
        /// <summary>
        /// 好评比率
        /// </summary>
        [DataMember]
        public double Praise {
            get;
            set;
        }
        /// <summary>
        /// 中评比率
        /// </summary>
        [DataMember]
        public double InTheComments {
            get;
            set;
        }
        /// <summary>
        /// 中评比率
        /// </summary>
        [DataMember]
        public double Bad {
            get;
            set;
        }
        /// <summary>
        /// 维修技术水平比率
        /// </summary>
        [DataMember]
        public double RepairTechnology {
            get;
            set;
        }
        /// <summary>
        /// 服务态度
        /// </summary>
        [DataMember]
        public double ServiceAttitude {
            get;
            set;
        }
        /// <summary>
        /// 服务及时性
        /// </summary>
        [DataMember]
        public double ServiceTime {
            get;
            set;
        }
        /// <summary>
        /// 服务环境
        /// </summary>
        [DataMember]
        public double ServiceHygiene {
            get;
            set;
        }
    }
}
