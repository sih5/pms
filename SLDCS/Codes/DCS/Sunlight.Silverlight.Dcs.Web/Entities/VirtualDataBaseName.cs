﻿using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class VirtualDataBaseName : EntityObject {
        [DataMemberAttribute]
        [Key]
        public int Id {
            get;
            set;
        }

        [DataMemberAttribute]
        public string Name {
            get;
            set;
        }
    }
}
