﻿using System.Data.Objects.DataClasses;
using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web
{
    [DataContract(IsReference = true)]
    public class VirtualDealerPartsSalesPrice : EntityObject
    {

        [Key]
        [DataMemberAttribute]
        public int Id
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartName
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal? SalesPrice
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal? RetailGuidePrice
        {
            get;
            set;
        }
       [DataMemberAttribute]
        public int Status
        {
            get;
            set;
        }
       [DataMemberAttribute]
       public string CreatorName
       {
           get;
           set;
       }
       [DataMemberAttribute]
       public DateTime? CreateTime
       {
           get;
           set;
       }
       [DataMemberAttribute]
       public string ModifierName
       {
           get;
           set;
       }
       [DataMemberAttribute]
       public DateTime? ModifyTime
       {
           get;
           set;
       }

    }
}
