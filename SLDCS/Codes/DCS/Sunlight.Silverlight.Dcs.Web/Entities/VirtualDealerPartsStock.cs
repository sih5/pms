﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 虚拟经销商配件库存
    /// </summary>
    [DataContract(IsReference = true)]
    public class VirtualDealerPartsStock : EntityObject {

        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }

        [DataMemberAttribute]
        public int DealerId {
            get;
            set;
        }

        [DataMemberAttribute]
        public string DealerCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string DealerName {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? SubDealerId {
            get;
            set;
        }

        [DataMemberAttribute]
        public int BranchId {
            get;
            set;
        }

        [DataMemberAttribute]
        public int SalesCategoryId {
            get;
            set;
        }

        [DataMemberAttribute]
        public string SalesCategoryName {
            get;
            set;
        }

        [DataMemberAttribute]
        public int SparePartId {
            get;
            set;
        }

        [DataMemberAttribute]
        public string SparePartCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string CreatorName {
            get;
            set;
        }

        [DataMemberAttribute]
        public DateTime? CreateTime {
            get;
            set;
        }

        [DataMemberAttribute]
        public string ModifierName {
            get;
            set;
        }

        [DataMemberAttribute]
        public DateTime? ModifyTime {
            get;
            set;
        }

        [DataMemberAttribute]
        public int Quantity {
            get;
            set;
        }

        /// <summary>
        /// 零售指导价 
        /// </summary>
        [DataMemberAttribute]
        public decimal RetailGuidePrice {
            get;
            set;
        }

        /// <summary>
        /// 价格类型
        /// </summary>
        [DataMemberAttribute]
        public int PriceType {
            get;
            set;
        }

        /// <summary>
        /// 基准销售价或全国统一价
        /// </summary>
        [DataMemberAttribute]
        public decimal BasicSalePrice {
            get;
            set;
        }

        /// <summary>
        /// 经销总价
        /// </summary>
        [DataMemberAttribute]
        public decimal BasicSalePriceAll {
            get;
            set;
        }

        /// <summary>
        /// 二级站编号
        /// </summary>
        [DataMemberAttribute]
        public string SubDealerCode {
            get;
            set;
        }

        /// <summary>
        /// 二级站名称
        /// </summary>
        [DataMemberAttribute]
        public string SubDealerName {
            get;
            set;
        }

        /// <summary>
        /// 配件名称
        /// </summary>
        [DataMemberAttribute]
        public string SparePartName {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? CompanyType {
            get;
            set;
        }

        [DataMemberAttribute]
        public string CompanyTypeName {
            get;
            set;
        }
        [DataMemberAttribute]
        public string DealerType {
            get;
            set;
        }

        [DataMemberAttribute]
        public string MarketingDepartmentName {
            get;
            set;
        }
        [DataMemberAttribute]
        public bool IsZero {
            get;
            set;
        }
    }
}
