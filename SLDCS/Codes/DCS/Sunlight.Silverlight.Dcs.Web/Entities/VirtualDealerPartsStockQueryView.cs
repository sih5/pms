﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class VirtualDealerPartsStockQueryView : EntityObject {

        [DataMemberAttribute]
        [Key]
        public int SparePartId {
            get;
            set;
        }

        [DataMember]
        public string SparePartCode {
            get;
            set;
        }

        [DataMember]
        public string SparePartName {
            get;
            set;
        }
        /// <summary>
        /// 保内销售价格
        /// </summary>
        [DataMember]
        public decimal? WarrantyPrice {
            get;
            set;
        }
        /// <summary>
        /// 保外销售价格
        /// </summary>
        [DataMember]
        public decimal? NoWarrantyPrice {
            get;
            set;
        }
        /// <summary>
        /// 可用库存
        /// </summary>
        [DataMember]
        public decimal? ValidStock {
            get;
            set;
        }
        /// <summary>
        /// 保外品牌名称
        /// </summary>
        [DataMember]
        public string NoWarrantyBrandName {
            get;
            set;
        }
        /// <summary>
        /// 保内品牌名称
        /// </summary>
        [DataMember]
        public string WarrantyBrandName {
            get;
            set;
        }
    }
}