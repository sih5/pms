﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class VirtualDealerPartsTransOrderDetail {
        [Key]
        [DataMemberAttribute]
        public int PartsId {
            get;
            set;
        }

        [DataMemberAttribute]
        public string PartsName {
            get;
            set;
        }

        [DataMemberAttribute]
        public string PartsCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public decimal? WarrantyPrice {
            get;
            set;
        }

        [DataMemberAttribute]
        public decimal? NoWarrantyPrice {
            get;
            set;
        }
        [DataMemberAttribute]
        public int Quantity {
            get;
            set;
        }

        [DataMemberAttribute]
        public int Stock {
            get;
            set;
        }

    }
}
