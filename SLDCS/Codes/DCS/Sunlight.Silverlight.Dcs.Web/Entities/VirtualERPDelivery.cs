﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class VirtualERPDelivery : EntityObject {
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ShopName {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PlatForm_Code {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Code {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? SyncType {
            get;
            set;
        }

        [DataMemberAttribute]
        public string Warehouse_Name {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? SyncTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Shop_Code {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? SyncStatus {
            get;
            set;
        }
    }
}
