﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web{
    /// <summary>
    /// 虚拟ERP盘点单
    /// </summary>
    [DataContract(IsReference = true)]
    public class VirtualERPStock :EntityObject{

        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }

        [DataMemberAttribute]
        public string WAREHOUSECODE {
            get;
            set;
        }

        [DataMemberAttribute]
        public string WarehouseName {
            get;
            set;
        }

        [DataMemberAttribute]
        public string SPAREPARTCODE {
            get;
            set;
        }

        [DataMemberAttribute]
        public string SparepartName {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? QUANTITY {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? SYNCSTATUS {
            get;
            set;
        }

        [DataMemberAttribute]
        public DateTime? SYNCTIME {
            get;
            set;
        }

        [DataMemberAttribute]
        public string ERRORMESSAGE {
            get;
            set;
        }
    }
}
