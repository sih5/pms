﻿using System;
using System.Data.Objects.DataClasses;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web
{
    [DataContract(IsReference = true)]
    public class VirtualEnterprisePartsCost : EntityObject
    {
        [Key]
        [DataMemberAttribute]
        public int Id
        {
            get;
            set;
        }

        [DataMemberAttribute]
        public int OwnerCompanyId
        {
            get;
            set;
        }
       
        [DataMemberAttribute]
        public string OwnerCompanyIdName
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int PartsSalesCategoryId
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartsSalesCategoryName
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int SparePartId

        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SparePartCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SparePartName
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ReferenceCode
        {
            get;
            set;
        }
        
        [DataMemberAttribute]
        public string Quantity
        {
            get;
            set;
        }

        [DataMemberAttribute]
        public string CostAmount
        {
            get;
            set;
        }

        [DataMemberAttribute]
        public string CostPrice
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int CreatorId
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CreatorName
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? CreateTime
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int ModifierId
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ModifierName
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? ModifyTime
        {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? PartABC
        {
            get;
            set;
        }
    }
}
