﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class VirtualExpenseAdjustmentBill {
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Code {
            get;
            set;
        }
        [DataMemberAttribute]
        public int Status {
            get;
            set;
        }
        [DataMemberAttribute]
        public int SettlementStatus {
            get;
            set;
        }
        [DataMemberAttribute]
        public int TransactionCategory {
            get;
            set;
        }
        [DataMemberAttribute]
        public string BranchName {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartsSalesCategoryName {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ServiceProductLineName {
            get;
            set;
        }
        [DataMemberAttribute]
        public string DealerCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CompanyCustomerCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string DealerName {
            get;
            set;
        }
        [DataMemberAttribute]
        public string MarketingDepartmentName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? MarketingDepartmentId {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? SourceType {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SourceCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public int DebitOrReplenish {
            get;
            set;
        }
        [DataMemberAttribute]
        public Decimal? TransactionAmount {
            get;
            set;
        }
        [DataMemberAttribute]
        public string TransactionReason {
            get;
            set;
        }
        [DataMemberAttribute]
        public string DealerContactPerson {
            get;
            set;
        }
        [DataMemberAttribute]
        public string DealerPhoneNumber {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CreatorName {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? CreateTime {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? ResponsibleUnitId {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? DealerId {
            get;
            set;
        }

        [DataMemberAttribute]
        public int BranchId {
            get;
            set;
        }
        [DataMemberAttribute]
        public int PartsSalesCategoryId {
            get;
            set;
        }

    }
}
