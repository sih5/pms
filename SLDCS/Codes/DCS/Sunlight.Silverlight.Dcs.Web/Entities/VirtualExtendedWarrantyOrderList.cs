﻿using System;
using System.Collections.Generic;
using System.Data.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sunlight.Silverlight.Dcs.Web {
    public class VirtualExtendedWarrantyOrderList : EntityObject {
        public int Id {
            get;
            set;
        }
        public string ExtendedWarrantyOrderCode {
            get;
            set;
        }

        public DateTime SalesDate {
            get;
            set;
        }

        public string ExtendedWarrantyProductName {
            get;
            set;
        }

        public double ?ExtendedWarrantyTerm {
            get;
            set;
        }

        public int Mileage {
            get;
            set;
        }

        public DateTime? EndDate {
            get;
            set;
        }
    }
}
