﻿using System.Data.Objects.DataClasses;
using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web
{
    [DataContract(IsReference = true)]
    public class VirtualFactoryPurchacePrice : EntityObject
    {

        [Key]
        [DataMemberAttribute]
        public int Id
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int SupplierId
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SupplierCode
        {
            get;
            set;
        }
         [DataMemberAttribute]
        public string SupplierName
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int SparePartId
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SparePartCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SparePartName
        {
            get;
            set;
        }

        [DataMemberAttribute]
        public decimal? ContractPrice
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? ValidFrom
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? ValidTo
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string UserCode
        {
            get;
            set;
        }
         [DataMemberAttribute]
        public DateTime? CreateTime
        {
            get;
            set;
        }
        //是否已生成
         [DataMemberAttribute]
        public bool? IsGenerate
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string HYCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? IsTemp
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal? Rate
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public bool? IsOrderable
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SupplierPartCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? LimitQty {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Source {
            get;
            set;
        }
    }
}
