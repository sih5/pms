﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web
{
      [DataContract(IsReference = true)]
    public class VirtualForPurchaseReturn : EntityObject
    {
           [Key]
           [DataMemberAttribute]
           public int Id {
                get;
                set;
            }
          [DataMemberAttribute]
          public int SparePartId
          {
              get;
              set;
          }
          //新增采购退货单时，查询入库检验单时用到
          [DataMemberAttribute]
          public int ReturnedQuantity
          {
              get;
              set;
          }
          [DataMemberAttribute]
          public string SupplierPartCode
          {
              get;
              set;
          }
    }
}
