﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 权益清单
    /// </summary>
    [DataContractAttribute(IsReference = true)]
    public class VirtualFreeRescue : EntityObject {
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }
        [DataMemberAttribute]
        public string BenifitNum {
            get;
            set;
        }
        [DataMemberAttribute]
        public string MemberNumber {
            get;
            set;
        }
        [DataMemberAttribute]
        public string FreeRescueName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? FreeRescueTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? FreeRescueDate {
            get;
            set;
        }

        [DataMemberAttribute]
        public string BenefiType {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? BenefiUom {
            get;
            set;
        }

        [DataMemberAttribute]
        public decimal? DiscountNum {
            get;
            set;
        }
    }
}
