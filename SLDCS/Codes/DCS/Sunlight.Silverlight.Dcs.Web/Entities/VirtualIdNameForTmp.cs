﻿using System.Data.Objects.DataClasses;
using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web
{
    [DataContract(IsReference = true)]
    public class VirtualIdNameForTmp : EntityObject
    {

        [Key]
        [DataMemberAttribute]
        public int Id
        {
            get;
            set;
        }
       
        [DataMemberAttribute]
        public string Code {
            get;
            set;
        }

        [DataMemberAttribute]
        public string Name {
            get;
            set;
        }

        [DataMemberAttribute]
        public DateTime? CreateTime {
            get;
            set;
        }
    }
}
