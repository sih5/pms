﻿using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 虚拟出入库单清单--用于打印配件采购结算明细
    /// </summary>
    [DataContract(IsReference = true)]
    public class VirtualInAndOutBoundDetail : EntityObject {
        /// <summary>
        /// 出入库单编号
        /// </summary>
        [DataMember]
        public string Code {
            get;
            set;
        }
        /// <summary>
        /// 图号
        /// </summary>
        [DataMember]
        public string PartCode {
            get;
            set;
        }
        /// <summary>
        /// 供应商图号
        /// </summary>
        [DataMember]
        public string SupplierPartCode {
            get;
            set;
        }
        /// <summary>
        /// 配件名称
        /// </summary>
        [DataMember]
        public string PartName {
            get;
            set;
        }
        /// <summary>
        /// 数量
        /// </summary>
        [DataMember]
        public int Quantity {
            get;
            set;
        }
        /// <summary>
        /// 计划价
        /// </summary>
        [DataMember]
        public decimal CostPrice {
            get;
            set;
        }
        /// <summary>
        /// 合同价格
        /// </summary>
        [DataMember]
        public decimal SettlementPrice {
            get;
            set;
        }
    }
}

