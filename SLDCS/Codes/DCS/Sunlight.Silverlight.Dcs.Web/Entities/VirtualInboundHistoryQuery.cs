﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 虚拟历史入库记录查询
    /// </summary>
    [DataContract(IsReference = true)]
    public class VirtualInboundHistoryQuery : EntityObject {


        /// <summary>
        ///分公司
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int BranchId {
            get;
            set;
        }
        /// <summary>
        /// 分公司编号
        /// </summary>
        [DataMemberAttribute]
        public string BranchCode {
            get;
            set;
        }
        /// <summary>
        ///分公司名称
        /// </summary>
        [DataMemberAttribute]
        public string BranchName {
            get;
            set;
        }
        /// <summary>
        ///品牌ID
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int PartsSalesCategoryId {
            get;
            set;
        }
        /// <summary>
        ///品牌
        /// </summary>
        [DataMemberAttribute]
        public string PartsSalesCategoryName {
            get;
            set;
        }
        /// <summary>
        /// 仓库ID
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int WarehouseId {
            get;
            set;
        }
        /// <summary>
        /// 仓库编号
        /// </summary>
        [DataMemberAttribute]
        public string WarehouseCode {
            get;
            set;
        }

        /// <summary>
        /// 仓库名称
        /// </summary>
        [DataMemberAttribute]
        public string WarehouseName {
            get;
            set;
        }

        /// <summary>
        /// 入库单编号
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public string PartsInboundOrderCode {
            get;
            set;
        }
        /// <summary>
        /// 入库单类型
        /// </summary>
        [DataMemberAttribute]
        public int? InboundType {
            get;
            set;
        }
        /// <summary>
        /// 采购订单类型
        /// </summary>
        [DataMemberAttribute]
        public string PartsPurchaseOrderTypeName {
            get;
            set;
        }
         /// <summary>
        /// 源单据编号
        /// </summary>
        [DataMemberAttribute]
        public string OriginalRequirementBillCode {
            get;
            set;
        }
        
            
    
        /// <summary>
        /// 结算状态
        /// </summary>
        [DataMemberAttribute]
        public int? settlementstatus {
            get;
            set;
        }
        /// <summary>
        /// 结算状态
        /// </summary>
        [DataMemberAttribute]
        public string SETTLEMENTSTATUSVALUE {
            get;
            set;
        }
         /// <summary>
        /// 结算单号
        /// </summary>
        [DataMemberAttribute]
        public string JsdNum {
            get;
            set;
        }
        /// <summary>
        /// 配件编号
        /// </summary>
        [DataMemberAttribute]
        public string PartCode {
            get;
            set;
        }

        /// <summary>
        /// 配件Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int PartId {
            get;
            set;
        }
        /// <summary>
        /// 配件名称
        /// </summary>
        [DataMemberAttribute]
        public string PartName {
            get;
            set;
        }
       
        /// <summary>
        /// 发货单位代码
        /// </summary>
        [DataMemberAttribute]
        public string CompanyCode {
            get;
            set;
        }
        /// <summary>
        /// 发货单位名称
        /// </summary>
        [DataMemberAttribute]
        public string CompanyName {
            get;
            set;
        }
        /// <summary>
        /// 入库数量
        /// </summary>
        [DataMemberAttribute]
        public int InQty {
            get;
            set;
        }

        /// <summary>
        /// 采购价
        /// </summary>
        [DataMemberAttribute]
        public decimal CostPrice {
            get;
            set;
        }
        /// <summary>
        /// 采购金额
        /// </summary>
        [DataMemberAttribute]
        public decimal TotalJH {
            get;
            set;
        }
        /// <summary>
        /// 计划价
        /// </summary>
        [DataMemberAttribute]
        public decimal Price {
            get;
            set;
        }

        /// <summary>
        /// 计划金额
        /// </summary>   
        [DataMemberAttribute]
        public decimal Total {
            get;
            set;
        }
     
        /// <summary>
        /// 创建时间
        /// </summary>   
        [DataMemberAttribute]
        public DateTime? Createtime {
            get;
            set;
        }
      
   
        /// <summary>
        /// 供应商编号
        /// </summary>   
        [DataMemberAttribute]
        public string SupplierCode {
            get;
            set;
        }
        /// <summary>
        /// 供应商名称
        /// </summary>
        [DataMemberAttribute]
        public string SupplierName {
            get;
            set;
        }

        
        [DataMemberAttribute]
        public int KIND {
            get;
            set;
        }
        [DataMemberAttribute]
        public string KINDNAME {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? PartsPurchaseOrderTypeId {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? status {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Name {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Code {
            get;
            set;
        }
    }
}