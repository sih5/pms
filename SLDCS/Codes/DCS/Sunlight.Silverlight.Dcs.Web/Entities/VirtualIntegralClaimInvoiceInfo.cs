﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 虚拟服务站保外发票
    /// </summary>
    [DataContract(IsReference = true)]
    public class VirtualIntegralClaimInvoiceInfo {
        /// <summary>
        /// Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }
        //发票代码
        [DataMemberAttribute]
        public string InvoiceCode {
            get;
            set;

        }
        [DataMemberAttribute]
        public decimal InvoiceTaxFee {
            get;
            set;

        }

        [DataMemberAttribute]
        public int? SettleType {
            get;
            set;

        }



        [DataMemberAttribute]
        public decimal NoTaxInvoiceFee {
            get;
            set;

        }
        //品牌
        [DataMemberAttribute]
        public string PartsSalesCategoryName {
            get;
            set;

        }
        [DataMemberAttribute]
        public string Remark {
            get;
            set;

        }
        //结算单编号
        [DataMemberAttribute]
        public string ClaimSettlementBillCode {
            get;
            set;

        }
        //发票号
        [DataMemberAttribute]
        public string InvoiceNumber {
            get;
            set;
        }
        //营销分公司编号
        [DataMemberAttribute]
        public string BranchCode {
            get;
            set;
        }
        //营销分公司名称
        [DataMemberAttribute]
        public string BranchName {
            get;
            set;
        }

        /// <summary>
        /// 发票金额
        /// </summary>
        [DataMemberAttribute]
        public decimal InvoiceFee {
            get;
            set;
        }

        [DataMemberAttribute]
        public double TaxRate {
            get;
            set;
        }
        [DataMemberAttribute]
        public int Status {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime InvoiceDate {
            get;
            set;
        }


        /// <summary>
        /// 服务站编号
        /// </summary>
        [DataMemberAttribute]
        public string DealerCode {
            get;
            set;
        }
        /// <summary>
        /// 服务站名称
        /// </summary>
        [DataMemberAttribute]
        public string DealerName {
            get;
            set;
        }
        /// <summary>
        /// 服务站业务编码
        /// </summary>
        [DataMemberAttribute]
        public string BusinessCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public int BranchId {
            get;
            set;
        }
        [DataMemberAttribute]
        public int DealerId {
            get;
            set;
        }
        [DataMemberAttribute]
        public int PartsSalesCategoryId {
            get;
            set;
        }

        [DataMemberAttribute]
        public string CreatorName {
            get;
            set;
        }

        [DataMemberAttribute]
        public DateTime? CreateTime {
            get;
            set;
        }

        [DataMemberAttribute]
        public string ModifierName {
            get;
            set;
        }

        [DataMemberAttribute]
        public DateTime? ModifyTime {
            get;
            set;
        }

        /// <summary>
        /// 作废人
        /// </summary>
        [DataMemberAttribute]
        public string AbandonerName {
            get;
            set;
        }
        /// <summary>
        /// 终审时间
        /// </summary>
        [DataMemberAttribute]
        public DateTime? AbandonTime {
            get;
            set;
        }
        /// <summary>
        /// 终审意见
        /// </summary>
        [DataMemberAttribute]
        public string ApproveComment {

            get;
            set;
        }
        /// <summary>
        /// 终审人
        /// </summary>
        [DataMemberAttribute]
        public string ApproverName {
            get;
            set;
        }

        /// <summary>
        /// 终审时间
        /// </summary>
        [DataMemberAttribute]
        public DateTime? ApproveTime {
            get;
            set;
        }

        ///<summary>
        ///初审人
        ///<summary>
        [DataMemberAttribute]
        public string InitialApproverName {
            get;
            set;
        }
        /// <summary>
        /// 初审意见
        /// </summary>
        [DataMemberAttribute]
        public string InitialApproverComment {
            get;
            set;
        }
        ///<summary>
        ///初审时间
        ///<summary>
        [DataMemberAttribute]
        public DateTime? InitialApproverTime {
            get;
            set;
        }

        /// <summary>
        /// 付款完成人
        /// </summary>
        [DataMemberAttribute]
        public string RejectName {
            get;
            set;
        }

        [DataMemberAttribute]
        public DateTime? RejectTime {
            get;
            set;
        }
        /// <summary>
        /// 付款完成审批意见
        /// </summary>
        [DataMemberAttribute]
        public string RejectComment {
            get;
            set;
        }
        /// <summary>
        /// 审批意见历史
        /// </summary>
        [DataMemberAttribute]
        public string ApproveCommentHistory {
            get;
            set;
        }
    }
}
