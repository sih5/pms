﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 虚拟发票
    /// </summary>
    [DataContract(IsReference = true)]
    public class VirtualInvoiceInformation : EntityObject {
        /// <summary>
        /// Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }

        [DataMemberAttribute]
        public string InvoiceCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string InvoiceNumber {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? PartsSalesCategoryId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartsSalesCategoryName {
            get;
            set;
        }
        [DataMemberAttribute]
        public string InvoiceCompanyCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string InvoiceCompanyName {
            get;
            set;
        }
        [DataMemberAttribute]
        public string InvoiceReceiveCompanyCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string InvoiceReceiveCompanyName {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal? InvoiceAmount {
            get;
            set;
        }
        [DataMemberAttribute]
        public double? TaxRate {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal? InvoiceTax {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? InvoiceDate {
            get;
            set;
        }
        [DataMemberAttribute]
        public int SourceType {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SourceCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public int OwnerCompanyId {
            get;
            set;
        }
        [DataMemberAttribute]
        public int InvoicePurpose {
            get;
            set;
        }
        [DataMemberAttribute]
        public int Type {
            get;
            set;
        }
        [DataMemberAttribute]
        public int Status {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Code {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CreatorName {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? CreateTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ApproverName {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? ApproveTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public int InvoiceCompanyId {
            get;
            set;
        }

        [DataMemberAttribute]
        public string BusinessCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string InvoiceDownloadLink {
            get;
            set;
        }
    }
}
