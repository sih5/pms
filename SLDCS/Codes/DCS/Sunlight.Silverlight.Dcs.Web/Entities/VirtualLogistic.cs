﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class VirtualLogistic : EntityObject {
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }
        [Key]
        [DataMemberAttribute]
        public string ObjId {
            get {
                return Guid.NewGuid().ToString().Replace("-", "").ToUpper();
            }
        }
        [DataMemberAttribute]
        public string ShippingCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string OrderCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ShippingCompanyName {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ShippingCompanyNumber {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? ApproveTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public string WarehouseName {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? ShippingDate {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? RequestedArrivalDate {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ReceivingAddress {
            get;
            set;
        }
        [DataMemberAttribute]
        public string LogisticName {
            get;
            set;
        }
        [DataMemberAttribute]
        public string TransportDriverPhone {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? ModifierId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ModifierName {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? ModifyTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? CancelId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CancelName {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? CancelTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? Status {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? ShippingMethod {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? SignStatus {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? PartsSalesOrderStatus {
            get;
            set;
        }
        [DataMemberAttribute]
        public int CurrentOrderType {
            get;
            set;
        }
    }
}
