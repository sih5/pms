﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 虚拟故障现象
    /// </summary>
    [DataContract(IsReference = true)]
    public class VirtualMalfunctionView : EntityObject {
        [Key]
        [DataMemberAttribute]
        public int MalfunctionId {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? ISISG {
            get;
            set;
        }
        [DataMemberAttribute]
        public string MalfunctionCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string MalfunctionDescription {
            get;
            set;
        }
        [DataMemberAttribute]
        public string MalfunctionQuickCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string MalfunctionRemark {
            get;
            set;
        }
        [DataMemberAttribute]
        public string MalfunctionCreatorName {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? MalfunctionCreateTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public string MalfunctionModifierName {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? MalfunctionModifyTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public int MalfunctionStatus {
            get;
            set;
        }
        [Key]
        [DataMemberAttribute]
        public int MalfunctionCategoryId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string MalfunctionCategoryCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string MalfunctionCategoryName {
            get;
            set;
        }
        [Key]
        [DataMemberAttribute]
        public int ParentId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ParentCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ParentName {
            get;
            set;
        }
        [Key]
        [DataMemberAttribute]
        public int? GrandParentId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string GrandParentCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string GrandParentName {
            get;
            set;
        }
    }
}
