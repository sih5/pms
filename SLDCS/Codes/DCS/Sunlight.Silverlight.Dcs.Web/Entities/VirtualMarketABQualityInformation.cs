﻿using System;
using System.Collections.Generic;
using System.Data.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 虚拟市场AB质量信息快报
    /// </summary>
    [DataContract(IsReference = true)]
    public class VirtualMarketABQualityInformation : EntityObject {
        /// <summary>
        /// 主键Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }

        /// <summary>
        /// 单据编号
        /// </summary>
        [DataMemberAttribute]
        public string Code {
            get;
            set;
        }

        /// <summary>
        /// 服务站名称
        /// </summary>
        [DataMemberAttribute]
        public string CompanyName {
            get;
            set;
        }

        /// <summary>
        /// 故障简述
        /// </summary>
        [DataMemberAttribute]
        public string FaultDescription {
            get;
            set;
        }

        /// <summary>
        /// 创建时间
        /// </summary>
        [DataMemberAttribute]
        public DateTime? CreateTime {
            get;
            set;
        }

        /// <summary>
        /// 故障台次
        /// </summary>
        [DataMemberAttribute]
        public int? FailureTimes {
            get;
            set;
        }

        /// <summary>
        /// 营销分公司
        /// </summary>
        [DataMemberAttribute]
        public string BranchName {
            get;
            set;
        }

        /// <summary>
        /// 品牌
        /// </summary>
        [DataMemberAttribute]
        public string PartsSalesCategoryName {
            get;
            set;
        }

        /// <summary>
        /// 状态
        /// </summary>
        [DataMemberAttribute]
        public int Status {
            get;
            set;
        }

        /// <summary>
        /// 驱动
        /// </summary>
        [DataMemberAttribute]
        public string Drive {
            get;
            set;
        }

        /// <summary>
        /// 车型号
        /// </summary>
        [DataMemberAttribute]
        public string ProductCategoryCode {
            get;
            set;
        }

        /// <summary>
        /// VIN
        /// </summary>
        [DataMemberAttribute]
        public string VIN {
            get;
            set;
        }

        /// <summary>
        /// 信息员/电话
        /// </summary>
        [DataMemberAttribute]
        public string InformationPhone {
            get;
            set;
        }

        /// <summary>
        /// 故障件名称
        /// </summary>
        [DataMemberAttribute]
        public string FaultyPartsName {
            get;
            set;
        }

        /// <summary>
        /// 故障模式
        /// </summary>
        [DataMemberAttribute]
        public string FailureMode {
            get;
            set;
        }

        /// <summary>
        /// 初步原因分析
        /// </summary>
        [DataMemberAttribute]
        public string FaultReason {
            get;
            set;
        }

        /// <summary>
        /// 运行里程
        /// </summary>
        [DataMemberAttribute]
        public int Mileage {
            get;
            set;
        }

        /// <summary>
        /// 版次
        /// </summary>
        [DataMemberAttribute]
        public string Revision {
            get;
            set;
        }

        /// <summary>
        /// 服务产品线
        /// </summary>
        [DataMemberAttribute]
        public string ServiceProductLineName {
            get;
            set;
        }

        /// <summary>
        /// 发动机型号
        /// </summary>
        [DataMemberAttribute]
        public string EngineModel {
            get;
            set;
        }

        /// <summary>
        /// 排放
        /// </summary>
        [DataMemberAttribute]
        public string Emission {
            get;
            set;
        }

        /// <summary>
        /// 祸首件责任代码
        /// </summary>
        [DataMemberAttribute]
        public string FaultyPartsSupplierCode {
            get;
            set;
        }

        /// <summary>
        /// 祸首件厂家名称
        /// </summary>
        [DataMemberAttribute]
        public string FaultyPartsSupplierName {
            get;
            set;
        }

        /// <summary>
        /// 故障件图号
        /// </summary>
        [DataMemberAttribute]
        public string FaultyPartsCode {
            get;
            set;
        }

        /// <summary>
        /// 故障属性
        /// </summary>
        [DataMemberAttribute]
        public int FailureType {
            get;
            set;
        }

        /// <summary>
        /// 故障车辆出厂编号明细
        /// </summary>
        [DataMemberAttribute]
        public string FailureSerialNumberDetail {
            get;
            set;
        }

        /// <summary>
        /// 已处理措施
        /// </summary>
        [DataMemberAttribute]
        public string HaveMeasures {
            get;
            set;
        }

        /// <summary>
        /// 销售时间
        /// </summary>
        [DataMemberAttribute]
        public DateTime? SalesDate {
            get;
            set;
        }

        /// <summary>
        /// 生产时间
        /// </summary>
        [DataMemberAttribute]
        public DateTime? OutOfFactoryDate {
            get;
            set;
        }

        /// <summary>
        /// 故障时间
        /// </summary>
        [DataMemberAttribute]
        public DateTime? FaultDate {
            get;
            set;
        }

        /// <summary>
        /// 道路条件
        /// </summary>
        [DataMemberAttribute]
        public string RoadConditions {
            get;
            set;
        }


        /// <summary>
        /// 载货种类
        /// </summary>
        [DataMemberAttribute]
        public string CargoTypes {
            get;
            set;
        }

        /// <summary>
        /// 服务站编号
        /// </summary>
        [DataMemberAttribute]
        public string CompanyCode {
            get;
            set;
        }

        /// <summary>
        /// 审批意见历史
        /// </summary>
        [DataMemberAttribute]
        public string ApproveCommentHistory {
            get;
            set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        [DataMemberAttribute]
        public string Remark {
            get;
            set;
        }

        /// <summary>
        /// 故障详细描述
        /// </summary>
        [DataMemberAttribute]
        public string FaultyDescription {
            get;
            set;
        }

        /// <summary>
        /// 故障地点
        /// </summary>
        [DataMemberAttribute]
        public string FaultyLocation {
            get;
            set;
        }

        /// <summary>
        /// 故障数量
        /// </summary>
        [DataMemberAttribute]
        public int? FaultyQuantity {
            get;
            set;
        }

        /// <summary>
        /// 故障性质
        /// </summary>
        [DataMemberAttribute]
        public string FaultyNature {
            get;
            set;
        }

        /// <summary>
        /// 信息级别
        /// </summary>
        [DataMemberAttribute]
        public string MessageLevel {
            get;
            set;
        }

        /// <summary>
        /// 站长/电话
        /// </summary>
        [DataMemberAttribute]
        public string StationMasterPhone {
            get;
            set;
        }

        /// <summary>
        /// 市场部服务管理员/电话
        /// </summary>
        [DataMemberAttribute]
        public string ManagePhone {
            get;
            set;
        }

        /// <summary>
        /// 载货重量
        /// </summary>
        [DataMemberAttribute]
        public double? CarryWeight {
            get;
            set;
        }

        /// <summary>
        /// 故障里程
        /// </summary>
        [DataMemberAttribute]
        public double? FaultyMileage {
            get;
            set;
        }

        /// <summary>
        /// 批次号
        /// </summary>
        [DataMemberAttribute]
        public string BatchNumber {
            get;
            set;
        }

        /// <summary>
        /// 创建人
        /// </summary>
        [DataMemberAttribute]
        public string CreatorName {
            get;
            set;
        }

        /// <summary>
        /// 修改人
        /// </summary>
        [DataMemberAttribute]
        public string ModifierName {
            get;
            set;
        }

        /// <summary>
        /// 修改时间
        /// </summary>
        [DataMemberAttribute]
        public DateTime? ModifyTime {
            get;
            set;
        }

        /// <summary>
        /// 终审人
        /// </summary>
        [DataMemberAttribute]
        public string ApproverName {
            get;
            set;
        }

        /// <summary>
        /// 终审时间
        /// </summary>
        [DataMemberAttribute]
        public DateTime? ApproveTime {
            get;
            set;
        }

        /// <summary>
        /// 初审人
        /// </summary>
        [DataMemberAttribute]
        public string InitialApproverName {
            get;
            set;
        }

        /// <summary>
        /// 初审时间
        /// </summary>
        [DataMemberAttribute]
        public DateTime? InitialApproverTime {
            get;
            set;
        }

        /// <summary>
        /// 驳回人
        /// </summary>
        [DataMemberAttribute]
        public string RejectName {
            get;
            set;
        }

        /// <summary>
        /// 驳回时间
        /// </summary>
        [DataMemberAttribute]
        public DateTime? RejectTime {
            get;
            set;
        }

        /// <summary>
        /// 会签人
        /// </summary>
        [DataMemberAttribute]
        public string CountersignatureCreatorName {
            get;
            set;
        }

        /// <summary>
        /// 会签时间
        /// </summary>
        [DataMemberAttribute]
        public DateTime? CountersignatureCreateTime {
            get;
            set;
        }

        /// <summary>
        /// 分销商Id
        /// </summary>
        [DataMemberAttribute]
        public int DealerId {
            get;
            set;
        }

        /// <summary>
        /// 配件销售类型Id
        /// </summary>
        [DataMemberAttribute]
        public int PartsSalesCategoryId {
            get;
            set;
        }

        /// <summary>
        /// 维修对象
        /// </summary>
        [DataMemberAttribute]
        public string RepairTarget {
            get;
            set;
        }

    }
}
