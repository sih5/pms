﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel.DomainServices.Server;
using System.Text;
using Sunlight.Silverlight.Dcs.Web.Entities;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 会员管理
    /// </summary>
    [DataContractAttribute(IsReference = true)]
    public class VirtualMemberInfo : EntityObject {
        [Key]
        [DataMemberAttribute]
        public string MemberNumber {
            get;
            set;
        }

        [DataMemberAttribute]
        public string CellPhoneNumber {
            get;
            set;
        }

        [DataMemberAttribute]
        public string MemberRank {
            get;
            set;
        }

        [DataMemberAttribute]
        public decimal? MemberValue {
            get;
            set;
        }
        [DataMemberAttribute]
        public string BusinessType {
            get;
            set;
        }

        [DataMemberAttribute]
        public string IdDocumentType {
            get;
            set;
        }

        [DataMemberAttribute]
        public string IdDocumentNumber {
            get;
            set;
        }

        [DataMemberAttribute]
        public string Name {
            get;
            set;
        }

        [DataMemberAttribute]
        public string ProvinceName {
            get;
            set;
        }

        [DataMemberAttribute]
        public string CityName {
            get;
            set;
        }

        [DataMemberAttribute]
        public string CountyName {
            get;
            set;
        }
        [DataMemberAttribute]
        public string DetailedAddress {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PostCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Gender {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Nation {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? Birthdate {
            get;
            set;
        }
        [DataMemberAttribute]
        public string IfMarried {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? MarriedDate {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Hobby {
            get;
            set;
        }
        [DataMemberAttribute]
        public string AnnualIncome {
            get;
            set;
        }

        [DataMemberAttribute]
        public string VIN {
            get;
            set;
        }

        [DataMemberAttribute]
        public string VehicleLicense {
            get;
            set;
        }

        [DataMemberAttribute]
        public string MemStatus {
            get;
            set;
        }

        private EntityCollection<VirtualFreeRescue> virtualFreeRescues;
        [DataMember]
        [Include]
        [Association("FK_MemberInfoInterfaces_Parent", "MemberNumber", "MemberNumber")]
        public EntityCollection<VirtualFreeRescue> VirtualFreeRescues {
            get {
                return this.virtualFreeRescues ??
                       (this.virtualFreeRescues = new EntityCollection<VirtualFreeRescue>());
            }
            set {
                virtualFreeRescues = value;
            }
        }
     }
}
