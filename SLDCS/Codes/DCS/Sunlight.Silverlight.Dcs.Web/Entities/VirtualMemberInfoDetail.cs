﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Sunlight.Silverlight.Dcs.Web.Entities {
    [DataContract(IsReference = true)]
    public class VirtualMemberInfoDetail {
        [Key]
        [DataMemberAttribute]
        public string MemberNumber {
            get;
            set;
        }

        [DataMemberAttribute]
        public string CellPhoneNumber {
            get;
            set;
        }

        [DataMemberAttribute]
        public string MemberRank {
            get;
            set;
        }

        [DataMemberAttribute]
        public decimal? MemberValue {
            get;
            set;
        }

        [DataMemberAttribute]
        public string BusinessType {
            get;
            set;
        }

        [DataMemberAttribute]
        public string IdDocumentType {
            get;
            set;
        }

        [DataMemberAttribute]
        public string IdDocumentNumber {
            get;
            set;
        }

        [DataMemberAttribute]
        public string Name {
            get;
            set;
        }

        [DataMemberAttribute]
        public string ProvinceName {
            get;
            set;
        }

        [DataMemberAttribute]
        public string CityName {
            get;
            set;
        }

        [DataMemberAttribute]
        public string CountyName {
            get;
            set;
        }
        [DataMemberAttribute]
        public string DetailedAddress {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PostCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Gender {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Nation {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? Birthdate {
            get;
            set;
        }
        [DataMemberAttribute]
        public string IfMarried {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? MarriedDate {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Hobby {
            get;
            set;
        }
        [DataMemberAttribute]
        public string AnnualIncome {
            get;
            set;
        }

        [DataMemberAttribute]
        public string FreeRescueName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? FreeRescueTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? FreeRescueDate {
            get;
            set;
        }

        [DataMemberAttribute]
        public string MemStatus {
            get;
            set;
        }

        [DataMemberAttribute]
        public string BenefiType {
            get;
            set;
        }
        [DataMemberAttribute]
        public string BenefiUom {
            get;
            set;
        }
        [DataMemberAttribute]
        public string DiscountNum {
            get;
            set;
        }
    }
}
