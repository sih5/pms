﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 虚拟配件出入库
    /// </summary>
    [DataContract(IsReference = true)]
    public class VirtualOutboundAndInboundBill : EntityObject {
        /// <summary>
        /// 出入库清单Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int BillId {
            get;
            set;
        }

        /// <summary>
        /// 仓库Id
        /// </summary>
        [DataMemberAttribute]
        public int WarehouseId {
            get;
            set;
        }

        /// <summary>
        /// 仓库编号
        /// </summary>
        [DataMemberAttribute]
        [Display(Name = "VirtualPartsStock_WarehouseCode", ResourceType = typeof(Resources.EntityStrings))]
        public string WarehouseCode {
            get;
            set;
        }

        /// <summary>
        /// 仓库名称
        /// </summary>
        [DataMemberAttribute]
        [Display(Name = "VirtualPartsStock_WarehouseName", ResourceType = typeof(Resources.EntityStrings))]
        public string WarehouseName {
            get;
            set;
        }

        /// <summary>
        /// 出入库单编号
        /// </summary>
        [DataMemberAttribute]
        public string BillCode {
            get;
            set;
        }

        /// <summary>
        /// 出入库类型
        /// </summary>
        [DataMemberAttribute]
        public int BillBusinessType {
            get;
            set;
        }

        /// <summary>
        ///  数量
        /// </summary>
        [DataMemberAttribute]
        public int Quantity {
            get;
            set;
        }

        /// <summary>
        /// 仓储企业Id
        /// </summary>
        [DataMemberAttribute]
        public int StorageCompanyId {
            get;
            set;
        }

        /// <summary>
        /// 仓储企业类型
        /// </summary>
        [DataMemberAttribute]
        public int StorageCompanyType {
            get;
            set;
        }

        /// <summary>
        /// 营销分公司Id
        /// </summary>
        [DataMemberAttribute]
        public int BranchId {
            get;
            set;
        }
        /// <summary>
        /// 销售类型Id
        /// </summary>
        [DataMemberAttribute]
        public int SalesCategoryId {
            get;
            set;
        }
        /// <summary>
        /// 销售订单类型Id
        /// </summary>
        [DataMemberAttribute]
        public int PartsSalesOrderTypeId {
            get;
            set;
        }

        /// <summary>
        /// 配件销售类型名称
        /// </summary>
        [DataMemberAttribute]
        public string PartsSalesCategoryName {
            get;
            set;
        }

        /// <summary>
        /// 配件销售订单类型名称
        /// </summary>
        [DataMemberAttribute]
        public string PartsSalesOrderTypeName {
            get;
            set;
        }

        /// <summary>
        /// 营销分公司
        /// </summary>
        [DataMemberAttribute]
        public string BranchName {
            get;
            set;
        }

        /// <summary>
        /// 对方单位编号
        /// </summary>
        [DataMemberAttribute]
        public string CounterpartCompanyCode {
            get;
            set;
        }

        /// <summary>
        /// 对方单位名称
        /// </summary>
        [DataMemberAttribute]
        public string CounterpartCompanyName {
            get;
            set;
        }

        /// <summary>
        /// 收货仓库编号
        /// </summary>
        [DataMemberAttribute]
        public string ReceivingWarehouseCode {
            get;
            set;
        }

        /// <summary>
        /// 收货仓库名称
        /// </summary>
        [DataMemberAttribute]
        public string ReceivingWarehouseName {
            get;
            set;
        }

        /// <summary>
        /// 配件Id
        /// </summary>
        [DataMemberAttribute]
        public int SparePartId {
            get;
            set;
        }

        /// <summary>
        ///  配件编号
        /// </summary>
        [DataMemberAttribute]
        [Display(Name = "VirtualPartsStock_SparePartCode", ResourceType = typeof(Resources.EntityStrings))]
        public string SparePartCode {
            get;
            set;
        }

        /// <summary>
        ///  配件名称
        /// </summary>
        [DataMemberAttribute]
        [Display(Name = "VirtualPartsStock_SparePartName", ResourceType = typeof(Resources.EntityStrings))]
        public string SparePartName {
            get;
            set;
        }

        /// <summary>
        /// 库区用途
        /// </summary>
        [DataMemberAttribute]
        [Display(Name = "VirtualPartsStock_WarehouseAreaCategory", ResourceType = typeof(Resources.EntityStrings))]
        public int WarehouseAreaCategory {
            get;
            set;
        }

        /// <summary>
        /// 库位Id
        /// </summary>
        [DataMemberAttribute]
        public int WarehouseAreaId {
            get;
            set;
        }
         
        /// <summary>
        ///  库位编号
        /// </summary>
        [DataMemberAttribute]
        [Display(Name = "VirtualPartsStock_WarehouseAreaCode", ResourceType = typeof(Resources.EntityStrings))]
        public string WarehouseAreaCode {
            get;
            set;
        }

        /// <summary>
        /// 计量单位
        /// </summary>
        [DataMemberAttribute]
        public string MeasureUnit {
            get;
            set;
        }
        /// <summary>
        /// 库区编号
        /// </summary>
        [DataMemberAttribute]
        public string WarehouseRegionCode {
            get;
            set;
        }

        /// <summary>
        /// 创建时间
        /// </summary>
        [DataMemberAttribute]
        public DateTime CreateTime {
            get;
            set;
        }

        /// <summary>
        /// 批发价
        /// </summary>
        [DataMemberAttribute]
        public decimal WholesalePrice {
            get;
            set;
        }

        
    }
}
