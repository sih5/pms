﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 积压件平台
    /// </summary>
    [DataContract(IsReference = true)]
    public class VirtualOverstockPartsPlatFormBill : EntityObject {
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? CenterId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CenterName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? DealerId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string DealerCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string DealerName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int SparePartId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SparePartCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SparePartName {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal? RetailGuidePrice {
            get;
            set;
        }
        [DataMemberAttribute]
        public double? DiscountRate {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal? DiscountedPrice {
            get;
            set;
        }
        [DataMemberAttribute]
        public string EntityStatus {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Attachment {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? Status {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? CreatorId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CreatorName {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? CreateTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? WarehouseId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string WarehouseCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string WarehouseName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int Quantity {
            get;
            set;
        }
    }
}
