﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 虚拟发票
    /// </summary>
    [DataContract(IsReference = true)]
    public class VirtualPartsBranch : EntityObject {
        /// <summary>
        /// Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }
        [DataMemberAttribute]
        public int PartId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartName {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CADCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public int Status {
            get;
            set;
        }
        [DataMemberAttribute]
        public int PartType {
            get;
            set;
        }
        [DataMemberAttribute]
        public string MeasureUnit {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Specification {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Feature {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ReferenceCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? CreateTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? ModifyTime {
            get;
            set;
        }
        /// <summary>
        /// 零售指导价
        /// </summary>
        [DataMemberAttribute]
        public decimal? RetailGuidePrice {
            get;
            set;
        }
        /// <summary>
        /// 销售价
        /// </summary>
        [DataMemberAttribute]
        public decimal? SalesPrice {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? PartsSalesCategoryId {
            get;
            set;
        }
        [DataMember]
        public string PartsSalesCategoryName {
            get;
            set;
        }
        /// <summary>
        /// 互换识别号
        /// </summary>
        [DataMemberAttribute]
        public string ExchangeIdentification {
            get;
            set;
        }
        /// <summary>
        /// 海外配件编号
        /// </summary>
        [DataMemberAttribute]
        public string OverseasPartsFigure {
            get;
            set;
        }
        /// <summary>
        /// 产品商标
        /// </summary>
        [DataMemberAttribute]
        public string ProductBrand {
            get;
            set;
        }
        /// <summary>
        /// 英文名称
        /// </summary>
        [DataMemberAttribute]
        public string EnglishName {
            get;
            set;
        }
        [DataMemberAttribute]
        public bool IsOrderable {
            get;
            set;
        }
        [DataMemberAttribute]
        public bool? IsSalable {
            get;
            set;
        }
        [DataMemberAttribute]
        public bool IsDirectSupply {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? MinSaleQuantity {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? ProductLifeCycle {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? LossType {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? PartsAttribution {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartsWarrantyCategoryName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int PartsReturnPolicy {
            get;
            set;
        }
        [DataMemberAttribute]
        public string GroupCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string GroupName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? PartABC {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? StockMaximum {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? StockMinimum {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal? PartsMaterialManageCost {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? PartsWarrantyLong {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? PlannedPriceCategory {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? AutoApproveUpLimit {
            get;
            set;
        }
        [DataMemberAttribute]
        public string StandardCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string StandardName {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Remark {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CreatorName {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ModifierName {
            get;
            set;
        }
        [DataMemberAttribute]
        public string AbandonerName {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? AbandonTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public int BranchId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string BranchName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? PartsWarhouseManageGranularity {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? WarrantySupplyStatus {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? PurchaseRoute {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? OMSparePartMark {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? PackingCoefficient1
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? PackingCoefficient2
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? PackingCoefficient3
        {
            get;
            set;
        }

        [DataMemberAttribute]
        public DateTime? BreakTime
        {
            get;
            set;
        }

        [DataMemberAttribute]
        public bool? IsAccreditPack
        {
            get;
            set;
        }

        [DataMemberAttribute]
        public bool? IsAutoCaclSaleProperty
        {
            get;
            set;
        }
         [DataMemberAttribute]
        public int? MainPackingType
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string FirPrin
        {
             get;
             set;
        }
        [DataMemberAttribute]
        public string SecPrin
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ThidPrin
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string FirPackingMaterial
        {
             get;
             set;
        }
        [DataMemberAttribute]
        public string SecPackingMaterial
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ThidPackingMaterial
        {
            get;
            set;
        }
    }
}
