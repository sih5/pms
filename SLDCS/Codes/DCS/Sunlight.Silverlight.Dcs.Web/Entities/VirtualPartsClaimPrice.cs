﻿using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 虚拟配件库存
    /// </summary>
    [DataContract(IsReference = true)]
    public class VirtualPartsClaimPrice : EntityObject {
        [DataMemberAttribute]
        public int BranchId {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? DealerId {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? DeleaveAmount {
            get;
            set;
        }
        [DataMemberAttribute]
        public string DeleavePartCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Feature {
            get;
            set;
        }
        [DataMemberAttribute]
        public string MeasureUnit {
            get;
            set;
        }
        [Key]
        [DataMemberAttribute]
        public int PartsSalesCategoryId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartsSalesCategoryName {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartsSupplierCode {
            get;
            set;
        }
        [Key]
        [DataMemberAttribute]
        public int PartsSupplierId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartsSupplierName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? PartsWarrantyCategoryId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartsWarrantyCategoryName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int PartType {
            get;
            set;
        }
        [Key]
        [DataMemberAttribute]
        public int Quantity {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal SalesPrice {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Specification {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? SubDealerId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string UsedPartsCode {
            get;
            set;
        }
        [Key]
        [DataMemberAttribute]
        public int UsedPartsId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string UsedPartsName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int UsedPartsReturnPolicy {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? SparePartId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SparePartCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SparePartName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int SparePartPartType {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SparePartMeasureUnit {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SparePartSpecification {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SparePartFeature {
            get;
            set;
        }
    }
}