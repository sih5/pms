﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Sunlight.Silverlight.Dcs.Web.Entities {
    [DataContract(IsReference = true)]
    public class VirtualPartsDifferenceBackBillDtl : EntityObject {
        [Key]
        [DataMemberAttribute]
        public int TaskId {
            get;
            set;
        }

        [DataMemberAttribute]
        public string TaskCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public int SparePartId {
            get;
            set;
        }

        [DataMemberAttribute]
        public string SparePartCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SparePartName {
            get;
            set;
        }
        
        [DataMemberAttribute]
        public int DiffQuantity {
            get;
            set;
        }

        [DataMemberAttribute]
        public string SourceCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? TraceProperty {
            get;
            set;
        }
    }
}
