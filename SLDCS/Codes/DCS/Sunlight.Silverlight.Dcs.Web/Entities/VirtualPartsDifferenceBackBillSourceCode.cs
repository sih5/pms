﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class VirtualPartsDifferenceBackBillSourceCode : EntityObject {
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }

        [DataMemberAttribute]
        public string SourceCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string Code {
            get;
            set;
        }

        [DataMemberAttribute]
        public string Status {
            get;
            set;
        }

        [DataMemberAttribute]
        public string WarehouseCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string WarehouseName {
            get;
            set;
        }
        
        [DataMemberAttribute]
        public string PartsInboundCheckBillCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string CreatorName {
            get;
            set;
        }

        [DataMemberAttribute]
        public DateTime? CreateTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? OriginalRequirementBillId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string OriginalRequirementBillCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? PartsSupplierId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartsSupplierCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartsSupplierName {
            get;
            set;
        }
    }
}
