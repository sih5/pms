﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class VirtualPartsExchange : EntityObject {
        [Key]
        [DataMember]
        public int Id {
            get;
            set;
        }
        [DataMember]
        public int PartsExchangeId {
            get;
            set;
        }
        /// <summary>
        /// 互换识别号
        /// </summary>
        [DataMemberAttribute]
        public string ExchangeCode {
            get;
            set;
        }

        /// <summary>
        /// 互换号
        /// </summary>
        [DataMemberAttribute]
        public string ExGroupCode {
            get;
            set;
        }

        /// <summary>
        /// 配件图号
        /// </summary>
        [DataMemberAttribute]
        public string PartCode {
            get;
            set;
        }

        /// <summary>
        /// 配件名称
        /// </summary>
        [DataMemberAttribute]
        public string PartName {
            get;
            set;
        }

        /// <summary>
        /// 状态
        /// </summary>
        [DataMemberAttribute]
        public int Status {
            get;
            set;
        }

        [DataMemberAttribute]
        public string Remark {
            get;
            set;
        }

        [DataMemberAttribute]
        public string CreatorName {
            get;
            set;
        }

        [DataMemberAttribute]
        public DateTime? CreateTime {
            get;
            set;
        }

        [DataMemberAttribute]
        public string ModifierName {
            get;
            set;
        }

        [DataMemberAttribute]
        public DateTime? ModifyTime {
            get;
            set;
        }

        [DataMemberAttribute]
        public string AbandonerName {
            get;
            set;
        }

        [DataMemberAttribute]
        public DateTime? AbandonTime {
            get;
            set;
        }
    }
}
