﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class VirtualPartsExchangeGroup : EntityObject {
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }
        /// <summary>
        /// 配件图号
        /// </summary>
        [DataMemberAttribute]
        public string SparePartCode {
            get;
            set;
        }
        /// <summary>
        /// 配件名称
        /// </summary>
        [DataMemberAttribute]
        public string SparePartName {
            get;
            set;
        }
        /// <summary>
        /// 零部件图号
        /// </summary>
        [DataMemberAttribute]
        public string ReferenceCode {
            get;
            set;
        }
        /// <summary>
        /// 互换识别号
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public string ExchangeCode {
            get;
            set;
        }
        /// <summary>
        /// PartsExchangeGroupId
        /// </summary>
        [DataMemberAttribute]
        public int PartsExchangeGroupId {
            get;
            set;
        }
        /// <summary>
        /// Status
        /// </summary>
        [DataMemberAttribute]
        public int Status {
            get;
            set;
        }
        /// <summary>
        /// Status
        /// </summary>
        [DataMemberAttribute]
        public DateTime? CreateTime {
            get;
            set;
        }
        /// <summary>
        /// 创建人
        /// </summary>
        [DataMemberAttribute]
        public string CreatorName {
            get;
            set;
        }
        /// <summary>
        /// 作废时间
        /// </summary>
        [DataMemberAttribute]
        public DateTime? AbandonTime {
            get;
            set;
        }
        /// <summary>
        /// 作废人
        /// </summary>
        [DataMemberAttribute]
        public string AbandonerName {
            get;
            set;
        }
    }
}
