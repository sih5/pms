﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 虚拟入库检验单
    /// </summary>
    [DataContract(IsReference = true)]
    public class VirtualPartsInboundCheckBill : EntityObject {
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Code {
            get;
            set;
        }
        [DataMemberAttribute]
        public int PartsInboundPlanId {
            get;
            set;
        }
        [DataMemberAttribute]
        public int WarehouseId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string WarehouseCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string WarehouseName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int StorageCompanyId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string StorageCompanyCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string StorageCompanyName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int StorageCompanyType {
            get;
            set;
        }
        [DataMemberAttribute]
        public int PartsSalesCategoryId {
            get;
            set;
        }
        [DataMemberAttribute]
        public int BranchId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string BranchCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string BranchName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int CounterpartCompanyId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CounterpartCompanyCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CounterpartCompanyName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int OriginalRequirementBillId {
            get;
            set;
        }
        [DataMemberAttribute]
        public int OriginalRequirementBillType {
            get;
            set;
        }
        [DataMemberAttribute]
        public string OriginalRequirementBillCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public int InboundType {
            get;
            set;
        }
        [DataMemberAttribute]
        public int SettlementStatus {
            get;
            set;
        }
        [DataMemberAttribute]
        public int Status {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? CustomerAccountId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string GPMSPurOrderCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PurOrderCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ReturnContainerNumber {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SAPPurchasePlanCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Remark {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? CreatorId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CreatorName {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? CreateTime {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? ModifierId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ModifierName {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? ModifyTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartsInboundPlanCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartsSalesCategoryName {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ERPSourceOrderCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartsSalesOrderCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? InvoiceType {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Objid {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal TotalAmount {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartsPurchaseOrderTypeNameQuery {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PlanSourceQuery {
            get;
            set;
        }
        [DataMemberAttribute]
        public int ReturnType {
            get;
            set;
        }

        [DataMemberAttribute]
        public string PWMSStorageCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string CPPartsPurchaseOrderCode
        {
            get;
            set;
        }

        [DataMemberAttribute]
        public string CPPartsInboundCheckCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SparePartCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SparePartName
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal TotalAmountForWarehouse
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string BatchNumber {
            get;
            set;
        }
        [DataMemberAttribute]
        public bool HasDifference {
            get;
            set;
        }
    }
}
