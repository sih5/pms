﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web
{
      [DataContract(IsReference = true)]
    public class VirtualPartsInboundCheckBillDetail : EntityObject
    {
          [Key]
          [DataMemberAttribute]
          public int Id
          {
              get;
              set;
          }
          [DataMemberAttribute]
          public int PartsInboundCheckBillId
          {
              get;
              set;
          }
          [DataMemberAttribute]
          public string DestWarehouseAreaCode
          {
              get;
              set;
          }
          [DataMemberAttribute]
          public string SparePartCode
          {
              get;
              set;
          }
          [DataMemberAttribute]
          public string SparePartName
          {
              get;
              set;
          }
          [DataMemberAttribute]
          public string OverseasPartsFigure
          {
              get;
              set;
          }
          [DataMemberAttribute]
          public string WarehouseAreaCode
          {
              get;
              set;
          }
          [DataMemberAttribute]
          public int InspectedQuantity
          {
              get;
              set;
          }
          [DataMemberAttribute]
          public decimal SettlementPrice
          {
              get;
              set;
          }
          [DataMemberAttribute]
          public string MeasureUnit
          {
              get;
              set;
          }
          [DataMemberAttribute]
          public string Remark
          {
              get;
              set;
          }
          [DataMemberAttribute]
          public string SpareOrderRemark
          {
              get;
              set;
          }
          [DataMemberAttribute]
          public int SparePartId
          {
              get;
              set;
          }
          [DataMemberAttribute]
          public string SupplierPartCode
          {
              get;
              set;
          }
           [DataMemberAttribute]
          public string BatchNumber {
              get;
              set;
          }
    }
}
