﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class VirtualPartsInboundCheckBillForPrint : EntityObject {
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }
        /**
       * 分公司名称
       **/
        [DataMemberAttribute]
        public string BranchName {
            get;
            set;
        }
        /**
         * 仓库名称
         **/
        [DataMemberAttribute]
        public string WarehouseName {
            get;
            set;
        }
        /**
         * 对方单位名称
         **/
        [DataMemberAttribute]
        public string CounterpartCompanyName {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Code {
            get;
            set;
        }
        /**
         * 入库计划单编号
         * */
        [DataMemberAttribute]
        public string PartsInboundPlanCode {
            get;
            set;
        }
        /**
        * 入库类型
        * */
        [DataMemberAttribute]
        public int InboundType {
            get;
            set;
        }
        /**
        * 收货日期
        * */
        [DataMemberAttribute]
        public DateTime? CreateTime {
            get;
            set;
        }
        /**
       * 收货员
       * */
        [DataMemberAttribute]
        public string CreatorName {
            get;
            set;
        }
        /**
         * 配件编号
        * */
        [DataMemberAttribute]
        public string SparePartCode {
            get;
            set;
        }
        /**
         * 配件编号
        * */
        [DataMemberAttribute]
        public string SparePartName {
            get;
            set;
        }
        /**
         * 红岩号
        * */
        [DataMemberAttribute]
        public string ReferenceCode {
            get;
            set;
        }
        /**
         * 单位
        * */
        [DataMemberAttribute]
        public string MeasureUnit {
            get;
            set;
        }
        /**
         * 检验量
         * */
        [DataMemberAttribute]
        public int InspectedQuantity {
            get;
            set;
        }
        /**
         * 包装信息（箱号）
        * */
        [DataMemberAttribute]
        public string PackingCode {
            get;
            set;
        }
        /**
        * 包材数量
        * */
        [DataMemberAttribute]
        public int PackNum {
            get;
            set;
        }
        /**
         * 上架库位
         * */
        [DataMemberAttribute]
        public string WarehouseAreaCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public int Quantity {
            get;
            set;
        }
        [DataMemberAttribute]
        public int SparePartId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string EnglishName {
            get;
            set;
        }
        /**
       * 数量（包装属性：箱标：取最大层级，件标：取：最小包装）
       * */
        [DataMemberAttribute]
        public int PackingCoefficient {
            get;
            set;
        }
        //批次号
        [DataMemberAttribute]
        public string BachMunber {
            get;
            set;
        }
        //打印次数
        [DataMemberAttribute]
        public int PrintNumber {
            get;
            set;

        }
        [DataMemberAttribute]
        public int PartsInboundCheckBillId {
            get;
            set;
        }
        //序列号
        [DataMemberAttribute]
        public int SerialNumber {
            get;
            set;
        }
        [DataMemberAttribute]
        public string FirMeasureUnit {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SecMeasureUnit {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ThidMeasureUnit {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? FirPrintNumber {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? SecPrintNumber {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? ThidPrintNumber {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? FirPackingCoefficient {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? SecPackingCoefficient {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? ThidPackingCoefficient {
            get;
            set;
        }
        [DataMemberAttribute]
        public string TopCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Priority {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? TraceProperty {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Jq {
            get;
            set;
        }
        [DataMemberAttribute]
        public string TracePropertys {
            get;
            set;
        }
        /**
        * 配件编号
       * */
        [DataMemberAttribute]
        public string SparePartCodeX {
            get;
            set;
        }
        /**
         * 批次号
         * **/
        [DataMemberAttribute]
        public string BatchNumber {
            get;
            set;
        }
    }
}
