﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 虚拟入库单
    /// </summary>
    [DataContract(IsReference = true)]
    public class VirtualPartsInboundPlan : EntityObject {
        
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Code {
            get;
            set;
        }
        [DataMemberAttribute]
        public int WarehouseId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string WarehouseCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string WarehouseName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int StorageCompanyId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string StorageCompanyCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string StorageCompanyName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int StorageCompanyType {
            get;
            set;
        }
        [DataMemberAttribute]
        public int BranchId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string BranchCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string BranchName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int PartsSalesCategoryId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartsSalesCategoryName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int CounterpartCompanyId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CounterpartCompanyCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CounterpartCompanyName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int SourceId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SourceCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public int InboundType {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? CustomerAccountId {
            get;
            set;
        }
        [DataMemberAttribute]
        public int OriginalRequirementBillId {
            get;
            set;
        }
        [DataMemberAttribute]
        public int OriginalRequirementBillType {
            get;
            set;
        }
        [DataMemberAttribute]
        public int Status {
            get;
            set;
        }
        [DataMemberAttribute]
        public bool IfWmsInterface {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Remark {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? CreatorId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CreatorName {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? CreateTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? ModifierId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ModifierName {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? ModifyTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public string OriginalRequirementBillCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal TotalAmountForWarehouse
        {
            get;
            set;
        }
        
        [DataMemberAttribute]
        public string PartsPurchaseOrderTypeName1
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PlanSource1
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int ReturnType
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ERPSourceOrderCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartsSalesOrderCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? PlanDeliveryTime
        {
            get;
            set;
        }
       
        [DataMemberAttribute]
        public DateTime? ArrivalDate
        {
            get;
            set;
        }

        /// <summary>
        /// 优先级
        /// </summary>
        [DataMemberAttribute]
        public int Priority {
            get;
            set;
        }

        [DataMemberAttribute]
        public decimal TotalAmount
        {
            get;
            set;
        }
    }
}
