﻿
using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class VirtualPartsInboundPlanDetail : EntityObject
    {
        /// <summary>
        /// 配件出库单清单Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }
        /// <summary>
        /// 入库计划单编号
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public string Code {
            get;
            set;
        }
        /// <summary>
        ///   供货单位：配件入库计划.对方单位名称								
        /// </summary>
        [DataMemberAttribute]
        public string CounterpartCompanyName {
            get;
            set;
        }
        /// <summary>
        /// 分公司名称
        /// </summary>
        [DataMemberAttribute]
        public string BranchName {
            get;
            set;
        }
        /// <summary>
        ///   顶层库区ID	
        /// </summary>
        [DataMemberAttribute]
        public int? TopLevelWarehouseAreaId {
            get;
            set;
        }
        /// <summary>
        ///    入库类型：配件入库计划.入库类型									
        /// </summary>
        [DataMemberAttribute]
        public int InboundType {
            get;
            set;
        }
        [DataMemberAttribute]
        public int SparePartId {
            get;
            set;
        }


        /// <summary>
        ///     采购订单类型：如果配件入库计划.入库类型=配件采购，原始需求单据类型=配件采购单								
        /// </summary>
        [DataMemberAttribute]
        public string PartsPurchaseOrderType {
            get;
            set;
        }

        /// <summary>
        ///           制单时间：配件入库计划.创建时间								
        /// </summary>
        [DataMemberAttribute]
        public DateTime? CreateTime {
            get;
            set;
        }

        /// <summary>
        ///   制单人：	   配件入库计划.创建人											
        /// </summary>
        [DataMemberAttribute]
        public string CreatorName {
            get;
            set;
        }

        /// <summary>
        ///       品牌：    配件入库计划.品牌.名称								
        /// </summary>
        [DataMemberAttribute]
        public string PartsSalesCategoryName {
            get;
            set;
        }

        /// <summary>
        ///    收货单位：配件入库计划.仓储企业名称									
        /// </summary>
        [DataMemberAttribute]
        public string StorageCompanyName {
            get;
            set;
        }

        /// <summary>
        ///    配件图号：配件入库计划清单.配件编号										
        /// </summary>
        [DataMemberAttribute]
        public string SparePartCode {
            get;
            set;
        }
        /// <summary>
        ///     配件名称：配件入库计划清单.配件名称			
        /// </summary>
        [DataMemberAttribute]
        public string SparePartName {
            get;
            set;
        }
        /// <summary>
        ///     数量：    配件入库计划清单.检验量										
        /// </summary>
        [DataMemberAttribute]
        public int? InspectedQuantity {
            get;
            set;
        }
        /// <summary>
        ///     数量：    配件入库计划清单.检验量										
        /// </summary>
        [DataMemberAttribute]
        public int PlannedAmount {
            get;
            set;
        }
        /// <summary>
        ///    单价：    配件入库计划清单.价格									
        /// </summary>
        [DataMemberAttribute]
        public decimal Price {
            get;
            set;
        }
        /// <summary>
        ///   备注：    配件入库计划清单.备注								
        /// </summary>
        [DataMemberAttribute]
        public string Remark {
            get;
            set;
        }


        /// <summary>
        ///    货位编号：库区库位.编号（配件库存.库位id=配件库区库位.id，库区库位.库区类型=保管区，配件库存.仓库Id=入库计划单.仓库Id,配件库存.配件id=入库计划单清单.配件id,如果取到多条，取min(库区库位.编号)）									
        /// </summary>
        [DataMemberAttribute]
        public string WarehouseAreaCode {
            get;
            set;
        }
         [DataMemberAttribute]
        public string MeasureUnit
        {
            get;
            set;

        }
         [DataMemberAttribute]
         public string SpareOrderRemark
         {
             get;
             set;

         }
        [DataMemberAttribute]
         public int PartsInboundPlanId
         {
             get;
             set;
         }
    }
}
