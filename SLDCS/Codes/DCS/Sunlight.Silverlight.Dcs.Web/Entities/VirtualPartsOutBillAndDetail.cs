﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class VirtualPartsOutBillAndDetail : EntityObject {
        /// <summary>
        /// 清单id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }

        /// <summary>
        /// 主单ID
        /// </summary>
        [DataMemberAttribute]
        public int PartsOutboundBillId {
            get;
            set;
        }

        /// <summary>
        /// 配件出库单.仓储企业名称
        /// </summary>
        public string StorageCompanyName {
            get;
            set;
        }

        /// <summary>
        /// 配件出库单.收货单位名称
        /// </summary>
        public string ReceivingCompanyName {
            get;
            set;
        }
        /// <summary>
        /// 配件出库单.销售订单类型名称
        /// </summary>
        public string PartsSalesOrderTypeName {
            get;
            set;
        }
        /// <summary>
        /// 配件出库单.原始需求单据编号
        /// </summary>
        public string OriginalRequirementBillCode {
            get;
            set;
        }
        /// <summary>
        /// 配件出库单.订单审批意见
        /// </summary>
        public string OrderApproveComment {
            get;
            set;
        }
        /// <summary>
        /// 配件出库单.原始需求单据类型
        /// </summary>
        public int OriginalRequirementBillType {
            get;
            set;
        }
        /// <summary>
        /// 配件出库单.发运方式
        /// </summary>
        public int? ShippingMethod {
            get;
            set;
        }
        /// <summary>
        ///出库类型：配件出库单.出库类型
        /// </summary>
        public int OutboundType {
            get;
            set;
        }

        /// <summary>
        /// 出库单号：配件出库单.配件出库单编号
        /// </summary>
        public string Code {
            get;
            set;
        }

        /// <summary>
        /// 出库仓库：配件出库单.配件仓库名称
        /// </summary>
        public string WarehouseName {
            get;
            set;
        }

        /// <summary>
        /// 出库库区：配件出库单清单.配件库位Id 所属库区.库区库位编号（库区库位.id=配件出库单清单.库位id）
        /// </summary>
        public string WarehouseArea {
            get;
            set;
        }

        /// <summary>
        ///货位号：配件出库单清单.库位编号）
        /// </summary>
        public string WarehouseAreaCode {
            get;
            set;
        }

        /// <summary>
        /// 营销分公司名称：配件出库单.营销分公司名称
        /// </summary>
        public string BranchName {
            get;
            set;
        }

        /// <summary>
        /// 配件出库单.备注
        /// </summary>
        public string Remark {
            get;
            set;
        }

        /// <summary>
        /// 配件图号：配件出库单清单.配件图号
        /// </summary>
        public string SparePartCode {
            get;
            set;
        }

        /// <summary>
        /// 配件名称：配件出库单清单.配件名称
        /// </summary>
        public string SparePartName {
            get;
            set;
        }

        /// <summary>
        /// 单位：配件信息.计量单位（配件出库单清单.配件Id=配件信息.Id）
        /// </summary>
        public string MeasureUnit {
            get;
            set;
        }

        /// <summary>
        /// 批次号：配件出库单清单.批次号
        /// </summary>
        public string BatchNumber {
            get;
            set;
        }

        /// <summary>
        /// 实出数:配件出库单清单.出库数量
        /// </summary>
        public int OutboundAmount {
            get;
            set;
        }

        /// <summary>
        /// 价格：配件出库单清单.结算价
        /// </summary>
        public decimal SettlementPrice {
            get;
            set;
        }

        /// <summary>
        /// 制单时间：配件出库单.创建时间
        /// </summary>
        public DateTime? CreateTime {
            get;
            set;
        }
        /// <summary>
        /// 制单人：配件出库单.创建人
        /// </summary>
        public string CreatorName {
            get;
            set;
        }
    }
}
