﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;
namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class VirtualPartsOutboundBillDetail : EntityObject {
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SparePartCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SparePartName {
            get;
            set;
        }
        [DataMemberAttribute]
        public string MeasureUnit {
            get;
            set;
        }
        [DataMemberAttribute]
        public string WarehouseAreaCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public int OutboundAmount {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal SettlementPrice {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Remark {
            get;
            set;
        }
        [DataMemberAttribute]
        public int PartsOutboundBillId {
            get;
            set;
        }

        [DataMemberAttribute]
        public int SparePartId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string BatchNumber {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal? OriginalPrice {
            get;
            set;
        }
        [DataMemberAttribute]
        public int WarehouseId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string WarehouseCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string WarehouseName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int PartsSalesOrderId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartsSalesOrderCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? SubmitTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public int PartsSalesOrderTypeId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartsSalesOrderTypeName {
            get;
            set;
        }
        [DataMemberAttribute]
        public bool? IfDirectProvision {
            get;
            set;
        }
        [DataMemberAttribute]
        public int SubmitCompanyId {
            get;
            set;
        }
        [DataMemberAttribute]
        public int SalesUnitId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ERPSourceOrderCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? CreateTime {
            get;
            set;
        }
        /// <summary>
        /// 可退货数量
        /// </summary>
        [DataMemberAttribute]
        public int ReturnedQuantity {
            get;
            set;
        }

        [DataMemberAttribute]
        public DateTime? PartsSalesOrderSubmitTime {
            get;
            set;
        }
        /// <summary>
        /// 追溯属性
        /// </summary>
        [DataMemberAttribute]
        public int? TraceProperty {
            get;
            set;
        }
    }
}
