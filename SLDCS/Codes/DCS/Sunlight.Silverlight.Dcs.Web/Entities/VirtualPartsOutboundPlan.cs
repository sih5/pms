﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;


namespace Sunlight.Silverlight.Dcs.Web
{/// <summary>
    /// 虚拟出库计划
    /// </summary>
    [DataContract(IsReference = true)]
    public class VirtualPartsOutboundPlan : EntityObject
    {

        [Key]
        [DataMemberAttribute]
        public int Id
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Code
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int WarehouseId
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string WarehouseCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string WarehouseName
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int StorageCompanyId
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string StorageCompanyCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string StorageCompanyName
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int StorageCompanyType
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int BranchId
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string BranchCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string BranchName
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int PartsSalesCategoryId
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartsSalesCategoryName
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int CounterpartCompanyId
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CounterpartCompanyCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CounterpartCompanyName
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int SourceId
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SourceCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int Status
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int OutboundType
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal TotalAmountForWarehouse
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal EcommerceMoney
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? CreateTime
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string OriginalRequirementBillCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ReceivingWarehouseCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ReceivingWarehouseName
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string OrderApproveComment
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Remark
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string StopComment
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CreatorName
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ModifierName
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? ModifyTime
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Stoper
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? StopTime
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ReceivingAddress
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? ShippingMethod
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartsSalesOrderTypeName
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SparePartCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SparePartName
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int OriginalRequirementBillType
        {
            get;
            set;
        }
         [DataMemberAttribute]
        public int? PartsSalesOrderTypeId
        {
            get;
            set;
        }
         [DataMemberAttribute]
         public int? TotalCount {
             get;
             set;
         }
         [DataMemberAttribute] 
        public string PartsShippingOrderCode 
        { 
            get; 
            set; 
        }
         [DataMemberAttribute] 
         public bool? IsInternalAllocationBill {
             get;
             set;
         }
         [DataMemberAttribute]
         public bool? IsTurn {
             get;
             set;
         }
         [DataMemberAttribute]
        public int? ReceivingWarehouseId{
             get;
             set;
        }
         [DataMemberAttribute]
         public double? ToWeight
         {
             get;
             set;
         }
         [DataMemberAttribute]
         public double? ToVolume
         {
             get;
             set;
         }
    }
}
