﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web.Entities
{
    [DataContract(IsReference = true)]
    public class VirtualPartsOutboundPlanDetail : EntityObject
    {
        [Key]
        [DataMemberAttribute]
        public int Id
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int PartsOutboundPlanId
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int SparePartId
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SparePartCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SparePartName
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int PlannedAmount
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int OutboundFulfillment
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal Price
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Remark
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string MeasureUnit
        {
            get;
            set;
        }
    }
}
