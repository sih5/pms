﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 虚拟配件价格查询
    /// </summary>
    [DataContract(IsReference = true)]
    public class VirtualPartsPriceHistory : EntityObject {


        /// <summary>
        ///分公司
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int BranchId {
            get;
            set;
        }
        /// <summary>
        /// 分公司编号
        /// </summary>
        [DataMemberAttribute]
        public string BranchCode {
            get;
            set;
        }
        /// <summary>
        ///分公司名称
        /// </summary>
        [DataMemberAttribute]
        public string BranchName {
            get;
            set;
        }
        /// <summary>
        ///品牌ID
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int PartsSalesCategoryId {
            get;
            set;
        }
        /// <summary>
        ///品牌
        /// </summary>
        [DataMemberAttribute]
        public string PartsSalesCategoryName {
            get;
            set;
        }
        /// <summary>
        /// 仓库ID
        /// </summary>
       // [Key]
        [DataMemberAttribute]
        public int? WarehouseId {
            get;
            set;
        }
        /// <summary>
        /// 仓库编号
        /// </summary>
        [DataMemberAttribute]
        public string WarehouseCode {
            get;
            set;
        }

        /// <summary>
        /// 仓库名称
        /// </summary>
        [DataMemberAttribute]
        public string WarehouseName {
            get;
            set;
        }

        /// <summary>
        /// 计量单位
        /// </summary>
        [DataMemberAttribute]
        public string  Measureunit {
            get;
            set;
        }
        /// <summary>
        /// 互换号
        /// </summary>
        [DataMemberAttribute]
        public string ExchangeCode {
            get;
            set;
        }
        /// <summary>
        /// 互换号
        /// </summary>
        [DataMemberAttribute]
        public string Exchangename {
            get;
            set;
        }

        /// <summary>
        /// 配件编号
        /// </summary>
        [DataMemberAttribute]
        public string PartCode {
            get;
            set;
        }

        /// <summary>
        /// 配件Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int PartId {
            get;
            set;
        }
        /// <summary>
        /// 配件名称
        /// </summary>
        [DataMemberAttribute]
        public string PartName {
            get;
            set;
        }
        /// <summary>
        /// 配件所属分类
        /// </summary>
        [DataMemberAttribute]
        public int? PartsAttribution {
            get;
            set;
        }

        /// <summary>
        /// 批发价为0
        /// </summary>
        [DataMemberAttribute]
        public int? IsZeroSalesPrice {
            get;
            set;
        }

        /// <summary>
        /// 零售价为0
        /// </summary>
        [DataMemberAttribute]
        public int? IsZeroRetailPrice {
            get;
            set;
        }
        /// <summary>
        /// 计划价为0
        /// </summary>
        [DataMemberAttribute]
        public int? IsZeroPlannedPrice {
            get;
            set;
        }
        /// <summary>
        /// 采购价为0
        /// </summary>
        [DataMemberAttribute]
        public int? IsZeroPurchasePrice {
            get;
            set;
        }

        /// <summary>
        /// 计划价创建时间
        /// </summary>   
        [DataMemberAttribute]
        public DateTime? Pcreatetime {
            get;
            set;
        }
        /// <summary>
        /// 销售价创建时间
        /// </summary>   
        [DataMemberAttribute]
        public DateTime? Screatetime {
            get;
            set;
        }
        /// <summary>
        /// 配件创建时间
        /// </summary>   
        [DataMemberAttribute]
        public DateTime? Createtime {
            get;
            set;
        }
        /// <summary>
        /// 销售价创建时间
        /// </summary>   
        [DataMemberAttribute]
        public DateTime? SalePriceCreateTime {
            get;
            set;
        }
        /// <summary>
        /// 供应商编号
        /// </summary>  
        //[Key]
        [DataMemberAttribute]
        public int? SupplierId {
            get;
            set;
        }
        /// <summary>
        /// 供应商编号
        /// </summary>   
        [DataMemberAttribute]
        public string SupplierCode {
            get;
            set;
        }
        /// <summary>
        /// 供应商名称
        /// </summary>
        [DataMemberAttribute]
        public string SupplierName {
            get;
            set;
        }


        /// <summary>
        /// 是否可采购
        /// </summary>
        [DataMemberAttribute]
        public Boolean? IsOrderable {
            get;
            set;
        }
        /// <summary>
        /// 是否可销售
        /// </summary>
        [DataMemberAttribute]
        public Boolean? IsSalable {
            get;
            set;
        }

        /// <summary>
        /// 采购价
        /// </summary>   
        [DataMemberAttribute]
        public decimal? PurchasePrice {
            get;
            set;
        }
        /// <summary>
        /// 批发价
        /// </summary>   
        [DataMemberAttribute]
        public decimal? SalesPrice {
            get;
            set;
        }
        /// <summary>
        /// 零售指导价
        /// </summary>   
        [DataMemberAttribute]
        public decimal? RetailGuidePrice {
            get;
            set;
        }
        /// <summary>
        /// 服务站批发价
        /// </summary>   
        [DataMemberAttribute]
        public decimal? Dealersalesprice {
            get;
            set;
        }
        /// <summary>
        /// 计划价
        /// </summary>   
        [DataMemberAttribute]
        public decimal? PlannedPrice {
            get;
            set;
        }
        /// <summary>
        /// Id
        /// </summary>  
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }
    }
}