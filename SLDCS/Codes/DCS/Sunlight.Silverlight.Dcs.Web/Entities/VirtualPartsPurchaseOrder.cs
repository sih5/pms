﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 查询旧件库存对应索赔单状态
    /// </summary>
    [DataContract(IsReference = true)]
    public class VirtualPartsPurchaseOrder : EntityObject {
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }

        [DataMemberAttribute]
        public string HWPurOrderCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string Code {
            get;
            set;
        }

        [DataMemberAttribute]
        public string BranchName {
            get;
            set;
        }

        [DataMemberAttribute]
        public string BranchCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string WarehouseName {
            get;

            set;
        }

        [DataMemberAttribute]
        public string PartsSalesCategoryName {
            get;
            set;
        }

        [DataMemberAttribute]
        public int PartsSupplierId
        {
            get;
            set;
        }

        [DataMemberAttribute]
        public string PartsSupplierCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string PartsSupplierName {
            get;
            set;
        }

        [DataMemberAttribute]
        public string PartsPurchaseOrderTypeName {
            get;
            set;
        }

        [DataMemberAttribute]
        public string ReceivingAddress {
            get;
            set;
        }

        [DataMemberAttribute]
        public bool IfDirectProvision {
            get;
            set;
        }

        [DataMemberAttribute]
        public string OriginalRequirementBillCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string ReceivingCompanyName {
            get;
            set;
        }

        [DataMemberAttribute]
        public decimal TotalAmount {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? ShippingMethod {
            get;
            set;
        }

        [DataMemberAttribute]
        public int Status {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? InStatus {
            get;
            set;
        }

        [DataMemberAttribute]
        public DateTime RequestedDeliveryTime {
            get;
            set;
        }

        [DataMemberAttribute]
        public string PlanSource {
            get;
            set;
        }

        [DataMemberAttribute]
        public string GPMSPurOrderCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string SAPPurchasePlanCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string AbandonOrStopReason {
            get;
            set;
        }

        [DataMemberAttribute]
        public string ConfirmationRemark {
            get;
            set;
        }

        [DataMemberAttribute]
        public string Remark {
            get;
            set;
        }

        [DataMemberAttribute]
        public string CreatorName {
            get;
            set;
        }

        [DataMemberAttribute]
        public DateTime? CreateTime {
            get;
            set;
        }

        [DataMemberAttribute]
        public string BusinessCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public int PartsSalesCategoryId {
            get;
            set;
        }

        [DataMemberAttribute]
        public int WarehouseId {
            get;
            set;
        }

        [DataMemberAttribute]
        public int PartsPurchaseOrderTypeId {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? OriginalRequirementBillType {
            get;
            set;
        }

        [DataMemberAttribute]
        public int BranchId {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? OriginalRequirementBillId {
            get;
            set;
        }

        [DataMemberAttribute]
        public string ModifierName {
            get;
            set;
        }

        [DataMemberAttribute]
        public DateTime? ModifyTime {
            get;
            set;
        }

        [DataMemberAttribute]
        public string AbandonerName {
            get;
            set;
        }

        [DataMemberAttribute]
        public DateTime? AbandonTime {
            get;
            set;
        }

        [DataMemberAttribute]
        public string SubmitterName {
            get;
            set;
        }

        [DataMemberAttribute]
        public DateTime? SubmitTime {
            get;
            set;
        }

        [DataMemberAttribute]
        public string ApproverName {
            get;
            set;
        }

        [DataMemberAttribute]
        public DateTime? ApproveTime {
            get;
            set;
        }

        [DataMemberAttribute]
        public string CloserName {
            get;
            set;
        }

        [DataMemberAttribute]
        public string ERPSourceOrderCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public DateTime? CloseTime {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? PurchaseOrderId {
            get {
                if(this.BranchCode == "2450" && this.PartsSupplierCode == "FT010063")
                    return this.Id;
                else {
                    return null;
                }
            }
        }

        [DataMemberAttribute]
        public bool? IfInnerDirectProvision {
            get;
            set;
        }

        [DataMemberAttribute]
        public string CPPartsPurchaseOrderCode
        {
            get;
            set;
        }

        [DataMemberAttribute]
        public string CPPartsInboundCheckCode
        {
            get;
            set;
        }
        //供应商联系人
        [DataMemberAttribute]
        public string Contact
        {
            get;
            set;
        }
        //供应商联系人手机
        [DataMemberAttribute]
        public string PhoneNumber
        {
            get;
            set;
        }
        //创建人.人员信息.联系电话
        [DataMemberAttribute]
        public string CellNumber
        {
            get;
            set;
        }
        //仓库.联系人
        [DataMemberAttribute]
        public string WarehouseContact
        {
            get;
            set;
        }
        //仓库.联系人电话
        [DataMemberAttribute]
        public string WarehousePhoneNumber
        {
            get;
            set;
        }
        //接口状态
        [DataMemberAttribute]
        public int? IoStatus
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public bool? IsTransSap 
        { 
            get;
            set;
        }
        //接口状态
        [DataMemberAttribute]
        public string Message
        {
            get;
            set;
        }
         [DataMemberAttribute]
        public string SparePartCode {
            get;
            set;
        }
         [DataMemberAttribute]
         public bool? IsPack {
             get;
             set;
         }
    }
}
