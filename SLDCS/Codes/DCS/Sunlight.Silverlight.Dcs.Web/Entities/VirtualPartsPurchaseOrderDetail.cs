﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 虚拟配件采购清单
    /// </summary>
    [DataContract(IsReference = true)]
    public class VirtualPartsPurchaseOrderDetail {
        [Key]
        [DataMemberAttribute]
        public int SparePartId {
            get;
            set;
        }

        /// <summary>
        ///  配件编号
        /// </summary>
        [DataMemberAttribute]
        public string SparePartCode {
            get;
            set;
        }

        /// <summary>
        ///  配件名称
        /// </summary>
        [DataMemberAttribute]
        public string SparePartName {
            get;
            set;
        }
        /// <summary>
        /// 采购订单编号
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public string PartsPurchaseOrderCode {
            get;
            set;
        }

        /// <summary>
        /// 采购订单状态
        /// </summary>
        [DataMemberAttribute]
        public int PartsPurchaseOrderStatus {
            get;
            set;
        }

        /// <summary>
        ///  未入库数量
        /// </summary>
        [DataMemberAttribute]
        public int UnInboundAmount {
            get;
            set;
        }

        /// <summary>
        ///  发运数量
        /// </summary>
        [DataMemberAttribute]
        public int ShippingAmount {
            get;
            set;
        }

        /// <summary>
        ///  采购数量
        /// </summary>
        [DataMemberAttribute]
        public int OrderAmount {
            get;
            set;
        }

        /// <summary>
        ///  确认数量
        /// </summary>
        [DataMemberAttribute]
        public int ConfirmAmount {
            get;
            set;
        }
        /// <summary>
        /// 单位
        /// </summary>
        [DataMemberAttribute]
        public string MeasureUnit
        {
            get;
            set;
        }
        /// <summary>
        /// 创建人
        /// </summary>
        [DataMemberAttribute]
        public string CreatorName
        {
            get;
            set;
        }
        /// <summary>
        /// 创建时间
        /// </summary>
        [DataMemberAttribute]
        public DateTime? CreateTime
        {
            get;
            set;
        }
        /// <summary>
        /// 采购订单类型
        /// </summary>
        [DataMemberAttribute]
        public int PartsPurchaseOrderTypeId
        {
            get;
            set;
        }
        /// <summary>
        /// 采购订单类型
        /// </summary>
        [DataMemberAttribute]
        public string PartsPurchaseOrderTypeName
        {
            get;
            set;
        }
        /// <summary>
        ///  本次发运量
        /// </summary>
        [DataMemberAttribute]
        public int UnShippingAmount
        {
            get;
            set;
        }
        /// <summary>
        ///  是否直供
        /// </summary>
        [DataMemberAttribute]
        public bool IfDirectProvision
        {
            get;
            set;
        }
        /// <summary>
        /// 配件与供应商关系.供应商图号
        /// </summary>
        [DataMemberAttribute]
        public string SupplierPartCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Remark
        {
            get;
            set;
        }
         [DataMemberAttribute]
        public decimal UnitPrice
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int WarehouseId
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int MInPackingAmount
        {
            get;
            set;
        }
        /// <summary>
        /// 配件供应商编号
        /// </summary>
        [DataMemberAttribute]
        public string PartsSupplierCode {
            get;
            set;
        }
        //追溯属性
        [DataMemberAttribute]
        public int? TraceProperty {
            get;
            set;
        }
    }
}
