﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 查询采购计划单信息
    /// </summary>
    [DataContract(IsReference = true)]
    public class VirtualPartsPurchasePlan : EntityObject {
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }

        [DataMemberAttribute]
        public string Code {
            get;
            set;
        }

        [DataMemberAttribute]
        public int BranchId
        {
            get;
            set;
        }

        [DataMemberAttribute]
        public string BranchCode
        {
            get;
            set;
        }

        [DataMemberAttribute]
        public string BranchName {
            get;
            set;
        }

        [DataMemberAttribute]
        public int WarehouseId
        {
            get;

            set;
        }

        [DataMemberAttribute]
        public string WarehouseCode
        {
            get;

            set;
        }

        [DataMemberAttribute]
        public string WarehouseName
        {
            get;

            set;
        }

        [DataMemberAttribute]
        public int PartsPlanTypeId
        {
            get;

            set;
        }

        [DataMemberAttribute]
        public int Status
        {
            get;

            set;
        }

        [DataMemberAttribute]
        public string Memo
        {
            get;

            set;
        }

        [DataMemberAttribute]
        public int? CreatorId
        {
            get;

            set;
        }

        [DataMemberAttribute]
        public string CreatorName
        {
            get;

            set;
        }

        [DataMemberAttribute]
        public DateTime? CreateTime
        {
            get;

            set;
        }

        [DataMemberAttribute]
        public int? ModifierId
        {
            get;

            set;
        }

        [DataMemberAttribute]
        public string ModifierName
        {
            get;

            set;
        }

        [DataMemberAttribute]
        public DateTime? ModifyTime
        {
            get;

            set;
        }

        [DataMemberAttribute]
        public int? AbandonerId
        {
            get;

            set;
        }

        [DataMemberAttribute]
        public string AbandonerName
        {
            get;

            set;
        }

        [DataMemberAttribute]
        public DateTime? AbandonTime
        {
            get;

            set;
        }

        [DataMemberAttribute]
        public int? SubmitterId
        {
            get;

            set;
        }

        [DataMemberAttribute]
        public string SubmitterName
        {
            get;

            set;
        }

        [DataMemberAttribute]
        public DateTime? SubmitTime
        {
            get;

            set;
        }

        [DataMemberAttribute]
        public int? ApproverId
        {
            get;

            set;
        }

        [DataMemberAttribute]
        public string ApproverName
        {
            get;

            set;
        }

        [DataMemberAttribute]
        public DateTime? ApproveTime
        {
            get;

            set;
        }

        [DataMemberAttribute]
        public int? CloserId
        {
            get;

            set;
        }

        [DataMemberAttribute]
        public string CloserName
        {
            get;

            set;
        }

        [DataMemberAttribute]
        public DateTime? CloseTime
        {
            get;

            set;
        }

        [DataMemberAttribute]
        public int? PartsSalesCategoryId
        {
            get;

            set;
        }

        [DataMemberAttribute]
        public string PartsSalesCategoryName
        {
            get;
            set;
        }

        [DataMemberAttribute]
        public string ApproveMemo
        {
            get;
            set;
        }

        [DataMemberAttribute]
        public string CloseMemo
        {
            get;
            set;
        }

        [DataMemberAttribute]
        public string PlanTypeName
        {
            get;
            set;
        }
    }
}
