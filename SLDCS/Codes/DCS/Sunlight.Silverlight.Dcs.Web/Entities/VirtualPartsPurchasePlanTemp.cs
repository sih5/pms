﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web{
    [DataContract(IsReference = true)]
    public class VirtualPartsPurchasePlanTemp : EntityObject {
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }
        [DataMemberAttribute]
        public int PartsPurchaseOrderId {
            get;
            set;
        }

        [DataMemberAttribute]
        public string PartsPurchaseOrderCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public int WarehouseId {
            get;
            set;
        }

        [DataMemberAttribute]
        public string WarehouseName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int PackSparePartId {
            get;
            set;
        }

        [DataMemberAttribute]
        public string PackSparePartCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PackSparePartName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? PackPlanQty {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? PackNum {
            get;
            set;
        }
        [DataMemberAttribute]
        public int SparePartId {
            get;
            set;
        }

        [DataMemberAttribute]
        public string SparePartCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SparePartName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? OrderAmount {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? Status {
            get;
            set;
        }
        [DataMemberAttribute]
        public int PartsPurchaseOrderStatus {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? ModifyTime {
            get;
            set;
        }
    }
}
