﻿using System.Data.Objects.DataClasses;
using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {

    [DataContract(IsReference = true)]
    public class VirtualPartsPurchasePricing : EntityObject
    {
        [Key]
        [DataMemberAttribute]
        public int Id
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int BranchId
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string BranchCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int PartId
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartName
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartReferenceCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string BranchName
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int PartsSupplierId
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartsSupplierCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartsSupplierName
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int PartsSalesCategoryId
        {
            get;
            set;
        }
        
        [DataMemberAttribute]
        public string PartsSalesCategoryName
        {
            get;
            set;
        }

        [DataMemberAttribute]
        public DateTime? ValidFrom
        {
            get;
            set;
        }

        [DataMemberAttribute]
        public DateTime? ValidTo
        {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? PriceType
        {
            get;
            set;
        }
         [DataMemberAttribute]
        public decimal? PurchasePrice {
            get;
            set;
        }
         [DataMemberAttribute]
        public int Status {
            get;
            set;
        }
         [DataMemberAttribute]
         public int CreatorId
         {
             get;
             set;
         }
         [DataMemberAttribute]
         public string CreatorName
         {
             get;
             set;
         }
         [DataMemberAttribute]
         public DateTime? CreateTime
         {
             get;
             set;
         }
         [DataMemberAttribute]
         public int? ModifierId
         {
             get;
             set;
         }
         [DataMemberAttribute]
         public string ModifierName
         {
             get;
             set;
         }
         [DataMemberAttribute]
         public DateTime? ModifyTime
         {
             get;
             set;
         }
         [DataMemberAttribute]
         public int? AbandonerId
         {
             get;
             set;
         }
         [DataMemberAttribute]
         public string AbandonerName
         {
             get;
             set;
         }
         [DataMemberAttribute]
         public DateTime? AbandonTime
         {
             get;
             set;
         }
         [DataMemberAttribute]
         public string Remark
         {
             get;
             set;
         }
         [DataMemberAttribute]
         public string OverseasPartsFigure
         {
             get;
             set;
         }
         [DataMemberAttribute]
         public int? LimitQty
         {
             get;
             set;
         }
         [DataMemberAttribute]
         public int? UsedQty
         {
             get;
             set;
         }
        /// <summary>
        /// 是否首选供应商
        /// </summary>
        [DataMemberAttribute]
        public bool? IsPrimary
        {
            get;
            set;
        }
        /// <summary>
        /// 红岩号
        /// </summary>
        [DataMemberAttribute]
        public string ReferenceCode
        {
            get;
            set;
        }
        /// <summary>
        /// 供应商图号
        /// </summary>
        [DataMemberAttribute]
        public string SupplierPartCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? DataSource
        {
            get;
            set;
        }
    }
}
