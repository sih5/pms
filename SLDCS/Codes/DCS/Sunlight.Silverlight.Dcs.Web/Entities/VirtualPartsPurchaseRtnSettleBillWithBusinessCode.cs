﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 虚拟配件采购退货结算成本
    /// </summary>
    [DataContract(IsReference = true)]
    public class VirtualPartsPurchaseRtnSettleBillWithBusinessCode : EntityObject {
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Code {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? WarehouseId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string WarehouseCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string WarehouseName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int BranchId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string BranchCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string BranchName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? PartsSalesCategoryId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartsSalesCategoryName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int PartsSupplierId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartsSupplierCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartsSupplierName {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal TotalSettlementAmount {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? OffsettedSettlementBillId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string OffsettedSettlementBillCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal InvoiceAmountDifference {
            get;
            set;
        }
        [DataMemberAttribute]
        public double TaxRate {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal Tax {
            get;
            set;
        }
        [DataMemberAttribute]
        public int InvoicePath {
            get;
            set;
        }
        [DataMemberAttribute]
        public int Status {
            get;
            set;
        }
        [DataMemberAttribute]
        public int SettlementPath {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Remark {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? CreatorId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CreatorName {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? CreateTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? ModifierId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ModifierName {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? ModifyTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? AbandonerId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string AbandonerName {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? AbandonTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? ApproverId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ApproverName {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? ApproveTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public string BusinessCode {
            get;
            set;
        }
        /// <summary>
        /// 开票总金额(汇总采购结算单对应的发票的发票金额)
        /// </summary>
        [DataMemberAttribute]
        public decimal InvoiceTotalAmount {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? InvoiceDate
        {
            get;
            set;
        }
        //含税总金额=不含税（结算总金额）+税额
        [DataMemberAttribute]
        public decimal TotalAmount
        {
            get;
            set;
        } 

    }
}
