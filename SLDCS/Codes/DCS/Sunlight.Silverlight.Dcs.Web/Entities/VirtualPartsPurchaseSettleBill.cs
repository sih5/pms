﻿using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 虚拟配件采购结算成本
    /// </summary>
    [DataContract(IsReference = true)]
    public class VirtualPartsPurchaseSettleBill : EntityObject {
        /// <summary>
        /// 采购结算单Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }

        /// <summary>
        /// 采购结算单编号
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public string Code {
            get;
            set;
        }

        /// <summary>
        /// 出入库单编号
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public string BillCode {
            get;
            set;
        }

        /// <summary>
        /// 出入库类型
        /// </summary>
        [DataMemberAttribute]
        public string BillBusinessType {
            get;
            set;
        }

        /// <summary>
        /// 配件Id
        /// </summary>
        [DataMemberAttribute]
        public int SparePartId {
            get;
            set;
        }

        /// <summary>
        ///  配件编号
        /// </summary>
        [Key]
        [DataMemberAttribute]
        [Display(Name = "VirtualPartsStock_SparePartCode", ResourceType = typeof(Resources.EntityStrings))]
        public string SparePartCode {
            get;
            set;
        }

        /// <summary>
        ///  配件名称
        /// </summary>
        [DataMemberAttribute]
        [Display(Name = "VirtualPartsStock_SparePartName", ResourceType = typeof(Resources.EntityStrings))]
        public string SparePartName {
            get;
            set;
        }

        /// <summary>
        ///  数量
        /// </summary>
        [DataMemberAttribute]
        public int Quantity {
            get;
            set;
        }

        /// <summary>
        /// 结算价
        /// </summary>
        [DataMemberAttribute]
        public decimal SettlementPrice {
            get;
            set;
        }

        /// <summary>
        /// 结算金额
        /// </summary>
        [DataMemberAttribute]
        public decimal SettlementAmount {
            get;
            set;
        }

        /// <summary>
        /// 计划价
        /// </summary>
        [DataMemberAttribute]
        public decimal PlanPrice {
            get;
            set;
        }

        /// <summary>
        /// 计划金额
        /// </summary>
        [DataMemberAttribute]
        public decimal PlanAmount {
            get;
            set;
        }

        /// <summary>
        /// 材料成本差异
        /// </summary>
        [DataMemberAttribute]
        public decimal MaterialCostVariance {
            get;
            set;
        }

    }
}
