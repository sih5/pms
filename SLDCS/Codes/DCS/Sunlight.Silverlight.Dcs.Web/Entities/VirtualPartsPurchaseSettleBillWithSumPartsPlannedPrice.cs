﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    [DataContractAttribute]
    public class VirtualPartsPurchaseSettleBillWithSumPlannedPrice : EntityObject {
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }

        [DataMemberAttribute]
        public string Code {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? WarehouseId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string WarehouseCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string WarehouseName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int BranchId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string BranchCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string BranchName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? PartsSalesCategoryId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartsSalesCategoryName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int PartsSupplierId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartsSupplierCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string BusinessCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string PartsSupplierName {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal TotalSettlementAmount {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? OffsettedSettlementBillId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string OffsettedSettlementBillCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public double TaxRate {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal Tax {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal InvoiceAmountDifference {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal TotalCostAmount {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal CostAmountDifference {
            get;
            set;
        }
        [DataMemberAttribute]
        public int Status {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? InvoiceFlag {
            get;
            set;
        }
        [DataMemberAttribute]
        public int SettlementPath {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Remark {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? CreatorId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CreatorName {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? CreateTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? ModifierId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ModifierName {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? ModifyTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? AbandonerId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string AbandonerName {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? AbandonTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? ApproverId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ApproverName {
            get;
            set;
        }

        [DataMemberAttribute]
        public DateTime? ApproveTime {
            get;
            set;
        }

        [DataMemberAttribute]
        public decimal SumPlannedPrice {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? InvoiceApproverId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string InvoiceApproverName {
            get;
            set;
        }

        [DataMemberAttribute]
        public DateTime? InvoiceApproveTime {
            get;
            set;
        }

        [DataMemberAttribute]
        public decimal InvoiceTotalAmount {
            get;
            set;
        }

        [DataMemberAttribute]
        public bool? IsUnifiedSettle
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? InvoiceDate
        {
            get;
            set;
        }
    }
}
