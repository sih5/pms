﻿using System;
using System.Data.Objects.DataClasses;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web
{
    [DataContract(IsReference = true)]
    public class VirtualPartsPurchaseSettleDetail : EntityObject
    {
        [Key]
        [DataMemberAttribute]
        public int Id
        {
            get;
            set;
        }

        [DataMemberAttribute]
        public int SerialNumber
        {
            get;
            set;
        }
       
        [DataMemberAttribute]
        public int PartsPurchaseSettleBillId
        {
            get;
            set;
        }

        [DataMemberAttribute]
        public int SparePartId

        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SparePartCode
        {
            get;
            set;
        }
       [DataMemberAttribute]
        public string SparePartName
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SupplierPartCode
        {
            get;
            set;
        }

        [DataMemberAttribute]
        public decimal SettlementPrice
        {
            get;
            set;
        }

        [DataMemberAttribute]
        public int QuantityToSettle
        {
            get;
            set;
        }


        [DataMemberAttribute]
        public decimal SettlementAmount
        {
            get;
            set;
        }

       [DataMemberAttribute]
        public string Remark
        {
            get;
            set;
        }
    }
}
