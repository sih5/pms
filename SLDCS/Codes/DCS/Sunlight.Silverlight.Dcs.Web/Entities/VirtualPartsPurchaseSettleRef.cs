﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class VirtualPartsPurchaseSettleRef : EntityObject {
        [Key]
        [DataMemberAttribute]
        public int SerialNumber {
            get;
            set;
        }

        [DataMemberAttribute]
        public int PartsPurchaseSettleBillId {
            get;
            set;
        }

        [DataMemberAttribute]
        public string SourceCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string OriginalRequirementBillCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public int SourceType {
            get;
            set;
        }
        [DataMemberAttribute]
        public string WarehouseName {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? SourceBillCreateTime {
            get;
            set;
        }
    }
}
