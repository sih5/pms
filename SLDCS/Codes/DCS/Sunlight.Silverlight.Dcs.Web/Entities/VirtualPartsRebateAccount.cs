﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 虚拟客户账户
    /// </summary>
    [DataContract(IsReference = true)]
    public class VirtualPartsRebateAccount : EntityObject {
        /// <summary>
        /// 配件返利变更明细Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }
        /// <summary>
        /// 营销分公司Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int BranchId {
            get;
            set;
        }
        /// <summary>
        /// 状态
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int Status {
            get;
            set;
        }
        /// <summary>
        /// 营销分公司编号
        /// </summary>
        [DataMemberAttribute]
        public string BranchName {
            get;
            set;
        }
        /// <summary>
        /// 账户组名称
        /// </summary>
        [DataMemberAttribute]
        public string AccountGroupName {
            get;
            set;
        }
        /// <summary>
        /// 源单据编号
        /// </summary>
        [DataMemberAttribute]
        public string SourceCode {
            get;
            set;
        }
        /// <summary>
        /// 客户名称
        /// </summary>
        [DataMemberAttribute]
        public string CompanyName {
            get;
            set;
        }
        /// <summary>
        /// 客户编号
        /// </summary>
        [DataMemberAttribute]
        public string CompanyCode {
            get;
            set;
        }
        /// <summary>
        /// 结存金额(H)
        /// </summary>
        [DataMemberAttribute]
        public decimal AccountBalanceAmount {
            get;
            set;
        }
        /// <summary>
        /// 账户组Id
        /// </summary>
        [DataMemberAttribute]
        public int? AccountGroupId {
            get;
            set;
        }
        /// <summary>
        /// 源单据类型
        /// </summary>
        [DataMemberAttribute]
        public int? SourceType {
            get;
            set;
        }
        /// <summary>
        /// 业务编码
        /// </summary>
        [DataMemberAttribute]
        public string BusinessCode {
            get;
            set;
        }
        /// <summary>
        ///  创建人
        /// </summary>
        [DataMemberAttribute]
        [Display(ResourceType = typeof(EntityStrings), Name = "_Common_CreatorName")]
        public string CreatorName {
            get;
            set;
        }
        /// <summary>
        ///  创建时间
        /// </summary>
        [DataMemberAttribute]
        [Display(ResourceType = typeof(EntityStrings), Name = "_Common_CreateTime")]
        public DateTime? CreateTime {
            get;
            set;
        }
        /// <summary>
        ///  修改人
        /// </summary>
        [DataMemberAttribute]
        [Display(ResourceType = typeof(EntityStrings), Name = "_Common_ModifierName")]
        public string ModifierName {
            get;
            set;
        }
        /// <summary>
        ///  修改时间
        /// </summary>
        [DataMemberAttribute]
        [Display(ResourceType = typeof(EntityStrings), Name = "_Common_ModifyTime")]
        public DateTime? ModifyTime {
            get;
            set;
        }

    }
}
