﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 虚拟客户账户
    /// </summary>
    [DataContract(IsReference = true)]
    public class VirtualPartsRebateApplication : EntityObject {
        /// <summary>
        /// 信用申请单Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }
        /// <summary>
        ///作废人Id
        /// </summary>
        [DataMemberAttribute]
        public int? AbandonerId {
            get;
            set;
        }
         /// <summary>
        ///销售类型Id
        /// </summary>
        [DataMemberAttribute]
        public int? PartsSalesCategoryId {
            get;
            set;
        }
        /// <summary>
        ///分公司Id
        /// </summary>
        [DataMemberAttribute]
        public int? BranchId {
            get;
            set;
        }
        /// <summary>
        /// 编号
        /// </summary>
        
        [DataMemberAttribute]
        public string  Code {
            get;
            set;
        }
        /// <summary>
        /// 担保企业Id
        /// </summary>
        
        [DataMemberAttribute]
        public int GuarantorCompanyId {
            get;
            set;
        }
        /// <summary>
        /// 状态
        /// </summary>
        
        [DataMemberAttribute]
        public int Status {
            get;
            set;
        }
        /// <summary>
        /// 使用条件
        /// </summary>
        
        [DataMemberAttribute]
        public int ApplyCondition {
            get;
            set;
        }
        /// <summary>
        /// 营销分公司编号
        /// </summary>
        [DataMemberAttribute]
        public string BranchName {
            get;
            set;
        }
        /// <summary>
        /// 信用额度
        /// </summary>
        [DataMemberAttribute]
        public decimal CredenceLimit {
            get;
            set;
        }
        /// <summary>
        /// 账户组名称
        /// </summary>
        [DataMemberAttribute]
        public string AccountGroupName {
            get;
            set;
        }
        /// <summary>
        /// 配件返利类型名称
        /// </summary>
        [DataMemberAttribute]
        public string PartsRebateTypeName {
            get;
            set;
        }
        /// <summary>
        /// 客户名称
        /// </summary>
        [DataMemberAttribute]
        public string CustomerCompanyCode {
            get;
            set;
        }
        /// <summary>
        /// 担保企业名称
        /// </summary>
        [DataMemberAttribute]
        public string PartsSalesCategoryName {
            get;
            set;
        }
        /// <summary>
        /// 客户编号
        /// </summary>
        [DataMemberAttribute]
        public string CustomerCompanyName {
            get;
            set;
        }
        /// <summary>
        /// 已使用金额(H)
        /// </summary>
        [DataMemberAttribute]
        public decimal Amount {
            get;
            set;
        }
        /// <summary>
        /// 账户组Id
        /// </summary>
        [DataMemberAttribute]
        public int? AccountGroupId {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? CustomerCompanyId {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? RebateDirection {
            get;
            set;
        }
        /// <summary>
        /// 申请原因
        /// </summary>
        [DataMemberAttribute]
        public string  Motive {
            get;
            set;
        }
        /// <summary>
        /// 业务编码
        /// </summary>
        [DataMemberAttribute]
        public string BusinessCode {
            get;
            set;
        }
        /// <summary>
        ///  创建人
        /// </summary>
        [DataMemberAttribute]
        [Display(ResourceType = typeof(EntityStrings), Name = "_Common_CreatorName")]
        public string CreatorName {
            get;
            set;
        }
        /// <summary>
        ///  创建时间
        /// </summary>
        [DataMemberAttribute]
        [Display(ResourceType = typeof(EntityStrings), Name = "_Common_CreateTime")]
        public DateTime? CreateTime {
            get;
            set;
        }
        /// <summary>
        ///  修改人
        /// </summary>
        [DataMemberAttribute]
        [Display(ResourceType = typeof(EntityStrings), Name = "_Common_ModifierName")]
        public string ModifierName {
            get;
            set;
        }
        /// <summary>
        ///  修改时间
        /// </summary>
        [DataMemberAttribute]
        [Display(ResourceType = typeof(EntityStrings), Name = "_Common_ModifyTime")]
        public DateTime? ModifyTime {
            get;
            set;
        }
        /// <summary>
        ///  审核时间
        /// </summary>
        [DataMemberAttribute]
        public DateTime? ApproveTime {
            get;
            set;
        }
        /// <summary>
        ///  审核人
        /// </summary>
        [DataMemberAttribute]
        public string ApproverName {
            get;
            set;
        }
        /// <summary>
        ///  备注
        /// </summary>
        [DataMemberAttribute]
        public string Remark {
            get;
            set;
        }
        /// <summary>
        ///  审批意见
        /// </summary>
        [DataMemberAttribute]
        public string ApprovalComment {
            get;
            set;
        }
        /// <summary>
        ///  作废时间
        /// </summary>
        [DataMemberAttribute]
        public DateTime? AbandonTime {
            get;
            set;
        }
        /// <summary>
        ///  作废人
        /// </summary>
        [DataMemberAttribute]
        public string AbandonerName {
            get;
            set;
        }
    }
}
