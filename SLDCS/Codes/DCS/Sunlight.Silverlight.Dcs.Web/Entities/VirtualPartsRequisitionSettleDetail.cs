﻿
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class VirtualPartsRequisitionSettleDetail : EntityObject {

        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }

        [DataMemberAttribute]
        public string SparePartCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string SparePartName {
            get;
            set;
        }

        [DataMemberAttribute]
        public int Quantity {
            get;
            set;
        }

        [DataMemberAttribute]
        public decimal SettlementPrice {
            get;
            set;
        }

        [DataMemberAttribute]
        public decimal CostPrice {
            get;
            set;
        }

        [DataMemberAttribute]
        public string SourceCode {
            get;
            set;
        }

    }
}