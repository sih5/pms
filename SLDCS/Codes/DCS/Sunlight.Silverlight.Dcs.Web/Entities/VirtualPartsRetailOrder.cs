﻿using System;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web
{
    [DataContract(IsReference = true)]
   public class VirtualPartsRetailOrder : EntityObject
    {
        /**
         * 
         **/
         [DataMemberAttribute]
        public int Id
        {
            get;
            set;
        }
        /**
         * 企业名称
         **/
         [DataMemberAttribute]
        public string CompanyName
        {
            get;
            set;
        }
         /**
          * 企业类型+出库单
          **/
         [DataMemberAttribute]
         public string OutBoundName
         {
             get;
             set;
         }
         /**
          * 零售单号
          **/
         [DataMemberAttribute]
         public string Code
         {
             get;
             set;
         }
         /**
          * 出库仓库
          **/
         [DataMemberAttribute]
         public string WarehouseName
         {
             get;
             set;
         }
        /**
          * 收货单位
          **/
         [DataMemberAttribute]
         public string CustomerUnit
         {
             get;
             set;
         }
        /**
          * 订单类型
          **/
        [DataMemberAttribute]
         public string OrderTypeName
         {
             get;
             set;
         }  
         /**
          * 出库时间
          **/
         [DataMemberAttribute]
         public DateTime? ApproveTime
         {
             get;
             set;
         }
    }
}
