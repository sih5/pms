﻿using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 配件销售加价率
    /// </summary>
    [DataContract(IsReference = true)]
    public class VirtualPartsSalePriceIncreaseRate : EntityObject {
        /// <summary>
        /// Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }

        [DataMemberAttribute]
        public int PartId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string GroupCode {
            get;
            set;
        }
    }
}
