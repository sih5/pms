﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class VirtualPartsSalesOrderOutboundPlanHw {
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }
        /// <summary>
        /// 单据编号
        /// </summary>
        [DataMemberAttribute]
        public int InterfaceId {
            get;
            set;
        }
        /// <summary>
        /// 单据编号
        /// </summary>
        [DataMemberAttribute]
        public string Code {
            get;
            set;
        }
        /// <summary>
        /// 同步时间
        /// </summary>
        [DataMemberAttribute]
        public DateTime? ModifyTime {
            get;
            set;
        }
        /// <summary>
        /// 错误信息
        /// </summary>
        [DataMemberAttribute]
        public string Message {
            get;
            set;
        }

        /// <summary>
        /// 状态
        /// </summary>
        [DataMemberAttribute]
        public int Status {
            get;
            set;
        }
        /// <summary>
        /// 单据Id
        /// </summary>
        [DataMemberAttribute]
        public int CodeId {
            get;
            set;
        }
        /// <summary>
        /// 状态
        /// </summary>
        [DataMemberAttribute]
        public string HandleStatus {
            get;
            set;
        }
    }
}
