﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 虚拟配件销售价格
    /// </summary>
    [DataContract(IsReference = true)]
    public class VirtualPartsSalesPrice : EntityObject {
        /// <summary>
        /// 配件Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int SparePartId {
            get;
            set;
        }
        [DataMemberAttribute]
        public int BranchId {
            get;
            set;
        }

        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartsSalesCategoryName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int PartsSalesCategoryId {
            get;
            set;
        }

        /// <summary>
        ///  价格类型
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int PriceType {
            get;
            set;
        }

        /// <summary>
        ///  配件编号
        /// </summary>
        [DataMemberAttribute]
        public string SparePartCode {
            get;
            set;
        }

        /// <summary>
        ///  配件名称
        /// </summary>
        [DataMemberAttribute]
        public string SparePartName {
            get;
            set;
        }

        /// <summary>
        ///  配件参考编号
        /// </summary>
        [DataMemberAttribute]
        public string ReferenceCode {
            get;
            set;
        }

        /// <summary>
        ///  配件参考名称
        /// </summary>
        [DataMemberAttribute]
        public string ReferenceName {
            get;
            set;
        }

        /// <summary>
        ///  配件特征说明
        /// </summary>
        [DataMemberAttribute]
        public string Feature {
            get;
            set;
        }
        [DataMemberAttribute]
        public int Status {
            get;
            set;
        }


        [DataMemberAttribute]
        public string CreatorName {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? CreateTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ModifierName {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? ModifyTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ApproverName {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? ApproveTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public string AbandonerName {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? AbandonTime {
            get;
            set;
        }

        /// <summary>
        ///  最小销售批量
        /// </summary>
        [DataMemberAttribute]
        public int? MinSaleQuantity {
            get;
            set;
        }

        /// <summary>
        ///  订单价格等级系数
        /// </summary>
        [DataMemberAttribute]
        public Double Coefficient {
            get;
            set;
        }

        /// <summary>
        ///  配件销售价
        /// </summary>
        [DataMemberAttribute]
        public Decimal PartsSalesPrice {
            get;
            set;
        }

        /// <summary>
        /// 协议价格
        /// </summary>
        [DataMemberAttribute]
        public Decimal? PartsTreatyPrice {
            get;
            set;
        }

        /// <summary>
        ///  价格
        /// </summary>
        [DataMemberAttribute]
        public Decimal Price {
            get;
            set;
        }
        /// <summary>
        ///  价格
        /// </summary>
        [DataMemberAttribute]
        public Decimal SalesPrice {
            get;
            set;
        }

        /// <summary>
        ///  价格
        /// </summary>
        [DataMemberAttribute]
        public Decimal DealerSalesPrice {
            get;
            set;
        }


        /// <summary>
        /// 计量单位
        /// </summary>
        [DataMemberAttribute]
        public string MeasureUnit {
            get;
            set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        [DataMemberAttribute]
        public string Remark {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal MaxExchangeSalePrice {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal PurchasePrice {
            get;
            set;
        }

        /// <summary>
        /// 中心库价
        /// </summary>
        [DataMemberAttribute]
        public decimal? CenterPrice {
            get;
            set;
        }

        /// <summary>
        /// 零售指导价
        /// </summary>
        [DataMemberAttribute]
        public decimal RetailGuidePrice {
            get;
            set;
        }

        /// <summary>
        /// 价格类型
        /// </summary>
        [DataMemberAttribute]
        public string PriceTypeName {
            get;
            set;
        }

        /// <summary>
        /// 配件ABC分类
        /// </summary>
        [DataMemberAttribute]
        public string ABCStrategy {
            get;
            set;
        }

        /// <summary>
        /// 配件最小包装数量
        /// </summary>
        [DataMemberAttribute]
        public int? MInPackingAmount {
            get;
            set;
        }
    }
}
