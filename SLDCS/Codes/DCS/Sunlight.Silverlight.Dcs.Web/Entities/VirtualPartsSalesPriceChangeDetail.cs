﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 虚拟配件销售价变更申请清单
    /// </summary>
    [DataContract(IsReference = true)]
    public class VirtualPartsSalesPriceChangeDetail : EntityObject {
        /// <summary>
        /// 配件Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int SparePartId {
            get;
            set;
        }

        /// <summary>
        /// 参考价
        /// </summary>
        [DataMemberAttribute]
        public Decimal ReferencePrice {
            get;
            set;
        }

        /// <summary>
        /// 采购价
        /// </summary>
        [DataMemberAttribute]
        public Decimal PurchasePrice {
            get;
            set;
        }
        /// <summary>
        /// 采购最高价格
        /// </summary>
        [DataMemberAttribute]
        public Decimal MaxPurchasePricing {
            get;
            set;
        }
        /// <summary>
        /// 互换件最高价格
        /// </summary>
        [DataMemberAttribute]
        public Decimal MaxExchangeSalePrice {
            get;
            set;
        }

        /// <summary>
        /// 销售价加价率
        /// </summary>
        [DataMemberAttribute]
        public Decimal IncreaseRateSale {
            get;
            set;
        }

        /// <summary>
        /// 零售价加价率
        /// </summary>
        [DataMemberAttribute]
        public Decimal IncreaseRateRetail {
            get;
            set;
        }

        /// <summary>
        /// 批发价加价率
        /// </summary>
        [DataMemberAttribute]
        public Decimal IncreaseRateDealerSalesPrice {
            get;
            set;
        }

        /// <summary>
        /// 原销售价格（配件销售价表中的销售价格）
        /// </summary>
        [DataMemberAttribute]
        public decimal PartsSalesPrice_SalePrice {
            get;
            set;
        }
        /// <summary>
        ///原零售指导价（配件零售指导价表中的 零售指导价）
        /// </summary>
        [DataMemberAttribute]
        public decimal PartsRetailGuidePrice_RetailGuidePrice {
            get;
            set;
        }
        /// <summary>
        /// 销售价浮动范围（上限）
        /// </summary>
        [DataMemberAttribute]
        public double? MaxSalesPriceFloating {

            get;
            set;
        }
        /// <summary>
        /// 销售价浮动范围（下限）
        /// </summary>
        [DataMemberAttribute]
        public double? MinSalesPriceFloating {

            get;
            set;
        }
        /// <summary>
        /// 零售价浮动范围（上限）
        /// </summary>
        [DataMemberAttribute]
        public double? MaxRetailOrderPriceFloating {

            get;
            set;
        }
        /// <summary>
        /// 零售价浮动范围（下限）
        /// </summary>
        [DataMemberAttribute]
        public double? MinRetailOrderPriceFloating {

            get;
            set;
        }
        /// <summary>
        /// 价格类型
        /// </summary>
        [DataMemberAttribute]
        public string PriceTypeName {

            get;
            set;
        }

    }
}
