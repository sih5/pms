﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 虚拟配件销售价格带库存
    /// </summary>
    [DataContract(IsReference = true)]
    public class VirtualPartsSalesPriceWithStock : EntityObject {
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }
        /// <summary>
        /// 仓库Id
        /// </summary>
        [DataMemberAttribute]
        public int WarehouseId
        {
            get;
            set;
        }

        /// <summary>
        /// 仓库编号
        /// </summary>
   
        [DataMemberAttribute]
        public string WarehouseCode {
            get;
            set;
        }

        /// <summary>
        /// 仓库名称
        /// </summary>
       
        [DataMemberAttribute]
        public string WarehouseName {
            get;
            set;
        }


        /// <summary>
        /// 代理库编号
        /// </summary>
        
        [DataMemberAttribute]
        public string AgencyCode {
            get;
            set;
        }

        /// <summary>
        /// 代理库名称
        /// </summary>
        
        [DataMemberAttribute]
        public string AgencyName {
            get;
            set;
        }

        /// <summary>
        /// 配件Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int SparePartId {
            get;
            set;
        }

        /// <summary>
        /// 配件编号
        /// </summary>
      
        [DataMemberAttribute]
        public string SparePartCode {
            get;
            set;
        }

        /// <summary>
        /// 配件名称
        /// </summary>
   
        [DataMemberAttribute]
        public string SparePartName {
            get;
            set;
        }

        /// <summary>
        /// 计量单位
        /// </summary>
        
        [DataMemberAttribute]
        public string MeasureUnit {
            get;
            set;
        }

        /// <summary>
        /// 研究院图号
        /// </summary>
      
        [DataMemberAttribute]
        public string AcademyCode {
            get;
            set;
        }

        /// <summary>
        /// 库存数量
        /// </summary>
       
        [DataMemberAttribute]
        public double StockQuantity
        {
            get;
            set;
        }

        /// <summary>
        /// 销售价
        /// </summary>
      
        [DataMemberAttribute]
        public decimal SalesPrice
        {
            get;
            set;
        }

        /// <summary>
        /// 特殊协议价
        /// </summary>
        
        [DataMemberAttribute]
        public decimal? SpecialTreatyPrice {
            get;
            set;
        }

        /// <summary>
        /// 服务站批发价
        /// </summary>
      
        [DataMemberAttribute]
        public decimal DealerSalesPrice {
            get;
            set;
        }

        /// <summary>
        /// 价格等级系数
        /// </summary>
       
        [DataMemberAttribute]
        public double PriceGradeCoefficient {
            get;
            set;
        }

        /// <summary>
        /// 价格
        /// </summary>
       
        [DataMemberAttribute]
        public decimal Price {
            get;
            set;
        }

        /// <summary>
        /// 仓库库存合计
        /// </summary>
        
        [DataMemberAttribute]
        public double? SumQuantity {
            get;
            set;
        }

        /// <summary>
        /// 仓库锁定库存合计
        /// </summary>
    
        [DataMemberAttribute]
        public double? SumLockedQuantity {
            get;
            set;
        }

        /// <summary>
        /// 仓库冻结库存合计
        /// </summary>
        
        [DataMemberAttribute]
        public double? SumCongelationStockQty {
            get;
            set;
        }

        /// <summary>
        /// 仓库不可用库存合计
        /// </summary>
      
        [DataMemberAttribute]
        public double? SumDisabledStock {
            get;
            set;
        }
    }
}
