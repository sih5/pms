﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web
{
    [DataContract(IsReference = true)]
    public class VirtualPartsSalesReturnBill : EntityObject
    {
        [Key]
        [DataMemberAttribute]
        public int ID
        {
            get;
            set;
        }

        [DataMemberAttribute]
        public string Code
        {
            get;
            set;
        }

        [DataMemberAttribute]
        public string ReturnCompanyName
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal TotalAmount
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SalesUnitName
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int ReturnType
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartsSalesOrderCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ReturnReason
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ContactPerson
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ContactPhone
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string WarehouseName
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int Status 
        { 
            get;
            set; 
        }
        [DataMemberAttribute]
        public string RejectComment
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string StopComment
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Remark
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? InvoiceRequirement
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string BlueInvoiceNumber
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CreatorName
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? CreateTime
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ApproverName
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? ApproveTime
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string RejecterName
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? RejectTime
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string AbandonerName
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? AbandonTime
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string StopName
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? StopTime
        {
            get;
            set;
        }

        [DataMemberAttribute]
        public decimal? ApproveAmount
        {
            get;
            set;
        }

        
    }
}
