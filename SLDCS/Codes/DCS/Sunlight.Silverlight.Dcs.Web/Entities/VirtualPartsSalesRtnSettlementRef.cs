﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Sunlight.Silverlight.Dcs.Web
{
    /// <summary>
    /// 销售退货结算关联单
    /// </summary>
    public partial class VirtualPartsSalesRtnSettlementRef
    {
        /// <summary>
        /// 计划总金额 
        /// </summary>
        [DataMemberAttribute]
        public decimal Id
        {
            get;
            set;
        }

        /// <summary>
        /// 销售退货结算单Id 
        /// </summary>
        [DataMemberAttribute]
        public int PartsSalesRtnSettlementId
        {
            get;
            set;
        }

        /// <summary>
        /// 入库单Id 
        /// </summary>
        [DataMemberAttribute]
        public int SourceId
        {
            get;
            set;
        }

        /// <summary>
        /// 入库单编号 
        /// </summary>
        [DataMemberAttribute]
        public string SourceCode
        {
            get;
            set;
        }

        /// <summary>
        /// 入库单创建时间 
        /// </summary>
        [DataMemberAttribute]
        public DateTime SourceBillCreateTime
        {
            get;
            set;
        }

        /// <summary>
        /// 仓库Id 
        /// </summary>
        [DataMemberAttribute]
        public int WarehouseId
        {
            get;
            set;
        }

        /// <summary>
        /// 仓库名称 
        /// </summary>
        [DataMemberAttribute]
        public string WarehouseName
        {
            get;
            set;
        }

        /// <summary>
        /// 结算金额 
        /// </summary>
        [DataMemberAttribute]
        public decimal SettlementAmount
        {
            get;
            set;
        }

        /// <summary>
        /// 计划金额 
        /// </summary>
        [DataMemberAttribute]
        public decimal CostPrice
        {
            get;
            set;
        }
    }
}
