﻿using System;
using System.Data.Objects.DataClasses;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web
{

    /// <summary>
    /// 用于配件上架单查询
    /// </summary>
    [DataContract(IsReference = true)]
    public class VirtualPartsShelvesTask : EntityObject
    {
        /// <summary>
        ///上架单id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int Id
        {
            get;
            set;
        }
        /// <summary>
        ///上架单号
        /// </summary>
        [DataMemberAttribute]
        public string Code
        {
            get;
            set;
        }

        /// <summary>
        ///包装任务单号
        /// </summary>
        [DataMemberAttribute]
        public string PackingTaskCode
        {
            get;
            set;
        }
        /// <summary>
        /// 入库计划单号
        /// </summary>
        [DataMemberAttribute]
        public string PartsInboundPlanCode
        {
            get;
            set;
        }

        /// <summary>
        /// 入库单号
        /// </summary>
        [DataMemberAttribute]
        public string PartsInboundCheckBillCode
        {
            get;
            set;
        }
        /// <summary>
        /// 仓库id
        /// </summary>
        [DataMemberAttribute]
        public int WarehouseId
        {
            get;
            set;
        }
        /// <summary>
        /// 仓库编号
        /// </summary>
        [DataMemberAttribute]
        public string WarehouseCode
        {
            get;
            set;
        }

        /// <summary>
        /// 仓库名称
        /// </summary>
        [DataMemberAttribute]
        public string WarehouseName
        {
            get;
            set;
        }
        /// <summary>
        /// 配件编号
        /// </summary>
        [DataMemberAttribute]
        public string SparePartCode
        {
            get;
            set;
        }

        /// <summary>
        ///配件名称
        /// </summary>
        [DataMemberAttribute]
        public string SparePartName
        {
            get;
            set;
        }

        /// <summary>
        ///批次编号
        /// </summary>
        [DataMemberAttribute]
        public string BatchNumber
        {
            get;
            set;
        }

        /// <summary>
        ///计划量
        /// </summary>
        [DataMemberAttribute]
        public int PlannedAmount
        {
            get;
            set;
        }
        /// <summary>
        ///已上架数量
        /// </summary>
        [DataMemberAttribute]
        public int ShelvesAmount
        {
            get;
            set;
        }
        /// <summary>
        ///原单据编号
        /// </summary>
        [DataMemberAttribute]
        public string SourceBillCode
        {
            get;
            set;
        }
        
        /// <summary>
        /// 状态
        /// </summary>
        [DataMemberAttribute]
        public int Status
        {
            get;
            set;
        }
        /// <summary>
        /// 创建人
        /// </summary>
        [DataMemberAttribute]
        public string CreatorName
        {
            get;
            set;
        }
        /// <summary>
        /// 创建时间
        /// </summary>
        [DataMemberAttribute]
        public DateTime? CreateTime
        {
            get;
            set;
        }
        /// <summary>
        /// 修改人
        /// </summary>
        [DataMemberAttribute]
        public string ModifierName

        {
            get;
            set;
        }
        /// <summary>
        /// 修改时间
        /// </summary>
        [DataMemberAttribute]
        public DateTime? ModifyTime
        {
            get;
            set;
        }
        /// <summary>
        /// 上架完成时间
        /// </summary>
        [DataMemberAttribute]
        public DateTime? ShelvesFinishTime
        {
            get;
            set;
        }

        [DataMemberAttribute]
        public string Remark
        {
            get;
            set;
        }
        /// <summary>
        /// 库位编号
        /// </summary>
        [DataMemberAttribute]
        public string WarehouseAreaCode
        {
            get;
            set;
        }
         /// <summary>
        /// 库区编号
        /// </summary>
        [DataMemberAttribute]
        public string WarehouseArea
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? InboundType {
            get;
            set;
        }
        [DataMemberAttribute]
        public bool HasDifference {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SIHCodes {
            get;
            set;
        }
    }
}
