﻿using System;
using System.Data.Objects.DataClasses;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web
{

    [DataContract(IsReference = true)]
    public class VirtualPartsShelvesTaskDetail : EntityObject
    {
        /// <summary>
        ///上架单id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int Id
        {
            get;
            set;
        }
        /// <summary>
        ///上架单号
        /// </summary>
        [DataMemberAttribute]
        public string Code
        {
            get;
            set;
        }
        /// <summary>
        ///包装任务单id
        /// </summary>
        [DataMemberAttribute]
        public int? PackingTaskId
        {
            get;
            set;
        }
        /// <summary>
        ///包装任务单号
        /// </summary>
        [DataMemberAttribute]
        public string PackingTaskCode
        {
            get;
            set;
        }
        /// <summary>
        /// 入库计划单Id
        /// </summary>
        [DataMemberAttribute]
        public int PartsInboundPlanId
        {
            get;
            set;
        }
        /// <summary>
        /// 入库计划单号
        /// </summary>
        [DataMemberAttribute]
        public string PartsInboundPlanCode
        {
            get;
            set;
        }
        /// <summary>
        /// 入库单id
        /// </summary>
        [DataMemberAttribute]
        public int? PartsInboundCheckBillId
        {
            get;
            set;
        }
        /// <summary>
        /// 入库单号
        /// </summary>
        [DataMemberAttribute]
        public string PartsInboundCheckBillCode
        {
            get;
            set;
        }
        /// <summary>
        /// 配件id
        /// </summary>
        [DataMemberAttribute]
        public int SparePartId
        {
            get;
            set;
        }
        /// <summary>
        /// 配件编号
        /// </summary>
        [DataMemberAttribute]
        public string SparePartCode
        {
            get;
            set;
        }

        /// <summary>
        ///配件名称
        /// </summary>
        [DataMemberAttribute]
        public string SparePartName
        {
            get;
            set;
        }

        /// <summary>
        ///计量单位
        /// </summary>
        [DataMemberAttribute]
        public string MeasureUnit
        {
            get;
            set;
        }
        /// <summary>
        ///批次编号
        /// </summary>
        [DataMemberAttribute]
        public string BatchNumber
        {
            get;
            set;
        }
        /// <summary>
        /// 货位
        /// </summary>
        [DataMemberAttribute]
        public int? WarehouseAreaId
        {
            get;
            set;
        }

        /// <summary>
        /// 货位
        /// </summary>
        [DataMemberAttribute]
        public string WarehouseAreaCode
        {
            get;
            set;
        }

        /// <summary>
        ///计划量
        /// </summary>
        [DataMemberAttribute]
        public int PlannedAmount
        {
            get;
            set;
        }
        /// <summary>
        ///已上架数量
        /// </summary>
        [DataMemberAttribute]
        public int? ShelvesAmount
        {
            get;
            set;
        }
        /// <summary>
        ///本次上架数量
        /// </summary>
        [DataMemberAttribute]
        public int NowShelvesAmount
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Remark
        {
            get;
            set;
        }

        /// <summary>
        ///仓库id
        /// </summary>
        [DataMemberAttribute]
        public int WarehouseId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string WarehouseCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string WarehouseName {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SIHCodes {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CurrSIHCodes {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? TraceProperty {
            get;
            set;
        }
    }
}
