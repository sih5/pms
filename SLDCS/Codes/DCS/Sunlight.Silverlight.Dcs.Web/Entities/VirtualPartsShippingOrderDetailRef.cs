﻿using System;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 虚拟配件出库发运关联单(用于打印配件发运单查询方法)
    /// </summary>
    [DataContract(IsReference = true)]
    public class VirtualPartsShippingOrderDetailRef : EntityObject {
        /// <summary>
        /// 货箱号
        /// </summary>
        [DataMemberAttribute]
        public string ContainerNumber {
            get;
            set;
        }

        /// <summary>
        /// 重量
        /// </summary>
        [DataMemberAttribute]
        public double? Weight {
            get;
            set;
        }

        /// <summary>
        /// 单位
        /// </summary>
        [DataMemberAttribute]
        public string MeasureUnit {
            get;
            set;
        }


         [DataMemberAttribute]
        public Nullable<int> ConfirmedAmount {
            get;
            set;
        }
         [DataMemberAttribute]
        public Nullable<int> DifferenceClassification {
            get;
            set;
        }
         [DataMemberAttribute]
        public int Id {
            get;
            set;
        }
         [DataMemberAttribute]
        public Nullable<int> InTransitDamageLossAmount {
            get;
            set;
        }
         [DataMemberAttribute]
        public int PartsShippingOrderId {
            get;
            set;
        }
         [DataMemberAttribute]
        public string Remark {
            get;
            set;
        }
         [DataMemberAttribute]
        public decimal SettlementPrice {
            get;
            set;
        }
         [DataMemberAttribute]
        public int ShippingAmount {
            get;
            set;
        }
         [DataMemberAttribute]
        public string SparePartCode {
            get;
            set;
        }
         [DataMemberAttribute]
        public int SparePartId {
            get;
            set;
        }
         [DataMemberAttribute]
        public string SparePartName {
            get;
            set;
        }
         [DataMemberAttribute]
        public Nullable<int> TransportLossesDisposeMethod {
            get;
            set;
        }
        /// <summary>
        /// 总重量
        /// </summary>
        [DataMemberAttribute]
        public double? AllWeight
        {
            get;
            set;
        }
        /// <summary>
        /// 体积
        /// </summary>
        [DataMemberAttribute]
        public double? Volume
        {
            get;
            set;
        }
        /// <summary>
        /// 总体积
        /// </summary>
        [DataMemberAttribute]
        public double? AllVolume
        {
            get;
            set;
        }
        //总货物金额
         [DataMemberAttribute]
        public decimal AllSettlementPrice
        {
            get;
            set;
        }
        [DataMemberAttribute]
         public string PartsOutboundPlanCode {
             get;
             set;
         }
        [DataMemberAttribute]
        public string PartsSalesOrderCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string BoxUpOrderCode {
            get;
            set;
        }
    }
}
