﻿using System;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 虚拟配件出库发运关联单(用于打印配件发运单查询方法)
    /// </summary>
    [DataContract(IsReference = true)]
    public class VirtualPartsShippingOrderRef : EntityObject {
        /// <summary>
        /// 发运单编号
        /// </summary>
        [DataMemberAttribute]
        public string PartsShippingOrderCode {
            get;
            set;
        }

        /// <summary>
        /// 收货单位编号
        /// </summary>
        [DataMemberAttribute]
        public string ReceivingCompanyCode {
            get;
            set;
        }

        /// <summary>
        /// 收货单位名称
        /// </summary>
        [DataMemberAttribute]
        public string ReceivingCompanyName {
            get;
            set;
        }

        /// <summary>
        /// 收货单位地址
        /// </summary>
        [DataMemberAttribute]
        public string ReceivingAddress {
            get;
            set;
        }

        /// <summary>
        /// 收货单位电话
        /// </summary>
        [DataMemberAttribute]
        public string ReceivingPhone {
            get;
            set;
        }

        /// <summary>
        /// 收货单位联系人
        /// </summary>
        [DataMemberAttribute]
        public string ReceivingPerson {
            get;
            set;
        }

        /// <summary>
        /// 承运单位名称
        /// </summary>
        [DataMemberAttribute]
        public string LogisticCompanyName {
            get;
            set;
        }

        /// <summary>
        /// 承运单位电话
        /// </summary>
        [DataMemberAttribute]
        public string LogisticCompanyPhone {
            get;
            set;
        }

        /// <summary>
        /// 承运单位联系人
        /// </summary>
        [DataMemberAttribute]
        public string LogisticCompanyPerson {
            get;
            set;
        }

        /// <summary>
        /// 发运单位名称
        /// </summary>
        [DataMemberAttribute]
        public string ShippingCompanyName {
            get;
            set;
        }

        /// <summary>
        /// 发运方式
        /// </summary>
        [DataMemberAttribute]
        public int? ShippingMethod {
            get;
            set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        [DataMemberAttribute]
        public string Remark {
            get;
            set;
        }

        /// <summary>
        /// 配件发运单类型
        /// </summary>
        [DataMemberAttribute]
        public int Type {
            get;
            set;
        }

        /// <summary>
        /// 源单据类型
        /// </summary>
        [DataMemberAttribute]
        public int? OriginalRequirementBillType {
            get;
            set;
        }

        /// <summary>
        /// 销售订单编号
        /// </summary>
        [DataMemberAttribute]
        public string OriginalRequirementBillCode {
            get;
            set;
        }

        /// <summary>
        /// 销售订单类型
        /// </summary>
        [DataMemberAttribute]
        public string PartsSalesOrderTypeName {
            get;
            set;
        }

        /// <summary>
        /// 发运日期
        /// </summary>
        [DataMemberAttribute]
        public DateTime? ShippingDate {
            get;
            set;
        }

        /// <summary>
        /// 要求到货日期
        /// </summary>
        [DataMemberAttribute]
        public DateTime? RequestedArrivalDate {
            get;
            set;
        }

        /// <summary>
        /// 运输里程
        /// </summary>
        [DataMemberAttribute]
        public int? TransportMileage {
            get;
            set;
        }

        /// <summary>
        /// 出库单编号
        /// </summary>
        [DataMemberAttribute]
        public string PartsOutboundBillCode {
            get;
            set;
        }

        /// <summary>
        /// 出库计划单号
        /// </summary>
        [DataMemberAttribute]
        public string PartsOutboundPlanCode {
            get;
            set;
        }

        /// <summary>
        /// 订单审批意见
        /// </summary>
        [DataMemberAttribute]
        public string OrderApproveComment {
            get;
            set;
        }

        /// <summary>
        /// 出库仓库地址
        /// </summary>
        [DataMemberAttribute]
        public string OutboundBillWarehouseAddress {
            get;
            set;
        }
        /// <summary>
        /// 收货仓库名称
        /// </summary>
        [DataMemberAttribute]
        public string ReceivingWarehouseName {
            get;
            set;
        }
        /// <summary>
        /// 联系人
        /// </summary>
        [DataMemberAttribute]
        public string Contact {
            get;
            set;
        }
        /// <summary>
        /// 联系电话
        /// </summary>
        [DataMemberAttribute]
        public string PhoneNumber {
            get;
            set;
        }
        /// <summary>
        /// 出库仓库名称
        /// </summary>
        [DataMemberAttribute]
        public string WarehouseName {
            get;
            set;
        }

        /// <summary>
        /// 品牌
        /// </summary>
        [DataMemberAttribute]
        public string PartsSalesCategoryName {
            get;
            set;
        }

        /// <summary>
        /// 制单人
        /// </summary>
        [DataMemberAttribute]
        public string CreatorName {
            get;
            set;
        }

        /// <summary>
        /// 制单时间
        /// </summary>
        [DataMemberAttribute]
        public DateTime? CreateTime {
            get;
            set;
        }

        /// <summary>
        /// 分公司名称
        /// </summary>
        [DataMemberAttribute]
        public string BranchName {
            get;
            set;
        }
        /// <summary>
        /// 联系人（承运司机）
        /// </summary>
        [DataMemberAttribute]
        public string TransportDriver
        {
            get;
            set;
        }
        /// <summary>
        /// 联系电话（司机电话）
        /// </summary>
        [DataMemberAttribute]
        public string TransportDriverPhone
        {
            get;
            set;
        }
        /// <summary>
        /// 收货单位联系人姓名
        /// </summary>
        [DataMemberAttribute]
        public string ContactPerson
        {
            get;
            set;
        }
        /// <summary>
        /// 收货单位联系人电话
        /// </summary>
        [DataMemberAttribute]
        public string ContactPhone
        {
            get;
            set;
        }
    }
}
