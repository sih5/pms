﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 虚拟配件库存
    /// </summary>
    [DataContract(IsReference = true)]
    public class VirtualPartsStock : EntityObject {
        /// <summary>
        /// 配件库存Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int PartsStockId {
            get;
            set;
        }

        /// <summary>
        ///  批次号
        /// </summary>
        [Key]
        [DataMemberAttribute]
        [Display(Name = "VirtualPartsStock_BatchNumber", ResourceType = typeof(Resources.EntityStrings))]
        public string BatchNumber {
            get {
                return batchNumber;
            }
            set {
                batchNumber = value ?? "";
            }
        }

        private string batchNumber = "";

        /// <summary>
        /// 仓库Id
        /// </summary>
        [DataMemberAttribute]
        public int WarehouseId {
            get;
            set;
        }

        /// <summary>
        /// 仓库编号
        /// </summary>
        [DataMemberAttribute]
        [Display(Name = "VirtualPartsStock_WarehouseCode", ResourceType = typeof(Resources.EntityStrings))]
        public string WarehouseCode {
            get;
            set;
        }

        /// <summary>
        /// 仓库名称
        /// </summary>
        [DataMemberAttribute]
        [Display(Name = "VirtualPartsStock_WarehouseName", ResourceType = typeof(Resources.EntityStrings))]
        public string WarehouseName {
            get;
            set;
        }

        /// <summary>
        /// 仓储企业Id
        /// </summary>
        [DataMemberAttribute]
        public int StorageCompanyId {
            get;
            set;
        }

        /// <summary>
        /// 仓储企业类型
        /// </summary>
        [DataMemberAttribute]
        public int StorageCompanyType {
            get;
            set;
        }

        /// <summary>
        /// 营销分公司Id
        /// </summary>
        [DataMemberAttribute]
        public int BranchId {
            get;
            set;
        }

        /// <summary>
        /// 配件Id
        /// </summary>
        [DataMemberAttribute]
        public int SparePartId {
            get;
            set;
        }

        /// <summary>
        ///  配件编号
        /// </summary>
        [DataMemberAttribute]
        [Display(Name = "VirtualPartsStock_SparePartCode", ResourceType = typeof(Resources.EntityStrings))]
        public string SparePartCode {
            get;
            set;
        }

        /// <summary>
        ///  配件名称
        /// </summary>
        [DataMemberAttribute]
        [Display(Name = "VirtualPartsStock_SparePartName", ResourceType = typeof(Resources.EntityStrings))]
        public string SparePartName {
            get;
            set;
        }

        /// <summary>
        /// 库区用途
        /// </summary>
        [DataMemberAttribute]
        [Display(Name = "VirtualPartsStock_WarehouseAreaCategory", ResourceType = typeof(Resources.EntityStrings))]
        public int WarehouseAreaCategory {
            get;
            set;
        }

        /// <summary>
        /// 库位Id
        /// </summary>
        [DataMemberAttribute]
        public int WarehouseAreaId {
            get;
            set;
        }

        /// <summary>
        ///  库位编号
        /// </summary>
        [DataMemberAttribute]
        [Display(Name = "VirtualPartsStock_WarehouseAreaCode", ResourceType = typeof(Resources.EntityStrings))]
        public string WarehouseAreaCode {
            get;
            set;
        }

        /// <summary>
        ///  库存数量
        /// </summary>
        [DataMemberAttribute]
        public int Quantity {
            get;
            set;
        }

        /// <summary>
        ///  （可用）数量
        /// </summary>
        [DataMemberAttribute]
        [Display(Name = "VirtualPartsStock_UsableQuantity", ResourceType = typeof(Resources.EntityStrings))]
        public int UsableQuantity {
            get;
            set;
        }

        /// <summary>
        ///  包装数量
        /// </summary>
        [DataMemberAttribute]
        public int? PackingAmount {
            get;
            set;
        }

        /// <summary>
        ///  包装规格
        /// </summary>
        [DataMemberAttribute]
        public string PackingSpecification {
            get;
            set;
        }

        /// <summary>
        /// 入库时间
        /// </summary>
        [DataMemberAttribute]
        [Display(Name = "VirtualPartsStock_InboundTime", ResourceType = typeof(Resources.EntityStrings))]
        public DateTime? InboundTime {
            get;
            set;
        }

        /// <summary>
        /// 计量单位
        /// </summary>
        [DataMemberAttribute]
        public string MeasureUnit {
            get;
            set;
        }
        /// <summary>
        /// 库区编号
        /// </summary>
        [DataMemberAttribute]
        public string WarehouseRegionCode {
            get;
            set;
        }

        /// <summary>
        /// 价格类型
        /// </summary>
        [DataMemberAttribute]
        public int PriceType {
            get;
            set;
        }

        /// <summary>
        /// 价格类型text
        /// </summary>
        [DataMemberAttribute]
        public string PriceTypeText {
            get;
            set;
        }

        /// <summary>
        /// 价格
        /// </summary>
        [DataMemberAttribute]
        public decimal? SalesPrice {
            get;
            set;
        }
        /// <summary>
        /// 配件销售类型Id
        /// </summary>
        [DataMemberAttribute]
        public int PartsSalesCategoryId {
            get;
            set;
        }
        /// <summary>
        /// 配件销售类型名称
        /// </summary>
        [DataMemberAttribute]
        public string PartsSalesCategoryName {
            get;
            set;
        }
        /// <summary>
        ///  锁定量
        /// </summary>
        [DataMemberAttribute]
        public int? LockedQty {
            get;
            set;
        }
        /// <summary>
        ///  待出量
        /// </summary>
        [DataMemberAttribute]
        public int? OutingQty {
            get;
            set;
        }

        /// <summary>
        ///  红岩号
        /// </summary>
        [DataMemberAttribute]
        public string ReferenceCode {
            get;
            set;
        }
    }
}
