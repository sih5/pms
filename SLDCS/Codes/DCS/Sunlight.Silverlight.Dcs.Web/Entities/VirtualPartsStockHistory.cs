﻿using System;
using System.Data.Objects.DataClasses;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web
{
    [DataContract(IsReference = true)]
    public class VirtualPartsStockHistory : EntityObject
    {
        [Key]
        [DataMemberAttribute]
        public int Id
        {
            get;
            set;
        }

        [DataMemberAttribute]
        public int PartsStockId
        {
            get;
            set;
        }

       [DataMemberAttribute]
        public int WarehouseId
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string WarehouseCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string WarehouseName
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int StorageCompanyId
        {
            get;
            set;
        }

         [DataMemberAttribute]
        public int StorageCompanyType
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int BranchId
        {
            get;
            set;
        }
         [DataMemberAttribute]
        public int WarehouseAreaId

        {
            get;
            set;
        }
          [DataMemberAttribute]
        public string WarehouseAreaCode

        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? WarehouseAreaCategoryId
        {
            get;
            set;
        }
         [DataMemberAttribute]
        public int? WarehouseAreaCategory
        {
            get;
            set;
        }
         [DataMemberAttribute]
        public int PartId

        {
            get;
            set;
        }
        
        [DataMemberAttribute]
        public string SparePartCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SparePartName
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ReferenceCode
        {
            get;
            set;
        }
         [DataMemberAttribute]
        public int Quantity
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Remark
        {
            get;
            set;
        }

        
        [DataMemberAttribute]
        public int CreatorId
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CreatorName
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? CreateTime
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int ModifierId
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ModifierName
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? ModifyTime
        {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? LockedQty
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? OutingQty
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? OldQuantity
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? ChangedQuantity
        {
            get;
            set;
        }
    }
}
