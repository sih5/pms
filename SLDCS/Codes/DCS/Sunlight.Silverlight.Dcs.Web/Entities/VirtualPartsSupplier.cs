﻿
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web.Entities {
    /// <summary>
    /// 虚拟供应商基本信息
    /// </summary>
    [DataContract(IsReference = true)]
    public class VirtualPartsSupplier : EntityObject {
        /// <summary>
        /// 供应商Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }

        /// <summary>
        /// 供应商编号
        /// </summary>
        [DataMemberAttribute]
        public string Code {
            get;
            set;
        }

        /// <summary>
        /// MDM供应商编码
        /// </summary>
        [DataMemberAttribute]
        public string SupplierCode {
            get;
            set;
        }

        /// <summary>
        /// 供应商名称
        /// </summary>
        [DataMemberAttribute]
        public string Name {
            get;
            set;
        }

        /// <summary>
        /// 简称
        /// </summary>
        [DataMemberAttribute]
        public string ShortName {
            get;
            set;
        }
        /// <summary>
        /// 省
        /// </summary>
        [DataMemberAttribute]
        public string ProvinceName {
            get;
            set;
        }
        /// <summary>
        /// 市
        /// </summary>
        [DataMemberAttribute]
        public string CityName {
            get;
            set;
        }
        /// <summary>
        /// 县
        /// </summary>
        [DataMemberAttribute]
        public string CountyName {
            get;
            set;
        }
        /// <summary>
        /// 联系人
        /// </summary>
        [DataMemberAttribute]
        public string ContactPerson {
            get;
            set;
        }

        /// <summary>
        /// 联系电话
        /// </summary>
        [DataMemberAttribute]
        public string ContactPhone {
            get;
            set;
        }

        /// <summary>
        ///  传真
        /// </summary>
        [DataMemberAttribute]
        public string Fax {
            get;
            set;
        }
        /// <summary>
        /// 电子邮箱
        /// </summary>
        [DataMemberAttribute]
        public string ContactMail {
            get;
            set;
        }
        /// <summary>
        /// 法定代表人
        /// </summary>
        [DataMemberAttribute]
        public string LegalRepresentative {
            get;
            set;
        }
        /// <summary>
        /// 经营范围
        /// </summary>
        [DataMemberAttribute]
        public string BusinessScope {
            get;
            set;
        }
        /// <summary>
        ///  状态
        /// </summary>
        [DataMemberAttribute]
        public int? Status {
            get;
            set;
        }
        /// <summary>
        ///  备注
        /// </summary>
        [DataMemberAttribute]
        public string Remark {
            get;
            set;
        }

        /// <summary>
        ///   供应商状态
        /// </summary>
        [DataMemberAttribute]
        public int? SupplierType {
            get;
            set;
        }

        /// <summary>
        ///   供应商业务编码
        /// </summary>
        [DataMemberAttribute]
        public string BusinessCode {
            get;
            set;
        }

        /// <summary>
        ///   供应商业务名称
        /// </summary>
        [DataMemberAttribute]
        public string BusinessName {
            get;
            set;
        }

        /// <summary>
        ///   品牌ID
        /// </summary>
        [DataMemberAttribute]
        public int PartsSalesCategoryId {
            get;
            set;
        }

        [DataMemberAttribute]
        public decimal? WaitSettlementAmount {
            get;
            set;
        }
    }
}
