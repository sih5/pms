﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 虚拟来款单
    /// </summary>
    [DataContract(IsReference = true)]
    public class VirtualPaymentBill : EntityObject {
        /// <summary>
        /// 来款单Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }
        /// <summary>
        /// 编号
        /// </summary>
        [DataMemberAttribute]
        public string Code {
            get;
            set;
        }
        /// <summary>
        /// 账户组名称
        /// </summary>
        [DataMemberAttribute]
        public string AccountGroupName {
            get;
            set;
        }
        /// <summary>
        /// 客户名称
        /// </summary>
        [DataMemberAttribute]
        public string CustomerCompanyName {
            get;
            set;
        }
        /// <summary>
        /// 客户编号
        /// </summary>
        [DataMemberAttribute]
        public string CustomerCompanyCode {
            get;
            set;
        }
        /// <summary>
        /// 金额(H)
        /// </summary>
        [DataMemberAttribute]
        public decimal Amount {
            get;
            set;
        }
        /// <summary>
        /// 来款方式
        /// </summary>
        [DataMemberAttribute]
        public int? PaymentMethod {
            get;
            set;
        }
        /// <summary>
        /// 摘要
        /// </summary>
        [DataMemberAttribute]
        public string Summary {
            get;
            set;
        }
        /// <summary>
        /// 银行账户
        /// </summary>
        [DataMemberAttribute]
        public string BankAccountNumber {
            get;
            set;
        }
        /// <summary>
        /// 经办人
        /// </summary>
        [DataMemberAttribute]
        public string Operator {
            get;
            set;
        }
        /// <summary>
        /// 状态
        /// </summary>
        [DataMemberAttribute]
        public int? Status {
            get;
            set;
        }
        /// <summary>
        /// 业务编码
        /// </summary>
        [DataMemberAttribute]
        public string BusinessCode {
            get;
            set;
        }
        /// <summary>
        ///  创建人
        /// </summary>
        [DataMemberAttribute]
        [Display(ResourceType = typeof(EntityStrings), Name = "_Common_CreatorName")]
        public string CreatorName {
            get;
            set;
        }
        /// <summary>
        ///  创建时间
        /// </summary>
        [DataMemberAttribute]
        [Display(ResourceType = typeof(EntityStrings), Name = "_Common_CreateTime")]
        public DateTime? CreateTime {
            get;
            set;
        }
        /// <summary>
        ///  修改人
        /// </summary>
        [DataMemberAttribute]
        [Display(ResourceType = typeof(EntityStrings), Name = "_Common_ModifierName")]
        public string ModifierName {
            get;
            set;
        }
        /// <summary>
        ///  修改时间
        /// </summary>
        [DataMemberAttribute]
        [Display(ResourceType = typeof(EntityStrings), Name = "_Common_ModifyTime")]
        public DateTime? ModifyTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? InvoiceDate
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? DueDate
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? IssueDate
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Drawer
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PayBank
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string MoneyOrderNum
        {
            get;
            set;
        }

        /// <summary>
        /// 凭证号
        /// </summary>
        [DataMemberAttribute]
        public string Message {
            get;
            set;
        }
    }
}
