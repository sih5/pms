﻿using System;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web
{
    [DataContract(IsReference = true)]
   public class VirtualPickingTask : EntityObject
    {
        /**
         * 分公司名称
         **/
         [DataMemberAttribute]
        public string BranchName
        {
            get;
            set;
        }
         /**
          * 仓库名称
          **/
         [DataMemberAttribute]
         public string WarehouseName
         {
             get;
             set;
         }
         /**
          * 对方单位名称
          **/
         [DataMemberAttribute]
         public string CounterpartCompanyName
         {
             get;
             set;
         }        
         /**
          * 订单类型名称
          **/
         [DataMemberAttribute]
         public string OrderTypeName
         {
             get;
             set;
         }
         /**
          * 任务单号
          **/
         [DataMemberAttribute]
         public string Code
         {
             get;
             set;
         }
    }
}
