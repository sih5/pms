﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class VirtualPickingTaskDetail : EntityObject {
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }
        [DataMemberAttribute]
        public int PickingTaskId {
            get;
            set;
        }
        [DataMemberAttribute]
        public int PartsOutboundPlanId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartsOutboundPlanCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SourceCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public int SparePartId {
            get;
            set;
        }
         [DataMemberAttribute]
        public string SparePartCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SparePartName {
            get;
            set;
        }
        [DataMemberAttribute]
        public string MeasureUnit {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? WarehouseAreaId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string WarehouseAreaCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? PlanQty {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? PickingQty {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? ShortPickingReason {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? CourrentPicking {
            get;
            set;
        }
        [DataMemberAttribute]
        public String ReferenceCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public String BatchNumber {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? StockQty
        {
            get;
            set;
        }
        /**
         * 分公司名称
         **/
         [DataMemberAttribute]
        public string BranchName
        {
            get;
            set;
        }
         /**
          * 仓库名称
          **/
         [DataMemberAttribute]
         public string WarehouseName
         {
             get;
             set;
         }
         /**
          * 对方单位名称
          **/
         [DataMemberAttribute]
         public string CounterpartCompanyName
         {
             get;
             set;
         }        
         /**
          * 订单类型名称
          **/
         [DataMemberAttribute]
         public string OrderTypeName
         {
             get;
             set;
         }
         /**
          * 任务单号
          **/
         [DataMemberAttribute]
         public string Code
         {
             get;
             set;
         }
        //顶层库区编号
        [DataMemberAttribute]
         public string TopLevelWarehouseAreaCode {
             get;
             set;
         }
        //顶层库区编号
        [DataMemberAttribute]
        public int TopLevelWarehouseAreaId {
            get;
            set;
        }
        /**
       * 入库仓库名称
       **/
        [DataMemberAttribute]
        public string InWarehouseName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? TraceProperty {
            get;
            set;
        }
        [DataMemberAttribute]
        public string TraceCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? CompanyType {
            get;
            set;
        }
    }
}
