﻿using System;
using System.Data.Objects.DataClasses;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web
{

    [DataContract(IsReference = true)]
    public class VirtualPickingTaskForBoxUp : EntityObject
    {
        /// <summary>
        ///拣货任务单id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int Id
        {
            get;
            set;
        }
        /// <summary>
        ///拣货任务单号
        /// </summary>
        [DataMemberAttribute]
        public string Code
        {
            get;
            set;
        }
        /// <summary>
        /// 品牌id
        /// </summary>
        [DataMemberAttribute]
        public int PartsSalesCategoryId
        {
            get;
            set;
        }

        /// <summary>
        /// 品牌
        /// </summary>
        [DataMemberAttribute]
        public string PartsSalesCategoryName
        {
            get;
            set;
        }

       
        [DataMemberAttribute]
        public int WarehouseId

        {
            get;
            set;
        }

        
        [DataMemberAttribute]
        public string WarehouseCode

        {
            get;
            set;
        }

        
        [DataMemberAttribute]
        public string WarehouseName

        {
            get;
            set;
        }

        [DataMemberAttribute]
        public int CounterpartCompanyId
        {
            get;
            set;

        }
        [DataMemberAttribute]
        public string CounterpartCompanyCode
        {
            get;
            set;
        }
       
        [DataMemberAttribute]
        public string CounterpartCompanyName
        {
            get;
            set;
        }

        [DataMemberAttribute]
        public string ReceivingAddress
        {
            get;
            set;

        }
        [DataMemberAttribute]
        public int OrderTypeId
        {
            get;
            set;

        }
        [DataMemberAttribute]
        public string OrderTypeName
        {
            get;
            set;

        }
        [DataMemberAttribute]
        public int ShippingMethod
        {
            get;
            set;

        }
        [DataMemberAttribute]
        public string CreatorName
        {
            get;
            set;
        }

        
        [DataMemberAttribute]
        public DateTime? CreateTime
        {
            get;
            set;
        }
       
        [DataMemberAttribute]
        public DateTime? PickingFinishTime
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? ReceivingWarehouseId
        {
            get;
            set;
        }
    }
}
