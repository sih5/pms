﻿using System.Data.Objects.DataClasses;
using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web
{
    [DataContract(IsReference = true)]
    public class VirtualPostSaleClaimsTmp : EntityObject
    {

        [Key]
        [DataMemberAttribute]
        public int Id
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ClaimBillCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int DealerId
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string DealerCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string DealerName
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int SparePartId
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SparePartCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SparePartName
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? Amount
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? Status
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? Type
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public bool? IsClaim
        {
            get;
            set;
        }

        [DataMemberAttribute]
        public DateTime? CreateTime
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? TransferTime
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public Decimal? SalesPrice {
            get;
            set;
        }
        [DataMemberAttribute]
        public Decimal? RetailGuidePrice {
            get;
            set;
        }
        [DataMemberAttribute]
        public string GroupCode {
            get;
            set;
        }
    }
}
