﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web
{
    /// <summary>
    /// 虚拟产品
    /// </summary>
    [DataContract(IsReference = true)]
    public class VirtualProduct : EntityObject 
    {
        [Key]
        [DataMemberAttribute]
        public int Id 
        { 
            get;
            set; 
        }
        /// <summary>
        /// 整车品牌编号（品牌代码）
        /// </summary>
        [DataMemberAttribute]
        public string BrandCode
        {
            get;
            set;
        }
       
        [DataMemberAttribute]
        public string BrandName 
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SubBrandCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SubBrandName
        {
            get;
            set; 
        }
        [DataMemberAttribute]
        public string TerraceCode
        {
            get;
            set; 
        }
        [DataMemberAttribute]
        public string TerraceName
        {
            get;
            set;
        }
        /// <summary>
        /// 整车产品线编号（产品线代码）
        /// </summary>
        [DataMemberAttribute]
        public string ProductLine
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ProductName
        {
            get;
            set;
        }

        /// <summary>
        /// 整车编号(产品编号)
        /// </summary>
        [DataMemberAttribute]
        public string Code
        {
            get;
            set;
        }

        /// <summary>
        /// 服务产品线名称
        /// </summary>
        [DataMemberAttribute]
        public string ServiceProductLineName
        {
            get;
            set;
        }

        [DataMemberAttribute]
        public string ProductCategoryCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ProductCategoryName
        {
            get;
            set;
        }
        [DataMemberAttribute]
        [Display(ResourceType = typeof(EntityStrings), Name = "_Common_Status")]
        public int Status
        {
            get;
            set;
        }
        /// <summary>
        /// 原始内部编号
        /// </summary>
        [DataMemberAttribute]
        public string OldInternalCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string AnnoucementNumber
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string TonnageCode
        {
            get;
            set; 
        }
        [DataMemberAttribute]
        public string EngineTypeCode
        {
            get;
            set; 
        }
        [DataMemberAttribute]
        public string EngineManufacturerCode
        {
            get;
            set; 
        }
        [DataMemberAttribute]
        public string GearSerialTypeCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string GearSerialManufacturerCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string RearAxleTypeCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string RearAxleManufacturerCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string TireTypeCode 
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string TireFormCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string BrakeModeCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string DriveModeCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string FrameConnetCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string VehicleSize
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string AxleDistanceCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string EmissionStandardCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ProductFunctionCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ProductLevelCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string VehicleTypeCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ColorDescribe
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? SeatingCapacity
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? Weight
        {
            get;
            set;
        }
        [DataMemberAttribute]
        [Display(ResourceType = typeof(EntityStrings), Name = "_Common_Remark")]
        public string Remark
        {
            get;
            set;
        }
        [DataMemberAttribute]
        [Display(ResourceType = typeof(EntityStrings), Name = "Product_OrderCycle")]
        public int? OrderCycle
        {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? PurchaseCycle 
        {
            get;
            set;
        }

       [DataMemberAttribute]
       [Display(ResourceType = typeof(EntityStrings), Name = "Product_ParameterDescription")]
        public string ParameterDescription
        {
            get;
            set;
        }
        [DataMemberAttribute]
        [Display(ResourceType = typeof(EntityStrings), Name = "Product_Version")]
        public string Version
        {
            get;
            set;
        }
        [DataMemberAttribute]
        [Display(ResourceType = typeof(EntityStrings), Name = "Product_Configuration")]
        public string Configuration
        {
            get;
            set;
        }
        [DataMemberAttribute]
        [Display(ResourceType = typeof(EntityStrings), Name = "Product_EngineCylinder")]
        public string EngineCylinder
        {
            get;
            set;
        }
        [DataMemberAttribute]
        [Display(ResourceType = typeof(EntityStrings), Name = "Product_EmissionStandard")]
        public string EmissionStandard
        {
            get;
            set;
        }
         [DataMemberAttribute]
         [Display(ResourceType = typeof(EntityStrings), Name = "Product_ManualAutomatic")]
        public string ManualAutomatic
        {
            get;
            set;
        }

         [DataMemberAttribute]
         [Display(ResourceType = typeof(EntityStrings), Name = "Product_ColorCode")]
        public string ColorCode
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Picture
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? CreatorId
        {
            get;
            set;
        }
        /// <summary>
        /// CreatorName
        /// </summary>
        [DataMemberAttribute]
        [Display(ResourceType = typeof(EntityStrings), Name = "_Common_CreatorName")]
        public string CreatorName
        {
            get;
            set;
        }
        [DataMemberAttribute]
        [Display(ResourceType = typeof(EntityStrings), Name = "_Common_CreateTime")]
        public DateTime? CreateTime
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? ModifierId
        {
            get;
            set;
        }
        /// <summary>
        /// 修改人
        /// </summary>
        [DataMemberAttribute]
        [Display(ResourceType = typeof(EntityStrings), Name = "_Common_ModifierName")]
        public string ModifierName
        {
            get;
            set;
        }
        [DataMemberAttribute]
        [Display(ResourceType = typeof(EntityStrings), Name = "_Common_ModifyTime")]
        public DateTime? ModifyTime
        {
            get;
            set;
        }
        [DataMemberAttribute]
        [Display(ResourceType = typeof(EntityStrings), Name = "_Common_AbandonerId")]
        public int? AbandonerId
        {
            get;
            set;
        }
        /// <summary>
        /// 作废人
        /// </summary>
        [DataMemberAttribute]
        [Display(ResourceType = typeof(EntityStrings), Name = "_Common_AbandonerName")]
        public string AbandonerName
        {
            get;
            set;
        }
        [DataMemberAttribute]
        [Display(ResourceType = typeof(EntityStrings), Name = "_Common_AbandonTime")]
        public DateTime? AbandonTime
        {
            get;
            set;
        }

    }
}
