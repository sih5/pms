﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web
{
    [DataContract(IsReference = true)]
   public class VirtualPurchasePricingChangeHi : EntityObject 
    {
        [Key]
        [DataMemberAttribute]
        public int Id
        {
            get;
            set;
        }
         [DataMemberAttribute]
        public int PartsSalesCategoryId
        {
            get;
            set;
        }
         [DataMemberAttribute]
         public string PartsSalesCategoryCode
         {
             get;
             set;
         }
         [DataMemberAttribute]
         public string PartsSalesCategoryName
         {
             get;
             set;
         }
         [DataMemberAttribute]
         public string SparePartCode
         {
             get;
             set;
         }
         [DataMemberAttribute]
         public string SparePartName
         {
             get;
             set;
         }
         [DataMemberAttribute]
         public decimal OldPurchasePrice
         {
             get;
             set;
         }
         [DataMemberAttribute]
         public decimal PurchasePrice
         {
             get;
             set;
         }
         [DataMemberAttribute]
         public double ChangeRatio
         {
             get;
             set;
         }
         [DataMemberAttribute]
         public decimal CenterPrice
         {
             get;
             set;
         }
         [DataMemberAttribute]
         public decimal DistributionPrice
         {
             get;
             set;
         }
         [DataMemberAttribute]
         public decimal RetailPrice
         {
             get;
             set;
         }
         [DataMemberAttribute]
         public DateTime? MaintenanceTime
         {
             get;
             set;
         }
         [DataMemberAttribute]
         public int Status
         {
             get;
             set;
         }
         [DataMemberAttribute]
         public string Remark
         {
             get;
             set;
         }
         [DataMemberAttribute]
         public int CreatorId
         {
             get;
             set;
         }
         [DataMemberAttribute]
         public string CreatorName
         {
             get;
             set;
         }
         [DataMemberAttribute]
         public DateTime? CreateTime
         {
             get;
             set;
         }
         [DataMemberAttribute]
         public int ModifierId
         {
             get;
             set;
         }
         [DataMemberAttribute]
         public string ModifierName
         {
             get;
             set;
         }
         [DataMemberAttribute]
         public DateTime? ModifyTime
         {
             get;
             set;
         }
         [DataMemberAttribute]
         public string IncreaseRateGroupId
         {
             get;
             set;
         }
        [DataMemberAttribute]
         public bool? IsPrimary
         {
             get;
             set;
         }
         [DataMemberAttribute]
         public string ReferenceCode
         {
             get;
             set;
         }
         [DataMemberAttribute]
         public DateTime? ValidFrom {
             get;
             set;
         }
         [DataMemberAttribute]
         public DateTime? ValidTo {
             get;
             set;
         }
    }
}
