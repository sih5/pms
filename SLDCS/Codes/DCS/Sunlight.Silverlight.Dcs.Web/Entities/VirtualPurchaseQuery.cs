﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 虚拟采购需求
    /// </summary>
    [DataContract(IsReference = true)]
    public class VirtualPurchaseQuery : EntityObject {
        /// <summary>
        ///分公司
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int BranchId {
            get;
            set;
        }

        /// <summary>
        ///品牌
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int PartsSalesCategoryId {
            get;
            set;
        }
        /// <summary>
        /// 配件图号
        /// </summary>
        [DataMemberAttribute]
        public string PartCode {
            get;
            set;
        }

        /// <summary>
        /// 配件Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int PartId {
            get;
            set;
        }
        /// <summary>
        /// 配件名称
        /// </summary>
        [DataMemberAttribute]
        public string PartName {
            get;
            set;
        }

        /// <summary>
        /// 库存
        /// </summary>
        [DataMemberAttribute]
        public int stock {
            get;
            set;
        }
        /// <summary>
        /// 锁定库存
        /// </summary>
        [DataMemberAttribute]
        public int LockedQty {
            get;
            set;
        }
        /// <summary>
        /// 可用库存
        /// </summary>
        [DataMemberAttribute]
        public int Usablequantity {
            get {
                return this.stock - this.LockedQty;
            }
            set {

            }
        }
        ///// <summary>
        ///// 可用库存
        ///// </summary>
        //[DataMemberAttribute]  
        //public int  Usablequantity {
        //    get;
        //    set;
        //}

        /// <summary>
        ///调拨在途数量
        /// </summary>
        [DataMemberAttribute]
        public int OnwayQty {
            get;
            set;
        }
        /// <summary>
        ///订单未满足数量
        /// </summary>
        [DataMemberAttribute]
        public int WMZQty {
            get;
            set;
        }
        /// <summary>
        ///出库数量
        /// </summary>
        [DataMemberAttribute]
        public int OutQty {
            get;
            set;
        }
        /// <summary>
        ///配件所属分类
        /// </summary>
        [DataMemberAttribute]
        public int? PartsAttribution {
            get;
            set;
        }
        [DataMemberAttribute]
        [Display(ResourceType = typeof(EntityStrings), Name = "_Common_CreateTime")]
        public DateTime? CreateTime {
            get;
            set;
        }
    }
}
