﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class VirtualQTSInfo : EntityObject {

        [Key]
        [DataMemberAttribute]
        public string ID {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CSCODE {
            get;
            set;
        }

        [DataMemberAttribute]
        public string PRODUCTNO {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ZCBMCODE {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? ASSEMBLY_DATE {
            get;
            set;
        }
        [DataMemberAttribute]
        public string BIND_DATE {
            get;
            set;
        }
        [DataMemberAttribute]
        public string FACTORY {
            get;
            set;
        }
        [DataMemberAttribute]
        public string MAPID {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PATCH {
            get;
            set;
        }
        [DataMemberAttribute]
        public string VIN {
            get;
            set;
        }
        [DataMemberAttribute]
        public string MAPNAME {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SOURCE_CODE {
            get;
            set;
        }
        [DataMemberAttribute]
        public string GONGYINGSBIANHAO {
            get;
            set;
        }
        [DataMemberAttribute]
        public string GONGYINGSNAME {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CHUCHANGID {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CHANGXIAN {
            get;
            set;
        }
    }
}
