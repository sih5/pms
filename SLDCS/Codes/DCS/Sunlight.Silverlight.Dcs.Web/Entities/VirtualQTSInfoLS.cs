﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class VirtualQTSInfoLS : EntityObject {
        [Key]
        [DataMemberAttribute]
        public string ID {
            get;
            set;
        }

        [DataMemberAttribute]
        public string SERNR {
            get;
            set;
        }

        [DataMemberAttribute]
        public string DATUM {
            get;
            set;
        }
        [DataMemberAttribute]
        public string UZEIT {
            get;
            set;
        }
        [DataMemberAttribute]
        public string KDAUF {
            get;
            set;
        }
        [DataMemberAttribute]
        public string KDPOS {
            get;
            set;
        }
        [DataMemberAttribute]
        public string MATNR {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CHARG {
            get;
            set;
        }
        [DataMemberAttribute]
        public string MENGE {
            get;
            set;
        }
        [DataMemberAttribute]
        public string BWART {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? CC_TIMES {
            get;
            set;
        }
        [DataMemberAttribute]
        public string MAKTX {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CG_MATNR {
            get;
            set;
        }
    }
}
