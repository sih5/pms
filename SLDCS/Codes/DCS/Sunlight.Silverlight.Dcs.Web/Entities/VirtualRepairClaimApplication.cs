﻿
using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 虚拟维修保养索赔申请单
    /// </summary>
    [DataContract(IsReference = true)]
    public class VirtualRepairClaimApplication : EntityObject {
        /// <summary>
        /// 主键Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CreatorName {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? IsAppPicture {
            get;
            set;
        }

        [DataMemberAttribute]
        public string Code {
            get;
            set;
        }
        [DataMemberAttribute]
        public int Status {
            get;
            set;
        }
        [DataMemberAttribute]
        public int ClaimType {
            get;
            set;
        }

        [DataMemberAttribute]
        public string IsCountersign {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? SubmitTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? VehicleInformationSalesDate {
            get;
            set;
        }
        [DataMemberAttribute]
        public int DealerId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string DealerCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string BusinessCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string DealerName {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SalesRegionRegionName {
            get;
            set;
        }
        [DataMemberAttribute]
        public string MarketingDepartmentName {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartsSalesCategoryName {
            get;
            set;
        }
        [DataMemberAttribute]
        public string RepairObjectRepairTarget {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ServiceProductLineViewProductLineName {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ServiceProductLineName {
            get;
            set;
        }
        [DataMemberAttribute]
        public string VehicleInformationVIN {
            get;
            set;
        }
        [DataMemberAttribute]
        public string VehicleLicensePlate {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? RepairRequestTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? ApplicationType {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? RepairType {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CarActuality {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal? TotalAmount {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal? EstimatedLaborCost {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal? EstimatedMaterialCost {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal? OtherCost {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ApproveCommentHistory {
            get;
            set;
        }
        [DataMemberAttribute]
        public int BranchId {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? FaultyPartsSupplierId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string VehicleInformationSerialNumber {
            get;
            set;
        }
        [DataMemberAttribute]
        public string FaultyPartsSupplierCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string FaultyPartsSupplierName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int ServiceProductLineId {
            get;
            set;
        }
        [DataMemberAttribute]
        public int PartsSalesCategoryId {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? CreateTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? RejectTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? InitialApproveTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? CheckTime {
            get;
            set;
        }
        /// <summary>
        /// 分公司编号
        /// </summary>
        [DataMemberAttribute]
        public string BranchCode {
            get;
            set;
        }
        /// <summary>
        /// 分公司名称
        /// </summary>
        [DataMemberAttribute]
        public string BranchName {
            get;
            set;
        }
        /// <summary>
        /// 大区
        /// </summary>
        [DataMemberAttribute]
        public string RegionName {
            get;
            set;
        }
        /// <summary>
        /// 维修对象
        /// </summary>
        [DataMemberAttribute]
        public string RepairTarget {
            get;
            set;
        }
        [DataMemberAttribute]
        public string RejectReason {
            get;
            set;
        }
    }
}
