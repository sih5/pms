﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {

    /// <summary>
    /// 虚拟维修索赔单
    /// </summary>
    [DataContract(IsReference = true)]
    public class VirtualRepairClaimBillForBussiness : EntityObject {

        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }

        [DataMemberAttribute]
        public int RepairOrderId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string BehindAxleTypTonneLevel {
            get;
            set;
        }


        [DataMemberAttribute]
        public int? IsAgreement {
            get;
            set;
        }

        [DataMemberAttribute]
        public DateTime? ArrivalTime {
            get;
            set;
        }

        [DataMemberAttribute]
        public DateTime? DepartureTime {
            get;
            set;

        }

        [DataMemberAttribute]
        public double? AtStationTime {
            get;
            set;
        }

        [DataMemberAttribute]
        public string SupplierCheckComment {
            get;
            set;
        }

        [DataMemberAttribute]
        public bool VIPVehicle {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? AdaptType {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? CustomerCategory {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? IsTeamCustomers {
            get;
            set;
        }

        [DataMemberAttribute]
        public string VehicleGrade {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SupplierIdeaNotPassReson {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? SupplierCheckNotP {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? SupplierCheckResult {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? SupplierConfirmNotP {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? SupplierConfirmResult {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ChassisGrade {
            get;
            set;
        }

        [DataMemberAttribute]
        public string RepairTarget {
            get;
            set;
        }

        [DataMemberAttribute]
        public string GearModel {
            get;
            set;
        }
        [DataMemberAttribute]
        public string FirstClassStationCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string FirstClassStationName {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CageCategory {
            get;
            set;
        }

        [DataMemberAttribute]
        public string ProductSeries {
            get;
            set;
        }

        [DataMemberAttribute]
        public string VehicleSeries {
            get;
            set;
        }

        [DataMemberAttribute]
        public string VehicleFunction {
            get;
            set;
        }

        [DataMemberAttribute]
        public string DriveModeCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public int ClaimType {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ServiceTripClaimApplicationRemark {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? FirstStoppageMileage {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ContactAddress {
            get;
            set;
        }

        /// <summary>
        /// 扣款金额
        /// </summary>
        [DataMemberAttribute]
        public decimal? DebitAmount {
            get;
            set;
        }
        /// <summary>
        /// 补款金额
        /// </summary>
        [DataMemberAttribute]
        public decimal? ReplenishmentAmount {
            get;
            set;
        }


        [DataMemberAttribute]
        public string FaultyPartsAssemblyCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string FaultyPartsAssemblyName {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CustomerCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string AbandonerName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int BranchId {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? ProductLineType {
            get;
            set;
        }

        /// <summary>
        /// 车牌号
        /// </summary>
        [DataMemberAttribute]
        public string VehicleLicensePlate {
            get;
            set;
        }
        /// <summary>
        /// 车主
        /// </summary>
        [DataMemberAttribute]
        public string RetainedCustomer {
            get;
            set;
        }
        /// <summary>
        /// 车主电话
        /// </summary>
        [DataMemberAttribute]
        public string RetainedCustomerPhone {
            get;
            set;
        }
        /// <summary>
        ///联系人
        /// </summary>
        [DataMemberAttribute]
        public string VehicleContactPerson {
            get;
            set;
        }
        /// <summary>
        /// 联系人电话
        /// </summary>
        [DataMemberAttribute]
        public string ContactPhone {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CancelReason {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ClaimBillCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string RepairContractCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? SupplierSettleStatus {
            get;
            set;
        }

        [DataMemberAttribute]
        public string RepairWorkOrder {
            get;
            set;
        }

        //质量快报编号
        [DataMemberAttribute]
        public string QualityInformationCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string InitialApprovertComment {
            get;
            set;
        }
        [DataMemberAttribute]
        public string InitialApproverName {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? RepairType {
            get;
            set;
        }

        [DataMemberAttribute]
        public int Status {
            get;
            set;
        }

        [DataMemberAttribute]
        public decimal? OtherCost {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? AutoApproveStatus {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? SupplierCheckStatus {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? SupplierConfirmStatus {
            get;
            set;
        }

        [DataMemberAttribute]
        public int SettlementStatus {
            get;
            set;
        }

        [DataMemberAttribute]
        public int UsedPartsDisposalStatus {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? RejectQty {
            get;
            set;
        }

        [DataMemberAttribute]
        public int DealerId {
            get;
            set;
        }

        [DataMemberAttribute]
        public string DealerCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string DealerName {
            get;
            set;
        }

        [DataMemberAttribute]
        public string SerialNumber {
            get;
            set;
        }

        [DataMemberAttribute]
        public DateTime? SalesDate {
            get;
            set;
        }

        [DataMemberAttribute]
        public DateTime? OutOfFactoryDate {
            get;
            set;
        }


        [DataMemberAttribute]
        public int? Mileage {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? WorkingHours {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? Capacity {
            get;
            set;
        }

        [DataMemberAttribute]
        public DateTime? RepairRequestTime {
            get;
            set;
        }

        [DataMemberAttribute]
        public int ServiceProductLineId {
            get;
            set;
        }

        [DataMemberAttribute]
        public string ServiceProductLineCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string ServiceProductLineName {
            get;
            set;
        }

        //销售车型名称
        [DataMemberAttribute]
        public string VehicleType {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? ApplicationType {
            get;
            set;
        }

        [DataMemberAttribute]
        public string ApplicationTypeName {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? MalfunctionId {
            get;
            set;
        }

        [DataMemberAttribute]
        public string MalfunctionCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string MalfunctionDescription {
            get;
            set;
        }

        [DataMemberAttribute]
        public string MalfunctionReason {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? FaultyPartsId {
            get;
            set;
        }

        [DataMemberAttribute]
        public string FaultyPartsCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string FaultyPartsName {
            get;
            set;
        }

        [DataMemberAttribute]
        public int ResponsibleUnitId {
            get;
            set;
        }

        [DataMemberAttribute]
        public string responsibleUnitCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string responsibleUnitName {
            get;
            set;
        }

        [DataMemberAttribute]
        public decimal LaborCost {
            get;
            set;
        }

        [DataMemberAttribute]
        public decimal MaterialCost {
            get;
            set;
        }

        [DataMemberAttribute]
        public decimal? PartsManagementCost {
            get;
            set;
        }

        [DataMemberAttribute]
        public decimal? TotalAmount {
            get;
            set;
        }

        [DataMemberAttribute]
        public string Remark {
            get;
            set;
        }

        [DataMemberAttribute]
        public int MarketingDepartmentId {
            get;
            set;
        }

        [DataMemberAttribute]
        public string marketingDepartmentCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ApproveCommentHistory {
            get;
            set;
        }

        [DataMemberAttribute]
        public string marketingDepartmentName {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? BridgeType {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? PartSource {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? ServiceTripDistance {
            set;
            get;
        }

        [DataMemberAttribute]
        public string ClaimSupplierName {
            get;
            set;
        }

        [DataMemberAttribute]
        public string ServiceActivityCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ServiceActivityName {
            get;
            set;
        }

        [DataMemberAttribute]
        public string ServiceActivityContent {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? ServiceActivityType {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? ClaimSupplierId {
            get;
            set;
        }

        [DataMemberAttribute]
        public string ClaimSupplierCode {
            get;
            set;
        }
        /// <summary>
        /// MDM供应商编码
        /// </summary>
        [DataMemberAttribute]
        public string SupplierCode {
            get;
            set;
        }


        [DataMemberAttribute]
        public DateTime? CreateTime {
            get;
            set;
        }

        [DataMemberAttribute]
        public string HavingPurchase {
            get {
                return PartSource == 3 ? EntityStrings.Export_Title_Yes : EntityStrings.Export_Title_No;
            }
        }
        [DataMemberAttribute]
        public bool IfClaimToSupplier {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? ProductType {
            get;
            set;
        }

        [DataMemberAttribute]
        public int PartsSalesCategoryId {
            get;
            set;
        }

        [DataMemberAttribute]
        public string PartsSalesCategoryName {
            get;
            set;
        }

        [DataMemberAttribute]
        public string EngineModel {
            get;
            set;
        }

        [DataMemberAttribute]
        public string EngineCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? CreatorId {
            get;
            set;
        }

        [DataMemberAttribute]
        public string CreatorName {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? ApproverId {
            get;
            set;
        }

        [DataMemberAttribute]
        public string ApproverName {
            get;
            set;
        }

        [DataMemberAttribute]
        public DateTime? ApproveTime {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? FinalApproverId {
            get;
            set;
        }

        [DataMemberAttribute]
        public string FinalApproverName {
            get;
            set;
        }

        [DataMemberAttribute]
        public DateTime? FinalApproveTime {
            get;
            set;
        }

        [DataMemberAttribute]
        public string SupplierConfirmComment {
            get;
            set;
        }

        [DataMemberAttribute]
        public string EngineSerialNumber {
            get;
            set;
        }

        [DataMemberAttribute]
        public string ServiceDepartmentComment {
            get;
            set;
        }

        [DataMemberAttribute]
        public string RejectReason {
            get;
            set;
        }

        [DataMemberAttribute]
        public string VideoMobileNumber {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? MVSOutRange {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? MVSOutTime {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? MVSOutDistance {
            get;
            set;
        }

        [DataMemberAttribute]
        public string MVSOutCoordinate {
            get;
            set;
        }

        [DataMemberAttribute]
        public string ApproveComment {
            get;
            set;
        }

        [DataMemberAttribute]
        public string CheckComment {
            get;
            set;
        }

        [DataMemberAttribute]
        public string RepairClaimApplicationCode {
            get;
            set;
        }

        /// <summary>
        /// 工时系数
        /// </summary>
        [DataMemberAttribute]
        public double? LaborCoefficient {
            get;
            set;
        }

        /// <summary>
        /// 配件索赔系数
        /// </summary>
        [DataMemberAttribute]
        public double? PartsClaimCoefficient {
            get;
            set;
        }
        /// <summary>
        /// 配件管理费率
        /// </summary>
        [DataMemberAttribute]
        public double Rate {
            get;
            set;
        }
        /// <summary>
        /// 工况类别
        /// </summary>
        [DataMemberAttribute]
        public string VehicleCategoryName {
            get;
            set;
        }

        /// <summary>
        /// 工况类别
        /// </summary>
        [DataMemberAttribute]
        public int CompanyType {
            get;
            set;
        }

        [DataMemberAttribute]
        public string SysCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? IsContainPhoto {
            get;
            set;
        }
        /// <summary>
        /// 维修对象
        /// </summary>
        [DataMemberAttribute]
        public string RepairObjectName {
            get;
            set;
        }
    }
}