﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web.Entities {
    public class VirtualRepairClaimBillForMergerExport {
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }

        [DataMemberAttribute]
        public string VehicleGrade {
            get;
            set;
        }

        [DataMemberAttribute]
        public string ChassisGrade {
            get;
            set;
        }

        [DataMemberAttribute]
        public string RepairTarget {
            get;
            set;
        }

        [DataMemberAttribute]
        public string VIN {
            get;
            set;
        }

        [DataMemberAttribute]
        public string CheckComment {
            get;
            set;
        }

        [DataMemberAttribute]
        public string VehicleLicensePlate {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? AdaptType {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? CustomerCategory {
            get;
            set;
        }

        [DataMemberAttribute]
        public bool VIPVehicle {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? IsTeamCustomers {
            get;
            set;
        }

        [DataMemberAttribute]
        public string FirstClassStationCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string FirstClassStationName {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? MalfunctionId {
            get;
            set;
        }

        [DataMemberAttribute]
        public string Name {
            get;
            set;
        }

        [DataMemberAttribute]
        public string CageCategory {
            get;
            set;
        }

        [DataMemberAttribute]
        public string ProductSeries {
            get;
            set;
        }

        [DataMemberAttribute]
        public string VehicleSeries {
            get;
            set;
        }

        [DataMemberAttribute]
        public string VehicleFunction {
            get;
            set;
        }

        [DataMemberAttribute]
        public string DriveModeCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? FirstStoppageMileage {
            get;
            set;
        }

        [DataMemberAttribute]
        public string T1Name {
            get;
            set;
        }

        [DataMemberAttribute]
        public string T2Name {
            get;
            set;
        }

        [DataMemberAttribute]
        public string ContactAddress {
            get;
            set;
        }

        [DataMemberAttribute]
        public int ClaimType {
            get;
            set;
        }


        [DataMemberAttribute]
        public int BranchId {
            get;
            set;
        }

        [DataMemberAttribute]
        public double? ServiceTripDistance {
            get;
            set;
        }

        [DataMemberAttribute]
        public string SupplierCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? RepairClaimApplicationId {
            get;
            set;
        }

        [DataMemberAttribute]
        public string BehindAxleTypTonneLevel {
            get;
            set;
        }

        [DataMemberAttribute]
        public string GearModel {
            get;
            set;
        }

        [DataMemberAttribute]
        public string CustomerCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string ClaimSupplierCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string ClaimSupplierName {
            get;
            set;
        }

        [DataMemberAttribute]
        public string SerialNumber {
            get;
            set;
        }

        [DataMemberAttribute]
        public bool IfClaimToSupplier {
            get;
            set;
        }

        [DataMemberAttribute]
        public string QualityInformationCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string ApproveCommentHistory {
            get;
            set;
        }

        [DataMemberAttribute]
        public int MarketingDepartmentId {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? RepairType {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? ProductLineType {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? ApplicationType {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? AutoApproveStatus {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? ProductType {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? ClaimSupplierId {
            get;
            set;
        }

        [DataMemberAttribute]
        public string RepairContractCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string RepairClaimApplicationCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string DealerCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string DealerName {
            get;
            set;
        }

        [DataMemberAttribute]
        public string RepairWorkOrder {
            get;
            set;
        }

        [DataMemberAttribute]
        public string DispatchingCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string TerraceName {
            get;
            set;
        }

        [DataMemberAttribute]
        public int PartsSalesCategoryId {
            get;
            set;
        }

        [DataMemberAttribute]
        public string PartsSalesCategoryName {
            get;
            set;
        }

        [DataMemberAttribute]
        public string ServiceProductLineName {
            get;
            set;
        }

        [DataMemberAttribute]
        public int ServiceProductLineId {
            get;
            set;
        }

        [DataMemberAttribute]
        public string ProductCategoryName {
            get;
            set;
        }

        [DataMemberAttribute]
        public DateTime? SalesDate {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? Mileage {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? Capacity {
            get;
            set;
        }

        [DataMemberAttribute]
        public DateTime? RepairRequestTime {
            get;
            set;
        }

        [DataMemberAttribute]
        public string VehicleContactPerson {
            get;
            set;
        }

        [DataMemberAttribute]
        public string ContactPhone {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? WorkingHours {
            get;
            set;
        }

        [DataMemberAttribute]
        public DateTime? OutOfFactoryDate {
            get;
            set;
        }

        [DataMemberAttribute]
        public string EngineModel {
            get;
            set;
        }

        [DataMemberAttribute]
        public string EngineSerialNumber {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? BridgeType {
            get;
            set;
        }

        [DataMemberAttribute]
        public string MalfunctionCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string MalfunctionDescription {
            get;
            set;
        }

        [DataMemberAttribute]
        public string FaultyPartsCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string FaultyPartsName {
            get;
            set;
        }

        [DataMemberAttribute]
        public string FaultyPartsSupplierCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string FaultyPartsSupplierName {
            get;
            set;
        }

        [DataMemberAttribute]
        public string MalfunctionReason {
            get;
            set;
        }

        [DataMemberAttribute]
        public string Remark {
            get;
            set;
        }

        [DataMemberAttribute]
        public string ServiceActivityCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? ServiceActivityType {
            get;
            set;
        }

        [DataMemberAttribute]
        public DateTime? ServiceActivityTime {
            get;
            set;
        }

        [DataMemberAttribute]
        public decimal MaterialCost {
            get;
            set;
        }

        [DataMemberAttribute]
        public decimal LaborCost {
            get;
            set;
        }

        [DataMemberAttribute]
        public decimal? PartsManagementCost {
            get;
            set;
        }

        [DataMemberAttribute]
        public decimal? TrimCost {
            get;
            set;
        }

        [DataMemberAttribute]
        public decimal? OtherCost {
            get;
            set;
        }

        [DataMemberAttribute]
        public string OtherCostReason {
            get;
            set;
        }

        [DataMemberAttribute]
        public decimal? TotalAmount {
            get;
            set;
        }

        [DataMemberAttribute]
        public string ClaimBillCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public int Status {
            get;
            set;
        }

        [DataMemberAttribute]
        public int SettlementStatus {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? SupplierSettleStatus {
            get;
            set;
        }

        [DataMemberAttribute]
        public string CreatorName {
            get;
            set;
        }

        [DataMemberAttribute]
        public DateTime? CreateTime {
            get;
            set;
        }

        [DataMemberAttribute]
        public string ModifierName {
            get;
            set;
        }

        [DataMemberAttribute]
        public DateTime? ModifyTime {
            get;
            set;
        }

        [DataMemberAttribute]
        public string ApproverName {
            get;
            set;
        }

        [DataMemberAttribute]
        public DateTime? ApproveTime {
            get;
            set;
        }

        [DataMemberAttribute]
        public string AbandonerName {
            get;
            set;
        }

        [DataMemberAttribute]
        public DateTime? AbandonTime {
            get;
            set;
        }

        [DataMemberAttribute]
        public string CancelReason {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? SupplierConfirmStatus {
            get;
            set;
        }

        [DataMemberAttribute]
        public string SupplierConfirmorName {
            get;
            set;
        }

        [DataMemberAttribute]
        public DateTime? SupplierConfirmTime {
            get;
            set;
        }

        [DataMemberAttribute]
        public string SupplierConfirmComment {
            get;
            set;
        }

        [DataMemberAttribute]
        public string SupplierCheckerName {
            get;
            set;
        }

        [DataMemberAttribute]
        public DateTime? SupplierCheckTime {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? SupplierCheckStatus {
            get;
            set;
        }

        [DataMemberAttribute]
        public string SupplierCheckComment {
            get;
            set;
        }

        [DataMemberAttribute]
        public bool CountRepairClaimMaterialDetail {
            get;
            set;
        }

        [DataMemberAttribute]
        public string RepairItemCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string RepairItemName {
            get;
            set;
        }

        [DataMemberAttribute]
        public double DefaultLaborHour {
            get;
            set;
        }

        [DataMemberAttribute]
        public decimal LaborUnitPrice {
            get;
            set;
        }

        [DataMemberAttribute]
        public decimal? RepairClaimItemDetailLaborCost {
            get;
            set;
        }

        [DataMemberAttribute]
        public string RepairClaimItemDetailRemark {
            get;
            set;
        }

        [DataMemberAttribute]
        public decimal? SumMaterialCost {
            get;
            set;
        }

        [DataMemberAttribute]
        public decimal? SumPartsManagementCost {
            get;
            set;
        }

        [DataMemberAttribute]
        public string UsedPartsCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string UsedPartsName {
            get;
            set;
        }

        [DataMemberAttribute]
        public string UsedPartsBarCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string UsedPartsSupplierName {
            get;
            set;
        }

        [DataMemberAttribute]
        public decimal UnitPrice {
            get;
            set;
        }

        [DataMemberAttribute]
        public int Quantity {
            get;
            set;
        }

        [DataMemberAttribute]
        public decimal RepairClaimMaterialDetailMaterialCost {
            get;
            set;
        }

        [DataMemberAttribute]
        public decimal? RepairClaimMaterialDetailPartsManagementCost {
            get;
            set;
        }

        [DataMemberAttribute]
        public int UsedPartsReturnPolicy {
            get;
            set;
        }

        [DataMemberAttribute]
        public int UsedPartsDisposalStatus {
            get;
            set;
        }

        [DataMemberAttribute]
        public string NewPartsCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string NewPartsName {
            get;
            set;
        }

        [DataMemberAttribute]
        public string NewPartsSecurityNumber {
            get;
            set;
        }

        [DataMemberAttribute]
        public string NewPartsSupplierName {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? PartSource {
            get;
            set;
        }

        [DataMemberAttribute]
        public string RepairClaimMaterialDetailRemark {
            get;
            set;
        }

        [DataMemberAttribute]
        public string VehicleCategoryName {
            get;
            set;
        }

        [DataMemberAttribute]
        public string FaultyPartsAssemblyCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string FaultyPartsAssemblyName {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? IsContainPhoto {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? UsedPartsDisposalStatusDetial {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? RepairObjectId {
            get;
            set;
        }

        [DataMemberAttribute]
        public string RepairObjectName {
            get;
            set;
        }
        [DataMemberAttribute]


        public int? SupplierCheckResult {
            get;
            set;
        }
        [DataMemberAttribute]


        public int? SupplierCheckNotP {
            get;
            set;
        }
        [DataMemberAttribute]


        public int? SupplierConfirmResult {
            get;
            set;
        }
        [DataMemberAttribute]

        public int? SupplierConfirmNotP {
            get;
            set;
        }
        [DataMemberAttribute]

        public string SupplierIdeaNotPassReson {
            get;
            set;
        }
    }
}
