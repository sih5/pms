﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    [DataContractAttribute(IsReference = true)]
    public class VirtualRepairClaimItemDetail : EntityObject {
        /// <summary>
        /// Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }

        /// <summary>
        /// 维修保养索赔单Id
        /// </summary>
        [DataMemberAttribute]
        public int RepairClaimBillId {
            get;
            set;
        }

        /// <summary>
        /// 维修单维修项目清单Id
        /// </summary>
        [DataMemberAttribute]
        public int? RepairOrderMaterialDetailId {
            get;
            set;
        }

        /// <summary>
        /// 维修项目Id
        /// </summary>
        [DataMemberAttribute]
        public int RepairItemId {
            get;
            set;
        }

        /// <summary>
        /// 维修项目编号
        /// </summary>
        [DataMemberAttribute]
        public string RepairItemCode {
            get;
            set;
        }

        /// <summary>
        /// 维修项目名称
        /// </summary>
        [DataMemberAttribute]
        public string RepairItemName {
            get;
            set;
        }

        /// <summary>
        /// 工时定额
        /// </summary>
        [DataMemberAttribute]
        public double DefaultLaborHour {
            get;
            set;
        }

        /// <summary>
        /// 工时单价
        /// </summary>
        [DataMemberAttribute]
        public Decimal LaborUnitPrice {
            get;
            set;
        }

        /// <summary>
        /// 工时费
        /// </summary>
        [DataMemberAttribute]
        public Decimal LaborCost {
            get;
            set;
        }

        /// <summary>
        /// 审核状态
        /// </summary>
        [DataMemberAttribute]
        public int ApproveStatus {
            get;
            set;
        }

        /// <summary>
        /// 是否自定义
        /// </summary>
        [DataMemberAttribute]
        public bool? IfSelfDefine {
            get;
            set;
        }

        /// <summary>
        /// 是否强制
        /// </summary>
        [DataMemberAttribute]
        public bool? IfObliged {
            get;
            set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        [DataMemberAttribute]
        public string Remark {
            get;
            set;
        }

        /// <summary>
        /// 维修工Id
        /// </summary>
        [DataMemberAttribute]
        public int? RepairWorkerId {
            get;
            set;
        }

        /// <summary>
        /// 维修工姓名
        /// </summary>
        [DataMemberAttribute]
        public string RepairWorkerName {
            get;
            set;
        }

        /// <summary>
        /// 配件管理费合计
        /// </summary>
        [DataMemberAttribute]
        public decimal TotalPartsManagementCost {
            get;
            set;
        }


        /// <summary>
        /// 材料费
        /// </summary>
        [DataMemberAttribute]
        public decimal MaterialCost
        {
            get;
            set;
        }

        /// <summary>
        /// 材料费合计
        /// </summary>
        [DataMemberAttribute]
        public decimal TotalMaterialCost {
            get;
            set;
        }

       /// <summary>
        /// 是否按工时单价索赔
        /// </summary>
        [DataMemberAttribute]
        public bool IfSPByLabor {
            get;
            set;
        }

        /// <summary>
        /// 工时系数
        /// </summary>
        [DataMemberAttribute]
        public double? LaborCoefficient
        {
            get;
            set;
        }
        /// <summary>
        /// 配件管理费率
        /// </summary>
        [DataMemberAttribute]
        public double? PartsClaimCoefficient
        {
            get;
            set;
        }

        /// <summary>
        /// 旧件运费系数
        /// </summary>
        [DataMemberAttribute]
        public double? OldPartTransCoefficient
        {
            get;
            set;
        }

        /// <summary>
        /// 索赔系数
        /// </summary>
        [DataMemberAttribute]
        public double Rate
        {
            get;
            set;
        }
    }
}

