﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class VirtualRepairClaimMaterialDetail : EntityObject {
        /// <summary>
        /// Id
        /// </summary>
        [Key]
        [DataMember]
        public int Id {
            get;
            set;
        }

        [DataMemberAttribute]
        public string UsedPartsCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string UsedPartsName {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? Quantity {
            get;
            set;
        }

        [DataMemberAttribute]
        public string ClaimBillCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string RepairContractCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public int Status {
            get;
            set;
        }

        [DataMemberAttribute]
        public string SerialNumber {
            get;
            set;
        }

        [DataMemberAttribute]
        public string ProductCategoryName {
            get;
            set;
        }

        [DataMemberAttribute]
        public string DealerCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string DealerName {
            get;
            set;
        }

        [DataMemberAttribute]
        public string ClaimSupplierCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string ClaimSupplierName {
            get;
            set;
        }

        [DataMemberAttribute]
        public DateTime? CreateTime {
            get;
            set;
        }

        [DataMemberAttribute]
        public decimal MaterialCost {
            get;
            set;
        }

        [DataMemberAttribute]
        public string MarketingDepartmentName {
            get;
            set;
        }

        [DataMemberAttribute]
        public int BranchId {
            get;
            set;
        }
    }
}
