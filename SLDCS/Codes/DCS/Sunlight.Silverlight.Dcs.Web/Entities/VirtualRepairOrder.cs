﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {

    [DataContract(IsReference = true)]
    public class VirtualRepairOrder : EntityObject {
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }
        [DataMemberAttribute]
        public int VehicleId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ServiceActivityCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ServiceActivityName {
            get;
            set;
        }

        [DataMemberAttribute]
        public string ServiceActivityContent {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? ServiceActivityType {
            get;
            set;
        }

        [DataMemberAttribute]
        public string Code {
            get;
            set;
        }
        [DataMemberAttribute]
        public int Status {
            get;
            set;
        }

        [DataMemberAttribute]
        public int DealerId {
            get;
            set;
        }

        [DataMemberAttribute]
        public string DealerCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Businesscode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string DealerName {
            get;
            set;
        }

        [DataMemberAttribute]
        public string CustomerCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CustomerName {
            get;
            set;
        }
        [DataMemberAttribute]
        public string MarketingDepartmentName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int RepairType {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? RejectStatus {
            get;
            set;
        }
        /// <summary>
        /// 车牌号
        /// </summary>
        [DataMemberAttribute]
        public string VehicleLicensePlate {
            get;
            set;
        }

        /// <summary>
        /// 出厂编号
        /// </summary>
        [DataMemberAttribute]
        public string SerialNumber {
            get;
            set;
        }
        /// <summary>
        /// 车主
        /// </summary>
        [DataMemberAttribute]
        public string RetainedCustomer {
            get;
            set;
        }
        /// <summary>
        /// 车主电话
        /// </summary>
        [DataMemberAttribute]
        public string RetainedCustomerPhone {
            get;
            set;
        }
        /// <summary>
        ///联系人
        /// </summary>
        [DataMemberAttribute]
        public string VehicleContactPerson {
            get;
            set;
        }
        /// <summary>
        /// 联系人电话
        /// </summary>
        [DataMemberAttribute]
        public string ContactPhone {
            get;
            set;
        }
        [DataMemberAttribute]
        public string VIN {
            get;
            set;
        }
        [DataMemberAttribute]
        public string BranchCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string BranchName {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ProductLineName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int ProductLineId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartsSalesCategoryName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int PartsSalesCategoryId {
            get;
            set;
        }

        [DataMemberAttribute]
        public decimal TotalAmount {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal LaborCost {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal MaterialCost {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal PartsManagementCost {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal OtherCost {
            get;
            set;
        }
        [DataMemberAttribute]
        public string VideoMobileNumber {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? MVSOutRange {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? MVSOutTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? MVSOutDistance {
            get;
            set;
        }
        [DataMemberAttribute]
        public string MVSOutCoordinate {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CreatorName {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? CreateTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ModifierName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int MarketDepartmentId {
            get;
            set;
        }

        [DataMemberAttribute]
        public DateTime? ModifyTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public string RepairFinishName {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? RepairFinishTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public string AbandonerName {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? AbandonTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public int BranchId {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? WarrantyStatus {
            get;
            set;
        }

        [DataMemberAttribute]
        public string SysCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string MemberCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string MemberName {
            get;
            set;
        }
        [DataMemberAttribute]
        public string MemberPhone {
            get;
            set;
        }
        [DataMemberAttribute]
        public string IDType {
            get;
            set;
        }
        [DataMemberAttribute]
        public string IDNumer {
            get;
            set;
        }
        [DataMemberAttribute]
        public string MemberRank {
            get;
            set;
        }
        /// <summary>
        /// 是否包含APP照片
        /// </summary>
        [DataMemberAttribute]
        public int? IsContainPhoto {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? RepairClaimBillCreateTime {
            get;
            set;
        }
        /// <summary>
        /// 维修对象
        /// </summary>
        [DataMemberAttribute]
        public string RepairTarget {
            get;
            set;
        }
        /// <summary>
        /// 销售大区-大区名称
        /// </summary>
        [DataMemberAttribute]
        public string RegionName {
            get;
            set;
        }

        /// <summary>
        /// 服务产品线Id
        /// </summary>
        [DataMemberAttribute]
        public int ServiceProductLineId {
            get;
            set;
        }

        /// <summary>
        /// 二级站编号
        /// </summary>
        [DataMemberAttribute]
        public string FirstClassStationCode {
            get;
            set;
        }

        /// <summary>
        /// 二级站名称
        /// </summary>
        [DataMemberAttribute]
        public string FirstClassStationName {
            get;
            set;
        }

        /// <summary>
        /// 生成索赔时间
        /// </summary>
        [DataMemberAttribute]
        public DateTime? RepairClaimBillCreateTime1 {
            get;
            set;
        }
        [DataMemberAttribute]
        public bool? IsOutPurOrSale {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? MemberHandStatus {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? BonusPointsStatus {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? FrozenStatus {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal FieldServiceCharge {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal? DeductionTotalAmount {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal? RedPacketsUsedPrice {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal? MemberUsedPrice {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal TowCharge {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal OtherTrafficFee {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? OutRange {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal TrimCost {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? RepairWorkOrderId {
            get;
            set;
        }

        [DataMemberAttribute]
        public string RepairWorkOrder {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? VehicleLocationCheck {
            get;
            set;
        }
    }
}
