﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 虚拟维修单故障清单
    /// </summary>
    [DataContract(IsReference = true)]
    public class VirtualRepairOrderFaultReason : EntityObject {

        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }
        [DataMemberAttribute]
        public string VehicleLicensePlate {
            get;
            set;
        }
        [DataMemberAttribute]
        public string VIN {
            get;
            set;
        }
        [DataMemberAttribute]
        public int VehicleId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string RepairTarget {
            get;
            set;
        }
        [DataMemberAttribute]
        public string BranchName {
            get;
            set;
        }

        [DataMemberAttribute]
        public int RepairType {
            get;
            set;
        }
        [DataMemberAttribute]
        public int Status {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Code {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? RejectStatus {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? CreateTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public string FaultyPartsCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string FaultyPartsName {
            get;
            set;
        }
    }
}
