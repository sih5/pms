﻿using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web.Entities {
    /// <summary>
    /// 虚拟维修单维修材料清单
    /// </summary>
    [DataContract(IsReference = true)]
    public class VirtualRepairOrderMaterialDetail : EntityObject {
        /// <summary>
        /// 总实收工时费
        /// </summary>
        [DataMemberAttribute]
        public decimal TotalActualLaborCost {
            get;
            set;
        }

        /// <summary>
        /// 总实收材料费
        /// </summary>
        [DataMemberAttribute]
        public decimal TotalActuralMaterialCost {
            get;
            set;
        }

        /// <summary>
        /// 其他费用
        /// </summary>
        [DataMemberAttribute]
        public decimal OtherCost {
            get;
            set;
        }

        /// <summary>
        /// 外出服务费
        /// </summary>
        [DataMemberAttribute]
        public decimal FieldServiceCharge {
            get;
            set;
        }

        /// <summary>
        /// 总费用
        /// </summary>
        [DataMemberAttribute]
        public decimal Total {
            get;
            set;
        }


        /// <summary>
        /// 权益抵扣
        /// </summary>
        [DataMemberAttribute]
        public decimal MemberRightsCost {
            get;
            set;
        }

        /// <summary>
        /// 工时折扣
        /// </summary>
        [DataMemberAttribute]
        public decimal CostDiscount {
            get;
            set;
        }
        /// <summary>
        /// 积分折扣折扣
        /// </summary>
        [DataMemberAttribute]
        public decimal MemberUsedIntegral {
            get;
            set;
        }
        /// <summary>
        /// 折扣总费用
        /// </summary>
        [DataMemberAttribute]
        public decimal TotalMember {
            get {
                return MemberUsedIntegral + CostDiscount + MemberRightsCost;
            }
        }

        [DataMemberAttribute]
        public decimal DeductionTotalAmount {
            get;
            set;
        }
        /// <summary>
        /// 红包抵扣费用
        /// </summary>
        [DataMemberAttribute]
        public decimal RedPacketsUsedPrice {
            get;
            set;
        }
    }
}
