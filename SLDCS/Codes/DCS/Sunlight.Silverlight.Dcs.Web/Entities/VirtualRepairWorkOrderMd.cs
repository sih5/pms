﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 虚拟维修工单
    /// </summary>
    [DataContract(IsReference = true)]
    public class VirtualRepairWorkOrderMd : EntityObject {
        /// <summary>
        /// Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Srnum {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ActiviteId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ChewId {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? Type {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? SubType {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? Status {
            get;
            set;
        }
        [DataMemberAttribute]
        public string VinCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string DetailType {
            get;
            set;
        }

        [DataMemberAttribute]
        public string VehicleLicensePlate {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartsSalesCategoryName {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SubBrand {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? OutOfFactoryDate {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? SalesDate {
            get;
            set;
        }
        [DataMemberAttribute]
        public string DriveMile {
            get;
            set;
        }
        [DataMemberAttribute]
        public string AccountName {
            get;
            set;
        }
        [DataMember]
        public string ContactName {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ContactCellPhone {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? UrgencyLevel {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Description {
            get;
            set;
        }
        [DataMemberAttribute]
        public bool IsUsed {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Expect {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? ExpectTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SourceChannel {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? SendTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? ProcessMode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartsCodeName {
            get;
            set;
        }
        [DataMemberAttribute]
        public string HandlePerson {
            get;
            set;
        }
        [DataMemberAttribute]
        public string RepairOrderCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartsSalesOrderCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SalesCategoryName {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? PartArriveTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? PartsSourceChannel {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ProcessContent {
            get;
            set;
        }
        [DataMemberAttribute]
        public string FalutGround {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? IsCanFinish {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? CannotHandleReason {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? RepairCategory {
            get;
            set;
        }
        [DataMemberAttribute]
        public int MarketDepartmentId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string MarketingDepartmentName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int BranchId {
            get;
            set;
        }
        [DataMemberAttribute]
        public int DealerId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string DealerCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string DealerName {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? ReceiveTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ReceiverName {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? RejectTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public string RejectorName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? RefuseReason {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? ArriveStationTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ArriveStationerName {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? LeavingTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public string LeavingName {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? ArriveTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ArriverName {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? StartRepairTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public string StartRepairName {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? FimishTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public string FimishName {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? RevokeTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public string RevokeName {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? IsClickTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public int PartsSalesCategoryId {
            get;
            set;
        }
    }
}
