﻿using System;
using System.Data.Objects.DataClasses;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class VirtualSIHCreditInfoDetail : EntityObject {
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }

        [DataMemberAttribute]
        public int MarketingDepartmentId {
            get;
            set;
        }

        [DataMemberAttribute]
        public string MarketingDepartmentCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string MarketingDepartmentName {
            get;
            set;
        }
        [Key]
        [DataMemberAttribute]
        public int CompanyId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CompanyCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string CompanyName {
            get;
            set;
        }

        [DataMemberAttribute]
        public int DealerId {
            get;
            set;
        }

        [DataMemberAttribute]
        public string DealerCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string DealerName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int AccountGroupId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string AccountGroupCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string AccountGroupName {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal? SIHCredit {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal? SIHCreditZB {
            get;
            set;
        }

        [DataMemberAttribute]
        public DateTime? ValidationTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? ExpireTime {
            get;
            set;
        }
    }
}
