﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Sunlight.Silverlight.Dcs.Web
{
    [DataContract(IsReference = true)]
    public class VirtualSYNCWMSINOUTLOGINFO
    {
        [Key]
        [DataMemberAttribute]
        public int OBJID {
            get;
            set;
        }

        [DataMemberAttribute]
        public DateTime? SYNCDATE {
            get;
            set;
        }

        [DataMemberAttribute]
        public string CODE {
            get;
            set;
        }
             
        [DataMemberAttribute]
        public int? SYNCTYPE {
            get;
            set;
        }

        public string SYNCTYPENAME
        {
            get;
            set;
        }  
        
        [DataMemberAttribute]
        public string SYNCCODE {
            get;
            set;
        }  
        
        [DataMemberAttribute]
        public string INOUTCODE {
            get;
            set;
        }  
       
        [DataMemberAttribute]
        public int? STATUS {
            get;
            set;
        }

        public string STATUSNAME
        {
            get;
            set;
        }


        [DataMemberAttribute]
        public string WareHousename
        {
            get;
            set;
        }    

    }
}
