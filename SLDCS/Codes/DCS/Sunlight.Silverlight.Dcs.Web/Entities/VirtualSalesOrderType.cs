﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;


namespace Sunlight.Silverlight.Dcs.Web{
    /// <summary>
    /// 虚拟订单类型
    /// </summary>
    [DataContract(IsReference = true)]
    public class VirtualSalesOrderType : EntityObject {
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Code {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Name {
            get;
            set;
        }
        /// <summary>
        /// 配件销售类型Id
        /// </summary>
        [DataMemberAttribute]
        public int PartsSalesCategoryId {
            get;
            set;
        }

    }
}
