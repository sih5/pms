﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class VirtualSapPaymentInfo : EntityObject {
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CompanyCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? FiscalYear {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? FiscalPeriod {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? VoucherCreateTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? TallyTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public string MdmSupplierCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string MdmCustomerCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartsSalesCategoryCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string GeneralLedger {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CurrencyCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal? Amount {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Item1 {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Item2 {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CreatorName {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Summary {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PaymentMethod {
            get;
            set;
        }
        [DataMemberAttribute]
        public string BillNumber {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? BillRowNumber {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? IntRunDate {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? IntRunTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? IsPassBack {
            get;
            set;
        }
        [DataMemberAttribute]
        public string TallyRecordNumber {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ErrorMessage {
            get;
            set;
        }
        [DataMemberAttribute]
        public string TallyMark {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? ReceiveTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? HandleStatus {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? HandleTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public string HandleMessage {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? PartsSalesCategoryId {
            get;
            set;
        }



        [DataMemberAttribute]
        public string PartsSalesCategoryName {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? PMSCustomerId {
            get;
            set;
        }

        [DataMemberAttribute]
        public string PMSCustomerName {
            get;
            set;
        }

        [DataMemberAttribute]
        public string PMSCustomerCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? PMSSupplierId {
            get;
            set;
        }

        [DataMemberAttribute]
        public string PMSSupplierName {
            get;
            set;
        }

        [DataMemberAttribute]
        public string PMSSupplierCode {
            get;
            set;
        }


    }
}
