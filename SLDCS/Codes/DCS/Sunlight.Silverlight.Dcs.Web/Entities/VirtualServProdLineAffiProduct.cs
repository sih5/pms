﻿using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class VirtualServProdLineAffiProduct : EntityObject {
        [Key]
        [DataMemberAttribute]
        public int VehicleId {
            get;
            set;
        }

        [Key]
        [DataMember]
        public int ServiceProductLineId {
            get;
            set;
        }
        [DataMember]
        public bool IsSplit {
            get;
            set;
        }

    }
}
