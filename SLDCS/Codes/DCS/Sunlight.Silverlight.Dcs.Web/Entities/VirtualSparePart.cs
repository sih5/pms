﻿using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;
using Sunlight.Silverlight.Dcs.Web.Resources;
using System;

namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class VirtualSparePart : EntityObject {
        /// <summary>
        /// 虚拟配件Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }

        /// <summary>
        /// 配件营销信息名称
        /// </summary>
        [DataMemberAttribute]
        public string PartsBranchName {
            get;
            set;
        }

        /// <summary>
        /// 品牌名称
        /// </summary>
        [DataMemberAttribute, Display(ResourceType = typeof(EntityStrings), Name = "PartsSalesCategory_Name")]
        public string PartsSalesCategoryName {
            get;
            set;
        }

        /// <summary>
        /// 品牌Id
        /// </summary>
        [DataMemberAttribute]
        public int? PartsSalesCategoryId {
            get;
            set;
        }

        /// <summary>
        /// 配件编号
        /// </summary>
        [DataMemberAttribute, Display(ResourceType = typeof(EntityStrings), Name = "SparePart_Code")]
        public string Code {
            get;
            set;
        }

        /// <summary>
        /// 配件名称
        /// </summary>
        [DataMemberAttribute, Display(ResourceType = typeof(EntityStrings), Name = "SparePart_Name")]
        public string Name {
            get;
            set;
        }

        /// <summary>
        /// 配件营销信息配件参图号
        /// </summary>
        [DataMemberAttribute]
        public string ReferenceCode {
            get;
            set;
        }

        /// <summary>
        /// 特性
        /// </summary>
        [DataMemberAttribute]
        public string Feature {
            get;
            set;
        }

        /// <summary>
        /// 配件供应商编号
        /// </summary>
        [DataMemberAttribute]
        public string PartsSupplierCode {
            get;
            set;
        }

        /// <summary>
        /// 配件供应商名称
        /// </summary>
        [DataMemberAttribute]
        public string PartsSupplierName {
            get;
            set;
        }

        /// <summary>
        /// 供应商配件图号
        /// </summary>
        [DataMemberAttribute]
        public string SupplierPartCode {
            get;
            set;
        }

        /// <summary>
        /// 供应商配件名称
        /// </summary>
        [DataMemberAttribute]
        public string SupplierPartName {
            get;
            set;
        }

        /// <summary>
        /// 采购价
        /// </summary>
        [DataMemberAttribute]
        public decimal PurchasePrice {
            get;
            set;
        }

        /// <summary>
        /// 采购价
        /// </summary>
        [DataMemberAttribute]
        public int? LimitQty {
            get;
            set;
        }

        /// <summary>
        /// 采购价
        /// </summary>
        [DataMemberAttribute]
        public int? UsedQty {
            get;
            set;
        }

        /// <summary>
        /// 计量单位
        /// </summary>
        [DataMemberAttribute]
        public string MeasureUnit {
            get;
            set;
        }

        /// <summary>
        /// 规格
        /// </summary>
        [DataMemberAttribute]
        public string Specification {
            get;
            set;
        }

        /// <summary>
        /// 状态
        /// </summary>
        [DataMemberAttribute]
        public int Status {
            get;
            set;
        }
        /// <summary>
        /// 包装数量
        /// </summary>
        [DataMemberAttribute]
        public int? PackingAmount {
            get;
            set;
        }

        /// <summary>
        /// 包装规格
        /// </summary>
        [DataMemberAttribute]
        public string PackingSpecification {
            get;
            set;
        }
        
        /// <summary>
        /// 是否可销售
        /// </summary>
        [DataMemberAttribute, Display(ResourceType = typeof(EntityStrings), Name = "PartsBranch_IsSalable")]
        public bool? IsSalable {
            get;
            set;
        }

        /// <summary>
        /// 是否可采购
        /// </summary>
        [DataMemberAttribute, Display(ResourceType = typeof(EntityStrings), Name = "PartsBranch_IsOrderable")]
        public bool IsOrderable {
            get;
            set;
        }

        /// <summary>
        /// 海外配件图号
        /// </summary>
        [DataMemberAttribute]
        public string OverseasPartsFigure {
            get;
            set;
        }

        /// <summary>
        /// CAD图号
        /// </summary>
        [DataMemberAttribute]
        public string CADCode {
            get;
            set;
        }

        /// <summary>
        /// 配件类型
        /// </summary>
        [DataMemberAttribute]
        public int? PartType {
            get;
            set;
        }

        /// <summary>
        /// 创建时间
        /// </summary>
        [DataMemberAttribute]
        public DateTime? CreateTime {
            get;
            set;
        }

        /// <summary>
        /// 修改时间
        /// </summary>
        [DataMemberAttribute]
        public DateTime? ModifyTime {
            get;
            set;
        }

        /// <summary>
        /// 配件主包装数量
        /// </summary>
        [DataMemberAttribute]
        public int? PackingCoefficient {
            get;
            set;
        }
        /// <summary>
        /// 配件ABC分类
        /// </summary>
        [DataMemberAttribute]
        public int? PartABC {
            get;
            set;
        }
        /// <summary>
        /// 服务站价
        /// </summary>
         [DataMemberAttribute]
        public decimal SalesPrice {
            get;
            set;
        }
         /// <summary>
         /// 中心库价
         /// </summary>
         [DataMemberAttribute]
         public decimal CenterPrice {
             get;
             set;
         }
         /// <summary>
         /// 零售价
         /// </summary>
         [DataMemberAttribute]
         public decimal RetailGuidePrice {
             get;
             set;
         }
        //价格类型
        [DataMemberAttribute]
         public string PriceTypeName {
             get;
             set;
         }
    }
}
