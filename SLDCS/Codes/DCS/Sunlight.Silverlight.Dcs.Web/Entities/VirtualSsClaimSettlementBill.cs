﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 服务站索赔结算单
    /// </summary>
    [DataContract(IsReference = true)]
    public class VirtualSsClaimSettlementBill : EntityObject {
        /// <summary>
        /// 服务站索赔结算单Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }
        /// <summary>
        /// 关联单据Id
        /// </summary>
        [DataMemberAttribute]
        public int? RelatedId {
            get;
            set;
        }
        /// <summary>
        /// 结算类型 DealerSettleType
        /// </summary>
        [DataMemberAttribute]
        public int? SettleType {
            get;
            set;
        }
        /// <summary>
        /// 结算方式  DealerSettleMethods
        /// </summary>
        [DataMemberAttribute]
        public int? SettleMethods {
            get;
            set;
        }
        /// <summary>
        /// 是否已开票
        /// </summary>
        [DataMemberAttribute]
        public bool IfInvoiced {
            get;
            set;
        }
        /// <summary>
        /// 状态
        /// </summary>
        [DataMemberAttribute]
        public int Status {
            get;
            set;
        }
        /// <summary>
        /// 结算单编号
        /// </summary>
        [DataMemberAttribute]
        public string Code {
            get;
            set;
        }
        /// <summary>
        /// 服务站Id
        /// </summary>
        [DataMemberAttribute]
        public int DealerId {
            get;
            set;
        }
        /// <summary>
        /// 服务站编号
        /// </summary>
        [DataMemberAttribute]
        public string DealerCode {
            get;
            set;
        }
        /// <summary>
        /// 服务站名称
        /// </summary>
        [DataMemberAttribute]
        public string DealerName {
            get;
            set;
        }
        /// <summary>
        /// 服务站名称
        /// </summary>
        [DataMemberAttribute]
        public string BusinessCode {
            get;
            set;
        }
        /// <summary>
        /// 营销分公司Id
        /// </summary>
        [DataMemberAttribute]
        public int BranchId {
            get;
            set;
        }
        /// <summary>
        /// 营销分公司编号
        /// </summary>
        [DataMemberAttribute]
        public string BranchCode {
            get;
            set;
        }
        /// <summary>
        /// 营销分公司名称
        /// </summary>
        [DataMemberAttribute]
        public string BranchName {
            get;
            set;
        }
        /// <summary>
        /// 配件销售类型Id
        /// </summary>
        [DataMemberAttribute]
        public int PartsSalesCategoryId {
            get;
            set;
        }
        /// <summary>
        /// 服务产品线Id
        /// </summary>
        [DataMemberAttribute]
        public int? ServiceProductLineId {
            get;
            set;
        }
        /// <summary>
        /// 产品线类型
        /// </summary>
        [DataMemberAttribute]
        public int? ProductLineType {
            get;
            set;
        }
        /// <summary>
        /// 结算开始时间
        /// </summary>
        [DataMemberAttribute]
        public DateTime? SettlementStartTime {
            get;
            set;
        }
        /// <summary>
        /// 结算截止时间
        /// </summary>
        [DataMemberAttribute]
        public DateTime? SettlementEndTime {
            get;
            set;
        }
        /// <summary>
        /// 工时费
        /// </summary>
        [DataMemberAttribute]
        public decimal LaborCost {
            get;
            set;
        }
        /// <summary>
        /// 材料费
        /// </summary>
        [DataMemberAttribute]
        public decimal MaterialCost {
            get;
            set;
        }
        /// <summary>
        /// 配件管理费
        /// </summary>
        [DataMemberAttribute]
        public decimal PartsManagementCost {
            get;
            set;
        }
        /// <summary>
        /// 外出费用
        /// </summary>
        [DataMemberAttribute]
        public decimal FieldServiceExpense {
            get;
            set;
        }
        /// <summary>
        /// 扣款金额
        /// </summary>
        [DataMemberAttribute]
        public decimal DebitAmount {
            get;
            set;
        }
        /// <summary>
        /// 补款金额
        /// </summary>
        [DataMemberAttribute]
        public decimal ComplementAmount {
            get;
            set;
        }
        /// <summary>
        /// 售前检查费用
        /// </summary>
        [DataMemberAttribute]
        public decimal? PreSaleAmount {
            get;
            set;
        }
        /// <summary>
        /// 其他费用
        /// </summary>
        [DataMemberAttribute]
        public decimal OtherCost {
            get;
            set;
        }
        /// <summary>
        /// 旧件激励金额
        /// </summary>
        [DataMemberAttribute]
        public decimal? UsedPartsEncourageAmount {
            get;
            set;
        }

        [DataMemberAttribute]
        public string PartsSalesCategoryName {
            get;
            set;
        }

        [DataMemberAttribute]
        public string ServiceProductLineName {
            get;
            set;
        }
        /// <summary>
        /// 费用合计
        /// </summary>
        [DataMemberAttribute]
        public decimal TotalAmount {
            get;
            set;
        }
        /// <summary>
        /// 税率
        /// </summary>
        [DataMemberAttribute]
        public double? TaxRate {
            get;
            set;
        }
        /// <summary>
        /// 开票差异金额
        /// </summary>
        [DataMemberAttribute]
        public decimal? InvoiceAmountDifference {
            get;
            set;
        }
        /// <summary>
        /// 备注
        /// </summary>
        [DataMemberAttribute]
        public string Remark {
            get;
            set;
        }
        /// <summary>
        /// 创建人Id
        /// </summary>
        [DataMemberAttribute]
        public int? CreatorId {
            get;
            set;
        }
        /// <summary>
        /// 创建人
        /// </summary>
        [DataMemberAttribute]
        public string CreatorName {
            get;
            set;
        }
        /// <summary>
        /// 创建时间
        /// </summary>
        [DataMemberAttribute]
        public DateTime? CreateTime {
            get;
            set;
        }
        /// <summary>
        /// 修改人Id
        /// </summary>
        [DataMemberAttribute]
        public int? ModifierId {
            get;
            set;
        }
        /// <summary>
        /// 修改人
        /// </summary>
        [DataMemberAttribute]
        public string ModifierName {
            get;
            set;
        }
        /// <summary>
        /// 修改时间
        /// </summary>
        [DataMemberAttribute]
        public DateTime? ModifyTime {
            get;
            set;
        }
        /// <summary>
        /// 审批人Id
        /// </summary>
        [DataMemberAttribute]
        public int? ApproverId {
            get;
            set;
        }
        /// <summary>
        /// 审批人
        /// </summary>
        [DataMemberAttribute]
        public string ApproverName {
            get;
            set;
        }
        /// <summary>
        /// 审批时间
        /// </summary>
        [DataMemberAttribute]
        public DateTime? ApproveTime {
            get;
            set;
        }
        /// <summary>
        /// 作废人Id
        /// </summary>
        [DataMemberAttribute]
        public int? AbandonerId {
            get;
            set;
        }
        /// <summary>
        /// 作废人
        /// </summary>
        [DataMemberAttribute]
        public string AbandonerName {
            get;
            set;
        }
        /// <summary>
        /// 作废时间
        /// </summary>
        [DataMemberAttribute]
        public DateTime? AbandonTime {
            get;
            set;
        }

        /// <summary>
        /// 市场部名称
        /// </summary>
        [DataMemberAttribute]
        public string DepartName {
            get;
            set;
        }
    }
}
