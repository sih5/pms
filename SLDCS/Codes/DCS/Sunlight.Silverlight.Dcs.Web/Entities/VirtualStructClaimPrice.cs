﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class VirtualStructClaimPrice : EntityObject {
        /// <summary>
        /// 配件销售订单Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int PartsSalesOrderId {
            get;
            set;
        }

        /// <summary>
        /// 配件销售订单编号
        /// </summary>
        [DataMemberAttribute]
        public string PartsSalesOrderCode {
            get;
            set;
        }

        /// <summary>
        /// 配件保修分类Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int? PartsWarrantyCategoryId {
            get;
            set;
        }

        /// <summary>
        /// 配件保修分类名称
        /// </summary>
        [DataMemberAttribute]
        public string PartsWarrantyCategoryName {
            get;
            set;
        }

        /// <summary>
        /// 配件Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int SparePartId {
            get;
            set;
        }

        /// <summary>
        /// 配件编号
        /// </summary>
        [DataMemberAttribute]
        public string SparePartCode {
            get;
            set;
        }

        /// <summary>
        /// 配件名称
        /// </summary>
        [DataMemberAttribute]
        public string SparePartName {
            get;
            set;
        }

        /// <summary>
        /// 供应商Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int SupplierId {
            get;
            set;
        }

        /// <summary>
        /// 供应商编号
        /// </summary>
        [DataMemberAttribute]
        public string SupplierCode {
            get;
            set;
        }

        /// <summary>
        /// 供应商名称
        /// </summary>
        [DataMemberAttribute]
        public string SupplierName {
            get;
            set;
        }

        /// <summary>
        /// 销售价
        /// </summary>
        [DataMemberAttribute]
        public decimal SalesPrice {
            get;
            set;
        }

        /// <summary>
        /// 旧件返回政策
        /// </summary>
        [DataMemberAttribute]
        public int PartsReturnPolicy {
            get;
            set;
        }

        /// <summary>
        /// 销售组织Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int SalesUnitId {
            get;
            set;
        }

        /// <summary>
        /// 销售组织名称
        /// </summary>
        [DataMemberAttribute]
        public string SalesUnitName {
            get;
            set;
        }

        /// <summary>
        /// 配件销售类型Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int PartsSalesCategoryId {
            get;
            set;
        }

        /// <summary>
        /// 配件销售类型名称
        /// </summary>
        [DataMemberAttribute]
        public string PartsSalesCategoryName {
            get;
            set;
        }

        /// <summary>
        /// 营销分公司Id 
        /// </summary>
        [DataMemberAttribute]
        public int BranchId {
            get;
            set;
        }

        /// <summary>
        /// 营销分公司名称
        /// </summary>
        [DataMemberAttribute]
        public string BranchName {
            get;
            set;
        }

        /// <summary>
        /// 创建时间
        /// </summary>
        [DataMemberAttribute]
        public DateTime? CreateTime {
            get;
            set;
        }

    }
}
