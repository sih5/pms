﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 虚拟索赔单（索赔结算源单据）
    /// </summary>
    [DataContract(IsReference = true)]
    public class VirtualSupplierClaimsSettleSourceBill : EntityObject {
        /// <summary>
        /// 源单据Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int SourceBillId {
            get;
            set;
        }

        /// <summary>
        /// 源单据类型（服务站索赔结算源单据类型）
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int SourceBillType {
            get;
            set;
        }

        /// <summary>
        /// 源单据编号
        /// </summary>
        [DataMemberAttribute]
        public string SourceBillCode {
            get;
            set;
        }

        /// <summary>
        /// 营销分公司Id
        /// </summary>
        [DataMemberAttribute]
        public int BranchId {
            get;
            set;
        }

        /// <summary>
        ///  营销分公司编号
        /// </summary>
        [DataMemberAttribute]
        public string BranchCode {
            get;
            set;
        }

        /// <summary>
        ///  营销分公司名称
        /// </summary>
        [DataMemberAttribute]
        public string BranchName {
            get;
            set;
        }

        /// <summary>
        ///  供应商Id
        /// </summary>
        [DataMemberAttribute]
        public int SupplierId {
            get;
            set;
        }

        /// <summary>
        ///  供应商编号
        /// </summary>
        [DataMemberAttribute]
        public string SupplierCode {
            get;
            set;
        }

        /// <summary>
        ///  供应商名称
        /// </summary>
        [DataMemberAttribute]
        public string SupplierName {
            get;
            set;
        }

        /// <summary>
        ///  工时费
        /// </summary>
        [DataMemberAttribute]
        public decimal LaborCost {
            get;
            set;
        }

        /// <summary>
        ///  材料费
        /// </summary>
        [DataMemberAttribute]
        public decimal MaterialCost {
            get;
            set;
        }

        /// <summary>
        ///  配件管理费
        /// </summary>
        [DataMemberAttribute]
        public decimal PartsManagementCost {
            get;
            set;
        }

        /// <summary>
        ///  外出费用
        /// </summary>
        [DataMemberAttribute]
        public decimal FieldServiceExpense {
            get;
            set;
        }

        /// <summary>
        ///  扣款金额
        /// </summary>
        [DataMemberAttribute]
        public decimal DebitAmount {
            get;
            set;
        }

        /// <summary>
        ///  补款金额
        /// </summary>
        [DataMemberAttribute]
        public decimal ComplementAmount {
            get;
            set;
        }

        /// <summary>
        ///  其他费用
        /// </summary>
        [DataMemberAttribute]
        public decimal OtherCost {
            get;
            set;
        }

        /// <summary>
        ///  费用总和
        /// </summary>
        [DataMemberAttribute]
        public decimal TotalAmount {
            get;
            set;
        }


        /// <summary>
        ///  创建时间
        /// </summary>
        [DataMemberAttribute]
        public DateTime? CreateTime {
            get;
            set;
        }

    }
}
