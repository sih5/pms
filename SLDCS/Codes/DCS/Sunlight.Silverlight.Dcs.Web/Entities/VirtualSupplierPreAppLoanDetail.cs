﻿
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class VirtualSupplierPreAppLoanDetail : EntityObject {

        [Key]
        [DataMemberAttribute]
        public int SupplierId {
            get;
            set;
        }


        [DataMemberAttribute]
        public string SupplierCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string SupplierCompanyCode {
            get;
            set;
        }



        [DataMemberAttribute]
        public string SupplierCompanyName {
            get;
            set;
        }

        [DataMemberAttribute]
        public decimal PlannedAmountOwed {
            get;
            set;
        }

        [DataMemberAttribute]
        public decimal ActualDebt {
            get;
            set;
        }

        [DataMemberAttribute]
        public string BankName {
            get;
            set;
        }

        [DataMemberAttribute]
        public string BankCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public decimal ExceedAmount {
            get;
            set;
        }
    }
}
