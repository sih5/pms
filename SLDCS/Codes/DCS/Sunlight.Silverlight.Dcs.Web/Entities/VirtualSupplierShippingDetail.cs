﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;
namespace Sunlight.Silverlight.Dcs.Web
{
    [DataContract(IsReference = true)]
    public class VirtualSupplierShippingDetail : EntityObject
    {
        [Key]
        [DataMemberAttribute]
        public int Id
        {
            get;
            set;
        }
         [DataMemberAttribute]
        public string ReferenceCode
        {
            get;
            set;
        }
         [DataMemberAttribute]
         public string SupplierPartCode
         {
             get;
             set;
         }
         [DataMemberAttribute]
         public string SparePartName
         {
             get;
             set;
         }
         [DataMemberAttribute]
         public string SparePartCode
         {
             get;
             set;
         }
         [DataMemberAttribute]
         public int Quantity
         {
             get;
             set;
         }
         [DataMemberAttribute]
         public string MeasureUnit
         {
             get;
             set;
         }
         [DataMemberAttribute]
         public string Code
         {
             get;
             set;
         }
         [DataMemberAttribute]
         public int PrintNumber
         {
             get;
             set;
         }
         [DataMemberAttribute]
         public int PackingCoefficient
         {
             get;
             set;
         }
         [DataMemberAttribute]
         public int SupplierShippingOrderId
         {
             get;
             set;
         }
        [DataMemberAttribute]
         public int SparePartId
         {
             get;
             set;
         }
        [DataMemberAttribute]
        public string BachMunber
        {
            get;
            set;
        }
        //序列号
        [DataMemberAttribute]
        public int SerialNumber
        {
            get;
            set;
        }

        [DataMemberAttribute]
        public string FirMeasureUnit
        {
            get;
            set;
        }

        [DataMemberAttribute]
        public string SecMeasureUnit
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ThidMeasureUnit
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? FirPrintNumber
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? SecPrintNumber
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? ThidPrintNumber
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? FirPackingCoefficient
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? SecPackingCoefficient
        {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? ThidPackingCoefficient
        {
            get;
            set;
        }
		
		        //计划量
        [DataMemberAttribute]
        public int PlanAmount
		{
            get;
            set;
        }
        //待运量
        [DataMemberAttribute]
        public int WaitShippingAmount
		{
            get;
            set;
        }
		[DataMemberAttribute]
        public int? ConfirmedAmount
		{
            get;
            set;
        }
		[DataMemberAttribute]
        public string Remark
		{
            get;
            set;
        }
		[DataMemberAttribute]
        public string SpareOrderRemark
		{
            get;
            set;
        }
        [DataMemberAttribute]
        public double? Weight {
            get;
            set;
        }
        [DataMemberAttribute]
        public double? Volume {
            get;
            set;
        }
    }
}
