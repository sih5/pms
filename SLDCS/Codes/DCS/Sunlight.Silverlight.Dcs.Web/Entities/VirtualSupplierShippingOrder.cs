﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web.Entities {
    [DataContract(IsReference = true)]
    public class VirtualSupplierShippingOrder : EntityObject {
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Code {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ReceivingCompanyName {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ReceivingWarehouseName {
            get;
            set;
        }
        //收货人
        [DataMemberAttribute]
        public string ContactPerson {
            get;
            set;
        }
        //收货人联系电话
        [DataMemberAttribute]
        public string ContactPhone {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ReceivingAddress {
            get;
            set;
        }
        //品种
        [DataMemberAttribute]
        public int Varieties {
            get;
            set;
        }
        //发货数量
        [DataMemberAttribute]
        public int VarietiesQutity {
            get;
            set;
        }
        //发货时间
        [DataMemberAttribute]
        public DateTime? ShippingDate {
            get;
            set;
        }
        //发运单位联系人
        [DataMemberAttribute]
        public string Contact {
            get;
            set;
        }
        //发运单位联系电话
        [DataMemberAttribute]
        public string PhoneNumber {
            get;
            set;
        }
        //营业地址
        [DataMemberAttribute]
        public string BusinessAddress {
            get;
            set;

        }
        //采购订单编号
        [DataMemberAttribute]
        public string PartsPurchaseOrderCode {
            get;
            set;

        }
        //订单类型
        [DataMemberAttribute]
        public string OrderType {
            get;
            set;

        }
        //发运方式
        [DataMemberAttribute]
        public int ShippingMethod {
            get;
            set;

        }
        //要求到货时间
        [DataMemberAttribute]
        public DateTime? RequestedDeliveryTime {
            get;
            set;

        }
        [DataMemberAttribute]
        public DateTime PlanDeliveryTime {
            get;
            set;

        }
        //发运方式
        [DataMemberAttribute]
        public string UserBranchName {
            get;
            set;

        }
        //制单时间
        [DataMemberAttribute]
        public DateTime? CreateTime {
            get;
            set;

        }
        //制单人
        [DataMemberAttribute]
        public string CreatorName {
            get;
            set;

        }
        //入库计划单单号
        [DataMemberAttribute]
        public string PartsInboundPlanCode {
            get;
            set;

        }
        [DataMemberAttribute]
        public string BranchName {
            get;
            set;

        }
        [DataMemberAttribute]
        public string PartsSalesCategoryName {
            get;
            set;

        }
        [DataMemberAttribute]
        public double? TotalWeight {
            get;
            set;
        }
        [DataMemberAttribute]
        public double? TotalVolume {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal? TotalPrice {
            get;
            set;
        }
        [DataMemberAttribute]
        public decimal TotalAmount {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartsSupplierCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartsSupplierName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int Status {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PlanSource {
            get;
            set;
        }
        [DataMemberAttribute]
        public bool IfDirectProvision {
            get;
            set;
        }
        [DataMemberAttribute]
        public string OriginalRequirementBillCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public bool? DirectProvisionFinished {
            get;
            set;
        }
        [DataMemberAttribute]
        public string DeliveryBillNumber {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? ArrivalDate {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? LogisticArrivalDate {
            get;
            set;
        }
        [DataMemberAttribute]
        public string LogisticCompany {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Driver {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Phone {
            get;
            set;
        }
        [DataMemberAttribute]
        public string VehicleLicensePlate {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Remark {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ERPSourceOrderCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public int PartsSalesCategoryId {
            get;
            set;
        }
        [DataMemberAttribute]
        public int ReceivingWarehouseId {
            get;
            set;
        }
        [DataMemberAttribute]
        public int BranchId {
            get;
            set;
        }
        [DataMemberAttribute]
        public string InternalAllocationBillCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartsPurchaseOrderRemark {
            get;
            set;
        }
         [DataMemberAttribute]
        public DateTime? ModifyArrive {
            get;
            set;
        }
         [DataMemberAttribute]
         public DateTime? InboundTime {
             get;
             set;
         }
         [DataMemberAttribute]
         public string Approver {
             get;
             set;
         }
         [DataMemberAttribute]
         public DateTime? ApproveDate {
             get;
             set;
         }
    }
}
