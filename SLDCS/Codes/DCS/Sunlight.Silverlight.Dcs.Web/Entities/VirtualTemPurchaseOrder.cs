﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 查询旧件库存对应索赔单状态
    /// </summary>
    [DataContract(IsReference = true)]
    public class VirtualTemPurchaseOrder : EntityObject {
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Code {
            get;
            set;
        }
        [DataMemberAttribute]
        public string WarehouseName {
            get;

            set;
        }
        
        [DataMemberAttribute]
        public string PartsSupplierCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string PartsSupplierName {
            get;
            set;
        }

        [DataMemberAttribute]
        public string ReceivingAddress {
            get;
            set;
        }
        [DataMemberAttribute]
        public string OriginalRequirementBillCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string ReceivingCompanyName {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? ShippingMethod {
            get;
            set;
        }

        [DataMemberAttribute]
        public int Status {
            get;
            set;
        }

        [DataMemberAttribute]
        public int OrderType {
            get;
            set;
        }
        //供应商联系人
        [DataMemberAttribute]
        public string Contact {
            get;
            set;
        }
        //供应商联系人手机
        [DataMemberAttribute]
        public string PhoneNumber {
            get;
            set;
        }
        //创建人.人员信息.联系电话
        [DataMemberAttribute]
        public string CellNumber {
            get;
            set;
        }
        //仓库.联系人
        [DataMemberAttribute]
        public string WarehouseContact {
            get;
            set;
        }
        //仓库.联系人电话
        [DataMemberAttribute]
        public string WarehousePhoneNumber {
            get;
            set;
        }
         [DataMemberAttribute]
        public string Buyer {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CreatorName {
             get;
             set;
        }
         [DataMemberAttribute]
        public DateTime? CreateTime {
            get;
            set;
        }
         [DataMemberAttribute]
         public string SubmitterName {
             get;
             set;
         }

         [DataMemberAttribute]
         public DateTime? SubmitTime {
             get;
             set;
         }
    }
}
