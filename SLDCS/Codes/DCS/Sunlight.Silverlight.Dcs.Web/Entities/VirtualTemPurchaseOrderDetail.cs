﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class VirtualTemPurchaseOrderDetail {
        [Key]
        [DataMemberAttribute]
        public int SparePartId {
            get;
            set;
        }

        /// <summary>
        ///  配件编号
        /// </summary>
        [DataMemberAttribute]
        public string SparePartCode {
            get;
            set;
        }

        /// <summary>
        ///  配件名称
        /// </summary>
        [DataMemberAttribute]
        public string SparePartName {
            get;
            set;
        }
        /// <summary>
        ///  临时采购订单编号
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public string TemPurchaseOrderCode {
            get;
            set;
        }
        /// <summary>
        ///  临时采购订单编号
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int TemPurchaseOrderId {
            get;
            set;
        }

        /// <summary>
        /// 采购订单状态
        /// </summary>
        [DataMemberAttribute]
        public int TemPurchaseOrderStatus {
            get;
            set;
        }


        /// <summary>
        ///  发运数量
        /// </summary>
        [DataMemberAttribute]
        public int ShippingAmount {
            get;
            set;
        }
        /// <summary>
        ///  采购数量
        /// </summary>
        [DataMemberAttribute]
        public int PlanAmount {
            get;
            set;
        }

        /// <summary>
        ///  确认数量
        /// </summary>
        [DataMemberAttribute]
        public int ConfirmAmount {
            get;
            set;
        }
        /// <summary>
        /// 单位
        /// </summary>
        [DataMemberAttribute]
        public string MeasureUnit {
            get;
            set;
        }

        /// <summary>
        /// 采购订单类型
        /// </summary>
        [DataMemberAttribute]
        public int OrderType {
            get;
            set;
        }
        /// <summary>
        ///  本次发运量
        /// </summary>
        [DataMemberAttribute]
        public int UnShippingAmount {
            get;
            set;
        }
        /// <summary>
        /// 配件与供应商关系.供应商图号
        /// </summary>
        [DataMemberAttribute]
        public string SupplierPartCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string Remark {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SuplierCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string OrderCompanyCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string OrderCompanyName {
            get;
            set;
        }
        /// <summary>
        /// 创建时间
        /// </summary>
        [DataMemberAttribute]
        public DateTime? CreateTime {
            get;
            set;
        }
    }
}
