﻿using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web.Entities {
    /// <summary>
    /// 虚拟旧件入库单清单
    /// </summary>
    [DataContract(IsReference = true)]
    public class VirtualUsedPartsInboundDetail : EntityObject {
        /// <summary>
        /// 库位编号
        /// </summary>
        [DataMemberAttribute]
        public string UsedPartsWarehouseAreaCode {
            get;
            set;
        }

        /// <summary>
        /// 旧件配件编号
        /// </summary>
        [DataMemberAttribute]
        public string UsedPartsCode {
            get;
            set;
        }

        /// <summary>
        /// 旧件配件名称
        /// </summary>
        [DataMemberAttribute]
        public string UsedPartsName {
            get;
            set;
        }

        /// <summary>
        /// 配件单位
        /// </summary>
        [DataMemberAttribute]
        public string MeasureUnit {
            get;
            set;
        }

        /// <summary>
        /// 旧件条码
        /// </summary>
        [DataMemberAttribute]
        public string UsedPartsBarCode {
            get;
            set;
        }

        /// <summary>
        /// 入库量
        /// </summary>
        [DataMemberAttribute]
        public int Quantity {
            get;
            set;
        }

        /// <summary>
        /// 结算价格
        /// </summary>
        [DataMemberAttribute]
        public decimal SettlementPrice {
            get;
            set;
        }
        /// <summary>
        /// 索赔单编号
        /// </summary>
        [DataMemberAttribute]
        public string ClaimBillCode {
            get;
            set;
        }

    }
}
