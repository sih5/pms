﻿
using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class VirtualUsedPartsInboundOrder : EntityObject {

        [Key]
        [DataMember]
        public int Id {
            get;
            set;
        }
        [DataMember]
        public string Code {
            get;
            set;
        }
        [DataMember]
        public string UsedPartsWarehouseCode {
            get;
            set;
        }
        [DataMember]
        public string UsedPartsWarehouseName {
            get;
            set;
        }

        [DataMember]
        public string Businesscode {
            get;
            set;
        }
        [DataMember]
        public int InboundType {
            get;
            set;
        }
        [DataMember]
        public string SourceCode {
            get;
            set;
        }
        [DataMember]
        public string RelatedCompanyCode {
            get;
            set;
        }
        [DataMember]
        public string RelatedCompanyName {
            get;
            set;
        }
        [DataMember]
        public decimal TotalAmount {
            get;
            set;
        }
        [DataMember]
        public int TotalQuantity {
            get;
            set;
        }
        [DataMember]
        public decimal UsedPartsEncourageAmount {
            get;
            set;
        }
        [DataMember]
        public string Operator {
            get;
            set;
        }
        [DataMember]
        public string Remark {
            get;
            set;
        }
        [DataMember]
        public int SettlementStatus {
            get;
            set;
        }
        [DataMember]
        public string CreatorName {
            get;
            set;
        }
        [DataMember]
        public DateTime? CreateTime {
            get;
            set;
        }
    }
}
