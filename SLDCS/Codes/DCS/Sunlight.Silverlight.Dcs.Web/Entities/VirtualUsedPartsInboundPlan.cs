﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 用于查询虚拟旧件入库计划
    /// </summary>
    [DataContract(IsReference = true)]
    public class VirtualUsedPartsInboundPlan : EntityObject {
        /// <summary>
        /// 源单据Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int SourceId {
            get;
            set;
        }

        /// <summary>
        /// 源单据编号
        /// </summary>
        [DataMemberAttribute]
        [Display(Name = "VirtualUsedPartsInboundPlanDetail_SourceCode", ResourceType = typeof(Resources.EntityStrings))]
        public string SourceCode {
            get;
            set;
        }

        /// <summary>
        /// 源单据生效时间
        /// </summary>
        [DataMemberAttribute]
        [Display(Name = "VirtualUsedPartsInboundPlan_SourceBillApprovalTime", ResourceType = typeof(Resources.EntityStrings))]
        public DateTime? SourceBillApprovalTime {
            get;
            set;
        }

        /// <summary>
        /// 旧件入库类型
        /// </summary>
        [Key]
        [DataMemberAttribute]
        [Display(Name = "VirtualUsedPartsInboundPlan_InboundType", ResourceType = typeof(Resources.EntityStrings))]
        public int? InboundType {
            get;
            set;
        }

        /// <summary>
        /// 旧件入库状态
        /// </summary>
        [DataMemberAttribute]
        [Display(Name = "VirtualUsedPartsInboundPlan_InboundStatus", ResourceType = typeof(Resources.EntityStrings))]
        public int? InboundStatus {
            get;
            set;
        }

        /// <summary>
        /// 入库仓库编号
        /// </summary>
        [DataMemberAttribute]
        [Display(Name = "VirtualUsedPartsInboundPlan_UsedPartsWarehouseCode", ResourceType = typeof(Resources.EntityStrings))]
        public string UsedPartsWarehouseCode {
            get;
            set;
        }

        /// <summary>
        /// 入库仓库名称
        /// </summary>
        [DataMemberAttribute]
        [Display(Name = "VirtualUsedPartsInboundPlan_UsedPartsWarehouseName", ResourceType = typeof(Resources.EntityStrings))]
        public string UsedPartsWarehouseName {
            get;
            set;
        }

        /// <summary>
        /// 旧件仓库Id
        /// </summary>
        [DataMemberAttribute]
        public int UsedPartsWarehouseId {
            get;
            set;
        }

        /// <summary>
        /// 相关单位Id
        /// </summary>
        [DataMemberAttribute]
        public int? RelatedCompanyId {
            get;
            set;
        }

        /// <summary>
        /// 相关单位编号
        /// </summary>
        [DataMemberAttribute]
        public string RelatedCompanyCode {
            get;
            set;
        }

        /// <summary>
        /// 相关单位名称
        /// </summary>
        [DataMemberAttribute]
        public string RelatedCompanyName {
            get;
            set;
        }

        /// <summary>
        /// 企业编号
        /// </summary>
        [DataMemberAttribute]
        public string CompanyCode {
            get;
            set;
        }

        /// <summary>
        /// 企业名称
        /// </summary>
        [DataMemberAttribute]
        public string CompanyName {
            get;
            set;
        }
    }
}
