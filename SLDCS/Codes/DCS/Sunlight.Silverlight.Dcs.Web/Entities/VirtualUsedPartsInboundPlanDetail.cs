﻿using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 用于查询虚拟旧件入库清单
    /// </summary>
    [DataContract(IsReference = true)]
    public class VirtualUsedPartsInboundPlanDetail : EntityObject {
        /// <summary>
        /// 旧件条码
        /// </summary>
        [Key]
        [DataMemberAttribute]
        [Display(Name = "VirtualUsedPartsInboundPlanDetail_UsedPartsBarCode", ResourceType = typeof(Resources.EntityStrings))]
        public string UsedPartsBarCode {
            get;
            set;
        }

        /// <summary>
        /// 配件编号
        /// </summary>
        [DataMemberAttribute]
        public string UsedPartsCode {
            get;
            set;
        }

        /// <summary>
        /// 验收状态
        /// </summary>
        [DataMemberAttribute]
        public int ReceptionStatus {
            get;
            set;
        }
        /// <summary>
        /// 验收备注
        /// </summary>
        [DataMemberAttribute]
        public string ReceptionRemark {
            get;
            set;
        }

        /// <summary>
        /// 配件名称
        /// </summary>
        [DataMemberAttribute]
        public string UsedPartsName {
            get;
            set;
        }


        /// <summary>
        /// 库位编号
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public string UsedPartsWarehouseAreaCode {
            get;
            set;
        }

        /// <summary>
        /// 计划量
        /// </summary>
        [DataMemberAttribute]
        [Display(Name = "VirtualUsedPartsInboundPlanDetail_PlannedAmount", ResourceType = typeof(Resources.EntityStrings))]
        public int? PlannedAmount {
            get;
            set;
        }

        /// <summary>
        /// 确认量
        /// </summary>
        [DataMemberAttribute]
        [Display(Name = "VirtualUsedPartsInboundPlanDetail_ConfirmedAmount", ResourceType = typeof(Resources.EntityStrings))]
        public int? ConfirmedAmount {
            get;
            set;
        }

        /// <summary>
        /// 入库量
        /// </summary>
        [DataMemberAttribute]
        [Display(Name = "VirtualUsedPartsInboundPlanDetail_InboundAmount", ResourceType = typeof(Resources.EntityStrings))]
        public int? InboundAmount {
            get;
            set;
        }

        /// <summary>
        /// 旧件序列号
        /// </summary>
        [DataMemberAttribute]
        [Display(Name = "VirtualUsedPartsInboundPlanDetail_UsedPartsSerialNumber", ResourceType = typeof(Resources.EntityStrings))]
        public string UsedPartsSerialNumber {
            get;
            set;
        }

        /// <summary>
        /// 旧件批次号
        /// </summary>
        [DataMemberAttribute]
        [Display(Name = "VirtualUsedPartsInboundPlanDetail_UsedPartsBatchNumber", ResourceType = typeof(Resources.EntityStrings))]
        public string UsedPartsBatchNumber {
            get;
            set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        [DataMemberAttribute]
        [Display(Name = "_Common_Remark", ResourceType = typeof(Resources.EntityStrings))]
        public string Remark {
            get;
            set;
        }

        /// <summary>
        /// 旧件入库单Id
        /// </summary>
        [DataMemberAttribute]
        public int? UsedPartsInboundOrderId {
            get;
            set;
        }

        /// <summary>
        /// 旧件配件Id
        /// </summary>
        [DataMemberAttribute]
        public int UsedPartsId {
            get;
            set;
        }

        /// <summary>
        /// 旧件库位Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int UsedPartsWarehouseAreaId {
            get;
            set;
        }

        /// <summary>
        /// 结算价格
        /// </summary>
        [DataMemberAttribute]
        public decimal? SettlementPrice {
            get;
            set;
        }

        /// <summary>
        /// 成本价格
        /// </summary>
        [DataMemberAttribute]
        public decimal? CostPrice {
            get;
            set;
        }

        /// <summary>
        /// 分公司Id
        /// </summary>
        [DataMemberAttribute]
        public int? BranchId {
            get;
            set;
        }

        /// <summary>
        /// 索赔单Id
        /// </summary>
        [DataMemberAttribute]
        public int? ClaimBillId {
            get;
            set;
        }

        /// <summary>
        /// 索赔单编号
        /// </summary>
        [DataMemberAttribute]
        public string ClaimBillCode {
            get;
            set;
        }

        /// <summary>
        /// 索赔单类型
        /// </summary>
        [DataMemberAttribute]
        public int? ClaimBillType {
            get;
            set;
        }

        /// <summary>
        /// 旧件供应商Id
        /// </summary>
        [DataMemberAttribute]
        public int? UsedPartsSupplierId {
            get;
            set;
        }

        /// <summary>
        /// 旧件供应商编号
        /// </summary>
        [DataMemberAttribute]
        public string UsedPartsSupplierCode {
            get;
            set;
        }

        /// <summary>
        /// 旧件供应商名称
        /// </summary>
        [DataMemberAttribute]
        public string UsedPartsSupplierName {
            get;
            set;
        }

        /// <summary>
        /// 是否祸首件
        /// </summary>
        [DataMemberAttribute]
        public bool? IfFaultyParts {
            get;
            set;
        }

        /// <summary>
        /// 祸首件编号
        /// </summary>
        [DataMemberAttribute]
        public string FaultyPartsCode {
            get;
            set;
        }

        /// <summary>
        /// 祸首件名称
        /// </summary>
        [DataMemberAttribute]
        public string FaultyPartsName {
            get;
            set;
        }


        /// <summary>
        /// 祸首件供应商Id
        /// </summary>
        [DataMemberAttribute]
        public int? FaultyPartsSupplierId {
            get;
            set;
        }

        /// <summary>
        /// 祸首件供应商编号
        /// </summary>
        [DataMemberAttribute]
        public string FaultyPartsSupplierCode {
            get;
            set;
        }

        /// <summary>
        /// 祸首件供应商名称
        /// </summary>
        [DataMemberAttribute]
        public string FaultyPartsSupplierName {
            get;
            set;
        }

        /// <summary>
        /// 责任单位Id
        /// </summary>
        [DataMemberAttribute]
        public int? ResponsibleUnitId {
            get;
            set;
        }

        /// <summary>
        /// 责任单位编号
        /// </summary>
        [DataMemberAttribute]
        public string ResponsibleUnitCode {
            get;
            set;
        }

        /// <summary>
        /// 源单据Id
        /// </summary>
        [DataMemberAttribute]
        public int SourceId {
            get;
            set;
        }
        /// <summary>
        /// 旧件激励金额
        /// </summary>
        [DataMemberAttribute]
        public decimal? UsedPartsEncourageAmount {
            get;
            set;
        }
    }
}
