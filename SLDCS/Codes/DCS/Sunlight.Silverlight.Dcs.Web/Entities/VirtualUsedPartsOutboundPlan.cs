﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 用于查询虚拟旧件出库计划
    /// </summary>
    [DataContract(IsReference = true)]
    public class VirtualUsedPartsOutboundPlan : EntityObject {
        /// <summary>
        /// 源单据Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int SourceId {
            get;
            set;
        }

        /// <summary>
        /// 源单据编号
        /// </summary>
        [DataMemberAttribute]
        public string SourceCode {
            get;
            set;
        }

        /// <summary>
        /// 源单据生效时间
        /// </summary>
        [DataMemberAttribute]
        public DateTime? SourceBillApprovalTime {
            get;
            set;
        }

        /// <summary>
        /// 旧件出库类型
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int? OutboundType {
            get;
            set;
        }

        /// <summary>
        /// 旧件出库状态
        /// </summary>
        [DataMemberAttribute]
        public int? OutboundStatus {
            get;
            set;
        }

        /// <summary>
        /// 出库仓库编号
        /// </summary>
        [DataMemberAttribute]
        public string UsedPartsWarehouseCode {
            get;
            set;
        }

        /// <summary>
        /// 出库仓库名称
        /// </summary>
        [DataMemberAttribute]
        public string UsedPartsWarehouseName {
            get;
            set;
        }

        /// <summary>
        /// 旧件仓库Id
        /// </summary>
        [DataMemberAttribute]
        public int UsedPartsWarehouseId {
            get;
            set;
        }

        /// <summary>
        /// 相关单位Id
        /// </summary>
        [DataMemberAttribute]
        public int? RelatedCompanyId {
            get;
            set;
        }

        /// <summary>
        /// 相关单位编号
        /// </summary>
        [DataMemberAttribute]
        public string RelatedCompanyCode {
            get;
            set;
        }

        /// <summary>
        /// 相关单位名称
        /// </summary>
        [DataMemberAttribute]
        public string RelatedCompanyName {
            get;
            set;
        }

        /// <summary>
        /// 业务编号
        /// </summary>
        [DataMemberAttribute]
        public string BusinessCode {
            get;
            set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        [DataMemberAttribute]
        public string Remark {
            get;
            set;
        }
    }
}
