﻿using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 用于查询虚拟旧件出库清单
    /// </summary>
    [DataContract(IsReference = true)]
    public class VirtualUsedPartsOutboundPlanDetail : EntityObject {
        /// <summary>
        /// 旧件条码
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public string UsedPartsBarCode {
            get;
            set;
        }

        /// <summary>
        /// 配件编号
        /// </summary>
        [DataMemberAttribute]
        public string UsedPartsCode {
            get;
            set;
        }

        /// <summary>
        /// 配件名称
        /// </summary>
        [DataMemberAttribute]
        public string UsedPartsName {
            get;
            set;
        }

        /// <summary>
        /// 库位编号
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public string UsedPartsWarehouseAreaCode {
            get;
            set;
        }

        /// <summary>
        /// 计划量
        /// </summary>
        [DataMemberAttribute]
        public int? PlannedAmount {
            get;
            set;
        }

        /// <summary>
        /// 确认量
        /// </summary>
        [DataMemberAttribute]
        public int? ConfirmedAmount {
            get;
            set;
        }

        /// <summary>
        /// 出库量
        /// </summary>
        [DataMemberAttribute]
        public int? OutboundAmount {
            get;
            set;
        }

        /// <summary>
        /// 旧件序列号
        /// </summary>
        [DataMemberAttribute]
        public string UsedPartsSerialNumber {
            get;
            set;
        }

        /// <summary>
        /// 旧件批次号
        /// </summary>
        [DataMemberAttribute]
        public string UsedPartsBatchNumber {
            get;
            set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        [DataMemberAttribute]
        [Display(Name = "_Common_Remark", ResourceType = typeof(Resources.EntityStrings))]
        public string Remark {
            get;
            set;
        }

        /// <summary>
        /// 旧件出库单Id
        /// </summary>
        [DataMemberAttribute]
        public int? UsedPartsOutboundOrderId {
            get;
            set;
        }

        /// <summary>
        /// 旧件配件Id
        /// </summary>
        [DataMemberAttribute]
        public int UsedPartsId {
            get;
            set;
        }

        /// <summary>
        /// 旧件库位Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int UsedPartsWarehouseAreaId {
            get;
            set;
        }

        /// <summary>
        /// 结算价格
        /// </summary>
        [DataMemberAttribute]
        public decimal? SettlementPrice {
            get;
            set;
        }

        /// <summary>
        /// 成本价格
        /// </summary>
        [DataMemberAttribute]
        public decimal? CostPrice {
            get;
            set;
        }

        /// <summary>
        /// 分公司Id
        /// </summary>
        [DataMemberAttribute]
        public int? BranchId {
            get;
            set;
        }

        [DataMemberAttribute]
        public int ClaimBillId {
            get;
            set;
        }

        [DataMemberAttribute]
        public int ClaimBillType {
            get;
            set;
        }

        [DataMemberAttribute]
        public string ClaimBillCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public bool? IfFaultyParts {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? FaultyPartsSupplierId {
            get;
            set;
        }

        [DataMemberAttribute]
        public string FaultyPartsSupplierCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string FaultyPartsSupplierName {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? ResponsibleUnitId {
            get;
            set;
        }

        [DataMemberAttribute]
        public string ResponsibleUnitCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string ResponsibleUnitName {
            get;
            set;
        }

        [DataMemberAttribute]
        public int? FaultyPartsId {
            get;
            set;
        }

        [DataMemberAttribute]
        public string FaultyPartsCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string FaultyPartsName {
            get;
            set;
        }

        [DataMemberAttribute]
        public int UsedPartsSupplierId {
            get;
            set;
        }

        [DataMemberAttribute]
        public string UsedPartsSupplierCode {
            get;
            set;
        }

        [DataMemberAttribute]
        public string UsedPartsSupplierName {
            get;
            set;
        }

        /// <summary>
        /// 源单据Id
        /// </summary>
        [DataMemberAttribute]
        public int SourceId {
            get;
            set;
        }
    }
}
