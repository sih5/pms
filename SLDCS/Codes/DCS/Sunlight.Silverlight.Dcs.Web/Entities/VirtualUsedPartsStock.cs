﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 虚拟旧件库存
    /// </summary>
    [DataContract(IsReference = true)]
    public class VirtualUsedPartsStock : EntityObject {
        /// <summary>
        /// 旧件配件Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int UsedPartsId {
            get;
            set;
        }
        /// <summary>
        /// 旧件库存Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int UsedPartsStockId {
            get;
            set;
        }
        /// <summary>
        /// 祸首件供应商Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int? FaultyPartsSupplierId {
            get;
            set;
        }
        /// <summary>
        /// 祸首件供应商编号
        /// </summary>
        [DataMemberAttribute]
        public string FaultyPartsSupplierCode {
            get;
            set;
        }
        /// <summary>
        /// 祸首件供应商名称
        /// </summary>
        [DataMemberAttribute]
        public string FaultyPartsSupplierName {
            get;
            set;
        }
        /// <summary>
        /// 索赔单编号
        /// </summary>
        [DataMemberAttribute]
        public string ClaimBillCode {
            get;
            set;
        }

        /// <summary>
        /// 索赔单创建时间
        /// </summary>
        [DataMemberAttribute]
        public DateTime? ClaimBillCreateTime {
            get;
            set;
        }

        /// <summary>
        /// 服务站名称
        /// </summary>
        [DataMemberAttribute]
        public string DealerName {
            get;
            set;
        }
        /// <summary>
        /// 责任单位编号
        /// </summary>
        [DataMemberAttribute]
        public string ResponsibleUnitCode {
            get;
            set;
        }
        /// <summary>
        /// 责任单位名称
        /// </summary>
        [DataMemberAttribute]
        public string ResponsibleUnitName {
            get;
            set;
        }
        /// <summary>
        /// 旧件配件名称
        /// </summary>
        [DataMemberAttribute]
        public string UsedPartsName {
            get;
            set;
        }
        /// <summary>
        /// 旧件配件编号
        /// </summary>
        [DataMemberAttribute]
        public string UsedPartsCode {
            get;
            set;
        }
        /// <summary>
        /// 旧件条码
        /// </summary>
        [DataMemberAttribute]
        public string UsedPartsBarCode {
            get;
            set;
        }
        /// <summary>
        /// 发运单编号
        /// </summary>
        [DataMemberAttribute]
        public string ShippingOrderCode {
            get;
            set;
        }
        /// <summary>
        /// 库存数量
        /// </summary>
        [DataMemberAttribute]
        public int? StorageQuantity {
            get;
            set;
        }
        /// <summary>
        /// 发运时间
        /// </summary>
        [DataMemberAttribute]
        public DateTime? CreateTime {
            get;
            set;
        }
        /// <summary>
        /// 结算价格
        /// </summary>
        [DataMemberAttribute]
        public decimal? SettlementPrice {
            get;
            set;
        }
        /// <summary>
        /// 是否祸首件
        /// </summary>
        [DataMemberAttribute]
        public bool? IfFaultyParts {
            get;
            set;
        }
    }
}
