﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web.Entities {
    /// <summary>
    /// 虚拟车辆信息
    /// </summary>
    [DataContract(IsReference = true)]
    public class VirtualVehicleInformation : EntityObject {
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }

        [DataMemberAttribute]
        public int RetainedCustomerId {
            get;
            set;
        }

        [DataMemberAttribute]
        public string EngineSerialNumber {
            get;
            set;
        }



        [DataMemberAttribute]
        public string VIN {
            get;
            set;
        }

        [DataMemberAttribute]
        public string OldVIN {
            get;
            set;
        }

        [DataMemberAttribute]
        public string VehicleGrade {
            get;
            set;
        }

        [DataMemberAttribute]
        public string ChassisGrade {
            get;
            set;
        }

        [DataMemberAttribute]
        public string VehicleLicensePlate {
            get;
            set;
        }

        [DataMemberAttribute]
        public string ServiceCustomerName {
            get;
            set;
        }

        [DataMemberAttribute]
        public string ServiceCustomerCellPhoneNumber {
            get;
            set;
        }
        [DataMemberAttribute]
        public bool IsWorkOff {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? SalesDate {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? OutOfFactoryDate {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ResponsibleUnitName {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ProductCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string VehicleSeries {
            get;
            set;
        }
        [DataMemberAttribute]
        public string BrandName {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ProductLineName {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ProductCategoryCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ProductCategoryName {
            get;
            set;
        }
        [DataMemberAttribute]
        public bool IsRoadVehicl {
            get;
            set;
        }
        [DataMemberAttribute]
        public int? BridgeType {
            get;
            set;
        }
        [DataMemberAttribute]
        public string EngineModel {
            get;
            set;
        }
        [DataMemberAttribute]
        public string GearModel {
            get;
            set;
        }
        [DataMemberAttribute]
        public string GPSNUMBER {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SIMCARD {
            get;
            set;
        }
        [DataMemberAttribute]
        public string SerialNumber {
            get;
            set;
        }
        [DataMemberAttribute]
        public string CreatorName {
            get;
            set;
        }
        [DataMemberAttribute]
        public DateTime? CreateTime {
            get;
            set;
        }
        [DataMemberAttribute]
        public string ModifierName {
            get;
            set;
        }

        [DataMemberAttribute]
        public DateTime? ModifyTime {
            get;
            set;
        }

        [DataMemberAttribute]
        public string ServiceProductLineName {
            get;
            set;
        }

        [DataMemberAttribute]
        public string ServicePartsSalesCategoryName {
            get;
            set;
        }
        [DataMemberAttribute]
        public string DEVICE_TYPE {
            get;
            set;
        }

        [DataMemberAttribute]
        public string IS_FINANCE {
            get;
            set;
        }

        [DataMemberAttribute]
        public string DID {
            get;
            set;
        }

        [DataMemberAttribute]
        public string COMM_CODE {
            get;
            set;
        }

        [DataMemberAttribute]
        public string STD_SIMCODE {
            get;
            set;
        }

        [DataMemberAttribute]
        public string EXT_SIMCODE {
            get;
            set;
        }

        [DataMemberAttribute]
        public DateTime? BOUND_TIME {
            get;
            set;
        }

        [DataMemberAttribute]
        public string SN {
            get;
            set;
        }
    }
}
