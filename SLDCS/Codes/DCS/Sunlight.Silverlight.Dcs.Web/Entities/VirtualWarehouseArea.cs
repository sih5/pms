﻿using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    [DataContractAttribute]
    public class VirtualWarehouseArea : EntityObject {
        //本虚拟BO用于优化查询库位

        /// <summary>
        /// 库区库位Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }

        /// <summary>
        /// 库存Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int PartStockId {
            get;
            set;
        }

        /// <summary>
        /// 库区库位编号
        /// </summary>
        [DataMemberAttribute]
        public string Code {
            get;
            set;
        }

        /// <summary>
        /// 仓库名称
        /// </summary>
        [DataMemberAttribute]
        public string WarehouseName {
            get;
            set;
        }

        /// <summary>
        /// 库区库位类型
        /// </summary>
        [DataMemberAttribute]
        public int AreaKind {
            get;
            set;
        }

        /// <summary>
        /// 库区用途
        /// </summary>
        [DataMemberAttribute]
        public int AreaCategory {
            get;
            set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        [DataMemberAttribute]
        public string Remark {
            get;
            set;
        }
    }
}
