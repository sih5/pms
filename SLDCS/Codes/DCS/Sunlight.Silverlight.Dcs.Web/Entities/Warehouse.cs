﻿using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 仓库
    /// </summary>
    public partial class Warehouse {
        /// <summary>
        ///仓库人员
        /// </summary>
        [DataMemberAttribute]
        public string OperatorName {
            get;
            set;
        }
    }
}
