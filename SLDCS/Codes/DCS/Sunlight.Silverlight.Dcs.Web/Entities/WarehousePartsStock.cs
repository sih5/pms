﻿using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 虚拟仓库配件库存
    /// </summary>
    [DataContract(IsReference = true)]
    public class WarehousePartsStock : EntityObject {

        /// <summary>
        /// 配件销售类型Id
        /// </summary>
        [DataMemberAttribute]
        public int PartsSalesCategoryId {
            get;
            set;
        }
        /// <summary>
        /// Id
        /// </summary>
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }

        /// <summary>
        /// 配件销售类型名称
        /// </summary>
        [DataMemberAttribute]
        public string PartsSalesCategoryName {
            get;
            set;
        }

        /// <summary>
        /// 供应商Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int SupplierId {
            get;
            set;
        }
        /// <summary>
        /// 仓库Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int WarehouseId {
            get;
            set;
        }

        /// <summary>
        /// 配件Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int SparePartId {
            get;
            set;
        }

        /// <summary>
        /// 库区编号
        /// </summary>
        [DataMemberAttribute]
        public string WarehouseAreaCode {
            get;
            set;
        }

        /// <summary>
        /// 仓库编号
        /// </summary>
        [DataMemberAttribute]
        public string WarehouseCode {
            get;
            set;
        }

        /// <summary>
        /// 仓库名称
        /// </summary>
        [DataMemberAttribute]
        public string WarehouseName {
            get;
            set;
        }

        /// <summary>
        /// 仓储企业Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int StorageCompanyId {
            get;
            set;
        }
        /// <summary>
        /// 仓储企业编号
        /// </summary>
        [DataMemberAttribute]
        public string StorageCompanyCode {
            get;
            set;
        }

        /// <summary>
        /// 仓储企业名称
        /// </summary>
        [DataMemberAttribute]
        public string StorageCompanyName {
            get;
            set;
        }

        /// <summary>
        /// 仓储企业类型
        /// </summary>
        [DataMemberAttribute]
        public int StorageCompanyType {
            get;
            set;
        }

        /// <summary>
        /// 营销分公司Id
        /// </summary>
        [DataMemberAttribute]
        public int BranchId {
            get;
            set;
        }

        /// <summary>
        /// 营销分公司编号
        /// </summary>
        [DataMemberAttribute]
        public string BranchCode {
            get;
            set;
        }

        /// <summary>
        /// 营销分公司名称
        /// </summary>
        [DataMemberAttribute]
        public string BranchName {
            get;
            set;
        }

        /// <summary>
        ///  配件编号
        /// </summary>
        [DataMemberAttribute]
        public string SparePartCode {
            get;
            set;
        }

        /// <summary>
        ///  配件名称
        /// </summary>
        [DataMemberAttribute]
        public string SparePartName {
            get;
            set;
        }

        /// <summary>
        ///  库存数量
        /// </summary>
        [DataMemberAttribute]
        public int Quantity {
            get;
            set;
        }
     
        [DataMemberAttribute]
        public int UseableQuantity {
            get;
            set;
        }

        /// <summary>
        ///  可用库存
        /// </summary>
        [DataMemberAttribute]
        public int UsableQuantity {
            get;
            set;
        }

        /// <summary>
        /// 配件销售价格
        /// </summary>
        [DataMemberAttribute]
        public decimal PartsSalesPrice {
            get;
            set;
        }
     
        [DataMemberAttribute]
        public decimal? SalesPrice {
            get;
            set;
        }
        /// <summary>
        /// 销售价格类型
        /// </summary>
        [DataMemberAttribute]
        public int PriceType {
            get;
            set;
        }
        /// <summary>
        /// 替换件Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int NewPartId {
            get;
            set;
        }

        /// <summary>
        /// 替换类型
        /// </summary>
        [DataMemberAttribute]
        public int ReplacementType {
            get;
            set;
        }

        /// <summary>
        /// 替换件编号
        /// </summary>
        [DataMemberAttribute]
        public string NewPartCode {
            get;
            set;
        }

        /// <summary>
        /// 替换件名称
        /// </summary>
        [DataMemberAttribute]
        public string NewPartName {
            get;
            set;
        }

        /// <summary>
        /// 配件采购价
        /// </summary>
        [DataMemberAttribute]
        public decimal PartsPurchasePricing {
            get;
            set;
        }

        /// <summary>
        ///  配件计划价
        /// </summary>
        [DataMemberAttribute]
        public decimal PartsPlannedPrice {
            get;
            set;
        }

        /// <summary>
        /// 零售指导价
        /// </summary>
        [DataMemberAttribute]
        public decimal PartsRetailGuidePrice {
            get;
            set;
        }

        /// <summary>
        /// 计量单位
        /// </summary>
        [DataMemberAttribute]
        public string MeasureUnit {
            get;
            set;
        }

        /// <summary>
        /// 储运中心
        /// </summary>
        [DataMemberAttribute]
        public int? StorageCenter {
            get;
            set;
        }
     
        [DataMemberAttribute]
        public string BrachName {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartsCode {
            get;
            set;
        }
        [DataMemberAttribute]
        public string PartsName {
            get;
            set;
        }

        /// <summary>
        /// 库存高限
        /// </summary>
        [DataMemberAttribute]
        public int? StockMaximum {
            get;
            set;
        }

        /// <summary>
        /// 库存低限
        /// </summary>
        [DataMemberAttribute]
        public int? StockMinimum {
            get;
            set;
        }

        /// <summary>
        /// 安全库存
        /// </summary>
        [DataMemberAttribute]
        public int? SafeStock {
            get;
            set;
        }
        /// <summary>
        /// 成本价
        /// </summary>
        [DataMemberAttribute]
        public decimal CostPrice
        {
            get;
            set;
        }

        /// <summary>
        /// 最小销售数量
        /// </summary>
        [DataMemberAttribute]
        public int? MInPackingAmount
        {
            get;
            set;
        }
        //品牌
        [DataMemberAttribute]
        public string Categoryname
        {
            get;
            set;
        }
        //品牌
        [DataMemberAttribute]
        public string ReferenceCode
        {
            get;
            set;
        }
        /// <summary>
        /// 建议售价
        /// </summary>
        [DataMemberAttribute]
        public decimal RetailguidePrice
        {
            get;
            set;
        }
        /// <summary>
        /// 账面库存
        /// </summary>
        [DataMemberAttribute]
        public int? TotalQty
        {
            get;
            set;
        }
        /// <summary>
        /// 待发区库存
        /// </summary>
        [DataMemberAttribute]
        public int? DfQty
        {
            get;
            set;
        }
        /// <summary>
        /// 实际库存
        /// </summary>
        [DataMemberAttribute]
        public int? ActualQty
        {
            get;
            set;
        }
        /// <summary>
        /// 实际可用库存
        /// </summary>
        [DataMemberAttribute]
        public int? ActualAvailableStock
        {
            get;
            set;
        }
        /// <summary>
        /// 订单锁定
        /// </summary>
        [DataMemberAttribute]
        public int? OrderlockQty
        {
            get;
            set;
        }
        /// <summary>
        /// 采购库存
        /// </summary>
        [DataMemberAttribute]
        public int? CgQty
        {
            get;
            set;
        }
        /// <summary>
        /// 待包装
        /// </summary>
        [DataMemberAttribute]
        public int? InlockQty
        {
            get;
            set;
        }
        /// <summary>
        /// 待上架
        /// </summary>
        [DataMemberAttribute]
        public int? ShelfQty
        {
            get;
            set;
        }
        /// <summary>
        /// 保底库存
        /// </summary>
        [DataMemberAttribute]
        public int? StockQty
        {
            get;
            set;
        }
    }
}
