﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    [DataContract(IsReference = true)]
    public class WarrantyPolicyNServProdLineExtend : EntityObject {

        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }

        ///<summary>
        /// 分公司Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int BranchId {
            get;
            set;
        }

        ///<summary>
        /// 分公司名称
        /// </summary>
        [DataMemberAttribute]
        public string BranchName {
            get;
            set;
        }

        ///<summary>
        /// 服务产品线Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int ServiceProductLineId {
            get;
            set;
        }

        ///<summary>
        /// 服务产品线编号
        /// </summary>
        [DataMemberAttribute]
        public string ProductLineCode {
            get;
            set;
        }

        ///<summary>
        /// 服务产品线名称
        /// </summary>
        [DataMemberAttribute]
        public string ProductLineName {
            get;
            set;
        }

        ///<summary>
        /// 产品线类型
        /// </summary>
        [DataMemberAttribute]
        public int? ProductLineType {
            get;
            set;
        }

        ///<summary>
        /// 保修政策Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int WarrantyPolicyId {
            get;
            set;
        }

        ///<summary>
        /// 保修政策编号
        /// </summary>
        [DataMemberAttribute]
        public string WarrantyPolicyCode {
            get;
            set;
        }

        ///<summary>
        /// 保修政策名称
        /// </summary>
        [DataMemberAttribute]
        public string WarrantyPolicyName {
            get;
            set;
        }

        ///<summary>
        /// 保修政策类型
        /// </summary>
        [DataMemberAttribute]
        public int WarrantyPolicyCategory {
            get;
            set;
        }

        ///<summary>
        /// 适用范围
        /// </summary>
        [DataMemberAttribute]
        public int ApplicationScope {
            get;
            set;
        }

        ///<summary>
        /// 含保修条款
        /// </summary>
        [DataMemberAttribute]
        public bool RepairTermInvolved {
            get;
            set;
        }

        ///<summary>
        /// 含保养条款
        /// </summary>
        [DataMemberAttribute]
        public bool MainteTermInvolved {
            get;
            set;
        }

        ///<summary>
        /// 含配件保修条款
        /// </summary>
        [DataMemberAttribute]
        public bool PartsWarrantyTermInvolved {
            get;
            set;
        }

        ///<summary>
        /// 状态
        /// </summary>
        [DataMemberAttribute]
        public int Status {
            get;
            set;
        }

        ///<summary>
        /// 创建人Id
        /// </summary>
        [DataMemberAttribute]
        public int? CreatorId {
            get;
            set;
        }

        ///<summary>
        /// 创建人名称
        /// </summary>
        [DataMemberAttribute]
        public string CreatorName {
            get;
            set;
        }

        ///<summary>
        /// 创建时间
        /// </summary>
        [DataMemberAttribute]
        public DateTime? CreateTime {
            get;
            set;
        }

        ///<summary>
        /// 修改人Id
        /// </summary>
        [DataMemberAttribute]
        public int? ModifierId {
            get;
            set;
        }

        ///<summary>
        /// 修改人名称
        /// </summary>
        [DataMemberAttribute]
        public string ModifierName {
            get;
            set;
        }

        ///<summary>
        /// 修改时间
        /// </summary>
        [DataMemberAttribute]
        public DateTime? ModifyTime {
            get;
            set;
        }

        ///<summary>
        /// 作废人Id
        /// </summary>
        [DataMemberAttribute]
        public int? AbandonerId {
            get;
            set;
        }

        ///<summary>
        /// 作废人名称
        /// </summary>
        [DataMemberAttribute]
        public string AbandonerName {
            get;
            set;
        }

        ///<summary>
        /// 作废时间
        /// </summary>
        [DataMemberAttribute]
        public DateTime? AbandonTime {
            get;
            set;
        }

        ///<summary>
        /// 备注
        /// </summary>
        [DataMemberAttribute]
        public string Remark {
            get;
            set;
        }

        ///<summary>
        /// 销售开始时间
        /// </summary>
        [DataMemberAttribute]
        public DateTime ValidationDate {
            get;
            set;
        }
        /// <summary>
        /// 销售截止日期
        /// </summary>
        [DataMemberAttribute]
        public DateTime ExpireDate {
            get;
            set;
        }
    }
}
