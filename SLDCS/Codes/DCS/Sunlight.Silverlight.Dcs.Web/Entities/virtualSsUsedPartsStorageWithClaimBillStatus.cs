﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 查询旧件库存对应索赔单状态
    /// </summary>
    [DataContract(IsReference = true)]
    public class virtualSsUsedPartsStorageWithClaimBillStatus : EntityObject {
        /// <summary>
        /// Id
        /// </summary>
        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }
        /// <summary>
        /// 索赔单Id
        /// </summary>
        [DataMemberAttribute]
        public int ClaimBillId {
            get;
            set;
        }
        /// <summary>
        /// 维修索赔单状态
        /// </summary>
        [DataMemberAttribute]
        public int RepairClaimStatus {
            get;
            set;
        }
        /// <summary>
        /// 配件索赔单状态
        /// </summary>
        [DataMemberAttribute]
        public int PartsClaimStatus {
            get;
            set;
        }
        /// <summary>
        /// 服务站编号
        /// </summary>
        [DataMemberAttribute]
        public string DealerCode {
            get;
            set;
        }
        /// <summary>
        /// 服务站名称
        /// </summary>
        [DataMemberAttribute]
        public string DealerName {
            get;
            set;
        }
        /// <summary>
        /// 服务站Id
        /// </summary>
        [DataMemberAttribute]
        public int DealerId {
            get;
            set;
        }
        /// <summary>
        /// 营销分公司IdId
        /// </summary>
        [DataMemberAttribute]
        public int BranchId {
            get;
            set;
        }
        /// <summary>
        /// 分公司名称
        /// </summary>
        [DataMemberAttribute]
        public string BranchName {
            get;
            set;
        }
        /// <summary>
        /// 索赔单编号
        /// </summary>
        [DataMemberAttribute]
        public string ClaimBillCode {
            get;
            set;
        }
        /// <summary>
        /// 配件销售类型
        /// </summary>
        [DataMemberAttribute]
        public int PartsSalesCategoryId {
            get;
            set;
        }
        /// <summary>
        /// 配件销售类型名称
        /// </summary>
        [DataMemberAttribute]
        public string PartsSalesCategoryName {
            get;
            set;
        }
        /// <summary>
        /// 旧件条码
        /// </summary>
        [DataMemberAttribute]
        public string UsedPartsBarCode {
            get;
            set;
        }
        /// <summary>
        /// 旧件返回政策
        /// </summary>
        [DataMemberAttribute]
        public int UsedPartsReturnPolicy {
            get;
            set;
        }
        /// <summary>
        /// 旧件配件编号
        /// </summary>
        [DataMemberAttribute]
        public string UsedPartsCode {
            get;
            set;
        }

        /// <summary>
        /// 旧件配件名称
        /// </summary>
        [DataMemberAttribute]
        public string UsedPartsName {
            get;
            set;
        }

        /// <summary>
        /// 旧件配件Id
        /// </summary>
        [DataMemberAttribute]
        public int UsedPartsId {
            get;
            set;
        }

        /// <summary>
        /// 已发运Id
        /// </summary>
        [DataMemberAttribute]
        public bool Shipped {
            get;
            set;
        }

        /// <summary>
        /// 单价Id
        /// </summary>
        [DataMemberAttribute]
        public decimal UnitPrice {
            get;
            set;
        }

        /// <summary>
        /// 索赔单类型
        /// </summary>
        [DataMemberAttribute]
        public int? ClaimBillType {
            get;
            set;
        }

        /// <summary>
        /// 旧件供应商Id
        /// </summary>
        [DataMemberAttribute]
        public int? UsedPartsSupplierId {
            get;
            set;
        }

        /// <summary>
        /// 旧件供应商编号
        /// </summary>
        [DataMemberAttribute]
        public string UsedPartsSupplierCode {
            get;
            set;
        }

        /// <summary>
        /// 旧件供应商名称
        /// </summary>
        [DataMemberAttribute]
        public string UsedPartsSupplierName {
            get;
            set;
        }

        /// <summary>
        /// 是否祸首件
        /// </summary>
        [DataMemberAttribute]
        public bool? IfFaultyParts {
            get;
            set;
        }

        /// <summary>
        /// 已发运String
        /// </summary>
        [DataMemberAttribute]
        public String ShippedString {
            get;
            set;
        }


        /// <summary>
        /// 是否祸首件String
        /// </summary>
        [DataMemberAttribute]
        public String IfFaultyPartsString {
            get;
            set;
        }
        /// <summary>
        /// 祸首件编号
        /// </summary>
        [DataMemberAttribute]
        public string FaultyPartsCode {
            get;
            set;
        }

        /// <summary>
        /// 祸首件名称
        /// </summary>
        [DataMemberAttribute]
        public string FaultyPartsName {
            get;
            set;
        }
        /// <summary>
        /// 祸首件Id
        /// </summary>
        [DataMemberAttribute]
        public int? FaultyPartsId {
            get;
            set;
        }

        /// <summary>
        /// 祸首件供应商Id
        /// </summary>
        [DataMemberAttribute]
        public int? FaultyPartsSupplierId {
            get;
            set;
        }

        /// <summary>
        /// 祸首件供应商编号
        /// </summary>
        [DataMemberAttribute]
        public string FaultyPartsSupplierCode {
            get;
            set;
        }

        /// <summary>
        /// 祸首件供应商名称
        /// </summary>
        [DataMemberAttribute]
        public string FaultyPartsSupplierName {
            get;
            set;
        }
        /// <summary>
        /// 索赔单生成时间
        /// </summary>
        [DataMemberAttribute]
        public DateTime? ClaimBillCreateTime {
            get;
            set;
        }
        /// <summary>
        /// 创建人
        /// </summary>
        [DataMemberAttribute]
        public string CreatorName {
            get;
            set;
        }
        /// <summary>
        /// 创建时间
        /// </summary>
        [DataMemberAttribute]
        public DateTime? CreateTime {
            get;
            set;
        }
        /// <summary>
        /// 修改人
        /// </summary>
        [DataMemberAttribute]
        public string ModifierName {
            get;
            set;
        }
        /// <summary>
        /// 修改时间
        /// </summary>
        [DataMemberAttribute]
        public DateTime? ModifyTime {
            get;
            set;
        }
    }
}
