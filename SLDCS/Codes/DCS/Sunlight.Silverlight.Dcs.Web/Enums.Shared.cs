﻿namespace Sunlight.Silverlight.Dcs.Web {
    //提交返回信息（异常内容）
    public enum DcsSubmitResultCode {
        异常 = 0,
        合理 = 1,
        不合理 = 2
    }

    //网络级别
    public enum DcsChannelGrade {
        一级 = 1,
        二级 = 2
    }

    //纳税人资格
    public enum DcsPartsSupplierTaxpayerKind {
        一般纳税人 = 1,
        小规模纳税人 = 2
    }

    //维修资质等级
    public enum DcsRepairQualificationGrade {
        一类 = 1,
        二类 = 2,
        三类 = 3,
        无 = 4
    }

    //供应商类型
    public enum DcsPartsSupplierType {
        正式供应商 = 1,
        临时供应商 = 2,
        紧急采购供应商 = 3
    }

    //配件类型
    public enum DcsSparePartPartType {
        总成件 = 1,
        配件 = 2,
        精品件 = 3,
        辅料 = 4,
        包材 = 5,
        总成件_领用 = 6,
        包材辅料 = 7
    }

    //损耗类型
    public enum DcsSparePartLossType {
        非常用件 = 1,
        常用件 = 2,
        易耗件 = 3,
        大总成 = 4
    }

    //配件ABC分类
    public enum DcsABCStrategyCategory {
        A = 1,
        B = 2,
        C = 3,
        D = 4,
        E = 5,
        F = 6,
        G = 7,
        H = 8,
        J = 9,
        Z = 10,
        R = 11
    }

    //集团ABC类别
    public enum DcsABCGroupCategory {
        A = 1,
        B = 2,
        C = 3,
        D = 4,
        E = 5,
        F = 6,
        G = 7,
        H = 8
    }

    //配件替互换类型
    public enum DcsPartsReplacementReplacementType {
        替换 = 1,
        互换 = 2
    }

    //主数据状态
    public enum DcsMasterDataStatus {
        有效 = 1,
        停用 = 2,
        作废 = 99
    }

    //基础数据状态
    public enum DcsBaseDataStatus {
        有效 = 1,
        作废 = 99
    }

    //采购价格状态
    public enum DcsPartsPurchasePricingStatus {
        新增 = 1,
        生效 = 2,
        作废 = 99
    }

    //采购价格变更申请单状态
    public enum DcsPartsPurchasePricingChangeStatus {
        新增 = 1,
        生效 = 2,
        初审通过 = 3,
        提交 = 4,
        审核通过 = 5,
        作废 = 99
    }

    //故障件索赔类型
    public enum DcsFaultClaimType {
        装车件索赔 = 1,
        配件索赔 = 2
    }

    //库区库位类型
    public enum DcsAreaKind {
        仓库 = 1,
        库区 = 2,
        库位 = 3
    }

    //库区用途
    public enum DcsAreaType {
        保管区 = 1,
        问题区 = 2,
        检验区 = 3,
        待发区 = 4
    }

    //存储策略
    public enum DcsStorageStrategy {
        定位存储 = 1,
        随机存储 = 2
    }

    //储运中心
    public enum DcsStorageCenter {
        北京CDC = 1,
        山东CDC = 2,
        广东CDC = 5,
        河南CDC = 6,
        虚拟储运中心 = 10
    }

    //市场部业务类型
    public enum DcsMarketingDepartmentType {
        销售 = 1,
        服务 = 2
    }

    //故障现象分类类型
    public enum DcsMalfunctionCategoryCategoryGroup {
        总成件 = 1,
        分总成 = 2,
        配件 = 3
    }

    //企业性质
    public enum DcsCorporateNature {
        国有企业 = 1,
        集体企业 = 2,
        私营企业 = 3,
        联营企业 = 4,
        外商投资企业 = 5,
        外商独资 = 6,
        中外合资合作 = 7,
        股份合作企业 = 8,
    }

    //税率
    public enum DcsTaxRate {
        TaxRate_17 = 1,
        TaxRate_13 = 2,
        TaxRate_6 = 3,
        TaxRate_3 = 4,
        TaxRate_多种_其他 = 5
    }

    //性别
    public enum DcsSexType {
        未填 = 0,
        男 = 1,
        女 = 2,
        未知 = 3
    }

    //地址用途
    public enum DcsAddressUsage {
        接收商品车 = 1,
        接收配件 = 2,
        接收其他物资 = 3
    }

    //区域类型
    public enum DcsRegionType {
        区 = 1,
        省 = 2,
        市 = 3,
        县_区 = 4
    }

    //旧件返回政策
    public enum DcsPartsWarrantyTermReturnPolicy {
        返回本部 = 1,
        供应商自行回收 = 2,
        监督处理 = 3,
        自行处理 = 4,
        总部调件 = 5
    }

    //配件保修类型
    public enum DcsPartsWarrantyTermPartsWarrantyType {
        装车件保修 = 1,
        更换件保修 = 2,
        销售件保修 = 3
    }

    //保修政策类型
    public enum DcsWarrantyPolicyCategory {
        标准保修 = 1,
        特殊保修 = 2,
        延保产品 = 3
    }

    //保养计费模式
    public enum DcsVehicleWarrantyTermBillingModel {
        固定金额 = 1,
        按实际发生收取 = 2
    }

    //服务适用范围
    public enum DcsWarrantyPolicyApplicationScope {
        部分车型 = 1,
        所有车型 = 2
    }

    //车辆状态
    public enum DcsVehicleStatus {
        已排产 = 1,
        总装中 = 2,
        已下线 = 3,
        本部仓库 = 4,
        出厂 = 5,
        经销商仓库 = 6,
        实销完成 = 7,
        游离 = 98,
        灭失 = 99
    }

    //用户类型
    public enum DcsCustomerType {
        个人用户 = 1,
        组织用户 = 2
    }

    //配件发运方式
    public enum DcsPartsShippingMethod {
        公路物流 = 1,
        客车 = 2,
        专车 = 3,
        顺丰公路 = 4,
        顺丰航空 = 5,
        自提 = 6,
        海运_出口 = 7,
        铁路_出口 = 8,
        空运_出口 = 9,
        DHL_出口 = 10,
        其他 = 11,
        EMS = 12,
        中邮快运 = 13,
        德邦物流 = 14,
        航空 = 15
    }

    //发运方
    public enum DcsUsedPartsShippingOrderDispatcher {
        经销商发运 = 1,
        厂方发运 = 2,
        第三方 = 3
    }

    //旧件发运单状态
    public enum DcsUsedPartsShippingOrderStatus {
        新增 = 1,
        已确认 = 2,
        作废 = 99
    }

    //旧件验收状态
    public enum DcsUsedPartsShippingDetailReceptionStatus {
        在途 = 1,
        已接收 = 2,
        未接收 = 3,
        罚没 = 4,
        不合格 = 5
    }

    //旧件物流损失清单状态
    public enum DcsUsedPartsLogisticLossDetailProcessStatus {
        丢失 = 1,
        未返回 = 2
    }

    //服务站旧件处理类型
    public enum DcsSsUsedPartsDisposalBillDisposalMethod {
        自行处理 = 1,
        监督处理 = 2,
        供应商自行回收 = 3,
        总部调件 = 4
    }

    //服务站旧件处理单状态
    public enum DcsSsUsedPartsDisposalBillStatus {
        新增 = 1,
        生效 = 2,
        作废 = 99
    }

    //维修保养索赔单索赔类型
    public enum DcsRepairClaimBillClaimType {
        维修索赔 = 1,
        保养索赔 = 2,
        服务活动 = 3,
        让度服务 = 10
    }

    //开票类型
    public enum DcsInvoiceType {
        为空 = 0,
        增值税发票 = 1,
        普通发票 = 2
    }

    //维修工种
    public enum DcsRepairItemJobType {
        机修 = 1,
        钣金 = 2,
        喷漆 = 3,
        其他 = 4
    }

    //维修结算属性
    public enum DcsRepairItemSettlementProperty {
        索赔 = 1,
        免费保养 = 2,
        免费维修 = 3,
        自费 = 4
    }

    //旧件出库类型
    public enum DcsUsedPartsOutboundOrderOutboundType {
        调拨出库 = 1,
        旧件处理出库 = 2,
        清退供应商出库 = 3,
        借用出库 = 4,
        加工领料出库 = 5
    }

    //旧件入库类型
    public enum DcsUsedPartsInboundOrderInboundType {
        服务站返件入库 = 1,
        调拨入库 = 2,
        借用归还 = 3,
        加工入库 = 4
    }

    //旧件出库状态
    public enum DcsUsedPartsOutboundStatus {
        待出库 = 1,
        部分出库 = 2,
        出库完成 = 3,
        终止出库 = 4
    }

    //旧件入库状态
    public enum DcsUsedPartsInboundStatus {
        待入库 = 1,
        部分入库 = 2,
        入库完成 = 3,
        终止入库 = 4
    }

    //旧件处理单状态
    public enum DcsUsedPartsDisposalBillStatus {
        新建 = 1,
        生效 = 2,
        作废 = 99
    }

    //旧件调拨单状态
    public enum DcsUsedPartsTransferOrderStatus {
        新建 = 1,
        生效 = 2,
        作废 = 99
    }

    //旧件加工单状态
    public enum DcsUsedPartsRefitBillStatus {
        新建 = 1,
        生效 = 2,
        作废 = 99
    }

    //旧件借用单状态
    public enum DcsUsedPartsLoanBillStatus {
        新建 = 1,
        生效 = 2,
        作废 = 99
    }

    //旧件移库单状态
    public enum DcsUsedPartsShiftOrderStatus {
        生效 = 1
    }

    //索赔单类型
    public enum DcsClaimBillType {
        维修索赔 = 1,
        保养索赔 = 2,
        服务活动索赔 = 3,
        配件索赔 = 4
    }

    //产品生命周期
    public enum DcsPartsBranchProductLifeCycle {
        新件 = 1,
        低频件 = 2,
        正常件 = 3,
        老件 = 4,
        淘汰件 = 5,
        技改件 = 6,
        停产件 = 7
    }

    //层次结构用途类型
    public enum DcsLayerStructurePurposePurposeType {
        维修项目分类 = 1,
        故障项目分类 = 2,
        产品分类 = 3
    }

    //服务活动适用范围
    public enum DcsServiceActivityApplicationScope {
        指定车辆清单 = 1,
        指定车辆条件不限台次 = 2,
        指定车辆与服务站关系清单 = 3,
        指定车辆条件与服务站配额 = 4,
        指定车辆条件并限制台次 = 5
    }

    //证件类型
    public enum DcsCustomerIdDocumentType {
        居民身份证 = 65011001,
        组织机构代码证 = 65011002,
        护照 = 65011003,
        军官证 = 65011004,
        //其它 = 65011005,
        统一社会信用代码 = 65011006
    }

    //行业类型
    public enum DcsCustomerBusinessType {
        交通_运输_物流 = 40351001,
        批发和零售 = 40351002,
        建筑_设计_装潢 = 40351003,
        采掘业_冶炼 = 40351004,
        制造_机械_设备 = 40351005,
        非盈利机构_政府 = 40351006,
        农业_渔业_林业 = 40351007,
        汽车及零配件 = 40351008,
        耐用消费品_服装_纺织_家具_家电_工艺品 = 40351009,
        快速消费品_食品_饮料_化妆品 = 40351010,
        服务业 = 40351011,
        酒店_餐饮_旅游 = 40351012,
        房地产 = 40351013,
        化工_能源 = 40351014,
        印刷_包装 = 40351015,
        生物_制药_保健_医药 = 40351016,
        贸易 = 40351017,
        广告_公关_会展 = 40351018,
        媒体_出版 = 40351019,
        咨询业 = 40351020,
        法律 = 40351021,
        中介服务 = 40351022,
        通讯_电信 = 40351023,
        互联网_电子商务 = 40351024,
        电子技术 = 40351025,
        计算机 = 40351026,
        金融_保险_证券 = 40351027,
        教育_培训 = 40351029,
        娱乐_体育 = 40351030,
        学术_科研_艺术 = 40351031,
        其它行业 = 40351032
    }

    //职业类型
    public enum DcsCustomerOccupationType {
        服务行业_餐饮_美容等服务员 = 65021001,
        个体户 = 65021002,
        工人 = 65021003,
        国营企业高层管理人员 = 65021004,
        国营企业一般职员 = 65021005,
        国营企业中层管理人员 = 65021006,
        教师_教授 = 65021007,
        农民 = 65021008,
        私营企业主 = 65021009,
        私营一般职员 = 65021010,
        外资_合资企业高层管理人员 = 65021011,
        外资_合资企业一般职员 = 65021012,
        外资_合资企业中层管理人员 = 65021013,
        销售人员_市场推广人员 = 65021014,
        学生 = 65021015,
        学术研究人员 = 65021016,
        演艺人员 = 65021017,
        政府机关领导干部 = 65021018,
        政府机关一般行政人员 = 65021019,
        专业人员_律师_会计_医生等 = 65021020,
        自由职业者_作家_艺术家等 = 65021021,
        城市务工人员 = 65021022,
        其它 = 65021023
    }

    //文化程度
    public enum DcsCustomerEducationLevel {
        初中及以下 = 65041001,
        高中_中专_技校_高职 = 65041002,
        大专 = 65041003,
        本科 = 65041004,
        硕士及以上 = 65041005
    }

    //基础客户类型
    public enum DcsCustomerCustomerType {
        个人客户 = 1,
        组织客户 = 2
    }

    //价格类型
    public enum DcsPartsSalesPricePriceType {
        基准销售价 = 1,
        全国统一价 = 2
    }

    //配件销售价状态
    public enum DcsPartsSalesPriceStatus {
        新建 = 1,
        生效 = 2,
        作废 = 99
    }

    //采购订单状态
    public enum DcsPartsPurchaseOrderStatus {
        新增 = 1,
        提交 = 2,
        部分确认 = 3,
        确认完毕 = 4,
        部分发运 = 5,
        发运完毕 = 6,
        终止 = 7,
        作废 = 99
    }

    //供应商发运单状态
    public enum DcsSupplierShippingOrderStatus {
        新建 = 1,
        收货确认 = 2,
        部分确认 = 3,
        终止 = 4,
        取消 = 5,
        作废 = 99
    }

    //企业类型
    public enum DcsCompanyType {
        分公司 = 1,
        服务站 = 2,
        代理库 = 3,
        物流公司 = 4,
        责任单位 = 5,
        配件供应商 = 6,
        服务站兼代理库 = 7,
        集团企业 = 8,
        出口客户 = 10
    }

    //服务活动类型
    public enum DcsServiceActivityType {
        车辆召回 = 1,
        特殊服务活动 = 2,
        一般服务活动 = 3,
        整改 = 4
    }

    //简单审核单据状态
    public enum DcsWorkflowOfSimpleApprovalStatus {
        新建 = 1,
        已审核 = 2,
        作废 = 99
    }

    //服务活动计费模式
    public enum DcsServiceActivityBillingMethod {
        固定费用 = 1,
        固定维修项目 = 2,
        按实际发生收取 = 3
    }

    //核算领域
    public enum DcsAccountGroupAccountType {
        配件销售 = 1,
        整车销售 = 2
    }

    //票据类型
    public enum DcsAccountReceivableBillType {
        商业承兑汇票 = 1,
        银行承兑汇票 = 2
    }

    //使用条件
    public enum DcsCredenceApplicationApplyCondition {
        合同范围内使用 = 1,
        自由支配使用 = 2
    }

    //运算符
    public enum DcsServiceActivityConditionMathOperator {
        大于 = 1,
        大于等于 = 2,
        等于 = 3,
        小于 = 4,
        小于等于 = 5
    }

    //摘要用途
    public enum DcsSummaryPurpose {
        收款 = 1,
        收款转账 = 2,
        付款 = 3,
        付款转账 = 4
    }

    //数据类型
    public enum DcsServiceActivityConditionDataType {
        日期 = 1,
        布尔 = 2,
        整数 = 3,
        字符 = 4
    }

    //来款单状态
    public enum DcsPaymentBillStatus {
        新建 = 1,
        已审核 = 2,
        已分割 = 3,
        作废 = 99
    }

    //采购订单类型
    public enum DcsPartsPurchaseOrderOrderType {
        月度采购订单 = 1,
        周度采购订单 = 2,
        紧急采购订单 = 3,
        专项采购订单 = 4
    }

    //应收票据状态
    public enum DcsAccountReceivableBillStatus {
        新增 = 1,
        已审核 = 2,
        已贴现 = 3,
        已背书转让 = 4,
        已兑现 = 5,
        作废 = 99
    }

    //付款方式
    public enum DcsPayOutBillPaymentMethod {
        电汇 = 1,
        现金 = 2,
        银行转账 = 3,
        承兑汇票 = 4
    }

    //配件仓库类型
    public enum DcsWarehouseType {
        总库 = 1,
        分库 = 2,
        虚拟库 = 99
    }

    //物流公司业务领域
    public enum DcsLogisticCompanyBusinessDomain {
        配件发运 = 1,
        旧件返回 = 2,
        整车发运 = 3
    }

    //外出服务索赔单状态
    public enum DcsServiceTripClaimBillStatus {
        新增 = 1,
        提交 = 2,
        初审通过 = 4,
        生效 = 6,
        审核不通过 = 10,
        作废 = 99
    }

    //扣补款单状态
    public enum DcsExpenseAdjustmentBillStatus {
        新增 = 1,
        生效 = 2,
        作废 = 99
    }

    //扣补款类型
    public enum DcsExpenseAdjustmentBillTransactionCategory {
        工时费 = 1,
        材料费 = 2,
        配件管理费 = 3,
        非负调整 = 4,
        政策性补贴费 = 5,
        服务管理奖励费 = 6,
        服务管理扣罚费 = 7,
        批量质量信誉索赔费 = 8,
        拖车费 = 9,
        质量事故费 = 10,
        其他 = 11,
        外出费用 = 12,
        让渡费用 = 13,
        旧件运费 = 14,
        强保工时费 = 15,
        强保材料费 = 16,
        强保配件管理费 = 17,
        会员积分费 = 18,
        红包费 = 19,
    }

    //扣补款源单据类型
    public enum DcsExpenseAdjustmentBillSourceType {
        维修索赔 = 1,
        保养索赔 = 2,
        服务活动索赔 = 3,
        配件索赔 = 4,
        外出服务索赔 = 5,
        售前检查 = 6,
        积分结算 = 7,
        红包结算 = 8,
    }

    //维修索赔单单据状态
    public enum DcsRepairClaimBillRepairClaimStatus {
        新增 = 1,
        提交 = 2,
        初审通过 = 3,
        生效 = 4,
        审核不通过 = 6,
        作废 = 99
    }

    //保养索赔单单据状态
    public enum DcsRepairClaimBillMaintClaimStatus {
        新增 = 1,
        提交 = 2,
        生效 = 4,
        已向责任单位索赔 = 4,
        审核不通过 = 6,
        作废 = 99
    }

    //服务活动索赔单单据状态
    public enum DcsRepairClaimBillServiceActivityStatus {
        新增 = 1,
        提交 = 2,
        初审通过 = 3,
        生效 = 4,
        审核不通过 = 5,
        作废 = 99
    }

    //配件索赔单单据状态
    public enum DcsPartsClaimBillStatus {
        新增 = 1,
        提交 = 2,
        审核通过 = 3,
        初审通过 = 4,
        审核不通过 = 5,
        作废 = 99
    }

    //索赔单结算状态
    public enum DcsClaimBillSettlementStatus {
        不结算 = 1,
        待结算 = 2,
        已结算 = 3
    }

    //申请单单据状态
    public enum DcsApplicationBillStatus {
        新增 = 1,
        提交 = 2,
        初审通过 = 3,
        生效 = 4,
        作废 = 99
    }

    //索赔单供应商确认状态
    public enum DcsRepairClaimBillSupplierConfirmStatus {
        未确认 = 1,
        已确认 = 2,
        确认未通过 = 3
    }

    //索赔单旧件处理状态
    public enum DcsRepairClaimBillUsedPartsDisposalStatus {
        旧件未全部到达 = 1,
        旧件全部到达 = 2
    }

    //索赔单维修项目清单状态
    public enum DcsRepairClaimItemDetailApproveStatus {
        未审核 = 0,
        审核通过 = 1,
        审核未通过 = 2
    }

    //索赔单维修材料清单旧件处理状态
    public enum DcsRepairClaimMaterialDetailUsedPartsDisposalStatus {
        未发运 = 1,
        已发运 = 2,
        已到达 = 3,
        不再发运 = 4
    }

    //维修申请类型
    public enum DcsRepairClaimApplicationRepairApplicationType {
        重大索赔 = 1,
        商品车维修 = 2,
        更换总成及典型故障索赔 = 3,
        管控件维修 = 4,
        外采或直拨维修 = 5,
        强制保养 = 6,
        商品车保养 = 7,
        批量整改 = 9,
        让度服务 = 10,
        油品补加 = 11
    }

    //保养申请类型
    public enum DcsRepairClaimApplicationMaintApplicationType {
        商品车保养 = 1,
        过期让度 = 2,
        额外让度 = 3
    }

    //扣补款企业类型
    public enum DcsExpenseAdjustmentBillCompanyType {
        服务站 = 1,
        供应商 = 2
    }

    //扣补款方向
    public enum DcsExpenseAdjustmentBillDebitOrReplenish {
        扣款 = 1,
        补款 = 2
    }

    //售出状态
    public enum DcsVehicleInformationSalesStatus {
        未售出 = 1,
        已售出 = 2
    }

    //配件索赔类型
    public enum DcsPartsClaimOrderClaimType {
        更换件索赔 = 1,
        销售件索赔 = 2
    }

    //采购退货原因
    public enum DcsPartsPurReturnOrderReturnReason {
        质量件退货 = 1/*,
        积压件退货 = 2*/
    }

    //原始需求单据类型
    public enum DcsOriginalRequirementBillType {
        配件销售订单 = 1,
        配件销售退货单 = 2,
        积压件调剂单 = 3,
        配件采购订单 = 4,
        配件采购退货单 = 5,
        配件调拨单 = 6,
        内部领入单 = 7,
        内部领出单 = 8,
        配件零售订单 = 9,
        配件零售退货单 = 10,
        企业间配件调拨 = 11,
        配件外采申请单 = 12,
        维修单 = 13,
        配件索赔单 = 14
    }

    //应收账业务类型
    public enum DcsAccountPaymentBusinessType {
        销售出库计划 = 1,
        销售出库 = 2,
        销售退货入库 = 3,
        销售结算 = 4,
        作废销售结算 = 5,
        销售反结算 = 6,
        终止销售出库计划 = 7,
        直供订单预扣款 = 8,
        积压件调剂预扣款 = 9,
        来款 = 10,
        三包费转配件款 = 11,
        开户 = 12,
        注销 = 13,
        企业间转账 = 15,
        客户转账 = 16
    }

    //应付帐业务类型
    public enum DcsAccountPayOutBusinessType {
        开户 = 1,
        注销 = 2,
        采购结算 = 3,
        采购退货结算 = 4,
        付款 = 5
    }

    //配件结算状态
    public enum DcsPartsSettlementStatus {
        不结算 = 1,
        待结算 = 2,
        已结算 = 3
    }

    //积压件申请单状态
    public enum DcsOverstockPartsAppStatus {
        新建 = 1,
        已提交 = 2,
        已审批 = 3,
        作废 = 99
    }

    //积压件调剂单状态
    public enum DcsOverstockPartsAdjustBillStatus {
        新建 = 1,
        生效 = 2,
        确认 = 3,
        完成 = 4,
        终止 = 5,
        作废 = 99
    }

    //配件入库检验单状态
    public enum DcsPartsInboundCheckBillStatus {
        新建 = 1,
        包装完成 = 2,
        上架完成 = 3,
        取消 = 4
    }

    //配件入库类型
    public enum DcsPartsInboundType {
        配件采购 = 1,
        销售退货 = 2,
        配件调拨 = 3,
        内部领入 = 4,
        配件零售退货 = 5,
        积压件调剂 = 6,
        配件外采 = 7,
        质量件索赔 = 8,
        代发退货 = 9
    }

    //配件入库计划状态
    public enum DcsPartsInboundPlanStatus {
        新建 = 1,
        检验完成 = 2,
        终止 = 3,
        部分检验 = 4,
        部分包装 = 5,
        包装完成 = 6,
        部分上架 = 7,
        上架完成 = 8,
        部分收货 = 9,
        收货完成 = 10,
        取消 = 11,
        强制检验完成 = 12
    }

    //配件出库类型
    public enum DcsPartsOutboundType {
        配件销售 = 1,
        采购退货 = 2,
        内部领出 = 3,
        配件调拨 = 4,
        积压件调剂 = 5,
        配件零售 = 6,
        质量件索赔 = 7
    }

    //配件出库计划状态
    public enum DcsPartsOutboundPlanStatus {
        新建 = 1,
        部分出库 = 2,
        出库完成 = 3,
        终止 = 4,
        部分分配 = 5,
        分配完成 = 6,
        拣货中 = 7,
        部分拣货 = 8,
        拣货完成 = 9,
        部分装箱 = 10,
        装箱完成 = 11,
        部分发运 = 12,
        发运完成 = 13
    }

    //配件在途状态
    public enum DcsPartsLogisticBatchShippingStatus {
        待运 = 1,
        发运 = 2,
        供应商发运 = 3
    }

    //配件批次看板单据类型
    public enum DcsPartsLogisticBatchBillDetailBillType {
        配件出库单 = 1,
        配件发运单 = 2,
        供应商发运单 = 3,
        配件入库计划 = 4,
        配件入库检验单 = 5
    }

    //配件批次看板状态
    public enum DcsPartsLogisticBatchStatus {
        新增 = 1,
        完成 = 2
    }

    //配件盘点单状态
    public enum DcsPartsInventoryBillStatus {
        新建 = 1,
        已结果录入 = 2,
        已库存覆盖 = 3,
        审核通过 = 4,
        初审通过 = 5,
        审批通过 = 6,
        高级审核通过=7,
        作废 = 99
    }

    //配件移库单状态
    public enum DcsPartsShiftOrderStatus {
        生效 = 1,
        新建 = 2,
        初审通过 = 3,
        作废 = 99,
        提交 = 4,
        终止 = 5 
    }

    //配件移库类型
    public enum DcsPartsShiftOrderType {
        上架 = 1,
        正常移库 = 2,
        问题区移库 = 3
    }

    //配件调拨单状态
    public enum DcsPartsTransferOrderStatus {
        新建 = 1,
        已审批 = 2,
        初审通过 = 3,
        作废 = 99
    }

    //配件调拨类型
    public enum DcsPartsTransferOrderType {
        储备调拨 = 1,
        专项调拨 = 2,
        急需调拨 = 3
    }

    //配件发运单状态
    public enum DcsPartsShippingOrderStatus {
        新建 = 1,
        收货确认 = 2,
        回执确认 = 3,
        已发货 = 4,
        待提货 = 5,
        待收货 = 6,
        作废 = 99
    }

    //到货模式
    public enum DcsArrivalMode {
        送货上门 = 1,
        货运站 = 2
    }

    //计划价申请单状态
    public enum DcsPlannedPriceAppStatus {
        新增 = 1,
        已审核 = 2,
        已执行 = 3,
        作废 = 99
    }

    //计账状态
    public enum DcsRecordStatus {
        不需要计账 = 1,
        未计账 = 2,
        已计账 = 3
    }

    //保有客户状态
    public enum DcsRetainedCustomerStatus {
        有效 = 1,
        流失 = 99
    }

    //销售订单状态
    public enum DcsPartsSalesOrderStatus {
        新增 = 1,
        提交 = 2,
        撤单 = 3,
        部分审批 = 4,
        审批完成 = 5,
        终止 = 6,
        作废 = 99
    }

    //信用申请单状态
    public enum DcsCredenceApplicationStatus {
        新增 = 1,
        审批通过 = 2,
        有效 = 3,
        失效 = 4,
        初审通过 = 5,
        已提交 = 6,
        审核通过 = 7,
        高级审核通过 = 8,
        作废 = 99
    }

    //服务站索赔结算源单据类型
    public enum DcsSsClaimSettlementDetailSourceType {
        维修索赔单 = 1,
        保养索赔单 = 2,
        服务活动索赔单 = 3,
        配件索赔单 = 4,
        外出服务索赔单 = 5,
        扣补款单 = 6,
        售前检查单 = 7,
        旧件入库单 = 8
    }

    //服务站索赔结算单状态
    public enum DcsSsClaimSettlementBillStatus {
        新增 = 1,
        已审批 = 2,
        发票登记 = 3,
        费用兑现 = 4,
        作废 = 99
    }

    //索赔结算指令状态
    public enum DcsSsClaimSettleInstructionStatus {
        有效 = 1,
        执行成功 = 2,
        执行失败 = 3,
        作废 = 99
    }

    //配件销售退货单状态
    public enum DcsPartsSalesReturnBillStatus {
        新增 = 1,
        提交 = 2,
        审核通过 = 3,
        终止 = 4,
        初审通过 = 5,
        审批通过 = 6,
        作废 = 99
    }

    //配件销售退货开票要求
    public enum DcsPartsSalesReturnBillInvoiceRequirement {
        出库合并开票 = 1,
        反开销售发票 = 2,
        开红字发票 = 3
    }

    //配件销售退货单退货类型
    public enum DcsPartsSalesReturnBillReturnType {
        正常退货 = 1,
        保底退货 = 2,
        特殊退货 = 3
    }

    //配件零售订单状态
    public enum DcsPartsRetailOrderStatus {
        新建 = 1,
        审批 = 2,
        开票 = 3,
        作废 = 99
    }

    //配件零售退货单状态
    public enum DcsPartsRetailReturnBillStatus {
        新建 = 1,
        审批 = 2,
        退款 = 3,
        作废 = 99
    }

    //订单处理方式
    public enum DcsPartsSalesOrderProcessDetailProcessMethod {
        未处理 = 1,
        本部处理 = 2,
        积压件平台 = 3,
        供应商直发 = 4,
        其他品牌调拨 = 5,
        统购分销 = 10,
        转中心库 = 11,
        电商平台审核 = 12,
        转中心库采购 = 13
    }

    //配件发运单类型
    public enum DcsPartsShippingOrderType {
        销售 = 1,
        退货 = 2,
        调剂 = 3,
        调拨 = 4,
        质量件索赔 = 5,
        领用 = 6
    }

    //配件采购结算单状态
    public enum DcsPartsPurchaseSettleStatus {
        新建 = 1,
        已审批 = 2,
        发票登记 = 3,
        已反冲 = 4,
        发票驳回 = 5,
        发票已审核 = 6,
        作废 = 99
    }

    //结算方向
    public enum DcsPartsPurchaseSettleBillSettlementPath {
        正常结算 = 1,
        反冲结算 = 2
    }

    //开票方向
    public enum DcsPartsPurchaseRtnSettleBillInvoicePath {
        开红字发票 = 1,
        反开销售发票 = 2
    }

    //配件采购结算关联单据类型
    public enum DcsPartsPurchaseSettleRefSourceType {
        配件入库检验单 = 5,
        配件出库单 = 1
    }

    //配件来款方式
    public enum DcsPaymentBillPaymentMethod {
        服务费 = 7
    }

    //发票源单据类型
    public enum DcsInvoiceInformationSourceType {
        配件销售结算单 = 1,
        配件销售退货结算单 = 2,
        配件采购结算单 = 3,
        配件采购退货结算单 = 4,
        服务站索赔结算单 = 5
    }

    //订单处理状态
    public enum DcsPartsSalesOrderProcessDetailOrderProcessStatus {
        满足 = 1,
        未满足 = 2
    }

    //配件销售结算单状态
    public enum DcsPartsSalesSettlementStatus {
        新建 = 1,
        已审批 = 2,
        发票登记 = 3,
        已反冲 = 4,
        作废 = 99
    }

    //配件销售结算关联单据类型
    public enum DcsPartsSalesSettlementRefSourceType {
        配件出库单 = 1,
        配件入库检验单 = 5
    }

    //配件销售退货结算单状态
    public enum DcsPartsSalesRtnSettlementStatus {
        新建 = 1,
        已审批 = 2,
        发票登记 = 3,
        已反冲 = 4,
        作废 = 99
    }

    //发票类型
    public enum DcsInvoiceInformationType {
        增值税发票 = 0,
        普通发票 = 1,
        电子普通发票 = 2,
        不开发票 = 3
    }

    //发票用途
    public enum DcsInvoiceInformationInvoicePurpose {
        整车销售 = 1,
        配件采购 = 2,
        配件销售 = 3,
        三包费用 = 4
    }

    //发票状态
    public enum DcsInvoiceInformationStatus {
        待开票 = 1,
        已开票 = 2,
        生效 = 3,
        作废 = 99
    }

    //配件物流源单据类型
    public enum DcsPartsLogisticBatchSourceType {
        配件出库计划 = 1,
        配件采购订单 = 2
    }

    //配件销售结算返利方式
    public enum DcsPartsSalesSettlementRebateMethod {
        直接折扣 = 1,
        返利池 = 2
    }

    //整车订单状态
    public enum DcsVehicleOrderStatus {
        拒绝 = 99,
        新建 = 1,
        提交 = 2,
        已确认 = 3,
        部分确认 = 4,
        部分审批 = 5,
        完成 = 6
    }

    //整车订单清单状态
    public enum DcsVehicleOrderDetailStatus {
        新建 = 1,
        拒绝 = 2,
        已确认 = 3,
        完成 = 4
    }

    //整车订单类型
    public enum DcsVehicleOrderOrderType {
        普通订单 = 1,
        紧急订单 = 2
    }

    //整车订单变更单清单状态
    public enum DcsVehicleOrderChangeDetailStatus {
        新建 = 1,
        拒绝 = 2,
        已确认 = 3,
        完成 = 4
    }

    //匹配类型
    public enum DcsVehiclePreAllocationRefMatchedType {
        订单 = 1,
        库存 = 2
    }

    //预分车关联单状态
    public enum DcsVehiclePreAllocationRefStatus {
        未匹配 = 1,
        库存匹配 = 2,
        处理完成 = 3,
        作废 = 99
    }

    //整车生产计划状态
    public enum DcsProductionPlanStatus {
        接受 = 1,
        拒绝 = 2
    }

    //JUMP月订单上传类型
    public enum DcsVehicleOrderPlanSummaryUploadType {
        新增 = 1,
        删除 = 2
    }

    //整车订单上传状态
    public enum DcsVehicleOrderPlanSummaryVehicleOrderUploadStatus {
        未上传 = 1,
        已上传 = 2
    }

    //整车退货单状态
    public enum DcsVehicleReturnOrderStatus {
        新增 = 1,
        作废 = 99,
        处理完成 = 2,
        经销商已出库 = 3,
        退货入库 = 4
    }

    //整车发运单状态
    public enum DcsVehicleShippingOrderStatus {
        新增 = 1,
        作废 = 99,
        已出库 = 2,
        允许接车 = 3,
        已运达 = 4
    }

    //整车发运方式
    public enum DcsVehicleShippingMethod {
        空运 = 1,
        水运 = 2,
        人工驾送 = 3,
        自提 = 4,
        公路 = 5,
        公铁联运 = 6,
        水陆联运 = 7,
        铁路 = 8,
        板车 = 9
    }

    //接车状态
    public enum DcsVehicleShippingDetailVehicleReceptionStatus {
        未接车 = 1,
        已接车 = 2
    }

    //预测车型
    public enum DcsDealerForecastDetailVehicleModel {
        Mazda3经典款2012年型 = 1,
        Mazda3_BU = 2,
        Mazda2 = 3,
        Mazda3星骋5HB = 4,
        Mazda3星骋SDN = 5
    }

    //预测厢型
    public enum DcsDealerForecastDetailWagonType {
        两厢 = 1,
        三厢 = 2,
        劲翔三厢 = 3
    }

    //预测排量
    public enum DcsDealerForecastDetailEngineCylinder {
        _1L3 = 1,
        _1L5 = 2,
        _1L6 = 3,
        _2L0 = 4
    }

    //整车发车审批单状态
    public enum DcsVehicleShipplanApprovalStatus {
        新增 = 1,
        作废 = 99,
        有效 = 2,
        部分出库 = 3,
        出库完成 = 4
    }

    //整车发车审批单清单状态
    public enum DcsVehicleShipplanApprovalDetailStatus {
        新增 = 1,
        有效 = 2,
        出库 = 3,
        作废 = 99
    }

    //可用资源质量状态
    public enum DcsVehicleAvailableResourceQualityStatus {
        合格 = 1,
        不合格 = 2
    }

    //可用资源锁定状态
    public enum DcsVehicleAvailableResourceLockStatus {
        未锁定 = 1,
        预分车锁定 = 2,
        审批单锁定 = 3
    }

    //单车跟踪表原单据类型
    public enum DcsVehicleTrackingFormSourceBillType {
        发车审批单 = 1,
        退货单 = 2
    }

    //单车跟踪状态
    public enum DcsVehicleTrackingFormStatus {
        在库 = 1,
        出库 = 2,
        经销商在库 = 3,
        经销商售出 = 4
    }

    //控制日期类型
    public enum DcsVehicleOrderChangePolicyShippingDateControl {
        计划发车日期 = 1,
        期望发车日期 = 2
    }

    //整车批发价格折扣等级清单价格计算方式
    public enum DcsVehPifaPriceDiscLevelDetailPriceCalculationMethod {
        批发价x加价率 = 1,
        批发价plus加价额 = 2,
        批发价x加价率plus加价额 = 3
    }

    //整车订货计划状态
    public enum DcsVehicleOrderPlanStatus {
        新建 = 1,
        提交 = 2,
        有效 = 3,
        作废 = 99
    }

    //经销商预测单状态（项目）
    public enum DcsDealerForecastStatus {
        新建 = 1,
        经销商审核未通过 = 2,
        区域经理待审批 = 3,
        区域经理审批通过 = 4,
        区域经理审批不通过 = 5,
        作废 = 99
    }

    //车辆类型
    public enum DcsVehicleInformationVehicleType {
        进口 = 1,
        公务 = 2,
        特殊 = 3,
        普通 = 4,
        一汽 = 5
    }

    //经销商买卖单状态
    public enum DcsDealerTransactionBillStatus {
        新建 = 1,
        已确认 = 2,
        有效 = 3,
        作废 = 99
    }

    //整车价格变更申请单状态
    public enum DcsVehiclePriceChangeAppStatus {
        新建 = 1,
        有效 = 2,
        已执行 = 3,
        作废 = 99
    }

    //整车基础数据状态
    public enum DcsVehicleBaseDataStatus {
        有效 = 1
    }

    //整车发车审批单调整类型
    public enum DcsVehShipplanApprovalChangeRecAdjustmentType {
        可用资源调整 = 1,
        审批单调整 = 2
    }

    //整车车型分类类型
    public enum DcsVehicleModelCategoryType {
        总部月目标车型 = 1,
        经销商月目标车型 = 2
    }

    //整车订货计划汇总单订单类型
    public enum DcsVehicleOrderPlanSummaryOrderType {
        订单 = 1,
        预测单 = 2
    }

    //整车仓库类型
    public enum DcsVehicleWarehouseType {
        总库 = 1,
        分库 = 2,
        虚拟库 = 3
    }

    //配件返利源单据类型
    public enum DcsPartsRebateChangeDetailSourceType {
        返利申请单 = 1,
        销售结算单 = 2
    }

    //库存状态
    public enum DcsDealerVehicleStockInventoryStatus {
        在途 = 0,
        在库 = 1,
        出库 = 2
    }

    //PDI检查状态
    public enum DcsDealerVehicleStockPDIStatus {
        不合格 = 0,
        合格 = 1
    }

    //产生库存类型
    public enum DcsDealerVehicleStockSourceType {
        采购入库 = 0,
        退货入库 = 1,
        调拨入库 = 2,
        经销商买卖入库 = 3
    }

    //库存锁定状态
    public enum DcsDealerVehicleStockLockStatus {
        未锁定 = 0,
        锁定 = 1
    }

    //经销商仓库类型
    public enum DcsDealerWarehouseType {
        整车仓库 = 1,
        配件仓库 = 2,
        旧件仓库 = 3
    }

    //旧件清退单状态
    public enum DcsUsedPartsReturnOrderStatus {
        新建 = 1,
        已审批 = 2,
        作废 = 99
    }

    //旧件清退类型
    public enum DcsUsedPartsReturnOrderReturnType {
        清退责任单位 = 1,
        清退配件供应商 = 2,
        内部部门 = 3
    }

    //配件领用结算单状态
    public enum DcsPartsRequisitionSettleBillStatus {
        新建 = 1,
        已审批 = 2,
        作废 = 99
    }

    //配件领用结算关联单据类型
    public enum DcsPartsRequisitionSettleRefSourceType {
        配件出库单 = 1,
        配件入库检验单 = 5
    }

    //凭证摘要
    public enum DcsVehiclePaymentBillCertificateSummary {
        整车款 = 1,
        维修款 = 2
    }

    //整车来款单状态
    public enum DcsVehiclePaymentBillStatus {
        新增 = 1,
        作废 = 99,
        有效 = 2
    }

    //整车汇票状态
    public enum DcsVehicleAccountReceivableBillStatus {
        新增 = 1,
        作废 = 99,
        已接收 = 2,
        银行查询 = 3,
        审核通过 = 4,
        已入账 = 5,
        转出申请 = 6,
        转出审批 = 7,
        出纳付票 = 8
    }

    //整车应收票据类型
    public enum DcsVehicleAccountReceivableBillType {
        两方承兑汇票 = 1,
        三方承兑汇票 = 2,
        质押汇票 = 3,
        其它 = 4
    }

    //整车价格状态
    public enum DcsVehiclePriceStatus {
        新建 = 1,
        有效 = 2,
        作废 = 99
    }

    //账户可用性
    public enum DcsVehicleCustomerAccountAvailability {
        可用 = 1,
        不可用 = 2
    }

    //整车经销商账户冻结状态
    public enum DcsVehicleDLRAccountFreezeBillStatus {
        新增 = 1,
        有效 = 2,
        作废 = 99
    }

    //整车应收票据转出类型
    public enum DcsVehicleAccountReceivableBillTransferOutType {
        汇票转出 = 1,
        贴现 = 2
    }

    //整车经销商信用额度申请状态
    public enum DcsVehicleDealerCreditLimitAppStatus {
        新建 = 1,
        有效 = 2,
        已执行 = 3,
        失效 = 4,
        作废 = 99
    }

    //整车应收账业务类型
    public enum DcsVehicleCustAccountHisDetailBusinessType {
        整车来款 = 1,
        整车销售出库 = 2,
        整车销售退货 = 3,
        汇票贴息 = 4,
        其它 = 5
    }

    //购车用途
    public enum DcsKeyAccountPurchasePurpose {
        业务用车 = 1,
        领导用车 = 2,
        员工用车 = 3,
        其它 = 4
    }

    //公司规模
    public enum DcsKeyAccountCompanyScale {
        _500以下 = 1,
        _500_3000 = 2,
        _3000以上 = 3
    }

    //批售奖励申请单
    public enum DcsWholesaleRewardAppStatus {
        新增 = 1,
        提交 = 2,
        有效 = 3,
        奖励发放确认 = 4,
        作废 = 99
    }

    //客户细分
    public enum DcsWholesaleRewardAppCustomerDetailCategory {
        汽车租凭 = 1,
        政府 = 2,
        公检法司 = 3,
        军队武警 = 4,
        企业 = 5,
        特殊订单 = 6,
        其它 = 7
    }

    //大客户状态
    public enum DcsKeyAccountStatus {
        作废 = 0,
        新增 = 1,
        已跟进 = 2,
        已成交 = 3,
        战败 = 4
    }

    //业务领域
    public enum DcsBusinessDomain {
        整车 = 1,
        服务 = 2,
        配件 = 3
    }

    //多级审核起始状态
    public enum DcsMultiLevelAuditConfigInitialStatus {
        新建 = 1,
        提交 = 2,
        审核中 = 3,
        审核通过 = 4,
        审核不通过 = 5,
        作废 = 99
    }

    //配件采购定价策略
    public enum DcsBranchstrategyPartsPurchasePricingStrategy {
        先定价 = 1,
        后定价 = 2
    }

    //客户性质
    public enum DcsKeyAccountCustomerProperty {
        车改或组织内员工购车 = 1,
        企事业_政府_社团 = 2,
        租赁 = 3,
        个人 = 4,
        其它 = 5
    }

    //员工购车可提交文件
    public enum DcsWholesaleApprovalEmployeePurchaseDocType {
        社保证明 = 1,
        公积金证明 = 2,
        完税证明 = 3,
        其它 = 4
    }

    //配件保内保外供货属性
    public enum DcsPartsBranchWarrantySupplyStatus {
        保内专供 = 1,
        保外专供 = 2,
        保内保外通用 = 3
    }

    //配件发运路损处理方式
    public enum DcsPartsShippingOrderTransportLossesDisposeMethod {
        销售退回 = 1,
        销售承运商 = 2,
        正常收货 = 3
    }

    //经销商盘点单状态
    public enum DcsDealerPartsInventoryBillStatus {
        新建 = 1,
        审核通过 = 2,
        初审通过 = 3,
        审批通过 = 4,
        生效 = 5,
        高级审核通过 = 6,
        作废 = 99
    }

    //配件外采原因
    public enum DcsPartsOuterPurchaseOuterPurchaseComment {
        抱怨客户 = 1,
        配件代理库无配件 = 2,
      //  CDC配件库无配件 = 3,
        停车待件 = 4,
        装饰件 = 5,
        标准件 = 6,
        索赔单作废 = 7,
        保养品 = 10
    }

    //配件外采申请状态
    public enum DcsPartsOuterPurchaseChangeStatus {
        新建 = 1,
        提交 = 2,
        生效 = 3,
        初审通过 = 4,
        审核通过 = 5,
        作废 = 99,
        确认通过 = 6,
        审批通过 = 7,
        高级审核通过 = 8
    }

    //路损处理状态
    public enum DcsPartsShippingOrderTransportLossesDisposeStatus {
        未处理 = 1,
        处理完毕 = 2
    }

    //配件运费计费方式
    public enum DcsPartsShippingOrderBillingMethod {
        吨_公里 = 1,
        体积 = 2,
        配件金额 = 3,
        包车 = 4
    }

    //配件销售价格变更申请状态
    public enum DcsPartsSalesPriceChangeStatus {
        新建 = 1,
        初审通过 = 2,
        生效 = 3,
        提交 = 4,
        审核通过 = 5,
        作废 = 99
    }

    //配件发运路损责任
    public enum DcsPartsShippingOrderDetailTransportLossesDispose {
        销售退回 = 1,
        销售承运商 = 2,
        正常收货 = 3
    }

    //快照状态
    public enum DcsSnapshoStatus {
        新建 = 1,
        生效 = 2,
        作废 = 99
    }

    //返利方向
    public enum DcsPartsRebateDirection {
        增加 = 1,
        扣减 = 2
    }

    //保内保外供货属性
    public enum DcsWarrantySupplyStatus {
        保内专供 = 1,
        保外专供 = 2,
        保内保外通用 = 3
    }

    //配件仓储管理粒度
    public enum DcsPartsBranchPartsWarhouseManageGranularity {
        批次管理 = 1,
        序列号管理 = 2,
        无批次管理 = 3
    }

    //维修工单状态
    public enum DcsRepairWorkOrderStatus {
        新建 = 1,
        已维修 = 2,
        异常终止 = 99
    }

    //申请单审核状态
    public enum DcsApplicationBilCheckStatus {
        初审 = 1,
        会签 = 2,
        审核 = 3,
        审批 = 4,
        通过 = 5
    }

    //维修单状态
    public enum DcsRepairStatus {
        二级站新建 = 1,
        二级站提交 = 2,
        来客登记 = 3,
        新建 = 4,
        维修待件 = 5,
        维修完工 = 6,
        生成索赔 = 7,
        已锁定 = 98,
        作废 = 99
    }

    //维修类型
    public enum DcsRepairType {
        普通维修 = 1,
        外出服务 = 2,
        强制保养 = 3,
        普通保养 = 4,
        商品车维修 = 5,
        事故维修 = 6,
        服务活动 = 7,
        商品车保养 = 8,
        让度服务 = 15,
        延保维修 = 16
    }

    //配件来源
    public enum DcsPartSource {
        正常 = 1,
        应急 = 2,
        外采 = 3
    }

    //企业间配件调拨类型
    public enum DcsCompanyTransferOrderType {
        服务站兼代理库调拨 = 2,
        紧急调拨 = 5,
        储备调拨 = 10
    }

    //驳回状态
    public enum DcsRejectStatus {
        未驳回 = 1,
        维修驳回 = 2,
        外出驳回 = 3,
        已驳回 = 4,
        全部驳回 = 5
    }

    //外出服务申请单状态
    public enum DcsServiceTripClaimApplicationStatus {
        新增 = 1,
        提交 = 2,
        初审通过 = 3,
        会签中 = 4,
        会签完成 = 5,
        终审通过 = 6,
        审核不通过 = 7,
        作废 = 99
    }

    //配件申请单状态
    public enum DcsPartsClaimApplication {
        新增 = 1,
        提交 = 2,
        初审通过 = 3,
        会签中 = 4,
        会签完成 = 5,
        终审通过 = 6,
        审核不通过 = 7,
        作废 = 99
    }

    //维修保养申请单状态
    public enum DcsRepairClaimApplication {
        新增 = 1,
        提交 = 2,
        初审通过 = 3,
        会签中 = 4,
        会签完成 = 5,
        终审通过 = 6,
        审核不通过 = 7,
        作废 = 99
    }

    //终止提报索赔起始时间类型
    public enum DcsStopClaimStartStatus {
        维修单生成后 = 1,
        维修完工后 = 2,
        呼叫中心维修完工后 = 3,
        扫码完工后 = 4
    }

    //维修单索赔状态
    public enum DcsRepairClaimStatus {
        未索赔 = 1,
        已索赔 = 2
    }

    //维修单结算属性
    public enum DcsRepairSettleAttribute {
        索赔 = 1,
        自费 = 2
    }

    //维修项目等级
    public enum DcsRepairItemItemGrade {
        大修 = 1,
        中修 = 2,
        小修 = 3
    }

    //保外维修类型
    public enum DcsWarrantyRepairTypeStatus {
        外出维修 = 1,
        普通维修 = 2,
        定期保养 = 5
    }

    //保外维修单状态
    public enum DcsWarrantyRepairStatus {
        新建 = 1,
        维修待件 = 2,
        维修完工 = 3,
        作废 = 99
    }

    //保内包围属性
    public enum DcsWarrantyStatus {
        保内 = 1,
        保外 = 2
    }

    //供应商配件索赔价格类型
    public enum DcsBranchSupplierRelationClaimPriceCategory {
        批发价 = 1,
        合同价 = 2
    }

    //领退料属性
    public enum DcsRequisitionReturnStatus {
        领料 = 1,
        退料 = 2
    }

    //二级站需求计划状态
    public enum DcsSecondClassStationPlanStatus {
        新建 = 1,
        提交 = 2,
        审核通过 = 3,
        终止 = 10,
        作废 = 99
    }

    //商品车售前检查状态
    public enum DcsCommercialvehicleCheckStatus {
        新建 = 1,
        提交 = 2,
        审核通过 = 3,
        作废 = 99
    }

    //二级站需求计划处理方式
    public enum DcsProcessMode {
        本企业满足 = 1,
        转销售订单 = 2
    }

    //采购价格类型
    public enum DcsPurchasePriceType {
        正式价格 = 1,
        临时价格 = 2
    }

    //车桥类型
    public enum DcsBridgeType {
        单桥 = 1,
        多桥 = 2
    }

    //维修附加属性
    public enum DcsRepairAddition {
        驾送维修 = 1,
        经销商接车维修 = 2,
        经销商库存车维修 = 3
    }

    //发动机整机索赔
    public enum DcsEngineClaim {
        整机索赔 = 1,
        非整机索赔 = 2
    }

    //发动机保修性质
    public enum DcsEngineWarrantyProperties {
        新零件保修 = 1,
        发动机保修 = 2
    }

    //发动机应用领域
    public enum DcsEngineApplicationArea {
        轻_重卡 = 1,
        商务车 = 2,
        公交 = 3,
        工程机械 = 4
    }

    //派工来源
    public enum DcsDispatchingSource {
        呼叫中心接口 = 1,
        实物监工接口 = 2
    }

    //派工类型
    public enum DcsDispatching {
        维修派工 = 1,
        外出派工 = 2,
        售前检查派工 = 3,
        服务活动 = 10
    }

    //售前检查状态
    public enum DcsPreSaleStatus {
        新建 = 1,
        提交 = 2,
        已审核 = 3,
        审核不通过 = 4,
        作废 = 99
    }

    //更换总成件原因
    public enum DcsChangeAssemblyReason {
        无拆散件 = 1,
        纠纷让度 = 2
    }

    //模板类型
    public enum DcsTempletType {
        维修模板 = 1,
        保养模板 = 2
    }

    //发动机损坏程度
    public enum DcsEngineDamagedCondition {
        一般 = 1,
        严重 = 2
    }

    //联系人类型
    public enum DcsVehicleLinkmanLinkmanType {
        车主 = 65151001,
        司机 = 65151002,
        联络人 = 65151003,
        送修人 = 65151004,
        车队长 = 65151005,
        设备经理 = 65151006,
        实际车主 = 65151007,

    }

    //索赔单供应商复核状态
    public enum DcsRepairClaimBillSupplierCheckStatus {
        未复核 = 1,
        已复核 = 2,
        复核不通过 = 3
    }

    //车辆保养类型
    public enum DcsVehicleMainteTermMainteType {
        强制保养 = 1,
        定期保养 = 2
    }

    //服务站发票状态
    public enum DcsDealerInvoiceStatus {
        新增 = 1,
        提交 = 2,
        初审通过 = 3,
        生效 = 4,
        付款完成 = 5,
        审核不通过 = 6,
        作废 = 99
    }

    //发票类别
    public enum DcsInvoiceSorts {
        电脑版 = 1,
        人工版 = 2
    }

    //服务站类型
    public enum DcsDealerServiceInfoServiceStationType {
        模拟站 = 1,
        星级服务中心 = 2,
        服务站 = 3,
        快修店 = 4,
        服务工程师 = 5,
        标杆服务站 = 6,
        自保站 = 7,
        大客户自保站 = 8
    }

    //旧件运距类型
    public enum DcsUsedPartsDistanceType {
        服务站_旧件库 = 1,
        旧件库_旧件总库 = 2
    }

    //服务站配件零售类型
    public enum DcsDealerPartsRetailOrderRetailOrderType {
        普通客户 = 1,
        二级站 = 2,
        大客户 = 10,
        社会修理厂 = 15,
        社会配件经销商 = 20
    }

    //二级站出库类型
    public enum DcsDealerPartsRetailOrderSubDealerlOrderType {
        配件零售 = 1
    }

    //产品线类型
    public enum DcsServiceProductLineViewProductLineType {
        服务产品线 = 1,
        发动机产品线 = 2
    }

    //城市级别
    public enum DcsCompanyCityLevel {
        省级 = 1,
        地级 = 2,
        县级 = 3
    }


    //车辆行驶路线
    public enum DcsDealerServiceExtVehicleTravelRoute {
        高速公路出入口 = 1,
        国家公路 = 2,
        省级公路 = 3,
        县级公路 = 4,
        乡村道路 = 5,
        非临近限行区域 = 6,
        无 = 7
    }

    //车辆使用特性
    public enum DcsDealerServiceExtVehicleUseSpeciality {
        物料集散地 = 1,
        物流中心 = 2,
        配货市场 = 3,
        工地 = 4,
        矿区 = 5,
        无 = 6
    }

    //车辆停靠点
    public enum DcsDealerServiceExtVehicleDockingStations {
        汽配城 = 1,
        服务区 = 2,
        加油站 = 3,
        住宅区 = 4,
        无 = 5
    }

    //变更履历状态
    public enum DcsDealerBusinessPermitHistoryStatus {
        新增 = 1,
        修改 = 2,
        作废 = 3,
        删除 = 4
    }

    //会签类型
    public enum DcsCountersignatureType {
        串行 = 1,
        并行 = 2
    }

    //会签申请类型
    public enum DcsCountersignatureApplyType {
        维修索赔申请 = 1,
        保养索赔申请 = 2,
        外出索赔申请 = 3,
        服务活动索赔申请 = 4,
        配件索赔申请 = 5,
        AB质量信息快报 = 10
    }

    //会签状态
    public enum DcsCountersignatureStatus {
        新建 = 1,
        会签完成 = 2
    }

    //会签详细状态
    public enum DcsCountersignatureDetailStatus {
        未会签 = 1,
        会签通过 = 2,
        会签不通过 = 3
    }

    //改装类型
    public enum DcsVehicleInformationAdaptType {
        牵引车 = 1,
        自卸车 = 2,
        奇兵 = 3,
        平板车 = 4,
        商混外购 = 5,
        欧曼1系 = 6,
        矿用自卸 = 7,
        油罐车 = 8,
        散装罐 = 9,
        搅拌车 = 10,
        环卫车 = 11,
        其他 = 12
    }

    //产品类型
    public enum DcsProductType {
        公路车 = 1,
        非公路车 = 2
    }

    //是否选项
    public enum DcsIsOrNot {
        是 = 1,
        否 = 0
    }

    //自动审核状态
    public enum DcsAutoApproveStatus {
        通过 = 1,
        不通过 = 2,
        未审核 = 0
    }

    //预申请类型
    public enum DcsApplicationTypeClass {
        维修申请 = 1
    }

    //发动机索赔属性
    public enum DcsEngineClaimType {
        正常 = 1,
        统修 = 2
    }

    //退货原因
    public enum DcsReturnReason {
        需向物流索赔退货 = 1,
        质量问题退货 = 2,
        服务站_专卖店提报计划错误退货 = 3,
        配件图标不准确退货 = 4,
        库房人员错发配件 = 5,
        公司同意请退 = 6,
        出库单形成库房未出库 = 7
    }

    //整改类型
    public enum DcsRectificationType {
        主动整改 = 1,
        被动整改 = 2,
        单车整改 = 3,
        批量整改 = 4
    }

    //人员属性
    public enum DcsPersonType {
        服务人员 = 1,
        销售人员 = 2
    }

    //维修权限
    public enum DcsServicePermission {
        大修 = 1,
        小修 = 2
    }

    //二级站类型
    public enum DcsSubDealerType {
        认证二级站 = 1,
        非认证二级站 = 2
    }

    //差异分类
    public enum DcsDifferenceClassification {
        数量差异 = 1,
        包装损坏 = 2,
        物流丢失 = 3,
        配件未到 = 4,
        图号不符 = 10,
        技术状态不符 = 15,
        配件损坏 = 20,
        总成缺件 = 25
    }

    //领用结算类型
    public enum DcsRequisitionSettleType {
        领入 = 1,
        领出 = 2
    }

    //售前检查维修方式
    public enum DcsPerSaleRepairMethod {
        更换 = 5,
        调整 = 10,
        补充 = 15,
        无需处理 = 20
    }

    //售前检查索赔单位
    public enum DcsPerSaleClaimUnit {
        制造工厂 = 5,
        供应商 = 10,
        物流 = 15,
        营销让渡 = 20,
        未索赔 = 25
    }

    //AB质量信息快报单据状态
    public enum DcsABQualityInformationStatus {
        新增 = 5,
        提交 = 10,
        初审通过 = 15,
        初审不通过 = 16,
        会签中 = 20,
        会签完成 = 25,
        终审通过 = 30,
        终审不通过 = 31,
        作废 = 35
    }

    //调拨方向
    public enum DcsTransferDirection {
        代理库向服务站 = 5,
        服务站向代理库 = 10
    }

    //抱怨分类
    public enum DcsComplaintClassification {
        A = 5,
        B = 10,
        C = 15
    }

    //预订单状态
    public enum DcsPreOrderStatus {
        新建 = 5,
        已使用 = 10,
        作废 = 99
    }

    //材料类型
    public enum DcsMaterialType {
        发动机 = 5,
        变速箱 = 10,
        中桥 = 15,
        后桥 = 20,
        散热器 = 25,
        减速机 = 30,
        前桥 = 35,
        电器 = 40,
        传动系 = 45,
        行驶系 = 50,
        车身 = 51
    }

    //紧急程度
    public enum DcsNotificationUrgentLevel {
        重要 = 1,
        普通 = 2
    }

    //Pms维修单状态
    public enum DcsPmsRepairOrderStatus {
        新建 = 1,
        完工审查完毕 = 2,
        登记维修项目和材料完毕 = 10,
        结算完毕 = 11,
        作废 = 0
    }

    //Pms索赔单状态
    public enum DcsPmsRepairClaimStatus {
        新建 = 1,
        已审核 = 3,
        已生成结算汇总单 = 8,
        对应结算汇总单已审核 = 9,
        已生成责任单位索赔单 = 14,
        已生成责任供应商索赔单 = 13,
        作废 = 0
    }

    //Pms维修属性
    public enum DcsPmsRepairType {
        普通保养 = 1,
        强制保养 = 2,
        普通维修 = 7,
        外出维修 = 8,
        事故维修 = 9,
        车辆召回 = 10,
        一般服务活动 = 11,
        商品车维修 = 12,
        商品车保养 = 13
    }

    //公告操作类型
    public enum DcsOperationType {
        查看公告 = 1,
        下载附件 = 2
    }

    //故障属性
    public enum DcsFailureType {
        零部件 = 5,
        综合故障 = 10,
        装调故障 = 15,
        事故车 = 20,
        着火车 = 25
    }

    //供应商索赔结算源单据类型
    public enum DcsSupplierClaimSettlementDetailSourceType {
        维修索赔单 = 5,
        保养索赔单 = 10,
        服务活动索赔单 = 15,
        外出服务索赔单 = 20,
        供应商扣补款单 = 25
    }

    //供应商索赔结算单状态
    public enum DcsSupplierClaimSettlementBillStatus {
        新增 = 5,
        已审批 = 10,
        作废 = 99
    }

    //有无属性
    public enum DcsYesOrNo {
        有 = 1,
        无 = 0
    }

    //销售与服务场地布局关系
    public enum DcsSaleServiceLayout {
        前后 = 1,
        左右 = 2,
        分离 = 3
    }

    //同步日志类型
    public enum DcsSyncType {
        发运单签收单 = 2,
        库存调整单 = 3,
        WMS仓库信息表 = 4,
        入库单 = 1,
        上架单 = 5
    }

    //WMS日志处理状态
    public enum DcsWMSSyncStatus {
        未处理 = 0,
        已处理 = 1
    }

    //同步状态
    public enum DcsSyncStatus {
        未处理 = 0,
        已处理 = 1
    }

    //DTM维修合同状态
    public enum DcsDTMRepairContractStatus {
        未登记 = 0,
        已登记 = 1
    }

    //DTM维修类型
    public enum DcsRepairContractRepairType {
        普通维修 = 0,
        事故维修 = 1,
        车辆召回 = 2,
        服务活动 = 3,
        商品车维修 = 4,
        物流损失维修 = 5,
        海运损失维修 = 6,
        精品件加装 = 7,
        店外商品车维修 = 8
    }

    //DTM维修索赔状态
    public enum DcsServiceClaimStatus {
        无索赔 = 0,
        产生索赔 = 1,
        部分生成索赔单 = 2,
        全部生成索赔单 = 3
    }

    //DTM免费保养状态
    public enum DcsRepairContractMaintenanceStatus {
        无免费保养 = 0,
        产生免费保养 = 1,
        已生成索赔单 = 2
    }

    //DTM维修结算属性
    public enum DcsServiceAccountingProperty {
        索赔 = 0,
        免费保养 = 1,
        免费维修 = 2,
        自费保养 = 3,
        自费维修 = 4,
        返修 = 5,
        技术服务 = 6,
        保险理赔 = 7
    }

    //PMS处理状态
    public enum DcsPMSHandleStatus {
        待处理 = 1,
        处理成功 = 2,
        处理失败 = 3
    }

    //市场部人员类型
    public enum DcsDepartmentPersonType {
        服务经理 = 1,
        服务后台人员 = 2,
        配件后台人员 = 3
    }

    //采购入库状态
    public enum DcsPurchaseInStatus {
        未入库 = 5,
        部分入库 = 10,
        强制完成 = 11,
        全部入库完成 = 15,
        作废 = 99
    }

    //SAP发票业务类型
    public enum DcsSAPInvoiceType {
        采购发票 = 1,
        销售发票 = 2,
        采购退货发票 = 3
    }

    //SAP业务编码
    public enum DcsSAPBussinessCode {
        销售退货结算单成本结转 = 1,
        销售结算单成本结转 = 2,
        采购退货结算 = 3,
        销售退货结算 = 4,
        采购退货出库 = 5,
        销售退货入库 = 6,
        领用结算 = 7,
        采购结算 = 8,
        采购入库 = 9,
        销售出库 = 10,
        调拨 = 11,
        盘点 = 12,
        成本变更 = 13,
        销售结算 = 14,
        销售退货结算开红票 = 15,
        采购退货结算开红票 = 16,
        采购结算供应商发票 = 17,
        一采购结算单多发票 = 18,
        一发票多采购结算单 = 19
    }

    //系统类型
    public enum DcsSysCode {
        EPC = 1,
        DMS = 2
    }

    //DMS系统类型
    public enum DcsDMSSysCode {
        LS = 1,
        TYDMS = 2,
        FD = 3,
        GCC = 4
    }

    //SAP处理状态
    public enum DcsSAPHandleStatus {
        未处理 = 1,
        已处理 = 2
    }

    //服务站索赔结算方式
    public enum DcsDealerSettleMethods {
        合并结算 = 1,
        拆分结算 = 2
    }

    //服务站索赔结算类型
    public enum DcsDealerSettleType {
        材料费 = 1,
        工时费 = 2,
        合并费用 = 3,
        积分类型 = 4,
        红包类型 = 5,
        延保索赔 = 6
    }

    //用油类别
    public enum DcsOilCategory {
        齿轮油 = 1,
        机油 = 2
    }

    //内部领用取价
    public enum DcsInternalUsePrice {
        计划价 = 1,
        批发价 = 2
    }

    //开票标记
    public enum DcsInvoiceFlag {
        一结算单多发票 = 1,
        多结算单一发票 = 2
    }

    //配件所属分类
    public enum DcsPartsAttribution {
        保养材料 = 1,
        变速器 = 2,
        标准件 = 3,
        车轮 = 4,
        车身 = 5,
        底盘 = 6,
        底盘电器 = 7,
        发动机及外围附件 = 8,
        后桥 = 9,
        精品 = 10,
        前桥及转向 = 11,
        悬挂 = 12,
        中桥 = 13,
        其他 = 14
    }

    //是否满意
    public enum DcsIsSatisfy {
        满意 = 1,
        不满意 = 2,
        未评价 = 3
    }

    //社会职称
    public enum DcsSocialTitle {
        初级 = 5,
        中级 = 10,
        高级 = 15,
        无 = 16
    }

    //工种
    public enum DcsWorkType {
        机修工 = 5,
        电气焊工 = 10,
        汽车电工 = 15,
        钣金工 = 20,
        喷漆工 = 25,
        车床工 = 30
    }

    //配件索赔单单据状态新
    public enum DcsPartsClaimBillStatusNew {
        新增 = 5,
        待配件公司审核 = 10,
        待中心库审核 = 11,
        中心库审核通过 = 12,
        审核通过 = 13,
        初审通过 = 14,
        终审通过 = 15,
        审核不通过 = 16,
        服务站已发运 = 20,
        区域已收货 = 25,
        区域已发运 = 26,
        入库完成 = 30,
        终止 = 98,
        作废 = 99
    }

    //统购分销策略
    public enum DcsPurDistStrategy {
        纳入统购分销 = 1,
        不纳入统购分销 = 2,
        统购分销主体 = 3
    }

    //统购分销处理状态
    public enum DcsPurDistStatus {
        未处理 = 1,
        已处理 = 2,
        处理失败 = 3
    }

    //退货出库状态
    public enum DcsReturnOutStatus {
        未出库 = 1,
        部分出库 = 2,
        出库完成 = 3,
        终止 = 4,
        作废 = 99
    }

    //计划价分类
    public enum DcsPlanPriceCategory_PriceCategory {
        高价 = 1,
        中价 = 2,
        低价 = 3
    }

    //库存核对类型
    public enum DcsStockCheckType {
        期初 = 1,
        借方_采购入库单 = 2,
        借方_配件调拨单 = 3,
        借方_价格调整单 = 4,
        借方_盘点单 = 5,
        借方_内部领入单 = 6,
        借方_采购退货出库单 = 7,
        贷方_销售出库单 = 8,
        贷方_内部领出单 = 9,
        贷方_销售退货入库单 = 10,
        期末 = 11
    }

    //发出商品核对
    public enum DcsGoodsShippedCheck {
        期初_销售出库单 = 1,
        期初_销售退货入库单 = 2,
        借方_销售出库单 = 3,
        借方_销售退货入库单 = 4,
        借方_销售退货结算单 = 5,
        贷方_销售结算单 = 6,
        期末_销售出库单 = 7,
        期末_销售退货入库单 = 8
    }

    //采购暂估核对
    public enum DcsPurchaseEstimateCheck {
        期初_采购入库单 = 1,
        期初_采购退货出库单 = 2,
        借方_采购结算单 = 3,
        贷方_采购入库单 = 4,
        贷方_采购退货出库单 = 5,
        贷方_采购退货结算单 = 6,
        期末_采购入库单 = 7,
        期末_采购退货出库单 = 8
    }

    //IMS同步状态
    public enum DcsIMSSyncStatus {
        同步成功 = 1,
        同步失败 = 2
    }

    //智能订货业务类型
    public enum DcsIntelligentOrderMonthlyBaseBusinesstype {
        采购入库 = 1,
        销售出库 = 2
    }

    //电商发票类型
    public enum DcsERPInvoiceInformation_Type {
        增值税发票 = 0,
        普通发票 = 1,
        电子普通发票 = 2,
        不开发票 = 3
    }

    //不满意要素
    public enum DcsDisContentFactor {
        维修技术水平 = 1,
        服务态度 = 2,
        服务及时性 = 3,
        服务站环境 = 4
    }

    //评价结果
    public enum DcsCustomerServiceEvaluateResult {
        好评 = 1,
        中评 = 2,
        差评 = 3
    }

    //装车明细是否一致
    public enum DcsIsSameToLoadingDetail {
        是 = 1,
        否 = 2,
        无对应值 = 3
    }

    //ERP同步状态
    public enum DcsERPDeliverySyncStatus {
        同步成功 = 1,
        同步失败 = 2,
        终止 = 3
    }

    //ERP同步类型
    public enum DcsERPDeliverySyncType {
        ERP订单 = 1
    }

    //ERP退款状态
    public enum DcsERPDeliveryRefund {
        未退款 = 0,
        退款 = 1
    }

    //回访单据类型
    public enum DcsReturnVisitType {
        维修单 = 1,
        工单 = 2
    }

    //工单类型
    public enum DcsWorkOrderType {
        抱怨工单 = 1,
        救援工单 = 2,
        报修工单 = 3
    }

    //紧急程度
    public enum DcsUrgencyLevel {
        一般 = 1,
        紧急 = 2,
        有曝光倾向 = 3,
        已曝光 = 4
    }

    //报修来源
    public enum DcsRepairSource {
        APP = 1,
        电话 = 2,
        官网 = 3,
        微信 = 4
    }

    //派工状态
    public enum DcsSendType {
        已派工 = 2,
        已接受 = 3,
        已拒绝 = 4,
        已出发 = 5,
        已到达 = 6,
        已到站 = 7,
        开始维修 = 8,
        处理完毕 = 9,
        已撤销 = 10
    }

    //是否完成救援
    public enum DcsIsCanFinish {
        是 = 1,
        未反馈 = 0,
        否 = 2
    }

    //无法处理原因
    public enum DcsCannotFReason {
        无维修设备 = 1,
        无配件 = 2,
        无维修技术 = 3,
        拆检总成 = 4,
        更换五大总成 = 5,
        疑难故障 = 6,
        事故车 = 7,
        天气_路况等原因 = 8,
        多故障维修 = 9,
        报修多台车 = 10,
        无维修工 = 12,
        未与客户达成一致 = 13

    }

    //拒绝原因
    public enum DcsRefuseReason {
        无配件 = 1,
        无维修工 = 2,
        无救援车 = 3,
        无维修权限 = 4,
        距离客户较远 = 5,
        已撤站_拟撤站 = 6,
        无维修能力 = 7,
        非本事业部负责品牌 = 8,
        非红岩客户 = 9,
        客户取消预约 = 10,
        重复出单 = 11
    }

    //报修类别
    public enum DcsRrpairCategory {
        A = 1,
        B = 2,
        C = 3,
        D = 4,
        E = 5
    }

    //处理方式
    public enum DcsProcessWay {
        维修 = 1,
        配件更换 = 2,
        解释安抚 = 3,
        让渡处理 = 4,
        其它 = 5
    }

    //配件来源渠道
    public enum DcsPartChannel {
        订购 = 1,
        外购 = 2,
        库存配件 = 3
    }

    //工单子类型
    public enum DcsWorkOrderSubType {
        服务抱怨 = 1,
        质量抱怨 = 2,
        客户救援 = 3,
        送车司机救援 = 4,
        报修 = 5,
        预约 = 6,
        车联网抱怨 = 7,
        车联网救援 = 8
    }

    //渠道
    public enum DcsChannel {
        手机APP = 1,
        电话评价 = 2
    }

    ////地址类型
    //public enum DcsCustomerAddressType {
    //    家庭地址 = 1,
    //    单位地址 = 2,
    //    其他 = 3
    //}
    //学历
    public enum DCSEducation {
        博士以上 = 0,
        博士 = 1,
        硕士 = 2,
        学士 = 3,
        大专 = 4,
        中专 = 5,
        高中 = 6,
        初中 = 7,
        小学 = 8,
        小学以下 = 9,
    }

    //技能等级
    public enum DCSSkillLevel {
        高级技师 = 0,
        技师 = 1,
        高级工 = 2,
        中级工 = 3,
        初级工 = 4,

    }

    //学历
    public enum DcsEducation {
        博士以上 = 0,
        博士 = 1,
        硕士 = 2,
        学士 = 3,
        大专 = 4,
        中专 = 5,
        高中 = 6,
        初中 = 7,
        小学 = 8,
        小学以下 = 9
    }

    //技能等级
    public enum DcsSkillLevel {
        高级技师 = 0,
        技师 = 1,
        高级工 = 2,
        中级工 = 3,
        初级工 = 4
    }

    //主干网络类型
    public enum DcsDlrSerInfoTrunkNetworkType {
        R300 = 0,
        R200 = 1,
        R100 = 2,
        R50 = 3
    }

    //电商同步状态
    public enum DcsEADeliverySyncStatus {
        未处理 = 1,
        同步成功 = 2,
        同步失败 = 3,
        终止 = 4
    }

    //电商同步类型
    public enum DcsEADeliverySyncType {
        电商订单 = 1
    }


    //职务
    public enum DcsJobPosition {
        民营企业_私营企业主 = 65021001,
        中层及以上管理人员_干部 = 65021002,
        基层管理人员_干部 = 65021003,
        普通职员_科员_工人 = 65021004,
        个体经营者 = 65021005,
        自由职业者 = 65021006,
        离退休 = 65021007,
        下岗_待业 = 65021008,
        学生 = 65021009,
        家庭主妇_家庭主男 = 65021010,
        其他 = 65021011

    }

    //客户组别
    public enum DcsCustomerGroup {
        普通客户 = 65091001,
        大客户 = 65091002,
        其它 = 65091003
    }

    //购车客户类型
    public enum DcsCarCustomerType {
        首次购买 = 65101001,
        本品换购 = 65101002,
        竞品换购 = 65101003,
        本品增购 = 65101004,
        竞品增购 = 65101005
    }

    //客户区域分类
    public enum DcsCustomerAreaType {
        城区用户 = 65111001,
        城郊用户 = 65111002,
        乡镇用户 = 65111003,
        农村用户 = 65111004
    }

    //民族
    public enum DcsNation {
        汉族 = 65121001,
        壮族 = 65121002,
        满族 = 65121003,
        回族 = 65121004,
        苗族 = 65121005,
        维吾尔族 = 65121006,
        土家族 = 65121007,
        彝族 = 65121008,
        蒙古族 = 65121009,
        藏族 = 65121010,
        布依族 = 65121011,
        侗族 = 65121012,
        瑶族 = 65121013,
        朝鲜族 = 65121014,
        白族 = 65121015,
        哈尼族 = 65121016,
        哈萨克族 = 65121017,
        黎族 = 65121018,
        傣族 = 65121019,
        畲族 = 65121020,
        傈僳族 = 65121021,
        仡佬族 = 65121022,
        东乡族 = 65121023,
        高山族 = 65121024,
        拉祜族 = 65121025,
        水族 = 65121026,
        佤族 = 65121027,
        纳西族 = 65121028,
        羌族 = 65121029,
        土族 = 65121030,
        仫佬族 = 65121031,
        锡伯族 = 65121032,
        柯尔克孜族 = 65121033,
        达斡尔族 = 65121034,
        景颇族 = 65121035,
        毛南族 = 65121036,
        撒拉族 = 65121037,
        布朗族 = 65121038,
        塔吉克族 = 65121039,
        阿昌族 = 65121040,
        普米族 = 65121041,
        鄂温克族 = 65121042,
        怒族 = 65121043,
        京族 = 65121044,
        基诺族 = 65121045,
        德昂族 = 65121046,
        保安族 = 65121047,
        俄罗斯族 = 65121048,
        裕固族 = 65121049,
        乌孜别克族 = 65121050,
        门巴族 = 65121051,
        鄂伦春族 = 65121052,
        独龙族 = 65121053,
        塔塔尔族 = 65121054,
        赫哲族 = 65121055,
        珞巴族 = 65121056
    }

    //家庭年收入
    public enum DcsAnnualIncome {
        _35万以上 = 65131001,
        _30_35万 = 65131002,
        _25_30万 = 65131003,
        _20_25万 = 65131004,
        _15_20万 = 65131005,
        _10_15万 = 65131006,
        _5_10万 = 65131007,
        _5万以下 = 65131008
    }

    //地址类型
    public enum DcsCustomerAddressType {
        家庭地址 = 65141001,
        单位地址 = 65141002,
        其它 = 65141003
    }

    //家庭成员人数
    public enum DcsFamilyComposition {
        一人 = 65061001,
        二人 = 65061002,
        三人 = 65061003,
        四人 = 65061004,
        五人及以上 = 65061005,
    }

    //家庭年收入
    public enum AnnualIncome {
        _35万以上 = 65131001,
        _30_35万 = 65131002,
        _25_30万 = 65131003,
        _20_25万 = 65131004,
        _15_20万 = 65131005,
        _10_15万 = 65131006,
        _5_10万 = 65131007,
        _5万以下 = 65131008,

    }

    public enum DcsMemberType {
        钻石级 = 1,
        金卡级 = 2,
        银卡级 = 3,
        普通级 = 4
    }

    //婚否
    public enum DcsIfMarried {
        已婚 = 65051001,
        未婚 = 65051002
    }

    //储备订单审核日
    public enum DcsOrderAppDate {
        星期一 = 1,
        星期二 = 2,
        星期三 = 3,
        星期四 = 4,
        星期五 = 5,
        星期六 = 6,
        星期日 = 7
    }

    //发动机维修权限
    public enum DcsEngineRepairPower {
        全职级 = 1,
        修理级 = 2,
        保养级 = 3
    }

    //结算星级
    public enum DcsSettlementGrade {
        不达标 = 0,
        达标 = 1,
        一星级 = 2,
        二星级 = 3,
        三星级 = 4,
        四星级 = 5,
        五星级 = 6,
        政策性星级 = 7
    }

    //星级
    public enum DcsGrade {
        不达标 = 0,
        达标 = 1,
        一星级 = 2,
        二星级 = 3,
        三星级 = 4,
        四星级 = 5,
        五星级 = 6
    }

    //闭环状态
    public enum DcsClosedLoopStatus {
        未闭环 = 1,
        闭环 = 2
    }

    //积分使用状态
    public enum DcsBonusPointsStatus {
        未使用积分 = 1,
        已使用积分 = 2
    }

    //客户分类
    public enum DcsCustomerCategory {
        日用工业品 = 1,
        危险品 = 2,
        日用工业品_轿运车 = 3,
        日用工业品_快递快运 = 4,
        日用工业品_冷链 = 5,
        煤炭及散货物流_煤炭运输 = 6,
        港口及区域集散运输_港口车 = 7,
        场地运输_港口车_港内 = 8,
        重载公路工程运输_砂石料散装罐 = 9,
        重载公路工程运输_大件运输 = 10,
        矿用工程_煤炭坑口 = 11,
        矿用工程_金属坑口 = 12,
        城建渣土_建筑垃圾 = 13,
        城建渣土_开槽土石 = 14,
        城建渣土_泥浆 = 15,
        城建渣土_野外工程 = 16,
        重载公路运输_砂石料运输 = 17,
        重载公路运输_煤炭运输 = 18,
        重载公路运输_矿产运输 = 19,
        轻量化公路运输_煤炭运输 = 20,
        轻量化公路运输_砂石料运输 = 21,
        散货物流_畜禽运输 = 22,
        超重载专线运输_能源矿物 = 23,
        超重载专线运输_散装物料 = 24,
        重载公路运输_建材煤炭 = 25,
        高效物流运输_快运物流 = 26,
        高效物流运输_冷链运输 = 27,
        高效物流运输_快递物流 = 28,
        散货物流_农副产品 = 29,
        散货物流_日用散货 = 30,
        其他 = 31
    }

    //积分索赔单状态
    public enum DcsIntegralStatus {
        新增 = 1,
        生效 = 2,
        作废 = 3,
        不通过 = 4
    }

    //积分索赔单结算状态
    public enum DcsSettlementStatus {
        已结算 = 1,
        未结算 = 2
    }

    //积分索赔结算单状态
    public enum DcsIntegralClaimBillSettleStatus {
        新建 = 1,
        生效 = 2,
        作废 = 3,
        发票登记 = 4
    }

    //积分索赔单类型
    public enum DcsIntegralClaimBillType {
        积分类型 = 1,
        红包类型 = 2
    }

    //红包使用状态
    public enum DcsRedPacketsMsgState {
        未使用 = 0,
        已使用 = 1
    }

    //采购计划状态
    public enum DcsPurchasePlanStatus {
        新增 = 1,
        数据异常 = 2,
        可分解 = 3,
        部分分解 = 4,
        分解完成 = 5,
        终止 = 6,
        申请终止 = 7
    }

    //采购计划类型
    public enum DcsPurchasePlanType {
        紧急采购计划 = 1,
        月度采购计划 = 2,
        专项采购计划 = 3,
        补发采购计划 = 4
    }

    //积分单据类型
    public enum DcsBonusPointsType {
        积分使用 = 1,
        积分返还 = 2
    }

    //积分结算状态
    public enum DcsBonusPointsSettlementStatus {
        未汇总 = 1,
        已汇总 = 2
    }

    //积分汇总状态
    public enum DcsBonusPointsSummaryStatus {
        新建 = 1,
        已审批 = 2,
        发票登记 = 3,
        作废 = 4
    }

    //权益类型
    public enum DcsBenefiUom {
        折扣 = 1,
        年次 = 2
    }

    //积分索赔结算指令状态
    public enum DcsIntegralSettleDictateStatus {
        有效 = 1,
        执行成功 = 2,
        执行失败 = 3,
        作废 = 4
    }

    //会员处理状态
    public enum DcsMemberHandStatus {
        处理成功 = 1,
        处理失败 = 2
    }

    //冻结状态
    public enum DcsFrozenStatus {
        未冻结 = 1,
        已冻结 = 2
    }

    //自动结算任务执行状态
    public enum DcsExecutionStatus {
        成功 = 1,
        部分成功 = 2,
        失败 = 3
    }

    //自动结算任务状态
    public enum DcsSettlementAutomaticTaskStatus {
        待执行 = 1,
        已执行 = 2
    }

    //销售订单企业类型
    public enum DcsEnterpriseType {
        企业 = 1,
        机关事业单位 = 2,
        个人 = 3,
        其他 = 4
    }

    //保外扣补款类型
    public enum DcsOutofWarrantyPaymentTransactionCategory {
        会员积分费 = 1,
        红包费 = 2
    }

    //保外扣补款源单据类型
    public enum DcsOutofWarrantyPaymentSourceType {
        会员积分费 = 1,
        红包费 = 2
    }

    //内部直供供应商
    public enum DcsInternalSupplier {
        FT001078 = 1
    }

    //企业证件类型
    public enum DcsCompanyDocumentType {
        营业执照 = 1,
        税务登记证 = 2,
        居民身份证 = 3,
        事业单位法人证书 = 4,
        统一社会信用代码证书 = 5,
        部队代码 = 6,
        护照 = 7,
        组织机构代码证 = 8,
        军官证 = 9,
        其他 = 99
    }

    //交易接口日志同步状态
    public enum DcsSynchronousState {
        未处理 = 1,
        处理成功 = 2,
        处理失败 = 3
    }

    //签收状态
    public enum DcsSignStatus {
        在途 = 1,
        已发送 = 2
    }

    //培训类型编码
    public enum DcsTrainingTypeCode {
        FTFU = 1,
        FTBU = 2
    }

    //对账函状态
    public enum DcsReconciliationStatus {
        新增 = 1,
        提交 = 2,
        生效 = 3,
        作废 = 99
    }

    //培训类型名称
    public enum DcsTrainingTypeName {
        集团级 = 1,
        事业本部级 = 2
    }

    //认证类型有效期
    public enum DcsValiDate {
        _1年 = 1,
        _2年 = 2,
        _3年 = 3,
        _4年 = 4,
        _5年 = 5,
        _6年 = 6,
        _7年 = 7,
        _8年 = 8,
        _9年 = 9,
        _10年 = 10
    }

    //海外接口
    public enum DcsInterfaceType {
        销售订单 = 1,
        出库计划单 = 2
    }

    //对账方向
    public enum DcsReconciliationDirection {
        应加未达账 = 1,
        应减已入账 = 2
    }

    //延保证件类型
    public enum DcsExtendedWarrantyIDType {
        身份证 = 1,
        组织机构代码证 = 2
    }

    //延保订单状态
    public enum DcsExtendedWarrantyStatus {
        新增 = 1,
        提交 = 2,
        生效 = 3,
        审核不通过 = 4,
        驳回 = 5,
        待审核 = 6,
        作废 = 99
    }

    //延保订单审核结果
    public enum DcsCheckerResult {
        通过 = 1,
        不通过 = 2,
        驳回 = 3
    }

    //系统自动审核期限
    public enum DcsAutomaticAuditPeriod {
        六个月 = 1
    }

    //索赔单供应商确认结果
    public enum DcsRepairClaimBillSupplierResult {
        不通过 = 0,
        通过 = 1
    }

    //索赔单供应商确认不通过原因
    public enum DcsRepairClaimBillSupplierNotP {
        责任划分有误 = 1,
        服务站违规操作 = 2,
        非保修范围内 = 3,
        涉嫌虚假索赔 = 4,
        信息有差异 = 5,
        其他 = 99
    }

    //经销商分公司管理信息 对外状态
    public enum DcsExternalState {
        有效 = 1,
        停用 = 2
    }

    //红包类型
    public enum DcsRedPacketsType {
        维修红包 = 1,
        保养红包 = 2,
        终免工时 = 3
    }

    //红包范围
    public enum DcsRedPacketsRange {
        _828红包 = 1,
        普通红包 = 2,
        元旦红包 = 3
    }
    //车辆位置校验
    public enum DcsVehicleLocationCheck {
        一致 = 0,
        不一致 = 1
    }

    //随车行订单处理单状态
    public enum DcsVehiclePartsHandleOrderStatus {
        提交 = 1,
        已分发 = 2,
        作废 = 99
    }

    //支付方式
    public enum DcsPaymentType {
        微信 = 1,
        支付宝 = 2,
        环讯 = 3,
        PMS余额支付 = 4,
        积分支付 = 5
    }

    //代理库电商订单状态
    public enum DcsAgencyRetailerOrderStatus {
        新建 = 1,
        部分确认 = 2,
        确认完毕 = 3,
        终止 = 4
    }

    //代理库电商订单发运状态
    public enum DcsShippingStatus {
        未发运 = 1,
        部分发运 = 2,
        发运完毕 = 3
    }

    //MDM供应商类型
    public enum DcsSupplierType {
        配件供应商 = 1,
        零部件供应商 = 2,
        配件零部件供应商 = 3
    }

    //服务站配件调拨单状态
    public enum DcsDealerPartsTransferOrderStatus {
        新增 = 1,
        提交 = 2,
        已审核 = 3,
        作废 = 99
    }

    //代理库发运单状态
    public enum DcsAgencyShippingOrderStatus {
        新建 = 1,
        已发货 = 2,
        待收货 = 3,
        收货确认 = 4
    }

    //下载状态
    public enum DcsDownloadStatus {
        生成中 = 1,
        未下载 = 2,
        已下载 = 3,
        作废 = 99
    }

    //公告回复渠道
    public enum DcsNotificationReply {
        PMS = 0,
        APP = 1
    }

    //单据类型
    public enum DcsCurrentOrderType {
        物流信息主单,
        配件销售订单
    }

    //物流服务评价
    public enum DcsLogisticsEvaluation {
        非常满意 = 1,
        满意 = 2,
        一般 = 3,
        较差 = 4
    }

    //运费处理状态
    public enum DcsFreightDisposalStatus {
        新建 = 1,
        待结算 = 2,
        已结算 = 3
    }

    //基金红包状态
    public enum DcsFundRedPostStatus {
        新建 = 1,
        生效 = 2,
        作废 = 99
    }

    //CRM客户性质
    public enum DcsCRMCustomerNature {
        个体客户 = 1,
        车队客户 = 2
    }

    //客户身份
    public enum DcsCustomerIdentity {
        一般散户 = 1,
        客户领袖 = 2,
        普通车队客户 = 3,
        种子客户 = 4,
        战略客户 = 5,
        TOP客户 = 6
    }

    //实销分类
    public enum DcsRealSalesType {
        客户实销 = 1,
        其它实销 = 2
    }

    //红包发放单位
    public enum DcsLssuingUnit {
        奥铃 = 1,
        商务汽车 = 2,
        拓陆者 = 3,
        欧马可 = 4,
        欧曼 = 5,
        时代 = 6,
        瑞沃 = 7,
        伽途 = 8,
        客户互动中心 = 9,
        客户服务部 = 10
    }

    //采购价格变动情况记录状态
    public enum DcsPurchasePricingChangeStatus {
        新增 = 1,
        已维护 = 2
    }

    //出口客户价格类型
    public enum DcsExportCustPriceType {
        中心库价 = 1,
        服务站价 = 2,
        建议售价 = 3,
        特殊协议价 = 4,
        其他 = 5
    }

    //依维柯价格申请单状态
    public enum DcsIvecoPriceAppStatus {
        新增 = 1,
        提交 = 2,
        初审通过 = 3,
        终审通过 = 4,
        作废 = 99
    }

    //依维柯价格申请单价格类型
    public enum DcsIvecoPriceAppPriceType {
        依维柯价 = 1
    }

    //短供原因（暂时不用）
    public enum DcsShortSupplyReason {
        库存不足 = 1
    }

    //包装单位类型
    public enum DcsPackingUnitType {
        一级包装 = 1,
        二级包装 = 2,
        三级包装 = 3
    }

    //配件包装属性申请单状态
    public enum DcsPackingPropertyAppStatus {
        新增 = 1,
        审核通过 = 2,
        审核不通过 = 3,
        提交 = 4,
        作废 = 99
    }

    //配件上架任务单状态
    public enum DcsShelvesTaskStatus {
        新增 = 1,
        部分上架 = 2,
        上架完成 = 3,
        取消 = 4,
        强制上架完成 = 5
    }

    //入库实绩_设备类型
    public enum DcsPartsInboundPer_EquipmentType {
        PC = 1,
        手持 = 2
    }

    //入库实绩_入库类型
    public enum DcsPartsInboundPer_InType {
        包装 = 1,
        上架 = 2
    }

    //包装任务单状态
    public enum DcsPackingTaskStatus {
        新增 = 1,
        部分包装 = 2,
        包装完成 = 3,
        取消 = 6,
        强制包装完成 = 7
    }

    //配件采购计划状态
    public enum DcsPurchasePlanOrderStatus {
        作废 = 0,
        新增 = 1,
        提交 = 2,
        初审通过 = 3,
        终审通过 = 4,
        审核通过 = 5
    }

    //配件采购计划类型
    public enum DcsPurchasePlanOrderType {
        正常计划 = 1,
        紧急计划 = 2,
        特急计划 = 3
    }

    public enum DcsPartsShelvesTaskStatus {
        新增 = 1,
        部分上架 = 2,
        上架完成 = 3,
        取消 = 4,
        强制上架完成 = 5
    }

    //拣货任务单状态
    public enum DcsPickingTaskStatus {
        新建 = 1,
        部分拣货 = 2,
        拣货完成 = 3,
        已生成装箱任务 = 4,
        作废 = 99
    }

    //装箱任务单状态
    public enum DcsBoxUpTaskStatus {
        新建 = 1,
        部分装箱 = 2,
        装箱完成 = 3,
        停用 = 4,
        终止 = 5,
        作废 = 99
    }

    //拣货任务单短拣原因
    public enum DcsShortPickingReason {
        不符最小包装 = 1,
        借条 = 2,
        损坏 = 3,
        包装问题 = 4,
        呆滞 = 5,
        状态问题 = 6,
        数量差异 = 7
    }

    //出库实绩_出库类型
    public enum DcsPartsOutboundPer_OutType {
        拣货 = 1,
        装箱 = 2
    }

    //出库实绩_设备类型
    public enum DcsPartsOutboundPer_EquipmentType {
        PC = 1,
        手持 = 2
    }

    //短供原因
    public enum DcsPartsPurchaseOrderDetail_ShortSupReason {
        价格错误 = 1,
        最小包装数 = 2,
        最小起订量 = 3,
        技术变更 = 4,
        断点停供 = 5,
        其他 = 6
    }

    //账期年
    public enum DcsAccountPeriodYear {
        _2018 = 1,
        _2019 = 2,
        _2020 = 3,
        _2021 = 4,
        _2022 = 5,
        _2023 = 6,
        _2024 = 7,
        _2025 = 8,
        _2026 = 9,
        _2027 = 10,
        _2028 = 11,
        _2029 = 12,
        _2030 = 13
    }

    //账期月
    public enum DcsAccountPeriodMonth {
        _1 = 1,
        _2 = 2,
        _3 = 3,
        _4 = 4,
        _5 = 5,
        _6 = 6,
        _7 = 7,
        _8 = 8,
        _9 = 9,
        _10 = 10,
        _11 = 11,
        _12 = 12
    }

    //账期状态
    public enum DcsAccountPeriodStatus {
        有效 = 1,
        已关闭 = 2
    }

    //配件采购退货单状态
    public enum DcsPartsPurReturnOrderStatus {
        新建 = 1,
        初审通过 = 3,
        终审通过 = 2,
        作废 = 99
    }

    //配件销售退货单_退货类型
    public enum DcsPartsSalesReturn_ReturnType {
        正常退货 = 1,
        保底退货 = 2,
        特殊退货 = 3
    }

    //配件销售订单类型_结算类型
    public enum DcsSalesOrderType_SettleType {
        销售结算 = 1,
        出口结算 = 2
    }

    //配件销售订单类型_业务类型
    public enum DcsSalesOrderType_BusinessType {
        国内销售 = 1,
        集团客户销售 = 2,
        三包垫件 = 3,
        国内赠送 = 4,
        出口IV = 5,
        出口其他 = 6,
        _6K1 = 7,
        中国IV = 8,
        CKD = 9,
        _6k2 = 10
    }

    //授信类型
    public enum DcsCredit_Type {
        正常授信 = 1,
        临时授信 = 2
    }

    //配件来款方式
    public enum DcsPaymentBill_PaymentType {
        正常来款_现金 = 1,
        正常来款_承兑 = 2,
        承兑手续费 = 3,
        罚款 = 4,
        季度返利 = 5,
        奖励 = 6,
        调差 = 7
    }

    //客户转账单_业务类型
    public enum DcsCustomerTransferBill_BusinessType {
        服务费结转 = 1,
        客户三方转款 = 2,
        供应商_客户 = 3,
        促销返利 = 4
    }

    //可提报单位
    public enum DcsSalesUnit_SubmitCompanyId {
        服务站 = 1,
        中心库 = 2,
        中心库和服务站 = 3
    }

    //内部领出单_类型
    public enum DcsInternalAllocationBill_Type {
        包材 = 1,
        三包 = 2,
        报废 = 3,
        其他 = 4
    }

    //配件特殊协议价变更申请单状态
    public enum DcsSpecialTreatyPriceChangeStatus {
        新建 = 1,
        已审核 = 2,
        初审通过 = 3,
        审核通过 = 4,
        作废 = 99,
        已提交 = 5
    }

    //内部领出单状态
    public enum DcsInternalAllocationBillStatus {
        新建 = 1,
        已审核 = 2,
        已初审 = 3,
        作废 = 99
    }

    //流水类型
    public enum DcsSerialType {
        审批待发 = 1,
        发出商品 = 2,
        账户金额 = 3
    }

    //借用单类型
    public enum DcsBorrowBillType {
        生产借用 = 1,
        销售借用 = 2
    }

    //借用单状态
    public enum DcsBorrowBillStatus {
        新增 = 1,
        提交 = 2,
        初审通过 = 3,
        终审通过 = 4,
        已出库 = 5,
        归还待确认 = 6,
        部分确认 = 7,
        确认完成 = 8,
        作废 = 99
    }

    //包装单位
    public enum DcsPackingUnit {
        件 = 1,
        套 = 2,
        升 = 3,
        袋 = 4,
        盒 = 5,
        箱 = 6
    }
    public enum DcsPartsBranchPurchaseRoute {
        向供应商采购 = 1,
        向制造部采购 = 2
    }

    public enum DcsPartsShiftOrderQuestionType {
        装箱差异 = 1,
        质量问题 = 2,
        技术问题 = 3,
        仓储管理 = 4
    }

    //采购价格数据来源
    public enum DcsPurchasePricingDataSource {
        SAP = 1,
        手工维护 = 2,
        OA = 3,
    }

    //服务站领料单状态
    public enum DcsPostSaleClaimsTmpStatus {
        生效 = 0,
        已退料 = 1
    }

    //服务站领料单类型
    public enum DcsPostSaleClaimsTmpType {
        领料单 = 1,
        退料单 = 2
    }

    //配件关键代码
    public enum DcsSparePartKeyCode {
        H1 = 1,
        H2 = 2,
        H3 = 3,
        M1 = 4,
        M2 = 5,
        R1 = 6,
        R2 = 7,
        R3 = 8,
        R4 = 9,
        R8 = 10,
        E1 = 11,
        E2 = 12,
        E3 = 13,
        E4 = 14,
        E8 = 15,
        Y1 = 16,
        Y2 = 17,
        Y3 = 18,
        Y4 = 19,
        Y8 = 20,
        MH = 21,
        BT_C = 22,
        BT_Z = 23,
        H4 = 24,
        H5 = 25,
        H8 = 26,
        M3 = 27,
        M4 = 28,
        M5 = 29,
        M8 = 30,
        C4 = 31,
        C6 = 32,
        C8 = 33,
        N4 = 34,
        N6 = 35,
        N8 = 36,
        A1 = 37,
        ZS = 38
    }

    //配件优先级
    public enum DcsPartsPriority {
        一级 = 1,
        二级 = 2,
        三级 = 3
    }

    //积压件平台表状态
    public enum DcsOverstockPartsPlatFormBillStatus {
        生效 = 1,
        下架 = 2
    }

    //配件类别
    public enum DCSABCSettingType {
        A = 1,
        B = 2,
        C = 3,
        D = 4,
        J = 5
    }

    //积压件调拨单状态
    public enum DCSOverstockPartsTransferOrderStatus {
        新建 = 1,
        提交 = 2,
        生效 = 3,
        作废 = 99
    }

    //配件库存系数类别
    public enum DcsPartsStockCoefficientType {
        上限 = 1,
        下限 = 2
    }

    //工作组
    public enum DCSWorkGroup {
        入库 = 1,
        包装 = 2,
        上架 = 3,
        下架 = 4,
        出库 = 5
    }

    //多级审核配置类型
    public enum DCSMultiLevelApproveConfigType {
        中心库配件盘点单 = 1,
        服务站配件盘点单 = 2,
        信用申请单 = 3,
        配件外采管理 = 4
    }

    //配件价格时限维护类型
    public enum DCSPartsPriceTimeLimitType {
        采购价格维护 = 1,
        建议售价维护 = 2
    }

    //强制储备单状态
    public enum DCSForceReserveBillStatus {
        新建 = 1,
        提交 = 2,
        审核通过 = 3,
        生效 = 4,
        作废 = 99,
        已确认 = 5
    }

    //版本号管理类型
    public enum DCSVersionInfoType {
        子集 = 1,
        合集 = 2
    }

    //精确码追溯类型
    public enum DCSAccurateTraceType {
        出库 = 1,
        入库 = 2
    }

    //精确码追溯企业类型
    public enum DCSAccurateTraceCompanyType {
        配件中心 = 1,
        中心库 = 2,
        服务站 = 3
    }

    //追溯码临时表类型
    public enum DCSTraceTempType {
        回退 = 1,
        盘点 = 2,
        直供 = 3
    }

    //追溯属性
    public enum DCSTraceProperty {
        精确追溯 = 1,
        批次追溯 = 2
    }

    //SIH授信信用管理状态
    public enum DCSSIHCreditInfoStatus {
        新增 = 1,
        提交 = 2,
        初审通过 = 3,
        审核通过 = 4,
        审批通过 = 5,
        生效 = 6,
        作废 = 99
    }

    //SIH授信信用管理清单状态
    public enum DCSSIHCreditInfoDetailStatus {
        生效 = 1,
        已失效 = 2
    }
    //精确码追溯状态
    public enum DCSAccurateTraceStatus {
        有效 = 1,
        不考核 = 2,
        盘点不考核 = 3,
        SIH车桥不考核 = 4,
        采购退货不考核 = 5,  
        差异回退不考核 =6,
        作废 = 99
    }

    //供应商零件永久性标识条码修正状态
    public enum DCSSupplierTraceCodeStatus {
        新增 = 1,
        提交 = 2,
        已审核 = 3,
        已审批 = 4,
        已作废 = 99
    }

    //SIH授信信用管理类型
    public enum DCSSIHCreditInfoType {
        配件中心 = 1,
        分销中心 = 2
    }

    //配件差异回退单状态
    public enum DCSPartsDifferenceBackBillStatus {
        新增 = 1,
        提交 = 2,
        审核通过 = 3,
        生效 = 4,
        初审通过 = 5,
        作废 = 99
    }

    //配件差异回退类型
    public enum DCSPartsDifferenceBackBillType {
        上架 = 1,
        包装 = 2,
        入库 = 3
    }

    //配件差异错误类型
    public enum DCSPartsDifferenceBackBillDetailErrorType {
        图号错误 = 1,
        数量错误 = 2
    }

    //供应商发运单到货方式
    public enum DCSSupplierShippingOrderArriveMethod {
        供应商确认 = 1,
        供货计划组确认 = 2,
        PDA收货确认 = 3,
        默认到货 = 4
    }

    //供应商发运单修改状态
    public enum DCSSupplierShippingOrderModifyStatus {
        已修改 = 1,
        审核通过 = 2
    }

    //供应商发运单确认状态
    public enum DCSSupplierShippingOrderComfirmStatus {
        新建 = 1,
        入库确认 = 2,
        确认完成 = 3
    }
    //临时采购计划单状态
    public enum DCSTemPurchasePlanOrderStatus {
        新建 = 1,
        提交 = 2,
        审核通过 = 3,
        复核通过 = 4,
        确认通过 = 5,
        作废 = 99
    }
    //临时采购计划单计划类型
    public enum DCSTemPurchasePlanOrderPlanType {
        正常 = 1,
        紧急 = 2,
        特急 = 3
    }
    //临时采购计划单客户属性
    public enum DCSTemPurchasePlanOrderCustomerType {
        服务站 = 1,
        经销商 = 2,
        中心库 = 3,
        集团企业 = 4,
        出口客户 =5
    }
    //临时采购计划单发运方式
    public enum DCSTemPurchasePlanOrderShippingMethod {
        顺丰公路 = 1,
        顺丰航空 = 2,
        德邦物流 = 3,
        航空 = 4
    }
    //临时采购计划单运费方式
    public enum DCSTemPurchasePlanOrderFreightType {
        到付 = 1,
        寄付 = 2
    }
    //临时采购计划单清单编号来源
    public enum DCSTemPurchasePlanOrderDetailCodeSource {
        SPM = 1,
        手工录入 = 2
    }
    //临时采购定单状态
    public enum DCSTemPurchaseOrderStatus {
        新增 = 1,
        提交 = 2,
        确认完毕 = 3,
        部分发运 = 4,
        发运完毕 = 5,
        终止 = 6,
        强制完成 = 7
    }
    //临时采购计划单审核状态
    public enum DCSTemPurchaseOrderApproveStatus {
        新增 = 1,
        提交 = 2,
        审核通过 = 3,
        审批通过 = 4,
        终止 = 5
    }
    //临时采购订单收货状态
    public enum DCSTemPurchaseOrderReceiveStatus {
        未收货 = 1,
        收货完成 = 2,
        部分收货 = 3
    }
    //供应商临时发运单状态
    public enum DCSTemSupplierShippingOrderStatus {
        新建 = 1,
        部分收货 = 2,
        收货完成 = 3,
        终止 = 4,
		作废 =99
    }
    //移库状态
    public enum DCSPartsShiftOrderShiftStatus {
        待移库 = 1,
        部分移库 = 2,
        移库完成 = 3       
    }

    public enum DCSPartsShiftSIHDetailType {
        上架 = 1,
        下架 = 2
    }
    // 配件领用属性
    public enum DCSSparePartCollectionAttribute {
        供应商 = 1
    }
    //配件标签
    public enum DCSSparePartLabel {
        件标 = 1,
        箱标 = 2,
    }
    //储备计划入库状态
    public enum DCSSupplierStorePlanOrderStatus {
        新建 = 1,
        部分入库 = 2,
        入库完成 = 3,
        终止 = 4
    }
    // 跨区域销售订单状态
    public enum DCSCrossSalesOrderStatus {
        新建 = 1,
        提交 = 2,
        终止 = 3,
        初审通过 = 4,
        审核通过 = 5,
        审批通过 = 6,
        高级审核通过 = 7,
        生效 = 8,
        作废 = 99
    }
    //跨区域销售订单品种类型
    public enum DCSCrossSalesOrderType {
        驾驶室 = 1,
        本体 = 2,
        车架 = 3,
        发动机 = 4,
        其他 = 5,
    }
    //责任组
    public enum DCSResponsibleMembersResTem {
        库管组 = 1,
        包装组 = 2,
        入库组 = 3,
        物流组 = 4,
        工程组 = 5
    }
    //中心配件差异回退单状态
    public enum DCSAgencyDifferenceBackBillStatus {
        新增 = 1,
        提交 = 2,
        判定通过 = 3,
        确认通过 = 4,
        处理通过 = 5,
        审核通过 = 6,
        审批通过 = 7,
        终止 = 8,
        作废 = 99
    }
}
