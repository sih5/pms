﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web
{
    partial class DcsDomainService
    {
        public string ExportCompanyAddress(int[] ids, string companycode, string companyname, int? status)
        {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format(ErrorStrings.Export_Title_ReceiveAddress, DateTime.Now.ToString("ddHHmmssffff")));
            var CompanyAddresses = GetCompanyAddressWithDetail();

            if (ids != null && ids.Length>0) {
                CompanyAddresses = CompanyAddresses.Where(r => ids.Contains(r.Id));
            }
            else {
                if (!string.IsNullOrEmpty(companycode)) {
                    CompanyAddresses = CompanyAddresses.Where(r => r.Company.Code.Contains(companycode));
                }
                if (!string.IsNullOrEmpty(companyname)) {
                    CompanyAddresses = CompanyAddresses.Where(r => r.Company.Name.Contains(companyname));
                }
                if (status.HasValue) {
                    CompanyAddresses = CompanyAddresses.Where(r => r.Status == status.Value);
                }
            }

            CompanyAddresses = CompanyAddresses.Where(r => r.Company.Type == (int)DcsCompanyType.服务站 || r.Company.Type == (int)DcsCompanyType.代理库 || r.Company.Type == (int)DcsCompanyType.服务站兼代理库);

            var CompanyAddressesArray = CompanyAddresses.ToArray();
            if (CompanyAddresses.Any()) {
                excelColumns.Add(ErrorStrings.ExportColumn_CompanyCode);
                excelColumns.Add(ErrorStrings.ExportColumn_CompanyName);
                excelColumns.Add(ErrorStrings.ExportColumn_Usage);
                excelColumns.Add(ErrorStrings.ExportColumn_ContactPerson);
                excelColumns.Add(ErrorStrings.ExportColumn_ContactPhone);
                excelColumns.Add(ErrorStrings.ExportColumn_ProvinceName);
                excelColumns.Add(ErrorStrings.ExportColumn_RegionName);
                excelColumns.Add(ErrorStrings.ExportColumn_CountryName);
                excelColumns.Add(ErrorStrings.ExportColumn_DetailAddress);
                excelColumns.Add(ErrorStrings.ExportColumn_Status);
                excelColumns.Add(ErrorStrings.ExportColumn_Creator);
                excelColumns.Add(ErrorStrings.ExportColumn_CreateTime);
                excelColumns.Add(ErrorStrings.ExportColumn_Modifier);
                excelColumns.Add(ErrorStrings.ExportColumn_ModifyTime);

                using (var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index =>
                    {
                        if (index == CompanyAddressesArray.Count() + 1)
                            return null;
                        if (index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = CompanyAddressesArray[index - 1];
                        var values = new object[] {
                            detail.Company==null?" ":detail.Company.Code,
                            detail.Company==null?" ":detail.Company.Name,
                            detail.Usage == null?" ": Enum.GetName(typeof(DcsAddressUsage),detail.Usage),
                            detail.ContactPerson,
                            detail.ContactPhone,
                            detail.TiledRegion == null? " " : detail.TiledRegion.ProvinceName,
                            detail.TiledRegion == null? " " : detail.TiledRegion.CityName,
                            detail.TiledRegion == null? " " : detail.TiledRegion.CountyName,
                            detail.DetailAddress,
                            detail.Usage == null ?" ": Enum.GetName(typeof(DcsMasterDataStatus),detail.Status),
                            detail.CreatorName,
                            detail.CreateTime,
                            detail.ModifierName,
                            detail.ModifyTime
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
    }
}
