﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string ExportCompanyInvoiceInfo(int? id, string companyCode, string companyName, int? invoiceType, int? status) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format(ErrorStrings.Export_Title_CompanyInvoiceInfo, DateTime.Now.ToString("ddHHmmssffff")));
            var companyInvoiceInfoes = GetCompanyInvoiceInfoes();

            if(id.HasValue) {
                companyInvoiceInfoes = companyInvoiceInfoes.Where(r => r.Id == id.Value);
            } else {
                if(!string.IsNullOrEmpty(companyCode)) {
                    companyInvoiceInfoes = companyInvoiceInfoes.Where(r => r.CompanyCode.ToUpper().Contains(companyCode.ToUpper()));
                }
                if(!string.IsNullOrEmpty(companyName)) {
                    companyInvoiceInfoes = companyInvoiceInfoes.Where(r => r.CompanyName.ToUpper().Contains(companyName.ToUpper()));
                }
                if(invoiceType.HasValue) {
                    companyInvoiceInfoes = companyInvoiceInfoes.Where(r => r.InvoiceType == invoiceType.Value);
                }
                if(status.HasValue) {
                    companyInvoiceInfoes = companyInvoiceInfoes.Where(r => r.Status == status.Value);
                }
            }

            var companyInvoiceInfoesArray = companyInvoiceInfoes.ToArray();
            if(companyInvoiceInfoes.Any()) {
                excelColumns.Add(ErrorStrings.ExportColumn_CompanyCode);
                excelColumns.Add(ErrorStrings.ExportColumn_CompanyName);
                excelColumns.Add(ErrorStrings.ExportColumn_Taxpayeridentification);
                excelColumns.Add(ErrorStrings.ExportColumn_InvoiceTitle);
                excelColumns.Add(ErrorStrings.ExportColumn_TaxpayerQualifications);
                excelColumns.Add(ErrorStrings.ExportColumn_InvoiceType);
                excelColumns.Add(ErrorStrings.ExportColumn_InvoiceLimit);
                excelColumns.Add(ErrorStrings.ExportColumn_InvoiceTax);
                excelColumns.Add(ErrorStrings.ExportColumn_Linkman);
                excelColumns.Add(ErrorStrings.ExportColumn_LinkPhone);
                excelColumns.Add(ErrorStrings.ExportColumn_LinkFax);
                excelColumns.Add(ErrorStrings.ExportColumn_Adderss);
                excelColumns.Add(ErrorStrings.ExportColumn_Phone);
                excelColumns.Add(ErrorStrings.ExportColumn_BankName);
                excelColumns.Add(ErrorStrings.ExportColumn_BankAccount);
                excelColumns.Add(ErrorStrings.ExportColumn_Remark);
                excelColumns.Add(ErrorStrings.ExportColumn_Creator);
                excelColumns.Add(ErrorStrings.ExportColumn_CreateTime);
                excelColumns.Add(ErrorStrings.ExportColumn_Modifier);
                excelColumns.Add(ErrorStrings.ExportColumn_ModifyTime);

                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == companyInvoiceInfoesArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = companyInvoiceInfoesArray[index - 1];
                        var values = new object[] {
                            detail.CompanyCode,detail.CompanyName,detail.TaxRegisteredNumber,detail.InvoiceTitle,detail.TaxpayerQualification,
                            Enum.GetName(typeof(DcsInvoiceType),detail.InvoiceType),detail.InvoiceAmountQuota,detail.InvoiceTax,detail.Linkman,detail.ContactNumber,
                            detail.Fax,detail.TaxRegisteredAddress,detail.TaxRegisteredPhone,detail.BankName,detail.BankAccount,
                            detail.Remark,detail.CreatorName,detail.CreateTime,detail.ModifierName,detail.ModifyTime
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
    }
}
