﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string ExportCustomerOrderPriceGrades(int[] ids, string customerCompanyCode,string customerCompanyName, string partsSalesOrderTypeCode,string partsSalesOrderTypeName, int? status,int?branchId ,DateTime? bCreateTime,DateTime? eCreateTime) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("客户企业订单价格等级{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var CompanyAddresses = ObjectContext.CustomerOrderPriceGrades.ToArray();

            if(ids != null && ids.Length > 0) {
                CompanyAddresses = CompanyAddresses.Where(r => ids.Contains(r.Id)).ToArray();
            } else {
                if(!string.IsNullOrEmpty(customerCompanyCode)) {
                    CompanyAddresses = CompanyAddresses.Where(r => r.CustomerCompanyCode.Contains(customerCompanyCode)).ToArray();
                }
                if(!string.IsNullOrEmpty(customerCompanyName)) {
                    CompanyAddresses = CompanyAddresses.Where(r => r.CustomerCompanyName.Contains(customerCompanyName)).ToArray();
                }
				if(!string.IsNullOrEmpty(partsSalesOrderTypeCode)) {
                    CompanyAddresses = CompanyAddresses.Where(r => r.PartsSalesOrderTypeCode.Contains(partsSalesOrderTypeCode)).ToArray();
                }
				if(!string.IsNullOrEmpty(partsSalesOrderTypeName)) {
                    CompanyAddresses = CompanyAddresses.Where(r => r.PartsSalesOrderTypeName.Contains(partsSalesOrderTypeName)).ToArray();
                }
                if(status.HasValue) {
                    CompanyAddresses = CompanyAddresses.Where(r => r.Status == status.Value).ToArray();
                }
				if(branchId.HasValue) {
                    CompanyAddresses = CompanyAddresses.Where(r => r.BranchId == branchId.Value).ToArray();
                }
				 if(bCreateTime.HasValue) {
                    CompanyAddresses = CompanyAddresses.Where(r => r.CreateTime >= bCreateTime.Value).ToArray();
                }
                if(eCreateTime.HasValue) {
                    CompanyAddresses = CompanyAddresses.Where(r => r.CreateTime <= eCreateTime.Value).ToArray();
                }
            }

            var CompanyAddressesArray = CompanyAddresses.ToArray();
            if(CompanyAddresses.Any()) {
				excelColumns.Add(ErrorStrings.ExportColumn_Status);
                excelColumns.Add(ErrorStrings.ExportColumn_CompanyCode);
                excelColumns.Add(ErrorStrings.ExportColumn_CompanyName);
                excelColumns.Add("订单类型编号");
                excelColumns.Add("订单类型名称");
                excelColumns.Add("系数");    
                excelColumns.Add("备注");   				
                excelColumns.Add(ErrorStrings.ExportColumn_Creator);
                excelColumns.Add(ErrorStrings.ExportColumn_CreateTime);
                excelColumns.Add(ErrorStrings.ExportColumn_Modifier);
                excelColumns.Add(ErrorStrings.ExportColumn_ModifyTime);
				excelColumns.Add("作废人");
                excelColumns.Add("作废时间");

                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == CompanyAddressesArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = CompanyAddressesArray[index - 1];
                        var values = new object[] {
							 Enum.GetName(typeof(DcsBaseDataStatus),detail.Status),
                            detail.CustomerCompanyCode,
                            detail.CustomerCompanyName,
                            detail.PartsSalesOrderTypeCode,
                            detail.PartsSalesOrderTypeName,
                            detail.Coefficient ,
                            detail.Remark,                            
                            detail.CreatorName,
                            detail.CreateTime,
                            detail.ModifierName,
                            detail.ModifyTime,
							detail.AbandonerName,
                            detail.AbandonTime
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
    }
}
