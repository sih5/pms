﻿using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string 导出角色人员清单(string roleName, string loginId, string name) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format(ErrorStrings.Export_Title_UserRoleDetails, DateTime.Now.ToString("ddHHmmssffff")));
            var data = 获取角色人员清单(roleName, loginId, name);
            var dataArray = data.ToArray();
            if(data.Any()) {
                excelColumns.Add(ErrorStrings.ExportColumn_RoleName);
                excelColumns.Add(ErrorStrings.ExportColumn_UserID);
                excelColumns.Add(ErrorStrings.ExportColumn_UserName);

                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.RoleName,
                            detail.LoginId,
                            detail.Name,
                            
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
        public string 导出企业权限模板(string name, string lV1, string actionName, string lV2, string lV3) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format(ErrorStrings.Export_Title_EnterprisePermissionTemplate, DateTime.Now.ToString("ddHHmmssffff")));
            var data = 获取企业权限模板(name, lV1, actionName, lV2, lV3);
            var dataArray = data.ToArray();
            if(data.Any()) {
                excelColumns.Add(ErrorStrings.ExportColumn_TemplateName);
                excelColumns.Add("LV1");
                excelColumns.Add("LV2");
                excelColumns.Add("LV3");
                excelColumns.Add(ErrorStrings.ExportColumn_Button);

                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.Name,
                            detail.LV1,
                            detail.LV2,
                            detail.LV3,
                            detail.ActionName,
                            
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
        public string 导出角色权限清单(string name, string actionName, string lV1, string lV2, string lV3) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format(ErrorStrings.Export_Title_RolePermission, DateTime.Now.ToString("ddHHmmssffff")));
            var data = 获取角色权限清单(name, actionName, lV1, lV2, lV3);
            var dataArray = data.ToArray();
            if(data.Any()) {
                excelColumns.Add("LV1");
                excelColumns.Add("LV2");
                excelColumns.Add("LV3");
                excelColumns.Add(ErrorStrings.ExportColumn_Button);
                excelColumns.Add(ErrorStrings.ExportColumn_Manager1);
                excelColumns.Add(ErrorStrings.ExportColumn_Manager2);
                excelColumns.Add(ErrorStrings.ExportColumn_Manager3);
                excelColumns.Add(ErrorStrings.ExportColumn_Manager4);
                excelColumns.Add(ErrorStrings.ExportColumn_Manager5);
                excelColumns.Add(ErrorStrings.ExportColumn_Manager6);
                excelColumns.Add(ErrorStrings.ExportColumn_Manager7);
                excelColumns.Add(ErrorStrings.ExportColumn_Manager8);
                excelColumns.Add(ErrorStrings.ExportColumn_Manager9);
                excelColumns.Add(ErrorStrings.ExportColumn_Manager10);
                excelColumns.Add(ErrorStrings.ExportColumn_Manager11);
                excelColumns.Add(ErrorStrings.ExportColumn_Manager12);
                excelColumns.Add(ErrorStrings.ExportColumn_Manager13);
                excelColumns.Add(ErrorStrings.ExportColumn_Manager14);
                excelColumns.Add(ErrorStrings.ExportColumn_Manager15);
                excelColumns.Add(ErrorStrings.ExportColumn_Manager16);
                excelColumns.Add(ErrorStrings.ExportColumn_Manager17);
                excelColumns.Add(ErrorStrings.ExportColumn_Manager18);
                excelColumns.Add(ErrorStrings.ExportColumn_Manager19);
                excelColumns.Add(ErrorStrings.ExportColumn_Manager20);
                excelColumns.Add(ErrorStrings.ExportColumn_Manager21);
                excelColumns.Add(ErrorStrings.ExportColumn_Manager22);
                excelColumns.Add(ErrorStrings.ExportColumn_Manager23);
                excelColumns.Add(ErrorStrings.ExportColumn_Manager24);
                excelColumns.Add(ErrorStrings.ExportColumn_Manager25);
                excelColumns.Add(ErrorStrings.ExportColumn_Manager26);
                excelColumns.Add(ErrorStrings.ExportColumn_Manager27);
                excelColumns.Add(ErrorStrings.ExportColumn_Manager28);
                excelColumns.Add(ErrorStrings.ExportColumn_Manager29);
                excelColumns.Add(ErrorStrings.ExportColumn_Manager30);
                excelColumns.Add(ErrorStrings.ExportColumn_Manager31);
                excelColumns.Add(ErrorStrings.ExportColumn_Manager32);
                excelColumns.Add(ErrorStrings.ExportColumn_Manager33);
                excelColumns.Add(ErrorStrings.ExportColumn_Manager34);
                excelColumns.Add(ErrorStrings.ExportColumn_Manager35);
                excelColumns.Add(ErrorStrings.ExportColumn_Manager36);
                excelColumns.Add(ErrorStrings.ExportColumn_Manager37);
                excelColumns.Add(ErrorStrings.ExportColumn_Manager38);
                excelColumns.Add(ErrorStrings.ExportColumn_Manager39);
                excelColumns.Add(ErrorStrings.ExportColumn_Manager40);
                excelColumns.Add(ErrorStrings.ExportColumn_Manager41);
                excelColumns.Add(ErrorStrings.ExportColumn_Manager42);
                excelColumns.Add(ErrorStrings.ExportColumn_Manager43);
                excelColumns.Add(ErrorStrings.ExportColumn_Manager44);
                excelColumns.Add(ErrorStrings.ExportColumn_Manager45);
                excelColumns.Add(ErrorStrings.ExportColumn_Manager46);

                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.LV1,
                            detail.LV2,
                            detail.LV3,
                            detail.ActionName,
                            detail.AccessoriesCenter ,
                            detail.BussinessMananger,
                            detail.AssistantDirector  ,
                            detail.SalesMananger ,
                            detail.SalesFilmMananger  ,
                            detail.OutSalesMananger ,
                            detail.ExtraMananger ,
                            detail.ServiceMananger ,
                            detail.BigMananger  ,
                            detail.PurchaseMananger  ,
                            detail.PlanMananger  ,
                            detail.AllocationMananger ,
                            detail.Buyer  ,
                            detail.MarketMananger  ,
                            detail.PriceMananger  ,
                            detail.CommercialMananger  ,
                            detail.NetMananger ,
                            detail.MarketCommissioner  ,
                            detail.IntegratedHead ,
                            detail.SystemAdministrator  ,
                            detail.AssistantSystem  ,
                            detail.PurchaseCommissioner  ,
                            detail.GeneralCommissioner  ,
                            detail.AccessoryTechnician  ,
                            detail.LogisticsManager  ,
                            detail.LogisticsFuManager  ,
                            detail.InBoundManager  ,
                            detail.InBoundHead ,
                            detail.InBoundMember ,
                            detail.PackingManager ,
                            detail.PackingHead  ,
                            detail.PackingMember  ,
                            detail.PackingEngineer  ,
                            detail.ReturnSales ,
                            detail.WarehouseManagement  ,
                            detail.WarehouseHead  ,
                            detail.WarehouseMember  ,
                            detail.WarehouseManager  ,
                            detail.SupervisorLogistics  ,
                            detail.LogistManager ,
                            detail.InventoryHead ,
                            detail.InventoryMember ,
                            detail.LogisticsWarehouse ,
                            detail.Treasurer ,
                            detail.TreasurerMember ,
                            detail.ServiceManager
                            
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }

    }
}
