﻿using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string 导出所有配件信息(string code, string referenceCode, int? partABC, int? increaseRateGroupId, bool? isSalable, bool? isOrderable, bool? isPrimary, bool? isEffective) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format(ErrorStrings.Export_Title_AllSparePartReport, DateTime.Now.ToString("ddHHmmssffff")));
            var data = getAllSpareParts(code, referenceCode, partABC, increaseRateGroupId, isSalable, isOrderable, isPrimary, isEffective);
            var dataArray = data.ToArray();
            if(data.Any()) {
                excelColumns.Add(ErrorStrings.ExportColumn_SupplierPartCode);
                excelColumns.Add(ErrorStrings.ExportColumn_PartCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SIHCode);
                excelColumns.Add(ErrorStrings.ExportColumn_MapName);
                excelColumns.Add(ErrorStrings.ExportColumn_EnglishSparePartName);
                excelColumns.Add(ErrorStrings.ExportColumn_ContractPrice);
                excelColumns.Add(ErrorStrings.ExportColumn_MeasureUnit);
                excelColumns.Add(ErrorStrings.ExportColumn_SupplierCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SupplierName);
                excelColumns.Add(ErrorStrings.ExportColumn_IsPrimarySupplier);
                excelColumns.Add(ErrorStrings.ExportColumn_DurationOfContract);
                excelColumns.Add(ErrorStrings.ExportColumn_IsProvisional);
                excelColumns.Add(ErrorStrings.ExportColumn_BreakPointTime);
                excelColumns.Add(ErrorStrings.ExportColumn_PackagingAuthorized);
                excelColumns.Add(ErrorStrings.ExportColumn_MinOrderQuantity);
                excelColumns.Add(ErrorStrings.ExportColumn_MInSaleingAmount);
                excelColumns.Add(ErrorStrings.ExportColumn_SupplyShippingCycle);
                excelColumns.Add(EntityStrings.PartConPurchasePlan_OrderCycle);
                excelColumns.Add(ErrorStrings.ExportColumn_SupplyArrivalCycle);
                excelColumns.Add(ErrorStrings.ExportColumn_RetailPrice);
                excelColumns.Add(ErrorStrings.ExportColumn_CenterPrice);
                excelColumns.Add(ErrorStrings.ExportColumn_DealerPrice);
                excelColumns.Add(ErrorStrings.ExportColumn_PartProp);
                excelColumns.Add(ErrorStrings.ExportColumn_IsSalable);
                excelColumns.Add(ErrorStrings.ExportColumn_IsOrderable);
                excelColumns.Add(ErrorStrings.ExportColumn_IsSupplierPutIn);
                excelColumns.Add(ErrorStrings.ExportColumn_PriceProp);
                excelColumns.Add(ErrorStrings.ExportColumn_PartType);
                excelColumns.Add(ErrorStrings.ExportColumn_PartGroup);
                excelColumns.Add(ErrorStrings.ExportColumn_ShelfLife);
                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.SupplierPartCode,
                            detail.Code,
                            detail.ReferenceCode,
                            detail.Name,
                            detail.EnglishName, 
                            detail.PurchasePrice,
                            detail.MeasureUnit,
                            detail.PartsSupplierCode,
                            detail.PartsSupplierName,
                            detail.IsPrimary==true?ErrorStrings.ExportColnum_Yes:ErrorStrings.ExportColnum_No,
                            detail.Htyxq,
                            detail.IsZg==true?ErrorStrings.ExportColnum_Yes:ErrorStrings.ExportColnum_No,
                            detail.BreakTime,
                            detail.IsAccreditPack==true?ErrorStrings.ExportColnum_Yes:ErrorStrings.ExportColnum_No,
                            detail.PackingCoefficient,
                            detail.MInPackingAmount,
                            detail.OrderCycle,
                            detail.OrderGoodsCycle,
                            detail.ArrivalCycle,
                            detail.RetailGuidePrice,
                            detail.CenterPrice,
                            detail.SalesPrice,
                             Enum.GetName(typeof(DcsABCStrategyCategory),detail.PartABC),
                            detail.IsSalable==true?ErrorStrings.ExportColnum_Yes:ErrorStrings.ExportColnum_No,
                            detail.IsOrderable==true?ErrorStrings.ExportColnum_Yes:ErrorStrings.ExportColnum_No,
                            detail.IsSupplierPutIn==true?ErrorStrings.ExportColnum_Yes:ErrorStrings.ExportColnum_No,
                            detail.GroupName,
                            Enum.GetName(typeof(DcsSparePartPartType),detail.PartType),
                             Enum.GetName(typeof(DcsPartsBranchPurchaseRoute),detail.PurchaseRoute),
                            detail.PartsWarrantyLong
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
        public string 导出没有价格所有配件信息(string code, string referenceCode, int? partABC, int? increaseRateGroupId, bool? isSalable, bool? isOrderable, bool? isPrimary, bool? isEffective) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format(ErrorStrings.Export_Title_AllSparePartReport, DateTime.Now.ToString("ddHHmmssffff")));
            var data = getAllSpareParts(code, referenceCode, partABC, increaseRateGroupId, isSalable, isOrderable, isPrimary, isEffective);
            var dataArray = data.ToArray();
            if(data.Any()) {
                excelColumns.Add(ErrorStrings.ExportColumn_SupplierPartCode);
                excelColumns.Add(ErrorStrings.ExportColumn_PartCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SIHCode);
                excelColumns.Add(ErrorStrings.ExportColumn_MapName);
                excelColumns.Add(ErrorStrings.ExportColumn_EnglishSparePartName);
                excelColumns.Add(ErrorStrings.ExportColumn_MeasureUnit);
                excelColumns.Add(ErrorStrings.ExportColumn_SupplierCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SupplierName);
                excelColumns.Add(ErrorStrings.ExportColumn_IsPrimarySupplier);
                excelColumns.Add(ErrorStrings.ExportColumn_DurationOfContract);
                excelColumns.Add(ErrorStrings.ExportColumn_IsProvisional);
                excelColumns.Add(ErrorStrings.ExportColumn_BreakPointTime);
                excelColumns.Add(ErrorStrings.ExportColumn_PackagingAuthorized);
                excelColumns.Add(ErrorStrings.ExportColumn_MinOrderQuantity);
                excelColumns.Add(ErrorStrings.ExportColumn_MInSaleingAmount);
                excelColumns.Add(ErrorStrings.ExportColumn_SupplyShippingCycle);
                excelColumns.Add(EntityStrings.PartConPurchasePlan_OrderCycle);
                excelColumns.Add(ErrorStrings.ExportColumn_SupplyArrivalCycle);
				 excelColumns.Add(ErrorStrings.ExportColumn_DealerPrice);
                excelColumns.Add(ErrorStrings.ExportColumn_PartProp);
                excelColumns.Add(ErrorStrings.ExportColumn_IsSalable);
                excelColumns.Add(ErrorStrings.ExportColumn_IsOrderable);
                excelColumns.Add(ErrorStrings.ExportColumn_IsSupplierPutIn);
                excelColumns.Add(ErrorStrings.ExportColumn_PriceProp);
                excelColumns.Add(ErrorStrings.ExportColumn_PartType);
                excelColumns.Add(ErrorStrings.ExportColumn_PartGroup);
                excelColumns.Add(ErrorStrings.ExportColumn_ShelfLife);
                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.SupplierPartCode,
                            detail.Code,
                            detail.ReferenceCode,
                            detail.Name,
                            detail.EnglishName, 
                            detail.MeasureUnit,
                            detail.PartsSupplierCode,
                            detail.PartsSupplierName,
                            detail.IsPrimary==true?ErrorStrings.ExportColnum_Yes:ErrorStrings.ExportColnum_No,
                            detail.Htyxq,
                            detail.IsZg==true?ErrorStrings.ExportColnum_Yes:ErrorStrings.ExportColnum_No,
                            detail.BreakTime,
                            detail.IsAccreditPack==true?ErrorStrings.ExportColnum_Yes:ErrorStrings.ExportColnum_No,
                            detail.PackingCoefficient,
                            detail.MInPackingAmount,
                            detail.OrderCycle,
                            detail.OrderGoodsCycle,
                            detail.ArrivalCycle,    
                            detail.SalesPrice,
                             Enum.GetName(typeof(DcsABCStrategyCategory),detail.PartABC),
                            detail.IsSalable==true?ErrorStrings.ExportColnum_Yes:ErrorStrings.ExportColnum_No,
                            detail.IsOrderable==true?ErrorStrings.ExportColnum_Yes:ErrorStrings.ExportColnum_No,
                            detail.IsSupplierPutIn==true?ErrorStrings.ExportColnum_Yes:ErrorStrings.ExportColnum_No,
                            detail.GroupName,
                            Enum.GetName(typeof(DcsSparePartPartType),detail.PartType),
                             Enum.GetName(typeof(DcsPartsBranchPurchaseRoute),detail.PurchaseRoute),
                            detail.PartsWarrantyLong
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
    }
}
