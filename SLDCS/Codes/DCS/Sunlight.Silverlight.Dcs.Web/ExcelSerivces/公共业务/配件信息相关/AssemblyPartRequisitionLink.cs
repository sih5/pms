﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string ExportAssemblyPartRequisitionLink(int[] ids, string partCode, int? keyCode, int? status, DateTime? createTimeStart, DateTime? createTimeEnd) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("总成件领用关系_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var assemblyPartRequisitionLinks = GetAssemblyPartRequisitionLinks();

            if(ids != null && ids.Any()) {
                assemblyPartRequisitionLinks = assemblyPartRequisitionLinks.Where(r => ids.Contains(r.Id));
            } else {
                if(!string.IsNullOrEmpty(partCode)) {
                    assemblyPartRequisitionLinks = assemblyPartRequisitionLinks.Where(r => r.PartCode.ToLower().Contains(partCode.ToLower()));
                }
                if (keyCode.HasValue) {
                    assemblyPartRequisitionLinks = assemblyPartRequisitionLinks.Where(r => r.KeyCode == keyCode.Value);
                }
                if(status.HasValue) {
                    assemblyPartRequisitionLinks = assemblyPartRequisitionLinks.Where(r => r.Status == status.Value);
                }
                if(createTimeStart.HasValue) {
                    assemblyPartRequisitionLinks = assemblyPartRequisitionLinks.Where(r => r.CreateTime >= createTimeStart.Value);
                }
                if(createTimeEnd.HasValue) {
                    assemblyPartRequisitionLinks = assemblyPartRequisitionLinks.Where(r => r.CreateTime <= createTimeEnd.Value);
                }
            }

            var datas = assemblyPartRequisitionLinks.ToArray();
            if(datas.Any()) {
                excelColumns.Add(ErrorStrings.ExportColumn_PartCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SparePartName);
                excelColumns.Add(ErrorStrings.AssemblyPartRequisitionLink_KeyCode);
                excelColumns.Add(ErrorStrings.ExportColumn_Quantity);
                excelColumns.Add(ErrorStrings.ExportColumn_Remark);
                excelColumns.Add(ErrorStrings.ExportColumn_Status);
                excelColumns.Add(ErrorStrings.ExportColumn_Creator);
                excelColumns.Add(ErrorStrings.ExportColumn_CreateTime);
                excelColumns.Add(ErrorStrings.ExportColumn_Modifier);
                excelColumns.Add(ErrorStrings.ExportColumn_ModifyTime);

                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == datas.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = datas[index - 1];
                        var values = new object[] {
                            detail.PartCode,
                            detail.PartName,
							Enum.GetName(typeof(DcsSparePartKeyCode),detail.KeyCode),
							detail.Qty,
                            detail.Remark,
                            Enum.GetName(typeof(DcsBaseDataStatus),detail.Status),
                            detail.CreatorName,
                            detail.CreateTime,
                            detail.ModifierName,
                            detail.ModifyTime
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
    }
}
