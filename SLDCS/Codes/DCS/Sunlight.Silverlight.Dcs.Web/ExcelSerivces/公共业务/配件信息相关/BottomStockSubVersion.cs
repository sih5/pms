﻿using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web
{
    partial class DcsDomainService
    {
        public string 导出合集版本信息(int[] ids, int? status, string colVersionCode, string sparePartCode, string sparePartName, string companyCode, string companyName, DateTime? bStartTime, DateTime? eStartTime, int? companyType) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath("配件保底库存合集版本号导出.xlsx");
            var data = GetBottomStockColVersionForExport(ids, status, colVersionCode, sparePartCode, sparePartName, companyCode, companyName, bStartTime, eStartTime, companyType);
            var dataArray = data.ToArray();
            if(data.Any()) {
                excelColumns.Add("企业编号");
                excelColumns.Add("企业名称");
                excelColumns.Add("企业类型");
                excelColumns.Add("配件编号");
                excelColumns.Add("配件名称");
                excelColumns.Add("合集版本号");
                excelColumns.Add("储备价格");
                excelColumns.Add("储备金额");
                excelColumns.Add("储备数量");
                excelColumns.Add("生效时间");
                excelColumns.Add("失效时间");
                
                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.CompanyCode,
                            detail.CompanyName,
							detail.CompanyType==7?"大型服务站":detail.CompanyType==3?"中心库":"服务站",
                            detail.SparePartCode,
                            detail.SparePartName,
                            detail.ColVersionCode, 
                            detail.SalePrice,
                            detail.ReserveFee,
                            detail.ReserveQty,
                            detail.StartTime,
                            detail.EndTime,                           
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
        public string 导出子集版本信息(int[] ids, int? status, string subVersionCode, string colVersionCode, string sparePartCode, string sparePartName, string companyCode, string companyName, DateTime? bStartTime, DateTime? eStartTime, int? companyType, string reserveTypeSubItem) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath("配件保底库存子集版本号导出.xlsx");
            var data = GetBottomStockSubVersionForExport(ids, status, subVersionCode, colVersionCode, sparePartCode, sparePartName, companyCode, companyName, bStartTime, eStartTime, companyType, reserveTypeSubItem);
            var dataArray = data.ToArray();
            if(data.Any()) {
                excelColumns.Add("企业编号");
                excelColumns.Add("企业名称");
                excelColumns.Add("企业类型");
                excelColumns.Add("配件编号");
                excelColumns.Add("配件名称");
				excelColumns.Add("子集版本号");
                excelColumns.Add("合集版本号");
                excelColumns.Add("储备类别");
                excelColumns.Add("储备类别子项目");
                excelColumns.Add("储备价格");
                excelColumns.Add("储备金额");
                excelColumns.Add("储备数量");
                excelColumns.Add("生效时间");
                excelColumns.Add("失效时间");
                
                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.CompanyCode,
                            detail.CompanyName,
							detail.CompanyType==7?"大型服务站":detail.CompanyType==3?"中心库":"服务站",
                            detail.SparePartCode,
                            detail.SparePartName,
							detail.SubVersionCode,
                            detail.ColVersionCode, 
                            detail.ReserveType,
                            detail.ReserveTypeSubItem,
                            detail.SalePrice,
                            detail.ReserveFee,
                            detail.ReserveQty,
                            detail.StartTime,
                            detail.EndTime,                           
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
			public string 导出服务站强制储备信息(int[] ids, int? status, string subVersionCode, string colVersionCode, string reserveTypeSubItem, string companyCode, string companyName, DateTime? bCreateTime, DateTime? eCreateTime, DateTime? bApproveTime, DateTime? eApproveTime, string reserveType,string centerName) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath("服务站强制储备清单明细导出.xlsx");
            var data = GetForceReserveBillsForDealerExport(ids, status, subVersionCode, colVersionCode, reserveTypeSubItem, companyCode, companyName, bCreateTime, eCreateTime, bApproveTime, eApproveTime,  reserveType,centerName);
            var dataArray = data.ToArray();
            if(data.Any()) {
				excelColumns.Add("状态");
                excelColumns.Add("企业编号");
                excelColumns.Add("企业名称");                              
				excelColumns.Add("子集版本号");
                excelColumns.Add("合集版本号");
				excelColumns.Add("储备类别");
				excelColumns.Add("储备类别子项目");
				excelColumns.Add("提交人");
				excelColumns.Add("提交时间");
				excelColumns.Add("审核人");
				excelColumns.Add("审核时间");
				excelColumns.Add("审批人");
				excelColumns.Add("审批时间");
				excelColumns.Add("驳回人");
				excelColumns.Add("驳回时间");
				excelColumns.Add("作废人");
				excelColumns.Add("作废时间");
				excelColumns.Add("创建人");
				excelColumns.Add("创建时间");
				excelColumns.Add("修改人");
				excelColumns.Add("修改时间");				
				excelColumns.Add("配件编号");
                excelColumns.Add("配件名称");                
				excelColumns.Add("建议储备数量");   
                excelColumns.Add("储备价格");				
                excelColumns.Add("储备数量");
				excelColumns.Add("储备金额");
                excelColumns.Add("中心库配件属性");
                
                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
							Enum.GetName(typeof(DCSForceReserveBillStatus),detail.Status),
                            detail.CompanyCode,
                            detail.CompanyName,                           
							detail.SubVersionCode,
                            detail.ColVersionCode, 
							detail.ReserveType,
							detail.ReserveTypeSubItem,
							detail.SubmitterName ,
							detail.SubmitTime,
							detail.CheckerName ,
							detail.CheckTime ,
							detail.ApproverName ,
							detail.ApproveTime ,
							detail.RejecterName ,
							detail.RejectTime ,
							detail.AbandonerName ,
							detail.AbandonTime ,
							detail.CreatorName ,
							detail.CreateTime ,
							detail.ModifierName ,
							detail.ModifyTime ,
							detail.SparePartCode,
                            detail.SparePartName,
							detail.SuggestForceReserveQty,
							detail.CenterPrice,
							detail.ForceReserveQty,                            
                            detail.ReserveFee,
                            Enum.GetName(typeof(DCSABCSettingType),detail.CenterPartProperty),                            
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }

            public string 导出服务站强制储备信息主单(int[] ids, int? status, string subVersionCode, string colVersionCode, string reserveTypeSubItem, string companyCode, string companyName, DateTime? bCreateTime, DateTime? eCreateTime, DateTime? bApproveTime, DateTime? eApproveTime, string reserveType,string centerName)
            {
                var excelColumns = new List<string>();
                var exportDataFileName = GetExportFilePath("服务站强制储备主单导出.xlsx");
                var data = GetForceReserveBillDealerMasterExport(ids, status, subVersionCode, colVersionCode, reserveTypeSubItem, companyCode, companyName, bCreateTime, eCreateTime, bApproveTime, eApproveTime, reserveType,centerName);
                var dataArray = data.ToArray();
                if (data.Any())
                {
                    excelColumns.Add("状态");
                    excelColumns.Add("企业编号");
                    excelColumns.Add("企业名称");
                    excelColumns.Add("子集版本号");
                    excelColumns.Add("合集版本号");
                    excelColumns.Add("储备类别");
                    excelColumns.Add("储备类别子项目");
                    excelColumns.Add("提交人");
                    excelColumns.Add("提交时间");
                    excelColumns.Add("审核人");
                    excelColumns.Add("审核时间");
                    excelColumns.Add("审批人");
                    excelColumns.Add("审批时间");
                    excelColumns.Add("驳回人");
                    excelColumns.Add("驳回时间");
                    excelColumns.Add("作废人");
                    excelColumns.Add("作废时间");
                    excelColumns.Add("创建人");
                    excelColumns.Add("创建时间");
                    excelColumns.Add("修改人");
                    excelColumns.Add("修改时间");
                  

                    using (var excelExport = new ExcelExport(exportDataFileName))
                    {
                        excelExport.ExportByRow(index =>
                        {
                            if (index == dataArray.Count() + 1)
                                return null;
                            if (index == 0)
                                return excelColumns.ToArray<Object>();
                            var detail = dataArray[index - 1];
                            var values = new object[] {
							Enum.GetName(typeof(DCSForceReserveBillStatus),detail.Status),
                            detail.CompanyCode,
                            detail.CompanyName,                           
							detail.SubVersionCode,
                            detail.ColVersionCode, 
							detail.ReserveType,
							detail.ReserveTypeSubItem,
							detail.SubmitterName ,
							detail.SubmitTime,
							detail.CheckerName ,
							detail.CheckTime ,
							detail.ApproverName ,
							detail.ApproveTime ,
							detail.RejecterName ,
							detail.RejectTime ,
							detail.AbandonerName ,
							detail.AbandonTime ,
							detail.CreatorName ,
							detail.CreateTime ,
							detail.ModifierName ,
							detail.ModifyTime ,
							                         
                        };
                            return values;
                        });
                    }
                }
                return exportDataFileName;
            }

            public string 导出中心库强制储备信息主单(int[] ids, int? status, string subVersionCode, string colVersionCode, string reserveTypeSubItem, string companyCode, string companyName, DateTime? bCreateTime, DateTime? eCreateTime, DateTime? bApproveTime, DateTime? eApproveTime, int companyType, string reserveType)
            {
                var excelColumns = new List<string>();
                var exportDataFileName = GetExportFilePath("中心库强制储备主单导出.xlsx");
                var data = GetForceReserveBillCenterMasterExport(ids, status, subVersionCode, colVersionCode, reserveTypeSubItem, companyCode, companyName, bCreateTime, eCreateTime, bApproveTime, eApproveTime, companyType, reserveType);
                var dataArray = data.ToArray();
                if (data.Any())
                {
                    excelColumns.Add("状态");
                    excelColumns.Add("企业编号");
                    excelColumns.Add("企业名称");
                    excelColumns.Add("子集版本号");
                    excelColumns.Add("合集版本号");
                    excelColumns.Add("储备类别");
                    excelColumns.Add("储备类别子项目");
                    excelColumns.Add("生效时间");
                    excelColumns.Add("提交人");
                    excelColumns.Add("提交时间");
                    excelColumns.Add("审核人");
                    excelColumns.Add("审核时间");
                    excelColumns.Add("审批人");
                    excelColumns.Add("审批时间");
                    excelColumns.Add("驳回人");
                    excelColumns.Add("驳回时间");
                    excelColumns.Add("作废人");
                    excelColumns.Add("作废时间");
                    excelColumns.Add("创建人");
                    excelColumns.Add("创建时间");
                    excelColumns.Add("修改人");
                    excelColumns.Add("修改时间");


                    using (var excelExport = new ExcelExport(exportDataFileName))
                    {
                        excelExport.ExportByRow(index =>
                        {
                            if (index == dataArray.Count() + 1)
                                return null;
                            if (index == 0)
                                return excelColumns.ToArray<Object>();
                            var detail = dataArray[index - 1];
                            var values = new object[] {
                            Enum.GetName(typeof(DCSForceReserveBillStatus),detail.Status),
                            detail.CompanyCode,
                            detail.CompanyName,                           
                            detail.SubVersionCode,
                            detail.ColVersionCode, 
                            detail.ReserveType,
                            detail.ReserveTypeSubItem,
                            detail.ValidateFrom,
                            detail.SubmitterName ,
                            detail.SubmitTime,
                            detail.CheckerName ,
                            detail.CheckTime ,
                            detail.ApproverName ,
                            detail.ApproveTime ,
                            detail.RejecterName ,
                            detail.RejectTime ,
                            detail.AbandonerName ,
                            detail.AbandonTime ,
                            detail.CreatorName ,
                            detail.CreateTime ,
                            detail.ModifierName ,
                            detail.ModifyTime ,
							                         
                        };
                            return values;
                        });
                    }
                }
                return exportDataFileName;
            }

    }
}
