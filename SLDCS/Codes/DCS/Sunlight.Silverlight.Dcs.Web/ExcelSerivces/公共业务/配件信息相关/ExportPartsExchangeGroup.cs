﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string ExportPartsExchangeGroup(int[] ids, string exGroupCode, string exchangeCode, int? status, DateTime? createTimeStart, DateTime? createTimeEnd) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format(ErrorStrings.Export_Title_PartsExchangeGroup, DateTime.Now.ToString("ddHHmmssffff")));
            var partsExchangeGroups = GetPartsExchangeGroups();

            if(ids != null && ids.Any()) {
                partsExchangeGroups = partsExchangeGroups.Where(r => ids.Contains(r.Id));
            } else {
                if(!string.IsNullOrEmpty(exGroupCode)) {
                    partsExchangeGroups = partsExchangeGroups.Where(r => r.ExGroupCode.ToLower().Contains(exGroupCode.ToLower()));
                }
                if(!string.IsNullOrEmpty(exchangeCode)) {
                    partsExchangeGroups = partsExchangeGroups.Where(r => r.ExchangeCode.ToLower().Contains(exchangeCode.ToLower()));
                }
                if(status.HasValue) {
                    partsExchangeGroups = partsExchangeGroups.Where(r => r.Status == status.Value);
                }
                if(createTimeStart.HasValue) {
                    partsExchangeGroups = partsExchangeGroups.Where(r => r.CreateTime >= createTimeStart.Value);
                }
                if(createTimeEnd.HasValue) {
                    partsExchangeGroups = partsExchangeGroups.Where(r => r.CreateTime <= createTimeEnd.Value);
                }
            }

            var PartsExchangesArray = partsExchangeGroups.ToArray();
            if(partsExchangeGroups.Any()) {
                excelColumns.Add(ErrorStrings.ExportColumn_ExGroupCode);
                excelColumns.Add(ErrorStrings.ExportColumn_ExchangeCode);
                excelColumns.Add(ErrorStrings.ExportColumn_Remark);
                excelColumns.Add(ErrorStrings.ExportColumn_Status);
                excelColumns.Add(ErrorStrings.ExportColumn_Creator);
                excelColumns.Add(ErrorStrings.ExportColumn_CreateTime);
                excelColumns.Add(ErrorStrings.ExportColumn_Modifier);
                excelColumns.Add(ErrorStrings.ExportColumn_ModifyTime);
                excelColumns.Add(ErrorStrings.ExportColumn_Abandoner);
                excelColumns.Add(ErrorStrings.ExportColumn_AbandonTime);

                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == PartsExchangesArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = PartsExchangesArray[index - 1];
                        var values = new object[] {
                            detail.ExGroupCode,
                            detail.ExchangeCode,
                            detail.Remark,
                            Enum.GetName(typeof(DcsBaseDataStatus),detail.Status),
                            detail.CreatorName,
                            detail.CreateTime,
                            detail.ModifierName,
                            detail.ModifyTime,
                            detail.AbandonerName,
                            detail.AbandonTime
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }

        public string ExportPartsExchangeGroupDetail(int[] ids, string exGroupCode, string exchangeCode, int? status, DateTime? createTimeStart, DateTime? createTimeEnd) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format(ErrorStrings.Export_Title_PartsExchangeGroupDetail, DateTime.Now.ToString("ddHHmmssffff")));
            var partsExchangeGroups = (IQueryable<PartsExchangeGroup>)ObjectContext.PartsExchangeGroups;
            if(ids != null && ids.Any()) {
                partsExchangeGroups = partsExchangeGroups.Where(r => ids.Contains(r.Id));
            } else {
                if(!string.IsNullOrEmpty(exGroupCode)) {
                    partsExchangeGroups = partsExchangeGroups.Where(r => r.ExGroupCode.ToLower().Contains(exGroupCode.ToLower()));
                }
                if(!string.IsNullOrEmpty(exchangeCode)) {
                    partsExchangeGroups = partsExchangeGroups.Where(r => r.ExchangeCode.ToLower().Contains(exchangeCode.ToLower()));
                }
                if(status.HasValue) {
                    partsExchangeGroups = partsExchangeGroups.Where(r => r.Status == status.Value);
                }
                if(createTimeStart.HasValue) {
                    partsExchangeGroups = partsExchangeGroups.Where(r => r.CreateTime >= createTimeStart.Value);
                }
                if(createTimeEnd.HasValue) {
                    partsExchangeGroups = partsExchangeGroups.Where(r => r.CreateTime <= createTimeEnd.Value);
                }
            }
            var result = from a in partsExchangeGroups
                         join c in ObjectContext.PartsExchanges.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on a.ExchangeCode equals c.ExchangeCode into temptable1
                         from t in temptable1.DefaultIfEmpty()
                         join b in ObjectContext.SpareParts.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on t.PartId equals b.Id into temptable2
                         from t2 in temptable2.DefaultIfEmpty()
                         select new {
                             ExGroupCode = a.ExGroupCode,
                             ExchangeCode = a.ExchangeCode,
                             Remark = a.Remark,
                             Status = a.Status,
                             CreatorName = a.CreatorName,
                             CreateTime = a.CreateTime,
                             ModifierName = a.ModifierName,
                             ModifyTime = a.ModifyTime,
                             AbandonerName = a.AbandonerName,
                             AbandonTime = a.AbandonTime,
                             SparePartCode = t2.Code,
                             SparePartName = t2.Name,
                             ReferenceCode = t2.ReferenceCode,
                         };
            var resultArray = result.ToArray();
            if(resultArray.Any()) {
                excelColumns.Add(ErrorStrings.ExportColumn_ExGroupCode);
                excelColumns.Add(ErrorStrings.ExportColumn_ExchangeCode);
                excelColumns.Add(ErrorStrings.ExportColumn_Remark);
                excelColumns.Add(ErrorStrings.ExportColumn_Status);
                excelColumns.Add(ErrorStrings.ExportColumn_Creator);
                excelColumns.Add(ErrorStrings.ExportColumn_CreateTime);
                excelColumns.Add(ErrorStrings.ExportColumn_Modifier);
                excelColumns.Add(ErrorStrings.ExportColumn_ModifyTime);
                excelColumns.Add(ErrorStrings.ExportColumn_Abandoner);
                excelColumns.Add(ErrorStrings.ExportColumn_AbandonTime);
                excelColumns.Add(ErrorStrings.ExportColumn_SparePartCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SparePartName);
                excelColumns.Add(ErrorStrings.ExportColumn_ReqPart);
                excelColumns.Add(ErrorStrings.ExportColumn_ExchangeIdentification);

                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == resultArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = resultArray[index - 1];
                        var values = new object[] {
                            detail.ExGroupCode,
                            detail.ExchangeCode,
                            detail.Remark,
                            Enum.GetName(typeof(DcsBaseDataStatus),detail.Status),
                            detail.CreatorName,
                            detail.CreateTime,
                            detail.ModifierName,
                            detail.ModifyTime,
                            detail.AbandonerName,
                            detail.AbandonTime,
                            detail.SparePartCode,
                            detail.SparePartName,
                            detail.ReferenceCode,
                            detail.ExchangeCode
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
    }
}
