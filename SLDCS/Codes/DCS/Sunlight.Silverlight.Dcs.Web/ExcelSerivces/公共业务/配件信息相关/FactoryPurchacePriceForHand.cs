﻿using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunlight.Silverlight.Dcs.Web.Resources;
using System.Collections.ObjectModel;

namespace Sunlight.Silverlight.Dcs.Web {
    public enum  StatusFirect {
        新增 = 1,
        价格可维护 = 2,
        临时价格更新 = 3,
        最低采购价 = 4
    }

    partial class DcsDomainService {
        public string 导出查询采购价待处理(string supplierCode, string supplierName, string sparePartCode, string sparePartName, int? status, bool? isOrderable) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("查询采购价待处理_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = 查询采购价待处理(supplierCode, supplierName, sparePartCode, sparePartName, status, isOrderable);
            var dataArray = data.ToArray();
            if(data.Any()) {
				excelColumns.Add(ErrorStrings.ExportColumn_SupplierPartCode);
                excelColumns.Add("红岩号");
                excelColumns.Add(ErrorStrings.ExportColumn_SparePartName);
				excelColumns.Add(ErrorStrings.ExportColumn_SupplierCode2);
                excelColumns.Add(ErrorStrings.ExportColumn_SupplierName);               
                excelColumns.Add("是否首选供应商");
				excelColumns.Add("是否可采购");
                excelColumns.Add("现首选供应商名称");
                excelColumns.Add("现首选供应商编号");
                excelColumns.Add("价格");
                excelColumns.Add("价格类型");
                excelColumns.Add("份额");
                excelColumns.Add("生效时间");
                excelColumns.Add("失效时间");
                excelColumns.Add("待处理类型");
                excelColumns.Add("数据来源");             

                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {                        
                            detail.SparePartCode,
                            detail.HYCode,
                            detail.SparePartName,
                            detail.SupplierCode,
                            detail.SupplierName,
							detail.IsPrimary==false?"否":"是",
							detail.IsOrderable==false?"否":"是",
                            detail.PartsSupplierNamePri,
                            detail.PartsSupplierCodePri,
                            detail.ContractPrice,
							Enum.GetName(typeof(DcsPurchasePriceType),detail.PriceType==null?0:detail.PriceType),
                            detail.Rate, 
                            detail.ValidFrom,
                            detail.ValidTo,
                            Enum.GetName(typeof(StatusFirect),detail.Status==null?0:detail.Status),
                            detail.Source
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
    }
}
