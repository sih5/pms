﻿using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string 导出出入库每小时工作量(DateTime? bCreateTime) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format(ErrorStrings.Export_Title_InOutWorkPerHourForReport, DateTime.Now.ToString("ddHHmmssffff")));
            var data = GetInOutWorkPerHoursBy();
			if(bCreateTime.HasValue){
				data=data.Where(r=>r.CreateTime==bCreateTime);
			}
            var dataArray = data.ToArray();
            if(data.Any()) {
                excelColumns.Add(ErrorStrings.InOutSettleSummaryReport1);
                excelColumns.Add(ErrorStrings.Report_Title_InOutWorkPerHour_TimeFrame);
                excelColumns.Add(ErrorStrings.Report_Title_InOutWorkPerHour_InPersonNum);
				excelColumns.Add(ErrorStrings.Report_Title_InOutWorkPerHour_InOrderLineNum);
                excelColumns.Add(ErrorStrings.Report_Title_InOutWorkPerHour_InOrderItemNum);               
				excelColumns.Add(ErrorStrings.ExportColumn_InboundAmount);
                excelColumns.Add(ErrorStrings.Report_Title_InOutWorkPerHour_InFee);               
                excelColumns.Add(ErrorStrings.Report_Title_InOutWorkPerHour_PackingPersonNum);
                excelColumns.Add(ErrorStrings.Report_Title_InOutWorkPerHour_PackingLineNum);
                excelColumns.Add(ErrorStrings.Report_Title_InOutWorkPerHour_PackingItemNum);
                excelColumns.Add(ErrorStrings.Report_Title_InOutWorkPerHour_PackingQty);
                excelColumns.Add(ErrorStrings.Report_Title_InOutWorkPerHour_PackingFee);				
                excelColumns.Add(ErrorStrings.Report_Title_InOutWorkPerHour_ShelvePersonNum);
                excelColumns.Add(ErrorStrings.Report_Title_InOutWorkPerHour_ShelveLineNum);
                excelColumns.Add(ErrorStrings.Report_Title_InOutWorkPerHour_ShelveItemNum);
                excelColumns.Add(ErrorStrings.Report_Title_InOutWorkPerHour_ShelveQty);
                excelColumns.Add(ErrorStrings.Report_Title_InOutWorkPerHour_ShelveFee);				
                excelColumns.Add(ErrorStrings.Report_Title_InOutWorkPerHour_UnShelvePersonNum);
                excelColumns.Add(ErrorStrings.Report_Title_InOutWorkPerHour_UnShelveLineNum);
                excelColumns.Add(ErrorStrings.Report_Title_InOutWorkPerHour_UnShelveItemNum);
                excelColumns.Add(ErrorStrings.Report_Title_InOutWorkPerHour_UnShelveQty);
                excelColumns.Add(ErrorStrings.Report_Title_InOutWorkPerHour_UnShelveFee);
                excelColumns.Add(ErrorStrings.Report_Title_InOutWorkPerHour_OutPersonNum);
                excelColumns.Add(ErrorStrings.Report_Title_InOutWorkPerHour_OutOrderLineNum);
                excelColumns.Add(ErrorStrings.Report_Title_InOutWorkPerHour_OutOrderItemNum);
                excelColumns.Add(ErrorStrings.Report_Title_InOutWorkPerHour_OutQty);
                excelColumns.Add(ErrorStrings.Report_Title_InOutWorkPerHour_OutFee);
                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.CreateTime,
                            detail.TimeFrame,
                            detail.InPersonNum,
                            detail.InOrderLineNum,
                            detail.InOrderItemNum, 
                            detail.InQty,
                            detail.InFee,
                            detail.PackingPersonNum,
                            detail.PackingLineNum,
                            detail.PackingItemNum,
                            detail.PackingQty,
                            detail.PackingFee,
                            detail.ShelvePersonNum,
                            detail.ShelveLineNum,
                            detail.ShelveItemNum,
                            detail.ShelveQty,
                            detail.ShelveFee,
                            detail.UnShelvePersonNum,
                            detail.UnShelveLineNum,
                            detail.UnShelveItemNum,
                            detail.UnShelveQty,
                            detail.UnShelveFee,                          
                            detail.OutPersonNum,
                            detail.OutOrderLineNum,
                            detail.OutOrderItemNum,
                            detail.OutQty,                            
                            detail.OutFee
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }

    }
}
