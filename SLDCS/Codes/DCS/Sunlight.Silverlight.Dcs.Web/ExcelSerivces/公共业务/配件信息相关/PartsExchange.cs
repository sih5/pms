﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string ExportPartsExchange(int? id, string exchangeCode, string exGroupCode, string sparePartCode, string sparePartName, int? status, DateTime? createTimeStart, DateTime? createTimeEnd) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format(ErrorStrings.Export_Title_PartsExchange, DateTime.Now.ToString("ddHHmmssffff")));
            var partsExchanges = 查询配件互换信息带互换组(sparePartCode, sparePartName, exGroupCode, status, exchangeCode, createTimeStart, createTimeEnd);

            //if(id.HasValue) {
            //    partsExchanges = partsExchanges.Where(r => r.Id == id.Value);
            //} else {
            //    if(!string.IsNullOrEmpty(exchangeCode)) {
            //        partsExchanges = partsExchanges.Where(r => r.ExchangeCode.ToLower().Contains(exchangeCode.ToLower()));
            //    }
            //    if(!string.IsNullOrEmpty(sparePartCode)) {
            //        partsExchanges = partsExchanges.Where(r =>r.PartCode.ToLower().Contains(sparePartCode.ToLower()));
            //    }
            //    if(!string.IsNullOrEmpty(sparePartName)) {
            //        partsExchanges = partsExchanges.Where(r =>r.PartName.ToLower().Contains(sparePartName.ToLower()));
            //    }
            //    if(status.HasValue) {
            //        partsExchanges = partsExchanges.Where(r => r.Status == status.Value);
            //    }
            //    if(createTimeStart.HasValue) {
            //        partsExchanges = partsExchanges.Where(r => r.CreateTime >= createTimeStart.Value);
            //    }
            //    if(createTimeEnd.HasValue) {
            //        partsExchanges = partsExchanges.Where(r => r.CreateTime <= createTimeEnd.Value);
            //    }
            //}

            var PartsExchangesArray = partsExchanges.ToArray();
            if(partsExchanges.Any()) {
                excelColumns.Add(ErrorStrings.ExportColumn_ExGroupCode);
                excelColumns.Add(ErrorStrings.ExportColumn_ExchangeCode);
				excelColumns.Add(ErrorStrings.ExportColumn_SparePartCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SparePartName);
                excelColumns.Add(ErrorStrings.ExportColumn_Remark);
                excelColumns.Add(ErrorStrings.ExportColumn_Creator);
                excelColumns.Add(ErrorStrings.ExportColumn_CreateTime);
                excelColumns.Add(ErrorStrings.ExportColumn_Modifier);
                excelColumns.Add(ErrorStrings.ExportColumn_ModifyTime);
                excelColumns.Add(ErrorStrings.ExportColumn_Abandoner);
                excelColumns.Add(ErrorStrings.ExportColumn_AbandonTime);

                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == PartsExchangesArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = PartsExchangesArray[index - 1];
                        var values = new object[] {
                            detail.ExGroupCode,
                            detail.ExchangeCode,
                            (detail.PartCode),
                            (detail.PartName),
                           // Enum.GetName(typeof(DcsBaseDataStatus),detail.Status),
                            detail.Remark,
                            detail.CreatorName,
                            detail.CreateTime,
                            detail.ModifierName,
                            detail.ModifyTime,
                            detail.AbandonerName,
                            detail.AbandonTime
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
    }
}

