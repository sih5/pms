﻿using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string 导出单中心库销售占比(int? warehouseId,string submitCompanyCode, string submitCompanyName, string sparePartCode, string sparePartName, DateTime? bCreateTime, DateTime? eCreateTime) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("单中心库销售占比_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = ObjectContext.SIHCenterPers.ToArray(); 
			if(!string.IsNullOrEmpty(sparePartCode)) {
                data = data.Where(t => t.SparePartCode == sparePartCode).ToArray();
            }
            if(!string.IsNullOrEmpty(sparePartName)) {
                data = data.Where(t => t.SparePartName == sparePartName).ToArray();
            }
			if(!string.IsNullOrEmpty(submitCompanyCode)) {
                data = data.Where(t => t.SubmitCompanyCode == submitCompanyCode).ToArray();
            }
			if(!string.IsNullOrEmpty(submitCompanyName)) {
                data = data.Where(t => t.SubmitCompanyName == submitCompanyName).ToArray();
            }
            if(warehouseId.HasValue) {
                data = data.Where(t => t.WarehouseId == warehouseId).ToArray();
            }
            if(bCreateTime.HasValue) {
                data = data.Where(t => t.CarryTime >= bCreateTime).ToArray();
            }
            if(eCreateTime.HasValue) {
                data = data.Where(t => t.CarryTime <= eCreateTime).ToArray();
            }
            var dataArray = data.ToArray();
            if(data.Any()) {
                excelColumns.Add("结转时间");
                excelColumns.Add("仓库编号");
                excelColumns.Add("仓库名称");
				excelColumns.Add("提报单位编号");
                excelColumns.Add("提报单位名称");
                excelColumns.Add("配件编号");
                excelColumns.Add("配件名称");
				excelColumns.Add("红岩号");
                excelColumns.Add("单中心库销售占比");                
                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.CarryTime,
                            detail.WarehouseCode,
							detail.WarehouseName,
							detail.SubmitCompanyCode,
                            detail.SubmitCompanyName,
							detail.SparePartCode,
							detail.SparePartName,
							detail.ReferenceCode,
                            detail.PerQty									
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }

    }
}
