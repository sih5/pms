﻿using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string 导出配件公司日均销量明细表(string sparePartCode, string sparePartName, int? warehouseId, DateTime? bCarryTime, DateTime? eCarryTime) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("配件公司日均销量明细表_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = ObjectContext.SIHDailySalesAverages.ToArray();
            
            if(!string.IsNullOrEmpty(sparePartCode)) {
                data = data.Where(t => t.SparePartCode == sparePartCode).ToArray();
            }
            if(!string.IsNullOrEmpty(sparePartName)) {
                data = data.Where(t => t.SparePartName == sparePartName).ToArray();
            }
            if(warehouseId.HasValue) {
                data = data.Where(t => t.WarehouseId == warehouseId).ToArray();
            }
            if(bCarryTime.HasValue) {
                data = data.Where(t => t.CarryTime >= bCarryTime).ToArray();
            }
            if(eCarryTime.HasValue) {
                data = data.Where(t => t.CarryTime <= eCarryTime).ToArray();
            }
            var dataArray = data.ToArray();
            if(data.Any()) {
                excelColumns.Add("结转时间");
                excelColumns.Add("仓库编号");
                excelColumns.Add("仓库名称");
                excelColumns.Add("配件编号");
                excelColumns.Add("红岩号");
                excelColumns.Add("配件名称");
                excelColumns.Add("配件属性");
				excelColumns.Add("经销价");
				excelColumns.Add("前第24个七天订货数量");
				excelColumns.Add("前第23个七天订货数量");
				excelColumns.Add("前第22个七天订货数量");
				excelColumns.Add("前第21个七天订货数量");
				excelColumns.Add("前第20个七天订货数量");
				excelColumns.Add("前第19个七天订货数量");
				excelColumns.Add("前第18个七天订货数量");
				excelColumns.Add("前第17个七天订货数量");
				excelColumns.Add("前第16个七天订货数量");
				excelColumns.Add("前第15个七天订货数量");
				excelColumns.Add("前第14个七天订货数量");
				excelColumns.Add("前第13个七天订货数量");
				excelColumns.Add("前第12个七天订货数量");
				excelColumns.Add("前第11个七天订货数量");
				excelColumns.Add("前第10个七天订货数量");
				excelColumns.Add("前第9个七天订货数量");
				excelColumns.Add("前第8个七天订货数量");
				excelColumns.Add("前第7个七天订货数量");
				excelColumns.Add("前第6个七天订货数量");
				excelColumns.Add("前第5个七天订货数量");
				excelColumns.Add("前第4个七天订货数量");
				excelColumns.Add("前第3个七天订货数量");
				excelColumns.Add("前第2个七天订货数量");
				excelColumns.Add("前第1个七天订货数量");
				excelColumns.Add("N天日均销量");
				excelColumns.Add("总频次");				
                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.CarryTime,
                            detail.WarehouseCode,
							detail.WarehouseName,
							detail.SparePartCode,
							detail.ReferenceCode,
							detail.SparePartName,
							Enum.GetName(typeof(DcsABCStrategyCategory),detail.PartABC==null? 0:detail.PartABC),
                            detail.SalesPrice,
                            detail.TwentyFourWeek,
                            detail.TwentyThreeWeek, 
                            detail.TwentyTwoWeek, 
							detail.TwentyOneWeek,
							detail.TwentyWeek,
							detail.NineteenWeek,
							detail.EightteenWeek,
							detail.SeventeenWeek,
							detail.SixteenWeek,
							detail.FifteenWeek,
							detail.FourteenWeek,
							detail.ThirtWeek,
							detail.TwelveWeek,
							detail.ElevenWeek,
							detail.TenWeek,
							detail.NineWeek,
							detail.EightWeek,
							detail.SevenWeek,
							detail.SixWeek,
							detail.FiveWeek,
							detail.FourWeek,
							detail.ThreeWeek,
							detail.TwoWeek,
							detail.OneWeek,
							detail.NDaysAvg,
							detail.OrderTimes							
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }

    }
}
