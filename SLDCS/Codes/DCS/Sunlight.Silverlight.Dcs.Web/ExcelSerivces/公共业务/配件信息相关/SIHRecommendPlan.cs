﻿using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string 导出配件公司推荐计划表(string sparePartCode, string sparePartName, int? warehouseId,  int? partABC, DateTime? bCarryTime, DateTime? eCarryTime) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("配件公司推荐计划表_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = ObjectContext.SIHRecommendPlans.ToArray();

            if(!string.IsNullOrEmpty(sparePartCode)) {
                data = data.Where(t => t.SparePartCode == sparePartCode).ToArray();
            }
            if(!string.IsNullOrEmpty(sparePartName)) {
                data = data.Where(t => t.SparePartName == sparePartName).ToArray();
            }
            
            if(partABC.HasValue) {
                data = data.Where(t => t.PartABC == partABC).ToArray();
            }
            if(warehouseId.HasValue) {
                data = data.Where(t => t.WarehouseId == warehouseId).ToArray();
            }
            if(bCarryTime.HasValue) {
                data = data.Where(t => t.CarryTime >= bCarryTime).ToArray();
            }
            if(eCarryTime.HasValue) {
                data = data.Where(t => t.CarryTime <= eCarryTime).ToArray();
            }
            var dataArray = data.ToArray();
            if(data.Any()) {
                excelColumns.Add("结转时间");
                excelColumns.Add("仓库编号");
                excelColumns.Add("仓库名称");
                excelColumns.Add("配件编号");
                excelColumns.Add("红岩号");
                excelColumns.Add("配件名称");
                excelColumns.Add("配件属性");
                excelColumns.Add("是否可销售");
                excelColumns.Add("是否可采购");
                excelColumns.Add("是否可直供");
				excelColumns.Add("直供客户数量");
                excelColumns.Add("经销价");
                excelColumns.Add("首选供应商名称");
                excelColumns.Add("前24周日最大订货数量");
				excelColumns.Add("储备系数");
				excelColumns.Add("上限系数");
                excelColumns.Add("下限系数");
                excelColumns.Add("计划周期");
                excelColumns.Add("到货周期");
                excelColumns.Add("临时天数");
                excelColumns.Add("N天日均销量");
                excelColumns.Add("总频次");
                excelColumns.Add("安全天数");                               
                excelColumns.Add("标准库存");
				excelColumns.Add("标准库存金额");
				excelColumns.Add("库存上限");
				excelColumns.Add("库存上限金额");
				excelColumns.Add("库存下限");
                excelColumns.Add("库存下限金额");
                excelColumns.Add("可用库存");
                excelColumns.Add("可用库存金额");
                excelColumns.Add("采购在途");
				excelColumns.Add("采购在途金额");
				excelColumns.Add("欠货");
				excelColumns.Add("理论计划数量");
				excelColumns.Add("推荐计划数量");
				excelColumns.Add("理论库存占比");
				excelColumns.Add("日销量占比");
				excelColumns.Add("包装倍数");
                excelColumns.Add("实际计划数量");
                excelColumns.Add("实际计划金额");
                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.CarryTime,
                            detail.WarehouseCode,
							detail.WarehouseName,
							detail.SparePartCode,
							detail.ReferenceCode,
							detail.SparePartName,
							Enum.GetName(typeof(DcsABCStrategyCategory),detail.PartABC==null? 0:detail.PartABC),
							detail.IsSalable==true?"是":"否",
                            detail.IsOrderable==true?"是":"否",
                            detail.IsDirectSupply==true?"是":"否",
							detail.DirecCount,
                            detail.SalesPrice,
							detail.PartsSupplierName,
                            detail.MaxOrderedQuantity,
                            detail.ReserveCoefficient, 
                            detail.UpperLimitCoefficient, 
							detail.LowerLimitcoefficient,
							detail.OrderGoodsCycle,
							detail.ArriveCycle,
							detail.TemDays,
							detail.NDaysAvg,
							detail.OrderTimes,
							detail.SafeDay,
							detail.StandStock,
							detail.StandStockFee,
							detail.UpperLimit,
							detail.UpperLimitFee,
							detail.LowerLimit,
							detail.LowerLimitFee,
							detail.AvailableStock,
							detail.AvailableStockFee,
							detail.OnWayNumber,
							detail.OnWayNumberFee,
							detail.CurrentShortager,
							detail.PlanQty,
							detail.RecommendQty,
							detail.StockPer,
                            detail.SalesPer	,
							detail.Pack,
							detail.ActQty,
							detail.ActQtyFee													
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }

    }
}
