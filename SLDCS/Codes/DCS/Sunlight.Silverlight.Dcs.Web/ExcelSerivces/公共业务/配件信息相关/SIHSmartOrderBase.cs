﻿using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string 导出配件公司智能订货基础信息(string sparePartCode, string sparePartName, int? warehouseId,string partsSupplierName,int? partABC, DateTime? bCarryTime, DateTime? eCarryTime) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("配件公司日均销量明细表_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = ObjectContext.SIHSmartOrderBases.ToArray();

            if(!string.IsNullOrEmpty(sparePartCode)) {
                data = data.Where(t => t.SparePartCode == sparePartCode).ToArray();
            }
            if(!string.IsNullOrEmpty(sparePartName)) {
                data = data.Where(t => t.SparePartName == sparePartName).ToArray();
            }
            if(!string.IsNullOrEmpty(partsSupplierName)) {
                data = data.Where(t => t.PartsSupplierName == partsSupplierName).ToArray();
            }
            if(partABC.HasValue) {
                data = data.Where(t => t.PartABC == partABC).ToArray();
            }
            if(warehouseId.HasValue) {
                data = data.Where(t => t.WarehouseId == warehouseId).ToArray();
            }
            if(bCarryTime.HasValue) {
                data = data.Where(t => t.CarryTime >= bCarryTime).ToArray();
            }
            if(eCarryTime.HasValue) {
                data = data.Where(t => t.CarryTime <= eCarryTime).ToArray();
            }
            var dataArray = data.ToArray();
            if(data.Any()) {
                excelColumns.Add("结转时间");
                excelColumns.Add("仓库编号");
                excelColumns.Add("仓库名称");
                excelColumns.Add("配件编号");
                excelColumns.Add("红岩号");
                excelColumns.Add("配件名称");
                excelColumns.Add("首选供应商名称");
                excelColumns.Add("配件属性");
                excelColumns.Add("是否可销售");
                excelColumns.Add("是否可采购");
                excelColumns.Add("是否可直供");
                excelColumns.Add("经销价");
                excelColumns.Add("最小订购量");
                excelColumns.Add("最小销售数量");
                excelColumns.Add("订货周期");
                excelColumns.Add("供应商发货周期");
                excelColumns.Add("供应商物流周期");
                excelColumns.Add("销售总频次");
                excelColumns.Add("标准库存");
                excelColumns.Add("安全天数");
                excelColumns.Add("储备系数");
                excelColumns.Add("库存上限系数");
                excelColumns.Add("库存下限系数");
                excelColumns.Add("到货周期");
                excelColumns.Add("库房天数");
                excelColumns.Add("临时天数");
                excelColumns.Add("可用库存");
                excelColumns.Add("采购在途");
                excelColumns.Add("当期欠货");
                excelColumns.Add("包装倍数");               
                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.CarryTime,
                            detail.WarehouseCode,
							detail.WarehouseName,
							detail.SparePartCode,
							detail.ReferenceCode,
							detail.SparePartName,
							detail.PartsSupplierName,
							Enum.GetName(typeof(DcsABCStrategyCategory),detail.PartABC==null? 0:detail.PartABC),
							detail.IsSalable==false?"否":"是",
                            detail.IsOrderable==false?"否":"是",
                            detail.IsDirectSupply==false?"否":"是",
                            detail.SalesPrice,
                            detail.MinBatch,
                            detail.MinPackingAmount, 
                            detail.OrderGoodsCycle, 
							detail.OrderCycle,
							detail.ArrivalCycle,
							detail.OrderTimes,
							detail.StandStock,
							detail.SafeDay,
							detail.ReserveCoefficient,
							detail.UpperLimitCoefficient,
							detail.lowerlimitcoefficient,
							detail.ArriveCycle,
							detail.WarehousDays,
							detail.TemDays,
							detail.AvailableStock,
							detail.OnWayNumber,
							detail.CurrentShortager,
							detail.pack												
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }

    }
}
