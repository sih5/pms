﻿using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string 导出智能订货处理方式配置(int[] ids, int? warehouseId, int? orderProcessMethod, int? status, DateTime? bCreateTime, DateTime? eCreateTime) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("智能订货企业配置_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = ObjectContext.SmartProcessMethods.ToArray();
            if(ids != null && ids.Any()) {
                data = data.Where(t => ids.Contains(t.Id)).ToArray();
            }
            if(warehouseId.HasValue) {
                data = data.Where(t => t.WarehouseId == warehouseId).ToArray();
            }
            if(orderProcessMethod.HasValue) {
                data = data.Where(t => t.OrderProcessMethod == orderProcessMethod).ToArray();
            }
            if(status.HasValue) {
                data = data.Where(t => t.Status == status).ToArray();
            }
            if(bCreateTime.HasValue) {
                data = data.Where(t => t.CreateTime >= bCreateTime).ToArray();
            }
            if(eCreateTime.HasValue) {
                data = data.Where(t => t.CreateTime <= eCreateTime).ToArray();
            }
            var dataArray = data.ToArray();
            if(data.Any()) {
                excelColumns.Add("仓库编号");
                excelColumns.Add("仓库名称");
				excelColumns.Add("订单处理方式");
                excelColumns.Add("状态");
                excelColumns.Add("创建人");
                excelColumns.Add("创建时间");
                excelColumns.Add("作废人");
                excelColumns.Add("作废时间");
                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.WarehouseCode,
                            detail.WarehouseName,
							Enum.GetName(typeof(DcsPartsSalesOrderProcessDetailProcessMethod),detail.OrderProcessMethod),
							Enum.GetName(typeof(DcsBaseDataStatus),detail.Status),
                            detail.CreatorName,
                            detail.CreateTime,
                            detail.AbandonerName, 
                            detail.AbandonTime                            
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }

    }
}
