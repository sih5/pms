﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web
{
    partial class DcsDomainService
    {
        public string ExportSsUsedPartsDisposalBill(int[] ids,string dealerCode,string dealerName,string branchName,int? partsSalesCategoryId,int? status,int? usedPartsDisposalMethod,DateTime? createTimeBegin, DateTime? createTimeEnd)
        {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("服务站旧件处理单_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var SsUsedPartsDisposalBillQuery = this.GetSsUsedPartsDisposalBills();

            if (ids != null && ids.Any()) {
                SsUsedPartsDisposalBillQuery = SsUsedPartsDisposalBillQuery.Where(r => ids.Contains(r.Id));
            }
            if (!string.IsNullOrEmpty(dealerCode)) {
                SsUsedPartsDisposalBillQuery = SsUsedPartsDisposalBillQuery.Where(r => r.DealerCode.ToLower().Contains(dealerCode.ToLower()));
            }
            if (!string.IsNullOrEmpty(dealerName)) {
                SsUsedPartsDisposalBillQuery = SsUsedPartsDisposalBillQuery.Where(r => r.DealerName.Contains(dealerName));
            }
            if (!string.IsNullOrEmpty(branchName)) {
                SsUsedPartsDisposalBillQuery = SsUsedPartsDisposalBillQuery.Where(r => r.BranchName.Contains(branchName));
            }
            if (status.HasValue) {
                SsUsedPartsDisposalBillQuery = SsUsedPartsDisposalBillQuery.Where(r => r.Status == status);
            }

            if (partsSalesCategoryId.HasValue) {
                SsUsedPartsDisposalBillQuery = SsUsedPartsDisposalBillQuery.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId);
            }
            if (createTimeBegin.HasValue) {
                SsUsedPartsDisposalBillQuery = SsUsedPartsDisposalBillQuery.Where(r => r.CreateTime >= createTimeBegin);
            }
            if (createTimeEnd.HasValue) {
                SsUsedPartsDisposalBillQuery = SsUsedPartsDisposalBillQuery.Where(r => r.CreateTime <= createTimeEnd);
            }

            var SsUsedPartsDisposalBillQuerys = from u in SsUsedPartsDisposalBillQuery
                                                     select new
                                                     {
                                                         Code = u.Code,
                                                         BranchName = u.BranchName,
                                                         PartsSalesCategoryName = u.PartsSalesCategory!=null? u.PartsSalesCategory.Name:" ",
                                                         DealerCode = u.DealerCode,
                                                         DealerName = u.DealerName,
                                                         UsedPartsDisposalMethod = u.UsedPartsDisposalMethod,
                                                         ProcessTime = u.ProcessTime,
                                                         TotalAmount = u.TotalAmount,
                                                         Status = u.Status,
                                                         Remark = u.Remark,
                                                         CreatorName = u.CreatorName,
                                                         CreateTime = u.CreateTime,
                                                         ModifierId = u.ModifierId,
                                                         ModifierName = u.ModifierName,
                                                         ApproverName = u.ApproverName,
                                                         ApproveTime = u.ApproveTime,
                                                         AbandonerName = u.AbandonerName,
                                                         AbandonTime = u.AbandonTime 
                                                     };


            var SsUsedPartsDisposalBillArray = SsUsedPartsDisposalBillQuerys.ToArray();


            if (SsUsedPartsDisposalBillArray.Any()) {
                excelColumns.Add(ErrorStrings.ExportColumn_PartsSalesOrderProcessCode);
                excelColumns.Add(ErrorStrings.ExportColumn_Branch);
                excelColumns.Add("品牌");
                excelColumns.Add("服务站编号");
                excelColumns.Add("服务站名称");
                excelColumns.Add("旧件处理类型");
                excelColumns.Add("处理时间");
                excelColumns.Add("总金额");
                excelColumns.Add(ErrorStrings.ExportColumn_Status);
                excelColumns.Add(ErrorStrings.ExportColumn_Remark);
                excelColumns.Add(ErrorStrings.ExportColumn_Creator);
                excelColumns.Add(ErrorStrings.ExportColumn_CreateTime);
                excelColumns.Add(ErrorStrings.ExportColumn_Modifier);
                excelColumns.Add(ErrorStrings.ExportColumn_ModifyTime);
                excelColumns.Add("审批人");
                excelColumns.Add("审批时间");
                excelColumns.Add(ErrorStrings.ExportColumn_Abandoner);
                excelColumns.Add(ErrorStrings.ExportColumn_AbandonTime);


                using (var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index =>
                    {
                        if (index == SsUsedPartsDisposalBillArray.Count() + 1)
                            return null;
                        if (index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = SsUsedPartsDisposalBillArray[index - 1];
                        var values = new object[] {
                            detail.Code, 
                            detail.BranchName,
                            detail.PartsSalesCategoryName,
                            detail.DealerCode, 
                            detail.DealerName, 
                            Enum.GetName(typeof(DcsSsUsedPartsDisposalBillDisposalMethod),detail.UsedPartsDisposalMethod),
                            detail.ProcessTime, 
                            detail.TotalAmount, 
                            Enum.GetName(typeof(DcsSsUsedPartsDisposalBillStatus),detail.Status),
                            detail.Remark, 
                            detail.CreatorName, 
                            detail.CreateTime, 
                            detail.ModifierId, 
                            detail.ModifierName, 
                            detail.ApproverName, 
                            detail.ApproveTime, 
                            detail.AbandonerName, 
                            detail.AbandonTime 
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
    }
}
