﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string ExportUsedPartsReturnPolicyHistory(int[] ids, string dealerCode, string dealerName, string partsCode, string partsName, int? partsSalesCategoryId, string claimBillCode, string usedPartsBarCode, int? usedPartsReturnPolicy, DateTime? createtimeBegin, DateTime? createtimeEnd) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("旧件返回政策履历_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var UsedPartsReturnPolicyHistoryQuery = this.GetUsedPartsReturnPolicyHistoriesWithDealer();

            if(ids != null && ids.Any()) {
                UsedPartsReturnPolicyHistoryQuery = UsedPartsReturnPolicyHistoryQuery.Where(r => ids.Contains(r.Id));
            }
            if(!string.IsNullOrEmpty(dealerCode)) {
                UsedPartsReturnPolicyHistoryQuery = UsedPartsReturnPolicyHistoryQuery.Where(r => r.Dealer.Code.ToLower().Contains(dealerCode.ToLower()));
            }
            if(!string.IsNullOrEmpty(dealerName)) {
                UsedPartsReturnPolicyHistoryQuery = UsedPartsReturnPolicyHistoryQuery.Where(r => r.Dealer.Name.ToLower().Contains(dealerName.ToLower()));
            }
            if(!string.IsNullOrEmpty(partsCode)) {
                UsedPartsReturnPolicyHistoryQuery = UsedPartsReturnPolicyHistoryQuery.Where(r => r.UsedPartsCode.ToLower().Contains(partsCode.ToLower()));
            }
            if(!string.IsNullOrEmpty(partsName)) {
                UsedPartsReturnPolicyHistoryQuery = UsedPartsReturnPolicyHistoryQuery.Where(r => r.UsedPartsName.ToLower().Contains(partsName.ToLower()));
            }
            if(partsSalesCategoryId.HasValue) {
                UsedPartsReturnPolicyHistoryQuery = UsedPartsReturnPolicyHistoryQuery.Where(r => r.PartsSalesCategory.Id == partsSalesCategoryId);
            }
            if(!string.IsNullOrEmpty(claimBillCode)) {
                UsedPartsReturnPolicyHistoryQuery = UsedPartsReturnPolicyHistoryQuery.Where(r => r.ClaimBillCode.ToLower().Contains(claimBillCode.ToLower()));
            }
            if(!string.IsNullOrEmpty(usedPartsBarCode)) {
                UsedPartsReturnPolicyHistoryQuery = UsedPartsReturnPolicyHistoryQuery.Where(r => r.UsedPartsBarCode.ToLower().Contains(usedPartsBarCode.ToLower()));
            }
            if(usedPartsReturnPolicy.HasValue) {
                UsedPartsReturnPolicyHistoryQuery = UsedPartsReturnPolicyHistoryQuery.Where(r => r.UsedPartsReturnPolicy == usedPartsReturnPolicy);
            }
            if(createtimeBegin.HasValue) {
                UsedPartsReturnPolicyHistoryQuery = UsedPartsReturnPolicyHistoryQuery.Where(r => r.CreateTime >= createtimeBegin);
            }
            if(createtimeEnd.HasValue) {
                UsedPartsReturnPolicyHistoryQuery = UsedPartsReturnPolicyHistoryQuery.Where(r => r.CreateTime <= createtimeEnd);
            }

            var usedPartsReturnPolicyHistoryQuerys = from u in UsedPartsReturnPolicyHistoryQuery
                                                     select new {
                                                         DealerCode = u.Dealer.Code,
                                                         DealerName = u.Dealer.Name,
                                                         PartsSalesCategoryName = u.PartsSalesCategory.Name,
                                                         ClaimBillCode = u.ClaimBillCode,
                                                         UsedPartsBarCode = u.UsedPartsBarCode,
                                                         UsedPartsReturnPolicy = u.UsedPartsReturnPolicy,
                                                         UsedPartsCode = u.UsedPartsCode,
                                                         UsedPartsName = u.UsedPartsName,
                                                         Shipped = u.Shipped,
                                                         UnitPrice = u.UnitPrice,
                                                         PartsManagementCost = u.PartsManagementCost,
                                                         IfFaultyParts = u.IfFaultyParts,
                                                         UsedPartsSupplierCode = u.UsedPartsSupplierCode,
                                                         FaultyPartsCode = u.FaultyPartsCode,
                                                         FaultyPartsName = u.FaultyPartsName,
                                                         FaultyPartsSupplierCode = u.FaultyPartsSupplierCode,
                                                         FaultyPartsSupplierName = u.FaultyPartsSupplierName,
                                                         CreatorName = u.CreatorName,
                                                         CreateTime = u.CreateTime
                                                     };


            var UsedPartsReturnPolicyHistoryArray = usedPartsReturnPolicyHistoryQuerys.ToArray();


            if(UsedPartsReturnPolicyHistoryArray.Any()) {
                excelColumns.Add("服务站编号");
                excelColumns.Add("服务站名称");
                excelColumns.Add("品牌");
                excelColumns.Add("索赔单编号");
                excelColumns.Add("旧件条码");
                excelColumns.Add("旧件返回政策");
                excelColumns.Add("旧件编号");
                excelColumns.Add("旧件名称");
                excelColumns.Add("已发运");
                excelColumns.Add("单价");
                excelColumns.Add("配件管理费");
                excelColumns.Add("是否祸首件");
                excelColumns.Add("旧件供应商编号");
                excelColumns.Add("祸首件编号");
                excelColumns.Add("祸首件名称");
                excelColumns.Add("祸首件供应商编号");
                excelColumns.Add("祸首件供应商名称");
                excelColumns.Add(ErrorStrings.ExportColumn_Creator);
                excelColumns.Add(ErrorStrings.ExportColumn_CreateTime);

                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == UsedPartsReturnPolicyHistoryArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = UsedPartsReturnPolicyHistoryArray[index - 1];
                        var values = new object[] {
                                detail.DealerCode,
                                detail.DealerName,
                                detail.PartsSalesCategoryName,
                                detail.ClaimBillCode,
                                detail.UsedPartsBarCode,
                                Enum.GetName(typeof(DcsPartsWarrantyTermReturnPolicy),detail.UsedPartsReturnPolicy),
                                detail.UsedPartsCode,
                                detail.UsedPartsName,
                                detail.Shipped?EntityStrings.Export_Title_Yes:EntityStrings.Export_Title_No,
                                detail.UnitPrice,
                                detail.PartsManagementCost,
                                detail.IfFaultyParts?EntityStrings.Export_Title_Yes:EntityStrings.Export_Title_No,
                                detail.UsedPartsSupplierCode,
                                detail.FaultyPartsCode,
                                detail.FaultyPartsName,
                                detail.FaultyPartsSupplierCode,
                                detail.FaultyPartsSupplierName,
                                detail.CreatorName,
                                detail.CreateTime
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
    }
}
