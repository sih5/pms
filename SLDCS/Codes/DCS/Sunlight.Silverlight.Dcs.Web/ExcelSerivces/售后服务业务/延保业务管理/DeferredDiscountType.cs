﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string 延保折扣类型导出(int[] ids, int? partsSalesCategoryId, string extendedWarrantyProductName, string extendedWarrantyAgioType, double? extendedWarrantyAgio, int? status, DateTime? createTimeStart, DateTime? createTimeEnd) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("延保折扣类型导出_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var deferredDiscountTypes = GetDeferredDiscountTypes();
            if(ids != null && ids.Length > 0) {
                deferredDiscountTypes = deferredDiscountTypes.Where(r => ids.Contains(r.Id));
            } else {
                if(partsSalesCategoryId.HasValue) {
                    deferredDiscountTypes = deferredDiscountTypes.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId);
                }
                if(!string.IsNullOrEmpty(extendedWarrantyProductName)) {
                    deferredDiscountTypes = deferredDiscountTypes.Where(r => r.ExtendedWarrantyProductName.ToLower().Contains(extendedWarrantyProductName.ToLower()));
                }
                if(status.HasValue) {
                    deferredDiscountTypes = deferredDiscountTypes.Where(r => r.Status == status);
                }
                if(!string.IsNullOrEmpty(extendedWarrantyAgioType)) {
                    deferredDiscountTypes = deferredDiscountTypes.Where(r => r.ExtendedWarrantyAgioType.ToLower().Contains(extendedWarrantyAgioType.ToLower()));
                }
                if(createTimeStart.HasValue) {
                    deferredDiscountTypes = deferredDiscountTypes.Where(r => r.CreatorTime >= createTimeStart);
                }
                if(createTimeEnd.HasValue) {
                    deferredDiscountTypes = deferredDiscountTypes.Where(r => r.CreatorTime <= createTimeEnd);
                }
                if(extendedWarrantyAgio.HasValue) {
                    deferredDiscountTypes = deferredDiscountTypes.Where(r => r.ExtendedWarrantyAgio == extendedWarrantyAgio);
                }
            }
            var deferredDiscountTypesArray = deferredDiscountTypes.ToArray();
            if(deferredDiscountTypesArray.Any()) {
                excelColumns.Add("品牌");
                excelColumns.Add("延保产品编号");
                excelColumns.Add("延保产品");
                excelColumns.Add("延保折扣类型");
                excelColumns.Add("延保折扣");
                excelColumns.Add("状态");
                excelColumns.Add("创建人");
                excelColumns.Add("创建时间");
                excelColumns.Add("修改人");
                excelColumns.Add("修改时间");
                excelColumns.Add("作废人");
                excelColumns.Add("作废时间");

                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == deferredDiscountTypesArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = deferredDiscountTypesArray[index - 1];
                        var values = new object[] {
                            detail.PartsSalesCategory, 
                            detail.ExtendedWarrantyProduct.ExtendedWarrantyProductCode,
                            detail.ExtendedWarrantyProductName, 
                            detail.ExtendedWarrantyAgioType, 
                            detail.ExtendedWarrantyAgio, 
                            Enum.GetName(typeof(DcsBaseDataStatus), 
                            detail.Status), 
                            detail.CreatorName, 
                            detail.CreatorTime, 
                            detail.ModifierName, 
                            detail.ModifierTime, 
                            detail.AbandonerName, 
                            detail.AbandonerTime
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
    }
}
