﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class UsedPartsInboundOrderAch : DcsSerivceAchieveBase {
        public string ExportUsedPartsInboundOrderWithDetail(int? id, string code, int? inboundType, string usedPartsWarehouseCode, string usedPartsWarehouseName, DateTime? createTimeStart, DateTime? createTimeEnd, string sourceCode, string relatedCompanyCode, string relatedCompanyName, int? settlementStatus) {
            var excelColumns = new List<string>();
            var exportDataFileName =DomainService. GetExportFilePath(string.Format("旧件入库单主清单_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var usedPartsInboundOrders = 旧件仓库人员查询旧件入库单();
            if(id.HasValue) {
                usedPartsInboundOrders = usedPartsInboundOrders.Where(r => r.Id == id);
            }else {
                if(!string.IsNullOrEmpty(code)) {
                    usedPartsInboundOrders = usedPartsInboundOrders.Where(r => r.Code == code);
                }
                if(inboundType.HasValue) {
                    usedPartsInboundOrders = usedPartsInboundOrders.Where(r => r.InboundType == inboundType);
                }
                if(!string.IsNullOrEmpty(usedPartsWarehouseCode)) {
                    usedPartsInboundOrders = usedPartsInboundOrders.Where(r => r.UsedPartsWarehouseCode == usedPartsWarehouseCode);
                }
                if(!string.IsNullOrEmpty(usedPartsWarehouseName)) {
                    usedPartsInboundOrders = usedPartsInboundOrders.Where(r => r.UsedPartsWarehouseName == usedPartsWarehouseName);
                }
                if(createTimeStart.HasValue) {
                    usedPartsInboundOrders = usedPartsInboundOrders.Where(r => r.CreateTime >= createTimeStart);
                }
                if(createTimeEnd.HasValue) {
                    usedPartsInboundOrders = usedPartsInboundOrders.Where(r => r.CreateTime <= createTimeEnd);
                }
                if(!string.IsNullOrEmpty(sourceCode)) {
                    usedPartsInboundOrders = usedPartsInboundOrders.Where(r => r.SourceCode == sourceCode);
                }
                if(!string.IsNullOrEmpty(relatedCompanyCode)) {
                    usedPartsInboundOrders = usedPartsInboundOrders.Where(r => r.RelatedCompanyCode == relatedCompanyCode);
                }
                if(!string.IsNullOrEmpty(relatedCompanyName)) {
                    usedPartsInboundOrders = usedPartsInboundOrders.Where(r => r.RelatedCompanyName == relatedCompanyName);
                }
                if(settlementStatus.HasValue) {
                    usedPartsInboundOrders = usedPartsInboundOrders.Where(r => r.SettlementStatus == settlementStatus);
                }
            }
            var dataToExport = from a in usedPartsInboundOrders
                               join b in ObjectContext.UsedPartsInboundDetails on a.Id equals b.UsedPartsInboundOrderId into t1
                               from c in t1.DefaultIfEmpty()
                               select new {
                                   a.Code,
                                   a.UsedPartsWarehouseName,
                                   a.InboundType,
                                   a.RelatedCompanyCode,
                                   a.RelatedCompanyName,
                                   c.UsedPartsCode,
                                   c.UsedPartsName,
                                   c.UsedPartsBarCode,
                                   c.UsedPartsWarehouseAreaCode,
                                   c.Quantity,
                                   c.SettlementPrice,
                                   c.CostPrice,
                                   c.Remark,
                                   c.UsedPartsSupplierCode,
                                   c.UsedPartsSupplierName,
                                   c.ClaimBillType,
                                   c.ClaimBillCode,
                                   c.IfFaultyParts,
                                   c.FaultyPartsSupplierCode,
                                   c.FaultyPartsSupplierName,
                                   c.ResponsibleUnitCode,
                                   c.ResponsibleUnitName,
                                   c.FaultyPartsCode,
                                   c.FaultyPartsName,
                               };
            var usedPartsInboundOrdersArray = dataToExport.ToArray();
            if(usedPartsInboundOrdersArray.Any()) {
                excelColumns.Add("旧件入库单编号");
                excelColumns.Add("入库仓库名称");
                excelColumns.Add("入库类型");
                excelColumns.Add("对方单位编号");
                excelColumns.Add("对方单位名称");
                excelColumns.Add("旧件配件编号");
                excelColumns.Add("旧件配件名称");
                excelColumns.Add("旧件条码");
                excelColumns.Add("旧件库位编号");
                excelColumns.Add("数量");
                excelColumns.Add("结算价格");
                excelColumns.Add("成本价格");
                excelColumns.Add("备注");
                excelColumns.Add("旧件供应商编号");
                excelColumns.Add("旧件供应商名称");
                excelColumns.Add("索赔单类型");
                excelColumns.Add("索赔单编号");
                excelColumns.Add("是否祸首件");
                excelColumns.Add("祸首件供应商编号");
                excelColumns.Add("祸首件供应商名称");
                excelColumns.Add("责任单位编号");
                excelColumns.Add("责任单位名称");
                excelColumns.Add("祸首件编号");
                excelColumns.Add("祸首件名称");
                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == usedPartsInboundOrdersArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = usedPartsInboundOrdersArray[index - 1];
                        var values = new object[] {
                            detail.Code,
                            detail.UsedPartsWarehouseName,
                            Enum.GetName(typeof(DcsUsedPartsInboundOrderInboundType),detail.InboundType),
                            detail.RelatedCompanyCode,
                            detail.RelatedCompanyName,
                            detail.UsedPartsCode,
                            detail.UsedPartsName,
                            detail.UsedPartsBarCode,
                            detail.UsedPartsWarehouseAreaCode,
                            detail.Quantity,
                            detail.SettlementPrice,
                            detail.CostPrice,
                            detail.Remark,
                            detail.UsedPartsSupplierCode,
                            detail.UsedPartsSupplierName,
                            Enum.GetName(typeof(DcsClaimBillType),detail.ClaimBillType),
                            detail.ClaimBillCode,
                            detail.IfFaultyParts.HasValue?(detail.IfFaultyParts.Value?"是":"否"):"",
                            detail.FaultyPartsSupplierCode,
                            detail.FaultyPartsSupplierName,
                            detail.ResponsibleUnitCode,
                            detail.ResponsibleUnitName,
                            detail.FaultyPartsCode,
                            detail.FaultyPartsName
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
        public string ExportUsedPartsInboundOrder(int[] ids, string code, int? inboundType, string usedPartsWarehouseCode, string usedPartsWarehouseName, DateTime? createTimeStart, DateTime? createTimeEnd, string sourceCode, string relatedCompanyCode, string relatedCompanyName, int? settlementStatus) {
            var excelColumns = new List<string>();
            var exportDataFileName =DomainService. GetExportFilePath(string.Format("旧件入库单_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var usedPartsInboundOrders = 旧件仓库人员查询旧件入库单();
            if(ids != null && ids.Length>0) {
                usedPartsInboundOrders = usedPartsInboundOrders.Where(r => ids.Contains(r.Id));
            }else {
                if(!string.IsNullOrEmpty(code)) {
                    usedPartsInboundOrders = usedPartsInboundOrders.Where(r => r.Code == code);
                }
                if(inboundType.HasValue) {
                    usedPartsInboundOrders = usedPartsInboundOrders.Where(r => r.InboundType == inboundType);
                }
                if(!string.IsNullOrEmpty(usedPartsWarehouseCode)) {
                    usedPartsInboundOrders = usedPartsInboundOrders.Where(r => r.UsedPartsWarehouseCode == usedPartsWarehouseCode);
                }
                if(!string.IsNullOrEmpty(usedPartsWarehouseName)) {
                    usedPartsInboundOrders = usedPartsInboundOrders.Where(r => r.UsedPartsWarehouseName == usedPartsWarehouseName);
                }
                if(createTimeStart.HasValue) {
                    usedPartsInboundOrders = usedPartsInboundOrders.Where(r => r.CreateTime >= createTimeStart);
                }
                if(createTimeEnd.HasValue) {
                    usedPartsInboundOrders = usedPartsInboundOrders.Where(r => r.CreateTime <= createTimeEnd);
                }
                if(!string.IsNullOrEmpty(sourceCode)) {
                    usedPartsInboundOrders = usedPartsInboundOrders.Where(r => r.SourceCode == sourceCode);
                }
                if(!string.IsNullOrEmpty(relatedCompanyCode)) {
                    usedPartsInboundOrders = usedPartsInboundOrders.Where(r => r.RelatedCompanyCode == relatedCompanyCode);
                }
                if(!string.IsNullOrEmpty(relatedCompanyName)) {
                    usedPartsInboundOrders = usedPartsInboundOrders.Where(r => r.RelatedCompanyName == relatedCompanyName);
                }
                if(settlementStatus.HasValue) {
                    usedPartsInboundOrders = usedPartsInboundOrders.Where(r => r.SettlementStatus == settlementStatus);
                }
            }
            var usedPartsInboundOrdersArray = usedPartsInboundOrders.ToArray();
            if(usedPartsInboundOrdersArray.Any()) {
                excelColumns.Add("旧件入库单编号");
                excelColumns.Add("入库仓库编号");
                excelColumns.Add("入库仓库名称");
                excelColumns.Add("入库类型");
                excelColumns.Add("源单据编号");
                excelColumns.Add("对方单位编号");
                excelColumns.Add("对方单位名称");
                excelColumns.Add("总金额");
                excelColumns.Add("旧件总数量");
                excelColumns.Add("经办人");
                excelColumns.Add("备注");
                excelColumns.Add("是否可结算");
                excelColumns.Add("是否已结算");
                excelColumns.Add("创建人");
                excelColumns.Add("创建时间");
                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == usedPartsInboundOrdersArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = usedPartsInboundOrdersArray[index - 1];
                        var values = new object[] {
                            detail.Code,
                            detail.UsedPartsWarehouseCode,
                            detail.UsedPartsWarehouseName,
                            Enum.GetName(typeof(DcsUsedPartsInboundOrderInboundType),detail.InboundType),
                            detail.SourceCode,
                            detail.RelatedCompanyCode,
                            detail.RelatedCompanyName,
                            detail.TotalAmount,
                            detail.TotalQuantity,
                            detail.Operator,
                            detail.Remark,
                            (detail.IfBillable.HasValue && detail.IfBillable.Value)?"是":"否",
                            (detail.IfAlreadySettled.HasValue && detail.IfAlreadySettled.Value)?"是":"否",
                            detail.CreatorName,
                            detail.CreateTime       
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 15:15:50
        //原分布类的函数全部转移到Ach                                                               

        public string ExportUsedPartsInboundOrderWithDetail(int? id, string code, int? inboundType, string usedPartsWarehouseCode, string usedPartsWarehouseName, DateTime? createTimeStart, DateTime? createTimeEnd, string sourceCode, string relatedCompanyCode, string relatedCompanyName, int? settlementStatus) {
            return new UsedPartsInboundOrderAch(this).ExportUsedPartsInboundOrderWithDetail(id,code,inboundType,usedPartsWarehouseCode,usedPartsWarehouseName,createTimeStart,createTimeEnd,sourceCode,relatedCompanyCode,relatedCompanyName,settlementStatus);
        }

        public string ExportUsedPartsInboundOrder(int[] ids, string code, int? inboundType, string usedPartsWarehouseCode, string usedPartsWarehouseName, DateTime? createTimeStart, DateTime? createTimeEnd, string sourceCode, string relatedCompanyCode, string relatedCompanyName, int? settlementStatus) {
            return new UsedPartsInboundOrderAch(this).ExportUsedPartsInboundOrder(ids,code,inboundType,usedPartsWarehouseCode,usedPartsWarehouseName,createTimeStart,createTimeEnd,sourceCode,relatedCompanyCode,relatedCompanyName,settlementStatus);
        }
    }
}
