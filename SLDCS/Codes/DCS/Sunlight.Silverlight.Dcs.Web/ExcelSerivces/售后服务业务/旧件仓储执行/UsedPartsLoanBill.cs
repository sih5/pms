﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class UsedPartsLoanBillAch : DcsSerivceAchieveBase {
        public string ExportUsedPartsLoanBill(int? id, string usedPartsWarehouseCode, string usedPartsWarehouseName, string code, string relatedCompanyName, int? status, int? outboundStatus, bool? ifReturned, int? inboundStatus, DateTime? createTimeStart, DateTime? createTimeEnd, DateTime? approveTimeStart, DateTime? approveTimeEnd) {
            var excelColumns = new List<string>();
            var exportDataFileName =DomainService. GetExportFilePath(string.Format("旧件借用单_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var usedPartsLoanBills = 旧件仓库人员查询旧件借用单();
            if(id.HasValue) {
                usedPartsLoanBills = usedPartsLoanBills.Where(r => r.Id == id);
            }else {
                if(!string.IsNullOrEmpty(usedPartsWarehouseCode)) {
                    usedPartsLoanBills = usedPartsLoanBills.Where(r => r.UsedPartsWarehouseCode == usedPartsWarehouseCode);
                }
                if(!string.IsNullOrEmpty(usedPartsWarehouseName)) {
                    usedPartsLoanBills = usedPartsLoanBills.Where(r => r.UsedPartsWarehouseName == usedPartsWarehouseName);
                }
                if(!string.IsNullOrEmpty(code)) {
                    usedPartsLoanBills = usedPartsLoanBills.Where(r => r.Code == code);
                }
                if(!string.IsNullOrEmpty(relatedCompanyName)) {
                    usedPartsLoanBills = usedPartsLoanBills.Where(r => r.RelatedCompanyName == relatedCompanyName);
                }
                if(status.HasValue) {
                    usedPartsLoanBills = usedPartsLoanBills.Where(r => r.Status == status);
                }
                if(outboundStatus.HasValue) {
                    usedPartsLoanBills = usedPartsLoanBills.Where(r => r.OutboundStatus == outboundStatus);
                }
                if(ifReturned.HasValue) {
                    usedPartsLoanBills = usedPartsLoanBills.Where(r => r.IfReturned == ifReturned);
                }
                if(inboundStatus.HasValue) {
                    usedPartsLoanBills = usedPartsLoanBills.Where(r => r.InboundStatus == inboundStatus);
                }
                if(createTimeStart.HasValue) {
                    usedPartsLoanBills = usedPartsLoanBills.Where(r => r.CreateTime >= createTimeStart);
                }
                if(createTimeEnd.HasValue) {
                    usedPartsLoanBills = usedPartsLoanBills.Where(r => r.CreateTime <= createTimeEnd);
                }
                if(approveTimeStart.HasValue) {
                    usedPartsLoanBills = usedPartsLoanBills.Where(r => r.ApproveTime >= approveTimeStart);
                }
                if(approveTimeEnd.HasValue) {
                    usedPartsLoanBills = usedPartsLoanBills.Where(r => r.ApproveTime <= approveTimeEnd);
                }
            }
            var dataToExport = usedPartsLoanBills.ToArray();
            if(usedPartsLoanBills.Any()) {
                excelColumns.Add("旧件借用单编号");
                excelColumns.Add("旧件仓库编号");
                excelColumns.Add("旧件仓库名称");
                excelColumns.Add("状态");
                excelColumns.Add("出库状态");
                excelColumns.Add("入库状态");
                excelColumns.Add("是否归还");
                excelColumns.Add("计划归还时间");
                excelColumns.Add("借用人");
                excelColumns.Add("归还人");
                excelColumns.Add("相关单位名称");
                excelColumns.Add("备注");
                excelColumns.Add("创建人");
                excelColumns.Add("创建时间");
                excelColumns.Add("修改人");
                excelColumns.Add("修改时间");
                excelColumns.Add("审批人");
                excelColumns.Add("审批时间");
                excelColumns.Add("作废人");
                excelColumns.Add("作废时间");
                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataToExport.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataToExport[index - 1];
                        var values = new object[] {
                            detail.Code,
                            detail.UsedPartsWarehouseCode,
                            detail.UsedPartsWarehouseName,
                            Enum.GetName(typeof(DcsUsedPartsLoanBillStatus),detail.Status),
                             detail.OutboundStatus.HasValue?Enum.GetName(typeof(DcsUsedPartsOutboundStatus),detail.OutboundStatus.Value):"",
                            detail.InboundStatus.HasValue?Enum.GetName(typeof(DcsUsedPartsInboundStatus),detail.InboundStatus.Value):"",
                            detail.IfReturned?"是":"否",
                            detail.PlannedReturnTime,
                            detail.LoanRequestor,
                            detail.ReturnExecutor,
                            detail.RelatedCompanyName,
                            detail.Remark,
                            detail.CreatorName,
                            detail.CreateTime,
                            detail.ModifierName,
                            detail.ModifyTime,
                            detail.ApproverName,
                            detail.ApproveTime,
                            detail.AbandonerName,
                            detail.AbandonTime,
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
        public string ExportUsedPartsLoanBillWithDetail(int? id, string usedPartsWarehouseCode, string usedPartsWarehouseName, string code, string relatedCompanyName, int? status, int? outboundStatus, bool? ifReturned, int? inboundStatus, DateTime? createTimeStart, DateTime? createTimeEnd, DateTime? approveTimeStart, DateTime? approveTimeEnd) {
            var excelColumns = new List<string>();
            var exportDataFileName =DomainService. GetExportFilePath(string.Format("旧件借用单_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var usedPartsLoanBills = 旧件仓库人员查询旧件借用单();
            if(id.HasValue) {
                usedPartsLoanBills = usedPartsLoanBills.Where(r => r.Id == id);
            }else {
                if(!string.IsNullOrEmpty(usedPartsWarehouseCode)) {
                    usedPartsLoanBills = usedPartsLoanBills.Where(r => r.UsedPartsWarehouseCode == usedPartsWarehouseCode);
                }
                if(!string.IsNullOrEmpty(usedPartsWarehouseName)) {
                    usedPartsLoanBills = usedPartsLoanBills.Where(r => r.UsedPartsWarehouseName == usedPartsWarehouseName);
                }
                if(!string.IsNullOrEmpty(code)) {
                    usedPartsLoanBills = usedPartsLoanBills.Where(r => r.Code == code);
                }
                if(!string.IsNullOrEmpty(relatedCompanyName)) {
                    usedPartsLoanBills = usedPartsLoanBills.Where(r => r.RelatedCompanyName == relatedCompanyName);
                }
                if(status.HasValue) {
                    usedPartsLoanBills = usedPartsLoanBills.Where(r => r.Status == status);
                }
                if(outboundStatus.HasValue) {
                    usedPartsLoanBills = usedPartsLoanBills.Where(r => r.OutboundStatus == outboundStatus);
                }
                if(ifReturned.HasValue) {
                    usedPartsLoanBills = usedPartsLoanBills.Where(r => r.IfReturned == ifReturned);
                }
                if(inboundStatus.HasValue) {
                    usedPartsLoanBills = usedPartsLoanBills.Where(r => r.InboundStatus == inboundStatus);
                }
                if(createTimeStart.HasValue) {
                    usedPartsLoanBills = usedPartsLoanBills.Where(r => r.CreateTime >= createTimeStart);
                }
                if(createTimeEnd.HasValue) {
                    usedPartsLoanBills = usedPartsLoanBills.Where(r => r.CreateTime <= createTimeEnd);
                }
                if(approveTimeStart.HasValue) {
                    usedPartsLoanBills = usedPartsLoanBills.Where(r => r.ApproveTime >= approveTimeStart);
                }
                if(approveTimeEnd.HasValue) {
                    usedPartsLoanBills = usedPartsLoanBills.Where(r => r.ApproveTime <= approveTimeEnd);
                }
            }
            var dataToExport = from a in usedPartsLoanBills
                               join b in ObjectContext.UsedPartsLoanDetails on a.Id equals b.UsedPartsLoanBillId into t1
                               from c in t1
                               select new {
                                   a.Code,
                                   a.UsedPartsWarehouseName,
                                   c.UsedPartsCode,
                                   c.UsedPartsName,
                                   c.UsedPartsBarCode,
                                   c.Price,
                                   c.PlannedAmount,
                                   c.ConfirmedAmount,
                                   c.OutboundAmount,
                                   c.InboundAmount,
                                   c.UsedPartsSupplierCode,
                                   c.UsedPartsSupplierName,
                                   c.ClaimBillType,
                                   c.ClaimBillCode,
                                   c.IfFaultyParts,
                                   c.FaultyPartsSupplierCode,
                                   c.FaultyPartsSupplierName,
                                   c.ResponsibleUnitCode,
                                   c.ResponsibleUnitName,
                                   c.FaultyPartsCode,
                                   c.FaultyPartsName,
                               };
            if(usedPartsLoanBills.Any()) {
                excelColumns.Add("旧件借用单编号");
                excelColumns.Add("旧件仓库名称");
                excelColumns.Add("旧件配件编号");
                excelColumns.Add("旧件配件名称");
                excelColumns.Add("旧件条码");
                excelColumns.Add("价格");
                excelColumns.Add("计划量");
                excelColumns.Add("确认量");
                excelColumns.Add("出库量");
                excelColumns.Add("入库量");
                excelColumns.Add("旧件供应商编号");
                excelColumns.Add("旧件供应商名称");
                excelColumns.Add("索赔单类型");
                excelColumns.Add("索赔单编号");
                excelColumns.Add("是否祸首件");
                excelColumns.Add("祸首件供应商编号");
                excelColumns.Add("祸首件供应商名称");
                excelColumns.Add("责任单位编号");
                excelColumns.Add("责任单位名称");
                excelColumns.Add("祸首件编号");
                excelColumns.Add("祸首件名称");
                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataToExport.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataToExport.ToArray()[index - 1];
                        var values = new object[] {
                            detail.Code,
                            detail.UsedPartsWarehouseName,
                            detail.UsedPartsCode,
                            detail.UsedPartsName,
                            detail.UsedPartsBarCode,
                            detail.Price,
                            detail.PlannedAmount,
                            detail.ConfirmedAmount,
                            detail.OutboundAmount,
                            detail.InboundAmount,
                            detail.UsedPartsSupplierCode,
                            detail.UsedPartsSupplierName,
                            Enum.GetName(typeof(DcsClaimBillType),detail.ClaimBillType),
                            detail.ClaimBillCode,
                            detail.IfFaultyParts.HasValue?(detail.IfFaultyParts.Value?"是":"否"):"",
                            detail.FaultyPartsSupplierCode,
                            detail.FaultyPartsSupplierName,
                            detail.ResponsibleUnitCode,
                            detail.ResponsibleUnitName,
                            detail.FaultyPartsCode,
                            detail.FaultyPartsName,
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 15:15:50
        //原分布类的函数全部转移到Ach                                                               

        public string ExportUsedPartsLoanBill(int? id, string usedPartsWarehouseCode, string usedPartsWarehouseName, string code, string relatedCompanyName, int? status, int? outboundStatus, bool? ifReturned, int? inboundStatus, DateTime? createTimeStart, DateTime? createTimeEnd, DateTime? approveTimeStart, DateTime? approveTimeEnd) {
            return new UsedPartsLoanBillAch(this).ExportUsedPartsLoanBill(id,usedPartsWarehouseCode,usedPartsWarehouseName,code,relatedCompanyName,status,outboundStatus,ifReturned,inboundStatus,createTimeStart,createTimeEnd,approveTimeStart,approveTimeEnd);
        }

        public string ExportUsedPartsLoanBillWithDetail(int? id, string usedPartsWarehouseCode, string usedPartsWarehouseName, string code, string relatedCompanyName, int? status, int? outboundStatus, bool? ifReturned, int? inboundStatus, DateTime? createTimeStart, DateTime? createTimeEnd, DateTime? approveTimeStart, DateTime? approveTimeEnd) {
            return new UsedPartsLoanBillAch(this).ExportUsedPartsLoanBillWithDetail(id,usedPartsWarehouseCode,usedPartsWarehouseName,code,relatedCompanyName,status,outboundStatus,ifReturned,inboundStatus,createTimeStart,createTimeEnd,approveTimeStart,approveTimeEnd);
        }
    }
}
