﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class VirtualUsedPartsInboundPlanDetailAch : DcsSerivceAchieveBase {
        /// <summary>
        /// 导出虚拟旧件入库清单
        /// </summary>
        public string ExportVirtualUsedPartsInboundPlanDetail(int? billId, int personnelId, string sourceCode, int? inboundType, int? inboundStatus, string usedPartsWarehouseCode, string usedPartsWarehouseName, DateTime? sourceBillTimeBegin, DateTime? sourceBillTimeEnd) {
            var excelColumns = new List<string>();
            var columns = new[]{"源单据编号","旧件入库类型","旧件入库状态","入库仓库名称","相关单位编号",
                "相关单位名称","旧件条码","配件编号","配件名称","库位编号","计划量","确认量","入库量",
                "结算价格","成本价格","索赔单类型","索赔单编号","旧件供应商编号","旧件供应商名称",
                "是否祸首件","祸首件编号","祸首件供应商编号","祸首件供应商名称","责任单位编号","备注"
            };
            excelColumns.AddRange(columns);
            var exportDataFileName =DomainService. GetExportFilePath("旧件入库清单.xlsx");
            var list = new List<VirtualUsedPartsInboundPlan>();
            var detailList = new List<VirtualUsedPartsInboundPlanDetail>();
            if(billId.HasValue) {
                var virtualUsedPartsInboundPlans = new VirtualUsedPartsInboundPlanAch(this.DomainService).查询虚拟旧件入库计划(null, null, null, null, null, null, null, personnelId).Where(r => r.SourceId == billId);
                foreach(var virtualUsedPartsInboundPlan in virtualUsedPartsInboundPlans) {
                    if(virtualUsedPartsInboundPlan.InboundType == null)
                        continue;
                    list.Add(virtualUsedPartsInboundPlan);
                    var virtualUsedPartsInboundPlanDetails =DomainService. 查询虚拟旧件入库清单(virtualUsedPartsInboundPlan.InboundType.Value, virtualUsedPartsInboundPlan.SourceId, true).ToList();
                    if(virtualUsedPartsInboundPlanDetails.Any()) {
                        detailList.AddRange(virtualUsedPartsInboundPlanDetails);
                    }else {
                        detailList.Add(new VirtualUsedPartsInboundPlanDetail {
                            SourceId = virtualUsedPartsInboundPlan.SourceId
                        });
                    }
                }
            }else {
                var virtualUsedPartsInboundPlans = new VirtualUsedPartsInboundPlanAch(this.DomainService).查询虚拟旧件入库计划(sourceCode, sourceBillTimeBegin, sourceBillTimeEnd, inboundType, inboundStatus, usedPartsWarehouseCode, usedPartsWarehouseName, personnelId);
                foreach(var virtualUsedPartsInboundPlan in virtualUsedPartsInboundPlans) {
                    if(virtualUsedPartsInboundPlan.InboundType == null)
                        continue;
                    list.Add(virtualUsedPartsInboundPlan);
                    var virtualUsedPartsInboundPlanDetails =DomainService. 查询虚拟旧件入库清单(virtualUsedPartsInboundPlan.InboundType.Value, virtualUsedPartsInboundPlan.SourceId, true).ToList();
                    if(virtualUsedPartsInboundPlanDetails.Any()) {
                        detailList.AddRange(virtualUsedPartsInboundPlanDetails);
                    }else {
                        detailList.Add(new VirtualUsedPartsInboundPlanDetail {
                            SourceId = virtualUsedPartsInboundPlan.SourceId
                        });
                    }
                }
            }
            if(detailList.Any()) {
                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == detailList.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = detailList[index - 1];
                        var planBill = list.First(r => r.SourceId == detail.SourceId);
                        var values = new object[] {
                            planBill.SourceCode,
                            planBill.InboundType.HasValue?Enum.GetName(typeof(DcsUsedPartsInboundOrderInboundType),planBill.InboundType.Value):null,
                            planBill.InboundStatus.HasValue?Enum.GetName(typeof(DcsUsedPartsInboundStatus),planBill.InboundStatus.Value):null,
                            planBill.UsedPartsWarehouseName,planBill.RelatedCompanyCode,planBill.RelatedCompanyName,
                            detail.UsedPartsBarCode,detail.UsedPartsCode,detail.UsedPartsName,detail.UsedPartsWarehouseAreaCode,
                            detail.PlannedAmount.HasValue?detail.PlannedAmount:null,
                            detail.ConfirmedAmount.HasValue?detail.ConfirmedAmount:null,
                            detail.InboundAmount.HasValue?detail.InboundAmount:null,
                            detail.SettlementPrice.HasValue?detail.SettlementPrice:null,
                            detail.CostPrice.HasValue?detail.CostPrice:null,
                            detail.ClaimBillType.HasValue?Enum.GetName(typeof(DcsClaimBillType),detail.ClaimBillType.Value):null,
                            detail.ClaimBillCode,detail.UsedPartsSupplierCode, detail.UsedPartsSupplierName, 
                            detail.IfFaultyParts.HasValue?(detail.IfFaultyParts.Value?"是":"否"):null,
                            detail.FaultyPartsCode,detail.FaultyPartsSupplierCode,detail.FaultyPartsSupplierName,detail.ResponsibleUnitCode,detail.Remark
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 15:15:50
        //原分布类的函数全部转移到Ach                                                               
        /// <summary>
        /// 导出虚拟旧件入库清单
        /// </summary>
        public string ExportVirtualUsedPartsInboundPlanDetail(int? billId, int personnelId, string sourceCode, int? inboundType, int? inboundStatus, string usedPartsWarehouseCode, string usedPartsWarehouseName, DateTime? sourceBillTimeBegin, DateTime? sourceBillTimeEnd) {
            return new VirtualUsedPartsInboundPlanDetailAch(this).ExportVirtualUsedPartsInboundPlanDetail(billId,personnelId,sourceCode,inboundType,inboundStatus,usedPartsWarehouseCode,usedPartsWarehouseName,sourceBillTimeBegin,sourceBillTimeEnd);
        }
    }
}
