﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class VirtualUsedPartsOutboundPlanDetailAch : DcsSerivceAchieveBase {
        /// <summary>
        /// 导出虚拟旧件出库清单
        /// </summary>
        public string ExportVirtualUsedPartsOutboundPlanDetail(int? billId, int personnelId, string sourceCode, int? outboundType, int? outboundStatus, string usedPartsWarehouseCode, string usedPartsWarehouseName, DateTime? sourceBillTimeBegin, DateTime? sourceBillTimeEnd, string relatedCompanyName, string businessCode) {
            var excelColumns = new List<string>();
            var columns = new[] {"源单据编号","旧件出库类型","旧件出库状态","出库仓库名称","相关单位编号","相关单位名称",
                "旧件条码","配件编号","配件名称","库位编号","计划量","确认量","出库量","结算价格","成本价格",
                "索赔单类型","索赔单编号","旧件供应商编号","旧件供应商名称","是否祸首件","祸首件编号",
                "祸首件供应商编号","祸首件供应商名称","责任单位编号","备注","清单备注"};
            excelColumns.AddRange(columns);
            var exportDataFileName =DomainService. GetExportFilePath("旧件出库清单.xlsx");
            var list = new List<VirtualUsedPartsOutboundPlan>();
            var detailList = new List<VirtualUsedPartsOutboundPlanDetail>();
            if(billId.HasValue) {
                var virtualUsedPartsOutboundPlans =DomainService. 查询虚拟旧件出库计划(null, null, null, null, null, null, null, personnelId, null, null).Where(r => r.SourceId == billId);
                foreach(var virtualUsedPartsOutboundPlan in virtualUsedPartsOutboundPlans) {
                    if(virtualUsedPartsOutboundPlan.OutboundType == null)
                        continue;
                    list.Add(virtualUsedPartsOutboundPlan);
                    var virtualUsedPartsOutboundPlanDetails =DomainService. 查询虚拟旧件出库清单(virtualUsedPartsOutboundPlan.OutboundType.Value, virtualUsedPartsOutboundPlan.SourceId, true, Silverlight.Web.Utils.GetCurrentUserInfo().Id, virtualUsedPartsOutboundPlan.UsedPartsWarehouseId).ToList();
                    if(virtualUsedPartsOutboundPlanDetails.Any()) {
                        detailList.AddRange(virtualUsedPartsOutboundPlanDetails);
                    }else {
                        detailList.Add(new VirtualUsedPartsOutboundPlanDetail {
                            SourceId = virtualUsedPartsOutboundPlan.SourceId,
                            ClaimBillType = 0
                        });
                    }

                }
            }else {
                var virtualUsedPartsOutboundPlans =DomainService. 查询虚拟旧件出库计划(sourceCode, sourceBillTimeBegin, sourceBillTimeEnd, outboundType, outboundStatus, usedPartsWarehouseCode, usedPartsWarehouseName, personnelId, relatedCompanyName, businessCode);
                foreach(var virtualUsedPartsOutboundPlan in virtualUsedPartsOutboundPlans) {
                    if(virtualUsedPartsOutboundPlan.OutboundType == null)
                        continue;
                    list.Add(virtualUsedPartsOutboundPlan);
                    var virtualUsedPartsOutboundPlanDetails =DomainService. 查询虚拟旧件出库清单(virtualUsedPartsOutboundPlan.OutboundType.Value, virtualUsedPartsOutboundPlan.SourceId, true, Silverlight.Web.Utils.GetCurrentUserInfo().Id, virtualUsedPartsOutboundPlan.UsedPartsWarehouseId).ToList();
                    if(virtualUsedPartsOutboundPlanDetails.Any()) {
                        detailList.AddRange(virtualUsedPartsOutboundPlanDetails);
                    }else {
                        detailList.Add(new VirtualUsedPartsOutboundPlanDetail {
                            SourceId = virtualUsedPartsOutboundPlan.SourceId,
                            ClaimBillType = 0
                        });
                    }
                }
            }
            if(detailList.Any()) {
                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == detailList.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = detailList[index - 1];
                        var planBill = list.First(r => r.SourceId == detail.SourceId);
                        var values = new object[] {
                            planBill.SourceCode,
                            planBill.OutboundType.HasValue?Enum.GetName(typeof(DcsUsedPartsOutboundOrderOutboundType),planBill.OutboundType.Value):null,
                            planBill.OutboundStatus.HasValue?Enum.GetName(typeof(DcsUsedPartsOutboundStatus),planBill.OutboundStatus.Value):null,
                            planBill.UsedPartsWarehouseName,
                            planBill.RelatedCompanyCode,
                            planBill.RelatedCompanyName,
                            detail.UsedPartsBarCode,detail.UsedPartsCode,detail.UsedPartsName,detail.UsedPartsWarehouseAreaCode,
                            detail.PlannedAmount.HasValue?detail.PlannedAmount:null,
                            detail.ConfirmedAmount.HasValue?detail.ConfirmedAmount:null,
                            detail.OutboundAmount.HasValue?detail.OutboundAmount:null,
                            detail.SettlementPrice.HasValue?detail.SettlementPrice:null,
                            detail.CostPrice.HasValue?detail.CostPrice:null,
                            detail.ClaimBillType==0?"":Enum.GetName(typeof(DcsClaimBillType),detail.ClaimBillType),
                            detail.ClaimBillCode,detail.UsedPartsSupplierCode, detail.UsedPartsSupplierName, 
                            detail.IfFaultyParts.HasValue?(detail.IfFaultyParts.Value?"是":"否"):null,
                            detail.FaultyPartsCode,detail.FaultyPartsSupplierCode,detail.FaultyPartsSupplierName,detail.ResponsibleUnitCode,
                            planBill.Remark,
                            detail.Remark
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
    
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 15:15:50
        //原分布类的函数全部转移到Ach                                                               
        /// <summary>
        /// 导出虚拟旧件出库清单
        /// </summary>
        public string ExportVirtualUsedPartsOutboundPlanDetail(int? billId, int personnelId, string sourceCode, int? outboundType, int? outboundStatus, string usedPartsWarehouseCode, string usedPartsWarehouseName, DateTime? sourceBillTimeBegin, DateTime? sourceBillTimeEnd, string relatedCompanyName, string businessCode) {
            return new VirtualUsedPartsOutboundPlanDetailAch(this).ExportVirtualUsedPartsOutboundPlanDetail(billId,personnelId,sourceCode,outboundType,outboundStatus,usedPartsWarehouseCode,usedPartsWarehouseName,sourceBillTimeBegin,sourceBillTimeEnd,relatedCompanyName,businessCode);
        }
    }
}
