﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string ExprtCustomerServiceEvaluate(int[] ids, string repairClaimBillCode, string vIN, int? partsSalesCategoryId, int? marketingDepartmentId, string dealerCode, string dealerName, int? evaluationResults, string evaluateSource, DateTime? repairRequestTimeBegin, DateTime? repairRequestTimeEnd) {
            var exportDataFileName = GetExportFilePath(string.Format("客户服务评价_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            IQueryable<VirtualCustomerServiceEvaluate> query = this.GetCustomerServiceEvaluateDetails();
            if(ids != null && ids.Length > 0) {
                query = query.Where(r => ids.Contains(r.Id));
            } else {
                if(!string.IsNullOrEmpty(repairClaimBillCode)) {
                    query = query.Where(r => r.RepairClaimBillCode.IndexOf(repairClaimBillCode.ToUpper()) != -1);
                }
                if(!string.IsNullOrEmpty(vIN)) {
                    query = query.Where(r => r.VIN.IndexOf(vIN.ToUpper()) != -1);
                }
                if(!string.IsNullOrEmpty(dealerCode)) {
                    query = query.Where(r => r.DealerCode.IndexOf(dealerCode.ToUpper()) != -1);
                }
                if(!string.IsNullOrEmpty(dealerName)) {
                    query = query.Where(r => r.DealerName.IndexOf(dealerName.ToUpper()) != -1);
                }
                if(!string.IsNullOrEmpty(evaluateSource)) {
                    query = query.Where(r => r.EvaluateSource.IndexOf(evaluateSource.ToUpper()) != -1);
                }
                if(partsSalesCategoryId.HasValue) {
                    query = query.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId);
                }
                if(marketingDepartmentId.HasValue) {
                    query = query.Where(r => r.MarketingDepartmentId == marketingDepartmentId);
                }
                if(evaluationResults.HasValue) {
                    query = query.Where(r => r.EvaluationResults == evaluationResults);
                }
                if(repairRequestTimeBegin.HasValue) {
                    query = query.Where(r => r.RepairRequestTime >= repairRequestTimeBegin);
                }
                if(repairRequestTimeEnd.HasValue) {
                    query = query.Where(r => r.RepairRequestTime < repairRequestTimeEnd);
                }
            }
            var result = query.ToArray();
            var excelColumns = new List<string>();
            if(result.Any()) {
                excelColumns.Add("索赔单编号");
                excelColumns.Add("VIN");
                excelColumns.Add("品牌");
                excelColumns.Add("市场部");
                excelColumns.Add("服务站编号");
                excelColumns.Add("服务站名称");
                excelColumns.Add("维修类型");
                excelColumns.Add("评价来源");
                excelColumns.Add("评价结果");
                excelColumns.Add("评价分值");
                excelColumns.Add("不满意要素");
                excelColumns.Add("客户意见");
                excelColumns.Add("客户姓名");
                excelColumns.Add("联系电话");
                excelColumns.Add("车牌号");
                excelColumns.Add("报修时间");
                excelColumns.Add("评价时间");
                excelColumns.Add("创建人");
            }
            using(var excelExport = new ExcelExport(exportDataFileName)) {
                excelExport.ExportByRow(index => {
                    if(index == result.Count() + 1)
                        return null;
                    if(index == 0)
                        return excelColumns.ToArray<Object>();
                    var detail = result[index - 1];
                    var values = new object[] {
                                        detail.RepairClaimBillCode,
                                        detail.VIN,
                                        detail.PartsSalesCategoryName,
                                        detail.MarketingDepartmentName,
                                        detail.DealerCode,
                                        detail.DealerName,
                                        detail.RepairType==0?"":Enum.GetName(typeof(DcsRepairType),detail.RepairType),
                                        detail.EvaluateSource,
                                        detail.EvaluationResults==0?"":Enum.GetName(typeof(DcsCustomerServiceEvaluateResult),detail.EvaluationResults),
                                        detail.EvaluateGrade,
                                        detail.DisContentFactor.HasValue?Enum.GetName(typeof(DcsDisContentFactor),detail.DisContentFactor):"",
                                        detail.CustomOpinion,
                                        detail.VehicleContactPerson,
                                        detail.ContactPhone,
                                        detail.VehicleLicensePlate,
                                        detail.RepairRequestTime,
                                        detail.EvaluateTime,
                                        detail.CreatorName,
                        };
                    return values;
                });
            }
            return exportDataFileName;
        }

        public string ExportCustomerServiceEvaluateStatistics(int[] dealerIds, int[] partsSalesCategoryIds, int[] marketingDepartmentIds, DateTime? repairRequestTimeBegin, DateTime? repairRequestTimeEnd) {
            var exportDataFileName = GetExportFilePath(string.Format("客户服务评价统计_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            IQueryable<VirtualCustomerServiceEvaluate> query = this.GetCustomerServiceEvaluateStatisticsDetails(repairRequestTimeBegin, repairRequestTimeEnd);
            if(dealerIds != null && dealerIds.Length > 0) {
                query = query.Where(r => dealerIds.Any(x => x == r.DealerId) && partsSalesCategoryIds.Any(x => x == r.PartsSalesCategoryId) && marketingDepartmentIds.Any(x => x == r.MarketingDepartmentId));
            }
            var result = query.ToArray();
            var excelColumns = new List<string>();
            if(result.Any()) {
                excelColumns.Add("品牌");
                excelColumns.Add("市场部");
                excelColumns.Add("服务站编号");
                excelColumns.Add("服务站名称");
                excelColumns.Add("评价数量");
                excelColumns.Add("好评比率(%)");
                excelColumns.Add("中评比率(%)");
                excelColumns.Add("差评比率(%)");
                excelColumns.Add("维修技术水平比率(%)");
                excelColumns.Add("服务态度比率(%)");
                excelColumns.Add("服务及时性比率(%)");
                excelColumns.Add("服务站环境比率(%)");
            }
            using(var excelExport = new ExcelExport(exportDataFileName)) {
                excelExport.ExportByRow(index => {
                    if(index == result.Count() + 1)
                        return null;
                    if(index == 0)
                        return excelColumns.ToArray<Object>();
                    var detail = result[index - 1];
                    var values = new object[] {
                                     detail.PartsSalesCategoryName,
                                     detail.MarketingDepartmentName,
                                     detail.DealerCode,
                                     detail.DealerName,
                                     Math.Round( detail.EvaluationQuantity,2),
                                     Math.Round(  detail.Praise,2),
                                     Math.Round(  detail.InTheComments,2),
                                     Math.Round(  detail.Bad,2),
                                     Math.Round(  detail.RepairTechnology,2),
                                     Math.Round( detail.ServiceAttitude,2),
                                     Math.Round( detail.ServiceTime,2),
                                     Math.Round(  detail.ServiceHygiene,2),
                        };
                    return values;
                });
            }
            return exportDataFileName;
        }
    }
}
