﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
       
        public string 导出服务站索赔领料单( string claimCode, string dealerCode, string dealerName, string sparePartCode, string sparePartName, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? bSettleDate, DateTime? eSettleDate) {
            var exportDataFileName = GetExportFilePath(string.Format("服务站索赔领料单_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
           var userInfo = Utils.GetCurrentUserInfo();
            var company=ObjectContext.Companies.Where(t=>t.Id==userInfo.EnterpriseId).First();
            IQueryable<DealerClaimSparePart> query=ObjectContext.DealerClaimSpareParts;
                if(company.Type!=(int)DcsCompanyType.分公司){
					query = query.Where(r => r.DealerId==userInfo.EnterpriseId);
			    }
                if(!string.IsNullOrEmpty(claimCode)) {
                    query = query.Where(r => r.ClaimCode.IndexOf(claimCode.ToUpper()) != -1);
                }
                if(!string.IsNullOrEmpty(dealerCode)) {
                    query = query.Where(r => r.DealerCode.IndexOf(dealerCode.ToUpper()) != -1);
                }
                if(!string.IsNullOrEmpty(dealerName)) {
                    query = query.Where(r => r.DealerName.IndexOf(dealerName.ToUpper()) != -1);
                }
                if(!string.IsNullOrEmpty(sparePartCode)) {
                    query = query.Where(r => r.SparePartCode.IndexOf(sparePartCode.ToUpper()) != -1);
                }
                if(!string.IsNullOrEmpty(sparePartName)) {
                    query = query.Where(r => r.SparePartName.IndexOf(sparePartName.ToUpper()) != -1);
                }              
                if(createTimeBegin.HasValue) {
                    query = query.Where(r => r.CreateTime >= createTimeBegin);
                }
                if(createTimeEnd.HasValue) {
                    query = query.Where(r => r.CreateTime < createTimeEnd);
                }
                if(bSettleDate.HasValue) {
                    query = query.Where(r => r.SettleDate >= bSettleDate);
                }
                if(eSettleDate.HasValue) {
                    query = query.Where(r => r.SettleDate < eSettleDate);
                }
            var result = query.ToArray();
            var excelColumns = new List<string>();
            if(result.Any()) {
                excelColumns.Add("索赔领料单号");
                excelColumns.Add("索赔单状态");
                excelColumns.Add("服务站编号");
                excelColumns.Add("服务站名称");
                excelColumns.Add("配件编号");
                excelColumns.Add("配件名称");
                excelColumns.Add("配件基准价");
                excelColumns.Add("数量");
                excelColumns.Add("索赔单结算时间");
                excelColumns.Add("创建时间");               
            }
            using(var excelExport = new ExcelExport(exportDataFileName)) {
                excelExport.ExportByRow(index => {
                    if(index == result.Count() + 1)
                        return null;
                    if(index == 0)
                        return excelColumns.ToArray<Object>();
                    var detail = result[index - 1];
                    var values = new object[] {
                                        detail.ClaimCode,
                                        detail.ClaimStatus,
                                        detail.DealerCode,
                                        detail.DealerName,
                                        detail.SparePartCode,
                                        detail.SparePartName,
                                        detail.Price,
                                        detail.Quantity,
                                        detail.SettleDate,
                                        detail.CreateTime,
                                        
                        };
                    return values;
                });
            }
            return exportDataFileName;
        }
    }
}








