﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string ExportMarketAbQualityInformationForCountersign(int[] ids, int? branchId, int? dealerId, string vIN, int? status, string branchName, string dealerName, string dealerCode, string code, string serviceProductLineName, int? failureType, string drive, DateTime? createTimeStart, DateTime? createTimeEnd) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("市场AB质量信息快报_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var marketABQualityInformations = 查询市场AB质量信息快报();
            var marketingDepartmentAndSalesRegions = (from a in marketABQualityInformations
                                                      join b in ObjectContext.DealerMarketDptRelations on a.DealerId equals b.DealerId into t1
                                                      from c in t1.DefaultIfEmpty()
                                                      join d in ObjectContext.MarketingDepartments on new {
                                                          a.PartsSalesCategoryId,
                                                          c.MarketId
                                                      } equals new {
                                                          d.PartsSalesCategoryId,
                                                          MarketId = d.Id
                                                      } into t2
                                                      from e in t2.DefaultIfEmpty()
                                                      join f in ObjectContext.RegionMarketDptRelations on e.Id equals f.MarketDepartmentId into t3
                                                      from g in t3.DefaultIfEmpty()
                                                      join h in ObjectContext.SalesRegions on g.SalesRegionId equals h.Id into t4
                                                      from j in t4.DefaultIfEmpty()
                                                      where e.Status == (int)DcsBaseDataStatus.有效 && g.Status == (int)DcsBaseDataStatus.有效
                                                      select new {
                                                          a,
                                                          e,
                                                          j
                                                      }).ToArray();



            //市场部（根据服务站与市场部关系获取市场部.Id(市场部.配件销售类型Id=市场AB质量快报.配件销售类型id，服务站与市场部关系.服务站Id=市场Ab质量快报.服务站Id)）
            if(ids != null && ids.Any()) {
                marketABQualityInformations = marketABQualityInformations.Where(r => ids.Contains(r.Id));
            } else {
                if(branchId.HasValue) {
                    marketABQualityInformations = marketABQualityInformations.Where(r => r.BranchId == branchId);
                }
                if(dealerId.HasValue) {
                    marketABQualityInformations = marketABQualityInformations.Where(r => r.DealerId == dealerId);
                }
                if(!string.IsNullOrEmpty(vIN)) {
                    marketABQualityInformations = marketABQualityInformations.Where(r => r.VIN.Contains(vIN));
                }
                if(status.HasValue) {
                    marketABQualityInformations = marketABQualityInformations.Where(r => r.Status == status);
                }
                if(!string.IsNullOrEmpty(branchName)) {
                    marketABQualityInformations = marketABQualityInformations.Where(r => r.Branch.Name.Contains(branchName));
                }
                if(!string.IsNullOrEmpty(dealerName)) {
                    marketABQualityInformations = marketABQualityInformations.Where(r => r.Company.Name.Contains(dealerName));
                }
                if(!string.IsNullOrEmpty(dealerCode)) {
                    marketABQualityInformations = marketABQualityInformations.Where(r => r.Company.Code.Contains(dealerCode));
                }
                if(!string.IsNullOrEmpty(code)) {
                    marketABQualityInformations = marketABQualityInformations.Where(r => r.Code.Contains(code));
                }
                //if(!string.IsNullOrEmpty(serviceProductLineName)) {
                //    marketABQualityInformations = marketABQualityInformations.Where(r => r.ServiceProductLine.Name.Contains(serviceProductLineName));
                //}
                if(failureType.HasValue) {
                    marketABQualityInformations = marketABQualityInformations.Where(r => r.FailureType == failureType);
                }
                if(!string.IsNullOrEmpty(drive)) {
                    marketABQualityInformations = marketABQualityInformations.Where(r => r.Drive.Contains(drive));
                }
                if(createTimeStart.HasValue) {
                    marketABQualityInformations = marketABQualityInformations.Where(r => r.CreateTime >= createTimeStart);
                }
                if(createTimeEnd.HasValue) {
                    marketABQualityInformations = marketABQualityInformations.Where(r => r.CreateTime <= createTimeEnd);
                }
            }
            var marketABQualityInformationsArray = marketABQualityInformations.ToArray();
            if(marketABQualityInformationsArray.Any()) {
                excelColumns.Add("单据编号");
                excelColumns.Add("状态");
                excelColumns.Add("营销分公司");
                excelColumns.Add("品牌");
                excelColumns.Add("服务产品线");
                excelColumns.Add("服务站编号");
                excelColumns.Add("服务站名称");
                excelColumns.Add("市场部");
                excelColumns.Add("大区");
                excelColumns.Add("故障简述");
                excelColumns.Add("VIN");
                excelColumns.Add("车型号");
                excelColumns.Add("销售时间");
                excelColumns.Add("生产时间");
                excelColumns.Add("故障时间");
                excelColumns.Add("运行里程");
                excelColumns.Add("道路条件");
                excelColumns.Add("载货种类");
                excelColumns.Add("祸首件责任代码");
                excelColumns.Add("祸首件厂家名称");
                excelColumns.Add("故障件图号");
                excelColumns.Add("故障件名称");
                excelColumns.Add("故障属性");
                excelColumns.Add("故障台次");
                excelColumns.Add("故障车辆出厂编号明细");
                excelColumns.Add("版次");
                excelColumns.Add("系列");
                excelColumns.Add("驱动");
                excelColumns.Add("初步原因分析");
                excelColumns.Add("已处理措施");
                excelColumns.Add("信息员/电话");
                excelColumns.Add("初审时间");
                excelColumns.Add("终审时间");
                excelColumns.Add("审批意见历史");
                excelColumns.Add("备注");
                excelColumns.Add("故障详细描述");
                excelColumns.Add("故障地点");
                excelColumns.Add("故障数量");
                excelColumns.Add("故障性质");
                excelColumns.Add("信息级别");
                excelColumns.Add("站长/电话");
                excelColumns.Add("市场部服务管理员/电话");
                excelColumns.Add("载货重量");
                excelColumns.Add("故障里程");
                excelColumns.Add("批次号");
                excelColumns.Add("创建人");
                excelColumns.Add("创建时间");
                excelColumns.Add("修改人");
                excelColumns.Add("修改时间");
                excelColumns.Add("初审人");
                excelColumns.Add("初审时间");
                excelColumns.Add("终审人");
                excelColumns.Add("终审时间");
                excelColumns.Add("驳回人");
                excelColumns.Add("驳回时间");



                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == marketABQualityInformationsArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = marketABQualityInformationsArray[index - 1];
                        var marketingDepartmentAndSalesRegion = marketingDepartmentAndSalesRegions.FirstOrDefault(r => r.a.DealerId == detail.DealerId && r.a.PartsSalesCategoryId == detail.PartsSalesCategoryId);
                        var values = new object[] {
                            detail.Code,
                            Enum.GetName(typeof(DcsABQualityInformationStatus),detail.Status),
                            detail.Branch.Name,
                            detail.PartsSalesCategory.Name,
                            //detail.ServiceProductLine.Name,
                            detail.Company.Code,
                            detail.Company.Name, 
                      
                            marketingDepartmentAndSalesRegion!=null?marketingDepartmentAndSalesRegion.e.Name:"",
                            marketingDepartmentAndSalesRegion!=null?marketingDepartmentAndSalesRegion.j.RegionName:"",

                            detail.FaultDescription,
                            detail.VIN,
                            detail.ProductCategoryCode,
                            detail.SalesDate,
                            detail.OutOfFactoryDate,
                            detail.FaultDate,
                            detail.Mileage,
                            detail.RoadConditions,
                            detail.CargoTypes,
                            detail.FaultyPartsSupplierCode,
                            detail.FaultyPartsSupplierName,
                            detail.FaultyPartsCode,
                            detail.FaultyPartsName,
                            detail.FailureType,
                            detail.FailureTimes,
                            detail.FailureSerialNumberDetail,
                            detail.Revision,
                            detail.Series,
                            detail.Drive,
                            detail.FaultReason,
                            detail.HaveMeasures,
                            detail.InformationPhone,
                            detail.InitialApproverTime,
                            detail.ApproveTime,
                            detail.ApproveCommentHistory,
                            detail.Remark,
                            detail.FaultyDescription,
                            detail.FaultyLocation,
                            detail.FaultyQuantity,
                            detail.FaultyNature,
                            detail.MessageLevel,
                            detail.StationMasterPhone,
                            detail.ManagePhone,
                            detail.CarryWeight,
                            detail.FaultyMileage,
                            detail.BatchNumber,
                            detail.CreatorName,
                            detail.CreateTime,
                            detail.ModifierName,
                            detail.ModifyTime,
                            detail.InitialApproverName,
                            detail.InitialApproverTime,
                            detail.ApproverName,
                            detail.ApproveTime,
                            detail.RejectName,
                            detail.RejectTime
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
        public string ExportMarketABQualityInformation(int[] ids, int? branchId, int? dealerId, string vIN, int? status, string branchName, string dealerName, string dealerCode, string code, string serviceProductLineName, int? failureType, string drive, DateTime? createTimeStart, DateTime? createTimeEnd) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("市场AB质量信息快报_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var marketABQualityInformations = 查询市场AB质量信息快报();
            var marketingDepartmentAndSalesRegions = (from a in marketABQualityInformations
                                                      join b in ObjectContext.DealerMarketDptRelations on a.DealerId equals b.DealerId into t1
                                                      from c in t1.DefaultIfEmpty()
                                                      join d in ObjectContext.MarketingDepartments on new {
                                                          a.PartsSalesCategoryId,
                                                          c.MarketId
                                                      } equals new {
                                                          d.PartsSalesCategoryId,
                                                          MarketId = d.Id
                                                      } into t2
                                                      from e in t2.DefaultIfEmpty()
                                                      join f in ObjectContext.RegionMarketDptRelations on e.Id equals f.MarketDepartmentId into t3
                                                      from g in t3.DefaultIfEmpty()
                                                      join h in ObjectContext.SalesRegions on g.SalesRegionId equals h.Id into t4
                                                      from j in t4.DefaultIfEmpty()
                                                      where e.Status == (int)DcsBaseDataStatus.有效 && g.Status == (int)DcsBaseDataStatus.有效
                                                      select new {
                                                          a,
                                                          e,
                                                          j
                                                      }).ToArray();



            //市场部（根据服务站与市场部关系获取市场部.Id(市场部.配件销售类型Id=市场AB质量快报.配件销售类型id，服务站与市场部关系.服务站Id=市场Ab质量快报.服务站Id)）
            if(ids != null && ids.Any()) {
                marketABQualityInformations = marketABQualityInformations.Where(r => ids.Contains(r.Id));
            } else {
                if(branchId.HasValue) {
                    marketABQualityInformations = marketABQualityInformations.Where(r => r.BranchId == branchId);
                }
                if(dealerId.HasValue) {
                    marketABQualityInformations = marketABQualityInformations.Where(r => r.DealerId == dealerId);
                }
                if(!string.IsNullOrEmpty(vIN)) {
                    marketABQualityInformations = marketABQualityInformations.Where(r => r.VIN.Contains(vIN));
                }
                if(status.HasValue) {
                    marketABQualityInformations = marketABQualityInformations.Where(r => r.Status == status);
                }
                if(!string.IsNullOrEmpty(branchName)) {
                    marketABQualityInformations = marketABQualityInformations.Where(r => r.Branch.Name.Contains(branchName));
                }
                if(!string.IsNullOrEmpty(dealerName)) {
                    marketABQualityInformations = marketABQualityInformations.Where(r => r.Company.Name.Contains(dealerName));
                }
                if(!string.IsNullOrEmpty(dealerCode)) {
                    marketABQualityInformations = marketABQualityInformations.Where(r => r.Company.Code.Contains(dealerCode));
                }
                if(!string.IsNullOrEmpty(code)) {
                    marketABQualityInformations = marketABQualityInformations.Where(r => r.Code.Contains(code));
                }
                //if(!string.IsNullOrEmpty(serviceProductLineName)) {
                //    marketABQualityInformations = marketABQualityInformations.Where(r => r.ServiceProductLine.Name.Contains(serviceProductLineName));
                //}
                if(failureType.HasValue) {
                    marketABQualityInformations = marketABQualityInformations.Where(r => r.FailureType == failureType);
                }
                if(!string.IsNullOrEmpty(drive)) {
                    marketABQualityInformations = marketABQualityInformations.Where(r => r.Drive.Contains(drive));
                }
                if(createTimeStart.HasValue) {
                    marketABQualityInformations = marketABQualityInformations.Where(r => r.CreateTime >= createTimeStart);
                }
                if(createTimeEnd.HasValue) {
                    marketABQualityInformations = marketABQualityInformations.Where(r => r.CreateTime <= createTimeEnd);
                }
            }
            var marketABQualityInformationsArray = marketABQualityInformations.ToArray();
            if(marketABQualityInformationsArray.Any()) {
                excelColumns.Add("单据编号");
                excelColumns.Add("状态");
                excelColumns.Add("营销分公司");
                excelColumns.Add("品牌");
                excelColumns.Add("服务产品线");
                excelColumns.Add("服务站编号");
                excelColumns.Add("服务站名称");
                excelColumns.Add("市场部");
                excelColumns.Add("大区");
                excelColumns.Add("故障简述");
                excelColumns.Add("VIN");
                excelColumns.Add("车型号");
                excelColumns.Add("销售时间");
                excelColumns.Add("生产时间");
                excelColumns.Add("故障时间");
                excelColumns.Add("运行里程");
                excelColumns.Add("道路条件");
                excelColumns.Add("载货种类");
                excelColumns.Add("祸首件责任代码");
                excelColumns.Add("祸首件厂家名称");
                excelColumns.Add("故障件图号");
                excelColumns.Add("故障件名称");
                excelColumns.Add("故障属性");
                excelColumns.Add("故障台次");
                excelColumns.Add("故障车辆出厂编号明细");
                excelColumns.Add("版次");
                excelColumns.Add("系列");
                excelColumns.Add("驱动");
                excelColumns.Add("初步原因分析");
                excelColumns.Add("已处理措施");
                excelColumns.Add("信息员/电话");
                excelColumns.Add("初审时间");
                excelColumns.Add("终审时间");
                excelColumns.Add("审批意见历史");
                excelColumns.Add("备注");
                excelColumns.Add("创建人");
                excelColumns.Add("创建时间");
                excelColumns.Add("修改人");
                excelColumns.Add("修改时间");
                excelColumns.Add("初审人");
                excelColumns.Add("初审时间");
                excelColumns.Add("终审人");
                excelColumns.Add("终审时间");
                excelColumns.Add("驳回人");
                excelColumns.Add("驳回时间");



                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == marketABQualityInformationsArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = marketABQualityInformationsArray[index - 1];
                        var marketingDepartmentAndSalesRegion = marketingDepartmentAndSalesRegions.FirstOrDefault(r => r.a.DealerId == detail.DealerId && r.a.PartsSalesCategoryId == detail.PartsSalesCategoryId);
                        var values = new object[] {
                            detail.Code,
                            Enum.GetName(typeof(DcsABQualityInformationStatus),detail.Status),
                            detail.Branch.Name,
                            detail.PartsSalesCategory.Name,
                            //detail.ServiceProductLine.Name,
                            detail.Company.Code,
                            detail.Company.Name, 
                      
                            marketingDepartmentAndSalesRegion!=null?marketingDepartmentAndSalesRegion.e.Name:"",
                            marketingDepartmentAndSalesRegion!=null?marketingDepartmentAndSalesRegion.j.RegionName:"",

                            detail.FaultDescription,
                            detail.VIN,
                            detail.ProductCategoryCode,
                            detail.SalesDate,
                            detail.OutOfFactoryDate,
                            detail.FaultDate,
                            detail.Mileage,
                            detail.RoadConditions,
                            detail.CargoTypes,
                            detail.FaultyPartsSupplierCode,
                            detail.FaultyPartsSupplierName,
                            detail.FaultyPartsCode,
                            detail.FaultyPartsName,
                            detail.FailureType,
                            detail.FailureTimes,
                            detail.FailureSerialNumberDetail,
                            detail.Revision,
                            detail.Series,
                            detail.Drive,
                            detail.FaultReason,
                            detail.HaveMeasures,
                            detail.InformationPhone,
                            detail.InitialApproverTime,
                            detail.ApproveTime,
                            detail.ApproveCommentHistory,
                            detail.Remark,
                            detail.CreatorName,
                            detail.CreateTime,
                            detail.ModifierName,
                            detail.ModifyTime,
                            detail.InitialApproverName,
                            detail.InitialApproverTime,
                            detail.ApproverName,
                            detail.ApproveTime,
                            detail.RejectName,
                            detail.RejectTime
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }

        public string ExportMarketABQualityInformationManagement(int[] ids, int? branchId, int? dealerId, string vIN, int? status, string branchName, string dealerName, string dealerCode, string code, string serviceProductLineName, int? failureType, string drive, DateTime? createTimeStart, DateTime? createTimeEnd, int? repairObjectId, string engModel) {
            var excelColumns = new List<string>();
            List<VirtualMarketABQualityInformation> AA = new List<VirtualMarketABQualityInformation>();
            var exportDataFileName = GetExportFilePath(string.Format("市场AB质量信息快报_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var marketABQualityInformations = GetMarketABQualityInformationDetails();
            var marketingDepartmentAndSalesRegions = (from a in marketABQualityInformations
                                                      join b in ObjectContext.DealerMarketDptRelations on a.DealerId equals b.DealerId into t1
                                                      from c in t1.DefaultIfEmpty()
                                                      join d in ObjectContext.MarketingDepartments on new {
                                                          a.PartsSalesCategoryId,
                                                          c.MarketId
                                                      } equals new {
                                                          d.PartsSalesCategoryId,
                                                          MarketId = d.Id
                                                      } into t2
                                                      from e in t2.DefaultIfEmpty()
                                                      join f in ObjectContext.RegionMarketDptRelations on e.Id equals f.MarketDepartmentId into t3
                                                      from g in t3.DefaultIfEmpty()
                                                      join h in ObjectContext.SalesRegions on g.SalesRegionId equals h.Id into t4
                                                      from j in t4.DefaultIfEmpty()
                                                      where e.Status == (int)DcsBaseDataStatus.有效 && g.Status == (int)DcsBaseDataStatus.有效
                                                      select new {
                                                          a,
                                                          e,
                                                          j
                                                      }).ToArray();



            //市场部（根据服务站与市场部关系获取市场部.Id(市场部.配件销售类型Id=市场AB质量快报.配件销售类型id，服务站与市场部关系.服务站Id=市场Ab质量快报.服务站Id)）
            if(ids != null && ids.Any()) {
                marketABQualityInformations = marketABQualityInformations.Where(r => ids.Contains(r.Id));
            } else {
                if(branchId.HasValue) {
                    marketABQualityInformations = marketABQualityInformations.Where(r => r.BranchId == branchId);
                }
                if(dealerId.HasValue) {
                    marketABQualityInformations = marketABQualityInformations.Where(r => r.DealerId == dealerId);
                }
                if(!string.IsNullOrEmpty(vIN)) {
                    marketABQualityInformations = marketABQualityInformations.Where(r => r.VIN.Contains(vIN));
                }
                if(status.HasValue) {
                    marketABQualityInformations = marketABQualityInformations.Where(r => r.Status == status);
                }
                if(!string.IsNullOrEmpty(branchName)) {
                    marketABQualityInformations = marketABQualityInformations.Where(r => r.Branch.Name.Contains(branchName));
                }
                if(!string.IsNullOrEmpty(dealerName)) {
                    marketABQualityInformations = marketABQualityInformations.Where(r => r.Company.Name.Contains(dealerName));
                }
                if(!string.IsNullOrEmpty(dealerCode)) {
                    marketABQualityInformations = marketABQualityInformations.Where(r => r.Company.Code.Contains(dealerCode));
                }
                if(!string.IsNullOrEmpty(code)) {
                    marketABQualityInformations = marketABQualityInformations.Where(r => r.Code.Contains(code));
                }
                if(!string.IsNullOrEmpty(serviceProductLineName)) {
                    marketABQualityInformations = marketABQualityInformations.Where(r => r.ServiceProductLineView.ProductLineName.Contains(serviceProductLineName));
                }
                if(failureType.HasValue) {
                    marketABQualityInformations = marketABQualityInformations.Where(r => r.FailureType == failureType);
                }
                if(!string.IsNullOrEmpty(drive)) {
                    marketABQualityInformations = marketABQualityInformations.Where(r => r.Drive.Contains(drive));
                }
                if(createTimeStart.HasValue) {
                    marketABQualityInformations = marketABQualityInformations.Where(r => r.CreateTime >= createTimeStart);
                }
                if(createTimeEnd.HasValue) {
                    marketABQualityInformations = marketABQualityInformations.Where(r => r.CreateTime <= createTimeEnd);
                }
                //if(repairObjectId.HasValue)
                //    marketABQualityInformations = marketABQualityInformations.Where(r => r.RepairObject.Id == repairObjectId);
                //if(!string.IsNullOrEmpty(engModel))
                //    marketABQualityInformations = marketABQualityInformations.Where(r => r.VehicleInformation.EngineModel.Contains(engModel));
            }
            int Id = 0;

            foreach(var item in marketABQualityInformations) {
                if(item.Status == (int)DcsABQualityInformationStatus.会签中 || item.Status == (int)DcsABQualityInformationStatus.会签完成) {
                    foreach(var items in item.Countersignatures) {
                        foreach(var item2 in items.CountersignatureDetails) {
                            var newItem = new VirtualMarketABQualityInformation();
                            newItem.Id = Id++;
                            newItem.Code = item.Code;
                            newItem.CompanyName = item.Company.Name;
                            newItem.FaultDescription = item.FaultDescription;
                            newItem.CreateTime = item.CreateTime;
                            newItem.FailureTimes = item.FailureTimes;
                            newItem.BranchName = item.Branch.Name;
                            newItem.PartsSalesCategoryName = item.PartsSalesCategory.Name;
                            newItem.Status = item.Status;
                            newItem.Drive = item.Drive;
                            newItem.ProductCategoryCode = item.ProductCategoryCode;
                            newItem.VIN = item.VIN;
                            //newItem.EngineModel = item.VehicleInformation.EngineModel;
                            newItem.InformationPhone = item.InformationPhone;
                            newItem.FaultyPartsName = item.FaultyPartsName;
                            newItem.FailureMode = item.FailureMode;
                            newItem.FaultReason = item.FaultReason;
                            newItem.Mileage = item.Mileage;
                            newItem.Revision = item.Revision;
                            newItem.ServiceProductLineName = item.ServiceProductLineView.ProductLineName;
                            newItem.Emission = item.Emission;
                            newItem.FaultyPartsSupplierCode = item.FaultyPartsSupplierCode;
                            newItem.FaultyPartsSupplierName = item.FaultyPartsSupplierName;
                            newItem.FaultyPartsCode = item.FaultyPartsCode;
                            newItem.FailureType = item.FailureType;
                            newItem.FailureSerialNumberDetail = item.FailureSerialNumberDetail;
                            newItem.HaveMeasures = item.HaveMeasures;
                            newItem.SalesDate = item.SalesDate;
                            newItem.OutOfFactoryDate = item.OutOfFactoryDate;
                            newItem.FaultDate = item.FaultDate;
                            newItem.RoadConditions = item.RoadConditions;
                            newItem.CargoTypes = item.CargoTypes;
                            newItem.CompanyCode = item.Company.Code;
                            newItem.ApproveCommentHistory = item.ApproveCommentHistory;
                            newItem.Remark = item.Remark;
                            newItem.FaultyDescription = item.FaultyDescription;
                            newItem.FaultyLocation = item.FaultyLocation;
                            newItem.FaultyQuantity = item.FaultyQuantity;
                            newItem.FaultyNature = item.FaultyNature;
                            newItem.MessageLevel = item.MessageLevel;
                            newItem.StationMasterPhone = item.StationMasterPhone;
                            newItem.ManagePhone = item.ManagePhone;
                            newItem.CarryWeight = item.CarryWeight;
                            newItem.FaultyMileage = item.FaultyMileage;
                            newItem.BatchNumber = item.BatchNumber;
                            newItem.CreatorName = item.CreatorName;
                            newItem.ModifierName = item.ModifierName;
                            newItem.ModifyTime = item.ModifyTime;
                            newItem.InitialApproverName = item.InitialApproverName;
                            newItem.InitialApproverTime = item.InitialApproverTime;
                            newItem.ApproverName = item.ApproverName;
                            newItem.ApproveTime = item.ApproveTime;
                            newItem.RejectName = item.RejectName;
                            newItem.RejectTime = item.RejectTime;
                            newItem.CountersignatureCreatorName = item2.Approver;
                            //newItem.CountersignatureCreateTime = items.CreateTime;
                            newItem.DealerId = item.DealerId;
                            newItem.PartsSalesCategoryId = item.PartsSalesCategoryId;
                            //newItem.RepairTarget = item.RepairObject.RepairTarget;
                            AA.Add(newItem);
                        }
                    }
                } else {
                    var newItem = new VirtualMarketABQualityInformation();
                    newItem.Id = Id++;
                    newItem.Code = item.Code;
                    newItem.CompanyName = item.Company.Name;
                    newItem.FaultDescription = item.FaultDescription;
                    newItem.CreateTime = item.CreateTime;
                    newItem.FailureTimes = item.FailureTimes;
                    newItem.BranchName = item.Branch.Name;
                    newItem.PartsSalesCategoryName = item.PartsSalesCategory.Name;
                    newItem.Status = item.Status;
                    newItem.Drive = item.Drive;
                    newItem.ProductCategoryCode = item.ProductCategoryCode;
                    newItem.VIN = item.VIN;
                    //newItem.EngineModel = item.VehicleInformation.EngineModel;
                    newItem.InformationPhone = item.InformationPhone;
                    newItem.FaultyPartsName = item.FaultyPartsName;
                    newItem.FailureMode = item.FailureMode;
                    newItem.FaultReason = item.FaultReason;
                    newItem.Mileage = item.Mileage;
                    newItem.Revision = item.Revision;
                    newItem.ServiceProductLineName = item.ServiceProductLineView.ProductLineName;
                    newItem.Emission = item.Emission;
                    newItem.FaultyPartsSupplierCode = item.FaultyPartsSupplierCode;
                    newItem.FaultyPartsSupplierName = item.FaultyPartsSupplierName;
                    newItem.FaultyPartsCode = item.FaultyPartsCode;
                    newItem.FailureType = item.FailureType;
                    newItem.FailureSerialNumberDetail = item.FailureSerialNumberDetail;
                    newItem.HaveMeasures = item.HaveMeasures;
                    newItem.SalesDate = item.SalesDate;
                    newItem.OutOfFactoryDate = item.OutOfFactoryDate;
                    newItem.FaultDate = item.FaultDate;
                    newItem.RoadConditions = item.RoadConditions;
                    newItem.CargoTypes = item.CargoTypes;
                    newItem.CompanyCode = item.Company.Code;
                    newItem.ApproveCommentHistory = item.ApproveCommentHistory;
                    newItem.Remark = item.Remark;
                    newItem.FaultyDescription = item.FaultyDescription;
                    newItem.FaultyLocation = item.FaultyLocation;
                    newItem.FaultyQuantity = item.FaultyQuantity;
                    newItem.FaultyNature = item.FaultyNature;
                    newItem.MessageLevel = item.MessageLevel;
                    newItem.StationMasterPhone = item.StationMasterPhone;
                    newItem.ManagePhone = item.ManagePhone;
                    newItem.CarryWeight = item.CarryWeight;
                    newItem.FaultyMileage = item.FaultyMileage;
                    newItem.BatchNumber = item.BatchNumber;
                    newItem.CreatorName = item.CreatorName;
                    newItem.ModifierName = item.ModifierName;
                    newItem.ModifyTime = item.ModifyTime;
                    newItem.InitialApproverName = item.InitialApproverName;
                    newItem.InitialApproverTime = item.InitialApproverTime;
                    newItem.ApproverName = item.ApproverName;
                    newItem.ApproveTime = item.ApproveTime;
                    newItem.RejectName = item.RejectName;
                    newItem.RejectTime = item.RejectTime;
                    newItem.CountersignatureCreatorName = null;
                    //newItem.CountersignatureCreateTime = items.CreateTime;
                    newItem.DealerId = item.DealerId;
                    newItem.PartsSalesCategoryId = item.PartsSalesCategoryId;
                    //newItem.RepairTarget = item.RepairObject.RepairTarget;
                    AA.Add(newItem);
                }
            }
            var marketABQualityInformationsArray = AA.ToArray();
            if(marketABQualityInformationsArray.Any()) {
                excelColumns.Add("单据编号");
                excelColumns.Add("服务站名称");
                excelColumns.Add("故障简述");
                excelColumns.Add("创建时间");
                excelColumns.Add("故障台次");
                excelColumns.Add("营销分公司");
                excelColumns.Add("品牌");
                excelColumns.Add("状态");
                excelColumns.Add("驱动");
                excelColumns.Add("车型号");
                excelColumns.Add("维修对象");
                excelColumns.Add("发动机型号");
                excelColumns.Add("VIN");
                excelColumns.Add("信息员/电话");
                excelColumns.Add("故障件名称");
                excelColumns.Add("故障模式");
                excelColumns.Add("初步原因分析");
                excelColumns.Add("运行里程");
                excelColumns.Add("版次");
                excelColumns.Add("服务产品线");
                excelColumns.Add("排放");
                excelColumns.Add("祸首件责任代码");
                excelColumns.Add("祸首件厂家名称");
                excelColumns.Add("故障件图号");
                excelColumns.Add("故障属性");
                excelColumns.Add("故障车辆出厂编号明细");
                excelColumns.Add("已处理措施");
                excelColumns.Add("销售时间");
                excelColumns.Add("生产时间");
                excelColumns.Add("故障时间");
                excelColumns.Add("道路条件");
                excelColumns.Add("载货种类");
                excelColumns.Add("服务站编号");
                excelColumns.Add("市场部");
                excelColumns.Add("大区");
                excelColumns.Add("初审时间");
                excelColumns.Add("终审时间");
                excelColumns.Add("审批意见历史");
                excelColumns.Add("备注");

                excelColumns.Add("故障详细描述");
                excelColumns.Add("故障地点");
                excelColumns.Add("故障数量");
                excelColumns.Add("故障性质");
                excelColumns.Add("信息级别");
                excelColumns.Add("站长/电话");
                excelColumns.Add("市场部服务管理员/电话");
                excelColumns.Add("载货重量");
                excelColumns.Add("故障里程");
                excelColumns.Add("批次号");

                excelColumns.Add("创建人");
                excelColumns.Add("修改人");
                excelColumns.Add("修改时间");
                excelColumns.Add("初审人");
                excelColumns.Add("初审时间");
                excelColumns.Add("终审人");
                excelColumns.Add("终审时间");
                excelColumns.Add("驳回人");
                excelColumns.Add("驳回时间");
                excelColumns.Add("会签人");
                //excelColumns.Add("会签时间");
                object[] values = null;
                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == marketABQualityInformationsArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = marketABQualityInformationsArray[index - 1];
                        var marketingDepartmentAndSalesRegion = marketingDepartmentAndSalesRegions.FirstOrDefault(r => r.a.DealerId == detail.DealerId && r.a.PartsSalesCategoryId == detail.PartsSalesCategoryId);
                        values = new object[] {
                            detail.Code,
                            detail.CompanyName,
                            detail.FaultDescription,
                            detail.CreateTime,
                            detail.FailureTimes,
                            detail.BranchName,
                            detail.PartsSalesCategoryName,
                            Enum.GetName(typeof(DcsABQualityInformationStatus),detail.Status),
                            detail.Drive,
                            detail.ProductCategoryCode,
                            detail.RepairTarget,
                            detail.EngineModel,
                            detail.VIN,                            
                            detail.InformationPhone,
                            detail.FaultyPartsName,
                            detail.FailureMode,
                            detail.FaultReason,
                            detail.Mileage,
                            detail.Revision,
                            detail.ServiceProductLineName,
                            detail.Emission,
                            detail.FaultyPartsSupplierCode,
                            detail.FaultyPartsSupplierName,
                            detail.FaultyPartsCode,
                            detail.FailureType,
                            detail.FailureSerialNumberDetail,
                            detail.HaveMeasures,
                            detail.SalesDate,
                            detail.OutOfFactoryDate,
                            detail.FaultDate,
                            detail.RoadConditions,
                            detail.CargoTypes,
                            detail.CompanyCode,
                            marketingDepartmentAndSalesRegion!=null?marketingDepartmentAndSalesRegion.e.Name:"",
                            marketingDepartmentAndSalesRegion!=null?marketingDepartmentAndSalesRegion.j.RegionName:"",
                            detail.InitialApproverTime,
                            detail.ApproveTime,
                            detail.ApproveCommentHistory,
                            detail.Remark,
                            detail.FaultyDescription,
                            detail.FaultyLocation,
                            detail.FaultyQuantity,
                            detail.FaultyNature,
                            detail.MessageLevel,
                            detail.StationMasterPhone,
                            detail.ManagePhone,
                            detail.CarryWeight,
                            detail.FaultyMileage,
                            detail.BatchNumber,
                            detail.CreatorName,
                            detail.ModifierName,
                            detail.ModifyTime,
                            detail.ApproverName,
                            detail.ApproveTime,
                            detail.InitialApproverName,
                            detail.InitialApproverTime,
                            detail.RejectName,
                            detail.RejectTime,
                            detail.CountersignatureCreatorName
                            //detail.CountersignatureCreateTime
                            };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
    }
}
