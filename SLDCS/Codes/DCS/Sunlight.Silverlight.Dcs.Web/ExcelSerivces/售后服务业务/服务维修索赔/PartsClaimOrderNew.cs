﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web {

    partial class DcsDomainService {
        /// <summary>
        /// 导出配件索赔单新
        /// </summary>
        /// <param name="ids">选中单据id</param>
        /// <param name="returnCompanyId">提报企业Id</param>
        /// <param name="rWarehouseCompanyId">退货仓库隶属企业id</param>
        /// <param name="branchId">分公司Id</param>
        /// <param name="partsSalesCategoryId">品牌</param>
        /// <param name="partsSalesOrderCode">销售订单编号</param>
        /// <param name="repairOrderCode">维修单编号</param>
        /// <param name="bussinessCode">业务编号</param>
        /// <param name="bussinessName">业务名称</param>
        /// <param name="cDCReturnWarehouseId">cdc退货仓库</param>
        /// <param name="cDCReturnWarehouseName">cdc退货仓库名称</param>
        /// <param name="faultyPartsCode">配件图号</param>
        /// <param name="faultyPartsName">配件名称</param>
        /// <param name="faultyPartsSupplierCode">供应商编号</param>
        /// <param name="faultyPartsSupplierName">供应商名称</param>
        /// <param name="status">状态</param>
        /// <param name="createTimeBegin">创建时间</param>
        /// <param name="createTimeEnd"></param>
        /// <param name="approveTimeBegin">审核时间</param>
        /// <param name="approveTimeEnd"></param>
        /// <returns></returns>
        public string ExportPartsClaimOrderNew(int[] ids,
            int? returnCompanyId, int? rWarehouseCompanyId, int? branchId, int? partsSalesCategoryId, string partsSalesOrderCode, string repairOrderCode, string bussinessCode, string bussinessName, int? cDCReturnWarehouseId, string cDCReturnWarehouseName,
            string faultyPartsCode, string faultyPartsName, string faultyPartsSupplierCode, string faultyPartsSupplierName, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? approveTimeBegin, DateTime? approveTimeEnd, string returnCompanyName, string code, string partsRetailOrderCode,string centerPartsSalesOrderCode) {
            var exportDataFileName = GetExportFilePath(string.Format("配件索赔单_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            IQueryable<PartsClaimOrderNewWithOtherInfo> query;
            if(ids != null && ids.Length > 0) {
                query = 查询配件索赔单含业务编码(null, null);
                query = query.Where(r => ids.Contains(r.Id));
            } else {
                query = 查询配件索赔单含业务编码(bussinessCode, bussinessName);
                if(returnCompanyId.HasValue) {
                    query = query.Where(r => r.ReturnCompanyId == returnCompanyId);
                }
                if(rWarehouseCompanyId.HasValue) {
                    query = query.Where(r => r.RWarehouseCompanyId == rWarehouseCompanyId);
                }
                if(branchId.HasValue) {
                    query = query.Where(r => r.BranchId == branchId);
                }
                if(partsSalesCategoryId.HasValue) {
                    query = query.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId);
                }
                if(!string.IsNullOrEmpty(partsSalesOrderCode)) {
                    query = query.Where(r => r.PartsSalesOrderCode.IndexOf(partsSalesOrderCode.ToUpper()) != -1);
                }
                if(!string.IsNullOrEmpty(returnCompanyName)) {
                    query = query.Where(r => r.ReturnCompanyName.IndexOf(returnCompanyName.ToUpper()) != -1);
                }
                if(!string.IsNullOrEmpty(code)) {
                    query = query.Where(r => r.Code.IndexOf(code.ToUpper()) != -1);
                }
                if(!string.IsNullOrEmpty(partsRetailOrderCode)) {
                    query = query.Where(r => r.PartsRetailOrderCode.IndexOf(partsRetailOrderCode.ToUpper()) != -1);
                }
                if(!string.IsNullOrEmpty(repairOrderCode)) {
                    query = query.Where(r => r.RepairOrderCode.IndexOf(repairOrderCode.ToUpper()) != -1);
                }

                if(cDCReturnWarehouseId.HasValue) {
                    query = query.Where(r => r.CDCReturnWarehouseId == cDCReturnWarehouseId);
                }
                if(!string.IsNullOrEmpty(cDCReturnWarehouseName)) {
                    query = query.Where(r => r.CDCReturnWarehouseName.IndexOf(cDCReturnWarehouseName.ToUpper()) != -1);
                }
                if(!string.IsNullOrEmpty(faultyPartsCode)) {
                    query = query.Where(r => r.FaultyPartsCode.IndexOf(faultyPartsCode.ToUpper()) != -1);
                }
                if(!string.IsNullOrEmpty(faultyPartsName)) {
                    query = query.Where(r => r.FaultyPartsName.IndexOf(faultyPartsName.ToUpper()) != -1);
                }
                if(!string.IsNullOrEmpty(faultyPartsSupplierCode)) {
                    query = query.Where(r => r.FaultyPartsSupplierCode.IndexOf(faultyPartsSupplierCode.ToUpper()) != -1);
                }
                if(!string.IsNullOrEmpty(faultyPartsSupplierName)) {
                    query = query.Where(r => r.FaultyPartsSupplierName.IndexOf(faultyPartsSupplierName.ToUpper()) != -1);
                }
                if(status.HasValue) {
                    query = query.Where(r => r.Status == status);
                }
                if(createTimeBegin.HasValue) {
                    query = query.Where(r => r.CreateTime >= createTimeBegin);
                }
                if(createTimeEnd.HasValue) {
                    query = query.Where(r => r.CreateTime < createTimeEnd);
                }
                if(approveTimeBegin.HasValue) {
                    query = query.Where(r => r.ApproveTime >= approveTimeBegin);
                }
                if(approveTimeEnd.HasValue) {
                    query = query.Where(r => r.ApproveTime < approveTimeEnd);
                }
                if(!string.IsNullOrEmpty(centerPartsSalesOrderCode)) {
                    query = query.Where(r => r.CenterPartsSalesOrderCode.IndexOf(centerPartsSalesOrderCode.ToUpper()) != -1);
                }
            }
            var result = query.ToArray();
            var excelColumns = new List<string>();
            if(result.Any()) {
                excelColumns.Add("配件索赔单编号");
                excelColumns.Add("品牌");
                excelColumns.Add("退货企业");
                excelColumns.Add("销售订单编号");
                excelColumns.Add("零售订单编号");
                excelColumns.Add("中心库销售订单编号");
                //excelColumns.Add("维修单号");
                excelColumns.Add("退货仓库编号");
                excelColumns.Add("退货仓库名称");
                excelColumns.Add("退货仓库隶属企业编号");
                excelColumns.Add("退货仓库隶属企业名称");
                excelColumns.Add("销售出库仓库编号");
                excelColumns.Add("销售出库仓库名称");
                excelColumns.Add("客户联系人");
                excelColumns.Add("客户联系电话");
                excelColumns.Add("配件图号");
                excelColumns.Add("配件名称");
                excelColumns.Add("供应商编号");
                excelColumns.Add("配件保修期");
                excelColumns.Add("报修时间");
                excelColumns.Add("配件销售时间");
                excelColumns.Add("配件销售时长");
                excelColumns.Add("是否超保");
                excelColumns.Add("材料费");
                excelColumns.Add("材料管理费");
                //excelColumns.Add("工时费");
                //excelColumns.Add("其他费用");
                excelColumns.Add("费用合计");
                excelColumns.Add("故障现象");
                excelColumns.Add("备注");
                excelColumns.Add("审批意见");
                excelColumns.Add("RDC入库计划单号");
                excelColumns.Add("RDC入库计划单状态");
                excelColumns.Add("RDC收货情况");
                //excelColumns.Add("CDC退货仓库");
                //excelColumns.Add("CDC入库计划单号");
                //excelColumns.Add("CDC入库计划单状态");
                excelColumns.Add("发运单号");
                excelColumns.Add("状态");
                excelColumns.Add("驳回状态");
                excelColumns.Add("驳回意见");
                excelColumns.Add("退货原因");
                excelColumns.Add("作废原因");
                excelColumns.Add("终止原因");
                excelColumns.Add("驳回次数");
                excelColumns.Add("创建人");
                excelColumns.Add("创建时间");
                excelColumns.Add("修改人");
                excelColumns.Add("修改时间");
                excelColumns.Add("审核人");
                excelColumns.Add("审核时间");
                excelColumns.Add("驳回人");
                excelColumns.Add("驳回时间");
                excelColumns.Add("终止人");
                excelColumns.Add("终止时间");
                excelColumns.Add("作废人 ");
                excelColumns.Add("作废时间");
            }
            using(var excelExport = new ExcelExport(exportDataFileName)) {
                excelExport.ExportByRow(index => {
                    if(index == result.Count() + 1)
                        return null;
                    if(index == 0)
                        return excelColumns.ToArray<Object>();
                    var detail = result[index - 1];
                    var values = new object[] {
                                        detail.Code,//配件索赔单编号
                                        detail.PartsSalesCategoryName,//品牌
                                        detail.ReturnCompanyName,//退货企业
                                        detail.PartsSalesOrderCode,//销售订单编号
                                        detail.PartsRetailOrderCode,//零售订单编号
                                        //detail.RepairOrderCode,//维修单号
                                        detail.CenterPartsSalesOrderCode,//中心库销售订单编号
                                        detail.ReturnWarehouseCode,//退货仓库编号
                                        detail.ReturnWarehouseName,//退货仓库名称
                                        detail.RWarehouseCompanyCode,//退货仓库隶属企业编号
                                        detail.RWarehouseCompanyName,//退货仓库隶属企业名称
                                        detail.SalesWarehouseCode,//销售出库仓库编号
                                        detail.SalesWarehouseName,//销售出库仓库名称
                                        detail.CustomerContactPerson,//客户联系人
                                        detail.CustomerContactPhone,//客户联系电话
                                        detail.FaultyPartsCode,//配件图号
                                        detail.FaultyPartsName,//配件名称
                                        detail.FaultyPartsSupplierCode,//供应商编号
                                        detail.PartsWarrantyLong,//配件保修期
                                        detail.RepairRequestTime,//报修时间
                                        detail.PartsSaleTime,//配件销售时间
                                        detail.PartsSaleLong,//配件销售时长
                                        detail.IsOutWarranty,//是否超保
                                        detail.MaterialCost,//材料费
                                        detail.MaterialManagementCost,//材料管理费
                                        //detail.LaborCost,//工时费
                                        //detail.OtherCost,//其他费用
                                        detail.TotalAmount,//费用合计
                                        detail.MalfunctionDescription,//故障现象
                                        detail.Remark,//备注
                                        detail.ApproveComment,//审批意见
                                        detail.RDCPartsInboundPlanCode,//RDC入库计划单号
                                        detail.RDCPartsInboundPlanStauts==0?"":Enum.GetName(typeof(DcsPartsInboundPlanStatus),detail.RDCPartsInboundPlanStauts),//RDC入库计划单状态//////////////////
                                        detail.RDCReceiptSituation,//RDC收货情况
                                        //detail.CDCReturnWarehouseName,//CDC退货仓库
                                        //detail.RDCPartsInboundPlanCode,//CDC入库计划单号
                                        //detail.RDCPartsInboundPlanStauts==0?"":Enum.GetName(typeof(DcsPartsInboundPlanStatus),detail.RDCPartsInboundPlanStauts),//CDC入库计划单状态//////////////////
                                        detail.ShippingCode,//发运单号
                                        detail.Status==0?"":Enum.GetName(typeof(DcsPartsClaimBillStatusNew),detail.Status),//状态////////////////////////////////////////////
                                        detail.RejectStatus==null?"":Enum.GetName(typeof(DcsRejectStatus),detail.RejectStatus.Value),//驳回状态///////////////////////////////
                                        detail.RejectReason,//驳回意见
                                        detail.ReturnReason,
                                        detail.AbandonerReason,
                                        detail.StopReason,
                                        detail.RejectQty,//驳回次数                                 
                                        detail.CreatorName,//创建人
                                        detail.CreateTime,//创建时间
                                        detail.ModifierName,//修改人
                                        detail.ModifyTime,//修改时间
                                        detail.ApproverName,//审批人
                                        detail.ApproveTime,//审批时间
                                        detail.RejectName,//驳回人
                                        detail.RejectTime,//驳回时间
                                        detail.CancelName,//终止人
                                        detail.CancelTime,//终止时间
                                        detail.AbandonerName,//作废人
                                        detail.AbandonTime//作废时间
                        };
                    return values;
                });
            }
            return exportDataFileName;



















        }
    }
}
