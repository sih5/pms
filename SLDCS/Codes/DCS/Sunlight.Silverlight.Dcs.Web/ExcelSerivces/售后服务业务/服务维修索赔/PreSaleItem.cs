﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web
{

    partial class DcsDomainService
    {

        public string ExportPreSaleItem(int[] ids, string category, int? status, int? partsSalesCategoryId, int? serviceProductLineId)
        {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("售前检查项目_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var userinfo = Utils.GetCurrentUserInfo();

            var PreSaleItems = GetPreSaleItemWithPartsSalesCategory();

            PreSaleItems = PreSaleItems.Where(r => r.BranchId == userinfo.EnterpriseId);

            if (ids != null && ids.Any()) {
                PreSaleItems = PreSaleItems.Where(r => ids.Contains(r.Id));
            }
            else {
                if (!string.IsNullOrEmpty(category)) {
                    PreSaleItems = PreSaleItems.Where(r => r.Category.Contains(category));
                }
                if (status.HasValue) {
                    PreSaleItems = PreSaleItems.Where(r => r.Status == status);
                }
                if (partsSalesCategoryId.HasValue) {
                    PreSaleItems = PreSaleItems.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId);
                }
                if (serviceProductLineId.HasValue) {
                    PreSaleItems = PreSaleItems.Where(r => r.ServiceProductLineId == serviceProductLineId);
                }
            }

            var PreSaleItemsArray = PreSaleItems.ToArray();
            if (PreSaleItemsArray.Any()) {
                excelColumns.Add("营销分公司");
                excelColumns.Add("品牌");
                excelColumns.Add("服务产品线");
                excelColumns.Add("类别");
                excelColumns.Add("检查项目");
                excelColumns.Add("故障模式");
                excelColumns.Add("状态");
                using (var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index =>
                    {
                        if (index == PreSaleItemsArray.Count() + 1)
                            return null;
                        if (index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = PreSaleItemsArray[index - 1];
                        var values = new object[] {
                            detail.Branch==null?"":detail.Branch.Name,
                            detail.PartsSalesCategory==null?"":detail.PartsSalesCategory.Name,
                            //detail.ServiceProductLine==null?"":detail.ServiceProductLine.Name,
                            detail.Category,
                            detail.CheckItem,
                            detail.FaultPattern,
                            Enum.GetName(typeof(DcsBaseDataStatus),detail.Status)
                       };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
    }
}




