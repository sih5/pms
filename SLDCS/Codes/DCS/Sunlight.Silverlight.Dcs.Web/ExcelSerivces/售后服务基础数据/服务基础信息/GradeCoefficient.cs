﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;


namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string ExportGradeCoefficient(int[] ids, int? partsSalesCategoryId, string grade, int? status, DateTime? createtimeBegin, DateTime? createtimeEnd) {
            var exportDataFileName = GetExportFilePath(string.Format("发动机产品线{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var EngineProductLines = GetGradeCoefficientByPersonSalesCenterLinks();
            if(ids != null && ids.Length > 0) {
                EngineProductLines = EngineProductLines.Where(r => ids.Contains(r.Id));
            } else {
                if(partsSalesCategoryId.HasValue) {
                    EngineProductLines = EngineProductLines.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId.Value);
                }
                if(!string.IsNullOrEmpty(grade)) {
                    EngineProductLines = EngineProductLines.Where(r => r.Grade.Contains(grade));
                }
                if(status.HasValue) {
                    EngineProductLines = EngineProductLines.Where(r => r.Status == status);
                }
                if(createtimeBegin != null) {
                    EngineProductLines = EngineProductLines.Where(r => r.CreateTime >= createtimeBegin);
                }
                if(createtimeEnd != null) {
                    EngineProductLines = EngineProductLines.Where(r => r.CreateTime <= createtimeEnd);
                }
            }
            var EngineProductLinesArray = EngineProductLines.ToArray();
            if(EngineProductLinesArray.Any()) {
                var excelColumns = new List<string>();
                excelColumns.Add("品牌");
                excelColumns.Add("系数");
                excelColumns.Add("备注");
                excelColumns.Add("状态");
                excelColumns.Add("创建人");
                excelColumns.Add("创建时间");
                excelColumns.Add("修改人");
                excelColumns.Add("修改时间");
                excelColumns.Add("作废人");
                excelColumns.Add("作废时间");
                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == EngineProductLinesArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = EngineProductLinesArray[index - 1];
                        var values = new object[] {
                            detail.PartsSalesCategoryName,
                            detail.Coefficient,
                            detail.Remark,
                            Enum.GetName(typeof(DcsBaseDataStatus),detail.Status),
                            detail.CreatorName,
                            detail.CreateTime,
                            detail.ModifierName,
                            detail.ModifyTime,
                            detail.AbandonerName,
                            detail.AbandonTime
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }

    }
}
