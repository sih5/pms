﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.Odbc;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string ExportQTSDataQueryForLS(string sernr, string kdauf, string matnr, DateTime? timeBegin, DateTime? timeEnd) {
            var exportDataFileName = GetExportFilePath(string.Format("车辆明细_雷萨_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));

            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT SERNR,DATUM,UZEIT,KDAUF,KDPOS,MATNR,CG_MATNR,CHARG,MENGE,BWART,CC_TIMES,MAKTX FROM \"_SYS_BIC\".\"QTS/CAL_LS_QTS\" where SERNR=?");
            OdbcCommand command = new OdbcCommand(); //command  对象
            if(string.IsNullOrWhiteSpace(sernr)) {
                throw new ValidationException("VIN码不能为空");
            }
            command.Parameters.Add(new OdbcParameter("@SERNR", sernr));
            if(!string.IsNullOrWhiteSpace(kdauf)) {
                sql.Append(" and KDAUF=? ");
                command.Parameters.Add(new OdbcParameter("@KDAUF", kdauf));
            }
            if(!string.IsNullOrWhiteSpace(matnr)) {
                sql.Append(" and MATNR=? ");
                command.Parameters.Add(new OdbcParameter("@MATNR", matnr));
            }
            if(timeBegin.HasValue && timeEnd.HasValue) {
                sql.Append(" and CC_TIMES>='" + timeBegin + "' ");
                sql.Append(" and CC_TIMES<='" + timeEnd + "' ");
            } else if(timeBegin.HasValue) {
                sql.Append(" and CC_TIMES>='" + timeBegin + "' ");
            } else if(timeEnd.HasValue) {
                sql.Append(" and CC_TIMES<='" + timeEnd + "' ");
            }
            command.CommandText = sql.ToString();
            DataSet ds = getDataSetBySql(command);

            List<VirtualQTSInfoLS> virtualQtsInfos = new List<VirtualQTSInfoLS>();
            for(int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                VirtualQTSInfoLS virtualQts = new VirtualQTSInfoLS();
                virtualQts.ID = (i + 1).ToString();
                virtualQts.SERNR = ds.Tables[0].Rows[i]["SERNR"].ToString();
                virtualQts.UZEIT = ds.Tables[0].Rows[i]["UZEIT"].ToString();
                virtualQts.KDAUF = ds.Tables[0].Rows[i]["KDAUF"].ToString();
                virtualQts.DATUM = ds.Tables[0].Rows[i]["DATUM"].ToString();
                virtualQts.KDPOS = ds.Tables[0].Rows[i]["KDPOS"].ToString();
                virtualQts.MATNR = ds.Tables[0].Rows[i]["MATNR"].ToString();
                virtualQts.CHARG = ds.Tables[0].Rows[i]["CHARG"].ToString();
                virtualQts.MENGE = ds.Tables[0].Rows[i]["MENGE"].ToString();
                virtualQts.BWART = ds.Tables[0].Rows[i]["BWART"].ToString();
                virtualQts.CC_TIMES = Convert.ToDateTime(ds.Tables[0].Rows[i]["CC_TIMES"]);
                virtualQts.MAKTX = ds.Tables[0].Rows[i]["MAKTX"].ToString();
                virtualQts.CG_MATNR = ds.Tables[0].Rows[i]["CG_MATNR"].ToString();
                virtualQtsInfos.Add(virtualQts);
            }
            if(virtualQtsInfos.Any()) {
                var excelColumns = new List<string>();
                excelColumns.Add("VIN");
                excelColumns.Add("日期");
                excelColumns.Add("时间");
                excelColumns.Add("销售订单编号");
                excelColumns.Add("销售订单行项");
                excelColumns.Add("整车编号");
                excelColumns.Add("图号");
                excelColumns.Add("供应商");
                excelColumns.Add("装配数量");
                excelColumns.Add("BWART");
                excelColumns.Add("日期时间");
                excelColumns.Add("采购件名称");
                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == virtualQtsInfos.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = virtualQtsInfos[index - 1];
                        var values = new object[] {
                            detail.SERNR,
                            detail.DATUM,
                            detail.UZEIT,
                            detail.KDAUF,
                            detail.KDPOS,
                            detail.MATNR,
                            detail.CG_MATNR,
                            detail.CHARG,
                            detail.MENGE,
                            detail.BWART,
                            detail.CC_TIMES,
                            detail.MAKTX
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
    }

}
