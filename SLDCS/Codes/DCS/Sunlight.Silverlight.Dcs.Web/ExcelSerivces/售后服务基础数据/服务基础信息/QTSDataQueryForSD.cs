﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.Odbc;
using System.Linq;
using System.Reflection;
using System.Text;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web
{
    partial class DcsDomainService
    {
        //public string ExportQTSDataQueryForSD(string vinCode, string mapid, string zcbmCode, string mapName, string factory, DateTime? assemblyBeginDate, DateTime? assemblyEndDate)
        //{
        //    var exportDataFileName = GetExportFilePath(string.Format("车辆明细_时代_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
        //    var qTSDataQueryForSDs = GetQTSDataQueryForSDs();
            
        //    if (!string.IsNullOrEmpty(vinCode))
        //    {
        //        qTSDataQueryForSDs = qTSDataQueryForSDs.Where(r => r.VinCode.Contains(vinCode));
        //    }
        //    if (!string.IsNullOrEmpty(mapid))
        //    {
        //        qTSDataQueryForSDs = qTSDataQueryForSDs.Where(r => r.Mapid.Contains(mapid));
        //    }
        //    if (!string.IsNullOrEmpty(zcbmCode))
        //    {
        //        qTSDataQueryForSDs = qTSDataQueryForSDs.Where(r => r.ZcbmCode.Contains(zcbmCode));
        //    }
        //    if (!string.IsNullOrEmpty(mapName))
        //    {
        //        qTSDataQueryForSDs = qTSDataQueryForSDs.Where(r => r.MapName.Contains(mapName));
        //    }
        //    if (!string.IsNullOrEmpty(factory))
        //    {
        //        qTSDataQueryForSDs = qTSDataQueryForSDs.Where(r => r.Factory.Contains(factory));
        //    }
        //    if (assemblyBeginDate != null)
        //    {
        //        qTSDataQueryForSDs = qTSDataQueryForSDs.Where(r => r.Assembly_Date >= assemblyBeginDate);
        //    }
        //    if (assemblyEndDate != null)
        //    {
        //        qTSDataQueryForSDs = qTSDataQueryForSDs.Where(r => r.Assembly_Date <= assemblyEndDate);
        //    }
        //    var qTSDataQueryForSDsArray = qTSDataQueryForSDs.ToArray();
        //    if (qTSDataQueryForSDsArray.Any())
        //    {
        //        var excelColumns = new List<string>();
        //        excelColumns.Add("QTS整车编码");
        //        excelColumns.Add("vin码");
        //        excelColumns.Add("装配日期");
        //        excelColumns.Add("物料供应商编码");
        //        excelColumns.Add("物料图号");
        //        excelColumns.Add("物料名称");
        //        excelColumns.Add("批次号");
        //        excelColumns.Add("生产线编号");

        //        using (var excelExport = new ExcelExport(exportDataFileName))
        //        {
        //            excelExport.ExportByRow(index =>
        //            {
        //                if (index == qTSDataQueryForSDsArray.Count() + 1)
        //                    return null;
        //                if (index == 0)
        //                    return excelColumns.ToArray<Object>();
        //                var detail = qTSDataQueryForSDsArray[index - 1];
        //                var values = new object[] {
        //                    detail.ZcbmCode,
        //                    detail.VinCode,
        //                    detail.Assembly_Date,
        //                    detail.Factory,
        //                    detail.Mapid,
        //                    detail.MapName,
        //                    detail.Patch,
        //                    detail.ProduceLine
        //                };
        //                return values;
        //            });
        //        }
        //    }
        //    return exportDataFileName;
        //}

        public string ExportQTSDataQueryForSD(string csCode, string vinCode, string mapid, string mapName, string factory, string zcbmcode, DateTime? timeBegin, DateTime? timeEnd) {
            var exportDataFileName = GetExportFilePath(string.Format("车辆明细_时代_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));

            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT CSCODE,PRODUCTNO,ZCBMCODE,ASSEMBLY_DATE, BIND_DATE,FACTORY,MAPID,PATCH,VIN,MAPNAME,SOURCE_CODE,GONGYINGSBIANHAO,GONGYINGSNAME,CHUCHANGID,CHANGXIAN FROM \"_SYS_BIC\".\"QTS/CAL_QTS_MERGE\" where SOURCE_CODE='QTS_AL' and VIN=?");
            OdbcCommand command = new OdbcCommand(); //command  对象
            if(string.IsNullOrWhiteSpace(vinCode)) {
                throw new ValidationException("VIN码不能为空");
            }
            command.Parameters.Add(new OdbcParameter("@VIN", vinCode));
            if(!string.IsNullOrWhiteSpace(mapid)) {
                sql.Append(" and MAPID=? ");
                command.Parameters.Add(new OdbcParameter("@MAPID", mapid));
            }
            if(!string.IsNullOrWhiteSpace(csCode)) {
                sql.Append(" and CSCODE=? ");
                command.Parameters.Add(new OdbcParameter("@CSCODE", csCode));
            }
            if(!string.IsNullOrWhiteSpace(zcbmcode)) {
                sql.Append(" and ZCBMCODE=? ");
                command.Parameters.Add(new OdbcParameter("@ZCBMCODE", zcbmcode));
            }
            if(!string.IsNullOrWhiteSpace(mapName)) {
                sql.Append(" and MAPNAME=? ");
                command.Parameters.Add(new OdbcParameter("@MAPNAME", mapName));
            }
            if(!string.IsNullOrWhiteSpace(factory)) {
                sql.Append(" and FACTORY=? ");
                command.Parameters.Add(new OdbcParameter("@FACTORY", factory));
            }
            if(timeBegin.HasValue && timeEnd.HasValue) {
                sql.Append(" and ASSEMBLY_DATE>='" + timeBegin + "' ");
                sql.Append(" and ASSEMBLY_DATE<='" + timeEnd + "' ");
            } else if(timeBegin.HasValue) {
                sql.Append(" and ASSEMBLY_DATE>='" + timeBegin + "' ");
            } else if(timeEnd.HasValue) {
                sql.Append(" and ASSEMBLY_DATE<='" + timeEnd + "' ");
            }
            command.CommandText = sql.ToString();
            DataSet ds = getDataSetBySql(command);

            List<VirtualQTSInfo> virtualQtsInfos = new List<VirtualQTSInfo>();
            for(int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                VirtualQTSInfo virtualQts = new VirtualQTSInfo();
                virtualQts.ID = (i + 1).ToString();
                virtualQts.CSCODE = ds.Tables[0].Rows[i]["CSCODE"].ToString();
                virtualQts.PRODUCTNO = ds.Tables[0].Rows[i]["PRODUCTNO"].ToString();
                virtualQts.ZCBMCODE = ds.Tables[0].Rows[i]["ZCBMCODE"].ToString();
                virtualQts.ASSEMBLY_DATE = Convert.ToDateTime(ds.Tables[0].Rows[i]["ASSEMBLY_DATE"]);
                virtualQts.BIND_DATE = ds.Tables[0].Rows[i]["BIND_DATE"].ToString();
                virtualQts.FACTORY = ds.Tables[0].Rows[i]["FACTORY"].ToString();
                virtualQts.MAPID = ds.Tables[0].Rows[i]["MAPID"].ToString();
                virtualQts.PATCH = ds.Tables[0].Rows[i]["PATCH"].ToString();
                virtualQts.VIN = ds.Tables[0].Rows[i]["VIN"].ToString();
                virtualQts.MAPNAME = ds.Tables[0].Rows[i]["MAPNAME"].ToString();
                virtualQts.SOURCE_CODE = ds.Tables[0].Rows[i]["SOURCE_CODE"].ToString();
                virtualQts.GONGYINGSBIANHAO = ds.Tables[0].Rows[i]["GONGYINGSBIANHAO"].ToString();
                virtualQts.CHUCHANGID = ds.Tables[0].Rows[i]["CHUCHANGID"].ToString();
                virtualQts.CHANGXIAN = ds.Tables[0].Rows[i]["CHANGXIAN"].ToString();
                virtualQtsInfos.Add(virtualQts); 
            }
            if(virtualQtsInfos.Any()) {
                var excelColumns = new List<string>();
                excelColumns.Add("订单号");
                excelColumns.Add("QTS整车编码");
                excelColumns.Add("VIN码");
                excelColumns.Add("总装下线日期");
                excelColumns.Add("物料供应商编码");
                excelColumns.Add("物料供应商名称");
                excelColumns.Add("物料图号");
                excelColumns.Add("物料名称");
                excelColumns.Add("批次号");
                excelColumns.Add("生产线编号");

                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == virtualQtsInfos.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = virtualQtsInfos[index - 1];
                        var values = new object[] {
                            detail.CSCODE,
                            detail.ZCBMCODE,
                            detail.VIN,
                            detail.ASSEMBLY_DATE,
                            detail.FACTORY,
                            detail.GONGYINGSNAME,
                            detail.MAPID,
                            detail.MAPNAME,
                            detail.PATCH,
                            detail.CHANGXIAN
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
    }
}