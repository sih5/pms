﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using System.Text;
using Sunlight.Silverlight.Dcs.Web.ExcelOperator;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        [Invoke]
        public string 导入审批单调整(string fileName) {
            var vehShipplanApprovalChangeRecs = new List<VehShipplanApprovalChangeRec>();
            var errorvehShipplanApprovalChangeRecs = new List<VehShipplanApprovalChangeRec>();
            List<object> excelColumns = null;

            //获取指定表结构
            List<string> notNullableFields;
            Dictionary<string, int> fieldLenght;
            this.GetEntitySchema(this.ObjectContext.VehShipplanApprovalChangeRecs.EntitySet, out notNullableFields, out fieldLenght);

            using(var excelOperator = new ExcelImport(Path.Combine(AttachmentFolder, GetImportFilePath(fileName)))) {
                excelOperator.AddColumnDataSource("源VIN", "OriginalVIN");
                excelOperator.AddColumnDataSource("目标VIN", "NewVIN");

                excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();

                excelOperator.LoadExcelRow(row => {
                    var vehShipplanApprovalChangeRe = new VehShipplanApprovalChangeRec {
                        OriginalVIN = row["OriginalVIN"],
                        NewVIN = row["NewVIN"],
                        AdjustmentType = (int)DcsVehShipplanApprovalChangeRecAdjustmentType.可用资源调整,
                        Status = (int)DcsVehicleBaseDataStatus.有效
                    };

                    var errorMsgs = new List<string>();
                    //原VIN码
                    var fieldIndex = notNullableFields.IndexOf("ORIGINALVIN");
                    if(string.IsNullOrEmpty(vehShipplanApprovalChangeRe.OriginalVIN)) {
                        if(fieldIndex > -1)
                            errorMsgs.Add(ErrorStrings.ImpVehShipplanApprovalChangeRec_Validation10);
                    } else {
                        if(Encoding.Default.GetByteCount(vehShipplanApprovalChangeRe.NewVIN) > fieldLenght["ORIGINALVIN"])
                            errorMsgs.Add(ErrorStrings.ImpVehShipplanApprovalChangeRec_Validation2);
                    }
                    //目标VIN码
                    fieldIndex = notNullableFields.IndexOf("NEWVIN");
                    if(string.IsNullOrEmpty(vehShipplanApprovalChangeRe.OriginalVIN)) {
                        if(fieldIndex > -1)
                            errorMsgs.Add(ErrorStrings.ImpVehShipplanApprovalChangeRec_Validation9);
                    } else {
                        if(Encoding.Default.GetByteCount(vehShipplanApprovalChangeRe.NewVIN) > fieldLenght["NEWVIN"])
                            errorMsgs.Add(ErrorStrings.ImpVehShipplanApprovalChangeRec_Validation1);
                    }
                    if(errorMsgs.Count > 0) {
                        vehShipplanApprovalChangeRe.ErrorMsg = string.Join("; ", errorMsgs);
                        errorvehShipplanApprovalChangeRecs.Add(vehShipplanApprovalChangeRe);
                    } else
                        vehShipplanApprovalChangeRecs.Add(vehShipplanApprovalChangeRe);

                    return false;
                });
            }

            var originalVINs = vehShipplanApprovalChangeRecs.Select(r => r.OriginalVIN.ToLower()).ToArray();
            var newVINs = vehShipplanApprovalChangeRecs.Select(r => r.NewVIN.ToLower()).ToArray();
            //根据目标VIN获取可用资源。
            var vehicleAvailableResources = this.ObjectContext.VehicleAvailableResources.Where(r => newVINs.Contains(r.VIN)).ToArray();
            //根据源VIN获取审批单清单。
            var vehicleShipplanApprovalDetails = this.ObjectContext.VehicleShipplanApprovalDetails.Where(r => originalVINs.Contains(r.VIN) && (r.Status == (int)DcsVehicleShipplanApprovalDetailStatus.新增 || r.Status == (int)DcsVehicleShipplanApprovalDetailStatus.有效)).ToArray();
            var vehicleShipplanApprovalIds = vehicleShipplanApprovalDetails.Select(r => r.VehicleShipplanApprovalId).ToArray();
            //根据源VIN获取审批单。
            var vehicleShipplanApprovals = this.ObjectContext.VehicleShipplanApprovals.Where(r => vehicleShipplanApprovalIds.Contains(r.Id) && r.Status != (int)DcsVehicleShipplanApprovalStatus.作废).ToArray();
            //根据目标VIN获取车辆信息
            var vehicleInformations = this.ObjectContext.VehicleInformations.Where(r => newVINs.Contains(r.VIN)).ToArray();
            //根据源VIN获取可用资源信息
            var originalVehicleAvailableResources = this.ObjectContext.VehicleAvailableResources.Where(r => originalVINs.Contains(r.VIN)).ToArray();
            for(var i = vehShipplanApprovalChangeRecs.Count - 1; i >= 0; i--) {
                var errorMsgs = new List<string>();
                if(vehShipplanApprovalChangeRecs.GroupBy(r => r.OriginalVIN).Any(v => v.Count() > 1))
                    errorMsgs.Add(ErrorStrings.ImpVehShipplanApprovalChangeRec_Validation5);
                var vehicleAvailableResource = vehicleAvailableResources.FirstOrDefault(r => r.VIN.ToLower() == vehShipplanApprovalChangeRecs[i].OriginalVIN.ToLower() && r.IsAvailable);
                if(vehicleAvailableResource != null)
                    errorMsgs.Add(ErrorStrings.ImpVehShipplanApprovalChangeRec_Validation3);
                var vehicleAvailableResourceLock = vehicleAvailableResources.FirstOrDefault(r => r.VIN.ToLower() == vehShipplanApprovalChangeRecs[i].OriginalVIN.ToLower() && r.LockStatus != (int)DcsVehicleAvailableResourceLockStatus.未锁定);
                if(vehicleAvailableResourceLock != null)
                    errorMsgs.Add(string.Format(ErrorStrings.ImpVehShipplanApprovalChangeRec_Validation4, vehShipplanApprovalChangeRecs[i].NewVIN));
                var vehicleShipplanApprovalDetailofvins = vehicleShipplanApprovalDetails.Where(r => r.VIN.ToLower() == vehShipplanApprovalChangeRecs[i].OriginalVIN.ToLower()).ToArray();
                if(vehicleShipplanApprovalDetailofvins.Count() != 1)
                    errorMsgs.Add(ErrorStrings.ImpVehShipplanApprovalChangeRec_Validation12);
                var vehicleShipplanApprovalDetail = vehicleShipplanApprovalDetailofvins.FirstOrDefault();
                if(vehicleShipplanApprovalDetail == null)
                    errorMsgs.Add(ErrorStrings.ImpVehShipplanApprovalChangeRec_Validation6);
                var vehicleShipplanApproval = vehicleShipplanApprovals.SingleOrDefault(r => r.Id == vehicleShipplanApprovalDetail.VehicleShipplanApprovalId);
                if(vehicleShipplanApproval == null)
                    errorMsgs.Add(ErrorStrings.ImpVehShipplanApprovalChangeRec_Validation7);
                var vehicleInformation = vehicleInformations.FirstOrDefault(r => r.VIN.ToLower() == vehShipplanApprovalChangeRecs[i].NewVIN.ToLower());
                if(vehicleInformation == null)
                    errorMsgs.Add(ErrorStrings.ImpVehShipplanApprovalChangeRec_Validation8);
                var originalVehicleAvailableResource = originalVehicleAvailableResources.FirstOrDefault(r => r.VIN.ToLower() == vehShipplanApprovalChangeRecs[i].OriginalVIN.ToLower());
                if(originalVehicleAvailableResource == null)
                    errorMsgs.Add(ErrorStrings.ImpVehShipplanApprovalChangeRec_Validation11);
                if(errorMsgs.Any()) {
                    vehShipplanApprovalChangeRecs[i].ErrorMsg = string.Join("; ", errorMsgs);
                    errorvehShipplanApprovalChangeRecs.Add(vehShipplanApprovalChangeRecs[i]);
                } else {
                    vehShipplanApprovalChangeRecs[i].OriginalShipplanApprovalCode = vehicleShipplanApproval.Code;
                    InsertToDatabase(vehShipplanApprovalChangeRecs[i]);
                    InsertVehShipplanApprovalChangeRecValidate(vehShipplanApprovalChangeRecs[i]);
                    vehicleShipplanApprovalDetail.VIN = vehShipplanApprovalChangeRecs[i].NewVIN;
                    vehicleShipplanApprovalDetail.VehicleId = vehicleInformation.Id;
                    UpdateVehicleShipplanApprovalDetailValidate(vehicleShipplanApprovalDetail);
                    originalVehicleAvailableResource.LockStatus = (int)DcsVehicleAvailableResourceLockStatus.未锁定;
                    UpdateVehicleAvailableResourceValidate(originalVehicleAvailableResource);
                    vehicleAvailableResource.LockStatus = (int)DcsVehicleAvailableResourceLockStatus.审批单锁定;
                    vehicleAvailableResource.VehicleShipplanApprovalId = vehicleShipplanApproval.Id;
                    vehicleAvailableResource.VehicleShipplanApprovalCode = vehicleShipplanApproval.Code;
                    UpdateVehicleAvailableResourceValidate(vehicleAvailableResource);
                }
            }
            var errorDataFilePath = "";
            if(errorvehShipplanApprovalChangeRecs.Count() > 0) {
                excelColumns.Add("校验信息");
                errorDataFilePath = GetErrorFilePath(fileName);
                using(var excelExport = new ExcelExport(Path.Combine(AttachmentFolder, errorDataFilePath))) {
                    excelExport.ExportByRow(index => {
                        if(index == errorvehShipplanApprovalChangeRecs.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray();
                        var vehShipplanApprovalChangeRec = errorvehShipplanApprovalChangeRecs[index - 1];
                        var values = new object[] {
                            vehShipplanApprovalChangeRec.OriginalVIN, vehShipplanApprovalChangeRec.NewVIN, vehShipplanApprovalChangeRec.ErrorMsg
                        };
                        return values;
                    });
                }
                errorvehShipplanApprovalChangeRecs = null;
            }

            return errorDataFilePath;
        }

    }
}
