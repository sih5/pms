﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using System.Text;
using Sunlight.Silverlight.Dcs.Web.ExcelOperator;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        [Invoke]
        public string 国产车导入(string fileName) {
            var userInfo = this.GetCurrentUserInfo();
            List<string> vehicleNotNullableFields;
            Dictionary<string, int> vehicleFieldLenght;
            this.GetEntitySchema(this.ObjectContext.VehicleInformations.EntitySet, out vehicleNotNullableFields, out vehicleFieldLenght);

            List<string> resourceNotNullableFields;
            Dictionary<string, int> resourceFieldLenght;
            this.GetEntitySchema(this.ObjectContext.VehicleAvailableResources.EntitySet, out resourceNotNullableFields, out resourceFieldLenght);

            List<object> excelColumns;
            var vehicleInformations = new List<VehicleInformation>();
            var errorVehicleInformations = new List<VehicleInformation>();
            var updateVehicleInformations = new List<VehicleInformation>();
            var vehicleAvailableResources = new List<VehicleAvailableResource>();
            var errorVehicleAvailableResources = new List<VehicleAvailableResource>();
            var updateVehicleAvailableResources = new List<VehicleAvailableResource>();
            using(var excelOperator = new ExcelImport(Path.Combine(AttachmentFolder, GetImportFilePath(fileName)))) {
                excelOperator.AddColumnDataSource("产品编号", "ProductCode");
                excelOperator.AddColumnDataSource("VIN", "VIN");
                excelOperator.AddColumnDataSource("发动机号", "EngineSerialNumber");
                excelOperator.AddColumnDataSource("下线时间", "RolloutDate", typeof(DateTime));
                excelOperator.AddColumnDataSource("SON", "ProductionPlanSON");
                excelOperator.AddColumnDataSource("下线仓库", "VehicleWarehouseCode");
                excelOperator.AddColumnDataSource("是否可用", "IsAvailable");

                excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();

                excelOperator.LoadExcelRow(row => {
                    var vehicleInformation = new VehicleInformation {
                        VIN = row["VIN"],
                        ProductCode = row["ProductCode"],
                        EngineSerialNumber = row["EngineSerialNumber"],
                        ProductionPlanSON = row["ProductionPlanSON"],
                        OldVIN = row["VIN"]
                    };
                    var vehicleAvailableResource = new VehicleAvailableResource {
                        VehicleWarehouseCode = row["VehicleWarehouseCode"],
                        VIN = row["VIN"],
                        ProductCode = row["ProductCode"]
                    };
                    vehicleInformation.VehicleAvailableResources.Add(vehicleAvailableResource);
                    var errorMsgs = new List<string>();
                    //VIN检查
                    var fieldIndex = vehicleNotNullableFields.IndexOf("VIN");
                    if(string.IsNullOrEmpty(vehicleInformation.VIN)) {
                        if(fieldIndex > -1)
                            errorMsgs.Add(ErrorStrings.ImpVehicleAvailableResource_Validation3);
                    } else {
                        if(Encoding.Default.GetByteCount(vehicleInformation.VIN) > vehicleFieldLenght["VIN"])
                            errorMsgs.Add(ErrorStrings.ImpVehicleAvailableResource_Validation4);
                    }
                    //产品编号检查
                    fieldIndex = vehicleNotNullableFields.IndexOf("PRODUCTCODE");
                    if(string.IsNullOrEmpty(vehicleInformation.ProductCode)) {
                        if(fieldIndex > -1)
                            errorMsgs.Add(ErrorStrings.ImpVehicleAvailableResource_Validation5);
                    } else {
                        if(Encoding.Default.GetByteCount(vehicleInformation.ProductCode) > vehicleFieldLenght["PRODUCTCODE"])
                            errorMsgs.Add(ErrorStrings.ImpVehicleAvailableResource_Validation6);
                    }
                    //发动机号检查 
                    fieldIndex = vehicleNotNullableFields.IndexOf("ENGINESERIALNUMBER");
                    if(string.IsNullOrEmpty(vehicleInformation.EngineSerialNumber)) {
                        if(fieldIndex > -1)
                            errorMsgs.Add(ErrorStrings.ImpVehicleAvailableResource_Validation7);
                    } else {
                        if(Encoding.Default.GetByteCount(vehicleInformation.EngineSerialNumber) > vehicleFieldLenght["ENGINESERIALNUMBER"])
                            errorMsgs.Add(ErrorStrings.ImpVehicleAvailableResource_Validation8);
                    }
                    //下线时间检查
                    if(!string.IsNullOrEmpty(row["RolloutDate"])) {
                        vehicleInformation.RolloutDateStr = row["RolloutDate"];
                        DateTime rolloutDate;
                        if(DateTime.TryParse(row["RolloutDate"], out rolloutDate))
                            vehicleInformation.RolloutDate = rolloutDate;
                        else
                            errorMsgs.Add(ErrorStrings.ImpVehicleAvailableResource_Validation9);
                    }
                    //SON检查
                    fieldIndex = vehicleNotNullableFields.IndexOf("SON");
                    if(string.IsNullOrEmpty(vehicleInformation.SON)) {
                        if(fieldIndex > -1)
                            errorMsgs.Add(ErrorStrings.ImpVehicleAvailableResource_Validation10);
                    } else {
                        if(Encoding.Default.GetByteCount(vehicleInformation.SON) > vehicleFieldLenght["SON"])
                            errorMsgs.Add(ErrorStrings.ImpVehicleAvailableResource_Validation11);
                    }
                    //下线仓库检查
                    fieldIndex = resourceNotNullableFields.IndexOf("VEHICLEWAREHOUSECODE");
                    if(string.IsNullOrEmpty(vehicleAvailableResource.VehicleWarehouseCode)) {
                        if(fieldIndex > -1)
                            errorMsgs.Add(ErrorStrings.ImpVehicleAvailableResource_Validation12);
                    } else {
                        if(Encoding.Default.GetByteCount(vehicleAvailableResource.VehicleWarehouseCode) > resourceFieldLenght["VEHICLEWAREHOUSECODE"])
                            errorMsgs.Add(ErrorStrings.ImpVehicleAvailableResource_Validation13);
                    }
                    //是否可用检查
                    fieldIndex = resourceNotNullableFields.IndexOf("ISAVAILABLE");
                    if(string.IsNullOrEmpty(row["IsAvailable"])) {
                        if(fieldIndex > -1)
                            errorMsgs.Add(ErrorStrings.ImpVehicleAvailableResource_Validation14);
                    } else
                        switch(row["IsAvailable"]) {
                            case "是":
                                vehicleAvailableResource.IsAvailableStr = row["IsAvailable"];
                                vehicleAvailableResource.IsAvailable = true;
                                break;
                            case "否":
                                vehicleAvailableResource.IsAvailableStr = row["IsAvailable"];
                                vehicleAvailableResource.IsAvailable = false;
                                break;
                            default:
                                vehicleAvailableResource.IsAvailableStr = row["IsAvailable"];
                                errorMsgs.Add(ErrorStrings.ImpVehicleAvailableResource_Validation15);
                                break;
                        }
                    if(errorMsgs.Count > 0) {
                        vehicleAvailableResource.ErrorMsgs = string.Join("; ", errorMsgs);
                        errorVehicleInformations.Add(vehicleInformation);
                        errorVehicleAvailableResources.Add(vehicleAvailableResource);
                    } else {
                        vehicleInformations.Add(vehicleInformation);
                        vehicleAvailableResources.Add(vehicleAvailableResource);
                    }
                    return false;
                });
                var productCode = vehicleInformations.Select(v => v.ProductCode.ToLower()).Distinct().ToArray();
                var products = this.ObjectContext.Products.Where(v => productCode.Contains(v.Code.ToLower()) && v.Status == (int)DcsMasterDataStatus.有效).Select(v => new {
                    v.Id,
                    v.Code,
                    v.Name
                }).ToArray();
                var productIds = products.Select(r => r.Id);
                var vehicleModelAffiProducts = this.ObjectContext.VehicleModelAffiProducts.Where(r => productIds.Contains(r.ProductId)).Select(r => new {
                    r.ProductId,
                    r.ProductCategoryCode,
                    r.ProductCategoryName
                }).ToArray();
                var vehicleWarehouseCodes = vehicleAvailableResources.Select(r => r.VehicleWarehouseCode.ToLower()).Distinct().ToArray();
                var vehicleWarehouses = this.ObjectContext.VehicleWarehouses.Where(r => vehicleWarehouseCodes.Contains(r.Code.ToLower())).Select(r => new {
                    r.Id,
                    r.Code,
                    r.Name
                }).ToArray();
                var vins = vehicleInformations.Select(r => r.VIN.ToLower()).Distinct().ToArray();
                var vCX5BUProducts = this.ObjectContext.VCX5BUProduct.Where(r => productCode.Contains(r.l4code.ToLower())).ToArray();
                var dbVehicleInformations = ObjectContext.VehicleInformations.Where(v => vins.Contains(v.VIN.ToLower())).ToArray();
                var dbVehicleAvailableResources = ObjectContext.VehicleAvailableResources.Where(v => vins.Contains(v.VIN.ToLower())).ToArray();
                for(var i = vehicleInformations.Count - 1; i >= 0; i--) {
                    var vCX5BUProduct = vCX5BUProducts.Where(r => r.l4code.ToLower() == vehicleInformations[i].ProductCode.ToLower()).ToArray();
                    if(vCX5BUProduct.Any()) {
                        vehicleAvailableResources[i].ErrorMsgs = ErrorStrings.ImpVehicleAvailableResource_Validation25;
                        errorVehicleInformations.Add(vehicleInformations[i]);
                        errorVehicleAvailableResources.Add(vehicleAvailableResources[i]);
                    } else {
                        var product = products.SingleOrDefault(v => String.Compare(v.Code, vehicleInformations[i].ProductCode, StringComparison.OrdinalIgnoreCase) == 0);
                        if(product == null) {
                            vehicleAvailableResources[i].ErrorMsgs = string.Format(ErrorStrings.ImpVehicleAvailableResource_Validation16, vehicleInformations[i].ProductCode);
                            errorVehicleInformations.Add(vehicleInformations[i]);
                            errorVehicleAvailableResources.Add(vehicleAvailableResources[i]);
                        } else {
                            vehicleInformations[i].ProductId = product.Id;
                            vehicleInformations[i].ProductName = product.Name;
                            vehicleAvailableResources[i].ProductId = product.Id;
                            vehicleAvailableResources[i].ProductName = product.Name;
                            var vehicleModelAffiProduct = vehicleModelAffiProducts.FirstOrDefault(v => v.ProductId == product.Id);
                            if(vehicleModelAffiProduct == null) {
                                vehicleAvailableResources[i].ErrorMsgs = string.Format(ErrorStrings.ImpVehicleAvailableResource_Validation17, vehicleInformations[i].ProductCode);
                                errorVehicleInformations.Add(vehicleInformations[i]);
                                errorVehicleAvailableResources.Add(vehicleAvailableResources[i]);
                            } else {
                                vehicleInformations[i].ProductCategoryCode = vehicleModelAffiProduct.ProductCategoryCode;
                                vehicleInformations[i].ProductCategoryName = vehicleModelAffiProduct.ProductCategoryName;
                                vehicleAvailableResources[i].ProductCategoryCode = vehicleModelAffiProduct.ProductCategoryCode;
                                vehicleAvailableResources[i].ProductCategoryName = vehicleModelAffiProduct.ProductCategoryName;
                                var vehicleWarehouse = vehicleWarehouses.FirstOrDefault(v => String.Compare(v.Code, vehicleAvailableResources[i].VehicleWarehouseCode, StringComparison.OrdinalIgnoreCase) == 0);
                                if(vehicleWarehouse == null) {
                                    vehicleAvailableResources[i].ErrorMsgs = string.Format(ErrorStrings.ImpVehicleAvailableResource_Validation18, vehicleAvailableResources[i].VehicleWarehouseCode);
                                    errorVehicleInformations.Add(vehicleInformations[i]);
                                    errorVehicleAvailableResources.Add(vehicleAvailableResources[i]);
                                } else {
                                    vehicleAvailableResources[i].VehicleWarehouseId = vehicleWarehouse.Id;
                                    vehicleAvailableResources[i].VehicleWarehouseName = vehicleWarehouse.Name;
                                    var checkVehicleInformations = vehicleInformations.Except(new[] { vehicleInformations[i] }).Where(r => r.VIN.ToLower() == vehicleInformations[i].VIN.ToLower()).ToArray();
                                    if(checkVehicleInformations.Any()) {
                                        vehicleAvailableResources[i].ErrorMsgs = ErrorStrings.ImpVehicleAvailableResource_Validation23;
                                        errorVehicleInformations.Add(vehicleInformations[i]);
                                        errorVehicleAvailableResources.Add(vehicleAvailableResources[i]);
                                    } else {
                                        checkVehicleInformations = vehicleInformations.Except(new[] { vehicleInformations[i] }).Where(r => r.ProductionPlanSON.ToLower() == vehicleInformations[i].ProductionPlanSON.ToLower()).ToArray();
                                        if(checkVehicleInformations.Any()) {
                                            vehicleAvailableResources[i].ErrorMsgs = ErrorStrings.ImpVehicleAvailableResource_Validation2;
                                            errorVehicleInformations.Add(vehicleInformations[i]);
                                            errorVehicleAvailableResources.Add(vehicleAvailableResources[i]);
                                        } else {
                                            var updateVehicleInformation = dbVehicleInformations.FirstOrDefault(r => r.VIN.ToLower() == vehicleInformations[i].VIN.ToLower());
                                            if(updateVehicleInformation != null) {
                                                UpdateToDatabase(updateVehicleInformation);
                                                updateVehicleInformation.ModifierId = userInfo.Id;
                                                updateVehicleInformation.ModifierName = userInfo.Name;
                                                updateVehicleInformation.ModifyTime = DateTime.Now;
                                                updateVehicleInformation.EngineSerialNumber = vehicleInformations[i].EngineSerialNumber;
                                                updateVehicleInformation.RolloutDate = vehicleInformations[i].RolloutDate;
                                                updateVehicleInformations.Add(vehicleInformations[i]);
                                            }
                                            var updateVehicleAvailableResource = dbVehicleAvailableResources.FirstOrDefault(r => r.VIN.ToLower() == vehicleAvailableResources[i].VIN.ToLower());
                                            if(updateVehicleAvailableResource != null) {
                                                UpdateToDatabase(updateVehicleAvailableResource);
                                                updateVehicleAvailableResource.ModifierId = userInfo.Id;
                                                updateVehicleAvailableResource.ModifierName = userInfo.Name;
                                                updateVehicleAvailableResource.ModifyTime = DateTime.Now;
                                                updateVehicleAvailableResource.IsAvailable = vehicleAvailableResources[i].IsAvailable;
                                                updateVehicleAvailableResources.Add(vehicleAvailableResources[i]);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            foreach(var vehicleInformation in vehicleInformations.Except(errorVehicleInformations).Except(updateVehicleInformations)) {
                vehicleInformation.BranchId = GlobalVar.DEFAULT_VEHICLEINFORMATION_BRANCHID;
                vehicleInformation.BranchCode = GlobalVar.DEFAULT_VEHICLEINFORMATION_BRANCHCODE;
                vehicleInformation.BranchName = GlobalVar.DEFAULT_VEHICLEINFORMATION_BRANCHNAME;
                vehicleInformation.VehicleType = (int)DcsVehicleInformationVehicleType.普通;
                vehicleInformation.VehicleCategoryId = GlobalVar.DEFAULT_DEALERVEHICLEMONTHQUOTA_VEHICLESALESORGID;
                vehicleInformation.VehicleCategoryName = GlobalVar.DEFAULT_DEALERVEHICLEMONTHQUOTA_VEHICLESALESORGNAME;
                vehicleInformation.Status = (int)DcsVehicleStatus.本部仓库;
                vehicleInformation.CreatorId = userInfo.Id;
                vehicleInformation.CreatorName = userInfo.Name;
                vehicleInformation.CreateTime = DateTime.Now;
                this.InsertToDatabase(vehicleInformation);
            }
            foreach(var vehicleAvailableResource in vehicleAvailableResources.Except(errorVehicleAvailableResources).Except(updateVehicleAvailableResources)) {
                vehicleAvailableResource.CreatorId = userInfo.Id;
                vehicleAvailableResource.CreatorName = userInfo.Name;
                vehicleAvailableResource.CreateTime = DateTime.Now;
                vehicleAvailableResource.BranchId = GlobalVar.DEFAULT_VEHICLEAVAILABLERESOURCE_BRANCHID;
                vehicleAvailableResource.BranchCode = GlobalVar.DEFAULT_VEHICLEAVAILABLERESOURCE_BRANCHCODE;
                vehicleAvailableResource.BranchName = GlobalVar.DEFAULT_VEHICLEAVAILABLERESOURCE_BRANCHNAME;
                vehicleAvailableResource.QualityStatus = (int)DcsVehicleAvailableResourceQualityStatus.合格;
                vehicleAvailableResource.LockStatus = (int)DcsVehicleAvailableResourceLockStatus.未锁定;
                this.InsertToDatabase(vehicleAvailableResource);
            }
            this.ObjectContext.SaveChanges();
            var errorDataFilePath = "";
            if(!errorVehicleInformations.Any() || !errorVehicleAvailableResources.Any())
                return errorDataFilePath;
            excelColumns.Add("校验信息");
            errorDataFilePath = GetErrorFilePath(fileName);
            using(var excelExport = new ExcelExport(Path.Combine(AttachmentFolder, errorDataFilePath))) {
                excelExport.ExportByRow(index => {
                    if(index == errorVehicleInformations.Count + 1)
                        return null;
                    if(index == 0)
                        return excelColumns.ToArray();
                    var vehicleInformation = errorVehicleInformations[index - 1];
                    var vehicleAvailableResource = errorVehicleAvailableResources[index - 1];
                    var values = new object[] {
                                vehicleInformation.ProductCode,vehicleInformation.VIN,vehicleInformation.EngineSerialNumber,vehicleInformation.RolloutDateStr,
                                vehicleInformation.ProductionPlanSON,vehicleAvailableResource.VehicleWarehouseCode,vehicleAvailableResource.IsAvailableStr,vehicleAvailableResource.ErrorMsgs
                            };
                    return values;
                });
            }
            return errorDataFilePath;
        }

        [Invoke]
        public string 导出可用资源信息(DateTime? startRolloutDate, DateTime? endRolloutDate, string vin, string engineSerialNumber, string productCategoryCode, bool? isAvailable, int? vehicleWarehouseId, int? lockStatus) {
            var excelColumns = new List<object>();
            var keyValueItems = this.ObjectContext.KeyValueItems.Where(v => v.Name == "VehicleAvailableResource_LockStatus" && v.Status == (int)DcsMasterDataStatus.有效).ToArray();
            var resultsql = from vehicleAvailableResource in ObjectContext.VehicleAvailableResources
                            from vehicleInformation in ObjectContext.VehicleInformations
                            from product in ObjectContext.Products
                            from productAffiProductCategory in ObjectContext.ProductAffiProductCategories
                            from productCategory in ObjectContext.ProductCategories
                            where vehicleAvailableResource.VehicleId == vehicleInformation.Id
                            && vehicleAvailableResource.ProductId == product.Id
                            && productAffiProductCategory.ProductId == product.Id
                            && productCategory.Id == productAffiProductCategory.ProductCategoryId
                            select new {
                                vehicleAvailableResource.VIN,
                                vehicleAvailableResource.ProductCode,
                                vehicleAvailableResource.ProductCategoryCode,
                                vehicleAvailableResource.LockStatus,
                                vehicleInformation.ProductionPlanSON,
                                vehicleInformation.EngineSerialNumber,
                                vehicleInformation.RolloutDate,
                                vehicleAvailableResource.IsAvailable,
                                CategoryCode = productCategory.Code,
                                vehicleAvailableResource.VehicleWarehouseId
                            };
            if(startRolloutDate.HasValue)
                resultsql = resultsql.Where(r => r.RolloutDate >= startRolloutDate.Value);
            if(endRolloutDate.HasValue)
                resultsql = resultsql.Where(r => r.RolloutDate <= endRolloutDate.Value);
            if(!string.IsNullOrEmpty(vin))
                resultsql = resultsql.Where(r => r.VIN.ToLower().Contains(vin.ToLower()));
            if(!string.IsNullOrEmpty(engineSerialNumber))
                resultsql = resultsql.Where(r => r.EngineSerialNumber.ToLower().Contains(engineSerialNumber.ToLower()));
            if(!string.IsNullOrEmpty(productCategoryCode))
                resultsql = resultsql.Where(r => r.ProductCategoryCode.ToLower().Contains(productCategoryCode.ToLower()));
            if(vehicleWarehouseId.HasValue)
                resultsql = resultsql.Where(r => r.VehicleWarehouseId == vehicleWarehouseId);
            if(isAvailable.HasValue)
                resultsql = resultsql.Where(r => r.IsAvailable == isAvailable.Value);
            if(lockStatus.HasValue)
                resultsql = resultsql.Where(r => r.LockStatus == lockStatus.Value);


            string dataFileName = "";
            var details = resultsql.ToArray();
            if(details.Any()) {
                excelColumns.Add("VIN");
                excelColumns.Add("产品编号");
                excelColumns.Add("车型编号");
                excelColumns.Add("锁定状态");
                excelColumns.Add("生产SON");
                excelColumns.Add("发动机序列号");
                excelColumns.Add("下线日期");
                excelColumns.Add("是否可用");
                excelColumns.Add("车型大类");
                dataFileName = GetExportFilePath("可用资源信息.xls");
                using(var excelExport = new ExcelExport(Path.Combine(AttachmentFolder, dataFileName))) {
                    excelExport.ExportByRow(index => {
                        if(index == details.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray();
                        var detail = details[index - 1];
                        var lockStatusStr = keyValueItems.Single(r => r.Key == detail.LockStatus).Value;
                        var values = new object[] {
                           detail.VIN,detail.ProductCode,detail.ProductCategoryCode,lockStatusStr,detail.ProductionPlanSON,detail.EngineSerialNumber,detail.RolloutDate,detail.IsAvailable?"是":"否",detail.CategoryCode
                        };
                        return values;
                    });
                }
            }
            return dataFileName;
        }

        [Invoke]
        public string MCO导入(string fileName) {
            var userInfo = this.GetCurrentUserInfo();
            List<string> vehicleNotNullableFields;
            Dictionary<string, int> vehicleFieldLenght;
            this.GetEntitySchema(this.ObjectContext.VehicleInformations.EntitySet, out vehicleNotNullableFields, out vehicleFieldLenght);

            List<string> resourceNotNullableFields;
            Dictionary<string, int> resourceFieldLenght;
            this.GetEntitySchema(this.ObjectContext.VehicleAvailableResources.EntitySet, out resourceNotNullableFields, out resourceFieldLenght);

            List<object> excelColumns;
            var vehicleInformations = new List<VehicleInformation>();
            var errorVehicleInformations = new List<VehicleInformation>();
            var updateVehicleInformations = new List<VehicleInformation>();
            var vehicleAvailableResources = new List<VehicleAvailableResource>();
            var errorVehicleAvailableResources = new List<VehicleAvailableResource>();
            var updateVehicleAvailableResources = new List<VehicleAvailableResource>();
            using(var excelOperator = new ExcelImport(Path.Combine(AttachmentFolder, GetImportFilePath(fileName)))) {
                excelOperator.AddColumnDataSource("产品编号", "ProductCode");
                excelOperator.AddColumnDataSource("VIN", "VIN");
                excelOperator.AddColumnDataSource("发动机号", "EngineSerialNumber");
                excelOperator.AddColumnDataSource("下线时间", "RolloutDate", typeof(DateTime));
                excelOperator.AddColumnDataSource("经销商编号", "DealerCode");

                excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();

                excelOperator.LoadExcelRow(row => {
                    var vehicleInformation = new VehicleInformation {
                        VIN = row["VIN"],
                        ProductCode = row["ProductCode"],
                        EngineSerialNumber = row["EngineSerialNumber"],
                        ProductionPlanSON = row["VIN"],
                        OldVIN = row["VIN"],
                        DealerCode = row["DealerCode"]
                    };
                    var vehicleAvailableResource = new VehicleAvailableResource {
                        VehicleWarehouseCode = "CAMSZJ-TJ",
                        VIN = row["VIN"],
                        ProductCode = row["ProductCode"]
                    };
                    vehicleInformation.VehicleAvailableResources.Add(vehicleAvailableResource);
                    var errorMsgs = new List<string>();
                    //VIN检查
                    var fieldIndex = vehicleNotNullableFields.IndexOf("VIN");
                    if(string.IsNullOrEmpty(vehicleInformation.VIN)) {
                        if(fieldIndex > -1)
                            errorMsgs.Add(ErrorStrings.ImpVehicleAvailableResource_Validation3);
                    } else {
                        if(Encoding.Default.GetByteCount(vehicleInformation.VIN) > vehicleFieldLenght["VIN"])
                            errorMsgs.Add(ErrorStrings.ImpVehicleAvailableResource_Validation4);
                    }
                    //产品编号检查
                    fieldIndex = vehicleNotNullableFields.IndexOf("PRODUCTCODE");
                    if(string.IsNullOrEmpty(vehicleInformation.ProductCode)) {
                        if(fieldIndex > -1)
                            errorMsgs.Add(ErrorStrings.ImpVehicleAvailableResource_Validation5);
                    } else {
                        if(Encoding.Default.GetByteCount(vehicleInformation.ProductCode) > vehicleFieldLenght["PRODUCTCODE"])
                            errorMsgs.Add(ErrorStrings.ImpVehicleAvailableResource_Validation6);
                    }
                    //发动机号检查
                    fieldIndex = vehicleNotNullableFields.IndexOf("ENGINESERIALNUMBER");
                    if(string.IsNullOrEmpty(vehicleInformation.EngineSerialNumber)) {
                        if(fieldIndex > -1)
                            errorMsgs.Add(ErrorStrings.ImpVehicleAvailableResource_Validation7);
                    } else {
                        if(Encoding.Default.GetByteCount(vehicleInformation.EngineSerialNumber) > vehicleFieldLenght["ENGINESERIALNUMBER"])
                            errorMsgs.Add(ErrorStrings.ImpVehicleAvailableResource_Validation8);
                    }
                    //下线时间检查
                    if(!string.IsNullOrEmpty(row["RolloutDate"])) {
                        vehicleInformation.RolloutDateStr = row["RolloutDate"];
                        DateTime rolloutDate;
                        if(DateTime.TryParse(row["RolloutDate"], out rolloutDate))
                            vehicleInformation.RolloutDate = rolloutDate;
                        else
                            errorMsgs.Add(ErrorStrings.ImpVehicleAvailableResource_Validation9);
                    }

                    if(errorMsgs.Count > 0) {
                        vehicleAvailableResource.ErrorMsgs = string.Join("; ", errorMsgs);
                        errorVehicleInformations.Add(vehicleInformation);
                        errorVehicleAvailableResources.Add(vehicleAvailableResource);
                    } else {
                        vehicleInformations.Add(vehicleInformation);
                        vehicleAvailableResources.Add(vehicleAvailableResource);
                    }
                    return false;
                });
                var productCode = vehicleInformations.Select(v => v.ProductCode.ToLower()).Distinct().ToArray();
                var products = this.ObjectContext.Products.Where(v => productCode.Contains(v.Code.ToLower()) && v.Status == (int)DcsMasterDataStatus.有效).Select(v => new {
                    v.Id,
                    v.Code,
                    v.Name
                }).ToArray();
                var productIds = products.Select(r => r.Id);
                var vehicleModelAffiProducts = this.ObjectContext.VehicleModelAffiProducts.Where(r => productIds.Contains(r.ProductId)).Select(r => new {
                    r.ProductId,
                    r.ProductCategoryCode,
                    r.ProductCategoryName
                }).ToArray();
                var vehicleWarehouseCodes = vehicleAvailableResources.Select(r => r.VehicleWarehouseCode.ToLower()).Distinct().ToArray();
                var vehicleWarehouses = this.ObjectContext.VehicleWarehouses.Where(r => vehicleWarehouseCodes.Contains(r.Code.ToLower())).Select(r => new {
                    r.Id,
                    r.Code,
                    r.Name
                }).ToArray();
                var vins = vehicleInformations.Select(r => r.VIN.ToLower()).Distinct().ToArray();
                var vCX5BUProducts = this.ObjectContext.VCX5BUProduct.Where(r => productCode.Contains(r.l4code.ToLower())).ToArray();
                var dbVehicleInformations = ObjectContext.VehicleInformations.Where(v => vins.Contains(v.VIN.ToLower())).ToArray();
                var dbVehicleAvailableResources = ObjectContext.VehicleAvailableResources.Where(v => vins.Contains(v.VIN.ToLower())).ToArray();
                for(var i = vehicleInformations.Count - 1; i >= 0; i--) {
                    var vCX5BUProduct = vCX5BUProducts.Where(r => r.l4code.ToLower() == vehicleInformations[i].ProductCode.ToLower()).ToArray();
                    if(!vCX5BUProduct.Any()) {
                        vehicleAvailableResources[i].ErrorMsgs = ErrorStrings.ImpVehicleAvailableResource_Validation26;
                        errorVehicleInformations.Add(vehicleInformations[i]);
                        errorVehicleAvailableResources.Add(vehicleAvailableResources[i]);
                    } else {
                        var product = products.SingleOrDefault(v => String.Compare(v.Code, vehicleInformations[i].ProductCode, StringComparison.OrdinalIgnoreCase) == 0);
                        if(product == null) {
                            vehicleAvailableResources[i].ErrorMsgs = string.Format(ErrorStrings.ImpVehicleAvailableResource_Validation16, vehicleInformations[i].ProductCode);
                            errorVehicleInformations.Add(vehicleInformations[i]);
                            errorVehicleAvailableResources.Add(vehicleAvailableResources[i]);
                        } else {
                            vehicleInformations[i].ProductId = product.Id;
                            vehicleInformations[i].ProductName = product.Name;
                            vehicleAvailableResources[i].ProductId = product.Id;
                            vehicleAvailableResources[i].ProductName = product.Name;
                            var vehicleModelAffiProduct = vehicleModelAffiProducts.FirstOrDefault(v => v.ProductId == product.Id);
                            if(vehicleModelAffiProduct == null) {
                                vehicleAvailableResources[i].ErrorMsgs = string.Format(ErrorStrings.ImpVehicleAvailableResource_Validation17, vehicleInformations[i].ProductCode);
                                errorVehicleInformations.Add(vehicleInformations[i]);
                                errorVehicleAvailableResources.Add(vehicleAvailableResources[i]);
                            } else {
                                vehicleInformations[i].ProductCategoryCode = vehicleModelAffiProduct.ProductCategoryCode;
                                vehicleInformations[i].ProductCategoryName = vehicleModelAffiProduct.ProductCategoryName;
                                vehicleAvailableResources[i].ProductCategoryCode = vehicleModelAffiProduct.ProductCategoryCode;
                                vehicleAvailableResources[i].ProductCategoryName = vehicleModelAffiProduct.ProductCategoryName;
                                var vehicleWarehouse = vehicleWarehouses.FirstOrDefault(v => String.Compare(v.Code, vehicleAvailableResources[i].VehicleWarehouseCode, StringComparison.OrdinalIgnoreCase) == 0);
                                if(vehicleWarehouse == null) {
                                    vehicleAvailableResources[i].ErrorMsgs = string.Format(ErrorStrings.ImpVehicleAvailableResource_Validation18, vehicleAvailableResources[i].VehicleWarehouseCode);
                                    errorVehicleInformations.Add(vehicleInformations[i]);
                                    errorVehicleAvailableResources.Add(vehicleAvailableResources[i]);
                                } else {
                                    vehicleAvailableResources[i].VehicleWarehouseId = vehicleWarehouse.Id;
                                    vehicleAvailableResources[i].VehicleWarehouseName = vehicleWarehouse.Name;
                                    var checkVehicleInformations = vehicleInformations.Except(new[] { vehicleInformations[i] }).Where(r => r.VIN.ToLower() == vehicleInformations[i].VIN.ToLower()).ToArray();
                                    if(checkVehicleInformations.Any()) {
                                        vehicleAvailableResources[i].ErrorMsgs = ErrorStrings.ImpVehicleAvailableResource_Validation23;
                                        errorVehicleInformations.Add(vehicleInformations[i]);
                                        errorVehicleAvailableResources.Add(vehicleAvailableResources[i]);
                                    } else {
                                        checkVehicleInformations = vehicleInformations.Except(new[] { vehicleInformations[i] }).Where(r => r.ProductionPlanSON.ToLower() == vehicleInformations[i].ProductionPlanSON.ToLower()).ToArray();
                                        if(checkVehicleInformations.Any()) {
                                            vehicleAvailableResources[i].ErrorMsgs = ErrorStrings.ImpVehicleAvailableResource_Validation2;
                                            errorVehicleInformations.Add(vehicleInformations[i]);
                                            errorVehicleAvailableResources.Add(vehicleAvailableResources[i]);
                                        } else {
                                            var updateVehicleInformation = dbVehicleInformations.FirstOrDefault(r => r.VIN.ToLower() == vehicleInformations[i].VIN.ToLower());
                                            if(updateVehicleInformation != null) {
                                                UpdateToDatabase(updateVehicleInformation);
                                                updateVehicleInformation.ModifierId = userInfo.Id;
                                                updateVehicleInformation.ModifierName = userInfo.Name;
                                                updateVehicleInformation.ModifyTime = DateTime.Now;
                                                updateVehicleInformation.EngineSerialNumber = vehicleInformations[i].EngineSerialNumber;
                                                updateVehicleInformation.RolloutDate = vehicleInformations[i].RolloutDate;
                                                updateVehicleInformations.Add(vehicleInformations[i]);
                                            }
                                            var updateVehicleAvailableResource = dbVehicleAvailableResources.FirstOrDefault(r => r.VIN.ToLower() == vehicleAvailableResources[i].VIN.ToLower());
                                            if(updateVehicleAvailableResource != null) {
                                                UpdateToDatabase(updateVehicleAvailableResource);
                                                updateVehicleAvailableResource.ModifierId = userInfo.Id;
                                                updateVehicleAvailableResource.ModifierName = userInfo.Name;
                                                updateVehicleAvailableResource.ModifyTime = DateTime.Now;
                                                updateVehicleAvailableResource.IsAvailable = vehicleAvailableResources[i].IsAvailable;
                                                updateVehicleAvailableResources.Add(vehicleAvailableResources[i]);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            foreach(var vehicleInformation in vehicleInformations.Except(errorVehicleInformations).Except(updateVehicleInformations)) {
                vehicleInformation.BranchId = GlobalVar.DEFAULT_VEHICLEINFORMATION_BRANCHID;
                vehicleInformation.BranchCode = GlobalVar.DEFAULT_VEHICLEINFORMATION_BRANCHCODE;
                vehicleInformation.BranchName = GlobalVar.DEFAULT_VEHICLEINFORMATION_BRANCHNAME;
                vehicleInformation.VehicleType = (int)DcsVehicleInformationVehicleType.进口;
                vehicleInformation.VehicleCategoryId = GlobalVar.DEFAULT_DEALERVEHICLEMONTHQUOTA_VEHICLESALESORGID;
                vehicleInformation.VehicleCategoryName = GlobalVar.DEFAULT_DEALERVEHICLEMONTHQUOTA_VEHICLESALESORGNAME;
                vehicleInformation.Status = (int)DcsVehicleStatus.本部仓库;
                vehicleInformation.CreatorId = userInfo.Id;
                vehicleInformation.CreatorName = userInfo.Name;
                vehicleInformation.CreateTime = DateTime.Now;
                this.InsertToDatabase(vehicleInformation);
            }
            foreach(var vehicleAvailableResource in vehicleAvailableResources.Except(errorVehicleAvailableResources).Except(updateVehicleAvailableResources)) {
                vehicleAvailableResource.CreatorId = userInfo.Id;
                vehicleAvailableResource.CreatorName = userInfo.Name;
                vehicleAvailableResource.CreateTime = DateTime.Now;
                vehicleAvailableResource.BranchId = GlobalVar.DEFAULT_VEHICLEAVAILABLERESOURCE_BRANCHID;
                vehicleAvailableResource.BranchCode = GlobalVar.DEFAULT_VEHICLEAVAILABLERESOURCE_BRANCHCODE;
                vehicleAvailableResource.BranchName = GlobalVar.DEFAULT_VEHICLEAVAILABLERESOURCE_BRANCHNAME;
                vehicleAvailableResource.QualityStatus = (int)DcsVehicleAvailableResourceQualityStatus.合格;
                vehicleAvailableResource.LockStatus = (int)DcsVehicleAvailableResourceLockStatus.未锁定;
                vehicleAvailableResource.IsAvailable = true;
                this.InsertToDatabase(vehicleAvailableResource);
            }
            this.ObjectContext.SaveChanges();
            if(!errorVehicleInformations.Any() || !errorVehicleAvailableResources.Any())
                return string.Empty;
            excelColumns.Add("校验信息");
            var errorDataFilePath = Path.Combine(AttachmentFolder, fileName);
            using(var excelExport = new ExcelExport(Path.Combine(AttachmentFolder, errorDataFilePath))) {
                excelExport.ExportByRow(index => {
                    if(index == errorVehicleInformations.Count + 1)
                        return null;
                    if(index == 0)
                        return excelColumns.ToArray();
                    var vehicleInformation = errorVehicleInformations[index - 1];
                    var vehicleAvailableResource = errorVehicleAvailableResources[index - 1];
                    var values = new object[] {
                                vehicleInformation.ProductCode,vehicleInformation.VIN,vehicleInformation.EngineSerialNumber,vehicleInformation.RolloutDateStr,vehicleInformation.DealerCode,vehicleAvailableResource.ErrorMsgs
                            };
                    return values;
                });
            }
            return errorDataFilePath;
        }
    }
}
