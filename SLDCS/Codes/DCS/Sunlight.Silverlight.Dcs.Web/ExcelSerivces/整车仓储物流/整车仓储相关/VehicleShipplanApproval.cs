﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Sunlight.Silverlight.Dcs.Web.ExcelOperator;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        [Invoke]
        public string 按车型导出发车审批单(string dealerName, string productCode, string productCategoryCode, int? vehicleFundsTypeId, DateTime? approveTimeStart, DateTime? approveTimeEnd, DateTime? creatTimeStart, DateTime? CreatTimeEnd) {
            var excelColumns = new List<object>();
            var resultsql = from vehicleShipplanApproval in ObjectContext.VehicleShipplanApprovals
                            from vehicleShipplanApprovalDetail in ObjectContext.VehicleShipplanApprovalDetails
                            from productAffiProductCategory in ObjectContext.ProductAffiProductCategories
                            from productCategory in ObjectContext.ProductCategories
                            from vehicleFundsType in ObjectContext.VehicleFundsTypes
                            where vehicleShipplanApproval.Id == vehicleShipplanApprovalDetail.VehicleShipplanApprovalId && vehicleShipplanApproval.Status != (int)DcsVehicleShipplanApprovalStatus.作废
                            && vehicleShipplanApprovalDetail.Status != (int)DcsVehicleShipplanApprovalDetailStatus.作废
                            && vehicleShipplanApprovalDetail.ProductId == productAffiProductCategory.ProductId
                            && productCategory.Id == productAffiProductCategory.ProductCategoryId
                            && vehicleShipplanApproval.VehicleFundsTypeId == vehicleFundsType.Id && vehicleFundsType.Status != (int)DcsBaseDataStatus.作废
                            select new {
                                VehicleShipplanApprovalCode = vehicleShipplanApproval.Code,
                                vehicleShipplanApproval.DealerCode,
                                vehicleShipplanApproval.DealerName,
                                vehicleShipplanApproval.VehicleFundsTypeId,
                                vehicleShipplanApproval.ApproveTime,
                                vehicleShipplanApproval.CreateTime,
                                vehicleShipplanApprovalDetail.VIN,
                                vehicleShipplanApprovalDetail.ProductCode,
                                vehicleShipplanApprovalDetail.ProductCategoryCode,
                                CategoryCode = productCategory.Code,
                                vehicleShipplanApproval.Status,
                                VehicleFundsTypeName = vehicleFundsType.Name,
                                vehicleShipplanApproval.Amount
                            };
            if(!string.IsNullOrEmpty(productCode))
                resultsql = resultsql.Where(r => r.ProductCode.ToLower().Contains(productCode.ToLower()));
            if(!string.IsNullOrEmpty(dealerName))
                resultsql = resultsql.Where(r => r.DealerName.ToLower().Contains(dealerName.ToLower()));
            if(!string.IsNullOrEmpty(productCategoryCode))
                resultsql = resultsql.Where(r => r.CategoryCode.ToLower().Contains(productCategoryCode.ToLower()));
            if(vehicleFundsTypeId.HasValue)
                resultsql = resultsql.Where(r => r.VehicleFundsTypeId == vehicleFundsTypeId.Value);
            if(approveTimeStart.HasValue)
                resultsql = resultsql.Where(r => r.ApproveTime.Value >= approveTimeStart);
            if(approveTimeEnd.HasValue)
                resultsql = resultsql.Where(r => r.ApproveTime.Value <= approveTimeEnd);
            if(creatTimeStart.HasValue)
                resultsql = resultsql.Where(r => r.CreateTime.Value >= creatTimeStart);
            if(CreatTimeEnd.HasValue)
                resultsql = resultsql.Where(r => r.CreateTime.Value <= CreatTimeEnd);
            var details = resultsql.ToArray();
            var dataFileName = "";
            if(details.Count() > 0) {
                excelColumns.Add("经销商编号");
                excelColumns.Add("经销商名称");
                excelColumns.Add("资金类型");
                excelColumns.Add("审核日期");
                excelColumns.Add("发车审核金额");
                excelColumns.Add("VIN");
                excelColumns.Add("产品编号");
                excelColumns.Add("车型编号");
                excelColumns.Add("车型大类");
                dataFileName = GetExportFilePath("发车审批单.xls");
                using(var excelExport = new ExcelExport(Path.Combine(AttachmentFolder, dataFileName))) {
                    excelExport.ExportByRow(index => {
                        if(index == details.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray();
                        var detail = details[index - 1];
                        var values = new object[] {
                            detail.DealerCode, detail.DealerName, detail.VehicleFundsTypeName, detail.ApproveTime,detail.Amount, detail.VIN, detail.ProductCode, detail.ProductCategoryCode, detail.CategoryCode
                        };
                        return values;
                    });
                }
            }
            return dataFileName;
        }
    }
}
