﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web
{
    partial class DcsDomainService
    {
        public string ExportAgencyDealerRelationQuery(string agencyCode, string agencyName, string dealerCode, string dealerName, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, string salesRegionName, string MarketingDepartmentName)
        {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("中心库与经销商隶属关系_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var agencyDealerRelation = GetAgencyDealerRelationsWithBranch(salesRegionName, MarketingDepartmentName);


            if (!string.IsNullOrEmpty(agencyCode))
            {
                agencyDealerRelation = agencyDealerRelation.Where(r => r.Company.Code.Contains(agencyCode));
            }
            if (!string.IsNullOrEmpty(agencyName))
            {
                agencyDealerRelation = agencyDealerRelation.Where(r => r.Company.Name.Contains(agencyName));
            }
            if (!string.IsNullOrEmpty(dealerCode))
            {
                agencyDealerRelation = agencyDealerRelation.Where(r => r.Company1.Code.Contains(dealerCode));
            }
            if (!string.IsNullOrEmpty(dealerName))
            {
                agencyDealerRelation = agencyDealerRelation.Where(r => r.Company1.Name.Contains(dealerName));
            }
            if (status.HasValue)
            {
                agencyDealerRelation = agencyDealerRelation.Where(r => r.Status == status.Value);
            }
            if (createTimeBegin.HasValue)
            {
                agencyDealerRelation = agencyDealerRelation.Where(r => r.CreateTime >= createTimeBegin);
            }
            if (createTimeEnd.HasValue)
            {
                agencyDealerRelation = agencyDealerRelation.Where(r => r.CreateTime <= createTimeEnd);
            }
            var DealeresArray = agencyDealerRelation.ToArray();
            if (agencyDealerRelation.Any())
            {

                excelColumns.Add(ErrorStrings.ExportColumn_Branch);
                excelColumns.Add(ErrorStrings.ExportColumn_AgencyCode);
                excelColumns.Add(ErrorStrings.ExportColumn_AgencyName);
                excelColumns.Add(ErrorStrings.ExportColumn_PartsSalesCategory);
                excelColumns.Add(ErrorStrings.ExportColumn_DealerCode);
                excelColumns.Add(ErrorStrings.ExportColumn_DealerName);
                excelColumns.Add(ErrorStrings.ExportColumn_Remark);
                excelColumns.Add(ErrorStrings.ExportColumn_Status);
                excelColumns.Add(ErrorStrings.ExportColumn_Creator);
                excelColumns.Add(ErrorStrings.ExportColumn_CreateTime);
                excelColumns.Add(ErrorStrings.ExportColumn_Modifier);
                excelColumns.Add(ErrorStrings.ExportColumn_ModifyTime);
                excelColumns.Add(ErrorStrings.ExportColumn_Abandoner);
                excelColumns.Add(ErrorStrings.ExportColumn_AbandonTime);

                using (var excelExport = new ExcelExport(exportDataFileName))
                {
                    excelExport.ExportByRow(index =>
                    {
                        if (index == DealeresArray.Count() + 1)
                            return null;
                        if (index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = DealeresArray[index - 1];
                        var values = new object[] {
                            detail.Branch==null?null:detail.Branch.Name,
                            detail.Company==null?null:detail.Company.Code,
                            detail.Company==null?null:detail.Company.Name,
                            detail.PartsSalesCategory==null?null:detail.PartsSalesCategory.Name,
                            detail.Company1==null?null:detail.Company1.Code,
                            detail.Company1==null?null:detail.Company1.Name,
                            detail.Remark,
                            detail.Status,                        
                            detail.CreatorName,
                            detail.CreateTime,
                            detail.ModifierName,
                            detail.ModifyTime,
                            detail.AbandonerName,
                            detail.AbandonTime
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
    }
}

