﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string ExportAuthenticationType(int[] ids, string authenticationTypeName, int? status, string creatorName, DateTime? createTimeStart, DateTime? createTimeEnd) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("服务培训认证类型_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var AuthenticationTypes = from a in ObjectContext.AuthenticationTypes select a;

            if(ids != null && ids.Length > 0) {
                AuthenticationTypes = AuthenticationTypes.Where(r => ids.Contains(r.Id));
            } else {
                if(!string.IsNullOrEmpty(authenticationTypeName)) {
                    AuthenticationTypes = AuthenticationTypes.Where(r => r.AuthenticationTypeName.Contains(authenticationTypeName));
                }
                if(status.HasValue) {
                    AuthenticationTypes = AuthenticationTypes.Where(r => r.Status == status);
                }
                if(!string.IsNullOrEmpty(creatorName)) {
                    AuthenticationTypes = AuthenticationTypes.Where(r => r.CreatorName == creatorName);
                }
                if(createTimeStart.HasValue) {
                    AuthenticationTypes = AuthenticationTypes.Where(r => r.CreateTime >= createTimeStart);
                }
                if(createTimeEnd.HasValue) {
                    AuthenticationTypes = AuthenticationTypes.Where(r => r.CreateTime <= createTimeEnd);
                }
            }

            var DealeresArray = AuthenticationTypes.ToArray();
            if(AuthenticationTypes.Any()) {
                excelColumns.Add("培训认证类型编码");
                excelColumns.Add("培训认证类型名称");
                excelColumns.Add(ErrorStrings.ExportColumn_Status);
                excelColumns.Add("有效期");
                excelColumns.Add(ErrorStrings.ExportColumn_Creator);
                excelColumns.Add(ErrorStrings.ExportColumn_CreateTime);
                excelColumns.Add(ErrorStrings.ExportColumn_Modifier);
                excelColumns.Add(ErrorStrings.ExportColumn_ModifyTime);
                excelColumns.Add(ErrorStrings.ExportColumn_Abandoner);
                excelColumns.Add(ErrorStrings.ExportColumn_AbandonTime);

                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == DealeresArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = DealeresArray[index - 1];
                        var values = new object[] {
                            detail.AuthenticationTypeCode,
                            detail.AuthenticationTypeName,
                            Enum.GetName(typeof(DcsBaseDataStatus),detail.Status),
                            (Enum.GetName(typeof(DcsValiDate),detail.ValDate)??"").Replace("_",""),
                            detail.CreatorName,
                            detail.CreateTime,
                            detail.ModifyName,
                            detail.ModifyTime,
                            detail.CancelName,
                            detail.CancelTime
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
    }
}
