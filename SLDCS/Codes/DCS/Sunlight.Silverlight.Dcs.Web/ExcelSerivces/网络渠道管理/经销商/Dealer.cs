﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string ExportDealer(int? id, string code, string name, int? status, DateTime? createTimeStart, DateTime? createTimeEnd) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("服务站经销商基本信息_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var Dealeres = GetDealersWithCompanyInvoiceInfoAndDealerServiceExt();

            if(id.HasValue) {
                Dealeres = Dealeres.Where(r => r.Id == id.Value);
            } else {
                if(!string.IsNullOrEmpty(code)) {
                    Dealeres = Dealeres.Where(r => r.Code.Contains(code));
                }
                if(!string.IsNullOrEmpty(name)) {
                    Dealeres = Dealeres.Where(r => r.Name.Contains(name));
                }
                if(status.HasValue) {
                    Dealeres = Dealeres.Where(r => r.Status == status.Value);
                }
                if(createTimeStart.HasValue) {
                    Dealeres = Dealeres.Where(r => r.CreateTime >= createTimeStart);
                }
                if(createTimeEnd.HasValue) {
                    Dealeres = Dealeres.Where(r => r.CreateTime <= createTimeEnd);
                }
            }

            var DealeresArray = Dealeres.ToArray();
            if(Dealeres.Any()) {
                excelColumns.Add(ErrorStrings.Dealer_Validation7);
                excelColumns.Add(ErrorStrings.Dealer_Validation8);
                excelColumns.Add(ErrorStrings.ExportColumn_ShortName);
                excelColumns.Add(ErrorStrings.ExportColumn_Manager46);
                //excelColumns.Add("MDM客户编码");
                //excelColumns.Add("MDM供应商编码");
                //excelColumns.Add("成立日期");
                excelColumns.Add(ErrorStrings.ExportColumn_ProvinceName);
                excelColumns.Add(ErrorStrings.ExportColumn_RegionName);
                excelColumns.Add(EntityStrings.Company_CountyName);
                excelColumns.Add(EntityStrings.DealerHistory_CityLevel);
                excelColumns.Add(ErrorStrings.ExportColumn_ContactPerson);
                excelColumns.Add(EntityStrings.DealerHistory_ContactPhone);
                excelColumns.Add("E-MAIL");
                excelColumns.Add(EntityStrings.DealerHistory_ContactMobile);
                excelColumns.Add(EntityStrings.DealerHistory_CompanyFax);
                excelColumns.Add(EntityStrings.DealerHistory_PostCode);
                excelColumns.Add(EntityStrings.Company_BusinessAddress);
                excelColumns.Add(EntityStrings.SubDealer_BusinessAddressLongitude);
                excelColumns.Add(EntityStrings.SubDealer_BusinessAddressLatitude);
                excelColumns.Add(EntityStrings.DealerHistory_Distance);
                excelColumns.Add(EntityStrings.Export_Validation_Dealer_Validation56);
                excelColumns.Add(EntityStrings.Export_Validation_Dealer_Validation57);
                excelColumns.Add(EntityStrings.Company_RegisterCode);
                excelColumns.Add(EntityStrings.Company_RegisterName);
                excelColumns.Add(EntityStrings.Company_RegisterDate);
                excelColumns.Add(EntityStrings.Company_RegisterCapital);
                excelColumns.Add(EntityStrings.DealerHistory_FixedAsset);
                excelColumns.Add(ErrorStrings.Export_Title_Agency_LegalRepresentative);
                excelColumns.Add(EntityStrings.DealerHistory_LegalRepresentTel);
                excelColumns.Add(EntityStrings.Company_CorporateNature);
                excelColumns.Add(EntityStrings.DealerHistory_BusinessScope);
                excelColumns.Add(EntityStrings.DealerHistory_RegisteredAddress);
                excelColumns.Add(EntityStrings.Company_IdDocumentType);
                excelColumns.Add(EntityStrings.DealerHistory_IdDocumentNumber);
                excelColumns.Add(EntityStrings.DealerHistory_RepairQualification);
                excelColumns.Add(EntityStrings.DealerServiceExt_OwnerCompany);
                excelColumns.Add(EntityStrings.DealerHistory_BuildTime);
                excelColumns.Add(EntityStrings.DealerHistory_TrafficRestrictionsdescribe);
                excelColumns.Add(EntityStrings.DealerServiceExt_MainBusinessAreas);
                excelColumns.Add(EntityStrings.DealerHistory_AndBusinessAreas);
                //excelColumns.Add("红岩授权品牌");
                //excelColumns.Add("非红岩授权品牌");
                excelColumns.Add(EntityStrings.DealerHistory_DangerousRepairQualification);
                excelColumns.Add(EntityStrings.DealerHistory_HasBranch);
                excelColumns.Add(EntityStrings.DealerServiceExt_VehicleTravelRoute);
                excelColumns.Add(EntityStrings.DealerServiceExt_VehicleUseSpeciality);
                excelColumns.Add(EntityStrings.DealerServiceExt_VehicleDockingStations);
                excelColumns.Add(EntityStrings.Dealer_SatationPeople);
                excelColumns.Add(EntityStrings.DealerServiceExt_RepairingArea);
                excelColumns.Add(EntityStrings.DealerServiceExt_ReceptionRoomArea);
                excelColumns.Add(EntityStrings.DealerServiceExt_ParkingArea);
                excelColumns.Add(EntityStrings.DealerServiceExt_PartWarehouseArea);
                excelColumns.Add(EntityStrings.DealerHistory_TaxRegisteredNumber);
                excelColumns.Add(EntityStrings.DealerHistory_TaxpayerQualification);
                excelColumns.Add(EntityStrings.DealerHistory_InvoiceAmountQuota);
                excelColumns.Add(EntityStrings.DealerHistory_InvoiceTitle);
                excelColumns.Add(EntityStrings.DealerHistory_InvoiceType);
                excelColumns.Add(EntityStrings.DealerHistory_BankName);
                excelColumns.Add(EntityStrings.CompanyInvoiceInfo_InvoiceTax);
                excelColumns.Add(EntityStrings.CompanyInvoiceInfo_BankAccount);
                excelColumns.Add(EntityStrings.DealerHistory_Linkman);
                excelColumns.Add(EntityStrings.DealerHistory_ContactNumber);
                excelColumns.Add(EntityStrings.CompanyInvoiceInfo_Fax);
                excelColumns.Add(EntityStrings.DealerHistory_TaxRegisteredAddress);
                excelColumns.Add(EntityStrings.DealerHistory_BankName);
                excelColumns.Add(ErrorStrings.ExportColumn_Status);
                excelColumns.Add(ErrorStrings.ExportColumn_Creator);
                excelColumns.Add(ErrorStrings.ExportColumn_CreateTime);
                excelColumns.Add(ErrorStrings.ExportColumn_Modifier);
                excelColumns.Add(ErrorStrings.ExportColumn_ModifyTime);

                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == DealeresArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = DealeresArray[index - 1];
                        var values = new object[] {
                            detail.Code,
                            detail.Company.Name,
                            detail.ShortName,
                            detail.Manager,
                            //detail.Company.CustomerCode,
                            //detail.Company.SupplierCode,
                            // detail.Company.FoundDate,
                            detail.Company.ProvinceName,
                            detail.Company.CityName,
                            detail.Company.CountyName,
                            detail.Company.CityLevel==null?null: (Enum.GetName(typeof(DcsCompanyCityLevel),detail.Company.CityLevel)),
                            detail.Company.ContactPerson,
                            detail.Company.ContactPhone, 
                            detail.Company.ContactMail,
                            detail.Company.ContactMobile,
                             detail.Company.Fax,
                            detail.Company.ContactPostCode, 
                            detail.Company.BusinessAddress,
                            detail.Company.BusinessAddressLongitude,
                            detail.Company.BusinessAddressLatitude,
                            detail.Company.Distance,
                            detail.Company.MinServiceCode,
                            detail.Company.MinServiceName,
                            detail.Company.RegisterCode,
                            detail.Company.RegisterName,
                            detail.Company.RegisterDate,
                            detail.Company.RegisterCapital,
                            detail.Company.FixedAsset,
                            detail.Company.LegalRepresentative,
                            detail.Company.LegalRepresentTel,
                            detail.Company.CorporateNature,
                            detail.Company.BusinessScope,
                            detail.Company.RegisteredAddress,
                            
                            detail.Company.IdDocumentType==null?null: Enum.GetName(typeof(DcsCustomerIdDocumentType),detail.Company.IdDocumentType),
                            detail.Company.IdDocumentNumber,
    
                          detail.DealerServiceExt==null?null: detail.DealerServiceExt.RepairQualification==null?null: Enum.GetName(typeof(DcsRepairQualificationGrade),detail.DealerServiceExt.RepairQualification),
                            detail.DealerServiceExt==null?null: detail.DealerServiceExt.OwnerCompany,
                           detail.DealerServiceExt==null?null: detail.DealerServiceExt.BuildTime,
                            detail.DealerServiceExt==null?null: detail.DealerServiceExt.TrafficRestrictionsdescribe,
                            detail.DealerServiceExt==null?null:  detail.DealerServiceExt.MainBusinessAreas,
                            detail.DealerServiceExt==null?null: detail.DealerServiceExt.AndBusinessAreas,
                           // detail.DealerServiceExt==null?null:detail.DealerServiceExt.BrandScope,
                           //detail.DealerServiceExt==null?null: detail.DealerServiceExt.CompetitiveBrandScope,
                           detail.DealerServiceExt==null?null:detail.DealerServiceExt.DangerousRepairQualification==null?null:Enum.GetName(typeof(DcsYesOrNo),detail.DealerServiceExt.DangerousRepairQualification),
                          detail.DealerServiceExt==null?null: detail.DealerServiceExt.HasBranch==null?null:  Enum.GetName(typeof(DcsYesOrNo),detail.DealerServiceExt.HasBranch),
                          detail.DealerServiceExt==null?null: detail.DealerServiceExt.VehicleTravelRoute==null?null: Enum.GetName(typeof(DcsDealerServiceExtVehicleTravelRoute),detail.DealerServiceExt.VehicleTravelRoute),
                           detail.DealerServiceExt==null?null: detail.DealerServiceExt.VehicleUseSpeciality,
                          detail.DealerServiceExt==null?null:   detail.DealerServiceExt.VehicleDockingStation,
                            detail.DealerServiceExt==null?null: detail.DealerServiceExt.EmployeeNumber,
                           detail.DealerServiceExt==null?null: detail.DealerServiceExt.RepairingArea,
                          detail.DealerServiceExt==null?null:  detail.DealerServiceExt.ReceptionRoomArea,
                           detail.DealerServiceExt==null?null: detail.DealerServiceExt.ParkingArea,
                           detail.DealerServiceExt==null?null: detail.DealerServiceExt.PartWarehouseArea,
                           // detail.Company.CompanyInvoiceInfo.TaxRegisteredNumber,
                        // detail.CompanyInvoiceInfo.Count()>0?null: detail.CompanyInvoiceInfo.FirstOrDefault().TaxRegisteredNumber),
                          
                        //detail.CompanyInvoiceInfo.Count()>0?null: detail.CompanyInvoiceInfo.FirstOrDefault().TaxpayerQualification==null?null: Enum.GetName(typeof(DcsPartsSupplierTaxpayerKind), detail.CompanyInvoiceInfo.FirstOrDefault().TaxpayerQualification),
                        //     detail.CompanyInvoiceInfo.Count()>0?null: detail.CompanyInvoiceInfo.FirstOrDefault().InvoiceAmountQuota,
                        //    detail.CompanyInvoiceInfo.Count()>0?null: detail.CompanyInvoiceInfo.FirstOrDefault().InvoiceTitle,
                        //  detail.CompanyInvoiceInfo.Count()>0?null: Enum.GetName(typeof(DcsInvoiceType), detail.CompanyInvoiceInfo.FirstOrDefault().InvoiceType),
                        //    detail.CompanyInvoiceInfo.Count()>0?null:detail.CompanyInvoiceInfo.FirstOrDefault().BankName,
                        //    detail.CompanyInvoiceInfo.Count()>0?null: detail.CompanyInvoiceInfo.FirstOrDefault().InvoiceTax,
                        //    detail.CompanyInvoiceInfo.Count()>0?null: detail.CompanyInvoiceInfo.FirstOrDefault().BankAccoun,
                        //   detail.CompanyInvoiceInfo.Count()>0?null: detail.CompanyInvoiceInfo.FirstOrDefault().Linkman,
                        //   detail.CompanyInvoiceInfo.Count()>0?null: detail.CompanyInvoiceInfo.FirstOrDefault().ContactNumber,
                        //   detail.CompanyInvoiceInfo.Count()>0?null: detail.CompanyInvoiceInfo.FirstOrDefault().Fax,
                        //    detail.CompanyInvoiceInfo.Count()>0?null:detail.CompanyInvoiceInfo.FirstOrDefault().TaxRegisteredAddress,
                        null,null,null,null,null, null,null,null,null,null,
                         null,null,null,null,null, null,null,null,null,null,
                          null,null,null,null,null, null,null,null,null,null,
                           Enum.GetName(typeof(DcsMasterDataStatus),detail.Status),
                            detail.CreatorName,
                            detail.CreateTime,
                            detail.ModifierName,
                            detail.ModifyTime
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }


        public string ExportDealerKeyEmployee(int[] ids, int? branchId, int? partsSalesCategoryId, string dealerCode, string dealerName, int? keyPositionId, int? isOnJob, bool? isKeyPositions, string name, string dptname, DateTime? createtimeBegin, DateTime? createtimeEnd) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("服务站/经销商关键岗位人员信息_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var DealerKeyEmployeequery = this.GetDealerKeyEmployeesWithDetailsByMarketDtpName(dptname);

            if(ids != null && ids.Any()) {
                DealerKeyEmployeequery = DealerKeyEmployeequery.Where(r => ids.Contains(r.Id));
            }

            if(branchId.HasValue) {
                DealerKeyEmployeequery = DealerKeyEmployeequery.Where(r => r.DealerKeyPosition.Branch.Id == branchId);
            }
            if(partsSalesCategoryId.HasValue) {
                DealerKeyEmployeequery = DealerKeyEmployeequery.Where(r => r.PartsSalesCategory.Id == partsSalesCategoryId);
            }
            if(!string.IsNullOrEmpty(dealerCode)) {
                DealerKeyEmployeequery = DealerKeyEmployeequery.Where(r => r.Dealer.Code.ToLower().Contains(dealerCode.ToLower()));
            }
            if(!string.IsNullOrEmpty(dealerName)) {
                DealerKeyEmployeequery = DealerKeyEmployeequery.Where(r => r.Dealer.Name.ToLower().Contains(dealerName.ToLower()));
            }
            if(keyPositionId.HasValue) {
                DealerKeyEmployeequery = DealerKeyEmployeequery.Where(r => r.DealerKeyPosition.Id == keyPositionId);
            }
            if(isOnJob.HasValue) {
                DealerKeyEmployeequery = DealerKeyEmployeequery.Where(r => r.IsOnJob == isOnJob);
            }
            if(isKeyPositions.HasValue) {
                DealerKeyEmployeequery = DealerKeyEmployeequery.Where(r => r.IsKeyPositions == isKeyPositions);
            }
            if(!string.IsNullOrEmpty(name)) {
                DealerKeyEmployeequery = DealerKeyEmployeequery.Where(r => r.Name.ToLower().Contains(name.ToLower()));
            }
            if(createtimeBegin.HasValue) {
                DealerKeyEmployeequery = DealerKeyEmployeequery.Where(r => r.CreateTime >= createtimeBegin);
            }
            if(createtimeEnd.HasValue) {
                DealerKeyEmployeequery = DealerKeyEmployeequery.Where(r => r.CreateTime <= createtimeEnd);
            }


            var dealerKeyEmployeequerys = from dke in DealerKeyEmployeequery
                                          select new {
                                              //BranchName=dke.DealerKeyPosition.Branch.Name,
                                              PartsSalesCategoryName = dke.PartsSalesCategory.Name,
                                              DealerCode = dke.Dealer.Code,
                                              DealerName = dke.Dealer.Name,
                                              PositionName = dke.DealerKeyPosition.PositionName,
                                              Responsibility = dke.DealerKeyPosition.Responsibility,
                                              Name = dke.Name,
                                              MobileNumber = dke.MobileNumber,
                                              PhoneNumber = dke.PhoneNumber,
                                              MarketingDepartmentName = dke.MarketingDepartment.Name,
                                              Sex = dke.Sex,
                                              IdCardNumber = dke.IdCardNumber,
                                              School = dke.School,
                                              Education = dke.Education,
                                              Professional = dke.Professional,
                                              ProfessionalRank = dke.ProfessionalRank,
                                              EntryTime = dke.EntryTime,
                                              IsOnJob = dke.IsOnJob,
                                              LeaveTime = dke.LeaveTime,
                                              WorkType = dke.WorkType,
                                              SkillLevel = dke.SkillLevel,
                                              DealerKeyPositionPositionName = dke.DealerKeyPosition.PositionName,
                                              TransferTime = dke.TransferTime,
                                              IsKeyPositions = dke.IsKeyPositions,
                                              Mail = dke.Mail,
                                              Remark = dke.Remark,
                                              CreateTime = dke.CreateTime,
                                              CreatorName = dke.CreatorName
                                          };

            var DealeresArray = dealerKeyEmployeequerys.ToArray();

            if(DealeresArray.Any()) {
                //excelColumns.Add(ErrorStrings.ExportColumn_Branch);
                excelColumns.Add("品牌");
                excelColumns.Add(ErrorStrings.Dealer_Validation7);
                excelColumns.Add(ErrorStrings.Dealer_Validation8);
                excelColumns.Add("岗位名称");
                excelColumns.Add("职责描述");
                excelColumns.Add("姓名");
                excelColumns.Add("手机号码");
                excelColumns.Add("固定电话");
                excelColumns.Add("市场部");
                excelColumns.Add("性别");
                excelColumns.Add("身份证号");
                excelColumns.Add("学校");
                excelColumns.Add("学历");
                excelColumns.Add("专业");
                excelColumns.Add("社会职称");
                excelColumns.Add("入职时间");
                excelColumns.Add("是否在职");
                excelColumns.Add("离职时间");
                excelColumns.Add("工种");
                excelColumns.Add("技能等级");
                excelColumns.Add("内部调岗前岗位");
                excelColumns.Add("调岗时间");
                excelColumns.Add("是否关键岗位");
                excelColumns.Add("邮箱");
                excelColumns.Add(ErrorStrings.ExportColumn_Remark);
                excelColumns.Add(ErrorStrings.ExportColumn_CreateTime);
                excelColumns.Add(ErrorStrings.ExportColumn_Creator);

                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == DealeresArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = DealeresArray[index - 1];
                        var values = new object[] {
                            //detail.BranchName,
                            detail.PartsSalesCategoryName,
                            detail.DealerCode,
                            detail.DealerName,
                            detail.PositionName,
                            detail.Responsibility,
                            detail.Name,
                            detail.MobileNumber,
                            detail.PhoneNumber,
                            detail.MarketingDepartmentName,
                            detail.Sex==null?"":Enum.GetName(typeof(DcsSexType),detail.Sex),
							detail.IdCardNumber,
							detail.School,
							detail.Education==null?"":Enum.GetName(typeof(DCSEducation),detail.Education),
							detail.Professional,
							detail.ProfessionalRank==null?"":Enum.GetName(typeof(DcsSocialTitle),detail.ProfessionalRank),
							detail.EntryTime,
							detail.IsOnJob==null?"":Enum.GetName(typeof(DcsIsOrNot),detail.IsOnJob),
							detail.LeaveTime,
							detail.WorkType==null?"":Enum.GetName(typeof(DcsWorkType),detail.WorkType),
							detail.SkillLevel==null?"":Enum.GetName(typeof(DCSSkillLevel),detail.SkillLevel),
							detail.DealerKeyPositionPositionName,
							detail.TransferTime,
                            detail.IsKeyPositions==null?"":detail.IsKeyPositions==true?EntityStrings.Export_Title_Yes:EntityStrings.Export_Title_No,
                            detail.Mail,
                            detail.Remark,
                            detail.CreateTime,
                            detail.CreatorName
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
    }
}

