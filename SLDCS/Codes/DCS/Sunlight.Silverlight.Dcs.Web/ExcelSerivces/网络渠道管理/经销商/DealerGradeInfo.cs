﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Dcs.Web.Resources;
using System.Text;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string ExportDealerGradeInfo(int[] ids, int? PartsSalesCategoryId, string gradename, int? status, DateTime? createtimeBegin, DateTime? createtimeEnd) {
            var exportDataFileName = GetExportFilePath(string.Format("品牌分级信息{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var DealerGradeInfos = GetDealerGradeInfoes();
            if(ids != null && ids.Length > 0) {
                DealerGradeInfos = DealerGradeInfos.Where(r => ids.Contains(r.Id));
            } else {
                if(PartsSalesCategoryId.HasValue) {
                    DealerGradeInfos = DealerGradeInfos.Where(r => r.PartsSalesCategoryId == PartsSalesCategoryId.Value);
                }
                //if(!string.IsNullOrEmpty(grade)) {
                //    BrandGradeMessages = BrandGradeMessages.Where(r => r.Grade==grade);
                //}
                if(status.HasValue) {
                    DealerGradeInfos = DealerGradeInfos.Where(r => r.Status == status);
                }
                if(createtimeBegin != null) {
                    DealerGradeInfos = DealerGradeInfos.Where(r => r.CreateTime >= createtimeBegin);
                }
                if(createtimeEnd != null) {
                    DealerGradeInfos = DealerGradeInfos.Where(r => r.CreateTime <= createtimeEnd);
                }
            }
            var BrandGradeMessagesArray = DealerGradeInfos.ToArray();
            if(BrandGradeMessagesArray.Any()) {
                var excelColumns = new List<string>();
                excelColumns.Add("品牌");
                excelColumns.Add("分级");
                excelColumns.Add(ErrorStrings.ExportColumn_Remark);
                excelColumns.Add(ErrorStrings.ExportColumn_Status);
                excelColumns.Add(ErrorStrings.ExportColumn_Creator);
                excelColumns.Add(ErrorStrings.ExportColumn_CreateTime);
                excelColumns.Add(ErrorStrings.ExportColumn_Modifier);
                excelColumns.Add(ErrorStrings.ExportColumn_ModifyTime);
                excelColumns.Add(ErrorStrings.ExportColumn_Abandoner);
                excelColumns.Add(ErrorStrings.ExportColumn_AbandonTime);
                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == BrandGradeMessagesArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = BrandGradeMessagesArray[index - 1];
                        var values = new object[] {
                            detail.PartsSalesCategoryName,
                            detail.GradeName,
                            detail.Remark,
                            Enum.GetName(typeof(DcsBaseDataStatus),detail.Status),
                            detail.CreatorName,
                            detail.CreateTime,
                            detail.ModifierName,
                            detail.ModifyTime,
                            detail.AbandonerName,
                            detail.AbandonTime
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
    }
}
