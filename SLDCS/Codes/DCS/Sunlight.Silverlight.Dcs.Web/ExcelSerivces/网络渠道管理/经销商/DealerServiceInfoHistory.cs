﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string ExportDealerServiceInfoHistory(int[] ids, string dealercode, string dealername, int? partsSalesCategoryId,string marketingDepartmentName,int? usedPartsWarehouseId,DateTime? createTimeStart, DateTime? createTimeEnd) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("经销商分公司管理信息履历_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var dealerServiceInfoHistoryQuery = GetDealerServiceInfoHistoryQuery();

            if(ids!=null && ids.Length>0) {
                dealerServiceInfoHistoryQuery = dealerServiceInfoHistoryQuery.Where(r => ids.Contains(r.Id));
            } else {
                if(!string.IsNullOrEmpty(dealercode)) {
                    dealerServiceInfoHistoryQuery = dealerServiceInfoHistoryQuery.Where(r => r.Dealer.Code.Contains(dealercode));
                }
                if(!string.IsNullOrEmpty(dealername)) {
                    dealerServiceInfoHistoryQuery = dealerServiceInfoHistoryQuery.Where(r => r.Dealer.Name.Contains(dealername));
                }
                if(partsSalesCategoryId.HasValue) {
                    dealerServiceInfoHistoryQuery = dealerServiceInfoHistoryQuery.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId.Value);
                }
                if(!string.IsNullOrEmpty(marketingDepartmentName)) {
                    dealerServiceInfoHistoryQuery = dealerServiceInfoHistoryQuery.Where(r => r.MarketingDepartment.Name.Contains(marketingDepartmentName));
                }
                if(createTimeStart.HasValue) {
                    dealerServiceInfoHistoryQuery = dealerServiceInfoHistoryQuery.Where(r => r.CreateTime >= createTimeStart);
                }
                if(createTimeEnd.HasValue) {
                    dealerServiceInfoHistoryQuery = dealerServiceInfoHistoryQuery.Where(r => r.CreateTime <= createTimeEnd);
                }
            }

            var dealerServiceInfoHistoryArray = dealerServiceInfoHistoryQuery.ToArray();
            if(dealerServiceInfoHistoryQuery.Any()) {
                excelColumns.Add("品牌");
                excelColumns.Add("服务站/经销商编号");
                excelColumns.Add("简称");
                excelColumns.Add("变更后简称");
                excelColumns.Add("业务编码");
                excelColumns.Add("变更后业务编码");
                excelColumns.Add("业务名称");
                excelColumns.Add("变更后业务名称");
                excelColumns.Add(ErrorStrings.ExportColumn_Status);
                excelColumns.Add("变更后状态");
                excelColumns.Add("营销公司");
                excelColumns.Add("变更后营销公司");
                excelColumns.Add("市场部");
                excelColumns.Add("变更后市场部");
                excelColumns.Add("网络模式");
                excelColumns.Add("变更后网络模式");
                excelColumns.Add("服务站类型");
                excelColumns.Add("变更后服务站类型");
                excelColumns.Add("区域");
                excelColumns.Add("变更后区域");
                excelColumns.Add("地区类别");
                excelColumns.Add("变更后地区类别");
                excelColumns.Add("维修权限");
                excelColumns.Add("变更后维修权限");
                excelColumns.Add("是否值守");
                excelColumns.Add("变更后是否值守");
                excelColumns.Add("24小时热线");
                excelColumns.Add("变更后24小时热线");
                excelColumns.Add("固定电话");
                excelColumns.Add("变更后固定电话");
                excelColumns.Add("传真");
                excelColumns.Add("变更后传真");
                excelColumns.Add("订货默认仓库");
                excelColumns.Add("变更后订货默认仓库");
                excelColumns.Add("服务站维修权限");
                excelColumns.Add("变更后服务站维修权限");
                excelColumns.Add("配件管理费率");
                excelColumns.Add("变更后配件管理费率");
                excelColumns.Add("外出服务费等级");
                excelColumns.Add("变更后外出服务费等级");
                excelColumns.Add("星级");
                excelColumns.Add("变更后星级");
                excelColumns.Add("结算星级");
                excelColumns.Add("变更后结算星级");
                excelColumns.Add("外出服务半径");
                excelColumns.Add("变更后外出服务半径");
                excelColumns.Add("旧件仓库");
                excelColumns.Add("变更后旧件仓库");
                excelColumns.Add("配件储备金额");
                excelColumns.Add("变更后配件储备金额");
                excelColumns.Add("材料费开票类型");
                excelColumns.Add("变更后材料费开票类型");
                excelColumns.Add("工时费开票类型");
                excelColumns.Add("变更后工时费开票类型");
                excelColumns.Add("工时费开票系数");
                excelColumns.Add("变更后工时费开票系数");
                excelColumns.Add("材料费开票系数");
                excelColumns.Add("变更后材料费开票系数");
                excelColumns.Add("销售与服务场地布局关系");
                excelColumns.Add("变更后销售与服务场地布局关系");
                excelColumns.Add("24小时手机号");
                excelColumns.Add("变更后24小时手机号");
                excelColumns.Add("主干网络类型");
                excelColumns.Add("变更后主干网络类型");
                excelColumns.Add("是否中心站");
                excelColumns.Add("变更后是否中心站");
                excelColumns.Add(ErrorStrings.ExportColumn_Creator);
                excelColumns.Add(ErrorStrings.ExportColumn_CreateTime);

                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dealerServiceInfoHistoryArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dealerServiceInfoHistoryArray[index - 1];
                        var values = new object[] {
                            detail.PartsSalesCategory==null?"":detail.PartsSalesCategory.Name,
                            detail.Dealer==null?"":detail.Dealer.Code,
                            detail.Dealer==null?"":detail.Dealer.Name,
                            detail.Dealer==null?"":detail.Dealer.ShortName,
                            detail.BusinessCode,
                            detail.ABusinessCode,
                            detail.BusinessName,
                            detail.ABusinessName,
                            detail.Status==null?"":Enum.GetName(typeof(DcsMasterDataStatus),detail.Status),
                            detail.AStatus==null?"":Enum.GetName(typeof(DcsMasterDataStatus),detail.AStatus),
                            detail.BusinessDivision,
                            detail.ABusinessDivision,
                            detail.MarketingDepartment==null?"":detail.MarketingDepartment.Name,
                            detail.AMarketingDepartment==null?"":detail.AMarketingDepartment.Name,
                            detail.ChannelCapability==null?"":detail.ChannelCapability.Name,
                            detail.AChannelCapability==null?"":detail.AChannelCapability.Name,
                            detail.ServiceStationType==null?"":Enum.GetName(typeof(DcsDealerServiceInfoServiceStationType),detail.ServiceStationType),
                            detail.AServiceStationType==null?"":Enum.GetName(typeof(DcsDealerServiceInfoServiceStationType),detail.AServiceStationType),
                            detail.Area,
                            detail.AArea,
                            detail.RegionType,
                            detail.ARegionType,
                            detail.ServicePermission,
                            detail.AServicePermission,
                            detail.IsOnDuty,
                            detail.AIsOnDuty,
                            detail.HotLine,
                            detail.AHotLine,
                            detail.Fix,
                            detail.AFix,
                            detail.Fax,
                            detail.AFax,
                            detail.Warehouse==null?"":detail.Warehouse.Name,
                            detail.AWarehouse==null?"":detail.AWarehouse.Name,
                            detail.RepairAuthorityGrade==null?"":Enum.GetName(typeof(DcsServicePermission),detail.RepairAuthorityGrade),
                            detail.ARepairAuthorityGrade==null?"":Enum.GetName(typeof(DcsServicePermission),detail.ARepairAuthorityGrade),
                            detail.PartsManagementCostGrade==null?"":detail.PartsManagementCostGrade.Name,
                            detail.APartsManagementCostGrade==null?"":detail.APartsManagementCostGrade.Name,
                            detail.Grade,
                            detail.AGrade,
                            detail.GradeCoefficient==null?"":detail.GradeCoefficient.Grade,
                            detail.AGradeCoefficient==null?"":detail.AGradeCoefficient.Grade,
                            detail.OutServiceradii,
                            detail.AOutServiceradii,
                            detail.UsedPartsWarehouse==null?"":detail.UsedPartsWarehouse.Name,
                            detail.AUsedPartsWarehouse==null?"":detail.AUsedPartsWarehouse.Name,
                            detail.PartReserveAmount,
                            detail.APartReserveAmount,
                            detail.MaterialCostInvoiceType==null?"":Enum.GetName(typeof(DcsInvoiceType),detail.MaterialCostInvoiceType),
                            detail.AMaterialCostInvoiceType==null?"":Enum.GetName(typeof(DcsInvoiceType),detail.AMaterialCostInvoiceType),
                            detail.LaborCostCostInvoiceType==null?"":Enum.GetName(typeof(DcsInvoiceType),detail.LaborCostCostInvoiceType),
                            detail.ALaborCostCostInvoiceType==null?"":Enum.GetName(typeof(DcsInvoiceType),detail.ALaborCostCostInvoiceType),
                            detail.LaborCostInvoiceRatio,
                            detail.ALaborCostInvoiceRatio,
                            detail.InvoiceTypeInvoiceRatio,
                            detail.AInvoiceTypeInvoiceRatio,
                            detail.SaleandServicesiteLayout==null?"":Enum.GetName(typeof(DcsSaleServiceLayout),detail.SaleandServicesiteLayout),
                            detail.ASaleandServicesiteLayout==null?"":Enum.GetName(typeof(DcsSaleServiceLayout),detail.ASaleandServicesiteLayout),
                            detail.HourPhone24,
                            detail.A24HourPhone,
                            detail.TrunkNetworkType==null?"":Enum.GetName(typeof(DcsDlrSerInfoTrunkNetworkType),detail.TrunkNetworkType),
                            detail.ATrunkNetworkType==null?"":Enum.GetName(typeof(DcsDlrSerInfoTrunkNetworkType),detail.ATrunkNetworkType),
                            detail.CenterStack,
                            detail.ACenterStack,
                            detail.CreatorName,
                            detail.CreateTime
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
    }
}
