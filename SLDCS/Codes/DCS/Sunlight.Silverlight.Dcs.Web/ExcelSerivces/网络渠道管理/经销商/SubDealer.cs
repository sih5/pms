﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string ExportSubDealer(int[] ids, string subDealerCode, string subDealerName, string dealerCode, string dealerName, int? status, DateTime? createTimeStart, DateTime? createTimeEnd) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("二级站信息-分公司_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var Dealeres = 查询二级站信息分公司();

            if(ids != null && ids.Length > 0) {
                Dealeres = Dealeres.Where(r => ids.Contains(r.Id));
            } else {
                if(!string.IsNullOrEmpty(subDealerCode)) {
                    Dealeres = Dealeres.Where(r => r.Code == subDealerCode);
                }
                if(!string.IsNullOrEmpty(subDealerName)) {
                    Dealeres = Dealeres.Where(r => r.Name == subDealerName);
                }
                if(!string.IsNullOrEmpty(dealerCode)) {
                    Dealeres = Dealeres.Where(r => r.Dealer.Code == dealerCode);
                }
                if(!string.IsNullOrEmpty(dealerName)) {
                    Dealeres = Dealeres.Where(r => r.Dealer.Name == dealerName);
                }
                if(status.HasValue) {
                    Dealeres = Dealeres.Where(r => r.Status == status.Value);
                }
                if(createTimeStart.HasValue) {
                    Dealeres = Dealeres.Where(r => r.CreateTime >= createTimeStart);
                }
                if(createTimeEnd.HasValue) {
                    Dealeres = Dealeres.Where(r => r.CreateTime <= createTimeEnd);
                }
            }

            var DealeresArray = Dealeres.ToArray();
            if(Dealeres.Any()) {
                excelColumns.Add("二级站编号");
                excelColumns.Add("二级站名称");
                excelColumns.Add("服务站编号");
                excelColumns.Add("服务站名称");
                excelColumns.Add(ErrorStrings.ExportColumn_Status);
                excelColumns.Add("服务站站长");
                excelColumns.Add("服务经理");
                excelColumns.Add("固定电话");
                excelColumns.Add("移动电话");
                excelColumns.Add("营业地址(经度)");
                excelColumns.Add("营业地址(纬度)");
                excelColumns.Add("地址");
                excelColumns.Add(ErrorStrings.ExportColumn_Remark);
                excelColumns.Add(ErrorStrings.ExportColumn_CreateTime);
                excelColumns.Add(ErrorStrings.ExportColumn_Creator);
                excelColumns.Add(ErrorStrings.ExportColumn_ModifyTime);
                excelColumns.Add(ErrorStrings.ExportColumn_Modifier);
                excelColumns.Add(ErrorStrings.ExportColumn_AbandonTime);
                excelColumns.Add(ErrorStrings.ExportColumn_Abandoner);

                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == DealeresArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = DealeresArray[index - 1];
                        var values = new object[] {
                            detail.Code,
                            detail.Name,
                            detail.Dealer.Code,
                            detail.Dealer.Name,
                            Enum.GetName(typeof(DcsBaseDataStatus),detail.Status),
                            detail.DealerManager,
                            detail.Manager,
                            detail.ManagerPhoneNumber,
                            detail.ManagerMobile,
                            //detail.BusinessAddressLongitude,
                            //detail.BusinessAddressLatitude,
                            detail.Address,
                            detail.Remark,
                            detail.CreateTime,
                            detail.CreatorName,
                            detail.ModifyTime,
                            detail.ModifierName,
                            detail.AbandonTime,
                            detail.AbandonerName
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
    }
}
