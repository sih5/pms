﻿using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web
{
    partial class DcsDomainService {
        public string ExportOverstockPartsPlatFormBills(int[] ids,string centerName,string dealerName,string sparePartCode,string sparePartName)
        {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("积压件平台表_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = 查询积压件平台明细(sparePartCode);
            
            if (!string.IsNullOrEmpty(centerName))
                data = data.Where(r => r.CenterName.Contains(centerName));
            if (!string.IsNullOrEmpty(dealerName))
                data = data.Where(r => r.DealerName.Contains(dealerName));
            if (!string.IsNullOrEmpty(sparePartName))
                data = data.Where(r => r.SparePartName.Contains(sparePartName));
            if (ids != null && ids.Any()) {
                data = data.Where(r => ids.Contains(r.Id));
            }
            var dataArray = data.ToArray();

            if (data.Any())
            {
                excelColumns.Add(ErrorStrings.ExportColumn_AgencyName);
                excelColumns.Add(ErrorStrings.Export_Column_Title_ServiceName);
                excelColumns.Add(ErrorStrings.Export_Column_Title_ServiceCode);
                excelColumns.Add(ErrorStrings.ExportColumn_PartCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SparePartName);
                excelColumns.Add(ErrorStrings.ExportColumn_WarehouseCode);
                excelColumns.Add(ErrorStrings.ExportColumn_WarehouseName);
                excelColumns.Add(ErrorStrings.Export_Column_Title_OverQuantity);
                excelColumns.Add(ErrorStrings.ExportColumn_PartsSalesPrice);
                excelColumns.Add(ErrorStrings.Export_Column_Title_DiscountRate);
                excelColumns.Add(ErrorStrings.Export_Column_Title_DiscountedPrice);
                excelColumns.Add(ErrorStrings.Export_Column_Title_EntityStatus);
                
                using (var excelExport = new ExcelExport(exportDataFileName))
                {
                    excelExport.ExportByRow(index =>
                    {
                        if (index == dataArray.Count() + 1)
                            return null;
                        if (index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.CenterName,
                            detail.DealerName,
							detail.DealerCode,
							detail.SparePartCode,
                            detail.SparePartName,
                            detail.WarehouseCode,
                            detail.WarehouseName,
							detail.Quantity,
							detail.RetailGuidePrice,
                            detail.DiscountRate,
                            detail.DiscountedPrice,
                            detail.EntityStatus
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }

    }
}

