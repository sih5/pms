﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string 导出中心库收货差异处理(int[] ids, int? resRelationshipId, int? resTem, string code, string storageCompanyCode, string storageCompanyName, string sparePartCode, string sparePartName, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? SubmitTimeBegin, DateTime? SubmitTimeEnd) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("中心库收货差异处理单_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = this.GetAgencyDifferenceBackBillsByUser();
            if(ids != null && ids.Length > 0) {
                data = data.Where(r => ids.Contains(r.Id));
            } else {
                if(resRelationshipId.HasValue) {
                    data = data.Where(r => r.ResRelationshipId == resRelationshipId);
                }
                if(!string.IsNullOrEmpty(code)) {
                    data = data.Where(r => r.Code.Contains(code));
                }
                if(!string.IsNullOrEmpty(storageCompanyCode)) {
                    data = data.Where(r => r.StorageCompanyCode.Contains(storageCompanyCode));
                }
                if(!string.IsNullOrEmpty(code)) {
                    data = data.Where(r => r.StorageCompanyName.Contains(storageCompanyName));
                }
                if(!string.IsNullOrEmpty(sparePartCode)) {
                    data = data.Where(r => r.SparePartCode.Contains(sparePartCode));
                }
                if(!string.IsNullOrEmpty(sparePartName)) {
                    data = data.Where(r => r.SparePartName.Contains(sparePartName));
                }
                if(resTem.HasValue) {
                    data = data.Where(r => r.ResTem == resTem);
                }
                if(status.HasValue) {
                    data = data.Where(r => r.Status == status);
                }
                if(createTimeBegin.HasValue) {
                    data = data.Where(r => r.CreateTime >= createTimeBegin.Value);
                }
                if(createTimeEnd.HasValue) {
                    data = data.Where(r => r.CreateTime <= createTimeEnd.Value);
                }

                if(SubmitTimeBegin.HasValue) {
                    data = data.Where(r => r.SubmitTime >= SubmitTimeBegin.Value);
                }
                if(SubmitTimeEnd.HasValue) {
                    data = data.Where(r => r.SubmitTime <= SubmitTimeEnd.Value);
                }
               
                
            }
            if(data.Any()) {
                excelColumns.Add("单据编号");
                excelColumns.Add("状态");
				excelColumns.Add("中心库编号");
				excelColumns.Add("中心库名称");
				excelColumns.Add("配件编号");
				excelColumns.Add("配件名称");
				excelColumns.Add("计量单位");
				excelColumns.Add("入库计划单号");
				excelColumns.Add("入库仓库编号");
				excelColumns.Add("入库仓库名称");
				excelColumns.Add("发运单号");
				excelColumns.Add("销售单号");
				excelColumns.Add("入库量");
				excelColumns.Add("实收量");
				excelColumns.Add("差异量");
				excelColumns.Add("订货价");
				excelColumns.Add("差异总金额");
				excelColumns.Add("差异原因");
				excelColumns.Add("责任类型");
				excelColumns.Add("责任组");
                excelColumns.Add(ErrorStrings.ExportColumn_Creator);
                excelColumns.Add(ErrorStrings.ExportColumn_CreateTime);
                excelColumns.Add(ErrorStrings.ExportColumn_Modifier);
                excelColumns.Add(ErrorStrings.ExportColumn_ModifyTime);
				excelColumns.Add("作废人");
				excelColumns.Add("作废时间");
                excelColumns.Add(ErrorStrings.ExportColumn_Submiter);
                excelColumns.Add(ErrorStrings.ExportColumn_SubmitTime);
				excelColumns.Add("判定人");
				excelColumns.Add("判定人时间");
				excelColumns.Add("确认人");
				excelColumns.Add("确认时间");
				excelColumns.Add("处理人");
				excelColumns.Add("处理时间");
				excelColumns.Add("处理意见");
				excelColumns.Add("审核人");
				excelColumns.Add("审核时间");
				excelColumns.Add("审批人");
				excelColumns.Add("审批时间");
				excelColumns.Add("驳回意见");
				excelColumns.Add("终止人");
				excelColumns.Add("终止时间");
				excelColumns.Add("终止意见");				
                var dataArray = data.ToArray();
                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == data.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {                          
                            detail.Code,
							Enum.GetName(typeof(DCSAgencyDifferenceBackBillStatus),detail.Status ),
                            detail.StorageCompanyCode,
                            detail.StorageCompanyName, 
                            detail.SparePartCode, 
                            detail.SparePartName, 
                            detail.MeasureUnit, 
                            detail.PartsInboundPlanCode,
							detail.InWarehouseCode,
							detail.InWarehouseName,
							detail.PartsShippingCode,
							detail.PartsSalesOrderCode,
							detail.InboundQty,
							detail.ActQty,
							detail.DifferQty,
							detail.OrderPrice,
							detail.DifferSum,
							detail.DIfferReason,
							detail.ResType,
							Enum.GetName(typeof(DCSResponsibleMembersResTem),detail.ResTem??0),
							detail.CreatorName,
                            detail.CreateTime,
                            detail.ModifierName,
                            detail.ModifyTime,
							detail.AbandonerName,
                            detail.AbandonTime,
                            detail.SubmitterName,
                            detail.SubmitTime,                           
                            detail.DetermineName,
                            detail.DetermineTime,
                            detail.ConfirmName,
                            detail.ConfirmTime,
                            detail.Handler,
							detail.HandlTime,
							detail.HandMemo,
							detail.CheckerName,
							detail.CheckTime,
							detail.ApproverName,
							detail.ApproveTime,
							detail.RejectMemo,
							detail.CloserName,
							detail.CloseTime,
						    detail.CloseMemo
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
    }
}
