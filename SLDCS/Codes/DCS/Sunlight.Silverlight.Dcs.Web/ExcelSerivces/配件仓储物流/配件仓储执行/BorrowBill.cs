﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web
{
    partial class DcsDomainService
    {
        public string ExportBorrowBillWithDetail(int[] ids, string Code, int? WarehouseId, int? Type, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? ApproveTimeBegin, DateTime? ApproveTimeEnd, DateTime? OutBoundTimeBegin, DateTime? OutBoundTimeEnd, DateTime? ExpectReturnTimeBegin, DateTime? ExpectReturnTimeEnd)
        {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("配件借用单清单合并导出_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = this.GetBorrowBillForDetails();
            if (ids != null && ids.Length > 0)
            {
                data = data.Where(r => ids.Contains(r.Id));
            }
            else
            {
                if (WarehouseId.HasValue)
                {
                    data = data.Where(r => r.WarehouseId == WarehouseId);
                }
                if (!string.IsNullOrEmpty(Code))
                {
                    data = data.Where(r => r.Code.Contains(Code));
                }
                if (Type.HasValue)
                {
                    data = data.Where(r => r.Type == Type);
                }
                if (status.HasValue)
                {
                    data = data.Where(r => r.Status == status);
                }
                if (createTimeBegin.HasValue)
                {
                    data = data.Where(r => r.CreateTime >= createTimeBegin.Value);
                }
                if (createTimeEnd.HasValue)
                {
                    data = data.Where(r => r.CreateTime <= createTimeEnd.Value);
                }

                if (ApproveTimeBegin.HasValue)
                {
                    data = data.Where(r => r.ApproveTime >= ApproveTimeBegin.Value);
                }
                if (ApproveTimeEnd.HasValue)
                {
                    data = data.Where(r => r.ApproveTime <= ApproveTimeEnd.Value);
                }
                if (OutBoundTimeBegin.HasValue)
                {
                    data = data.Where(r => r.OutBoundTime >= OutBoundTimeBegin.Value);
                }
                if (OutBoundTimeEnd.HasValue)
                {
                    data = data.Where(r => r.OutBoundTime <= OutBoundTimeEnd.Value);
                }
                if (ExpectReturnTimeBegin.HasValue)
                {
                    data = data.Where(r => r.ExpectReturnTime >= ExpectReturnTimeBegin.Value);
                }
                if (ExpectReturnTimeEnd.HasValue)
                {
                    data = data.Where(r => r.ExpectReturnTime <= ExpectReturnTimeEnd.Value);
                }
            }
            var borrowBill = data;
            var exportData = from a in borrowBill
                             join b in ObjectContext.BorrowBillDetails on a.Id equals b.BorrowBillId
                             select new
                             {
                                 a.Code,
                                 a.Status,
                                 a.Type,
                                 a.BorrowDepartmentName,
                                 a.BorrowName,
                                 a.ContactMethod,
                                 a.WarehouseCode,
                                 a.WarehouseName,
                                 a.CreatorName,
                                 a.CreateTime,
                                 a.ModifierName,
                                 a.ModifyTime,
                                 a.SubmitterName,
                                 a.SubmitTime,
                                 a.InitialApproverName,
                                 a.InitialApproveTime,
                                 a.ApproverName,
                                 a.ApproveTime,
                                 a.OutBoundTime,
                                 a.ReturnerName,
                                 a.ReturnTime,
                                 b.SparePartCode,
                                 b.SparePartName,
                                 b.WarehouseAreaCode,
                                 b.SihCode,
                                 b.MeasureUnit,
                                 b.BorrowQty,
                                 b.OutboundAmount,
                                 b.ConfirmingQty,
                                 b.ConfirmedQty
                             };
            if (exportData.Any())
            {
                excelColumns.Add(ErrorStrings.ExportColumn_OrderStatus);
                excelColumns.Add(ErrorStrings.ExportColumn_BorrowBillCode);
                excelColumns.Add(ErrorStrings.ExportColumn_BorrowBillType);
                excelColumns.Add(ErrorStrings.ExportColumn_BorrowBillDepartment);
                excelColumns.Add(ErrorStrings.ExportColumn_BorrowBillPerson);
                excelColumns.Add(ErrorStrings.ExportColumn_BorrowBillContact);
                excelColumns.Add(ErrorStrings.ExportColumn_WarehouseCode);
                excelColumns.Add(ErrorStrings.ExportColumn_WarehouseName);
                excelColumns.Add(ErrorStrings.ExportColumn_Creator);
                excelColumns.Add(ErrorStrings.ExportColumn_CreateTime);
                excelColumns.Add(ErrorStrings.ExportColumn_Modifier);
                excelColumns.Add(ErrorStrings.ExportColumn_ModifyTime);
                excelColumns.Add(ErrorStrings.ExportColumn_Submiter);
                excelColumns.Add(ErrorStrings.ExportColumn_SubmitTime);
                excelColumns.Add(ErrorStrings.ExportColumn_InitApprover);
                excelColumns.Add(ErrorStrings.ExportColumn_InitApproveTime);
                excelColumns.Add(ErrorStrings.ExportColumn_Approver);
                excelColumns.Add(ErrorStrings.ExportColumn_ApproveTime);
                excelColumns.Add(ErrorStrings.ExportColumn_OutTime);
                excelColumns.Add(ErrorStrings.ExportColumn_ReturnExecutor);
                excelColumns.Add(ErrorStrings.ExportColumn_ReturnDate);
                excelColumns.Add("配件编号");
                excelColumns.Add("配件名称");
                excelColumns.Add("库位编号");
                excelColumns.Add("红岩号");
                excelColumns.Add("单位");
                excelColumns.Add("借用量");
                excelColumns.Add("出库量");
                excelColumns.Add("待确认数");
                excelColumns.Add("已确认数");
                var dataArray = exportData.ToArray();
                using (var excelExport = new ExcelExport(exportDataFileName))
                {
                    excelExport.ExportByRow(index =>
                    {
                        if (index == exportData.Count() + 1)
                            return null;
                        if (index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            Enum.GetName(typeof(DcsBorrowBillStatus),detail.Status??0),
                            detail.Code,
                            Enum.GetName(typeof(DcsBorrowBillType),detail.Type??0),
                            detail.BorrowDepartmentName,
                            detail.BorrowName, 
                            detail.ContactMethod, 
                            detail.WarehouseCode, 
                            detail.WarehouseName, 
                            detail.CreatorName,
                            detail.CreateTime,
                            detail.ModifierName,
                            detail.ModifyTime,
                            detail.SubmitterName,
                            detail.SubmitTime,
                            detail.InitialApproverName,
                            detail.InitialApproveTime,
                            detail.ApproverName,
                            detail.ApproveTime,
                            detail.OutBoundTime,
                            detail.ReturnerName,
                            detail.ReturnTime,
                            detail.SparePartCode,
                            detail.SparePartName,
                            detail.WarehouseAreaCode,
                            detail.SihCode,
                            detail.MeasureUnit,
                            detail.BorrowQty,
                            detail.OutboundAmount,
                            detail.ConfirmingQty,
                            detail.ConfirmedQty
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
        public string ExportBorrowBill(int[] ids, string Code, int? WarehouseId, int? Type, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd,DateTime? ApproveTimeBegin, DateTime? ApproveTimeEnd, DateTime? OutBoundTimeBegin, DateTime? OutBoundTimeEnd, DateTime? ExpectReturnTimeBegin, DateTime? ExpectReturnTimeEnd)
        {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("配件借用单_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = this.GetBorrowBills();
            if (ids != null && ids.Length > 0)
            {
                data = data.Where(r => ids.Contains(r.Id));
            }
            else
            {
                if (WarehouseId.HasValue)
                {
                    data = data.Where(r => r.WarehouseId == WarehouseId);
                }
                if (!string.IsNullOrEmpty(Code))
                {
                    data = data.Where(r => r.Code.Contains(Code));
                }
                if (Type.HasValue)
                {
                    data = data.Where(r => r.Type == Type);
                }
                if (status.HasValue)
                {
                    data = data.Where(r => r.Status == status);
                }
                if (createTimeBegin.HasValue)
                {
                    data = data.Where(r => r.CreateTime >= createTimeBegin.Value);
                }
                if (createTimeEnd.HasValue)
                {
                    data = data.Where(r => r.CreateTime <= createTimeEnd.Value);
                }

                if(ApproveTimeBegin.HasValue) {
                    data = data.Where(r => r.ApproveTime >= ApproveTimeBegin.Value);
                }
                if (ApproveTimeEnd.HasValue)
                {
                    data = data.Where(r => r.ApproveTime <= ApproveTimeEnd.Value);
                }
                if (OutBoundTimeBegin.HasValue)
                {
                    data = data.Where(r => r.OutBoundTime >= OutBoundTimeBegin.Value);
                }
                if (OutBoundTimeEnd.HasValue)
                {
                    data = data.Where(r => r.OutBoundTime <= OutBoundTimeEnd.Value);
                }
                if (ExpectReturnTimeBegin.HasValue)
                {
                    data = data.Where(r => r.ExpectReturnTime >= ExpectReturnTimeBegin.Value);
                }
                if (ExpectReturnTimeEnd.HasValue)
                {
                    data = data.Where(r => r.ExpectReturnTime <= ExpectReturnTimeEnd.Value);
                }
            }
            if (data.Any())
            {
                excelColumns.Add(ErrorStrings.ExportColumn_OrderStatus);
                excelColumns.Add(ErrorStrings.ExportColumn_BorrowBillCode);
                excelColumns.Add(ErrorStrings.ExportColumn_BorrowBillType);
                excelColumns.Add(ErrorStrings.ExportColumn_BorrowBillDepartment);
                excelColumns.Add(ErrorStrings.ExportColumn_BorrowBillPerson);
                excelColumns.Add(ErrorStrings.ExportColumn_BorrowBillContact);
                excelColumns.Add(ErrorStrings.ExportColumn_WarehouseCode);
                excelColumns.Add(ErrorStrings.ExportColumn_WarehouseName);
                excelColumns.Add(ErrorStrings.ExportColumn_Creator);
                excelColumns.Add(ErrorStrings.ExportColumn_CreateTime);
                excelColumns.Add(ErrorStrings.ExportColumn_Modifier);
                excelColumns.Add(ErrorStrings.ExportColumn_ModifyTime);
                excelColumns.Add(ErrorStrings.ExportColumn_Submiter);
                excelColumns.Add(ErrorStrings.ExportColumn_SubmitTime);
                excelColumns.Add(ErrorStrings.ExportColumn_InitApprover);
                excelColumns.Add(ErrorStrings.ExportColumn_InitApproveTime);
                excelColumns.Add(ErrorStrings.ExportColumn_Approver);
                excelColumns.Add(ErrorStrings.ExportColumn_ApproveTime);
                excelColumns.Add(ErrorStrings.ExportColumn_OutTime);
                excelColumns.Add(ErrorStrings.ExportColumn_ReturnExecutor);
                excelColumns.Add(ErrorStrings.ExportColumn_ReturnDate);
                var dataArray = data.ToArray();
                using (var excelExport = new ExcelExport(exportDataFileName))
                {
                    excelExport.ExportByRow(index =>
                    {
                        if (index == data.Count() + 1)
                            return null;
                        if (index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            Enum.GetName(typeof(DcsBorrowBillStatus),detail.Status??0),
                            detail.Code,
                            Enum.GetName(typeof(DcsBorrowBillType),detail.Type??0),
                            detail.BorrowDepartmentName,
                            detail.BorrowName, 
                            detail.ContactMethod, 
                            detail.WarehouseCode, 
                            detail.WarehouseName, 
                            detail.CreatorName,
                            detail.CreateTime,
                            detail.ModifierName,
                            detail.ModifyTime,
                            detail.SubmitterName,
                            detail.SubmitTime,
                            detail.InitialApproverName,
                            detail.InitialApproveTime,
                            detail.ApproverName,
                            detail.ApproveTime,
                            detail.OutBoundTime,
                            detail.ReturnerName,
                            detail.ReturnTime
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
    }
}
