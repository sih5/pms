﻿using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string 导出中心库库存汇总(string marketingDepartmentNames, string centerNames, DateTime? bCreateTime, DateTime? eCreateTime, string dealerNames,  string newType) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("中心库库存汇总_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = 中心库库存汇总(marketingDepartmentNames, centerNames, bCreateTime, eCreateTime, dealerNames, newType);
            var dataArray = data.ToArray();
            if(data.Any()) {
                excelColumns.Add(ErrorStrings.Export_Title_PartsSalesWeeklyBase_CreateTime);
                excelColumns.Add(ErrorStrings.ExportColumn_MarketingDepartment);
                excelColumns.Add(ErrorStrings.ExportColumn_AgencyName);
                excelColumns.Add(ErrorStrings.ExportColumn_DealerName);
                excelColumns.Add(ErrorStrings.QueryPanel_KvValue_CentralPartsSalesOrder_NewType);
                excelColumns.Add(ErrorStrings.Report_DataDeTailPanel_Text_BottomStock_Entries);
                excelColumns.Add(ErrorStrings.ExportColumn_Quantity);
                excelColumns.Add(ErrorStrings.ExportColumn_PartsSalesPriceAmount);
             
                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.YearMonth,
                            detail.MarketingDepartmentName,
                            detail.CenterName,
                            detail.DealerName,
							Enum.GetName(typeof(DCSABCSettingType),detail.NewType==null?0:detail.NewType),
                            detail.Entries,
                            detail.ActualStock, 
                            detail.SalesTotal
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }

    }
}
