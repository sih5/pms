﻿using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string 导出出入库统计(int? warehouseId, DateTime? bRecordTime, DateTime? eRecordTime) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("入库、包装、上架、出库数据报表_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = 查询出入库统计(warehouseId, bRecordTime, eRecordTime);
            var dataArray = data.ToArray();
            if(data.Any()) {
                excelColumns.Add(ErrorStrings.InOutSettleSummaryReport1);
                excelColumns.Add(ErrorStrings.InOutSettleSummaryReport2);
                excelColumns.Add(ErrorStrings.InOutSettleSummaryReport3);
                excelColumns.Add(ErrorStrings.InOutSettleSummaryReport4);
				excelColumns.Add(ErrorStrings.InOutSettleSummaryReport61);
                excelColumns.Add(ErrorStrings.InOutSettleSummaryReport5);				
                excelColumns.Add(ErrorStrings.InOutSettleSummaryReport6);
                excelColumns.Add(ErrorStrings.InOutSettleSummaryReport7);
                excelColumns.Add(ErrorStrings.InOutSettleSummaryReport8);
                excelColumns.Add(ErrorStrings.InOutSettleSummaryReport9);
                excelColumns.Add(ErrorStrings.InOutSettleSummaryReport10);
                excelColumns.Add(ErrorStrings.InOutSettleSummaryReport11);
                excelColumns.Add(ErrorStrings.InOutSettleSummaryReport12);
                excelColumns.Add(ErrorStrings.InOutSettleSummaryReport13);
				excelColumns.Add(ErrorStrings.InOutSettleSummaryReport62);
                excelColumns.Add(ErrorStrings.InOutSettleSummaryReport14);				
                excelColumns.Add(ErrorStrings.InOutSettleSummaryReport15);
                excelColumns.Add(ErrorStrings.InOutSettleSummaryReport16);
                excelColumns.Add(ErrorStrings.InOutSettleSummaryReport17);
                excelColumns.Add(ErrorStrings.InOutSettleSummaryReport18);
                excelColumns.Add(ErrorStrings.InOutSettleSummaryReport19);
				excelColumns.Add(ErrorStrings.InOutSettleSummaryReport63);
                excelColumns.Add(ErrorStrings.InOutSettleSummaryReport20);
                excelColumns.Add(ErrorStrings.InOutSettleSummaryReport21);
                excelColumns.Add(ErrorStrings.InOutSettleSummaryReport22);
                excelColumns.Add(ErrorStrings.InOutSettleSummaryReport23);
                excelColumns.Add(ErrorStrings.InOutSettleSummaryReport24);
                excelColumns.Add(ErrorStrings.InOutSettleSummaryReport25);
                excelColumns.Add(ErrorStrings.InOutSettleSummaryReport26);
                excelColumns.Add(ErrorStrings.InOutSettleSummaryReport27);
                excelColumns.Add(ErrorStrings.InOutSettleSummaryReport28);
				excelColumns.Add(ErrorStrings.InOutSettleSummaryReport64);
                excelColumns.Add(ErrorStrings.InOutSettleSummaryReport29);
                excelColumns.Add(ErrorStrings.InOutSettleSummaryReport30);
                excelColumns.Add(ErrorStrings.InOutSettleSummaryReport31);
                excelColumns.Add(ErrorStrings.InOutSettleSummaryReport32);
                excelColumns.Add(ErrorStrings.InOutSettleSummaryReport33);
                excelColumns.Add(ErrorStrings.InOutSettleSummaryReport34);
				excelColumns.Add(ErrorStrings.InOutSettleSummaryReport65); 
                excelColumns.Add(ErrorStrings.InOutSettleSummaryReport35);				
                excelColumns.Add(ErrorStrings.InOutSettleSummaryReport36);
                excelColumns.Add(ErrorStrings.InOutSettleSummaryReport37);
                excelColumns.Add(ErrorStrings.InOutSettleSummaryReport38);				
                excelColumns.Add(ErrorStrings.InOutSettleSummaryReport39);
                excelColumns.Add(ErrorStrings.InOutSettleSummaryReport40);
                excelColumns.Add(ErrorStrings.InOutSettleSummaryReport41);
                excelColumns.Add(ErrorStrings.InOutSettleSummaryReport42);
                excelColumns.Add(ErrorStrings.InOutSettleSummaryReport43);
				excelColumns.Add(ErrorStrings.InOutSettleSummaryReport66); 
                excelColumns.Add(ErrorStrings.InOutSettleSummaryReport44);				
                excelColumns.Add(ErrorStrings.InOutSettleSummaryReport45);
                excelColumns.Add(ErrorStrings.InOutSettleSummaryReport46);				
                excelColumns.Add(ErrorStrings.InOutSettleSummaryReport47);
				excelColumns.Add(ErrorStrings.InOutSettleSummaryReport67);
                excelColumns.Add(ErrorStrings.InOutSettleSummaryReport48);
                excelColumns.Add(ErrorStrings.InOutSettleSummaryReport49);
                excelColumns.Add(ErrorStrings.InOutSettleSummaryReport50);
				excelColumns.Add(ErrorStrings.InOutSettleSummaryReport68);
                excelColumns.Add(ErrorStrings.InOutSettleSummaryReport51);
                excelColumns.Add(ErrorStrings.InOutSettleSummaryReport52);
                excelColumns.Add(ErrorStrings.InOutSettleSummaryReport53);
				excelColumns.Add(ErrorStrings.InOutSettleSummaryReport69);
                excelColumns.Add(ErrorStrings.InOutSettleSummaryReport54);
                excelColumns.Add(ErrorStrings.InOutSettleSummaryReport55);
                excelColumns.Add(ErrorStrings.InOutSettleSummaryReport56);
				excelColumns.Add(ErrorStrings.InOutSettleSummaryReport70);
                excelColumns.Add(ErrorStrings.InOutSettleSummaryReport57);
                excelColumns.Add(ErrorStrings.InOutSettleSummaryReport58);
                excelColumns.Add(ErrorStrings.InOutSettleSummaryReport59);
                excelColumns.Add(ErrorStrings.InOutSettleSummaryReport60);

                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.RecordTimes,
                            detail.InPurchaseList,
                            detail.InReturnList,
                            detail.InPurchaseListAll,
							detail.InPurchaseInReturnItem,
                            detail.InPurchaseQty,
                            detail.InReturnQty, 
                            detail.InPurchaseQtyAll,
                            detail.InPurchaseFee,
                            detail.InReturnFee,
                            detail.InPurchaseFeeAll,
                            detail.WaitInPurchaseList,
                            detail.WaitInLineList,
                            detail.WaitInPurchaseListAll,
							detail.InPurchaseInReturnItem,
                            detail.WaitInArriveFee,
                            detail.WaitInLineFee,
                            detail.WaitInArriveFeeAll,
                            detail.PackingPurchaseList,
                            detail.PackingReturnList ,
                            detail.PackingPurchaseListAll,
							detail.PackingPurchaseReturnItem,
                            detail.PackingPurchaseQty ,
                            detail.PackingReturnQty ,
                            detail.PackingPurchaseQtyAll,
                            detail.PackingPurchaseFee ,
                            detail.PackingReturnFee,
                            detail.PackingPurchaseFeeAll,
                            detail.WaitPackPurchaseList ,
                            detail.WaitPackReturnList ,
                            detail.WaitPackPurchaseListAll,
							detail.WaitPackPurchaseReturnItem,
                            detail.WaitPackPurchaseFee ,
                            detail.WaitPackReturnFee ,
                            detail.WaitPackPurchaseFeeAll,
                            detail.ShelvesPurchaseList ,
                            detail.ShelvesReturnList ,
                            detail.ShelvesPurchaseListAll ,
							detail.ShelvesPurchasesReturnItem,
                            detail.ShelvesPurchaseQty ,
                            detail.ShelvesReturnQty ,
                            detail.ShelvesPurchaseQtyAll,
                            detail.ShelvesPurchaseFee ,
                            detail.ShelvesReturnFee ,
                            detail.ShelvesPurchaseFeeAll,
                            detail.WaitShelvesPurchaseLis ,
                            detail.WaitShelvesReturnList ,
                            detail.WaitShelvesPurchaseLisAll,
							detail.WaitShelvesPurchaseReturnItem,
                            detail.WaitShelvesPurchaseFee ,
                            detail.WaitShelvesReturnFee ,
                            detail.WaitShelvesPurchaseFeeAll,
                            detail.SaleApproveList ,
							detail.SaleApproveItem,
                            detail.SaleApproveQty ,
                            detail.SaleApproveFee ,
                            detail.PickingList ,
							detail.PickingItem,
                            detail.PickingQty ,
                            detail.PickingFee ,
                            detail.OutList ,
							detail.OutItem,							
                            detail.OutQty ,
                            detail.OutFee ,
                            detail.WaitOutList ,
							detail.WaitOutItem,
                            detail.WaitOutQty ,
                            detail.WaitOutFee ,
                            detail.ShippingFee,
                            detail.ShippingWeight
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }

    }
}

