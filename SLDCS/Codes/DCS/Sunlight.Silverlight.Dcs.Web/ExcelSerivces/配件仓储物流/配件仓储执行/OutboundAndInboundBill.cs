﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string ExportSalesSettlementOutboundAndInboundBillDetail(int companyId, int destCompanyId, int? warehouseId, int? customerAccountId, int? partsSaleCategoryId, int[] inBoundType, int[] outBoundType, DateTime? endTime, int? settleType, int? businessType,bool? isOil)
        {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("待销售结算出入库明细_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = 查询待销售结算出入库明细New(companyId, destCompanyId, warehouseId, customerAccountId, partsSaleCategoryId, inBoundType, outBoundType, endTime, settleType, businessType, isOil);

            var dataArray = data.ToArray();
            if(data.Any()) {
                excelColumns.Add(ErrorStrings.ExportColumn_DealerCode);
                excelColumns.Add(ErrorStrings.ExportColumn_DealerName);
                excelColumns.Add(ErrorStrings.ExportColumn_SourceCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SourceType);
                excelColumns.Add(ErrorStrings.ExportColumn_OrderType);
                excelColumns.Add(ErrorStrings.ExportColumn_SettlementAmount);
                excelColumns.Add(ErrorStrings.ExportColumn_SourceCreateTime);
                excelColumns.Add(ErrorStrings.ExportColumn_PartWarehouse);

                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.CounterpartCompanyCode,
                            detail.CounterpartCompanyName,
                            detail.BillCode,
                            Enum.GetName(typeof(DcsPartsLogisticBatchBillDetailBillType),detail.BillType),
                            detail.PartsSalesOrderTypeName,
                            detail.SettlementAmount,
                            detail.BillCreateTime,
                            detail.WarehouseName
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }

        public string ExportSalesSettlementOutboundAndInboundBillDetailWithGoldenTaxClassify(int companyId, int destCompanyId, int? warehouseId, int? customerAccountId, int? partsSaleCategoryId, int[] inBoundType, int[] outBoundType, DateTime? endTime, int? settleType, int? businessType)
        {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("配件无分类编码清单_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = 查询待销售结算出入库清单明细New(companyId, destCompanyId, warehouseId, customerAccountId, partsSaleCategoryId, inBoundType, outBoundType, endTime, settleType, businessType);
             var dataArray = data.ToArray();
            if(data.Any()) {
                excelColumns.Add(ErrorStrings.ExportColumn_OutCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SparePartCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SparePartName);
                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.BillCode, detail.SparePartCode, detail.SparePartName
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }

        public string ExportSalesSettlementOutboundAndInboundBillDetailCIF(int companyId, int destCompanyId, int? warehouseId, int? customerAccountId, int? partsSaleCategoryId, int[] inBoundType, int[] outBoundType, DateTime? endTime)
        {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("待销售结算出入库明细_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = 查询待销售结算出入库明细采埃孚(companyId, destCompanyId, warehouseId, customerAccountId, partsSaleCategoryId, inBoundType, outBoundType, endTime, null);

            var dataArray = data.ToArray();
            if (data.Any())
            {
                excelColumns.Add(ErrorStrings.ExportColumn_DealerCode);
                excelColumns.Add(ErrorStrings.ExportColumn_DealerName);
                excelColumns.Add(ErrorStrings.ExportColumn_SourceCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SourceType);
                excelColumns.Add(ErrorStrings.ExportColumn_SettlementAmount);
                excelColumns.Add(ErrorStrings.ExportColumn_SourceCreateTime);
                excelColumns.Add(ErrorStrings.ExportColumn_PartWarehouse);

                using (var excelExport = new ExcelExport(exportDataFileName))
                {
                    excelExport.ExportByRow(index => {
                        if (index == dataArray.Count() + 1)
                            return null;
                        if (index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.CounterpartCompanyCode,
                            detail.CounterpartCompanyName,
                            detail.BillCode,
                            Enum.GetName(typeof(DcsPartsLogisticBatchBillDetailBillType),detail.BillType),
                            detail.SettlementAmount,
                            detail.BillCreateTime,
                            detail.WarehouseName
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }

        public string ExportSalesSettlementOutboundAndInboundBillDetailWithGoldenTaxClassifyCIF(int companyId, int destCompanyId, int? warehouseId, int? customerAccountId, int? partsSaleCategoryId, int[] inBoundType, int[] outBoundType, DateTime? endTime)
        {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("配件无分类编码清单_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = 查询待销售结算出入库清单明细采埃孚(companyId, destCompanyId, warehouseId, customerAccountId, partsSaleCategoryId, inBoundType, outBoundType, endTime);
            var dataArray = data.ToArray();
            if (data.Any())
            {
                excelColumns.Add(ErrorStrings.ExportColumn_OutCode);
                excelColumns.Add("配件图号");
                excelColumns.Add("配件名称");
                using (var excelExport = new ExcelExport(exportDataFileName))
                {
                    excelExport.ExportByRow(index => {
                        if (index == dataArray.Count() + 1)
                            return null;
                        if (index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.BillCode, detail.SparePartCode, detail.SparePartName
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }


        public string ExportPurchaseSettlementOutboundAndInboundBillDetail(int storageCompanyId, int counterpartCompanyId, int? warehouseId, int? customerAccountId, int? partsSaleCategoryId, int[] inboundTypes, int[] outboundTypes, DateTime? endTime) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("待采购结算出入库采购价为0明细_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = 查询带采购价的待采购结算出入库明细(storageCompanyId, counterpartCompanyId, warehouseId, customerAccountId, partsSaleCategoryId, inboundTypes, outboundTypes, endTime);

            var dataArray = data.ToArray();
            if(data.Any()) {
                excelColumns.Add("出入库单据编号");
                excelColumns.Add(ErrorStrings.ExportColumn_WarehouseName);
                excelColumns.Add("配件图号");
                excelColumns.Add("配件名称");
                excelColumns.Add("采购价格");

                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.BillCode,
                            detail.WarehouseName,
                            detail.SparePartCode,
                            detail.SparePartName,
                            detail.PurchasePrice
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }

        public string ExportPurchaseSettlementOutboundAndInboundBillDetailForWith(int partsSalesCategoryId, int partsSupplierId, int partsPurchasePricingStrategy, int[] inboundTypes, int[] outboundTypes) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("待采购结算出入库采购价为0明细_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));

            // var data;
            // data = 查询待结算出入库明细清单(inboundTypes, outboundTypes, null);


            List<OutboundAndInboundDetail> data;
            IQueryable<OutboundAndInboundDetail> data1;
            string billCode = "", warehouseName = "";
            if(partsPurchasePricingStrategy == (int)DcsBranchstrategyPartsPurchasePricingStrategy.先定价) {
                data1 = 查询待结算出入库明细清单(inboundTypes, outboundTypes, null);
                var partsInboundCheckBillBillId1 = data1.Where(o => o.BillType == (int)DcsPartsLogisticBatchBillDetailBillType.配件入库检验单).Select(p => p.BillId);
                var partsOutboundBillBillId1 = data1.Where(o => o.BillType == (int)DcsPartsLogisticBatchBillDetailBillType.配件出库单).Select(p => p.BillId);
                var partsInboundCheckBills1 = this.ObjectContext.PartsInboundCheckBills.Where(v => partsInboundCheckBillBillId1.Contains(v.Id)).ToArray();
                var partsOutboundBills1 = this.ObjectContext.PartsOutboundBills.Where(v => partsOutboundBillBillId1.Contains(v.Id)).ToArray();

                var dataArray = data1.Where(v => v.SettlementPrice == 0).Distinct().ToArray();
                if(data1.Any()) {
                    excelColumns.Add("出入库单据编号");
                    excelColumns.Add(ErrorStrings.ExportColumn_WarehouseName);
                    excelColumns.Add("配件图号");
                    excelColumns.Add("配件名称");
                    excelColumns.Add("采购价格");

                    using(var excelExport = new ExcelExport(exportDataFileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == dataArray.Count() + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray<Object>();
                            var detail = dataArray[index - 1];
                            if(detail.BillType == (int)DcsPartsLogisticBatchBillDetailBillType.配件入库检验单) {
                                billCode = partsInboundCheckBills1.FirstOrDefault(v => v.Id == detail.BillId).Code;
                                warehouseName = partsInboundCheckBills1.FirstOrDefault(v => v.Id == detail.BillId).WarehouseName;
                            } else {
                                billCode = partsOutboundBills1.FirstOrDefault(v => v.Id == detail.BillId).Code;
                                warehouseName = partsOutboundBills1.FirstOrDefault(v => v.Id == detail.BillId).WarehouseName;
                            }
                            var values = new object[] {
                            billCode,
                            warehouseName,
                            detail.SparePartCode,
                            detail.SparePartName,
                            detail.SettlementPrice
                        };
                            return values;
                        });
                    }
                }
            } else {
                data = 查询待结算出入库明细清单后定价(inboundTypes, outboundTypes, partsSalesCategoryId, partsSupplierId);
                var partsInboundCheckBillBillId = data.Where(o => o.BillType == (int)DcsPartsLogisticBatchBillDetailBillType.配件入库检验单).Select(p => p.BillId);
                var partsOutboundBillBillId = data.Where(o => o.BillType == (int)DcsPartsLogisticBatchBillDetailBillType.配件出库单).Select(p => p.BillId);
                var partsInboundCheckBills = this.ObjectContext.PartsInboundCheckBills.Where(v => partsInboundCheckBillBillId.Contains(v.Id)).ToArray();
                var partsOutboundBills = this.ObjectContext.PartsOutboundBills.Where(v => partsOutboundBillBillId.Contains(v.Id)).ToArray();

                var dataArray = data.Where(v => v.SettlementPrice == 0).Distinct().ToArray();
                if(data.Any()) {
                    excelColumns.Add("出入库单据编号");
                    excelColumns.Add(ErrorStrings.ExportColumn_WarehouseName);
                    excelColumns.Add("配件图号");
                    excelColumns.Add("配件名称");
                    excelColumns.Add("采购价格");

                    using(var excelExport = new ExcelExport(exportDataFileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == dataArray.Count() + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray<Object>();
                            var detail = dataArray[index - 1];
                            if(detail.BillType == (int)DcsPartsLogisticBatchBillDetailBillType.配件入库检验单) {
                                billCode = partsInboundCheckBills.FirstOrDefault(v => v.Id == detail.BillId).Code;
                                warehouseName = partsInboundCheckBills.FirstOrDefault(v => v.Id == detail.BillId).WarehouseName;
                            } else {
                                billCode = partsOutboundBills.FirstOrDefault(v => v.Id == detail.BillId).Code;
                                warehouseName = partsOutboundBills.FirstOrDefault(v => v.Id == detail.BillId).WarehouseName;
                            }
                            var values = new object[] {
                            billCode,
                            warehouseName,
                            detail.SparePartCode,
                            detail.SparePartName,
                            detail.SettlementPrice
                        };
                            return values;
                        });
                    }
                }
            }

            return exportDataFileName;
        }


        public string ExportPurchaseSettlementOutboundAndInboundBillDetailForWithGoldenTaxClassifyCode(int storageCompanyId, int counterpartCompanyId, int? warehouseId, int? customerAccountId, int? partsSaleCategoryId, int[] outBoundTypes, DateTime? endTime) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("配件无分类编码清单导出_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            IQueryable<OutboundAndInboundBill> data1;
            data1 = 查询待采购退货结算出库明细2(storageCompanyId, counterpartCompanyId, warehouseId, customerAccountId, partsSaleCategoryId,outBoundTypes, endTime);

            if(data1.Any()) {
                var dataArray = data1.Where(v => string.IsNullOrEmpty(v.GoldenTaxClassifyCode)).Distinct().ToArray();
                if(dataArray.Length > 0) {
                    excelColumns.Add(ErrorStrings.ExportColumn_OutInCode);
                    excelColumns.Add(ErrorStrings.ExportColumn_SparePartCode);
                    excelColumns.Add(ErrorStrings.ExportColumn_SparePartName);
                    using(var excelExport = new ExcelExport(exportDataFileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == dataArray.Count() + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray<Object>();
                            var detail = dataArray[index - 1];
                            var values = new object[] {
                                detail.BillCode, detail.SparePartCode, detail.SparePartName
                            };
                            return values;
                        });
                    }
                }
                return exportDataFileName;
            } else {
                return null;
            }
            

        }
    }
}

