﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string ExportPartsInboundCheckBill(int[] ids, string code, DateTime? startTime, DateTime? endTime, string cpPartsPurchaseOrderCode, string cpPartsInboundCheckCode) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("配件入库检验单供应商_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = GetPartsInboundCheckBillWithDetailBySupplierId();

            if(ids != null && ids.Any()) {
                data = data.Where(r => ids.Contains(r.Id));
            } else {
                if(!string.IsNullOrEmpty(code)) {
                    data = data.Where(r => r.Code.Contains(code));
                }
                if(startTime.HasValue) {
                    data = data.Where(r => r.CreateTime >= startTime.Value);
                }
                if(endTime.HasValue) {
                    data = data.Where(r => r.CreateTime <= endTime.Value);
                }
                if(!string.IsNullOrEmpty(cpPartsPurchaseOrderCode)) {
                    data = data.Where(r => r.CPPartsPurchaseOrderCode.Contains(cpPartsPurchaseOrderCode));
                }
                if(!string.IsNullOrEmpty(cpPartsInboundCheckCode)) {
                    data = data.Where(r => r.CPPartsInboundCheckCode.Contains(cpPartsInboundCheckCode));
                }
            }

            var dataArray = data.ToArray();
            if(data.Any()) {
                excelColumns.Add(ErrorStrings.ExportColumn_PartsInboundCheckBill_Code);
                excelColumns.Add(ErrorStrings.ExportColumn_WarehouseCode);
                excelColumns.Add(ErrorStrings.ExportColumn_WarehouseCode);
                excelColumns.Add(ErrorStrings.ExportColumn_InType);
                excelColumns.Add(ErrorStrings.ExportColumn_TotalAmount);
                excelColumns.Add(ErrorStrings.ExportColumn_OriginalRequirementBillCode);
                excelColumns.Add(ErrorStrings.ExportColumn_PartsInboundPlan_Code);
                excelColumns.Add(ErrorStrings.ExportColumn_PartsSalesCategory);
                excelColumns.Add(ErrorStrings.ExportColumn_Branch2);
                excelColumns.Add(ErrorStrings.ExportColumn_CounterpartCompanyCode);
                excelColumns.Add(ErrorStrings.ExportColumn_CounterpartCompanyName);
                excelColumns.Add(ErrorStrings.ExportColumn_ObjId);
                excelColumns.Add(ErrorStrings.ExportColumn_SettleProp);
                excelColumns.Add(ErrorStrings.ExportColumn_Remark);
                excelColumns.Add(ErrorStrings.ExportColumn_Creator);
                excelColumns.Add(ErrorStrings.ExportColumn_CreateTime);
                excelColumns.Add(ErrorStrings.ExportColumn_Modifier);
                excelColumns.Add(ErrorStrings.ExportColumn_ModifyTime);
                excelColumns.Add(ErrorStrings.ExportColumn_OriginalPurchaseOrderCode);
                excelColumns.Add(ErrorStrings.ExportColumn_OriginalInCode);

                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.Code,
                            detail.WarehouseCode,
                            detail.WarehouseName,
                            Enum.GetName(typeof(DcsPartsInboundType),detail.InboundType),
                            detail.PartsInboundCheckBillDetails.Sum(r => r.SettlementPrice * r.InspectedQuantity),
                            detail.OriginalRequirementBillCode,
                            detail.PartsInboundPlan!=null?detail.PartsInboundPlan.Code:"",
                            detail.PartsSalesCategory!=null?detail.PartsSalesCategory.Name:"",
                            detail.BranchName,
                            detail.CounterpartCompanyCode,
                            detail.CounterpartCompanyName,
                            detail.Objid,
                            Enum.GetName(typeof(DcsPartsSettlementStatus),detail.SettlementStatus),
                            detail.Remark,
                            detail.CreatorName,
                            detail.CreateTime,
                            detail.ModifierName,
                            detail.ModifyTime,
                            detail.CPPartsPurchaseOrderCode,
                            detail.CPPartsInboundCheckCode
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }

        public string ExportPartsInboundCheckBillNotSupplier(int[] ids, string originalRequirementBillCode, string code, int? warehouseId, int? inboundType, DateTime? begianDate, DateTime? endDate, string counterpartCompanyName, int? settlementStatus, string sparePartCode, string sparePartName, string partsInboundPlanCode, string batchNumber) {
            var userInfo = Utils.GetCurrentUserInfo();
            var query = 仓库人员查询配件入库检验单New(null, null, null, null, null,null).Where(r => r.StorageCompanyId == userInfo.EnterpriseId);

            if(ids != null && ids.Any()) {
                query = query.Where(r => ids.Contains(r.Id));
            } else {
                if(!string.IsNullOrEmpty(code)) {
                    query = query.Where(r => r.Code.Contains(code));
                }
                if(!string.IsNullOrEmpty(originalRequirementBillCode)) {
                    query = query.Where(r => r.OriginalRequirementBillCode.Contains(originalRequirementBillCode));
                }
                if(warehouseId.HasValue) {
                    query = query.Where(r => r.WarehouseId == warehouseId.Value);
                }
                if(inboundType.HasValue) {
                    query = query.Where(r => r.InboundType == inboundType.Value);
                }
                if(begianDate.HasValue) {
                    query = query.Where(r => r.CreateTime >= begianDate.Value);
                }
                if(endDate.HasValue) {
                    query = query.Where(r => r.CreateTime <= endDate.Value);
                }
                if(!string.IsNullOrEmpty(counterpartCompanyName)) {
                    query = query.Where(r => r.CounterpartCompanyName.Contains(counterpartCompanyName));
                }
                if(settlementStatus.HasValue) {
                    query = query.Where(r => r.SettlementStatus == settlementStatus.Value);
                }
                if(!string.IsNullOrEmpty(sparePartCode)) {
                    query = query.Where(r => ObjectContext.PartsInboundCheckBillDetails.Any(w => w.SparePartCode.Contains(sparePartCode)));
                }
                if(!string.IsNullOrEmpty(sparePartName)) {
                    query = query.Where(r => ObjectContext.PartsInboundCheckBillDetails.Any(w => w.SparePartName.Contains(sparePartName)));
                }
                if(!string.IsNullOrEmpty(partsInboundPlanCode)) {
                    query = query.Where(r => r.PartsInboundPlanCode.Contains(partsInboundPlanCode));
                }
                if(!string.IsNullOrEmpty(batchNumber)) {
                    query = query.Where(r => ObjectContext.PartsInboundCheckBillDetails.Any(w => w.BatchNumber.Contains(batchNumber)));
                }
            }
            var partsInboundCheckBills = query.ToArray();
            var queryReuslt = partsInboundCheckBills.Select(r => new object[] {
                r.Code,
                r.WarehouseName,
                Enum.GetName(typeof(DcsPartsInboundType),r.InboundType),
                r.CounterpartCompanyCode,
                r.CounterpartCompanyName,                
                r.TotalAmount,
                r.PartsPurchaseOrderTypeNameQuery,                
                r.OriginalRequirementBillCode,
                r.PartsInboundPlanCode,
                r.PartsSalesCategoryName,
                r.BranchName,
                r.WarehouseCode,                                
                r.PlanSourceQuery,   
                r.Objid,
                Enum.GetName(typeof(DcsPartsInboundCheckBillStatus),r.Status),
                Enum.GetName(typeof(DcsPartsSettlementStatus),r.SettlementStatus),
                Enum.GetName(typeof(DcsPartsSalesReturnBillReturnType),r.ReturnType),
                r.Remark,
                r.ERPSourceOrderCode,
                r.CreatorName,
                r.CreateTime,
                r.ModifierName,
                r.ModifyTime
            }).ToArray();

            var excelColumns = new List<string> {
                ErrorStrings.ExportColumn_PartsInboundCheckBill_Code,
                ErrorStrings.ExportColumn_WarehouseCode,
                ErrorStrings.ExportColumn_InType,
                ErrorStrings.ExportColumn_CounterpartCompanyCode,
                ErrorStrings.ExportColumn_CounterpartCompanyName,
                ErrorStrings.ExportColumn_TotalAmount,
                ErrorStrings.ExportColumn_PurchaseOrderType,
                ErrorStrings.ExportColumn_OriginalRequirementBillCode,
                ErrorStrings.ExportColumn_PartsInboundPlan_Code,
                ErrorStrings.ExportColumn_PartsSalesCategory,
                ErrorStrings.ExportColumn_Branch2,
                ErrorStrings.ExportColumn_WarehouseCode,                               
                ErrorStrings.ExportColumn_PlanSource,                              
                ErrorStrings.ExportColumn_ObjId,
                ErrorStrings.ExportColumn_Status,
                ErrorStrings.ExportColumn_SettlementStatus,
                ErrorStrings.ExportColumn_ReturnBillType,
                ErrorStrings.ExportColumn_Remark,
                ErrorStrings.ExportColumn_ERPSourceOrderCode,
                ErrorStrings.ExportColumn_Creator,
                ErrorStrings.ExportColumn_CreateTime,
                ErrorStrings.ExportColumn_Modifier,
                ErrorStrings.ExportColumn_ModifyTime
            };
            return ExportAllData(queryReuslt, excelColumns, "配件入库检验单");
        }

        public string ExportPartsInboundCheckBillNotSupplierNoPrice(int[] ids, string originalRequirementBillCode, int? partsSalesCategoryId, string code, int? warehouseId, int? inboundType, DateTime? begianDate, DateTime? endDate, string counterpartCompanyCode, string counterpartCompanyName, string eRPOrderCode, string partsSalesOrderCode, int? settlementStatus, int? partsSalesOrderInvoiceType, int? partsPurchaseOrderTypeId, string sAPPurchasePlanCode) {
            var userInfo = Utils.GetCurrentUserInfo();
            var query = 仓库人员查询配件入库检验单New(partsPurchaseOrderTypeId, null, null,null,null,null).Where(r => r.StorageCompanyId == userInfo.EnterpriseId);

            if(ids != null && ids.Any()) {
                query = query.Where(r => ids.Contains(r.Id));
            } else {
                if(partsSalesCategoryId.HasValue) {
                    query = query.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId.Value);
                }
                if(!string.IsNullOrEmpty(code)) {
                    query = query.Where(r => r.Code.Contains(code));
                }
                if(!string.IsNullOrEmpty(originalRequirementBillCode)) {
                    query = query.Where(r => r.OriginalRequirementBillCode.Contains(originalRequirementBillCode));
                }
                if(warehouseId.HasValue) {
                    query = query.Where(r => r.WarehouseId == warehouseId.Value);
                }
                if(inboundType.HasValue) {
                    query = query.Where(r => r.InboundType == inboundType.Value);
                }
                if(begianDate.HasValue) {
                    query = query.Where(r => r.CreateTime >= begianDate.Value);
                }
                if(endDate.HasValue) {
                    query = query.Where(r => r.CreateTime <= endDate.Value);
                }
                if(!string.IsNullOrEmpty(counterpartCompanyCode)) {
                    query = query.Where(r => r.CounterpartCompanyCode.Contains(counterpartCompanyCode));
                }
                if(!string.IsNullOrEmpty(counterpartCompanyName)) {
                    query = query.Where(r => r.CounterpartCompanyName.Contains(counterpartCompanyName));
                }
                //PartsInboundPlan.PartsSalesReturnBill.PartsSalesOrder.ERPOrderCode
                if(!string.IsNullOrEmpty(eRPOrderCode)) {
                    query = query.Where(r => r.ERPSourceOrderCode.Contains(eRPOrderCode));
                }
                if(!string.IsNullOrEmpty(sAPPurchasePlanCode)) {
                    query = query.Where(r => r.SAPPurchasePlanCode.Contains(sAPPurchasePlanCode));
                }
                if(!string.IsNullOrEmpty(partsSalesOrderCode)) {
                    query = query.Where(r => r.PartsSalesOrderCode.Contains(partsSalesOrderCode));
                }
                if(partsSalesOrderInvoiceType.HasValue) {
                    query = query.Where(r => r.InvoiceType == partsSalesOrderInvoiceType.Value);
                }
                if(settlementStatus.HasValue) {
                    query = query.Where(r => r.SettlementStatus == settlementStatus.Value);
                }
            }
            var partsInboundCheckBills = query.ToArray();
            var partsPurchaseOrders = GetPartsPurchaseOrderByIds(partsInboundCheckBills.Where(r => r.InboundType == (int)DcsPartsInboundType.配件采购).Select(r => r.OriginalRequirementBillId).ToArray()).ToArray();

            var queryReuslt = partsInboundCheckBills.Select(r => new object[] {
                r.Code,
                r.WarehouseCode,
                r.WarehouseName,
                Enum.GetName(typeof(DcsPartsInboundType),r.InboundType),
                r.OriginalRequirementBillCode,
                r.PartsInboundPlanCode,
                r.PartsSalesCategoryName,
                r.BranchName,
                r.PlanSourceQuery,
                r.CounterpartCompanyCode,
                r.CounterpartCompanyName,  
                //r.PartsInboundCheckBillDetails.Sum(v=>v.SettlementPrice * v.InspectedQuantity),
                r.PartsPurchaseOrderTypeNameQuery,
                r.Objid,
                r.GPMSPurOrderCode,
                Enum.GetName(typeof(DcsPartsInboundCheckBillStatus),r.Status),
                Enum.GetName(typeof(DcsPartsSettlementStatus),r.SettlementStatus),
                Enum.GetName(typeof(DcsPartsSalesReturnBillReturnType),r.ReturnType),
                r.Remark,
                //r.PartsInboundPlan.PartsSalesReturnBill.PartsSalesOrder==null?null: r.PartsInboundPlan.PartsSalesReturnBill.PartsSalesOrder.ERPOrderCode,
                //r.PartsInboundPlan==null?null: (r.PartsInboundPlan.PartsSalesReturnBill==null?null:(r.PartsInboundPlan.PartsSalesReturnBill.PartsSalesOrder==null?null:(r.PartsInboundPlan.PartsSalesReturnBill.PartsSalesOrder.ERPSourceOrderCode))),
                r.ERPSourceOrderCode,
                r.SAPPurchasePlanCode,
                r.SettlementStatus,
                //r.PartsInboundPlan.PartsSalesReturnBill.PartsSalesOrder==null?null:r.PartsInboundPlan.PartsSalesReturnBill.PartsSalesOrder.Code,
                  r.PartsSalesOrderCode,
                //r.PartsInboundPlan.PartsSalesReturnBill.PartsSalesOrder==null?null:r.PartsInboundPlan.PartsSalesReturnBill.PartsSalesOrder.InvoiceType==null?null:Enum.GetName(typeof(DcsERPInvoiceInformation_Type),r.PartsInboundPlan.PartsSalesReturnBill.PartsSalesOrder.InvoiceType),
                  r.InvoiceType,
                r.CreatorName,
                r.CreateTime,
                r.ModifierName,
                r.ModifyTime
            }).ToArray();

            var excelColumns = new List<string> {
                ErrorStrings.ExportColumn_PartsInboundCheckBill_Code,
                ErrorStrings.ExportColumn_WarehouseCode,
                ErrorStrings.ExportColumn_WarehouseCode,
                ErrorStrings.ExportColumn_InType,
                ErrorStrings.ExportColumn_OriginalRequirementBillCode,
                ErrorStrings.ExportColumn_PartsInboundPlan_Code,
                ErrorStrings.ExportColumn_PartsSalesCategory,
                ErrorStrings.ExportColumn_Branch2,
                ErrorStrings.ExportColumn_PlanSource,
                ErrorStrings.ExportColumn_CounterpartCompanyCode,
                ErrorStrings.ExportColumn_CounterpartCompanyName,
                //ErrorStrings.ExportColumn_TotalAmount,
                ErrorStrings.ExportColumn_PurchaseOrderType,
                ErrorStrings.ExportColumn_ObjId,
                ErrorStrings.ExportColumn_GPMSPurchaseOrderCode,
                ErrorStrings.ExportColumn_Status,
                ErrorStrings.ExportColumn_SettlementStatus,
                ErrorStrings.ExportColumn_ReturnBillType,
                ErrorStrings.ExportColumn_Remark,
                ErrorStrings.ExportColumn_ERPSourceOrderCode,
                ErrorStrings.ExportColumn_SAPPurchasePlanCode,
                ErrorStrings.ExportColumn_PartsSalesOrder_Code,
                ErrorStrings.ExportColumn_InvoiceType,
                ErrorStrings.ExportColumn_Creator,
                ErrorStrings.ExportColumn_CreateTime,
                ErrorStrings.ExportColumn_Modifier,
                ErrorStrings.ExportColumn_ModifyTime
            };
            return ExportAllData(queryReuslt, excelColumns, "配件入库检验单");
        }
    }
}
