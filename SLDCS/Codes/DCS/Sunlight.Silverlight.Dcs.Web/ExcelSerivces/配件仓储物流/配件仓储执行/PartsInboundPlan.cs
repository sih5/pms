﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string ExportPartsInboundPlan(int[] ids, int? partsSalesCategoryId, int? status, string code, string sourceCode, int? warehouseId, int? inboundType, DateTime? begianDate, DateTime? endDate, string counterpartCompanyCode, string counterpartCompanyName, string eRPOrderCode, string partsSalesOrderCode, int? partsPurchaseOrderTypeId, string SAPPurchasePlanCode) {
            var userInfo = Utils.GetCurrentUserInfo();
            var query = 仓库人员查询配件入库计划(partsPurchaseOrderTypeId, null, null, null).Where(r => r.StorageCompanyId == userInfo.EnterpriseId);

            if(ids != null && ids.Any()) {
                query = query.Where(r => ids.Contains(r.Id));
            } else {
                if(partsSalesCategoryId.HasValue) {
                    query = query.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId.Value);
                }
                if(status.HasValue) {
                    query = query.Where(r => r.Status == status.Value);
                }
                if(!string.IsNullOrEmpty(code)) {
                    query = query.Where(r => r.Code.Contains(code));
                }
                if(!string.IsNullOrEmpty(sourceCode)) {
                    query = query.Where(r => r.SourceCode.Contains(sourceCode));
                }
                if(warehouseId.HasValue) {
                    query = query.Where(r => r.WarehouseId == warehouseId.Value);
                }

                if(inboundType.HasValue) {
                    query = query.Where(r => r.InboundType == inboundType.Value);
                }
                if(begianDate.HasValue) {
                    query = query.Where(r => r.CreateTime >= begianDate.Value);
                }
                if(endDate.HasValue) {
                    query = query.Where(r => r.CreateTime <= endDate.Value);
                }
                if(!string.IsNullOrEmpty(counterpartCompanyCode)) {
                    query = query.Where(r => r.CounterpartCompanyCode.Contains(counterpartCompanyCode));
                }
                if(!string.IsNullOrEmpty(counterpartCompanyName)) {
                    query = query.Where(r => r.CounterpartCompanyName.Contains(counterpartCompanyName));
                }
                if(!string.IsNullOrEmpty(SAPPurchasePlanCode)) {
                    query = query.Where(r => r.SAPPurchasePlanCode.Contains(SAPPurchasePlanCode));
                }
            }
            var partsInboundPlans = query.ToArray();
            var partsPurchaseOrders = GetPartsPurchaseOrderByIds(partsInboundPlans.Where(r => r.InboundType == (int)DcsPartsInboundType.配件采购).Select(r => r.OriginalRequirementBillId).ToArray()).ToArray();

            foreach(var partsInboundPlan in partsInboundPlans.Where(r => r.InboundType == (int)DcsPartsInboundType.配件采购)) {
                var partsPurchaseOrder = partsPurchaseOrders.FirstOrDefault(r => r.Id == partsInboundPlan.OriginalRequirementBillId);
                if(partsPurchaseOrder != null) {
                    partsInboundPlan.PartsPurchaseOrderTypeName = partsPurchaseOrder.PartsPurchaseOrderType.Name;
                    partsInboundPlan.PlanSource = partsPurchaseOrder.PlanSource;
                }
            }
            var queryReuslt = partsInboundPlans.Select(r => new object[] {
                r.Code,
                r.WarehouseName,
                Enum.GetName(typeof(DcsPartsInboundType),r.InboundType),
                r.CounterpartCompanyCode,
                r.CounterpartCompanyName,
                r.PartsInboundPlanDetails.Sum(v=>v.Price*v.PlannedAmount),
                r.PartsPurchaseOrderTypeName,
                r.OriginalRequirementBillCode,
                r.PartsSalesCategory!=null?r.PartsSalesCategory.Name:string.Empty,
                r.BranchName,
                r.WarehouseCode,
                r.PlanSource,               
                Enum.GetName(typeof(DcsPartsInboundPlanStatus),r.Status),
                r.SourceCode,
                Enum.GetName(typeof(DcsPartsSalesReturnBillReturnType),r.PartsSalesReturnBill==null?-1:r.PartsSalesReturnBill.ReturnType),
                r.Remark,
                r.ERPSourceOrderCode,
                r.PartsSalesOrderCode,
                r.CreatorName,
                r.CreateTime,
                r.ModifierName,
                r.ModifyTime,
                r.ArrivalDate,
                r.SAPPurchasePlanCode
            }).ToArray();

            var excelColumns = new List<string> {
                ErrorStrings.ExportColumn_PartsInboundPlan_Code,
                ErrorStrings.ExportColumn_WarehouseName,
                ErrorStrings.ExportColumn_InType,
                ErrorStrings.ExportColumn_CounterpartCompanyCode,
                ErrorStrings.ExportColumn_CounterpartCompanyName,
                ErrorStrings.ExportColumn_TotalAmount,
                ErrorStrings.ExportColumn_PurchaseOrderType,
                ErrorStrings.ExportColumn_OriginalCode,
                ErrorStrings.ExportColumn_PartsSalesCategory,
                ErrorStrings.ExportColumn_Branch2,
                ErrorStrings.ExportColumn_WarehouseCode,                 
                ErrorStrings.ExportColumn_PlanSource,                
                ErrorStrings.ExportColumn_Status,                
                ErrorStrings.ExportColumn_SourceCode,
				ErrorStrings.ExportColumn_ReturnBillType,
                ErrorStrings.ExportColumn_Remark,
                ErrorStrings.ExportColumn_ERPSourceOrderCode,
                ErrorStrings.ExportColumn_PartsSalesOrder_Code,
                ErrorStrings.ExportColumn_Creator,
                ErrorStrings.ExportColumn_CreateTime,
                ErrorStrings.ExportColumn_Modifier,
                ErrorStrings.ExportColumn_ModifyTime,
                ErrorStrings.ExportColumn_ArriveDate,
                ErrorStrings.ExportColumn_SAPPurchasePlanCode
            };
            return ExportAllData(queryReuslt, excelColumns, "配件入库计划单");
        }

        public string ExportPartsInboundPlanNoPrice(int[] ids, int? partsSalesCategoryId, int? status, string code, string sourceCode, int? warehouseId, int? inboundType, DateTime? begianDate, DateTime? endDate, string counterpartCompanyCode, string counterpartCompanyName, string eRPOrderCode, string partsSalesOrderCode, int? partsPurchaseOrderTypeId) {
            var userInfo = Utils.GetCurrentUserInfo();
            var query = 仓库人员查询配件入库计划(partsPurchaseOrderTypeId, null, null, null).Where(r => r.StorageCompanyId == userInfo.EnterpriseId);

            if(ids != null && ids.Any()) {
                query = query.Where(r => ids.Contains(r.Id));
            } else {
                if(partsSalesCategoryId.HasValue) {
                    query = query.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId.Value);
                }
                if(status.HasValue) {
                    query = query.Where(r => r.Status == status.Value);
                }
                if(!string.IsNullOrEmpty(code)) {
                    query = query.Where(r => r.Code.Contains(code));
                }
                if(!string.IsNullOrEmpty(sourceCode)) {
                    query = query.Where(r => r.SourceCode.Contains(sourceCode));
                }
                if(warehouseId.HasValue) {
                    query = query.Where(r => r.WarehouseId == warehouseId.Value);
                }

                if(inboundType.HasValue) {
                    query = query.Where(r => r.InboundType == inboundType.Value);
                }
                if(begianDate.HasValue) {
                    query = query.Where(r => r.CreateTime >= begianDate.Value);
                }
                if(endDate.HasValue) {
                    query = query.Where(r => r.CreateTime <= endDate.Value);
                }
                if(!string.IsNullOrEmpty(counterpartCompanyCode)) {
                    query = query.Where(r => r.CounterpartCompanyCode.Contains(counterpartCompanyCode));
                }
                if(!string.IsNullOrEmpty(counterpartCompanyName)) {
                    query = query.Where(r => r.CounterpartCompanyName.Contains(counterpartCompanyName));
                }
            }
            var partsInboundPlans = query.ToArray();
            var partsPurchaseOrders = GetPartsPurchaseOrderByIds(partsInboundPlans.Where(r => r.InboundType == (int)DcsPartsInboundType.配件采购).Select(r => r.OriginalRequirementBillId).ToArray()).ToArray();

            foreach(var partsInboundPlan in partsInboundPlans.Where(r => r.InboundType == (int)DcsPartsInboundType.配件采购)) {
                var partsPurchaseOrder = partsPurchaseOrders.FirstOrDefault(r => r.Id == partsInboundPlan.OriginalRequirementBillId);
                if(partsPurchaseOrder != null) {
                    partsInboundPlan.PartsPurchaseOrderTypeName = partsPurchaseOrder.PartsPurchaseOrderType.Name;
                    partsInboundPlan.PlanSource = partsPurchaseOrder.PlanSource;
                }
            }
            var queryReuslt = partsInboundPlans.Select(r => new object[] {
                r.Code,
                r.WarehouseCode,
                r.WarehouseName,
                Enum.GetName(typeof(DcsPartsInboundType),r.InboundType),
                r.PartsSalesCategory!=null?r.PartsSalesCategory.Name:string.Empty,
                r.BranchName,
                //r.PartsPurchaseOrderTypeName,
                r.PlanSource,
                r.CounterpartCompanyCode,
                r.CounterpartCompanyName,
                //r.PartsInboundPlanDetails.Sum(v=>v.Price*v.PlannedAmount),
                r.PartsPurchaseOrderTypeName,
                Enum.GetName(typeof(DcsPartsInboundPlanStatus),r.Status),
                r.OriginalRequirementBillCode,
                r.SourceCode,
                Enum.GetName(typeof(DcsPartsSalesReturnBillReturnType),r.PartsSalesReturnBill==null?-1:r.PartsSalesReturnBill.ReturnType),
                r.Remark,
                r.ERPSourceOrderCode,
                r.PartsSalesOrderCode,
                r.CreatorName,
                r.CreateTime,
                r.ModifierName,
                r.ModifyTime,
                r.ArrivalDate
            }).ToArray();

            var excelColumns = new List<string> {
                ErrorStrings.ExportColumn_PartsInboundPlan_Code,
                ErrorStrings.ExportColumn_WarehouseCode,
                ErrorStrings.ExportColumn_WarehouseName,
                ErrorStrings.ExportColumn_InType,
                ErrorStrings.ExportColumn_PartsSalesCategory,
                ErrorStrings.ExportColumn_Branch2,
                ErrorStrings.ExportColumn_PlanSource,
                ErrorStrings.ExportColumn_CounterpartCompanyCode,
                ErrorStrings.ExportColumn_CounterpartCompanyName,
                //.ExportColumn_TotalAmount,
                ErrorStrings.ExportColumn_PurchaseOrderType,
                ErrorStrings.ExportColumn_Status,
                ErrorStrings.ExportColumn_OriginalCode,
                ErrorStrings.ExportColumn_SourceCode,
                ErrorStrings.ExportColumn_ReturnBillType,
                ErrorStrings.ExportColumn_Remark,
                ErrorStrings.ExportColumn_ERPSourceOrderCode,
                ErrorStrings.ExportColumn_PartsSalesOrder_Code,
                ErrorStrings.ExportColumn_Creator,
                ErrorStrings.ExportColumn_CreateTime,
                ErrorStrings.ExportColumn_Modifier,
                ErrorStrings.ExportColumn_ModifyTime,
                ErrorStrings.ExportColumn_ArriveDate
            };
            return ExportAllData(queryReuslt, excelColumns, "配件入库计划单");
        }

    }
}
