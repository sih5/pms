﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string ExportPartsOutboundPlan(int[] ids, string code, string sourceCode, int? warehouseId, int? status, int? outboundType, DateTime? begianDate, DateTime? endDate, string counterpartCompanyName, string partsSalesOrderTypeName, string SAPPurchasePlanCode, string ZPNUMBER, string eRPSourceOrderCode, string sparePartCode, string sparePartName)
        {
            var userInfo = Utils.GetCurrentUserInfo();
            var query = GetPartsOutboundPlansWithPartsCategory(sparePartCode, sparePartName).Where(r => r.StorageCompanyId == userInfo.EnterpriseId);

            if(ids != null && ids.Any()) {
                query = query.Where(r => ids.Contains(r.Id));
            } else {
              
                if(!string.IsNullOrEmpty(code)) {
                    query = query.Where(r => r.Code.Contains(code));
                }
                if(!string.IsNullOrEmpty(sourceCode)) {
                    query = query.Where(r => r.SourceCode.Contains(sourceCode));
                }
                if(!string.IsNullOrEmpty(eRPSourceOrderCode)) {
                    query = query.Where(r => r.ERPSourceOrderCode.Contains(eRPSourceOrderCode));
                }

                if(warehouseId.HasValue) {
                    query = query.Where(r => r.WarehouseId == warehouseId.Value);
                }
                if(status.HasValue) {
                    query = query.Where(r => r.Status == status.Value);
                }
                if(outboundType.HasValue) {
                    query = query.Where(r => r.OutboundType == outboundType.Value);
                }
                if(begianDate.HasValue) {
                    query = query.Where(r => r.CreateTime >= begianDate.Value);
                }
                if(endDate.HasValue) {
                    query = query.Where(r => r.CreateTime <= endDate.Value);
                }
               
                if(!string.IsNullOrEmpty(counterpartCompanyName)) {
                    query = query.Where(r => r.CounterpartCompanyName.Contains(counterpartCompanyName));
                }
                if(!string.IsNullOrEmpty(partsSalesOrderTypeName)) {
                    query = query.Where(r => r.PartsSalesOrderTypeName.Contains(partsSalesOrderTypeName));
                }
                if(!string.IsNullOrEmpty(SAPPurchasePlanCode)) {
                    query = query.Where(r => r.SAPPurchasePlanCode.Contains(SAPPurchasePlanCode));
                }
                if(!string.IsNullOrEmpty(ZPNUMBER)) {
                    query = query.Where(r => r.ZPNUMBER.Contains(ZPNUMBER));
                }
            }
            var queryReuslt = query.ToArray().Select(r => new object[] {
                r.Code,
                r.WarehouseCode,
                r.WarehouseName,
                Enum.GetName(typeof(DcsPartsOutboundType),r.OutboundType),
                r.PartsOutboundPlanDetails.Sum(v=>v.Price*v.PlannedAmount),
                r.EcommerceMoney,
                r.OriginalRequirementBillCode,
                r.PartsSalesCategory.Name,
                r.BranchName,
                r.PartsSalesOrderTypeName,
                r.CounterpartCompanyCode,
                r.CounterpartCompanyName,
                r.ReceivingWarehouseCode,
                r.ReceivingWarehouseName,
                Enum.GetName(typeof(DcsPartsOutboundPlanStatus),r.Status),
                r.OrderApproveComment,
                r.Remark,
                r.StopComment,
                r.CreatorName,
                r.CreateTime,
                r.ModifierName,
                r.ModifyTime,
                r.Stoper,
                r.StopTime,
                r.SAPPurchasePlanCode,
                r.ZPNUMBER,
                r.ERPSourceOrderCode
            }).ToArray();

            var excelColumns = new List<string> {
                "出库计划单编号",
                ErrorStrings.ExportColumn_WarehouseCode,
                ErrorStrings.ExportColumn_WarehouseName,
                "出库类型",
                "总金额",
                "电商金额",
                "原始需求单据编号",
                "品牌",
                "营销分公司",
                "销售订单类型",
                "对方单位编号",
                "对方单位名称",
                "收货仓库编号",
                "收货仓库名称",               
                ErrorStrings.ExportColumn_Status,
                "订单审批意见",
                ErrorStrings.ExportColumn_Remark,
                "终止原因",
                ErrorStrings.ExportColumn_Creator,
                ErrorStrings.ExportColumn_CreateTime,
                ErrorStrings.ExportColumn_Modifier,
                ErrorStrings.ExportColumn_ModifyTime,
                "终止人",
                "终止时间",
                "SAP采购计划单号",
                "出库包装订单号",
                "平台单号"
            };
            return ExportAllData(queryReuslt, excelColumns, "配件出库计划单");
        }

        public string ExportAgencyPartsOutboundPlan(int[] ids, int? partsSalesCategoryId, string code, string sourceCode, int? warehouseId, int? status, int? outboundType, DateTime? begianDate, DateTime? endDate, string counterpartCompanyCode, string counterpartCompanyName, string partsSalesOrderTypeName, string ERPSourceOrderCode) {
            var userInfo = Utils.GetCurrentUserInfo();
            var query = GetAgencyPartsOutboundPlansWithPartsCategory();

            if(ids != null && ids.Any()) {
                query = query.Where(r => ids.Contains(r.Id));
            } else {
                if(partsSalesCategoryId.HasValue) {
                    query = query.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId.Value);
                }
                if(!string.IsNullOrEmpty(code)) {
                    query = query.Where(r => r.Code.Contains(code));
                }
                if(!string.IsNullOrEmpty(sourceCode)) {
                    query = query.Where(r => r.SourceCode.Contains(sourceCode));
                }
                if(warehouseId.HasValue) {
                    query = query.Where(r => r.WarehouseId == warehouseId.Value);
                }
                if(status.HasValue) {
                    query = query.Where(r => r.Status == status.Value);
                }
                if(outboundType.HasValue) {
                    query = query.Where(r => r.OutboundType == outboundType.Value);
                }
                if(begianDate.HasValue) {
                    query = query.Where(r => r.CreateTime >= begianDate.Value);
                }
                if(endDate.HasValue) {
                    query = query.Where(r => r.CreateTime <= endDate.Value);
                }
                if(!string.IsNullOrEmpty(counterpartCompanyCode)) {
                    query = query.Where(r => r.CounterpartCompanyCode.Contains(counterpartCompanyCode));
                }
                if(!string.IsNullOrEmpty(counterpartCompanyName)) {
                    query = query.Where(r => r.CounterpartCompanyName.Contains(counterpartCompanyName));
                }
                if(!string.IsNullOrEmpty(partsSalesOrderTypeName)) {
                    query = query.Where(r => r.PartsSalesOrderTypeName.Contains(partsSalesOrderTypeName));
                }
                if(!string.IsNullOrEmpty(ERPSourceOrderCode)) {
                    query = query.Where(r => r.PartsSalesOrderTypeName.Contains(ERPSourceOrderCode));
                }
            }
            var queryReuslt = query.ToArray().Select(r => new object[] {
                r.Code,
                r.WarehouseCode,
                r.WarehouseName,
                Enum.GetName(typeof(DcsPartsOutboundType),r.OutboundType),
                r.APartsOutboundPlanDetails.Sum(x => x.Price * x.PlannedAmount),
                r.OriginalRequirementBillCode,
                r.PartsSalesCategory.Name,
                r.ERPSourceOrderCode,
                r.BranchName,
                r.PartsSalesOrderTypeName,
                r.CounterpartCompanyCode,
                r.CounterpartCompanyName,
                r.ReceivingWarehouseCode,
                r.ReceivingWarehouseName,
                Enum.GetName(typeof(DcsPartsOutboundPlanStatus),r.Status),
                r.Remark,
                r.StopComment,
                r.CreatorName,
                r.CreateTime,
                r.ModifierName,
                r.ModifyTime,
                r.Stoper,
                r.StopTime
            }).ToArray();

            var excelColumns = new List<string> {
                "出库计划单编号",
                ErrorStrings.ExportColumn_WarehouseCode,
                ErrorStrings.ExportColumn_WarehouseName,
                "出库类型",
                "总金额",
                "原始需求单据编号",
                "品牌",
                "ERP平台单号",
                "营销分公司",
                "销售订单类型",
                "对方单位编号",
                "对方单位名称",
                "收货仓库编号",
                "收货仓库名称",               
                ErrorStrings.ExportColumn_Status,
                ErrorStrings.ExportColumn_Remark,
                "终止原因",
                ErrorStrings.ExportColumn_Creator,
                ErrorStrings.ExportColumn_CreateTime,
                ErrorStrings.ExportColumn_Modifier,
                ErrorStrings.ExportColumn_ModifyTime,
                "终止人",
                "终止时间"
            };
            return ExportAllData(queryReuslt, excelColumns, "配件出库计划单-代理库");
        }
    }
}
