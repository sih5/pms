﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string 导出采购订单完成情况统计(DateTime? bPurchasePlanTime, DateTime? ePurchasePlanTime, DateTime? bTheoryDeliveryTime, DateTime? eTheoryDeliveryTime) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("采购订单完成情况统计_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = 采购订单完成情况统计(bPurchasePlanTime, ePurchasePlanTime, bTheoryDeliveryTime, eTheoryDeliveryTime,true);

            var dataArray = data.ToArray();
            if(data.Any()) {
                excelColumns.Add(EntityStrings.PartsPurchaseOrderFinish_Month);
                excelColumns.Add(EntityStrings.PartsPurchaseOrderFinish_Date);
                excelColumns.Add(EntityStrings.PartsPurchaseOrderFinish_Items);
                excelColumns.Add(EntityStrings.PartsPurchaseOrderOutTime_Items);
                excelColumns.Add(EntityStrings.PartsPurchaseOrderFinishOnTime_Items);
                excelColumns.Add(EntityStrings.PartsPurchaseOrderFinishOutTime_Items);
                excelColumns.Add(EntityStrings.PartsPurchaseOrderFinishOutTime_Item_7s);
                excelColumns.Add(EntityStrings.PartsPurchaseOrderFinishOutTime_Item_14s);
                excelColumns.Add(EntityStrings.PartsPurchaseOrderFinishOutTime_Item_21s);
                excelColumns.Add(EntityStrings.PartsPurchaseOrderFinishOutTime_Item_22s);
                excelColumns.Add(EntityStrings.PartsPurchaseOrderFinishOutTime_Item_25s);
                excelColumns.Add(EntityStrings.PartsPurchaseOrderFinishOutTime_Item_23s);
                excelColumns.Add(EntityStrings.PartsPurchaseOrderFinishOutTime_Item_24);

                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.Month,
                            detail.Day,
                            detail.AllItem,
                            detail.ArriveItem,
                            detail.OnTime,
                            detail.OutTime,
                            detail.SevenUn,
                            detail.EightUn,
                            detail.TwentyUn,
                            detail.OverTwentyUn,
                            detail.StopAmount,
                            detail.Undue,
                            detail.Mzl
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
        public string 导出供应商采购订单完成情况统计(DateTime? bPurchasePlanTime, DateTime? ePurchasePlanTime, DateTime? bTheoryDeliveryTime, DateTime? eTheoryDeliveryTime, string supplierName) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("供应商采购订单完成情况统计_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = 供应商采购订单完成情况统计(bPurchasePlanTime, ePurchasePlanTime, bTheoryDeliveryTime, eTheoryDeliveryTime, supplierName,true);

            var dataArray = data.ToArray();
            if(data.Any()) {
                excelColumns.Add(EntityStrings.PartsPurchaseOrderFinish_Month);
                excelColumns.Add(EntityStrings.PartsOuterPurchaselist_Supplier);
                excelColumns.Add(EntityStrings.PartsPurchaseOrderFinish_Item);
                excelColumns.Add(EntityStrings.PartsPurchaseOrderOutTime_Item);
                excelColumns.Add(EntityStrings.PartsPurchaseOrderFinishOnTime_Item);
                excelColumns.Add(EntityStrings.PartsPurchaseOrderFinishOutTime_Item);
                excelColumns.Add(EntityStrings.PartsPurchaseOrderFinishOutTime_Item_7);
                excelColumns.Add(EntityStrings.PartsPurchaseOrderFinishOutTime_Item_14);
                excelColumns.Add(EntityStrings.PartsPurchaseOrderFinishOutTime_Item_21);
                excelColumns.Add(EntityStrings.PartsPurchaseOrderFinishOutTime_Item_22);
                excelColumns.Add(EntityStrings.PartsPurchaseOrderFinishOutTime_Item_25);
                excelColumns.Add(EntityStrings.PartsPurchaseOrderFinishOutTime_Item_23);
                excelColumns.Add(EntityStrings.PartsPurchaseOrderFinishOutTime_Item_24);

                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.Month,
                            detail.SupplierName,
                            detail.AllItem,
                            detail.ArriveItem,
                            detail.OnTime,
                            detail.OutTime,
                            detail.SevenUn,
                            detail.EightUn,
                            detail.TwentyUn,
                            detail.OverTwentyUn,
                            detail.StopAmount,
                            detail.Undue,
                            detail.Mzl
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
        public string 导出采购订单在途明细情况(DateTime? bPurchasePlanTime, DateTime? ePurchasePlanTime, DateTime? bTheoryDeliveryTime, DateTime? eTheoryDeliveryTime, string kvFinishi, int? kvOverTime, string kvUndueTime) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("采购订单在途明细_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = 采购单在途明细表(bPurchasePlanTime, ePurchasePlanTime, bTheoryDeliveryTime, eTheoryDeliveryTime, kvFinishi, kvOverTime, kvUndueTime);

            var dataArray = data.ToArray();
            if(data.Any()) {
                excelColumns.Add(ErrorStrings.ExportColumn_PurchasePlanCode);
                excelColumns.Add(ErrorStrings.ExportColumn_PartsPurchaseOrderCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SupplierName);
                excelColumns.Add(EntityStrings.SupplierShippingDetail_SupplierPartCode);
                excelColumns.Add(ErrorStrings.ExportColumn_PartCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SIHCode);
                excelColumns.Add(ErrorStrings.ExportColumn_MapName);
                excelColumns.Add(ErrorStrings.ExportColumn_PartProp);
                excelColumns.Add(ErrorStrings.ExportColumn_PurchasePlanAmount);
                excelColumns.Add(ErrorStrings.ExportColumn_SupplierConfirmedAmount);
                excelColumns.Add(ErrorStrings.ExportColumn_SupplierConfirmedReason);
                excelColumns.Add(ErrorStrings.ExportColumn_SupplierShippingAmount);
                excelColumns.Add(ErrorStrings.ExportColumn_InboundAmount);
                excelColumns.Add(ErrorStrings.ExportColumn_InboundFinishAmount);
                excelColumns.Add(ErrorStrings.ExportColumn_OnwayAmount);
                excelColumns.Add(ErrorStrings.ExportColumn_PartsPurchaseOrderCreateTime);
                excelColumns.Add(ErrorStrings.ExportColumn_SupplierConfirmedTime);
                excelColumns.Add(ErrorStrings.ExportColumn_SupplierShippingTime);
                excelColumns.Add(ErrorStrings.ExportColumn_DeliveryTime);
                excelColumns.Add(ErrorStrings.ExportColumn_InboundTime);
                excelColumns.Add(ErrorStrings.ExportColumn_PlanType);
                excelColumns.Add(ErrorStrings.ExportColumn_Warehouse);
                excelColumns.Add(ErrorStrings.ExportColumn_SupplyShippingCycle);
                excelColumns.Add(ErrorStrings.ExportColumn_SupplyArrivalCycle);
                excelColumns.Add(ErrorStrings.ExportColumn_TheoreticalArrivalDate);
                excelColumns.Add(ErrorStrings.ExportColumn_OutTime1);
                excelColumns.Add(ErrorStrings.ExportColumn_OutTime2);
                excelColumns.Add(ErrorStrings.ExportColumn_PurchasePlanRemark);
                excelColumns.Add(ErrorStrings.ExportColumn_CompletionSituation);
                excelColumns.Add(ErrorStrings.ExportColumn_Weight);
                excelColumns.Add(ErrorStrings.ExportColumn_Volume);
                excelColumns.Add(ErrorStrings.ExportColumn_DealerPrice);
                excelColumns.Add(ErrorStrings.Export_Title_PartsPurchaseOrderFinish_PersonName);
                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.PurchasePlanCode,
                            detail.PurchaseCode,
                            detail.SupplierName,
                            detail.SupplierPartCode,
                            detail.SpareCode,
                            detail.SihCode,
                            detail.MapName,
                             Enum.GetName(typeof(DcsABCStrategyCategory),detail.PartABC==null?0:detail.PartABC),
                            detail.PurchasePlanQty,
                            detail.SupplyConfirmQty,
                            detail.SupplyConfirmReason,
                            detail.SupplyShippingQty,
                            detail.InboundQty,
                            detail.InboundForceQty,
                            detail.OnLineQty,
                            detail.PurchaseCreateTime,
                            detail.SupplyConfirmTime,
                            detail.SupplyShippingTime,
                            detail.ExpectDeliveryTime,
                            detail.InboundTime,
                            detail.PlanType,
                            detail.WarehouseName,
                            detail.SupplyShippingCycle,
                            detail.SupplyArrivalCycle,
                            detail.TheoryDeliveryTime,
                            detail.OverTime,
                            detail.UndueTime,
                            detail.PurchasePlanRemark,
                            detail.FinishDescribe,
                            detail.Weight,
                            detail.Volume,
                            detail.Salesprice,
                            detail.PersonName
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
        public string 导出采购订单短供明细情况(DateTime? bPurchasePlanTime, DateTime? ePurchasePlanTime, DateTime? bTheoryDeliveryTime, DateTime? eTheoryDeliveryTime) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("采购订单短供明细_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = 采购单短供明细(bPurchasePlanTime, ePurchasePlanTime, bTheoryDeliveryTime, eTheoryDeliveryTime);

            var dataArray = data.ToArray();
            if(data.Any()) {
                excelColumns.Add(ErrorStrings.ExportColumn_PurchasePlanCode);
                excelColumns.Add(ErrorStrings.ExportColumn_PartsPurchaseOrderCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SupplierName);
                excelColumns.Add(ErrorStrings.ExportColumn_SupplierPartCode);
                excelColumns.Add(ErrorStrings.ExportColumn_PartCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SIHCode);
                excelColumns.Add(ErrorStrings.ExportColumn_MapName);
                excelColumns.Add(ErrorStrings.ExportColumn_PartProp);
                excelColumns.Add(ErrorStrings.ExportColumn_PurchasePlanAmount);
                excelColumns.Add(ErrorStrings.ExportColumn_SupplierConfirmedAmount);
                excelColumns.Add(ErrorStrings.ExportColumn_SupplierConfirmedReason);
                excelColumns.Add(ErrorStrings.ExportColumn_SupplierShippingAmount);
                excelColumns.Add(ErrorStrings.ExportColumn_PartsPurchaseOrderCreateTime);
                excelColumns.Add(ErrorStrings.ExportColumn_SupplierConfirmedTime);
                excelColumns.Add(ErrorStrings.ExportColumn_ArriveAmount);
                excelColumns.Add(ErrorStrings.ExportColumn_OwingAmount);
                excelColumns.Add(ErrorStrings.Export_Title_PartsPurchaseOrderFinish_PersonName);
                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.PurchasePlanCode,
                            detail.PurchaseCode,
                            detail.SupplierName,
                            detail.SupplierPartCode,
                            detail.SpareCode,
                            detail.SihCode,
                            detail.MapName,
                             Enum.GetName(typeof(DcsABCStrategyCategory),detail.PartABC==null?0:detail.PartABC),
                            detail.PurchasePlanQty,
                            detail.SupplyConfirmQty,
                            detail.SupplyConfirmReason,
                            detail.SupplyShippingQty,
                            detail.PurchaseCreateTime,
                            detail.SupplyConfirmTime,
                            detail.InboundQty,
                            detail.OwningQty,
                            detail.PersonName
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
    }
}

