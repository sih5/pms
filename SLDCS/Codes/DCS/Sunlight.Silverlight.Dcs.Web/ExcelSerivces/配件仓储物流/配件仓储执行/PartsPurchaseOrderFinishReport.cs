﻿using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    public enum InBoundStatus {
        已完成 = 1,
        部分完成 = 2,
        未完成 = 3      
    }
    partial class DcsDomainService {
        public string 导出配件优先级分析报表(DateTime? createTime, string partCode, int? partabc, string supplierName, int? priority, int? oweOrderNum) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("配件优先级分析报表_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = 获取配件优先级分析信息(createTime, partCode, partabc, supplierName, priority, oweOrderNum);
            var dataArray = data.ToArray();         

            if(data.Any()) {
                excelColumns.Add(ErrorStrings.ExportColumn_SupplierPartCode);
                excelColumns.Add(ErrorStrings.ExportColumn_PartCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SIHCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SparePartName);
                excelColumns.Add(ErrorStrings.Export_Title_SupplierName);
                excelColumns.Add(ErrorStrings.ExportColumn_PartProp);
                excelColumns.Add(ErrorStrings.Export_Title_DailyAmount);
                excelColumns.Add(ErrorStrings.Export_Title_QtyLowerLimit);
                excelColumns.Add(ErrorStrings.Export_Title_UsedQty);
                excelColumns.Add(ErrorStrings.Export_Title_BottomUsedQty);
                excelColumns.Add(ErrorStrings.Export_Title_SupplierOnWay);
                excelColumns.Add(ErrorStrings.Export_Title_OverTimeSupplierOnWay);
                excelColumns.Add(ErrorStrings.Export_Title_UnInbound);
                excelColumns.Add(ErrorStrings.Export_Title_UnPack);
                excelColumns.Add(ErrorStrings.Export_Title_UnShelve);
                excelColumns.Add(ErrorStrings.Export_Title_OwingOrder);
                excelColumns.Add(ErrorStrings.Export_Title_OwingNum);
                excelColumns.Add(ErrorStrings.Export_Title_Priority);
                excelColumns.Add(ErrorStrings.Export_Title_PriorityCreateTime);
                excelColumns.Add(ErrorStrings.Export_Title_DurativeDayNum);
                excelColumns.Add(ErrorStrings.Export_Title_TotalDurativeDayNum);
				excelColumns.Add(ErrorStrings.ExportColumn_IsOrderable);
				excelColumns.Add(ErrorStrings.ExportColumn_IsSalable);
                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.SupplierPartCode,
                            detail.PartCode,
                            detail.ReferenceCode,
                            detail.PartName,
                            detail.SupplierName, 
                             Enum.GetName(typeof(DcsABCStrategyCategory),detail.PartABC==null?0:detail.PartABC),
                            detail.DailySaleNum,                           
                            detail.QtyLowerLimit,
                            detail.ActualUseableQty,
                            detail.StockQty,
                            detail.SupplyOnLineQty,
                            detail.OverdueOnLineQty,
                            detail.WaitInQty,
                            detail.WaitPackQty,
                            detail.WaitShelvesQty,
                            detail.OwePartNum,
                            detail.OweOrderNum,                           
                             Enum.GetName(typeof(DcsPartsPriority),detail.Priority==null?0:detail.Priority),
                            detail.PriorityCreateTime,
                            detail.DurationDay,
                            detail.TotalDurativeDayNum,
							 detail.IsOrderable != null?((bool)detail.IsOrderable?"是":"否"):string.Empty,
							 detail.IsSalable != null?((bool)detail.IsSalable?"是":"否"):string.Empty,
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
        public string 导出优先入库明细表(DateTime? createTime, string partCode, string supplierName, int? inboundFinishStatus, DateTime? bPriorityCreateTime, DateTime? ePriorityCreateTime, DateTime? bPlanDeliveryTime, DateTime? ePlanDeliveryTime) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("优先入库明细表_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = 优先入库明细表(createTime, partCode, supplierName, inboundFinishStatus, bPriorityCreateTime, ePriorityCreateTime, bPlanDeliveryTime, ePlanDeliveryTime);
            var dataArray = data.ToArray();
            if(data.Any()) {
                excelColumns.Add(ErrorStrings.Export_Title_Priority);
                excelColumns.Add(ErrorStrings.Export_Title_PriorityCreateTime);
                excelColumns.Add(ErrorStrings.ExportColumn_PartsInboundPlan_Code);
                excelColumns.Add(ErrorStrings.ExportColumn_PartsPurchaseOrderCode);
                excelColumns.Add(ErrorStrings.ExportColumn_PartCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SIHCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SparePartName);
                excelColumns.Add(ErrorStrings.ExportColumn_SupplierShippingAmount);
                excelColumns.Add(ErrorStrings.ExportColumn_InboundAmount);
                excelColumns.Add(ErrorStrings.ExportColumn_ShouldInboundAmount);
                excelColumns.Add(ErrorStrings.ExportColumn_Title_PartsPurchase_ShallInboundEntries);
                excelColumns.Add(ErrorStrings.ExportColumn_Title_PartsPurchase_ActualInboundEntries);
                excelColumns.Add(ErrorStrings.ExportColumn_SupplierShippingTime);
                excelColumns.Add(ErrorStrings.ExportColumn_DeliveryTime);
                excelColumns.Add(ErrorStrings.ExportColumn_InFinishTime);
                excelColumns.Add(ErrorStrings.ExportColumn_InFinishStatus);
                excelColumns.Add(ErrorStrings.Export_Title_SupplierName);
                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                             Enum.GetName(typeof(DcsPartsPriority),detail.Priority),
                            detail.PriorityCreateTime,
                            detail.InboundPlanCode,
                            detail.PartsPurchaseOrderCode,
                            detail.SparePartCode,
                            detail.ReferenceCode,
                            detail.SparePartName, 
                            detail.ShippingAmount,                           
                            detail.InspectedQuantity,
                            detail.ShallInBoundQty,
                            detail.ShallInboundEntries,
                            detail.ActualInboundEntries,
                            detail.ShippingDate,
                            detail.PlanDeliveryTime,
                            detail.InboundFinishTime,
                            Enum.GetName(typeof(InBoundStatus),detail.InboundFinishStatus==null?0:detail.InboundFinishStatus),
                            detail.SupplierName,                                                      
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
    }
}
