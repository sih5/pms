﻿using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web
{
    partial class DcsDomainService
    {
        public string 导出上架完成情况统计(DateTime? bPackModifyTime, DateTime? ePackModifyTime, string partsInboundCheckBillCode, int? warehouseId, string sparePartCode, string referenceCode, int? partABC, string partsPurchaseOrderCode, DateTime? bShelvesFinishTime, DateTime? eShelvesFinishTime)
        {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("上架完成情况统计_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = GetPartsShelvesTaskFinishs(bPackModifyTime, ePackModifyTime, partsInboundCheckBillCode, warehouseId, sparePartCode, referenceCode, partABC, partsPurchaseOrderCode, bShelvesFinishTime, eShelvesFinishTime);
            var dataArray = data.ToArray();
            if (data.Any())
            {
                excelColumns.Add(ErrorStrings.ExportColumn_PurchaseOrderCode);
                excelColumns.Add(ErrorStrings.ExportColumn_InboundCode);
                excelColumns.Add(ErrorStrings.ExportColumn_Warehouse);
                excelColumns.Add(ErrorStrings.ExportColumn_PartCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SupplierPartCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SIHCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SparePartName);
                excelColumns.Add(ErrorStrings.ExportColumn_PartProp);
                excelColumns.Add(ErrorStrings.ExportColumn_InboundAmount);
                excelColumns.Add(ErrorStrings.ExportColumn_PackingQuantity);
                excelColumns.Add(ErrorStrings.ExportColumn_ShelvesQuantity);
                excelColumns.Add(ErrorStrings.ExportColumn_PackingFinishTime);
                excelColumns.Add(ErrorStrings.ExportColumn_ShelvesCreateTime);
                excelColumns.Add(ErrorStrings.ExportColumn_ShelvesFinishTime);
                excelColumns.Add(ErrorStrings.ExportColumn_RetailPrice);

                using (var excelExport = new ExcelExport(exportDataFileName))
                {
                    excelExport.ExportByRow(index =>
                    {
                        if (index == dataArray.Count() + 1)
                            return null;
                        if (index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.PartsPurchaseOrderCode,
                            detail.PartsInboundCheckBillCode,
                            detail.WarehouseName,
                            detail.SparePartCode,
                            detail.SupplierPartCode, 
                            detail.ReferenceCode,
                            detail.SparePartName,
                             Enum.GetName(typeof(DcsABCStrategyCategory),detail.PartABC==null?0:detail.PartABC),
                            detail.InspectedQuantity,
                            detail.PackingQty,
                            detail.ShelvesAmount,
                            detail.PackModifyTime.ToString(),
                            detail.CreateTime.ToString(),
                            detail.ShelvesFinishTime.ToString(),
                            detail.RetailGuidePrice
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }

    }
}

