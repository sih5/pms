﻿using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string 导出发运单统计(string code, string partsSalesOrderCode, DateTime? bShippingDate, DateTime? eShippingDate, string appraiserName) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("导出发运单统计_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = 发运单统计查询(code, partsSalesOrderCode, bShippingDate, eShippingDate, appraiserName);
            var dataArray = data.ToArray();

            if(data.Any()) {
                excelColumns.Add(ErrorStrings.ExportColumn_ShippingOrderCode);
                excelColumns.Add(ErrorStrings.ExportColumn_ShippingDate);
                excelColumns.Add(ErrorStrings.ExportColumn_OutWarehouse);
                excelColumns.Add(ErrorStrings.ExportColumn_OrderType);
                excelColumns.Add(ErrorStrings.ExportColumn_ShippingMethod);
                excelColumns.Add(ErrorStrings.ExportColumn_ShippingAmount2);
                excelColumns.Add(ErrorStrings.ExportColumn_ReceiveCompany);
                excelColumns.Add(ErrorStrings.ExportColumn_ReceiveAdress);
                excelColumns.Add(ErrorStrings.ExportColumn_ReceivePerson);
                excelColumns.Add(ErrorStrings.ExportColumn_LogisticCompanyName);
                excelColumns.Add(ErrorStrings.ExportColumn_DeliveryBillNumber);
                excelColumns.Add(ErrorStrings.ExportColumn_Weight);
                excelColumns.Add(ErrorStrings.ExportColumn_Volume);
                excelColumns.Add(ErrorStrings.ExportColumn_OldWeight);
                excelColumns.Add(ErrorStrings.ExportColumn_OldVolume);
                excelColumns.Add(ErrorStrings.ExportColumn_ApprovePerson);
                excelColumns.Add(ErrorStrings.ExportColumn_SalesOrderCode);
                excelColumns.Add(ErrorStrings.ExportColumn_Remark);

                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.Code,
                            detail.ShippingDate,
                            detail.WarehouseName,
                            detail.PartsSalesOrderTypeName,
                            Enum.GetName(typeof(DcsPartsShippingMethod),detail.ShippingMethod==null?0:detail.ShippingMethod),
                            detail.SettlementPriceAll,
                            detail.ReceivingCompanyName,
                            detail.ReceivingAddress,
                            detail.LinkName,
                            detail.LogisticCompanyName,
                            detail.DeliveryBillNumber,
                            detail.Weight,
                            detail.Volume,
                            detail.OldWeight,
                            detail.OldVolume,
                            detail.AppraiserName,
                            detail.PartsSalesOrderCode,
                            detail.Remark
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
    }
}
