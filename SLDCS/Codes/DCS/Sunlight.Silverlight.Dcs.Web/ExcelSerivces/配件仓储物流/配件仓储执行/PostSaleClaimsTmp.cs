﻿using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web
{
    partial class DcsDomainService {
        public string exportPostSaleClaimsTmps(int[] ids,string claimBillCode,string sparePartCode,string sparePartName,string dealerCode,string dealerName,int? type,DateTime? bCreateTime,DateTime? eCreateTime)
        {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("服务站领料单_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = getPostSaleClaims(claimBillCode, sparePartCode, sparePartName, dealerCode, dealerName, type, bCreateTime, eCreateTime);
            if (ids != null && ids.Any()) {
                data = data.Where(r => ids.Contains(r.Id));
            }
            var dataArray = data.ToArray();
            if (data.Any())
            {
                excelColumns.Add(ErrorStrings.ExportColumn_Type);
                excelColumns.Add(ErrorStrings.ExportColumn_ClaimBillCode);
                excelColumns.Add(ErrorStrings.ExportColumn_DealerCode);
                excelColumns.Add(ErrorStrings.ExportColumn_DealerName);
                excelColumns.Add(ErrorStrings.ExportColumn_IsClaim);
                excelColumns.Add(ErrorStrings.ExportColumn_PartCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SparePartName);
                excelColumns.Add(ErrorStrings.ExportColumn_Quantity);
                excelColumns.Add(ErrorStrings.ExportColumn_Status);
                excelColumns.Add(ErrorStrings.ExportColumn_CreateTime);
                excelColumns.Add(ErrorStrings.ExportColumn_TransferTime);
				excelColumns.Add("经销价");
				excelColumns.Add("建议售价");
				excelColumns.Add("价格属性");
                
                using (var excelExport = new ExcelExport(exportDataFileName))
                {
                    excelExport.ExportByRow(index =>
                    {
                        if (index == dataArray.Count() + 1)
                            return null;
                        if (index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            Enum.GetName(typeof(DcsPostSaleClaimsTmpType),detail.Type==null?0:detail.Type),
                            detail.ClaimBillCode,
                            detail.DealerCode,
                            detail.DealerName,
                            detail.IsClaim == true?EntityStrings.Export_Title_Yes:EntityStrings.Export_Title_No, 
                            detail.SparePartCode,
                            detail.SparePartName,
                            detail.Amount,
                            Enum.GetName(typeof(DcsPostSaleClaimsTmpStatus),detail.Status==null?0:detail.Status),
                            detail.CreateTime.ToString(),
                            detail.TransferTime.ToString(),
							detail.SalesPrice,
							detail.RetailGuidePrice,
							detail.GroupCode,
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }

    }
}

