﻿using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string 导出优先包装上架明细表(DateTime? createTime, int? priority, int? packingFinishStatus, int? shelvesFinishStatus, DateTime? bPriorityPackingTime, DateTime? ePriorityPackingTime, DateTime? bPriorityShelvesTime, DateTime? ePriorityShelvesTime) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("优先包装上架明细表_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = 获取优先包装上架明细表(createTime, priority, packingFinishStatus, shelvesFinishStatus, bPriorityPackingTime, ePriorityPackingTime, bPriorityShelvesTime, ePriorityShelvesTime);
            var dataArray = data.ToArray();

            if(data.Any()) {
                excelColumns.Add(ErrorStrings.Export_Title_Priority);
				excelColumns.Add(ErrorStrings.ExportColumn_PartsPurchaseOrderCode);
				excelColumns.Add(ErrorStrings.ExportColumn_InboundCode);
                excelColumns.Add(ErrorStrings.ExportColumn_PartCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SIHCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SparePartName);
                excelColumns.Add(ErrorStrings.ExportColumn_InboundAmount);
                excelColumns.Add(ErrorStrings.ExportColumn_PackingQuantity);
                excelColumns.Add(ErrorStrings.ExportColumn_PackingItem);
                excelColumns.Add(ErrorStrings.ExportColumn_ActPackingItem);
                excelColumns.Add(ErrorStrings.ExportColumn_ShelvesQuantity);
                excelColumns.Add(ErrorStrings.ExportColumn_ShelvesItem);
                excelColumns.Add(ErrorStrings.ExportColumn_ActShelvesItem);
                excelColumns.Add(ErrorStrings.ExportColumn_InFinishTime);
                excelColumns.Add(ErrorStrings.ExportColumn_PackingFinishTime);
                excelColumns.Add(ErrorStrings.ExportColumn_PackingStatus);
                excelColumns.Add(ErrorStrings.ExportColumn_ShelvesFinishTime);
                excelColumns.Add(ErrorStrings.ExportColumn_ShelvesFinishStatus);                
                excelColumns.Add(ErrorStrings.Export_Title_PriorityCreateTime);
                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
							 Enum.GetName(typeof(DcsPartsPriority),detail.Priority),
                            detail.PartsPurchaseOrderCode,
                            detail.InboundCode,
							detail.SparePartCode,
                            detail.ReferenceCode,
                            detail.SparePartName,
                            detail.InspectedQuantity, 
                            detail.PackingQty,                           
                            detail.ShallPackingEntries,
                            detail.ActualPackingEntries,
                            detail.ShelvesAmount,
                            detail.ShallShelvesEntries,
                            detail.ActualShelvesEntries,
                            detail.InBoundFinishTime,
                            detail.PackingFinishTime,
                            Enum.GetName(typeof(InBoundStatus),detail.PackingFinishStatus==null?0:detail.PackingFinishStatus),                           
                            detail.ShelvesFinishTime,  
                            Enum.GetName(typeof(InBoundStatus),detail.ShelvesFinishStatus==null?0:detail.ShelvesFinishStatus),   							
                            detail.PriorityCreateTime,                           
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
		 public string 导出优先包装上架明细汇总表(DateTime? bCreateTime, DateTime? eCreateTime, int? priority, int? type) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("优先包装上架明细汇总表_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = 获取优先包装上架明细汇总表(bCreateTime, eCreateTime, priority,type);
            var dataArray = data.ToArray();

            if(data.Any()) {
				excelColumns.Add(ErrorStrings.Export_Title_Month);
				excelColumns.Add(ErrorStrings.InOutSettleSummaryReport1);
                excelColumns.Add(ErrorStrings.Export_Title_Priority);
				excelColumns.Add(ErrorStrings.Export_StorageNumber);
				excelColumns.Add(ErrorStrings.Export_ActStorageNumber);
                excelColumns.Add(ErrorStrings.ExportColumn_Title_PartsPurchase_ShallInboundEntries);
                excelColumns.Add(ErrorStrings.ExportColumn_Title_PartsPurchase_ActualInboundEntries);
                excelColumns.Add(ErrorStrings.Export_CompletionRate);
                excelColumns.Add(ErrorStrings.Export_CompletionEntryRate);
                excelColumns.Add(ErrorStrings.Export_PackingNumber);
                excelColumns.Add(ErrorStrings.Export_PackedNumber);
                excelColumns.Add(ErrorStrings.ExportColumn_PackingItem);
                excelColumns.Add(ErrorStrings.ExportColumn_ActPackingItem);
                excelColumns.Add(ErrorStrings.Export_PackRate);
                excelColumns.Add(ErrorStrings.Export_PackItemRate);
                excelColumns.Add(ErrorStrings.Export_ShelveNumber);
                excelColumns.Add(ErrorStrings.Export_ShelvedNumber);
                excelColumns.Add(ErrorStrings.ExportColumn_ShelvesItem);
                excelColumns.Add(ErrorStrings.ExportColumn_ActShelvesItem);
                excelColumns.Add(ErrorStrings.Export_ShelveRate);
                excelColumns.Add(ErrorStrings.Export_ShelveItemRate);                
                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
							detail.Month,
							detail.Dates,
							 Enum.GetName(typeof(DcsPartsPriority),detail.Priority),
                            detail.StorageNumber,
                            detail.ActStorageNumber,
							detail.StorageNumberItem,
                            detail.ActStorageNumberItem,
                            detail.CompletionRate,
                            detail.CompletionEntryRate, 
                            detail.PackingNumber,                           
                            detail.PackedNumber,
                            detail.PackingNumberItem,
                            detail.PackedNumberItem,
                            detail.PackRate,
                            detail.PackItemRate,
                            detail.ShelveNumber,
                            detail.ShelvedNumber,
							detail.ShelveNumberItem,
                            detail.ShelvedNumberItem,                           
                            detail.ShelveRate,  
                            detail.ShelveItemRate                          
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
    }
}
