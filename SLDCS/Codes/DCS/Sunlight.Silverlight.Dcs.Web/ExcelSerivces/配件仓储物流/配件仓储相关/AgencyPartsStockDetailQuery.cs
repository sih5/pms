﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string ExportAgencyPartsStockDetailQuery(int[] ids,string companyCode, string companyName,string sparePartCode, string sparePartName, bool? greaterThanZero,bool? isAgencyQuery) {
           var excelColumns = new List<string>();
           var fileName = GetExportFilePath(string.Format("中心库配件库存_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
           var data = 查询中心库配件库存(companyCode, companyName, sparePartCode, sparePartName, greaterThanZero,isAgencyQuery);
          
            if(ids != null && ids.Any()) {
                data = data.Where(r => ids.Contains(r.Id));
            }
            var agencyPartsStock = data;
            var dataArray = data.ToArray();
            if(agencyPartsStock.Any()) {
                excelColumns.Add(ErrorStrings.ExportColumn_CompanyCode);
                excelColumns.Add(ErrorStrings.ExportColumn_CompanyName);
                excelColumns.Add(ErrorStrings.ExportColumn_WarehouseCode);
                excelColumns.Add(ErrorStrings.ExportColumn_WarehouseName);
                excelColumns.Add(ErrorStrings.ExportColumn_PartCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SparePartName);
                excelColumns.Add(ErrorStrings.ExportColumn_SparePartABC);
                excelColumns.Add(ErrorStrings.ExportColumn_CenterPrice);
                excelColumns.Add(ErrorStrings.ExportColumn_AllStock);
                excelColumns.Add(ErrorStrings.ExportColumn_AllStockAmount);
                excelColumns.Add(ErrorStrings.ExportColumn_ActUsableStock);
                excelColumns.Add(ErrorStrings.ExportColumn_ActUsableStockAmount);

                using(var excelExport = new ExcelExport(fileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.CompanyCode,
                            detail.CompanyName,
                            detail.WarehouseCode,
                            detail.WarehouseName,
                            detail.SparePartCode,
                            detail.SparePartName,
                            detail.PartABC == null?null:Enum.GetName(typeof(DCSABCSettingType),detail.PartABC),
                            detail.CenterPrice,
                            detail.Quantity,
                            detail.QuantityAll,
                            detail.ActualAvailableStock,
                            detail.ActualAll
                        };
                        return values;
                    });
                }
            }
            return fileName;
        }
        public string ExportAgencyPartsStockForDealerQuery(int[] ids, string companyCode, string companyName, string sparePartCode, string sparePartName, bool? greaterThanZero) {
            var excelColumns = new List<string>();
            var fileName = GetExportFilePath(string.Format("中心库配件库存_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = 大服务站库存查询(companyCode, companyName, sparePartCode, sparePartName, greaterThanZero);

            if(ids != null && ids.Any()) {
                data = data.Where(r => ids.Contains(r.Id));
            }
            var agencyPartsStock = data;
            var dataArray = data.ToArray();
            if(agencyPartsStock.Any()) {
                excelColumns.Add(ErrorStrings.ExportColumn_CompanyCode);
                excelColumns.Add(ErrorStrings.ExportColumn_CompanyName);
                excelColumns.Add(ErrorStrings.ExportColumn_WarehouseCode);
                excelColumns.Add(ErrorStrings.ExportColumn_WarehouseName);
                excelColumns.Add(ErrorStrings.ExportColumn_PartCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SparePartName);
                excelColumns.Add(ErrorStrings.ExportColumn_SparePartABC);
                excelColumns.Add(ErrorStrings.ExportColumn_CenterPrice);
                excelColumns.Add(ErrorStrings.ExportColumn_AllStock);
                excelColumns.Add(ErrorStrings.ExportColumn_AllStockAmount);
                excelColumns.Add(ErrorStrings.ExportColumn_ActUsableStock);
                excelColumns.Add(ErrorStrings.ExportColumn_ActUsableStockAmount);

                using(var excelExport = new ExcelExport(fileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.CompanyCode,
                            detail.CompanyName,
                            detail.WarehouseCode,
                            detail.WarehouseName,
                            detail.SparePartCode,
                            detail.SparePartName,
                            detail.PartABC == null?null:Enum.GetName(typeof(DCSABCSettingType),detail.PartABC),
                            detail.CenterPrice,
                            detail.Quantity,
                            detail.QuantityAll,
                            detail.ActualAvailableStock,
                            detail.ActualAll
                        };
                        return values;
                    });
                }
            }
            return fileName;
        }
    }
}
