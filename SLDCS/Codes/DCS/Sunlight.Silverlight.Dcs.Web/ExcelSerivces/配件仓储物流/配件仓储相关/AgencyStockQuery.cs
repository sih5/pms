﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService{
        /// <summary>
        /// 代理库库存查询导出
        /// </summary>
        public string ExportAgencyStockQuery(int? partsSalesCategoryId, string agencyCode, string agencyName, int? branchId, string partCode, string partName) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("代理库库存查询_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = 代理库库存查询();
            if(partsSalesCategoryId.HasValue) {
                data = data.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId.Value);
            }
            if(!string.IsNullOrEmpty(agencyCode)) {
                data = data.Where(r => r.AgencyCode.Contains(agencyCode));
            }
            if(!string.IsNullOrEmpty(agencyName)) {
                data = data.Where(r => r.AgencyName.Contains(agencyName));
            }
            if(branchId.HasValue) {
                data = data.Where(r => r.BranchId == branchId.Value);
            }
            if(!string.IsNullOrEmpty(partCode)) {
                data = data.Where(r => r.PartCode.Contains(partCode));
            }
            if(!string.IsNullOrEmpty(partName)) {
                data = data.Where(r => r.PartName.Contains(partName));
            }
            var dataArray = data.ToArray();
            if(dataArray.Any()) {
                excelColumns.Add(ErrorStrings.ExportColumn_PartsSalesCategory);
                excelColumns.Add("代理库编号");
                excelColumns.Add("代理库名称");
                excelColumns.Add(ErrorStrings.ExportColumn_WarehouseName);
                excelColumns.Add("配件图号");
                excelColumns.Add(ErrorStrings.ExportColumn_SparePartName);
                excelColumns.Add("库存");
                excelColumns.Add("可用库存");
                excelColumns.Add("批发价");
                excelColumns.Add("批发金额");
                excelColumns.Add("配件所属分类");

                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.PartsSalesCategoryName,
                            detail.AgencyCode,
                            detail.AgencyName,
                            detail.WarehouseName,
                            detail.PartCode,
                            detail.PartName,
                            detail.Qty,
                            detail.UsableQty,
                            detail.Saleprice,
                            detail.Totalprice,
                            detail.PartsAttribution
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }

        /// <summary>
        /// 服务站向代理库提报订单查询导出
        /// </summary>
        public string ExportServiceAgentsHistoryQuery(int? branchId, int? salesCategoryId, int? salesUnitOwnerCompanyId, int? partsSalesOrderTypeId, string submitCompanyCode, string submitCompanyName, string code, DateTime? startCreateTime, DateTime? endCreateTime, DateTime? startSubmitTime, DateTime? endSubmitTime) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("服务站向代理库提报订单查询_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = 服务站向代理库提报订单查询();

            if(branchId.HasValue) {
                data = data.Where(r => r.BranchId == branchId.Value);
            }
            if(salesCategoryId.HasValue) {
                data = data.Where(r => r.SalesCategoryId == salesCategoryId.Value);
            }
            if(salesUnitOwnerCompanyId.HasValue) {
                data = data.Where(r => r.SalesUnitOwnerCompanyId == salesUnitOwnerCompanyId.Value);
            }
            if(partsSalesOrderTypeId.HasValue) {
                data = data.Where(r => r.PartsSalesOrderTypeId == partsSalesOrderTypeId.Value);
            }
            if(!string.IsNullOrEmpty(submitCompanyCode)) {
                data = data.Where(r => r.SubmitCompanyCode.Contains(submitCompanyCode));
            }
            if(!string.IsNullOrEmpty(submitCompanyName)) {
                data = data.Where(r => r.SubmitCompanyName.Contains(submitCompanyName));
            }
            if(!string.IsNullOrEmpty(code)) {
                data = data.Where(r => r.Code.Contains(code));
            }
            if(startCreateTime.HasValue) {
                data = data.Where(r => r.CreateTime >= startCreateTime.Value);
            }
            if(endCreateTime.HasValue) {
                data = data.Where(r => r.CreateTime <= endCreateTime.Value);
            }
            if(startSubmitTime.HasValue) {
                data = data.Where(r => r.SubmitTime >= startSubmitTime.Value);
            }
            if(endSubmitTime.HasValue) {
                data = data.Where(r => r.SubmitTime <= endSubmitTime.Value);
            }

            var dataArray = data.ToArray();
            if(data.Any()) {
                excelColumns.Add("代理库编号");
                excelColumns.Add("代理库名称");
                excelColumns.Add("服务站编号");
                excelColumns.Add("服务站名称");
                excelColumns.Add(ErrorStrings.ExportColumn_PartsSalesCategory);
                excelColumns.Add("订单编号");
                excelColumns.Add("配件销售订单类型");
                excelColumns.Add("配件图号");
                excelColumns.Add(ErrorStrings.ExportColumn_SparePartName);
                excelColumns.Add(ErrorStrings.ExportColumn_OrderQuantity);
                excelColumns.Add("满足数量");
                excelColumns.Add("单价");
                excelColumns.Add("订货金额");
                excelColumns.Add("订货仓库");
                excelColumns.Add("是否直供");
                excelColumns.Add(ErrorStrings.ExportColumn_Status);
                excelColumns.Add("是否欠款");
                excelColumns.Add("订单总金额");
                excelColumns.Add(ErrorStrings.ExportColumn_Remark);
                excelColumns.Add("发运方式");
                excelColumns.Add("收货地址");
                excelColumns.Add("要求到货时间");
                excelColumns.Add("终止原因");
                excelColumns.Add(ErrorStrings.ExportColumn_Creator);
                excelColumns.Add(ErrorStrings.ExportColumn_CreateTime);
                excelColumns.Add(ErrorStrings.ExportColumn_Modifier);
                excelColumns.Add(ErrorStrings.ExportColumn_ModifyTime);
                excelColumns.Add("提交人");
                excelColumns.Add("提交时间");
                excelColumns.Add(ErrorStrings.ExportColumn_Approver);
                excelColumns.Add(ErrorStrings.ExportColumn_ApproveTime);
                excelColumns.Add("初次审批时间");
                excelColumns.Add(ErrorStrings.ExportColumn_Abandoner);
                excelColumns.Add(ErrorStrings.ExportColumn_AbandonTime);

                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.SalesUnitOwnerCompanyCode,
                            detail.SalesUnitOwnerCompanyName,
                            detail.SubmitCompanyCode,
                            detail.SubmitCompanyName,
                            detail.SalesCategoryName,
                            detail.Code, 
                            detail.PartsSalesOrderTypeName,
                            detail.SparePartCode,
                            detail.SparePartName, 
                            detail.OrderedQuantity,
                            detail.ApproveQuantity,
                            detail.OrderPrice, 
                            detail.OrderSum,
                            detail.WarehouseName,
                            detail.IfDirectProvision?EntityStrings.Export_Title_Yes:EntityStrings.Export_Title_No,
                            Enum.GetName(typeof(DcsPartsSalesOrderStatus),detail.Status),
                            detail.IsDebt?EntityStrings.Export_Title_Yes:EntityStrings.Export_Title_No,
                            detail.TotalAmount,
                            detail.Remark,
                            Enum.GetName(typeof(DcsPartsShippingMethod),detail.ShippingMethod),
                            detail.ReceivingAddress,
                            detail.RequestedDeliveryTime,
                            detail.StopComment,
                            detail.CreatorName,
                            detail.CreateTime,
                            detail.ModifierName,
                            detail.ModifyTime,
                            detail.SubmitterName,
                            detail.SubmitTime,
                            detail.ApproverName,
                            detail.ApproveTime,
                            detail.FirstApproveTime,
                            detail.AbandonerName,
                            detail.AbandonTime
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }

        /// <summary>
        /// 配件销售在途查询导出
        /// </summary>
        public string ExportPartsSalesQuery(int? branchId, int? warehouseId, string receivingCompanyCode, string receivingCompanyName) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("配件销售在途查询_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = 配件销售在途查询();

            if(branchId.HasValue) {
                data = data.Where(r => r.BranchId == branchId.Value);
            }
            if(warehouseId.HasValue) {
                data = data.Where(r => r.WarehouseId == warehouseId.Value);
            }
            if(!string.IsNullOrEmpty(receivingCompanyCode)) {
                data = data.Where(r => r.ReceivingCompanyCode.Contains(receivingCompanyCode));
            }
            if(!string.IsNullOrEmpty(receivingCompanyName)) {
                data = data.Where(r => r.ReceivingCompanyName.Contains(receivingCompanyName));
            }
            var dataArray = data.ToArray();
            if(data.Any()) {
                excelColumns.Add(ErrorStrings.ExportColumn_WarehouseName);
                excelColumns.Add(ErrorStrings.ExportColumn_ReceivingCompanyCode);
                excelColumns.Add(ErrorStrings.ExportColumn_ReceivingCompanyName);
                excelColumns.Add(ErrorStrings.ExportColumn_SparePartCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SparePartName);
                excelColumns.Add(ErrorStrings.ExportColumn_TradePrice);
                excelColumns.Add(ErrorStrings.ExportColumn_Quantity);
                excelColumns.Add(ErrorStrings.ExportColumn_TradeAmount);

                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.WarehouseName,
                            detail.ReceivingCompanyCode,
                            detail.ReceivingCompanyName,
                            detail.SparePartCode,
                            detail.SparePartName,
                            detail.SettlementPrice,
                            detail.Qty,
                            detail.TotalPrice
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }


        /// <summary>
        /// 配件价格查询
        /// </summary>
        public string 导出配件价格查询(int? branchId, int? partsSalesCategoryId, string partCode, string partName, int? isZeroSalesPrice, int? isZeroRetailPrice, int? isZeroPlannedPrice, int? isZeroPurchasePrice, string supplierCode, string supplierName, DateTime? startCreateTime, DateTime? endCreateTime, DateTime? startSubmitTime, DateTime? endSubmitTime) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("配件价格查询_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = 配件价格查询();


            //if(!string.IsNullOrEmpty(branchCode)) {
            //    data = data.Where(r => r.BranchCode.Contains(branchCode));
            //}
            if(partsSalesCategoryId.HasValue) {
                data = data.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId.Value);
            }

            if(!string.IsNullOrEmpty(partCode)) {
                data = data.Where(r => r.PartCode.Contains(partCode));
            }
            if(!string.IsNullOrEmpty(partName)) {
                data = data.Where(r => r.PartName.Contains(partName));
            }
            if(!string.IsNullOrEmpty(supplierCode)) {
                data = data.Where(r => r.SupplierCode.Contains(supplierCode));
            }
            if(!string.IsNullOrEmpty(supplierName)) {
                data = data.Where(r => r.SupplierName.Contains(supplierName));
            }
            if(branchId.HasValue) {
                data = data.Where(r => r.BranchId == branchId.Value);
            }

            if(isZeroSalesPrice.HasValue) {
                data = data.Where(r => r.IsZeroSalesPrice == isZeroSalesPrice.Value);
            }
            if(isZeroRetailPrice.HasValue) {
                data = data.Where(r => r.IsZeroRetailPrice == isZeroRetailPrice.Value);
            }
            if(isZeroPlannedPrice.HasValue) {
                data = data.Where(r => r.IsZeroPlannedPrice == isZeroPlannedPrice.Value);
            }
            if(isZeroPurchasePrice.HasValue) {
                data = data.Where(r => r.IsZeroPurchasePrice == isZeroPurchasePrice.Value);
            }

            if(startCreateTime.HasValue) {
                data = data.Where(r => r.Pcreatetime >= startCreateTime.Value);
            }
            if(endCreateTime.HasValue) {
                data = data.Where(r => r.Pcreatetime <= endCreateTime.Value);
            }
            if(startSubmitTime.HasValue) {
                data = data.Where(r => r.Screatetime >= startSubmitTime.Value);
            }
            if(endSubmitTime.HasValue) {
                data = data.Where(r => r.Screatetime <= endSubmitTime.Value);
            }

            var dataArray = data.ToArray();
            if(data.Any()) {
                excelColumns.Add("营销公司");
                excelColumns.Add("配件图号");
                excelColumns.Add(ErrorStrings.ExportColumn_SparePartName);
                excelColumns.Add(ErrorStrings.ExportColumn_PartsSalesCategory);
                excelColumns.Add("计量单位");
                excelColumns.Add("互换号");
                excelColumns.Add("是否可采购");
                excelColumns.Add("是否可销售");
                excelColumns.Add("采购价");
                excelColumns.Add("批发价");
                excelColumns.Add("零售指导价");
                excelColumns.Add("服务站批发价");
                excelColumns.Add("配件创建时间");
                excelColumns.Add(ErrorStrings.ExportColumn_SupplierCode2);
                excelColumns.Add(ErrorStrings.ExportColumn_SupplierName);
                excelColumns.Add("计划价");
                excelColumns.Add("配件所属分类");
                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.BranchCode,
                            detail.PartCode,
                            detail.PartName,
                            detail.PartsSalesCategoryName,
                            detail.Measureunit,
                            detail.ExchangeCode, 
                            detail.IsOrderable == null ? "" : detail.IsOrderable == true ? EntityStrings.Export_Title_Yes : EntityStrings.Export_Title_No,
                            detail.IsSalable == null ? "" : detail.IsSalable == true ? EntityStrings.Export_Title_Yes : EntityStrings.Export_Title_No,
                            detail.PurchasePrice, 
                            detail.SalesPrice,
                            detail.RetailGuidePrice,
                            detail.Dealersalesprice, 
                            detail.Createtime,
                            detail.SupplierCode,
							detail.SupplierName,
							detail.PlannedPrice ,                 
                            detail.PartsAttribution == null ? "" : Enum.GetName(typeof(DcsPartsAttribution),detail.PartsAttribution),
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }

        public string 导出代理库出库记录查询(int? branchId, int? brandId, int? warehouseId, int? outboundType, string storageCompanyCode, string storageCompanyName, string partCode, string partName, string businessode, DateTime? startCreateTime, DateTime? endCreateTime) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("代理库出库记录查询_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = 代理库出库记录查询();

            if(branchId.HasValue) {
                data = data.Where(r => r.BranchId == branchId.Value);
            }
            if(brandId.HasValue) {
                data = data.Where(r => r.BrandId == brandId.Value);
            }
            if(outboundType.HasValue) {
                data = data.Where(r => r.OutboundType == outboundType.Value);
            }
            if (warehouseId.HasValue){
                data = data.Where(r => r.WarehouseId == warehouseId.Value);
            }
            if(!string.IsNullOrEmpty(storageCompanyCode)) {
                data = data.Where(r => r.StorageCompanyCode.Contains(storageCompanyCode));
            }
            if(!string.IsNullOrEmpty(storageCompanyName)) {
                data = data.Where(r => r.StorageCompanyName.Contains(storageCompanyName));
            }
            if(!string.IsNullOrEmpty(partCode)) {
                data = data.Where(r => r.PartCode.Contains(partCode));
            }
            if(!string.IsNullOrEmpty(partName)) {
                data = data.Where(r => r.PartName.Contains(partName));
            }
            if(!string.IsNullOrEmpty(businessode)) {
                data = data.Where(r => r.Businessode.Contains(businessode));
            }
           

            var dataArray = data.ToArray();
            if(data.Any()) {
                excelColumns.Add("出库类型");
                excelColumns.Add("仓储企业编号");
                excelColumns.Add("仓储企业名称");
                excelColumns.Add("业务编码");
                excelColumns.Add(ErrorStrings.ExportColumn_Province);
                excelColumns.Add(ErrorStrings.ExportColumn_WarehouseCode);
                excelColumns.Add(ErrorStrings.ExportColumn_WarehouseName);
                excelColumns.Add(ErrorStrings.ExportColumn_PartsSalesCategory);
                excelColumns.Add(ErrorStrings.ExportColumn_OutCode);
                excelColumns.Add(ErrorStrings.ExportColumn_OutboundTime);
                excelColumns.Add("原始需求单据编号");
                excelColumns.Add("原始需求单据类型");
                excelColumns.Add("收货单位编号");
                excelColumns.Add("收货单位名称");
                excelColumns.Add("收货仓库编号");
                excelColumns.Add("收货仓库名称");
                excelColumns.Add("库位");
                excelColumns.Add(ErrorStrings.ExportColumn_PartCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SparePartName);
                excelColumns.Add("出库金额");
                excelColumns.Add(ErrorStrings.ExportColumn_SettlementStatus);
                excelColumns.Add(ErrorStrings.ExportColumn_Branch);
     

                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            Enum.GetName(typeof(DcsPartsOutboundType),detail.OutboundType),
                            detail.StorageCompanyCode,
                            detail.StorageCompanyName,
                            detail.Businessode,
                            detail.ProvinceName,
                            detail.WarehouseCode, 
                            detail.WarehouseName,
                            detail.BrandName,
                            detail.OutboundCode, 
                            detail.OutboundTime,
                            detail.OriginalRequirementBillCode,                         
                            Enum.GetName(typeof(DcsOriginalRequirementBillType),detail.OriginalRequirementBillType),                 
                            detail.ReceivingCompanyCode,
                            detail.ReceivingCompanyName,                         
                            detail.ReceivingWarehouseCode,
                            detail.ReceivingWarehouseName,
                            detail.WarehouseAreaCode,
                            detail.PartCode,
                            detail.PartName,
                            detail.Price,
                            Enum.GetName(typeof(DcsPartsSettlementStatus),detail.SettlementStatus),
                            detail.BranchName,
                      
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
        //branchId, partsInboundOrderCode, inboundType, partsSalesCategoryId, originalRequirementBillCode, warehouseName, settlementstatus, partCode, partName, companyName, createTimeBegin, createTimeEnd
        public string 导出历史入库记录查询(int? branchId, string partsInboundOrderCode, int? inboundType, int? partsSalesCategoryId, string originalRequirementBillCode,string warehouseName, int? settlementstatus, string partCode, string partName, string companyName, DateTime? createTimeBegin, DateTime? createTimeEnd) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("导出历史入库记录查询_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = 历史入库记录查询();

            if(branchId.HasValue) {
                data = data.Where(r => r.BranchId == branchId.Value);
            }
            if(inboundType.HasValue) {
                data = data.Where(r => r.InboundType == inboundType.Value);
            }
            if(partsSalesCategoryId.HasValue) {
                data = data.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId.Value);
            }
           
            if(settlementstatus.HasValue) {
                data = data.Where(r => r.settlementstatus == settlementstatus.Value);
            }
            if(!string.IsNullOrEmpty(partsInboundOrderCode)) {
                data = data.Where(r => r.PartsInboundOrderCode.Contains(partsInboundOrderCode));
            }
            if(!string.IsNullOrEmpty(originalRequirementBillCode)) {
                data = data.Where(r => r.OriginalRequirementBillCode.Contains(originalRequirementBillCode));
            }
         
            if(!string.IsNullOrEmpty(partCode)) {
                data = data.Where(r => r.PartCode.Contains(partCode));
            }
            if(!string.IsNullOrEmpty(partName)) {
                data = data.Where(r => r.PartName.Contains(partName));
            }
            if(!string.IsNullOrEmpty(companyName)) {
                data = data.Where(r => r.CompanyName.Contains(companyName));
            }
            if(createTimeBegin.HasValue) {
                data = data.Where(r => r.Createtime >= createTimeBegin.Value);
            }
            if(createTimeEnd.HasValue) {
                data = data.Where(r => r.Createtime <= createTimeEnd.Value);
            }


            var dataArray = data.ToArray();
            if(data.Any()) {
                excelColumns.Add("仓库代码");
                excelColumns.Add(ErrorStrings.ExportColumn_WarehouseName);
                excelColumns.Add(ErrorStrings.ExportColumn_PartsSalesCategory);
                excelColumns.Add("入库单编号");
                excelColumns.Add("入库单编号类型");
                excelColumns.Add("源单据编号");
                excelColumns.Add("采购订单类型");
                excelColumns.Add("发货单位代码");
                excelColumns.Add("发货单位名称");
                excelColumns.Add(ErrorStrings.ExportColumn_InboundTime);
                excelColumns.Add(ErrorStrings.ExportColumn_SettlementStatus);
                excelColumns.Add(ErrorStrings.ExportColumn_PartCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SparePartName);
                excelColumns.Add(ErrorStrings.ExportColumn_InboundAmount);
                excelColumns.Add("采购价");
                excelColumns.Add("采购金额");
                excelColumns.Add("计划价");
                excelColumns.Add("计划金额");
                excelColumns.Add(ErrorStrings.ExportColumn_SettleBillCode);
         
                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {                       
                            detail.WarehouseCode,
                            detail.WarehouseName,
                            detail.PartsSalesCategoryName,
                            detail.PartsInboundOrderCode,
                            Enum.GetName(typeof(DcsPartsInboundType),detail.InboundType),  
                            detail.OriginalRequirementBillCode, 
                            Enum.GetName(typeof(DcsPartsPurchaseOrderOrderType),detail.PartsPurchaseOrderTypeId), 
                            detail.CompanyCode,
                            detail.CompanyName, 
                            detail.Createtime,
                            Enum.GetName(typeof(DcsPartsSettlementStatus),detail.settlementstatus),           
                            detail.PartCode,
                            detail.PartName,
                            detail.InQty,
                            detail.CostPrice,
                            detail.TotalJH,
                            detail.Price,
                            detail.Total,
                            detail.JsdNum,                            
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
    }
}
