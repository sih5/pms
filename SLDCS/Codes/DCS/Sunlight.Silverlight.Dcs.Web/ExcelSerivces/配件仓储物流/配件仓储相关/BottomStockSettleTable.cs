﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string 导出保底库存需提报计划明细表(int? type, string distributionCenterName, string centerName, string dealerName,bool? overZero) {
            var excelColumns = new List<string>();
            var fileName = GetExportFilePath(string.Format("保底库存需提报计划明细表_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = 保底库存需提报计划明细表(type, distributionCenterName, centerName, dealerName,overZero);
            var agencyPartsStock = data;
            var dataArray = data.ToArray();
            if(agencyPartsStock.Any()) {
                excelColumns.Add(ErrorStrings.ExportColumn_MarketingDepartment);
                excelColumns.Add(ErrorStrings.ExportColumn_AgencyName);
                excelColumns.Add(EntityStrings.RepairWorkOrderMd_DealerCode);
                excelColumns.Add(ErrorStrings.Export_Title_DealerNames);
                excelColumns.Add(ErrorStrings.ExportColumn_PartCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SparePartName);
                excelColumns.Add(ErrorStrings.ExportColumn_ActUsableStock);
                excelColumns.Add(ErrorStrings.ExportColumn_OnwayAmount);
                excelColumns.Add(ErrorStrings.ExportColumn_BottomQty);
                excelColumns.Add("应补提数");
                excelColumns.Add("应补提金额");

                using(var excelExport = new ExcelExport(fileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.DistributionCenterName,
                            detail.CenterName,
                            detail.DealerCode,
                            detail.DealerName,
                            detail.SparePartCode,
                            detail.SparePartName,
                            detail.ActualAvailableStock,
                            detail.OnwayQty,
                            detail.StockQty,
                            detail.DifferenceQty,
                            detail.DifferenceMoney
                        };
                        return values;
                    });
                }
            }
            return fileName;
        }
        public string 导出储备类别保底覆盖率报表(int? type, string distributionCenterName, string centerName, string dealerName, int? reserveTypeId, string reserveTypeSubItem) {
            var excelColumns = new List<string>();
            var fileName = GetExportFilePath(string.Format("保底覆盖率报表_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = 储备类别保底覆盖率报表(type, distributionCenterName, centerName, dealerName, reserveTypeId, reserveTypeSubItem);
            var agencyPartsStock = data;
            var dataArray = data.ToArray();
            if(agencyPartsStock.Any()) {
              
                excelColumns.Add(ErrorStrings.ExportColumn_MarketingDepartment);
                excelColumns.Add(ErrorStrings.ExportColumn_AgencyName);
                excelColumns.Add(ErrorStrings.Export_Title_DealerNames);
                excelColumns.Add(ErrorStrings.Export_AccountSpeciesNum);
                excelColumns.Add(ErrorStrings.Export_AccountFee);
                excelColumns.Add(ErrorStrings.Export_DifferSpeciesNum);
                excelColumns.Add(ErrorStrings.Export_BottomCoverage);
                excelColumns.Add(ErrorStrings.ExportColumn_DifferenceMoney);
                excelColumns.Add("储备类别");
				excelColumns.Add("储备类别子项目");
                using(var excelExport = new ExcelExport(fileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {                         
                            detail.DistributionCenterName,
                            detail.CenterName,
                            detail.DealerName,
                            detail.AccountSpeciesNum,
                            detail.AccountFee,
                            detail.DifferSpeciesNum,
                            detail.BottomCoverage,
							detail.DifferenceMoney,
							detail.ReserveType,
							detail.ReserveTypeSubItem
                        };
                        return values;
                    });
                }
            }
            return fileName;
        }
        public string 导出保底覆盖率报表(int? type, string distributionCenterName, string centerName, string dealerName, DateTime? bCreateTime, DateTime? eCreateTime) {
            var excelColumns = new List<string>();
            var fileName = GetExportFilePath(string.Format("保底覆盖率报表_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = 查询保底覆盖率报表(type, distributionCenterName, centerName, dealerName, bCreateTime, eCreateTime);
            var agencyPartsStock = data;
            var dataArray = data.ToArray();
            if(agencyPartsStock.Any()) {
                excelColumns.Add(ErrorStrings.Export_Year);
                excelColumns.Add(ErrorStrings.Export_Month);
                excelColumns.Add(ErrorStrings.Export_Week);
                excelColumns.Add(ErrorStrings.ExportColumn_MarketingDepartment);
                excelColumns.Add(ErrorStrings.ExportColumn_AgencyName);
                excelColumns.Add(ErrorStrings.Export_Title_DealerNames);
                excelColumns.Add(ErrorStrings.Export_AccountSpeciesNum);
                excelColumns.Add(ErrorStrings.Export_AccountFee);
                excelColumns.Add("保底已储备品种");
                excelColumns.Add("保底已储备金额");
                excelColumns.Add("差异品种数");
                excelColumns.Add(ErrorStrings.ExportColumn_DifferenceMoney);
                excelColumns.Add(ErrorStrings.Export_BottomCoverage);
                

                using(var excelExport = new ExcelExport(fileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.Year,
                            detail.Month,
                            detail.Week,
                            detail.DistributionCenterName,
                            detail.CenterName,
                            detail.DealerName,
                            detail.AccountSpeciesNum,
                            detail.AccountFee,
                            detail.ActualSpeciesNum,
                            detail.ActualFee,
                            detail.DifferSpeciesNum,                            
							detail.DifferenceMoney,
                            detail.BottomCoverage
                        };
                        return values;
                    });
                }
            }
            return fileName;
        }
    }
}
