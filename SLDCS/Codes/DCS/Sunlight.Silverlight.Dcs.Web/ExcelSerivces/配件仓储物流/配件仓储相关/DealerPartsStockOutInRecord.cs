﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string ExportDealerPartsStockOutInRecord(string[] billCodes, string billCode, int[] sparepartIds, DateTime? dateTimeBegin, DateTime? dateTimeEnd) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("导出出入库记录_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var query = 出入库记录查询(billCode, sparepartIds, dateTimeBegin, dateTimeEnd);
            //            根据查询传入参数调用查询后导出数据：
            //查询视图：服务站历史出入库查询（DealerPartsStockOutInRecord）
            //服务站历史出入库查询.PartsId in （参数.配件ID列表）
            //若参数.单据编号不为空，则服务站历史出入库查询.code =参数.单据编号
            //若参数.发生日期不为空，则服务站历史出入库查询.CreateTime 在参数.发生日期的时间范围内
            //导出字段：单据编号，出入库类型，配件图号，配件名称，数量，发生日期
            if(billCodes != null && billCodes.Any()) {
                query = query.Where(r => billCodes.Contains(r.code.ToUpper()));
            }
            var dealerPartsStockOutInRecordArray = query.ToArray();
            if(dealerPartsStockOutInRecordArray.Any()) {
                excelColumns.Add(ErrorStrings.ExportColumn_OrderCode);
                excelColumns.Add(ErrorStrings.ExportColumn_InoutType);
                excelColumns.Add(ErrorStrings.ExportColumn_SparePartCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SparePartName);
                excelColumns.Add(ErrorStrings.ExportColumn_Quantity);
                excelColumns.Add(ErrorStrings.ExportColumn_HappenDate);

                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dealerPartsStockOutInRecordArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dealerPartsStockOutInRecordArray[index - 1];
                        var values = new object[] {
                            detail.code,
                            detail.OutInBandtype,
                            detail.PartsCode,
                            detail.PartsName,
                            detail.Quantity,
                            detail.CreateTime
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
    }
}
