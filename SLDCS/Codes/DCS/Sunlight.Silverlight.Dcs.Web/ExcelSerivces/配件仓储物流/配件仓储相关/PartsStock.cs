﻿
using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string ExportPartsStock(int[] ids, int? storageCompanyId, int? warehouseId, int? warehouseAreaCategoryValue, string sparePartName, string sparePartsCode, string warehouseAreaCode, int? priceType, bool? greaterThanZero, string warehouseRegionCode) {
            var query = 查询配件库位库存1(null, warehouseAreaCategoryValue, null, null, warehouseRegionCode, priceType, greaterThanZero, sparePartsCode);
            var userInfo = Utils.GetCurrentUserInfo();
            if(ids != null && ids.Any()) {
                query = query.Where(r => ids.Contains(r.PartsStockId));
            } else {
                if(storageCompanyId.HasValue) {
                    query = query.Where(r => r.StorageCompanyId == userInfo.EnterpriseId);
                }
                if(warehouseId.HasValue) {
                    query = query.Where(r => r.WarehouseId == warehouseId.Value);
                }
                if(warehouseAreaCategoryValue.HasValue) {
                    query = query.Where(r => r.WarehouseAreaCategory == warehouseAreaCategoryValue);
                }
                if (!string.IsNullOrEmpty(sparePartsCode)) {
                    var spareparts = sparePartsCode.Split(',');
                    if (spareparts.Length == 1) {
                        var sparepartcode = spareparts[0];
                        query = query.Where(r => r.SparePartCode.Contains(sparepartcode));
                    } else {
                        query = query.Where(r => spareparts.Contains(r.SparePartCode));
                    }
                }
                if(!string.IsNullOrEmpty(sparePartName)) {
                    query = query.Where(r => r.SparePartName.Contains(sparePartName));
                }
                if(!string.IsNullOrEmpty(warehouseAreaCode)) {
                    query = query.Where(r => r.WarehouseAreaCode.Contains(warehouseAreaCode));
                }
            }
                   
            var queryReuslt = query.ToArray();

            var list = new List<object[]>();
            try {
                for(int k = 0; k < queryReuslt.Length; k++) {
                    object o0 = queryReuslt[k].PartsSalesCategoryName;
                    Object o1 = queryReuslt[k].WarehouseName;
                    Object o2 = Enum.GetName(typeof(DcsAreaType), queryReuslt[k].WarehouseAreaCategory);
                    Object o8 = queryReuslt[k].WarehouseRegionCode;
                    Object o3 = queryReuslt[k].WarehouseAreaCode;
                    Object o4 = queryReuslt[k].SparePartCode;
                    Object o5 = queryReuslt[k].SparePartName;
                    Object o6 = queryReuslt[k].Quantity;
                    Object o7 = queryReuslt[k].SalesPrice;
                    Object o9 = queryReuslt[k].LockedQty;
                    Object[] objects = new[] {
                        o0,o1, o2,o8, o3, o4, o5, o6,o9, o7
                    };
                    list.Add(objects);
                }

                var excelColumns = new List<string> {
                 ErrorStrings.ExportColumn_PartsSalesCategory,
                 ErrorStrings.ExportColumn_WarehouseName,
                 ErrorStrings.ExportColumn_WarehouseAreaCategory,
                 ErrorStrings.ExportColumn_WarehouseArea2,
                 ErrorStrings.ExportColumn_WarehouseAreaCode2, 
                 ErrorStrings.ExportColumn_PartCode,
                 ErrorStrings.ExportColumn_SparePartName, 
                 ErrorStrings.ExportColumn_Quantity,
                 "锁定量",
                 ErrorStrings.ExportColumn_SalePrice
            };
                return ExportAllData(list.ToArray(), excelColumns, "配件库存明细");
            } catch(Exception ex) {
                var check = list.Count;
                throw ex;
            }
        }

        public string ExactExportPartsStock(int[] ids, int? storageCompanyId, int? warehouseId, int? warehouseAreaCategoryValue, string sparePartName, string sparePartsCode, string warehouseAreaCode, int? priceType, bool? greaterThanZero) {
            var query = 查询配件库位库存1(null, warehouseAreaCategoryValue, null, null, null, priceType, greaterThanZero, null);
            var userInfo = Utils.GetCurrentUserInfo();
            if (ids != null && ids.Any()) {
                query = query.Where(r => ids.Contains(r.PartsStockId));
            } else {

                if (storageCompanyId.HasValue) {
                    query = query.Where(r => r.StorageCompanyId == userInfo.EnterpriseId);
                }
                if (warehouseId.HasValue) {
                    query = query.Where(r => r.WarehouseId == warehouseId.Value);
                }
                if (warehouseAreaCategoryValue.HasValue) {
                    query = query.Where(r => r.WarehouseAreaCategory == warehouseAreaCategoryValue);
                }
                if (!string.IsNullOrEmpty(sparePartsCode)) {
                    var spareparts = sparePartsCode.Split(',');
                    if (spareparts.Length == 1) {
                        var sparepartcode = spareparts[0];
                        query = query.Where(r => r.SparePartCode.Equals(sparepartcode));
                    } else {
                        query = query.Where(r => spareparts.Contains(r.SparePartCode));
                    }
                }
                if (!string.IsNullOrEmpty(sparePartName)) {
                    query = query.Where(r => r.SparePartName.Contains(sparePartName));
                }
                if (!string.IsNullOrEmpty(warehouseAreaCode)) {
                    query = query.Where(r => r.WarehouseAreaCode.Contains(warehouseAreaCode));
                }
            }

            var queryReuslt = query.ToArray();
            
            var list = new List<object[]>();
            try {
                for (int k = 0; k < queryReuslt.Length; k++) {
                    object o0 = queryReuslt[k].PartsSalesCategoryName;
                    Object o1 = queryReuslt[k].WarehouseName;
                    Object o2 = Enum.GetName(typeof(DcsAreaType), queryReuslt[k].WarehouseAreaCategory);
                    Object o3 = queryReuslt[k].WarehouseAreaCode;
                    Object o4 = queryReuslt[k].SparePartCode;
                    Object o5 = queryReuslt[k].SparePartName;
                    Object o6 = queryReuslt[k].Quantity;
                    Object o7 = queryReuslt[k].SalesPrice;
					Object o9 = queryReuslt[k].LockedQty;
                    Object[] objects = new[] {
                        o0,o1, o2, o3, o4, o5, o6,o9, o7
                    };
                    list.Add(objects);
                }

                if (queryReuslt.Length == 0) {
                    object o0 = "";
                    Object o1 = "";
                    Object o2 = "";
                    Object o3 = "";
                    Object o4 = "";
                    Object o5 = "";
                    Object o6 ="";
                    Object o7 = "";
					Object o9 ="";
                    Object[] objects = new[] {
                        o0,o1, o2, o3, o4, o5, o6,o9, o7
                    };
                    list.Add(objects);
                }
                var excelColumns = new List<string> {
                 ErrorStrings.ExportColumn_PartsSalesCategory,
                 ErrorStrings.ExportColumn_WarehouseName,
                 ErrorStrings.ExportColumn_WarehouseAreaCategory,
                 ErrorStrings.ExportColumn_WarehouseAreaCode2, 
                 ErrorStrings.ExportColumn_PartCode,
                 ErrorStrings.ExportColumn_SparePartName, 
                 ErrorStrings.ExportColumn_Quantity, 
				 "锁定量",
                 ErrorStrings.ExportColumn_SalePrice
            };
                return ExportAllData(list.ToArray(), excelColumns, "配件库存明细");
            } catch (Exception ex) {
                var check = list.Count;
                throw ex;
            }
        }

        // 根据传入的条件查询配件库存明细
        private IQueryable<PartsStock> GetPartsStock(bool? greaterThanZero) {
            if(greaterThanZero == null) {
                return from a in ObjectContext.PartsStocks
                       select a;
            } else if((bool)greaterThanZero) {
                return from a in ObjectContext.PartsStocks
                       where a.Quantity > 0
                       select a;
            } else {
                return from a in ObjectContext.PartsStocks
                       where a.Quantity <= 0
                       select a;
            }
        }

        public string ExportVirtualPartsStockForShelves(int warehouseId, string partCode, string partName, string warehouseAreaCode) {
            var result = 查询待上架配件库存(warehouseId);
            if(!string.IsNullOrEmpty(partCode)) {
                result = result.Where(r => r.SparePartCode.Contains(partCode));
            }
            if(!string.IsNullOrEmpty(partName)) {
                result = result.Where(r => r.SparePartName.Contains(partName));
            }
            if(!string.IsNullOrEmpty(warehouseAreaCode)) {
                result = result.Where(r => r.WarehouseAreaCode.Contains(warehouseAreaCode));
            }
            var queryReuslt = result.ToArray().Select(r => new object[] {
               r.SparePartCode,
                r.SparePartName,
                r.UsableQuantity
            }).ToArray();
            var excelColumns = new List<string> {
                ErrorStrings.ExportColumn_SparePartCode,
                ErrorStrings.ExportColumn_SparePartName,
                ErrorStrings.ExportColumn_Quantity,
            };
            return ExportAllData(queryReuslt, excelColumns, "待上架清单");
        }

        public string ExportPartsStockForWarranty(int storageCompanyId, int? warehouseId, int? partsSalesCategyId, string partsName, string partsCode) {
            var result = 只查询仓库库存保外(storageCompanyId, null, null);
            if(!string.IsNullOrEmpty(partsCode)) {
                result = result.Where(r => r.SparePartCode.Contains(partsCode));
            }
            if(!string.IsNullOrEmpty(partsName)) {
                result = result.Where(r => r.SparePartName.Contains(partsName));
            }
            if(warehouseId.HasValue) {
                result = result.Where(r => r.WarehouseId == warehouseId);
            }
            if(partsSalesCategyId.HasValue) {
                result = result.Where(r => r.PartsSalesCategoryId == partsSalesCategyId);
            }
            var queryReuslt = result.ToArray().Select(r => new object[] {
                r.WarehouseCode,
                r.WarehouseName,
                r.BranchName,
                r.SparePartCode,
                r.SparePartName,
                Enum.GetName(typeof(DcsPartsSalesPricePriceType), r.PriceType),
                r.PartsSalesPrice,
                r.Quantity,
                r.UsableQuantity
            }).ToArray();
            var excelColumns = new List<string> {
				ErrorStrings.ExportColumn_WarehouseCode,
				ErrorStrings.ExportColumn_WarehouseName, 
				ErrorStrings.ExportColumn_Branch2, 
				ErrorStrings.ExportColumn_PartCode, 
				ErrorStrings.ExportColumn_SparePartName,
				ErrorStrings.ExportColumn_PriceType,
				"销售价",
				"数量",
				"可用库存"
            };
            return ExportAllData(queryReuslt, excelColumns, "仓库库存_保外");
        }

        public string ExportWarehousePartsStockQuerys(int[] ids, int? storageCompanyId, int? warehouseId, string partsName, string partsCode, int? partsSalesCategoryId, int? storageCenter) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("仓库库存_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var userInfo = Utils.GetCurrentUserInfo();
            var data = 只查询仓库库存(userInfo.EnterpriseId, null, null);

            if(ids != null && ids.Any()) {
                data = data.Where(r => ids.Contains(r.Id));
            } else {
                if(storageCompanyId.HasValue) {
                    data = data.Where(r => r.StorageCompanyId == storageCompanyId.Value);
                }
                if(warehouseId.HasValue) {
                    data = data.Where(r => r.WarehouseId == warehouseId.Value);
                }
                if(!string.IsNullOrEmpty(partsName)) {
                    data = data.Where(r => r.PartsName.Contains(partsName));
                }
                if(!string.IsNullOrEmpty(partsCode)) {
                    data = data.Where(r => r.PartsCode.Contains(partsCode));
                }
                if(partsSalesCategoryId.HasValue) {
                    data = data.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId.Value);
                }
                if(storageCenter.HasValue) {
                    data = data.Where(r => r.StorageCenter == storageCenter.Value);
                }
            }
            var dataArray = data.ToArray();
            if(dataArray.Any()) {
                excelColumns.Add(ErrorStrings.ExportColumn_WarehouseCode);
                excelColumns.Add(ErrorStrings.ExportColumn_WarehouseName);
                excelColumns.Add(ErrorStrings.ExportColumn_StorageCenter);
                excelColumns.Add(ErrorStrings.ExportColumn_Branch2);
                excelColumns.Add(ErrorStrings.ExportColumn_PartCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SparePartName);
                excelColumns.Add(ErrorStrings.ExportColumn_DealerPrice);
                excelColumns.Add(ErrorStrings.ExportColumn_Quantity);
                excelColumns.Add(ErrorStrings.ExportColumn_UsableStock);

                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.WarehouseCode,
                            detail.WarehouseName,
                            Enum.GetName(typeof(DcsStorageCenter),  detail.StorageCenter),                         
                            detail.BranchName,
                            detail.SparePartCode,
                            detail.SparePartName,
                            detail.PartsSalesPrice,
                            detail.Quantity,
                            detail.UsableQuantity,
                        
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
    }
}
