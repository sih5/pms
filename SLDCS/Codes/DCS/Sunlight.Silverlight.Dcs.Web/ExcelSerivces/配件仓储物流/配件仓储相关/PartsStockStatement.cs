﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService{
        
        public string ExportPartsStockStatement(int? warehouseId, string sparePartCode, string sparePartName, string referenceCode, DateTime? beginTime, DateTime? endTime) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("库存对账报表_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = 查询库存对账报表(warehouseId,sparePartCode,sparePartName,referenceCode,beginTime,endTime);
            
            var dataArray = data.ToArray();
            if (data.Any())
            {
                excelColumns.Add(ErrorStrings.ExportColumn_WarehouseCode);
                excelColumns.Add(ErrorStrings.ExportColumn_WarehouseName);
                excelColumns.Add(ErrorStrings.ExportColumn_PartCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SparePartName);
                excelColumns.Add(ErrorStrings.ExportColumn_SIHCode);
                excelColumns.Add(ErrorStrings.ExportColumn_BeginQuantity);
                excelColumns.Add(ErrorStrings.ExportColumn_OutboundAmount);
                excelColumns.Add(ErrorStrings.ExportColumn_InboundAmount);
                excelColumns.Add(ErrorStrings.ExportColumn_InventoryQuantity);
                excelColumns.Add(ErrorStrings.ExportColumn_EndQuantity);

                using (var excelExport = new ExcelExport(exportDataFileName))
                {
                    excelExport.ExportByRow(index =>
                    {
                        if (index == dataArray.Count() + 1)
                            return null;
                        if (index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.WarehouseCode,
                            detail.WarehouseName,
                            detail.SparePartCode,
                            detail.SparePartName,
                            detail.ReferenceCode,
                            detail.BeginQuantity,
                            detail.OutQuantity,
                            detail.InQuantity,
                            detail.InventoryQuantity,
                            detail.EndQuantity
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }

    }
}
