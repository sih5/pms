﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        /// <summary>
        /// 导出配件追溯码库位库龄
        /// </summary>
        public string 导出配件追溯码库位库龄( string warehouseName, string warehouseAreaCode, string storehouseAreaCode, string sparePartCode, int? traceProperty, string traceCode, bool? greaterThanZero, int? kvAge,int?companyType) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("配件追溯码库位库龄_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var userInfo = Utils.GetCurrentUserInfo();
            var traceWarehouseAges = 查询配件追溯码库位库龄(warehouseName, warehouseAreaCode, storehouseAreaCode, sparePartCode, traceProperty, traceCode, greaterThanZero, kvAge, null, companyType);
           
            var DealerPartsStocksWithSubDealerArray = traceWarehouseAges.ToArray();
            if(DealerPartsStocksWithSubDealerArray.Any()) {
                excelColumns.Add("配件编号");
                excelColumns.Add("配件名称");
                excelColumns.Add("仓库名称");
                excelColumns.Add("库区编号");
                excelColumns.Add("库位编号");
                excelColumns.Add("追溯属性");
                excelColumns.Add("数量");
                excelColumns.Add("经销价");
				excelColumns.Add("追溯码");
                excelColumns.Add("SIH标签码");
				excelColumns.Add("入库时间");
				excelColumns.Add("库龄");

                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == DealerPartsStocksWithSubDealerArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = DealerPartsStocksWithSubDealerArray[index - 1];
                        var values = new object[] {
                            detail.SparePartCode,
                            detail.SparePartName,
                            detail.WarehouseName,
                            detail.StorehouseAreaCode,
                            detail.WarehouseAreaCode,
							 Enum.GetName(typeof(DCSTraceProperty), detail.TraceProperty==null?0:detail.TraceProperty),
                            detail.InQty,
                            detail.SalesPrice,
                            detail.TraceCode,
							detail.SIHLabelCode,
							detail.InBoundDate,
							detail.Age
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
		 /// <summary>
        /// 导出服务站配件追溯码库位库龄
        /// </summary>
        public string 导出服务站配件追溯码库位库龄(string sparePartCode, int? traceProperty, string traceCode, bool? greaterThanZero, int? kvAge, string companyCode, string companyName) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("服务站配件追溯码库位库龄_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var userInfo = Utils.GetCurrentUserInfo();
            var traceWarehouseAges = 查询服务站配件追溯码库位库龄(sparePartCode, traceProperty, traceCode, greaterThanZero, kvAge, companyCode, companyName);
           
            var DealerPartsStocksWithSubDealerArray = traceWarehouseAges.ToArray();
            if(DealerPartsStocksWithSubDealerArray.Any()) {
                excelColumns.Add("服务站编号");
                excelColumns.Add("服务站名称");  
                excelColumns.Add("配件编号");
                excelColumns.Add("配件名称");                
                excelColumns.Add("追溯属性");
                excelColumns.Add("数量");
                excelColumns.Add("经销价");
				excelColumns.Add("追溯码");
                excelColumns.Add("SIH标签码");
				excelColumns.Add("入库时间");
				excelColumns.Add("库龄");

                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == DealerPartsStocksWithSubDealerArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = DealerPartsStocksWithSubDealerArray[index - 1];
                        var values = new object[] {
                            detail.CompanyCode,
                            detail.CompanyName,
                            detail.SparePartCode,
                            detail.SparePartName,                          
							 Enum.GetName(typeof(DCSTraceProperty), detail.TraceProperty==null?0:detail.TraceProperty),
                            detail.InQty,
                            detail.SalesPrice,
                            detail.TraceCode,
							detail.SIHLabelCode,
							detail.InBoundDate,
							detail.Age
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
		// <summary>
        /// 导出配件入库精确码物流跟踪
        /// </summary>
        public string 导出配件入库精确码物流跟踪( string sparePartCode, string counterpartCompanyCode, string traceCode, string counterpartCompanyName, string sIHLabelCode,DateTime? bInBoundDate,DateTime? eInBoundDate,string packingTaskCode,int? warehouseId,int? status ,string originalRequirementBillCode) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("配件入库精确码物流跟踪_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var userInfo = Utils.GetCurrentUserInfo();
            var traceWarehouseAges = 配件入库精确码物流跟踪(sparePartCode,counterpartCompanyCode,traceCode,counterpartCompanyName,sIHLabelCode,bInBoundDate,eInBoundDate,packingTaskCode,warehouseId,status,originalRequirementBillCode);
           
            var DealerPartsStocksWithSubDealerArray = traceWarehouseAges.ToArray();
            if(DealerPartsStocksWithSubDealerArray.Any()) {
                excelColumns.Add("对方单位代码");
                excelColumns.Add("对方单位名称"); 
                excelColumns.Add("配件编号");
                excelColumns.Add("配件名称");  
                excelColumns.Add("追溯码");	
                excelColumns.Add("SIH标签码");		
                excelColumns.Add("仓库编号");
                excelColumns.Add("仓库名称");					
                excelColumns.Add("原始单据编号");
                excelColumns.Add("供应商发运单号");                				               
				excelColumns.Add("入库时间");
				excelColumns.Add("包装任务单号");
				excelColumns.Add("入库类型");
				excelColumns.Add("状态");
                excelColumns.Add("入库数量");
                excelColumns.Add("出库数量");

                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == DealerPartsStocksWithSubDealerArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = DealerPartsStocksWithSubDealerArray[index - 1];
                        var values = new object[] {
							detail.SuplierCode,
                            detail.SuplierName,     
                            detail.SparePartCode,
                            detail.SparePartName,    
                            detail.TraceCode,
                            detail.SIHLabelCode,
							detail.WarehouseCode,
							detail.WarehouseName,	
							detail.OriginalRequirementBillCode,	
							detail.SourceCode,	
							detail.InBoundDate,
							detail.PackingTaskCode,							
							Enum.GetName(typeof(DcsPartsInboundType), detail.InboundType==null?0:detail.InboundType),
                            Enum.GetName(typeof(DCSAccurateTraceStatus), detail.Status==null?0:detail.Status),
							detail.InQty,	
						    detail.OutQty

                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
		// <summary>
        /// 导出配件入库精确码物流跟踪
        /// </summary>
        public string 导出中心库配件入库精确码物流跟踪( string sparePartCode, string inBoundCode, string traceCode, string originalRequirementBillCode, string sIHLabelCode,DateTime? bInBoundDate,DateTime? eInBoundDate,string warehouseName,int? status,string sourceCode ) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("中心库配件入库精确码物流跟踪_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var userInfo = Utils.GetCurrentUserInfo();
            var traceWarehouseAges = 中心库配件入库精确码物流跟踪(sparePartCode,inBoundCode,traceCode,originalRequirementBillCode,sIHLabelCode,bInBoundDate,eInBoundDate,warehouseName,status,sourceCode);
           
            var DealerPartsStocksWithSubDealerArray = traceWarehouseAges.ToArray();
            if(DealerPartsStocksWithSubDealerArray.Any()) {			
                excelColumns.Add("配件编号");
                excelColumns.Add("配件名称");  
                excelColumns.Add("追溯码");	
                excelColumns.Add("SIH标签码");		
                excelColumns.Add("仓库编号");
                excelColumns.Add("仓库名称");					
                excelColumns.Add("原始单据编号");
				excelColumns.Add("收货单号");
                excelColumns.Add("入库单号");                				               
				excelColumns.Add("入库时间");
				excelColumns.Add("状态");
				excelColumns.Add("入库数量");
                excelColumns.Add("出库数量");

                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == DealerPartsStocksWithSubDealerArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = DealerPartsStocksWithSubDealerArray[index - 1];
                        var values = new object[] {						 
                            detail.SparePartCode,
                            detail.SparePartName,    
                            detail.TraceCode,
                            detail.SIHLabelCode,
							detail.WarehouseCode,
							detail.WarehouseName,	
							detail.OriginalRequirementBillCode,	
							detail.SourceCode,	
							detail.InBoundCode,
							detail.InBoundDate,						
                            Enum.GetName(typeof(DCSAccurateTraceStatus), detail.Status==null?0:detail.Status),
							detail.InQty,	
						    detail.OutQty
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
		/// <summary>
        /// 导出配件追溯码库位库龄
        /// </summary>
        public string 导出SIH标签码库位库龄( string warehouseName, string warehouseAreaCode, string storehouseAreaCode, string sparePartCode, int? traceProperty, string sIHLabelCode, bool? greaterThanZero, int? kvAge) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("配件追溯码库位库龄_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var userInfo = Utils.GetCurrentUserInfo();
            var traceWarehouseAges = 查询SIH标签码库位库龄(warehouseName,warehouseAreaCode,storehouseAreaCode,sparePartCode,traceProperty,sIHLabelCode,greaterThanZero,kvAge,null);
           
            var DealerPartsStocksWithSubDealerArray = traceWarehouseAges.ToArray();
            if(DealerPartsStocksWithSubDealerArray.Any()) {
                excelColumns.Add("配件编号");
                excelColumns.Add("配件名称");
                excelColumns.Add("仓库名称");
                excelColumns.Add("库区编号");
                excelColumns.Add("库位编号");
                excelColumns.Add("追溯属性");
                excelColumns.Add("数量");
                excelColumns.Add("经销价");
                excelColumns.Add("SIH标签码");
				excelColumns.Add("入库时间");
				excelColumns.Add("库龄");

                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == DealerPartsStocksWithSubDealerArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = DealerPartsStocksWithSubDealerArray[index - 1];
                        var values = new object[] {
                            detail.SparePartCode,
                            detail.SparePartName,
                            detail.WarehouseName,
                            detail.StorehouseAreaCode,
                            detail.WarehouseAreaCode,
							 Enum.GetName(typeof(DCSTraceProperty), detail.TraceProperty==null?0:detail.TraceProperty),
                            detail.InQty,
                            detail.SalesPrice,
							detail.SIHLabelCode,
							detail.InBoundDate,
							detail.Age
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
    }
}
