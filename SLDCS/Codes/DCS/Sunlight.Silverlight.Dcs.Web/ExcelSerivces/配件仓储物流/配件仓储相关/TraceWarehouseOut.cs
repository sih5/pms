﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        /// <summary>
        /// 导出配件出库精确码物流跟踪
        /// </summary>
        public string 导出配件出库精确码物流跟踪(int? warehouseId, string sparePartCode, string traceCode, string originalRequirementBillCode, string outCode, string counterpartCompanyName, int? outboundType, int? status, DateTime? bCreatetime, DateTime? eCreatetime) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("配件出库精确码物流跟踪_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var userInfo = Utils.GetCurrentUserInfo();
            var traceWarehouseAges = 配件出库精确码物流跟踪(warehouseId, sparePartCode, traceCode, originalRequirementBillCode, outCode, counterpartCompanyName, outboundType, status, bCreatetime,eCreatetime);

            var DealerPartsStocksWithSubDealerArray = traceWarehouseAges.ToArray();
            if(DealerPartsStocksWithSubDealerArray.Any()) {
				excelColumns.Add("仓库编号");
				excelColumns.Add("仓库名称");
                excelColumns.Add("配件编号");
                excelColumns.Add("配件名称");
                excelColumns.Add("追溯码");
                excelColumns.Add("SIH标签码");
                excelColumns.Add("原始单据编号");
                excelColumns.Add("出库单号");
                excelColumns.Add("出库时间");
                excelColumns.Add("客户名称");
                excelColumns.Add("客户编号");
                excelColumns.Add("收货单号");               
                excelColumns.Add("出库类型");
                excelColumns.Add("状态");

                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == DealerPartsStocksWithSubDealerArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = DealerPartsStocksWithSubDealerArray[index - 1];
                        var values = new object[] {
							detail.WarehouseCode,
							detail.WarehouseName,
                            detail.SparePartCode,
                            detail.SparePartName,
                            detail.TraceCode,
							detail.SIHLabelCode,
                            detail.OriginalRequirementBillCode,
                            detail.OutCode,
							detail.OutTime,
							detail.CounterpartCompanyName,
							detail.CounterpartCompanyCode,
							detail.ShippingCode,
							Enum.GetName(typeof(DcsPartsOutboundType), detail.OutboundType==null?0:detail.OutboundType), 
                            Enum.GetName(typeof(DCSAccurateTraceStatus), detail.Status==null?0:detail.Status), 							
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        } 
        /// <summary>
        /// 导出配件出库精确码物流跟踪
        /// </summary>
        public string 导出中心库配件出库精确码物流跟踪(string warehouseName, string sparePartCode, string traceCode, string originalRequirementBillCode, string outCode, string counterpartCompanyCode, DateTime? bOutTime,DateTime? eOutTime) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("中心库配件出库精确码物流跟踪_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var userInfo = Utils.GetCurrentUserInfo();
            var traceWarehouseAges = 中心库配件出库精确码物流跟踪(warehouseName, sparePartCode, traceCode, originalRequirementBillCode, outCode, counterpartCompanyCode, bOutTime, eOutTime);

            var DealerPartsStocksWithSubDealerArray = traceWarehouseAges.ToArray();
            if(DealerPartsStocksWithSubDealerArray.Any()) {
				excelColumns.Add("仓库编号");
				excelColumns.Add("仓库名称");
                excelColumns.Add("配件编号");
                excelColumns.Add("配件名称");
                excelColumns.Add("追溯码");
                excelColumns.Add("SIH标签码");
                excelColumns.Add("中心库原始单据编号");
                excelColumns.Add("中心库出库单号");
                excelColumns.Add("中心库出库时间");
                excelColumns.Add("收货单位名称");
                excelColumns.Add("收货单位编号");
                excelColumns.Add("收货单号");               
                excelColumns.Add("出库类型");
                excelColumns.Add("状态");

                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == DealerPartsStocksWithSubDealerArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = DealerPartsStocksWithSubDealerArray[index - 1];
                        var values = new object[] {
							detail.WarehouseCode,
							detail.WarehouseName,
                            detail.SparePartCode,
                            detail.SparePartName,
                            detail.TraceCode,
							detail.SIHLabelCode,
                            detail.OriginalRequirementBillCode,
                            detail.OutCode,
							detail.OutTime,
							detail.CounterpartCompanyName,
							detail.CounterpartCompanyCode,
							detail.ShippingCode,
							Enum.GetName(typeof(DcsPartsOutboundType), detail.OutboundType==null?0:detail.OutboundType), 
                            Enum.GetName(typeof(DCSAccurateTraceStatus), detail.Status==null?0:detail.Status), 							
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        } 		
    }
}
