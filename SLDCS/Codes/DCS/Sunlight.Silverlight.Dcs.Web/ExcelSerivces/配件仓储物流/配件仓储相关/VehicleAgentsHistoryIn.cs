﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;


namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string 导出代理库入库记录查询( string branchName, string partsSalesCategoryName, string warehouseName, int? inboundType, string storageCompanyCode, string storageCompanyName, string partCode, string partName, DateTime? createTimeBegin, DateTime? createTimeEnd) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("代理库入库记录_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var Dealeres = 代理库入库记录查询();

            if(!string.IsNullOrEmpty(branchName)) {
                Dealeres = Dealeres.Where(r => r.BranchName == branchName);
                }
            if(!string.IsNullOrEmpty(partsSalesCategoryName)) {
                Dealeres = Dealeres.Where(r => r.PartsSalesCategoryName == partsSalesCategoryName);
                }
            if(!string.IsNullOrEmpty(warehouseName)) {
                Dealeres = Dealeres.Where(r => r.WarehouseName == warehouseName);
            }
            if(!string.IsNullOrEmpty(storageCompanyCode)) {
                Dealeres = Dealeres.Where(r => r.StorageCompanyCode == storageCompanyCode);
            }
            if(!string.IsNullOrEmpty(storageCompanyName)) {
                Dealeres = Dealeres.Where(r => r.StorageCompanyName == storageCompanyName);
            }
            if(!string.IsNullOrEmpty(partCode)) {
                Dealeres = Dealeres.Where(r => r.PartCode == partCode);
            }
            if(!string.IsNullOrEmpty(partName)) {
                Dealeres = Dealeres.Where(r => r.PartName == partName);
            }
        
            if(inboundType.HasValue) {
                Dealeres = Dealeres.Where(r => r.InboundType == inboundType.Value);
                }
            if(createTimeBegin.HasValue) {
                Dealeres = Dealeres.Where(r => r.PartsInboundPlanTime >= createTimeBegin);
                }
                if(createTimeEnd.HasValue) {
                    Dealeres = Dealeres.Where(r => r.PartsInboundPlanTime <= createTimeEnd);
                }
            

            var DealeresArray = Dealeres.ToArray();
            if(Dealeres.Any()) {
                excelColumns.Add("入库类型");
                excelColumns.Add("仓储企业编号");
                excelColumns.Add("仓储企业名称");
                excelColumns.Add(ErrorStrings.ExportColumn_Province);
                excelColumns.Add(ErrorStrings.ExportColumn_WarehouseCode);
                excelColumns.Add(ErrorStrings.ExportColumn_WarehouseName);
                excelColumns.Add("品牌");
                excelColumns.Add("入库单编号");
                excelColumns.Add(ErrorStrings.ExportColumn_InboundTime);
                excelColumns.Add("原始需求单据编号");
                excelColumns.Add("原始需求单据类型");
                excelColumns.Add("库位");
                excelColumns.Add(ErrorStrings.ExportColumn_PartCode);
                excelColumns.Add("配件名称");
                excelColumns.Add(ErrorStrings.ExportColumn_InboundAmount);
                excelColumns.Add("代理库批发价");
                excelColumns.Add("入库金额");
                excelColumns.Add(ErrorStrings.ExportColumn_SettlementStatus);
                excelColumns.Add(ErrorStrings.ExportColumn_Branch);
                excelColumns.Add("对方单位编号");
                excelColumns.Add("对方单位名称");

                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == DealeresArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = DealeresArray[index - 1];
                        var values = new object[] {
                            Enum.GetName(typeof(DcsPartsInboundType),detail.InboundType),
                            detail.StorageCompanyCode,
                            detail.StorageCompanyName,
                            detail.ProvinceName,
                            detail.WarehouseCode,
                            detail.WarehouseName,
                            detail.PartsSalesCategoryName,
                            detail.PartsInboundPlanCode,
                            detail.PartsInboundPlanTime, 
                            detail.OriginalRequirementBillCode,
                            Enum.GetName(typeof(DcsOriginalRequirementBillType),detail.OriginalRequirementBillType),
                            detail.WarehouseAreaCode,
                            detail.PartCode,
                            detail.PartName,
                            detail.InspectedQuantity,
                            detail.SettlementPrice,
                            detail.Price,
                            Enum.GetName(typeof(DcsPartsSettlementStatus),detail.SettlementStatus),
                            detail.BranchName,
                            detail.CounterPartCompanyCode,
                            detail.CounterPartCompanyName
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
    }
}