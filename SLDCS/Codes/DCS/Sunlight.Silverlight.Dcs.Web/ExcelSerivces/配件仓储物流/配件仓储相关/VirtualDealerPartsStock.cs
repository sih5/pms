﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        /// <summary>
        /// 导出经销商配件库存
        /// </summary>
        public string ExportVirtualDealerPartsStock(int[] ids, int? salesCategoryId, int? priceType, string sparePartCode, string sparePartName, int? companyType, string dealerName,bool? isDealerQuery,bool? isZero) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("经销商配件库存_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var userInfo = Utils.GetCurrentUserInfo();
            var dealerPartsStocks = GetDealerPartsStocksWithSubDealer(sparePartCode, isDealerQuery, isZero);
            var query = dealerPartsStocks;
            if(ids != null && ids.Any()) {
                query = query.Where(r => ids.Contains(r.Id));
            } else {
                if(companyType.HasValue) {
                    query = query.Where(r => r.CompanyType == companyType.Value);
                }
                if(!string.IsNullOrEmpty(sparePartName)) {
                    query = query.Where(r => r.SparePartName.ToUpper().Contains(sparePartName.ToUpper()));
                }
                if(!string.IsNullOrEmpty(dealerName)) {
                    query = query.Where(r => r.DealerName.ToUpper().Contains(dealerName.ToUpper()));
                }
            }
            var DealerPartsStocksWithSubDealerArray = query.ToArray();
            if(DealerPartsStocksWithSubDealerArray.Any()) {
                excelColumns.Add("服务站编号");
                excelColumns.Add("服务站名称");
                excelColumns.Add("企业类型");
                excelColumns.Add("分销中心");
                excelColumns.Add("品牌");
                excelColumns.Add("配件编号");
                excelColumns.Add("配件名称");
                excelColumns.Add("数量");
                excelColumns.Add("经销价");
                excelColumns.Add("经销总价");

                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == DealerPartsStocksWithSubDealerArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = DealerPartsStocksWithSubDealerArray[index - 1];
                        var values = new object[] {
                            detail.DealerCode,
                            detail.DealerName,
                            detail.DealerType,
                            detail.MarketingDepartmentName,
                            detail.SalesCategoryName,
                            detail.SparePartCode,
                            detail.SparePartName,
                            detail.Quantity,
                            detail.BasicSalePrice,
                            detail.BasicSalePriceAll
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
    }
}
