﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string ExportVirtualLogistic(int[] logisticIds, int[] processIds, string shippingCode, string orderCode, DateTime? beginShippingDate, DateTime? endShippingDate, DateTime? beginApproveTime, DateTime? endApproveTime, DateTime? beginRequestedArrivalDate, DateTime? endRequestedArrivalDate, string warehouseName, string logisticName, int? signStatus) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("物流信息主单以及履历_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = 导出物流信息主单以及履历(signStatus, false, processIds, logisticIds);

            if(!string.IsNullOrEmpty(shippingCode)) {
                data = data.Where(r => r.ShippingCode.Contains(shippingCode));
            }
            if(!string.IsNullOrEmpty(orderCode)) {
                data = data.Where(r => r.OrderCode.Contains(orderCode));
            }
            if(beginShippingDate.HasValue) {
                data = data.Where(r => r.ShippingDate >= beginShippingDate.Value);
            }
            if(endShippingDate.HasValue) {
                data = data.Where(r => r.ShippingDate <= endShippingDate.Value);
            }
            if(beginApproveTime.HasValue) {
                data = data.Where(r => r.ApproveTime >= beginApproveTime.Value);
            }
            if(endApproveTime.HasValue) {
                data = data.Where(r => r.ApproveTime <= endApproveTime.Value);
            }
            if(beginRequestedArrivalDate.HasValue) {
                data = data.Where(r => r.RequestedArrivalDate >= beginRequestedArrivalDate.Value);
            }
            if(endRequestedArrivalDate.HasValue) {
                data = data.Where(r => r.RequestedArrivalDate <= endRequestedArrivalDate.Value);
            }
            if(!string.IsNullOrEmpty(warehouseName)) {
                data = data.Where(r => r.WarehouseName.Contains(warehouseName));
            }
            if(!string.IsNullOrEmpty(logisticName)) {
                data = data.Where(r => r.LogisticName.Contains(logisticName));
            }

            var dataArray = data.ToArray();
            if(data.Any()) {
                excelColumns.Add(ErrorStrings.ExportColumn_PartsSalesOrder_Code);
                excelColumns.Add(ErrorStrings.ExportColumn_SalesOrderStatus);
                excelColumns.Add(ErrorStrings.ExportColumn_OrderApproveTIme);
                excelColumns.Add("发货仓库名称");
                excelColumns.Add("目的地名称");
                excelColumns.Add("出库完成时间");
                excelColumns.Add("发运单编号");
                excelColumns.Add("计划到达时间");
                excelColumns.Add("承运商名称");
                excelColumns.Add("发运方式");
                excelColumns.Add("承运商联系电话");
                excelColumns.Add("签收状态");
                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.OrderCode,
                            detail.PartsSalesOrderStatus.HasValue ? Enum.GetName(typeof(DcsPartsSalesOrderStatus), detail.PartsSalesOrderStatus) : "",
                            detail.ApproveTime,
                            detail.WarehouseName,
                            detail.ReceivingAddress,
                            detail.ShippingDate,
                            detail.ShippingCode,
                            detail.RequestedArrivalDate, 
                            detail.LogisticName,
                            detail.ShippingMethod.HasValue ? Enum.GetName(typeof(DcsPartsShippingMethod), detail.ShippingMethod) : "", 
                            detail.TransportDriverPhone,
                            detail.SignStatus == 2 ? "已签收" : detail.SignStatus == 1 ? "在途" : ""
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }

        public string ExportMergeVirtualLogistic(int[] logisticIds, int[] processIds, string shippingCode, string orderCode, DateTime? beginShippingDate, DateTime? endShippingDate, DateTime? beginApproveTime, DateTime? endApproveTime, DateTime? beginRequestedArrivalDate, DateTime? endRequestedArrivalDate, string warehouseName, string logisticName, int? signStatus) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("物流信息主清单以及履历_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = 导出物流信息主单以及履历(signStatus, false, processIds, logisticIds);

            if(!string.IsNullOrEmpty(shippingCode)) {
                data = data.Where(r => r.ShippingCode.Contains(shippingCode));
            }
            if(!string.IsNullOrEmpty(orderCode)) {
                data = data.Where(r => r.OrderCode.Contains(orderCode));
            }
            if(beginShippingDate.HasValue) {
                data = data.Where(r => r.ShippingDate >= beginShippingDate.Value);
            }
            if(endShippingDate.HasValue) {
                data = data.Where(r => r.ShippingDate <= endShippingDate.Value);
            }
            if(beginApproveTime.HasValue) {
                data = data.Where(r => r.ApproveTime >= beginApproveTime.Value);
            }
            if(endApproveTime.HasValue) {
                data = data.Where(r => r.ApproveTime <= endApproveTime.Value);
            }
            if(beginRequestedArrivalDate.HasValue) {
                data = data.Where(r => r.RequestedArrivalDate >= beginRequestedArrivalDate.Value);
            }
            if(endRequestedArrivalDate.HasValue) {
                data = data.Where(r => r.RequestedArrivalDate <= endRequestedArrivalDate.Value);
            }
            if(!string.IsNullOrEmpty(warehouseName)) {
                data = data.Where(r => r.WarehouseName.Contains(warehouseName));
            }
            if(!string.IsNullOrEmpty(logisticName)) {
                data = data.Where(r => r.LogisticName.Contains(logisticName));
            }
            var dataArray = data.ToArray();
            var logisticsids = dataArray.Where(r => r.CurrentOrderType == (int)DcsCurrentOrderType.物流信息主单).Select(r => r.Id).ToArray();
            var logisticsDetails = ObjectContext.LogisticsDetails.Where(r => logisticsids.Contains(r.Logisticsid)).ToArray();
            var exprot1 = (from a in dataArray.Where(r => r.CurrentOrderType == (int)DcsCurrentOrderType.物流信息主单)
                           join c in logisticsDetails on a.Id equals c.Logisticsid into temp
                           from b in temp.DefaultIfEmpty()
                           select new {
                               Id = a.Id,
                               PartsSalesOrderStatus = a.PartsSalesOrderStatus,
                               ShippingCode = a.ShippingCode,
                               OrderCode = a.OrderCode,
                               ShippingCompanyName = a.ShippingCompanyName,
                               ShippingCompanyNumber = a.ShippingCompanyNumber,
                               ApproveTime = a.ApproveTime,
                               WarehouseName = a.WarehouseName,
                               ShippingDate = a.ShippingDate,
                               RequestedArrivalDate = a.RequestedArrivalDate,
                               ReceivingAddress = a.ReceivingAddress,
                               LogisticName = a.LogisticName,
                               TransportDriverPhone = a.TransportDriverPhone,
                               ModifierId = a.ModifierId,
                               ModifierName = a.ModifierName,
                               ModifyTime = a.ModifyTime,
                               CancelId = a.CancelId,
                               CancelName = a.CancelName,
                               CancelTime = a.CancelTime,
                               Status = a.Status,
                               ShippingMethod = a.ShippingMethod,
                               SignStatus = a.SignStatus,
                               CurrentOrderType = (int)DcsCurrentOrderType.物流信息主单,
                               PointName = b == null ? String.Empty : b.PointName,
                               PointTime = b == null ? default(DateTime?) : b.PointTime,
                               DShippingMethod =  b == null ? default(int?) : b.ShippingMethod,
                               ScanTime = b == null ? default(DateTime?) : b.ScanTime,
                               ScanName = b == null ? String.Empty : b.ScanName,
                               Remark = b == null ? String.Empty: b.Remark
                           }).ToArray();
            var exprot2 = (from a in dataArray.Where(r => r.CurrentOrderType != (int)DcsCurrentOrderType.物流信息主单)
                           select new {
                               Id = a.Id,
                               PartsSalesOrderStatus = a.PartsSalesOrderStatus,
                               ShippingCode = a.ShippingCode,
                               OrderCode = a.OrderCode,
                               ShippingCompanyName = a.ShippingCompanyName,
                               ShippingCompanyNumber = a.ShippingCompanyNumber,
                               ApproveTime = a.ApproveTime,
                               WarehouseName = a.WarehouseName,
                               ShippingDate = a.ShippingDate,
                               RequestedArrivalDate = a.RequestedArrivalDate,
                               ReceivingAddress = a.ReceivingAddress,
                               LogisticName = a.LogisticName,
                               TransportDriverPhone = a.TransportDriverPhone,
                               ModifierId = a.ModifierId,
                               ModifierName = a.ModifierName,
                               ModifyTime = a.ModifyTime,
                               CancelId = a.CancelId,
                               CancelName = a.CancelName,
                               CancelTime = a.CancelTime,
                               Status = a.Status,
                               ShippingMethod = a.ShippingMethod,
                               SignStatus = a.SignStatus,
                               CurrentOrderType = (int)DcsCurrentOrderType.配件销售订单,
                               PointName = String.Empty,
                               PointTime = default(DateTime?),
                               DShippingMethod = default(int?),
                               ScanTime = default(DateTime?),
                               ScanName = String.Empty,
                               Remark = String.Empty,
                           }).ToArray();
            var exprot = exprot1.Union(exprot2).OrderBy(r => r.OrderCode).ToArray();
            if(exprot.Any()) {
                excelColumns.Add(ErrorStrings.ExportColumn_PartsSalesOrder_Code);
                excelColumns.Add(ErrorStrings.ExportColumn_SalesOrderStatus);
                excelColumns.Add(ErrorStrings.ExportColumn_OrderApproveTIme);
                excelColumns.Add("发货仓库名称");
                excelColumns.Add("目的地名称");
                excelColumns.Add("出库完成时间");
                excelColumns.Add("发运单编号");
                excelColumns.Add("计划到达时间");
                excelColumns.Add("发运方式");
                excelColumns.Add("承运商名称");
                excelColumns.Add("承运商联系电话");
                excelColumns.Add("签收状态");
                excelColumns.Add("到达站点时间");
                excelColumns.Add("发运方式");
                excelColumns.Add("到达站点名称");
                excelColumns.Add("扫码时间");
                excelColumns.Add("扫码签收人");
                excelColumns.Add(ErrorStrings.ExportColumn_Remark);
                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == exprot.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = exprot[index - 1];
                        var values = new object[] {
                            detail.OrderCode,
                            detail.PartsSalesOrderStatus.HasValue ? Enum.GetName(typeof(DcsPartsSalesOrderStatus), detail.PartsSalesOrderStatus) : "",
                            detail.ApproveTime,
                            detail.WarehouseName, 
                            detail.ReceivingAddress,
                            detail.ShippingDate,
                            detail.ShippingCode,
                            detail.RequestedArrivalDate, 
                            detail.ShippingMethod.HasValue ? Enum.GetName(typeof(DcsPartsShippingMethod), detail.ShippingMethod) : "",
                            detail.LogisticName, 
                            detail.TransportDriverPhone,
                            detail.SignStatus == 2 ? "已签收" : detail.SignStatus == 1 ? "在途" : "",
                            detail.PointTime,
                            detail.DShippingMethod.HasValue ? Enum.GetName(typeof(DcsPartsShippingMethod), detail.DShippingMethod) : "",
                            detail.PointName,
                            detail.ScanTime,
                            detail.ScanName,
                            detail.Remark
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
    }
}