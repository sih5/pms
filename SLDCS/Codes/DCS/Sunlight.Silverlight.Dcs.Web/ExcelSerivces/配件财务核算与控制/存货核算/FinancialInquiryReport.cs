﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string 导出财务查询统计( int? warehouseId) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format(ErrorStrings.Export_Title_FinancialReport, DateTime.Now.ToString("ddHHmmssffff")));
            var data = 财务查询统计(warehouseId);
            var dataArray = data.ToArray();
            if(data.Any()) {
                excelColumns.Add(ErrorStrings.ExportColumn_WarehouseName);
                excelColumns.Add(ErrorStrings.ExportColumn_WarehouseCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SaleOutBound);
                excelColumns.Add(ErrorStrings.ExportColumn_PartsPurchaseReturn);
                excelColumns.Add(ErrorStrings.ExportColumn_Internal);
                excelColumns.Add(ErrorStrings.ExportColumn_PartsPurchaseInBound);
                excelColumns.Add(ErrorStrings.ExportColumn_SaleReturn);             
                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.WarehouseName,
                            detail.WarehouseCode,
                            detail.SaleOutBound,
                            detail.PartsPurchaseReturn, 
                            detail.Internal,
                            detail.PartsPurchaseInBound,
                            detail.SaleReturn
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
        public string 导出内部领用统计(int? warehouseId, string sparePartCode, DateTime? bPartsOutboundBillTime, DateTime? ePartsOutboundBillTime, DateTime? bPartsRequisitionSettleBillDate, DateTime? ePartsRequisitionSettleBillDate) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format(ErrorStrings.Export_Title_InternalAllocationBillReport, DateTime.Now.ToString("ddHHmmssffff")));
            var data = 导出内部领用统计查询(warehouseId, sparePartCode, bPartsOutboundBillTime, ePartsOutboundBillTime, bPartsRequisitionSettleBillDate, ePartsRequisitionSettleBillDate);
            var dataArray = data.ToArray();
            if(data.Any()) {
                excelColumns.Add(ErrorStrings.ExportColumn_WarehouseCode);
                excelColumns.Add(ErrorStrings.ExportColumn_WarehouseName);                
                excelColumns.Add(ErrorStrings.ExportColumn_OutboundTime);
                excelColumns.Add(ErrorStrings.ExportColumn_OrderType);
                excelColumns.Add(ErrorStrings.ExportColumn_CollarType);
                excelColumns.Add(ErrorStrings.ExportColumn_PartCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SparePartName);
                excelColumns.Add(ErrorStrings.ExportColumn_OutboundAmount);
                excelColumns.Add(ErrorStrings.ExportColumn_CostPrice);
                excelColumns.Add(ErrorStrings.ExportColumn_OutboundCostPrice);
                excelColumns.Add(ErrorStrings.ExportColumn_SettleBillCode);
                excelColumns.Add(EntityStrings.FDWarrantyMessage_jiesuanshijian);
                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.WarehouseCode,
                            detail.WarehouseName,
                            detail.PartsOutboundBillTime,
                            detail.OrderType,
                            Enum.GetName(typeof(DcsInternalAllocationBill_Type),detail.Type==null?0:detail.Type),
                            detail.SparePartCode,
                            detail.SparePartName,
                            detail.OutboundAmount,
                            detail.CostPrice,
                            detail.CostPriceAll,
                            detail.PartsRequisitionSettleBillCode,
                            detail.PartsRequisitionSettleBillDate
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
    }
}
