﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string 导出历史库存(int[] ids, int? warehouseId, string partCode, string partName, DateTime? snapshoTime, int? partsSalesCategoryId, int? storageCompanyType, bool? greaterThanZero) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format(ErrorStrings.Export_Title_PartHistoryStock, DateTime.Now.ToString("ddHHmmssffff")));
            var data = GetPartsHistoryStocksWithSpartsParts(greaterThanZero);
            if(ids.Any())
                data = data.Where(r => ids.Contains(r.Id));
            else {
                if(warehouseId.HasValue)
                    data = data.Where(r => r.WarehouseId == warehouseId.Value);
                if(!string.IsNullOrWhiteSpace(partCode))
                    data = data.Where(r => r.SparePart.Code.ToUpper().Contains(partCode.ToUpper()));
                if(!string.IsNullOrWhiteSpace(partName))
                    data = data.Where(r => r.SparePart.Name.ToUpper().Contains(partName.ToUpper()));
                if(snapshoTime.HasValue)
                    data = data.Where(r => r.SnapshoTime == snapshoTime);
                if(partsSalesCategoryId.HasValue)
                    data = data.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId.Value);
                if(storageCompanyType.HasValue)
                    data = data.Where(r => r.StorageCompanyType == storageCompanyType.Value);
            }

            var dataArray = data.ToArray();
            if(data.Any()) {
                excelColumns.AddRange(new[] {
                    ErrorStrings.ExportColumn_BranchName,
					ErrorStrings.ExportColumn_WarehouseName,
					ErrorStrings.ExportColumn_PartsSalesCategory,
					ErrorStrings.ExportColumn_StorageCompanyType,
					ErrorStrings.ExportColumn_StorageCompanyName,
					ErrorStrings.ExportColumn_WarehouseAreaCode,
					ErrorStrings.ExportColumn_WarehouseAreaType,
					ErrorStrings.ExportColumn_PartCode,
					ErrorStrings.ExportColumn_SparePartName,
					ErrorStrings.ExportColumn_Quantity,
					ErrorStrings.ExportColumn_CostPrice2,
					ErrorStrings.ExportColumn_CostAmount,
					ErrorStrings.ExportColumn_SalesPrice,
					ErrorStrings.ExportColumn_SalesAmount,
					ErrorStrings.ExportColumn_CreateTime,
					ErrorStrings.ExportColumn_Creator,
					ErrorStrings.ExportColumn_ModifyTime,
					ErrorStrings.ExportColumn_Modifier
                });
                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.Branch==null?"":detail.Branch.Name,
                            detail.Warehouse != null ? detail.Warehouse.Name:"",
                            detail.PartsSalesCategory !=null?detail.PartsSalesCategory.Name:"",
                            Enum.GetName(typeof(DcsCompanyType),detail.StorageCompanyType),
                            detail.Company.Name,
                            detail.WarehouseArea !=null ? detail.WarehouseArea.Code:"",
                            Enum.GetName(typeof(DcsAreaType),detail.WarehouseAreaCategory.Category),
                            detail.SparePart.Code,
                            detail.SparePart.Name,
                            detail.Quantity,
                            detail.PlannedPrice,
                            detail.PlannedPriceAmount,
                            detail.SalePrice,
                            detail.SaleAmount,
                            detail.CreateTime,
                            detail.CreatorName,
                            detail.ModifierName,
                            detail.ModifyTime
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }

        public string 导出配件历史库存汇总信息(int[] ids, int? warehouseId, int? storageCompanyType, DateTime? snapshoTime, int? partsSalesCategoryId, bool? greaterThanZero) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format(ErrorStrings.Export_Title_PartHistoryStockDetail, DateTime.Now.ToString("ddHHmmssffff")));
            var data = HistoryStockSumSets(greaterThanZero);
            if(ids.Any())
                data = data.Where(r => ids.Contains(r.Id));
            else {
                if(warehouseId.HasValue)
                    data = data.Where(r => r.WarehouseId == warehouseId.Value);
                if(snapshoTime.HasValue)
                    data = data.Where(r => r.SnapshoTime == snapshoTime);
                if(partsSalesCategoryId.HasValue)
                    data = data.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId.Value);
                if(storageCompanyType.HasValue)
                    data = data.Where(r => r.StorageCompanyType == storageCompanyType.Value);
            }

            var dataArray = data.ToArray();
            if(data.Any()) {
                excelColumns.AddRange(new[] {
                    ErrorStrings.ExportColumn_BranchName,
					ErrorStrings.ExportColumn_WarehouseName,
					ErrorStrings.ExportColumn_WarehouseCode,
					ErrorStrings.ExportColumn_PartsSalesCategory,
					ErrorStrings.ExportColumn_StorageCompanyType,
					ErrorStrings.ExportColumn_StorageCompanyName,
					ErrorStrings.ExportColumn_CreateTime,
					ErrorStrings.ExportColumn_CostAmount
                });
                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.BranchName,
                            detail.WarehouseName,
                            detail.WarehouseCode,
                            detail.PartsSalesCategoryName !=null?detail.PartsSalesCategoryName:"",
                            Enum.GetName(typeof(DcsCompanyType),detail.StorageCompanyType),
                            detail.CompanyName,
                            detail.CreateTime,
                            detail.PlannedPriceAmount
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
    }
}



