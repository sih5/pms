﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string 导出资金月结明细(int[] ids,int? fundMonthlySettleBillId,string customerCompanyCode,string customerCompanyName) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format(ErrorStrings.Export_Title_FundMonthlySettleBill, DateTime.Now.ToString("ddHHmmssffff")));
            var data = GetFundMonthlySettleBillDetails();
            if (ids != null && ids.Any()) {
                data = data.Where(r => ids.Contains(r.Id));
            } else {
                if (fundMonthlySettleBillId.HasValue) {
                    data = data.Where(r => r.FundMonthlySettleBillId == fundMonthlySettleBillId);
                }
                if (!string.IsNullOrEmpty(customerCompanyCode)) {
                    data = data.Where(r => r.CustomerCompanyCode.Contains(customerCompanyCode));
                }
                if (!string.IsNullOrEmpty(customerCompanyName)) {
                    data = data.Where(r => r.CustomerCompanyName.Contains(customerCompanyName));
                }
            }
            var dataArray = data.ToArray();
            if(data.Any()) {
                excelColumns.Add(ErrorStrings.ExportColumn_CustomerCompanyCode);
                excelColumns.Add(ErrorStrings.ExportColumn_CustomerCompanyName);
                excelColumns.Add(ErrorStrings.ExportColumn_AccountGroupName);
                excelColumns.Add(ErrorStrings.ExportColumn_InitialAmount);
                excelColumns.Add(ErrorStrings.ExportColumn_ChangeAmount);
                excelColumns.Add(ErrorStrings.ExportColumn_FinalAmount);
                excelColumns.Add(ErrorStrings.ExportColumn_MonthlySettleYear);
                excelColumns.Add(ErrorStrings.ExportColumn_MonthlySettleMonth);    
                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.CustomerCompanyCode,
                            detail.CustomerCompanyName,
                            detail.AccountGroupName,
                            detail.InitialAmount, 
                            detail.ChangeAmount,
                            detail.FinalAmount,
                            detail.MonthlySettleYear,
                            detail.MonthlySettleMonth
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }

    }
}
