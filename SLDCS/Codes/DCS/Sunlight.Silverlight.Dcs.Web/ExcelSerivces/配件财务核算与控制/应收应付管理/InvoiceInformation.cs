﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string ExportInvoiceInformation(int[] ids, int? partssalescategoryId, int? ownerCompanyId, int? invoicePurpose, string invoiceNumber, string sourceCode, string invoiceCompanyCode, string invoiceCompanyName, string invoiceReceiveCompanyCode, string invoiceReceiveCompanyName, DateTime? createTimeStart, DateTime? createTimeEnd, DateTime? invoiceDateStart, DateTime? invoiceDateEnd, string creatorName, int? status) {

            var query = GetInvoiceInformationsByPartsSalesCategoryId(partssalescategoryId);
            if(ids != null && ids.Any()) {
                query = query.Where(r => ids.Contains(r.Id));
            } else {
                if(!string.IsNullOrEmpty(invoiceNumber)) {
                    query = query.Where(r => r.InvoiceNumber.Contains(invoiceNumber));
                }
                if(!string.IsNullOrEmpty(sourceCode)) {
                    query = query.Where(r => r.SourceCode.Contains(sourceCode));
                }
                if(!string.IsNullOrEmpty(invoiceCompanyCode)) {
                    query = query.Where(r => r.InvoiceCompanyCode.Contains(invoiceCompanyCode));
                }
                if(!string.IsNullOrEmpty(invoiceCompanyName)) {
                    query = query.Where(r => r.InvoiceCompanyName.Contains(invoiceCompanyName));
                }
                if(!string.IsNullOrEmpty(invoiceReceiveCompanyCode)) {
                    query = query.Where(r => r.InvoiceReceiveCompanyCode.Contains(invoiceReceiveCompanyCode));
                }
                if(!string.IsNullOrEmpty(invoiceReceiveCompanyName)) {
                    query = query.Where(r => r.InvoiceReceiveCompanyName.Contains(invoiceReceiveCompanyName));
                }
                if(createTimeStart.HasValue) {
                    query = query.Where(r => r.CreateTime >= createTimeStart.Value);
                }
                if(createTimeEnd.HasValue) {
                    query = query.Where(r => r.CreateTime <= createTimeEnd.Value);
                }
                if(invoiceDateStart.HasValue) {
                    query = query.Where(r => r.InvoiceDate >= invoiceDateStart.Value);
                }
                if(invoiceDateEnd.HasValue) {
                    query = query.Where(r => r.InvoiceDate <= invoiceDateEnd.Value);
                }
                if(status.HasValue) {
                    query = query.Where(r => r.Status == status.Value);
                }
                if(ownerCompanyId.HasValue) {
                    query = query.Where(r => r.OwnerCompanyId == ownerCompanyId.Value);
                }
                if(invoicePurpose.HasValue) {
                    query = query.Where(r => r.InvoicePurpose == invoicePurpose.Value);
                }
                if(!string.IsNullOrEmpty(creatorName)) {
                    query = query.Where(r => r.CreatorName.Contains(creatorName));
                }
            }
            var queryReuslt = query.ToArray().Select(r => new object[] {
                r.InvoiceCode,
                r.InvoiceNumber,
                r.InvoiceCompanyCode,
                r.InvoiceCompanyName,
                r.InvoiceReceiveCompanyCode,
                r.InvoiceReceiveCompanyName,
                r.InvoiceAmount,
                r.TaxRate,
                r.InvoiceTax,
                r.InvoiceDate,
                Enum.GetName(typeof(DcsInvoiceInformationSourceType),r.SourceType),
                r.SourceCode,
                Enum.GetName(typeof(DcsInvoiceInformationInvoicePurpose),r.InvoicePurpose),
                Enum.GetName(typeof(DcsInvoiceInformationType),r.Type),
                Enum.GetName(typeof(DcsInvoiceInformationStatus),r.Status),
                r.Code,
                r.CreatorName,
                r.CreateTime
            }).ToArray();

            var excelColumns = new List<string> {
                ErrorStrings.ExportColumn_InvoiceCode,
                ErrorStrings.ExportColumn_InvoiceNumber,
                ErrorStrings.ExportColumn_InvoiceCompanyCode,
                ErrorStrings.ExportColumn_InvoiceCompanyName,
                ErrorStrings.ExportColumn_InvoiceReceiveCompanyCode,
                ErrorStrings.ExportColumn_InvoiceReceiveCompanyName,
                ErrorStrings.ExportColumn_InvoiceFee,
                ErrorStrings.ExportColumn_TaxRate,
                ErrorStrings.ExportColumn_InvoiceTaxFee,
                ErrorStrings.ExportColumn_InvoiceDate,
                ErrorStrings.ExportColumn_SourceType,
                ErrorStrings.ExportColumn_SourceCode,
                ErrorStrings.ExportColumn_InvoicePurpose,
                ErrorStrings.ExportColumn_InvoiceInformationType,
                ErrorStrings.ExportColumn_Status,
                ErrorStrings.ExportColumn_OrderCode,
                ErrorStrings.ExportColumn_Creator,
                ErrorStrings.ExportColumn_CreateTime
            };
            return ExportAllData(queryReuslt, excelColumns, "配件销售发票");
        }

        public string ExportInvoiceInformationOfPurchase(int[] ids, int? partssalescategoryId, int? ownerCompanyId, int? invoicePurpose, string invoiceNumber, string sourceCode, string invoiceCompanyCode, string invoiceCompanyName, int? invoiceReceiveCompanyId, int? type, DateTime? createTimeStart, DateTime? createTimeEnd, DateTime? approveTimeStart, DateTime? approveTimeEnd, int? status)
        {


            var query = from a in ObjectContext.InvoiceInformations.Where(r => r.SourceType == (int)DcsInvoiceInformationSourceType.配件采购退货结算单 || r.SourceType == (int)DcsInvoiceInformationSourceType.配件采购结算单)
                        join c in ObjectContext.PartsSalesCategories.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on a.PartsSalesCategoryId equals c.Id into tempTable
                        from t in tempTable.DefaultIfEmpty()
                        select new {
                            a.Id,
                            a.InvoiceCode,
                            a.InvoiceNumber,
                            a.PartsSalesCategoryId,
                            PartsSalesCategoryName = t.Name,
                            a.InvoiceCompanyCode,
                            a.InvoiceCompanyName,
                            a.InvoiceReceiveCompanyCode,
                            a.InvoiceReceiveCompanyName,
                            a.InvoiceAmount,
                            a.TaxRate,
                            a.InvoiceTax,
                            a.InvoiceDate,
                            a.SourceType,
                            a.SourceCode,
                            a.OwnerCompanyId,
                            a.InvoicePurpose,
                            a.Type,
                            a.Status,
                            a.Code,
                            a.CreatorName,
                            a.CreateTime,
                            a.ApproverName,
                            a.ApproveTime,
                            a.InvoiceCompanyId

                        };
            
            if(ids != null && ids.Any()) {
                query = query.Where(r => ids.Contains(r.Id));
            } else {
                if(partssalescategoryId.HasValue) {
                    query = query.Where(r => r.PartsSalesCategoryId == partssalescategoryId.Value);
                }
                if(!string.IsNullOrEmpty(invoiceNumber)) {
                    query = query.Where(r => r.InvoiceNumber.Contains(invoiceNumber));
                }
                if(!string.IsNullOrEmpty(sourceCode)) {
                    query = query.Where(r => r.SourceCode.Contains(sourceCode));
                }
                if(!string.IsNullOrEmpty(invoiceCompanyCode)) {
                    query = query.Where(r => r.InvoiceCompanyCode.Contains(invoiceCompanyCode));
                }
                if(!string.IsNullOrEmpty(invoiceCompanyName)) {
                    query = query.Where(r => r.InvoiceCompanyName.Contains(invoiceCompanyName));
                }
                if(type.HasValue) {
                    query = query.Where(r => r.Type == type.Value);
                }
                if(invoiceReceiveCompanyId.HasValue) {
                    query = query.Where(r => r.InvoiceCompanyId == invoiceReceiveCompanyId.Value);
                }
                if(createTimeStart.HasValue) {
                    query = query.Where(r => r.CreateTime >= createTimeStart.Value);
                }
                if(createTimeEnd.HasValue) {
                    query = query.Where(r => r.CreateTime <= createTimeEnd.Value);
                }
                if (approveTimeStart.HasValue)
                {
                    query = query.Where(r => r.ApproveTime >= approveTimeStart.Value);
                }
                if (approveTimeEnd.HasValue)
                {
                    query = query.Where(r => r.ApproveTime <= approveTimeEnd.Value);
                }

                if(status.HasValue) {
                    query = query.Where(r => r.Status == status.Value);
                }
                if(ownerCompanyId.HasValue) {
                    query = query.Where(r => r.OwnerCompanyId == ownerCompanyId.Value);
                }
                if(invoicePurpose.HasValue) {
                    query = query.Where(r => r.InvoicePurpose == invoicePurpose.Value);
                }

            }
            var queryReuslt = query.OrderByDescending(r=>r.ApproveTime).ToArray().Select(r => new object[] {
                r.InvoiceCode,
                r.InvoiceNumber,
                r.PartsSalesCategoryName,
                r.InvoiceCompanyCode,
                r.InvoiceCompanyName,
                r.InvoiceReceiveCompanyCode,
                r.InvoiceReceiveCompanyName,
                r.InvoiceAmount,
                r.TaxRate,
                r.InvoiceTax,
                r.InvoiceDate,
                Enum.GetName(typeof(DcsInvoiceInformationSourceType),r.SourceType),
                r.SourceCode,
                Enum.GetName(typeof(DcsInvoiceInformationInvoicePurpose),r.InvoicePurpose),
                Enum.GetName(typeof(DcsInvoiceInformationType),r.Type),
                Enum.GetName(typeof(DcsInvoiceInformationStatus),r.Status),
                r.Code,
                r.CreatorName,
                r.CreateTime,
                r.ApproverName,
                r.ApproveTime
            }).ToArray();
            var excelColumns = new List<string> {
                ErrorStrings.ExportColumn_InvoiceCode,
                ErrorStrings.ExportColumn_InvoiceNumber,
                ErrorStrings.ExportColumn_PartsSalesCategory,
                ErrorStrings.ExportColumn_InvoiceCompanyCode,
                ErrorStrings.ExportColumn_InvoiceCompanyName,
                ErrorStrings.ExportColumn_InvoiceReceiveCompanyCode,
                ErrorStrings.ExportColumn_InvoiceReceiveCompanyName,
                ErrorStrings.ExportColumn_InvoiceFee,
                ErrorStrings.ExportColumn_TaxRate,
                ErrorStrings.ExportColumn_InvoiceTaxFee,
                ErrorStrings.ExportColumn_InvoiceDate,
                ErrorStrings.ExportColumn_SourceType,
                ErrorStrings.ExportColumn_SourceCode,
                ErrorStrings.ExportColumn_InvoicePurpose,
                ErrorStrings.ExportColumn_InvoiceInformationType,
                ErrorStrings.ExportColumn_Status,
                ErrorStrings.ExportColumn_OrderCode,
                ErrorStrings.ExportColumn_Creator,
                ErrorStrings.ExportColumn_CreateTime,
                ErrorStrings.ExportColumn_InvoiceApproverName,
                ErrorStrings.ExportColumn_InvoiceApproveTime
            };
            return ExportAllData(queryReuslt, excelColumns, "配件采购发票");
        }

    }
}
