﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public IEnumerable<PartsPurchaseOrderDetail> 查询待导入配件采购订单清单(string fileName) {
            var enterpriseCode = this.GetCurrentUserInfo().EnterpriseCode ?? string.Empty;
            var filePath = Path.Combine(this.RootDir, enterpriseCode, fileName);

            //获取指定实体结构
            List<string> notNullableFields;
            Dictionary<string, int> fieldLenght;
            this.GetEntitySchema(this.ObjectContext.PartsPurchaseOrderDetails.EntitySet, out notNullableFields, out fieldLenght);

            var details = new List<PartsPurchaseOrderDetail>();
            var errorDetails = new List<PartsPurchaseOrderDetail>();
            using(var excelOperator = new ExcelImport(filePath)) {
                excelOperator.AddColumnDataSource("配件编号", "SparePartCode");
                excelOperator.AddColumnDataSource("配件名称", "SparePartName");
                excelOperator.AddColumnDataSource("单价", "UnitPrice");
                excelOperator.AddColumnDataSource("订货量", "OrderAmount");

                //判断不允许为空的表字段是否存在对应值
                foreach(var field in notNullableFields) {
                    if(field == "ID" || field == "STATUS")
                        continue;
                    if(excelOperator.IndexOfFieldName(field) == -1)
                        throw new Exception(string.Format("不允许为空的字段“{0}”缺少映射值", field));
                }

                excelOperator.LoadExcelRow(row => {
                    int unitPrice;
                    int orderAmount;
                    int.TryParse(row["UnitPrice"], out unitPrice);
                    int.TryParse(row["OrderAmount"], out orderAmount);
                    var detail = new PartsPurchaseOrderDetail {
                        SparePartCode = row["SparePartCode"],
                        SparePartName = row["SparePartName"],
                        UnitPrice = unitPrice,
                        OrderAmount = orderAmount
                    };

                    var errorMsgs = new List<string>();
                    //配件编号检查
                    var fieldIndex = notNullableFields.IndexOf("SPAREPARTCODE");
                    if(string.IsNullOrEmpty(detail.SparePartCode)) {
                        if(fieldIndex > -1)
                            errorMsgs.Add("配件编号不允许为空");
                    } else {
                        if(Encoding.Default.GetByteCount(detail.SparePartCode) > fieldLenght["SPAREPARTCODE"])
                            errorMsgs.Add("配件编号超长");
                    }

                    //区分错误数据和合法数据，如果不存在其他校验，此处可直接导入数据库
                    if(errorMsgs.Count > 0) {
                        //错误信息字段赋值，此处实体没有实现字段扩展，故注释
                        //detail.ErrorMsg = string.Join("; ", errorMsgs);
                        errorDetails.Add(detail);
                    } else
                        details.Add(detail);

                    //是否停止Excel导入
                    return false;
                });

                var sparePartCodes = details.Select(v => v.SparePartCode.ToLower()).Distinct().ToArray();
                var spareParts = this.ObjectContext.SpareParts.Where(v => sparePartCodes.Contains(v.Code.ToLower()) && v.Status == (int)DcsMasterDataStatus.有效).Select(v => new {
                    v.Id,
                    v.Code
                }).ToArray();

                for(var i = details.Count - 1; i >= 0; i--) {
                    var sparePart = spareParts.SingleOrDefault(v => System.String.Compare(v.Code, details[i].SparePartCode, StringComparison.OrdinalIgnoreCase) == 0);
                    if(sparePart == null) {
                        details.RemoveAt(i);
                        errorDetails.Add(details[i]);
                    } else {
                        details[i].SparePartId = sparePart.Id;
                        details[i].Id = i;
                    }
                }
            }

            return details;
        }
    }
}
