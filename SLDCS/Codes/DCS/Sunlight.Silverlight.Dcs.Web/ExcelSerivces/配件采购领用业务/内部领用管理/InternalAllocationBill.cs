﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string ExportInternalAllocationBill(int[] ids, int? warehouseId, string code, string departmentName, int? status,DateTime? createTimeBegin,DateTime? createTimeEnd,int? type)
        {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("内部领出单_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var internalAllocationBill = GetInternalAllocationBills();

            var userinfo=Utils.GetCurrentUserInfo();

            internalAllocationBill = internalAllocationBill.Where(r => r.BranchId == userinfo.EnterpriseId);
            if (ids != null && ids.Any()) {
                internalAllocationBill = internalAllocationBill.Where(r => ids.Contains(r.Id));
            }
            else {
                if (warehouseId.HasValue){
                    internalAllocationBill = internalAllocationBill.Where(r => r.WarehouseId == warehouseId);
                }
                if (!string.IsNullOrEmpty(code)) {
                    internalAllocationBill = internalAllocationBill.Where(r => r.Code.Contains(code));
                }
                if (!string.IsNullOrEmpty(departmentName)) {
                    internalAllocationBill = internalAllocationBill.Where(r => r.DepartmentName.Contains(departmentName));
                }
                if (status.HasValue) {
                    internalAllocationBill = internalAllocationBill.Where(r => r.Status == status.Value);
                }
                if (createTimeBegin.HasValue) {
                    internalAllocationBill = internalAllocationBill.Where(r => r.CreateTime >= createTimeBegin.Value);
                }
                if (createTimeEnd.HasValue) {
                    internalAllocationBill = internalAllocationBill.Where(r => r.CreateTime <= createTimeEnd.Value);
                }
                if(type.HasValue) {
                    internalAllocationBill = internalAllocationBill.Where(r => r.Type == type.Value);
                }
            }

            var internalAllocationBillArray = internalAllocationBill.ToArray();
            if (internalAllocationBill.Any()) {
                excelColumns.Add(ErrorStrings.ExportColumn_InternalAllocationBillCode);
                excelColumns.Add(ErrorStrings.ExportColumn_Branch);
                excelColumns.Add(ErrorStrings.ExportColumn_InternalAllocationWarehouse);
                excelColumns.Add(ErrorStrings.ExportColumn_InternalAllocationDepartment);
                excelColumns.Add(ErrorStrings.ExportColumn_TotalAmount);
                excelColumns.Add(ErrorStrings.ExportColumn_InternalAllocationReason);
                excelColumns.Add(ErrorStrings.ExportColumn_Operator);
                excelColumns.Add(ErrorStrings.ExportColumn_Status);
                excelColumns.Add(ErrorStrings.ExportColumn_Type);
                excelColumns.Add(ErrorStrings.ExportColumn_SourceCode);
                excelColumns.Add(ErrorStrings.ExportColumn_Remark);
                excelColumns.Add(ErrorStrings.ExportColumn_Creator);
                excelColumns.Add(ErrorStrings.ExportColumn_CreateTime);
                excelColumns.Add(ErrorStrings.ExportColumn_Modifier);
                excelColumns.Add(ErrorStrings.ExportColumn_ModifyTime);
                excelColumns.Add(ErrorStrings.ExportColumn_Confirmor);
                excelColumns.Add(ErrorStrings.ExportColumn_ConfirmTime);
                excelColumns.Add(ErrorStrings.ExportColumn_Abandoner);
                excelColumns.Add(ErrorStrings.ExportColumn_AbandonTime);

                using (var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index =>
                    {
                        if (index == internalAllocationBillArray.Count() + 1)
                            return null;
                        if (index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = internalAllocationBillArray[index - 1];
                        var values = new object[] {
                            detail.Code,
                            detail.BranchName,
                            detail.WarehouseName,
                            detail.DepartmentName,
                            detail.TotalAmount,
                            detail.Reason,
                            detail.Operator,
                            Enum.GetName(typeof(DcsWorkflowOfSimpleApprovalStatus),detail.Status),
                            Enum.GetName(typeof(DcsInternalAllocationBill_Type),detail.Type),
                            detail.SourceCode,
                            detail.Remark,
                            detail.CreatorName,
                            detail.CreateTime,
                            detail.ModifierName,
                            detail.ModifyTime,
                            detail.ApproverName,
                            detail.ApproveTime,
                            detail.AbandonerName,
                            detail.AbandonTime
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
    }
}
