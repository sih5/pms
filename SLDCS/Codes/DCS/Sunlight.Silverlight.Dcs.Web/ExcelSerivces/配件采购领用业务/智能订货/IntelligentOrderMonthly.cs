﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string ExportIntelligentOrderMonthly(int[] ids, int? salesCategoryId, string partCode, string partName, int? partABC, int? productLifeCycle, int? plannedType, string orderTypeName) {
            var exportDataFileName = GetExportFilePath(string.Format("月度累计数据查询_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var intelligentOrderMonthlys = GetIntelligentOrderMonthlies();

            if(ids != null && ids.Any()) {
                intelligentOrderMonthlys = intelligentOrderMonthlys.Where(r => ids.Contains(r.Id));
            } else {
                if(salesCategoryId.HasValue) {
                    intelligentOrderMonthlys = intelligentOrderMonthlys.Where(r => r.SALESCATEGORYID == salesCategoryId.Value);
                }
                if(!string.IsNullOrEmpty(partCode)) {
                    intelligentOrderMonthlys = intelligentOrderMonthlys.Where(r => r.PARTCODE.Contains(partCode));
                }
                if(!string.IsNullOrEmpty(partName)){
                    intelligentOrderMonthlys = intelligentOrderMonthlys.Where(r => r.PARTNAME.Contains(partName));
                }
                if(partABC.HasValue) {
                    intelligentOrderMonthlys = intelligentOrderMonthlys.Where(r => r.PARTABC == partABC.Value);
                }
                if(productLifeCycle.HasValue){
                    intelligentOrderMonthlys = intelligentOrderMonthlys.Where(r => r.PRODUCTLIFECYCLE == productLifeCycle.Value);
                }
                if(plannedType.HasValue) {
                    intelligentOrderMonthlys = intelligentOrderMonthlys.Where(r => r.PLANNEDPRICETYPE == plannedType.Value);
                }
                if(!string.IsNullOrEmpty(orderTypeName)) {
                    intelligentOrderMonthlys = intelligentOrderMonthlys.Where(r => r.ORDERTYPENAME.Contains(orderTypeName));
                }
            }


            var queryResult = intelligentOrderMonthlys.ToArray().Select(r => new object[]{
                r.PARTCODE,r.PARTNAME,r.SALESCATEGORYCODE,r.SALESCATEGORYNAME,r.ORDERTYPENAME,r.SUPPLIERCODE,r.SUPPLIERNAME,
                r.PARTABC != null?Enum.GetName(typeof(DcsABCStrategyCategory),r.PARTABC):string.Empty,
                r.PRODUCTLIFECYCLE!=null?Enum.GetName(typeof(DcsPartsBranchProductLifeCycle),r.PRODUCTLIFECYCLE):string.Empty,
                r.PLANNEDPRICETYPE!=null?Enum.GetName(typeof(DcsPlanPriceCategory_PriceCategory),r.PLANNEDPRICETYPE):string.Empty,
                r.ORDERQUANTITY1,r.ORDERQUANTITY2,r.ORDERQUANTITY3,r.ORDERQUANTITY4,r.ORDERQUANTITY5,
                r.ORDERQUANTITY6,r.ORDERQUANTITY7,r.ORDERQUANTITY8,r.ORDERQUANTITY9,r.ORDERQUANTITY10,r.ORDERQUANTITY11,r.ORDERQUANTITY12,
                r.FREQUENCY1,r.FREQUENCY2,r.FREQUENCY3,r.FREQUENCY4,r.FREQUENCY5,r.FREQUENCY6,r.FREQUENCY7,r.FREQUENCY8,r.FREQUENCY9,r.FREQUENCY10,
                r.FREQUENCY11,r.FREQUENCY12,r.FREQUENCYFOR12M,r.ORDERQUANTITYFOR3M,r.ORDERQUANTITYFOR6M,r.ORDERQUANTITYFOR12M,
                r.MONTHAVGFOR3M,r.MONTHAVGFOR6M,r.MONTHAVGFOR12M,r.DAYAVGFOR1M,r.DAYAVGFOR3M,r.DAYAVGFOR6M,r.DAYAVGFOR12M
            
            }).ToArray();

            var excelColumns = new List<string> {
                "配件图号","配件名称","品牌编号","品牌名称","分组类别","供应商编号","供应商名称","配件ABC分类",
                "产品生命周期","计划价分类","T-1月销量","T-2月销量","T-3月销量","T-4月销量","T-5月销量","T-6月销量","T-7月销量",
                "T-8月销量","T-9月销量","T-10月销量","T-11月销量","T-12月销量","T-1月频次","T-2月频次","T-3月频次","T-4月频次","T-5月频次",
                "T-6月频次","T-7月频次","T-8月频次","T-9月频次","T-10月频次","T-11月频次","T-12月频次","近12月频次","近3个月销量","近6个月销量","近12个月销量",
                "近3个月月均","近6个月月均","近12个月月均","近1个月日均","近3个月日均","近6个月日均","近12个月日均"
            };

            return ExportAllData(queryResult, excelColumns, "月度累计数据查询");
        }
    }
}
