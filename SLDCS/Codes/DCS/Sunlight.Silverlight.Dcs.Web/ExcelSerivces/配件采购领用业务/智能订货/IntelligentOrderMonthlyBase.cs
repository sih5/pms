﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string ExportIntelligentOrderMonthlyBase(int[] ids, int? salesCategoryId, string partCode, string partName, int? orderTypeId, string occurMonth, string supplierCode, string supplierName, int? startTotalMonthlyFrequency, int? endTotalMonthlyFrequency) {
            var exportDataFileName = GetExportFilePath(string.Format("月度基础数据查询_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            //var intelligentOrderMonthlyBases = GetIntelligentOrderMonthlyBases();
            var intelligentOrderMonthlyBases = GetIntelligentOrderMonthlyBasesByFrquency(startTotalMonthlyFrequency, endTotalMonthlyFrequency);

            if(ids != null && ids.Any()) {
                intelligentOrderMonthlyBases = intelligentOrderMonthlyBases.Where(r => ids.Contains(r.Id));
            } else {
                if(salesCategoryId.HasValue) {
                    intelligentOrderMonthlyBases = intelligentOrderMonthlyBases.Where(r => r.SalesCategoryId == salesCategoryId.Value);
                }
                if(!string.IsNullOrEmpty(partCode)) {
                    intelligentOrderMonthlyBases = intelligentOrderMonthlyBases.Where(r => r.PartCode.Contains(partCode));
                }
                if(!string.IsNullOrEmpty(partName)) {
                    intelligentOrderMonthlyBases = intelligentOrderMonthlyBases.Where(r => r.PartName.Contains(partName));
                }
                if(orderTypeId.HasValue) {
                    intelligentOrderMonthlyBases = intelligentOrderMonthlyBases.Where(r => r.OrderTypeId == orderTypeId.Value);
                }
                if(!string.IsNullOrEmpty(occurMonth)) {
                    intelligentOrderMonthlyBases = intelligentOrderMonthlyBases.Where(r => r.OccurMonth.Contains(occurMonth));
                }
                if(!string.IsNullOrEmpty(supplierCode)) {
                    intelligentOrderMonthlyBases = intelligentOrderMonthlyBases.Where(r => r.SupplierCode.Contains(supplierCode));
                }
                if(!string.IsNullOrEmpty(supplierName)) {
                    intelligentOrderMonthlyBases = intelligentOrderMonthlyBases.Where(r => r.SupplierName.Contains(supplierName));

                }
                if(startTotalMonthlyFrequency.HasValue) {
                    intelligentOrderMonthlyBases = intelligentOrderMonthlyBases.Where(r => r.TotalMonthlyFrequency >= startTotalMonthlyFrequency.Value);
                }
                if(endTotalMonthlyFrequency.HasValue){
                intelligentOrderMonthlyBases = intelligentOrderMonthlyBases.Where(r => r.TotalMonthlyFrequency <= endTotalMonthlyFrequency.Value);
                }
            }

            var queryResult = intelligentOrderMonthlyBases.ToArray().Select(r => new object[]{
              r.BranchCode,
              r.BranchName,
              r.OrderTypeName,
              r.IfDirectProvision==true?"是":"否",
              r.SalesCategoryCode,
              r.SalesCategoryName,
              r.PartCode,
              r.PartName,
              r.BusinessType != null?Enum.GetName(typeof(DcsIntelligentOrderMonthlyBaseBusinesstype),r.BusinessType):string.Empty,
              r.SupplierCode,
              r.SupplierName,
              r.PartABC!=null?Enum.GetName(typeof(DcsABCStrategyCategory),r.PartABC):string.Empty,
              r.ProductLifeCycle !=null?Enum.GetName(typeof(DcsPartsBranchProductLifeCycle),r.ProductLifeCycle):string.Empty,
              r.PartsBranchCreateTime,
              r.PlannedPrice,
              r.PlannedPriceType!=null?Enum.GetName(typeof(DcsPlanPriceCategory_PriceCategory),r.PlannedPriceType):string.Empty,
              r.StockQuantity,
              r.StockAmount,
              r.TotalQuantity,
              r.TotalMonthlyFrequency,
              r.OccurMonth
            }).ToArray();

            var excelColumns = new List<string> {
                "分公司编号","分公司名称","订单类型名称","是否直供","品牌编号","品牌名称","配件图号","配件名称",
                "业务类型","供应商编号","供应商名称","配件ABC分类","产品生命周期","分品牌创建时间","计划价","计划价分类",
                "库存数量","库存金额","数量合计","当月频次合计","发生月份"
            };

            return ExportAllData(queryResult, excelColumns, "月度基础数据查询");
        }
    }
}
