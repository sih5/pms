﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string ExportIntelligentOrderWeekly(int[] ids, int? salesCategoryId, string partCode, string partName, int? partABC, int? productLifeCycle, int? plannedPriceType, string orderTypeName) {
            var exportDataFileName = GetExportFilePath(string.Format("周度累计数据查询_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var intelligentOrderWeeklys = GetIntelligentOrderWeeklies();

            if(ids != null && ids.Any()) {
                intelligentOrderWeeklys = intelligentOrderWeeklys.Where(r => ids.Contains(r.Id));
            } else {
                if(salesCategoryId.HasValue) {
                    intelligentOrderWeeklys = intelligentOrderWeeklys.Where(r => r.SALESCATEGORYID == salesCategoryId.Value);
                }
                if(!string.IsNullOrEmpty(partCode)) {
                    intelligentOrderWeeklys = intelligentOrderWeeklys.Where(r => r.PARTCODE.Contains(partCode));
                }
                if(!string.IsNullOrEmpty(partName)) {
                    intelligentOrderWeeklys = intelligentOrderWeeklys.Where(r => r.PARTNAME.Contains(partName));
                }
                if(partABC.HasValue){
                    intelligentOrderWeeklys = intelligentOrderWeeklys.Where(r => r.PARTABC == partABC.Value);
                }
                if(productLifeCycle.HasValue) {
                    intelligentOrderWeeklys = intelligentOrderWeeklys.Where(r => r.PRODUCTLIFECYCLE == productLifeCycle.Value);
                }
                if(plannedPriceType.HasValue) {
                    intelligentOrderWeeklys = intelligentOrderWeeklys.Where(r => r.PLANNEDPRICETYPE == plannedPriceType.Value);
                }
                if(!string.IsNullOrEmpty(orderTypeName)) {
                    intelligentOrderWeeklys = intelligentOrderWeeklys.Where(r => r.ORDERTYPENAME.Contains(orderTypeName));
                }
            }

            var queryResult = intelligentOrderWeeklys.ToArray().Select(r => new object[]{
               r.BRANCHCODE,r.BRANCHNAME,r.SALESCATEGORYCODE,r.SALESCATEGORYNAME,r.PARTCODE,r.PARTNAME,r.ORDERTYPENAME,r.SUPPLIERCODE,r.SUPPLIERNAME,
               r.PARTABC != null?Enum.GetName(typeof(DcsABCStrategyCategory),r.PARTABC):string.Empty,
               r.PRODUCTLIFECYCLE != null?Enum.GetName(typeof(DcsPartsBranchProductLifeCycle),r.PRODUCTLIFECYCLE):string.Empty,
               r.PLANNEDPRICETYPE != null?Enum.GetName(typeof(DcsPlanPriceCategory_PriceCategory),r.PLANNEDPRICETYPE):string.Empty,
               r.AVG,r.DEVIATION,
               r.ORDERQUANTITY1,r.ORDERQUANTITY2,r.ORDERQUANTITY3,r.ORDERQUANTITY3,r.ORDERQUANTITY4,r.ORDERQUANTITY5,r.ORDERQUANTITY6,r.ORDERQUANTITY7,
               r.ORDERQUANTITY8,r.ORDERQUANTITY9,r.ORDERQUANTITY10,r.ORDERQUANTITY11,r.ORDERQUANTITY12,r.ORDERQUANTITY13,r.ORDERQUANTITY14,
               r.ORDERQUANTITY15,r.ORDERQUANTITY16,r.ORDERQUANTITY17,r.ORDERQUANTITY18,r.ORDERQUANTITY19,r.ORDERQUANTITY20,r.ORDERQUANTITY21,r.ORDERQUANTITY22,
               r.ORDERQUANTITY23,r.ORDERQUANTITY24,r.ORDERQUANTITY25,r.ORDERQUANTITY26,r.ORDERQUANTITY27,r.ORDERQUANTITY28,r.ORDERQUANTITY29,
               r.ORDERQUANTITY30,r.ORDERQUANTITY31,r.ORDERQUANTITY32,r.ORDERQUANTITY33,r.ORDERQUANTITY34,r.ORDERQUANTITY35,r.ORDERQUANTITY36,
               r.ORDERQUANTITY37,r.ORDERQUANTITY38,r.ORDERQUANTITY39,r.ORDERQUANTITY40,r.ORDERQUANTITY41,r.ORDERQUANTITY42,r.ORDERQUANTITY43,
               r.ORDERQUANTITY44,r.ORDERQUANTITY45,r.ORDERQUANTITY46,r.ORDERQUANTITY47,r.ORDERQUANTITY48,r.ORDERQUANTITY49,r.ORDERQUANTITY50,
               r.ORDERQUANTITY51,r.ORDERQUANTITY52
            
            }).ToArray();

            var excelColumns = new List<string>{
            "分公司编号","分公司名称","品牌编号","品牌名称","配件图号","配件名称","分组类别","供应商编号","供应商名称","配件ABC分类","产品生命周期","计划价分类",
            "近52周均","近52周需求标准差", 
            "T-1周销量","T-2周销量","T-3周销量","T-4周销量","T-5周销量","T-6周销量","T-7周销量",
            "T-8周销量","T-9周销量","T-10周销量","T-11周销量","T-12周销量","T-13周销量","T-14周销量",
            "T-15周销量","T-16周销量","T-17周销量","T-18周销量","T-19周销量","T-20周销量","T-21周销量","T-22周销量",
            "T-23周销量","T-24周销量","T-25周销量","T-26周销量","T-27周销量","T-28周销量","T-29周销量",
            "T-30周销量","T-31周销量","T-32周销量","T-33周销量","T-34周销量","T-35周销量","T-36周销量",
            "T-37周销量","T-38周销量","T-39周销量","T-40周销量","T-41周销量","T-42周销量","T-43周销量",
            "T-44周销量","T-45周销量","T-46周销量","T-47周销量","T-48周销量","T-49周销量","T-50周销量",
            "T-51周销量","T-52周销量"


            };

            return ExportAllData(queryResult,excelColumns,"周度累计数据查询");
        }
    }
}
