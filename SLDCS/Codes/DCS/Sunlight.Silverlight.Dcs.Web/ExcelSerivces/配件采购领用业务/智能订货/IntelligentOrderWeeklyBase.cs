﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string ExportIntelligentOrderWeeklyBase(int[] ids, int? saleCategoryId, string partCode, string partName, int? orderTypeId, int? partABC, int? productLifeCycle, int? yearD, int? startWeeklyD,int? endWeeklyD) {
            var exportDataFileName = GetExportFilePath(string.Format("周度基础数据查询_{0}.xlsx", DateTime.Now.ToString("ddHHmssffff")));
            //var intelligentOrderWeeklyBases = GetIntelligentOrderWeeklyBases();
            var intelligentOrderWeeklyBases = GetIntelligentOrderWeeklyBaseByWeeks(yearD, startWeeklyD, endWeeklyD);
            if(ids != null && ids.Any()) {
                intelligentOrderWeeklyBases = intelligentOrderWeeklyBases.Where(r => ids.Contains(r.Id));
            } else {
                if(saleCategoryId.HasValue) {
                    intelligentOrderWeeklyBases = intelligentOrderWeeklyBases.Where(r => r.SalesCategoryId == saleCategoryId.Value);
                }
                if(!string.IsNullOrEmpty(partCode)) {
                    intelligentOrderWeeklyBases = intelligentOrderWeeklyBases.Where(r => r.PartCode.Contains(partCode));
                }
                if(!string.IsNullOrEmpty(partName)) {
                    intelligentOrderWeeklyBases = intelligentOrderWeeklyBases.Where(r => r.PartName.Contains(partName));
                }
                if(orderTypeId.HasValue) {
                    intelligentOrderWeeklyBases = intelligentOrderWeeklyBases.Where(r => r.OrderTypeId == orderTypeId.Value);
                }
                if(partABC.HasValue) {
                    intelligentOrderWeeklyBases = intelligentOrderWeeklyBases.Where(r => r.PartABC == partABC.Value);
                }
                if(productLifeCycle.HasValue) {
                    intelligentOrderWeeklyBases = intelligentOrderWeeklyBases.Where(r => r.ProductLifeCycle == productLifeCycle.Value);
                }
                if(yearD.HasValue) {
                    intelligentOrderWeeklyBases = intelligentOrderWeeklyBases.Where(r => r.YearD == yearD.Value);
                }
                //if(weeklyD.HasValue) {
                //    intelligentOrderWeeklyBases = intelligentOrderWeeklyBases.Where(r => r.WeeklyD == weeklyD.Value);
                //}
                if(startWeeklyD.HasValue) {
                    intelligentOrderWeeklyBases = intelligentOrderWeeklyBases.Where(r => r.WeeklyD >= startWeeklyD.Value);
                }
                if(endWeeklyD.HasValue) {
                    intelligentOrderWeeklyBases = intelligentOrderWeeklyBases.Where(r => r.WeeklyD <= endWeeklyD.Value);
                }
            }

            var queryResult = intelligentOrderWeeklyBases.ToArray().Select(r => new object[] { 
            r.BranchCode,r.BranchName,r.OrderTypeName,r.IfDirectProvision,
              r.SalesCategoryCode,
              r.SalesCategoryName,
              r.PartCode,
              r.PartName,
              r.BusinessType != null?Enum.GetName(typeof(DcsIntelligentOrderMonthlyBaseBusinesstype),r.BusinessType):string.Empty,
              r.SupplierCode,
              r.SupplierName,
              r.PartABC!=null?Enum.GetName(typeof(DcsABCStrategyCategory),r.PartABC):string.Empty,
              r.ProductLifeCycle !=null?Enum.GetName(typeof(DcsPartsBranchProductLifeCycle),r.ProductLifeCycle):string.Empty,
              r.PartsBranchCreateTime,
              r.PlannedPrice,
              r.PlannedPriceType!=null?Enum.GetName(typeof(DcsPlanPriceCategory_PriceCategory),r.PlannedPriceType):string.Empty,
              r.StockQuantity,
              r.StockAmount,
              r.TotalQuantity,
              r.TotalWeeklyFrequency,
              r.YearD,
              r.WeeklyD
            }).ToArray();

            var excelColumns = new List<string> {
                 "分公司编号","分公司名称","订单类型名称","是否直供","品牌编号","品牌名称","配件图号","配件名称",
                "业务类型","供应商编号","供应商名称","配件ABC分类","产品生命周期","分品牌创建时间","计划价","计划价分类",
                "库存数量","库存金额","数量合计","周频次合计","年度","周度"
            };

            return ExportAllData(queryResult, excelColumns, "周度基础数据查询");
        }

    }
}
