﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string ExportPartNotConPurchasePlan(int? partsSalesCategoryId, string partCode, string supplierPartCode, string partName, int? partABC, int? productLifeCycle, int? plannedPriceCategory, bool? egAdvicePurchase, bool? isMeetSupplementPlan, bool? isMeetEmergencyPlan) {
            var exportDataFileName = GetExportFilePath(string.Format("配件非常规采购计划_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var userInfo = Utils.GetCurrentUserInfo();
            var partNotConPurchasePlan = ObjectContext.PartNotConPurchasePlans.Where(r => r.BranchId == userInfo.EnterpriseId);
            //if(ids != null && ids.Any()) {
            //    partNotConPurchasePlan = partNotConPurchasePlan.Where(r => ids.Contains(r.id
            //} else {}
            if(partsSalesCategoryId.HasValue) {
                partNotConPurchasePlan = partNotConPurchasePlan.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId.Value);
            }
            if(!string.IsNullOrEmpty(partCode)) {
                partNotConPurchasePlan = partNotConPurchasePlan.Where(r => r.PartCode.Contains(partCode));
            }
            if(!string.IsNullOrEmpty(partName)) {
                partNotConPurchasePlan = partNotConPurchasePlan.Where(r => r.PartName.Contains(partName));
            }
            if(!string.IsNullOrEmpty(supplierPartCode)) {
                partNotConPurchasePlan = partNotConPurchasePlan.Where(r => r.SupplierPartCode.Contains(supplierPartCode));
            }
            if(partABC.HasValue) {
                partNotConPurchasePlan = partNotConPurchasePlan.Where(r => r.PartABC == partABC.Value);
            }
            if(productLifeCycle.HasValue) {
                partNotConPurchasePlan = partNotConPurchasePlan.Where(r => r.ProductLifeCycle == productLifeCycle.Value);
            }
            if(plannedPriceCategory.HasValue) {
                partNotConPurchasePlan = partNotConPurchasePlan.Where(r => r.PlannedPriceCategory == partsSalesCategoryId.Value);
            }
            if(isMeetSupplementPlan.HasValue) {
                partNotConPurchasePlan = partNotConPurchasePlan.Where(r => r.IsMeetSupplementPlan == isMeetSupplementPlan.Value);
            }
            if(isMeetEmergencyPlan.HasValue) {
                partNotConPurchasePlan = partNotConPurchasePlan.Where(r => r.IsMeetEmergencyPlan == isMeetEmergencyPlan.Value);
            }
            if(egAdvicePurchase.HasValue) {
                if(egAdvicePurchase.Value)
                    partNotConPurchasePlan = partNotConPurchasePlan.Where(r => r.EgAdvicePurchase > 0);
                else {
                    partNotConPurchasePlan = partNotConPurchasePlan.Where(r => r.EgAdvicePurchase < 0);
                }
            }
            var queryResult = partNotConPurchasePlan.ToArray().Select(r => new object[]{
             r.PartCode,
r.PartName,
r.PartsSalesCategoryName,
r.DefaultOrderWarehouseName,
r.SupplierPartCode,
r.OrderCycle,
r.EmergencyArrivalPeriod,
r.MonthlyArrivalPeriod,
r.ArrivalReplenishmentCycle,
r.PartABC != null?Enum.GetName(typeof(DcsABCStrategyCategory),r.PartABC):string.Empty,
r.ProductLifeCycle != null?Enum.GetName(typeof(DcsPartsBranchProductLifeCycle),r.ProductLifeCycle):string.Empty,
r.PlannedPriceCategory != null?Enum.GetName(typeof(DcsPlanPriceCategory_PriceCategory),r.PlannedPriceCategory):string.Empty,
r.OrderQuantityFor12M,
r.PlannedPrice,
r.FREQUENCY1,
r.DAYAVGFOR1M,
r.OrderQuantity1,
r.ORDERQUANTITY2,
r.ORDERQUANTITY3,
r.SafeStock,
r.AllValidStocknums,
r.PurchaseOLnums,
r.PCnums,
r.TransferOLnums,
r.Replacementnums,
r.ReplacePurchaseOLnums,
r.PartsExchangenums,
r.PartsExchangeOLnums,
r.SLNCnums,
r.DeliveryTime,
r.RsAdvicePurchase,
r.EgAdvicePurchase,
 r.IsMeetSupplementPlan != null?((bool)r.IsMeetSupplementPlan?"是":"否"):string.Empty,
r.IsMeetEmergencyPlan != null?((bool)r.IsMeetEmergencyPlan?"是":"否"):string.Empty
            }).ToArray();
            var excelColumns = new List<string> {
               "配件编号","配件名称","品牌","默认订货仓库","首选供应商编码","订货周期","紧急到货周期","常规到货周期","补充计划到货周期","配件ABC分类"
               ,"产品生命周期","计划价分类","近一年出库频次","计划价","上月出库频次",
"近1月日均销量","T-1月销量","T-2月销量","T-3月销量","安全库存","有效库存","采购在途"
,"采购待确认量","调拨在途量","替换件库存","替换件采购在途","互换件库存","互换件采购在途","销售待审量","在途预计到货时间","补货建议采购量","紧急建议采购量","是否满足补充采购计划","是否满足紧急采购计划"
            };
            return ExportAllData(queryResult, excelColumns, exportDataFileName);
        }
    }
}
