﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string ExportPartSafeStock(int[] ids, int? productLifeCycle, int? plannedPriceType,int? startOrderQuantityFor12M,int? endOrderQuantityFor12M, double? serviceLevel, string partCode, string partName, string primarySupplierCode, string primarySupplierName) {
            var exportDataFileName = GetExportFilePath(string.Format("安全库存管理_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            //var partSafeStocks = GetPartSafeStocks();
            var partSafeStocks = GetPartSafeStocksByQuantity(startOrderQuantityFor12M,endOrderQuantityFor12M);

            if(ids != null && ids.Any()) {
                partSafeStocks = partSafeStocks.Where(r => ids.Contains(r.Id));
            } else {
                if(productLifeCycle.HasValue) {
                    partSafeStocks = partSafeStocks.Where(r => r.PruductLifeCycle == productLifeCycle.Value);
                }
                if(plannedPriceType.HasValue) {
                    partSafeStocks = partSafeStocks.Where(r => r.PlannedPriceType == plannedPriceType.Value);
                }
                if(startOrderQuantityFor12M.HasValue) {
                    partSafeStocks = partSafeStocks.Where(r => r.OrderQuantityFor12M >= startOrderQuantityFor12M.Value);
                }
                if(endOrderQuantityFor12M.HasValue) {
                    partSafeStocks = partSafeStocks.Where(r => r.OrderQuantityFor12M <= endOrderQuantityFor12M.Value);
                }
                if(serviceLevel.HasValue) {
                    partSafeStocks = partSafeStocks.Where(r => r.ServiceLevel == serviceLevel.Value);
                }
                if(!string.IsNullOrEmpty(partCode)) {
                    partSafeStocks = partSafeStocks.Where(r => r.PartCode.Contains(partCode));
                }
                if(!string.IsNullOrEmpty(partName)) {
                    partSafeStocks = partSafeStocks.Where(r => r.PartName.Contains(partName));
                }
                if(!string.IsNullOrEmpty(primarySupplierCode)) {
                    partSafeStocks = partSafeStocks.Where(r => r.PrimarySupplierCode.Contains(primarySupplierCode));
                }
                if(!string.IsNullOrEmpty(primarySupplierName)) {
                    partSafeStocks = partSafeStocks.Where(r => r.PrimarySupplierName.Contains(primarySupplierName));
                }
            }

            var queryResult = partSafeStocks.ToArray().Select(r => new object[] { 
             r.PartCode,r.PartName,r.PartsSalesCategoryName,r.PrimarySupplierCode,r.PrimarySupplierName,r.OrderQuantityFor12M,
             r.CreatDuration,
             r.PartABC != null?Enum.GetName(typeof(DcsABCStrategyCategory),r.PartABC):string.Empty,
             r.PruductLifeCycle != null?Enum.GetName(typeof(DcsPartsBranchProductLifeCycle),r.PruductLifeCycle):string.Empty,
             r.PlannedPriceType != null?Enum.GetName(typeof(DcsPlanPriceCategory_PriceCategory),r.PlannedPriceType):string.Empty,
             r.ServiceLevel,r.SafeCoefficient,
             r.NearFTWStandardDiff,r.NearFTWAverage,r.SafeStock
            
            }).ToArray();

            var excelColumns = new List<string> {
                "配件图号","配件名称","品牌","首选供应商编码","首选供应商名称","近12月出库频次",
                "创建时长","配件ABC分类","产品生命周期","计划价分类","服务水平","安全系数",
                "近52周需求标准差","近52周周均","安全库存"
            };

            return ExportAllData(queryResult, excelColumns, "安全库存管理");

        }
    }
}
