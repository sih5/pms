﻿using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string 导出采购入库及供应商差价报表(DateTime? bCreateTime, DateTime? eCreateTime, DateTime? bPartsPurchaseOrderDate, DateTime? ePartsPurchaseOrderDate, string code, string partsPurchaseOrderCode, string sparePartCode, string supplierPartName, int? settleBillStatus, string settleBillCode) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("采购入库及供应商差价报表_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = 采购入库及供应商差价报表查询(bCreateTime, eCreateTime, bPartsPurchaseOrderDate, ePartsPurchaseOrderDate, code, partsPurchaseOrderCode, sparePartCode, supplierPartName, settleBillStatus, settleBillCode);
            var dataArray = data.ToArray();
            if(data.Any()) {
                excelColumns.Add(ErrorStrings.ExportColumn_InboundCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SupplierName);
                excelColumns.Add(ErrorStrings.ExportColumn_SupplierCode2);
                excelColumns.Add(ErrorStrings.ExportColumn_PartCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SupplierPartCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SIHCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SparePartName);
                excelColumns.Add(ErrorStrings.ExportColumn_PartABC);
                excelColumns.Add(ErrorStrings.ExportColumn_InboundAmount);
                excelColumns.Add(ErrorStrings.ExportColumn_InboundTime);
                excelColumns.Add(ErrorStrings.ExportColumn_InvoicePrice);
                excelColumns.Add(ErrorStrings.ExportColumn_PartsPurchaseOrderCode);
                excelColumns.Add(ErrorStrings.ExportColumn_PartsPurchaseOrderCreateTime);
                excelColumns.Add(ErrorStrings.ExportColumn_OrderType);
                excelColumns.Add(ErrorStrings.ExportColumn_SettleBillCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SettleBillStatus);
                excelColumns.Add(ErrorStrings.ExportColumn_SAPCode);
                excelColumns.Add(ErrorStrings.ExportColumn_PurchasePrice);
                excelColumns.Add(ErrorStrings.ExportColumn_DifferPurchasePrice);
                excelColumns.Add(ErrorStrings.ExportColumn_DifferPurchasePriceAll);
                excelColumns.Add(ErrorStrings.ExportColumn_PartsInboundCheckBillRemark);
                excelColumns.Add(ErrorStrings.ExportColumn_PartsPurchaseOrderRemark);
                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {                        
                            detail.Code,
                            detail.PartsSupplierName,
                            detail.PartsSupplierCode,
                            detail.SparePartCode, 
                            detail.SupplierPartCode,
                            detail.ReferenceCode,
                            detail.SparePartName,
                           Enum.GetName(typeof(DcsABCStrategyCategory),detail.PartABC),
                            detail.InspectedQuantity,
                            detail.CreateTime,
                            detail.SettlementPrice,
                            detail.PartsPurchaseOrderCode,
                            detail.PartsPurchaseOrderDate,
                            detail.PartsPurchaseOrderType,
                            detail.SettleBillCode,
                            Enum.GetName(typeof(DcsPartsPurchaseSettleStatus),detail.SettleBillStatus==null?0:detail.SettleBillStatus),
                            detail.SapCode,
                            detail.PurchasePrice,
                            detail.DifferPurchasePrice,
                            detail.DifferPurchasePriceAll,
                            detail.PartsInboundCheckBillRemark,                            
                            detail.PartsPurchaseOrderRemark
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
        public string 导出配件采购入库检验单统计报表(DateTime? bCreateTime, DateTime? eCreateTime, string code, string partsSupplierCode, string partsSupplierName, string sparePartCode, string supplierPartCode, string sparePartName, int? partABC, string settleBillStatus, string invoice, string referenceCode) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("配件采购入库检验单统计报表_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = 配件采购入库检验单统计(bCreateTime, eCreateTime,  code,  partsSupplierCode,  partsSupplierName,  sparePartCode,  supplierPartCode,  sparePartName,  partABC,  settleBillStatus,  invoice,  referenceCode);
            var dataArray = data.ToArray();
            if(data.Any()) {
                excelColumns.Add(ErrorStrings.ExportColumn_InboundCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SupplierName);
                excelColumns.Add(ErrorStrings.ExportColumn_SupplierCode2);
                excelColumns.Add(ErrorStrings.ExportColumn_SAPSparePartCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SupplierPartCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SIHCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SAPSparePartName);
                excelColumns.Add(ErrorStrings.ExportColumn_PartsABC);
                excelColumns.Add(ErrorStrings.ExportColumn_Price);
                excelColumns.Add(ErrorStrings.ExportColumn_Quantity);
                excelColumns.Add(ErrorStrings.ExportColumn_Money);
                excelColumns.Add(ErrorStrings.ExportColumn_CostPrice2);
                excelColumns.Add(ErrorStrings.ExportColumn_CostAmount);
                excelColumns.Add(ErrorStrings.ExportColumn_PurchaseDate);
                excelColumns.Add(ErrorStrings.ExportColumn_SettlementStatus);
                excelColumns.Add(ErrorStrings.ExportColumn_SettleBillCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SAPCode);
                excelColumns.Add(ErrorStrings.ExportColumn_InvoiceNumber);
                excelColumns.Add(ErrorStrings.ExportColumn_Remark);
               
                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {                        
                            detail.Code,
                            detail.PartsSupplierName,
                            detail.PartsSupplierCode,
                            detail.SparePartCode, 
                            detail.SupplierPartCode,
                            detail.ReferenceCode,
                            detail.SparePartName,
                           Enum.GetName(typeof(DcsABCStrategyCategory),detail.PartABC),
                            detail.SettlementPrice,
                             detail.InspectedQuantity,
                            detail.SettlementPriceSum,
                            detail.CostPrice,
                            detail.CostPriceAll,                          
                            detail.CreateTime,
                            Enum.GetName(typeof(DcsPartsPurchaseSettleStatus),detail.SettleBillStatus==null?0:detail.SettleBillStatus),
                            detail.SettleBillCode,
                            "",
                            detail.Invoice,
                            detail.PartsInboundCheckBillRemark
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
        public string 导出中心库入库单统计(DateTime? bCreateTime, DateTime? eCreateTime, string code, string sparePartCode, string referenceCode, int? warehouseId, int? originalRequirementBillType, string partsSalesOrderCode) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("中心库入库单统计报表_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = 中心库入库单统计(bCreateTime, eCreateTime, code, sparePartCode, referenceCode, warehouseId, originalRequirementBillType, partsSalesOrderCode);
            var dataArray = data.ToArray();
            if(data.Any()) {
                excelColumns.Add(ErrorStrings.ExportColumn_MarketingDepartment);
                excelColumns.Add(ErrorStrings.ExportColumn_WarehouseCode);
                excelColumns.Add(ErrorStrings.ExportColumn_WarehouseName);
                excelColumns.Add(ErrorStrings.ExportColumn_Province);
                excelColumns.Add("订单号");
                excelColumns.Add(ErrorStrings.ExportColumn_PartsSalesOrderProcessCode);
                excelColumns.Add(ErrorStrings.ExportColumn_OrderType);
                excelColumns.Add(ErrorStrings.ExportColumn_OrderType2);
                excelColumns.Add("对方单位名称");
                excelColumns.Add(ErrorStrings.ExportColumn_InboundCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SIHCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SAPSparePartCode);
                excelColumns.Add(ErrorStrings.ExportColumn_MapName);
                excelColumns.Add(ErrorStrings.ExportColumn_PartABC);
                excelColumns.Add(ErrorStrings.ExportColumn_InboundAmount);
                excelColumns.Add(ErrorStrings.ExportColumn_CenterPrice);
                excelColumns.Add(ErrorStrings.ExportColumn_CenterPriceAll);
                excelColumns.Add(ErrorStrings.ExportColumn_RetailPrice);
                excelColumns.Add(ErrorStrings.ExportColumn_SettlementPriceSum);
                excelColumns.Add(ErrorStrings.ExportColumn_InboundTime);
                excelColumns.Add(ErrorStrings.ExportColumn_Remark);
                excelColumns.Add(ErrorStrings.ExportColumn_PriceProp);

                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {                        
                            detail.MarketingDepartmentName,
                            detail.WarehouseCode,
                            detail.WarehouseName,
                            detail.Province, 
                            detail.PartsSalesOrderCode,
                            detail.PartsSalesOrderProcessCode,
                            detail.PartsSalesOrderTypeName,
                             Enum.GetName(typeof(DcsPartsInboundType),detail.InboundType==null?0:detail.InboundType),    
                             detail.CounterpartCompanyName,
                            detail.Code,
                            detail.ReferenceCode,
                            detail.SparePartCode,
                            detail.SparePartName,
                            Enum.GetName(typeof(DcsABCStrategyCategory),detail.PartABC),
                            detail.InspectedQuantity,
                            detail.SettlementPrice,
                            detail.SettlementPriceSum,
                            detail.OriginalPrice,
                            detail.OriginalPriceSum,
                            detail.CreateTime,
                            detail.PartsInboundCheckBillRemark,
                            detail.GroupName
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
        public string 导出服务站入库单统计(string code, string inboundType, string marketingDepartmentName, string storageCompanyName, string receivingCompanyName, DateTime? bConfirmedReceptionTime, DateTime? eConfirmedReceptionTime) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("服务站入库单统计_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = 服务站入库单统计(code, inboundType, marketingDepartmentName, storageCompanyName, receivingCompanyName, bConfirmedReceptionTime, eConfirmedReceptionTime);
            var dataArray = data.ToArray();
            if(data.Any()) {
                excelColumns.Add(ErrorStrings.ExportColumn_MarketingDepartment);
                excelColumns.Add(ErrorStrings.ExportColumn_AgencyName);
                excelColumns.Add(ErrorStrings.ExportColumn_DealerCode);
                excelColumns.Add(ErrorStrings.ExportColumn_DealerName);
                excelColumns.Add(ErrorStrings.ExportColumn_OrderCode);
                excelColumns.Add(ErrorStrings.ExportColumn_OrderType2);
                excelColumns.Add(ErrorStrings.ExportColumn_InboundAmount);
                excelColumns.Add(ErrorStrings.ExportColumn_InboundTime);
                excelColumns.Add(ErrorStrings.ExportColumn_SparePartCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SparePartName);
                excelColumns.Add(ErrorStrings.ExportColumn_DealerPrice2);
                excelColumns.Add(ErrorStrings.ExportColumn_OriginalPrice2);
                excelColumns.Add(ErrorStrings.ExportColumn_SettlementPrice2);
                excelColumns.Add(ErrorStrings.ExportColumn_SettlementPriceAll2);
                excelColumns.Add(ErrorStrings.ExportColumn_PriceProp);
                

                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] { 
                            detail.MarketingDepartmentName,
                            detail.StorageCompanyName,
                            detail.ReceivingCompanyCode,
                            detail.ReceivingCompanyName,
                            detail.Code,
                             detail.InboundType,                           
                            detail.Quantity,
                            detail.ConfirmedReceptionTime,
                            detail.PartsCode,
                            detail.PartsName,
                            detail.CostPrice, 
                            detail.OriginalPrice,
                            detail.SettlementPrice,
                            detail.SettlementPrice*detail.Quantity,
                            detail.GroupName
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
    }
}
