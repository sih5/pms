﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string ExportVirtualPartsPurchaseOrderDetail(int[] partIds, int salesCategoryId) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("采购订单查看信息_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var details = 查询采购订单未入库量(salesCategoryId, partIds).ToArray();
            if(details.Any()) {
                excelColumns.Add(ErrorStrings.ExportColumn_PartsPurchaseOrderCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SparePartCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SparePartName);
                excelColumns.Add(ErrorStrings.ExportColumn_PurchaseAmount);
                excelColumns.Add(ErrorStrings.ExportColumn_ConfirmAmount);
                excelColumns.Add(ErrorStrings.ExportColumn_ShippingAmount);
                excelColumns.Add(ErrorStrings.ExportColumn_UnInboundAmount);
                excelColumns.Add(ErrorStrings.ExportColumn_PartsPurchaseOrderStatus);
                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == details.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray();
                        var detail = details[index - 1];
                        var values = new object[] {
                            detail.PartsPurchaseOrderCode,
                            detail.SparePartCode,
                            detail.SparePartName,
                            detail.OrderAmount,
                            detail.ConfirmAmount,
                            detail.ShippingAmount,
                            detail.UnInboundAmount,
                            Enum.GetName(typeof(DcsPartsPurchaseOrderStatus),detail.PartsPurchaseOrderStatus)
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
    }
}
