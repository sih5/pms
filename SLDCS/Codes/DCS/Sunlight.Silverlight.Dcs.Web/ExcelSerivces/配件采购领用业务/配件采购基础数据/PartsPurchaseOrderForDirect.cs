﻿using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunlight.Silverlight.Dcs.Web.Resources;
using System.Collections.ObjectModel;

namespace Sunlight.Silverlight.Dcs.Web {
    public enum FinishiStatusFirect {
        未到考核期 = 1,
        超期未满足 = 2,
        按期满足 = 3,
        超期满足 = 4,
        不计考核 = 5
    }

    partial class DcsDomainService {
        public string 导出直供采购订单统计报表(string partsSupplierName, string code, string sparepartCode, int? finishStatus, int? status, string originalRequirementBillCode, string personName, DateTime? bSubmitTime, DateTime? eSubmitTime) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("直供采购订单统计报表_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = 直供采购订单完成情况统计(partsSupplierName, code, sparepartCode, finishStatus, status, originalRequirementBillCode, personName, bSubmitTime, eSubmitTime);
            var dataArray = data.ToArray();
            if(data.Any()) {
                excelColumns.Add(ErrorStrings.ExportColumn_SupplierName);
                excelColumns.Add(ErrorStrings.ExportColumn_SupplierCode2);
                excelColumns.Add("采购订单编号");
                excelColumns.Add("采购订单状态");
                excelColumns.Add("销售订单类型");
                excelColumns.Add("订单备注");
                excelColumns.Add("创建人");
                excelColumns.Add("采购员");
                excelColumns.Add("原始需求单据");
                excelColumns.Add("配件图号");
                excelColumns.Add(ErrorStrings.ExportColumn_SupplierPartCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SparePartName);
                excelColumns.Add("配件ABC分类");
                excelColumns.Add("计划量");
                excelColumns.Add("确认量");
                excelColumns.Add("供应商回告原因");
                excelColumns.Add("供应商发运量");
                excelColumns.Add("直供收货确认量");
                excelColumns.Add("配件公司入库数量");
                excelColumns.Add("理论发货时间");
                excelColumns.Add("终止人");
                excelColumns.Add("直供完成情况");
                excelColumns.Add("是否传SAP");
                excelColumns.Add("采购订单提交时间");
                excelColumns.Add("供应商确认时间");
                excelColumns.Add("供应商发运时间");
                excelColumns.Add("预计到达时间");
                excelColumns.Add("中心库收货确认时间");
                excelColumns.Add("配件公司入库时间");
                excelColumns.Add("物流公司");
                excelColumns.Add("发运方式");
                excelColumns.Add("物流单号");
                excelColumns.Add("入库状态");
                excelColumns.Add("收货单位");
                excelColumns.Add("收货地址");
                excelColumns.Add("收货人");
                excelColumns.Add("收货人电话");
                excelColumns.Add("运单备注");

                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {                        
                            detail.PartsSupplierName,
                            detail.PartsSupplierCode,
                            detail.Code,
                             Enum.GetName(typeof(DcsPartsPurchaseOrderStatus),detail.Status),
                             detail.PartsSalesOrderTypeName,
                             detail.Remark,
                             detail.CreatorName,
                             detail.PersonName,
                             detail.OriginalRequirementBillCode,
                            detail.SparePartCode, 
                            detail.SupplierPartCode,
                            detail.SparePartName,
                           detail.ABCStrategy,
                           detail.OrderAmount,
                           detail.ConfirmedAmount,
						    Enum.GetName(typeof(DcsPartsPurchaseOrderDetail_ShortSupReason),detail.ShortSupReason==null?0:detail.ShortSupReason),
                           detail.Quantity,
                           detail.ShipCon,
                            detail.InspectedQuantity,
                            detail.OrderShippingTime,
                            detail.CloserName,
                             Enum.GetName(typeof(FinishiStatusFirect),detail.FinishStatus==null?0:detail.FinishStatus),
                             detail.IsTransSap==false?"否":"是",
                            detail.SubmitTime,
                            detail.ApproveTime,
                            detail.ShippingDate,
                            detail.ExpectedArrivalDate,
                            detail.ConfirmationTime,
                            detail.SIHInboundDate,
                            detail.LogisticCompany,
                            Enum.GetName(typeof(DcsPartsShippingMethod),detail.ShippingMethod==null?0:detail.ShippingMethod),
                            detail.DeliveryBillNumber,
                            Enum.GetName(typeof(DcsPurchaseInStatus),detail.InStatus==null?0:detail.InStatus),
                            detail.ReceivingCompanyName,
                            detail.ReceivingAddress,
                            detail.ContactPerson,
                            detail.ContactPhone,                            
                            detail.ShipRemark
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
    }
}
