﻿using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web
{
    partial class DcsDomainService
    {
        public string 导出采购订单统计(string sparePartCode, string partsSupplierName, int? status, int? partsPurchaseOrderStatus, DateTime? bPartsPurchasePlanTime, DateTime? ePartsPurchasePlanTime, DateTime? bPartsPurchaseOrderDate, DateTime? ePartsPurchaseOrderDate, bool? isOnWayNumber, string remark, string supplierPartCode, DateTime? bSubmitTime, DateTime? eSubmitTime)
        {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("采购订单统计_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = GetPartsPurchasePlanReports(sparePartCode, partsSupplierName, status, partsPurchaseOrderStatus, bPartsPurchasePlanTime, ePartsPurchasePlanTime, bPartsPurchaseOrderDate, ePartsPurchaseOrderDate, isOnWayNumber, remark, supplierPartCode, bSubmitTime, eSubmitTime);
            var dataArray = data.ToArray();
            if (data.Any())
            {
                excelColumns.Add(ErrorStrings.ExportColumn_PurchasePlanCode);
                excelColumns.Add(ErrorStrings.ExportColumn_PartsPurchaseOrderCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SupplierPartCode);
                excelColumns.Add(ErrorStrings.ExportColumn_PartCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SIHCode);
                excelColumns.Add(ErrorStrings.ExportColumn_MapName);
                excelColumns.Add(ErrorStrings.ExportColumn_PartProp);
                excelColumns.Add(ErrorStrings.ExportColumn_PurchasePlanAmount);
                excelColumns.Add(ErrorStrings.ExportColumn_SupplierConfirmedAmount);
                excelColumns.Add(ErrorStrings.ExportColumn_SupplierConfirmedReason);
                excelColumns.Add(ErrorStrings.ExportColumn_SupplierShippingAmount);
                excelColumns.Add(ErrorStrings.ExportColumn_InboundAmount);
                excelColumns.Add(ErrorStrings.ExportColumn_InboundFinishAmount);
                excelColumns.Add(ErrorStrings.ExportColumn_OnwayAmount);
                excelColumns.Add(ErrorStrings.ExportColumn_PartsPurchaseOrderCreateTime);
                excelColumns.Add(ErrorStrings.ExportColumn_SupplierConfirmedTime);
                excelColumns.Add(ErrorStrings.ExportColumn_SupplierShippingTime);
                excelColumns.Add(ErrorStrings.ExportColumn_DeliveryTime);
                excelColumns.Add(ErrorStrings.ExportColumn_InboundTime);
                excelColumns.Add(ErrorStrings.ExportColumn_PlanType);
				excelColumns.Add(ErrorStrings.ExportColumn_OrderSubmitTime);
                excelColumns.Add(ErrorStrings.ExportColumn_PurchasePlanStatus);
                excelColumns.Add(ErrorStrings.ExportColumn_PurchaseOrderStatus);
                excelColumns.Add(ErrorStrings.ExportColumn_Warehouse);
                excelColumns.Add(ErrorStrings.ExportColumn_PlanPerson);
                excelColumns.Add(ErrorStrings.ExportColumn_SupplierName);
                excelColumns.Add(ErrorStrings.ExportColumn_SupplyShippingCycle);
                excelColumns.Add(ErrorStrings.ExportColumn_SupplyArrivalCycle);
                excelColumns.Add(ErrorStrings.ExportColumn_TheoreticalArrivalDate);
                excelColumns.Add(ErrorStrings.ExportColumn_ShippingStatus);
                excelColumns.Add(ErrorStrings.ExportColumn_InStatus);
                excelColumns.Add(ErrorStrings.ExportColumn_PackingMaterialSIHcode);
                excelColumns.Add(ErrorStrings.ExportColumn_PackingMaterialPartCode);
                excelColumns.Add(ErrorStrings.ExportColumn_PackingMaterialTransferAmount);
                excelColumns.Add(ErrorStrings.ExportColumn_PurchasePlanRemark);
                excelColumns.Add(ErrorStrings.ExportColumn_CompletionSituation);
                excelColumns.Add(ErrorStrings.Export_Title_PartsPurchaseOrderFinish_PersonName);
                using (var excelExport = new ExcelExport(exportDataFileName))
                {
                    excelExport.ExportByRow(index =>
                    {
                        if (index == dataArray.Count() + 1)
                            return null;
                        if (index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.Code,
                            detail.PartsPurchaseOrderCode,
                            detail.SupplierPartCode,
                            detail.SparePartCode,
                            detail.ReferenceCode, 
                            detail.SparePartName,
                            Enum.GetName(typeof(DcsABCStrategyCategory),detail.PartABC),
                            detail.PlanAmount,
                            detail.ConfirmedAmount,
                            Enum.GetName(typeof(DcsPartsPurchaseOrderDetail_ShortSupReason),detail.ShortSupReason),
                            detail.ShippingAmount,
                            detail.InspectedQuantity,
                            detail.CompletionQuantity,
                            detail.OnWayNumber,
                            detail.PartsPurchaseOrderDate.Value,
                            detail.ConfirmationDate,
                            detail.ShippingDate, 
                            detail.ExpectedArrivalDate,
                            detail.PartsInboundCheckBillDate,
                            detail.PartsPlanTypeName,
							detail.SubmitTime,
                            Enum.GetName(typeof(DcsPurchasePlanOrderStatus),detail.Status.Value),
                            Enum.GetName(typeof(DcsPartsPurchaseOrderStatus),detail.PartsPurchaseOrderStatus.Value),
                            detail.WarehouseName,
                            detail.SubmitterName,                           
                            detail.PartsSupplierName,
                            detail.OrderCycle,
                            detail.ArrivalCycle,
                            detail.TheoreticalArrivalDate,
                            detail.ShippingStatus,
                            Enum.GetName(typeof(DcsPurchaseInStatus),detail.InStatus),
                            detail.PackingMaterialReF,
                            detail.PackingMaterial,
                            detail.PackingCoefficient,
                            detail.Remark,
                            detail.CompletionSituation,
                            detail.PersonName
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }

    }
}

