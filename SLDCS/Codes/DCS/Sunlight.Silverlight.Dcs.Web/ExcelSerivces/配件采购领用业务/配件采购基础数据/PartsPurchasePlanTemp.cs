﻿using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string 导出采购订单对应包材明细查询(int? partsPurchaseOrderStatus, string partsPurchaseOrderCode, string sparePartCode, string packSparePartCode) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("采购订单对应包材明细报表_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = 采购订单对应包材明细查询(partsPurchaseOrderStatus, partsPurchaseOrderCode, sparePartCode, packSparePartCode);
            var dataArray = data.ToArray();
            if(data.Any()) {
                excelColumns.Add(ErrorStrings.ExportColumn_PartsPurchaseOrderCode);
                excelColumns.Add(ErrorStrings.ExportColumn_PartsPurchaseOrderStatus);
                excelColumns.Add(ErrorStrings.ExportColumn_WarehouseName);
                excelColumns.Add(ErrorStrings.ExportColumn_SparePartCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SparePartName);
                excelColumns.Add("包材采购数量");
                excelColumns.Add("包材图号");
                excelColumns.Add("包材名称");
                excelColumns.Add("单位包装数量（包装设置）");
                excelColumns.Add("备件采购数量");
                excelColumns.Add("状态");
                excelColumns.Add("作废时间");
                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {                        
                            detail.PartsPurchaseOrderCode,
							Enum.GetName(typeof(DcsPartsPurchaseOrderStatus),detail.PartsPurchaseOrderStatus),
                            detail.WarehouseName,
                            detail.SparePartCode,
                            detail.SparePartName, 
                            detail.OrderAmount,
                            detail.PackSparePartCode,
                            detail.PackSparePartName,
                            detail.PackNum,
                            detail.PackPlanQty,
                            Enum.GetName(typeof(DcsBaseDataStatus),detail.Status),
                            detail.ModifyTime
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
    }
}
