﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {

        public string 导出编辑校验问题配件(string[] partCode) {
            var exportDataFileName = GetExportFilePath(string.Format("存在多个分品牌配件清单_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var excelColumns = new List<string>();
            var dPartCode = partCode.Distinct().ToArray();
            if(dPartCode.Any()) {
                excelColumns.Add(ErrorStrings.ExportColumn_PartCode);
                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dPartCode.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray();
                        var detail = dPartCode[index - 1];
                        var values = new object[] {
                          detail
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }

        public string 导出配件采购价格变更申请清单(int[] parentIds, int? partsSalesCategoryId, string code, int? status,string sparePartCode,string sparePartName, DateTime? creatorTimeBegin, DateTime? creatorTimeEnd) {
            var exportDataFileName = GetExportFilePath(string.Format("已匹配配件采购价格变更申请清单_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));

            var excelColumns = new List<string>();
            var details = from d in ObjectContext.PartsPurchasePricingDetails
                          join p in ObjectContext.PartsPurchasePricingChanges on d.ParentId equals p.Id
                          select new {
                              p,
                              d
                          };
            if(parentIds != null && parentIds.Any()) {
                details = details.Where(r => parentIds.Contains(r.d.ParentId));
            } else {
                IQueryable<PartsPurchasePricingChange> partsPurchasePricingChange = ObjectContext.PartsPurchasePricingChanges;
                if(partsSalesCategoryId.HasValue) {
                    partsPurchasePricingChange = partsPurchasePricingChange.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId);
                }
                if(status.HasValue) {
                    partsPurchasePricingChange = partsPurchasePricingChange.Where(r => r.Status == status);
                }
                if(creatorTimeBegin.HasValue) {
                    partsPurchasePricingChange = partsPurchasePricingChange.Where(r => r.CreateTime >= creatorTimeBegin);
                }
                if(partsSalesCategoryId.HasValue) {
                    partsPurchasePricingChange = partsPurchasePricingChange.Where(r => r.CreateTime <= creatorTimeEnd);
                }
                if(!string.IsNullOrEmpty(code)) {
                    partsPurchasePricingChange = partsPurchasePricingChange.Where(r => r.Code == code);
                }
                if (!string.IsNullOrEmpty(sparePartCode))
                    details = details.Where(r => r.d.PartCode.Contains(sparePartCode));
                if (!string.IsNullOrEmpty(sparePartName))
                    details = details.Where(r => r.d.PartName.Contains(sparePartName));
                details = details.Where(r => partsPurchasePricingChange.Any(x => r.d.ParentId == x.Id));
            }
            var result = details.ToArray();

            if(result.Any()) {
                excelColumns.Add(ErrorStrings.ExportColumn_ApplyCode);
                excelColumns.Add(ErrorStrings.ExportColumn_PartsSalesCategory);
                excelColumns.Add(ErrorStrings.ExportColumn_Status);
                excelColumns.Add(ErrorStrings.ExportColumn_Remark);
                excelColumns.Add(ErrorStrings.ExportColumn_Creator);
                excelColumns.Add(ErrorStrings.ExportColumn_CreateTime);
                excelColumns.Add(ErrorStrings.ExportColumn_Modifier);
                excelColumns.Add(ErrorStrings.ExportColumn_ModifyTime);
                excelColumns.Add(ErrorStrings.ExportColumn_Approver);
                excelColumns.Add(ErrorStrings.ExportColumn_ApproveTime);
                excelColumns.Add(ErrorStrings.ExportColumn_Abandoner);
                excelColumns.Add(ErrorStrings.ExportColumn_AbandonTime);
                                 

                excelColumns.Add(ErrorStrings.ExportColumn_PartCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SparePartName);
                excelColumns.Add(ErrorStrings.ExportColumn_SupplierCode2);
                excelColumns.Add(ErrorStrings.ExportColumn_SupplierName);
                excelColumns.Add(ErrorStrings.ExportColumn_PriceType);
                excelColumns.Add(ErrorStrings.ExportColumn_Price2);
                excelColumns.Add(ErrorStrings.ExportColumn_ValidFrom);
                excelColumns.Add(ErrorStrings.ExportColumn_ValidTo);
                excelColumns.Add(ErrorStrings.ExportColumn_RenferencePrice);
                excelColumns.Add(ErrorStrings.ExportColumn_IsPrimary);
                excelColumns.Add(ErrorStrings.ExportColumn_LimitQty);
                excelColumns.Add(ErrorStrings.ExportColumn_MinPurchasePrice);
                excelColumns.Add(ErrorStrings.ExportColumn_MinPriceSupplier);
                excelColumns.Add(ErrorStrings.ExportColumn_IsPrimaryMinPrice);
                excelColumns.Add(ErrorStrings.ExportColumn_DataSource);
                excelColumns.Add(ErrorStrings.ExportColumn_Remark);
                excelColumns.Add("变更比例");
                excelColumns.Add("原首选供应商");
                excelColumns.Add("原首选价格");
                excelColumns.Add("原首选价格类型");
                //int rowindex = 1;
                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == result.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray();
                        var detail = result[index - 1];
                        var values = new object[] {
                            detail.p.Code, 
                            detail.p.PartsSalesCategory.Name, 
                            Enum.GetName(typeof(DcsPartsPurchasePricingChangeStatus),detail.p.Status),
                            detail.p.Remark, 
                            detail.p.CreatorName, 
                            detail.p.CreateTime, 
                            detail.p.ModifierName, 
                            detail.p.ModifyTime, 
                            detail.p.ApproverName, 
                            detail.p.ApproveTime, 
							detail.p.AbandonerName, 
                            detail.p.AbandonTime, 
                            detail.d.PartCode, 
                            detail.d.PartName, 
                            detail.d.SupplierCode, 
                            detail.d.SupplierName, 
                            detail.d.PriceType != null ? Enum.GetName(typeof(DcsPurchasePriceType),detail.d.PriceType) : string.Empty,
                            detail.d.Price,
                            detail.d.ValidFrom, 
                            detail.d.ValidTo,
                            detail.d.ReferencePrice,
                            detail.d.IsPrimary == true ? ErrorStrings.ExportColnum_Yes:ErrorStrings.ExportColnum_No,
                            detail.d.LimitQty,
                            detail.d.MinPurchasePrice,
                            detail.d.MinPriceSupplier,
                            detail.d.IsPrimaryMinPrice == true ? ErrorStrings.ExportColnum_Yes:ErrorStrings.ExportColnum_No,
                            detail.d.DataSource != null ? Enum.GetName(typeof(DcsPurchasePricingDataSource),detail.d.DataSource) : string.Empty,
                            detail.d.Remark,                            
                            Convert.ToDecimal(detail.d.ReferencePrice) == 0?"0":Math.Round((((detail.d.Price - Convert.ToDecimal(detail.d.ReferencePrice)) / Convert.ToDecimal(detail.d.ReferencePrice)) * 100),2) + "%",
                            detail.d.OldPriSupplierName,
                            detail.d.OldPriPrice,
                            detail.d.OldPriPriceType != null ? Enum.GetName(typeof(DcsPurchasePriceType),detail.d.OldPriPriceType) : string.Empty,
                        };
                        //rowindex++;
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }

        public string 导出配件采购价格变更申请营销清单(int[] parentIds, int? partsSalesCategoryId, string code, int? status, DateTime? creatorTimeBegin, DateTime? creatorTimeEnd) {
            var exportDataFileName = GetExportFilePath(string.Format("已匹配配件采购价格变更申请营销清单_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));

            var excelColumns = new List<string>();
            var details = from d in ObjectContext.PartsPurchasePricingDetails
                          join p in ObjectContext.PartsPurchasePricingChanges on d.ParentId equals p.Id
                          select new {
                              p,
                              d
                          };
            if(parentIds != null && parentIds.Any()) {
                details = details.Where(r => parentIds.Contains(r.d.ParentId));
            } else {
                IQueryable<PartsPurchasePricingChange> partsPurchasePricingChange = ObjectContext.PartsPurchasePricingChanges;
                if(partsSalesCategoryId.HasValue) {
                    partsPurchasePricingChange = partsPurchasePricingChange.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId);
                }
                if(status.HasValue) {
                    partsPurchasePricingChange = partsPurchasePricingChange.Where(r => r.Status == status);
                }
                if(creatorTimeBegin.HasValue) {
                    partsPurchasePricingChange = partsPurchasePricingChange.Where(r => r.CreateTime >= creatorTimeBegin);
                }
                if(partsSalesCategoryId.HasValue) {
                    partsPurchasePricingChange = partsPurchasePricingChange.Where(r => r.CreateTime <= creatorTimeEnd);
                }
                if(!string.IsNullOrEmpty(code)) {
                    partsPurchasePricingChange = partsPurchasePricingChange.Where(r => r.Code == code);
                }
                details = details.Where(r => partsPurchasePricingChange.Any(x => r.d.ParentId == x.Id));
            }
            var result = details.ToArray();

            if(result.Any()) {
                excelColumns.Add("申请单编号");
                excelColumns.Add("品牌");
                excelColumns.Add(ErrorStrings.ExportColumn_Status);
                excelColumns.Add(ErrorStrings.ExportColumn_Remark);
                excelColumns.Add(ErrorStrings.ExportColumn_Creator);
                excelColumns.Add(ErrorStrings.ExportColumn_CreateTime);
                excelColumns.Add(ErrorStrings.ExportColumn_Modifier);
                excelColumns.Add(ErrorStrings.ExportColumn_ModifyTime);
                excelColumns.Add("审批人");
                excelColumns.Add("审批时间");
                excelColumns.Add(ErrorStrings.ExportColumn_Abandoner);
                excelColumns.Add(ErrorStrings.ExportColumn_AbandonTime);

                excelColumns.Add(ErrorStrings.ExportColumn_PartCode);
                excelColumns.Add("配件名称");
                excelColumns.Add(ErrorStrings.ExportColumn_SupplierCode2);
                excelColumns.Add(ErrorStrings.ExportColumn_SupplierName);
                excelColumns.Add(ErrorStrings.ExportColumn_PriceType);
                excelColumns.Add("价格");
                excelColumns.Add(ErrorStrings.ExportColumn_ValidFrom);
                excelColumns.Add(ErrorStrings.ExportColumn_ValidTo);
                excelColumns.Add("参考价格");
                excelColumns.Add(ErrorStrings.ExportColumn_Remark);
                excelColumns.Add("变动幅度");
                //int rowindex = 1;
                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == result.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray();
                        var detail = result[index - 1];
                        var values = new object[] {
							detail.p.Code, 
                            detail.p.PartsSalesCategory.Name, 
                            Enum.GetName(typeof(DcsPartsPurchasePricingChangeStatus),detail.p.Status),
                            detail.p.Remark, 
                            detail.p.CreatorName, 
                            detail.p.CreateTime, 
                            detail.p.ModifierName, 
                            detail.p.ModifyTime, 
                            detail.p.ApproverName, 
                            detail.p.ApproveTime, 
							detail.p.AbandonerName, 
                            detail.p.AbandonTime, 
							
                             detail.d.PartCode,
							 detail.d.PartName,
							 detail.d.SupplierCode,
							 detail.d.SupplierName,
                             detail.d.PriceType!=null?Enum.GetName(typeof(DcsPurchasePriceType),detail.d.PriceType):string.Empty,
                             detail.d.Price,
                             detail.d.ValidFrom,
							 detail.d.ValidTo,
							 detail.d.ReferencePrice,
							 detail.d.Remark,
                             Convert.ToDecimal(detail.d.ReferencePrice)!=0?(Math.Round((detail.d.Price - Convert.ToDecimal(detail.d.ReferencePrice)) / Convert.ToDecimal(detail.d.ReferencePrice),2) * 100) + "%":"0"
                        };
                        //rowindex++;
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
    }
}
