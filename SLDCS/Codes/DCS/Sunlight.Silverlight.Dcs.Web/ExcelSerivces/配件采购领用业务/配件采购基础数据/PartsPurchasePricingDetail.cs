﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string ExportPartsPurchasePricingDetail(int[] ids, int? branchId, string partCode, string partName,string referenceCode, string supplierCode, string supplierName,string supplierPartCode, int? partsSalesCategoryId, DateTime? validFromStart, DateTime? validFromEnd) {
            var query = 查询配件采购价格变更履历();
            var userinfo = Utils.GetCurrentUserInfo();
            if(ids != null && ids.Any()) {
                query = query.Where(r => ids.Contains(r.Id));
            } else {
                if(branchId.HasValue) {
                    query = query.Where(r => r.PartsPurchasePricingChange.BranchId == userinfo.EnterpriseId);
                }
                if(!string.IsNullOrEmpty(partCode)) {
                    query = query.Where(r => r.PartCode.Contains(partCode));
                }
                if(!string.IsNullOrEmpty(partName)) {
                    query = query.Where(r => r.PartName.Contains(partName));
                }
                if(!string.IsNullOrEmpty(referenceCode)) {
                    query = query.Where(r => r.SparePart.ReferenceCode.Contains(referenceCode));
                }
                if(!string.IsNullOrEmpty(supplierCode)) {
                    query = query.Where(r => r.SupplierCode.Contains(supplierCode));
                }
                if(!string.IsNullOrEmpty(supplierName)) {
                    query = query.Where(r => r.SupplierName.Contains(supplierName));
                }
                if(!string.IsNullOrEmpty(supplierPartCode)) {
                    query = query.Where(r => r.SupplierPartCode.Contains(supplierPartCode));
                }
                if(partsSalesCategoryId.HasValue) {
                    query = query.Where(r => r.PartsPurchasePricingChange.PartsSalesCategoryId == partsSalesCategoryId.Value);
                }
                if(validFromStart.HasValue)
                    query = query.Where(r => r.ValidFrom >= validFromStart);
                if(validFromEnd.HasValue)
                    query = query.Where(r => r.ValidFrom <= validFromEnd);

            }
            var queryReuslt = query.ToArray().Select(r => new object[] {
                r.PartsPurchasePricingChange.Code,
                r.PartsPurchasePricingChange.PartsSalesCategoryName,
                r.PartCode,
                r.PartName,
                r.SparePart.ReferenceCode,
                r.SupplierCode,
                r.SupplierName,
                r.SupplierPartCode,
				r.IsPrimary!=null?(r.IsPrimary.Value==true?"是":"否"):"否",
				r.IfPurchasable!=null?(r.IfPurchasable.Value==true?"是":"否"):"否",
                r.PriceType!=null?Enum.GetName(typeof(DcsPurchasePriceType),r.PriceType):string.Empty,
                r.Price,
                r.ValidFrom,
                r.ValidTo,
                Enum.GetName(typeof(DcsPartsPurchasePricingChangeStatus),r.PartsPurchasePricingChange.Status),
                r.Remark,
                r.PartsPurchasePricingChange.CreatorName,
                r.PartsPurchasePricingChange.CreateTime,
                r.PartsPurchasePricingChange.ModifierName,
                r.PartsPurchasePricingChange.ModifyTime,
                r.PartsPurchasePricingChange.ApproverName,
                r.PartsPurchasePricingChange.ApproveTime
            }).ToArray();

            var excelColumns = new List<string> {
                ErrorStrings.ExportColumn_ApplyCode,
                ErrorStrings.ExportColumn_PartsSalesCategory,
                ErrorStrings.ExportColumn_PartCode,
                ErrorStrings.ExportColumn_SparePartName,
                ErrorStrings.ExportColumn_SIHCode,
                ErrorStrings.ExportColumn_SupplierCode2,
                ErrorStrings.ExportColumn_SupplierName,
                ErrorStrings.ExportColumn_SupplierPartCode,
                ErrorStrings.ExportColumn_IsPrimarySupplier,
                ErrorStrings.ExportColumn_IsOrderable,
                ErrorStrings.ExportColumn_PriceType,
                ErrorStrings.ExportColumn_Price2,
                ErrorStrings.ExportColumn_ValidFrom,
                ErrorStrings.ExportColumn_ValidTo,
                ErrorStrings.ExportColumn_Status,
                ErrorStrings.ExportColumn_Remark,
                ErrorStrings.ExportColumn_Creator,
                ErrorStrings.ExportColumn_CreateTime,
                ErrorStrings.ExportColumn_Modifier,
                ErrorStrings.ExportColumn_ModifyTime,
                ErrorStrings.ExportColumn_Approver,
                ErrorStrings.ExportColumn_ApproveTime
            };
            return ExportAllData(queryReuslt, excelColumns, "配件采购价格变更履历");
        }

        public string ExportSparePartWithPartsPurchasePricing(int[] ids, string partCode, string partName, DateTime? validFromStart, DateTime? validFromEnd)
        {
            var query = GetSparePartWithPartsPurchasePricing();
            var userinfo = Utils.GetCurrentUserInfo();

            if(ids != null && ids.Any()) {
                query = query.Where(r => ids.Contains(r.Id));
            } else {
                if(!string.IsNullOrEmpty(partCode)) {
                    query = query.Where(r => r.Code.Contains(partCode));
                }
                if(!string.IsNullOrEmpty(partName)) {
                    query = query.Where(r => r.Name.Contains(partName));
                }
                if(validFromStart.HasValue)
                    query = query.Where(r => r.CreateTime >= validFromStart);
                if(validFromEnd.HasValue)
                    query = query.Where(r => r.CreateTime <= validFromEnd);

            }
            var queryReuslt = query.ToArray().Select(r => new object[] {
                r.Code,
                r.Name,
                r.SupplierName,
                r.PartsPurchasePricings.FirstOrDefault().PurchasePrice,
                r.PartsPurchasePricings.FirstOrDefault().ValidFrom
            }).ToArray();

            var excelColumns = new List<string> {
                "配件图号",
                "配件名称",
                "首选供应商",
                "采购价格",
                ErrorStrings.ExportColumn_ValidFrom
            };
            return ExportAllData(queryReuslt, excelColumns, "新产品明细");
        }
    }
}
