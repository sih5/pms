﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string ExportPartsSalesSettlementBill(int[] inboundIds, int[] outboundIds, int? partsSalesCategoryId, int? partsSupplierId, string filename) {
            var excelColumns = new List<string>();
            List<PartsPurchaseSettleDetail> partsPurchaseSettleDetails;
            string exportDataFileName = GetExportFilePath(string.Format("{0}_{1}.xlsx", filename, DateTime.Now.ToString("ddHHmmssffff")));
            //if(!partsSalesCategoryId.HasValue) {
            var details = 查询待结算出入库明细清单(inboundIds, outboundIds, null).ToArray();
            partsPurchaseSettleDetails = details.Select(item => new PartsPurchaseSettleDetail {
                SparePartCode = item.SparePartCode,
                SparePartName = item.SparePartName,
                //QuantityToSettle = item.BillType == (int)DcsPartsLogisticBatchBillDetailBillType.配件出库单 ? -item.Quantity : item.Quantity,
                QuantityToSettle = item.Quantity,
                SettlementPrice = item.SettlementPrice,
                SettlementAmount = item.SettlementAmount,
            }).ToList();
            //} else {
            //    if(partsSupplierId == null) {
            //        throw new ValidationException("配件供应商不能为空");
            //    }
            //    var details = 查询待结算出入库明细清单后定价(inboundIds, outboundIds, partsSalesCategoryId.Value, partsSupplierId.Value).ToArray();
            //    partsPurchaseSettleDetails = details.Select(item => new PartsPurchaseSettleDetail {
            //        SparePartCode = item.SparePartCode,
            //        SparePartName = item.SparePartName,
            //        QuantityToSettle = item.Quantity,
            //        SettlementPrice = item.SettlementPrice,
            //        SettlementAmount = item.SettlementAmount,
            //    }).ToList();

            //}

            if(partsPurchaseSettleDetails.Any()) {
                excelColumns.Add(ErrorStrings.ExportColumn_PartCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SparePartName);
                excelColumns.Add(ErrorStrings.ExportColumn_SettlementPrice);
                excelColumns.Add(ErrorStrings.ExportColumn_SettlementQuantity);
                excelColumns.Add(ErrorStrings.ExportColumn_SettlementAmount);
                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == partsPurchaseSettleDetails.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = partsPurchaseSettleDetails[index - 1];
                        var values = new object[] {
                             detail.SparePartCode,detail.SparePartName,detail.SettlementPrice,
                             detail.QuantityToSettle,detail.SettlementAmount
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }

        public string ExportPartsPurchaseSettleBill(int[] ids, int? branchId, string branchName, int? partsSupplierId, string code, int? warehouseId, string partsSupplierCode, string partsSupplierName, int? settlementPath, int? partsSalesCategoryId, DateTime? startDateTime, DateTime? endDateTime, int? status, string creatorName, DateTime? startInoviceApproveTime, DateTime? endInoviceApproveTime, string businessCode, string fileName) {
            var excelColumns = new object[] {
                ErrorStrings.ExportColumn_SettlementCode,
                ErrorStrings.ExportColumn_Branch,
                ErrorStrings.ExportColumn_PartsSalesCategory,
                ErrorStrings.ExportColumn_WarehouseName,
                ErrorStrings.ExportColumn_SupplierCode2,
                ErrorStrings.ExportColumn_SupplierName,
                ErrorStrings.ExportColumn_SettlementTotalAmount,
                ErrorStrings.ExportColumn_TaxRate,
                //"计划价合计",
                ErrorStrings.ExportColumn_InvoiceTotalAmount,
                ErrorStrings.ExportColumn_InvoiceDifference,
                ErrorStrings.ExportColumn_CostDifference,
                ErrorStrings.ExportColumn_Status,
                ErrorStrings.ExportColumn_SettlementPath,
                ErrorStrings.ExportColumn_OffsettedSettlementBillCode,
                ErrorStrings.ExportColumn_Remark,
                ErrorStrings.ExportColumn_Creator,
                ErrorStrings.ExportColumn_CreateTime,
                ErrorStrings.ExportColumn_Modifier,
                ErrorStrings.ExportColumn_ModifyTime,
                ErrorStrings.ExportColumn_Approver,
                ErrorStrings.ExportColumn_ApproveTime,
                ErrorStrings.ExportColumn_InvoiceApproverName,
                ErrorStrings.ExportColumn_InvoiceApproveTime,
                ErrorStrings.ExportColumn_Abandoner,
                ErrorStrings.ExportColumn_AbandonTime
            };
            var exportDataFileName = GetExportFilePath(string.Format("{0}_{1}.xlsx", fileName, DateTime.Now.ToString("ddHHmmssffff")));
            var partsPurchaseSettleBill = GetPartsPurchaseSettleBillWithPartsSalesCategory();
            if(ids != null && ids.Any()) {
                partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => ids.Contains(r.Id));
            } else {
                if(branchId.HasValue) {
                    partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.BranchId == branchId.Value);
                }
                if(!string.IsNullOrEmpty(branchName)) {
                    partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.BranchName.Contains(branchName));
                }
                if(partsSupplierId.HasValue) {
                    partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.PartsSupplierId == partsSupplierId.Value);
                }
                if(!string.IsNullOrEmpty(code)) {
                    partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.Code.Contains(code));
                }
                if(warehouseId.HasValue) {
                    partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.WarehouseId == warehouseId);
                }
                if(!string.IsNullOrEmpty(partsSupplierCode)) {
                    partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.PartsSupplierCode.Contains(partsSupplierCode));
                }
                if(!string.IsNullOrEmpty(partsSupplierName)) {
                    partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.PartsSupplierName.Contains(partsSupplierName));
                }
                if(settlementPath.HasValue) {
                    partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.SettlementPath == settlementPath.Value);
                }
                if(partsSalesCategoryId.HasValue) {
                    partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId);
                }
                if(startDateTime.HasValue) {
                    partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.CreateTime >= startDateTime.Value);
                }
                if(endDateTime.HasValue) {
                    partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.CreateTime <= endDateTime.Value);
                }
                if(startInoviceApproveTime.HasValue) {
                    partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.InvoiceApproveTime >= startInoviceApproveTime.Value);
                }
                if(endInoviceApproveTime.HasValue) {
                    partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.InvoiceApproveTime <= endInoviceApproveTime.Value);
                }
                if(status.HasValue) {
                    partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.Status == status.Value);
                }
                if(!string.IsNullOrEmpty(creatorName)) {
                    partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.CreatorName.Contains(creatorName));
                }
                if(!string.IsNullOrEmpty(businessCode)) {
                    partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.BusinessCode.Contains(businessCode));
                }
            }
            var result = partsPurchaseSettleBill.OrderByDescending(r => r.InvoiceApproveTime).ToArray();
            if(result.Any()) {
                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == result.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns;
                        var detail = result[index - 1];
                        var values = new object[] {
                            detail.Code,
                            detail.BranchCode,
                            detail.PartsSalesCategoryName,
                            detail.WarehouseName,
                            detail.PartsSupplierCode,
                            detail.PartsSupplierName,
                            //detail.TotalSettlementAmount,
                            detail.TaxRate,
                            //detail.Tax,
                            detail.SumPlannedPrice,
                            detail.InvoiceTotalAmount,
                            detail.InvoiceAmountDifference,
                            detail.CostAmountDifference,
                            Enum.GetName(typeof(DcsPartsPurchaseSettleStatus),detail.Status),
                            Enum.GetName(typeof(DcsPartsPurchaseSettleBillSettlementPath),detail.SettlementPath),
                            detail.OffsettedSettlementBillCode,
                            detail.Remark,
                            detail.CreatorName,
                            new StyleDataField(detail.CreateTime,"yyyy年m月d日 hh:mm:ss"),
                            detail.ModifierName,
                            new StyleDataField(detail.ModifyTime,"yyyy年m月d日 hh:mm:ss"),
                            detail.ApproverName,
                            new StyleDataField(detail.ApproveTime,"yyyy年m月d日 hh:mm:ss"),
                            detail.InvoiceApproverName,
                            new StyleDataField(detail.InvoiceApproveTime,"yyyy年m月d日 hh:mm:ss"),
                            detail.AbandonerName,
                            detail.AbandonTime
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }

        public string ExportPartsPurchaseSettleBillWithDetail(int[] ids, int? branchId, string branchName, int? partsSupplierId, string code, int? warehouseId, string partsSupplierCode, string partsSupplierName, int? settlementPath, int? partsSalesCategoryId, DateTime? startDateTime, DateTime? endDateTime, int? status, string creatorName, DateTime? startInoviceApproveTime, DateTime? endInoviceApproveTime, string businessCode, string fileName) {
                var excelColumns = new object[] {
                    ErrorStrings.ExportColumn_SettlementCode,
					ErrorStrings.ExportColumn_Branch,
					ErrorStrings.ExportColumn_PartsSalesCategory,
					ErrorStrings.ExportColumn_WarehouseName,
					ErrorStrings.ExportColumn_SupplierCode2,
					ErrorStrings.ExportColumn_SupplierName,
					ErrorStrings.ExportColumn_SettlementTotalAmount,
					ErrorStrings.ExportColumn_TaxRate,
					ErrorStrings.ExportColumn_TaxFee,
					ErrorStrings.ExportColumn_InvoiceTotalAmount,
					ErrorStrings.ExportColumn_InvoiceDifference,
					ErrorStrings.ExportColumn_CostDifference,
					ErrorStrings.ExportColumn_Status,
					ErrorStrings.ExportColumn_SettlementPath,
					ErrorStrings.ExportColumn_OffsettedSettlementBillCode,
					ErrorStrings.ExportColumn_Remark,
					ErrorStrings.ExportColumn_Creator,
					ErrorStrings.ExportColumn_CreateTime,
					ErrorStrings.ExportColumn_Modifier,
					ErrorStrings.ExportColumn_ModifyTime,
					ErrorStrings.ExportColumn_Approver,
					ErrorStrings.ExportColumn_ApproveTime,
					ErrorStrings.ExportColumn_InvoiceApproverName,
					ErrorStrings.ExportColumn_InvoiceApproveTime,
					ErrorStrings.ExportColumn_Abandoner,
					ErrorStrings.ExportColumn_AbandonTime,
					ErrorStrings.ExportColumn_ContactOrderCode,
					ErrorStrings.ExportColumn_OriginalRequirementBillCode,
					ErrorStrings.ExportColumn_SourceCode,
					ErrorStrings.ExportColumn_DeliveryBillNumber,
					ErrorStrings.ExportColumn_OrderType2,
					ErrorStrings.ExportColumn_WarehouseName,
					ErrorStrings.ExportColumn_PartCode,
					ErrorStrings.ExportColumn_SparePartName,
					ErrorStrings.ExportColumn_SettlementPrice,
					ErrorStrings.ExportColumn_SettlementQuantity,
					ErrorStrings.ExportColumn_SettlementAmount

                };
                var exportDataFileName = GetExportFilePath(string.Format("{0}_{1}.xlsx", fileName, DateTime.Now.ToString("ddHHmmssffff")));
                var partsPurchaseSettleBill = GetPartsPurchaseSettleBillWithPartsSalesCategory();
                if(ids != null && ids.Any()) {
                    partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => ids.Contains(r.Id));
                } else {
                    if(branchId.HasValue) {
                        partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.BranchId == branchId.Value);
                    }
                    if(!string.IsNullOrEmpty(branchName)) {
                        partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.BranchName.Contains(branchName));
                    }
                    if(partsSupplierId.HasValue) {
                        partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.PartsSupplierId == partsSupplierId.Value);
                    }
                    if(!string.IsNullOrEmpty(code)) {
                        partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.Code.Contains(code));
                    }
                    if(warehouseId.HasValue) {
                        partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.WarehouseId == warehouseId);
                    }
                    if(!string.IsNullOrEmpty(partsSupplierCode)) {
                        partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.PartsSupplierCode.Contains(partsSupplierCode));
                    }
                    if(!string.IsNullOrEmpty(partsSupplierName)) {
                        partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.PartsSupplierName.Contains(partsSupplierName));
                    }
                    if(settlementPath.HasValue) {
                        partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.SettlementPath == settlementPath.Value);
                    }
                    if(partsSalesCategoryId.HasValue) {
                        partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId);
                    }
                    if(startDateTime.HasValue) {
                        partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.CreateTime >= startDateTime.Value);
                    }
                    if(endDateTime.HasValue) {
                        partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.CreateTime <= endDateTime.Value);
                    }
                    if(startInoviceApproveTime.HasValue) {
                        partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.InvoiceApproveTime >= startInoviceApproveTime.Value);
                    }
                    if(endInoviceApproveTime.HasValue) {
                        partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.InvoiceApproveTime <= endInoviceApproveTime.Value);
                    }
                    if(status.HasValue) {
                        partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.Status == status.Value);
                    }
                    if(!string.IsNullOrEmpty(creatorName)) {
                        partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.CreatorName.Contains(creatorName));
                    }
                    if(!string.IsNullOrEmpty(businessCode)) {
                        partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.BusinessCode.Contains(businessCode));
                    }
                }
                var resultIn = (from a in partsPurchaseSettleBill
                    join bt in ObjectContext.PartsPurchaseSettleRefs.Where(r => r.SourceType == (int)DcsPartsPurchaseSettleRefSourceType.配件入库检验单) on a.Id equals bt.PartsPurchaseSettleBillId 
                    join d in ObjectContext.PartsInboundCheckBills on bt.SourceId equals d.Id 
                    join pt in ObjectContext.PartsInboundPlans on d.PartsInboundPlanId equals pt.Id 
                    join st in ObjectContext.SupplierShippingOrders on pt.SourceId equals st.Id 
                    join f in ObjectContext.PartsInboundCheckBillDetails on d.Id equals f.PartsInboundCheckBillId 
                    select new {
                        a.Code,
                        a.BranchCode,
                        a.PartsSalesCategoryName,
                        a.WarehouseName,
                        a.PartsSupplierCode,
                        a.PartsSupplierName,
                        a.TotalSettlementAmount,
                        a.TaxRate,
                        a.Tax,
                        a.SumPlannedPrice,
                        a.InvoiceTotalAmount,
                        a.InvoiceAmountDifference,
                        a.CostAmountDifference,
                        a.Status,
                        a.SettlementPath,
                        a.OffsettedSettlementBillCode,
                        a.Remark,
                        a.CreatorName,
                        a.CreateTime,
                        a.ModifierName,
                        a.ModifyTime,
                        a.ApproverName,
                        a.ApproveTime,
                        a.InvoiceApproverName,
                        a.InvoiceApproveTime,
                        a.AbandonerName,
                        a.AbandonTime,
                        bt.SourceCode,
                        OriginalRequirementBillCode = pt.OriginalRequirementBillCode,
                        YSourceCode= pt.SourceCode,
                        st.DeliveryBillNumber,
                        bt.SourceType,
                        RefWarehouseName = bt.WarehouseName,

                        f.SparePartCode,
                        f.SparePartName,
                        f.SettlementPrice,
                        Quantity = f.InspectedQuantity,
                        Amount = f.SettlementPrice * f.InspectedQuantity
                    });
                var resultOut = (from a in partsPurchaseSettleBill
                    join bt in ObjectContext.PartsPurchaseSettleRefs.Where(r => r.SourceType == (int)DcsPartsPurchaseSettleRefSourceType.配件出库单) on a.Id equals bt.PartsPurchaseSettleBillId 
                    join d in ObjectContext.PartsOutboundBills on bt.SourceId equals d.Id 
                    join pt in ObjectContext.PartsOutboundPlans on d.PartsOutboundPlanId equals pt.Id 
                    join st in ObjectContext.PartsPurReturnOrders on pt.SourceId equals st.Id 
                    join f in ObjectContext.PartsOutboundBillDetails on d.Id equals f.PartsOutboundBillId 
                    select new {
                        a.Code,
                        a.BranchCode,
                        a.PartsSalesCategoryName,
                        a.WarehouseName,
                        a.PartsSupplierCode,
                        a.PartsSupplierName,
                        a.TotalSettlementAmount,
                        a.TaxRate,
                         a.Tax,
                        a.SumPlannedPrice,
                        a.InvoiceTotalAmount,
                        a.InvoiceAmountDifference,
                        a.CostAmountDifference,
                        a.Status,
                        a.SettlementPath,
                        a.OffsettedSettlementBillCode,
                        a.Remark,
                        a.CreatorName,
                        a.CreateTime,
                        a.ModifierName,
                        a.ModifyTime,
                        a.ApproverName,
                        a.ApproveTime,
                        a.InvoiceApproverName,
                        a.InvoiceApproveTime,
                        a.AbandonerName,
                        a.AbandonTime,
                        bt.SourceCode,
                        OriginalRequirementBillCode = st.Code,
                        YSourceCode = "",
                        DeliveryBillNumber="",
                        bt.SourceType,
                        RefWarehouseName = bt.WarehouseName,
                        f.SparePartCode,
                        f.SparePartName,
                        f.SettlementPrice,
                        Quantity = -(f.OutboundAmount),
                        Amount = -(f.SettlementPrice * f.OutboundAmount)
                    });
                var result = (resultIn.Union(resultOut)).OrderByDescending(r => r.InvoiceApproveTime).ToArray();
                if(result.Any()) {
                    using(var excelExport = new ExcelExport(exportDataFileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == result.Count() + 1)
                                return null;
                            if(index == 0)
                                return excelColumns;
                            var detail = result[index - 1];
                            var values = new object[] {
                                detail.Code, detail.BranchCode, detail.PartsSalesCategoryName, detail.WarehouseName, detail.PartsSupplierCode, detail.PartsSupplierName, detail.TotalSettlementAmount, detail.TaxRate,
                                // detail.SumPlannedPrice,
                                 detail.Tax,detail.InvoiceTotalAmount, detail.InvoiceAmountDifference, detail.CostAmountDifference, Enum.GetName(typeof(DcsPartsPurchaseSettleStatus), detail.Status), Enum.GetName(typeof(DcsPartsPurchaseSettleBillSettlementPath), detail.SettlementPath), detail.OffsettedSettlementBillCode, detail.Remark, detail.CreatorName,string.Format("{0:yyyy年MM月dd HH:mm:ss}",detail.CreateTime) , detail.ModifierName, string.Format("{0:yyyy年MM月dd HH:mm:ss}",detail.ModifyTime), detail.ApproverName, string.Format("{0:yyyy年MM月dd HH:mm:ss}",detail.ApproveTime), detail.InvoiceApproverName, string.Format("{0:yyyy年MM月dd HH:mm:ss}",detail.InvoiceApproveTime), detail.AbandonerName, string.Format("{0:yyyy年MM月dd HH:mm:ss}",detail.AbandonTime), detail.SourceCode, detail.OriginalRequirementBillCode,detail.YSourceCode,detail.DeliveryBillNumber,  Enum.GetName(typeof(DcsPartsPurchaseSettleRefSourceType),detail.SourceType), detail.RefWarehouseName, detail.SparePartCode, detail.SparePartName, detail.SettlementPrice, detail.Quantity, detail.Amount,
                            };
                            return values;
                        });
                    }
                }
                return exportDataFileName;
        }
        public string ExportPartsPurchaseSettleBillWithDetailBySupplier(int[] ids, string branchName, int? partsSupplierId, string code, int? warehouseId, int? settlementPath, int? partsSalesCategoryId, DateTime? startDateTime, DateTime? endDateTime, int? status,  string fileName) {
                var excelColumns = new object[] {
                    ErrorStrings.ExportColumn_SettlementCode,
					ErrorStrings.ExportColumn_Branch,
					ErrorStrings.ExportColumn_PartsSalesCategory,
					ErrorStrings.ExportColumn_WarehouseName,
					ErrorStrings.ExportColumn_SupplierCode2,
					ErrorStrings.ExportColumn_SupplierName,
					ErrorStrings.ExportColumn_SettlementTotalAmount,
					ErrorStrings.ExportColumn_TaxRate,
					ErrorStrings.ExportColumn_TaxFee,
					ErrorStrings.ExportColumn_InvoiceTotalAmount,
					ErrorStrings.ExportColumn_InvoiceDifference,
					ErrorStrings.ExportColumn_CostDifference,
					ErrorStrings.ExportColumn_Status,
					ErrorStrings.ExportColumn_SettlementPath,
					ErrorStrings.ExportColumn_OffsettedSettlementBillCode,
					ErrorStrings.ExportColumn_Remark,
					ErrorStrings.ExportColumn_Creator,
					ErrorStrings.ExportColumn_CreateTime,
					ErrorStrings.ExportColumn_Modifier,
					ErrorStrings.ExportColumn_ModifyTime,
					ErrorStrings.ExportColumn_Approver,
					ErrorStrings.ExportColumn_ApproveTime,
					ErrorStrings.ExportColumn_InvoiceApproverName,
					ErrorStrings.ExportColumn_InvoiceApproveTime,
					ErrorStrings.ExportColumn_Abandoner,
					ErrorStrings.ExportColumn_AbandonTime,
					ErrorStrings.ExportColumn_ContactOrderCode,
					ErrorStrings.ExportColumn_OriginalRequirementBillCode,
					ErrorStrings.ExportColumn_SourceCode,
					ErrorStrings.ExportColumn_DeliveryBillNumber,
					ErrorStrings.ExportColumn_OrderType2,
					ErrorStrings.ExportColumn_WarehouseName,
					"供应商图号",
					ErrorStrings.ExportColumn_SparePartName,
					ErrorStrings.ExportColumn_SettlementPrice,
					ErrorStrings.ExportColumn_SettlementQuantity,
					ErrorStrings.ExportColumn_SettlementAmount

                };
                var exportDataFileName = GetExportFilePath(string.Format("{0}_{1}.xlsx", fileName, DateTime.Now.ToString("ddHHmmssffff")));
                var partsPurchaseSettleBill = GetPartsPurchaseSettleBillWithPartsSalesCategory();
                if(ids != null && ids.Any()) {
                    partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => ids.Contains(r.Id));
                } else {
                  
                    if(!string.IsNullOrEmpty(branchName)) {
                        partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.BranchName.Contains(branchName));
                    }
                    if(partsSupplierId.HasValue) {
                        partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.PartsSupplierId == partsSupplierId.Value);
                    }
                    if(!string.IsNullOrEmpty(code)) {
                        partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.Code.Contains(code));
                    }
                    if(warehouseId.HasValue) {
                        partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.WarehouseId == warehouseId);
                    }                   
                    if(settlementPath.HasValue) {
                        partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.SettlementPath == settlementPath.Value);
                    }
                    if(partsSalesCategoryId.HasValue) {
                        partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId);
                    }
                    if(startDateTime.HasValue) {
                        partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.CreateTime >= startDateTime.Value);
                    }
                    if(endDateTime.HasValue) {
                        partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.CreateTime <= endDateTime.Value);
                    }                  
                    if(status.HasValue) {
                        partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.Status == status.Value);
                    }
                }
                var resultIn = (from a in partsPurchaseSettleBill
                    join bt in ObjectContext.PartsPurchaseSettleRefs.Where(r => r.SourceType == (int)DcsPartsPurchaseSettleRefSourceType.配件入库检验单) on a.Id equals bt.PartsPurchaseSettleBillId 
                    join d in ObjectContext.PartsInboundCheckBills on bt.SourceId equals d.Id 
                    join pt in ObjectContext.PartsInboundPlans on d.PartsInboundPlanId equals pt.Id 
                    join st in ObjectContext.SupplierShippingOrders on pt.SourceId equals st.Id 
                    join f in ObjectContext.PartsInboundCheckBillDetails on d.Id equals f.PartsInboundCheckBillId 
					 join c in ObjectContext.PartsSupplierRelations on new {
                             f.SparePartId,
                             a.PartsSupplierId
                         } equals new {
                             SparePartId = c.PartId,
                             PartsSupplierId = c.SupplierId
                         } into t
						  from t1 in t.DefaultIfEmpty()
                    select new {
                        a.Code,
                        a.BranchCode,
                        a.PartsSalesCategoryName,
                        a.WarehouseName,
                        a.PartsSupplierCode,
                        a.PartsSupplierName,
                        a.TotalSettlementAmount,
                        a.TaxRate,
                        a.Tax,
                        a.SumPlannedPrice,
                        a.InvoiceTotalAmount,
                        a.InvoiceAmountDifference,
                        a.CostAmountDifference,
                        a.Status,
                        a.SettlementPath,
                        a.OffsettedSettlementBillCode,
                        a.Remark,
                        a.CreatorName,
                        a.CreateTime,
                        a.ModifierName,
                        a.ModifyTime,
                        a.ApproverName,
                        a.ApproveTime,
                        a.InvoiceApproverName,
                        a.InvoiceApproveTime,
                        a.AbandonerName,
                        a.AbandonTime,
                        bt.SourceCode,
                        OriginalRequirementBillCode = pt.OriginalRequirementBillCode,
                        YSourceCode= pt.SourceCode,
                        st.DeliveryBillNumber,
                        bt.SourceType,
                        RefWarehouseName = bt.WarehouseName,
                        t1.SupplierPartCode,
                        f.SparePartName,
                        f.SettlementPrice,
                        Quantity = f.InspectedQuantity,
                        Amount = f.SettlementPrice * f.InspectedQuantity
                    });
                var resultOut = (from a in partsPurchaseSettleBill
                    join bt in ObjectContext.PartsPurchaseSettleRefs.Where(r => r.SourceType == (int)DcsPartsPurchaseSettleRefSourceType.配件出库单) on a.Id equals bt.PartsPurchaseSettleBillId 
                    join d in ObjectContext.PartsOutboundBills on bt.SourceId equals d.Id 
                    join pt in ObjectContext.PartsOutboundPlans on d.PartsOutboundPlanId equals pt.Id 
                    join st in ObjectContext.PartsPurReturnOrders on pt.SourceId equals st.Id 
                    join f in ObjectContext.PartsOutboundBillDetails on d.Id equals f.PartsOutboundBillId 
					join c in ObjectContext.PartsSupplierRelations on new {
                             f.SparePartId,
                             a.PartsSupplierId
                         } equals new {
                             SparePartId = c.PartId,
                             PartsSupplierId = c.SupplierId
                         } into t
						  from t1 in t.DefaultIfEmpty()
                    select new {
                        a.Code,
                        a.BranchCode,
                        a.PartsSalesCategoryName,
                        a.WarehouseName,
                        a.PartsSupplierCode,
                        a.PartsSupplierName,
                        a.TotalSettlementAmount,
                        a.TaxRate,
                         a.Tax,
                        a.SumPlannedPrice,
                        a.InvoiceTotalAmount,
                        a.InvoiceAmountDifference,
                        a.CostAmountDifference,
                        a.Status,
                        a.SettlementPath,
                        a.OffsettedSettlementBillCode,
                        a.Remark,
                        a.CreatorName,
                        a.CreateTime,
                        a.ModifierName,
                        a.ModifyTime,
                        a.ApproverName,
                        a.ApproveTime,
                        a.InvoiceApproverName,
                        a.InvoiceApproveTime,
                        a.AbandonerName,
                        a.AbandonTime,
                        bt.SourceCode,
                        OriginalRequirementBillCode = st.Code,
                        YSourceCode = "",
                        DeliveryBillNumber="",
                        bt.SourceType,
                        RefWarehouseName = bt.WarehouseName,
                        t1.SupplierPartCode,
                        f.SparePartName,
                        f.SettlementPrice,
                        Quantity = -(f.OutboundAmount),
                        Amount = -(f.SettlementPrice * f.OutboundAmount)
                    });
                var result = (resultIn.Union(resultOut)).OrderByDescending(r => r.InvoiceApproveTime).ToArray();
                if(result.Any()) {
                    using(var excelExport = new ExcelExport(exportDataFileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == result.Count() + 1)
                                return null;
                            if(index == 0)
                                return excelColumns;
                            var detail = result[index - 1];
                            var values = new object[] {
                                detail.Code, detail.BranchCode, detail.PartsSalesCategoryName, detail.WarehouseName, detail.PartsSupplierCode, detail.PartsSupplierName, detail.TotalSettlementAmount, detail.TaxRate,
                                // detail.SumPlannedPrice,
                                 detail.Tax,detail.InvoiceTotalAmount, detail.InvoiceAmountDifference, detail.CostAmountDifference, Enum.GetName(typeof(DcsPartsPurchaseSettleStatus), detail.Status), Enum.GetName(typeof(DcsPartsPurchaseSettleBillSettlementPath), detail.SettlementPath), detail.OffsettedSettlementBillCode, detail.Remark, detail.CreatorName,string.Format("{0:yyyy年MM月dd HH:mm:ss}",detail.CreateTime) , detail.ModifierName, string.Format("{0:yyyy年MM月dd HH:mm:ss}",detail.ModifyTime), detail.ApproverName, string.Format("{0:yyyy年MM月dd HH:mm:ss}",detail.ApproveTime), detail.InvoiceApproverName, string.Format("{0:yyyy年MM月dd HH:mm:ss}",detail.InvoiceApproveTime), detail.AbandonerName, string.Format("{0:yyyy年MM月dd HH:mm:ss}",detail.AbandonTime), detail.SourceCode, detail.OriginalRequirementBillCode,detail.YSourceCode,detail.DeliveryBillNumber,  Enum.GetName(typeof(DcsPartsPurchaseSettleRefSourceType),detail.SourceType), detail.RefWarehouseName, detail.SupplierPartCode, detail.SparePartName, detail.SettlementPrice, detail.Quantity, detail.Amount,
                            };
                            return values;
                        });
                    }
                }
                return exportDataFileName;
        }
        //采购结算管理-导出（含是否统一结算的过滤）
        public string ExportPartsPurchaseSettleBillByUnifiedSettle(int[] ids, int? branchId, string branchName, int? partsSupplierId, string code, int? warehouseId, string partsSupplierCode, string partsSupplierName, int? settlementPath, int? partsSalesCategoryId, DateTime? startDateTime, DateTime? endDateTime, int? status, string creatorName, DateTime? startInoviceApproveTime, DateTime? endInoviceApproveTime, string businessCode, string fileName, DateTime? startInvoiceDate, DateTime? endInvoiceDate)
        {
            var excelColumns = new object[] {
				ErrorStrings.ExportColumn_SettlementCode,
				ErrorStrings.ExportColumn_Branch,
				ErrorStrings.ExportColumn_PartsSalesCategory,
				ErrorStrings.ExportColumn_WarehouseName,
				ErrorStrings.ExportColumn_SupplierCode2,
				ErrorStrings.ExportColumn_SupplierName,
				ErrorStrings.ExportColumn_SettlementTotalAmount,
				ErrorStrings.ExportColumn_TaxRate,
				ErrorStrings.ExportColumn_TaxFee,
				ErrorStrings.ExportColumn_InvoiceTotalAmount,
				ErrorStrings.ExportColumn_InvoiceDifference,
				ErrorStrings.ExportColumn_CostDifference,
				ErrorStrings.ExportColumn_Status,
				ErrorStrings.ExportColumn_SettlementPath,
				ErrorStrings.ExportColumn_OffsettedSettlementBillCode,
				ErrorStrings.ExportColumn_Remark,
				ErrorStrings.ExportColumn_Creator,
				ErrorStrings.ExportColumn_CreateTime,
				ErrorStrings.ExportColumn_Modifier,
				ErrorStrings.ExportColumn_ModifyTime,
				ErrorStrings.ExportColumn_Approver,
				ErrorStrings.ExportColumn_ApproveTime,
				ErrorStrings.ExportColumn_InvoiceApproverName,
				ErrorStrings.ExportColumn_InvoiceApproveTime,
				ErrorStrings.ExportColumn_Abandoner,
				ErrorStrings.ExportColumn_AbandonTime,
                ErrorStrings.ExportColumn_InvoiceDate2
            };
            var exportDataFileName = GetExportFilePath(string.Format("{0}_{1}.xlsx", fileName, DateTime.Now.ToString("ddHHmmssffff")));
            var partsPurchaseSettleBill = GetPartsPurchaseSettleBillWithPartsSalesCategoryByUnifiedSettle();            
            if (ids != null && ids.Any())
            {
                partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => ids.Contains(r.Id));
            }
            else
            {
                if (branchId.HasValue)
                {
                    partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.BranchId == branchId.Value);
                }
                if (!string.IsNullOrEmpty(branchName))
                {
                    partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.BranchName.Contains(branchName));
                }
                if (partsSupplierId.HasValue)
                {
                    partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.PartsSupplierId == partsSupplierId.Value);
                }
                if (!string.IsNullOrEmpty(code))
                {
                    partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.Code.Contains(code));
                }
                if (warehouseId.HasValue)
                {
                    partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.WarehouseId == warehouseId);
                }
                if (!string.IsNullOrEmpty(partsSupplierCode))
                {
                    partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.PartsSupplierCode.Contains(partsSupplierCode));
                }
                if (!string.IsNullOrEmpty(partsSupplierName))
                {
                    partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.PartsSupplierName.Contains(partsSupplierName));
                }
                if (settlementPath.HasValue)
                {
                    partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.SettlementPath == settlementPath.Value);
                }
                if (partsSalesCategoryId.HasValue)
                {
                    partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId);
                }
                if (startDateTime.HasValue)
                {
                    partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.CreateTime >= startDateTime.Value);
                }
                if (endDateTime.HasValue)
                {
                    partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.CreateTime <= endDateTime.Value);
                }
                if (startInoviceApproveTime.HasValue)
                {
                    partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.InvoiceApproveTime >= startInoviceApproveTime.Value);
                }
                if (endInoviceApproveTime.HasValue)
                {
                    partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.InvoiceApproveTime <= endInoviceApproveTime.Value);
                }
                if (status.HasValue)
                {
                    partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.Status == status.Value);
                }
                if (!string.IsNullOrEmpty(creatorName))
                {
                    partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.CreatorName.Contains(creatorName));
                }
                if (!string.IsNullOrEmpty(businessCode))
                {
                    partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.BusinessCode.Contains(businessCode));
                }
                if (startInvoiceDate.HasValue)
                {
                    partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.InvoiceDate >= startInvoiceDate.Value);
                }
                if (endInvoiceDate.HasValue)
                {
                    partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.InvoiceDate <= endInvoiceDate.Value);
                }
            }
            var result = partsPurchaseSettleBill.OrderByDescending(r => r.InvoiceApproveTime).ToArray();
            if (result.Any())
            {
                using (var excelExport = new ExcelExport(exportDataFileName))
                {
                    excelExport.ExportByRow(index => {
                        if (index == result.Count() + 1)
                            return null;
                        if (index == 0)
                            return excelColumns;
                        var detail = result[index - 1];
                        var values = new object[] {
                            detail.Code,
                            detail.BranchCode,
                            detail.PartsSalesCategoryName,
                            detail.WarehouseName,
                            detail.PartsSupplierCode,
                            detail.PartsSupplierName,
                            detail.TotalSettlementAmount,
                            detail.TaxRate,
                            //detail.Tax,
                            detail.SumPlannedPrice,
                            detail.InvoiceTotalAmount,
                            detail.InvoiceAmountDifference,
                            detail.CostAmountDifference,
                            Enum.GetName(typeof(DcsPartsPurchaseSettleStatus),detail.Status),
                            Enum.GetName(typeof(DcsPartsPurchaseSettleBillSettlementPath),detail.SettlementPath),
                            detail.OffsettedSettlementBillCode,
                            detail.Remark,
                            detail.CreatorName,
                            new StyleDataField(detail.CreateTime,"yyyy年m月d日 hh:mm:ss"),
                            detail.ModifierName,
                            new StyleDataField(detail.ModifyTime,"yyyy年m月d日 hh:mm:ss"),
                            detail.ApproverName,
                            new StyleDataField(detail.ApproveTime,"yyyy年m月d日 hh:mm:ss"),
                            detail.InvoiceApproverName,
                            new StyleDataField(detail.InvoiceApproveTime,"yyyy年m月d日 hh:mm:ss"),
                            detail.AbandonerName,
                            detail.AbandonTime,
                            detail.InvoiceDate
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }

        //采购结算管理-合并导出（含是否统一结算的过滤）
        public string ExportPartsPurchaseSettleBillWithDetailByUnifiedSettle(int[] ids, int? branchId, string branchName, int? partsSupplierId, string code, int? warehouseId, string partsSupplierCode, string partsSupplierName, int? settlementPath, int? partsSalesCategoryId, DateTime? startDateTime, DateTime? endDateTime, int? status, string creatorName, DateTime? startInoviceApproveTime, DateTime? endInoviceApproveTime, string businessCode, string fileName, DateTime? startInvoiceDate, DateTime? endInvoiceDate)
        {
            var excelColumns = new object[] {
					ErrorStrings.ExportColumn_SettlementCode,
					ErrorStrings.ExportColumn_Branch,
					ErrorStrings.ExportColumn_PartsSalesCategory,
					ErrorStrings.ExportColumn_WarehouseName,
					ErrorStrings.ExportColumn_SupplierCode2,
					ErrorStrings.ExportColumn_SupplierName,
					ErrorStrings.ExportColumn_SettlementTotalAmount,
					ErrorStrings.ExportColumn_TaxRate,
					ErrorStrings.ExportColumn_TaxFee,
					ErrorStrings.ExportColumn_InvoiceTotalAmount,
					ErrorStrings.ExportColumn_InvoiceDifference,
					ErrorStrings.ExportColumn_CostDifference,
					ErrorStrings.ExportColumn_Status,
					ErrorStrings.ExportColumn_InvoiceDate2,
					ErrorStrings.ExportColumn_SettlementPath,
					ErrorStrings.ExportColumn_OffsettedSettlementBillCode,
					ErrorStrings.ExportColumn_Remark, 
					ErrorStrings.ExportColumn_Creator,
					ErrorStrings.ExportColumn_CreateTime,
					ErrorStrings.ExportColumn_Modifier,
					ErrorStrings.ExportColumn_ModifyTime,
					ErrorStrings.ExportColumn_Approver,
					ErrorStrings.ExportColumn_ApproveTime,
					ErrorStrings.ExportColumn_InvoiceApproverName,
					ErrorStrings.ExportColumn_InvoiceApproveTime,
					ErrorStrings.ExportColumn_Abandoner,
					ErrorStrings.ExportColumn_AbandonTime,
					ErrorStrings.ExportColumn_ContactOrderCode,
					ErrorStrings.ExportColumn_OriginalRequirementBillCode,
					ErrorStrings.ExportColumn_SourceCode,
					ErrorStrings.ExportColumn_DeliveryBillNumber, 
					ErrorStrings.ExportColumn_OrderType2,
					ErrorStrings.ExportColumn_WarehouseName,
					ErrorStrings.ExportColumn_PartCode,
					ErrorStrings.ExportColumn_SparePartName,
					ErrorStrings.ExportColumn_SettlementPrice,
					ErrorStrings.ExportColumn_SettlementQuantity,
					ErrorStrings.ExportColumn_SettlementAmount

                };
            var exportDataFileName = GetExportFilePath(string.Format("{0}_{1}.xlsx", fileName, DateTime.Now.ToString("ddHHmmssffff")));
            var partsPurchaseSettleBill = GetPartsPurchaseSettleBillWithPartsSalesCategoryByUnifiedSettle();
            if (ids != null && ids.Any())
            {
                partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => ids.Contains(r.Id));
            }
            else
            {
                if (branchId.HasValue)
                {
                    partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.BranchId == branchId.Value);
                }
                if (!string.IsNullOrEmpty(branchName))
                {
                    partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.BranchName.Contains(branchName));
                }
                if (partsSupplierId.HasValue)
                {
                    partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.PartsSupplierId == partsSupplierId.Value);
                }
                if (!string.IsNullOrEmpty(code))
                {
                    partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.Code.Contains(code));
                }
                if (warehouseId.HasValue)
                {
                    partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.WarehouseId == warehouseId);
                }
                if (!string.IsNullOrEmpty(partsSupplierCode))
                {
                    partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.PartsSupplierCode.Contains(partsSupplierCode));
                }
                if (!string.IsNullOrEmpty(partsSupplierName))
                {
                    partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.PartsSupplierName.Contains(partsSupplierName));
                }
                if (settlementPath.HasValue)
                {
                    partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.SettlementPath == settlementPath.Value);
                }
                if (partsSalesCategoryId.HasValue)
                {
                    partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId);
                }
                if (startDateTime.HasValue)
                {
                    partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.CreateTime >= startDateTime.Value);
                }
                if (endDateTime.HasValue)
                {
                    partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.CreateTime <= endDateTime.Value);
                }
                if (startInoviceApproveTime.HasValue)
                {
                    partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.InvoiceApproveTime >= startInoviceApproveTime.Value);
                }
                if (endInoviceApproveTime.HasValue)
                {
                    partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.InvoiceApproveTime <= endInoviceApproveTime.Value);
                }
                if (status.HasValue)
                {
                    partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.Status == status.Value);
                }
                if (!string.IsNullOrEmpty(creatorName))
                {
                    partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.CreatorName.Contains(creatorName));
                }
                if (!string.IsNullOrEmpty(businessCode))
                {
                    partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.BusinessCode.Contains(businessCode));
                }
                if (startInvoiceDate.HasValue)
                {
                    partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.InvoiceDate >= startInvoiceDate.Value);
                }
                if (endInvoiceDate.HasValue)
                {
                    partsPurchaseSettleBill = partsPurchaseSettleBill.Where(r => r.InvoiceDate <= endInvoiceDate.Value);
                }
            }
            var resultIn = (from a in partsPurchaseSettleBill
                            join b in ObjectContext.PartsPurchaseSettleRefs.Where(r => r.SourceType == (int)DcsPartsPurchaseSettleRefSourceType.配件入库检验单) on a.Id equals b.PartsPurchaseSettleBillId
                            join c in ObjectContext.PartsInboundCheckBills on b.SourceId equals c.Id into t1
                            from d in t1.DefaultIfEmpty()
                            join p in ObjectContext.PartsInboundPlans on d.PartsInboundPlanId equals p.Id
                            join s in ObjectContext.SupplierShippingOrders on p.SourceId equals s.Id
                            join h in ObjectContext.PartsPurchaseOrders on s.PartsPurchaseOrderId equals h.Id
                            join e in ObjectContext.PartsInboundCheckBillDetails on d.Id equals e.PartsInboundCheckBillId into t2
                            from f in t2.DefaultIfEmpty()
                            join g in ObjectContext.SupplierShippingOrders on new {
                               Id= p.SourceId,
                               Code= p.SourceCode
                            } equals new {
                                g.Id,
                                g.Code
                            } into gg 
                            from gt in gg.DefaultIfEmpty() 

                            select new {
                                a.Code,
                                a.BranchCode,
                                a.PartsSalesCategoryName,
                                a.WarehouseName,
                                a.PartsSupplierCode,
                                a.PartsSupplierName,
                                a.TotalSettlementAmount,
                                a.TaxRate,
                                //a.Tax,
                                a.SumPlannedPrice,
                                a.InvoiceTotalAmount,
                                a.InvoiceAmountDifference,
                                a.CostAmountDifference,
                                a.Status,
                                a.InvoiceDate,
                                a.SettlementPath,
                                a.OffsettedSettlementBillCode,
                                a.Remark,
                                a.CreatorName,
                                a.CreateTime,
                                a.ModifierName,
                                a.ModifyTime,
                                a.ApproverName,
                                a.ApproveTime,
                                a.InvoiceApproverName,
                                a.InvoiceApproveTime,
                                a.AbandonerName,
                                a.AbandonTime,

                                b.SourceCode,
                                YSourceCode = p.SourceCode,
                                gt.DeliveryBillNumber,
                                OriginalRequirementBillCode = h.Code,
                                b.SourceType,
                                RefWarehouseName = b.WarehouseName,

                                f.SparePartCode,
                                f.SparePartName,
                                f.SettlementPrice,
                                Quantity = f.InspectedQuantity,
                                Amount = f.SettlementPrice * f.InspectedQuantity
                            });
            var resultOut = (from a in partsPurchaseSettleBill
                             join b in ObjectContext.PartsPurchaseSettleRefs.Where(r => r.SourceType == (int)DcsPartsPurchaseSettleRefSourceType.配件出库单) on a.Id equals b.PartsPurchaseSettleBillId
                             join c in ObjectContext.PartsOutboundBills on b.SourceId equals c.Id into t1
                             from d in t1.DefaultIfEmpty()
                             join p in ObjectContext.PartsOutboundPlans on d.PartsOutboundPlanId equals p.Id
                             join s in ObjectContext.PartsPurReturnOrders on p.SourceId equals s.Id
                             join e in ObjectContext.PartsOutboundBillDetails on d.Id equals e.PartsOutboundBillId into t2
                             from f in t2.DefaultIfEmpty()
                             select new
                             {
                                 a.Code,
                                 a.BranchCode,
                                 a.PartsSalesCategoryName,
                                 a.WarehouseName,
                                 a.PartsSupplierCode,
                                 a.PartsSupplierName,
                                 a.TotalSettlementAmount,
                                 a.TaxRate,
                                 // a.Tax,
                                 a.SumPlannedPrice,
                                 a.InvoiceTotalAmount,
                                 a.InvoiceAmountDifference,
                                 a.CostAmountDifference,
                                 a.Status,
                                 a.InvoiceDate,
                                 a.SettlementPath,
                                 a.OffsettedSettlementBillCode,
                                 a.Remark,
                                 a.CreatorName,
                                 a.CreateTime,
                                 a.ModifierName,
                                 a.ModifyTime,
                                 a.ApproverName,
                                 a.ApproveTime,
                                 a.InvoiceApproverName,
                                 a.InvoiceApproveTime,
                                 a.AbandonerName,
                                 a.AbandonTime,
                                 b.SourceCode,
                                 YSourceCode="",
                                 DeliveryBillNumber="",
                                 OriginalRequirementBillCode = s.Code,
                                 b.SourceType,
                                 RefWarehouseName = b.WarehouseName,
                                 f.SparePartCode,
                                 f.SparePartName,
                                 f.SettlementPrice,
                                 Quantity = -(f.OutboundAmount),
                                 Amount = -(f.SettlementPrice * f.OutboundAmount)
                             });
            var result = (resultIn.Union(resultOut)).OrderByDescending(r => r.InvoiceApproveTime).ToArray();
            if (result.Any())
            {
                using (var excelExport = new ExcelExport(exportDataFileName))
                {
                    excelExport.ExportByRow(index => {
                        if (index == result.Count() + 1)
                            return null;
                        if (index == 0)
                            return excelColumns;
                        var detail = result[index - 1];
                        var values = new object[] {
                                detail.Code, detail.BranchCode, detail.PartsSalesCategoryName, detail.WarehouseName, detail.PartsSupplierCode, detail.PartsSupplierName, detail.TotalSettlementAmount, detail.TaxRate,
                                //detail.Tax,
                                detail.SumPlannedPrice, detail.InvoiceTotalAmount, detail.InvoiceAmountDifference, detail.CostAmountDifference, Enum.GetName(typeof(DcsPartsPurchaseSettleStatus), detail.Status),detail.InvoiceDate, Enum.GetName(typeof(DcsPartsPurchaseSettleBillSettlementPath), detail.SettlementPath), detail.OffsettedSettlementBillCode, detail.Remark, detail.CreatorName,string.Format("{0:yyyy年MM月dd HH:mm:ss}",detail.CreateTime) , detail.ModifierName, string.Format("{0:yyyy年MM月dd HH:mm:ss}",detail.ModifyTime), detail.ApproverName, string.Format("{0:yyyy年MM月dd HH:mm:ss}",detail.ApproveTime), detail.InvoiceApproverName, string.Format("{0:yyyy年MM月dd HH:mm:ss}",detail.InvoiceApproveTime), detail.AbandonerName, string.Format("{0:yyyy年MM月dd HH:mm:ss}",detail.AbandonTime), detail.SourceCode, detail.OriginalRequirementBillCode,detail.YSourceCode,detail.DeliveryBillNumber, Enum.GetName(typeof(DcsPartsPurchaseSettleRefSourceType), detail.SourceType), detail.RefWarehouseName, detail.SparePartCode, detail.SparePartName, detail.SettlementPrice, detail.Quantity, detail.Amount,
                            };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }

    }
}