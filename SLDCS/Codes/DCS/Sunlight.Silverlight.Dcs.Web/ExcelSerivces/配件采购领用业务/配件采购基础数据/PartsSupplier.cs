﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string ExportPartsSupplier(int[] ids, string code, string name, int? status, int? supplierType) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("配件供应商_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var partsSupplieres = GetPartsSuppliersWithCompany();

            if(ids != null && ids.Any()) {
                partsSupplieres = partsSupplieres.Where(r => ids.Contains(r.Id));
            } else {
                if(!string.IsNullOrEmpty(code)) {
                    partsSupplieres = partsSupplieres.Where(r => r.Code.Contains(code));
                }
                if(!string.IsNullOrEmpty(name)) {
                    partsSupplieres = partsSupplieres.Where(r => r.Name.Contains(name));
                }
                if(status.HasValue) {
                    partsSupplieres = partsSupplieres.Where(r => r.Status == status.Value);
                }
                if(supplierType.HasValue) {
                    partsSupplieres = partsSupplieres.Where(r => r.SupplierType == supplierType.Value);
                }
            }

            var companyInvoiceInfoesArray = partsSupplieres.ToArray();
            if(partsSupplieres.Any()) {
                excelColumns.Add(ErrorStrings.ExportColumn_SupplierCode2);
                excelColumns.Add(ErrorStrings.ExportColumn_SupplierName);
                
                excelColumns.Add(ErrorStrings.ExportColumn_ShortName);
                excelColumns.Add(ErrorStrings.ExportColumn_ProvinceName);
                excelColumns.Add(ErrorStrings.ExportColumn_RegionName);
                excelColumns.Add(ErrorStrings.ExportColumn_CountryName);
                excelColumns.Add(ErrorStrings.ExportColumn_CompanyAddress);
                excelColumns.Add(ErrorStrings.ExportColumn_ContactPerson1);
                excelColumns.Add("短信电话");
                excelColumns.Add(ErrorStrings.ExportColumn_ContactPersonPhone1);
                excelColumns.Add(ErrorStrings.ExportColumn_Fax);
                excelColumns.Add(ErrorStrings.ExportColumn_Email);
                excelColumns.Add(ErrorStrings.ExportColumn_ContactPerson2);
                excelColumns.Add(ErrorStrings.ExportColumn_ContactPersonPhone2);
                excelColumns.Add(ErrorStrings.ExportColumn_Email2);
                excelColumns.Add(ErrorStrings.ExportColumn_LegalRepresentative);
                excelColumns.Add(ErrorStrings.ExportColumn_BusinessScope);
                excelColumns.Add(ErrorStrings.ExportColumn_SupplierStatus);
                excelColumns.Add(ErrorStrings.ExportColumn_Status);
                excelColumns.Add(ErrorStrings.ExportColumn_Remark);
                excelColumns.Add(ErrorStrings.ExportColumn_Creator);
                excelColumns.Add(ErrorStrings.ExportColumn_CreateTime);
                excelColumns.Add(ErrorStrings.ExportColumn_Modifier);
                excelColumns.Add(ErrorStrings.ExportColumn_ModifyTime);

                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == companyInvoiceInfoesArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = companyInvoiceInfoesArray[index - 1];
                        var values = new object[] {
                            detail.Code,
                            detail.Name,
                            detail.ShortName,
                            detail.Company.ProvinceName,
                            detail.Company.CityName,
                            detail.Company.CountyName,
                            detail.Company.ContactAddress,
                            detail.Company.ContactPerson,
                            detail.Company.ContactPhone,
                            detail.Company.ContactMobile,
                            detail.Company.Fax,
                            detail.Company.ContactMail,
                            detail.Company.BusinessLinkName,
                            detail.Company.BusinessContactMethod,
                            detail.Company.BusinessLinkerEMail,
                            detail.Company.LegalRepresentative,
                            detail.Company.BusinessScope,
                            detail.SupplierType!=null?Enum.GetName(typeof(DcsPartsSupplierType),detail.SupplierType.Value):"",
                            detail.Status!=null?Enum.GetName(typeof(DcsMasterDataStatus),detail.Status.Value):"",
                            detail.Remark,
                            detail.CreatorName,
                            detail.CreateTime,
                            detail.ModifierName,
                            detail.ModifyTime

                            
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
    }
}
