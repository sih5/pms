﻿using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string 导出采购价格有效到期提醒(int? daysRemaining, string sparepartCode, string supplierPartCode, string partsSupplierCode, string partsSupplierName, int? priceType) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("采购价格有效到期提醒_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = 获取采购价格有效到期提醒(daysRemaining, sparepartCode, supplierPartCode, partsSupplierCode, partsSupplierName, priceType);
            var dataArray = data.ToArray();
            if(data.Any()) {
                excelColumns.Add(ErrorStrings.ExportColumn_PartCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SparePartName);
                excelColumns.Add(ErrorStrings.ExportColumn_SupplierCode2);
                excelColumns.Add(ErrorStrings.ExportColumn_SupplierName);
                excelColumns.Add(ErrorStrings.ExportColumn_IsPrimarySupplier);
                excelColumns.Add(ErrorStrings.ExportColumn_PriceType);
                excelColumns.Add(ErrorStrings.ExportColumn_Price2);
                excelColumns.Add(ErrorStrings.ExportColumn_ValidFrom);
                excelColumns.Add(ErrorStrings.ExportColumn_ValidTo);
                excelColumns.Add(ErrorStrings.ExportColumn_DaysRemaining);
              
                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.SparepartCode,
                            detail.SparepartName,
                            detail.PartsSupplierCode,
                            detail.PartsSupplierName,
                            (detail.IsPrimary==null?false:detail.IsPrimary)==false?EntityStrings.Export_Title_No:EntityStrings.Export_Title_Yes, 
                             Enum.GetName(typeof(DcsPurchasePriceType),detail.PriceType==null?0:detail.PriceType),
                            detail.PurchasePrice,
                            detail.ValidFrom,
                            detail.ValidTo,
                            detail.DaysRemaining
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }

    }
}