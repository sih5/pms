﻿using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web{
    partial class DcsDomainService {
        public string 导出库龄报表统计(string sparePartCode, string sparePartName, string referenceCode) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("库龄报表_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = 库龄报表统计(sparePartCode,sparePartName, referenceCode);
            var dataArray = data.ToArray();
                if(data.Any()) {
                excelColumns.Add(ErrorStrings.ExportColumn_PartCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SIHCode);
                excelColumns.Add(ErrorStrings.ExportColumn_MapName);
                excelColumns.Add(ErrorStrings.ExportColumn_PartProp);
                excelColumns.Add(ErrorStrings.ExportColumn_StockCostAmount);
                excelColumns.Add(ErrorStrings.ExportColumn_Price);
                excelColumns.Add(ErrorStrings.ExportColumn_EnterpriseStock);
                excelColumns.Add(ErrorStrings.ExportColumn_90DaysStock);
                excelColumns.Add(ErrorStrings.ExportColumn_Mondy);
                excelColumns.Add(ErrorStrings.ExportColumn_90To180DaysStock);
                excelColumns.Add(ErrorStrings.ExportColumn_Mondy);
                excelColumns.Add(ErrorStrings.ExportColumn_181To360DaysStock);
                excelColumns.Add(ErrorStrings.ExportColumn_Mondy);
                excelColumns.Add(ErrorStrings.ExportColumn_361To720DaysStock);
                excelColumns.Add(ErrorStrings.ExportColumn_Mondy);
                excelColumns.Add(ErrorStrings.ExportColumn_721To1080DaysStock);
                excelColumns.Add(ErrorStrings.ExportColumn_Mondy);
                excelColumns.Add(ErrorStrings.ExportColumn_AgoStock);
                excelColumns.Add(ErrorStrings.ExportColumn_Mondy);
                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.SparePartCode,
                            detail.ReferenceCode,
                            detail.SparePartName,
                             Enum.GetName(typeof(DcsABCStrategyCategory),detail.PartABC==null?0:detail.PartABC),
                            detail.CostAmount,
                            detail.CostPrice, 
                            detail.Quantity,
                            detail.QuantityOne,
                            detail.QuantityOneAmount,
                            detail.QuantityTwo,
                            detail.QuantityTwoAmount,
                            detail.QuantityThree,
                            detail.QuantityThreeAmount,
                            detail.QuantityFour,
                            detail.QuantityFourAmount,
                            detail.QuantityFive,
                            detail.QuantityFiveAmount,
                            detail.QuantityMore,
                            detail.QuantityMoreAmount
                        };
                        return values;
                    });
                }
                }
                return exportDataFileName;
        }
		  public string 导出中心库库龄报表统计(string sparePartCode, string sparePartName, string referenceCode,string centerName) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("中心库库龄报表_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = 中心库库龄报表统计(sparePartCode,sparePartName, referenceCode,centerName);
            var dataArray = data.ToArray();
                if(data.Any()) {
			    excelColumns.Add(ErrorStrings.ExportColumn_AgencyCode);
				excelColumns.Add(ErrorStrings.ExportColumn_AgencyName);
                excelColumns.Add(ErrorStrings.ExportColumn_PartCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SIHCode);
                excelColumns.Add(ErrorStrings.ExportColumn_MapName);
                excelColumns.Add(ErrorStrings.ExportColumn_PartProp);
                excelColumns.Add(ErrorStrings.ExportColumn_StockCostAmount);
                excelColumns.Add(ErrorStrings.ExportColumn_Price);
                excelColumns.Add(ErrorStrings.ExportColumn_EnterpriseStock);
                excelColumns.Add(ErrorStrings.ExportColumn_90DaysStock);
                excelColumns.Add(ErrorStrings.ExportColumn_Mondy);
                excelColumns.Add(ErrorStrings.ExportColumn_90To180DaysStock);
                excelColumns.Add(ErrorStrings.ExportColumn_Mondy);
                excelColumns.Add(ErrorStrings.ExportColumn_181To360DaysStock);
                excelColumns.Add(ErrorStrings.ExportColumn_Mondy);
                excelColumns.Add(ErrorStrings.ExportColumn_361To720DaysStock);
                excelColumns.Add(ErrorStrings.ExportColumn_Mondy);
                excelColumns.Add(ErrorStrings.ExportColumn_721To1080DaysStock);
                excelColumns.Add(ErrorStrings.ExportColumn_Mondy);
                excelColumns.Add(ErrorStrings.ExportColumn_AgoStock);
                excelColumns.Add(ErrorStrings.ExportColumn_Mondy);
                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
							detail.CenterCode,
							detail.CenterName,
                            detail.SparePartCode,
                            detail.ReferenceCode,
                            detail.SparePartName,
                             Enum.GetName(typeof(DcsABCStrategyCategory),detail.PartABC==null?0:detail.PartABC),
                            detail.CostAmount,
                            detail.CostPrice, 
                            detail.Quantity,
                            detail.QuantityOne,
                            detail.QuantityOneAmount,
                            detail.QuantityTwo,
                            detail.QuantityTwoAmount,
                            detail.QuantityThree,
                            detail.QuantityThreeAmount,
                            detail.QuantityFour,
                            detail.QuantityFourAmount,
                            detail.QuantityFive,
                            detail.QuantityFiveAmount,
                            detail.QuantityMore,
                            detail.QuantityMoreAmount
                        };
                        return values;
                    });
                }
                }
                return exportDataFileName;
        }
    }
}
