﻿using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web
{
    partial class DcsDomainService
    {
        public string 导出临时订单配件资料待维护(string code, string sparePartCode, string suplierCode, string suplierName, bool? isPurchase, bool? isSales)
        {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("临时订单配件资料待维护_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = getTemPurchaseOrderSpareParts(code, sparePartCode, suplierCode, suplierName, isPurchase, isSales);
            var dataArray = data.ToArray();
            if (data.Any())
            {
                excelColumns.Add("临时订单编号");
                excelColumns.Add("配件编号");
                excelColumns.Add("配件名称");
                excelColumns.Add("供应商编号");
                excelColumns.Add("供应商名称");
                excelColumns.Add("采购价");
                excelColumns.Add("中心库价");
                excelColumns.Add("服务站价");
                using (var excelExport = new ExcelExport(exportDataFileName))
                {
                    excelExport.ExportByRow(index =>
                    {
                        if (index == dataArray.Count() + 1)
                            return null;
                        if (index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {                        
                            detail.Code,
                            detail.SparePartCode,
                            detail.SparePartName,
                            detail.SuplierCode, 
                            detail.SuplierName,
                            detail.PurchasePrice,
                            detail.CenterPrice,
                            detail.SalesPrice                          
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }      
    }
}
