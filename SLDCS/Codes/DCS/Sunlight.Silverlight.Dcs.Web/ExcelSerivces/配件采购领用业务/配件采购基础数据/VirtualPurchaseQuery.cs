﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        /// <summary>
        /// 采购需求查询导出
        /// </summary>
        public string ExportPurchaseQuery(int? branchId, int? partsSalesCategoryId, string partCode, string partName, DateTime? startCreateTime, DateTime? endCreateTime) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("采购需求查询_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = 查询配件采购需求();

            if(branchId.HasValue) {
                data = data.Where(r => r.BranchId == branchId.Value);
            }
            if(partsSalesCategoryId.HasValue) {
                data = data.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId.Value);
            }
            if(!string.IsNullOrEmpty(partCode)) {
                data = data.Where(r => r.PartCode.Contains(partCode));
            }
            if(!string.IsNullOrEmpty(partName)) {
                data = data.Where(r => r.PartName.Contains(partName));
            }
            if(startCreateTime.HasValue) {
                data = data.Where(r => r.CreateTime >= startCreateTime.Value);
            }
            if(endCreateTime.HasValue) {
                data = data.Where(r => r.CreateTime <= endCreateTime.Value);
            }
            var dataArray = data.ToArray();
            if(dataArray.Any()) {
                excelColumns.Add(ErrorStrings.ExportColumn_SparePartCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SparePartName);
                excelColumns.Add(ErrorStrings.ExportColumn_StockCostAmount);
                excelColumns.Add(ErrorStrings.ExportColumn_UsableStock);
                excelColumns.Add(ErrorStrings.ExportColumn_OnwayQty);
                excelColumns.Add(ErrorStrings.ExportColumn_UntFulfilledQty);
                excelColumns.Add(ErrorStrings.ExportColumn_OutboundAmount);
                excelColumns.Add(ErrorStrings.ExportColumn_PartsAttribution);

                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.PartCode,
                            detail.PartName,
                            detail.stock,
                            detail.Usablequantity,
                            detail.OnwayQty,
                            detail.WMZQty,
                            detail.OutQty,
                            detail.PartsAttribution
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;

        }
    }
}
