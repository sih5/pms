﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string 导出采购订单完成情况统计(DateTime? bPurchasePlanTime, DateTime? ePurchasePlanTime, DateTime? bTheoryDeliveryTime, DateTime? eTheoryDeliveryTime) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("采购订单完成情况统计_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = 采购订单完成情况统计(bPurchasePlanTime, ePurchasePlanTime, bTheoryDeliveryTime, eTheoryDeliveryTime);
          
            var dataArray = data.ToArray();
            if(data.Any()) {
                excelColumns.Add("月份");
                excelColumns.Add("日期");
                excelColumns.Add("采购订单总条目数");
                excelColumns.Add("采购订单到期总条目数");
                excelColumns.Add("采购订单按期完成条目数");
                excelColumns.Add("采购订单超期完成条目数");
                excelColumns.Add("采购订单超期7天内未完成条目数");
                excelColumns.Add("采购订单超期14天内未完成条目数");
                excelColumns.Add("采购订单超期21天内未完成条目数");
                excelColumns.Add("采购订单超期21天以上未完成条目数");
                excelColumns.Add("采购订单未到期未完成条目数");
                excelColumns.Add("按期到货满足率(%)");               

                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.Month,
                            detail.Day,
                            detail.AllItem,
                            detail.ArriveItem,
                            detail.OnTime,
                            detail.OutTime,
                            detail.SevenUn,
                            detail.EightUn,
                            detail.TwentyUn,
                            detail.OverTwentyUn,
                            detail.Undue,
                            detail.Mzl
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
        public string 导出供应商采购订单完成情况统计(DateTime? bPurchasePlanTime, DateTime? ePurchasePlanTime, DateTime? bTheoryDeliveryTime, DateTime? eTheoryDeliveryTime, string supplierName) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("采购订单完成情况统计_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = 供应商采购订单完成情况统计(bPurchasePlanTime, ePurchasePlanTime, bTheoryDeliveryTime, eTheoryDeliveryTime, supplierName);

            var dataArray = data.ToArray();
            if(data.Any()) {
                excelColumns.Add("月份");
                excelColumns.Add("日期");
                excelColumns.Add("供应商");
                excelColumns.Add("采购订单总条目数");
                excelColumns.Add("采购订单到期总条目数");
                excelColumns.Add("采购订单按期完成条目数");
                excelColumns.Add("采购订单超期完成条目数");
                excelColumns.Add("采购订单超期7天内未完成条目数");
                excelColumns.Add("采购订单超期14天内未完成条目数");
                excelColumns.Add("采购订单超期21天内未完成条目数");
                excelColumns.Add("采购订单超期21天以上未完成条目数");
                excelColumns.Add("采购订单未到期未完成条目数");
                excelColumns.Add("按期到货满足率(%)");

                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.Month,
                            detail.Day,
                            detail.SupplierName,
                            detail.AllItem,
                            detail.ArriveItem,
                            detail.OnTime,
                            detail.OutTime,
                            detail.SevenUn,
                            detail.EightUn,
                            detail.TwentyUn,
                            detail.OverTwentyUn,
                            detail.Undue,
                            detail.Mzl
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
    }
}

