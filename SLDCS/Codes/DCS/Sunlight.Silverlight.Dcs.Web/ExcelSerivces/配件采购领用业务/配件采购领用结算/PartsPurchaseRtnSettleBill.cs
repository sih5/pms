﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string 导出配件采购退货结算单(int[] ids, int? branchId, string code, string partsSupplierCode, string partsSupplierName, int? warehouseid, int? settlementPath, int? status, int? partsSalesCategoryId, DateTime? createTimeBegin, DateTime? createTimeEnd, string creatorName, string businessCode, DateTime? invoiceDateBegin, DateTime? invoiceDateEnd)
        {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("配件采购退货结算单_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = GetPartsPurchaseRtnSettleBillsWithBusinessCode();
            if(ids.Count() > 0) {
                data = data.Where(r => ids.Contains(r.Id));
            } else {
                if(branchId.HasValue)
                    data = data.Where(r => r.BranchId == branchId);
                if(partsSalesCategoryId.HasValue)
                    data = data.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId);
                if(warehouseid.HasValue)
                    data = data.Where(r => r.WarehouseId == warehouseid);
                if(status.HasValue)
                    data = data.Where(r => r.Status == status);
                if(settlementPath.HasValue)
                    data = data.Where(r => r.SettlementPath == settlementPath);
                if(status.HasValue)
                    data = data.Where(r => r.Status == status);
                if(createTimeBegin.HasValue) {
                    data = data.Where(r => r.CreateTime >= createTimeBegin.Value);
                }
                if(createTimeEnd.HasValue) {
                    data = data.Where(r => r.CreateTime <= createTimeEnd.Value);
                }
                if(!string.IsNullOrEmpty(code)) {
                    data = data.Where(r => r.Code.Contains(code));
                }
                if(!string.IsNullOrEmpty(partsSupplierCode)) {
                    data = data.Where(r => r.PartsSupplierCode.Contains(partsSupplierCode));
                }
                if(!string.IsNullOrEmpty(partsSupplierName)) {
                    data = data.Where(r => r.PartsSupplierName.Contains(partsSupplierName));
                }
                if(!string.IsNullOrEmpty(creatorName)) {
                    data = data.Where(r => r.CreatorName.Contains(creatorName));
                }
                if(!string.IsNullOrEmpty(businessCode)) {
                    data = data.Where(r => r.BusinessCode.Contains(businessCode));
                }
                if (invoiceDateBegin.HasValue)
                {
                    data = data.Where(r => r.InvoiceDate >= invoiceDateBegin.Value);
                }
                if (invoiceDateEnd.HasValue)
                {
                    data = data.Where(r => r.InvoiceDate <= invoiceDateEnd.Value);
                }
            }
            var dataArray = data.ToArray();
            if(data.Any()) {
                excelColumns.Add(ErrorStrings.ExportColumn_SettlementCode);
                excelColumns.Add(ErrorStrings.ExportColumn_Branch);
                excelColumns.Add(ErrorStrings.ExportColumn_WarehouseName);
                excelColumns.Add(ErrorStrings.ExportColumn_PartsSalesCategory);
                excelColumns.Add(ErrorStrings.ExportColumn_SupplierCode2);
                excelColumns.Add(ErrorStrings.ExportColumn_SupplierName);
                excelColumns.Add(ErrorStrings.ExportColumn_SettlementTotalAmount);
                excelColumns.Add(ErrorStrings.ExportColumn_InvoiceTotalAmount);
                excelColumns.Add(ErrorStrings.ExportColumn_TaxRate);
                excelColumns.Add(ErrorStrings.ExportColumn_TaxFee);
                excelColumns.Add(ErrorStrings.ExportColumn_InvoiceDifference);
                excelColumns.Add(ErrorStrings.ExportColumn_TotalAmount2);
                excelColumns.Add(ErrorStrings.ExportColumn_Status);
                excelColumns.Add(ErrorStrings.ExportColumn_InvoiceDate2);
                excelColumns.Add(ErrorStrings.ExportColumn_InvoicePath);
                excelColumns.Add(ErrorStrings.ExportColumn_SettlementPath);
                excelColumns.Add(ErrorStrings.ExportColumn_OffsettedSettlementBillCode);
                excelColumns.Add(ErrorStrings.ExportColumn_Remark);
                excelColumns.Add(ErrorStrings.ExportColumn_Creator);
                excelColumns.Add(ErrorStrings.ExportColumn_CreateTime);

                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.Code,
                            detail.BranchName,
                            detail.WarehouseName,
                            detail.PartsSalesCategoryName,
                            detail.PartsSupplierCode,
                            detail.PartsSupplierName,
                            detail.TotalSettlementAmount,
                            detail.InvoiceTotalAmount,
                            detail.TaxRate,
                            detail.Tax,
                            detail.InvoiceAmountDifference,
                            detail.TotalAmount,
                            Enum.GetName(typeof(DcsPartsPurchaseSettleStatus),detail.Status),
                            detail.InvoiceDate,
                            Enum.GetName(typeof(DcsPartsPurchaseRtnSettleBillInvoicePath), detail.InvoicePath),
                            Enum.GetName(typeof(DcsPartsPurchaseSettleBillSettlementPath),detail.SettlementPath),
                            detail.OffsettedSettlementBillCode,
                            detail.Remark,
                            detail.CreatorName,
                            detail.CreateTime
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
    }
}

