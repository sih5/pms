﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string 合并导出配件领用结算单(int[] ids, int? branchId, int? partsSalesCategoryId, int? departmentId, DateTime? createTimeBegin, DateTime? createTimeEnd, string code, int? status, int settleType, DateTime? invoiceDateBegin, DateTime? invoiceDateEnd,int? type)
        {
            var excelColumns = new List<string>();
            IQueryable<PartsRequisitionSettleBillExport> partsRequisitionSettleBillExports = null;
            var data = this.ObjectContext.PartsRequisitionSettleBills.Where(r => r.SettleType == settleType);
            if(ids != null && ids.Any()) {
                data = data.Where(r => ids.Contains(r.Id));
            } else {
                if(branchId.HasValue)
                    data = data.Where(r => r.BranchId == branchId);
                if(partsSalesCategoryId.HasValue)
                    data = data.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId);
                if(departmentId.HasValue)
                    data = data.Where(r => r.DepartmentId == departmentId);
                if(status.HasValue)
                    data = data.Where(r => r.Status == status);
                if(createTimeBegin.HasValue) {
                    data = data.Where(r => r.CreateTime >= createTimeBegin.Value);
                }
                if(createTimeEnd.HasValue) {
                    data = data.Where(r => r.CreateTime <= createTimeEnd.Value);
                }
                if(!string.IsNullOrEmpty(code)) {
                    data = data.Where(r => r.Code.Contains(code));
                }
                if (invoiceDateBegin.HasValue)
                {
                    data = data.Where(r => r.InvoiceDate >= invoiceDateBegin.Value);
                }
                if (invoiceDateEnd.HasValue)
                {
                    data = data.Where(r => r.InvoiceDate <= invoiceDateEnd.Value);
                }
                if(type.HasValue)
                    data = data.Where(r => r.Type == type);
            }
            var id = data.ToArray().Select(r => r.Id);
            //找出库单
            var partsOutboundBillDetails = from a in this.ObjectContext.PartsRequisitionSettleRefs.Where(r => id.Contains(r.PartsRequisitionSettleBillId) && r.SourceType == (int)DcsPartsRequisitionSettleRefSourceType.配件出库单)
                                           from b in this.ObjectContext.PartsOutboundBillDetails
                                           where a.SourceId == b.PartsOutboundBillId
                                           select new {
                                               a.SourceCode,
                                               a.SourceType,
                                               a.PartsRequisitionSettleBillId,
                                               b.SparePartCode,
                                               b.SparePartName,
                                               b.SettlementPrice,
                                               Quantity = b.OutboundAmount,
                                               b.CostPrice,
                                               tempAmont = b.CostPrice * b.OutboundAmount
                                           };
            //找入库单
            var partsInboundCheckBillDetails = from a in this.ObjectContext.PartsRequisitionSettleRefs.Where(r => id.Contains(r.PartsRequisitionSettleBillId) && r.SourceType == (int)DcsPartsRequisitionSettleRefSourceType.配件入库检验单)
                                               from b in this.ObjectContext.PartsInboundCheckBillDetails
                                               where a.SourceId == b.PartsInboundCheckBillId
                                               select new {
                                                   a.SourceCode,
                                                   a.SourceType,
                                                   a.PartsRequisitionSettleBillId,
                                                   b.SparePartCode,
                                                   b.SparePartName,
                                                   b.SettlementPrice,
                                                   Quantity = b.InspectedQuantity,
                                                   b.CostPrice,
                                                   tempAmont = (-1) * b.CostPrice * b.InspectedQuantity
                                               };
            var tempStructQuery = from a in (partsOutboundBillDetails.Union(partsInboundCheckBillDetails))
                                  group a by new {
                                      a.PartsRequisitionSettleBillId
                                  }
                                      into tempTable
                                  select new {
                                      settlementId = tempTable.Key.PartsRequisitionSettleBillId,
                                      totalAmount = (decimal?)tempTable.Sum(r => r.tempAmont)
                                  };
            partsRequisitionSettleBillExports = from a in data
                                                from b in partsOutboundBillDetails
                                                where a.Id == b.PartsRequisitionSettleBillId
                                                join c in tempStructQuery on a.Id equals c.settlementId into tempTable1
                                                from t in tempTable1.DefaultIfEmpty()
                                                select new PartsRequisitionSettleBillExport {
                                                    Id = a.Id,
                                                    Code = a.Code,//结算单编号
                                                    BranchName = a.BranchName, //营销分公司
                                                    PartsSalesCategoryName = a.PartsSalesCategoryName, //品牌
                                                    DepartmentName = a.DepartmentName,//部门
                                                    SettleType = a.SettleType, //结算类型
                                                    TotalSettlementAmount = a.TotalSettlementAmount, //结算总金额
                                                    Status = a.Status,//状态
                                                    InvoiceDate=a.InvoiceDate,//过账日期
                                                    Remark = a.Remark, //备注
                                                    CreatorName = a.CreatorName, //创建人
                                                    CreateTime = a.CreateTime, //创建时间
                                                    ModifierName = a.ModifierName,  //修改人
                                                    ModifyTime = a.ModifyTime,//修改时间
                                                    ApproverName = a.ApproverName, //审批人
                                                    ApproveTime = a.ApproveTime,//审批时间
                                                    AbandonerName = a.AbandonerName, //作废人
                                                    AbandonTime = a.AbandonTime, //作废时间
                                                    SourceCode = b.SourceCode,
                                                    SourceType = b.SourceType,
                                                    SparePartCode = b.SparePartCode,
                                                    SparePartName = b.SparePartName,
                                                    SettlementPrice = b.SettlementPrice,
                                                    OutboundAmount = b.Quantity,
                                                    SettlementAmount = b.SettlementPrice * b.Quantity,
                                                    PlannedPriceTotalAmount = t.totalAmount ?? 0,
                                                    Type=a.Type
                                                };
            var checkBillDetail = from a in data
                                  from b in partsInboundCheckBillDetails
                                  where a.Id == b.PartsRequisitionSettleBillId
                                  join c in tempStructQuery on a.Id equals c.settlementId into tempTable1
                                  from t in tempTable1.DefaultIfEmpty()
                                  select new PartsRequisitionSettleBillExport {
                                      Id = a.Id,
                                      Code = a.Code,//结算单编号
                                      BranchName = a.BranchName, //营销分公司
                                      PartsSalesCategoryName = a.PartsSalesCategoryName, //品牌
                                      DepartmentName = a.DepartmentName,//部门
                                      SettleType = a.SettleType, //结算类型
                                      TotalSettlementAmount = a.TotalSettlementAmount, //结算总金额
                                      Status = a.Status,//状态
                                      InvoiceDate = a.InvoiceDate,//过账日期
                                      Remark = a.Remark, //备注
                                      CreatorName = a.CreatorName, //创建人
                                      CreateTime = a.CreateTime, //创建时间
                                      ModifierName = a.ModifierName,  //修改人
                                      ModifyTime = a.ModifyTime,//修改时间
                                      ApproverName = a.ApproverName, //审批人
                                      ApproveTime = a.ApproveTime,//审批时间
                                      AbandonerName = a.AbandonerName, //作废人
                                      AbandonTime = a.AbandonTime, //作废时间
                                      SourceCode = b.SourceCode,
                                      SourceType = b.SourceType,
                                      SparePartCode = b.SparePartCode,
                                      SparePartName = b.SparePartName,
                                      SettlementPrice = b.SettlementPrice,
                                      OutboundAmount = b.Quantity,
                                      SettlementAmount = b.SettlementPrice * b.Quantity,
                                      PlannedPriceTotalAmount = t.totalAmount ?? 0,
                                      Type=a.Type
                                  };
            partsRequisitionSettleBillExports = partsRequisitionSettleBillExports != null ? partsRequisitionSettleBillExports.Concat(checkBillDetail) : checkBillDetail;
            excelColumns.Add(ErrorStrings.ExportColumn_SettlementCode);
            excelColumns.Add(ErrorStrings.ExportColumn_Branch2);
            excelColumns.Add(ErrorStrings.ExportColumn_PartsSalesCategory);
            excelColumns.Add(ErrorStrings.ExportColumn_Department);
            excelColumns.Add(ErrorStrings.ExportColumn_SettleType);
            excelColumns.Add(ErrorStrings.ExportColumn_SettlementTotalAmount);
         
            excelColumns.Add(ErrorStrings.ExportColumn_Status);
            excelColumns.Add(ErrorStrings.ExportColumn_Type);
            excelColumns.Add(ErrorStrings.ExportColumn_InvoiceDate2);
            excelColumns.Add(ErrorStrings.ExportColumn_Remark);
            excelColumns.Add(ErrorStrings.ExportColumn_Creator);
            excelColumns.Add(ErrorStrings.ExportColumn_CreateTime);
            excelColumns.Add(ErrorStrings.ExportColumn_Modifier);
            excelColumns.Add(ErrorStrings.ExportColumn_ModifyTime);
            excelColumns.Add(ErrorStrings.ExportColumn_Approver);
            excelColumns.Add(ErrorStrings.ExportColumn_ApproveTime);
            excelColumns.Add(ErrorStrings.ExportColumn_Abandoner);
            excelColumns.Add(ErrorStrings.ExportColumn_AbandonTime);
            excelColumns.Add(ErrorStrings.ExportColumn_ContactOrderCode);
            excelColumns.Add(ErrorStrings.ExportColumn_OrderType2);
            excelColumns.Add(ErrorStrings.ExportColumn_PartCode);
            excelColumns.Add(ErrorStrings.ExportColumn_SparePartName);
            excelColumns.Add(ErrorStrings.ExportColumn_SettlementPrice);
            excelColumns.Add(ErrorStrings.ExportColumn_SettlementQuantity);
            excelColumns.Add(ErrorStrings.ExportColumn_SettlementAmount);

            var queryReuslt = partsRequisitionSettleBillExports.ToArray().Select(detail => new object[] {
                    detail.Code, detail.BranchName, detail.PartsSalesCategoryName, detail.DepartmentName, Enum.GetName(typeof(DcsRequisitionSettleType), detail.SettleType), detail.TotalSettlementAmount,Enum.GetName(typeof(DcsPartsRequisitionSettleBillStatus), detail.Status),Enum.GetName(typeof(DcsInternalAllocationBill_Type),detail.Type), detail.InvoiceDate, detail.Remark, detail.CreatorName, detail.CreateTime, detail.ModifyTime, detail.ModifierName, detail.ApproverName, detail.ApproveTime, detail.AbandonTime, detail.AbandonerName, detail.SourceCode, Enum.GetName(typeof(DcsPartsRequisitionSettleRefSourceType), detail.SourceType), detail.SparePartCode, detail.SparePartName, detail.SettlementPrice, detail.OutboundAmount, detail.SettlementAmount
                }).ToArray();
            return ExportAllData(queryReuslt, excelColumns, "配件领用结算单");
        }
    }
}
