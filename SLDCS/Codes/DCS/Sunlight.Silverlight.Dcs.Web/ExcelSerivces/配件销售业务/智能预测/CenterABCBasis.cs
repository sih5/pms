﻿using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string 导出中心库ABC分类基础(string centerCode, string sparePartCode, int? newType, int? oldType, int? weekNum, string centerName) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("中心库ABC分类基础_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = this.QueryCenterABCBasis();
               if(newType.HasValue) {
                data = data.Where(r => r.NewType == newType);
                }
                if(!string.IsNullOrEmpty(centerCode)) {
                    data = data.Where(r => r.CenterCode.Contains(centerCode));
                }
                if(!string.IsNullOrEmpty(centerName)) {
                    data = data.Where(r => r.CenterName.Contains(centerName));
                }
                if(!string.IsNullOrEmpty(sparePartCode)) {
                    data = data.Where(r => r.SparePartCode.Contains(sparePartCode));
                }
                if(weekNum.HasValue) {
                    data = data.Where(r => r.WeekNum == weekNum);
                }
                if(oldType.HasValue) {
                    data = data.Where(r => r.OldType == oldType);
                }
               
            var dataArray = data.ToArray();
            if(data.Any()) {
                excelColumns.Add(ErrorStrings.Export_Title_CenterABCBasis_WeekNum);
                excelColumns.Add(ErrorStrings.ExportColumn_AgencyCode);
                excelColumns.Add(ErrorStrings.ExportColumn_AgencyName);
                excelColumns.Add(ErrorStrings.ExportColumn_SparePartCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SIHCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SparePartName);
                excelColumns.Add(ErrorStrings.Export_Title_CenterABCBasis_OldType);
                excelColumns.Add(ErrorStrings.ExportColumn_SalePrice);
                excelColumns.Add(ErrorStrings.Export_Title_CenterABCBasis_SaleSubNumber);
                excelColumns.Add(ErrorStrings.Export_Title_CenterABCBasis_SaleSubAmount);
                excelColumns.Add(ErrorStrings.Export_Title_CenterABCBasis_SubAmountPercentage);
                excelColumns.Add(ErrorStrings.Export_Title_CenterABCBasis_SaleSubFrequency);
                excelColumns.Add(ErrorStrings.Export_Title_CenterABCBasis_SubFrequencyPercentage);
                excelColumns.Add(ErrorStrings.Export_Title_CenterABCBasis_NewType);               

                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.WeekNum,
                            detail.CenterCode,
                            detail.CenterName,
                            detail.SparePartCode,                            
                            detail.SihCode,
                            detail.SparePartName,
							Enum.GetName(typeof(DcsABCStrategyCategory),detail.OldType??0),
                            detail.SalesPrice,
                            detail.SaleSubNumber,
                            detail.SaleSubAmount,
                            detail.SubAmountPercentage,
                            detail.SaleSubFrequency,
                            detail.SubFrequencyPercentage,
                            Enum.GetName(typeof(DCSABCSettingType),detail.NewType??0)						
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }

    }
}

