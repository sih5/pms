﻿using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string 导出中配件周度销量基础数据(int? week, string sparePartCode, int? warehouseId, int? partsSalesOrderTypeId) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("配件周度销量基础数据_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = this.QueryPartsSalesWeeklyBasesForCenter();
            if(week.HasValue) {
                data = data.Where(r => r.Week == week);
            }        
            if(!string.IsNullOrEmpty(sparePartCode)) {
                data = data.Where(r => r.SparePartCode.Contains(sparePartCode));
            }
            if(warehouseId.HasValue) {
                data = data.Where(r => r.WarehouseId == warehouseId);
            }
            if(partsSalesOrderTypeId.HasValue) {
                data = data.Where(r => r.PartsSalesOrderTypeId == partsSalesOrderTypeId);
            }

            var dataArray = data.ToArray();
            if(data.Any()) {
                excelColumns.Add(ErrorStrings.Export_Year);
                excelColumns.Add(ErrorStrings.Export_Title_CenterABCBasis_WeekNum);
				excelColumns.Add(ErrorStrings.ExportColumn_Warehouse);
				excelColumns.Add(ErrorStrings.ExportColumn_OrderType);
                excelColumns.Add(ErrorStrings.ExportColumn_SparePartCode);                
                excelColumns.Add(ErrorStrings.ExportColumn_SparePartName);                
                excelColumns.Add(ErrorStrings.ExportColumn_Quantity);
                excelColumns.Add(ErrorStrings.Export_Title_PartsSalesWeeklyBase_CreateTime);              

                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.Year,
                            detail.Week,
                            detail.WarehouseName,                            
                            detail.PartsSalesOrderTypeName,
                            detail.SparePartCode,
                            detail.SparePartName,
                            detail.Quantity,
                            detail.CreateTime                          	
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }

    }
}

