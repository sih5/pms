﻿using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string 导出配件库存系数(int? type, int? productType, int? status) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("配件库存系数_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = this.ExportForPartsStockCoefficient(type,productType,status);

            var dataArray = data.ToArray();
            if(data.Any()) {
                excelColumns.Add(ErrorStrings.DataGridView_Title_PartsStockCoefficientPrice_Type);
                excelColumns.Add(ErrorStrings.DataGridView_Title_PartsStockCoefficientPrice_ProductType);
                excelColumns.Add(ErrorStrings.ExportColumn_Status);
                excelColumns.Add(ErrorStrings.ExportColumn_Creator);
                excelColumns.Add(ErrorStrings.ExportColumn_CreateTime);
                excelColumns.Add(ErrorStrings.ExportColumn_Modifier);
                excelColumns.Add(ErrorStrings.ExportColumn_ModifyTime);
                excelColumns.Add(ErrorStrings.DataGridView_Title_PartsStockCoefficientPrice_MinPrice);
                excelColumns.Add(ErrorStrings.DataGridView_Title_PartsStockCoefficientPrice_MaxPrice);
                excelColumns.Add(ErrorStrings.DataGridView_Title_PartsStockCoefficientPrice_Coefficient);              

                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            Enum.GetName(typeof(DcsPartsStockCoefficientType),detail.Type??0),
                            Enum.GetName(typeof(DCSABCSettingType),detail.ProductType??0),
                            Enum.GetName(typeof(DcsBaseDataStatus),detail.Status??0),
                            detail.CreatorName,
                            detail.CreateTime,
                            detail.ModifierName,                            
                            detail.ModifyTime,
                            detail.MinPrice,							
                            detail.MaxPrice,
                            detail.Coefficient
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }

    }
}

