﻿using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string 导出销售订单日均销量(string centerName, string sparePartCode, int? type,DateTime? bCreateTime,DateTime? eCreateTime) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("销售订单日均销量_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = this.SalesOrderDailyAvgByCenter();
            if(type.HasValue) {
                data = data.Where(r => r.Type == type);
            }
            if(!string.IsNullOrEmpty(centerName)) {
                data = data.Where(r => r.CenterName.Contains(centerName));
            }
            if(!string.IsNullOrEmpty(sparePartCode)) {
                data = data.Where(r => r.SparePartCode.Contains(sparePartCode));
            }           
            if(bCreateTime.HasValue){
				data =data.Where(r=>r.CreateTime>=bCreateTime);
			}
			if(eCreateTime.HasValue){
				data =data.Where(r=>r.CreateTime<=eCreateTime);
			}
            var dataArray = data.ToArray();
            if(data.Any()) {
                excelColumns.Add(ErrorStrings.Export_Year);
                excelColumns.Add(ErrorStrings.Export_Title_PartsSalesWeeklyBase_CreateTime);
                excelColumns.Add(ErrorStrings.ExportColumn_Warehouse);
                excelColumns.Add(ErrorStrings.DataGridView_SalesOrderDailyAvg_Type);
                excelColumns.Add(ErrorStrings.Export_SalesOrderDailyAvg_OrderCycle);
                excelColumns.Add(ErrorStrings.Export_SalesOrderDailyAvg_ArrivalCycle);
                excelColumns.Add(ErrorStrings.Export_SalesOrderDailyAvg_SafeStockDays);
                excelColumns.Add(ErrorStrings.Export_SalesOrderDailyAvg_ReserveCoefficient);
                excelColumns.Add(ErrorStrings.Export_SalesOrderDailyAvg_StockUpperCoefficient);
                excelColumns.Add(ErrorStrings.Export_SalesOrderDailyAvg_PurchasePrice);
                excelColumns.Add(ErrorStrings.ExportColumn_PackingQuantity);
                excelColumns.Add(ErrorStrings.ExportColumn_UsableStock);
                excelColumns.Add(ErrorStrings.ExportColumn_OnwayAmount);
				excelColumns.Add(ErrorStrings.ExportColumn_OwingAmount);
				excelColumns.Add(ErrorStrings.ExportColumn_PartCode);
				excelColumns.Add(ErrorStrings.ExportColumn_SparePartName);
				excelColumns.Add(ErrorStrings.Export_SalesOrderDailyAvg_T3WeekAvgQty);
				excelColumns.Add(ErrorStrings.Export_SalesOrderDailyAvg_T2WeekAvgQty);
				excelColumns.Add(ErrorStrings.Export_SalesOrderDailyAvg_T1WeekAvgQty);
				excelColumns.Add(ErrorStrings.Export_SalesOrderDailyAvg_TWeekAvgQty);
				excelColumns.Add(ErrorStrings.Export_SalesOrderDailyAvg_TWeekActualAvgQty);
				excelColumns.Add(ErrorStrings.Export_SalesOrderDailyAvg_StandardQty);

                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.Year,
                            detail.CreateTime,
                            detail.WarehouseName,                            
                            Enum.GetName(typeof(DCSABCSettingType),detail.Type??0),	
                            detail.OrderCycle,
							detail.ArrivalCycle,
                            detail.SafeStockDays,
                            detail.ReserveCoefficient,
                            detail.StockUpperCoefficient,
                            detail.PurchasePrice,
                            detail.PackingQty,
                            detail.UsableQty,
                            detail.OnLineQty,
							detail.OweQty,
							detail.SparePartCode,
							detail.SparePartName,
							detail.T3WeekAvgQty,
							detail.T2WeekAvgQty,
							detail.T1WeekAvgQty,
							detail.TWeekAvgQty,
							detail.TWeekActualAvgQty,
							detail.StandardQty						
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }

    }
}

