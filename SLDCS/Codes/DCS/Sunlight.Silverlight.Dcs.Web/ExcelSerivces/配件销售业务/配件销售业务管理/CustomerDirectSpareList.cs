﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string ExportCustomerDirectSpareList(int[] ids, int? partsSalesCategoryId, string customerCode, string customerName, string sparePartCode, string sparePartName, int? status, bool? isPrimary)
        {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("客户直供配件清单维护_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
          
            if(ids != null &&ids.Length > 0) {
              var  data =ObjectContext.CustomerDirectSpareLists.Where(r=>ids.Contains(r.Id)).ToArray();
                if (data.Any())
                {
                    excelColumns.Add(ErrorStrings.ExportColumn_PartsSalesCategoryName);
                    excelColumns.Add(ErrorStrings.ExportColumn_CustomerCode);
                    excelColumns.Add(ErrorStrings.ExportColumn_CustomerName);
                    excelColumns.Add(ErrorStrings.ExportColumn_PartCode);
                    excelColumns.Add(ErrorStrings.ExportColumn_SparePartName);
                    excelColumns.Add(ErrorStrings.ExportColumn_Status);
                    excelColumns.Add(ErrorStrings.ExportColumn_Creator);
                    excelColumns.Add(ErrorStrings.ExportColumn_CreateTime);
                    excelColumns.Add(ErrorStrings.ExportColumn_Modifier);
                    excelColumns.Add(ErrorStrings.ExportColumn_ModifyTime);                 
                    var dataArray = data.ToArray();
                    using (var excelExport = new ExcelExport(exportDataFileName))
                    {
                        excelExport.ExportByRow(index =>
                        {
                            if (index == data.Count() + 1)
                                return null;
                            if (index == 0)
                                return excelColumns.ToArray<Object>();
                            var detail = dataArray[index - 1];
                            var values = new object[] {
                            detail.PartsSalesCategoryName,
                            detail.CustomerCode,
                            detail.CustomerName,
                            detail.SparePartCode,
                            detail.SparePartName, 
                            Enum.GetName(typeof(DcsBaseDataStatus),detail.Status??0),
                            detail.CreatorName,
                            detail.CreateTime,
                            detail.ModifierName,
                            detail.ModifyTime,
                           
                        };
                            return values;
                        });
                    }
                }
            } else {
                var data = this.导出直供清单(isPrimary, partsSalesCategoryId, customerCode, customerName, sparePartCode, sparePartName, status);                
                if(data.Any())
                {
                    excelColumns.Add(ErrorStrings.ExportColumn_PartsSalesCategoryName);
                    excelColumns.Add(ErrorStrings.ExportColumn_CustomerCode);
                    excelColumns.Add(ErrorStrings.ExportColumn_CustomerName);
                    excelColumns.Add(ErrorStrings.ExportColumn_PartCode);
                    excelColumns.Add(ErrorStrings.ExportColumn_SparePartName);
                    excelColumns.Add(ErrorStrings.ExportColumn_Status);
                    excelColumns.Add(ErrorStrings.ExportColumn_Creator);
                    excelColumns.Add(ErrorStrings.ExportColumn_CreateTime);
                    excelColumns.Add(ErrorStrings.ExportColumn_Modifier);
                    excelColumns.Add(ErrorStrings.ExportColumn_ModifyTime);
                    excelColumns.Add(ErrorStrings.ExportColumn_IsPrimarySupplier);
                    excelColumns.Add(ErrorStrings.ExportColumn_SupplierCode2);
                    excelColumns.Add(ErrorStrings.ExportColumn_SupplierName);
                    var dataArray = data.ToArray();
                    using (var excelExport = new ExcelExport(exportDataFileName))
                    {
                        excelExport.ExportByRow(index =>
                        {
                            if (index == data.Count() + 1)
                                return null;
                            if (index == 0)
                                return excelColumns.ToArray<Object>();
                            var detail = dataArray[index - 1];
                            var values = new object[] {
                            detail.PartsSalesCategoryName,
                            detail.CustomerCode,
                            detail.CustomerName,
                            detail.SparePartCode,
                            detail.SparePartName, 
                            Enum.GetName(typeof(DcsBaseDataStatus),detail.Status??0),
                            detail.CreatorName,
                            detail.CreateTime,
                            detail.ModifierName,
                            detail.ModifyTime,
                            detail.IsPrimary==true?EntityStrings.Export_Title_Yes:EntityStrings.Export_Title_No,
                            detail.PartsSupplierCode,
                            detail.PartsSupplierName
                        };
                            return values;
                        });
                    }
                }
            } 
           
            return exportDataFileName;
        }
    }
}
