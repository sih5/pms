﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string ExportCustomerSupplyInitialFeeSet(int[] ids, int? partsSalesCategoryId, string customerCode, string customerName, string supplierCode, string supplierName, int? status) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("客户与供应商起订金额设置_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = this.GetCustomerSupplyInitialFeeSets();
            if(ids != null && ids.Length > 0) {
                data = data.Where(r => ids.Contains(r.Id));
            } else {
                if(partsSalesCategoryId.HasValue) {
                    data = data.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId);
                }
                if(!string.IsNullOrEmpty(customerCode)) {
                    data = data.Where(r => r.CustomerCode.Contains(customerCode));
                }
                if(!string.IsNullOrEmpty(customerName)) {
                    data = data.Where(r => r.CustomerName.Contains(customerName));
                }
                if(!string.IsNullOrEmpty(supplierCode)) {
                    data = data.Where(r => r.SupplierCode.Contains(supplierCode));
                }
                if(!string.IsNullOrEmpty(supplierName)) {
                    data = data.Where(r => r.SupplierName.Contains(supplierName));
                }
                if(status.HasValue) {
                    data = data.Where(r => r.Status == status);
                }
            }
            if(data.Any()) {
                excelColumns.Add(ErrorStrings.ExportColumn_PartsSalesCategoryName);
                excelColumns.Add(ErrorStrings.ExportColumn_CustomerCode);
                excelColumns.Add(ErrorStrings.ExportColumn_CustomerName);
                excelColumns.Add(ErrorStrings.ExportColumn_SupplierCode2);
                excelColumns.Add(ErrorStrings.ExportColumn_SupplierName);
                excelColumns.Add(ErrorStrings.ExportColumn_OrderMoney);
                excelColumns.Add("约定发货时间");
                excelColumns.Add(ErrorStrings.ExportColumn_Status);
                excelColumns.Add(ErrorStrings.ExportColumn_Creator);
                excelColumns.Add(ErrorStrings.ExportColumn_CreateTime);
                excelColumns.Add(ErrorStrings.ExportColumn_Modifier);
                excelColumns.Add(ErrorStrings.ExportColumn_ModifyTime);
                var dataArray = data.ToArray();
                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == data.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.PartsSalesCategoryName,
                            detail.CustomerCode,
                            detail.CustomerName,
                            detail.SupplierCode,
                            detail.SupplierName,
                            detail.InitialFee,
                            detail.AppointShippingDays,
                            Enum.GetName(typeof(DcsBaseDataStatus),detail.Status??0),
                            detail.CreatorName,
                            detail.CreateTime,
                            detail.ModifierName,
                            detail.ModifyTime
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
    }
}
