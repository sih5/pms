﻿using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string 导出服务站出库单(string code, string outInBandtype, string sparePartCode, string sparePartName, string dealerName, DateTime? bOutBoundTime, DateTime? eOutBoundTime, string marketingDepartmentName, string centerName) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("服务站出库单_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = GetDealerPartsOutboundBills(code, outInBandtype, sparePartCode, sparePartName, dealerName, bOutBoundTime, eOutBoundTime, marketingDepartmentName, centerName);
            var dataArray = data.ToArray();
            if(data.Any()) {
                excelColumns.Add(ErrorStrings.ExportColumn_MarketingDepartment);
                excelColumns.Add(ErrorStrings.ExportColumn_AgencyName);
                excelColumns.Add(ErrorStrings.ExportColumn_DelaerNewCode);
                excelColumns.Add(ErrorStrings.ExportColumn_DealerName);                
                excelColumns.Add(ErrorStrings.ExportColumn_CustomerType);
                excelColumns.Add(ErrorStrings.ExportColumn_CustomerName);
                excelColumns.Add(EntityStrings.IMSPurchaseLoginInfo_Code);
                excelColumns.Add(ErrorStrings.ExportColumn_OutboundTime);
                excelColumns.Add(ErrorStrings.ExportColumn_OrderType2);
                excelColumns.Add(ErrorStrings.ExportColumn_SIHCode);
                excelColumns.Add(ErrorStrings.ExportColumn_PartCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SparePartName);
                excelColumns.Add(ErrorStrings.InOutSettleSummaryReport54);
                excelColumns.Add(ErrorStrings.ExportColumn_DealerPrice2);
                excelColumns.Add(ErrorStrings.PartsSalesOrderProcessDetail_OrderPrice);
                excelColumns.Add(ErrorStrings.ExportColumn_SettlementPrice2);
                excelColumns.Add(ErrorStrings.ExportColumn_SettlementPriceAll2);
                excelColumns.Add(ErrorStrings.ExportColumn_PriceProp);                             
                excelColumns.Add(ErrorStrings.ExportColumn_AgencyCode);
               
                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.MarketingDepartmentName,
                            detail.CenterName,                            
                            detail.DealerCode,
                            detail.DealerName,
                            detail.CustomerType,
                            detail.Customer,
                            detail.Code,
                            detail.OutBoundTime,
                            detail.OutInBandtype,
                            detail.ReferenceCode,
                            detail.PartsCode,
                            detail.PartsName,
                            detail.Quantity, 
                            detail.CostPrice,
                            detail.ReturnPrice,
                            detail.OriginalPrice,
                            detail.Quantity*detail.OriginalPrice,
                            detail.GroupName,                          
                            detail.CenterCode
                            
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }

    }
}

