﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string 导出销售退货入库单统计(DateTime? bCreateTime, DateTime? eCreateTime, string partsSalesReturnBillCode, string partsSalesOrderCode, string sparePartCode, string code, string settlementStatus, string settlementNo, string sAPSysInvoiceNumber, string counterpartCompanyName, string counterpartCompanyCode) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("销售退货入库单统计_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = 销售退货入库单统计(bCreateTime, eCreateTime, partsSalesReturnBillCode, partsSalesOrderCode, sparePartCode, code, settlementStatus, settlementNo, sAPSysInvoiceNumber, counterpartCompanyName, counterpartCompanyCode);
            var dataArray = data.ToArray();
            if(data.Any()) {
                excelColumns.Add(ErrorStrings.ExportColumn_MarketingDepartment);
                excelColumns.Add(ErrorStrings.ExportColumn_Province);
                excelColumns.Add(ErrorStrings.ExportColumn_InBoundWarehouse);
                excelColumns.Add(ErrorStrings.ExportColumn_CoustomerCompanyName);
                excelColumns.Add(ErrorStrings.ExportColumn_CoustomerCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SalesRtnCode);
                excelColumns.Add(ErrorStrings.ExportColumn_OutBoundWarehouse);
                excelColumns.Add("退货原因");
                excelColumns.Add(ErrorStrings.ExportColumn_InboundCode);
                excelColumns.Add(ErrorStrings.ExportColumn_PartCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SparePartName);
                excelColumns.Add(ErrorStrings.ExportColumn_SparePartABC);
                excelColumns.Add(ErrorStrings.ExportColumn_SettlementStatus);
                excelColumns.Add(ErrorStrings.ExportColumn_InboundAmount);
                excelColumns.Add(ErrorStrings.ExportColumn_BeforeDIsCountPrice);
                excelColumns.Add(ErrorStrings.ExportColumn_BeforeDIsCountAmount);
                excelColumns.Add(ErrorStrings.ExportColumn_AfterDIsCountPrice);
                excelColumns.Add(ErrorStrings.ExportColumn_AfterDIsCountAmount);
                excelColumns.Add(ErrorStrings.ExportColumn_IsDiscount);
                excelColumns.Add(ErrorStrings.ExportColumn_PriceType);
                excelColumns.Add(ErrorStrings.ExportColumn_InBoundPrice);
                excelColumns.Add(ErrorStrings.ExportColumn_InBoundPriceAmount);
                excelColumns.Add(ErrorStrings.ExportColumn_SettleBillCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SAPCode);
                excelColumns.Add(ErrorStrings.ExportColumn_InvoiceNumber);
                excelColumns.Add(ErrorStrings.ExportColumn_InboundTime);
                excelColumns.Add(ErrorStrings.ExportColumn_Creator);
                excelColumns.Add(ErrorStrings.ExportColumn_IsRed);

                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.MarketingDepartmentName,
                            detail.Province,                            
                            detail.WarehouseName,
                            detail.CounterpartCompanyName,
                            detail.CounterpartCompanyCode,
                            detail.PartsSalesReturnBillCode,
                            detail.ReturnWarehouseCode,
                            detail.ReturnReason,
                            detail.Code,
                            detail.SparePartCode,
                            detail.SparePartName,
                            Enum.GetName(typeof(DcsABCStrategyCategory),detail.PartABC==null?0:detail.PartABC),
                            Enum.GetName(typeof(DcsPartsSalesSettlementStatus),detail.SettlementStatus==null?0:detail.SettlementStatus),
                            detail.InspectedQuantity,
                            detail.OriginalPrice, 
                            detail.OriginalPriceAll,
                            detail.SettlementPrice,
                            detail.SettlementPriceAll,
                            detail.IsDiscount?EntityStrings.Export_Title_Yes:EntityStrings.Export_Title_No,
                            detail.PriceType,
                            detail.CostPrice,
                            detail.CostPriceAll,
                            detail.SettlementNo,
                            detail.SAPSysInvoiceNumber,
                            detail.InvoiceNumber,
                            detail.CreateTime,
                            detail.CreatorName,
                            detail.IsRed?EntityStrings.Export_Title_Yes:EntityStrings.Export_Title_No,
                            
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }

    }
}

