﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string 导出库存不足的配件明细(int id) {
            var exportDataFileName = GetExportFilePath(string.Format("库存不足的配件明细_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var details = new List<PartsSalesReturnBillDetail>();
            var excelColumns = new List<string>();

            var partsSalesReturnBill = ObjectContext.PartsSalesReturnBills.Single(r => r.Id == id);
            var partsSalesReturnBillDetails = partsSalesReturnBill.PartsSalesReturnBillDetails;
            var company = ObjectContext.Companies.SingleOrDefault(r => r.Id == partsSalesReturnBill.ReturnCompanyId && r.Status == (int)DcsMasterDataStatus.有效);
            if(company == null)
                throw new ValidationException(ErrorStrings.PartsSalesReturnBill_Validation5);

            var dbSalesUnit = ObjectContext.SalesUnits.SingleOrDefault(r => r.Id == partsSalesReturnBill.SalesUnitId && r.Status != (int)DcsMasterDataStatus.作废);
            if(dbSalesUnit == null)
                throw new ValidationException(ErrorStrings.PartsSalesReturnBill_Validation11);

            if(company.Type == (int)DcsCompanyType.代理库 || company.Type == (int)DcsCompanyType.服务站兼代理库) {
                var partsOutboundPlanDetailPartIds = partsSalesReturnBillDetails.Select(r => r.SparePartId);
                var partsLockedStocks = ObjectContext.PartsLockedStocks.Where(r => r.WarehouseId == partsSalesReturnBill.ReturnWarehouseId && partsOutboundPlanDetailPartIds.Contains(r.PartId)).ToArray();
                var partsStocks = ObjectContext.PartsStocks.Where(r => r.WarehouseId == partsSalesReturnBill.ReturnWarehouseId && partsOutboundPlanDetailPartIds.Contains(r.PartId)).ToArray();
                var wmsCongelationStockViews = ObjectContext.WmsCongelationStockViews.Where(r => r.PartsSalesCategoryId == dbSalesUnit.PartsSalesCategoryId && r.WarehouseId == partsSalesReturnBill.ReturnWarehouseId && partsOutboundPlanDetailPartIds.Contains(r.SparePartId)).ToArray();

                foreach(var partsSalesReturnBillDetail in partsSalesReturnBillDetails) {
                    if(details.Any(r => r.SparePartId == partsSalesReturnBillDetail.SparePartId))
                        continue;
                    var partsLockedStock = partsLockedStocks.FirstOrDefault(r => r.PartId == partsSalesReturnBillDetail.SparePartId);
                    var partsStocksAmount = partsStocks.Where(r => r.PartId == partsSalesReturnBillDetail.SparePartId).Sum(v => v.Quantity);
                    var congelationStockQtyAmount = wmsCongelationStockViews.Where(r => r.SparePartId == partsSalesReturnBillDetail.SparePartId).Sum(v => v.CongelationStockQty);
                    var disabledStockAmount = wmsCongelationStockViews.Where(r => r.SparePartId == partsSalesReturnBillDetail.SparePartId).Sum(v => v.DisabledStock);
                    if(partsLockedStock == null) {
                        if(partsStocksAmount - partsSalesReturnBillDetail.ReturnedQuantity - congelationStockQtyAmount - disabledStockAmount < 0) {
                            details.Add(partsSalesReturnBillDetails.First(r => r.SparePartId == partsSalesReturnBillDetail.SparePartId));
                        }
                    } else {
                        if(partsStocksAmount - partsLockedStock.LockedQuantity - partsSalesReturnBillDetail.ReturnedQuantity - congelationStockQtyAmount - disabledStockAmount < 0) {
                            details.Add(partsSalesReturnBillDetails.First(r => r.SparePartId == partsSalesReturnBillDetail.SparePartId));
                        }
                    }
                }
            }
            if(company.Type == (int)DcsCompanyType.服务站) {
                var partsOutboundPlanDetailPartIds = partsSalesReturnBillDetails.Select(r => r.SparePartId);
                var dealerPartsStocks = ObjectContext.DealerPartsStocks.Where(r => r.SalesCategoryId == dbSalesUnit.PartsSalesCategoryId && r.DealerId == partsSalesReturnBill.SubmitCompanyId && partsOutboundPlanDetailPartIds.Contains(r.SparePartId)).ToArray();
                foreach(var partsSalesReturnBillDetail in partsSalesReturnBillDetails) {
                    if(details.Any(r => r.SparePartId == partsSalesReturnBillDetail.SparePartId))
                        continue;
                    var partsStocksAmount = dealerPartsStocks.Where(r => r.SparePartId == partsSalesReturnBillDetail.SparePartId).Sum(v => v.Quantity);
                    if(partsStocksAmount - partsSalesReturnBillDetail.ReturnedQuantity < 0) {
                        details.Add(partsSalesReturnBillDetails.First(r => r.SparePartId == partsSalesReturnBillDetail.SparePartId));
                    }
                }
            }
            if(details.Any()) {
                excelColumns.Add(ErrorStrings.ExportColumn_PartCode);
                excelColumns.Add("配件名称");
                excelColumns.Add("计量单位");
                excelColumns.Add("退货数量");
                excelColumns.Add("原始订单价格");
                excelColumns.Add("退货价格");
                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == details.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray();
                        var detail = details[index - 1];
                        var values = new object[] {
                             detail.SparePartCode,detail.SparePartName,detail.MeasureUnit,detail.ReturnedQuantity,detail.OriginalOrderPrice,detail.ReturnPrice
                        };
                        return values;
                    });
                }
                return exportDataFileName;
            }
            return null;

        }
    }
}
