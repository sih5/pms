﻿using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string ExportSaleAdjustment(int? customerType, string agencyName, string customerName, string sparePartCodes,DateTime? bAdjustTime, DateTime? eAdjustTime) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("销售调价补差-明细_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = GetSaleAdjustmentReport(customerType, agencyName, customerName,sparePartCodes,bAdjustTime,eAdjustTime);
            var dataArray = data.ToArray();
            if(data.Any()) {
                excelColumns.Add("客户类型");
				excelColumns.Add("中心库名称");
                excelColumns.Add("客户名称");
				excelColumns.Add("客户代码");
				excelColumns.Add("配件编号");
				excelColumns.Add("配件名称");
				excelColumns.Add("调价前价格");
				excelColumns.Add("调价后价格");
				excelColumns.Add("调价金额");
				excelColumns.Add("调价幅度");
				excelColumns.Add("过去三月采购量");
				excelColumns.Add("实际可用库存");
				excelColumns.Add("常规库存");
                excelColumns.Add("保底库存");
                excelColumns.Add("保底库存补差金额");
                excelColumns.Add("销售在途");
                excelColumns.Add("欠货在途");
				excelColumns.Add("补差数量");
				excelColumns.Add("补差金额");
				excelColumns.Add("调价时间");

                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.CustomerType == (int)DcsCompanyType.代理库 ? "中心库" : "常规服务站",
                            detail.AgencyName,
                            detail.CustomerName,
                            detail.CustomerCode,
                            detail.SparePartCode,
                            detail.SparePartName,
                            detail.OldPrice, 
                            detail.Price,
                            detail.Difference,
							detail.DifferenceRate,
							detail.PastThreeMonthsPruchase,
							detail.UsableQty,
							detail.NormalStock,
							detail.StockQty,
							detail.StockQtyFee,
							detail.OnwaySales,
							detail.OutBoundAmount,
							detail.AdjustQty,
							detail.AdjustAmount,
							detail.AdjustTime,
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }

	public string ExportSaleAdjustmentSum(int? customerType, string agencyName, string customerName,DateTime? bAdjustTime, DateTime? eAdjustTime) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("销售调价补差-汇总_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = GetSaleAdjustmentSumReport(customerType, agencyName, customerName,bAdjustTime,eAdjustTime);
            var dataArray = data.ToArray();
            if(data.Any()) {
                excelColumns.Add("客户类型");
				excelColumns.Add("中心库名称");
                excelColumns.Add("客户名称");
				excelColumns.Add("客户代码");
				excelColumns.Add("补差总金额");
				excelColumns.Add("补差起始时间");
                excelColumns.Add("补差批次");

                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.CustomerType == 3 ? "中心库" : "常规服务站",
                            detail.AgencyName,
                            detail.CustomerName,
                            detail.CustomerCode,
							detail.AdjustAmount,
							detail.AdjustTime,
							detail.BatchNumber
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
    }
}
