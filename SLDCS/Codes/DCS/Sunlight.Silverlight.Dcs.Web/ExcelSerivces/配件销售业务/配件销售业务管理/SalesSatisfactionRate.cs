﻿using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string 导出销售订单一次满足率对中心库(string name, DateTime? bApproveTime, DateTime? eApproveTime) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("销售订单一次满足率对中心库_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = GetSalesSatisfactionCentralRateReport(name, bApproveTime, eApproveTime);
            var dataArray = data.ToArray();
            if(data.Any()) {
                excelColumns.Add(ErrorStrings.ExportColumn_CustomerName);
                excelColumns.Add(ErrorStrings.ExportColumn_CustomerType);
                excelColumns.Add("未满足行数");
                excelColumns.Add("订单满足行数");
                excelColumns.Add("订单行数");
                excelColumns.Add("一次满足率%");
                excelColumns.Add("审核通过金额");
                excelColumns.Add("订单总金额");
                excelColumns.Add(ErrorStrings.ExportColumn_MoneyRate);


                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.Name,
                            detail.Type,
                            detail.UnYcpzs,
                            detail.Ycpzs,
                            detail.Totalpz,
                            detail.Tmmzl,
                            detail.Ycfee, 
                            detail.Totalfee,
                            detail.Jemzl
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }

    }
}
