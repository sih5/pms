﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string ExportDealerPartsSalesPriceQuery(int[] ids,string sparePartCode, string sparePartName, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("服务站配件销售价_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = 查询配件经销价和零售价(sparePartCode,sparePartName,status,createTimeBegin,createTimeEnd);

            if (ids != null && ids.Any()) {
                data = data.Where(r => ids.Contains(r.Id));
            } else {
                if (!string.IsNullOrEmpty(sparePartCode))
                    data = data.Where(r => r.PartCode.Contains(sparePartCode));
                if (!string.IsNullOrEmpty(sparePartName))
                    data = data.Where(r => r.PartName.Contains(sparePartName));
                if (status.HasValue)
                    data = data.Where(r => r.Status == status);
                if (createTimeBegin.HasValue)
                    data = data.Where(r => r.CreateTime > createTimeBegin);
                if (createTimeEnd.HasValue)
                    data = data.Where(r => r.CreateTime <= createTimeEnd);
            }
            var sales = data;
            var dataArray = data.ToArray();

            if(sales.Any()) {
                excelColumns.Add(ErrorStrings.ExportColumn_Status);
                excelColumns.Add(ErrorStrings.ExportColumn_PartCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SparePartName);
                excelColumns.Add(ErrorStrings.ExportColumn_DealerPrice);
                excelColumns.Add(ErrorStrings.ExportColumn_RetailsPrices);               

                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            Enum.GetName(typeof(DcsBaseDataStatus),detail.Status),
                            detail.PartCode,
                            detail.PartName,
                            detail.SalesPrice,
                            detail.RetailGuidePrice,                           
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
    }
}

