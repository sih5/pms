﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string ExportPartsSalesPrice(int[] ids, int branchId, string sparePartCode, string sparePartName, int? partsSalesCategoryId, int? priceType, int? status, bool? isSalable, bool? isOrderable, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? modifyTimeBegin, DateTime? modifyTimeEnd,bool? isExactExport) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("配件销售价_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = 主查询配件销售价(isSalable, isOrderable, partsSalesCategoryId,priceType,null).Where(r => r.BranchId == branchId);
           
            if(ids != null && ids.Any()) {
                data = data.Where(r => ids.Contains(r.Id));
            } else {
                if (!string.IsNullOrEmpty(sparePartCode)) {
                    var spareparts = sparePartCode.Split(',');
                    if (spareparts.Length == 1) {
                        var sparepartcode = spareparts[0];
                        if (isExactExport.HasValue && isExactExport.Value == true) {
                            data = data.Where(r => r.SparePartCode == sparepartcode); ;
                        } else {
                            data = data.Where(r => r.SparePartCode.Contains(sparepartcode)); ;
                        }
                    } else {
                        data = data.Where(r => spareparts.Contains(r.SparePartCode));
                    }
                }
                if(!string.IsNullOrEmpty(sparePartName)) {
                    data = data.Where(r => r.SparePartName.Contains(sparePartName));
                }
                if(status.HasValue) {
                    data = data.Where(r => r.Status == status);
                }
                if(createTimeBegin.HasValue) {
                    data = data.Where(r => r.CreateTime >= createTimeBegin);
                }
                if(createTimeEnd.HasValue) {
                    data = data.Where(r => r.CreateTime <= createTimeEnd);
                }
                if(modifyTimeBegin.HasValue) {
                    data = data.Where(r => r.ModifyTime >= modifyTimeBegin);
                }
                if(modifyTimeEnd.HasValue) {
                    data = data.Where(r => r.ModifyTime <= modifyTimeEnd);
                }
            }
           
            var partsSalesOrders = data;
            var dataArray = partsSalesOrders.ToArray();
            var partsSalePriceIncreaseRateIds = dataArray.Select(r => r.PriceType).Distinct().ToArray();
            var partsSalePriceIncreaseRates = ObjectContext.PartsSalePriceIncreaseRates.Where(r => partsSalePriceIncreaseRateIds.Contains(r.Id)).ToArray();

            excelColumns.Add(ErrorStrings.ExportColumn_PartsSalesCategory);
            excelColumns.Add(ErrorStrings.ExportColumn_SparePartCode);
            excelColumns.Add(ErrorStrings.ExportColumn_SparePartName);
            excelColumns.Add(ErrorStrings.ExportColumn_PartsSalesPrice);
            excelColumns.Add(ErrorStrings.ExportColumn_CenterPrice);
            excelColumns.Add(ErrorStrings.ExportColumn_DelaerPrices);
            excelColumns.Add(ErrorStrings.ExportColumn_PriceType);
            //excelColumns.Add("是否用于索赔");
            //excelColumns.Add(ErrorStrings.ExportColumn_ValidFrom);
            //excelColumns.Add(ErrorStrings.ExportColumn_ValidTo);
            excelColumns.Add(ErrorStrings.ExportColumn_Status);
            excelColumns.Add(ErrorStrings.ExportColumn_Remark);
            excelColumns.Add(ErrorStrings.ExportColumn_Creator);
            excelColumns.Add(ErrorStrings.ExportColumn_CreateTime);
            excelColumns.Add(ErrorStrings.ExportColumn_Modifier);
            excelColumns.Add(ErrorStrings.ExportColumn_ModifyTime);
            excelColumns.Add(ErrorStrings.ExportColumn_Approver);
            excelColumns.Add(ErrorStrings.ExportColumn_ApproveTime);
            excelColumns.Add(ErrorStrings.ExportColumn_Abandoner);
            excelColumns.Add(ErrorStrings.ExportColumn_AbandonTime);

            if (partsSalesOrders.Any()) {
                using (var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if (index == dataArray.Count() + 1)
                            return null;
                        if (index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var rate = partsSalePriceIncreaseRates.Where(r => r.Id == detail.PriceType).FirstOrDefault();
                        var values = new object[] {
                            detail.PartsSalesCategoryName,
                            detail.SparePartCode,
                            detail.SparePart.Name,
                            detail.SparePart.PartsRetailGuidePrices.Any()? detail.SparePart.PartsRetailGuidePrices.First().RetailGuidePrice:0,
                            detail.CenterPrice,
                            detail.SalesPrice,
                            rate==null?null:rate.GroupCode,
                            //detail.IfClaim.HasValue?(detail.IfClaim.Value?EntityStrings.Export_Title_Yes:EntityStrings.Export_Title_No):"",
                            //detail.ValidationTime,
                            //detail.ExpireTime,
                            Enum.GetName(typeof(DcsBaseDataStatus),detail.Status),
                            detail.Remark,
                            detail.CreatorName,
                            detail.CreateTime,
                            detail.ModifierName,
                            detail.ModifyTime,
                            detail.ApproverName,
                            detail.ApproveTime,
                            detail.AbandonerName,
                            detail.AbandonTime
                        };
                        return values;
                    });
                }
            } else { 
                using (var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if (index == dataArray.Count() + 1)
                            return null;
                        if (index == 0)
                            return excelColumns.ToArray<Object>();
                        return null;
                    });
                }
            }
            return exportDataFileName;
        }
        public string ExportPartsSalesPriceGroup(int[] ids, int branchId, string sparePartCode, string sparePartName, int? partsSalesCategoryId, int? priceType, int? status, bool? isSalable, bool? isOrderable, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? modifyTimeBegin, DateTime? modifyTimeEnd, bool? isExactExport) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("配件销售价_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = 主查询配件销售价(isSalable, isOrderable, partsSalesCategoryId, priceType, null).Where(r => r.BranchId == branchId);

            if(ids != null && ids.Any()) {
                data = data.Where(r => ids.Contains(r.Id));
            } else {
                if(!string.IsNullOrEmpty(sparePartCode)) {
                    var spareparts = sparePartCode.Split(',');
                    if(spareparts.Length == 1) {
                        var sparepartcode = spareparts[0];
                        if(isExactExport.HasValue && isExactExport.Value == true) {
                            data = data.Where(r => r.SparePartCode == sparepartcode); ;
                        } else {
                            data = data.Where(r => r.SparePartCode.Contains(sparepartcode)); ;
                        }
                    } else {
                        data = data.Where(r => spareparts.Contains(r.SparePartCode));
                    }
                }
                if(!string.IsNullOrEmpty(sparePartName)) {
                    data = data.Where(r => r.SparePartName.Contains(sparePartName));
                }
                if(status.HasValue) {
                    data = data.Where(r => r.Status == status);
                }
                if(createTimeBegin.HasValue) {
                    data = data.Where(r => r.CreateTime >= createTimeBegin);
                }
                if(createTimeEnd.HasValue) {
                    data = data.Where(r => r.CreateTime <= createTimeEnd);
                }
                if(modifyTimeBegin.HasValue) {
                    data = data.Where(r => r.ModifyTime >= modifyTimeBegin);
                }
                if(modifyTimeEnd.HasValue) {
                    data = data.Where(r => r.ModifyTime <= modifyTimeEnd);
                }
            }

            var partsSalesOrders = data;
            var dataArray = partsSalesOrders.ToArray();
            var partsSalePriceIncreaseRateIds = dataArray.Select(r => r.PriceType).Distinct().ToArray();
            var partsSalePriceIncreaseRates = ObjectContext.PartsSalePriceIncreaseRates.Where(r => partsSalePriceIncreaseRateIds.Contains(r.Id)).ToArray();

            excelColumns.Add(ErrorStrings.ExportColumn_PartsSalesCategory);
            excelColumns.Add(ErrorStrings.ExportColumn_SparePartCode);
            excelColumns.Add(ErrorStrings.ExportColumn_SparePartName);
            excelColumns.Add("集团客户价");
            excelColumns.Add(ErrorStrings.ExportColumn_CenterPrice);
            excelColumns.Add(ErrorStrings.ExportColumn_PriceType);
            //excelColumns.Add("是否用于索赔");
            //excelColumns.Add(ErrorStrings.ExportColumn_ValidFrom);
            //excelColumns.Add(ErrorStrings.ExportColumn_ValidTo);
            excelColumns.Add(ErrorStrings.ExportColumn_Status);
            excelColumns.Add(ErrorStrings.ExportColumn_Remark);
            excelColumns.Add(ErrorStrings.ExportColumn_Creator);
            excelColumns.Add(ErrorStrings.ExportColumn_CreateTime);
            excelColumns.Add(ErrorStrings.ExportColumn_Modifier);
            excelColumns.Add(ErrorStrings.ExportColumn_ModifyTime);
            excelColumns.Add(ErrorStrings.ExportColumn_Approver);
            excelColumns.Add(ErrorStrings.ExportColumn_ApproveTime);
            excelColumns.Add(ErrorStrings.ExportColumn_Abandoner);
            excelColumns.Add(ErrorStrings.ExportColumn_AbandonTime);

            if(partsSalesOrders.Any()) {
                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var rate = partsSalePriceIncreaseRates.Where(r => r.Id == detail.PriceType).FirstOrDefault();
                        var values = new object[] {
                            detail.PartsSalesCategoryName,
                            detail.SparePartCode,
                            detail.SparePart.Name,
                            detail.SparePart.PartsRetailGuidePrices.Any()? detail.SparePart.PartsRetailGuidePrices.First().RetailGuidePrice:0,
                            detail.CenterPrice,
                            rate==null?null:rate.GroupCode,
                            //detail.IfClaim.HasValue?(detail.IfClaim.Value?EntityStrings.Export_Title_Yes:EntityStrings.Export_Title_No):"",
                            //detail.ValidationTime,
                            //detail.ExpireTime,
                            Enum.GetName(typeof(DcsBaseDataStatus),detail.Status),
                            detail.Remark,
                            detail.CreatorName,
                            detail.CreateTime,
                            detail.ModifierName,
                            detail.ModifyTime,
                            detail.ApproverName,
                            detail.ApproveTime,
                            detail.AbandonerName,
                            detail.AbandonTime
                        };
                        return values;
                    });
                }
            } else {
                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        return null;
                    });
                }
            }
            return exportDataFileName;
        }
    }
}

