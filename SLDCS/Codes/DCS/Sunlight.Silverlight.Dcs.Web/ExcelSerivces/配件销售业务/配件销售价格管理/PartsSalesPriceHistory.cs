﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string ExportPartsSalesPriceHistory(int BranchId, string sparePartCode, string sparePartName, int? partsSalesCategoryId, DateTime? createTimeBegin, DateTime? createTimeEnd) {
            var excelColumns = new List<string>();

            var fileName = GetExportFilePath(string.Format("配件销售价_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));

            var data = ObjectContext.PartsSalesPriceChangeDetails
                       .Include("PartsSalesPriceChange")
                       .Where(c => c.PartsSalesPriceChange.BranchId == BranchId
                           && c.PartsSalesPriceChange.Status == (int)DcsPartsSalesPriceChangeStatus.生效);

            if(!string.IsNullOrEmpty(sparePartCode)) {
                data = data.Where(r => r.SparePartCode.Contains(sparePartCode));
            }
            if(!string.IsNullOrEmpty(sparePartName)) {
                data = data.Where(r => r.SparePartName.Contains(sparePartName));
            }
            if(partsSalesCategoryId.HasValue) {
                data = data.Where(r => r.PartsSalesPriceChange.PartsSalesCategoryId == partsSalesCategoryId);
            }
            if(createTimeBegin.HasValue) {
                data = data.Where(r => r.PartsSalesPriceChange.CreateTime >= createTimeBegin);
            }
            if(createTimeEnd.HasValue) {
                data = data.Where(r => r.PartsSalesPriceChange.CreateTime <= createTimeEnd);
            }

            data = data.OrderBy(r => r.Id);

            var partsSalesOrders = data;
            var dataArray = partsSalesOrders.ToArray();
            if(partsSalesOrders.Any()) {
                excelColumns.Add(ErrorStrings.ExportColumn_ApplicationCode);
                excelColumns.Add(ErrorStrings.ExportColumn_PartCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SparePartName);
                excelColumns.Add(ErrorStrings.ExportColumn_PartsSalesCategory);
                excelColumns.Add(ErrorStrings.ExportColumn_PartsSalesPrice);
                excelColumns.Add(ErrorStrings.ExportColumn_CenterPrice);
                excelColumns.Add(ErrorStrings.ExportColumn_DelaerPrices);
                excelColumns.Add(ErrorStrings.ExportColumn_PriceType);
                excelColumns.Add(ErrorStrings.ExportColumn_Creator);
                excelColumns.Add(ErrorStrings.ExportColumn_CreateTime);
                excelColumns.Add(ErrorStrings.ExportColumn_Modifier);
                excelColumns.Add(ErrorStrings.ExportColumn_ModifyTime);
                excelColumns.Add(ErrorStrings.ExportColumn_InitApprover);
                excelColumns.Add(ErrorStrings.ExportColumn_InitApproveTime);
                excelColumns.Add(ErrorStrings.ExportColumn_FinalApproveName);
                excelColumns.Add(ErrorStrings.ExportColumn_FinalApproveTime);

                using(var excelExport = new ExcelExport(fileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.PartsSalesPriceChange.Code,
                            detail.SparePartCode,
                            detail.SparePartName,
                            detail.PartsSalesPriceChange.PartsSalesCategoryName,
                            detail.RetailGuidePrice,
                            detail.CenterPrice,
                            detail.SalesPrice,
                            Enum.GetName(typeof(DcsPartsSalesPricePriceType),detail.PriceType),
                            detail.PartsSalesPriceChange.CreatorName,
                            detail.PartsSalesPriceChange.CreateTime,
                            detail.PartsSalesPriceChange.ModifierName,
                            detail.PartsSalesPriceChange.ModifyTime,
                            detail.PartsSalesPriceChange.InitialApproverName,
                            detail.PartsSalesPriceChange.InitialApproveTime,
                            detail.PartsSalesPriceChange.FinalApproverName,
                            detail.PartsSalesPriceChange.FinalApproveTime
                        };
                        return values;
                    });
                }
            }
            return fileName;
        }
    }
}
