﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string ExportPartsSalesPriceRetailGuidePriceByBranchType(int[] ids, string sparePartCode, string sparePartName, int? branchId, int? status, int? partsSalesCategoryId, int? priceType, bool? isSalable, bool? isOrderable, DateTime? createTimeBegin, DateTime? createTimeEnd) {

            var exportDataFileName = GetExportFilePath(string.Format("配件销售价查询_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var partsSalesPriceRetailGuidePrices = GetPartsSalesPriceRetailGuidePriceByBranchType(isSalable, isOrderable);

            if(ids != null && ids.Any()) {
                partsSalesPriceRetailGuidePrices = partsSalesPriceRetailGuidePrices.Where(r => ids.Contains(r.Id));
            } else {
                if(!string.IsNullOrEmpty(sparePartCode)) {
                    partsSalesPriceRetailGuidePrices = partsSalesPriceRetailGuidePrices.Where(r => r.SparePartCode.Contains(sparePartCode));
                }
                if(!string.IsNullOrEmpty(sparePartName)) {
                    partsSalesPriceRetailGuidePrices = partsSalesPriceRetailGuidePrices.Where(r => r.SparePartName.Contains(sparePartName));
                }
                if(branchId.HasValue) {
                    partsSalesPriceRetailGuidePrices = partsSalesPriceRetailGuidePrices.Where(r => r.BranchId == branchId.Value);
                }
                if(status.HasValue) {
                    partsSalesPriceRetailGuidePrices = partsSalesPriceRetailGuidePrices.Where(r => r.Status == status.Value);
                }
                if(partsSalesCategoryId.HasValue) {
                    partsSalesPriceRetailGuidePrices = partsSalesPriceRetailGuidePrices.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId.Value);
                }
                if(priceType.HasValue) {
                    partsSalesPriceRetailGuidePrices = partsSalesPriceRetailGuidePrices.Where(r => r.PriceType == priceType.Value);
                }
                if(createTimeBegin.HasValue) {
                    partsSalesPriceRetailGuidePrices = partsSalesPriceRetailGuidePrices.Where(r => r.CreateTime >= createTimeBegin.Value);
                }
                if(createTimeEnd.HasValue) {
                    partsSalesPriceRetailGuidePrices = partsSalesPriceRetailGuidePrices.Where(r => r.CreateTime <= createTimeEnd.Value);
                }
            }

            var queryResult = partsSalesPriceRetailGuidePrices.ToArray().Select(r => new object[]{
            r.PartsSalesCategoryName,
            r.SparePartCode,
            r.SparePartName,
            r.SalesPrice,
            r.CenterPrice,
            r.RetailGuidePrice,
            Enum.GetName(typeof(DcsBaseDataStatus),r.Status),
            r.Remark           
            }).ToArray();

            var excelColumns = new List<string>{
				ErrorStrings.ExportColumn_PartsSalesCategory,
				ErrorStrings.ExportColumn_PartCode,
				ErrorStrings.ExportColumn_SparePartName,
				ErrorStrings.ExportColumn_DealerPrice,
				ErrorStrings.ExportColumn_CenterPrice,
				ErrorStrings.ExportColumn_PartsSalesPrice,
				ErrorStrings.ExportColumn_Status,
				ErrorStrings.ExportColumn_Remark				
            };

            return ExportAllData(queryResult, excelColumns, "配件销售价查询");
        }
    }
}
