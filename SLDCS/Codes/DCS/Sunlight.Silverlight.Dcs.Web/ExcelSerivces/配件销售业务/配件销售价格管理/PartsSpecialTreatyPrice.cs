﻿
using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string ExportPartsSpecialTreatyPrice(int[] ids, int? branchId, string companyCode, string companyName, int? partsSalesCategoryId, int? status, string sparePartsCode, string sparePartName, DateTime? createTimeStart, DateTime? createTimeEnd) {
            var query = GetPartsSpecialTreatyPricesWithCompany();
            if(ids != null && ids.Any()) {
                query = query.Where(r => ids.Contains(r.Id));
            } else {
                if(branchId.HasValue) {
                    query = query.Where(r => r.BranchId == branchId.Value);
                }
                if(!string.IsNullOrEmpty(companyCode)) {
                    query = query.Where(r => r.Company.Code.Contains(companyCode));
                }
                if(!string.IsNullOrEmpty(companyName)) {
                    query = query.Where(r => r.Company.Name.Contains(companyName));
                }
                if(partsSalesCategoryId.HasValue) {
                    query = query.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId.Value);
                }
                if(status.HasValue) {
                    query = query.Where(r => r.Status == status.Value);
                }
                if(!string.IsNullOrEmpty(sparePartsCode)) {
                    query = query.Where(r => r.SparePartCode.Contains(sparePartsCode));
                }
                if(!string.IsNullOrEmpty(sparePartName)) {
                    query = query.Where(r => r.SparePartName.Contains(sparePartName));
                }
                if(createTimeStart.HasValue) {
                    query = query.Where(r => r.CreateTime >= createTimeStart.Value);
                }
                if(createTimeEnd.HasValue) {
                    query = query.Where(r => r.CreateTime <= createTimeEnd);
                }
            }
            var queryReuslt = query.ToArray().Select(r => new object[] {
                r.Company.Code,
                r.Company.Name,
                r.PartsSalesCategoryName,
                r.SparePart.Code,
                r.SparePart.Name,
                r.ValidationTime,
                r.ExpireTime,
                r.TreatyPrice,
                Enum.GetName(typeof(DcsBaseDataStatus),r.Status),
                r.Remark,
                r.CreatorName,
                r.CreateTime,
                r.ModifierName,
                r.ModifyTime,
                r.AbandonerName,
                r.AbandonTime
            }).ToArray();

            var excelColumns = new List<string> {
                 ErrorStrings.ExportColumn_CompanyCode,
                 ErrorStrings.ExportColumn_CompanyName,
                 ErrorStrings.ExportColumn_PartsSalesCategory, 
                 ErrorStrings.ExportColumn_PartCode,
                 ErrorStrings.ExportColumn_SparePartName, 
                 ErrorStrings.ExportColumn_ValidFrom,
                 ErrorStrings.ExportColumn_ValidTo,
                 ErrorStrings.ExportColumn_AgreementPrice,
                 ErrorStrings.ExportColumn_Status,
                 ErrorStrings.ExportColumn_Remark,
                 ErrorStrings.ExportColumn_Creator,
                 ErrorStrings.ExportColumn_CreateTime,
                 ErrorStrings.ExportColumn_Modifier,
                 ErrorStrings.ExportColumn_ModifyTime,
                 ErrorStrings.ExportColumn_Abandoner,
                 ErrorStrings.ExportColumn_AbandonTime
            };
            return ExportAllData(queryReuslt, excelColumns, "配件特殊协议价");
        }
    }
}
