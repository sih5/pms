﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web
{
    partial class DcsDomainService
    {
        public string ExportPurchasePricingChangeHis(int[] ids, string sparePartCode, string sparePartName,string referenceCode, DateTime? CreateTimeStart, DateTime? CreateTimeFromEnd)
        {

            var query = 查询采购价格变动情况记录();
            if (ids != null && ids.Any())
            {
                query = query.Where(r => ids.Contains(r.Id));
            }
            else
            {
                if (!string.IsNullOrEmpty(sparePartCode))
                {
                    query = query.Where(r => r.SparePartCode.Contains(sparePartCode));
                }
                if (!string.IsNullOrEmpty(referenceCode))
                {
                    query = query.Where(r => r.ReferenceCode.Contains(referenceCode));
                }
                if (!string.IsNullOrEmpty(sparePartName))
                {
                    query = query.Where(r => r.SparePartName.Contains(sparePartName));
                }
                if (CreateTimeStart.HasValue)
                    query = query.Where(r => r.CreateTime >= CreateTimeStart);
                if (CreateTimeFromEnd.HasValue)
                    query = query.Where(r => r.CreateTime <= CreateTimeFromEnd);

            } var queryReuslt = query.ToArray().Select(r => new object[] {
               r.PartsSalesCategoryName,
                Enum.GetName(typeof(DcsMasterDataStatus),r.Status),
               r.SparePartCode,
               r.SparePartName,
               r.ReferenceCode,
               r.IsPrimary==true?EntityStrings.Export_Title_Yes:EntityStrings.Export_Title_No,
               r.OldPurchasePrice,
               r.PurchasePrice,
               r.ChangeRatio,
               r.CenterPrice,
               r.DistributionPrice,
               r.RetailPrice,
               r.MaintenanceTime,
               r.ValidFrom,
               r.ValidTo,
               r.IncreaseRateGroupId,
               r.Remark,
               r.CreatorName,
               r.CreateTime,
               r.ModifierName,
               r.ModifyTime

            }).ToArray();

            var excelColumns = new List<string> {
                ErrorStrings.ExportColumn_PartsSalesCategory,
                ErrorStrings.ExportColumn_Status,
                ErrorStrings.ExportColumn_SparePartCode,
                ErrorStrings.ExportColumn_SparePartName,
                ErrorStrings.ExportColumn_SIHCode,
                ErrorStrings.ExportColumn_IsPrimary,
                ErrorStrings.ExportColumn_OldPurchasePrice,
                ErrorStrings.ExportColumn_ChangePurchasePrice,
                ErrorStrings.ExportColumn_ChangeRate,
                ErrorStrings.ExportColumn_CenterPrice,
                ErrorStrings.ExportColumn_DealerPrice,
                ErrorStrings.ExportColumn_RetailsPrices,
                ErrorStrings.ExportColumn_SalesPriceModifty,
				ErrorStrings.ExportColumn_ValidFrom,
				ErrorStrings.ExportColumn_ValidTo,
                ErrorStrings.ExportColumn_PriceProp,
                ErrorStrings.ExportColumn_Remark,
                ErrorStrings.ExportColumn_Creator,
                ErrorStrings.ExportColumn_CreateTime,
                ErrorStrings.ExportColumn_Modifier,
                ErrorStrings.ExportColumn_ModifyTime
            };
            return ExportAllData(queryReuslt, excelColumns, "采购价格变动情况记录");
        }
    }
}
