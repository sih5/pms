﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string ExportPartsRebateApplication(int[] ids, int? branchId, int? partsSalesCategoryId, string code, string customerCompanyCode, string customerCompanyName, string partsRebateTypeName, string accountGroupName, DateTime? createTimeStart, DateTime? createTimeEnd, int? status, string creatorName, string businessCode) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("配件返利申请单_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = GetVirtualPartsRebateApplicationWithDetails(partsSalesCategoryId, code, customerCompanyCode, customerCompanyName, accountGroupName, partsRebateTypeName, status, creatorName, createTimeStart, createTimeEnd, businessCode);

            if(ids.Count() > 0) {
                data = data.Where(r => ids.Contains(r.Id));
            } else {
                if(branchId.HasValue) {
                    data = data.Where(r => r.BranchId == branchId);
                }
                if(partsSalesCategoryId.HasValue) {
                    data = data.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId);
                }
                if(!string.IsNullOrEmpty(code)) {
                    data = data.Where(r => r.Code.Contains(code));
                }
                if(!string.IsNullOrEmpty(customerCompanyCode)) {
                    data = data.Where(r => r.CustomerCompanyCode.Contains(customerCompanyCode));
                }
                if(!string.IsNullOrEmpty(customerCompanyName)) {
                    data = data.Where(r => r.CustomerCompanyName.Contains(customerCompanyName));
                }
                if(!string.IsNullOrEmpty(partsRebateTypeName)) {
                    data = data.Where(r => r.PartsRebateTypeName == partsRebateTypeName);
                }
                if(!string.IsNullOrEmpty(accountGroupName)) {
                    data = data.Where(r => r.AccountGroupName.Contains(accountGroupName));
                }
                if(createTimeStart.HasValue) {
                    data = data.Where(r => r.CreateTime >= createTimeStart);
                }
                if(createTimeEnd.HasValue) {
                    data = data.Where(r => r.CreateTime <= createTimeEnd);
                }
                if(status.HasValue) {
                    data = data.Where(r => r.Status == status);
                }
                if(!string.IsNullOrEmpty(creatorName)) {
                    data = data.Where(r => r.CreatorName.Contains(creatorName));
                }
                if(!string.IsNullOrEmpty(businessCode)) {
                    data = data.Where(r => r.BusinessCode.Contains(businessCode));
                }
            }

            var dataArray = data.ToArray();
            if(data.Any()) {
                excelColumns.Add("申请单编号");
                excelColumns.Add("营销分公司");
                excelColumns.Add("客户企业编号");
                excelColumns.Add("客户企业名称");
                excelColumns.Add("品牌");
                excelColumns.Add("配件返利类型");
                excelColumns.Add("状态");
                excelColumns.Add("返利方向");
                excelColumns.Add("账户组");
                excelColumns.Add("金额");
                excelColumns.Add("申请原因");
                excelColumns.Add("审批意见");
                excelColumns.Add("备注");
                excelColumns.Add("创建人");
                excelColumns.Add("创建时间");
                excelColumns.Add("修改人");
                excelColumns.Add("修改时间");
                excelColumns.Add("作废人");
                excelColumns.Add("作废时间");
                excelColumns.Add("审核人");
                excelColumns.Add("审核时间");

                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.Code,
                            detail.BranchName,
                            detail.CustomerCompanyCode,
                            detail.CustomerCompanyName,
                            detail.PartsSalesCategoryName,
                            detail.PartsRebateTypeName,
                            Enum.GetName(typeof(DcsWorkflowOfSimpleApprovalStatus),detail.Status),
                            Enum.GetName(typeof(DcsPartsRebateDirection), detail.RebateDirection),
                            detail.AccountGroupName,
                            detail.Amount,
                            detail.Motive,
                            detail.ApprovalComment,
                            detail.Remark,
                            detail.CreatorName,
                            detail.CreateTime,
                            detail.ModifierName,
                            detail.ModifyTime,
                            detail.AbandonerName,
                            detail.AbandonTime,
                            detail.ApproverName,
                            detail.ApproveTime
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
    }
}
