﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web
{
    partial class DcsDomainService
    {
        public string ExportPartsSalesRtnSettlement(int[] ids, int? salesCompanyId, int? partsSalesCategoryId, string code, string customerCompanyCode, string customerCompanyName, int? accountGroupId, DateTime? createTimeStart, DateTime? createTimeEnd, string creatorName, int? status)
        {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("配件销售退货结算单_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = 查询配件销售退货结算单();
            if ((ids!=null) && (ids.Count() > 0))
            {
                data = data.Where(r => ids.Contains(r.Id));
            }
            else
            {
                if (salesCompanyId.HasValue)
                {
                    data = data.Where(r => r.SalesCompanyId == salesCompanyId);
                }
                if (partsSalesCategoryId.HasValue)
                {
                    data = data.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId);
                }
                if (!string.IsNullOrEmpty(code))
                {
                    data = data.Where(r => r.Code.Contains(code));
                }
                if (!string.IsNullOrEmpty(customerCompanyCode))
                {
                    data = data.Where(r => r.CustomerCompanyCode.Contains(customerCompanyCode));
                }
                if (!string.IsNullOrEmpty(customerCompanyName))
                {
                    data = data.Where(r => r.CustomerCompanyName.Contains(customerCompanyName));
                }
                if (accountGroupId.HasValue)
                {
                    data = data.Where(r => r.AccountGroupId == accountGroupId);
                }
                if (createTimeStart.HasValue)
                {
                    data = data.Where(r => r.CreateTime >= createTimeStart);
                }
                if (createTimeEnd.HasValue)
                {
                    data = data.Where(r => r.CreateTime <= createTimeEnd);
                }
                if (status.HasValue)
                {
                    data = data.Where(r => r.Status == status);
                }
                if (!string.IsNullOrEmpty(creatorName))
                {
                    data = data.Where(r => r.CreatorName.Contains(creatorName));
                }
            }
            var dataArray = data.ToArray();

            var rtnSettlementIds = dataArray.Select(r => r.Id).ToArray();
            var queryResult = from a in this.ObjectContext.PartsSalesRtnSettlementRefs
                              from b in this.ObjectContext.PartsInboundCheckBillDetails
                              where a.SourceId == b.PartsInboundCheckBillId
                              select new {
                                  rtnSettlementId = a.PartsSalesRtnSettlementId,
                                  tempAmount = b.CostPrice * b.InspectedQuantity
                              };
            var tempQuery = from a in queryResult
                            group a by new {
                                a.rtnSettlementId
                            } into tempTable
                            select new {
                                rtnSettlementId = tempTable.Key.rtnSettlementId,
                                totalAmount = tempTable.Sum(r => r.tempAmount)
                            };
            var rtnQuery = tempQuery.Where(r => rtnSettlementIds.Contains(r.rtnSettlementId)).ToArray();

            if (data.Any())
            {
                excelColumns.Add("结算单编号");
                excelColumns.Add("销售企业");
                excelColumns.Add("品牌");
                excelColumns.Add("客户账户");
                excelColumns.Add("客户企业编码");
                excelColumns.Add("客户企业名称");
                excelColumns.Add("结算总金额");
                excelColumns.Add("成本价合计");
                excelColumns.Add("折扣金额");
                excelColumns.Add("税率");
                excelColumns.Add("税额");
                excelColumns.Add("开票差异金额");
                excelColumns.Add("状态");
                excelColumns.Add("开票方向");
                excelColumns.Add("结算方向");
                excelColumns.Add("被反冲结算单编号");
                excelColumns.Add("备注");
                excelColumns.Add("创建人");
                excelColumns.Add("创建时间");
                using (var excelExport = new ExcelExport(exportDataFileName))
                {
                    excelExport.ExportByRow(index =>
                    {
                        if (index == dataArray.Count() + 1)
                            return null;
                        if (index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];

                        var temp = rtnQuery.SingleOrDefault(r => r.rtnSettlementId == detail.Id);
                        var plannedPriceSum = temp == null ? 0 : temp.totalAmount;

                        var values = new object[] {
                            detail.Code,
                            detail.SalesCompanyName,
                            detail.PartsSalesCategoryName,
                            detail.AccountGroupName,
                            detail.CustomerCompanyCode,
                            detail.CustomerCompanyName,
                            detail.TotalSettlementAmount,
                            plannedPriceSum,
                            detail.Discount,
                            detail.TaxRate,
                            detail.Tax,
                            detail.InvoiceAmountDifference,
                            Enum.GetName(typeof(DcsPartsSalesSettlementStatus),detail.Status),
                            Enum.GetName(typeof(DcsPartsPurchaseRtnSettleBillInvoicePath),detail.InvoicePath),    
                            Enum.GetName(typeof(DcsPartsPurchaseSettleBillSettlementPath),detail.SettlementPath),
                            detail.OffsettedSettlementBillCode,
                            detail.Remark,
                            detail.CreatorName,
                            detail.CreateTime
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
    }
}
