﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string ExportPartsSalesSettlement(int[] ids, int? salesCompanyId, int? partsSalesCategoryId, string code, string customerCompanyCode, string customerCompanyName, int? accountGroupId, DateTime? createTimeStart, DateTime? createTimeEnd, string creatorName, int? status, int? settleType, int? businessType, string businessCode) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("配件销售结算单_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = 查询配件销售结算单统计计划价合计();
            data = data.Where(r => r.IsUnifiedSettle == null || r.IsUnifiedSettle == false);
            if(ids.Count() > 0) {
                data = data.Where(r => ids.Contains(r.Id));
            } else {
                if(salesCompanyId.HasValue) {
                    data = data.Where(r => r.SalesCompanyId == salesCompanyId);
                }
                if(partsSalesCategoryId.HasValue) {
                    data = data.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId);
                }
                if(!string.IsNullOrEmpty(code)) {
                    data = data.Where(r => r.Code.Contains(code));
                }
                if(!string.IsNullOrEmpty(customerCompanyCode)) {
                    data = data.Where(r => r.CustomerCompanyCode.Contains(customerCompanyCode));
                }
                if(!string.IsNullOrEmpty(customerCompanyName)) {
                    data = data.Where(r => r.CustomerCompanyName.Contains(customerCompanyName));
                }
                if(accountGroupId.HasValue) {
                    data = data.Where(r => r.AccountGroupId == accountGroupId);
                }
                if(createTimeStart.HasValue) {
                    data = data.Where(r => r.CreateTime >= createTimeStart);
                }
                if(createTimeEnd.HasValue) {
                    data = data.Where(r => r.CreateTime <= createTimeEnd);
                }
                if(status.HasValue) {
                    data = data.Where(r => r.Status == status);
                }
                if(!string.IsNullOrEmpty(creatorName)) {
                    data = data.Where(r => r.CreatorName.Contains(creatorName));
                }
                if(settleType.HasValue) {
                    data = data.Where(r => r.SettleType == settleType);
                }
                if(businessType.HasValue) {
                    data = data.Where(r => r.BusinessType == businessType);
                }
                if(!string.IsNullOrEmpty(businessCode)) {
                    data = data.Where(r => r.BusinessCode.Contains(businessCode));
                }
            }
            var dataArray = data.ToArray();
            if(data.Any()) {
                excelColumns.Add("状态");
                excelColumns.Add("结算单编号");
                excelColumns.Add("销售企业");
                excelColumns.Add("客户账户");
                excelColumns.Add("客户企业编码");
                excelColumns.Add("客户企业名称");
                excelColumns.Add("结算总金额");
                excelColumns.Add("不含税金额");
                excelColumns.Add("折让不含税金额");
                excelColumns.Add("税率");
                excelColumns.Add("税额");
                excelColumns.Add("结算方向");
                excelColumns.Add("被反冲结算单编号");
                excelColumns.Add("备注");
                excelColumns.Add("创建人");
                excelColumns.Add("创建时间");
                excelColumns.Add("修改人");
                excelColumns.Add("修改时间");
                excelColumns.Add("审核人");
                excelColumns.Add("审核时间");
                excelColumns.Add("发票登记人");
                excelColumns.Add("发票登记时间");
                excelColumns.Add("作废人");
                excelColumns.Add("作废时间");
                excelColumns.Add("过账日期");
                excelColumns.Add("结算类型");
                excelColumns.Add("业务类型");
                excelColumns.Add("品牌");

                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            Enum.GetName(typeof(DcsPartsSalesSettlementStatus),detail.Status),
                            detail.Code,
                            detail.SalesCompanyName,                            
                            detail.AccountGroupName,
                            detail.CustomerCompanyCode,
                            detail.CustomerCompanyName,
                            detail.TotalSettlementAmount,
                            detail.NoTaxAmount,
                            detail.RebateAmount,
                            detail.TaxRate,
                            detail.Tax,
                            Enum.GetName(typeof(DcsPartsPurchaseSettleBillSettlementPath),detail.SettlementPath),
                            detail.OffsettedSettlementBillCode,
                            detail.Remark,
                            detail.CreatorName,
                            detail.CreateTime,
                            detail.ModifierName,
                            detail.ModifyTime,
                            detail.ApproverName,
                            detail.ApproveTime,
                            detail.InvoiceRegistrationOperator,
                            detail.InvoiceRegistrationTime,
                            detail.AbandonerName,
                            detail.AbandonTime,
                            detail.InvoiceDate,
                            Enum.GetName(typeof(DcsSalesOrderType_SettleType),detail.SettleType),
                            Enum.GetName(typeof(DcsSalesOrderType_BusinessType),detail.BusinessType),
                            detail.PartsSalesCategoryName
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }

        public string ExportVirtualPartsSalesSettlement(int[] selectedIds, int id) {
            var excelColumns = new List<string> {
                                    "出入库单编号",
                                    "出入库单类型",
                                    "配件编号",
                                    "配件名称",
                                    "数量",
                                    "结算价",
                                    "结算金额",
                                    "成本价",
                                    "材料成本差异",
                                    "成本金额"
                                };
            var exportDataFileName = GetExportFilePath(string.Format("虚拟配件销售结算成本_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = 查询虚拟配件销售结算成本(id);

            if(selectedIds.Count() > 0) {
                data = data.Where(r => selectedIds.Contains(r.Id));
            }

            var dataArray = data.ToArray();
            if(data.Any()) {
                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.BillCode,
                            detail.BillBusinessType,
                            detail.SparePartCode,
                            detail.SparePartName,
                            detail.Quantity,
                            detail.SettlementPrice,
                            detail.SettlementAmount,
                            detail.PlanPrice,
                            detail.MaterialCostVariance,
                            detail.PlanAmount
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
    }
}
