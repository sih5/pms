﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string 导出结算单与出入库单关联发票统计(DateTime? bOutCreateTime, DateTime? eOutCreateTime, DateTime? bCreateTime, DateTime? eCreateTime, string partsOutboundBillCode, string code, string invoiceNumber, string submitCompanyName) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("结算单与出入库单关联发票查询_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = GetSettlementInvoices(bOutCreateTime, eOutCreateTime, bCreateTime, eCreateTime, partsOutboundBillCode, code, invoiceNumber, submitCompanyName);
            var dataArray = data.ToArray();
            if(data.Any()) {
                excelColumns.Add(ErrorStrings.ExportColumn_MarketingDepartment);
                excelColumns.Add(ErrorStrings.ExportColumn_WarehouseName);
                excelColumns.Add(ErrorStrings.ExportColumn_Province);
                excelColumns.Add(ErrorStrings.ExportColumn_PartsSalesOrder_Code);
                excelColumns.Add(ErrorStrings.ExportColumn_CustomerName);
                excelColumns.Add(ErrorStrings.ExportColumn_OutCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SIHCode);
                excelColumns.Add(ErrorStrings.ExportColumn_PartCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SparePartName);
                excelColumns.Add(ErrorStrings.ExportColumn_PurchaseOrTransfer);
                excelColumns.Add(ErrorStrings.ExportColumn_ProductType);
                excelColumns.Add(ErrorStrings.InOutSettleSummaryReport54);
                excelColumns.Add(ErrorStrings.ExportColumn_OutBoundPrice);
                excelColumns.Add(ErrorStrings.ExportColumn_OutBoundPriceAmount);
                excelColumns.Add(ErrorStrings.ExportColumn_DIscountPrice);
                excelColumns.Add(ErrorStrings.ExportColumn_DIscountAmount);
                excelColumns.Add(ErrorStrings.ExportColumn_OutboundTime);
                excelColumns.Add(ErrorStrings.ExportColumn_SettlementStatus);
                excelColumns.Add(ErrorStrings.ExportColumn_BusinessType);
                excelColumns.Add(ErrorStrings.ExportColumn_Remark);
                excelColumns.Add(ErrorStrings.ExportColumn_SettleBillCode);
                excelColumns.Add(ErrorStrings.ExportColumn_InvoiceNumber);
                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.MarketingDepartmentName,
                            detail.WarehouseName,
                            detail.Province,
                            detail.PartsSalesOrderCode,
                            detail.SubmitCompanyName,
                            detail.PartsOutboundBillCode,
                            detail.ReferenceCode,
                            detail.SparePartCode,
                            detail.SparePartName,
                            Enum.GetName(typeof(DcsPartsBranchPurchaseRoute),detail.PurchaseRoute==null?0:detail.PurchaseRoute),
                            Enum.GetName(typeof(DcsABCStrategyCategory),detail.PartABC==null?0:detail.PartABC),
                            detail.OutboundAmount,
                            detail.CostPrice,
                            detail.CostPriceAmount, 
                            detail.SettlementPrice,
                            detail.SettlementPriceAmount,
                            detail.PartsOutboundBillCreateTime,
                            Enum.GetName(typeof(DcsPartsSalesSettlementStatus),detail.Status==null?0:detail.Status),
                            Enum.GetName(typeof(DcsSalesOrderType_BusinessType),detail.BusinessType==null?0:detail.BusinessType),
                            detail.Remark,
                            detail.Code,
                            detail.InvoiceNumber
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
    }
}
