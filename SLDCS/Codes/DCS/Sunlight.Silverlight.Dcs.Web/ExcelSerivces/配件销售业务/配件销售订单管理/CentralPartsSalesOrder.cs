﻿using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string 导出中心库销售一次满足率日期报表(int? type, DateTime? bApproveTime, DateTime? eApproveTime) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("中心库销售一次满足率日期报表统计_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = 获取中心库销售一次满足率日期报表(type, bApproveTime, eApproveTime);
            var dataArray = data.ToArray();
            if(data.Any()) {
                excelColumns.Add("年度");
                excelColumns.Add("月份");
                excelColumns.Add("日期");
                excelColumns.Add("订单行数");
                excelColumns.Add("订单满足行数");
                excelColumns.Add("未满足行数");
                excelColumns.Add("一次满足率(%)");
                excelColumns.Add("订单总金额");
                excelColumns.Add("审核通过金额");
                
                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.Year,
                            detail.Month,
                            detail.Day,
                            detail.OrderAmount,
                            detail.SatisfactionAmount, 
                            detail.UnSatisfactionAmount,
                            detail.OneTimeRate,
                            detail.TotalOrderAmount,
                            detail.ApproveTotalOrderAmount                           
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
        public string 导出中心库销售一次满足率服务站报表(int? type, DateTime? bApproveTime, DateTime? eApproveTime, string centerName, string dealerName) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("中心库销售一次满足率服务站报表统计_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = 获取中心库销售一次满足率服务站报表(type, bApproveTime, eApproveTime, centerName, dealerName);
            var dataArray = data.ToArray();
            if(data.Any()) {
                excelColumns.Add("中心库名称");
                excelColumns.Add("服务站名称");
                excelColumns.Add("订单行数");
                excelColumns.Add("订单满足行数");
                excelColumns.Add("未满足行数");
                excelColumns.Add("一次满足率(%)");
                excelColumns.Add("订单总金额");
                excelColumns.Add("审核通过金额");

                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.CenterName,
                            detail.DealerName,
                            detail.OrderAmount,
                            detail.SatisfactionAmount, 
                            detail.UnSatisfactionAmount,
                            detail.OneTimeRate,
                            detail.TotalOrderAmount,
                            detail.ApproveTotalOrderAmount                           
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
        public string 导出配件公司销售一次满足率日期报表(int? type, DateTime? bApproveTime, DateTime? eApproveTime) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("配件公司销售一次满足率日期报表统计_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = 获取配件公司销售一次满足率日期报表(type, bApproveTime, eApproveTime);
            var dataArray = data.ToArray();
            if(data.Any()) {
                excelColumns.Add("年度");
                excelColumns.Add("月份");
                excelColumns.Add("日期");
                excelColumns.Add("订单行数");
                excelColumns.Add("订单满足行数");
                excelColumns.Add("未满足行数");
                excelColumns.Add("一次满足率(%)");
                excelColumns.Add("订单总金额");
                excelColumns.Add("审核通过金额");

                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.Year,
                            detail.Month,
                            detail.Day,
                            detail.OrderAmount,
                            detail.SatisfactionAmount, 
                            detail.UnSatisfactionAmount,
                            detail.OneTimeRate,
                            detail.TotalOrderAmount,
                            detail.ApproveTotalOrderAmount                           
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
        public string 导出配件公司销售一次满足率对中心库报表(DateTime? bApproveTime, DateTime? eApproveTime, string centerName) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("中配件公司销售一次满足率对中心库报表统计_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = 获取配件公司销售一次满足率对中心库报表(bApproveTime, eApproveTime, centerName);
            var dataArray = data.ToArray();
            if(data.Any()) {
                excelColumns.Add("中心库名称");
                excelColumns.Add("订单行数");
                excelColumns.Add("订单满足行数");
                excelColumns.Add("未满足行数");
                excelColumns.Add("一次满足率(%)");
                excelColumns.Add("订单总金额");
                excelColumns.Add("审核通过金额");

                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.CenterName,
                            detail.OrderAmount,
                            detail.SatisfactionAmount, 
                            detail.UnSatisfactionAmount,
                            detail.OneTimeRate,
                            detail.TotalOrderAmount,
                            detail.ApproveTotalOrderAmount                           
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
    }
}
