﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string ExportDealerPartsRetailOrderWithDetails(int[] ids, int? branchId, int? partsSalesCategoryId, string code,int? retailOrderType,string customer,string subDealerName,int? status, DateTime? createTimeBegin, DateTime? createTimeEnd) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("服务站/经销商零售订单主清单_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var datas = GetPartsRetailOrderWithDetailsForExport();
            IQueryable<DealerPartsRetailOrderEx> data = datas;
            var userInfo = Utils.GetCurrentUserInfo();
            data = data.Where(r => r.DealerId == userInfo.EnterpriseId);
            if(ids != null && ids.Length > 0) {
                data = data.Where(r => ids.Contains(r.Id));
            } else {
                if(branchId.HasValue) {
                    data = data.Where(r => r.BranchId == branchId);
                }
                if(partsSalesCategoryId.HasValue) {
                    data = data.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId);
                }
                if(!string.IsNullOrEmpty(code)) {
                    data = data.Where(r => r.Code.Contains(code));
                }
                if(retailOrderType.HasValue) {
                    data = data.Where(r => r.RetailOrderType == retailOrderType);
                }
                if(!string.IsNullOrEmpty(customer)) {
                    data = data.Where(r => r.Customer.Contains(customer));
                }
                if(!string.IsNullOrEmpty(subDealerName)) {
                    data = data.Where(r => r.SubDealerName.Contains(subDealerName));
                }
                if(status.HasValue) {
                    data = data.Where(r => r.Status == status);
                }
                if(createTimeBegin.HasValue) {
                    data = data.Where(r => r.CreateTime >= createTimeBegin);
                }
                if(createTimeEnd.HasValue) {
                    data = data.Where(r => r.CreateTime <= createTimeEnd);
                }
            }
            
            var dataArray = data.ToArray();


            if(dataArray.Any()) {
                excelColumns.Add("状态");
                excelColumns.Add("订单编号");
                excelColumns.Add("提报单位编号");
                excelColumns.Add("提报单位名称");
                excelColumns.Add("零售类型");
				excelColumns.Add("客户姓名");
				excelColumns.Add("客户电话");
				excelColumns.Add("二级站名称");
				excelColumns.Add("客户手机");
				excelColumns.Add("客户单位");
				excelColumns.Add("客户地址");
				excelColumns.Add("总金额");
				excelColumns.Add("审核意见");
				excelColumns.Add("备注");
				excelColumns.Add("创建人");
				excelColumns.Add("创建时间");
				excelColumns.Add("修改人");
				excelColumns.Add("修改时间");
				excelColumns.Add("审批人");
				excelColumns.Add("审批时间");
				excelColumns.Add("作废人");
				excelColumns.Add("作废时间");
				excelColumns.Add("分公司名称");
				excelColumns.Add("品牌");
				excelColumns.Add("序号");
				excelColumns.Add("配件编号");
				excelColumns.Add("配件名称");
				excelColumns.Add("数量");
				excelColumns.Add("单价");
				excelColumns.Add("备注");
                excelColumns.Add("追溯属性");
                excelColumns.Add("标签码");
                excelColumns.Add("经销价");
                excelColumns.Add("建议售价");
                excelColumns.Add("价格属性");
				
                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            Enum.GetName(typeof(DcsWorkflowOfSimpleApprovalStatus),detail.Status),
							detail.Code,
							detail.DealerCode,
							detail.DealerName,
							Enum.GetName(typeof(DcsDealerPartsRetailOrderRetailOrderType),detail.RetailOrderType),
							detail.Customer,
							detail.CustomerPhone,
							detail.SubDealerName,
							detail.CustomerCellPhone,
							detail.CustomerUnit,
							detail.Address,
							detail.TotalAmount,
							detail.ApprovalComment,
							detail.Remark,
							detail.CreatorName,
							detail.CreateTime,
							detail.ModifierName,
							detail.ModifyTime,
							detail.ApproverName,
							detail.ApproveTime,
							detail.AbandonerName,
							detail.AbandonTime,
							detail.BranchName,
							detail.SalesCategoryName,
							detail.SerialNumber,
							detail.PartsCode,
							detail.PartsName,
							detail.Quantity,
							detail.Price,
							detail.DetailRemark,
                            Enum.GetName(typeof(DCSTraceProperty),detail.TraceProperty??0),
                            detail.SIHLabelCode,
                            detail.SalesPrice,
                            detail.RetailGuidePrice,
                            detail.GroupCode
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
    }
}
