﻿using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string 导出中心库销售出库单(DateTime? bShippingDate, DateTime? eShippingDate, string companyCode, string companyName, string code, string sparePartCode, string referenceCode, string partsSalesOrderCode, string submitCompanyName, int? customerTypeInt, int? customerTypeTy)
        {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("中心库销售出库单统计_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = GetPartsOutboundBillCentrals(bShippingDate, eShippingDate, companyCode, companyName, code, sparePartCode, referenceCode, partsSalesOrderCode, submitCompanyName, customerTypeInt,customerTypeTy);
            var dataArray = data.ToArray();
            if(data.Any()) {
                excelColumns.Add(ErrorStrings.ExportColumn_MarketingDepartment);
                excelColumns.Add(ErrorStrings.ExportColumn_AgencyCode);
                excelColumns.Add(ErrorStrings.ExportColumn_AgencyName2);
                excelColumns.Add(ErrorStrings.ExportColumn_WarehouseName);
                excelColumns.Add(ErrorStrings.ExportColumn_Province);
                excelColumns.Add(ErrorStrings.ExportColumn_CustomerCode);
                excelColumns.Add(ErrorStrings.ExportColumn_CustomerName);
                excelColumns.Add(ErrorStrings.ExportColumn_CustomerType);
                excelColumns.Add(ErrorStrings.ExportColumn_SalesOrderCode);
                excelColumns.Add(ErrorStrings.ExportColumn_PartsSalesOrderProcessCode);
                excelColumns.Add(ErrorStrings.ExportColumn_OutCode);                
                excelColumns.Add(ErrorStrings.ExportColumn_SIHCode);
                excelColumns.Add(ErrorStrings.ExportColumn_PartCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SparePartName);
                excelColumns.Add(ErrorStrings.ExportColumn_OrderType);
                excelColumns.Add(ErrorStrings.ExportColumn_PartProp);
                excelColumns.Add(ErrorStrings.InOutSettleSummaryReport54);
                excelColumns.Add(ErrorStrings.ExportColumn_StockCost);
                excelColumns.Add(ErrorStrings.ExportColumn_StockCostAmount2);
                excelColumns.Add(ErrorStrings.ExportColumn_PartsSalesPrice);
                excelColumns.Add(ErrorStrings.ExportColumn_PartsSalesPriceAmount);
                excelColumns.Add(ErrorStrings.ExportColumn_DiscountedPrice);
                excelColumns.Add(ErrorStrings.ExportColumn_DiscountedRate);
                excelColumns.Add(ErrorStrings.ExportColumn_DealerPrice2);
                excelColumns.Add(ErrorStrings.ExportColumn_DealerPriceAmount);
                excelColumns.Add(ErrorStrings.ExportColumn_ShippingTime);
                excelColumns.Add(ErrorStrings.ExportColumn_QuantityToSettle);
                excelColumns.Add(ErrorStrings.ExportColumn_Remark);
                excelColumns.Add(ErrorStrings.ExportColumn_PriceProp);
                excelColumns.Add(ErrorStrings.ExportColumn_SettlementStatus);
               
                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.MarketingDepartmentName,
                            detail.CompanyCode,
                            detail.CompanyName,
                            detail.WarehouseName,
                            detail.Province, 
                            detail.SubmitCompanyCode,
                            detail.SubmitCompanyName,
                            detail.CustomerType,
                            detail.PartsSalesOrderCode,
                            detail.PartsSalesOrderProcessCode,
                            detail.Code,
                            detail.ReferenceCode,
                            detail.SparePartCode,
                            detail.SparePartName,
                            detail.PartsSalesOrderTypeName,
                            Enum.GetName(typeof(DcsABCStrategyCategory),detail.PartABC==null?0:detail.PartABC),
                            detail.OutboundAmount,
                            detail.CostPrice,
                            detail.CostPriceAmount,
                            detail.OriginalPrice,
                            detail.OriginalPriceAmount,
                            detail.ConcessionPrice,
                            detail.ConcessionRate+"%",
                            detail.SettlementPrice,
                            detail.SettlementPriceAmount,
                            detail.ShippingDate,
                            detail.QuantityToSettle,
                            detail.Remark,
                            detail.GroupName,
                            Enum.GetName(typeof(DcsPartsSettlementStatus),detail.SettlementStatus==null?0:detail.SettlementStatus),
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
        public string 导出采购退货统计(DateTime? bOutBoundTime, DateTime? eOutBoundTime, string partsPurReturnOrderCode, string settlementStatus, string settlementNo, string sparePartCode) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("采购退货统计_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = 采购退货统计(bOutBoundTime, eOutBoundTime, partsPurReturnOrderCode, settlementStatus, settlementNo, sparePartCode);
            var dataArray = data.ToArray();
            if(data.Any()) {
                excelColumns.Add(ErrorStrings.ExportColumn_SupplierCode2);
                excelColumns.Add(ErrorStrings.ExportColumn_SupplierName);
                excelColumns.Add(ErrorStrings.ExportColumn_PartsPurReturnOrderCode);
                excelColumns.Add(ErrorStrings.ExportColumn_OutboundTime);
                excelColumns.Add(ErrorStrings.ExportColumn_OutCode);
                excelColumns.Add(ErrorStrings.ExportColumn_WarehouseCode);
                excelColumns.Add(ErrorStrings.ExportColumn_WarehouseName);
                excelColumns.Add(ErrorStrings.ExportColumn_PartCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SparePartName);
                excelColumns.Add(ErrorStrings.InOutSettleSummaryReport54);
                excelColumns.Add(ErrorStrings.ExportColumn_SparePartABC);
                excelColumns.Add(ErrorStrings.ExportColumn_CostPrice);
                excelColumns.Add(ErrorStrings.ExportColumn_CostPriceAll);
                excelColumns.Add(ErrorStrings.ExportColumn_OutPrice);
                excelColumns.Add(ErrorStrings.ExportColumn_OutAmount);
                excelColumns.Add(ErrorStrings.ExportColumn_OutSettleStatus);
                excelColumns.Add(ErrorStrings.ExportColumn_SettleBillCode);
                excelColumns.Add(ErrorStrings.ExportColumn_VoucherNum);                

                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.PartsSupplierCode,
                            detail.PartsSupplierName,
                            detail.PartsPurReturnOrderCode,
                            detail.OutBoundTime,
                            detail.Code, 
                            detail.WarehouseCode,
                            detail.WarehouseName,
                            detail.SparePartCode,
                            detail.SparePartName,
                            detail.OutboundAmount,
                            Enum.GetName(typeof(DcsABCStrategyCategory),detail.PartABC==null?0:detail.PartABC),
                            detail.CostPrice,
                            detail.CostPriceAmount,
                            detail.SettlementPrice,
                            detail.SettlementPriceAmount,
                            Enum.GetName(typeof(DcsPartsPurchaseSettleStatus),detail.SettlementStatus==null?0:detail.SettlementStatus),
                            detail.SettlementNo,
                            detail.VoucherNo
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
    }
}
