﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web
{
    partial class DcsDomainService
    {
        public string ExportPartsSalesOrderDetailForApprove(List<PartsSalesOrderDetail> partsSalesOrderDetails,List<int> stockQuantitys,List<string> supplierWarehouseNames,List<string> minSaleQuantitys, string type, string filename) { 
            var excelColumns = new List<string>();
            string exportDataFileName = GetExportFilePath(string.Format("{0}_{1}.xlsx", filename, DateTime.Now.ToString("ddHHmmssffff")));
            if (partsSalesOrderDetails.Any() && "本部处理".Equals(type)) {
                excelColumns.Add(ErrorStrings.ExportColumn_SparePartCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SparePartName);
                excelColumns.Add(ErrorStrings.ExportColumn_OrderQuantity);
                excelColumns.Add(ErrorStrings.ExportColumn_UntFulfilledQty2);
                excelColumns.Add(ErrorStrings.ExportColumn_UsableStock);
                excelColumns.Add(ErrorStrings.ExportColumn_SalesPrice);
                excelColumns.Add(ErrorStrings.ExportColumn_CargoWarehouse);
                excelColumns.Add(ErrorStrings.ExportColumn_ButtomStock);
                excelColumns.Add(ErrorStrings.ExportColumn_IfCanNewPart);
                using (var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if (index == partsSalesOrderDetails.Count() + 1)
                            return null;
                        if (index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = partsSalesOrderDetails[index - 1];
                        var values = new object[] {
                             detail.SparePartCode,
                             detail.SparePartName,
                             detail.OrderedQuantity,
                             detail.UnfulfilledQuantity,
                             stockQuantitys[index - 1],
                             detail.OrderPrice,
                             supplierWarehouseNames[index - 1],
                             minSaleQuantitys[index - 1],
                             detail.IfCanNewPart == true ? EntityStrings.Export_Title_Yes:EntityStrings.Export_Title_No
                        };
                        return values;
                    });
                }
            } else { 
                excelColumns.Add(ErrorStrings.ExportColumn_SparePartCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SparePartName);
                excelColumns.Add(ErrorStrings.ExportColumn_OrderQuantity);
                excelColumns.Add(ErrorStrings.ExportColumn_UntFulfilledQty2);
                excelColumns.Add(ErrorStrings.ExportColumn_UsableStock);
                excelColumns.Add(ErrorStrings.ExportColumn_SalesPrice);
                excelColumns.Add(ErrorStrings.ExportColumn_CargoWarehouse);
                excelColumns.Add(ErrorStrings.ExportColumn_IfCanNewPart);
                using (var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if (index == partsSalesOrderDetails.Count() + 1)
                            return null;
                        if (index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = partsSalesOrderDetails[index - 1];
                        var values = new object[] {
                             detail.SparePartCode,
                             detail.SparePartName,
                             detail.OrderedQuantity,
                             detail.UnfulfilledQuantity,
                             stockQuantitys[index - 1],
                             detail.OrderPrice,
                             supplierWarehouseNames[index - 1],
                             detail.IfCanNewPart == true ? EntityStrings.Export_Title_Yes:EntityStrings.Export_Title_No
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
        public string ExportPartsSalesOrderForReport(int? id, int submitCompanyId, string code, string salesUnitOwnerCompanyName, int? partsSalesOrderTypeId, int? salesCategoryId, int? status, int? warehouseId, bool? isDebt, bool? ifDirectProvision, DateTime? createTimeStart, DateTime? createTimeEnd,string sparePartCode,string sparePartName)
        {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("配件销售订单提报合并导出_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = GetPartsSalesOrders().Where(r => r.SubmitCompanyId == submitCompanyId && (!r.IsExport.HasValue || r.IsExport != 1));

            if (id.HasValue)
            {
                data = data.Where(r => r.Id == id);
            }
            else
            {
                if (!string.IsNullOrEmpty(code))
                {
                    data = data.Where(r => r.Code.Contains(code));
                }
                if (!string.IsNullOrEmpty(salesUnitOwnerCompanyName))
                {
                    data = data.Where(r => r.SalesUnitOwnerCompanyName.Contains(salesUnitOwnerCompanyName));
                }
                if (partsSalesOrderTypeId.HasValue)
                {
                    data = data.Where(r => r.PartsSalesOrderTypeId == partsSalesOrderTypeId);
                }
                if (salesCategoryId.HasValue)
                {
                    data = data.Where(r => r.SalesCategoryId == salesCategoryId);
                }
                if (status.HasValue)
                {
                    data = data.Where(r => r.Status == status);
                }
                if (warehouseId.HasValue)
                {
                    data = data.Where(r => r.WarehouseId == warehouseId);
                }
                if (isDebt.HasValue)
                {
                    data = data.Where(r => r.IsDebt == isDebt);
                }
                if (ifDirectProvision.HasValue)
                {
                    data = data.Where(r => r.IfDirectProvision == ifDirectProvision);
                }
                if (createTimeStart.HasValue)
                {
                    data = data.Where(r => r.CreateTime >= createTimeStart);
                }
                if (createTimeEnd.HasValue)
                {
                    data = data.Where(r => r.CreateTime <= createTimeEnd);
                }
            }

            var partsSalesOrders = data;
            var dataToExport = from a in partsSalesOrders
                               from b in a.PartsSalesOrderDetails.DefaultIfEmpty()
                               join c in ObjectContext.Warehouses on a.WarehouseId equals c.Id into t1
                               from d in t1.DefaultIfEmpty()
                               join e in ObjectContext.Warehouses on a.ReceivingWarehouseId equals e.Id into t2
                               from f in t2.DefaultIfEmpty()
                               select new
                               {
                                   a.Code,
                                   a.SalesCategoryName,
                                   a.SalesUnitOwnerCompanyName,
                                   WarehouseName = d.Name,
                                   a.SubmitCompanyCode,
                                   a.SubmitCompanyName,
                                   a.CustomerType,
                                   a.PartsSalesOrderTypeName,
                                   a.IfDirectProvision,
                                   a.Status,
                                   a.IsDebt,
                                   a.TotalAmount,
                                   a.Remark,
                                   a.ReceivingAddress,
                                   a.ShippingMethod,
                                   a.RequestedDeliveryTime,
                                   a.StopComment,
                                   a.CreatorName,
                                   a.CreateTime,
                                   a.SubmitterName,
                                   a.SubmitTime,
                                   a.ApproverName,
                                   a.ApproveTime,
                                   a.AbandonerName,
                                   a.AbandonTime,
                                   b.SparePartCode,
                                   b.SparePartName,
                                   b.OrderedQuantity,
                                   b.ApproveQuantity,
                                   b.OriginalPrice,
                                   b.OrderPrice,
                                   b.DiscountedPrice,
                                   b.OrderSum,
                                   DetailRemark = b.Remark,
                                   ReceivingWarehouseCode = f.Code,
                                   ReceivingWarehouseName = f.Name,
                                   b.PriceTypeName,
                                   b.ABCStrategy
                               };
            if (!string.IsNullOrEmpty(sparePartCode)) {
                dataToExport = dataToExport.Where(r => r.SparePartCode.Contains(sparePartCode));
            }
            if (!string.IsNullOrEmpty(sparePartName)) {
                dataToExport = dataToExport.Where(r => r.SparePartName.Contains(sparePartName));
            }

            var dataArray = dataToExport.ToArray();
            if (partsSalesOrders.Any())
            {
                excelColumns.Add(ErrorStrings.ExportColumn_PartSalesOrder_Code);
                excelColumns.Add(ErrorStrings.ExportColumn_PartsSalesCategory);
                excelColumns.Add(ErrorStrings.ExportColumn_SupplyUnit);
                excelColumns.Add(ErrorStrings.ExportColumn_OrderWarehouse);
                excelColumns.Add(ErrorStrings.ExportColumn_CustomerCode);
                excelColumns.Add(ErrorStrings.ExportColumn_CustomerName);
                excelColumns.Add(ErrorStrings.ExportColumn_CustomerProp);
                excelColumns.Add(ErrorStrings.ExportColumn_OrderType3);
                excelColumns.Add(ErrorStrings.ExportColumn_IfDirectProvision);
                excelColumns.Add(ErrorStrings.ExportColumn_Status);
                excelColumns.Add(ErrorStrings.ExportColumn_IsDebt);
                excelColumns.Add(ErrorStrings.ExportColumn_OrderSum);
                excelColumns.Add(ErrorStrings.ExportColumn_Remark);
                excelColumns.Add(ErrorStrings.ExportColumn_ReceiveAdress);
                excelColumns.Add(ErrorStrings.ExportColumn_ShippingMethod);
                excelColumns.Add(ErrorStrings.ExportColumn_RequestedDeliveryTime);
                excelColumns.Add(ErrorStrings.ExportColumn_StopReason);
                excelColumns.Add(ErrorStrings.ExportColumn_Creator);
                excelColumns.Add(ErrorStrings.ExportColumn_CreateTime);
                excelColumns.Add(ErrorStrings.ExportColumn_Submiter);
                excelColumns.Add(ErrorStrings.ExportColumn_SubmitTime);
                excelColumns.Add(ErrorStrings.ExportColumn_Approver);
                excelColumns.Add(ErrorStrings.ExportColumn_ApproveTime);
                excelColumns.Add(ErrorStrings.ExportColumn_Abandoner);
                excelColumns.Add(ErrorStrings.ExportColumn_AbandonTime);
                excelColumns.Add(ErrorStrings.ExportColumn_PartCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SparePartName);
                excelColumns.Add(ErrorStrings.ExportColumn_OrderQuantity);
                excelColumns.Add(ErrorStrings.ExportColumn_FullQuantity);
                excelColumns.Add(ErrorStrings.ExportColumn_PartsSalesPrice);
                excelColumns.Add(ErrorStrings.ExportColumn_OrderPrice);
                excelColumns.Add(ErrorStrings.ExportColumn_PriceDiscount);
                excelColumns.Add(ErrorStrings.ExportColumn_OrderAmount);
                excelColumns.Add(ErrorStrings.ExportColumn_DetailRemark);
                excelColumns.Add(ErrorStrings.ExportColumn_ReceiveWarehouseCode);
                excelColumns.Add(ErrorStrings.ExportColumn_ReceiveWarehouseName);
                excelColumns.Add(ErrorStrings.ExportColumn_PriceType);
                excelColumns.Add(ErrorStrings.ExportColumn_SparePartABC);                
                using (var excelExport = new ExcelExport(exportDataFileName))
                {
                    excelExport.ExportByRow(index =>
                    {
                        if (index == dataArray.Count() + 1)
                            return null;
                        if (index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.Code, detail.SalesCategoryName, detail.SalesUnitOwnerCompanyName, detail.WarehouseName, detail.SubmitCompanyCode, detail.SubmitCompanyName, 
                            Enum.GetName(typeof(DcsCompanyType), detail.CustomerType), detail.PartsSalesOrderTypeName, detail.IfDirectProvision ? EntityStrings.Export_Title_Yes : EntityStrings.Export_Title_No, 
                            Enum.GetName(typeof(DcsPartsSalesOrderStatus), detail.Status), detail.IsDebt ? EntityStrings.Export_Title_Yes : EntityStrings.Export_Title_No, detail.TotalAmount, detail.Remark, 
                            detail.ReceivingAddress, GetKeyValueItemByKey("PartsShipping_Method", detail.ShippingMethod).Value, detail.RequestedDeliveryTime,
                            detail.StopComment, detail.CreatorName, detail.CreateTime, detail.SubmitterName, detail.SubmitTime, detail.ApproverName, detail.ApproveTime,
                            detail.AbandonerName, detail.AbandonTime, detail.SparePartCode, detail.SparePartName, detail.OrderedQuantity, detail.ApproveQuantity,detail.OriginalPrice,
                            detail.OrderPrice,detail.DiscountedPrice, detail.OrderSum, detail.DetailRemark ,detail. ReceivingWarehouseCode,detail.ReceivingWarehouseName,detail.PriceTypeName,detail.ABCStrategy
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }

        public string ExportPartsSalesOrderWithDetailsByPersonnel(int[] ids, string eRPSourceOrderCode, int personnelId, int? provinceId, int? cityId, string sparePartCode, string code, string submitCompanyCode, string submitCompanyName, int? partsSalesOrderTypeId, int? salesCategoryId, List<int> statusList, int? warehouseId, bool? isDebt, bool? ifDirectProvision, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? submitTimeBegin, DateTime? submitTimeEnd, DateTime? abandonTimeBegin, DateTime? abandonTimeEnd, DateTime? approveTimeBegin, DateTime? approveTimeEnd, bool? isAutoApprove, bool? ifInnerDirectProvision, string SAPPurchasePlanCode, /*bool? isVehicleOutage,*/ string cpPartsPurchaseOrderCode, string cpPartsInboundCheckCode,int? autoApproveStatus)
        {
            var exportDataFileName = GetExportFilePath(string.Format("配件销售订单主清单_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var tempQuery = this.根据审批人员查询销售订单省市(personnelId, provinceId, cityId, sparePartCode).Where(r => !r.IsExport.HasValue || r.IsExport != 1);
            try
            {
                var query0 = from a in tempQuery
                             join b in ObjectContext.PartsSalesOrderDetails on a.Id equals b.PartsSalesOrderId
                             join d in ObjectContext.PartsBranches.Where(r => r.Status == (int)DcsBaseDataStatus.有效)
                             on new
                             {
                                 SalesCategoryId = a.SalesCategoryId,
                                 PartId = b.SparePartId
                             } equals new
                             {
                                 SalesCategoryId = d.PartsSalesCategoryId,
                                 PartId = d.PartId
                             } into tempdata
                             from t in tempdata.DefaultIfEmpty()
                             select new
                             {
                                 a.Code,
                                 a.Time,
                                 a.SalesCategoryName,
                                 a.SalesUnitOwnerCompanyName,
                                 // c.Name,
                                 a.SubmitCompanyCode,
                                 a.SubmitCompanyName,
                                 a.Province,
                                 a.City,
                                 a.CustomerType, //字典项
                                 a.PartsSalesOrderTypeName,
                                 a.IfDirectProvision,
                                 a.IfAgencyService,
                                 a.Status, //字典项
                                 a.IsDebt,
                                 a.TotalAmount,
                                 a.EcommerceMoney,
                                 Remark1 = a.Remark,
                                 a.ReceivingAddress,
                                 a.ContactPerson,
                                 a.ContactPhone,
                                 a.ShippingMethod, //字典项
                                 a.RequestedDeliveryTime,
                                 a.StopComment,
                                 a.CreatorName,
                                 a.CreateTime,
                                 a.SubmitterName,
                                 a.SubmitTime,
                                 a.ApproverName,
                                 a.ApproveTime,
                                 a.FirstApproveTime,
                                 a.AbandonerName,
                                 a.AbandonTime,
                                 a.IsTransSuccess,
                                 a.Remark,
                                 t.PartsAttribution,
                                 b.SparePartCode,
                                 b.SparePartName,
                                 b.OrderedQuantity,
                                 b.ApproveQuantity,
                                 b.OrderPrice,
                                 b.OriginalPrice,
                                 b.DiscountedPrice,
                                 b.OrderSum,
                                 DetailRemark = b.Remark,
                                 a.Id,
                                 a.PartsSalesOrderTypeId,
                                 a.SalesCategoryId,
                                 a.WarehouseId,
                                 a.ERPOrderCode,
                                 a.ERPSourceOrderCode,
                                 a.IsBarter,
                                 a.IsAutoApprove,
                                 a.AutoApproveStatus,
                                 a.AutoApproveComment,
                                 a.AutoApproveTime,
                                 a.IfInnerDirectProvision,
                                 a.OverseasDemandSheetNo,
                                 a.IsVehicleOutage,
                                 a.CPPartsPurchaseOrderCode,
                                 a.CPPartsInboundCheckCode,
                                 b.PriceTypeName,
                                 b.ABCStrategy,
                                 a.SourceBillCode,
                                 b.SalesPrice
                             };
                var query = from a in query0
                            join c in ObjectContext.Warehouses on a.WarehouseId equals c.Id into tempdata1
                            from t in tempdata1.DefaultIfEmpty()
                            select new
                            {
                                a.Code,
                                a.Time,
                                a.SalesCategoryName,
                                a.SalesUnitOwnerCompanyName,
                                t.Name,
                                a.SubmitCompanyCode,
                                a.SubmitCompanyName,
                                a.Province,
                                a.City,
                                a.CustomerType, //字典项
                                a.PartsSalesOrderTypeName,
                                a.IfDirectProvision,
                                a.IfAgencyService,
                                a.Status, //字典项
                                a.IsDebt,
                                a.TotalAmount,
                                a.EcommerceMoney,
                                a.Remark1,
                                a.ReceivingAddress,
                                a.ContactPerson,
                                a.ContactPhone,
                                a.ShippingMethod, //字典项
                                a.RequestedDeliveryTime,
                                a.StopComment,
                                a.CreatorName,
                                a.CreateTime,
                                a.SubmitterName,
                                a.SubmitTime,
                                a.ApproverName,
                                a.ApproveTime,
                                a.FirstApproveTime,
                                a.AbandonerName,
                                a.AbandonTime,
                                a.IsTransSuccess,
                                a.Remark,
                                a.PartsAttribution,
                                a.SparePartCode,
                                a.SparePartName,
                                a.OrderedQuantity,
                                a.ApproveQuantity,
                                a.OrderPrice,
                                a.OriginalPrice,
                                a.DiscountedPrice,
                                a.OrderSum,
                                a.DetailRemark,
                                a.Id,
                                a.PartsSalesOrderTypeId,
                                a.SalesCategoryId,
                                a.WarehouseId,
                                a.ERPOrderCode,
                                a.ERPSourceOrderCode,
                                a.IsBarter,
                                a.IsAutoApprove,
                                a.AutoApproveStatus,
                                a.AutoApproveComment,
                                a.AutoApproveTime,
                                a.IfInnerDirectProvision,
                                a.OverseasDemandSheetNo,
                                a.IsVehicleOutage,
                                a.CPPartsPurchaseOrderCode,
                                a.CPPartsInboundCheckCode,
                                a.PriceTypeName,
                                a.ABCStrategy,
                                a.SourceBillCode,
                                a.SalesPrice
                            };
                if (ids != null && ids.Length > 0)
                {
                    query = query.Where(r => ids.Contains(r.Id));
                }
                else
                {
                    if (!string.IsNullOrEmpty(code))
                    {
                        query = query.Where(r => r.Code.Contains(code));
                    }
                    if (!string.IsNullOrEmpty(submitCompanyCode))
                    {
                        query = query.Where(r => r.SubmitCompanyCode.Contains(submitCompanyCode));
                    }
                    if (!string.IsNullOrEmpty(submitCompanyName))
                    {
                        query = query.Where(r => r.SubmitCompanyName.Contains(submitCompanyName));
                    }
                    if (partsSalesOrderTypeId.HasValue)
                    {
                        query = query.Where(r => r.PartsSalesOrderTypeId == partsSalesOrderTypeId);
                    }
                    if (!string.IsNullOrEmpty(eRPSourceOrderCode))
                    {
                        query = query.Where(r => r.ERPSourceOrderCode.Contains(eRPSourceOrderCode));
                    }
                    if (salesCategoryId.HasValue)
                    {
                        query = query.Where(r => r.SalesCategoryId == salesCategoryId);
                    }
                    if (statusList != null && statusList.Any())
                    {
                        query = query.Where(r => statusList.Contains(r.Status));
                    }
                    if (warehouseId.HasValue)
                    {
                        query = query.Where(r => r.WarehouseId == warehouseId);
                    }
                    if (isDebt.HasValue)
                    {
                        query = query.Where(r => r.IsDebt == isDebt);
                    }
                    //if(isVehicleOutage.HasValue) {
                    //    query = query.Where(r => r.IsVehicleOutage == isVehicleOutage);
                    //}
                    if (ifDirectProvision.HasValue)
                    {
                        query = query.Where(r => r.IfDirectProvision == ifDirectProvision);
                    }
                    if (createTimeBegin.HasValue)
                    {
                        query = query.Where(r => r.CreateTime >= createTimeBegin);
                    }
                    if (createTimeEnd.HasValue)
                    {
                        query = query.Where(r => r.CreateTime <= createTimeEnd);
                    }
                    if (submitTimeBegin.HasValue)
                    {
                        query = query.Where(r => r.SubmitTime >= submitTimeBegin);
                    }
                    if (submitTimeEnd.HasValue)
                    {
                        query = query.Where(r => r.SubmitTime <= submitTimeEnd);
                    }

                    if (abandonTimeBegin.HasValue)
                    {
                        query = query.Where(r => r.AbandonTime >= abandonTimeBegin);
                    }
                    if (abandonTimeEnd.HasValue)
                    {
                        query = query.Where(r => r.AbandonTime <= abandonTimeEnd);
                    }

                    if (approveTimeBegin.HasValue)
                    {
                        query = query.Where(r => r.ApproveTime >= approveTimeBegin);
                    }
                    if (approveTimeEnd.HasValue)
                    {
                        query = query.Where(r => r.ApproveTime <= approveTimeEnd);
                    }
                    if (isAutoApprove.HasValue)
                    {
                        query = query.Where(r => r.IsAutoApprove == isAutoApprove);
                    }
                    if (ifInnerDirectProvision.HasValue)
                    {
                        query = query.Where(r => r.IfInnerDirectProvision == ifInnerDirectProvision);
                    }
                    if (!string.IsNullOrEmpty(SAPPurchasePlanCode))
                    {
                        query = query.Where(r => r.OverseasDemandSheetNo.Contains(SAPPurchasePlanCode));
                    }
                    if (!string.IsNullOrEmpty(cpPartsPurchaseOrderCode))
                    {
                        query = query.Where(r => r.CPPartsPurchaseOrderCode.Contains(cpPartsPurchaseOrderCode));
                    }
                    if (!string.IsNullOrEmpty(cpPartsInboundCheckCode))
                    {
                        query = query.Where(r => r.CPPartsInboundCheckCode.Contains(cpPartsInboundCheckCode));
                    }
                    if (!string.IsNullOrEmpty(sparePartCode))
                    {
                        query = query.Where(r => r.SparePartCode.Contains(sparePartCode));
                    }
                    if (autoApproveStatus.HasValue) {
                        query = query.Where(r => r.AutoApproveStatus == autoApproveStatus);
                    }

                }
                var result = query.ToArray();
                if (result.Any())
                {
                    var excelColumns = new List<string>();
                    excelColumns.AddRange(new[] {
						ErrorStrings.ExportColumn_PartSalesOrder_Code,
						ErrorStrings.ExportColumn_Times,
						ErrorStrings.ExportColumn_PartsSalesCategory,
						ErrorStrings.ExportColumn_SupplyUnit, 
						ErrorStrings.ExportColumn_OrderWarehouse,
						ErrorStrings.ExportColumn_CustomerCode, 
						ErrorStrings.ExportColumn_CustomerName, 
						ErrorStrings.ExportColumn_ProvinceName,
						ErrorStrings.ExportColumn_RegionName, 
						ErrorStrings.ExportColumn_CustomerProp,
						ErrorStrings.ExportColumn_OrderType3, 
						ErrorStrings.ExportColumn_IfDirectProvision, 
						ErrorStrings.ExportColumn_Status,
						ErrorStrings.ExportColumn_OrderSum,
						ErrorStrings.ExportColumn_Remark,
						ErrorStrings.ExportColumn_ReceiveAdress,
						ErrorStrings.ExportColumn_ContactPerson,
						ErrorStrings.ExportColumn_ContactPhone,
						ErrorStrings.ExportColumn_ShippingMethod,
						ErrorStrings.ExportColumn_RequestedDeliveryTime,
						ErrorStrings.ExportColumn_StopReason,
						ErrorStrings.ExportColumn_Creator,
						ErrorStrings.ExportColumn_CreateTime,
						ErrorStrings.ExportColumn_Submiter,
						ErrorStrings.ExportColumn_SubmitTime,
						ErrorStrings.ExportColumn_OriginalOrderCode,
						ErrorStrings.ExportColumn_IfAgencyService, 
						ErrorStrings.ExportColumn_Approver, 
						ErrorStrings.ExportColumn_ApproveTime,
						ErrorStrings.ExportColumn_FirstApproveTime, 
						ErrorStrings.ExportColumn_Abandoner, 
						ErrorStrings.ExportColumn_AbandonTime,
						ErrorStrings.ExportColumn_IsAutoApprove,
						ErrorStrings.ExportColumn_AutoApproveStatus,
						ErrorStrings.ExportColumn_AutoApproveComment,
						ErrorStrings.ExportColumn_AutoApproveTime, 
						ErrorStrings.ExportColumn_PartsAttribution,
						ErrorStrings.ExportColumn_SparePartCode, 
						ErrorStrings.ExportColumn_SparePartName, 
						ErrorStrings.ExportColumn_OrderQuantity,
						ErrorStrings.ExportColumn_FullQuantity,
						ErrorStrings.ExportColumn_PartsSalesPrice, 
						ErrorStrings.ExportColumn_OrderPrice,
						ErrorStrings.ExportColumn_PriceDiscount,
						ErrorStrings.ExportColumn_OrderAmount,
						ErrorStrings.ExportColumn_DetailRemark,
						ErrorStrings.ExportColumn_PriceType,
						ErrorStrings.ExportColumn_SparePartABC,
                        ErrorStrings.ExportColumn_DealerPrice
                });
                    using (var excelExport = new ExcelExport(exportDataFileName))
                    {
                        excelExport.ExportByRow(index =>
                        {
                            if (index == result.Count() + 1)
                                return null;
                            if (index == 0)
                                return excelColumns.ToArray<Object>();
                            var detail = result[index - 1];
                            var values = new object[] {
                            detail.Code,detail.Time, detail.SalesCategoryName, detail.SalesUnitOwnerCompanyName, detail.Name, detail.SubmitCompanyCode, detail.SubmitCompanyName,
                            detail.Province, detail.City, Enum.GetName(typeof(DcsCompanyType), detail.CustomerType), detail.PartsSalesOrderTypeName,(detail.IfDirectProvision?EntityStrings.Export_Title_Yes:EntityStrings.Export_Title_No), 
                            Enum.GetName(typeof(DcsPartsSalesOrderStatus), detail.Status),detail.TotalAmount, detail.Remark, detail.ReceivingAddress, detail.ContactPerson,detail.ContactPhone,
                            Enum.GetName(typeof(DcsPartsShippingMethod),detail.ShippingMethod),detail.RequestedDeliveryTime, detail.StopComment, 
                            detail.CreatorName, detail.CreateTime, detail.SubmitterName, detail.SubmitTime,detail.SourceBillCode,(detail.IfAgencyService?EntityStrings.Export_Title_Yes:EntityStrings.Export_Title_No), detail.ApproverName, detail.ApproveTime,detail.FirstApproveTime, detail.AbandonerName, 
                            detail.AbandonTime,Enum.GetName(typeof(DcsIsOrNot),(detail.IsAutoApprove.HasValue?Convert.ToInt32(detail.IsAutoApprove.Value):0)),(detail.AutoApproveStatus.HasValue?Enum.GetName(typeof(DcsAutoApproveStatus),detail.AutoApproveStatus):""), detail.AutoApproveComment,detail.AutoApproveTime,
                            (detail.PartsAttribution.HasValue?Enum.GetName(typeof(DcsPartsAttribution),detail.PartsAttribution):""),detail.SparePartCode, detail.SparePartName, 
                            detail.OrderedQuantity, detail.ApproveQuantity,detail.OriginalPrice,
                            detail.OrderPrice,detail.DiscountedPrice, detail.OrderSum, detail.DetailRemark,detail.PriceTypeName,detail.ABCStrategy,detail.SalesPrice
                        };
                            return values;
                        });
                    }
                }
                return exportDataFileName;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string ExportPartsSalesOrderWithDetailsByPersonnelIsAgency(int[] ids, string eRPSourceOrderCode, int personnelId, int? provinceId, int? cityId, string code, string submitCompanyCode, string submitCompanyName, int? partsSalesOrderTypeId, int? salesCategoryId, int? status, int? warehouseId, bool? isDebt, bool? ifDirectProvision, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? submitTimeBegin, DateTime? submitTimeEnd, DateTime? abandonTimeBegin, DateTime? abandonTimeEnd, DateTime? approveTimeBegin, DateTime? approveTimeEnd, bool? isAutoApprove, bool? ifInnerDirectProvision, string SAPPurchasePlanCode)
        {
            var exportDataFileName = GetExportFilePath(string.Format("配件销售订单主清单_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var tempQuery = this.根据审批人员查询销售订单省市代理库(personnelId, provinceId, cityId).Where(r => !r.IsExport.HasValue || r.IsExport != 1);
            try
            {
                var query0 = from a in tempQuery
                             join b in ObjectContext.PartsSalesOrderDetails on a.Id equals b.PartsSalesOrderId
                             join d in ObjectContext.PartsBranches.Where(r => r.Status == (int)DcsBaseDataStatus.有效)
                             on new
                             {
                                 SalesCategoryId = a.SalesCategoryId,
                                 PartId = b.SparePartId
                             } equals new
                             {
                                 SalesCategoryId = d.PartsSalesCategoryId,
                                 PartId = d.PartId
                             } into tempdata
                             from t in tempdata.DefaultIfEmpty()
                             select new
                             {
                                 a.Code,
                                 a.SalesCategoryName,
                                 a.SalesUnitOwnerCompanyName,
                                 // c.Name,
                                 a.SubmitCompanyCode,
                                 a.SubmitCompanyName,
                                 a.Province,
                                 a.City,
                                 a.CustomerType, //字典项
                                 a.PartsSalesOrderTypeName,
                                 a.IfDirectProvision,
                                 a.Status, //字典项
                                 a.IsDebt,
                                 a.TotalAmount,
                                 Remark1 = a.Remark,
                                 a.ReceivingAddress,
                                 a.ShippingMethod, //字典项
                                 a.RequestedDeliveryTime,
                                 a.StopComment,
                                 a.CreatorName,
                                 a.CreateTime,
                                 a.SubmitterName,
                                 a.SubmitTime,
                                 a.ApproverName,
                                 a.ApproveTime,
                                 a.FirstApproveTime,
                                 a.AbandonerName,
                                 a.AbandonTime,
                                 a.IsTransSuccess,
                                 a.Remark,
                                 t.PartsAttribution,
                                 b.SparePartCode,
                                 b.SparePartName,
                                 b.OrderedQuantity,
                                 b.ApproveQuantity,
                                 b.OrderPrice,
                                 b.OrderSum,
                                 DetailRemark = b.Remark,
                                 a.Id,
                                 a.PartsSalesOrderTypeId,
                                 a.SalesCategoryId,
                                 a.WarehouseId,
                                 a.ERPOrderCode,
                                 a.ERPSourceOrderCode,
                                 a.IsBarter,
                                 a.IsAutoApprove,
                                 a.AutoApproveComment,
                                 a.AutoApproveTime,
                                 a.IfInnerDirectProvision,
                                 a.OverseasDemandSheetNo
                             };
                var query = from a in query0
                            join c in ObjectContext.Warehouses on a.WarehouseId equals c.Id into tempdata1
                            from t in tempdata1.DefaultIfEmpty()
                            select new
                            {
                                a.Code,
                                a.SalesCategoryName,
                                a.SalesUnitOwnerCompanyName,
                                t.Name,
                                a.SubmitCompanyCode,
                                a.SubmitCompanyName,
                                a.Province,
                                a.City,
                                a.CustomerType, //字典项
                                a.PartsSalesOrderTypeName,
                                a.IfDirectProvision,
                                a.Status, //字典项
                                a.IsDebt,
                                a.TotalAmount,
                                a.Remark1,
                                a.ReceivingAddress,
                                a.ShippingMethod, //字典项
                                a.RequestedDeliveryTime,
                                a.StopComment,
                                a.CreatorName,
                                a.CreateTime,
                                a.SubmitterName,
                                a.SubmitTime,
                                a.ApproverName,
                                a.ApproveTime,
                                a.FirstApproveTime,
                                a.AbandonerName,
                                a.AbandonTime,
                                a.IsTransSuccess,
                                a.Remark,
                                a.PartsAttribution,
                                a.SparePartCode,
                                a.SparePartName,
                                a.OrderedQuantity,
                                a.ApproveQuantity,
                                a.OrderPrice,
                                a.OrderSum,
                                a.DetailRemark,
                                a.Id,
                                a.PartsSalesOrderTypeId,
                                a.SalesCategoryId,
                                a.WarehouseId,
                                a.ERPOrderCode,
                                a.ERPSourceOrderCode,
                                a.IsBarter,
                                a.IsAutoApprove,
                                a.AutoApproveComment,
                                a.AutoApproveTime,
                                a.IfInnerDirectProvision,
                                a.OverseasDemandSheetNo
                            };
                if (ids != null && ids.Length > 0)
                {
                    query = query.Where(r => ids.Contains(r.Id));
                }
                else
                {
                    if (!string.IsNullOrEmpty(code))
                    {
                        query = query.Where(r => r.Code.Contains(code));
                    }
                    if (!string.IsNullOrEmpty(submitCompanyCode))
                    {
                        query = query.Where(r => r.SubmitCompanyCode.Contains(submitCompanyCode));
                    }
                    if (!string.IsNullOrEmpty(submitCompanyName))
                    {
                        query = query.Where(r => r.SubmitCompanyName.Contains(submitCompanyName));
                    }
                    if (partsSalesOrderTypeId.HasValue)
                    {
                        query = query.Where(r => r.PartsSalesOrderTypeId == partsSalesOrderTypeId);
                    }
                    if (!string.IsNullOrEmpty(eRPSourceOrderCode))
                    {
                        query = query.Where(r => r.ERPSourceOrderCode.Contains(eRPSourceOrderCode));
                    }
                    if (salesCategoryId.HasValue)
                    {
                        query = query.Where(r => r.SalesCategoryId == salesCategoryId);
                    }
                    if (status.HasValue)
                    {
                        query = query.Where(r => r.Status == status);
                    }
                    if (warehouseId.HasValue)
                    {
                        query = query.Where(r => r.WarehouseId == warehouseId);
                    }
                    if (isDebt.HasValue)
                    {
                        query = query.Where(r => r.IsDebt == isDebt);
                    }
                    if (ifDirectProvision.HasValue)
                    {
                        query = query.Where(r => r.IfDirectProvision == ifDirectProvision);
                    }
                    if (createTimeBegin.HasValue)
                    {
                        query = query.Where(r => r.CreateTime >= createTimeBegin);
                    }
                    if (createTimeEnd.HasValue)
                    {
                        query = query.Where(r => r.CreateTime <= createTimeEnd);
                    }
                    if (submitTimeBegin.HasValue)
                    {
                        query = query.Where(r => r.SubmitTime >= submitTimeBegin);
                    }
                    if (submitTimeEnd.HasValue)
                    {
                        query = query.Where(r => r.SubmitTime <= submitTimeEnd);
                    }

                    if (abandonTimeBegin.HasValue)
                    {
                        query = query.Where(r => r.AbandonTime >= abandonTimeBegin);
                    }
                    if (abandonTimeEnd.HasValue)
                    {
                        query = query.Where(r => r.AbandonTime <= abandonTimeEnd);
                    }

                    if (approveTimeBegin.HasValue)
                    {
                        query = query.Where(r => r.ApproveTime >= approveTimeBegin);
                    }
                    if (approveTimeEnd.HasValue)
                    {
                        query = query.Where(r => r.ApproveTime <= approveTimeEnd);
                    }
                    if (isAutoApprove.HasValue)
                    {
                        query = query.Where(r => r.IsAutoApprove == isAutoApprove);
                    }
                    if (ifInnerDirectProvision.HasValue)
                    {
                        query = query.Where(r => r.IsAutoApprove == ifInnerDirectProvision);
                    }
                    if (!string.IsNullOrEmpty(SAPPurchasePlanCode))
                    {
                        query = query.Where(r => r.OverseasDemandSheetNo.Contains(SAPPurchasePlanCode));
                    }

                }
                var result = query.ToArray();


                if (result.Any())
                {
                    var excelColumns = new List<string>();
                    excelColumns.AddRange(new[] {
					ErrorStrings.ExportColumn_PartSalesOrder_Code,
					ErrorStrings.ExportColumn_PartsSalesCategory,
					ErrorStrings.ExportColumn_SupplyUnit,
					ErrorStrings.ExportColumn_OrderWarehouse,
					ErrorStrings.ExportColumn_CustomerCode,
					ErrorStrings.ExportColumn_CustomerName,
					ErrorStrings.ExportColumn_ProvinceName, 
					ErrorStrings.ExportColumn_RegionName,
					ErrorStrings.ExportColumn_CustomerProp, 
					ErrorStrings.ExportColumn_OrderType3, 
					ErrorStrings.ExportColumn_IfDirectProvision,
					ErrorStrings.ExportColumn_Status,
					ErrorStrings.ExportColumn_IsDebt,
					ErrorStrings.ExportColumn_OrderSum, 
					ErrorStrings.ExportColumn_Remark,
					ErrorStrings.ExportColumn_ReceiveAdress,
					ErrorStrings.ExportColumn_ShippingMethod,
					ErrorStrings.ExportColumn_RequestedDeliveryTime, 
					ErrorStrings.ExportColumn_StopReason,
					ErrorStrings.ExportColumn_Creator,
					ErrorStrings.ExportColumn_CreateTime,
					ErrorStrings.ExportColumn_Submiter, 
					ErrorStrings.ExportColumn_SubmitTime,
					ErrorStrings.ExportColumn_Approver,
					ErrorStrings.ExportColumn_ApproveTime,
					ErrorStrings.ExportColumn_FirstApproveTime,
					ErrorStrings.ExportColumn_Abandoner,
					ErrorStrings.ExportColumn_AbandonTime,
					ErrorStrings.ExportColumn_IsTransSuccess,
					ErrorStrings.ExportColumn_Remark, 
					ErrorStrings.ExportColumn_PartsAttribution,
					ErrorStrings.ExportColumn_SparePartCode,
					ErrorStrings.ExportColumn_SparePartName, 
					ErrorStrings.ExportColumn_OrderQuantity, 
					ErrorStrings.ExportColumn_FullQuantity,
					ErrorStrings.ExportColumn_Price,
					ErrorStrings.ExportColumn_OrderAmount,
					ErrorStrings.ExportColumn_DetailRemark,
					ErrorStrings.ExportColumn_ErpCode,
					ErrorStrings.ExportColumn_IsExchange,
					ErrorStrings.ExportColumn_IsAutoApproveOrder,
					ErrorStrings.ExportColumn_AutoApproveTime,
					ErrorStrings.ExportColumn_AutoApproveComment,
					ErrorStrings.ExportColumn_IfInnerDirectProvision,
					ErrorStrings.ExportColumn_SAPPurchasePlanCode
                });
                    using (var excelExport = new ExcelExport(exportDataFileName))
                    {
                        excelExport.ExportByRow(index =>
                        {
                            if (index == result.Count() + 1)
                                return null;
                            if (index == 0)
                                return excelColumns.ToArray<Object>();
                            var detail = result[index - 1];
                            var values = new object[] {
                            detail.Code, detail.SalesCategoryName, detail.SalesUnitOwnerCompanyName, detail.Name, detail.SubmitCompanyCode, detail.SubmitCompanyName,
                            detail.Province, detail.City, Enum.GetName(typeof(DcsCompanyType), detail.CustomerType), detail.PartsSalesOrderTypeName, (detail.IfDirectProvision?EntityStrings.Export_Title_Yes:EntityStrings.Export_Title_No), 
                            Enum.GetName(typeof(DcsPartsSalesOrderStatus), detail.Status),
                            (detail.IsDebt?EntityStrings.Export_Title_Yes:EntityStrings.Export_Title_No), 
                            detail.TotalAmount, detail.Remark1, detail.ReceivingAddress, 
                            Enum.GetName(typeof(DcsPartsShippingMethod),detail.ShippingMethod),
                            detail.RequestedDeliveryTime, detail.StopComment, 
                            detail.CreatorName, detail.CreateTime, detail.SubmitterName, detail.SubmitTime, detail.ApproverName, detail.ApproveTime,detail.FirstApproveTime, detail.AbandonerName, 
                            detail.AbandonTime,(detail.IsTransSuccess.HasValue?Enum.GetName(typeof(DcsIsOrNot),detail.IsTransSuccess.Value):EntityStrings.Export_Title_No),detail.Remark, (detail.PartsAttribution.HasValue?Enum.GetName(typeof(DcsPartsAttribution),detail.PartsAttribution):""),detail.SparePartCode, detail.SparePartName, detail.OrderedQuantity, detail.ApproveQuantity, detail.OrderPrice,
                            detail.OrderSum, detail.DetailRemark,detail.ERPSourceOrderCode,Enum.GetName(typeof(DcsIsOrNot),detail.IsBarter),
                            Enum.GetName(typeof(DcsIsOrNot),(detail.IsAutoApprove.HasValue?Convert.ToInt32(detail.IsAutoApprove.Value):0)), detail.AutoApproveComment,detail.AutoApproveTime,(detail.IfInnerDirectProvision==null?EntityStrings.Export_Title_No:((bool)detail.IfInnerDirectProvision?EntityStrings.Export_Title_Yes:EntityStrings.Export_Title_No)),detail.OverseasDemandSheetNo
                        };
                            return values;
                        });
                    }
                }
                return exportDataFileName;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string ExportPartsSalesOrderTraceQuery(int[] ids, string code, string partCode, string partName, string submitCompanyCode, string submitCompanyName, int? salesCategoryId, int? warehouseId, DateTime? submitTimeBegin, DateTime? submitTimeEnd, int? SalesUnitOwnerCompanyId, string erpSourceOrderCode)
        {
            var exportDataFileName = GetExportFilePath(string.Format("配件销售订单跟踪信息_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var query = this.根据登陆人员查询销售订单(submitTimeBegin.Value, submitTimeEnd.Value, erpSourceOrderCode, SalesUnitOwnerCompanyId);
            try
            {
                if (ids != null && ids.Length > 0)
                {
                    query = query.Where(r => ids.Contains(r.DetailId));
                }
                else
                {
                    if (!string.IsNullOrEmpty(code))
                    {
                        query = query.Where(r => r.PartsSalesOrderCode.Contains(code));
                    }
                    if (!string.IsNullOrEmpty(partCode))
                    {
                        query = query.Where(r => r.SparePartCode.Contains(partCode));
                    }
                    if (!string.IsNullOrEmpty(partName))
                    {
                        query = query.Where(r => r.SparePartName.Contains(partName));
                    }
                    if (!string.IsNullOrEmpty(submitCompanyCode))
                    {
                        query = query.Where(r => r.SubmitCompanyCode.Contains(submitCompanyCode));
                    }
                    if (!string.IsNullOrEmpty(submitCompanyName))
                    {
                        query = query.Where(r => r.SubmitCompanyName.Contains(submitCompanyName));
                    }
                    if (salesCategoryId.HasValue)
                    {
                        query = query.Where(r => r.SalesCategoryId == salesCategoryId);
                    }
                    if (warehouseId.HasValue)
                    {
                        query = query.Where(r => r.WarehouseId == warehouseId);
                    }
                    if (submitTimeBegin.HasValue)
                    {
                        query = query.Where(r => r.SubmitTime >= submitTimeBegin);
                    }
                    if (submitTimeEnd.HasValue)
                    {
                        query = query.Where(r => r.SubmitTime <= submitTimeEnd);
                    }
                    //if(SalesUnitOwnerCompanyId.HasValue) {
                    //    query = query.Where(r => r.SalesUnitOwnerCompanyId == SalesUnitOwnerCompanyId.Value);
                    //}
                }
                var result = query.ToArray();
                if (result.Any())
                {
                    var excelColumns = new List<string>();
                    excelColumns.AddRange(new[] {
                    ErrorStrings.ExportColumn_PartSalesOrder_Code,
					ErrorStrings.ExportColumn_PartsSalesCategory,
					"订货仓库编号",
					"订货仓库名称",
					ErrorStrings.ExportColumn_CustomerCode,
					ErrorStrings.ExportColumn_CustomerName, 
					ErrorStrings.ExportColumn_CustomerProp,
					"销售订单类型",
					ErrorStrings.ExportColumn_SalesOrderStatus,
					"销售订单提交时间",
					"销售订单审核时间",
					"出库计划单号",
					"出库计划状态", 
					"发运单号", 
					"发运状态", 
					ErrorStrings.ExportColumn_ShippingTime, 
					ErrorStrings.ExportColumn_ShippingMethod,
					"收货人", 
					"收货时间",
					"ERP平台单号 ",
					"代理库入库计划单号",
					"代理库入库单号", 
					ErrorStrings.ExportColumn_SparePartCode, 
					ErrorStrings.ExportColumn_SparePartName, 
					"订单提报数量",
					"订单审核数量", 
					ErrorStrings.InOutSettleSummaryReport54,
					"发运数量",
					"确认数量", 
					"代理库入库数量",
					"所属代理库"
                });
                    using (var excelExport = new ExcelExport(exportDataFileName))
                    {
                        excelExport.ExportByRow(index =>
                        {
                            if (index == result.Count() + 1)
                                return null;
                            if (index == 0)
                                return excelColumns.ToArray<Object>();
                            var detail = result[index - 1];
                            var values = new object[] {
                            detail.PartsSalesOrderCode, detail.SalesCategoryName, detail.WarehouseCode, detail.WarehouseName, detail.SubmitCompanyCode, detail.SubmitCompanyName,
                            Enum.GetName(typeof(DcsCompanyType), detail.CustomerType), detail.PartsSalesOrderTypeName,  Enum.GetName(typeof(DcsPartsSalesOrderStatus), detail.Status), 
                            detail.SubmitTime,detail.ApproveTime,detail.PartsOutboundPlanCode, detail.PartsOutboundPlanStatus!=null?Enum.GetName(typeof(DcsPartsOutboundPlanStatus),detail.PartsOutboundPlanStatus):" ",
                            detail.PartsShippingOrderCode, detail.ShippingStatus!=null?Enum.GetName(typeof(DcsPartsShippingOrderStatus),detail.ShippingStatus):" ", detail.ShippingDate, 
                            detail.ShippingMethod!=null?Enum.GetName(typeof(DcsPartsShippingMethod),detail.ShippingMethod):" ",
                            detail.PSOrderConsigneeName,detail.ConfirmedReceptionTime,detail.ERPSourceOrderCode, detail.PartsInboundPlanCode, 
                            detail.PartsInboundCheckBillCode,detail.SparePartCode, detail.SparePartName, detail.OrderedQuantity, detail.ApproveQuantity, detail.OutboundQuantity,
                            detail.ShippingQuantity, detail.ConfirmQuantity,detail.InboundQuantity,detail.SalesUnitOwnerCompanyName
                        };
                            return values;
                        });
                    }
                }
                return exportDataFileName;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string ExportPartsSalesOrderForExport(int? id, string contractCode, string receivingCompanyCode, string receivingCompanyName, int? partsSalesOrderTypeId, int? salesCategoryId, int? status, DateTime? createTimeStart, DateTime? createTimeEnd)
        {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("出口订单导出_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var query = GetPartsSalesOrders().Where(r => r.IsExport.HasValue && r.IsExport == 1);
            var data = from a in query 
                        join b in ObjectContext.PartsSalesOrderDetails 
                        on a.Id equals b.PartsSalesOrderId into tempdata1
                        from t in tempdata1.DefaultIfEmpty()
                        select new
                        {
                            a.Code,
                            a.ContractCode,
                            a.ReceivingCompanyCode,
                            a.ReceivingCompanyName,
                            a.PartsSalesOrderTypeName,
                            a.Status,
                            a.PriceType,
                            a.InvoiceReceiveCompanyName,
                            a.ShippingMethod,
                            a.TotalAmount,
                            a.PackingMethod,
                            a.ReceivingAddress,
                            a.Remark,
                            a.CreatorName,
                            a.CreateTime,
                            a.ModifierName,
                            a.ModifyTime,
                            a.ApproverName,
                            a.ApproveTime,
                            a.SalesCategoryId,
                            a.PartsSalesOrderTypeId,
                            a.Id,
                            t.SparePartCode,
                            t.SparePartName,
                            t.OrderedQuantity,
                            t.ApproveQuantity,
                            t.OriginalPrice,
                            t.OrderPrice,
                            t.DiscountedPrice,
                            t.OrderSum,
                            t.SalesPrice,
                            detailRemark=t.Remark,
                            t.PriceTypeName,
                            t.ABCStrategy,
                            t.OrderNo
                        };

            if (id.HasValue)
            {
                data = data.Where(r => r.Id == id);
            }
            else
            {
                if (!string.IsNullOrEmpty(contractCode))
                {
                    data = data.Where(r => r.ContractCode.Contains(contractCode));
                }
                if (!string.IsNullOrEmpty(receivingCompanyCode))
                {
                    data = data.Where(r => r.ReceivingCompanyCode.Contains(receivingCompanyCode));
                }
                if (!string.IsNullOrEmpty(receivingCompanyName))
                {
                    data = data.Where(r => r.ReceivingCompanyName.Contains(receivingCompanyName));
                }
                if (partsSalesOrderTypeId.HasValue)
                {
                    data = data.Where(r => r.PartsSalesOrderTypeId == partsSalesOrderTypeId);
                }
                if (salesCategoryId.HasValue)
                {
                    data = data.Where(r => r.SalesCategoryId == salesCategoryId);
                }
                if (status.HasValue)
                {
                    data = data.Where(r => r.Status == status);
                }
                if (createTimeStart.HasValue)
                {
                    data = data.Where(r => r.CreateTime >= createTimeStart);
                }
                if (createTimeEnd.HasValue)
                {
                    data = data.Where(r => r.CreateTime <= createTimeEnd);
                }
            }

            var dataArray = data.ToArray();
            if (dataArray.Any())
            {
                excelColumns.Add(ErrorStrings.ExportColumn_OrderCode);
                excelColumns.Add(ErrorStrings.ExportColumn_ExportContactCode);
                excelColumns.Add(ErrorStrings.ExportColumn_ExportCustomerCode);
                excelColumns.Add(ErrorStrings.ExportColumn_ExportCustomerName);
                excelColumns.Add(ErrorStrings.ExportColumn_OrderType3);
                excelColumns.Add(ErrorStrings.ExportColumn_Status);
                excelColumns.Add(ErrorStrings.ExportColumn_PriceType);
                excelColumns.Add(ErrorStrings.ExportColumn_InvoiceCompany);
                excelColumns.Add(ErrorStrings.ExportColumn_ShippingMethod);
                excelColumns.Add(ErrorStrings.ExportColumn_OrderSum);
                excelColumns.Add(ErrorStrings.ExportColumn_PackingMethod);
                excelColumns.Add(ErrorStrings.ExportColumn_ReceiveAdress);
                excelColumns.Add(ErrorStrings.ExportColumn_Remark);
                excelColumns.Add(ErrorStrings.ExportColumn_Creator);
                excelColumns.Add(ErrorStrings.ExportColumn_CreateTime);
                excelColumns.Add(ErrorStrings.ExportColumn_Modifier);
                excelColumns.Add(ErrorStrings.ExportColumn_ModifyTime);
                excelColumns.Add(ErrorStrings.ExportColumn_Approver);
                excelColumns.Add(ErrorStrings.ExportColumn_ApproveTime);
                excelColumns.Add(ErrorStrings.ExportColumn_SparePartCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SparePartName);
                excelColumns.Add(ErrorStrings.ExportColumn_OrderQuantity);
                excelColumns.Add(ErrorStrings.ExportColumn_FullQuantity);
                excelColumns.Add(ErrorStrings.ExportColumn_PartsSalesPrice);
                excelColumns.Add(ErrorStrings.ExportColumn_OrderPrice);
                excelColumns.Add(ErrorStrings.ExportColumn_PriceDiscount);
                excelColumns.Add(ErrorStrings.ExportColumn_OrderAmount);
                excelColumns.Add("经销价");
                excelColumns.Add(ErrorStrings.ExportColumn_Remark);
                excelColumns.Add(ErrorStrings.ExportColumn_PriceType);
                excelColumns.Add(ErrorStrings.ExportColumn_SparePartABC);
                excelColumns.Add(ErrorStrings.ExportColumn_Order_Code);
                using (var excelExport = new ExcelExport(exportDataFileName))
                {
                    excelExport.ExportByRow(index =>
                    {
                        if (index == dataArray.Count() + 1)
                            return null;
                        if (index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.Code, detail.ContractCode, detail.ReceivingCompanyCode, detail.ReceivingCompanyName, detail.PartsSalesOrderTypeName, 
                            Enum.GetName(typeof(DcsPartsSalesOrderStatus), detail.Status), Enum.GetName(typeof(DcsExportCustPriceType), detail.PriceType??0),
                            detail.InvoiceReceiveCompanyName, GetKeyValueItemByKey("PartsShipping_Method", detail.ShippingMethod).Value,
                            detail.TotalAmount, detail.PackingMethod, detail.ReceivingAddress, detail.Remark,detail.CreatorName,detail.CreateTime,detail.ModifierName,
                            detail.ModifyTime,detail.ApproverName,detail.ApproveTime,detail.SparePartCode,detail.SparePartName,detail.OrderedQuantity,detail.ApproveQuantity,
                            detail.OriginalPrice,detail.OrderPrice,detail.DiscountedPrice,detail.OrderSum,detail.SalesPrice,detail.detailRemark,detail.PriceTypeName,detail.ABCStrategy,detail.OrderNo
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
    }
}
