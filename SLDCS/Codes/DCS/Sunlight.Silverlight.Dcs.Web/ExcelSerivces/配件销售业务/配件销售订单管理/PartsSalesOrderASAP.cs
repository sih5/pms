﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string ExportPartsSalesOrderAndPartsOutboundPlanASAP(int[] ids, string code, int? interfaceId, int? status, DateTime? createTimeStart, DateTime? createTimeEnd) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("配件销售订单或出口计划接口数据_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var partsSalesOrderAndPartsOutboundPlan = GetPartsSalesOrderAndPartsOutboundPlanASAP();
            IQueryable<VirtualPartsSalesOrderOutboundPlanHw> data = partsSalesOrderAndPartsOutboundPlan;
            if(ids != null && ids.Length > 0) {
                data = data.Where(r => ids.Contains(r.CodeId));
            } else {
                if(interfaceId.HasValue) {
                    data = data.Where(r => r.Status == interfaceId);
                }
                if(!string.IsNullOrEmpty(code)) {
                    data = data.Where(r => r.Code.Contains(code));
                }
                if(status.HasValue) {
                    data = data.Where(r => r.Status == status);
                }
                if(createTimeStart.HasValue) {
                    data = data.Where(r => r.ModifyTime >= createTimeStart);
                }
                if(createTimeEnd.HasValue) {
                    data = data.Where(r => r.ModifyTime <= createTimeEnd);
                }
            }
            
            var dataArray = data.ToArray();
            if(dataArray.Any()) {
                excelColumns.Add(EntityStrings.IMSPurchaseLoginInfo_Code);
                excelColumns.Add(EntityStrings.PartsSalesOrderAndPartsOutboundPlanASAP_InterfaceId);
                excelColumns.Add(EntityStrings.PartsSalesOrderAndPartsOutboundPlanASAP_Status);
                excelColumns.Add(EntityStrings.PartsSalesOrderAndPartsOutboundPlanASAP_ModifyTime);
                excelColumns.Add(EntityStrings.PartsSalesOrderAndPartsOutboundPlanASAP_Message);
                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.Code, Enum.GetName(typeof(DcsInterfaceType), detail.InterfaceId), Enum.GetName(typeof(DcsSynchronousState), detail.Status), detail.ModifyTime, detail.Message
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
    }
}
