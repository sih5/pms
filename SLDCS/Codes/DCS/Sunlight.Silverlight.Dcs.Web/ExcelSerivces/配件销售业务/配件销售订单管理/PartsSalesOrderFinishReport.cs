﻿using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string 导出销售订单完成情况统计(string code, string referenceCode, DateTime? bCreateTime, DateTime? eCreateTime, DateTime? bApproveTime, DateTime? eApproveTime, DateTime? bPickingFinishTime, DateTime? ePickingFinishTime, DateTime? bShippingDate, DateTime? eShippingDate, int? warehouseId, string submitCompanyName, string sparePartCode, int? status, string partsOutboundBillCode, string contractCode, string partsPurchaseOrderCode, string marketingDepartmentName, DateTime? bSubmitTime, DateTime? eSubmitTime, int? partsSalesOrderTypeId) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("销售订单完成情况统计_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = GetPartsSalesOrderFinishReports(code, referenceCode, bCreateTime, eCreateTime, bApproveTime, eApproveTime, bPickingFinishTime, ePickingFinishTime, bShippingDate, eShippingDate, warehouseId, submitCompanyName, sparePartCode, status, partsOutboundBillCode, contractCode, partsPurchaseOrderCode, marketingDepartmentName, bSubmitTime, eSubmitTime, partsSalesOrderTypeId);
            var dataArray = data.ToArray();
            if(data.Any()) {
                excelColumns.Add(ErrorStrings.ExportColumn_MarketingDepartment);
                excelColumns.Add(ErrorStrings.ExportColumn_WarehouseName);
                excelColumns.Add(ErrorStrings.ExportColumn_Province);
                excelColumns.Add(ErrorStrings.ExportColumn_CustomerCode);
                excelColumns.Add(ErrorStrings.ExportColumn_CustomerName);
                excelColumns.Add(ErrorStrings.ExportColumn_ReceiveCompany);
                excelColumns.Add(ErrorStrings.ExportColumn_PartCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SIHCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SparePartName);
                excelColumns.Add(ErrorStrings.ExportColumn_EnglishSparePartName);
                excelColumns.Add(ErrorStrings.ExportColumn_PartProp);
                excelColumns.Add(ErrorStrings.ExportColumn_PartGroup);
                excelColumns.Add(ErrorStrings.ExportColumn_IfDirectProvision);
                excelColumns.Add(EntityStrings.RepairWorkOrderMd_ProcessMode);
                excelColumns.Add(ErrorStrings.ExportColumn_OrderQuantity);
                excelColumns.Add(ErrorStrings.PartsSalesOrderProcessDetail_CurrentFulfilledQuantity);
                excelColumns.Add(ErrorStrings.PartsSalesOrderProcessDetail_UnApproveQuantity);
                excelColumns.Add(ErrorStrings.PartsSalesOrderProcessDetail_OutBoundQuanTity);
                excelColumns.Add(ErrorStrings.PartsSalesOrderProcessDetail_DIfferenceQuanTity);
                excelColumns.Add(ErrorStrings.ExportColumn_PriceProp);
                excelColumns.Add(ErrorStrings.PartsSalesOrderProcessDetail_Price);
                excelColumns.Add(ErrorStrings.PartsSalesOrderProcessDetail_Amount);
                excelColumns.Add(ErrorStrings.PartsSalesOrderProcessDetail_OrderPrice);
                excelColumns.Add(ErrorStrings.PartsSalesOrderProcessDetail_OrderAmount);
                excelColumns.Add(ErrorStrings.ExportColumn_Weight);
                excelColumns.Add(ErrorStrings.ExportColumn_Volume);
                excelColumns.Add(ErrorStrings.ExportColumn_PartsSalesOrder_Code);
                excelColumns.Add(ErrorStrings.ExportColumn_PartsSalesOrderProcessCode);
                excelColumns.Add(ErrorStrings.ExportColumn_Times);
                excelColumns.Add(ErrorStrings.ExportColumn_OutBoundCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SalesOrderStatus);
                excelColumns.Add(ErrorStrings.ExportColumn_OutPlanStatus);
                excelColumns.Add(ErrorStrings.ExportColumn_FirApproveTime);
                excelColumns.Add(ErrorStrings.ExportColumn_SalesRemark);
                excelColumns.Add(ErrorStrings.ExportColumn_FirstsApproveTime);
                excelColumns.Add(ErrorStrings.ExportColumn_ShippingTime);
                excelColumns.Add(ErrorStrings.ExportColumn_ShippingCreateTime);
                excelColumns.Add(ErrorStrings.ExportColumn_OrderType3);
                excelColumns.Add(EntityStrings.PartsOutboundBill_ContractCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SalesCreateTime);
                excelColumns.Add(ErrorStrings.ExportColumn_SubmitTime);
                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.MarketingDepartmentName,							
                            detail.WarehouseName,
                            detail.Province,
                            detail.SubmitCompanyCode,
                            detail.SubmitCompanyName, 
                            detail.ReceivingCompanyName,
                            detail.SparePartCode,
                            detail.ReferenceCode,
                            detail.SparePartName,
                            detail.EnglishName,
                            Enum.GetName(typeof(DcsABCStrategyCategory),detail.PartABC==null?0:detail.PartABC),
                            Enum.GetName(typeof(DcsPartsBranchPurchaseRoute),detail.PurchaseRoute==null?0:detail.PurchaseRoute),
                            detail.IfDirectProvision?EntityStrings.Export_Title_Yes:EntityStrings.Export_Title_No,
                           Enum.GetName(typeof(DcsPartsSalesOrderProcessDetailProcessMethod),detail.OrderProcessMethod==null?0:detail.OrderProcessMethod),
                            detail.OrderedQuantity,
                            detail.ApproveQuantity,
                            detail.UnApproveQuantity,
                          //  detail.PickingQty,
                            detail.OutboundAmount,
                            detail.DifferenceQuantity,
                            detail.GroupName,
                            detail.OrderPrice,
                            detail.OrderSum,
                            detail.OriginalPrice,
                            detail.OriginalPriceSum,
                            detail.Weight,
                             detail.Volume,
                            detail.Code,
                            detail.PartsSalesOrderProcessCode,
                            detail.Time,
                            detail.PartsOutboundBillCode,
                            Enum.GetName(typeof(DcsPartsSalesOrderStatus),detail.Status==null?0:detail.Status),
                            Enum.GetName(typeof(DcsPartsOutboundPlanStatus),detail.PartsOutboundPlanStatus==null?0:detail.PartsOutboundPlanStatus),
                            detail.ApproveTime,
                            detail.Remark,
                            detail.FirstApproveTime,
                            detail.ShippingDate,
                            detail.ShippingCreatTime,
                            detail.PartsSalesOrderTypeName,
                            detail.ContractCode,
                            detail.CreateTime,
                           detail.SubmitTime
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
        public string 导出中心库销售订单完成情况统计(string code, string referenceCode, DateTime? bCreateTime, DateTime? eCreateTime, DateTime? bApproveTime, DateTime? eApproveTime, DateTime? bShippingDate, DateTime? eShippingDate, int? warehouseId, string submitCompanyName, string sparePartCode, int? status, string partsOutboundBillCode, string contractCode, string partsPurchaseOrderCode, string marketingDepartmentName, int? retailStatus, DateTime? bSubmitTime, DateTime? eSubmitTime, int? partsSalesOrderTypeId) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("中心库销售订单完成情况统计_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = GetCentralPartsSalesOrderFinishs(code, referenceCode, bCreateTime, eCreateTime, bApproveTime, eApproveTime, bShippingDate, eShippingDate, warehouseId, submitCompanyName, sparePartCode, status, partsOutboundBillCode, contractCode, partsPurchaseOrderCode, marketingDepartmentName, retailStatus, bSubmitTime, eSubmitTime, partsSalesOrderTypeId);
            var dataArray = data.ToArray();
            if(data.Any()) {
                excelColumns.Add(ErrorStrings.ExportColumn_MarketingDepartment);
				excelColumns.Add(ErrorStrings.ExportColumn_AgencyName);
                excelColumns.Add(ErrorStrings.ExportColumn_WarehouseName);
                excelColumns.Add(ErrorStrings.ExportColumn_Province);
                excelColumns.Add(ErrorStrings.ExportColumn_CustomerCode);
                excelColumns.Add(ErrorStrings.ExportColumn_CustomerName);
                excelColumns.Add(ErrorStrings.ExportColumn_ReceiveCompany);
                excelColumns.Add(ErrorStrings.ExportColumn_PartCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SIHCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SparePartName);
                excelColumns.Add(ErrorStrings.ExportColumn_EnglishSparePartName);
                excelColumns.Add(ErrorStrings.ExportColumn_PartProp);
                excelColumns.Add(ErrorStrings.ExportColumn_PartGroup);
                excelColumns.Add(ErrorStrings.ExportColumn_IfDirectProvision);
                excelColumns.Add(EntityStrings.RepairWorkOrderMd_ProcessMode);
                excelColumns.Add(ErrorStrings.ExportColumn_OrderQuantity);
                excelColumns.Add(ErrorStrings.PartsSalesOrderProcessDetail_CurrentFulfilledQuantity);
                excelColumns.Add(ErrorStrings.PartsSalesOrderProcessDetail_UnApproveQuantity);
                excelColumns.Add(ErrorStrings.PartsSalesOrderProcessDetail_OutBoundQuanTity);
                excelColumns.Add(ErrorStrings.PartsSalesOrderProcessDetail_DIfferenceQuanTity);
                excelColumns.Add(ErrorStrings.ExportColumn_PriceProp);
                excelColumns.Add(ErrorStrings.PartsSalesOrderProcessDetail_Price);
                excelColumns.Add(ErrorStrings.PartsSalesOrderProcessDetail_Amount);
                excelColumns.Add(ErrorStrings.PartsSalesOrderProcessDetail_OrderPrice);
                excelColumns.Add(ErrorStrings.PartsSalesOrderProcessDetail_OrderAmount);
                excelColumns.Add(ErrorStrings.ExportColumn_Weight);
                excelColumns.Add(ErrorStrings.ExportColumn_Volume);
                excelColumns.Add(ErrorStrings.ExportColumn_PartsSalesOrder_Code);
                excelColumns.Add(ErrorStrings.ExportColumn_PartsSalesOrderProcessCode);
                excelColumns.Add(ErrorStrings.ExportColumn_Times);
                excelColumns.Add(ErrorStrings.ExportColumn_OutBoundCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SalesOrderStatus);
                excelColumns.Add(ErrorStrings.ExportColumn_OutPlanStatus);
                excelColumns.Add(ErrorStrings.ExportColumn_FirApproveTime);
                excelColumns.Add(ErrorStrings.ExportColumn_SalesRemark);
                excelColumns.Add(ErrorStrings.ExportColumn_FirstsApproveTime);
                excelColumns.Add(ErrorStrings.ExportColumn_ShippingTime);
                excelColumns.Add(ErrorStrings.ExportColumn_ShippingCreateTime);
                excelColumns.Add(ErrorStrings.ExportColumn_OrderType3);
                excelColumns.Add(EntityStrings.PartsOutboundBill_ContractCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SalesCreateTime);
                excelColumns.Add(ErrorStrings.ExportColumn_SubmitTime);
                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.MarketingDepartmentName,
							detail.SalesUnitOwnerCompanyName,
                            detail.WarehouseName,
                            detail.Province,
                            detail.SubmitCompanyCode,
                            detail.SubmitCompanyName, 
                            detail.ReceivingCompanyName,
                            detail.SparePartCode,
                            detail.ReferenceCode,
                            detail.SparePartName,
                            detail.EnglishName,
                            Enum.GetName(typeof(DcsABCStrategyCategory),detail.PartABC==null?0:detail.PartABC),
                            Enum.GetName(typeof(DcsPartsBranchPurchaseRoute),detail.PurchaseRoute==null?0:detail.PurchaseRoute),
                            "",
                           Enum.GetName(typeof(DcsPartsSalesOrderProcessDetailProcessMethod),detail.OrderProcessMethod==null?0:detail.OrderProcessMethod),
                            detail.OrderedQuantity,
                            detail.ApproveQuantity,
                            detail.UnApproveQuantity,
                            detail.OutboundAmount,
                            detail.DifferenceQuantity,
                            detail.GroupName,
                            detail.OrderPrice,
                            detail.OrderSum,
                            detail.OriginalPrice,
                            detail.OriginalPriceSum,
                            detail.Weight,
                             detail.Volume,
                            detail.Code,
                            detail.PartsSalesOrderProcessCode,
                            detail.Time,
                            detail.PartsOutboundBillCode,
                            detail.StatusStr,
                            Enum.GetName(typeof(DcsPartsOutboundPlanStatus),detail.PartsOutboundPlanStatus==null?0:detail.PartsOutboundPlanStatus),
                            detail.ApproveTime,
                            detail.Remark,
                            detail.FirstApproveTime,
                            detail.ShippingDate,
                            detail.ShippingCreatTime,
                            detail.PartsSalesOrderTypeName,
                            detail.ContractCode,
                            detail.CreateTime,
                            detail.SubmitTime
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }

        public string 导出中心库欠货台账统计(string submitCompanyName, string referenceCode, string sparePartCode) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("中心库欠货台账统计_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = GetPartsSalesOrderOwnings(submitCompanyName, referenceCode, sparePartCode); 
            var dataArray = data.ToArray();
            if(data.Any()) {
                excelColumns.Add(ErrorStrings.ExportColumn_MarketingDepartment);
                excelColumns.Add(ErrorStrings.ExportColumn_AgencyName);
                excelColumns.Add(ErrorStrings.ExportColumn_CustomerName);
                excelColumns.Add(ErrorStrings.ExportColumn_CustomerCode);
                excelColumns.Add(ErrorStrings.ExportColumn_PartCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SIHCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SparePartName);
                excelColumns.Add(ErrorStrings.ExportColumn_UnThrough);
                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                               detail.MarketingDepartmentName,
                            detail.CompanyName,
                            detail.SubmitCompanyName,
                            detail.SubmitCompanyCode,
                            detail.SparePartCode,
                            detail.ReferenceCode,
                            detail.SparePartName,
                            detail.UnApproveQuantity
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
        public string 销售出库单统计导出(int? warehouseId, DateTime? bShippingDate, DateTime? eShippingDate, string marketingDepartmentName, string province, string code, string partsOutboundBillCode, string sparePartName, string referenceCode, string submitCompanyName, string settlementStatus, int? partsSalesOrderTypeId, int? groupNameId, string sparePartCode) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("SIH销售出库单统计_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = GetCentralPartsSalesOutBoundList(warehouseId, bShippingDate, eShippingDate, marketingDepartmentName, province, code, partsOutboundBillCode, sparePartName, referenceCode, submitCompanyName, settlementStatus, partsSalesOrderTypeId, groupNameId,sparePartCode);
            var dataArray = data.ToArray();
            if(data.Any()) {
                excelColumns.Add(ErrorStrings.ExportColumn_MarketingDepartment);
                excelColumns.Add(ErrorStrings.ExportColumn_WarehouseName);
                excelColumns.Add(ErrorStrings.ExportColumn_Province);
                excelColumns.Add(ErrorStrings.ExportColumn_CustomerCode);
                excelColumns.Add(ErrorStrings.ExportColumn_CustomerName);
                excelColumns.Add(ErrorStrings.ExportColumn_CustomerType);
                excelColumns.Add(ErrorStrings.ExportColumn_SalesOrderCode);
                excelColumns.Add(ErrorStrings.ExportColumn_PartsSalesOrderProcessCode);
                excelColumns.Add(ErrorStrings.ExportColumn_OutCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SIHCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SAPSparePartCode);
                excelColumns.Add(ErrorStrings.ExportColumn_MapName);
                excelColumns.Add(ErrorStrings.ExportColumn_OrderType3);
                excelColumns.Add(ErrorStrings.ExportColumn_PartABC);
                excelColumns.Add(ErrorStrings.InOutSettleSummaryReport54);
                excelColumns.Add(ErrorStrings.ExportColumn_StockCost);
                excelColumns.Add(ErrorStrings.ExportColumn_StockCostAmount2);
                excelColumns.Add(ErrorStrings.ExportColumn_PartsSalesPrice);
                excelColumns.Add(ErrorStrings.ExportColumn_SettlementPriceSum);
                excelColumns.Add(ErrorStrings.ExportColumn_DiscountedPrice);
                excelColumns.Add(ErrorStrings.ExportColumn_DiscountedRate);
                excelColumns.Add(ErrorStrings.ExportColumn_FinalPriceCenter);
                excelColumns.Add(ErrorStrings.ExportColumn_FinalAmountCenter);
                excelColumns.Add(ErrorStrings.ExportColumn_DealerPrice2);
                excelColumns.Add(ErrorStrings.ExportColumn_DealerPriceAmount);
                excelColumns.Add(ErrorStrings.ExportColumn_ShippingTime);
                excelColumns.Add(ErrorStrings.ExportColumn_QuantityToSettle);
                excelColumns.Add(ErrorStrings.ExportColumn_Remark);
                excelColumns.Add(ErrorStrings.ExportColumn_PriceProp);
                excelColumns.Add(ErrorStrings.ExportColumn_SettlementStatus);
                excelColumns.Add(ErrorStrings.ExportColumn_IsReplace);
                excelColumns.Add(ErrorStrings.ExportColumn_IsReplaceSap);
                excelColumns.Add(ErrorStrings.ExportColumn_IsOwing);
                excelColumns.Add(ErrorStrings.ExportColumn_SAPCode);
                excelColumns.Add(ErrorStrings.ExportColumn_InvoiceNumber);
                excelColumns.Add(ErrorStrings.ExportColumn_SettleBillCode);
                excelColumns.Add(ErrorStrings.ExportColumn_IfDirectProvision);
                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.MarketingDepartmentName,
                            detail.WarehouseName,
                            detail.Province,
                            detail.SubmitCompanyCode,
                            detail.SubmitCompanyName, 
                            detail.CustomerTypeStr,
                            detail.Code,
                            detail.PartsSalesOrderProcessCode,
                            detail.PartsOutboundBillCode,
                            detail.ReferenceCode,
                            detail.SparePartCode,
                            detail.SparePartName,
                            detail.PartsSalesOrderTypeName,
                            Enum.GetName(typeof(DcsABCStrategyCategory),detail.PartABC==null?0:detail.PartABC),
                            detail.OutboundAmount,
                            detail.CostPrice,
                            detail.CostPriceAll,
                            detail.OriginalPrice,
                            detail.OriginalPriceSum,
                            detail.DiscountAmount,
                            detail.DiscountAmountRate,
                            detail.Settlementprice,
                            detail.SettlementpriceAll,
                            detail.SalesPrice,
                            detail.SalesPriceAll,
                            detail.ShippingDate,
                             detail.QuantityToSettle,
                            detail.Remark,
                            detail.GroupName,
                            Enum.GetName(typeof(DcsPartsSalesSettlementStatus),detail.SettlementStatus==null?0:detail.SettlementStatus),
                            EntityStrings.Export_Title_No,
                            "",
                            EntityStrings.Export_Title_No,
                            "",
                            "",
                            detail.SettlementCode,
                            detail.IfDirectProvision?EntityStrings.Export_Title_Yes:EntityStrings.Export_Title_No,
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
        public string 导出销售在途统计(DateTime? bApproveTime, DateTime? eApproveTime, DateTime? bShippingDate, DateTime? eShippingDate, string shippingCode, string submitCompanyName, int? submitCompanyType) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("销售在途统计_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = 销售在途统计(bApproveTime, eApproveTime, bShippingDate, eShippingDate, shippingCode, submitCompanyName, submitCompanyType);
            var dataArray = data.ToArray();
            if(data.Any()) {
                excelColumns.Add(ErrorStrings.ExportColumn_MarketingDepartment);
                excelColumns.Add(ErrorStrings.ExportColumn_WarehouseName);
                excelColumns.Add(ErrorStrings.ExportColumn_Province);
                excelColumns.Add(ErrorStrings.ExportColumn_PartsSalesOrder_Code);
                excelColumns.Add(ErrorStrings.ExportColumn_PartsSalesOrderProcessCode);
                excelColumns.Add(EntityStrings._Common__CheckTime);
                excelColumns.Add(ErrorStrings.ExportColumn_OrderQuantity);
                excelColumns.Add(ErrorStrings.ExportColumn_FullQuantity);
                excelColumns.Add(ErrorStrings.ExportColumn_CustomerName);
                excelColumns.Add(ErrorStrings.ExportColumn_SIHCode);
                excelColumns.Add(ErrorStrings.ExportColumn_SAPSparePartCode);                
                excelColumns.Add(ErrorStrings.ExportColumn_ProductName);
                excelColumns.Add(ErrorStrings.ExportColumn_OrderType3);
                excelColumns.Add(ErrorStrings.ExportColumn_PartProp);
                excelColumns.Add(ErrorStrings.InOutSettleSummaryReport54);
                excelColumns.Add(ErrorStrings.ExportColumn_SaleOrderPrice);
                excelColumns.Add(ErrorStrings.ExportColumn_SaleOrderAmount);
                excelColumns.Add(ErrorStrings.ExportColumn_PartsSalesPrice);
                excelColumns.Add(ErrorStrings.ExportColumn_ShippingTime);
                excelColumns.Add(ErrorStrings.ExportColumn_RequestedDeliveryTime);
                excelColumns.Add(ErrorStrings.ExportColumn_Remark);               
                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.MarketingDepartmentName,
                            detail.WarehouseName,
                            detail.Province,
                            detail.Code,
                            detail.PartsSalesOrderProcessCode,
                            detail.ApproveTime,
                            detail.OrderedQuantity,
                            detail.ApproveQuantity,
                            detail.SubmitCompanyName, 
                            detail.ReferenceCode,
                            detail.SparePartCode,                           
                            detail.SparePartName,
                            detail.PartsSalesOrderTypeName,
                            Enum.GetName(typeof(DcsABCStrategyCategory),detail.PartABC==null?0:detail.PartABC),
                            detail.OutboundAmount,
                            detail.OrderPrice,
                            detail.OrderSum,
                            detail.OriginalPrice,
                            detail.ShippingDate,
                            detail.RequestedDeliveryTime,
                            detail.Remark                          
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
    }
}
