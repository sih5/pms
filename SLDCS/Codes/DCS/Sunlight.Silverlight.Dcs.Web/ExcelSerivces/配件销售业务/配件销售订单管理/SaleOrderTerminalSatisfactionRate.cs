﻿using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public string 导出终端满足率明细(string centerName, DateTime? bSubmitTime, DateTime? eSubmitTime, string marketingDepartmentName, string dealerName) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("终端满足率明细_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = 终端满足率明细查询(centerName, bSubmitTime, eSubmitTime, marketingDepartmentName, dealerName);
            var dataArray = data.ToArray();
            if(data.Any()) {
                excelColumns.Add(ErrorStrings.ExportColumn_MarketingDepartment);
                excelColumns.Add(ErrorStrings.ExportColumn_AgencyName);
                excelColumns.Add(ErrorStrings.Export_Title_DealerNames);
                excelColumns.Add(ErrorStrings.ExportColumn_SalesCode);
                excelColumns.Add(ErrorStrings.Export_Title_SubmitTime);
                excelColumns.Add(ErrorStrings.ExportColumn_OrderType3);
                excelColumns.Add(ErrorStrings.ExportColumn_PartCode);
                excelColumns.Add(ErrorStrings.ExportColumn_OrderQuantity);
                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                               detail.MarketingDepartmentName,
                            detail.CenterName,
                            detail.DealerName,
                            detail.Code,
                            detail.SubmitTime,
                            detail.PartsSalesOrderTypeName,
                            detail.SparePartCode,
                            detail.OrderedQuantity                           
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
        public string 导出终端满足率汇总报表(string centerName, int? year, int? month, int? unit, string marketingDepartmentName, string dealerName) {
            var excelColumns = new List<string>();
            var exportDataFileName = GetExportFilePath(string.Format("终端满足率明细_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            var data = 查询终端满足率汇总报表(centerName, year, month, unit, marketingDepartmentName, dealerName);
            var dataArray = data.ToArray();
            if(data.Any()) {
                excelColumns.Add(ErrorStrings.Export_Title_Year);
                excelColumns.Add(ErrorStrings.Export_Title_Month);
                excelColumns.Add(ErrorStrings.ExportColumn_MarketingDepartment);
                excelColumns.Add(ErrorStrings.ExportColumn_AgencyName);
                excelColumns.Add(ErrorStrings.Export_Title_DealerNames);
                excelColumns.Add(ErrorStrings.DataGridCiew_Title_PartsSales_EmergencyOrderNum);
                excelColumns.Add(ErrorStrings.DataGridCiew_Title_PartsSales_ExEmergencyOrderNum);
                excelColumns.Add(ErrorStrings.DataGridCiew_Title_PartsSales_AB);
                excelColumns.Add(ErrorStrings.DataGridCiew_Title_PartsSales_OrderAmount);
                excelColumns.Add(ErrorStrings.DataGridCiew_Title_PartsSales_EmergencyRate);
                excelColumns.Add(ErrorStrings.DataGridCiew_Title_PartsSales_AbRate);
                excelColumns.Add(ErrorStrings.DataGridCiew_Title_PartsSales_TerminalRate);
                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;
                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];
                        var values = new object[] {
                            detail.Year,
                            detail.Month,
                            detail.MarketingDepartmentName,
                            detail.CenterName,
                            detail.DealerName,
                            detail.EmergencyOrderNum,
                            detail.ExEmergencyOrderNum,
                            detail.AbNum,
                            detail.OrderNum,
                            detail.EmergencyRate,
                            detail.AbRate,
                            detail.TerminalRate
                        };
                        return values;
                    });
                }
            }
            return exportDataFileName;
        }
    }
}
