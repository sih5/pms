﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
#if !SqlServer
using Devart.Data.Oracle;
#endif

namespace Sunlight.Silverlight.Dcs.Web.ExcelService.Helper {
    public abstract class DbHelper {
        public static DbHelper GetDbHelp(string connectionStr) {
            if(string.IsNullOrEmpty(connectionStr))
                throw new ArgumentNullException(connectionStr);
#if SqlServer
            return new SqlServerDbHelper(connectionStr);
#else
            return new OracleDbHelper(connectionStr);
#endif
        }

        /// <summary>
        /// 参数表示符
        /// </summary>
        public string ParamMark {
            get;
            protected set;
        }

        /// <summary>
        /// 当前数据库当前时间的函数
        /// </summary>
        public string Now {
            get;
            protected set;
        }

        /// <summary>
        /// 分批查询数据，每次查询量
        /// </summary>
        public const int PAGE_SIZE = 500;

        public abstract DbConnection CreateDbConnection();

        public abstract DbCommand CreateDbCommand(string commandText, DbConnection conn, DbTransaction ts);

        public abstract DbParameter CreateDbParameter(string parameterName, object value);

        /// <summary>
        /// 获取Insert操作语句（使用参数方式）
        /// </summary>
        /// <param name="tableName">表名</param>
        /// <param name="keyName">主键字段</param>
        /// <param name="fieldName">其他待新增字段</param>
        /// <returns></returns>
        public abstract string GetInsertSql(string tableName, string keyName, string[] fieldName);

        /// <summary>
        /// 获取Update操作语句（使用参数方式）
        /// </summary>
        /// <param name="tableName">表名</param>
        /// <param name="modifyFields">待修改字段</param>
        /// <param name="conditionFields">条件字段（连接符and、比较符等于）</param>
        /// <returns></returns>
        public abstract string GetUpdateSql(string tableName, string[] modifyFields, string[] conditionFields);

        /// <summary>
        /// 执行insert操作
        /// </summary>
        /// <param name="command">待执行DbCommond</param>
        /// <param name="keyField">主键字段</param>
        /// <returns>自动生成的主键值</returns>
        public abstract int ExecuteInsert(DbCommand command, string keyField);

        /// <summary>
        /// 使用in条件查询数据
        /// </summary>
        /// <param name="sql">查询语句</param>
        /// <param name="field">in条件字段</param>
        /// <param name="isStringField">是否为字符类型</param>
        /// <param name="values">in值列表</param>
        /// <param name="action">查询结果单条处理方法，string[]：单条记录值，按sql字段顺序，bool是否需要继续读取下一条数据
        /// </param>
        public abstract void QueryDataWithInOperator(string sql, string field, bool isStringField, string[] values, Func<string[], bool> action);

        /// <summary>
        /// 使用in条件查询数据 查询修改未提交事物之后的数据
        /// </summary>
        /// <param name="sql">查询语句</param>
        /// <param name="field">in条件字段</param>
        /// <param name="isStringField">是否为字符类型</param>
        /// <param name="values">in值列表</param>
        /// <param name="action"/>查询结果单条处理方法，string[]：单条记录值，按sql字段顺序，bool是否需要继续读取下一条数据
        /// <param name="ts">事物</param>
        /// <param name="conn">sql连接</param>
        public abstract void QueryDataWithInOperator(string sql, string field, bool isStringField, string[] values, Func<string[], bool> action, DbTransaction ts, DbConnection conn);

        /// <summary>
        /// 使用in条件查询数据不区分大小写
        /// </summary>
        /// <param name="sql">查询语句</param>
        /// <param name="field">in条件字段</param>
        /// <param name="isStringField">是否为字符类型</param>
        /// <param name="values">in值列表</param>
        /// <param name="action">查询结果单条处理方法，string[]：单条记录值，按sql字段顺序，bool是否需要继续读取下一条数据
        /// </param>
        public abstract void QueryDataWithInOperatorByLower(string sql, string field, bool isStringField, string[] values, Func<string[], bool> action);

        /// <summary>
        /// 获取指定表结构
        /// </summary>
        /// <param name="tableName"></param>
        /// <returns></returns>
        public abstract DataTable GetTableSchema(string tableName);

        /// <summary>
        /// 获取指定表结构
        /// </summary>
        /// <param name="tableName">表名</param>
        /// <param name="notNullableFields">不允许为空的字段名称（大写）</param>
        /// <param name="fieldLenght">string类型字段长度，字段名称（大写）、长度</param>
        public abstract void GetTableSchema(string tableName, out List<string> notNullableFields, out Dictionary<string, int> fieldLenght);

        /// <summary>
        /// 获取数据库当前时间
        /// </summary>
        /// <returns></returns>
        public abstract DateTime GetDbTime();

        public abstract DataTable QueryData(string tableName);
    }

#if !SqlServer
    class OracleDbHelper : DbHelper {
        private readonly string connectionString;

        private static readonly Dictionary<string, string> SequenceNames = new Dictionary<string, string> {
            {
                "DealerSalesExt", null
            }, {
                "DealerServiceExt", null
            }, {
                "Branch", null
            }, {
                "Dealer", null
            }, {
                "PartsSupplier", null
            }, {
                "LogisticCompany", null
            }, {
                "ResponsibleUnit", null
            }, {
                "Agency", null
            }, {
                "ServiceActivityMaterialDetail", "ServiceActivityMatlDetail"
            }, {
                "VehicleAttribute", null
            }, {
                "CustomerOrderPriceGradeHistory", "CustomerOrderPriceGradeHis"
            }, {
                "VehicleShippingOrderChangeRec", "VehShippingOrderChangeRec"
            }, {
                "VehRetailPriceChangeAppDetail", "VehRetailPriceChangeAppDtl"
            }, {
                "VehicleShipplanApprovalDetail", "VehicleShipplanApprovalDtl"
            }, {
                "VehicleDLRStartSecurityDeposit", "VehDLRStartSecurityDeposit"
            }, {
                "PartsSalesRtnSettlementDetail", "PartsSalesRtnSettlementDtl"
            }, {
                "VehPifaPriceChangeAppDetail", "VehPifaPriceChangeAppDtl"
            }
        };

        private static string GetValueOfTableId(string tableName) {
            if(SequenceNames.ContainsKey(tableName)) {
                var sequenceName = SequenceNames[tableName];
                return string.IsNullOrEmpty(sequenceName) ? null : string.Format("S_{0}.Nextval", sequenceName);
            }
            return string.Format("S_{0}.Nextval", tableName);
        }

        public OracleDbHelper(string connectionStr) {
            ParamMark = ":";
            Now = "sysdate";
            connectionString = connectionStr;
        }

        public override DbConnection CreateDbConnection() {
            return new OracleConnection(connectionString);
        }

        public override DbCommand CreateDbCommand(string commandText, DbConnection dbConnection, DbTransaction ts) {
            return ts == null ? new OracleCommand(commandText, (OracleConnection)dbConnection) : new OracleCommand(commandText, (OracleConnection)dbConnection, (OracleTransaction)ts);
        }

        public override DbParameter CreateDbParameter(string parameterName, object value) {
            return new OracleParameter(parameterName, value);
        }

        public override string GetInsertSql(string tableName, string keyName, string[] fieldNames) {
            if(string.IsNullOrEmpty(keyName)) {
                return string.Format("insert into {0} ({1}) Values (:{2})", tableName, string.Join(",", fieldNames), string.Join(",:", fieldNames));
            } else {
                return string.Format("insert into {0} ({1},{2}) Values ({3},:{4})", tableName, keyName, string.Join(",", fieldNames), GetValueOfTableId(tableName) ?? (":" + keyName), string.Join(",:", fieldNames));
            }

        }

        public override string GetUpdateSql(string tableName, string[] modifyFields, string[] conditionFields) {
            var modifyFieldStr = string.Join(",", modifyFields.Select(v => string.Format(" {0}=:{0} ", v)).ToArray());
            var conditionFieldStr = string.Join("and", conditionFields.Select(v => string.Format(" {0}=:{0} ", v)).ToArray());
            return string.Format("Update {0} Set {1} Where {2}", tableName, modifyFieldStr, conditionFieldStr);
        }

        public override int ExecuteInsert(DbCommand command, string keyField) {
            OracleParameter keyParam = null;
            if(!string.IsNullOrEmpty(keyField)) {
                keyParam = new OracleParameter("key_" + keyField, OracleDbType.Integer, ParameterDirection.Output);
                command.Parameters.Add(keyParam);
                command.CommandText = string.Format("declare begin {0} returning {1} into :key_{1}; end;", command.CommandText, keyField);
            }
            var result = command.ExecuteNonQuery();
            if(keyParam != null)
                result = Convert.ToInt32(keyParam.Value);
            return result;
        }
        public override void QueryDataWithInOperator(string sql, string field, bool isStringField, string[] values, Func<string[], bool> func, DbTransaction ts, DbConnection conn) {
            var paramNum = values.Length >= PAGE_SIZE ? PAGE_SIZE : values.Length;
            var sqlParams = new string[paramNum];
            for(var i = 0;i < paramNum;i++)
                sqlParams[i] = string.Format("{0}Param{1}", ParamMark, i);

            var sqlQuery = string.Concat(sql, sql.IndexOf(" where ", StringComparison.OrdinalIgnoreCase) > -1 ? " and " : " where ", isStringField ? string.Format(" upper({0})", field) : field, " in ({0})");
            sqlQuery = sqlQuery.IndexOf(" where 1=1  and ", System.StringComparison.Ordinal) > 0 ? sqlQuery.Replace(" where 1=1  and ", " where ") : sqlQuery.Replace(" where 1=1 ", "");
            var dbParams = new List<OracleParameter>();
            var needStop = false;
            conn.Open();
            for(var i = 0;i < values.Length;i++) {
                dbParams.Add(new OracleParameter(string.Concat("Param", i % PAGE_SIZE), values[i]));
                if((i + 1) % PAGE_SIZE == 0) {
                    var command = new OracleCommand(string.Format(sqlQuery, string.Join(",", sqlParams)), (OracleConnection)conn, (OracleTransaction)ts);
                    command.Parameters.AddRange(dbParams.ToArray());
                    var reader = command.ExecuteReader();
                    var result = new string[reader.FieldCount];
                    while(reader.Read()) {
                        for(var index = 0;index < reader.FieldCount;index++)
                            result[index] = reader[index].ToString();
                        needStop = func(result);
                        if(needStop)
                            break;
                    }
                    reader.Close();
                    dbParams.Clear();
                }
                if(needStop)
                    break;
            }
            if(dbParams.Count > 0) {
                var command = new OracleCommand(string.Format(sqlQuery, string.Join(",", sqlParams.Take(dbParams.Count))), (OracleConnection)conn, (OracleTransaction)ts);
                command.Parameters.AddRange(dbParams.ToArray());
                var reader = command.ExecuteReader();
                var result = new string[reader.FieldCount];
                while(reader.Read()) {
                    for(var index = 0;index < reader.FieldCount;index++)
                        result[index] = reader[index].ToString();
                    if(func(result))
                        break;
                }
                reader.Close();
            }
        }
        public override void QueryDataWithInOperator(string sql, string field, bool isStringField, string[] values, Func<string[], bool> func) {
            var paramNum = values.Length >= PAGE_SIZE ? PAGE_SIZE : values.Length;
            var sqlParams = new string[paramNum];
            for(var i = 0;i < paramNum;i++)
                sqlParams[i] = string.Format("{0}Param{1}", ParamMark, i);

            using(var conn = new OracleConnection(connectionString)) {
                var sqlQuery = string.Concat(sql, sql.IndexOf(" where ", StringComparison.OrdinalIgnoreCase) > -1 ? " and " : " where ", isStringField ? string.Format(" upper({0})", field) : field, " in ({0})");
                sqlQuery = sqlQuery.IndexOf(" where 1=1  and ", System.StringComparison.Ordinal) > 0 ? sqlQuery.Replace(" where 1=1  and ", " where ") : sqlQuery.Replace(" where 1=1 ", "");
                var dbParams = new List<OracleParameter>();
                var needStop = false;
                conn.Open();
                for(var i = 0;i < values.Length;i++) {
                    dbParams.Add(new OracleParameter(string.Concat("Param", i % PAGE_SIZE), values[i]));
                    if((i + 1) % PAGE_SIZE == 0) {
                        var command = new OracleCommand(string.Format(sqlQuery, string.Join(",", sqlParams)), conn, null);
                        command.Parameters.AddRange(dbParams.ToArray());
                        var reader = command.ExecuteReader();
                        var result = new string[reader.FieldCount];
                        while(reader.Read()) {
                            for(var index = 0;index < reader.FieldCount;index++)
                                result[index] = reader[index].ToString();
                            needStop = func(result);
                            if(needStop)
                                break;
                        }
                        reader.Close();
                        dbParams.Clear();
                    }
                    if(needStop)
                        break;
                }
                if(dbParams.Count > 0) {
                    var command = new OracleCommand(string.Format(sqlQuery, string.Join(",", sqlParams.Take(dbParams.Count))), conn, null);
                    command.Parameters.AddRange(dbParams.ToArray());
                    var reader = command.ExecuteReader();
                    var result = new string[reader.FieldCount];
                    while(reader.Read()) {
                        for(var index = 0;index < reader.FieldCount;index++)
                            result[index] = reader[index].ToString();
                        if(func(result))
                            break;
                    }
                    reader.Close();
                }
            }
        }

        public override void QueryDataWithInOperatorByLower(string sql, string field, bool isStringField, string[] values, Func<string[], bool> func) {
            var paramNum = values.Length >= PAGE_SIZE ? PAGE_SIZE : values.Length;
            var sqlParams = new string[paramNum];
            for(var i = 0;i < paramNum;i++)
                sqlParams[i] = string.Format("{0}Param{1}", ParamMark, i);

            using(var conn = new OracleConnection(connectionString)) {
                var sqlQuery = string.Concat(sql, sql.IndexOf(" where ", StringComparison.OrdinalIgnoreCase) > -1 ? " and " : " where ", isStringField ? string.Format(" {0}", field) : field, " in ({0})");
                var dbParams = new List<OracleParameter>();
                var needStop = false;
                conn.Open();
                for(var i = 0;i < values.Length;i++) {
                    dbParams.Add(new OracleParameter(string.Concat("Param", i % PAGE_SIZE), values[i]));
                    if((i + 1) % PAGE_SIZE == 0) {
                        var command = new OracleCommand(string.Format(sqlQuery, string.Join(",", sqlParams)), conn, null);
                        command.Parameters.AddRange(dbParams.ToArray());
                        var reader = command.ExecuteReader();
                        var result = new string[reader.FieldCount];
                        while(reader.Read()) {
                            for(var index = 0;index < reader.FieldCount;index++)
                                result[index] = reader[index].ToString();
                            needStop = func(result);
                            if(needStop)
                                break;
                        }
                        reader.Close();
                        dbParams.Clear();
                    }
                    if(needStop)
                        break;
                }
                if(dbParams.Count > 0) {
                    var command = new OracleCommand(string.Format(sqlQuery, string.Join(",", sqlParams.Take(dbParams.Count))), conn, null);
                    command.Parameters.AddRange(dbParams.ToArray());
                    var reader = command.ExecuteReader();
                    var result = new string[reader.FieldCount];
                    while(reader.Read()) {
                        for(var index = 0;index < reader.FieldCount;index++)
                            result[index] = reader[index].ToString();
                        if(func(result))
                            break;
                    }
                    reader.Close();
                }
            }
        }

        public override DataTable GetTableSchema(string tableName) {
            using(var connect = new OracleConnection(connectionString)) {
                connect.Open();
                var adapter = new OracleDataAdapter("select * from " + tableName, connect);
                var dt = new DataTable();
                return adapter.FillSchema(dt, SchemaType.Mapped);
            }
        }

        public override void GetTableSchema(string tableName, out List<string> notNullableFields, out Dictionary<string, int> fieldLenght) {
            notNullableFields = new List<string>();
            fieldLenght = new Dictionary<string, int>();
            using(var connect = new OracleConnection(connectionString)) {
                connect.Open();
                var reader = new OracleCommand(string.Format("select column_name, data_type, data_length, nullable from user_tab_columns where table_name='{0}'", tableName.ToUpper()), connect).ExecuteReader();
                while(reader.Read()) {
                    if(reader[1].ToString() == "VARCHAR2")
                        fieldLenght.Add(reader[0].ToString().ToUpper(), int.Parse(reader[2].ToString()));
                    if(reader[3].ToString() == "N")
                        notNullableFields.Add(reader[0].ToString().ToUpper());
                }
            }
        }

        public override DateTime GetDbTime() {
            using(var connect = new OracleConnection(connectionString)) {
                connect.Open();
                return DateTime.Parse(new OracleCommand("select sysdate from dual", connect).ExecuteScalar().ToString());
            }
        }

        public override DataTable QueryData(string sql) {
            using(var connect = new OracleConnection(connectionString)) {
                connect.Open();
                var adapter = new OracleDataAdapter(sql, connect);
                DataSet ds = new DataSet();
                adapter.Fill(ds);
                DataTable dt = ds.Tables[0];
                return dt;
            }
        }
    }
#endif

    class SqlServerDbHelper : DbHelper {
        private readonly string connectionString;

        public SqlServerDbHelper(string connectionStr) {
            ParamMark = "@";
            Now = "GetDate()";
            connectionString = connectionStr;
        }

        public override DbConnection CreateDbConnection() {
            return new SqlConnection(connectionString);
        }

        public override DbCommand CreateDbCommand(string commandText, DbConnection dbConnection, DbTransaction ts) {
            return ts == null ? new SqlCommand(commandText, (SqlConnection)dbConnection) : new SqlCommand(commandText, (SqlConnection)dbConnection, (SqlTransaction)ts);
        }

        public override DbParameter CreateDbParameter(string parameterName, object value) {
            return new SqlParameter("@" + parameterName, value);
        }

        public override string GetInsertSql(string tableName, string keyName, string[] fieldNames) {
            return string.Format("insert into {0} ({1}) Values (@{2})", tableName, string.Join(",", fieldNames), string.Join(",@", fieldNames));
        }

        public override string GetUpdateSql(string tableName, string[] modifyFields, string[] conditionFields) {
            var modifyFieldStr = string.Join(",", modifyFields.Select(v => string.Format(" {0}=@{0} ", v)).ToArray());
            var conditionFieldStr = string.Join("and", conditionFields.Select(v => string.Format(" {0}=@{0} ", v)).ToArray());
            return string.Format("Update {0} Set {1} Where {2}", tableName, modifyFieldStr, conditionFieldStr);
        }

        public override int ExecuteInsert(DbCommand command, string keyField) {
            if(string.IsNullOrEmpty(keyField)) {
                return command.ExecuteNonQuery();
            } else {
                command.CommandText += " SELECT @@IDENTITY";
                return Convert.ToInt32(command.ExecuteScalar());
            }
        }
        public override void QueryDataWithInOperator(string sql, string field, bool isStringField, string[] values, Func<string[], bool> func, DbTransaction ts, DbConnection conn) {
            var paramNum = values.Length >= PAGE_SIZE ? PAGE_SIZE : values.Length;
            var sqlParams = new string[paramNum];
            for(var i = 0;i < paramNum;i++)
                sqlParams[i] = string.Format("{0}Param{1}", ParamMark, i);

            var sqlQuery = string.Concat(sql, sql.IndexOf(" where ", StringComparison.OrdinalIgnoreCase) > -1 ? " and " : " where ", isStringField ? string.Format(" upper({0})", field) : field, " in ({0})");
            var dbParams = new List<SqlParameter>();
            var needStop = false;
            conn.Open();
            for(var i = 0;i < values.Length;i++) {
                dbParams.Add(new SqlParameter(string.Concat("Param", i % PAGE_SIZE), values[i]));
                if((i + 1) % PAGE_SIZE == 0) {
                    var command = new SqlCommand(string.Format(sqlQuery, string.Join(",", sqlParams)), (SqlConnection)conn, (SqlTransaction)ts);
                    command.Parameters.AddRange(dbParams.ToArray());
                    var reader = command.ExecuteReader();
                    var result = new string[reader.FieldCount];
                    while(reader.Read()) {
                        for(var index = 0;index < reader.FieldCount;index++)
                            result[index] = reader[index].ToString();
                        needStop = func(result);
                        if(needStop)
                            break;
                    }
                    reader.Close();
                    dbParams.Clear();
                }
                if(needStop)
                    break;
            }
            if(dbParams.Count > 0) {
                var command = new SqlCommand(string.Format(sqlQuery, string.Join(",", sqlParams.Take(dbParams.Count))), (SqlConnection)conn, (SqlTransaction)ts);
                command.Parameters.AddRange(dbParams.ToArray());
                var reader = command.ExecuteReader();
                var result = new string[reader.FieldCount];
                while(reader.Read()) {
                    for(var index = 0;index < reader.FieldCount;index++)
                        result[index] = reader[index].ToString();
                    if(func(result))
                        break;
                }
                reader.Close();
            }
        }

        public override void QueryDataWithInOperator(string sql, string field, bool isStringField, string[] values, Func<string[], bool> func) {
            var paramNum = values.Length >= PAGE_SIZE ? PAGE_SIZE : values.Length;
            var sqlParams = new string[paramNum];
            for(var i = 0;i < paramNum;i++)
                sqlParams[i] = string.Format("{0}Param{1}", ParamMark, i);

            using(var conn = new SqlConnection(connectionString)) {
                var sqlQuery = string.Concat(sql, sql.IndexOf(" where ", StringComparison.OrdinalIgnoreCase) > -1 ? " and " : " where ", isStringField ? string.Format(" upper({0})", field) : field, " in ({0})");
                var dbParams = new List<SqlParameter>();
                var needStop = false;
                conn.Open();
                for(var i = 0;i < values.Length;i++) {
                    dbParams.Add(new SqlParameter(string.Concat("Param", i % PAGE_SIZE), values[i]));
                    if((i + 1) % PAGE_SIZE == 0) {
                        var command = new SqlCommand(string.Format(sqlQuery, string.Join(",", sqlParams)), conn, null);
                        command.Parameters.AddRange(dbParams.ToArray());
                        var reader = command.ExecuteReader();
                        var result = new string[reader.FieldCount];
                        while(reader.Read()) {
                            for(var index = 0;index < reader.FieldCount;index++)
                                result[index] = reader[index].ToString();
                            needStop = func(result);
                            if(needStop)
                                break;
                        }
                        reader.Close();
                        dbParams.Clear();
                    }
                    if(needStop)
                        break;
                }
                if(dbParams.Count > 0) {
                    var command = new SqlCommand(string.Format(sqlQuery, string.Join(",", sqlParams.Take(dbParams.Count))), conn, null);
                    command.Parameters.AddRange(dbParams.ToArray());
                    var reader = command.ExecuteReader();
                    var result = new string[reader.FieldCount];
                    while(reader.Read()) {
                        for(var index = 0;index < reader.FieldCount;index++)
                            result[index] = reader[index].ToString();
                        if(func(result))
                            break;
                    }
                    reader.Close();
                }


            }


        }
        public override void QueryDataWithInOperatorByLower(string sql, string field, bool isStringField, string[] values, Func<string[], bool> func) {
            var paramNum = values.Length >= PAGE_SIZE ? PAGE_SIZE : values.Length;
            var sqlParams = new string[paramNum];
            for(var i = 0;i < paramNum;i++)
                sqlParams[i] = string.Format("{0}Param{1}", ParamMark, i);

            using(var conn = new SqlConnection(connectionString)) {
                var sqlQuery = string.Concat(sql, sql.IndexOf(" where ", StringComparison.OrdinalIgnoreCase) > -1 ? " and " : " where ", isStringField ? string.Format(" {0}", field) : field, " in ({0})");
                var dbParams = new List<SqlParameter>();
                var needStop = false;
                conn.Open();
                for(var i = 0;i < values.Length;i++) {
                    dbParams.Add(new SqlParameter(string.Concat("Param", i % PAGE_SIZE), values[i]));
                    if((i + 1) % PAGE_SIZE == 0) {
                        var command = new SqlCommand(string.Format(sqlQuery, string.Join(",", sqlParams)), conn, null);
                        command.Parameters.AddRange(dbParams.ToArray());
                        var reader = command.ExecuteReader();
                        var result = new string[reader.FieldCount];
                        while(reader.Read()) {
                            for(var index = 0;index < reader.FieldCount;index++)
                                result[index] = reader[index].ToString();
                            needStop = func(result);
                            if(needStop)
                                break;
                        }
                        reader.Close();
                        dbParams.Clear();
                    }
                    if(needStop)
                        break;
                }
                if(dbParams.Count > 0) {
                    var command = new SqlCommand(string.Format(sqlQuery, string.Join(",", sqlParams.Take(dbParams.Count))), conn, null);
                    command.Parameters.AddRange(dbParams.ToArray());
                    var reader = command.ExecuteReader();
                    var result = new string[reader.FieldCount];
                    while(reader.Read()) {
                        for(var index = 0;index < reader.FieldCount;index++)
                            result[index] = reader[index].ToString();
                        if(func(result))
                            break;
                    }
                    reader.Close();
                }


            }


        }

        public override DataTable QueryData(string sql) {
            using(var connect = new SqlConnection(connectionString)) {
                connect.Open();
                var adapter = new SqlDataAdapter(sql, connect);
                DataSet ds = new DataSet();
                adapter.Fill(ds);
                DataTable dt = ds.Tables[0];
                return dt;
            }
        }

        public override DataTable GetTableSchema(string tableName) {
            using(var connect = new SqlConnection(connectionString)) {
                connect.Open();
                var adapter = new SqlDataAdapter("select * from " + tableName, connect);
                var dt = new DataTable();
                return adapter.FillSchema(dt, SchemaType.Mapped);
            }
        }

        public override void GetTableSchema(string tableName, out List<string> notNullableFields, out Dictionary<string, int> fieldLenght) {
            notNullableFields = new List<string>();
            fieldLenght = new Dictionary<string, int>();
            using(var connect = new SqlConnection(connectionString)) {
                connect.Open();
                var reader = new SqlCommand(string.Format("select name, system_type_id, max_length, is_nullable from sys.columns where OBJECT_ID = object_id('{0}')", tableName), connect).ExecuteReader();
                while(reader.Read()) {
                    if(reader[1].ToString() == "167")
                        fieldLenght.Add(reader[0].ToString().ToUpper(), int.Parse(reader[2].ToString()));
                    if(!bool.Parse(reader[3].ToString()))
                        notNullableFields.Add(reader[0].ToString().ToUpper());
                }
            }
        }

        public override DateTime GetDbTime() {
            using(var connect = new SqlConnection(connectionString)) {
                connect.Open();
                return DateTime.Parse(new SqlCommand("select GetDate()", connect).ExecuteScalar().ToString());
            }
        }
    }

    /// <summary>
    /// 编号生成器
    /// </summary>
    class CodeGenerator {
        private readonly DbHelper db;

        // 流水号重置类型，分别为按自然日、自然月、自然年        
        private enum SerialResetType {
            None = 0,
            Daily = 1,
            Monthly = 2,
            Yearly = 3
        }
        // 仅匹配类似{DATE:yyyyMMdd}格式文本
        private readonly Regex regexp = new Regex(@"(?<=\{(?!\{))([^:}]+)(?=:*[^}]*\}(?!\}))");
        // 占位符值
        private SortedList<string, object> placeHolders = new SortedList<string, object>();

        private CodeGenerator(DbHelper dbHelper) {
            db = dbHelper;
        }

        /// <summary>
        /// 将文本占位符替换为索引占位符
        /// 
        /// 如{DATE:yyyyMMdd}替换为{0:yyyyMMdd}
        /// </summary>
        /// <param name="match">文本占位符</param>
        /// <returns>索引占位符</returns>
        private string ReplaceText(Match match) {
            var placeHolder = match.Groups[1].Value;
            if(placeHolders.ContainsKey(placeHolder))
                return placeHolders.IndexOfKey(placeHolder).ToString(CultureInfo.InvariantCulture);
            throw new FormatException("未找到占位符" + placeHolder + "的替代值");
        }

        /// <summary>
        /// 根据编码规则生成新编号，该函数在单独的事务中完成。
        /// </summary>
        /// <param name="name">名称</param>
        /// <param name="parameters">附加参数</param>
        /// <returns>编号</returns>
        private string NewCode(string name, SortedList<string, object> parameters) {
            string result;

            using(var conn = db.CreateDbConnection()) {
                conn.Open();
                var id = "";
                var template = "";
                var resetType = 0;
                var commandTemplate = db.CreateDbCommand(string.Format("select Id, ResetType, Template from CodeTemplate where Name={0}Name and IsActived=1", db.ParamMark), conn, null);
                commandTemplate.Parameters.Add(db.CreateDbParameter("Name", name));
                var reader = commandTemplate.ExecuteReader();
                var hasValue = false;
                while(reader.Read()) {
                    if(hasValue) {
                        throw new Exception("存在多个名称为" + name + "的编码规则");
                    } else {
                        hasValue = true;
                    }
                    id = reader[0].ToString();
                    resetType = int.Parse(reader[1].ToString());
                    template = reader[2].ToString();
                }
                reader.Close();
                if(!hasValue)
                    throw new Exception("未找到名称为" + name + "的有效编码规则");
                DateTime createDate;
                switch(resetType) {
                    case (int)SerialResetType.None:
                        createDate = DateTime.MinValue;
                        break;
                    case (int)SerialResetType.Daily:
                        createDate = DateTime.Today;
                        break;
                    case (int)SerialResetType.Monthly:
                        createDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                        break;
                    case (int)SerialResetType.Yearly:
                        createDate = new DateTime(DateTime.Now.Year, 1, 1);
                        break;
                    default:
                        createDate = DateTime.MinValue;
                        break;
                }
                var corpCode = string.Empty;
                if(parameters.ContainsKey("CORPCODE") && parameters["CORPCODE"] != null)
                    corpCode = parameters["CORPCODE"].ToString();

                var command = db.CreateDbCommand("GetSerial", conn, null);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(db.CreateDbParameter("FTemplateId", id));
                command.Parameters.Add(db.CreateDbParameter("FCreateDate", createDate));
                command.Parameters.Add(db.CreateDbParameter("FCorpCode", corpCode));
                var param = db.CreateDbParameter("FSerial", 0);
#if SqlServer
                param.Direction = ParameterDirection.InputOutput;
#else
                param.Direction = ParameterDirection.ReturnValue;
#endif
                command.Parameters.Add(param);
                command.ExecuteNonQuery();
                var serial = param.Value;

                // 应用编码规则的模板
                placeHolders = parameters;
                placeHolders["DATE"] = DateTime.Now;
                placeHolders["SERIAL"] = serial;
                template = regexp.Replace(template, ReplaceText);
                result = string.Format(template, placeHolders.Values.ToArray());
            }
            return result;
        }

        /// <summary>
        /// 根据编码规则生成新编号，该函数在单独的事务中完成。
        /// 
        /// 编码规则模板可包含有多个格式为<c>{文本占位符:格式}</c>的元素。
        /// 其中占位符<c>DATE</c>表示当前服务端时间，<c>SERIAL</c>当前规则流水号。
        /// </summary>
        /// <example>
        /// <![CDATA[
        /// 例：某实体的编码规则为XS_{SALER}{DATE:yyyyMM}{SERIAL:000}，当前时间为2011-07-12 20:00:00，流水号为3。
        /// <code>
        /// // XS_HX20110712003
        /// CodeGenerator.Generate("SaleOrder", new SortedList<string, Object> { { "SALER", "HX" } });        
        /// </code>
        /// ]]>
        /// </example>
        /// <param name="dbHelper">用于获取编码规则数据库相关内容 </param>
        /// <param name="name">规则名称，用于从数据库中获取该名称的有效编码规则。</param>
        /// <param name="parameters">附加参数，用于替换模板中的对应文本占位符。</param>
        /// <returns>编号</returns>
        /// <exception cref="Exception">未找到该名称的有效编码规则。</exception>
        /// <exception cref="FormatException">未找到规则模板指定的占位符值。</exception>
        /// <exception cref="FormatException">规则模板格式有误，无法完成string.Format。</exception>
        private static string Generate(DbHelper dbHelper, string name, SortedList<string, Object> parameters) {
            return new CodeGenerator(dbHelper).NewCode(name, parameters);
        }

        /// <summary>
        /// 根据编码规则生成新编号，该函数在单独的事务中完成。
        ///         
        /// 编码规则模板可包含有多个格式为<c>{文本占位符:格式}</c>的元素。
        /// 其中占位符<c>DATE</c>表示当前服务端时间；
        /// <c>SERIAL</c>表示当前规则流水号；
        /// <c>CORPCODE</c>表示当前人员所属企业编号。
        /// </summary>
        /// <example>
        /// 例：某实体的编码规则为AS{DATE:yyyyMMdd}{SERIAL:000}，则返回值为AS20110712001。
        /// </example>
        /// <param name="dbHelper">用于获取编码规则数据库相关内容 </param>
        /// <param name="name">规则名称，用于从数据库中获取该名称的有效编码规则。</param>
        /// <returns>编号</returns>
        /// <exception cref="Exception">未找到该名称的有效编码规则。</exception>
        /// <exception cref="FormatException">未找到规则模板指定的占位符值。</exception>
        /// <exception cref="FormatException">规则模板格式有误，无法完成string.Format。</exception>
        public static string Generate(DbHelper dbHelper, string name) {
            return Generate(dbHelper, name, new SortedList<string, object>());
        }

        /// <summary>
        /// 根据编码规则生成新编号，该函数在单独的事务中完成。
        ///         
        /// 编码规则模板可包含有多个格式为<c>{文本占位符:格式}</c>的元素。
        /// 其中占位符<c>DATE</c>表示当前服务端时间；
        /// <c>SERIAL</c>表示当前规则流水号；
        /// <c>CORPCODE</c>表示当前人员所属企业编号。
        /// </summary>
        /// <example>
        /// <![CDATA[
        /// 例：某实体的编码规则为AS{DATE:yyyyMM}{SERIAL:000}_{CORPCODE}，当前时间为2011-07-12 20:00:00，流水号为3。
        /// <code>
        /// // AS20110712003_ES042
        /// CodeGenerator.Generate("AgencyService", "ES042");        
        /// </code>
        /// ]]>
        /// </example>///
        /// <param name="dbHelper">用于获取编码规则数据库相关内容 </param>
        /// <param name="name">规则名称，用于从数据库中获取该名称的有效编码规则。</param>
        /// <param name="corpCode">企业编号，当前人员所属企业编号，该参数影响流水号的取值</param>
        /// <returns>编号</returns>
        /// <exception cref="Exception">未找到该名称的有效编码规则。</exception>
        /// <exception cref="FormatException">未找到规则模板指定的占位符值。</exception>
        /// <exception cref="FormatException">规则模板格式有误，无法完成string.Format。</exception>
        public static string Generate(DbHelper dbHelper, string name, string corpCode) {
            return Generate(dbHelper, name, new SortedList<string, object> { { "CORPCODE", corpCode } });
        }
    }
}