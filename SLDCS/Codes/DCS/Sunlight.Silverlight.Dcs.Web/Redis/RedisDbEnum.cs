﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sunlight.Silverlight.Dcs.Web.Redis {
    /// <summary>
    /// 根据业务划分ReidsDB库
    /// Redis默认16个DB,可以通过配置文件调整数量
    /// </summary>
    public class RedisDbEnum {
        public enum DbEnum {
            维修单管理_分公司 = 0,
            保内维修单管理_服务站 = 1,
            保外维修单管理_服务站 = 2,
            维修索赔单管理分公司 = 3,
            维修索赔申请单管理_分公司 = 4,
            维修索赔申请单管理_服务站 = 5,
            配件出库单查询 = 6,
            配件入库检验单查询 = 7,
            配件采购订单管理 = 8,
            配件收货管理 = 9,
        }
    }
}
