using System;

namespace Sunlight.Silverlight.Dcs.Web.Reporting {
    public static class CommonConverter {
        public static string CmycurD(decimal num) {
            string str1 = "零壹贰叁肆伍陆柒捌玖";            //0-9所对应的汉字 
            string str2 = "万仟佰拾亿仟佰拾万仟佰拾元角分"; //数字位所对应的汉字 
            string str3 = "";    //从原num值中取出的值 
            string str4 = "";    //数字的字符串形式 
            string str5 = "";  //人民币大写金额形式 
            int i;    //循环变量 
            int j;    //num的值乘以100的字符串长度 
            string ch1 = "";    //数字的汉语读法 
            string ch2 = "";    //数字位的汉字读法 
            int nzero = 0;  //用来计算连续的零值是几个 
            int temp;            //从原num值中取出的值 

            //num = Math.Round(Math.Abs(num), 2);    //将num取绝对值并四舍五入取2位小数 
            str4 = ((long)(num * 100)).ToString();        //将num乘100并转换成字符串形式 
            j = str4.Length;      //找出最高位 
            if(j > 15) {
                return "溢出";
            }
            str2 = str2.Substring(15 - j);   //取出对应位数的str2的值。如：200.55,j为5所以str2=佰拾元角分 

            //循环取出每一位需要转换的值 
            for(i = 0; i < j; i++) {
                str3 = str4.Substring(i, 1);          //取出需转换的某一位的值 
                temp = Convert.ToInt32(str3);      //转换为数字 
                if(i != (j - 3) && i != (j - 7) && i != (j - 11) && i != (j - 15)) {
                    //当所取位数不为元、万、亿、万亿上的数字时 
                    if(str3 == "0") {
                        ch1 = "";
                        ch2 = "";
                        nzero = nzero + 1;
                    } else {
                        if(str3 != "0" && nzero != 0) {
                            ch1 = "零" + str1.Substring(temp * 1, 1);
                            ch2 = str2.Substring(i, 1);
                            nzero = 0;
                        } else {
                            ch1 = str1.Substring(temp * 1, 1);
                            ch2 = str2.Substring(i, 1);
                            nzero = 0;
                        }
                    }
                } else {
                    //该位是万亿，亿，万，元位等关键位 
                    if(str3 != "0" && nzero != 0) {
                        ch1 = "零" + str1.Substring(temp * 1, 1);
                        ch2 = str2.Substring(i, 1);
                        nzero = 0;
                    } else {
                        if(str3 != "0" && nzero == 0) {
                            ch1 = str1.Substring(temp * 1, 1);
                            ch2 = str2.Substring(i, 1);
                            nzero = 0;
                        } else {
                            if(str3 == "0" && nzero >= 3) {
                                ch1 = "";
                                ch2 = "";
                                nzero = nzero + 1;
                            } else {
                                if(j >= 11) {
                                    ch1 = "";
                                    nzero = nzero + 1;
                                } else {
                                    ch1 = "";
                                    ch2 = str2.Substring(i, 1);
                                    nzero = nzero + 1;
                                }
                            }
                        }
                    }
                }
                if(i == (j - 11) || i == (j - 3)) {
                    //如果该位是亿位或元位，则必须写上 
                    ch2 = str2.Substring(i, 1);
                }
                str5 = str5 + ch1 + ch2;

                if(i == j - 1 && str3 == "0") {
                    //最后一位（分）为0时，加上“整” 
                    str5 = str5 + '整';
                }
            }
            if(num == 0) {
                str5 = "零元整";
            }
            return str5;
        }

        /// <summary> 
        /// 一个重载，将字符串先转换成数字在调用CmycurD(decimal num) 
        /// </summary> 
        /// <param name="numstr">用户输入的金额，字符串形式未转成decimal</param> 
        /// <returns></returns> 
        public static string CmycurD(object numstr) {
            try {
                var num = Convert.ToDecimal(numstr);
                return CmycurD(num);
            } catch {
                return "非数字形式！";
            }
        }

        public static string NullStringConverterFormat(object value) {
            if(value == null)
                return "";
            var result = value.ToString();
            return result;
        }

        public static string GetAccountAvailabilityConvertFormat(object accountAvailability) {
            if(accountAvailability == null)
                return null;
            string result;
            switch((int)accountAvailability) {
                case 1:
                    result = "可用"; break;
                case 2:
                    result = "不可用"; break;
                default:
                    result = ""; break;
            }
            return result;
        }

        public static string GetBoolConvertFormat(bool value) {
            return value ? "是" : "否";
        }


        public static string TotalRecordsConverterFormat(object value) {
            return string.Format("总计 {0} 条记录", (int)value);
        }

        public static string CurrentPagesConverterFormat(object value) {
            return string.Format("第 {0} 页", (int)value);
        }

        public static string ServiceRateConverterFormat(object zCnum, object saleNum) {
            string result = null;
            if(zCnum != null && saleNum != null) {
                var dZCnum = double.Parse(zCnum.ToString());
                var dSaleNum = double.Parse(saleNum.ToString());
                if(dSaleNum != 0) {
                    result = (dZCnum / dSaleNum).ToString("P2");
                }
            }
            return result;
        }

        public static string DateTimeConverterFormat(object value) {
            var dt = (DateTime)value;
            return dt.GetDateTimeFormats('D')[0];
        }

        /// <summary>
        /// 时间格式用如：2013/10/18 显示
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string DateTimeConverterFormat1(object value) {
            if(value == null) {
                return null;
            } else {
                var dt = (DateTime)value;
                return dt.GetDateTimeFormats('d')[0];
            }
        }
        public static string ConverterEnumToString(string name, int key) {
            var domainService = new DcsDomainService();
            var keyValueItem = domainService.GetKeyValueItemByKey(name, key);
            return keyValueItem == null ? null : keyValueItem.Value;
        }

        public static string PartsInboundCheckBillInboundTypeFormat(int key) {
            var domainService = new DcsDomainService();
            var keyValueItem = domainService.GetKeyValueItemByKey("Parts_InboundType", key);
            return keyValueItem == null ? null : keyValueItem.Value;
        }

        public static string PartsShippingOrderOutboundTypeFormat(int key) {
            var domainService = new DcsDomainService();
            var keyValueItem = domainService.GetKeyValueItemByKey("Parts_OutboundType", key);
            return keyValueItem == null ? null : keyValueItem.Value;
        }

        public static string PartsInventoryBillWarehouseAreaCategoryFormat(int key) {
            var domainService = new DcsDomainService();
            var keyValueItem = domainService.GetKeyValueItemByKey("Area_Category", key);
            return keyValueItem == null ? null : keyValueItem.Value;
        }
        //车辆类型
        public static string RepairOrderManagementVehicleTypeFormat(int key) {
            var domainService = new DcsDomainService();
            var keyValueItem = domainService.GetKeyValueItemByKey("VehicleInformation_VehicleType", key);
            return keyValueItem == null ? null : keyValueItem.Value;
        }

        //维修类型
        public static string RepairOrderManagementRepairTpyeFormat(int key) {
            var domainService = new DcsDomainService();
            var keyValueItem = domainService.GetKeyValueItemByKey("Repair_Tpye", key);
            return keyValueItem == null ? null : keyValueItem.Value;
        }

        //维修单结算属性
        public static string RepairOrderManagementRepairSettleAttributeFormat(object key) {
            string result = string.Empty;
            if(key == null) {
                return result;
            } else {
                var domainService = new DcsDomainService();
                var keyValueItem = domainService.GetKeyValueItemByKey("RepairSettle_Attribute", Convert.ToInt32(key));
                return keyValueItem == null ? null : keyValueItem.Value;
            }
        }

        //故障件索赔类型
        public static string RepairOrderManagementFaultClaimTypeFormat(int key) {
            var domainService = new DcsDomainService();
            var keyValueItem = domainService.GetKeyValueItemByKey("FaultClaim_Type", key);
            return keyValueItem == null ? null : keyValueItem.Value;
        }
        //旧件入库类型
        public static string UsedPartsInboundOrderQueryInBoundTypeFormat(int key) {
            var domainService = new DcsDomainService();
            var keyValueItem = domainService.GetKeyValueItemByKey("UsedPartsInboundOrder_InboundType", key);
            return keyValueItem == null ? null : keyValueItem.Value;
        }
        //旧件清退类型
        public static string UsedPartsReturnOrderReturnTypeFormat(int key) {
            var domainService = new DcsDomainService();
            var keyValueItem = domainService.GetKeyValueItemByKey("UsedPartsReturnOrder_ReturnType", key);
            return keyValueItem == null ? null : keyValueItem.Value;
        }
        //将索赔转换成保修索赔 将自费转换成客户自费
        public static string GetAccountAvailabilityRepairSettleAttributeFormat(object accountAvailability) {
            if(accountAvailability == null)
                return null;
            string result;
            switch((string)accountAvailability) {
                case "索赔":
                    result = "保修索赔"; break;
                case "自费":
                    result = "客户自费"; break;
                default:
                    result = ""; break;
            }
            return result;
        }

        //实收工时费
        public static string GetActualLaborCost(object accountAvailability, object actualLaborCost) {
            if(accountAvailability == null)
                return null;
            if(actualLaborCost == null)
                return null;
            double result;
            switch((string)accountAvailability) {
                case "索赔":
                    result = 0; break;
                case "自费":
                    result = Convert.ToDouble(actualLaborCost.ToString()); break;
                default:
                    result = 0; break;
            }
            return result.ToString();
        }

        //如果只为空转换为0
        public static string GetTransition(object result) {
            int result1;
            if(result == null) {
                result1 = 0;
            } else {
                result1 = Convert.ToInt32(result.ToString());
            }
            return result1.ToString();
        }

        //获取固定的日期格式(配件标签打印使用)
        public static String GetSolidDate(int currentYear) {
            const int yaerValue = 2013; // 标志年份 指定2013年从M开始
            int varianCemetric = 0; // 系统年份和传值的年份的差值
            int remainder; // 去大于26的余数 取摸%
            var number = new String[] { "M", "N", "O", "P", "Q", "R", "S",
				"T", "U", "V", "W", "X", "Y", "Z", "A", "B", "C", "D", "E",
				"F", "G", "H", "I", "J", "K", "L" }; // 定义要替换的字母数组
            String returnyear = null; // 返回值
            int year = DateTime.Now.Year;
            if(currentYear >= 2013) { // 区分传过来的参数是否是正确
                if(currentYear == year) {// 如果系统年份和传参年份相等
                    varianCemetric = year - yaerValue; // 传值的年份相等系统年份 之间系统年份减掉指定的年份
                    // 这样获得差值就是取字母下标
                    if(varianCemetric >= 26) { // 如果差值超过了25 需要重新开始取字母
                        remainder = varianCemetric % 26; // 取差值的% 余数
                        returnyear = number[remainder]; // 取字母
                    } else { //如果在26个字母范围内可以直接取
                        returnyear = number[varianCemetric]; //直接取字母替换掉年份
                    }
                } else {// 传参数和当前系统的年份不相等
                    varianCemetric = currentYear - yaerValue;  //因为参数已经做过过滤  大于1900年  所以直接传参直接减即可
                    if(varianCemetric >= 26) {
                        remainder = varianCemetric % 26;
                        returnyear = number[remainder];
                    } else {
                        returnyear = number[varianCemetric];
                    }
                }

            } else {  //传的参数小于指定的年份  不能直接用传的年份  转化为系统年份
                varianCemetric = year - yaerValue;
                if(varianCemetric >= 26) {
                    remainder = varianCemetric % 26;
                    returnyear = number[remainder];
                } else {
                    returnyear = number[varianCemetric];
                }
            }
            return returnyear + DateTime.Now.ToString("MMdd") + "0000";
        }

        //获取固定的日期格式(供应商配件标签打印使用)
        public static String GetSolidDateForSupplierPrintLabel(int currentYear) {
            const int yaerValue = 2011; // 标志年份 指定2011年从A开始
            int varianCemetric = 0; // 系统年份和传值的年份的差值
            int remainder; // 去大于20的余数 取摸%
            var number = new String[] { "A", "B", "C", "D", "E",
				"F", "G", "H", "I", "J", "K", "L","M", "N", "O", "P", "Q", "R", "S",
				"T" }; // 定义要替换的字母数组
            String returnyear = null; // 年份返回值
            int year = DateTime.Now.Year;
            if(currentYear >= 2011) { // 区分传过来的参数是否是正确
                if(currentYear == year) {// 如果系统年份和传参年份相等
                    varianCemetric = year - yaerValue; // 传值的年份相等系统年份 之间系统年份减掉指定的年份
                    // 这样获得差值就是取字母下标
                    if(varianCemetric >= 20) { // 如果差值超过了20 需要重新开始取字母
                        remainder = varianCemetric % 20; // 取差值的% 余数
                        returnyear = number[remainder]; // 取字母
                    } else { //如果在20个字母范围内可以直接取
                        returnyear = number[varianCemetric]; //直接取字母替换掉年份
                    }
                } else {// 传参数和当前系统的年份不相等
                    varianCemetric = currentYear - yaerValue;  //因为参数已经做过过滤  大于1900年  所以直接传参直接减即可
                    if(varianCemetric >= 20) {
                        remainder = varianCemetric % 20;
                        returnyear = number[remainder];
                    } else {
                        returnyear = number[varianCemetric];
                    }
                }
            }
            int monthValue = DateTime.Now.Month;//获取当前月份
            string returnMonth = "";//月份返回值
            switch(monthValue) {
                case 1:
                    returnMonth = "A";
                    break;
                case 2:
                    returnMonth = "B";
                    break;
                case 3:
                    returnMonth = "C";
                    break;
                case 4:
                    returnMonth = "D";
                    break;
                case 5:
                    returnMonth = "E";
                    break;
                case 6:
                    returnMonth = "F";
                    break;
                case 7:
                    returnMonth = "G";
                    break;
                case 8:
                    returnMonth = "H";
                    break;
                case 9:
                    returnMonth = "I";
                    break;
                case 10:
                    returnMonth = "J";
                    break;
                case 11:
                    returnMonth = "K";
                    break;
                case 12:
                    returnMonth = "L";
                    break;
            }
            return "P" + returnyear + returnMonth + DateTime.Now.ToString("dd");
        }


        //发运方式
        public static string UsedPartsShippingOrderTypeMethodFormat(object key) {
            string result = string.Empty;
            if(key == null) {
                return result;
            } else {
                var domainService = new DcsDomainService();
                var keyValueItem = domainService.GetKeyValueItemByKey("PartsShipping_Method", Convert.ToInt32(key));
                return keyValueItem == null ? null : keyValueItem.Value;
            }
        }
        //临时发运单发运方式
        public static string TemSupplierShippingOrderTypeMethodFormat(object key) {
            string result = string.Empty;
            if(key == null) {
                return result;
            } else {
                var domainService = new DcsDomainService();
                var keyValueItem = domainService.GetKeyValueItemByKey("TemPurchasePlanOrderShippingMethod", Convert.ToInt32(key));
                return keyValueItem == null ? null : keyValueItem.Value;
            }
        }
        //发运类型
        public static string UsedPartsShippingOrderTypeTypeFormat(object key) {
            string result = string.Empty;
            if(key == null) {
                return result;
            } else {
                var domainService = new DcsDomainService();
                var keyValueItem = domainService.GetKeyValueItemByKey("PartsShippingOrder_Type", Convert.ToInt32(key));
                return keyValueItem == null ? null : keyValueItem.Value;
            }
        }
        //客户是否承担外出差旅费
        public static string GetOutClaimSupplierName(object outClaimSupplierName) {
            string result;
            if(outClaimSupplierName == null) {
                result = "是";
            } else {
                result = "否";
            }
            return result;
        }

        //外采原因
        public static string GetOuterPurchaseCommentNameFormat(int key) {
            var domainService = new DcsDomainService();
            var keyValueItem = domainService.GetKeyValueItemByKey("PartsOuterPurchase_OuterPurchaseComment", key);
            return keyValueItem == null ? null : keyValueItem.Value;
        }

        //实收外出服务费
        public static string GetServiceCharge(object outClaimSupplierName, object FieldServiceCharge) {
            int result;
            if(outClaimSupplierName == null) {
                result = Convert.ToInt32(FieldServiceCharge.ToString());
            } else {
                result = 0;
            }
            return result.ToString();
        }

        //结算单实收费用合计
        public static string GetTotolAmount(object Number1, object Number2, object Number3, object outClaimSupplierName) {
            double result;
            if(Number1 == null && Number2 == null && Number3 == null && outClaimSupplierName == null) {
                result = 0;
            } else if(outClaimSupplierName != null) {
                if(Number1 == null) {
                    result = Convert.ToDouble(Number2.ToString());
                } else if(Number2 == null) {
                    result = Convert.ToDouble(Number1.ToString());
                } else {
                    result = Convert.ToDouble(Number1.ToString()) + Convert.ToDouble(Number2.ToString());
                }
            } else if(outClaimSupplierName == null && Number1 == null && Number2 != null && Number3 != null) {
                result = Convert.ToDouble(Number2.ToString()) + Convert.ToDouble(Number3.ToString());
            } else if(Number2 == null && outClaimSupplierName == null && Number1 != null && Number3 != null) {
                result = Convert.ToDouble(Number1.ToString()) + Convert.ToDouble(Number3.ToString());
            } else if(Number3 == null && outClaimSupplierName == null && Number1 != null && Number2 != null) {
                result = Convert.ToDouble(Number1.ToString()) + Convert.ToDouble(Number2.ToString());
            } else if(Number1 == null && Number2 == null && outClaimSupplierName == null && Number3 != null) {
                result = Convert.ToDouble(Number3.ToString());
            } else if(Number1 == null && Number3 == null && Number2 != null && outClaimSupplierName == null) {
                result = Convert.ToDouble(Number2.ToString());
            } else if(Number2 == null && Number3 == null && outClaimSupplierName == null && Number1 != null) {
                result = Convert.ToDouble(Number1.ToString());
            } else {
                result = Convert.ToDouble(Number1.ToString()) + Convert.ToDouble(Number2.ToString()) + Convert.ToDouble(Number3.ToString());
            }
            return result.ToString();
        }

        //是否配件销售订单
        public static string GetIsOrNotPartsSalesOrder(int OriginalRequirementBillType, object PartsSalesOrderTypeName) {
            string partsSalesOrderTypeName = string.Empty;
            if(OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件销售订单) {
                if(PartsSalesOrderTypeName == null) {
                    return partsSalesOrderTypeName;
                } else {
                    return PartsSalesOrderTypeName.ToString();
                }
            } else {
                if(PartsSalesOrderTypeName == null) {
                    return partsSalesOrderTypeName;
                } else {
                    return partsSalesOrderTypeName;
                }
            }
        }

        //根据配件原始销售订单类型=配件销售订单 获取配件销售订单编号
        public static string GetIsOrNotOriginalRequirementBillCode(int OriginalRequirementBillType, object OriginalRequirementBillCode) {
            string originalRequirementBillCode = string.Empty;
            if(OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件销售订单) {
                if(OriginalRequirementBillCode == null) {
                    return originalRequirementBillCode;
                } else {
                    return OriginalRequirementBillCode.ToString();
                }
            } else {
                if(OriginalRequirementBillCode == null) {
                    return originalRequirementBillCode;
                } else {
                    return originalRequirementBillCode;
                }
            }
        }
		 public static string TracePropertyFormat(int key) {
            var domainService = new DcsDomainService();
            var keyValueItem = domainService.GetKeyValueItemByKey("TraceProperty", key);
            return keyValueItem == null ? null : keyValueItem.Value;
        }
         public static string TemPurchaseOrderOrderTypeFormat(int key) {
             var domainService = new DcsDomainService();
             var keyValueItem = domainService.GetKeyValueItemByKey("TemPurchasePlanOrderPlanType", key);
             return keyValueItem == null ? null : keyValueItem.Value;
         }
         public static string TemPurchaseOrderShippingMethodFormat(int key) {
             var domainService = new DcsDomainService();
             var keyValueItem = domainService.GetKeyValueItemByKey("TemPurchasePlanOrderShippingMethod", key);
             return keyValueItem == null ? null : keyValueItem.Value;
         }
         //移库类型
         public static string GetPartsShiftOrderTypeFormat(int key) {
             var domainService = new DcsDomainService();
             var keyValueItem = domainService.GetKeyValueItemByKey("PartsShiftOrder_Type", key);
             return keyValueItem == null ? null : keyValueItem.Value;
         }
    }
}

