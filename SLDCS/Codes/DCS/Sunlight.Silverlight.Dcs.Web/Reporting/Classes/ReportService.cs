using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using System.Text.RegularExpressions;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Entities;
using Sunlight.Silverlight.Shell.Web;
using Sunlight.Silverlight.Web;
using System.Globalization;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public KeyValueItem GetKeyValueItemByKey(string name, int key) {
            return ObjectContext.KeyValueItems.FirstOrDefault(e => e.Name == name && e.Key == key);
        }

        public string ConverterEnumToString(string name, int key) {
            var keyValueItem = ObjectContext.KeyValueItems.FirstOrDefault(e => e.Name == name && e.Key == key);
            return keyValueItem == null ? null : keyValueItem.Value;
        }

        public PartsInboundCheckBill GetPartsInboundCheckBillByIdForPrint(int PartsInboundCheckBillId) {
            var partsInboundCheckBill = ObjectContext.PartsInboundCheckBills.SingleOrDefault(e => e.Id == PartsInboundCheckBillId);
            if(partsInboundCheckBill != null && partsInboundCheckBill.OriginalRequirementBillType == 4) {
                var partsPurchaseOrder = ObjectContext.PartsPurchaseOrders.SingleOrDefault(t => t.Id == partsInboundCheckBill.OriginalRequirementBillId);
                var partsPurchaseOrderType = ObjectContext.PartsPurchaseOrderTypes.SingleOrDefault(r => r.Id == partsPurchaseOrder.PartsPurchaseOrderTypeId);
                if(partsPurchaseOrderType != null)
                    partsInboundCheckBill.PartsPurchaseOrderType = partsPurchaseOrderType.Name;
            }
            return partsInboundCheckBill;
        }

        public IEnumerable<PartsInboundCheckBillDetail> GetPartsInboundCheckBillDetailByIdForPrint(int PartsInboundCheckBillId) {
            var PartsInboundCheckBill = ObjectContext.PartsInboundCheckBills.SingleOrDefault(r => r.Id == PartsInboundCheckBillId);
            var partsInboundCheckBillDetails = ObjectContext.PartsInboundCheckBillDetails.Where(r => r.PartsInboundCheckBillId == PartsInboundCheckBillId).ToArray();
            var sparePartIds = partsInboundCheckBillDetails.Select(r => r.SparePartId).ToArray();
            var partsStockStruct = from a in ObjectContext.PartsStocks.Where(r => r.WarehouseId == PartsInboundCheckBill.WarehouseId && sparePartIds.Any(v => v == r.PartId))
                                   join b in ObjectContext.WarehouseAreas.Where(r => r.AreaKind == (int)DcsAreaKind.库位) on a.WarehouseAreaId equals b.Id
                                   join c in ObjectContext.WarehouseAreaCategories.Where(r => r.Category == (int)DcsAreaType.保管区) on b.AreaCategoryId equals c.Id
                                   select new {
                                       a.PartId,
                                       b.Code
                                   };
            var structPart = partsStockStruct.GroupBy(r => new {
                r.PartId
            }).Select(r => new {
                partId = r.Key.PartId,
                areacode = r.Min(v => v.Code)
            }).ToArray();
            var spareparts = ObjectContext.SpareParts.Where(r => sparePartIds.Any(v => v == r.Id)).ToArray();
            foreach(var item in partsInboundCheckBillDetails) {
                var temp = spareparts.FirstOrDefault(r => r.Id == item.SparePartId);
                item.Unit = temp == null ? "" : temp.MeasureUnit;

            }
            foreach(var item in partsInboundCheckBillDetails) {
                var temp = structPart.FirstOrDefault(r => r.partId == item.SparePartId);
                item.DestWarehouseAreaCode = temp == null ? "" : temp.areacode;

            }
            return partsInboundCheckBillDetails.OrderBy(r => r.SparePartId);

        }

        //入库检验单打印工程车
        public PartsInboundCheckBill GetPartsInboundCheckBillByIdForGcForPrint(int partsInboundCheckBillId) {
            var partsInboundCheckBill = ObjectContext.PartsInboundCheckBills.Single(e => e.Id == partsInboundCheckBillId);
            var partsInboundCheckBillDetails = ObjectContext.PartsInboundCheckBillDetails.Where(r => r.PartsInboundCheckBillId == partsInboundCheckBillId).ToArray();
            var sparePartIds = partsInboundCheckBillDetails.Select(r => r.SparePartId).ToArray();
            var partsStockStruct = from a in ObjectContext.PartsStocks.Where(r => r.WarehouseId == partsInboundCheckBill.WarehouseId && sparePartIds.Any(v => v == r.PartId))
                                   join b in ObjectContext.WarehouseAreas.Where(r => r.AreaKind == (int)DcsAreaKind.库位) on a.WarehouseAreaId equals b.Id
                                   join c in ObjectContext.WarehouseAreaCategories.Where(r => r.Category == (int)DcsAreaType.保管区) on b.AreaCategoryId equals c.Id
                                   join d in ObjectContext.WarehouseAreas.Where(r => r.AreaKind == (int)DcsAreaKind.库区) on b.ParentId equals d.Id
                                   select new {
                                       a.PartId,
                                       d.Code
                                   };
            var structPart = partsStockStruct.GroupBy(r => new {
                r.PartId
            }).Select(r => new {
                partId = r.Key.PartId,
                areacode = r.Min(v => v.Code)
            }).ToArray();
            foreach(var item in partsInboundCheckBillDetails) {
                var temp = structPart.FirstOrDefault(r => r.partId == item.SparePartId);
                partsInboundCheckBill.WarehouseAreaCode = temp == null ? "" : temp.areacode;
            }
            switch(partsInboundCheckBill.OriginalRequirementBillType) {
                case (int)DcsOriginalRequirementBillType.配件采购订单:
                    var temp = ObjectContext.PartsPurchaseOrders.FirstOrDefault(r => r.Id == partsInboundCheckBill.OriginalRequirementBillId);
                    partsInboundCheckBill.PrintApproverName = temp == null ? "" : temp.CreatorName;
                    break;
                case (int)DcsOriginalRequirementBillType.配件销售退货单:
                    var temp2 = ObjectContext.PartsSalesReturnBills.FirstOrDefault(r => r.Id == partsInboundCheckBill.OriginalRequirementBillId);
                    partsInboundCheckBill.PrintApproverName = temp2 == null ? "" : temp2.ApproverName;
                    break;
                case (int)DcsOriginalRequirementBillType.内部领入单:
                    var temp3 = ObjectContext.InternalAcquisitionBills.FirstOrDefault(r => r.Id == partsInboundCheckBill.OriginalRequirementBillId);
                    partsInboundCheckBill.PrintApproverName = temp3 == null ? "" : temp3.ApproverName;
                    break;
                case (int)DcsOriginalRequirementBillType.配件零售退货单:
                    var temp4 = ObjectContext.PartsRetailReturnBills.FirstOrDefault(r => r.Id == partsInboundCheckBill.OriginalRequirementBillId);
                    partsInboundCheckBill.PrintApproverName = temp4 == null ? "" : temp4.ApproverName;
                    break;
                case (int)DcsOriginalRequirementBillType.配件调拨单:
                    var temp5 = ObjectContext.PartsTransferOrders.FirstOrDefault(r => r.Id == partsInboundCheckBill.OriginalRequirementBillId);
                    partsInboundCheckBill.PrintApproverName = temp5 == null ? "" : temp5.ApproverName;
                    break;
                case (int)DcsOriginalRequirementBillType.积压件调剂单:
                    var temp6 = ObjectContext.OverstockPartsAdjustBills.FirstOrDefault(r => r.Id == partsInboundCheckBill.OriginalRequirementBillId);
                    partsInboundCheckBill.PrintApproverName = temp6 == null ? "" : temp6.CreatorName;
                    break;
                case (int)DcsOriginalRequirementBillType.配件外采申请单:
                    var temp7 = ObjectContext.PartsOuterPurchaseChanges.FirstOrDefault(r => r.Id == partsInboundCheckBill.OriginalRequirementBillId);
                    partsInboundCheckBill.PrintApproverName = temp7 == null ? "" : temp7.ApproverName;
                    break;
                case (int)DcsOriginalRequirementBillType.配件索赔单:
                    var temp8 = ObjectContext.PartsClaimOrderNews.FirstOrDefault(r => r.Id == partsInboundCheckBill.OriginalRequirementBillId);
                    partsInboundCheckBill.PrintApproverName = temp8 == null ? "" : temp8.ApproverName;
                    break;
                case (int)DcsOriginalRequirementBillType.配件销售订单:
                    var temp9 = ObjectContext.PartsSalesOrders.FirstOrDefault(r => r.Id == partsInboundCheckBill.OriginalRequirementBillId);
                    partsInboundCheckBill.PrintApproverName = temp9 == null ? "" : temp9.ApproverName;
                    break;

            }
            return partsInboundCheckBill;
        }

        //配件采购结算单查询出入库清单
        public IQueryable<VirtualInAndOutBoundDetail> GetVirtualInAndOutBoundDetailByPurchaseSettleBillId(int partsPurchaseSettleBillId) {
            var partsPurchaseSettle = ObjectContext.PartsPurchaseSettleBills.FirstOrDefault(r => r.Id == partsPurchaseSettleBillId);
            var partsPurchaseSettleRefs = ObjectContext.PartsPurchaseSettleRefs.Where(r => r.PartsPurchaseSettleBillId == partsPurchaseSettleBillId);
            var result1 = from partsPurchaseSettleRef in partsPurchaseSettleRefs
                          from partsInboundCheckBillDetail in ObjectContext.PartsInboundCheckBillDetails
                          join partsSupplierRelation in ObjectContext.PartsSupplierRelations.Where(r => r.Status != (int)DcsBaseDataStatus.作废) on new {
                              partsInboundCheckBillDetail.SparePartId,
                              partsPurchaseSettle.PartsSupplierId
                          } equals new { 
                           SparePartId =  partsSupplierRelation.PartId,
                            PartsSupplierId = partsSupplierRelation.SupplierId
                          } into temp
                          from t in temp.DefaultIfEmpty()
                          where partsPurchaseSettleRef.SourceType == (int)DcsPartsPurchaseSettleRefSourceType.配件入库检验单 && partsPurchaseSettleRef.SourceId == partsInboundCheckBillDetail.PartsInboundCheckBillId && partsInboundCheckBillDetail.InspectedQuantity != 0
                          select new VirtualInAndOutBoundDetail {
                              Code = partsInboundCheckBillDetail.PartsInboundCheckBill.Code,
                              PartCode = partsInboundCheckBillDetail.SparePartCode,
                              PartName = partsInboundCheckBillDetail.SparePartName,
                              Quantity = partsInboundCheckBillDetail.InspectedQuantity,
                              CostPrice = partsInboundCheckBillDetail.CostPrice,
                              SettlementPrice = partsInboundCheckBillDetail.SettlementPrice,
                              SupplierPartCode = t.SupplierPartCode
                          };
            var result2 = from partsPurchaseSettleRef in partsPurchaseSettleRefs
                          from partsOutboundBillDetail in ObjectContext.PartsOutboundBillDetails
                          join partsSupplierRelation in ObjectContext.PartsSupplierRelations.Where(r => r.Status != (int)DcsBaseDataStatus.作废) on new {
                              partsOutboundBillDetail.SparePartId,
                              partsPurchaseSettle.PartsSupplierId
                          } equals new { 
                           SparePartId =  partsSupplierRelation.PartId,
                            PartsSupplierId = partsSupplierRelation.SupplierId
                          } into temp
                          from t in temp.DefaultIfEmpty()
                          where partsPurchaseSettleRef.SourceId == partsOutboundBillDetail.PartsOutboundBillId && partsPurchaseSettleRef.SourceType == (int)DcsPartsPurchaseSettleRefSourceType.配件出库单 && partsOutboundBillDetail.OutboundAmount != 0
                          select new VirtualInAndOutBoundDetail {
                              Code = partsOutboundBillDetail.PartsOutboundBill.Code,
                              PartCode = partsOutboundBillDetail.SparePartCode,
                              PartName = partsOutboundBillDetail.SparePartName,
                              Quantity = partsOutboundBillDetail.OutboundAmount * -1,
                              CostPrice = partsOutboundBillDetail.CostPrice,
                              SettlementPrice = partsOutboundBillDetail.SettlementPrice,
                              SupplierPartCode = t.SupplierPartCode
                          };
            return result1.Union(result2);
        }

        public IQueryable<PartsOutboundBill> GetPartsOutboundBillByIdForPrint(int PartsOutboundBillId) {
            var userInfo = Utils.GetCurrentUserInfo();
            var bill = ObjectContext.PartsOutboundBills.Where(e => e.Id == PartsOutboundBillId).FirstOrDefault();
            if(bill.StorageCompanyType == (int)DcsCompanyType.分公司) {
                bill.AgencyTitle = "出库单";
            } else {
                bill.AgencyTitle = "中心库出库单";
            }
            return ObjectContext.PartsOutboundBills.Where(e => e.Id == PartsOutboundBillId);
        }

        public IQueryable<AgencyPartsOutboundBill> GetAgencyPartsOutboundBillByIdForPrint(int PartsOutboundBillId) {
            return ObjectContext.AgencyPartsOutboundBills.Where(e => e.Id == PartsOutboundBillId);
        }


        public IQueryable<PartsOutboundBillDetail> GetPartsOutboundBillDetailByIdForPrint(int PartsOutboundBillId) {
            return ObjectContext.PartsOutboundBillDetails.Where(e => e.PartsOutboundBillId == PartsOutboundBillId).Include("SparePart");
        }

        public IQueryable<APartsOutboundBillDetail> GetAPartsOutboundBillDetailByIdForPrint(int PartsOutboundBillId) {
            return ObjectContext.APartsOutboundBillDetails.Where(e => e.PartsOutboundBillId == PartsOutboundBillId).Include("SparePart");
        }

        //工程车打印出库单
        public IQueryable<VirtualPartsOutBillAndDetail> 查询工程车打印出库单(int partsOutboundBillId) {
            var outBillAndDetailquery = from a in ObjectContext.PartsOutboundBills
                                        where a.Id == partsOutboundBillId
                                        join b in ObjectContext.PartsOutboundBillDetails on a.Id equals b.PartsOutboundBillId
                                        join c in ObjectContext.SpareParts on b.SparePartId equals c.Id
                                        join d in ObjectContext.WarehouseAreas on b.WarehouseAreaId equals d.Id
                                        join e in ObjectContext.WarehouseAreas on d.ParentId equals e.Id into temp
                                        from tt in temp.DefaultIfEmpty()
                                        select new VirtualPartsOutBillAndDetail {
                                            Id = b.Id,
                                            PartsOutboundBillId = a.Id,
                                            ShippingMethod = a.ShippingMethod,
                                            ReceivingCompanyName = a.ReceivingCompanyName,
                                            StorageCompanyName = a.StorageCompanyName,
                                            OriginalRequirementBillCode = a.OriginalRequirementBillCode,
                                            OriginalRequirementBillType = a.OriginalRequirementBillType,
                                            OrderApproveComment = a.OrderApproveComment,
                                            OutboundType = a.OutboundType,
                                            Code = a.Code,
                                            CreatorName = a.CreatorName,
                                            WarehouseName = a.WarehouseName,
                                            CreateTime = a.CreateTime,
                                            PartsSalesOrderTypeName = a.PartsSalesOrderTypeName,
                                            WarehouseArea = tt.Code,
                                            BranchName = a.BranchName,
                                            Remark = a.Remark,
                                            WarehouseAreaCode = b.WarehouseAreaCode,
                                            SparePartCode = b.SparePartCode,
                                            SparePartName = b.SparePartName,
                                            MeasureUnit = c.MeasureUnit,
                                            BatchNumber = b.BatchNumber,
                                            OutboundAmount = b.OutboundAmount,
                                            SettlementPrice = b.SettlementPrice
                                        };
            return outBillAndDetailquery.OrderBy(r => r.Id);
        }

        public IEnumerable<VirtualPartsInboundPlanDetail> 查询配件入库分区打印(int partsInboundPlanId) {
            var partsPurchaseOrderType = string.Empty; //订单类型
            var partsInboundPlans = ObjectContext.PartsInboundPlans.SingleOrDefault(r => r.Id == partsInboundPlanId);
            if(partsInboundPlans.OriginalRequirementBillType == 4) {
                var partsPurchaseOrder = ObjectContext.PartsPurchaseOrders.SingleOrDefault(r => r.Id == partsInboundPlans.OriginalRequirementBillId);
                var type = ObjectContext.PartsPurchaseOrderTypes.SingleOrDefault(r => r.Id == partsPurchaseOrder.PartsPurchaseOrderTypeId);
                if(type != null)
                    partsPurchaseOrderType = type.Name;
            } else {
                partsPurchaseOrderType = Enum.GetName(typeof(DcsPartsInboundType), partsInboundPlans.InboundType);
            }


            var partsInboundPlanDetails = ObjectContext.PartsInboundPlanDetails.Where(r => r.PartsInboundPlanId == partsInboundPlanId).ToArray();
            var sparePartIds = partsInboundPlanDetails.Select(r => r.SparePartId).ToArray();
            var partsStockStruct = from a in ObjectContext.PartsStocks.Where(r => r.WarehouseId == partsInboundPlans.WarehouseId && sparePartIds.Any(v => v == r.PartId))
                                   join b in ObjectContext.WarehouseAreas.Where(r => r.AreaKind == (int)DcsAreaKind.库位) on a.WarehouseAreaId equals b.Id
                                   join c in ObjectContext.WarehouseAreaCategories.Where(r => r.Category == (int)DcsAreaType.保管区) on b.AreaCategoryId equals c.Id
                                   select new {
                                       a.PartId,
                                       b.Code,
                                       b.TopLevelWarehouseAreaId
                                   };
            var structPart = partsStockStruct.GroupBy(r => new {
                r.PartId,
                r.TopLevelWarehouseAreaId
            }).Select(r => new {
                partId = r.Key.PartId,
                TopLevelWarehouseAreaId = r.Key.TopLevelWarehouseAreaId,
                areacode = r.Min(v => v.Code)
            }).ToArray();
            var partsInboundPlanDetail = from a in ObjectContext.PartsInboundPlans
                                         where a.Id == partsInboundPlanId
                                         join b in ObjectContext.PartsInboundPlanDetails on a.Id equals b.PartsInboundPlanId
                                         join c in ObjectContext.PartsSalesCategories on a.PartsSalesCategoryId equals c.Id into temp
                                         from tt in temp.DefaultIfEmpty()
                                         select new VirtualPartsInboundPlanDetail {
                                             Id = b.Id,
                                             Code = a.Code,
                                             BranchName = a.BranchName,
                                             CounterpartCompanyName = a.CounterpartCompanyName,
                                             InboundType = a.InboundType,
                                             CreateTime = a.CreateTime,
                                             CreatorName = a.CreatorName,
                                             PartsPurchaseOrderType = partsPurchaseOrderType,
                                             PartsSalesCategoryName = tt.Name,
                                             StorageCompanyName = a.StorageCompanyName,
                                             SparePartCode = b.SparePartCode,
                                             SparePartName = b.SparePartName,
                                             InspectedQuantity = b.InspectedQuantity,
                                             Price = b.Price,
                                             SparePartId = b.SparePartId,
                                             Remark = b.Remark,
                                             PlannedAmount = b.PlannedAmount
                                         };
            var details = partsInboundPlanDetail.ToArray();
            foreach(var item in details) {
                var temp = structPart.FirstOrDefault(r => r.partId == item.SparePartId);
                item.WarehouseAreaCode = temp == null ? "" : temp.areacode;
                item.TopLevelWarehouseAreaId = temp == null ? null : temp.TopLevelWarehouseAreaId;
            }
            return details.OrderBy(r => r.WarehouseAreaCode);
        }


        public IQueryable<PartsPurchaseRtnSettleBill> GetPartsPurchaseRtnSettleBillByIdForPrint(int PartsPurchaseRtnSettleBillId) {
            return ObjectContext.PartsPurchaseRtnSettleBills.Where(e => e.Id == PartsPurchaseRtnSettleBillId);
        }

        public IQueryable<PartsPurchaseRtnSettleDetail> GetPartsPurchaseRtnSettleBillDetailByIdForPrint(int PartsPurchaseRtnSettleBillId) {
            return ObjectContext.PartsPurchaseRtnSettleDetails.Where(e => e.PartsPurchaseRtnSettleBillId == PartsPurchaseRtnSettleBillId);
        }

        public IEnumerable<PartsInboundPlanDetail> GetPartsInboundPlanDetailByIdForPrint(int partsInboundPlanId) {
            var partsInboundPlan = ObjectContext.PartsInboundPlans.SingleOrDefault(r => r.Id == partsInboundPlanId);
            var partsInboundPlanDetails = ObjectContext.PartsInboundPlanDetails.Where(r => r.PartsInboundPlanId == partsInboundPlanId ).ToArray();
            var sparePartIds = partsInboundPlanDetails.Select(r => r.SparePartId).ToArray();
            var partsStockStruct = from a in ObjectContext.PartsStocks.Where(r => r.WarehouseId == partsInboundPlan.WarehouseId && sparePartIds.Any(v => v == r.PartId))
                                   join b in ObjectContext.WarehouseAreas.Where(r => r.AreaKind == (int)DcsAreaKind.库位) on a.WarehouseAreaId equals b.Id
                                   join c in ObjectContext.WarehouseAreaCategories.Where(r => r.Category == (int)DcsAreaType.保管区) on b.AreaCategoryId equals c.Id
                                   select new {
                                       a.PartId,
                                       b.Code
                                   };
            var structPart = partsStockStruct.GroupBy(r => new {
                r.PartId
            }).Select(r => new {
                partId = r.Key.PartId,
                areacode = r.Min(v => v.Code)
            }).ToArray();

            var spareparts = ObjectContext.SpareParts.Where(r => sparePartIds.Any(v => v == r.Id)).ToArray();
            foreach(var item in partsInboundPlanDetails) {
                var temp = spareparts.FirstOrDefault(r => r.Id == item.SparePartId);
                item.Unit = temp == null ? "" : temp.MeasureUnit;

            }
            foreach(var item in partsInboundPlanDetails) {
                var temp = structPart.FirstOrDefault(r => r.partId == item.SparePartId);
                item.WarehouseAreaCode = temp == null ? "" : temp.areacode;

            }

            return partsInboundPlanDetails.OrderBy(r => r.SparePartId);
        }

        public PartsInboundPlan GetPartsInboundPlanByIdForPrint(int partsInboundPlanId) {
            var partsInboundPlan = ObjectContext.PartsInboundPlans.SingleOrDefault(e => e.Id == partsInboundPlanId);
            var partsSalesCategory = ObjectContext.PartsSalesCategories.SingleOrDefault(e => e.Id == partsInboundPlan.PartsSalesCategoryId);
            if(partsInboundPlan != null && partsSalesCategory != null) {
                partsInboundPlan.PartsSalesCategorieName = partsSalesCategory.Name;
                if(partsInboundPlan.OriginalRequirementBillType == 4) {
                    var partsPurchaseOrder = ObjectContext.PartsPurchaseOrders.SingleOrDefault(t => t.Id == partsInboundPlan.OriginalRequirementBillId);
                    var partsPurchaseOrderType = ObjectContext.PartsPurchaseOrderTypes.SingleOrDefault(r => r.Id == partsPurchaseOrder.PartsPurchaseOrderTypeId);
                    if(partsPurchaseOrderType != null)
                        partsInboundPlan.PartsPurchaseOrderType = partsPurchaseOrderType.Name;
                }
            }
            return partsInboundPlan;

        }

        public IQueryable<PartsSalesSettlement> GetPartsSalesSettlementByIdForPrint(int PartsSalesSettlementId) {
            return ObjectContext.PartsSalesSettlements.Where(e => e.Id == PartsSalesSettlementId);
        }

        public IQueryable<PartsPurchaseSettleBill> GetPartsPurchaseSettleBillForPrint(int partsPurchaseSettleBillId) {
            return ObjectContext.PartsPurchaseSettleBills.Where(e => e.Id == partsPurchaseSettleBillId);
        }

        public IQueryable<PartsPurchaseSettleRef> GetPartsPurchaseSettleRefForPrint(int partsPurchaseSettleBillId) {
            return ObjectContext.PartsPurchaseSettleRefs.Where(e => e.PartsPurchaseSettleBillId == partsPurchaseSettleBillId);
        }

        public IQueryable<PartsPurchaseSettleDetail> GetPartsPurchaseSettleDetailByIdForPrint(int partsPurchaseSettleBillId) {
            return ObjectContext.PartsPurchaseSettleDetails.Where(e => e.PartsPurchaseSettleBillId == partsPurchaseSettleBillId);
        }

        public IQueryable<PartsSalesSettlementRef> GetPartsSalesSettlementRefByIdForPrint(int PartsSalesSettlementId) {
            var partsSalesSettlementRef = ObjectContext.PartsSalesSettlementRefs.Where(e => e.PartsSalesSettlementId == PartsSalesSettlementId).Include("PartsSalesSettlement").Include("Warehouse");
            return partsSalesSettlementRef;
        }

        public IQueryable<PartsShippingOrderRef> GetPartsShippingOrderRefByIdForPrint(int PartsShippingOrderId) {
            return ObjectContext.PartsShippingOrderRefs.Where(e => e.PartsShippingOrderId == PartsShippingOrderId).Include("PartsShippingOrder").Include("PartsShippingOrder.Company").Include("PartsOutboundBill").Include("PartsOutboundBill.Warehouse").Include("PartsOutboundBill.PartsSalesOrderType");
        }

        public IEnumerable<VirtualPartsShippingOrderDetailRef> GetPartsShippingOrderDetailByIdForPrint(int PartsShippingOrderId)
        {
            string SQL=@"select tt.ContainerNumber,
       sum(AllWeight) as AllWeight,
       sum(AllVolume) as AllVolume,
       sum(AllSettlementPrice) as AllSettlementPrice
  from (SELECT Extent1.ContainerNumber AS ContainerNumber,
               Extent2.Weight * Extent1.ShippingAmount AS AllWeight,
               Extent2.Volume * Extent1.ShippingAmount AS AllVolume,
               (case
                 when pd.originalprice is null then
                  Extent1.SettlementPrice * Extent1.ShippingAmount
                 else
                  pd.originalprice * Extent1.ShippingAmount
               end) as AllSettlementPrice
        
          FROM PartsShippingOrderDetail Extent1
         INNER JOIN SparePart Extent2
            ON Extent1.SparePartId = Extent2.Id
          left join partsoutboundplan pl
            on Extent1.Partsoutboundplanid = pl.id
          left join partssalesorder ps
            on pl.sourceid = ps.id
           and ps.code = pl.sourcecode
          left join partssalesorderdetail pd
            on ps.id = pd.partssalesorderid
           and pd.sparepartid = Extent1.Sparepartid
         WHERE Extent1.PartsShippingOrderId = "+PartsShippingOrderId+") tt GROUP BY tt.ContainerNumber";
            var detail = ObjectContext.ExecuteStoreQuery<VirtualPartsShippingOrderDetailRef>(SQL).ToList();
            return detail;

        }
        public IEnumerable<VirtualPartsShippingOrderDetailRef> GetPartsShippingOrderDetailByIdForDetail(int PartsShippingOrderId) {

            string SQL = @"Select tt.PartsOutboundPlanCode, tt.PartsSalesOrderCode, tt.BoxUpOrderCode
  from (select ps.PartsOutboundPlanCode,
               pb.sourcecode as PartsSalesOrderCode,
               (case
                 when pg.code is null then
                  bo.code
                 else
                  pg.code
               end) as BoxUpOrderCode
          from PartsShippingOrderDetail ps
          join PartsShippingOrder po
            on ps.PartsShippingOrderId = po.Id
          join partsoutboundplan pb
            on pb.id = ps.partsoutboundplanid
          left join pickingtaskdetail pk
            on ps.taskid = pk.pickingtaskid
           and ps.tasktype = 1
          left join pickingtask pg
            on pk.pickingtaskid = pg.id
          left join boxuptaskdetail pl
            on ps.taskid = pl.boxuptaskid
           and ps.tasktype = 2
          left join boxuptask bo
            on pl.boxuptaskid = bo.id
         where po.id=" + PartsShippingOrderId + @") tt group by tt.PartsOutboundPlanCode, tt.PartsSalesOrderCode, tt.BoxUpOrderCode union all select distinct (select max(code)
          from partsoutboundplan ipl
         where ia.id = ipl.sourceid
           and ia.code = ipl.sourcecode
           and ipl.originalrequirementbillcode = ia.code) as PartsOutboundPlanCode,
		   ia.code as PartsSalesOrderCode,
		   '' as BoxUpOrderCode
		  from PartsShippingOrderDetail ps
		  join PartsShippingOrder po
			on ps.PartsShippingOrderId = po.Id
		  join partsoutboundplan pb
			on pb.id = ps.partsoutboundplanid
		  join InternalAllocationBill ia
			on pb.sourcecode = ia.sourcecode where po.id=" + PartsShippingOrderId;
            var detail = ObjectContext.ExecuteStoreQuery<VirtualPartsShippingOrderDetailRef>(SQL).ToList();
            var outCode = detail.Where(t => t.PartsSalesOrderCode.StartsWith("IAC") && t.PartsOutboundPlanCode != null).Select(t => t.PartsOutboundPlanCode).FirstOrDefault();
            if(outCode != null && detail.Any(y=>y.PartsOutboundPlanCode==null)) {
                foreach(var dd in detail.Where(t => t.PartsOutboundPlanCode == null && t.PartsSalesOrderCode.StartsWith("IAC"))) {
                   dd.PartsOutboundPlanCode=outCode;
               }
            }            
            return detail;

        }
        public IEnumerable<VirtualPickingTaskDetail> GetPickingTaskDetailsByIdVir(int pickingTaskId, string warehouseAreaIds) {
            var user = Utils.GetCurrentUserInfo();
            string SQL = @"SELECT 
  Extent1.PartsOutboundPlanCode,
  Extent1.SparePartCode,
  Extent1.SparePartName,
  Extent1.MeasureUnit,
  Extent1.WarehouseAreaCode,
  Extent1.PlanQty,
  Extent1.PickingQty,
  nvl(Extent2.Quantity,0) as StockQty
  FROM    PickingTaskDetail Extent1
  left JOIN PartsStock Extent2 ON (Extent1.SparePartId = Extent2.PartId) AND (Extent1.WarehouseAreaId = Extent2.WarehouseAreaId)
  INNER JOIN WarehouseArea Extent3 ON Extent1.WarehouseAreaId = Extent3.Id
  INNER JOIN WarehouseAreaManager Extent4 ON Extent3.TopLevelWarehouseAreaId = Extent4.WarehouseAreaId
  left join WarehouseArea w on Extent3.TopLevelWarehouseAreaId=w.id 
  WHERE ((Extent1.PickingTaskId = " + pickingTaskId + ") AND (Extent3.Status = 1)) AND (Extent4.ManagerId = " + user.Id + ") and w.id in(   ";
            var warehouseAreaIdsTurn=warehouseAreaIds.Replace("%2c", ",").ToString();
            var ids = warehouseAreaIdsTurn.Split(',').ToArray();
            for(var i = 0; i < ids.Length-1; i++) {
                if(ids.Length-1 == i + 1) {
                    SQL += ids[i];
                } else {
                    SQL += ids[i] + ",";
                }
            }
            SQL +=  ")";
            SQL += " ORDER BY Extent1.WarehouseAreaCode , Extent1.SparePartCode";
            var detail = ObjectContext.ExecuteStoreQuery<VirtualPickingTaskDetail>(SQL).ToList();
            return detail;
        }
        public IEnumerable<VirtualBoxUpTaskDetail> GetBoxDetailByIdVir(int PartsShippingOrderId) {
            var user = Utils.GetCurrentUserInfo();
            string SQL = @"select pb.sourcecode,
                           pl.sparepartcode,
                           pl.sparepartname,
                           sp.measureunit,
                           pl.BoxUpQty,
                           pl.BoxUpQty * sp.weight as weight,
                           pl.boxupqty * sp.volume as volume
                      from PartsShippingOrderDetail ps
                      join PartsShippingOrder po
                        on ps.PartsShippingOrderId = po.Id
                      join partsoutboundplan pb
                        on pb.id = ps.partsoutboundplanid
                      left join boxuptaskdetail pl
                        on pl.PartsOutboundPlanId = pb.id
                       and ps.sparepartid = pl.sparepartid
                      join sparepart sp
                        on pl.sparepartid = sp.id where po.id=" + PartsShippingOrderId + @"union all select ia.code sourcecode,
                                                                   iad.sparepartcode,
                                                                   iad.SparePartName,
                                                                   sp.measureunit,
                                                                   iad.Quantity as BoxUpQty,
                                                                   iad.Quantity * sp.weight as weight,
                                                                   iad.Quantity * sp.volume as volume
                                                              from PartsShippingOrderDetail ps
                                                              join PartsShippingOrder po
                                                                on ps.PartsShippingOrderId = po.Id
                                                              join partsoutboundplan pb
                                                                on pb.id = ps.partsoutboundplanid
                                                              join InternalAllocationBill ia
                                                                on pb.sourcecode = ia.sourcecode
                                                              join InternalAllocationDetail iad
                                                                on ia.id = iad.internalallocationbillid
                                                              join sparepart sp
                                                                on iad.sparepartid = sp.id where po.id=" + PartsShippingOrderId;

            var detail = ObjectContext.ExecuteStoreQuery<VirtualBoxUpTaskDetail>(SQL).ToList();
            return detail;
        }
        public IQueryable<VirtualPickingTaskDetail> GetPickingTaskDetailsByIdVirNew(int pickingTaskId, string warehouseAreaIds) {
            var warehouseAreaIdsTurn = warehouseAreaIds.Replace("%2c", ",").ToString();
            var ids = warehouseAreaIdsTurn.Split(',').ToArray();
            int[] areaIds = new int[ids.Length - 1];
            for(var i = 0; i < ids.Length - 1; i++) {
                areaIds[i] = Int32.Parse(ids[i]);
            }
            var user = Utils.GetCurrentUserInfo();
            var detailList = (from a in ObjectContext.PickingTaskDetails.Where(r => r.PickingTaskId == pickingTaskId)
                              join b in ObjectContext.PartsStocks on new {
                                  a.SparePartId,
                                  WarehouseAreaId = a.WarehouseAreaId.Value
                              } equals new {
                                  SparePartId = b.PartId,
                                  b.WarehouseAreaId
                              } join n in ObjectContext.PartsOutboundPlans on a.PartsOutboundPlanId equals n.Id
                             join p in ObjectContext.PickingTasks on a.PickingTaskId equals p.Id
                             join y in ObjectContext.Companies on p.BranchId equals y.Id
                              join t in ObjectContext.WarehouseAreas.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on a.WarehouseAreaId equals t.Id
                              join c in ObjectContext.WarehouseAreaManagers.Where(r => r.ManagerId == user.Id) on t.TopLevelWarehouseAreaId equals c.WarehouseAreaId
                              join d in ObjectContext.WarehouseAreas.Where(r => areaIds.Contains(r.Id)) on t.TopLevelWarehouseAreaId equals d.Id 
                              select new VirtualPickingTaskDetail {
                                  PartsOutboundPlanCode = a.PartsOutboundPlanCode,
                                  SparePartCode = a.SparePartCode,
                                  SparePartName = a.SparePartName,
                                  MeasureUnit = a.MeasureUnit,
                                  WarehouseAreaCode = a.WarehouseAreaCode,
                                  PlanQty = a.PlanQty,
                                  PickingQty = a.PickingQty,
                                  StockQty = b.Quantity,
                                  BranchName=y.Name,
                                  Code=p.Code,
                                  WarehouseName=p.WarehouseName,
                                  CounterpartCompanyName=p.CounterpartCompanyName,
                                  OrderTypeName=p.OrderTypeName,
                                  WarehouseAreaId=d.Id,
                                  TopLevelWarehouseAreaCode=d.Code,
                                  InWarehouseName = n.ReceivingWarehouseName
                              });
            return detailList.OrderBy(t => t.WarehouseAreaCode).ThenBy(r => r.SparePartCode);
        }
        public IQueryable<APartsShippingOrderDetail> GetAPartsShippingOrderDetailByIdForPrint(int PartsShippingOrderId) {
            return ObjectContext.APartsShippingOrderDetails.Where(e => e.PartsShippingOrderId == PartsShippingOrderId).Include("SparePart");
        }

        public IQueryable<VirtualPartsShippingOrderDetailRef> GetPartsShippingOrderDetailByIdForPrint1(int PartsShippingOrderId) {
            var vPartsOutboundBills = from a in ObjectContext.PartsOutboundBills
                                      join b in ObjectContext.PartsOutboundBillDetails on a.Id equals b.PartsOutboundBillId
                                      select new {
                                          PartsOutboundBillId = a.Id,
                                          a.InterfaceRecordId,
                                          b.SparePartId,
                                          b.OutboundAmount
                                      };
            var virtualPartsShippingOrderDetailRef = from a in ObjectContext.PartsShippingOrderDetails
                                                     join f in ObjectContext.SpareParts on a.SparePartId equals f.Id
                                                     join b in ObjectContext.PartsShippingOrders.Where(r => r.Id == PartsShippingOrderId) on a.PartsShippingOrderId equals b.Id
                                                     join c in ObjectContext.PartsShippingOrderRefs on b.Id equals c.PartsShippingOrderId
                                                     join d in vPartsOutboundBills on new {
                                                         c.PartsOutboundBillId,
                                                         a.SparePartId
                                                     } equals new {
                                                         d.PartsOutboundBillId,
                                                         d.SparePartId
                                                     }
                                                     select new VirtualPartsShippingOrderDetailRef {
                                                         ConfirmedAmount = a.ConfirmedAmount,
                                                         DifferenceClassification = a.DifferenceClassification,
                                                         ContainerNumber = d.InterfaceRecordId,
                                                         Id = a.Id,
                                                         InTransitDamageLossAmount = a.InTransitDamageLossAmount,
                                                         PartsShippingOrderId = a.PartsShippingOrderId,
                                                         Remark = a.Remark,
                                                         SettlementPrice = a.SettlementPrice,
                                                         ShippingAmount = d.OutboundAmount,
                                                         SparePartCode = f.Code,
                                                         SparePartId = a.SparePartId,
                                                         SparePartName = a.SparePartName,
                                                         TransportLossesDisposeMethod = a.TransportLossesDisposeMethod,
                                                         Weight = f.Weight,
                                                         MeasureUnit = f.MeasureUnit
                                                     };
            return virtualPartsShippingOrderDetailRef.Distinct().OrderBy(r => r.ContainerNumber);
        }

        //供应商发运单
        public IQueryable<SupplierShippingOrder> GetSupplierShippingOrderByIdForPrint(int SupplierShippingOrderId) {
            return ObjectContext.SupplierShippingOrders.Where(e => e.Id == SupplierShippingOrderId).Include("Company");
        }

        public IQueryable<SupplierShippingDetail> GetSupplierShippingDetailByIdForPrint(int SupplierShippingOrderId) {
            return ObjectContext.SupplierShippingDetails.Where(e => e.SupplierShippingOrderId == SupplierShippingOrderId).Include("SupplierShippingOrder.Company");
        }

        //配件销售退货结算单
        public IQueryable<PartsSalesRtnSettlement> GetPartsSalesRtnSettlementByIdForPrint(int PartsSalesRtnSettlementId) {
            return ObjectContext.PartsSalesRtnSettlements.Where(e => e.Id == PartsSalesRtnSettlementId);
        }

        public IQueryable<PartsSalesRtnSettlementDetail> GetPartsSalesRtnSettlementDetailByIdForPrint(int PartsSalesRtnSettlementId) {
            return ObjectContext.PartsSalesRtnSettlementDetails.Where(e => e.PartsSalesRtnSettlementId == PartsSalesRtnSettlementId);
        }

        public IQueryable<VirtualPartsSalesRtnSettlementRef> GetPartsSalesRtnSettlementRefByIdForPrint(int PartsSalesRtnSettlementId) {
            var saleRtnSettlementRef = ObjectContext.PartsSalesRtnSettlementRefs.Where(e => e.PartsSalesRtnSettlementId == PartsSalesRtnSettlementId);
            var inboundCheckBillIds = saleRtnSettlementRef.Select(r => r.SourceId).ToArray();

            var saleRtnSettlementRefWithCostPrice = from setRef in saleRtnSettlementRef
                                                    from a in ObjectContext.PartsInboundCheckBills.Where(r => r.Id == setRef.SourceId)
                                                    join b in ObjectContext.PartsInboundCheckBillDetails on a.Id equals b.PartsInboundCheckBillId

                                                    group new {
                                                        setRef,
                                                        b
                                                    } by new {
                                                        setRef.Id,
                                                        setRef.PartsSalesRtnSettlementId,
                                                        setRef.SourceId,
                                                        setRef.SourceCode,
                                                        setRef.SourceBillCreateTime,
                                                        setRef.WarehouseId,
                                                        setRef.WarehouseName,
                                                        setRef.SettlementAmount
                                                    }
                                                        into total
                                                        select new VirtualPartsSalesRtnSettlementRef {
                                                            Id = total.Key.Id,
                                                            PartsSalesRtnSettlementId = total.Key.PartsSalesRtnSettlementId,
                                                            SourceId = total.Key.SourceId,
                                                            SourceCode = total.Key.SourceCode,
                                                            SourceBillCreateTime = total.Key.SourceBillCreateTime,
                                                            WarehouseId = total.Key.WarehouseId,
                                                            WarehouseName = total.Key.WarehouseName,
                                                            SettlementAmount = total.Key.SettlementAmount,
                                                            CostPrice = total.Sum(p => p.b.InspectedQuantity * p.b.CostPrice)
                                                        };
            return saleRtnSettlementRefWithCostPrice;
        }

        public IQueryable<PartsInboundCheckBillDetail> GetPartsInboundCheckBillByPartsSalesRtnSettlementIdForPrint(int PartsSalesRtnSettlementId) {
            return ObjectContext.PartsInboundCheckBillDetails.Where(r => ObjectContext.PartsSalesRtnSettlementRefs.Any(v => v.SourceId == r.PartsInboundCheckBillId && v.PartsSalesRtnSettlementId == PartsSalesRtnSettlementId));
        }        

        //配件移库单
        public IQueryable<PartsShiftOrder> GetPartsShiftOrderByIdForPrint(int PartsShiftOrderId) {
            return ObjectContext.PartsShiftOrders.Where(e => e.Id == PartsShiftOrderId);
        }
        //上架清单
        public IQueryable<VirPartsShiftOrderDetail> GetPartsShiftOrderDetailByIdForPrint(int PartsShiftOrderId) {
            var result = from a in ObjectContext.PartsShiftOrderDetails.Where(e => e.PartsShiftOrderId == PartsShiftOrderId)
                         join b in ObjectContext.WarehouseAreas on a.OriginalWarehouseAreaId equals b.Id
                         join c in ObjectContext.WarehouseAreas on b.ParentId equals c.Id
                         join d in ObjectContext.WarehouseAreas on a.DestWarehouseAreaId equals d.Id
                         join h in ObjectContext.WarehouseAreas on d.ParentId equals h.Id
                         select new VirPartsShiftOrderDetail {
                             Id = a.Id,
                             PartsShiftOrderId = a.PartsShiftOrderId,
                             SparePartId = a.SparePartId,
                             SparePartCode = a.SparePartCode,
                             SparePartName = a.SparePartName,
                             OriginalWarehouseAreaId = a.OriginalWarehouseAreaId,
                             OriginalWarehouseAreaCode = a.OriginalWarehouseAreaCode,
                             OriginalWarehouseAreaCategory = a.OriginalWarehouseAreaCategory,
                             DestWarehouseAreaId = a.DestWarehouseAreaId,
                             DestWarehouseAreaCode = a.DestWarehouseAreaCode,
                             DestWarehouseAreaCategory = a.DestWarehouseAreaCategory,
                             BatchNumber = a.BatchNumber,
                             Quantity = a.Quantity,
                             OrWarehouseAreaCode = c.Code,
                             DeWarehouseAreaCode = h.Code,
                         };
             return result.OrderBy(t=>t.DestWarehouseAreaCode);
        }
    //下架架清单
        public IQueryable<VirPartsShiftOrderDetail> GetPartsShiftOrderDetailDownByIdForPrint(int PartsShiftOrderId) {
            var result = from a in ObjectContext.PartsShiftOrderDetails.Where(e => e.PartsShiftOrderId == PartsShiftOrderId)
                         join b in ObjectContext.WarehouseAreas on a.OriginalWarehouseAreaId equals b.Id
                         join c in ObjectContext.WarehouseAreas on b.ParentId equals c.Id
                         join d in ObjectContext.WarehouseAreas on a.DestWarehouseAreaId equals d.Id
                         join h in ObjectContext.WarehouseAreas on d.ParentId equals h.Id
                         select new VirPartsShiftOrderDetail {
                             Id = a.Id,
                             PartsShiftOrderId = a.PartsShiftOrderId,
                             SparePartId = a.SparePartId,
                             SparePartCode = a.SparePartCode,
                             SparePartName = a.SparePartName,
                             OriginalWarehouseAreaId = a.OriginalWarehouseAreaId,
                             OriginalWarehouseAreaCode = a.OriginalWarehouseAreaCode,
                             OriginalWarehouseAreaCategory = a.OriginalWarehouseAreaCategory,
                             DestWarehouseAreaId = a.DestWarehouseAreaId,
                             DestWarehouseAreaCode = a.DestWarehouseAreaCode,
                             DestWarehouseAreaCategory = a.DestWarehouseAreaCategory,
                             BatchNumber = a.BatchNumber,
                             Quantity = a.Quantity,
                             OrWarehouseAreaCode = c.Code,
                             DeWarehouseAreaCode = h.Code,
                         };
             return result.OrderBy(t=>t.OrWarehouseAreaCode);
        }

        public IQueryable<PartsRequisitionSettleBill> GetPartsRequisitionSettleBillPrint(int PartsRequisitionSettleBillId) {
            return ObjectContext.PartsRequisitionSettleBills.Where(e => e.Id == PartsRequisitionSettleBillId);
        }

        //内部领用结算单
        public IQueryable<VirtualPartsRequisitionSettleDetail> GetPartsRequisitionSettleDetailForPrint(int PartsRequisitionSettleBillId) {
            var partsRequisitionSettleRefs = ObjectContext.PartsRequisitionSettleRefs.Where(r => r.PartsRequisitionSettleBillId == PartsRequisitionSettleBillId);
            var resultIn = from partsRequisitionSettleRef in partsRequisitionSettleRefs
                           from partsInboundCheckBillDetail in ObjectContext.PartsInboundCheckBillDetails
                           where partsRequisitionSettleRef.SourceType == (int)DcsPartsPurchaseSettleRefSourceType.配件入库检验单 && partsRequisitionSettleRef.SourceId == partsInboundCheckBillDetail.PartsInboundCheckBillId && partsInboundCheckBillDetail.InspectedQuantity != 0
                           select new VirtualPartsRequisitionSettleDetail {
                               SourceCode = partsRequisitionSettleRef.SourceCode,
                               SparePartCode = partsInboundCheckBillDetail.SparePartCode,
                               SparePartName = partsInboundCheckBillDetail.SparePartName,
                               Quantity = partsInboundCheckBillDetail.InspectedQuantity,
                               CostPrice = partsInboundCheckBillDetail.CostPrice,
                               SettlementPrice = partsInboundCheckBillDetail.SettlementPrice,
                           };
            var resultOut = from partsRequisitionSettleRef in partsRequisitionSettleRefs
                            from partsOutboundBillDetail in ObjectContext.PartsOutboundBillDetails
                            where partsRequisitionSettleRef.SourceId == partsOutboundBillDetail.PartsOutboundBillId && partsRequisitionSettleRef.SourceType == (int)DcsPartsPurchaseSettleRefSourceType.配件出库单 && partsOutboundBillDetail.OutboundAmount != 0
                            select new VirtualPartsRequisitionSettleDetail {
                                SourceCode = partsRequisitionSettleRef.SourceCode,
                                SparePartCode = partsOutboundBillDetail.SparePartCode,
                                SparePartName = partsOutboundBillDetail.SparePartName,
                                Quantity = partsOutboundBillDetail.OutboundAmount,
                                CostPrice = partsOutboundBillDetail.CostPrice,
                                SettlementPrice = partsOutboundBillDetail.SettlementPrice
                            };
            return resultIn.Union(resultOut);
        }

        //出库计划单
        public IQueryable<PartsOutboundPlan> GetPartsOutboundPlanForPrint(int PartsOutboundPlanId) {
            return ObjectContext.PartsOutboundPlans.Where(e => e.Id == PartsOutboundPlanId);
        }

        public IQueryable<AgencyPartsOutboundPlan> GetAgencyPartsOutboundPlanForPrint(int PartsOutboundPlanId) {
            return ObjectContext.AgencyPartsOutboundPlans.Where(e => e.Id == PartsOutboundPlanId);
        }

        public IEnumerable<APartsOutboundPlanDetail> GetAPartsOutboundPlanDetailForPrint(int PartsOutboundPlanId) {
            var agencyPartsOutboundPlan = ObjectContext.AgencyPartsOutboundPlans.SingleOrDefault(r => r.Id == PartsOutboundPlanId);
            var partsOutboundPlanDetails = ObjectContext.APartsOutboundPlanDetails.Where(r => r.PartsOutboundPlanId == PartsOutboundPlanId).ToArray();
            var sparePartIds = partsOutboundPlanDetails.Select(r => r.SparePartId).ToArray();
            var partsStockStruct = from a in ObjectContext.PartsStocks.Where(r => r.WarehouseId == agencyPartsOutboundPlan.WarehouseId && sparePartIds.Any(v => v == r.PartId))
                                   join b in ObjectContext.WarehouseAreas.Where(r => r.AreaKind == (int)DcsAreaKind.库位) on a.WarehouseAreaId equals b.Id
                                   join c in ObjectContext.WarehouseAreaCategories.Where(r => r.Category == (int)DcsAreaType.保管区) on b.AreaCategoryId equals c.Id
                                   select new {
                                       a.PartId,
                                       b.Code
                                   };
            var structPart = partsStockStruct.GroupBy(r => new {
                r.PartId
            }).Select(r => new {
                partId = r.Key.PartId,
                areacode = r.Min(v => v.Code)
            }).ToArray();

            var spareparts = ObjectContext.SpareParts.Where(r => sparePartIds.Any(v => v == r.Id)).ToArray();
            foreach(var item in partsOutboundPlanDetails) {
                var temp = spareparts.FirstOrDefault(r => r.Id == item.SparePartId);
                item.Unit = temp == null ? "" : temp.MeasureUnit;

            }
            foreach(var item in partsOutboundPlanDetails) {
                var temp = structPart.FirstOrDefault(r => r.partId == item.SparePartId);
                item.WarehouseAreaCode = temp == null ? "" : temp.areacode;

            }
            return partsOutboundPlanDetails.OrderBy(r => r.SparePartId);
        }

        public IEnumerable<PartsOutboundPlanDetail> GetPartsOutboundPlanDetailForPrint(int PartsOutboundPlanId) {
            var partsOutboundPlan = ObjectContext.PartsOutboundPlans.SingleOrDefault(r => r.Id == PartsOutboundPlanId);
            var partsOutboundPlanDetails = ObjectContext.PartsOutboundPlanDetails.Where(r => r.PartsOutboundPlanId == PartsOutboundPlanId).ToArray();
            var sparePartIds = partsOutboundPlanDetails.Select(r => r.SparePartId).ToArray();
            var partsStockStruct = from a in ObjectContext.PartsStocks.Where(r => r.WarehouseId == partsOutboundPlan.WarehouseId && sparePartIds.Any(v => v == r.PartId))
                                   join b in ObjectContext.WarehouseAreas.Where(r => r.AreaKind == (int)DcsAreaKind.库位) on a.WarehouseAreaId equals b.Id
                                   join c in ObjectContext.WarehouseAreaCategories.Where(r => r.Category == (int)DcsAreaType.保管区) on b.AreaCategoryId equals c.Id
                                   select new {
                                       a.PartId,
                                       b.Code
                                   };
            var structPart = partsStockStruct.GroupBy(r => new {
                r.PartId
            }).Select(r => new {
                partId = r.Key.PartId,
                areacode = r.Min(v => v.Code)
            }).ToArray();

            var spareparts = ObjectContext.SpareParts.Where(r => sparePartIds.Any(v => v == r.Id)).ToArray();
            foreach(var item in partsOutboundPlanDetails) {
                var temp = spareparts.FirstOrDefault(r => r.Id == item.SparePartId);
                item.Unit = temp == null ? "" : temp.MeasureUnit;

            }
            foreach(var item in partsOutboundPlanDetails) {
                var temp = structPart.FirstOrDefault(r => r.partId == item.SparePartId);
                item.WarehouseAreaCode = temp == null ? "" : temp.areacode;

            }
            return partsOutboundPlanDetails.OrderBy(r => r.SparePartId);
        }

        //结算汇总单
        public IQueryable<SettleBillInvoiceLink> GetSettleBillInvoiceLinkForPrint(int ClaimSettlementBillId) {
            return ObjectContext.SettleBillInvoiceLinks.Where(e => e.ClaimSettlementBillId == ClaimSettlementBillId).Include("DealerInvoiceInformation").Include("SsClaimSettlementBill");
        }

        //旧件清退单
        public IQueryable<UsedPartsReturnOrder> GetUsedPartsReturnOrderForPrint(int UsedPartsReturnOrderId) {
            return ObjectContext.UsedPartsReturnOrders.Where(e => e.Id == UsedPartsReturnOrderId);
        }

        public IQueryable<UsedPartsReturnDetail> GetUsedPartsReturnDetailForPrint(int UsedPartsReturnOrderId) {
            return ObjectContext.UsedPartsReturnDetails.Where(e => e.UsedPartsReturnOrderId == UsedPartsReturnOrderId).OrderBy(r => r.UsedPartsName);
        }

        //旧件发运单
        public IQueryable<UsedPartsShippingOrder> GetUsedPartsShippingOrderForPrint(int UsedPartsShippingOrderId) {
            return ObjectContext.UsedPartsShippingOrders.Where(e => e.Id == UsedPartsShippingOrderId).Include("UsedPartsWarehouse");
        }

        public IEnumerable<UsedPartsShippingDetail> GetUsedPartsShippingDetailForPrint(int UsedPartsShippingOrderId) {
            var usedPartsShippingDetails = ObjectContext.UsedPartsShippingDetails.Where(e => e.UsedPartsShippingOrderId == UsedPartsShippingOrderId).OrderBy(r => r.UsedPartsName).ThenBy(r => r.ClaimBillId).ToArray();
            var claimBillIds = usedPartsShippingDetails.Select(r => r.ClaimBillId).Distinct().ToArray();
            //var data = from a in ObjectContext.RepairClaimBills.Where(r => claimBillIds.Contains(r.Id))
            //           select new {
            //               a.Id,
            //               SerialNumber = a.VIN.Substring(9, 8),
            //               a.RepairClaimApplicationId
            //           };
            //var result = data.ToArray();
            //foreach(var usedPartsShippingDetail in usedPartsShippingDetails) {
            //    var strut = result.FirstOrDefault(r => r.Id == usedPartsShippingDetail.ClaimBillId);
            //    if(strut != null) {
            //        usedPartsShippingDetail.RepairClaimApplication = (strut.RepairClaimApplicationId == null ? "否" : "是");
            //        usedPartsShippingDetail.VehicleSerialNumber = strut.SerialNumber;
            //    }
            //}
            return usedPartsShippingDetails;
        }

        //旧件发运单条码标签
        public IEnumerable<UsedPartsShippingDetail> GetUsedPartsShippingDetailWithLableForPrint(int usedPartsShippingOrderId) {
            var usedPartsShippingDetails = ObjectContext.UsedPartsShippingDetails.Where(e => e.UsedPartsShippingOrderId == usedPartsShippingOrderId).ToArray();
            var usedPartsBarCodes = usedPartsShippingDetails.Select(r => r.UsedPartsBarCode).ToArray();
            //var repairOrderMaterialDetails = ObjectContext.RepairOrderMaterialDetails.Include("RepairOrderItemDetail.RepairOrderFaultReason").Where(r => usedPartsBarCodes.Contains(r.UsedPartsBarCode)).ToArray();
            //foreach(var usedPartsShippingDetail in usedPartsShippingDetails) {
            //    var malfunctionReason = repairOrderMaterialDetails.FirstOrDefault(r => r.UsedPartsBarCode == usedPartsShippingDetail.UsedPartsBarCode);
            //    if(malfunctionReason != null)
            //        usedPartsShippingDetail.MalfunctionReason = malfunctionReason.RepairOrderItemDetail.RepairOrderFaultReason.MalfunctionReason;
            //}
            return usedPartsShippingDetails;
        }

        //旧件发运单选择条码标签
        public IEnumerable<UsedPartsShippingDetail> GetUsedPartsShippingDetailWithLableForChoosePrint(int[] usedPartsShippingDetailIds) {
            var usedPartsShippingDetails = ObjectContext.UsedPartsShippingDetails.Where(e => usedPartsShippingDetailIds.Contains(e.Id)).ToArray();
            var usedPartsBarCodes = usedPartsShippingDetails.Select(r => r.UsedPartsBarCode).ToArray();
            //var repairOrderMaterialDetails = ObjectContext.RepairOrderMaterialDetails.Include("RepairOrderItemDetail.RepairOrderFaultReason").Where(r => usedPartsBarCodes.Contains(r.UsedPartsBarCode)).ToArray();
            //foreach(var usedPartsShippingDetail in usedPartsShippingDetails) {
            //    var malfunctionReason = repairOrderMaterialDetails.FirstOrDefault(r => r.UsedPartsBarCode == usedPartsShippingDetail.UsedPartsBarCode);
            //    if(malfunctionReason != null)
            //        usedPartsShippingDetail.MalfunctionReason = malfunctionReason.RepairOrderItemDetail.RepairOrderFaultReason.MalfunctionReason;
            //}
            return usedPartsShippingDetails;
        }

        //服务站旧件库存条码标签
        public IEnumerable<SsUsedPartsStorage> GetSsUsedPartsStorageWithLableForPrint(int[] ids) {
            var ssUsedPartsStorages = ObjectContext.SsUsedPartsStorages.Where(r => ids.Contains(r.Id)).ToArray();
            var usedPartsBarCodes = ssUsedPartsStorages.Select(r => r.UsedPartsBarCode).ToArray();
            //var repairOrderMaterialDetails = ObjectContext.RepairOrderMaterialDetails.Include("RepairOrderItemDetail.RepairOrderFaultReason").Where(r => usedPartsBarCodes.Contains(r.UsedPartsBarCode)).ToArray();
            //foreach(var ssUsedPartsStorage in ssUsedPartsStorages) {
            //    var malfunctionReason = repairOrderMaterialDetails.FirstOrDefault(r => r.UsedPartsBarCode == ssUsedPartsStorage.UsedPartsBarCode);
            //    if(malfunctionReason != null)
            //        ssUsedPartsStorage.MalfunctionReason = malfunctionReason.RepairOrderItemDetail.RepairOrderFaultReason.MalfunctionReason;
            //}
            return ssUsedPartsStorages;
        }

        //旧件库存条码标签
        public IEnumerable<UsedPartsStock> GetUsedPartsStockWithLableForPrint(int[] ids) {
            var usedPartsStocks = ObjectContext.UsedPartsStocks.Where(r => ids.Contains(r.Id)).ToArray();
            var usedPartsBarCodes = usedPartsStocks.Select(r => r.UsedPartsBarCode).ToArray();
            //var repairOrderMaterialDetails = ObjectContext.RepairOrderMaterialDetails.Include("RepairOrderItemDetail.RepairOrderFaultReason").Where(r => usedPartsBarCodes.Contains(r.UsedPartsBarCode)).ToArray();
            //foreach(var usedPartsStock in usedPartsStocks) {
            //    var malfunctionReason = repairOrderMaterialDetails.FirstOrDefault(r => r.UsedPartsBarCode == usedPartsStock.UsedPartsBarCode);
            //    if(malfunctionReason != null)
            //        usedPartsStock.MalfunctionReason = malfunctionReason.RepairOrderItemDetail.RepairOrderFaultReason.MalfunctionReason;
            //}
            return usedPartsStocks;
        }

        //库区库位管理打印库位标签
        public IQueryable<WarehouseArea> GetWarehouseAreaWithLablePrint(int[] ids) {
            return ObjectContext.WarehouseAreas.Where(r => ids.Contains(r.Id));
        }

        //配件库存明细查询打印混合标签
        public IQueryable<VirtualPartsStock> GetVirtualPartsStockWithLablePrint(int[] ids) {
            return 查询配件库位库存(null, null, null, null, null, null, null).Where(r => ids.Contains(r.PartsStockId)).AsQueryable();
        }

        //旧件入库单
        //public IQueryable<UsedPartsInboundOrder> GetUsedPartsInboundOrderForPrint(int usedPartsInboundOrderId) {
        //    return ObjectContext.UsedPartsInboundOrders.Where(e => e.Id == usedPartsInboundOrderId);
        //}
        public UsedPartsInboundOrder GetUsedPartsInboundOrderForPrint(int usedPartsInboundOrderId) {
            var usedPartsInboundOrder = ObjectContext.UsedPartsInboundOrders.SingleOrDefault(e => e.Id == usedPartsInboundOrderId);
            var usedPartsShippingOrder = ObjectContext.UsedPartsShippingOrders.SingleOrDefault(r => r.Code == usedPartsInboundOrder.SourceCode);
            if(usedPartsShippingOrder != null) {
                usedPartsInboundOrder.ReceptionRemark = this.bsubstring(usedPartsShippingOrder.ReceptionRemark, 22);
            }
            return usedPartsInboundOrder;
        }

        public UsedPartsInboundOrder GetUsedPartsInboundOrderFinanceForPrint(int usedPartsInboundOrderId) {
            var usedPartsInboundOrder = ObjectContext.UsedPartsInboundOrders.SingleOrDefault(e => e.Id == usedPartsInboundOrderId);
            var usedPartsShippingOrder = ObjectContext.UsedPartsShippingOrders.SingleOrDefault(r => r.Code == usedPartsInboundOrder.SourceCode);
            if(usedPartsShippingOrder != null) {
                usedPartsInboundOrder.ReceptionRemark = this.bsubstring(usedPartsShippingOrder.ReceptionRemark, 22);

            }
            var personel = ObjectContext.Personnels.SingleOrDefault(r => r.Id == usedPartsInboundOrder.CreatorId);
            if(personel != null) {
                var branchs = ObjectContext.Branches.FirstOrDefault(r => r.Id == personel.CorporationId);
                if(branchs != null)
                    usedPartsInboundOrder.BranchName = branchs.Name;
            }


            return usedPartsInboundOrder;
        }

        public IQueryable<UsedPartsInboundDetail> GetUsedPartsInboundOrderDetailForPrint(int usedPartsInboundOrderId) {
            return ObjectContext.UsedPartsInboundDetails.Where(e => e.UsedPartsInboundOrderId == usedPartsInboundOrderId).Include("SparePart");
        }

        //旧件出库单
        public IQueryable<UsedPartsOutboundOrder> GetUsedPartsOutboundOrderForPrint(int UsedPartsOutboundOrderId) {
            return ObjectContext.UsedPartsOutboundOrders.Where(e => e.Id == UsedPartsOutboundOrderId);
        }

        public IQueryable<UsedPartsOutboundDetail> GetUsedPartsOutboundDetailForPrint(int UsedPartsOutboundOrderId) {
            return ObjectContext.UsedPartsOutboundDetails.Where(e => e.UsedPartsOutboundOrderId == UsedPartsOutboundOrderId).Include("SparePart");
        }

        //配件盘点单
        public IQueryable<PartsInventoryBill> GetPartsInventoryBillByIdForPrint(int partsInventoryBillId) {
            return ObjectContext.PartsInventoryBills.Where(e => e.Id == partsInventoryBillId);
        }


        public IEnumerable<VirPartsInventoryDetail> GetPartsInventoryDetailByIdForPrint(int partsInventoryBillId) {
            var user = Utils.GetCurrentUserInfo();
            string SQL = @"select pi.sparepartcode,
                               pi.sparepartname,
                               pi.WarehouseAreaCode,
                               pi.CurrentStorage,
                               pi.StorageAfterInventory,
                               pb.referencecode,
                               top.code as TopCode,
							   (case when pi.Remark like '%PDA%' then cast('PDA' as varchar2(50)) else  cast('' as varchar2(50)) end ) as Remark
                          from PartsInventoryDetail pi
                          join partsbranch pb
                            on pi.sparepartid = pb.partid
                          join WarehouseArea wh
                            on pi.warehouseareaid = wh.id
                          join WarehouseArea top
                            on wh.toplevelwarehouseareaid = top.id
                           and pb.status = 1
                         where pi.PartsInventoryBillId =" + partsInventoryBillId + " order by top.code asc,pi.WarehouseAreaCode asc,pi.sparepartcode asc";
          
            var detail = ObjectContext.ExecuteStoreQuery<VirPartsInventoryDetail>(SQL).ToList();
            return detail;
        }
        //旧件处理单
        public IQueryable<UsedPartsDisposalDetail> GetUsedPartsDisposalDetailByIdForPrint(int usedPartsDisposalBillId) {
            return ObjectContext.UsedPartsDisposalDetails.Where(e => e.UsedPartsDisposalBillId == usedPartsDisposalBillId).OrderBy(r => r.UsedPartsCode);
        }

        // 入库库区
        public WarehouseArea GetWarehouseAreaForValue(int PartsShiftOrderId) {
            var a = ObjectContext.PartsShiftOrders.SingleOrDefault(e => e.Id == PartsShiftOrderId);
            var b = ObjectContext.PartsShiftOrderDetails.Where(e => e.PartsShiftOrderId == PartsShiftOrderId).Select(e => e.DestWarehouseAreaId).First();
            return ObjectContext.WarehouseAreas.SingleOrDefault(ex => ex.WarehouseId == a.WarehouseId && ex.Id == b && ex.AreaKind == (int)DcsAreaKind.库区);
        }
        

        //采购退货结算单
        public IEnumerable<VehiclePartsPurchaseRtnSettleDetail> GetForPRINT(int PartsPurchaseRtnSettleBillId) {
            var partsPurchaseRtnSettleBills = from partsPurchaseRtnSettleRef in ObjectContext.PartsPurchaseRtnSettleRefs
                                              from partsPurchaseRtnSettleDetail in ObjectContext.PartsPurchaseRtnSettleDetails
                                              where (partsPurchaseRtnSettleRef.PartsPurchaseRtnSettleBillId == partsPurchaseRtnSettleDetail.PartsPurchaseRtnSettleBillId && partsPurchaseRtnSettleDetail.PartsPurchaseRtnSettleBillId == PartsPurchaseRtnSettleBillId)
                                              select new {
                                                  SourceId = partsPurchaseRtnSettleRef.SourceId,
                                                  SourceCode = partsPurchaseRtnSettleRef.SourceCode,
                                                  SparePartId = partsPurchaseRtnSettleDetail.SparePartId,
                                                  SparePartCode = partsPurchaseRtnSettleDetail.SparePartCode,
                                                  SparePartName = partsPurchaseRtnSettleDetail.SparePartName,
                                                  QuantityToSettle = partsPurchaseRtnSettleDetail.QuantityToSettle,
                                                  SettlementAmount = partsPurchaseRtnSettleDetail.SettlementPrice
                                              };

            var results = from partsOutboundBill in ObjectContext.PartsOutboundBills
                          from partsPurchaseRtnSettleBill in partsPurchaseRtnSettleBills
                          from partsOutboundBillDetail in ObjectContext.PartsOutboundBillDetails
                          where (partsPurchaseRtnSettleBill.SourceId == partsOutboundBill.Id && partsOutboundBill.Id == partsOutboundBillDetail.PartsOutboundBillId && partsPurchaseRtnSettleBill.SparePartId == partsOutboundBillDetail.SparePartId && partsPurchaseRtnSettleBill.QuantityToSettle == partsOutboundBillDetail.OutboundAmount)
                          select new {
                              SourceId = partsPurchaseRtnSettleBill.SourceId,
                              SourceCode = partsPurchaseRtnSettleBill.SourceCode,
                              SparePartCode = partsPurchaseRtnSettleBill.SparePartCode,
                              SparePartName = partsPurchaseRtnSettleBill.SparePartName,
                              QuantityToSettle = partsPurchaseRtnSettleBill.QuantityToSettle,
                              SettlementAmount = partsPurchaseRtnSettleBill.SettlementAmount,
                              CostPrice = partsOutboundBillDetail.CostPrice
                          };
            var result = results.Select(r => new VehiclePartsPurchaseRtnSettleDetail {
                SourceCode = r.SourceCode,
                SparePartCode = r.SparePartCode,
                SparePartName = r.SparePartName,
                QuantityToSettle = r.QuantityToSettle,
                SettlementAmount = r.SettlementAmount,
                CostPrice = r.CostPrice
            });
            return result.Distinct().ToArray();
        }

        //配件检验入库单查询打印配件标签
        public IQueryable<PartsInboundCheckBillDetail> GetPartsInboundCheckBillDetailByIdForPrintLabel(int partsInboundCheckBillId) {
            return ObjectContext.PartsInboundCheckBillDetails.Where(e => e.PartsInboundCheckBillId == partsInboundCheckBillId).OrderBy(e => e.SparePartId).Include("SparePart");
        }

        public IQueryable<PartsInboundPlanDetail> GetPartsInboundPlanDetailByIdForPrintLabel(int id) {
            return ObjectContext.PartsInboundPlanDetails.Where(e => e.Id == id).OrderBy(e => e.SparePartId).Include("SparePart");
        }

        //配件入库计划返回计量单位
        public IQueryable<PartsInboundPlanDetail> GetPartsInboundPlanDetailByIdForLabel(int[] id) {
            var partsInboundPlanDetails = ObjectContext.PartsInboundPlanDetails.Where(r => id.Contains(r.Id)).ToArray();
            var sparePartIds = partsInboundPlanDetails.Select(r => r.SparePartId).ToArray();
            var spareparts = ObjectContext.SpareParts.Where(r => sparePartIds.Any(v => v == r.Id)).ToArray();
            foreach(var item in partsInboundPlanDetails) {
                var temp = spareparts.FirstOrDefault(r => r.Id == item.SparePartId);
                item.Unit = temp == null ? "" : temp.MeasureUnit;
                item.OverseasPartsFigure = temp == null ? "" : temp.OverseasPartsFigure;

            }
            return ObjectContext.PartsInboundPlanDetails.Where(r => id.Any(v => v == r.Id));
        }

        public IQueryable<PartsInboundCheckBillDetail> GetPartsInboundCheckBillDetailByIdForLabel(int[] id) {
            return ObjectContext.PartsInboundCheckBillDetails.Where(r => id.Any(v => v == r.Id));
        }

        //供应商发运管理打印配件标签1
        public IQueryable<SupplierShippingDetail> GetSupplierShippingDetailByIdForLabel(int[] id) {
            return ObjectContext.SupplierShippingDetails.Where(r => id.Any(v => v == r.Id)).OrderBy(e => e.SparePartId).Include("SparePart");
        }

        //供应商发运管理打印配件标签2
        public IQueryable<SparePart> GetSparePartByIdForSupplierPartsTagPrinttingLabel(int[] id) {
            return ObjectContext.SpareParts.Where(r => id.Any(v => v == r.Id));
        }

        //配件入库计划查询打印WMS配件标签
        public IQueryable<PartsInboundPlanDetail> GetPartsInboundPlanDetailsByIdForLabel(int[] id) {
            return ObjectContext.PartsInboundPlanDetails.Where(r => id.Any(v => v == r.Id)).OrderBy(e => e.SparePartId).Include("SparePart");
        }

        //维修单故障原因
        //public IQueryable<RepairOrderFaultReason> GetRepairOrderFaultReasonByIdForPrint(int RepairOrderId) {
        //    return ObjectContext.RepairOrderFaultReasons.Where(v => v.RepairOrderId == RepairOrderId);
        //}

        //结算单维修项目
        //public IQueryable<RepairOrderItemDetail> GetRepairOrderItemDetailByIdForPrint(int RepairOrderId) {
        //    var a = ObjectContext.RepairOrderFaultReasons.Where(v => v.RepairOrderId == RepairOrderId).Select(e => e.Id);
        //    return ObjectContext.RepairOrderItemDetails.Where(s => a.Contains(s.RepairOrderFaultReasonId));
        //}

        //结算单合计费用
        public IEnumerable<VirtualRepairOrderMaterialDetail> GetVirtualRepairOrderMaterialDetailByRepairOrderId(int RepairOrderId) {
            //var actualLaborCost = from repairOrderFaultReasons in ObjectContext.RepairOrderFaultReasons
            //                      from repairOrderItemDetail in ObjectContext.RepairOrderItemDetails
            //                      where (repairOrderFaultReasons.Id == repairOrderItemDetail.RepairOrderFaultReasonId && repairOrderFaultReasons.RepairOrderId == RepairOrderId)
            //                      select new {
            //                          ActualLaborCost = repairOrderItemDetail.ActualLaborCost,
            //                          SettleAttribute = repairOrderItemDetail.SettleAttribute,
            //                          RepairClaimItemDetailId = repairOrderItemDetail.Id,
            //                          RepairOrderId = repairOrderFaultReasons.RepairOrderId
            //                      };
            //var resultActualLaborCost = actualLaborCost.Where(e => e.SettleAttribute == (int)DcsRepairSettleAttribute.自费).Sum(e => e.ActualLaborCost);
            //var acturalMaterialCost = from laborCost in actualLaborCost
            //                          from repairOrderMaterialDetail in ObjectContext.RepairOrderMaterialDetails
            //                          where (laborCost.RepairClaimItemDetailId == repairOrderMaterialDetail.RepairClaimItemDetailId && laborCost.RepairOrderId == RepairOrderId)
            //                          select new {
            //                              ActuralMaterialCost = repairOrderMaterialDetail.ActuralMaterialCost,
            //                              SettleAttribute = laborCost.SettleAttribute
            //                          };
            //var resultActuralMaterialCost = acturalMaterialCost.Where(e => e.SettleAttribute == (int)DcsRepairSettleAttribute.自费).Sum(e => e.ActuralMaterialCost);
            //var result = actualLaborCost.Select(r => new VirtualRepairOrderMaterialDetail {
            //    TotalActualLaborCost = resultActualLaborCost,
            //    TotalActuralMaterialCost = resultActuralMaterialCost,
            //    Total = resultActualLaborCost + resultActuralMaterialCost
            //});
            //return result;
            return null;
        }

        //保外维修单  结算单打印(会员) 费用计算
        public VirtualRepairOrderMaterialDetail GetVirtualRepairOrderMaterialByRepairOrderId(int RepairOrderId) {
            //var repairOrder = ObjectContext.RepairOrders.SingleOrDefault(r => r.Id == RepairOrderId);
            //var actualLaborCost = from repairOrderFaultReasons in ObjectContext.RepairOrderFaultReasons
            //                      from repairOrderItemDetail in ObjectContext.RepairOrderItemDetails
            //                      where (repairOrderFaultReasons.Id == repairOrderItemDetail.RepairOrderFaultReasonId && repairOrderFaultReasons.RepairOrderId == RepairOrderId)
            //                      select new {
            //                          ActualLaborCost = repairOrderItemDetail.ActualLaborCost,
            //                          SettleAttribute = repairOrderItemDetail.SettleAttribute,
            //                          RepairClaimItemDetailId = repairOrderItemDetail.Id,
            //                          RepairOrderId = repairOrderFaultReasons.RepairOrderId,
            //                          OtherCost = repairOrderFaultReasons.OtherCost
            //                      };
            //var resultActualLaborCost = actualLaborCost.Where(e => e.SettleAttribute == (int)DcsRepairSettleAttribute.自费).Sum(e => e.ActualLaborCost);
            //var otherCost = actualLaborCost.Where(e => e.SettleAttribute == (int)DcsRepairSettleAttribute.自费).Sum(e => e.OtherCost);
            //var acturalMaterialCost = from laborCost in actualLaborCost
            //                          from repairOrderMaterialDetail in ObjectContext.RepairOrderMaterialDetails
            //                          where (laborCost.RepairClaimItemDetailId == repairOrderMaterialDetail.RepairClaimItemDetailId && laborCost.RepairOrderId == RepairOrderId)
            //                          select new {
            //                              ActuralMaterialCost = repairOrderMaterialDetail.ActuralMaterialCost,
            //                              SettleAttribute = laborCost.SettleAttribute
            //                          };
            //var resultActuralMaterialCost = acturalMaterialCost.Where(e => e.SettleAttribute == (int)DcsRepairSettleAttribute.自费).Sum(e => e.ActuralMaterialCost);
            //var integra = ObjectContext.IntegralClaimBills.FirstOrDefault(r => r.RepairOrderId == RepairOrderId && r.IntegralClaimBillType == (int)DcsDealerSettleType.红包类型);
            //var integralClaimBill = ObjectContext.IntegralClaimBills.FirstOrDefault(r => r.RepairOrderId == RepairOrderId);
            var result = new VirtualRepairOrderMaterialDetail();
            //result.TotalActualLaborCost = resultActualLaborCost;
            //result.FieldServiceCharge = repairOrder.OtherTrafficFee + repairOrder.TowCharge + repairOrder.FieldServiceCharge;
            //result.OtherCost = otherCost + repairOrder.TrimCost;
            //result.TotalActuralMaterialCost = resultActuralMaterialCost;
            //result.Total = resultActualLaborCost + resultActuralMaterialCost + result.OtherCost + result.FieldServiceCharge;
            //result.MemberRightsCost = (integralClaimBill == null ? 0 : (integralClaimBill.MemberRightsCost ?? 0));
            //result.RedPacketsUsedPrice = (integra == null ? 0 : (integra.RedPacketsUsedPrice ?? 0));
            //result.CostDiscount = (integralClaimBill == null ? 0 : (integralClaimBill.CostDiscount ?? 0));
            //result.MemberUsedIntegral = ((integralClaimBill == null ? 0 : (integralClaimBill.MemberUsedIntegral ?? 0)) / 100);
            //result.DeductionTotalAmount = (integralClaimBill == null ? result.Total : (integralClaimBill.DeductionTotalAmount ?? 0));
            return result;
        }

        public VirtualPickingTask GetPickingTaskByIdVir(int pickingTaskId)
        {
            var picking = (from a in ObjectContext.PickingTasks.Where(r => r.Id == pickingTaskId)
                           join b in ObjectContext.Companies on a.BranchId equals b.Id
                           select new VirtualPickingTask
                           {
                               BranchName = b.Name,
                               Code = a.Code,
                               WarehouseName = a.WarehouseName,
                               CounterpartCompanyName = a.CounterpartCompanyName,
                               OrderTypeName = a.OrderTypeName
                           }).FirstOrDefault();
            return picking;
        }

        public VirtualPartsRetailOrder GetVirtualPartsRetailOrder(int PartsRetailOrderId,int CompanyId)
        {
            var company = ObjectContext.Companies.Where(r => r.Id == CompanyId).FirstOrDefault();
            if(company.Type == (int)DcsCompanyType.服务站 || company.Type == (int)DcsCompanyType.服务站兼代理库){
                var retails = from a in ObjectContext.DealerPartsRetailOrders.Where(r => r.Id == PartsRetailOrderId)
                              select new VirtualPartsRetailOrder {
                                  Id = a.Id,
                                  CompanyName = a.DealerName,
                                  OutBoundName = "服务站出库单",
                                  Code = a.Code,
                                  WarehouseName = a.DealerCode,
                                  OrderTypeName = "零售出库",
                                  ApproveTime = a.ApproveTime,
                                  CustomerUnit = a.Customer
                              };
                return retails.FirstOrDefault();
            }else{
                var retails = from a in ObjectContext.PartsRetailOrders.Where(r => r.Id == PartsRetailOrderId)
                              select new VirtualPartsRetailOrder {
                                  Id = a.Id,
                                  CompanyName = a.SalesUnitOwnerCompanyName,
                                  OutBoundName = "中心库出库单",
                                  Code = a.Code,
                                  WarehouseName = a.WarehouseName,
                                  OrderTypeName = "零售出库",
                                  ApproveTime = a.ApproveTime,
                                  CustomerUnit = a.CustomerName
                              };
                return retails.FirstOrDefault();
            }
        }

        public IQueryable<VirtualPartsRetailOrderDetail> GetVirtualPartsRetailOrderDetails(int PartsRetailOrderId, int CompanyId) {
            var company = ObjectContext.Companies.Where(r => r.Id == CompanyId).FirstOrDefault();
            if (company.Type == (int)DcsCompanyType.服务站 || company.Type == (int)DcsCompanyType.服务站兼代理库) {
                var retails = from a in ObjectContext.DealerRetailOrderDetails.Where(r => r.DealerPartsRetailOrderId == PartsRetailOrderId)
                              join b in ObjectContext.SpareParts on a.PartsId equals b.Id
                              select new VirtualPartsRetailOrderDetail {
                                  SparePartCode = a.PartsCode,
                                  SparePartName = a.PartsName,
                                  MeasureUnit = b.MeasureUnit,
                                  Quantity = a.Quantity,
                                  SalesPrice = a.Price,
                                  TotalSalesPrice = a.Quantity * a.Price,
                                  Remark = a.Remark
                              };
                return retails;
            } else {
                var retails = from a in ObjectContext.PartsRetailOrderDetails.Where(r => r.PartsRetailOrderId == PartsRetailOrderId)
                              join b in ObjectContext.SpareParts on a.SparePartId equals b.Id
                              select new VirtualPartsRetailOrderDetail {
                                  SparePartCode = a.SparePartCode,
                                  SparePartName = a.SparePartName,
                                  MeasureUnit = b.MeasureUnit,
                                  Quantity = a.Quantity,
                                  SalesPrice = a.SalesPrice,
                                  TotalSalesPrice = a.Quantity * a.SalesPrice,
                                  Remark = a.Remark
                              };
                return retails;
            }
        }
        public VirtualPartsShippingOrderRef GetVirtualPartsShippingOrderRefById(int PartsShippingOrderId) {
            var virtualPartsShippingOrderRef = new VirtualPartsShippingOrderRef();
          //  var partsShippingOrderRef = ObjectContext.PartsShippingOrderRefs.Where(e => e.PartsShippingOrderId == PartsShippingOrderId).ToArray();
           // var PartsOutboundBillIds = partsShippingOrderRef.Select(r => r.PartsOutboundBillId).ToArray();
            if (PartsShippingOrderId >0)
            {
              //  var partsOutboundBill = ObjectContext.PartsOutboundBills.Where(e => PartsOutboundBillIds.Contains(e.Id)).ToArray();
                var partsShippingOrder = ObjectContext.PartsShippingOrders.SingleOrDefault(e => e.Id == PartsShippingOrderId);
                var branch = ObjectContext.Branches.SingleOrDefault(e => e.Id == partsShippingOrder.BranchId);
              //  var receiving = ObjectContext.Companies.SingleOrDefault(e => e.Id == partsShippingOrder.ReceivingCompanyId);
             //   var logistic = ObjectContext.Companies.SingleOrDefault(e => e.Code == partsShippingOrder.LogisticCompanyCode && e.Name == partsShippingOrder.LogisticCompanyName);
               // var partsSalesCategory = ObjectContext.PartsSalesCategories.SingleOrDefault(e => e.Id == partsShippingOrder.PartsSalesCategoryId);
                virtualPartsShippingOrderRef.PartsShippingOrderCode = partsShippingOrder.Code;
                virtualPartsShippingOrderRef.ReceivingCompanyCode = partsShippingOrder.ReceivingCompanyCode;
                virtualPartsShippingOrderRef.ReceivingCompanyName = partsShippingOrder.ReceivingCompanyName;
                virtualPartsShippingOrderRef.ReceivingAddress = partsShippingOrder.ReceivingAddress;
                //virtualPartsShippingOrderRef.ReceivingPhone = receiving.ContactPhone;
                //virtualPartsShippingOrderRef.ReceivingPerson = receiving.ContactPerson;
                virtualPartsShippingOrderRef.TransportDriver = partsShippingOrder.TransportDriver;
                virtualPartsShippingOrderRef.TransportDriverPhone = partsShippingOrder.TransportDriverPhone;

                virtualPartsShippingOrderRef.LogisticCompanyName = partsShippingOrder.LogisticCompanyName;
                //if(logistic != null) {
                //    virtualPartsShippingOrderRef.LogisticCompanyName = logistic.Name;
                //    virtualPartsShippingOrderRef.LogisticCompanyPhone = logistic.ContactPhone;
                //    virtualPartsShippingOrderRef.LogisticCompanyPerson = logistic.ContactPerson;
                //} else {
                //    virtualPartsShippingOrderRef.LogisticCompanyName = partsShippingOrder.LogisticCompanyName;
                //    virtualPartsShippingOrderRef.LogisticCompanyPhone = string.Empty;
                //    virtualPartsShippingOrderRef.LogisticCompanyPerson = string.Empty;
                //}
                virtualPartsShippingOrderRef.ReceivingWarehouseName = partsShippingOrder.ReceivingWarehouseName;
                virtualPartsShippingOrderRef.WarehouseName = partsShippingOrder.WarehouseName;
                //var warehouse = ObjectContext.Warehouses.SingleOrDefault(r => r.Id == partsShippingOrder.WarehouseId);
                //if(warehouse != null) {
                //    virtualPartsShippingOrderRef.PhoneNumber = warehouse.PhoneNumber;
                //    virtualPartsShippingOrderRef.Contact = warehouse.Contact;
                //}
              //  virtualPartsShippingOrderRef.ShippingCompanyName = ObjectContext.Companies.SingleOrDefault(e => e.Id == partsShippingOrder.ShippingCompanyId).Name;
                virtualPartsShippingOrderRef.ShippingMethod = partsShippingOrder.ShippingMethod;
                //virtualPartsShippingOrderRef.OriginalRequirementBillType = partsShippingOrder.OriginalRequirementBillType;
                //virtualPartsShippingOrderRef.OriginalRequirementBillCode = partsShippingOrder.OriginalRequirementBillCode;
                virtualPartsShippingOrderRef.ShippingDate = partsShippingOrder.ShippingDate;
                //发运单类型
                virtualPartsShippingOrderRef.Type = partsShippingOrder.Type;
                //virtualPartsShippingOrderRef.Remark = partsShippingOrder.Remark;
                virtualPartsShippingOrderRef.RequestedArrivalDate = partsShippingOrder.RequestedArrivalDate;
               // virtualPartsShippingOrderRef.TransportMileage = partsShippingOrder.TransportMileage;
                //int i = 1;
                //string partsOutboundBillCodes = "";
                //foreach(var p in partsOutboundBill) {
                //    if(i == partsOutboundBill.Length) {
                //        partsOutboundBillCodes = partsOutboundBillCodes + p.Code;
                //    } else {
                //        partsOutboundBillCodes = partsOutboundBillCodes + p.Code + ",";
                //    }
                //    i++;
                //}
                //virtualPartsShippingOrderRef.PartsOutboundBillCode = partsOutboundBillCodes;
                //virtualPartsShippingOrderRef.OrderApproveComment = partsShippingOrder.OrderApproveComment;
                //virtualPartsShippingOrderRef.OutboundBillWarehouseAddress = ObjectContext.Warehouses.SingleOrDefault(e => e.Id == partsShippingOrder.WarehouseId).Address;
                //virtualPartsShippingOrderRef.PartsSalesCategoryName = partsSalesCategory.Name;
                //virtualPartsShippingOrderRef.CreatorName = partsShippingOrder.CreatorName;
                //virtualPartsShippingOrderRef.CreateTime = partsShippingOrder.CreateTime;
                virtualPartsShippingOrderRef.BranchName = branch.Name;
                if(partsShippingOrder.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件销售订单 && partsShippingOrder.OriginalRequirementBillId != default(int)) {
                    var partsSalesOrder = ObjectContext.PartsSalesOrders.SingleOrDefault(r => r.Id == partsShippingOrder.OriginalRequirementBillId);
                    if(partsSalesOrder != null) {
                        virtualPartsShippingOrderRef.PartsSalesOrderTypeName = partsSalesOrder.PartsSalesOrderTypeName;                       
                    }
                }
                //查询收货单位的联系人联系电话
                var partsSale = (from a in ObjectContext.PartsShippingOrders.Where(r => r.Id == PartsShippingOrderId)
                                 join b in ObjectContext.PartsShippingOrderDetails on a.Id equals b.PartsShippingOrderId
                                join c in ObjectContext.PartsOutboundPlans on b.PartsOutboundPlanId equals c.Id
                                join d in ObjectContext.PartsSalesOrders on c.OriginalRequirementBillId equals d.Id 
                                select new{
                                    ContactPerson = d.ContactPerson,
                                    ContactPhone = d.ContactPhone
                                }).ToList();
                if(partsSale.Count>0){
                    virtualPartsShippingOrderRef.ContactPerson = partsSale.First().ContactPerson;
                    virtualPartsShippingOrderRef.ContactPhone = partsSale.First().ContactPhone;
                }
            }
            return virtualPartsShippingOrderRef;
        }

        public VirtualPartsShippingOrderRef GetVirtualAgencyPartsShippingOrderRefById(int PartsShippingOrderId) {
            var virtualPartsShippingOrderRef = new VirtualPartsShippingOrderRef();
            var partsShippingOrderRef = ObjectContext.AgencyPartsShippingOrderRefs.Where(e => e.PartsShippingOrderId == PartsShippingOrderId).ToArray();
            var PartsOutboundBillIds = partsShippingOrderRef.Select(r => r.PartsOutboundBillId).ToArray();
            if(partsShippingOrderRef != null) {
                var partsOutboundBill = ObjectContext.AgencyPartsOutboundBills.Where(e => PartsOutboundBillIds.Contains(e.Id)).ToArray();
                var partsShippingOrder = ObjectContext.AgencyPartsShippingOrders.SingleOrDefault(e => e.Id == PartsShippingOrderId);
                var branch = ObjectContext.Branches.SingleOrDefault(e => e.Id == partsShippingOrder.BranchId);
                var receiving = ObjectContext.Companies.SingleOrDefault(e => e.Id == partsShippingOrder.ReceivingCompanyId);
                var logistic = ObjectContext.Companies.SingleOrDefault(e => e.Code == partsShippingOrder.LogisticCompanyCode && e.Name == partsShippingOrder.LogisticCompanyName);
                var partsSalesCategory = ObjectContext.PartsSalesCategories.SingleOrDefault(e => e.Id == partsShippingOrder.PartsSalesCategoryId);
                virtualPartsShippingOrderRef.PartsShippingOrderCode = partsShippingOrder.Code;
                virtualPartsShippingOrderRef.ReceivingCompanyCode = partsShippingOrder.ReceivingCompanyCode;
                virtualPartsShippingOrderRef.ReceivingCompanyName = partsShippingOrder.ReceivingCompanyName;
                // virtualPartsShippingOrderRef.ReceivingAddress = partsShippingOrder.ReceivingAddress;
                virtualPartsShippingOrderRef.ReceivingPhone = receiving.ContactPhone;
                virtualPartsShippingOrderRef.ReceivingPerson = receiving.ContactPerson;
                if(logistic != null) {
                    virtualPartsShippingOrderRef.LogisticCompanyName = logistic.Name;
                    virtualPartsShippingOrderRef.LogisticCompanyPhone = logistic.ContactPhone;
                    virtualPartsShippingOrderRef.LogisticCompanyPerson = logistic.ContactPerson;
                } else {
                    virtualPartsShippingOrderRef.LogisticCompanyName = partsShippingOrder.LogisticCompanyName;
                    virtualPartsShippingOrderRef.LogisticCompanyPhone = string.Empty;
                    virtualPartsShippingOrderRef.LogisticCompanyPerson = string.Empty;
                }
                virtualPartsShippingOrderRef.ReceivingWarehouseName = partsShippingOrder.ReceivingWarehouseName;
                virtualPartsShippingOrderRef.WarehouseName = partsShippingOrder.WarehouseName;
                var warehouse = ObjectContext.Warehouses.SingleOrDefault(r => r.Id == partsShippingOrder.WarehouseId);
                if(warehouse != null) {
                    virtualPartsShippingOrderRef.PhoneNumber = warehouse.PhoneNumber;
                    virtualPartsShippingOrderRef.Contact = warehouse.Contact;
                }
                virtualPartsShippingOrderRef.ShippingCompanyName = ObjectContext.Companies.SingleOrDefault(e => e.Id == partsShippingOrder.ShippingCompanyId).Name;
                virtualPartsShippingOrderRef.ShippingMethod = partsShippingOrder.ShippingMethod;
                virtualPartsShippingOrderRef.OriginalRequirementBillType = partsShippingOrder.OriginalRequirementBillType;
                virtualPartsShippingOrderRef.OriginalRequirementBillCode = partsShippingOrder.OriginalRequirementBillCode;
                virtualPartsShippingOrderRef.ShippingDate = partsShippingOrder.ShippingDate;
                virtualPartsShippingOrderRef.Type = partsShippingOrder.Type;
                virtualPartsShippingOrderRef.Remark = partsShippingOrder.Remark;
                virtualPartsShippingOrderRef.RequestedArrivalDate = partsShippingOrder.RequestedArrivalDate;
                virtualPartsShippingOrderRef.TransportMileage = partsShippingOrder.TransportMileage;
                int i = 1;
                string partsOutboundBillCodes = "";
                foreach(var p in partsOutboundBill) {
                    if(i == partsOutboundBill.Length) {
                        partsOutboundBillCodes = partsOutboundBillCodes + p.Code;
                    } else {
                        partsOutboundBillCodes = partsOutboundBillCodes + p.Code + ",";
                    }
                    i++;
                }
                virtualPartsShippingOrderRef.PartsOutboundBillCode = partsOutboundBillCodes;
                virtualPartsShippingOrderRef.OrderApproveComment = partsShippingOrder.OrderApproveComment;
                virtualPartsShippingOrderRef.OutboundBillWarehouseAddress = ObjectContext.Warehouses.SingleOrDefault(e => e.Id == partsShippingOrder.WarehouseId).Address;
                virtualPartsShippingOrderRef.PartsSalesCategoryName = partsSalesCategory.Name;
                virtualPartsShippingOrderRef.CreatorName = partsShippingOrder.CreatorName;
                virtualPartsShippingOrderRef.CreateTime = partsShippingOrder.CreateTime;
                virtualPartsShippingOrderRef.BranchName = branch.Name;
                if(partsShippingOrder.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件销售订单 && partsShippingOrder.OriginalRequirementBillId != default(int)) {
                    var partsSalesOrder = ObjectContext.PartsSalesOrders.SingleOrDefault(r => r.Id == partsShippingOrder.OriginalRequirementBillId);
                    if(partsSalesOrder != null) {
                        virtualPartsShippingOrderRef.PartsSalesOrderTypeName = partsSalesOrder.PartsSalesOrderTypeName;
                        virtualPartsShippingOrderRef.ReceivingPhone = partsSalesOrder.ContactPhone;
                        virtualPartsShippingOrderRef.ReceivingPerson = partsSalesOrder.ContactPerson;
                        virtualPartsShippingOrderRef.ReceivingAddress = partsSalesOrder.ReceivingAddress;
                        virtualPartsShippingOrderRef.Remark = partsShippingOrder.Remark != null ? string.Format("{0},{1}", partsShippingOrder.Remark, partsSalesOrder.Remark) : partsSalesOrder.Remark;
                    }
                }
            }
            return virtualPartsShippingOrderRef;
        }
        public VirtualPartsShippingOrderRef GetVirtualPartsShippingOrderRefById1(int PartsShippingOrderId) {
            var virtualPartsShippingOrderRef = new VirtualPartsShippingOrderRef();
            var partsShippingOrderRef = ObjectContext.PartsShippingOrderRefs.Where(e => e.PartsShippingOrderId == PartsShippingOrderId).ToArray();
            var PartsOutboundBillIds = partsShippingOrderRef.Select(r => r.PartsOutboundBillId).ToArray();
            if(partsShippingOrderRef != null) {
                var partsOutboundBill = ObjectContext.PartsOutboundBills.Where(e => PartsOutboundBillIds.Contains(e.Id)).ToArray();
                var partsShippingOrder = ObjectContext.PartsShippingOrders.SingleOrDefault(e => e.Id == PartsShippingOrderId);
                var branch = ObjectContext.Branches.SingleOrDefault(e => e.Id == partsShippingOrder.BranchId);
                var receiving = ObjectContext.Companies.SingleOrDefault(e => e.Id == partsShippingOrder.ReceivingCompanyId);
                var logistic = ObjectContext.Companies.SingleOrDefault(e => e.Code == partsShippingOrder.LogisticCompanyCode && e.Name == partsShippingOrder.LogisticCompanyName);
                var partsSalesCategory = ObjectContext.PartsSalesCategories.SingleOrDefault(e => e.Id == partsShippingOrder.PartsSalesCategoryId);
                virtualPartsShippingOrderRef.PartsShippingOrderCode = partsShippingOrder.Code;
                virtualPartsShippingOrderRef.ReceivingCompanyCode = partsShippingOrder.ReceivingCompanyCode;
                virtualPartsShippingOrderRef.ReceivingCompanyName = partsShippingOrder.ReceivingCompanyName;
                virtualPartsShippingOrderRef.ReceivingAddress = partsShippingOrder.ReceivingAddress;
                virtualPartsShippingOrderRef.ReceivingPhone = receiving.ContactPhone;
                virtualPartsShippingOrderRef.ReceivingPerson = receiving.ContactPerson;
                if(logistic != null) {
                    virtualPartsShippingOrderRef.LogisticCompanyName = logistic.Name;
                    virtualPartsShippingOrderRef.LogisticCompanyPhone = logistic.ContactPhone;
                    virtualPartsShippingOrderRef.LogisticCompanyPerson = logistic.ContactPerson;
                } else {
                    virtualPartsShippingOrderRef.LogisticCompanyName = partsShippingOrder.LogisticCompanyName;
                    virtualPartsShippingOrderRef.LogisticCompanyPhone = string.Empty;
                    virtualPartsShippingOrderRef.LogisticCompanyPerson = string.Empty;
                }
                virtualPartsShippingOrderRef.ReceivingWarehouseName = partsShippingOrder.ReceivingWarehouseName;
                virtualPartsShippingOrderRef.WarehouseName = partsShippingOrder.WarehouseName;
                var warehouse = ObjectContext.Warehouses.SingleOrDefault(r => r.Id == partsShippingOrder.WarehouseId);
                if(warehouse != null) {
                    virtualPartsShippingOrderRef.PhoneNumber = warehouse.PhoneNumber;
                    virtualPartsShippingOrderRef.Contact = warehouse.Contact;
                }
                virtualPartsShippingOrderRef.ShippingCompanyName = ObjectContext.Companies.SingleOrDefault(e => e.Id == partsShippingOrder.ShippingCompanyId).Name;
                virtualPartsShippingOrderRef.ShippingMethod = partsShippingOrder.ShippingMethod;
                virtualPartsShippingOrderRef.OriginalRequirementBillType = partsShippingOrder.OriginalRequirementBillType;
                virtualPartsShippingOrderRef.OriginalRequirementBillCode = partsShippingOrder.OriginalRequirementBillCode;
                virtualPartsShippingOrderRef.ShippingDate = partsShippingOrder.ShippingDate;
                virtualPartsShippingOrderRef.Type = partsShippingOrder.Type;
                virtualPartsShippingOrderRef.Remark = partsShippingOrder.Remark;
                virtualPartsShippingOrderRef.RequestedArrivalDate = partsShippingOrder.RequestedArrivalDate;
                virtualPartsShippingOrderRef.TransportMileage = partsShippingOrder.TransportMileage;
                virtualPartsShippingOrderRef.PartsOutboundBillCode = string.Join(",", partsOutboundBill.Select(r => r.Code).Distinct());
                var partsOutboundPlanCodes = from a in ObjectContext.PartsOutboundPlans
                                             join b in ObjectContext.PartsOutboundBills.Where(e => PartsOutboundBillIds.Contains(e.Id)) on a.Id equals b.PartsOutboundPlanId
                                             select a.Code;
                virtualPartsShippingOrderRef.PartsOutboundPlanCode = string.Join(",", partsOutboundPlanCodes.Distinct());
                virtualPartsShippingOrderRef.OrderApproveComment = partsShippingOrder.OrderApproveComment;
                virtualPartsShippingOrderRef.OutboundBillWarehouseAddress = ObjectContext.Warehouses.SingleOrDefault(e => e.Id == partsShippingOrder.WarehouseId).Address;
                virtualPartsShippingOrderRef.PartsSalesCategoryName = partsSalesCategory.Name;
                virtualPartsShippingOrderRef.CreatorName = partsShippingOrder.CreatorName;
                virtualPartsShippingOrderRef.CreateTime = partsShippingOrder.CreateTime;
                virtualPartsShippingOrderRef.BranchName = branch.Name;
                if(partsShippingOrder.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件销售订单 && partsShippingOrder.OriginalRequirementBillId != default(int)) {
                    var partsSalesOrder = ObjectContext.PartsSalesOrders.SingleOrDefault(r => r.Id == partsShippingOrder.OriginalRequirementBillId);
                    if(partsSalesOrder != null) {
                        virtualPartsShippingOrderRef.PartsSalesOrderTypeName = partsSalesOrder.PartsSalesOrderTypeName;
                        virtualPartsShippingOrderRef.ReceivingPhone = partsSalesOrder.ContactPhone;
                        virtualPartsShippingOrderRef.ReceivingPerson = partsSalesOrder.ContactPerson;
                        virtualPartsShippingOrderRef.Remark = partsShippingOrder.Remark != null ? string.Format("{0},{1}", partsShippingOrder.Remark, partsSalesOrder.Remark) : partsSalesOrder.Remark;
                    }
                }
            }
            return virtualPartsShippingOrderRef;
        }
        //配件分品牌商务信息管理打印配件标签
        [Query(HasSideEffects = true)]
        public IQueryable<SparePart> GetSparePartByIds(int[] ids) {
            return ObjectContext.SpareParts.Where(r => ids.Contains(r.Id)).OrderBy(r => r.Code);
        }        

        public string bsubstring(string s, int length) {
            int target = 0;
            int b = 0;
            if(s == null)
                return s;
            for(int i = 0; i < s.Length; i++) {
                var check = Regex.IsMatch(s[i].ToString(), @"[\u4e00-\u9fbb]+$");
                var temp = check ? 2 : 1;
                if(b + temp > length) {
                    target = i;
                    break;
                } else {
                    b = b + temp;
                }
            }
            if(target == 0) {
                target = s.Length;
            }
            var result = s.Substring(0, target);
            return result;
        }
                

        //销售结算发票清单打印
        public IEnumerable<InvoiceInformation> GetInvoiceInformationForPrint(int partsSalesSettlementId) {
            var partsSalesSettlement = ObjectContext.PartsSalesSettlements.SingleOrDefault(r => r.Id == partsSalesSettlementId);
            var invoiceInformation = ObjectContext.InvoiceInformations.Where(r => r.SourceCode == partsSalesSettlement.Code && r.Status != (int)DcsInvoiceInformationStatus.作废).ToArray();
            foreach(var information in invoiceInformation) {
                if(partsSalesSettlement.PartsSalesCategoryName.Contains("北区")) {
                    information.InvoiceCompanyName = "北汽红岩汽车股份有限公司诸城汽车厂";
                } else if(partsSalesSettlement.PartsSalesCategoryName.Contains("南区")) {
                    information.InvoiceCompanyName = "北汽红岩汽车股份有限公司长沙汽车厂";
                }
            }
            return invoiceInformation.OrderBy(r => r.Id);
        }

        public IEnumerable<PartsSalesSettlementDetail> GetPartsSalesSettlementDetailByIdForPrint(int partsSalesSettlementId) {
            var result = ObjectContext.PartsSalesSettlementDetails.Where(e => e.PartsSalesSettlementId == partsSalesSettlementId).ToArray();

            foreach(var item in result) {
                item.SparePartName = this.bsubstring(item.SparePartName, 32);
                //item.SparePartCode = this.bsubstring(item.SparePartCode, 25);
                item.SparePartCode = this.bsubstring(item.SparePartCode, 22);
            }
            return result;
        }

        //配件采购结算管理 工程车打印
        public PartsPurchaseSettleBill GetPartsPurchaseSettleBillById(int id) {
            var partsPurchaseSettleBill = ObjectContext.PartsPurchaseSettleBills.Single(r => r.Id == id);

            partsPurchaseSettleBill.SumSettlementAmount = this.ObjectContext.PartsPurchaseSettleRefs.Where(r => r.PartsPurchaseSettleBillId == id).Sum(r => r.SettlementAmount);

            return partsPurchaseSettleBill;
        }

        public IEnumerable<PartsPurchaseSettleRef> GetPartsPurchaseSettleRefsBySettleId(int settleId) {
            var partsPurchaseSettleRef = this.ObjectContext.PartsPurchaseSettleRefs.Where(r => r.PartsPurchaseSettleBillId == settleId).ToArray();
            foreach(var purchaseSettleRef in partsPurchaseSettleRef) {
                if(purchaseSettleRef.SourceType == (int)DcsPartsPurchaseSettleRefSourceType.配件入库检验单) {
                    purchaseSettleRef.PlannedAmount = ObjectContext.PartsInboundCheckBillDetails.Where(r => r.PartsInboundCheckBillId == purchaseSettleRef.SourceId).Sum(r => r.CostPrice * r.InspectedQuantity);
                } else if(purchaseSettleRef.SourceType == (int)DcsPartsPurchaseSettleRefSourceType.配件出库单) {
                    purchaseSettleRef.PlannedAmount = ObjectContext.PartsOutboundBillDetails.Where(r => r.PartsOutboundBillId == purchaseSettleRef.SourceId).Sum(r => -(r.CostPrice) * r.OutboundAmount);
                }
            }
            return partsPurchaseSettleRef.OrderBy(r => r.SourceCode);
        }

        public UsedPartsInboundOrder GetUsedPartsInboundOrderById(int id) {
            var usedPartsInboundOrder = this.ObjectContext.UsedPartsInboundOrders.SingleOrDefault(r => r.Id == id);
            var usedPartsShippingOrder = this.ObjectContext.UsedPartsShippingOrders.SingleOrDefault(r => r.Code == usedPartsInboundOrder.SourceCode);
            if(usedPartsShippingOrder != null) {
                usedPartsInboundOrder.ReceptionRemark = usedPartsShippingOrder.ReceptionRemark;
            }
            return usedPartsInboundOrder;
        }

        public IEnumerable<UsedPartsInboundDetail> GetUsedPartsInboundDetailByUsedPartsInboundOrderId(int usuedPartsInboundOrderId) {
            var usedPartsInboundDetails = ObjectContext.UsedPartsInboundDetails.Where(r => r.UsedPartsInboundOrderId == usuedPartsInboundOrderId).ToArray();
            int Number = 1;
            foreach(var partsInboundDetail in usedPartsInboundDetails) {
                partsInboundDetail.Number = Number;
                Number++;
            }
            return usedPartsInboundDetails;
        }

        public SupplierPreApprovedLoan GetSupplierPreApprovedLoanById(int supplierPreApprovedLoanId) {
            return this.ObjectContext.SupplierPreApprovedLoans.SingleOrDefault(r => r.Id == supplierPreApprovedLoanId);
        }

        public IQueryable<SupplierPreAppLoanDetail> GetSupplierPreAppLoanDetailForPrint(int supplierPreApprovedLoanId, string pintType) {
            switch(pintType) {
                case "PrintAll":
                    return this.ObjectContext.SupplierPreAppLoanDetails.Where(r => r.SupplierPreApprovedLoanId == supplierPreApprovedLoanId).OrderBy(r => r.SupplierCode);
                case "ZeroPrint":
                    return this.ObjectContext.SupplierPreAppLoanDetails.Where(r => r.SupplierPreApprovedLoanId == supplierPreApprovedLoanId && (r.ApprovalAmount == 0 || r.ApprovalAmount == null)).OrderBy(r => r.SupplierCode);
                case "NonZeroPrint":
                    return this.ObjectContext.SupplierPreAppLoanDetails.Where(r => r.SupplierPreApprovedLoanId == supplierPreApprovedLoanId && r.ApprovalAmount > 0).OrderBy(r => r.SupplierCode);
            }
            return null;

        }

        public UsedPartsTransferOrder GetUsedPartsTransferOrderById(int usedPartsTransferOrderId) {
            var usedPartsTransferOrder = this.ObjectContext.UsedPartsTransferOrders.SingleOrDefault(r => r.Id == usedPartsTransferOrderId);
            var usedPartsWarehouse = this.ObjectContext.UsedPartsWarehouses.SingleOrDefault(r => r.Id == usedPartsTransferOrder.OriginWarehouseId);
            if(usedPartsWarehouse != null) {
                var branch = this.ObjectContext.Branches.SingleOrDefault(r => r.Id == usedPartsWarehouse.BranchId);
                usedPartsTransferOrder.ContactPhone = usedPartsWarehouse.ContactPhone;
                usedPartsTransferOrder.Contact = usedPartsWarehouse.Contact;
                if(branch != null) {
                    usedPartsTransferOrder.OriginWarehouseCompanyCode = branch.Code;
                    usedPartsTransferOrder.OriginWarehouseCompanyName = branch.Name;
                }
            }
            var warehouse = this.ObjectContext.UsedPartsWarehouses.SingleOrDefault(r => r.Id == usedPartsTransferOrder.DestinationWarehouseId);
            if(warehouse != null) {
                var branch = this.ObjectContext.Branches.SingleOrDefault(r => r.Id == usedPartsWarehouse.BranchId);
                if(branch != null) {
                    usedPartsTransferOrder.DestinationWarehouseCompanyCode = branch.Code;
                    usedPartsTransferOrder.DestinationWarehouseCompanyName = branch.Name;
                }
            }
            return usedPartsTransferOrder;
        }

        public IEnumerable<UsedPartsTransferDetail> GetUsedPartsTransferDetailById(int usedPartsTransferOrderId) {
            var usedPartsTransferDetails = this.ObjectContext.UsedPartsTransferDetails.Where(r => r.UsedPartsTransferOrderId == usedPartsTransferOrderId);
            int Number = 1;
            foreach(var partsInboundDetail in usedPartsTransferDetails) {
                partsInboundDetail.IfFaultyParts = partsInboundDetail.IfFaultyParts ?? false;
                partsInboundDetail.Number = Number;
                Number++;
            }
            return usedPartsTransferDetails;
        }

        //旧件调拨单条码打印
        public IEnumerable<UsedPartsTransferDetail> GetUsedPartsTransferOrderWithLableForPrint(int usedPartsTransferOrderId) {
            var usedPartsTransferDetails = ObjectContext.UsedPartsTransferDetails.Where(e => e.UsedPartsTransferOrderId == usedPartsTransferOrderId).ToArray();
            var usedPartsBarCodes = usedPartsTransferDetails.Select(r => r.UsedPartsBarCode).ToArray();
            //var repairOrderMaterialDetails = ObjectContext.RepairOrderMaterialDetails.Include("RepairOrderItemDetail.RepairOrderFaultReason").Where(r => usedPartsBarCodes.Contains(r.UsedPartsBarCode)).ToArray();
            var usedPartsTransferOrder = this.ObjectContext.UsedPartsTransferOrders.SingleOrDefault(r => r.Id == usedPartsTransferOrderId);
            var usedPartsWarehouse = this.ObjectContext.UsedPartsWarehouses.SingleOrDefault(r => r.Id == usedPartsTransferOrder.OriginWarehouseId);
            var partsSalesCategoryName = "";
            if(usedPartsWarehouse != null) {
                partsSalesCategoryName = this.ObjectContext.PartsSalesCategories.SingleOrDefault(r => r.Id == usedPartsWarehouse.PartsSalesCategoryId).Name;
            }
            foreach(var usedPartsTransferDetail in usedPartsTransferDetails) {
                //var repairClaimBill = this.ObjectContext.RepairClaimBills.SingleOrDefault(r => r.Id == usedPartsTransferDetail.ClaimBillId);
                //var malfunctionReason = repairOrderMaterialDetails.FirstOrDefault(r => r.UsedPartsBarCode == usedPartsTransferDetail.UsedPartsBarCode);
                //usedPartsTransferDetail.PartsSalesCategoryName = partsSalesCategoryName;
                //if(repairClaimBill != null)
                //    usedPartsTransferDetail.RepairClaimBillVIN = repairClaimBill.VIN;
                //if(malfunctionReason != null)
                //    usedPartsTransferDetail.MalfunctionReason = malfunctionReason.RepairOrderItemDetail.RepairOrderFaultReason.MalfunctionReason;
            }
            return usedPartsTransferDetails;
        }

        private class TempClassForPrint {
            public int PartId {
                get;
                set;
            }

            public string PositionCode {
                get;
                set;
            }

            public string TopLvAreaCode {
                get;
                set;
            }

            public string MeasureUnit {
                get;
                set;
            }

        }

        public IEnumerable<PartsOutboundPlanDetailWithPosition> GetPartsOutboundPlanForSplitArea(int partsOutboundPlanId) {
            var partsOutboundPlan = ObjectContext.PartsOutboundPlans.SingleOrDefault(r => r.Id == partsOutboundPlanId);
            if(partsOutboundPlan == null) {
                return null;
            }
            var partsOutboundPlanDetails = ObjectContext.PartsOutboundPlanDetails.Where(r => r.PartsOutboundPlanId == partsOutboundPlanId).ToArray();
            var partsOutboundPlanDetailWithPositions = new List<PartsOutboundPlanDetailWithPosition>();
            var sparePartIds = partsOutboundPlanDetails.Select(r => r.SparePartId).ToArray();
            var partsStockStruct = from a in ObjectContext.PartsStocks.Where(r => r.WarehouseId == partsOutboundPlan.WarehouseId && sparePartIds.Contains(r.PartId))
                                   join b in ObjectContext.WarehouseAreas.Where(r => r.WarehouseId == partsOutboundPlan.WarehouseId && r.AreaKind == (int)DcsAreaKind.库位) on a.WarehouseAreaId equals b.Id
                                   join c in ObjectContext.WarehouseAreaCategories.Where(r => r.Category == (int)DcsAreaType.保管区) on b.AreaCategoryId equals c.Id
                                   join d in ObjectContext.WarehouseAreas.Where(r => r.AreaKind == (int)DcsAreaKind.库区) on b.ParentId equals d.Id into tempTable1
                                   from t1 in tempTable1.DefaultIfEmpty()
                                   select new {
                                       a.PartId,
                                       PositionCode = b.Code,
                                       TopLvAreaCode = t1.Code
                                   };
            var structPart = (from t in
                                  (from a in ObjectContext.SpareParts.Where(r => sparePartIds.Contains(r.Id))
                                   join b in partsStockStruct on a.Id equals b.PartId into tempTable
                                   from t in tempTable.DefaultIfEmpty()
                                   select new {
                                       PartId = a.Id,
                                       MeasureUnit = a.MeasureUnit ?? "",
                                       PositionCode = t.PositionCode ?? "",
                                       TopLvAreaCode = t.TopLvAreaCode ?? ""
                                   })
                              group t by new {
                                  t.PartId,
                                  t.MeasureUnit,
                                  t.TopLvAreaCode
                              }
                                  into tempTableGroup
                                  select new TempClassForPrint {
                                      PartId = tempTableGroup.Key.PartId,
                                      MeasureUnit = tempTableGroup.Key.MeasureUnit,
                                      TopLvAreaCode = tempTableGroup.Key.TopLvAreaCode,
                                      PositionCode = tempTableGroup.Min(v => v.PositionCode)
                                  }).ToArray();
            var tempIds1 = structPart.Where(r => r.TopLvAreaCode != "").Select(r => r.PartId).Distinct().ToArray();
            var tempIds2 = sparePartIds.Except(tempIds1).Distinct().ToArray();
            List<TempClassForPrint> objs = new List<TempClassForPrint>();
            foreach(var i in tempIds1) {
                var item = structPart.Where(r => r.TopLvAreaCode != "").Where(r => r.PartId == i).First();
                objs.Add(item);
            }
            foreach(var i in tempIds2) {
                var item = structPart.Where(r => r.TopLvAreaCode == "").Where(r => r.PartId == i).First();
                objs.Add(item);
            }
            foreach(var partsOutboundPlanDetail in partsOutboundPlanDetails) {
                var tempItems = objs.Where(r => r.PartId == partsOutboundPlanDetail.SparePartId).ToArray();
                if(tempItems.Length > 0) {
                    foreach(var item in tempItems) {
                        var partsOutboundPlanDetailWithPosition = new PartsOutboundPlanDetailWithPosition {
                            Id = partsOutboundPlan.Id,
                            ReceivingWarehouseName = partsOutboundPlan.ReceivingWarehouseName,
                            Code = partsOutboundPlan.Code,
                            PartsSalesOrderTypeName = partsOutboundPlan.PartsSalesOrderTypeName,
                            PlannedAmount = partsOutboundPlanDetail.PlannedAmount,
                            WarehouseCode = partsOutboundPlan.WarehouseCode,
                            BranchName = partsOutboundPlan.BranchName,
                            CounterpartCompanyName = partsOutboundPlan.CounterpartCompanyName,
                            OutboundType = partsOutboundPlan.OutboundType,
                            ShippingMethod = partsOutboundPlan.ShippingMethod,
                            SourceCode = partsOutboundPlan.SourceCode,
                            PartsSalesCategoryName = partsOutboundPlan.PartsSalesCategory.Name,
                            StorageCompanyName = partsOutboundPlan.StorageCompanyName,
                            CreatorId = partsOutboundPlan.CreatorId,
                            CreatorName = partsOutboundPlan.CreatorName,
                            CreateTime = partsOutboundPlan.CreateTime,
                            SparePartId = partsOutboundPlanDetail.SparePartId,
                            SparePartCode = partsOutboundPlanDetail.SparePartCode,
                            SparePartName = partsOutboundPlanDetail.SparePartName,
                            OutboundFulfillment = partsOutboundPlanDetail.OutboundFulfillment,
                            Remark = partsOutboundPlanDetail.Remark,
                            TopLevelAreaCode = item.TopLvAreaCode,
                            PositionCode = item.PositionCode,
                            MeasureUnit = item.MeasureUnit

                        };
                        partsOutboundPlanDetailWithPositions.Add(partsOutboundPlanDetailWithPosition);
                    }
                }
            }
            return partsOutboundPlanDetailWithPositions.OrderBy(r => r.PositionCode);


        }

        public IEnumerable<PartsOutboundPlanDetailWithPosition> GetPartsOutboundPlanNotPartitionPrintForSplitArea(int partsOutboundPlanId) {
            var userInfo = Utils.GetCurrentUserInfo();
            var partsOutboundPlan = ObjectContext.PartsOutboundPlans.Include("PartsSalesCategory").SingleOrDefault(r => r.Id == partsOutboundPlanId);
            if(partsOutboundPlan == null) {
                return null;
            }
            var partsOutboundPlanDetails = ObjectContext.PartsOutboundPlanDetails.Where(r => r.PartsOutboundPlanId == partsOutboundPlanId).ToArray();
            var partsOutboundPlanDetailWithPositions = new List<PartsOutboundPlanDetailWithPosition>();
            var sparePartIds = partsOutboundPlanDetails.Select(r => r.SparePartId).ToArray();
            var partsStockStruct = from a in ObjectContext.PartsStocks.Where(r => r.WarehouseId == partsOutboundPlan.WarehouseId && sparePartIds.Contains(r.PartId))
                                   join b in ObjectContext.WarehouseAreas.Where(r => r.WarehouseId == partsOutboundPlan.WarehouseId && r.AreaKind == (int)DcsAreaKind.库位) on a.WarehouseAreaId equals b.Id
                                   join c in ObjectContext.WarehouseAreaCategories.Where(r => r.Category == (int)DcsAreaType.保管区) on b.AreaCategoryId equals c.Id
                                   join d in ObjectContext.WarehouseAreas.Where(r => r.AreaKind == (int)DcsAreaKind.库区) on b.ParentId equals d.Id into tempTable1
                                   from t1 in tempTable1.DefaultIfEmpty()
                                   join e in ObjectContext.WarehouseAreas on b.Id equals e.TopLevelWarehouseAreaId into tempTable2
                                   from t2 in tempTable2.DefaultIfEmpty()
                                   join f in ObjectContext.WarehouseAreaManagers.Where(r => r.ManagerId == userInfo.Id) on t2.Id equals f.WarehouseAreaId
                                   select new {
                                       a.PartId,
                                       PositionCode = b.Code,
                                       TopLvAreaCode = t1.Code
                                   };
            var structPart = (from t in
                                  (from a in ObjectContext.SpareParts.Where(r => sparePartIds.Contains(r.Id))
                                   join b in partsStockStruct on a.Id equals b.PartId into tempTable
                                   from t in tempTable.DefaultIfEmpty()
                                   select new {
                                       PartId = a.Id,
                                       MeasureUnit = a.MeasureUnit ?? "",
                                       PositionCode = t.PositionCode ?? "",
                                       TopLvAreaCode = t.TopLvAreaCode ?? ""
                                   })
                              group t by new {
                                  t.PartId,
                                  t.MeasureUnit,
                                  t.TopLvAreaCode
                              }
                                  into tempTableGroup
                                  select new TempClassForPrint {
                                      PartId = tempTableGroup.Key.PartId,
                                      MeasureUnit = tempTableGroup.Key.MeasureUnit,
                                      TopLvAreaCode = tempTableGroup.Key.TopLvAreaCode,
                                      PositionCode = tempTableGroup.Min(v => v.PositionCode)
                                  }).ToArray();
            var tempIds1 = structPart.Where(r => r.TopLvAreaCode != "").Select(r => r.PartId).Distinct().ToArray();
            var tempIds2 = sparePartIds.Except(tempIds1).Distinct().ToArray();
            List<TempClassForPrint> objs = new List<TempClassForPrint>();
            foreach(var i in tempIds1) {
                var item = structPart.Where(r => r.TopLvAreaCode != "").Where(r => r.PartId == i).First();
                objs.Add(item);
            }
            foreach(var i in tempIds2) {
                var item = structPart.Where(r => r.TopLvAreaCode == "").Where(r => r.PartId == i).First();
                objs.Add(item);
            }
            foreach(var partsOutboundPlanDetail in partsOutboundPlanDetails) {
                var tempItems = objs.Where(r => r.PartId == partsOutboundPlanDetail.SparePartId).ToArray();
                if(tempItems.Length > 0) {
                    foreach(var item in tempItems) {
                        var partsOutboundPlanDetailWithPosition = new PartsOutboundPlanDetailWithPosition {
                            Id = partsOutboundPlan.Id,
                            ReceivingWarehouseName = partsOutboundPlan.ReceivingWarehouseName,
                            Code = partsOutboundPlan.Code,
                            PartsSalesOrderTypeName = partsOutboundPlan.PartsSalesOrderTypeName,
                            PlannedAmount = partsOutboundPlanDetail.PlannedAmount,
                            WarehouseCode = partsOutboundPlan.WarehouseCode,
                            BranchName = partsOutboundPlan.BranchName,
                            CounterpartCompanyName = partsOutboundPlan.CounterpartCompanyName,
                            OutboundType = partsOutboundPlan.OutboundType,
                            ShippingMethod = partsOutboundPlan.ShippingMethod,
                            SourceCode = partsOutboundPlan.SourceCode,
                            PartsSalesCategoryName = partsOutboundPlan.PartsSalesCategory.Name,
                            StorageCompanyName = partsOutboundPlan.StorageCompanyName,
                            CreatorId = partsOutboundPlan.CreatorId,
                            CreatorName = partsOutboundPlan.CreatorName,
                            CreateTime = partsOutboundPlan.CreateTime,
                            SparePartId = partsOutboundPlanDetail.SparePartId,
                            SparePartCode = partsOutboundPlanDetail.SparePartCode,
                            SparePartName = partsOutboundPlanDetail.SparePartName,
                            OutboundFulfillment = partsOutboundPlanDetail.PlannedAmount - (partsOutboundPlanDetail.OutboundFulfillment ?? 0),
                            Remark = partsOutboundPlanDetail.Remark,
                            TopLevelAreaCode = item.TopLvAreaCode,
                            PositionCode = item.PositionCode,
                            MeasureUnit = item.MeasureUnit

                        };
                        partsOutboundPlanDetailWithPositions.Add(partsOutboundPlanDetailWithPosition);
                    }
                }
            }
            return partsOutboundPlanDetailWithPositions.Where(p => p.OutboundFulfillment > 0).OrderBy(r => r.PositionCode);


        }

        public IEnumerable<PartsOutboundPlanDetailWithPosition> GetPartsOutboundPlanNotWarehouse(int partsOutboundPlanId) {
            var partsOutboundPlan = ObjectContext.PartsOutboundPlans.Include("PartsSalesCategory").SingleOrDefault(r => r.Id == partsOutboundPlanId);
            if(partsOutboundPlan == null) {
                return null;
            }
            var partsOutboundPlanDetails = ObjectContext.PartsOutboundPlanDetails.Where(r => r.PartsOutboundPlanId == partsOutboundPlanId).ToArray();
            var partsOutboundPlanDetailWithPositions = new List<PartsOutboundPlanDetailWithPosition>();
            var sparePartIds = partsOutboundPlanDetails.Select(r => r.SparePartId).ToArray();
            var partsStockStruct = from a in ObjectContext.PartsStocks.Where(r => r.WarehouseId == partsOutboundPlan.WarehouseId && sparePartIds.Contains(r.PartId))
                                   join b in ObjectContext.WarehouseAreas.Where(r => r.WarehouseId == partsOutboundPlan.WarehouseId && r.AreaKind == (int)DcsAreaKind.库位) on a.WarehouseAreaId equals b.Id
                                   join c in ObjectContext.WarehouseAreaCategories.Where(r => r.Category == (int)DcsAreaType.保管区) on b.AreaCategoryId equals c.Id
                                   join d in ObjectContext.WarehouseAreas.Where(r => r.AreaKind == (int)DcsAreaKind.库区) on b.ParentId equals d.Id into tempTable1
                                   from t1 in tempTable1.DefaultIfEmpty()
                                   select new {
                                       a.PartId,
                                       PositionCode = b.Code,
                                       TopLvAreaCode = t1.Code
                                   };
            var structPart = (from t in
                                  (from a in ObjectContext.SpareParts.Where(r => sparePartIds.Contains(r.Id))
                                   join b in partsStockStruct on a.Id equals b.PartId into tempTable
                                   from t in tempTable.DefaultIfEmpty()
                                   select new {
                                       PartId = a.Id,
                                       MeasureUnit = a.MeasureUnit ?? "",
                                       PositionCode = t.PositionCode ?? "",
                                       TopLvAreaCode = t.TopLvAreaCode ?? ""
                                   })
                              group t by new {
                                  t.PartId,
                                  t.MeasureUnit,
                                  t.TopLvAreaCode
                              }
                                  into tempTableGroup
                                  select new TempClassForPrint {
                                      PartId = tempTableGroup.Key.PartId,
                                      MeasureUnit = tempTableGroup.Key.MeasureUnit,
                                      TopLvAreaCode = tempTableGroup.Key.TopLvAreaCode,
                                      PositionCode = tempTableGroup.Min(v => v.PositionCode)
                                  }).ToArray();
            var tempIds1 = structPart.Where(r => r.TopLvAreaCode != "").Select(r => r.PartId).Distinct().ToArray();
            var tempIds2 = sparePartIds.Except(tempIds1).Distinct().ToArray();
            List<TempClassForPrint> objs = new List<TempClassForPrint>();
            foreach(var i in tempIds1) {
                var item = structPart.Where(r => r.TopLvAreaCode != "").Where(r => r.PartId == i).First();
                objs.Add(item);
            }
            foreach(var i in tempIds2) {
                var item = structPart.Where(r => r.TopLvAreaCode == "").Where(r => r.PartId == i).First();
                objs.Add(item);
            }
            foreach(var partsOutboundPlanDetail in partsOutboundPlanDetails) {
                var tempItems = objs.Where(r => r.PartId == partsOutboundPlanDetail.SparePartId).ToArray();
                if(tempItems.Length > 0) {
                    foreach(var item in tempItems) {
                        var partsOutboundPlanDetailWithPosition = new PartsOutboundPlanDetailWithPosition {
                            Id = partsOutboundPlan.Id,
                            ReceivingWarehouseName = partsOutboundPlan.ReceivingWarehouseName,
                            Code = partsOutboundPlan.Code,
                            PartsSalesOrderTypeName = partsOutboundPlan.PartsSalesOrderTypeName,
                            PlannedAmount = partsOutboundPlanDetail.PlannedAmount,
                            WarehouseCode = partsOutboundPlan.WarehouseCode,
                            BranchName = partsOutboundPlan.BranchName,
                            CounterpartCompanyName = partsOutboundPlan.CounterpartCompanyName,
                            OutboundType = partsOutboundPlan.OutboundType,
                            ShippingMethod = partsOutboundPlan.ShippingMethod,
                            SourceCode = partsOutboundPlan.SourceCode,
                            PartsSalesCategoryName = partsOutboundPlan.PartsSalesCategory.Name,
                            StorageCompanyName = partsOutboundPlan.StorageCompanyName,
                            CreatorId = partsOutboundPlan.CreatorId,
                            CreatorName = partsOutboundPlan.CreatorName,
                            CreateTime = partsOutboundPlan.CreateTime,
                            SparePartId = partsOutboundPlanDetail.SparePartId,
                            SparePartCode = partsOutboundPlanDetail.SparePartCode,
                            SparePartName = partsOutboundPlanDetail.SparePartName,
                            OutboundFulfillment = partsOutboundPlanDetail.PlannedAmount - (partsOutboundPlanDetail.OutboundFulfillment ?? 0),
                            Remark = partsOutboundPlanDetail.Remark,
                            TopLevelAreaCode = item.TopLvAreaCode,
                            PositionCode = item.PositionCode,
                            MeasureUnit = item.MeasureUnit

                        };
                        partsOutboundPlanDetailWithPositions.Add(partsOutboundPlanDetailWithPosition);
                    }
                }
            }
            var h = partsOutboundPlanDetailWithPositions.Where(p => p.OutboundFulfillment > 0).OrderBy(r => r.PositionCode);
            return h;


        }

        public IEnumerable<PartsOutboundPlanDetailWithPosition> GetPartsOutboundPlanNotPartitionPrint(int partsOutboundPlanId) {
            var userInfo = Utils.GetCurrentUserInfo();
            var partsOutboundPlan = ObjectContext.PartsOutboundPlans.Include("PartsSalesCategory").SingleOrDefault(r => r.Id == partsOutboundPlanId);
            if(partsOutboundPlan == null) {
                return null;
            }
            var partsOutboundPlanDetails = ObjectContext.PartsOutboundPlanDetails.Where(r => r.PartsOutboundPlanId == partsOutboundPlanId).ToArray();
            var partsOutboundPlanDetailWithPositions = new List<PartsOutboundPlanDetailWithPosition>();
            var sparePartIds = partsOutboundPlanDetails.Select(r => r.SparePartId).ToArray();
            var partsStockStruct = from a in ObjectContext.PartsStocks.Where(r => r.WarehouseId == partsOutboundPlan.WarehouseId && sparePartIds.Contains(r.PartId))
                                   join b in ObjectContext.WarehouseAreas.Where(r => r.WarehouseId == partsOutboundPlan.WarehouseId && r.AreaKind == (int)DcsAreaKind.库位) on a.WarehouseAreaId equals b.Id
                                   join c in ObjectContext.WarehouseAreaCategories.Where(r => r.Category == (int)DcsAreaType.保管区) on b.AreaCategoryId equals c.Id
                                   join d in ObjectContext.WarehouseAreas.Where(r => r.AreaKind == (int)DcsAreaKind.库区) on b.ParentId equals d.Id into tempTable1
                                   from t1 in tempTable1.DefaultIfEmpty()
                                   join e in ObjectContext.WarehouseAreas on b.Id equals e.TopLevelWarehouseAreaId into tempTable2
                                   from t2 in tempTable2.DefaultIfEmpty()
                                   join f in ObjectContext.WarehouseAreaManagers.Where(r => r.ManagerId == userInfo.Id) on t2.Id equals f.WarehouseAreaId
                                   select new {
                                       a.PartId,
                                       PositionCode = b.Code,
                                       TopLvAreaCode = t1.Code
                                   };
            var structPart = (from t in
                                  (from a in ObjectContext.SpareParts.Where(r => sparePartIds.Contains(r.Id))
                                   join b in partsStockStruct on a.Id equals b.PartId into tempTable
                                   from t in tempTable.DefaultIfEmpty()
                                   select new {
                                       PartId = a.Id,
                                       MeasureUnit = a.MeasureUnit ?? "",
                                       PositionCode = t.PositionCode ?? "",
                                       TopLvAreaCode = t.TopLvAreaCode ?? ""
                                   })
                              group t by new {
                                  t.PartId,
                                  t.MeasureUnit,
                                  t.TopLvAreaCode
                              }
                                  into tempTableGroup
                                  select new TempClassForPrint {
                                      PartId = tempTableGroup.Key.PartId,
                                      MeasureUnit = tempTableGroup.Key.MeasureUnit,
                                      TopLvAreaCode = tempTableGroup.Key.TopLvAreaCode,
                                      PositionCode = tempTableGroup.Min(v => v.PositionCode)
                                  }).ToArray();
            var tempIds1 = structPart.Where(r => r.TopLvAreaCode != "").Select(r => r.PartId).Distinct().ToArray();
            var tempIds2 = sparePartIds.Except(tempIds1).Distinct().ToArray();
            List<TempClassForPrint> objs = new List<TempClassForPrint>();
            foreach(var i in tempIds1) {
                var item = structPart.Where(r => r.TopLvAreaCode != "").Where(r => r.PartId == i).First();
                objs.Add(item);
            }
            foreach(var i in tempIds2) {
                var item = structPart.Where(r => r.TopLvAreaCode == "").Where(r => r.PartId == i).First();
                objs.Add(item);
            }
            foreach(var partsOutboundPlanDetail in partsOutboundPlanDetails) {
                var tempItems = objs.Where(r => r.PartId == partsOutboundPlanDetail.SparePartId).ToArray();
                if(tempItems.Length > 0) {
                    foreach(var item in tempItems) {
                        var partsOutboundPlanDetailWithPosition = new PartsOutboundPlanDetailWithPosition {
                            Id = partsOutboundPlan.Id,
                            ReceivingWarehouseName = partsOutboundPlan.ReceivingWarehouseName,
                            Code = partsOutboundPlan.Code,
                            PartsSalesOrderTypeName = partsOutboundPlan.PartsSalesOrderTypeName,
                            PlannedAmount = partsOutboundPlanDetail.PlannedAmount,
                            WarehouseCode = partsOutboundPlan.WarehouseCode,
                            BranchName = partsOutboundPlan.BranchName,
                            CounterpartCompanyName = partsOutboundPlan.CounterpartCompanyName,
                            OutboundType = partsOutboundPlan.OutboundType,
                            ShippingMethod = partsOutboundPlan.ShippingMethod,
                            SourceCode = partsOutboundPlan.SourceCode,
                            PartsSalesCategoryName = partsOutboundPlan.PartsSalesCategory.Name,
                            StorageCompanyName = partsOutboundPlan.StorageCompanyName,
                            CreatorId = partsOutboundPlan.CreatorId,
                            CreatorName = partsOutboundPlan.CreatorName,
                            CreateTime = partsOutboundPlan.CreateTime,
                            SparePartId = partsOutboundPlanDetail.SparePartId,
                            SparePartCode = partsOutboundPlanDetail.SparePartCode,
                            SparePartName = partsOutboundPlanDetail.SparePartName,
                            OutboundFulfillment = partsOutboundPlanDetail.PlannedAmount - (partsOutboundPlanDetail.OutboundFulfillment ?? 0),
                            Remark = partsOutboundPlanDetail.Remark,
                            TopLevelAreaCode = item.TopLvAreaCode,
                            PositionCode = item.PositionCode,
                            MeasureUnit = item.MeasureUnit

                        };
                        partsOutboundPlanDetailWithPositions.Add(partsOutboundPlanDetailWithPosition);
                    }
                }
            }
            return partsOutboundPlanDetailWithPositions.Where(p => p.OutboundFulfillment > 0).OrderBy(r => r.PositionCode);


        }

        public IEnumerable<SupplierPreAppLoanDetail> GetSupplierPreAppLoanDetailById(int supplierPreApprovedLoanId) {
            //GroupPrint
            var supplierPreAppLoanDetails = this.ObjectContext.SupplierPreAppLoanDetails.Where(r => r.SupplierPreApprovedLoanId == supplierPreApprovedLoanId && r.ApprovalAmount > 0).OrderBy(r => r.SupplierCode).ToArray();
            int groupid = 1, group = 1;
            foreach(var supplierPreAppLoanDetail in supplierPreAppLoanDetails) {
                supplierPreAppLoanDetail.GroupPrint = groupid;
                if(group % 3 == 0)
                    groupid++;
                group++;
            }
            return supplierPreAppLoanDetails;
        }

        public PartsOuterPurchaseChange GetPartsOuterPurchaseChangePrintById(int partsOuterPurchaseChangeId) {
            return this.ObjectContext.PartsOuterPurchaseChanges.SingleOrDefault(r => r.Id == partsOuterPurchaseChangeId);
        }

        public IEnumerable<PartsClaimOrderNew> GetPartsClaimOrderNewByIdPrint(string partsClaimOrderNewId) {
            var idStrings = partsClaimOrderNewId.Split(',');
            var ids = new int[idStrings.Length];
            for(int i = 0; i < idStrings.Length; i++) {
                ids[i] = int.Parse(idStrings[i]);
            }

            var partsClaimOrderNews = this.ObjectContext.PartsClaimOrderNews.Where(r => ids.Contains(r.Id)).ToArray();
            var returnCompanyIds = partsClaimOrderNews.Select(r => r.ReturnCompanyId).Distinct().ToArray();
            var companys = this.ObjectContext.Companies.Where(x => returnCompanyIds.Contains(x.Id)).ToArray();
            if(companys.Any()) {
                foreach(var partsClaimOrderNew in partsClaimOrderNews) {
                    var company = companys.Single(r => r.Id == partsClaimOrderNew.ReturnCompanyId);
                    if(company != null)
                        partsClaimOrderNew.ReturnCompanyName = company.Name;
                }

            }
            return partsClaimOrderNews;
        }

        public IEnumerable<PartsOuterPurchaselist> GetPartsOuterPurchaselistByPartsOuterPurchaseChangeId(int partsOuterPurchaseChangeId) {
            var partsOuterPurchaselists = this.ObjectContext.PartsOuterPurchaselists.Where(r => r.PartsOuterPurchaseChangeId == partsOuterPurchaseChangeId).ToArray();
            var number = 1;
            foreach(var partsOuterPurchaselist in partsOuterPurchaselists) {
                partsOuterPurchaselist.Number = number;
                number++;
            }
            return partsOuterPurchaselists;

        }

        public ExtendedWarrantyOrder GetExtendedWarrantyOrderForPrint(int extendedWarrantyOrderId) {
            var extendedWarrantyOrder = this.ObjectContext.ExtendedWarrantyOrders.SingleOrDefault(r => r.Id == extendedWarrantyOrderId);
            var company = this.ObjectContext.Companies.SingleOrDefault(r => r.Status == (int)DcsBaseDataStatus.有效 && r.Code == extendedWarrantyOrder.DealerCode);
            extendedWarrantyOrder.MailingAddress = extendedWarrantyOrder.Province + extendedWarrantyOrder.City + extendedWarrantyOrder.Region;
            if(company != null) {
                extendedWarrantyOrder.BusinessAddress = company.BusinessAddress;
                extendedWarrantyOrder.ContactPhone = company.ContactPhone;
            }
            //var vehicleChangeMsg = this.ObjectContext.VehicleChangeMsgs.Where(r => r.VIN == extendedWarrantyOrder.VIN).OrderByDescending(r => r.CreateTime).FirstOrDefault();
            //if(vehicleChangeMsg != null) {
            //    extendedWarrantyOrder.VehicleChangeCustomerName = vehicleChangeMsg.CustomerName;
            //}
            var extendedWarrantyProducts = this.ObjectContext.ExtendedWarrantyProducts.Where(r => this.ObjectContext.ExtendedWarrantyOrderLists.Any(x => x.ExtendedWarrantyOrderId == extendedWarrantyOrderId && x.ExtendedWarrantyProductId == r.Id));
            var names = extendedWarrantyProducts.Select(r => r.ExtendedWarrantyProductName).ToArray();
            extendedWarrantyOrder.Codes = string.Join(",", names);
            extendedWarrantyOrder.PayStartTimeMileage = extendedWarrantyProducts.Where(r => r.PayStartTimeMileage != null).Min(r => (int)r.PayStartTimeMileage);
            extendedWarrantyOrder.PayEndTimeMileage = extendedWarrantyProducts.Where(r => r.PayEndTimeMileage != null).Max(r => (int)r.PayEndTimeMileage);
            extendedWarrantyOrder.PayStartTimeCondition = extendedWarrantyProducts.Min(r => (int)r.PayStartTimeMileage);
            extendedWarrantyOrder.PayEndTimeCondition = extendedWarrantyProducts.Max(r => (int)r.PayEndTimeCondition);
            return extendedWarrantyOrder;
        }

        public IEnumerable<VirtualExtendedWarrantyOrderList> GetExtendedWarrantyOrderListByEWId(int extendedWarrantyOrderId) {
            return (from a in this.ObjectContext.ExtendedWarrantyOrders.Where(r => r.Id == extendedWarrantyOrderId)
                    join b in this.ObjectContext.ExtendedWarrantyOrderLists on a.Id equals b.ExtendedWarrantyOrderId
                    join c in this.ObjectContext.ExtendedWarrantyProducts on b.ExtendedWarrantyProductId equals c.Id
                    select new VirtualExtendedWarrantyOrderList {
                        Id = b.Id,
                        ExtendedWarrantyOrderCode = a.ExtendedWarrantyOrderCode,
                        SalesDate = a.SalesDate,
                        ExtendedWarrantyProductName = c.ExtendedWarrantyProductName,
                        ExtendedWarrantyTerm = c.ExtendedWarrantyTerm,
                        Mileage = a.Mileage,
                        EndDate = b.EndDate
                    }).ToArray();
        }

        //public IQueryable<RepairMemberRightsDetail> GetRepairMemberRightsDetailsForPrint(int repairOrderId) {
        //    var result = this.ObjectContext.RepairMemberRightsDetails.Where(r => r.RepairOrderId == repairOrderId);
        //    return result;
        //}

        public CAReconciliation GetCAReconciliationById(int id) {
            var item = this.ObjectContext.CAReconciliations.Include("CAReconciliationLists").SingleOrDefault(r => r.Id == id);
            item.AfterAdjustmentAmount = (item.CustomerAccountAmount ?? 0) + item.CAReconciliationLists.Where(r => r.ReconciliationDirection == (int)DcsReconciliationDirection.应加未达账).Sum(r => r.Money) - item.CAReconciliationLists.Where(r => r.ReconciliationDirection == (int)DcsReconciliationDirection.应减已入账).Sum(r => r.Money);
            //判断发起方
            var personnel = this.ObjectContext.Personnels.FirstOrDefault(r => r.Id == item.CreatorId);
            var company = this.ObjectContext.Companies.FirstOrDefault(r => r.Id == personnel.CorporationId);
            if(company.Type == (int)DcsCompanyType.分公司) {
                item.LeftMess = "单位盖章: " + "对账人: " + item.ReconciliationName + "  对账时间： " + item.ReconciliationTime.ToShortDateString();
                item.RightMess = "单位盖章:    " + "对账人: " + item.ConfirmorName + "  对账时间： " + (item.ConfirmorTime.HasValue ? item.ConfirmorTime.Value.ToShortDateString() : "");
            } else {
                item.LeftMess = "单位盖章: " + "对账人: " + item.ConfirmorName + "  对账时间： " + (item.ConfirmorTime.HasValue ? item.ConfirmorTime.Value.ToShortDateString() : "");
                item.RightMess = "单位盖章:    " + "对账人: " + item.ReconciliationName + "  对账时间： " + item.ReconciliationTime.ToShortDateString();
            }
            return item;
        }


        public IList<CAReconciliationList> GetCAReconciliationListsByCon(int cAReconciliationId, int reconciliationDirection, bool front) {
            var cAReconciliationList = this.ObjectContext.CAReconciliationLists.Where(r => r.CAReconciliationId == cAReconciliationId && r.ReconciliationDirection == reconciliationDirection).Take(10).OrderBy(r => r.Id).ToList();
            var count = cAReconciliationList.Count();
            var result = new List<CAReconciliationList>();
            foreach(var detail in cAReconciliationList) {
                result.Add(new CAReconciliationList() {
                    Id = detail.Id,
                    Time1 = detail.Time,
                    Abstract = detail.Abstract,
                    Money1 = detail.Money
                });
            }
            if(count < 16) {
                var ID = -1;
                for(int i = 1; i <= 16 - count; i++) {
                    result.Add(new CAReconciliationList() {
                        Id = ID--,
                        Time1 = null,
                        Abstract = "",
                        Money1 = null
                    });
                }
            }
            if(front)
                return result.Take(Convert.ToInt32(Math.Round(Convert.ToDouble(8), 0))).OrderBy(r => r.Id).ToList();
            else
                return result.Skip(Convert.ToInt32(Math.Round(Convert.ToDouble(8), 0))).OrderBy(r => r.Id).ToList();

        }

        public VirtualBoxUpTask GetBoxUpTaskByIdVir(int boxUpTaskId)
        {
            var userInfo = Utils.GetCurrentUserInfo();
            var branch = ObjectContext.Branches.SingleOrDefault(r => r.Id == userInfo.EnterpriseId);
            //主单
            //订单类型：装箱任务单清单.第一条拣货任务单.订单类型  
            var boxUp = (from a in ObjectContext.BoxUpTasks.Where(r => r.Id == boxUpTaskId)
                           join b in ObjectContext.BoxUpTaskDetails on a.Id equals b.BoxUpTaskId
                           join c in ObjectContext.PickingTasks on b.PickingTaskId equals c.Id
                           join n in ObjectContext.PartsOutboundPlans on b.PartsOutboundPlanId equals n.Id
                           select new VirtualBoxUpTask
                           {
                               Code = a.Code,
                               WarehouseName = a.WarehouseName,
                               CounterpartCompanyName = a.CounterpartCompanyName,
                               CreateTime = a.CreateTime,
                               OrderTypeName = c.OrderTypeName,
                               CreatorName=a.CreatorName,
                               InWarehouseName=n.ReceivingWarehouseName
                           }).FirstOrDefault();
            boxUp.BranchName = branch.Name;
            return boxUp;
        }
        public IQueryable<VirtualBoxUpTaskDetail> GetBoxUpTaskDetailsByIdVir(int boxUpTaskId)
        {
            var userInfo = Utils.GetCurrentUserInfo();
            var detailList = (from a in ObjectContext.BoxUpTaskDetails.Where(r => r.BoxUpTaskId == boxUpTaskId)
                              join f in ObjectContext.PartsOutboundPlans on a.PartsOutboundPlanId equals f.Id
                              join b in ObjectContext.PickingTaskDetails on  a.PickingTaskDetailId equals b.Id
                              join c in ObjectContext.SpareParts on a.SparePartId equals c.Id
                              join g in ObjectContext.WarehouseAreas on b.WarehouseAreaId equals g.Id  
                              join h in ObjectContext.WarehouseAreas on g.TopLevelWarehouseAreaId equals h.Id
                              select new VirtualBoxUpTaskDetail
                              {
                                  OriginalRequirementBillCode = f.OriginalRequirementBillCode,
                                  WarehouseAreaCode = b.WarehouseAreaCode,
                                  SparePartCode = a.SparePartCode,
                                  SparePartName = a.SparePartName,
                                  MeasureUnit = c.MeasureUnit,                                 
                                  PlanQty = a.PlanQty.Value,
                                  Weight = a.PlanQty * c.Weight,
                                  Volume = a.PlanQty * c.Volume,
                                  TopLevelWarehouseAreaCode = h.Code
                              });
            return detailList.OrderBy(r => r.TopLevelWarehouseAreaCode).ThenBy(e => e.SparePartCode);
        }
        public IEnumerable<VirtualPartsInboundCheckBillForPrint> GetPartsInboundCheckBillForPrintVir(int paraPartsInboundCheckBillId)
        {

            string SQL = @"SELECT Extent1.SparePartId,Extent2.StorageCompanyName as BranchName,Extent2.WarehouseName, Extent2.CounterpartCompanyName,
                        Extent2.Code,Extent4.Code AS PartsInboundPlanCode, Extent2.InboundType,Extent2.CreateTime, Extent1.SparePartCode,
                        Extent5.ReferenceCode, Extent1.SparePartName, Extent5.MeasureUnit,
       kk.WarehouseAreaCode as WarehouseAreaCode,
       kk.topCode as TopCode,
       kk.Quantity,
       Extent2.CreatorName,
       Extent1.InspectedQuantity,
         (select tt.ReferenceCode
          from PartsBranchPackingProp s
          join partsbranch t
            on s.partsbranchid = t.id
           and t.status = 1
           join SparePart tt on s.PackingMaterial=tt.code
         where s.SparePartId = Extent1.sparepartid
           and PackingType =
               (select max(PackingType)
                  from PartsBranchPackingProp s
                  join partsbranch t
                    on s.partsbranchid = t.id
                   and t.status = 1
                 where SparePartId = Extent1.sparepartid) and rownum= 1) as PackingCode,
        ceil ( Extent1.InspectedQuantity/(select PackingCoefficient
          from PartsBranchPackingProp s join partsbranch t on s.partsbranchid=t.id and t.status=1
         where SparePartId = Extent1.sparepartid
           and PackingType =
               (select max(PackingType)
                  from PartsBranchPackingProp s join partsbranch t on s.partsbranchid=t.id and t.status=1
                 where SparePartId = Extent1.sparepartid))) as PackNum,
        Extent2.BranchName,
   kv.Value as Priority,
   kv2.Value as TracePropertys,Extent1.BatchNumber
  FROM PartsInboundCheckBillDetail Extent1
 INNER JOIN PartsInboundCheckBill Extent2
    ON Extent1.PartsInboundCheckBillId = Extent2.Id
 INNER JOIN PartsInboundPlan Extent4
    ON Extent2.PartsInboundPlanId = Extent4.Id
 INNER JOIN SparePart Extent5
    ON Extent1.SparePartId = Extent5.Id
  left join (select warehousearea.code as WarehouseAreaCode,
                    partsstock.warehouseid,
                    partsstock.partid as partid,
                    wh.code as topCode,
                     partsstock.Quantity,
                    row_number() over(partition by partsstock.warehouseid, partsstock.partid order by WarehouseArea.Id) my_rank
               from partsstock
              inner join warehousearea
                 on partsstock.warehouseareaid = warehousearea.id
              inner join warehouseareacategory
                 on warehouseareacategory.id = warehousearea.areacategoryid
               join warehousearea wh
                 on wh.id = warehousearea.toplevelwarehouseareaid
              where warehouseareacategory.category = 1) kk
    on kk.partid = Extent1.sparepartid
   and kk.warehouseid = extent2.warehouseid
   and kk.my_rank = 1
left join PartsPriorityBill pp on Extent1.SparePartId= pp.PartId
left join KeyValueItem kv on pp.Priority=kv.key and kv.name='PartPriority'
left join KeyValueItem kv2 on Extent5.TraceProperty=kv2.key and kv2.name='TraceProperty'
 WHERE Extent1.PartsInboundCheckBillId = " + paraPartsInboundCheckBillId + " order by Extent1.SparePartCode";
            var detail = ObjectContext.ExecuteStoreQuery<VirtualPartsInboundCheckBillForPrint>(SQL).ToList();
            return detail;
        }
        public IEnumerable<VirtualPartsInboundCheckBillForPrint> GetPartsInboundCheckBillForPrintCh(int[] sparePartIds, int[] ids)
        {
            string SQL = @"SELECT Extent1.Id,
       Extent1.SparePartCode,
       Extent1.SparePartName,
       Extent1.SparePartId,
       Extent1.InspectedQuantity,
       Extent1.PartsInboundCheckBillId,
       Extent1.BatchNumber             as BachMunber,
       Extent3.Code                    as Code,
       Extent2.Measureunit,
       p1.measureunit as FirMeasureUnit,
       p2.measureunit as SecMeasureUnit,
       p3.measureunit as ThidMeasureUnit,
      ( case when p1.isboxstandardprint=1 and p1.packingcoefficient>0  then ceil(Extent1.Inspectedquantity/p1.packingcoefficient) else 0 end )as FirPrintNumber,
      ( case when p2.isboxstandardprint=1 and p2.packingcoefficient>0  then ceil(Extent1.Inspectedquantity/p2.packingcoefficient) else 0 end )as SecPrintNumber,
       ( case when p3.isboxstandardprint=1 and p3.packingcoefficient>0  then ceil(Extent1.Inspectedquantity/p3.packingcoefficient) else 0 end )as ThidPrintNumber,
       p1.packingcoefficient as FirPackingCoefficient,
       p2.packingcoefficient as SecPackingCoefficient,
       p3.packingcoefficient as ThidPackingCoefficient,
(select a.serial from CodeTemplateSerial a join codetemplate b on a.templateid=b.id and b.name='BoxPrintNumber')   as SerialNumber,
Extent2.Englishname ,Extent2.TraceProperty
  FROM PartsInboundCheckBillDetail Extent1
  join PartsInboundCheckBill Extent3
    on Extent1.PartsInboundCheckBillId = Extent3.Id
 INNER JOIN SparePart Extent2
    ON Extent1.SparePartId = Extent2.Id
  left join  partsbranch pb
    on Extent1.Sparepartid = pb.partid
   and pb.status = 1
  left join PartsBranchPackingProp p1
    on p1.partsbranchid = pb.id
   and p1.PackingType = 1
  left join PartsBranchPackingProp p2
    on p2.partsbranchid = pb.id
   and p2.PackingType = 2
  left join PartsBranchPackingProp p3
    on p3.partsbranchid = pb.id
   and p3.PackingType = 3
 WHERE Extent1.PartsInboundCheckBillId in( ";

            for (var i = 0; i < ids.Length; i++)
            {
                if (ids.Length == i + 1)
                {
                    SQL += ids[i];
                }
                else
                {
                    SQL += ids[i] + ",";
                }
            }
            SQL += ") and Extent1.Id in (";
            for (var i = 0; i < sparePartIds.Length; i++)
            {
                if (sparePartIds.Length == i + 1)
                {
                    SQL+=sparePartIds[i];
                }
                else
                {
                    SQL+=sparePartIds[i] + ",";
                }
            }
            SQL+=")";
            SQL+=" order by extent1.sparepartcode";
            var detail = ObjectContext.ExecuteStoreQuery<VirtualPartsInboundCheckBillForPrint>(SQL).ToList();         
            return detail;
        }
        public IEnumerable<VirtualPartsInboundCheckBillForPrint> GetPartsInboundCheckBillForPrintPcs(int[] ids, int[] partsInboundCheckBillIds)
        {
            string SQL = @"SELECT Extent1.Id,
       Extent1.SparePartCode,
       Extent1.SparePartName,
       Extent1.SparePartId,
       Extent1.InspectedQuantity,
       Extent1.PartsInboundCheckBillId,
       Extent1.BatchNumber             as BachMunber,
       Extent3.Code                    as Code,
       Extent2.Measureunit,
       p1.measureunit as FirMeasureUnit,
       p2.measureunit as SecMeasureUnit,
       p3.measureunit as ThidMeasureUnit,
      ( case when p1.isprintpartstandard=1 and p1.packingcoefficient>0  then ceil(Extent1.Inspectedquantity/p1.packingcoefficient) else 0 end )as FirPrintNumber,
      ( case when p2.isprintpartstandard=1 and p2.packingcoefficient>0  then ceil(Extent1.Inspectedquantity/p2.packingcoefficient) else 0 end )as SecPrintNumber,
       ( case when p3.isprintpartstandard=1 and p3.packingcoefficient>0  then ceil(Extent1.Inspectedquantity/p3.packingcoefficient) else 0 end )as ThidPrintNumber,
       p1.packingcoefficient as FirPackingCoefficient,
       p2.packingcoefficient as SecPackingCoefficient,
       p3.packingcoefficient as ThidPackingCoefficient,Extent2.TraceProperty
  FROM PartsInboundCheckBillDetail Extent1
  join PartsInboundCheckBill Extent3
    on Extent1.PartsInboundCheckBillId = Extent3.Id
 INNER JOIN SparePart Extent2
    ON Extent1.SparePartId = Extent2.Id
  left join  partsbranch pb
    on Extent1.Sparepartid = pb.partid
   and pb.status = 1
  left join PartsBranchPackingProp p1
    on p1.partsbranchid = pb.id
   and p1.PackingType = 1
  left join PartsBranchPackingProp p2
    on p2.partsbranchid = pb.id
   and p2.PackingType = 2
  left join PartsBranchPackingProp p3
    on p3.partsbranchid = pb.id
   and p3.PackingType = 3
 WHERE Extent1.PartsInboundCheckBillId in( ";

            for (var i = 0; i < partsInboundCheckBillIds.Length; i++)
            {
                if (partsInboundCheckBillIds.Length == i + 1)
                {
                    SQL += partsInboundCheckBillIds[i];
                }
                else
                {
                    SQL += partsInboundCheckBillIds[i] + ",";
                }
            }
            SQL += ") and Extent1.Id in (";
            for (var i = 0; i < ids.Length; i++)
            {
                if (ids.Length == i + 1)
                {
                    SQL += ids[i];
                }
                else
                {
                    SQL += ids[i] + ",";
                }
            }
            SQL += ")";
            var detail = ObjectContext.ExecuteStoreQuery<VirtualPartsInboundCheckBillForPrint>(SQL).ToList();
           
            return detail;
        }
        public VirtualPartsPurchaseOrder GetPartsPurchaseOrderForPrint(int partsPurchaseOrderId)
        {
            var result=(from a in ObjectContext.PartsPurchaseOrders.Where(r=>r.Id==partsPurchaseOrderId)
                        join  b in  ObjectContext.Companies on a.PartsSupplierId equals b.Id 
                        join c in ObjectContext.Warehouses on a.WarehouseId  equals c.Id
                        join d in ObjectContext.Personnels on a.CreatorId equals d.Id into dd
                        from dh in dd.DefaultIfEmpty()
                        join h in ObjectContext.PartsPurchaseOrderTypes on a.PartsPurchaseOrderTypeId equals h.Id
                        join sw in ObjectContext.PartsSalesOrders on new {
                            Id=a.OriginalRequirementBillId.Value,
                            a.IfDirectProvision
                        } equals new {
                            sw.Id,
							IfDirectProvision=true
                        } into ss 
                        from po in ss.DefaultIfEmpty()
                        select new VirtualPartsPurchaseOrder(){
                            Id=a.Id,
                            Code=a.Code,
                            PartsSupplierName = a.PartsSupplierName,
                            PartsPurchaseOrderTypeName=h.Name,
                            Contact = b.ContactPhone,
                            PhoneNumber = b.ContactPhone,
                            WarehouseName = a.WarehouseName,
                            ReceivingAddress = a.ReceivingAddress,
                            CreatorName = a.CreatorName,
                            CellNumber = dh.CellNumber,
                            WarehouseContact= a.IfDirectProvision==true? po.ContactPerson : c.Contact,
                            WarehousePhoneNumber= a.IfDirectProvision==true? po.ContactPhone : c.PhoneNumber,
                            ShippingMethod = a.ShippingMethod,
                            RequestedDeliveryTime = a.RequestedDeliveryTime,
                            CreateTime = a.RequestedDeliveryTime,
                            SubmitterName = a.SubmitterName,
                            SubmitTime = a.SubmitTime,
                            BranchName=a.BranchName
                        }).FirstOrDefault();
            return result;
        }
        public VirtualTemPurchaseOrder GetTemPartsPurchaseOrderForPrint(int temPurchaseOrderId) {
            var result = (from a in ObjectContext.TemPurchaseOrders.Where(r => r.Id == temPurchaseOrderId)
                          select new VirtualTemPurchaseOrder() {
                              Id = a.Id,
                              Code = a.Code,
                              PartsSupplierName = a.SuplierName,
                              OrderType = a.OrderType.Value,
                              Contact = a.Linker,
                              PhoneNumber = a.LinkPhone,
                              WarehouseName = a.WarehouseName,
                              ReceivingAddress = a.ReceiveAddress,
                              CreatorName = a.CreatorName,
                              WarehouseContact = a.Linker,
                              WarehousePhoneNumber = a.LinkPhone,
                              ShippingMethod = a.ShippingMethod,
                              SubmitterName = a.SubmitterName,
                              SubmitTime = a.SubmitTime,
							  CreateTime=a.CreateTime,
							  Buyer=a.Buyer
                          }).FirstOrDefault();
            return result;
        }
        public IEnumerable<VirtualPartsPurchaseOrderDetail> GetPartsPurchaseOrderDetailForPrint(int partsPurchaseOrderId)
        {
            IEnumerable<VirtualPartsPurchaseOrderDetail> detail = (
                from a in ObjectContext.PartsPurchaseOrderDetails.Where(r=>r.PartsPurchaseOrderId==partsPurchaseOrderId)
                               select new VirtualPartsPurchaseOrderDetail
                {
                    SupplierPartCode = a.SupplierPartCode,
                    SparePartName = a.SparePartName,
                    MeasureUnit=a.MeasureUnit,
                    UnitPrice=a.UnitPrice,
                    OrderAmount = a.OrderAmount,
                    Remark=a.Remark
                } ).ToList();          
            return detail;
        }
		public IEnumerable<TemPurchaseOrderDetail> GetTemPurchaseOrderDetailForPrint(int temPurchaseOrderId)
        {
            IEnumerable<TemPurchaseOrderDetail> detail = (
                from a in ObjectContext.TemPurchaseOrderDetails.Where(r=>r.TemPurchaseOrderId==temPurchaseOrderId)
                               select a).ToList();          
            return detail;
        }
        public VirtualSupplierShippingOrder GetSupplierShippingOrderForPrint(int supplierShippingOrderId)
        {

            var userInfo = Utils.GetCurrentUserInfo();
            var order = ObjectContext.SupplierShippingOrders.Where(r => r.Id == supplierShippingOrderId).FirstOrDefault();
            var detail = from a in ObjectContext.SupplierShippingDetails.Where(r => r.SupplierShippingOrderId == supplierShippingOrderId)
                         join b in ObjectContext.SupplierShippingOrders on a.SupplierShippingOrderId equals b.Id
                         join c in ObjectContext.PartsRetailGuidePrices on new {
                             a.SparePartId,
                             b.PartsSalesCategoryId,
                             Status = (int)DcsBaseDataStatus.有效
                         } equals new {
                             c.SparePartId,
                             c.PartsSalesCategoryId,
                             Status = c.Status
                         }
                         select new {
                             SupplierShippingOrderId = b.Id,
                             SalesPrice = c.RetailGuidePrice,
                             Quantity = a.Quantity
                         };
            var detailSum = from a in detail
                            group a by a.SupplierShippingOrderId into tempt
                            select new {
                                SupplierShippingOrderId = tempt.Key,
                                TotalAmount = tempt.Sum(r => r.SalesPrice * r.Quantity)
                            };
            VirtualSupplierShippingOrder result = new VirtualSupplierShippingOrder();
            if (order.IfDirectProvision)
            {
                  result = (from a in ObjectContext.SupplierShippingOrders.Where(r => r.Id == supplierShippingOrderId)
                           join b in ObjectContext.Companies on a.PartsSupplierId equals b.Id
                          join c in ObjectContext.PartsSalesOrders on a.OriginalRequirementBillId equals c.Id
                           from  t in ObjectContext.Companies 
                           join d in detailSum on a.Id equals d.SupplierShippingOrderId into dd
                           from dt in dd.DefaultIfEmpty()
						   join pp in ObjectContext.PartsPurchaseOrders on a.PartsPurchaseOrderId equals pp.Id
						  // let  bills =ObjectContext.InternalAllocationBills.Where(q=>q.SourceCode==a.OriginalRequirementBillCode&&ObjectContext.SupplierShippingDetails.Any(y=>y.SupplierShippingOrderId==a.Id &&q.ZcId==y.SparePartId)).Select(r=>r.Code)
                           where t.Id ==userInfo.EnterpriseId
                              select new VirtualSupplierShippingOrder
                              {
                                  Id = a.Id,
                                  Code = a.Code,
                                  ReceivingCompanyName = a.ReceivingCompanyName,
                                  ReceivingWarehouseName = a.ReceivingWarehouseName,
                                  ContactPerson=c.ContactPerson,
                                  ContactPhone=c.ContactPhone,
                                  ReceivingAddress=a.ReceivingAddress,
                                  Varieties = ObjectContext.SupplierShippingDetails.Where(s => s.SupplierShippingOrderId == supplierShippingOrderId).Count(),
                                  VarietiesQutity = ObjectContext.SupplierShippingDetails.Where(s => s.SupplierShippingOrderId == supplierShippingOrderId).Select(d => d.Quantity).Sum(),
                                  TotalPrice = dt.TotalAmount == null ? 0 : dt.TotalAmount,
                                  ShippingDate = a.ShippingDate,
                                  Contact = b.ContactPerson,
                                  PhoneNumber = b.ContactMobile,
                                  BusinessAddress = b.BusinessAddress,
                                  PartsPurchaseOrderCode = a.PartsPurchaseOrderCode,
                                  OrderType = a.PartsSalesCategoryName,
                                  ShippingMethod = a.ShippingMethod,
                                  RequestedDeliveryTime = a.RequestedDeliveryTime,
                                  CreateTime=a.CreateTime,
                                  CreatorName=a.CreatorName,
                                  BranchName=a.BranchName,
                                  TotalWeight=a.TotalWeight,
                                  TotalVolume=a.TotalVolume,
								  OriginalRequirementBillCode=a.OriginalRequirementBillCode,
								  PartsPurchaseOrderRemark=pp.Remark
                              }
               ).FirstOrDefault();
			  var  interCodes= ObjectContext.InternalAllocationBills.Where(q=>q.SourceCode==result.OriginalRequirementBillCode&&ObjectContext.SupplierShippingDetails.Any(y=>y.SupplierShippingOrderId==result.Id &&q.ZcId==y.SparePartId)).Select(r=>r.Code).ToList();
                if(interCodes.Count()>0)
			   result.InternalAllocationBillCode=String.Join(",",interCodes);      
            }
            else
            {
                result = (from a in ObjectContext.SupplierShippingOrders.Where(r => r.Id == supplierShippingOrderId)
                          join b in ObjectContext.Companies on a.PartsSupplierId equals b.Id
                          join c in ObjectContext.Warehouses on a.ReceivingWarehouseId equals c.Id
                          join d in ObjectContext.PartsPurchaseOrders on a.PartsPurchaseOrderId equals d.Id
                          join k in ObjectContext.PartsPurchaseOrderTypes on d.PartsPurchaseOrderTypeId equals k.Id
                          join p in ObjectContext.PartsInboundPlans on a.Id equals p.SourceId
                          from t in ObjectContext.Companies
                          join e in detailSum on a.Id equals e.SupplierShippingOrderId into dd
                          from dt in dd.DefaultIfEmpty()
                          where t.Id == userInfo.EnterpriseId
                          select new VirtualSupplierShippingOrder
                          {
                              Id = a.Id,
                              Code = a.Code,
                              ReceivingCompanyName = a.ReceivingCompanyName,
                              ReceivingWarehouseName = a.ReceivingWarehouseName,
                              ContactPerson = c.Contact,
                              ContactPhone = c.PhoneNumber,
                              ReceivingAddress = a.ReceivingAddress,
                              Varieties = ObjectContext.SupplierShippingDetails.Where(s => s.SupplierShippingOrderId == supplierShippingOrderId).Count(),
                              VarietiesQutity = ObjectContext.SupplierShippingDetails.Where(s => s.SupplierShippingOrderId == supplierShippingOrderId).Select(m => m.Quantity).Sum(),
                              TotalPrice = dt.TotalAmount == null ? 0 : dt.TotalAmount,
                              ShippingDate = a.ShippingDate,
                              Contact = b.ContactPerson,
                              PhoneNumber = b.ContactMobile,
                              BusinessAddress = b.BusinessAddress,
                              PartsPurchaseOrderCode = a.PartsPurchaseOrderCode,
                              OrderType = k.Name,
                              ShippingMethod = a.ShippingMethod,
                              RequestedDeliveryTime = a.RequestedDeliveryTime,
                              CreateTime = a.CreateTime,
                              CreatorName = a.CreatorName,
                              PartsInboundPlanCode=p.Code,
                              BranchName = a.BranchName,
                              UserBranchName = t.Name,
                              TotalWeight = a.TotalWeight,
                              TotalVolume = a.TotalVolume,
							  PartsPurchaseOrderRemark=d.Remark
                          }
               ).FirstOrDefault();
            }
            return result;
        }
		 public VirtualTemSupplierShippingOrder GetTemSupplierShippingOrderForPrint(int supplierShippingOrderId)
        {
			var userInfo = Utils.GetCurrentUserInfo();
			var order = (from a in  ObjectContext.TemSupplierShippingOrders.Where(r => r.Id == supplierShippingOrderId)
			            join r in ObjectContext.Warehouses on a.ReceivingWarehouseId equals r.Id
						join b in ObjectContext.Companies on a.PartsSupplierId equals b.Id
						join c in ObjectContext.TemPurchaseOrders on a.TemPurchaseOrderId equals c.Id
						 from t in ObjectContext.Companies
						  where t.Id == userInfo.EnterpriseId
                         select new VirtualTemSupplierShippingOrder{
							 Id=a.Id,
							 Code=a.Code,
							 ReceivingCompanyName = a.ReceivingCompanyName,
                              ReceivingWarehouseName = a.ReceivingWarehouseName,
                              ContactPerson = c.Linker,
                              ContactPhone = c.LinkPhone,
                              ReceivingAddress = a.ReceivingAddress,
                              Varieties = ObjectContext.TemShippingOrderDetails.Where(s => s.ShippingOrderId == supplierShippingOrderId).Count(),
                              VarietiesQutity = ObjectContext.TemShippingOrderDetails.Where(s => s.ShippingOrderId == supplierShippingOrderId).Select(m => m.Quantity).Sum(),
                              ShippingDate = a.ShippingDate,
                              Contact = b.ContactPerson,
                              PhoneNumber = b.ContactMobile,
                              BusinessAddress = b.BusinessAddress,
                              PartsPurchaseOrderCode = a.TemPurchaseOrderCode,
							  RequestedDeliveryTime=a.PlanDeliveryTime,
                              OrderType = c.OrderType.Value,
                              ShippingMethod = a.ShippingMethod,
                              CreateTime = a.CreateTime,
                              CreatorName = a.CreatorName,
                              UserBranchName = t.Name,
                              TotalWeight = a.TotalWeight,
                              TotalVolume = a.TotalVolume,
						 }).FirstOrDefault();
		    return order;
			
		}
		 public IEnumerable<VirtualSupplierShippingDetail> GetTemSupplierShippingOrderDetailForPrint(int supplierShippingOrderId)
        {
            IEnumerable<VirtualSupplierShippingDetail> detail = (
                from a in ObjectContext.TemShippingOrderDetails.Where(r => r.ShippingOrderId == supplierShippingOrderId)
                join s in ObjectContext.TemSupplierShippingOrders on a.ShippingOrderId equals s.Id
                join b in ObjectContext.SpareParts on a.SparePartId equals b.Id
                join c in ObjectContext.PartsSupplierRelations on new { PartId = a.SparePartId, SupplierId = s.PartsSupplierId } equals new { c.PartId, c.SupplierId } into d
                from relation in d.DefaultIfEmpty()
                select new VirtualSupplierShippingDetail
                {
                    SparePartName = a.SparePartName,
                    MeasureUnit = a.MeasureUnit,
                    SparePartCode = a.SparePartCode,
                    Quantity = a.Quantity,
                    ReferenceCode = b.ReferenceCode,
                    SupplierPartCode = relation.SupplierPartCode
                }).ToList();
           
            return detail;
		}
        public IEnumerable<VirtualSupplierShippingDetail> GetSupplierShippingOrderDetailForPrint(int supplierShippingOrderId)
        {
          //  bool ispri = true;
            IEnumerable<VirtualSupplierShippingDetail> detail = (
                from a in ObjectContext.SupplierShippingDetails.Where(r => r.SupplierShippingOrderId == supplierShippingOrderId)
                join s in ObjectContext.SupplierShippingOrders on a.SupplierShippingOrderId equals s.Id
                join b in ObjectContext.SpareParts on a.SparePartId equals b.Id
                join c in ObjectContext.PartsSupplierRelations on new { PartId = a.SparePartId, SupplierId = s.PartsSupplierId } equals new { c.PartId, c.SupplierId } into d
                from relation in d.DefaultIfEmpty()
                select new VirtualSupplierShippingDetail
                {
                    SparePartName = a.SparePartName,
                    MeasureUnit = a.MeasureUnit,
                    SparePartCode = a.SparePartCode,
                    Quantity = a.Quantity,
                    ReferenceCode = b.ReferenceCode,
                    SupplierPartCode = relation.SupplierPartCode
                }).ToList();
           
            return detail;
        }
		public IEnumerable<VirtualSupplierShippingDetail> GetSupplierShippingOrderDetailForPrintA4(int supplierShippingOrderId)
        {
          //  bool ispri = true;
            IEnumerable<VirtualSupplierShippingDetail> detail = (
                from a in ObjectContext.SupplierShippingDetails.Where(r => r.SupplierShippingOrderId == supplierShippingOrderId)
                join s in ObjectContext.SupplierShippingOrders on a.SupplierShippingOrderId equals s.Id
                join b in ObjectContext.SpareParts on a.SparePartId equals b.Id
                join c in ObjectContext.PartsSupplierRelations on new { PartId = a.SparePartId, SupplierId = s.PartsSupplierId } equals new { c.PartId, c.SupplierId } into d
                from relation in d.DefaultIfEmpty()
                select new VirtualSupplierShippingDetail
                {
                    SparePartName = a.SparePartName,
                    MeasureUnit = a.MeasureUnit,
                    SparePartCode = a.SparePartCode,
                    Quantity = a.Quantity,
                    ReferenceCode = b.ReferenceCode,
                    SupplierPartCode = relation.SupplierPartCode
                }).ToList();
            IEnumerable<VirtualSupplierShippingDetail> internalAllocationDetail = (
                from a in ObjectContext.InternalAllocationDetails
                join s in ObjectContext.InternalAllocationBills on a.InternalAllocationBillId equals s.Id
                join su in ObjectContext.SupplierShippingOrders.Where(t => t.Id == supplierShippingOrderId) on s.SourceCode equals su.OriginalRequirementBillCode
                join sd in ObjectContext.SupplierShippingDetails on new {
                    ZcId=s.ZcId.Value,
					Id=su.Id                   
                } equals new {
                    ZcId=sd.SparePartId,
                    Id=sd.SupplierShippingOrderId
                }
                join b in ObjectContext.SpareParts on a.SparePartId equals b.Id
                select new VirtualSupplierShippingDetail {
                    SparePartName = a.SparePartName,
                    SparePartCode = a.SparePartCode,
                    MeasureUnit = b.MeasureUnit,
                    Quantity = a.Quantity,
                    SupplierPartCode = a.SparePartCode,
                    ReferenceCode = b.ReferenceCode
                }
                ).ToList();
            return detail.Union(internalAllocationDetail);
        }
        public IEnumerable<VirtualSupplierShippingDetail> GetSupplierShippingOrderForPrintPcs(int[] ids, int[] supplierShippingOrderIds)
        {
            string SQL = @"SELECT Extent1.Id,
       Extent1.SparePartCode,
       Extent1.SparePartName,
       Extent1.SparePartId,
       Extent1.Quantity,
       Extent2.EnglishName,
       Extent1.SupplierShippingOrderId,
       substr(Extent3.Code, length(Extent3.Code) - 11) BachMunber,     
       Extent3.Code as Code,
         Extent2.Measureunit,
       p1.measureunit as FirMeasureUnit,
       p2.measureunit as SecMeasureUnit,
       p3.measureunit as ThidMeasureUnit,
           ( case when p1.isprintpartstandard=1 and p1.packingcoefficient>0  then ceil(Extent1.Quantity/p1.packingcoefficient) else 0 end )as FirPrintNumber,
      ( case when p2.isprintpartstandard=1 and p2.packingcoefficient>0  then ceil(Extent1.Quantity/p2.packingcoefficient) else 0 end )as SecPrintNumber,
       ( case when p3.isprintpartstandard=1 and p3.packingcoefficient>0  then ceil(Extent1.Quantity/p3.packingcoefficient) else 0 end )as ThidPrintNumber,
       p1.packingcoefficient as FirPackingCoefficient,
       p2.packingcoefficient as SecPackingCoefficient,
       p3.packingcoefficient as ThidPackingCoefficient,
(select a.serial from CodeTemplateSerial a join codetemplate b on a.templateid=b.id and b.name='BoxPrintNumber')   as SerialNumber
  FROM SupplierShippingDetail Extent1
  join SupplierShippingOrder Extent3
    on Extent1.SupplierShippingOrderId = Extent3.Id
 INNER JOIN SparePart Extent2
    ON Extent1.SparePartId = Extent2.Id
     left join  partsbranch pb
    on Extent1.Sparepartid = pb.partid
   and pb.status = 1
  left join PartsBranchPackingProp p1
    on p1.partsbranchid = pb.id
   and p1.PackingType = 1
  left join PartsBranchPackingProp p2
    on p2.partsbranchid = pb.id
   and p2.PackingType = 2
  left join PartsBranchPackingProp p3
    on p3.partsbranchid = pb.id
   and p3.PackingType = 3
 WHERE Extent1.SupplierShippingOrderId in( ";

            for (var i = 0; i < supplierShippingOrderIds.Length; i++)
            {
                if (supplierShippingOrderIds.Length == i + 1)
                {
                    SQL += supplierShippingOrderIds[i];
                }
                else
                {
                    SQL += supplierShippingOrderIds[i] + ",";
                }
            }
            SQL += ") and Extent1.Id in (";
            for (var i = 0; i < ids.Length; i++)
            {
                if (ids.Length == i + 1)
                {
                    SQL += ids[i];
                }
                else
                {
                    SQL += ids[i] + ",";
                }
            }
            SQL += ")";
            var detail = ObjectContext.ExecuteStoreQuery<VirtualSupplierShippingDetail>(SQL).ToList();          
            return detail;
        }
        public IEnumerable<VirtualSupplierShippingDetail> GetSupplierShippingOrderForPrintCh(int[] sparePartIds, int[] ids)
        {
            string SQL = @"SELECT Extent1.Id,
       Extent1.SparePartCode,
       Extent1.SparePartName,
       Extent1.SparePartId,
       Extent1.Quantity,
       Extent2.EnglishName,
       Extent1.SupplierShippingOrderId,
       substr(Extent3.Code, length(Extent3.Code) - 11) BachMunber,     
       Extent3.Code as Code,
         Extent2.Measureunit,
       p1.measureunit as FirMeasureUnit,
       p2.measureunit as SecMeasureUnit,
       p3.measureunit as ThidMeasureUnit,
           ( case when p1.isboxstandardprint=1 and p1.packingcoefficient>0  then ceil(Extent1.Quantity/p1.packingcoefficient) else 0 end )as FirPrintNumber,
      ( case when p2.isboxstandardprint=1 and p2.packingcoefficient>0  then ceil(Extent1.Quantity/p2.packingcoefficient) else 0 end )as SecPrintNumber,
       ( case when p3.isboxstandardprint=1 and p3.packingcoefficient>0  then ceil(Extent1.Quantity/p3.packingcoefficient) else 0 end )as ThidPrintNumber,
       p1.packingcoefficient as FirPackingCoefficient,
       p2.packingcoefficient as SecPackingCoefficient,
       p3.packingcoefficient as ThidPackingCoefficient,
(select a.serial from CodeTemplateSerial a join codetemplate b on a.templateid=b.id and b.name='BoxPrintNumber')   as SerialNumber
  FROM SupplierShippingDetail Extent1
  join SupplierShippingOrder Extent3
    on Extent1.SupplierShippingOrderId = Extent3.Id
 INNER JOIN SparePart Extent2
    ON Extent1.SparePartId = Extent2.Id
     left join  partsbranch pb
    on Extent1.Sparepartid = pb.partid
   and pb.status = 1
  left join PartsBranchPackingProp p1
    on p1.partsbranchid = pb.id
   and p1.PackingType = 1
  left join PartsBranchPackingProp p2
    on p2.partsbranchid = pb.id
   and p2.PackingType = 2
  left join PartsBranchPackingProp p3
    on p3.partsbranchid = pb.id
   and p3.PackingType = 3
 WHERE Extent1.SupplierShippingOrderId in( ";

            for (var i = 0; i < ids.Length; i++)
            {
                if (ids.Length == i + 1)
                {
                    SQL += ids[i];
                }
                else
                {
                    SQL += ids[i] + ",";
                }
            }
            SQL += ") and Extent1.Id in (";
            for (var i = 0; i < sparePartIds.Length; i++)
            {
                if (sparePartIds.Length == i + 1)
                {
                    SQL += sparePartIds[i];
                }
                else
                {
                    SQL += sparePartIds[i] + ",";
                }
            }
            SQL += ")";
            var detail = ObjectContext.ExecuteStoreQuery<VirtualSupplierShippingDetail>(SQL).ToList();
            //更新流水号
            int acountFir = detail.Sum(r => r.FirPrintNumber).Value;
            int acountSec = detail.Sum(r => r.SecPrintNumber).Value;
            int AcountThid = detail.Sum(r => r.ThidPrintNumber).Value;
            int printAcount = acountFir + acountSec + AcountThid;
            if (printAcount > 0)
            {
                var codeser = ObjectContext.CodeTemplateSerials.Where(r => ObjectContext.CodeTemplates.Any(d => r.TemplateId == d.Id && d.Name == "BoxPrintNumber")).FirstOrDefault();
                codeser.Serial += printAcount;
                UpdateToDatabase(codeser);
                ObjectContext.SaveChanges();
            }
            return detail;
        }
        public IEnumerable<VirtualPartsInboundCheckBillForPrint> GetPartsStockQueryForPcPrint(int id) {
            string SQL = @"SELECT Extent2.Name          as SparePartName,
                           Extent2.Code          as SparePartCode,
                           Extent2.Measureunit,
                           Extent2.MeasureUnit        as FirMeasureUnit,
                           Extent2.MInPackingAmount as FirPackingCoefficient,
                           Extent2.id as SparePartId, Extent2.TraceProperty,
                          (select a.serial from CodeTemplateSerial a join codetemplate b on a.templateid=b.id and b.name='SpartPrintNumber')   as SerialNumber
                      from SparePart Extent2
                      left join partsbranch pb
                        on Extent2.id = pb.partid
                       and pb.status = 1                     
                     where Extent2.id=" + id;
            var detail = ObjectContext.ExecuteStoreQuery<VirtualPartsInboundCheckBillForPrint>(SQL).ToList();
            //更新流水号
            var codeser = ObjectContext.CodeTemplateSerials.Where(r => ObjectContext.CodeTemplates.Any(d => r.TemplateId == d.Id && d.Name == "SpartPrintNumber")).FirstOrDefault();
            codeser.Serial += 1;
            UpdateToDatabase(codeser);
            ObjectContext.SaveChanges();
            return detail;
        }
    }
}