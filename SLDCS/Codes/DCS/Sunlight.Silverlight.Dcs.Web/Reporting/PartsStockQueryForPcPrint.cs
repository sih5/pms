namespace Sunlight.Silverlight.Dcs.Web.Reporting {
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    /// <summary>
    /// Summary description for PartsStockQueryForPcPrint.
    /// </summary>
    public partial class PartsStockQueryForPcPrint : Telerik.Reporting.Report {
        public PartsStockQueryForPcPrint() {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }
        private void PartsStockQueryForPcPrint_NeedDataSource(object sender, EventArgs e) {
            var report = (Telerik.Reporting.Processing.Report)sender;
            // 取参数值
            //配件ID
            var id = report.Parameters["id"].Value.ToString();
            var time = report.Parameters["time"].Value.ToString().Split('+')[0].ToString();
            var number = Int32.Parse(report.Parameters["number"].Value.ToString());
            DateTime printTime = DateTime.Parse(time);
            string month = "";
            if(printTime.Month < 10) {
                month = "0" + printTime.Month.ToString();
            } else {
                month = printTime.Month.ToString();
            }
            string day = "";
            if(printTime.Day < 10) {
                day = "0" + printTime.Day.ToString();
            } else {
                day = printTime.Day.ToString();
            }
            string timeString = printTime.Year.ToString() + month + day;
            var domainService = new DcsDomainService();
            //根据配件id 查询配件信息
            var partsInboundPlanDetails = domainService.GetPartsStockQueryForPcPrint(Int32.Parse(id.ToString())).OrderBy(r => r.SparePartId).ToArray();
            var details = new List<VirtualPartsInboundCheckBillForPrint>();
            //修改清单记录的数量属性，为客户端传入的值
            foreach(var item in partsInboundPlanDetails) {

                for(var i = 0; i < number; i++) {
                    var planDetail = new VirtualPartsInboundCheckBillForPrint();
                    planDetail.SparePartCode = item.SparePartCode;
                    planDetail.SparePartName = item.SparePartName;
                    planDetail.MeasureUnit = item.MeasureUnit + "/" + item.FirMeasureUnit;
                    planDetail.PackingCoefficient = item.FirPackingCoefficient.Value;
                    planDetail.BachMunber = timeString + (item.SerialNumber + 1).ToString();

                    if(planDetail.SparePartName.Length > 18) {
                        planDetail.SparePartName = planDetail.SparePartName.Substring(0, 18);
                        planDetail.SparePartName = planDetail.SparePartName + "...";
                    }

                    if(item.TraceProperty.HasValue && item.TraceProperty == (int)DCSTraceProperty.精确追溯) {
                        planDetail.Jq = planDetail.BachMunber + "|" + (i + 1) + "|JQ";
                        planDetail.SparePartCodeX = planDetail.SparePartCode + " JQ";
                        planDetail.BachMunber = planDetail.BachMunber + "|" + (i + 1);
                    } else {
                        planDetail.Jq = planDetail.BachMunber;
                        planDetail.SparePartCodeX = planDetail.SparePartCode;
                    }
                    details.Add(planDetail);
                }
              
            }          
            // 将构造的结果集绑定到 DetailSection
            report.DataSource = details;
        }

    }
}