﻿namespace Sunlight.Silverlight.Dcs.Web.Reporting {
    partial class ReportAgencyPartsShippingOrders {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup11 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup12 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup13 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup14 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup15 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup16 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup17 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup18 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup19 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup20 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup21 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup22 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup23 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup24 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup25 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.ReportParameter reportParameter1 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter2 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            this.textBox10 = new Telerik.Reporting.TextBox();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.textBox8 = new Telerik.Reporting.TextBox();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.textBox34 = new Telerik.Reporting.TextBox();
            this.textBox35 = new Telerik.Reporting.TextBox();
            this.textBox36 = new Telerik.Reporting.TextBox();
            this.textBox40 = new Telerik.Reporting.TextBox();
            this.textBox42 = new Telerik.Reporting.TextBox();
            this.textBox44 = new Telerik.Reporting.TextBox();
            this.textBox46 = new Telerik.Reporting.TextBox();
            this.textBox48 = new Telerik.Reporting.TextBox();
            this.textBox90 = new Telerik.Reporting.TextBox();
            this.textBox91 = new Telerik.Reporting.TextBox();
            this.textBox96 = new Telerik.Reporting.TextBox();
            this.textBox116 = new Telerik.Reporting.TextBox();
            this.textBox117 = new Telerik.Reporting.TextBox();
            this.textBox118 = new Telerik.Reporting.TextBox();
            this.pageHeaderSection1 = new Telerik.Reporting.PageHeaderSection();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.textBox131 = new Telerik.Reporting.TextBox();
            this.textBox132 = new Telerik.Reporting.TextBox();
            this.textBox133 = new Telerik.Reporting.TextBox();
            this.textBox134 = new Telerik.Reporting.TextBox();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.textBox7 = new Telerik.Reporting.TextBox();
            this.textBox9 = new Telerik.Reporting.TextBox();
            this.detail = new Telerik.Reporting.DetailSection();
            this.table1 = new Telerik.Reporting.Table();
            this.textBox30 = new Telerik.Reporting.TextBox();
            this.textBox31 = new Telerik.Reporting.TextBox();
            this.textBox32 = new Telerik.Reporting.TextBox();
            this.textBox33 = new Telerik.Reporting.TextBox();
            this.PartsShippingOrderDetailDataSource = new Telerik.Reporting.ObjectDataSource();
            this.table2 = new Telerik.Reporting.Table();
            this.textBox37 = new Telerik.Reporting.TextBox();
            this.textBox38 = new Telerik.Reporting.TextBox();
            this.textBox39 = new Telerik.Reporting.TextBox();
            this.textBox41 = new Telerik.Reporting.TextBox();
            this.textBox43 = new Telerik.Reporting.TextBox();
            this.textBox45 = new Telerik.Reporting.TextBox();
            this.textBox47 = new Telerik.Reporting.TextBox();
            this.textBox49 = new Telerik.Reporting.TextBox();
            this.table6 = new Telerik.Reporting.Table();
            this.textBox93 = new Telerik.Reporting.TextBox();
            this.textBox94 = new Telerik.Reporting.TextBox();
            this.textBox95 = new Telerik.Reporting.TextBox();
            this.textBox97 = new Telerik.Reporting.TextBox();
            this.table9 = new Telerik.Reporting.Table();
            this.textBox119 = new Telerik.Reporting.TextBox();
            this.textBox120 = new Telerik.Reporting.TextBox();
            this.textBox121 = new Telerik.Reporting.TextBox();
            this.textBox58 = new Telerik.Reporting.TextBox();
            this.textBox61 = new Telerik.Reporting.TextBox();
            this.textBox60 = new Telerik.Reporting.TextBox();
            this.textBox56 = new Telerik.Reporting.TextBox();
            this.textBox62 = new Telerik.Reporting.TextBox();
            this.textBox63 = new Telerik.Reporting.TextBox();
            this.textBox64 = new Telerik.Reporting.TextBox();
            this.textBox66 = new Telerik.Reporting.TextBox();
            this.textBox50 = new Telerik.Reporting.TextBox();
            this.textBox65 = new Telerik.Reporting.TextBox();
            this.textBox67 = new Telerik.Reporting.TextBox();
            this.textBox68 = new Telerik.Reporting.TextBox();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.textBox11 = new Telerik.Reporting.TextBox();
            this.textBox12 = new Telerik.Reporting.TextBox();
            this.textBox13 = new Telerik.Reporting.TextBox();
            this.textBox14 = new Telerik.Reporting.TextBox();
            this.textBox15 = new Telerik.Reporting.TextBox();
            this.textBox16 = new Telerik.Reporting.TextBox();
            this.textBox17 = new Telerik.Reporting.TextBox();
            this.textBox19 = new Telerik.Reporting.TextBox();
            this.textBox21 = new Telerik.Reporting.TextBox();
            this.textBox23 = new Telerik.Reporting.TextBox();
            this.textBox25 = new Telerik.Reporting.TextBox();
            this.textBox69 = new Telerik.Reporting.TextBox();
            this.textBox70 = new Telerik.Reporting.TextBox();
            this.textBox71 = new Telerik.Reporting.TextBox();
            this.textBox72 = new Telerik.Reporting.TextBox();
            this.textBox73 = new Telerik.Reporting.TextBox();
            this.textBox74 = new Telerik.Reporting.TextBox();
            this.textBox75 = new Telerik.Reporting.TextBox();
            this.textBox76 = new Telerik.Reporting.TextBox();
            this.textBox20 = new Telerik.Reporting.TextBox();
            this.textBox24 = new Telerik.Reporting.TextBox();
            this.PartsShippingOrderDataSource = new Telerik.Reporting.ObjectDataSource();
            this.pageFooterSection1 = new Telerik.Reporting.PageFooterSection();
            this.textBox53 = new Telerik.Reporting.TextBox();
            this.textBox55 = new Telerik.Reporting.TextBox();
            this.textBox57 = new Telerik.Reporting.TextBox();
            this.textBox59 = new Telerik.Reporting.TextBox();
            this.textBox154 = new Telerik.Reporting.TextBox();
            this.textBox155 = new Telerik.Reporting.TextBox();
            this.textBox156 = new Telerik.Reporting.TextBox();
            this.textBox157 = new Telerik.Reporting.TextBox();
            this.textBox158 = new Telerik.Reporting.TextBox();
            this.textBox159 = new Telerik.Reporting.TextBox();
            this.textBox160 = new Telerik.Reporting.TextBox();
            this.textBox162 = new Telerik.Reporting.TextBox();
            this.textBox163 = new Telerik.Reporting.TextBox();
            this.textBox164 = new Telerik.Reporting.TextBox();
            this.textBox165 = new Telerik.Reporting.TextBox();
            this.textBox166 = new Telerik.Reporting.TextBox();
            this.textBox167 = new Telerik.Reporting.TextBox();
            this.textBox86 = new Telerik.Reporting.TextBox();
            this.textBox112 = new Telerik.Reporting.TextBox();
            this.textBox130 = new Telerik.Reporting.TextBox();
            this.textBox18 = new Telerik.Reporting.TextBox();
            this.textBox22 = new Telerik.Reporting.TextBox();
            this.textBox26 = new Telerik.Reporting.TextBox();
            this.textBox27 = new Telerik.Reporting.TextBox();
            this.textBox28 = new Telerik.Reporting.TextBox();
            this.textBox29 = new Telerik.Reporting.TextBox();
            this.textBox51 = new Telerik.Reporting.TextBox();
            this.textBox52 = new Telerik.Reporting.TextBox();
            this.textBox54 = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // textBox10
            // 
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9311326742172241D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox10.StyleName = "";
            this.textBox10.Value = "发货品种：";
            // 
            // textBox3
            // 
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.2021849155426025D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox3.Value = "= CountDistinct(Fields.SparePartCode)";
            // 
            // textBox8
            // 
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.7462025880813599D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox8.StyleName = "";
            this.textBox8.Value = "发货数量：";
            // 
            // textBox4
            // 
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.6941665410995483D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox4.Value = "= Sum(Fields.ShippingAmount)";
            // 
            // textBox34
            // 
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.84437555074691772D), Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D));
            this.textBox34.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox34.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox34.Value = "序号";
            // 
            // textBox35
            // 
            this.textBox35.Name = "textBox35";
            this.textBox35.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.1160297393798828D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox35.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox35.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox35.Value = "配件图号";
            // 
            // textBox36
            // 
            this.textBox36.Name = "textBox36";
            this.textBox36.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.2316255569458008D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox36.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox36.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox36.Value = "配件名称";
            // 
            // textBox40
            // 
            this.textBox40.Name = "textBox40";
            this.textBox40.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.021648645401001D), Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D));
            this.textBox40.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox40.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox40.StyleName = "";
            this.textBox40.Value = "重量";
            // 
            // textBox42
            // 
            this.textBox42.Name = "textBox42";
            this.textBox42.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.94221335649490356D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox42.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox42.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox42.StyleName = "";
            this.textBox42.Value = "单位";
            // 
            // textBox44
            // 
            this.textBox44.Name = "textBox44";
            this.textBox44.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8738036155700684D), Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D));
            this.textBox44.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox44.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox44.StyleName = "";
            this.textBox44.Value = "单价";
            // 
            // textBox46
            // 
            this.textBox46.Name = "textBox46";
            this.textBox46.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.1014145612716675D), Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D));
            this.textBox46.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox46.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox46.StyleName = "";
            this.textBox46.Value = "数量";
            // 
            // textBox48
            // 
            this.textBox48.Name = "textBox48";
            this.textBox48.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.8157364130020142D), Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D));
            this.textBox48.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox48.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox48.StyleName = "";
            this.textBox48.Value = "金额（元）";
            // 
            // textBox90
            // 
            this.textBox90.Name = "textBox90";
            this.textBox90.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.7885687351226807D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox90.Value = "发货金额（元）：";
            // 
            // textBox91
            // 
            this.textBox91.Name = "textBox91";
            this.textBox91.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.5903911590576172D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox91.Value = "";
            // 
            // textBox96
            // 
            this.textBox96.Name = "textBox96";
            this.textBox96.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.79229885339736938D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox96.StyleName = "";
            // 
            // textBox116
            // 
            this.textBox116.Name = "textBox116";
            this.textBox116.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.6562505960464478D), Telerik.Reporting.Drawing.Unit.Cm(0.40000000596046448D));
            this.textBox116.Value = "总重量：";
            // 
            // textBox117
            // 
            this.textBox117.Name = "textBox117";
            this.textBox117.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.5029153823852539D), Telerik.Reporting.Drawing.Unit.Cm(0.40000000596046448D));
            this.textBox117.Value = "= Sum(Fields.SparePart.Width * Fields.ShippingAmount)";
            // 
            // textBox118
            // 
            this.textBox118.Name = "textBox118";
            this.textBox118.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.099999979138374329D), Telerik.Reporting.Drawing.Unit.Cm(0.40000000596046448D));
            // 
            // pageHeaderSection1
            // 
            this.pageHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Cm(2.1000001430511475D);
            this.pageHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox1,
            this.textBox131,
            this.textBox132,
            this.textBox133,
            this.textBox134,
            this.textBox5,
            this.textBox6,
            this.textBox7,
            this.textBox9});
            this.pageHeaderSection1.Name = "pageHeaderSection1";
            this.pageHeaderSection1.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.pageHeaderSection1.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.pageHeaderSection1.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            // 
            // textBox1
            // 
            this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.0575220100581646D), Telerik.Reporting.Drawing.Unit.Cm(0.40000000596046448D));
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.818155288696289D), Telerik.Reporting.Drawing.Unit.Cm(0.70000004768371582D));
            this.textBox1.Style.Font.Bold = true;
            this.textBox1.Style.Font.Name = "黑体";
            this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(15D);
            this.textBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox1.Value = "{Fields.BranchName}配件发运单";
            // 
            // textBox131
            // 
            this.textBox131.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.05000000074505806D), Telerik.Reporting.Drawing.Unit.Cm(1.4319361448287964D));
            this.textBox131.Name = "textBox131";
            this.textBox131.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.2519192695617676D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox131.Style.Font.Name = "宋体";
            this.textBox131.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox131.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox131.Value = "发运单编号：";
            // 
            // textBox132
            // 
            this.textBox132.CanShrink = true;
            this.textBox132.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.3021194934844971D), Telerik.Reporting.Drawing.Unit.Cm(1.4319361448287964D));
            this.textBox132.Name = "textBox132";
            this.textBox132.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.0559892654418945D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox132.Style.Font.Name = "宋体";
            this.textBox132.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox132.Value = "= Fields.PartsShippingOrderCode";
            // 
            // textBox133
            // 
            this.textBox133.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(8.3583087921142578D), Telerik.Reporting.Drawing.Unit.Cm(1.4319361448287964D));
            this.textBox133.Name = "textBox133";
            this.textBox133.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.6862430572509766D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox133.Style.Font.Name = "宋体";
            this.textBox133.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox133.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox133.Value = "实际到货日期（必填）：";
            // 
            // textBox134
            // 
            this.textBox134.CanShrink = true;
            this.textBox134.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.796252250671387D), Telerik.Reporting.Drawing.Unit.Cm(1.4319361448287964D));
            this.textBox134.Name = "textBox134";
            this.textBox134.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.4699999988079071D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox134.Style.Font.Name = "宋体";
            this.textBox134.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox134.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox134.Value = "年";
            // 
            // textBox5
            // 
            this.textBox5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.001893997192383D), Telerik.Reporting.Drawing.Unit.Cm(1.4319361448287964D));
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.4699999988079071D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox5.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox5.Value = "月";
            // 
            // textBox6
            // 
            this.textBox6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.119160652160645D), Telerik.Reporting.Drawing.Unit.Cm(1.4319361448287964D));
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.4699999988079071D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox6.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox6.Value = "日";
            // 
            // textBox7
            // 
            this.textBox7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(16.144266128540039D), Telerik.Reporting.Drawing.Unit.Cm(1.4319361448287964D));
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.4699999988079071D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox7.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox7.Value = "分";
            // 
            // textBox9
            // 
            this.textBox9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(16.927911758422852D), Telerik.Reporting.Drawing.Unit.Cm(1.4319361448287964D));
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.5362446308135986D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox9.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox9.Value = "接收人：";
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Cm(6.2999992370605469D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.table1,
            this.table2,
            this.table6,
            this.table9,
            this.textBox58,
            this.textBox61,
            this.textBox60,
            this.textBox56,
            this.textBox62,
            this.textBox63,
            this.textBox64,
            this.textBox66,
            this.textBox50,
            this.textBox65,
            this.textBox67,
            this.textBox68,
            this.textBox2,
            this.textBox11,
            this.textBox12,
            this.textBox13,
            this.textBox14,
            this.textBox15,
            this.textBox16,
            this.textBox17,
            this.textBox19,
            this.textBox21,
            this.textBox23,
            this.textBox25,
            this.textBox69,
            this.textBox70,
            this.textBox71,
            this.textBox72,
            this.textBox73,
            this.textBox74,
            this.textBox75,
            this.textBox76,
            this.textBox20,
            this.textBox24});
            this.detail.Name = "detail";
            this.detail.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.detail.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.detail.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            // 
            // table1
            // 
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.9311330318450928D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.2021846771240234D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.7462024688720703D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.6941665410995483D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0D)));
            this.table1.Body.SetCellContent(0, 0, this.textBox30);
            this.table1.Body.SetCellContent(0, 1, this.textBox31);
            this.table1.Body.SetCellContent(0, 2, this.textBox32);
            this.table1.Body.SetCellContent(0, 3, this.textBox33);
            tableGroup1.Name = "Group2";
            tableGroup1.ReportItem = this.textBox10;
            tableGroup2.ReportItem = this.textBox3;
            tableGroup3.Name = "Group1";
            tableGroup3.ReportItem = this.textBox8;
            tableGroup4.ReportItem = this.textBox4;
            this.table1.ColumnGroups.Add(tableGroup1);
            this.table1.ColumnGroups.Add(tableGroup2);
            this.table1.ColumnGroups.Add(tableGroup3);
            this.table1.ColumnGroups.Add(tableGroup4);
            this.table1.DataSource = this.PartsShippingOrderDetailDataSource;
            this.table1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox30,
            this.textBox31,
            this.textBox32,
            this.textBox33,
            this.textBox10,
            this.textBox3,
            this.textBox8,
            this.textBox4});
            this.table1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(11.301990509033203D), Telerik.Reporting.Drawing.Unit.Cm(0.50030052661895752D));
            this.table1.Name = "table1";
            tableGroup6.Name = "Group8";
            tableGroup5.ChildGroups.Add(tableGroup6);
            tableGroup5.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup5.Name = "DetailGroup";
            this.table1.RowGroups.Add(tableGroup5);
            this.table1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.5736865997314453D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.table1.Style.Font.Name = "宋体";
            this.table1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            // 
            // textBox30
            // 
            this.textBox30.Name = "textBox30";
            this.textBox30.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9311326742172241D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.textBox30.StyleName = "";
            this.textBox30.Value = "";
            // 
            // textBox31
            // 
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.2021849155426025D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.textBox31.StyleName = "";
            // 
            // textBox32
            // 
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.7462025880813599D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.textBox32.StyleName = "";
            this.textBox32.Value = "";
            // 
            // textBox33
            // 
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.6941665410995483D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.textBox33.StyleName = "";
            // 
            // PartsShippingOrderDetailDataSource
            // 
            this.PartsShippingOrderDetailDataSource.DataMember = "GetAPartsShippingOrderDetailByIdForPrint";
            this.PartsShippingOrderDetailDataSource.DataSource = typeof(Sunlight.Silverlight.Dcs.Web.DcsDomainService);
            this.PartsShippingOrderDetailDataSource.Name = "PartsShippingOrderDetailDataSource";
            this.PartsShippingOrderDetailDataSource.Parameters.AddRange(new Telerik.Reporting.ObjectDataSourceParameter[] {
            new Telerik.Reporting.ObjectDataSourceParameter("PartsShippingOrderId", typeof(int), "= Parameters.agencyPartsShippingOrder.Value")});
            // 
            // table2
            // 
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.84437543153762817D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(7.1160306930541992D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(4.2316255569458008D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.0216490030288696D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.94221371412277222D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.8738038539886475D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.1014147996902466D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.8157365322113037D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.69999992847442627D)));
            this.table2.Body.SetCellContent(0, 0, this.textBox37);
            this.table2.Body.SetCellContent(0, 1, this.textBox38);
            this.table2.Body.SetCellContent(0, 2, this.textBox39);
            this.table2.Body.SetCellContent(0, 3, this.textBox41);
            this.table2.Body.SetCellContent(0, 4, this.textBox43);
            this.table2.Body.SetCellContent(0, 5, this.textBox45);
            this.table2.Body.SetCellContent(0, 6, this.textBox47);
            this.table2.Body.SetCellContent(0, 7, this.textBox49);
            tableGroup7.Name = "Group7";
            tableGroup7.ReportItem = this.textBox34;
            tableGroup8.Name = "Group8";
            tableGroup8.ReportItem = this.textBox35;
            tableGroup9.Name = "Group9";
            tableGroup9.ReportItem = this.textBox36;
            tableGroup10.Name = "Group10";
            tableGroup10.ReportItem = this.textBox40;
            tableGroup11.Name = "Group11";
            tableGroup11.ReportItem = this.textBox42;
            tableGroup12.Name = "Group12";
            tableGroup12.ReportItem = this.textBox44;
            tableGroup13.Name = "Group13";
            tableGroup13.ReportItem = this.textBox46;
            tableGroup14.Name = "Group14";
            tableGroup14.ReportItem = this.textBox48;
            this.table2.ColumnGroups.Add(tableGroup7);
            this.table2.ColumnGroups.Add(tableGroup8);
            this.table2.ColumnGroups.Add(tableGroup9);
            this.table2.ColumnGroups.Add(tableGroup10);
            this.table2.ColumnGroups.Add(tableGroup11);
            this.table2.ColumnGroups.Add(tableGroup12);
            this.table2.ColumnGroups.Add(tableGroup13);
            this.table2.ColumnGroups.Add(tableGroup14);
            this.table2.ColumnHeadersPrintOnEveryPage = true;
            this.table2.DataSource = this.PartsShippingOrderDetailDataSource;
            this.table2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox37,
            this.textBox38,
            this.textBox39,
            this.textBox41,
            this.textBox43,
            this.textBox45,
            this.textBox47,
            this.textBox49,
            this.textBox34,
            this.textBox35,
            this.textBox36,
            this.textBox40,
            this.textBox42,
            this.textBox44,
            this.textBox46,
            this.textBox48});
            this.table2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.057522125542163849D), Telerik.Reporting.Drawing.Unit.Cm(5.09999942779541D));
            this.table2.Name = "table2";
            tableGroup15.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup15.Name = "DetailGroup";
            this.table2.RowGroups.Add(tableGroup15);
            this.table2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.946849822998047D), Telerik.Reporting.Drawing.Unit.Cm(1.1999999284744263D));
            this.table2.Style.Font.Name = "宋体";
            // 
            // textBox37
            // 
            this.textBox37.CanShrink = true;
            this.textBox37.Name = "textBox37";
            this.textBox37.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.8443756103515625D), Telerik.Reporting.Drawing.Unit.Cm(0.69999992847442627D));
            this.textBox37.Value = "= RowNumber()";
            // 
            // textBox38
            // 
            this.textBox38.CanShrink = true;
            this.textBox38.Name = "textBox38";
            this.textBox38.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.1160297393798828D), Telerik.Reporting.Drawing.Unit.Cm(0.69999992847442627D));
            this.textBox38.Value = "= Fields.SparePartCode";
            // 
            // textBox39
            // 
            this.textBox39.CanShrink = true;
            this.textBox39.Name = "textBox39";
            this.textBox39.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.2316255569458008D), Telerik.Reporting.Drawing.Unit.Cm(0.69999992847442627D));
            this.textBox39.Value = "= Fields.SparePart.Name";
            // 
            // textBox41
            // 
            this.textBox41.CanShrink = true;
            this.textBox41.Name = "textBox41";
            this.textBox41.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.0216487646102905D), Telerik.Reporting.Drawing.Unit.Cm(0.69999992847442627D));
            this.textBox41.StyleName = "";
            this.textBox41.Value = "= Fields.SparePart.Weight";
            // 
            // textBox43
            // 
            this.textBox43.CanShrink = true;
            this.textBox43.Name = "textBox43";
            this.textBox43.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.94221347570419312D), Telerik.Reporting.Drawing.Unit.Cm(0.69999992847442627D));
            this.textBox43.StyleName = "";
            this.textBox43.Value = "= Fields.SparePart.MeasureUnit";
            // 
            // textBox45
            // 
            this.textBox45.CanShrink = true;
            this.textBox45.Name = "textBox45";
            this.textBox45.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8738048076629639D), Telerik.Reporting.Drawing.Unit.Cm(0.69999992847442627D));
            this.textBox45.StyleName = "";
            this.textBox45.Value = "";
            // 
            // textBox47
            // 
            this.textBox47.CanShrink = true;
            this.textBox47.Name = "textBox47";
            this.textBox47.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.101414680480957D), Telerik.Reporting.Drawing.Unit.Cm(0.69999992847442627D));
            this.textBox47.StyleName = "";
            this.textBox47.Value = "= Fields.ShippingAmount";
            // 
            // textBox49
            // 
            this.textBox49.CanShrink = true;
            this.textBox49.Name = "textBox49";
            this.textBox49.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.8157370090484619D), Telerik.Reporting.Drawing.Unit.Cm(0.69999992847442627D));
            this.textBox49.StyleName = "";
            this.textBox49.Value = "";
            // 
            // table6
            // 
            this.table6.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.7885689735412598D)));
            this.table6.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.9342972040176392D)));
            this.table6.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.6560943126678467D)));
            this.table6.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.79229879379272461D)));
            this.table6.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0D)));
            this.table6.Body.SetCellContent(0, 0, this.textBox93);
            this.table6.Body.SetCellContent(0, 1, this.textBox94);
            this.table6.Body.SetCellContent(0, 2, this.textBox95);
            this.table6.Body.SetCellContent(0, 3, this.textBox97);
            tableGroup16.ReportItem = this.textBox90;
            tableGroup18.Name = "group6";
            tableGroup17.ChildGroups.Add(tableGroup18);
            tableGroup17.ChildGroups.Add(tableGroup19);
            tableGroup17.ReportItem = this.textBox91;
            tableGroup20.Name = "Group1";
            tableGroup20.ReportItem = this.textBox96;
            this.table6.ColumnGroups.Add(tableGroup16);
            this.table6.ColumnGroups.Add(tableGroup17);
            this.table6.ColumnGroups.Add(tableGroup20);
            this.table6.DataSource = this.PartsShippingOrderDetailDataSource;
            this.table6.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox93,
            this.textBox94,
            this.textBox95,
            this.textBox97,
            this.textBox90,
            this.textBox91,
            this.textBox96});
            this.table6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(11.29271411895752D), Telerik.Reporting.Drawing.Unit.Cm(2.0007009506225586D));
            this.table6.Name = "table6";
            tableGroup21.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup21.Name = "DetailGroup";
            this.table6.RowGroups.Add(tableGroup21);
            this.table6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.1712589263916016D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.table6.Style.Font.Name = "宋体";
            // 
            // textBox93
            // 
            this.textBox93.Name = "textBox93";
            this.textBox93.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.7885687351226807D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            // 
            // textBox94
            // 
            this.textBox94.Name = "textBox94";
            this.textBox94.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9342966079711914D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            // 
            // textBox95
            // 
            this.textBox95.Name = "textBox95";
            this.textBox95.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.6560947895050049D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            // 
            // textBox97
            // 
            this.textBox97.Name = "textBox97";
            this.textBox97.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.79229885339736938D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.textBox97.StyleName = "";
            // 
            // table9
            // 
            this.table9.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.6562507152557373D)));
            this.table9.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.502915620803833D)));
            this.table9.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.099999979138374329D)));
            this.table9.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0D)));
            this.table9.Body.SetCellContent(0, 0, this.textBox119);
            this.table9.Body.SetCellContent(0, 1, this.textBox120);
            this.table9.Body.SetCellContent(0, 2, this.textBox121);
            tableGroup22.ReportItem = this.textBox116;
            tableGroup23.ReportItem = this.textBox117;
            tableGroup24.ReportItem = this.textBox118;
            this.table9.ColumnGroups.Add(tableGroup22);
            this.table9.ColumnGroups.Add(tableGroup23);
            this.table9.ColumnGroups.Add(tableGroup24);
            this.table9.DataSource = null;
            this.table9.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox119,
            this.textBox120,
            this.textBox121,
            this.textBox116,
            this.textBox117,
            this.textBox118});
            this.table9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.700000762939453D), Telerik.Reporting.Drawing.Unit.Cm(1.5999997854232788D));
            this.table9.Name = "table9";
            tableGroup25.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup25.Name = "DetailGroup";
            this.table9.RowGroups.Add(tableGroup25);
            this.table9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.2591662406921387D), Telerik.Reporting.Drawing.Unit.Cm(0.40000000596046448D));
            this.table9.Style.Font.Name = "宋体";
            // 
            // textBox119
            // 
            this.textBox119.Name = "textBox119";
            this.textBox119.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.6562508344650269D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            // 
            // textBox120
            // 
            this.textBox120.Name = "textBox120";
            this.textBox120.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.5029163360595703D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            // 
            // textBox121
            // 
            this.textBox121.Name = "textBox121";
            this.textBox121.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.10000000149011612D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            // 
            // textBox58
            // 
            this.textBox58.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(1.5005003213882446D));
            this.textBox58.Name = "textBox58";
            this.textBox58.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.4718751907348633D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox58.Style.Font.Name = "宋体";
            this.textBox58.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox58.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox58.Value = "发运单位名称：";
            // 
            // textBox61
            // 
            this.textBox61.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.4889750480651855D), Telerik.Reporting.Drawing.Unit.Cm(1.5005003213882446D));
            this.textBox61.Name = "textBox61";
            this.textBox61.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.8037385940551758D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox61.Style.Font.Name = "宋体";
            this.textBox61.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox61.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox61.Value = "= Fields.ShippingCompanyName";
            // 
            // textBox60
            // 
            this.textBox60.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(11.292715072631836D), Telerik.Reporting.Drawing.Unit.Cm(1.5005003213882446D));
            this.textBox60.Name = "textBox60";
            this.textBox60.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.7883299589157105D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox60.Style.Font.Name = "宋体";
            this.textBox60.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox60.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox60.Value = "发运方式：";
            // 
            // textBox56
            // 
            this.textBox56.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(13.09999942779541D), Telerik.Reporting.Drawing.Unit.Cm(1.5005003213882446D));
            this.textBox56.Name = "textBox56";
            this.textBox56.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.5079145431518555D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox56.Style.Font.Name = "宋体";
            this.textBox56.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox56.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox56.Value = "= Sunlight.Silverlight.Dcs.Web.Reporting.CommonConverter.UsedPartsShippingOrderTy" +
    "peMethodFormat(Fields.ShippingMethod)";
            // 
            // textBox62
            // 
            this.textBox62.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(0.00010012308484874666D));
            this.textBox62.Name = "textBox62";
            this.textBox62.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.4889752864837646D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox62.Style.Font.Name = "宋体";
            this.textBox62.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox62.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox62.Value = "收货单位名称：";
            // 
            // textBox63
            // 
            this.textBox63.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.4891753196716309D), Telerik.Reporting.Drawing.Unit.Cm(0.00010012308484874666D));
            this.textBox63.Name = "textBox63";
            this.textBox63.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.2095937728881836D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox63.Style.Font.Name = "宋体";
            this.textBox63.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox63.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox63.Value = "= Fields.ReceivingCompanyName";
            // 
            // textBox64
            // 
            this.textBox64.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.7605762481689453D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.textBox64.Name = "textBox64";
            this.textBox64.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.4889752864837646D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox64.Style.Font.Name = "宋体";
            this.textBox64.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox64.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox64.Value = "收货单位地址：";
            // 
            // textBox66
            // 
            this.textBox66.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.249552726745606D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.textBox66.Name = "textBox66";
            this.textBox66.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.6411194801330566D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox66.Style.Font.Name = "宋体";
            this.textBox66.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox66.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox66.Value = "= Fields.ReceivingAddress";
            // 
            // textBox50
            // 
            this.textBox50.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(0.50030052661895752D));
            this.textBox50.Name = "textBox50";
            this.textBox50.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.4889752864837646D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox50.Style.Font.Name = "宋体";
            this.textBox50.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox50.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox50.Value = "收货单位电话：";
            // 
            // textBox65
            // 
            this.textBox65.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.4891753196716309D), Telerik.Reporting.Drawing.Unit.Cm(0.50030052661895752D));
            this.textBox65.Name = "textBox65";
            this.textBox65.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.5099999904632568D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox65.Style.Font.Name = "宋体";
            this.textBox65.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox65.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox65.Value = "= Fields.ReceivingPhone";
            // 
            // textBox67
            // 
            this.textBox67.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(5D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox67.Name = "textBox67";
            this.textBox67.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.4997996091842651D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox67.Style.Font.Name = "宋体";
            this.textBox67.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox67.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox67.Value = "联系人：";
            // 
            // textBox68
            // 
            this.textBox68.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(6.5399999618530273D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox68.Name = "textBox68";
            this.textBox68.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.960824728012085D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox68.Style.Font.Name = "宋体";
            this.textBox68.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox68.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox68.Value = "= Fields.ReceivingPerson";
            // 
            // textBox2
            // 
            this.textBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.057522125542163849D), Telerik.Reporting.Drawing.Unit.Cm(1.0002998113632202D));
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.4889752864837646D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox2.Style.Font.Name = "宋体";
            this.textBox2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox2.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox2.Value = "承运单位名称：";
            // 
            // textBox11
            // 
            this.textBox11.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.5466976165771484D), Telerik.Reporting.Drawing.Unit.Cm(1.0002998113632202D));
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.7460165023803711D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox11.Style.Font.Name = "宋体";
            this.textBox11.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox11.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox11.Value = "= Fields.LogisticCompanyName";
            // 
            // textBox12
            // 
            this.textBox12.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(11.302090644836426D), Telerik.Reporting.Drawing.Unit.Cm(1.0002998113632202D));
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.2690255641937256D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox12.Style.Font.Name = "宋体";
            this.textBox12.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox12.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox12.Value = "电话：";
            // 
            // textBox13
            // 
            this.textBox13.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.766045570373535D), Telerik.Reporting.Drawing.Unit.Cm(1.0002998113632202D));
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.6692647933959961D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox13.Style.Font.Name = "宋体";
            this.textBox13.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox13.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox13.Value = "= Fields.LogisticCompanyPhone";
            // 
            // textBox14
            // 
            this.textBox14.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(16.4353084564209D), Telerik.Reporting.Drawing.Unit.Cm(1.0005009174346924D));
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.7140274047851563D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox14.Style.Font.Name = "宋体";
            this.textBox14.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox14.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox14.Value = "联系人：";
            // 
            // textBox15
            // 
            this.textBox15.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(18.1815128326416D), Telerik.Reporting.Drawing.Unit.Cm(1.0002998113632202D));
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.6776536703109741D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox15.Style.Font.Name = "宋体";
            this.textBox15.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox15.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox15.Value = "= Fields.LogisticCompanyPerson";
            // 
            // textBox16
            // 
            this.textBox16.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(2.0007009506225586D));
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.4718751907348633D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox16.Style.Font.Name = "宋体";
            this.textBox16.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox16.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox16.Value = "销售订单编号：";
            // 
            // textBox17
            // 
            this.textBox17.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.5466976165771484D), Telerik.Reporting.Drawing.Unit.Cm(2.0007009506225586D));
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.7458162307739258D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox17.Style.Font.Name = "宋体";
            this.textBox17.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox17.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox17.Value = "= Sunlight.Silverlight.Dcs.Web.Reporting.CommonConverter.GetIsOrNotOriginalRequir" +
    "ementBillCode(Fields.OriginalRequirementBillType, Fields.OriginalRequirementBill" +
    "Code)";
            // 
            // textBox19
            // 
            this.textBox19.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(2.500901460647583D));
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.4718751907348633D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox19.Style.Font.Name = "宋体";
            this.textBox19.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox19.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox19.Value = "销售订单类型：";
            // 
            // textBox21
            // 
            this.textBox21.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.017100054770708084D), Telerik.Reporting.Drawing.Unit.Cm(3.0011019706726074D));
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.4718751907348633D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox21.Style.Font.Name = "宋体";
            this.textBox21.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox21.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox21.Value = "出库单号：";
            // 
            // textBox23
            // 
            this.textBox23.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.5466976165771484D), Telerik.Reporting.Drawing.Unit.Cm(2.500901460647583D));
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.6695632934570312D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox23.Style.Font.Name = "宋体";
            this.textBox23.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox23.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox23.Value = "= Sunlight.Silverlight.Dcs.Web.Reporting.CommonConverter.GetIsOrNotPartsSalesOrde" +
    "r(Fields.OriginalRequirementBillType, Fields.PartsSalesOrderTypeName)";
            // 
            // textBox25
            // 
            this.textBox25.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(5.216461181640625D), Telerik.Reporting.Drawing.Unit.Cm(2.500901460647583D));
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.7835386991500855D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox25.Style.Font.Name = "宋体";
            this.textBox25.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox25.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox25.Value = "发运日期：";
            // 
            // textBox69
            // 
            this.textBox69.Format = "{0:d}";
            this.textBox69.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(7.0001997947692871D), Telerik.Reporting.Drawing.Unit.Cm(2.5007007122039795D));
            this.textBox69.Name = "textBox69";
            this.textBox69.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.3438842296600342D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox69.Style.Font.Name = "宋体";
            this.textBox69.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox69.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox69.Value = "= Fields.ShippingDate";
            // 
            // textBox70
            // 
            this.textBox70.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.344284057617188D), Telerik.Reporting.Drawing.Unit.Cm(2.500901460647583D));
            this.textBox70.Name = "textBox70";
            this.textBox70.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.4836218357086182D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox70.Style.Font.Name = "宋体";
            this.textBox70.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox70.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox70.Value = "要求到货日期：";
            // 
            // textBox71
            // 
            this.textBox71.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.828106880187988D), Telerik.Reporting.Drawing.Unit.Cm(2.500901460647583D));
            this.textBox71.Name = "textBox71";
            this.textBox71.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.4836218357086182D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox71.Style.Font.Name = "宋体";
            this.textBox71.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox71.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox71.Value = "= Fields.RequestedArrivalDate";
            // 
            // textBox72
            // 
            this.textBox72.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.331045150756836D), Telerik.Reporting.Drawing.Unit.Cm(2.5007007122039795D));
            this.textBox72.Name = "textBox72";
            this.textBox72.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.133110523223877D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox72.Style.Font.Name = "宋体";
            this.textBox72.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox72.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox72.Value = "运输里程（公里）：";
            // 
            // textBox73
            // 
            this.textBox73.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(18.464357376098633D), Telerik.Reporting.Drawing.Unit.Cm(2.500901460647583D));
            this.textBox73.Name = "textBox73";
            this.textBox73.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.394810676574707D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox73.Style.Font.Name = "宋体";
            this.textBox73.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox73.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox73.Value = "= Fields.TransportMileage";
            // 
            // textBox74
            // 
            this.textBox74.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.4891753196716309D), Telerik.Reporting.Drawing.Unit.Cm(3.0011022090911865D));
            this.textBox74.Name = "textBox74";
            this.textBox74.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(17.369993209838867D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox74.Style.Font.Name = "宋体";
            this.textBox74.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox74.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox74.Value = "= Fields.PartsOutboundBillCode";
            // 
            // textBox75
            // 
            this.textBox75.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.5D), Telerik.Reporting.Drawing.Unit.Cm(4.099799633026123D));
            this.textBox75.Name = "textBox75";
            this.textBox75.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(17.369993209838867D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox75.Style.Font.Name = "宋体";
            this.textBox75.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox75.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox75.Value = "= Fields.OrderApproveComment";
            // 
            // textBox76
            // 
            this.textBox76.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(4.099799633026123D));
            this.textBox76.Name = "textBox76";
            this.textBox76.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.4718751907348633D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox76.Style.Font.Name = "宋体";
            this.textBox76.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox76.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox76.Value = "订单审批意见：";
            // 
            // textBox20
            // 
            this.textBox20.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(4.5997991561889648D));
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.1999999284744263D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox20.Style.Font.Name = "宋体";
            this.textBox20.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox20.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox20.Value = "备注：";
            // 
            // textBox24
            // 
            this.textBox24.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.2001996040344238D), Telerik.Reporting.Drawing.Unit.Cm(4.5997991561889648D));
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(18.641868591308594D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox24.Style.Font.Name = "宋体";
            this.textBox24.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox24.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox24.Value = "= Fields.Remark";
            // 
            // PartsShippingOrderDataSource
            // 
            this.PartsShippingOrderDataSource.DataMember = "GetVirtualAgencyPartsShippingOrderRefById";
            this.PartsShippingOrderDataSource.DataSource = "Sunlight.Silverlight.Dcs.Web.DcsDomainService, Sunlight.Silverlight.Dcs.Web, Vers" +
    "ion=1.0.0.0, Culture=neutral, PublicKeyToken=null";
            this.PartsShippingOrderDataSource.Name = "PartsShippingOrderDataSource";
            this.PartsShippingOrderDataSource.Parameters.AddRange(new Telerik.Reporting.ObjectDataSourceParameter[] {
            new Telerik.Reporting.ObjectDataSourceParameter("PartsShippingOrderId", typeof(int), "= Parameters.agencyPartsShippingOrder.Value")});
            // 
            // pageFooterSection1
            // 
            this.pageFooterSection1.Height = Telerik.Reporting.Drawing.Unit.Cm(3.3968751430511475D);
            this.pageFooterSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox53,
            this.textBox55,
            this.textBox57,
            this.textBox59,
            this.textBox154,
            this.textBox155,
            this.textBox156,
            this.textBox157,
            this.textBox158,
            this.textBox159,
            this.textBox160,
            this.textBox162,
            this.textBox163,
            this.textBox164,
            this.textBox165,
            this.textBox166,
            this.textBox167,
            this.textBox86,
            this.textBox112,
            this.textBox130,
            this.textBox18,
            this.textBox22,
            this.textBox26,
            this.textBox27,
            this.textBox28,
            this.textBox29,
            this.textBox51,
            this.textBox52,
            this.textBox54});
            this.pageFooterSection1.Name = "pageFooterSection1";
            this.pageFooterSection1.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.pageFooterSection1.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.pageFooterSection1.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.pageFooterSection1.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.pageFooterSection1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.pageFooterSection1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.pageFooterSection1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            // 
            // textBox53
            // 
            this.textBox53.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.05000000074505806D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.textBox53.Name = "textBox53";
            this.textBox53.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9500000476837158D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox53.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox53.Style.Font.Bold = false;
            this.textBox53.Style.Font.Name = "宋体";
            this.textBox53.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox53.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox53.Value = "制单单位:";
            // 
            // textBox55
            // 
            this.textBox55.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.000199556350708D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.textBox55.Name = "textBox55";
            this.textBox55.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.3579092025756836D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox55.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox55.Style.Font.Bold = false;
            this.textBox55.Style.Font.Name = "宋体";
            this.textBox55.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox55.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox55.Value = "= Fields.ShippingCompanyName";
            // 
            // textBox57
            // 
            this.textBox57.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(8.3999996185302734D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.textBox57.Name = "textBox57";
            this.textBox57.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.4600000381469727D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox57.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox57.Style.Font.Bold = false;
            this.textBox57.Style.Font.Name = "宋体";
            this.textBox57.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox57.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox57.Value = "制单人:";
            // 
            // textBox59
            // 
            this.textBox59.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.8999996185302734D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.textBox59.Name = "textBox59";
            this.textBox59.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.0631392002105713D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox59.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox59.Style.Font.Name = "宋体";
            this.textBox59.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox59.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox59.Value = "= Fields.CreatorName";
            // 
            // textBox154
            // 
            this.textBox154.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(11.963337898254395D), Telerik.Reporting.Drawing.Unit.Cm(0.00020024616969749332D));
            this.textBox154.Name = "textBox154";
            this.textBox154.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.7383198738098145D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox154.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox154.Style.Font.Bold = false;
            this.textBox154.Style.Font.Name = "宋体";
            this.textBox154.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox154.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox154.Value = "制单时间:";
            // 
            // textBox155
            // 
            this.textBox155.Format = "{0:G}";
            this.textBox155.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(13.70185661315918D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.textBox155.Name = "textBox155";
            this.textBox155.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9549415111541748D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox155.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox155.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox155.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox155.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox155.Value = "= Fields.CreateTime";
            // 
            // textBox156
            // 
            this.textBox156.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(16.656999588012695D), Telerik.Reporting.Drawing.Unit.Cm(0.00020024616969749332D));
            this.textBox156.Name = "textBox156";
            this.textBox156.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.386972188949585D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox156.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox156.Style.Font.Bold = false;
            this.textBox156.Style.Font.Name = "宋体";
            this.textBox156.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox156.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox156.Value = "打印人:";
            // 
            // textBox157
            // 
            this.textBox157.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(18.044172286987305D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.textBox157.Name = "textBox157";
            this.textBox157.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9099999666213989D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox157.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox157.Style.Font.Name = "宋体";
            this.textBox157.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox157.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox157.Value = "= Parameters.UserName.Value";
            // 
            // textBox158
            // 
            this.textBox158.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.05000000074505806D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox158.Name = "textBox158";
            this.textBox158.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox158.Style.Font.Bold = false;
            this.textBox158.Style.Font.Name = "宋体";
            this.textBox158.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox158.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox158.Value = "打印时间:";
            // 
            // textBox159
            // 
            this.textBox159.Format = "{0:G}";
            this.textBox159.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.0501997470855713D), Telerik.Reporting.Drawing.Unit.Cm(0.50020039081573486D));
            this.textBox159.Name = "textBox159";
            this.textBox159.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.8399999141693115D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox159.Style.Font.Name = "宋体";
            this.textBox159.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox159.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox159.Value = "= Now()";
            // 
            // textBox160
            // 
            this.textBox160.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.6640844345092773D), Telerik.Reporting.Drawing.Unit.Cm(0.50020039081573486D));
            this.textBox160.Name = "textBox160";
            this.textBox160.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.68000000715255737D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox160.Style.Font.Name = "宋体";
            this.textBox160.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox160.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox160.Value = "注:";
            // 
            // textBox162
            // 
            this.textBox162.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.644065856933594D), Telerik.Reporting.Drawing.Unit.Cm(0.50040066242218018D));
            this.textBox162.Name = "textBox162";
            this.textBox162.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.5D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox162.Style.Font.Name = "宋体";
            this.textBox162.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox162.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox162.Value = "第";
            // 
            // textBox163
            // 
            this.textBox163.Angle = 0D;
            this.textBox163.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(16.144266128540039D), Telerik.Reporting.Drawing.Unit.Cm(0.50040066242218018D));
            this.textBox163.Name = "textBox163";
            this.textBox163.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.61000001430511475D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox163.Style.Font.Name = "宋体";
            this.textBox163.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox163.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox163.Value = "= PageNumber";
            // 
            // textBox164
            // 
            this.textBox164.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(16.754467010498047D), Telerik.Reporting.Drawing.Unit.Cm(0.50040066242218018D));
            this.textBox164.Name = "textBox164";
            this.textBox164.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.51999998092651367D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox164.Style.Font.Name = "宋体";
            this.textBox164.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox164.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox164.Value = "页";
            // 
            // textBox165
            // 
            this.textBox165.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(17.274667739868164D), Telerik.Reporting.Drawing.Unit.Cm(0.50040066242218018D));
            this.textBox165.Name = "textBox165";
            this.textBox165.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.54000002145767212D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox165.Style.Font.Name = "宋体";
            this.textBox165.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox165.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox165.Value = "共";
            // 
            // textBox166
            // 
            this.textBox166.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(17.81486701965332D), Telerik.Reporting.Drawing.Unit.Cm(0.50020039081573486D));
            this.textBox166.Name = "textBox166";
            this.textBox166.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.73000001907348633D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox166.Style.Font.Name = "宋体";
            this.textBox166.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox166.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox166.Value = "= PageCount";
            // 
            // textBox167
            // 
            this.textBox167.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(18.545066833496094D), Telerik.Reporting.Drawing.Unit.Cm(0.50040066242218018D));
            this.textBox167.Name = "textBox167";
            this.textBox167.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.68999999761581421D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox167.Style.Font.Name = "宋体";
            this.textBox167.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox167.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox167.Value = "页";
            // 
            // textBox86
            // 
            this.textBox86.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.05000000074505806D), Telerik.Reporting.Drawing.Unit.Cm(1D));
            this.textBox86.Name = "textBox86";
            this.textBox86.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.6383357048034668D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox86.Style.Font.Name = "宋体";
            this.textBox86.Value = "注：本单据一式四联，白色联仓库留底，";
            // 
            // textBox112
            // 
            this.textBox112.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.05000000074505806D), Telerik.Reporting.Drawing.Unit.Cm(1.5D));
            this.textBox112.Name = "textBox112";
            this.textBox112.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.6383357048034668D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox112.Style.Font.Name = "宋体";
            this.textBox112.Value = "黄/粉/蓝色联由承运商在收货单位签字盖章后，";
            // 
            // textBox130
            // 
            this.textBox130.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.05000000074505806D), Telerik.Reporting.Drawing.Unit.Cm(2D));
            this.textBox130.Name = "textBox130";
            this.textBox130.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.63833475112915D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox130.Style.Font.Name = "宋体";
            this.textBox130.Value = "黄色联保留收货单位，粉/蓝色联由承运商带回，";
            // 
            // textBox18
            // 
            this.textBox18.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.049999773502349854D), Telerik.Reporting.Drawing.Unit.Cm(2.5002002716064453D));
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.59999942779541D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox18.Style.Font.Name = "宋体";
            this.textBox18.Value = "蓝色联附在发票转配件分公司结算运费，完成后各自保存。";
            // 
            // textBox22
            // 
            this.textBox22.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.09999942779541D), Telerik.Reporting.Drawing.Unit.Cm(1.0999999046325684D));
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9296023845672607D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox22.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox22.Style.Font.Bold = false;
            this.textBox22.Style.Font.Name = "宋体";
            this.textBox22.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox22.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox22.Value = "收货仓库:";
            // 
            // textBox26
            // 
            this.textBox26.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.11312484741211D), Telerik.Reporting.Drawing.Unit.Cm(1.6000000238418579D));
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9296023845672607D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox26.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox26.Style.Font.Bold = false;
            this.textBox26.Style.Font.Name = "宋体";
            this.textBox26.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox26.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox26.Value = "发货仓库:";
            // 
            // textBox27
            // 
            this.textBox27.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.09999942779541D), Telerik.Reporting.Drawing.Unit.Cm(2.09999942779541D));
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9296023845672607D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox27.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox27.Style.Font.Bold = false;
            this.textBox27.Style.Font.Name = "宋体";
            this.textBox27.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox27.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox27.Value = "发货联系人:";
            // 
            // textBox28
            // 
            this.textBox28.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.383423805236816D), Telerik.Reporting.Drawing.Unit.Cm(2.09999942779541D));
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.2057374715805054D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox28.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox28.Style.Font.Bold = false;
            this.textBox28.Style.Font.Name = "宋体";
            this.textBox28.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox28.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox28.Value = "电话:";
            // 
            // textBox29
            // 
            this.textBox29.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(11.09999942779541D), Telerik.Reporting.Drawing.Unit.Cm(1.0999999046325684D));
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.7756786346435547D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox29.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox29.Style.Font.Bold = false;
            this.textBox29.Style.Font.Name = "宋体";
            this.textBox29.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox29.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox29.Value = "= Fields.ReceivingWarehouseName";
            // 
            // textBox51
            // 
            this.textBox51.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(11.09999942779541D), Telerik.Reporting.Drawing.Unit.Cm(1.6000000238418579D));
            this.textBox51.Name = "textBox51";
            this.textBox51.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.7756786346435547D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox51.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox51.Style.Font.Bold = false;
            this.textBox51.Style.Font.Name = "宋体";
            this.textBox51.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox51.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox51.Value = "= Fields.WarehouseName";
            // 
            // textBox52
            // 
            this.textBox52.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(11.09999942779541D), Telerik.Reporting.Drawing.Unit.Cm(2.09999942779541D));
            this.textBox52.Name = "textBox52";
            this.textBox52.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.2832245826721191D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox52.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox52.Style.Font.Bold = false;
            this.textBox52.Style.Font.Name = "宋体";
            this.textBox52.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox52.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox52.Value = "= Fields.Contact";
            // 
            // textBox54
            // 
            this.textBox54.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.600000381469727D), Telerik.Reporting.Drawing.Unit.Cm(2.09999942779541D));
            this.textBox54.Name = "textBox54";
            this.textBox54.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.5468566417694092D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox54.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox54.Style.Font.Bold = false;
            this.textBox54.Style.Font.Name = "宋体";
            this.textBox54.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox54.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox54.Value = "= Fields.PhoneNumber";
            // 
            // ReportAgencyPartsShippingOrders
            // 
            this.DataSource = this.PartsShippingOrderDataSource;
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pageHeaderSection1,
            this.detail,
            this.pageFooterSection1});
            this.Name = "ReportPartsShippingOrders";
            this.PageSettings.Landscape = true;
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Pixel(25D), Telerik.Reporting.Drawing.Unit.Pixel(19D), Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Custom;
            this.PageSettings.PaperSize = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(140D), Telerik.Reporting.Drawing.Unit.Mm(220D));
            reportParameter1.Name = "agencyPartsShippingOrder";
            reportParameter1.Type = Telerik.Reporting.ReportParameterType.Integer;
            reportParameter2.Name = "UserName";
            reportParameter2.Text = "UserName";
            this.ReportParameters.Add(reportParameter1);
            this.ReportParameters.Add(reportParameter2);
            this.Style.BackgroundColor = System.Drawing.Color.White;
            this.Style.Font.Name = "宋体";
            this.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
            this.Width = Telerik.Reporting.Drawing.Unit.Cm(20.018331527709961D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.PageHeaderSection pageHeaderSection1;
        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.PageFooterSection pageFooterSection1;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.Table table1;
        private Telerik.Reporting.TextBox textBox30;
        private Telerik.Reporting.TextBox textBox31;
        private Telerik.Reporting.TextBox textBox32;
        private Telerik.Reporting.TextBox textBox33;
        private Telerik.Reporting.TextBox textBox10;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.Table table2;
        private Telerik.Reporting.TextBox textBox37;
        private Telerik.Reporting.TextBox textBox38;
        private Telerik.Reporting.TextBox textBox39;
        private Telerik.Reporting.TextBox textBox34;
        private Telerik.Reporting.TextBox textBox35;
        private Telerik.Reporting.TextBox textBox36;
        private Telerik.Reporting.TextBox textBox41;
        private Telerik.Reporting.TextBox textBox43;
        private Telerik.Reporting.TextBox textBox45;
        private Telerik.Reporting.TextBox textBox47;
        private Telerik.Reporting.TextBox textBox49;
        private Telerik.Reporting.TextBox textBox40;
        private Telerik.Reporting.TextBox textBox42;
        private Telerik.Reporting.TextBox textBox44;
        private Telerik.Reporting.TextBox textBox46;
        private Telerik.Reporting.TextBox textBox48;
        private Telerik.Reporting.ObjectDataSource PartsShippingOrderDetailDataSource;
        private Telerik.Reporting.ObjectDataSource PartsShippingOrderDataSource;
        private Telerik.Reporting.Table table6;
        private Telerik.Reporting.TextBox textBox93;
        private Telerik.Reporting.TextBox textBox94;
        private Telerik.Reporting.TextBox textBox95;
        private Telerik.Reporting.TextBox textBox97;
        private Telerik.Reporting.TextBox textBox90;
        private Telerik.Reporting.TextBox textBox91;
        private Telerik.Reporting.TextBox textBox96;
        private Telerik.Reporting.Table table9;
        private Telerik.Reporting.TextBox textBox119;
        private Telerik.Reporting.TextBox textBox120;
        private Telerik.Reporting.TextBox textBox121;
        private Telerik.Reporting.TextBox textBox116;
        private Telerik.Reporting.TextBox textBox117;
        private Telerik.Reporting.TextBox textBox118;
        private Telerik.Reporting.TextBox textBox53;
        private Telerik.Reporting.TextBox textBox55;
        private Telerik.Reporting.TextBox textBox57;
        private Telerik.Reporting.TextBox textBox59;
        private Telerik.Reporting.TextBox textBox154;
        private Telerik.Reporting.TextBox textBox155;
        private Telerik.Reporting.TextBox textBox156;
        private Telerik.Reporting.TextBox textBox157;
        private Telerik.Reporting.TextBox textBox158;
        private Telerik.Reporting.TextBox textBox159;
        private Telerik.Reporting.TextBox textBox160;
        private Telerik.Reporting.TextBox textBox162;
        private Telerik.Reporting.TextBox textBox163;
        private Telerik.Reporting.TextBox textBox164;
        private Telerik.Reporting.TextBox textBox165;
        private Telerik.Reporting.TextBox textBox166;
        private Telerik.Reporting.TextBox textBox167;
        private Telerik.Reporting.TextBox textBox86;
        private Telerik.Reporting.TextBox textBox112;
        private Telerik.Reporting.TextBox textBox130;
        private Telerik.Reporting.TextBox textBox131;
        private Telerik.Reporting.TextBox textBox132;
        private Telerik.Reporting.TextBox textBox133;
        private Telerik.Reporting.TextBox textBox134;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.TextBox textBox9;
        private Telerik.Reporting.TextBox textBox18;
        private Telerik.Reporting.TextBox textBox22;
        private Telerik.Reporting.TextBox textBox26;
        private Telerik.Reporting.TextBox textBox27;
        private Telerik.Reporting.TextBox textBox28;
        private Telerik.Reporting.TextBox textBox29;
        private Telerik.Reporting.TextBox textBox51;
        private Telerik.Reporting.TextBox textBox52;
        private Telerik.Reporting.TextBox textBox54;
        private Telerik.Reporting.TextBox textBox58;
        private Telerik.Reporting.TextBox textBox61;
        private Telerik.Reporting.TextBox textBox60;
        private Telerik.Reporting.TextBox textBox56;
        private Telerik.Reporting.TextBox textBox62;
        private Telerik.Reporting.TextBox textBox63;
        private Telerik.Reporting.TextBox textBox64;
        private Telerik.Reporting.TextBox textBox66;
        private Telerik.Reporting.TextBox textBox50;
        private Telerik.Reporting.TextBox textBox65;
        private Telerik.Reporting.TextBox textBox67;
        private Telerik.Reporting.TextBox textBox68;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox11;
        private Telerik.Reporting.TextBox textBox12;
        private Telerik.Reporting.TextBox textBox13;
        private Telerik.Reporting.TextBox textBox14;
        private Telerik.Reporting.TextBox textBox15;
        private Telerik.Reporting.TextBox textBox16;
        private Telerik.Reporting.TextBox textBox17;
        private Telerik.Reporting.TextBox textBox19;
        private Telerik.Reporting.TextBox textBox21;
        private Telerik.Reporting.TextBox textBox23;
        private Telerik.Reporting.TextBox textBox25;
        private Telerik.Reporting.TextBox textBox69;
        private Telerik.Reporting.TextBox textBox70;
        private Telerik.Reporting.TextBox textBox71;
        private Telerik.Reporting.TextBox textBox72;
        private Telerik.Reporting.TextBox textBox73;
        private Telerik.Reporting.TextBox textBox74;
        private Telerik.Reporting.TextBox textBox75;
        private Telerik.Reporting.TextBox textBox76;
        private Telerik.Reporting.TextBox textBox20;
        private Telerik.Reporting.TextBox textBox24;
    }
}