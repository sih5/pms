﻿
namespace Sunlight.Silverlight.Dcs.Web.Reporting {
    using System;
    using Telerik.Reporting;

    /// <summary>
    /// Summary description for ReportMixLabel.
    /// </summary>
    public partial class ReportMixLabel : Report {
        public ReportMixLabel() {
            InitializeComponent();
        }
        private void ReportMixLabel_NeedDataSource(object sender, EventArgs e) {
            var service = new DcsDomainService();
            var report = (Telerik.Reporting.Processing.Report)sender;
            string parameter = report.Parameters["paraPartsStockId"].Value.ToString().Trim();
            var aa = parameter.Split(',');
            int[] a = new int[aa.Length];
            for(int i = 0; i < aa.Length; i++) {
                a[i] = int.Parse(aa[i]);
            }
            var data = service.GetVirtualPartsStockWithLablePrint(a);
            report.DataSource = data;
        }

    }
}