using System.Collections.Generic;
using System.Linq;

namespace Sunlight.Silverlight.Dcs.Web.Reporting {
    using System;

    /// <summary>
    /// Summary description for ReportPartsClaimOrderNewLabel.
    /// </summary>
    public partial class ReportPartsClaimOrderNewLabel : Telerik.Reporting.Report {
        public ReportPartsClaimOrderNewLabel() {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }

        public IEnumerable<Wrapper> IndexData(IEnumerable<PartsClaimOrderNew> data) {
            int i = 0;
            return data.Select(o => new Wrapper {
                Index = i++,
                Data = o
            });
        }
        public class Wrapper {
            public int Index {
                get;
                set;
            }
            public PartsClaimOrderNew Data {
                get;
                set;
            }
        }
        private void ReportPartsClaimOrderNewLabel_NeedDataSource(object sender, EventArgs e) {
            var service = new DcsDomainService();
            var report = (Telerik.Reporting.Processing.Report)sender;
            var data = service.GetPartsClaimOrderNewByIdPrint(report.Parameters["partsClaimOrderNewId"].Value.ToString());
            this.list2.DataSource = IndexData(data);
        }
    }
}