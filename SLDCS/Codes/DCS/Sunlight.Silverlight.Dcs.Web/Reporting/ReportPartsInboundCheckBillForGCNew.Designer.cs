namespace Sunlight.Silverlight.Dcs.Web.Reporting {
    partial class ReportPartsInboundCheckBillForGCNew {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup11 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup12 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup13 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup14 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup15 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup16 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup17 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup18 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup19 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup20 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.Barcodes.Code128Encoder code128Encoder1 = new Telerik.Reporting.Barcodes.Code128Encoder();
            Telerik.Reporting.ReportParameter reportParameter1 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter2 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.tbxSupplyCompany = new Telerik.Reporting.TextBox();
            this.textBox7 = new Telerik.Reporting.TextBox();
            this.textBox20 = new Telerik.Reporting.TextBox();
            this.textBox25 = new Telerik.Reporting.TextBox();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.textBox16 = new Telerik.Reporting.TextBox();
            this.textBox11 = new Telerik.Reporting.TextBox();
            this.textBox28 = new Telerik.Reporting.TextBox();
            this.textBox33 = new Telerik.Reporting.TextBox();
            this.textBox41 = new Telerik.Reporting.TextBox();
            this.textBox39 = new Telerik.Reporting.TextBox();
            this.textBox37 = new Telerik.Reporting.TextBox();
            this.textBox35 = new Telerik.Reporting.TextBox();
            this.textBox29 = new Telerik.Reporting.TextBox();
            this.detail = new Telerik.Reporting.DetailSection();
            this.table1 = new Telerik.Reporting.Table();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.textBox8 = new Telerik.Reporting.TextBox();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.textBox9 = new Telerik.Reporting.TextBox();
            this.textBox23 = new Telerik.Reporting.TextBox();
            this.textBox24 = new Telerik.Reporting.TextBox();
            this.textBox26 = new Telerik.Reporting.TextBox();
            this.textBox27 = new Telerik.Reporting.TextBox();
            this.PartsInboundCheckBillDataSource = new Telerik.Reporting.ObjectDataSource();
            this.table2 = new Telerik.Reporting.Table();
            this.textBox30 = new Telerik.Reporting.TextBox();
            this.textBox32 = new Telerik.Reporting.TextBox();
            this.textBox34 = new Telerik.Reporting.TextBox();
            this.textBox36 = new Telerik.Reporting.TextBox();
            this.textBox38 = new Telerik.Reporting.TextBox();
            this.textBox40 = new Telerik.Reporting.TextBox();
            this.textBox42 = new Telerik.Reporting.TextBox();
            this.textBox10 = new Telerik.Reporting.TextBox();
            this.textBox12 = new Telerik.Reporting.TextBox();
            this.textBox13 = new Telerik.Reporting.TextBox();
            this.textBox14 = new Telerik.Reporting.TextBox();
            this.textBox15 = new Telerik.Reporting.TextBox();
            this.textBox17 = new Telerik.Reporting.TextBox();
            this.textBox18 = new Telerik.Reporting.TextBox();
            this.textBox22 = new Telerik.Reporting.TextBox();
            this.textBox21 = new Telerik.Reporting.TextBox();
            this.PartsInboundCheckBillDetailDataSource = new Telerik.Reporting.ObjectDataSource();
            this.barcode1 = new Telerik.Reporting.Barcode();
            this.textBox31 = new Telerik.Reporting.TextBox();
            this.textBox19 = new Telerik.Reporting.TextBox();
            this.pageHeaderSection1 = new Telerik.Reporting.PageHeaderSection();
            this.tbxReportTitle = new Telerik.Reporting.TextBox();
            this.pageFooterSection1 = new Telerik.Reporting.PageFooterSection();
            this.textBox79 = new Telerik.Reporting.TextBox();
            this.textBox78 = new Telerik.Reporting.TextBox();
            this.textBox76 = new Telerik.Reporting.TextBox();
            this.textBox75 = new Telerik.Reporting.TextBox();
            this.textBox74 = new Telerik.Reporting.TextBox();
            this.textBox64 = new Telerik.Reporting.TextBox();
            this.textBox59 = new Telerik.Reporting.TextBox();
            this.textBox57 = new Telerik.Reporting.TextBox();
            this.textBox56 = new Telerik.Reporting.TextBox();
            this.textBox55 = new Telerik.Reporting.TextBox();
            this.textBox54 = new Telerik.Reporting.TextBox();
            this.textBox53 = new Telerik.Reporting.TextBox();
            this.textBox43 = new Telerik.Reporting.TextBox();
            this.textBox44 = new Telerik.Reporting.TextBox();
            this.textBox45 = new Telerik.Reporting.TextBox();
            this.textBox51 = new Telerik.Reporting.TextBox();
            this.textBox52 = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // textBox1
            // 
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.3018739223480225D), Telerik.Reporting.Drawing.Unit.Cm(0.40000000596046448D));
            this.textBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox1.Value = "供货单位：";
            // 
            // tbxSupplyCompany
            // 
            this.tbxSupplyCompany.CanGrow = false;
            this.tbxSupplyCompany.Name = "tbxSupplyCompany";
            this.tbxSupplyCompany.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.2812042236328125D), Telerik.Reporting.Drawing.Unit.Cm(0.40000000596046448D));
            this.tbxSupplyCompany.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.tbxSupplyCompany.StyleName = "";
            this.tbxSupplyCompany.Value = "= Fields.CounterpartCompanyName";
            // 
            // textBox7
            // 
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.7460020780563355D), Telerik.Reporting.Drawing.Unit.Cm(0.40000000596046448D));
            this.textBox7.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox7.StyleName = "";
            // 
            // textBox20
            // 
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.1378302574157715D), Telerik.Reporting.Drawing.Unit.Cm(0.40000000596046448D));
            this.textBox20.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox20.StyleName = "";
            // 
            // textBox25
            // 
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.2539992332458496D), Telerik.Reporting.Drawing.Unit.Cm(0.40000000596046448D));
            this.textBox25.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox25.StyleName = "";
            this.textBox25.Value = "检验单号：";
            // 
            // textBox3
            // 
            this.textBox3.CanGrow = false;
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.2808327674865723D), Telerik.Reporting.Drawing.Unit.Cm(0.40000000596046448D));
            this.textBox3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox3.Value = "= Fields.Code";
            // 
            // textBox16
            // 
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.948866069316864D), Telerik.Reporting.Drawing.Unit.Cm(0.44999995827674866D));
            this.textBox16.Value = "序号";
            // 
            // textBox11
            // 
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.66871976852417D), Telerik.Reporting.Drawing.Unit.Cm(0.44999995827674866D));
            this.textBox11.StyleName = "";
            this.textBox11.Value = "目标库位";
            // 
            // textBox28
            // 
            this.textBox28.CanShrink = false;
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.014122486114502D), Telerik.Reporting.Drawing.Unit.Cm(0.44999998807907104D));
            this.textBox28.Value = "配件图号";
            // 
            // textBox33
            // 
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.5408120155334473D), Telerik.Reporting.Drawing.Unit.Cm(0.44999995827674866D));
            this.textBox33.StyleName = "";
            this.textBox33.Value = "配件名称";
            // 
            // textBox41
            // 
            this.textBox41.Name = "textBox41";
            this.textBox41.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.88762921094894409D), Telerik.Reporting.Drawing.Unit.Cm(0.44999995827674866D));
            this.textBox41.StyleName = "";
            this.textBox41.Value = "单位";
            // 
            // textBox39
            // 
            this.textBox39.Name = "textBox39";
            this.textBox39.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.562848687171936D), Telerik.Reporting.Drawing.Unit.Cm(0.44999995827674866D));
            this.textBox39.StyleName = "";
            this.textBox39.Value = "计划数";
            // 
            // textBox37
            // 
            this.textBox37.Name = "textBox37";
            this.textBox37.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.3506826162338257D), Telerik.Reporting.Drawing.Unit.Cm(0.44999998807907104D));
            this.textBox37.StyleName = "";
            this.textBox37.Value = "实收数";
            // 
            // textBox35
            // 
            this.textBox35.Name = "textBox35";
            this.textBox35.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.0593652725219727D), Telerik.Reporting.Drawing.Unit.Cm(0.44999998807907104D));
            this.textBox35.StyleName = "";
            this.textBox35.Value = "计划价";
            // 
            // textBox29
            // 
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.88738435506820679D), Telerik.Reporting.Drawing.Unit.Cm(0.44999998807907104D));
            this.textBox29.Value = "备注";
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Cm(3.0564627647399902D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.table1,
            this.table2});
            this.detail.KeepTogether = false;
            this.detail.Name = "detail";
            this.detail.Style.Font.Name = "宋体";
            this.detail.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            // 
            // table1
            // 
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.3018753528594971D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(6.2811985015869141D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.7460019588470459D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.1378307342529297D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.2539997100830078D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(5.2808327674865723D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.3999999463558197D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.3999999463558197D)));
            this.table1.Body.SetCellContent(0, 0, this.textBox4);
            this.table1.Body.SetCellContent(0, 1, this.textBox5);
            this.table1.Body.SetCellContent(0, 3, this.textBox6);
            this.table1.Body.SetCellContent(0, 2, this.textBox8);
            this.table1.Body.SetCellContent(1, 0, this.textBox2);
            this.table1.Body.SetCellContent(1, 1, this.textBox9, 1, 3);
            this.table1.Body.SetCellContent(0, 4, this.textBox23);
            this.table1.Body.SetCellContent(1, 4, this.textBox24);
            this.table1.Body.SetCellContent(0, 5, this.textBox26);
            this.table1.Body.SetCellContent(1, 5, this.textBox27);
            tableGroup1.ReportItem = this.textBox1;
            tableGroup2.ReportItem = this.tbxSupplyCompany;
            tableGroup3.Name = "Group1";
            tableGroup3.ReportItem = this.textBox7;
            tableGroup4.ReportItem = this.textBox20;
            tableGroup5.Name = "group";
            tableGroup5.ReportItem = this.textBox25;
            tableGroup6.Name = "group1";
            tableGroup6.ReportItem = this.textBox3;
            this.table1.ColumnGroups.Add(tableGroup1);
            this.table1.ColumnGroups.Add(tableGroup2);
            this.table1.ColumnGroups.Add(tableGroup3);
            this.table1.ColumnGroups.Add(tableGroup4);
            this.table1.ColumnGroups.Add(tableGroup5);
            this.table1.ColumnGroups.Add(tableGroup6);
            this.table1.DataSource = this.PartsInboundCheckBillDataSource;
            this.table1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox4,
            this.textBox5,
            this.textBox8,
            this.textBox6,
            this.textBox23,
            this.textBox26,
            this.textBox2,
            this.textBox9,
            this.textBox24,
            this.textBox27,
            this.textBox1,
            this.tbxSupplyCompany,
            this.textBox7,
            this.textBox20,
            this.textBox25,
            this.textBox3});
            this.table1.KeepTogether = false;
            this.table1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(9.9921220680698752E-05D));
            this.table1.Name = "table1";
            tableGroup8.Name = "Group2";
            tableGroup9.Name = "Group3";
            tableGroup7.ChildGroups.Add(tableGroup8);
            tableGroup7.ChildGroups.Add(tableGroup9);
            tableGroup7.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup7.Name = "DetailGroup";
            this.table1.RowGroups.Add(tableGroup7);
            this.table1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(20.001739501953125D), Telerik.Reporting.Drawing.Unit.Cm(1.1999999284744263D));
            this.table1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.table1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            // 
            // textBox4
            // 
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.3018741607666016D), Telerik.Reporting.Drawing.Unit.Cm(0.39999997615814209D));
            this.textBox4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox4.Value = "入库仓库：";
            // 
            // textBox5
            // 
            this.textBox5.CanGrow = false;
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.2812042236328125D), Telerik.Reporting.Drawing.Unit.Cm(0.39999997615814209D));
            this.textBox5.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox5.Value = "= Fields.WarehouseName";
            // 
            // textBox6
            // 
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.1378302574157715D), Telerik.Reporting.Drawing.Unit.Cm(0.39999997615814209D));
            this.textBox6.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox6.Value = "= Sunlight.Silverlight.Dcs.Web.Reporting.CommonConverter.PartsInboundCheckBillInb" +
    "oundTypeFormat(Fields.InboundType)";
            // 
            // textBox8
            // 
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.7460020780563355D), Telerik.Reporting.Drawing.Unit.Cm(0.39999997615814209D));
            this.textBox8.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox8.StyleName = "";
            this.textBox8.Value = "入库类型：";
            // 
            // textBox2
            // 
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.3018741607666016D), Telerik.Reporting.Drawing.Unit.Cm(0.40000003576278687D));
            this.textBox2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox2.StyleName = "";
            this.textBox2.Value = "备    注：";
            // 
            // textBox9
            // 
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(10.165031433105469D), Telerik.Reporting.Drawing.Unit.Cm(0.39999997615814209D));
            this.textBox9.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox9.StyleName = "";
            this.textBox9.Value = "= Fields.Remark";
            // 
            // textBox23
            // 
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.2539992332458496D), Telerik.Reporting.Drawing.Unit.Cm(0.39999997615814209D));
            this.textBox23.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox23.StyleName = "";
            this.textBox23.Value = "采购订单类型:";
            // 
            // textBox24
            // 
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.2539992332458496D), Telerik.Reporting.Drawing.Unit.Cm(0.39999997615814209D));
            this.textBox24.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox24.StyleName = "";
            // 
            // textBox26
            // 
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.2808327674865723D), Telerik.Reporting.Drawing.Unit.Cm(0.39999997615814209D));
            this.textBox26.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox26.StyleName = "";
            this.textBox26.Value = "= Fields.PartsPurchaseOrderType";
            // 
            // textBox27
            // 
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.2808327674865723D), Telerik.Reporting.Drawing.Unit.Cm(0.39999997615814209D));
            this.textBox27.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox27.StyleName = "";
            // 
            // PartsInboundCheckBillDataSource
            // 
            this.PartsInboundCheckBillDataSource.DataMember = "GetPartsInboundCheckBillByIdForPrint";
            this.PartsInboundCheckBillDataSource.DataSource = typeof(Sunlight.Silverlight.Dcs.Web.DcsDomainService);
            this.PartsInboundCheckBillDataSource.Name = "PartsInboundCheckBillDataSource";
            this.PartsInboundCheckBillDataSource.Parameters.AddRange(new Telerik.Reporting.ObjectDataSourceParameter[] {
            new Telerik.Reporting.ObjectDataSourceParameter("PartsInboundCheckBillId", typeof(int), "=Parameters.paraPartsInboundCheckBillId.Value")});
            // 
            // table2
            // 
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.94886642694473267D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.6687207221984863D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(5.01412296295166D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(4.5408120155334473D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.88762915134429932D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.5628496408462524D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.3506827354431152D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.0593652725219727D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.887384295463562D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.45000001788139343D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.45000001788139343D)));
            this.table2.Body.SetCellContent(0, 0, this.textBox30);
            this.table2.Body.SetCellContent(0, 8, this.textBox32);
            this.table2.Body.SetCellContent(0, 3, this.textBox34);
            this.table2.Body.SetCellContent(0, 7, this.textBox36);
            this.table2.Body.SetCellContent(0, 6, this.textBox38);
            this.table2.Body.SetCellContent(0, 5, this.textBox40);
            this.table2.Body.SetCellContent(0, 4, this.textBox42);
            this.table2.Body.SetCellContent(0, 2, this.textBox10);
            this.table2.Body.SetCellContent(0, 1, this.textBox12);
            this.table2.Body.SetCellContent(1, 0, this.textBox13);
            this.table2.Body.SetCellContent(1, 1, this.textBox14);
            this.table2.Body.SetCellContent(1, 2, this.textBox15);
            this.table2.Body.SetCellContent(1, 3, this.textBox17);
            this.table2.Body.SetCellContent(1, 4, this.textBox18);
            this.table2.Body.SetCellContent(1, 5, this.textBox22);
            this.table2.Body.SetCellContent(1, 6, this.textBox21, 1, 3);
            tableGroup10.ReportItem = this.textBox16;
            tableGroup11.Name = "Group6";
            tableGroup11.ReportItem = this.textBox11;
            tableGroup12.ReportItem = this.textBox28;
            tableGroup13.Name = "Group1";
            tableGroup13.ReportItem = this.textBox33;
            tableGroup14.Name = "Group5";
            tableGroup14.ReportItem = this.textBox41;
            tableGroup15.Name = "Group4";
            tableGroup15.ReportItem = this.textBox39;
            tableGroup16.Name = "Group3";
            tableGroup16.ReportItem = this.textBox37;
            tableGroup17.Name = "Group2";
            tableGroup17.ReportItem = this.textBox35;
            tableGroup18.ReportItem = this.textBox29;
            this.table2.ColumnGroups.Add(tableGroup10);
            this.table2.ColumnGroups.Add(tableGroup11);
            this.table2.ColumnGroups.Add(tableGroup12);
            this.table2.ColumnGroups.Add(tableGroup13);
            this.table2.ColumnGroups.Add(tableGroup14);
            this.table2.ColumnGroups.Add(tableGroup15);
            this.table2.ColumnGroups.Add(tableGroup16);
            this.table2.ColumnGroups.Add(tableGroup17);
            this.table2.ColumnGroups.Add(tableGroup18);
            this.table2.ColumnHeadersPrintOnEveryPage = true;
            this.table2.DataSource = this.PartsInboundCheckBillDetailDataSource;
            this.table2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox30,
            this.textBox12,
            this.textBox10,
            this.textBox34,
            this.textBox42,
            this.textBox40,
            this.textBox38,
            this.textBox36,
            this.textBox32,
            this.textBox13,
            this.textBox14,
            this.textBox15,
            this.textBox17,
            this.textBox18,
            this.textBox22,
            this.textBox21,
            this.textBox16,
            this.textBox11,
            this.textBox28,
            this.textBox33,
            this.textBox41,
            this.textBox39,
            this.textBox37,
            this.textBox35,
            this.textBox29});
            this.table2.KeepTogether = false;
            this.table2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.9921220680698752E-05D), Telerik.Reporting.Drawing.Unit.Cm(1.2002999782562256D));
            this.table2.Name = "table2";
            tableGroup19.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup19.Name = "DetailGroup";
            tableGroup20.Name = "Group7";
            this.table2.RowGroups.Add(tableGroup19);
            this.table2.RowGroups.Add(tableGroup20);
            this.table2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.920433044433594D), Telerik.Reporting.Drawing.Unit.Cm(1.3500000238418579D));
            // 
            // textBox30
            // 
            this.textBox30.CanShrink = true;
            this.textBox30.Name = "textBox30";
            this.textBox30.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.948866069316864D), Telerik.Reporting.Drawing.Unit.Cm(0.44999995827674866D));
            this.textBox30.Value = "= RowNumber()";
            // 
            // textBox32
            // 
            this.textBox32.CanShrink = true;
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.88738435506820679D), Telerik.Reporting.Drawing.Unit.Cm(0.45000001788139343D));
            this.textBox32.Value = "= Fields.Remark";
            // 
            // textBox34
            // 
            this.textBox34.CanShrink = true;
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.5408120155334473D), Telerik.Reporting.Drawing.Unit.Cm(0.45000004768371582D));
            this.textBox34.StyleName = "";
            this.textBox34.Value = "= Fields.SparePartName";
            // 
            // textBox36
            // 
            this.textBox36.CanShrink = true;
            this.textBox36.Name = "textBox36";
            this.textBox36.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.0593652725219727D), Telerik.Reporting.Drawing.Unit.Cm(0.45000001788139343D));
            this.textBox36.StyleName = "";
            this.textBox36.TextWrap = false;
            this.textBox36.Value = "= Fields.CostPrice";
            // 
            // textBox38
            // 
            this.textBox38.CanShrink = true;
            this.textBox38.Name = "textBox38";
            this.textBox38.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.3506826162338257D), Telerik.Reporting.Drawing.Unit.Cm(0.45000001788139343D));
            this.textBox38.StyleName = "";
            this.textBox38.Value = "= Fields.InspectedQuantity";
            // 
            // textBox40
            // 
            this.textBox40.CanShrink = true;
            this.textBox40.Name = "textBox40";
            this.textBox40.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.562848687171936D), Telerik.Reporting.Drawing.Unit.Cm(0.44999995827674866D));
            this.textBox40.StyleName = "";
            this.textBox40.Value = "= Fields.InspectedQuantity";
            // 
            // textBox42
            // 
            this.textBox42.CanShrink = true;
            this.textBox42.Name = "textBox42";
            this.textBox42.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.88762921094894409D), Telerik.Reporting.Drawing.Unit.Cm(0.44999995827674866D));
            this.textBox42.StyleName = "";
            this.textBox42.Value = "= Fields.Unit";
            // 
            // textBox10
            // 
            this.textBox10.CanGrow = true;
            this.textBox10.CanShrink = true;
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.014122486114502D), Telerik.Reporting.Drawing.Unit.Cm(0.45000001788139343D));
            this.textBox10.Value = "= Fields.SparePartCode";
            // 
            // textBox12
            // 
            this.textBox12.CanGrow = true;
            this.textBox12.CanShrink = true;
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.66871976852417D), Telerik.Reporting.Drawing.Unit.Cm(0.44999995827674866D));
            this.textBox12.StyleName = "";
            this.textBox12.Value = "= Fields.DestWarehouseAreaCode";
            // 
            // textBox13
            // 
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.948866069316864D), Telerik.Reporting.Drawing.Unit.Cm(0.44999998807907104D));
            this.textBox13.StyleName = "";
            // 
            // textBox14
            // 
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.6687195301055908D), Telerik.Reporting.Drawing.Unit.Cm(0.44999998807907104D));
            this.textBox14.StyleName = "";
            // 
            // textBox15
            // 
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.014122486114502D), Telerik.Reporting.Drawing.Unit.Cm(0.45000001788139343D));
            this.textBox15.StyleName = "";
            // 
            // textBox17
            // 
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.5408120155334473D), Telerik.Reporting.Drawing.Unit.Cm(0.44999995827674866D));
            this.textBox17.StyleName = "";
            // 
            // textBox18
            // 
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.88762921094894409D), Telerik.Reporting.Drawing.Unit.Cm(0.44999998807907104D));
            this.textBox18.StyleName = "";
            // 
            // textBox22
            // 
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.5628485679626465D), Telerik.Reporting.Drawing.Unit.Cm(0.44999998807907104D));
            this.textBox22.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox22.StyleName = "";
            this.textBox22.Value = "总金额：";
            // 
            // textBox21
            // 
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.2974324226379395D), Telerik.Reporting.Drawing.Unit.Cm(0.45000001788139343D));
            this.textBox21.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox21.StyleName = "";
            this.textBox21.Value = "= Sum(Fields.CostPrice * Fields.InspectedQuantity)";
            // 
            // PartsInboundCheckBillDetailDataSource
            // 
            this.PartsInboundCheckBillDetailDataSource.DataMember = "GetPartsInboundCheckBillDetailByIdForPrint";
            this.PartsInboundCheckBillDetailDataSource.DataSource = typeof(Sunlight.Silverlight.Dcs.Web.DcsDomainService);
            this.PartsInboundCheckBillDetailDataSource.Name = "PartsInboundCheckBillDetailDataSource";
            this.PartsInboundCheckBillDetailDataSource.Parameters.AddRange(new Telerik.Reporting.ObjectDataSourceParameter[] {
            new Telerik.Reporting.ObjectDataSourceParameter("PartsInboundCheckBillId", typeof(int), "=Parameters.paraPartsInboundCheckBillId.Value")});
            // 
            // barcode1
            // 
            this.barcode1.Checksum = false;
            this.barcode1.Encoder = code128Encoder1;
            this.barcode1.Name = "barcode1";
            this.barcode1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.2386250495910645D), Telerik.Reporting.Drawing.Unit.Cm(1.3000001907348633D));
            this.barcode1.Value = "= Fields.SparePartCode";
            // 
            // textBox31
            // 
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.2489578723907471D), Telerik.Reporting.Drawing.Unit.Cm(1.3000001907348633D));
            this.textBox31.Value = "= Fields.SparePartCode";
            // 
            // textBox19
            // 
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9999998807907105D), Telerik.Reporting.Drawing.Unit.Cm(0.52375012636184692D));
            this.textBox19.StyleName = "";
            this.textBox19.Value = "= Fields.EntityKey.EntityKeyValues";
            // 
            // pageHeaderSection1
            // 
            this.pageHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Cm(1.4028081893920898D);
            this.pageHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.tbxReportTitle});
            this.pageHeaderSection1.Name = "pageHeaderSection1";
            // 
            // tbxReportTitle
            // 
            this.tbxReportTitle.Anchoring = ((Telerik.Reporting.AnchoringStyles)((((Telerik.Reporting.AnchoringStyles.Top | Telerik.Reporting.AnchoringStyles.Bottom) 
            | Telerik.Reporting.AnchoringStyles.Left) 
            | Telerik.Reporting.AnchoringStyles.Right)));
            this.tbxReportTitle.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.3018743991851807D), Telerik.Reporting.Drawing.Unit.Cm(0.20000006258487701D));
            this.tbxReportTitle.Name = "tbxReportTitle";
            this.tbxReportTitle.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(15.000003814697266D), Telerik.Reporting.Drawing.Unit.Cm(0.800000011920929D));
            this.tbxReportTitle.Style.Font.Bold = true;
            this.tbxReportTitle.Style.Font.Italic = false;
            this.tbxReportTitle.Style.Font.Name = "宋体";
            this.tbxReportTitle.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.tbxReportTitle.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Solid;
            this.tbxReportTitle.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.tbxReportTitle.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.tbxReportTitle.Value = "= Fields.BranchName+\"配件入库检验单\"";
            // 
            // pageFooterSection1
            // 
            this.pageFooterSection1.Height = Telerik.Reporting.Drawing.Unit.Cm(3D);
            this.pageFooterSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox79,
            this.textBox78,
            this.textBox76,
            this.textBox75,
            this.textBox74,
            this.textBox64,
            this.textBox59,
            this.textBox57,
            this.textBox56,
            this.textBox55,
            this.textBox54,
            this.textBox53,
            this.textBox43,
            this.textBox44,
            this.textBox45,
            this.textBox51,
            this.textBox52});
            this.pageFooterSection1.Name = "pageFooterSection1";
            this.pageFooterSection1.Style.Font.Name = "宋体";
            this.pageFooterSection1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            // 
            // textBox79
            // 
            this.textBox79.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.025993891060352325D), Telerik.Reporting.Drawing.Unit.Cm(0.00019766234618145973D));
            this.textBox79.Name = "textBox79";
            this.textBox79.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.7740061283111572D), Telerik.Reporting.Drawing.Unit.Cm(0.49999919533729553D));
            this.textBox79.Style.Font.Bold = false;
            this.textBox79.Value = "制单单位：";
            // 
            // textBox78
            // 
            this.textBox78.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.025993891060352325D), Telerik.Reporting.Drawing.Unit.Cm(0.6563643217086792D));
            this.textBox78.Name = "textBox78";
            this.textBox78.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.7740058898925781D), Telerik.Reporting.Drawing.Unit.Cm(0.49999919533729553D));
            this.textBox78.Style.Font.Bold = false;
            this.textBox78.Value = "打印时间：";
            // 
            // textBox76
            // 
            this.textBox76.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(19.181949615478516D), Telerik.Reporting.Drawing.Unit.Cm(0.65636301040649414D));
            this.textBox76.Name = "textBox76";
            this.textBox76.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.53253436088562012D), Telerik.Reporting.Drawing.Unit.Cm(0.5000004768371582D));
            this.textBox76.Value = "页";
            // 
            // textBox75
            // 
            this.textBox75.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(18.589553833007812D), Telerik.Reporting.Drawing.Unit.Cm(0.65636301040649414D));
            this.textBox75.Name = "textBox75";
            this.textBox75.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.53253436088562012D), Telerik.Reporting.Drawing.Unit.Cm(0.5000004768371582D));
            this.textBox75.Value = "= PageCount";
            // 
            // textBox74
            // 
            this.textBox74.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(18.056818008422852D), Telerik.Reporting.Drawing.Unit.Cm(0.65636301040649414D));
            this.textBox74.Name = "textBox74";
            this.textBox74.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.53253436088562012D), Telerik.Reporting.Drawing.Unit.Cm(0.5000004768371582D));
            this.textBox74.Value = "共";
            // 
            // textBox64
            // 
            this.textBox64.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(17.524087905883789D), Telerik.Reporting.Drawing.Unit.Cm(0.65636301040649414D));
            this.textBox64.Name = "textBox64";
            this.textBox64.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.53253436088562012D), Telerik.Reporting.Drawing.Unit.Cm(0.5000004768371582D));
            this.textBox64.Value = "页";
            // 
            // textBox59
            // 
            this.textBox59.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(16.991352081298828D), Telerik.Reporting.Drawing.Unit.Cm(0.65636301040649414D));
            this.textBox59.Name = "textBox59";
            this.textBox59.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.53253436088562012D), Telerik.Reporting.Drawing.Unit.Cm(0.5000004768371582D));
            this.textBox59.Value = "= PageNumber";
            // 
            // textBox57
            // 
            this.textBox57.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(16.3903865814209D), Telerik.Reporting.Drawing.Unit.Cm(0.65636301040649414D));
            this.textBox57.Name = "textBox57";
            this.textBox57.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.53253436088562012D), Telerik.Reporting.Drawing.Unit.Cm(0.5000004768371582D));
            this.textBox57.Value = "第";
            // 
            // textBox56
            // 
            this.textBox56.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.80020010471344D), Telerik.Reporting.Drawing.Unit.Cm(0.00019766234618145973D));
            this.textBox56.Name = "textBox56";
            this.textBox56.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.4995999336242676D), Telerik.Reporting.Drawing.Unit.Cm(0.49999919533729553D));
            this.textBox56.Value = "= Fields.StorageCompanyName";
            // 
            // textBox55
            // 
            this.textBox55.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(7.7767066955566406D), Telerik.Reporting.Drawing.Unit.Cm(0.00019766234618145973D));
            this.textBox55.Name = "textBox55";
            this.textBox55.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.366384744644165D), Telerik.Reporting.Drawing.Unit.Cm(0.5000004768371582D));
            this.textBox55.Value = "=Fields.CreatorName";
            // 
            // textBox54
            // 
            this.textBox54.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.80020010471344D), Telerik.Reporting.Drawing.Unit.Cm(0.6563643217086792D));
            this.textBox54.Name = "textBox54";
            this.textBox54.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.4995999336242676D), Telerik.Reporting.Drawing.Unit.Cm(0.49999919533729553D));
            this.textBox54.Value = "= Now()";
            // 
            // textBox53
            // 
            this.textBox53.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(6.3000001907348633D), Telerik.Reporting.Drawing.Unit.Cm(0.00019766234618145973D));
            this.textBox53.Name = "textBox53";
            this.textBox53.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.4765061140060425D), Telerik.Reporting.Drawing.Unit.Cm(0.5000004768371582D));
            this.textBox53.Style.Font.Bold = false;
            this.textBox53.Value = "制单人：";
            // 
            // textBox43
            // 
            this.textBox43.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.228326797485352D), Telerik.Reporting.Drawing.Unit.Cm(0.00019766234618145973D));
            this.textBox43.Name = "textBox43";
            this.textBox43.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.7716735601425171D), Telerik.Reporting.Drawing.Unit.Cm(0.49999919533729553D));
            this.textBox43.Style.Font.Bold = false;
            this.textBox43.Value = "制单时间：";
            // 
            // textBox44
            // 
            this.textBox44.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.000198364257813D), Telerik.Reporting.Drawing.Unit.Cm(0.00019766234618145973D));
            this.textBox44.Name = "textBox44";
            this.textBox44.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.2070531845092773D), Telerik.Reporting.Drawing.Unit.Cm(0.5000004768371582D));
            this.textBox44.Value = "=Fields.CreateTime";
            // 
            // textBox45
            // 
            this.textBox45.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(16.207450866699219D), Telerik.Reporting.Drawing.Unit.Cm(0.00019766234618145973D));
            this.textBox45.Name = "textBox45";
            this.textBox45.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.7613679170608521D), Telerik.Reporting.Drawing.Unit.Cm(0.5000004768371582D));
            this.textBox45.Value = "打印人：";
            // 
            // textBox51
            // 
            this.textBox51.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(18.10572624206543D), Telerik.Reporting.Drawing.Unit.Cm(0.00019766234618145973D));
            this.textBox51.Name = "textBox51";
            this.textBox51.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.7941733598709106D), Telerik.Reporting.Drawing.Unit.Cm(0.5000004768371582D));
            this.textBox51.Value = "= Parameters.UserName.Value";
            // 
            // textBox52
            // 
            this.textBox52.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(7D), Telerik.Reporting.Drawing.Unit.Cm(0.6563643217086792D));
            this.textBox52.Name = "textBox52";
            this.textBox52.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.77650618553161621D), Telerik.Reporting.Drawing.Unit.Cm(0.5000004768371582D));
            this.textBox52.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox52.Value = "注：";
            // 
            // ReportPartsInboundCheckBillForGCNew
            // 
            this.DataSource = this.PartsInboundCheckBillDataSource;
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.detail,
            this.pageHeaderSection1,
            this.pageFooterSection1});
            this.Name = "Test";
            this.PageSettings.Landscape = true;
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Pixel(25D), Telerik.Reporting.Drawing.Unit.Pixel(19D), Telerik.Reporting.Drawing.Unit.Mm(9D), Telerik.Reporting.Drawing.Unit.Mm(9D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Custom;
            this.PageSettings.PaperSize = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(14D), Telerik.Reporting.Drawing.Unit.Cm(22D));
            reportParameter1.Name = "paraPartsInboundCheckBillId";
            reportParameter1.Type = Telerik.Reporting.ReportParameterType.Integer;
            reportParameter2.Name = "UserName";
            reportParameter2.Text = "UserName";
            this.ReportParameters.Add(reportParameter1);
            this.ReportParameters.Add(reportParameter2);
            this.Style.BackgroundColor = System.Drawing.Color.White;
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
            this.Width = Telerik.Reporting.Drawing.Unit.Cm(20D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.Table table1;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox9;
        private Telerik.Reporting.TextBox tbxSupplyCompany;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.TextBox textBox19;
        private Telerik.Reporting.PageHeaderSection pageHeaderSection1;
        private Telerik.Reporting.TextBox tbxReportTitle;
        private Telerik.Reporting.ObjectDataSource PartsInboundCheckBillDataSource;
        private Telerik.Reporting.ObjectDataSource PartsInboundCheckBillDetailDataSource;
        private Telerik.Reporting.Table table2;
        private Telerik.Reporting.TextBox textBox30;
        private Telerik.Reporting.TextBox textBox31;
        private Telerik.Reporting.TextBox textBox32;
        private Telerik.Reporting.TextBox textBox34;
        private Telerik.Reporting.TextBox textBox36;
        private Telerik.Reporting.TextBox textBox38;
        private Telerik.Reporting.TextBox textBox40;
        private Telerik.Reporting.TextBox textBox42;
        private Telerik.Reporting.TextBox textBox16;
        private Telerik.Reporting.TextBox textBox28;
        private Telerik.Reporting.TextBox textBox33;
        private Telerik.Reporting.TextBox textBox41;
        private Telerik.Reporting.TextBox textBox39;
        private Telerik.Reporting.TextBox textBox37;
        private Telerik.Reporting.TextBox textBox35;
        private Telerik.Reporting.TextBox textBox29;
        private Telerik.Reporting.Barcode barcode1;
        private Telerik.Reporting.PageFooterSection pageFooterSection1;
        private Telerik.Reporting.TextBox textBox79;
        private Telerik.Reporting.TextBox textBox78;
        private Telerik.Reporting.TextBox textBox76;
        private Telerik.Reporting.TextBox textBox75;
        private Telerik.Reporting.TextBox textBox74;
        private Telerik.Reporting.TextBox textBox64;
        private Telerik.Reporting.TextBox textBox59;
        private Telerik.Reporting.TextBox textBox57;
        private Telerik.Reporting.TextBox textBox56;
        private Telerik.Reporting.TextBox textBox55;
        private Telerik.Reporting.TextBox textBox54;
        private Telerik.Reporting.TextBox textBox53;
        private Telerik.Reporting.TextBox textBox43;
        private Telerik.Reporting.TextBox textBox44;
        private Telerik.Reporting.TextBox textBox45;
        private Telerik.Reporting.TextBox textBox51;
        private Telerik.Reporting.TextBox textBox52;
        private Telerik.Reporting.TextBox textBox10;
        private Telerik.Reporting.TextBox textBox12;
        private Telerik.Reporting.TextBox textBox11;
        private Telerik.Reporting.TextBox textBox13;
        private Telerik.Reporting.TextBox textBox14;
        private Telerik.Reporting.TextBox textBox15;
        private Telerik.Reporting.TextBox textBox17;
        private Telerik.Reporting.TextBox textBox18;
        private Telerik.Reporting.TextBox textBox22;
        private Telerik.Reporting.TextBox textBox21;
        private Telerik.Reporting.TextBox textBox23;
        private Telerik.Reporting.TextBox textBox24;
        private Telerik.Reporting.TextBox textBox26;
        private Telerik.Reporting.TextBox textBox27;
        private Telerik.Reporting.TextBox textBox20;
        private Telerik.Reporting.TextBox textBox25;
        private Telerik.Reporting.TextBox textBox3;
    }
}