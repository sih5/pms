namespace Sunlight.Silverlight.Dcs.Web.Reporting
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    /// <summary>
    /// Summary description for ReportPartsInboundCheckLabelForEn.
    /// </summary>
    public partial class ReportPartsInboundCheckLabelForEn : Telerik.Reporting.Report
    {
        public ReportPartsInboundCheckLabelForEn()
        {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }
        private void ReportPartsInboundCheckLabelForEn_NeedDataSource(object sender, EventArgs e)
        {
             var report = (Telerik.Reporting.Processing.Report)sender;
            // 取参数值
            var pararPartsTags = report.Parameters["PararPartsTags"].Value.ToString().Replace("%2c", ",");
          
          //  var id = Convert.ToInt32(report.Parameters["PartsInboundCheckBillId"].Value);
            var ids = new List<int>();
            var idsStr = pararPartsTags.Split(',');
            for (int i = 0; i < idsStr.Count() - 1; i++)
            {
                ids.Add(Convert.ToInt32(idsStr[i]));
            } 
            var billId = report.Parameters["PartsInboundCheckBillId"].Value.ToString().Replace("%2c", ",");
            var billIds = new List<int>();
            var billIdStr = billId.Split(',');
            for (int i = 0; i < billIdStr.Count() - 1; i++)
            {
                billIds.Add(Convert.ToInt32(billIdStr[i]));
            } 
            var domainService = new DcsDomainService();
            // 获取客户端传入的配件入库计划清单的Id集合
            // 调用服务端方法，查询清单数据
            var partsInboundPlanDetails = domainService.GetPartsInboundCheckBillForPrintCh(ids.ToArray(), billIds.ToArray()).OrderBy(r => r.SparePartCode).ToArray();
            var details = new List<VirtualPartsInboundCheckBillForPrint>();
            //修改清单记录的数量属性，为客户端传入的值
            foreach (var item in partsInboundPlanDetails)
            {
                int firPrintNumber = item.FirPrintNumber.Value;
                for (int i = 0; i < firPrintNumber; i++)
                {
                    var planDetail = new VirtualPartsInboundCheckBillForPrint();
                    planDetail.SparePartCode = item.SparePartCode;
                    planDetail.EnglishName = item.EnglishName == null ? "" : item.EnglishName;
                    planDetail.PackingCoefficient = item.FirPackingCoefficient.Value;
                    planDetail.BachMunber = item.BachMunber == null ? "" : item.BachMunber;
                    planDetail.SparePartName = item.SparePartName;
                    if(planDetail.EnglishName.Length > 30) {
                        planDetail.EnglishName = planDetail.EnglishName.Substring(0, 30);
                        planDetail.EnglishName = planDetail.EnglishName + "...";
                    }
                    string[] sArray = planDetail.BachMunber.Split('_');
                    if(sArray.Count() > 1) {
                        planDetail.BachMunber = sArray[1];
                    } else {
                        if(planDetail.BachMunber.Length > 16) {
                            planDetail.BachMunber = planDetail.BachMunber.Substring(planDetail.BachMunber.Length - 16, 16);
                        }
                    } 
                    if(planDetail.SparePartName.Length > 20) {
                        planDetail.SparePartName = planDetail.SparePartName.Substring(0, 20);
                        planDetail.SparePartName = planDetail.SparePartName + "...";
                    }
                    if(item.TraceProperty.HasValue && item.TraceProperty == (int)DCSTraceProperty.精确追溯) {
                        planDetail.BachMunber = planDetail.BachMunber + "|" + (i + 1) + "|JQ";
                        planDetail.SparePartCodeX = planDetail.SparePartCode + " JQ";
                        planDetail.BachMunber = planDetail.BachMunber + "|" + (i + 1);
                    } else {
                        planDetail.BachMunber = planDetail.BachMunber ;
                        planDetail.SparePartCodeX = planDetail.SparePartCode;
                    }
                    details.Add(planDetail);
                }
                int secPrintNumber = item.SecPrintNumber.Value;
                for (int i = 0; i < secPrintNumber; i++)
                {
                    var planDetail = new VirtualPartsInboundCheckBillForPrint();
                    planDetail.SparePartCode = item.SparePartCode;
                    planDetail.EnglishName = item.EnglishName == null ? "" : item.EnglishName;
                    planDetail.PackingCoefficient = item.SecPackingCoefficient.Value;
                    planDetail.BachMunber = item.BachMunber == null ? "" : item.BachMunber;
                    planDetail.SparePartName = item.SparePartName;
                    if(planDetail.EnglishName.Length > 30) {
                        planDetail.EnglishName = planDetail.EnglishName.Substring(0, 30);
                        planDetail.EnglishName = planDetail.EnglishName + "...";
                    }
                    string[] sArray = planDetail.BachMunber.Split('_');
                    if(sArray.Count() > 1) {
                        planDetail.BachMunber = sArray[1];
                    } else {
                        if(planDetail.BachMunber.Length > 16) {
                            planDetail.BachMunber = planDetail.BachMunber.Substring(planDetail.BachMunber.Length - 16, 16);
                        }
                    } 
                    if(planDetail.SparePartName.Length > 20) {
                        planDetail.SparePartName = planDetail.SparePartName.Substring(0, 20);
                        planDetail.SparePartName = planDetail.SparePartName + "...";
                    }
                    if(item.TraceProperty.HasValue && item.TraceProperty == (int)DCSTraceProperty.精确追溯) {
                        planDetail.BachMunber = planDetail.BachMunber + "|" + (i + 1) + "|JQ";
                        planDetail.SparePartCodeX = planDetail.SparePartCode + " JQ";
                        planDetail.BachMunber = planDetail.BachMunber + "|" + (i + 1);
                    } else {
                        planDetail.BachMunber = planDetail.BachMunber ;
                        planDetail.SparePartCodeX = planDetail.SparePartCode;
                    }
                    details.Add(planDetail);
                } int ThidPrintNumber = item.ThidPrintNumber.Value;
                for (int i = 0; i < ThidPrintNumber; i++)
                {
                    var planDetail = new VirtualPartsInboundCheckBillForPrint();
                    planDetail.SparePartCode = item.SparePartCode;
                    planDetail.EnglishName = item.EnglishName == null ? "" : item.EnglishName;
                    planDetail.PackingCoefficient = item.ThidPackingCoefficient.Value;
                    planDetail.BachMunber = item.BachMunber == null ? "" : item.BachMunber;
                    planDetail.SparePartName = item.SparePartName;
                    if(planDetail.EnglishName.Length > 30) {
                        planDetail.EnglishName = planDetail.EnglishName.Substring(0, 30);
                        planDetail.EnglishName = planDetail.EnglishName + "...";
                    }
                    if(planDetail.SparePartName.Length > 20) {
                        planDetail.SparePartName = planDetail.SparePartName.Substring(0, 20);
                        planDetail.SparePartName = planDetail.SparePartName + "...";
                    }
                    string[] sArray = planDetail.BachMunber.Split('_');
                    if(sArray.Count() > 1) {
                        planDetail.BachMunber = sArray[1];
                    } else {
                        if(planDetail.BachMunber.Length > 16) {
                            planDetail.BachMunber = planDetail.BachMunber.Substring(planDetail.BachMunber.Length - 16, 16);
                        }
                    } 
                    if(item.TraceProperty.HasValue && item.TraceProperty == (int)DCSTraceProperty.精确追溯) {
                        planDetail.BachMunber = planDetail.BachMunber + "|" + (i + 1) + "|JQ";
                        planDetail.SparePartCodeX = planDetail.SparePartCode + " JQ";
                        planDetail.BachMunber = planDetail.BachMunber + "|" + (i + 1);
                    } else {
                        planDetail.BachMunber = planDetail.BachMunber ;
                        planDetail.SparePartCodeX = planDetail.SparePartCode;
                    }
                    details.Add(planDetail);
                }
            }
            //调用生成WMS标签序列号方法
            //domainService.InsertWMSLabelPrintingSerialList2(details);
            // 将构造的结果集绑定到 DetailSection
            report.DataSource = details;
        }

    }
}