namespace Sunlight.Silverlight.Dcs.Web.Reporting {
    partial class ReportPartsInboundPlan {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup11 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup12 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup13 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup14 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup15 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup16 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup17 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup18 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup19 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup20 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup21 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup22 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup23 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup24 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup25 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup26 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup27 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup28 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup29 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup30 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup31 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup32 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup33 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup34 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup35 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup36 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup37 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup38 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup39 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup40 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup41 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup42 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup43 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup44 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup45 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup46 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup47 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup48 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup49 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup50 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup51 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.ReportParameter reportParameter1 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter2 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.textBox52 = new Telerik.Reporting.TextBox();
            this.textBox9 = new Telerik.Reporting.TextBox();
            this.textBox53 = new Telerik.Reporting.TextBox();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.textBox54 = new Telerik.Reporting.TextBox();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.textBox55 = new Telerik.Reporting.TextBox();
            this.textBox11 = new Telerik.Reporting.TextBox();
            this.textBox56 = new Telerik.Reporting.TextBox();
            this.textBox51 = new Telerik.Reporting.TextBox();
            this.textBox57 = new Telerik.Reporting.TextBox();
            this.textBox12 = new Telerik.Reporting.TextBox();
            this.textBox58 = new Telerik.Reporting.TextBox();
            this.textBox24 = new Telerik.Reporting.TextBox();
            this.textBox18 = new Telerik.Reporting.TextBox();
            this.textBox13 = new Telerik.Reporting.TextBox();
            this.textBox60 = new Telerik.Reporting.TextBox();
            this.textBox20 = new Telerik.Reporting.TextBox();
            this.textBox61 = new Telerik.Reporting.TextBox();
            this.textBox22 = new Telerik.Reporting.TextBox();
            this.textBox62 = new Telerik.Reporting.TextBox();
            this.textBox14 = new Telerik.Reporting.TextBox();
            this.textBox63 = new Telerik.Reporting.TextBox();
            this.textBox26 = new Telerik.Reporting.TextBox();
            this.textBox65 = new Telerik.Reporting.TextBox();
            this.textBox28 = new Telerik.Reporting.TextBox();
            this.textBox66 = new Telerik.Reporting.TextBox();
            this.textBox36 = new Telerik.Reporting.TextBox();
            this.textBox67 = new Telerik.Reporting.TextBox();
            this.textBox34 = new Telerik.Reporting.TextBox();
            this.textBox68 = new Telerik.Reporting.TextBox();
            this.textBox29 = new Telerik.Reporting.TextBox();
            this.textBox69 = new Telerik.Reporting.TextBox();
            this.textBox30 = new Telerik.Reporting.TextBox();
            this.textBox70 = new Telerik.Reporting.TextBox();
            this.textBox38 = new Telerik.Reporting.TextBox();
            this.textBox71 = new Telerik.Reporting.TextBox();
            this.textBox40 = new Telerik.Reporting.TextBox();
            this.textBox72 = new Telerik.Reporting.TextBox();
            this.textBox47 = new Telerik.Reporting.TextBox();
            this.textBox73 = new Telerik.Reporting.TextBox();
            this.textBox49 = new Telerik.Reporting.TextBox();
            this.textBox77 = new Telerik.Reporting.TextBox();
            this.pageHeaderSection1 = new Telerik.Reporting.PageHeaderSection();
            this.tbxReportTitle = new Telerik.Reporting.TextBox();
            this.detail = new Telerik.Reporting.DetailSection();
            this.table1 = new Telerik.Reporting.Table();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.textBox10 = new Telerik.Reporting.TextBox();
            this.PartsInboundPlanDataSource = new Telerik.Reporting.ObjectDataSource();
            this.table2 = new Telerik.Reporting.Table();
            this.textBox15 = new Telerik.Reporting.TextBox();
            this.textBox16 = new Telerik.Reporting.TextBox();
            this.textBox17 = new Telerik.Reporting.TextBox();
            this.textBox21 = new Telerik.Reporting.TextBox();
            this.textBox23 = new Telerik.Reporting.TextBox();
            this.textBox27 = new Telerik.Reporting.TextBox();
            this.textBox25 = new Telerik.Reporting.TextBox();
            this.PartsInboundPlanDetailDataSource = new Telerik.Reporting.ObjectDataSource();
            this.table3 = new Telerik.Reporting.Table();
            this.textBox31 = new Telerik.Reporting.TextBox();
            this.textBox32 = new Telerik.Reporting.TextBox();
            this.textBox33 = new Telerik.Reporting.TextBox();
            this.textBox35 = new Telerik.Reporting.TextBox();
            this.textBox37 = new Telerik.Reporting.TextBox();
            this.textBox39 = new Telerik.Reporting.TextBox();
            this.textBox43 = new Telerik.Reporting.TextBox();
            this.textBox45 = new Telerik.Reporting.TextBox();
            this.textBox46 = new Telerik.Reporting.TextBox();
            this.textBox48 = new Telerik.Reporting.TextBox();
            this.textBox44 = new Telerik.Reporting.TextBox();
            this.textBox50 = new Telerik.Reporting.TextBox();
            this.pageFooterSection1 = new Telerik.Reporting.PageFooterSection();
            this.barcode2 = new Telerik.Reporting.Barcode();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // textBox1
            // 
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.2115800380706787D), Telerik.Reporting.Drawing.Unit.Cm(0.550333559513092D));
            this.textBox1.Style.Font.Bold = true;
            this.textBox1.Value = "供货单位：";
            // 
            // textBox52
            // 
            this.textBox52.Name = "textBox52";
            this.textBox52.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.2115800380706787D), Telerik.Reporting.Drawing.Unit.Cm(0.30149993300437927D));
            this.textBox52.Style.Font.Bold = true;
            this.textBox52.StyleName = "";
            // 
            // textBox9
            // 
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9520050287246704D), Telerik.Reporting.Drawing.Unit.Cm(0.550333559513092D));
            this.textBox9.StyleName = "";
            this.textBox9.Value = "= Fields.CounterpartCompanyName";
            // 
            // textBox53
            // 
            this.textBox53.Name = "textBox53";
            this.textBox53.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9520050287246704D), Telerik.Reporting.Drawing.Unit.Cm(0.30149993300437927D));
            this.textBox53.StyleName = "";
            // 
            // textBox2
            // 
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.2273309230804443D), Telerik.Reporting.Drawing.Unit.Cm(0.550333559513092D));
            this.textBox2.Style.Font.Bold = true;
            this.textBox2.Value = "入库类型：";
            // 
            // textBox54
            // 
            this.textBox54.Name = "textBox54";
            this.textBox54.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.2273309230804443D), Telerik.Reporting.Drawing.Unit.Cm(0.30149993300437927D));
            this.textBox54.Style.Font.Bold = true;
            this.textBox54.StyleName = "";
            // 
            // textBox3
            // 
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.1751291751861572D), Telerik.Reporting.Drawing.Unit.Cm(0.550333559513092D));
            this.textBox3.Value = "= Sunlight.Silverlight.Dcs.Web.Reporting.CommonConverter.PartsInboundCheckBillInb" +
    "oundTypeFormat(Fields.InboundType)";
            // 
            // textBox55
            // 
            this.textBox55.Name = "textBox55";
            this.textBox55.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.1751291751861572D), Telerik.Reporting.Drawing.Unit.Cm(0.30149993300437927D));
            this.textBox55.StyleName = "";
            // 
            // textBox11
            // 
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.0225827693939209D), Telerik.Reporting.Drawing.Unit.Cm(0.550333559513092D));
            this.textBox11.Style.Font.Bold = true;
            this.textBox11.StyleName = "";
            this.textBox11.Value = "入库计划单号：";
            // 
            // textBox56
            // 
            this.textBox56.Name = "textBox56";
            this.textBox56.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.0225827693939209D), Telerik.Reporting.Drawing.Unit.Cm(0.30149996280670166D));
            this.textBox56.Style.Font.Bold = true;
            this.textBox56.StyleName = "";
            // 
            // textBox51
            // 
            this.textBox51.Name = "textBox51";
            this.textBox51.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.6113724708557129D), Telerik.Reporting.Drawing.Unit.Cm(0.550333559513092D));
            this.textBox51.StyleName = "";
            this.textBox51.Value = "= Fields.Code";
            // 
            // textBox57
            // 
            this.textBox57.Name = "textBox57";
            this.textBox57.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.6113724708557129D), Telerik.Reporting.Drawing.Unit.Cm(0.30149993300437927D));
            this.textBox57.StyleName = "";
            // 
            // textBox12
            // 
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.2018916606903076D), Telerik.Reporting.Drawing.Unit.Cm(0.52916675806045532D));
            this.textBox12.Style.Font.Bold = true;
            this.textBox12.Value = "序号";
            // 
            // textBox58
            // 
            this.textBox58.Name = "textBox58";
            this.textBox58.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.2018916606903076D), Telerik.Reporting.Drawing.Unit.Cm(0.32266658544540405D));
            this.textBox58.Style.Font.Bold = true;
            this.textBox58.StyleName = "";
            // 
            // textBox24
            // 
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.4260098934173584D), Telerik.Reporting.Drawing.Unit.Cm(0.52916675806045532D));
            this.textBox24.Style.Font.Bold = true;
            this.textBox24.StyleName = "";
            this.textBox24.Value = "配件图号";
            // 
            // textBox18
            // 
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.4260098934173584D), Telerik.Reporting.Drawing.Unit.Cm(0.32266658544540405D));
            this.textBox18.Style.Font.Bold = true;
            this.textBox18.StyleName = "";
            // 
            // textBox13
            // 
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.9638271331787109D), Telerik.Reporting.Drawing.Unit.Cm(0.52916675806045532D));
            this.textBox13.Style.Font.Bold = true;
            this.textBox13.Value = "配件名称";
            // 
            // textBox60
            // 
            this.textBox60.Name = "textBox60";
            this.textBox60.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.9638271331787109D), Telerik.Reporting.Drawing.Unit.Cm(0.32266658544540405D));
            this.textBox60.Style.Font.Bold = true;
            this.textBox60.StyleName = "";
            // 
            // textBox20
            // 
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.1115753650665283D), Telerik.Reporting.Drawing.Unit.Cm(0.52916675806045532D));
            this.textBox20.Style.Font.Bold = true;
            this.textBox20.StyleName = "";
            this.textBox20.Value = "数量";
            // 
            // textBox61
            // 
            this.textBox61.Name = "textBox61";
            this.textBox61.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.1115753650665283D), Telerik.Reporting.Drawing.Unit.Cm(0.32266658544540405D));
            this.textBox61.Style.Font.Bold = true;
            this.textBox61.StyleName = "";
            // 
            // textBox22
            // 
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.5615921020507813D), Telerik.Reporting.Drawing.Unit.Cm(0.52916675806045532D));
            this.textBox22.Style.Font.Bold = true;
            this.textBox22.StyleName = "";
            this.textBox22.Value = "单位";
            // 
            // textBox62
            // 
            this.textBox62.Name = "textBox62";
            this.textBox62.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.5615921020507813D), Telerik.Reporting.Drawing.Unit.Cm(0.32266658544540405D));
            this.textBox62.Style.Font.Bold = true;
            this.textBox62.StyleName = "";
            // 
            // textBox14
            // 
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.1639039516448975D), Telerik.Reporting.Drawing.Unit.Cm(0.52916675806045532D));
            this.textBox14.Style.Font.Bold = true;
            this.textBox14.Value = "单价";
            // 
            // textBox63
            // 
            this.textBox63.Name = "textBox63";
            this.textBox63.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.1639039516448975D), Telerik.Reporting.Drawing.Unit.Cm(0.32266658544540405D));
            this.textBox63.Style.Font.Bold = true;
            this.textBox63.StyleName = "";
            // 
            // textBox26
            // 
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.7712006568908691D), Telerik.Reporting.Drawing.Unit.Cm(0.52916675806045532D));
            this.textBox26.Style.Font.Bold = true;
            this.textBox26.StyleName = "";
            this.textBox26.Value = "备注";
            // 
            // textBox65
            // 
            this.textBox65.Name = "textBox65";
            this.textBox65.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.7712006568908691D), Telerik.Reporting.Drawing.Unit.Cm(0.32266658544540405D));
            this.textBox65.Style.Font.Bold = true;
            this.textBox65.StyleName = "";
            // 
            // textBox28
            // 
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.0890882015228271D), Telerik.Reporting.Drawing.Unit.Cm(0.94191652536392212D));
            this.textBox28.Style.Font.Bold = true;
            this.textBox28.Value = "制单单位：";
            // 
            // textBox66
            // 
            this.textBox66.Name = "textBox66";
            this.textBox66.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.0890882015228271D), Telerik.Reporting.Drawing.Unit.Cm(0.513166606426239D));
            this.textBox66.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox66.Style.Font.Bold = true;
            this.textBox66.StyleName = "";
            // 
            // textBox36
            // 
            this.textBox36.Name = "textBox36";
            this.textBox36.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.4824748039245605D), Telerik.Reporting.Drawing.Unit.Cm(0.94191652536392212D));
            this.textBox36.StyleName = "";
            this.textBox36.Value = "= Fields.StorageCompanyName";
            // 
            // textBox67
            // 
            this.textBox67.Name = "textBox67";
            this.textBox67.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.4824748039245605D), Telerik.Reporting.Drawing.Unit.Cm(0.513166606426239D));
            this.textBox67.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox67.StyleName = "";
            // 
            // textBox34
            // 
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.8119179010391235D), Telerik.Reporting.Drawing.Unit.Cm(0.94191652536392212D));
            this.textBox34.Style.Font.Bold = true;
            this.textBox34.StyleName = "";
            this.textBox34.Value = "制单人：";
            // 
            // textBox68
            // 
            this.textBox68.Name = "textBox68";
            this.textBox68.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.8119179010391235D), Telerik.Reporting.Drawing.Unit.Cm(0.513166606426239D));
            this.textBox68.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox68.Style.Font.Bold = true;
            this.textBox68.StyleName = "";
            // 
            // textBox29
            // 
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.2018530368804932D), Telerik.Reporting.Drawing.Unit.Cm(0.94191652536392212D));
            this.textBox29.Value = "= Fields.CreatorName";
            // 
            // textBox69
            // 
            this.textBox69.Name = "textBox69";
            this.textBox69.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.2018530368804932D), Telerik.Reporting.Drawing.Unit.Cm(0.513166606426239D));
            this.textBox69.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox69.StyleName = "";
            // 
            // textBox30
            // 
            this.textBox30.Name = "textBox30";
            this.textBox30.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9867312908172607D), Telerik.Reporting.Drawing.Unit.Cm(0.94191652536392212D));
            this.textBox30.Style.Font.Bold = true;
            this.textBox30.Value = "制单时间：";
            // 
            // textBox70
            // 
            this.textBox70.Name = "textBox70";
            this.textBox70.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9867312908172607D), Telerik.Reporting.Drawing.Unit.Cm(0.513166606426239D));
            this.textBox70.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox70.Style.Font.Bold = true;
            this.textBox70.StyleName = "";
            // 
            // textBox38
            // 
            this.textBox38.Name = "textBox38";
            this.textBox38.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.270003080368042D), Telerik.Reporting.Drawing.Unit.Cm(0.94191652536392212D));
            this.textBox38.StyleName = "";
            this.textBox38.Value = "= Fields.CreateTime";
            // 
            // textBox71
            // 
            this.textBox71.Name = "textBox71";
            this.textBox71.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.270003080368042D), Telerik.Reporting.Drawing.Unit.Cm(0.513166606426239D));
            this.textBox71.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox71.StyleName = "";
            // 
            // textBox40
            // 
            this.textBox40.Name = "textBox40";
            this.textBox40.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.5983067750930786D), Telerik.Reporting.Drawing.Unit.Cm(0.94191652536392212D));
            this.textBox40.Style.Font.Bold = true;
            this.textBox40.StyleName = "";
            this.textBox40.Value = "打印人：";
            // 
            // textBox72
            // 
            this.textBox72.Name = "textBox72";
            this.textBox72.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.5983067750930786D), Telerik.Reporting.Drawing.Unit.Cm(0.513166606426239D));
            this.textBox72.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox72.Style.Font.Bold = true;
            this.textBox72.StyleName = "";
            // 
            // textBox47
            // 
            this.textBox47.Name = "textBox47";
            this.textBox47.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.730002760887146D), Telerik.Reporting.Drawing.Unit.Cm(0.94191652536392212D));
            this.textBox47.StyleName = "";
            this.textBox47.Value = "= Parameters.UserName.Value";
            // 
            // textBox73
            // 
            this.textBox73.Name = "textBox73";
            this.textBox73.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.730002760887146D), Telerik.Reporting.Drawing.Unit.Cm(0.513166606426239D));
            this.textBox73.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox73.StyleName = "";
            // 
            // textBox49
            // 
            this.textBox49.Name = "textBox49";
            this.textBox49.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.0296225547790527D), Telerik.Reporting.Drawing.Unit.Cm(0.94191652536392212D));
            this.textBox49.StyleName = "";
            // 
            // textBox77
            // 
            this.textBox77.Name = "textBox77";
            this.textBox77.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.0296225547790527D), Telerik.Reporting.Drawing.Unit.Cm(0.513166606426239D));
            this.textBox77.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox77.StyleName = "";
            // 
            // pageHeaderSection1
            // 
            this.pageHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Cm(1.3999999761581421D);
            this.pageHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.tbxReportTitle});
            this.pageHeaderSection1.Name = "pageHeaderSection1";
            this.pageHeaderSection1.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            // 
            // tbxReportTitle
            // 
            this.tbxReportTitle.Anchoring = ((Telerik.Reporting.AnchoringStyles)((((Telerik.Reporting.AnchoringStyles.Top | Telerik.Reporting.AnchoringStyles.Bottom)
            | Telerik.Reporting.AnchoringStyles.Left)
            | Telerik.Reporting.AnchoringStyles.Right)));
            this.tbxReportTitle.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.69999986886978149D), Telerik.Reporting.Drawing.Unit.Cm(0.29999992251396179D));
            this.tbxReportTitle.Name = "tbxReportTitle";
            this.tbxReportTitle.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(17.67363166809082D), Telerik.Reporting.Drawing.Unit.Cm(1.0999000072479248D));
            this.tbxReportTitle.Style.Font.Bold = true;
            this.tbxReportTitle.Style.Font.Italic = false;
            this.tbxReportTitle.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(16D);
            this.tbxReportTitle.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Solid;
            this.tbxReportTitle.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.tbxReportTitle.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.tbxReportTitle.Value = "  北汽福田北京福田雷萨泵送机械分公司配件入库计划单";
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Cm(10.299998283386231D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.table1,
            this.table2,
            this.table3});
            this.detail.Name = "detail";
            // 
            // table1
            // 
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.2115800380706787D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.9520049095153809D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.2273309230804443D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.1751291751861572D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.0225827693939209D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(7.6113724708557129D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.88899928331375122D)));
            this.table1.Body.SetCellContent(0, 0, this.textBox4);
            this.table1.Body.SetCellContent(0, 2, this.textBox5);
            this.table1.Body.SetCellContent(0, 3, this.textBox6);
            this.table1.Body.SetCellContent(0, 1, this.textBox10);
            this.table1.Body.SetCellContent(0, 5, this.barcode2);
            tableGroup2.Name = "Group4";
            tableGroup2.ReportItem = this.textBox1;
            tableGroup1.ChildGroups.Add(tableGroup2);
            tableGroup1.ReportItem = this.textBox52;
            tableGroup4.Name = "Group5";
            tableGroup4.ReportItem = this.textBox9;
            tableGroup3.ChildGroups.Add(tableGroup4);
            tableGroup3.Name = "Group1";
            tableGroup3.ReportItem = this.textBox53;
            tableGroup6.Name = "Group6";
            tableGroup6.ReportItem = this.textBox2;
            tableGroup5.ChildGroups.Add(tableGroup6);
            tableGroup5.ReportItem = this.textBox54;
            tableGroup8.Name = "Group7";
            tableGroup8.ReportItem = this.textBox3;
            tableGroup7.ChildGroups.Add(tableGroup8);
            tableGroup7.ReportItem = this.textBox55;
            tableGroup10.Name = "Group8";
            tableGroup10.ReportItem = this.textBox11;
            tableGroup9.ChildGroups.Add(tableGroup10);
            tableGroup9.Name = "Group2";
            tableGroup9.ReportItem = this.textBox56;
            tableGroup12.Name = "Group9";
            tableGroup12.ReportItem = this.textBox51;
            tableGroup11.ChildGroups.Add(tableGroup12);
            tableGroup11.Name = "Group3";
            tableGroup11.ReportItem = this.textBox57;
            this.table1.ColumnGroups.Add(tableGroup1);
            this.table1.ColumnGroups.Add(tableGroup3);
            this.table1.ColumnGroups.Add(tableGroup5);
            this.table1.ColumnGroups.Add(tableGroup7);
            this.table1.ColumnGroups.Add(tableGroup9);
            this.table1.ColumnGroups.Add(tableGroup11);
            this.table1.DataSource = this.PartsInboundPlanDataSource;
            this.table1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox4,
            this.textBox5,
            this.textBox6,
            this.textBox10,
            this.textBox52,
            this.textBox1,
            this.textBox53,
            this.textBox9,
            this.textBox54,
            this.textBox2,
            this.textBox55,
            this.textBox3,
            this.textBox56,
            this.textBox11,
            this.textBox57,
            this.textBox51,
            this.barcode2});
            this.table1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.3999999463558197D), Telerik.Reporting.Drawing.Unit.Cm(0.90000009536743164D));
            this.table1.Name = "table1";
            tableGroup13.Groupings.AddRange(new Telerik.Reporting.Grouping[] {
            new Telerik.Reporting.Grouping(null)});
            tableGroup13.Name = "DetailGroup";
            this.table1.RowGroups.Add(tableGroup13);
            this.table1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.200000762939453D), Telerik.Reporting.Drawing.Unit.Cm(1.7408328056335449D));
            this.table1.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            // 
            // textBox4
            // 
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.2115800380706787D), Telerik.Reporting.Drawing.Unit.Cm(0.88899928331375122D));
            this.textBox4.Style.Font.Bold = true;
            this.textBox4.Value = "收货单位：";
            // 
            // textBox5
            // 
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.2273309230804443D), Telerik.Reporting.Drawing.Unit.Cm(0.88899928331375122D));
            this.textBox5.Style.Font.Bold = true;
            this.textBox5.Value = "品牌：";
            // 
            // textBox6
            // 
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.1751291751861572D), Telerik.Reporting.Drawing.Unit.Cm(0.88899928331375122D));
            this.textBox6.Value = "= Fields.PartsSalesCategory.Name";
            // 
            // textBox10
            // 
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9520050287246704D), Telerik.Reporting.Drawing.Unit.Cm(0.88899928331375122D));
            this.textBox10.StyleName = "";
            this.textBox10.Value = "= Fields.StorageCompanyName";
            // 
            // PartsInboundPlanDataSource
            // 
            this.PartsInboundPlanDataSource.DataMember = "GetPartsInboundPlanByIdForPrint";
            this.PartsInboundPlanDataSource.DataSource = typeof(Sunlight.Silverlight.Dcs.Web.DcsDomainService);
            this.PartsInboundPlanDataSource.Name = "PartsInboundPlanDataSource";
            this.PartsInboundPlanDataSource.Parameters.AddRange(new Telerik.Reporting.ObjectDataSourceParameter[] {
            new Telerik.Reporting.ObjectDataSourceParameter("partsInboundPlanId", typeof(int), "=Parameters.partsInboundPlanId.Value")});
            // 
            // table2
            // 
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.2018916606903076D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.4260098934173584D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(4.9638271331787109D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.1115754842758179D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.5615923404693604D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.1639041900634766D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.7712006568908691D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(1.0159996747970581D)));
            this.table2.Body.SetCellContent(0, 0, this.textBox15);
            this.table2.Body.SetCellContent(0, 2, this.textBox16);
            this.table2.Body.SetCellContent(0, 5, this.textBox17);
            this.table2.Body.SetCellContent(0, 3, this.textBox21);
            this.table2.Body.SetCellContent(0, 4, this.textBox23);
            this.table2.Body.SetCellContent(0, 6, this.textBox27);
            this.table2.Body.SetCellContent(0, 1, this.textBox25);
            tableGroup15.Name = "Group6";
            tableGroup15.ReportItem = this.textBox12;
            tableGroup14.ChildGroups.Add(tableGroup15);
            tableGroup14.ReportItem = this.textBox58;
            tableGroup17.Name = "Group7";
            tableGroup17.ReportItem = this.textBox24;
            tableGroup16.ChildGroups.Add(tableGroup17);
            tableGroup16.Name = "Group1";
            tableGroup16.ReportItem = this.textBox18;
            tableGroup19.Name = "Group8";
            tableGroup19.ReportItem = this.textBox13;
            tableGroup18.ChildGroups.Add(tableGroup19);
            tableGroup18.ReportItem = this.textBox60;
            tableGroup21.Name = "Group9";
            tableGroup21.ReportItem = this.textBox20;
            tableGroup20.ChildGroups.Add(tableGroup21);
            tableGroup20.Name = "Group2";
            tableGroup20.ReportItem = this.textBox61;
            tableGroup23.Name = "Group10";
            tableGroup23.ReportItem = this.textBox22;
            tableGroup22.ChildGroups.Add(tableGroup23);
            tableGroup22.Name = "Group3";
            tableGroup22.ReportItem = this.textBox62;
            tableGroup25.Name = "Group11";
            tableGroup25.ReportItem = this.textBox14;
            tableGroup24.ChildGroups.Add(tableGroup25);
            tableGroup24.ReportItem = this.textBox63;
            tableGroup27.Name = "Group13";
            tableGroup27.ReportItem = this.textBox26;
            tableGroup26.ChildGroups.Add(tableGroup27);
            tableGroup26.Name = "Group5";
            tableGroup26.ReportItem = this.textBox65;
            this.table2.ColumnGroups.Add(tableGroup14);
            this.table2.ColumnGroups.Add(tableGroup16);
            this.table2.ColumnGroups.Add(tableGroup18);
            this.table2.ColumnGroups.Add(tableGroup20);
            this.table2.ColumnGroups.Add(tableGroup22);
            this.table2.ColumnGroups.Add(tableGroup24);
            this.table2.ColumnGroups.Add(tableGroup26);
            this.table2.DataSource = this.PartsInboundPlanDetailDataSource;
            this.table2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox15,
            this.textBox16,
            this.textBox17,
            this.textBox21,
            this.textBox23,
            this.textBox27,
            this.textBox25,
            this.textBox58,
            this.textBox12,
            this.textBox18,
            this.textBox24,
            this.textBox60,
            this.textBox13,
            this.textBox61,
            this.textBox20,
            this.textBox62,
            this.textBox22,
            this.textBox63,
            this.textBox14,
            this.textBox65,
            this.textBox26});
            this.table2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.3999999463558197D), Telerik.Reporting.Drawing.Unit.Cm(2.8999998569488525D));
            this.table2.Name = "table2";
            tableGroup28.Groupings.AddRange(new Telerik.Reporting.Grouping[] {
            new Telerik.Reporting.Grouping(null)});
            tableGroup28.Name = "DetailGroup";
            this.table2.RowGroups.Add(tableGroup28);
            this.table2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.200000762939453D), Telerik.Reporting.Drawing.Unit.Cm(1.8678330183029175D));
            this.table2.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            // 
            // textBox15
            // 
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.2018916606903076D), Telerik.Reporting.Drawing.Unit.Cm(1.0159996747970581D));
            this.textBox15.Value = "= RowNumber()";
            // 
            // textBox16
            // 
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.9638271331787109D), Telerik.Reporting.Drawing.Unit.Cm(1.0159996747970581D));
            this.textBox16.Value = "= Fields.SparePartName";
            // 
            // textBox17
            // 
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.1639039516448975D), Telerik.Reporting.Drawing.Unit.Cm(1.0159996747970581D));
            this.textBox17.Value = "= Fields.Price";
            // 
            // textBox21
            // 
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.1115753650665283D), Telerik.Reporting.Drawing.Unit.Cm(1.0159996747970581D));
            this.textBox21.StyleName = "";
            this.textBox21.Value = "= Fields.InspectedQuantity";
            // 
            // textBox23
            // 
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.5615921020507813D), Telerik.Reporting.Drawing.Unit.Cm(1.0159996747970581D));
            this.textBox23.StyleName = "";
            this.textBox23.Value = "= Fields.SparePart.MeasureUnit";
            // 
            // textBox27
            // 
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.7712006568908691D), Telerik.Reporting.Drawing.Unit.Cm(1.0159996747970581D));
            this.textBox27.StyleName = "";
            this.textBox27.Value = "= Fields.Remark";
            // 
            // textBox25
            // 
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.4260098934173584D), Telerik.Reporting.Drawing.Unit.Cm(1.0159996747970581D));
            this.textBox25.StyleName = "";
            this.textBox25.Value = "= Fields.SparePartCode";
            // 
            // PartsInboundPlanDetailDataSource
            // 
            this.PartsInboundPlanDetailDataSource.DataMember = "GetPartsInboundPlanDetailByIdForPrint";
            this.PartsInboundPlanDetailDataSource.DataSource = typeof(Sunlight.Silverlight.Dcs.Web.DcsDomainService);
            this.PartsInboundPlanDetailDataSource.Name = "PartsInboundPlanDetailDataSource";
            this.PartsInboundPlanDetailDataSource.Parameters.AddRange(new Telerik.Reporting.ObjectDataSourceParameter[] {
            new Telerik.Reporting.ObjectDataSourceParameter("partsInboundPlanId", typeof(int), "=Parameters.partsInboundPlanId.Value")});
            // 
            // table3
            // 
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.0890884399414062D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.4824745655059814D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.8119179010391235D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.2018527984619141D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.9867314100265503D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.270003080368042D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.5983067750930786D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.37268593907356262D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.51791930198669434D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.46297523379325867D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.37642210721969604D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.0296225547790527D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.94191664457321167D)));
            this.table3.Body.SetCellContent(0, 0, this.textBox31);
            this.table3.Body.SetCellContent(0, 3, this.textBox32);
            this.table3.Body.SetCellContent(0, 4, this.textBox33);
            this.table3.Body.SetCellContent(0, 2, this.textBox35);
            this.table3.Body.SetCellContent(0, 1, this.textBox37);
            this.table3.Body.SetCellContent(0, 5, this.textBox39);
            this.table3.Body.SetCellContent(0, 10, this.textBox43);
            this.table3.Body.SetCellContent(0, 8, this.textBox45);
            this.table3.Body.SetCellContent(0, 6, this.textBox46);
            this.table3.Body.SetCellContent(0, 7, this.textBox48);
            this.table3.Body.SetCellContent(0, 9, this.textBox44);
            this.table3.Body.SetCellContent(0, 11, this.textBox50);
            tableGroup30.Name = "Group10";
            tableGroup30.ReportItem = this.textBox28;
            tableGroup29.ChildGroups.Add(tableGroup30);
            tableGroup29.ReportItem = this.textBox66;
            tableGroup32.Name = "Group11";
            tableGroup32.ReportItem = this.textBox36;
            tableGroup31.ChildGroups.Add(tableGroup32);
            tableGroup31.Name = "Group2";
            tableGroup31.ReportItem = this.textBox67;
            tableGroup34.Name = "Group12";
            tableGroup34.ReportItem = this.textBox34;
            tableGroup33.ChildGroups.Add(tableGroup34);
            tableGroup33.Name = "Group1";
            tableGroup33.ReportItem = this.textBox68;
            tableGroup36.Name = "Group13";
            tableGroup36.ReportItem = this.textBox29;
            tableGroup35.ChildGroups.Add(tableGroup36);
            tableGroup35.ReportItem = this.textBox69;
            tableGroup38.Name = "Group14";
            tableGroup38.ReportItem = this.textBox30;
            tableGroup37.ChildGroups.Add(tableGroup38);
            tableGroup37.ReportItem = this.textBox70;
            tableGroup40.Name = "Group15";
            tableGroup40.ReportItem = this.textBox38;
            tableGroup39.ChildGroups.Add(tableGroup40);
            tableGroup39.Name = "Group3";
            tableGroup39.ReportItem = this.textBox71;
            tableGroup42.Name = "Group16";
            tableGroup42.ReportItem = this.textBox40;
            tableGroup41.ChildGroups.Add(tableGroup42);
            tableGroup41.Name = "Group4";
            tableGroup41.ReportItem = this.textBox72;
            tableGroup45.Name = "Group7";
            tableGroup46.Name = "Group18";
            tableGroup47.Name = "Group19";
            tableGroup48.Name = "Group20";
            tableGroup44.ChildGroups.Add(tableGroup45);
            tableGroup44.ChildGroups.Add(tableGroup46);
            tableGroup44.ChildGroups.Add(tableGroup47);
            tableGroup44.ChildGroups.Add(tableGroup48);
            tableGroup44.Name = "Group17";
            tableGroup44.ReportItem = this.textBox47;
            tableGroup43.ChildGroups.Add(tableGroup44);
            tableGroup43.Name = "Group8";
            tableGroup43.ReportItem = this.textBox73;
            tableGroup50.Name = "Group21";
            tableGroup50.ReportItem = this.textBox49;
            tableGroup49.ChildGroups.Add(tableGroup50);
            tableGroup49.Name = "Group9";
            tableGroup49.ReportItem = this.textBox77;
            this.table3.ColumnGroups.Add(tableGroup29);
            this.table3.ColumnGroups.Add(tableGroup31);
            this.table3.ColumnGroups.Add(tableGroup33);
            this.table3.ColumnGroups.Add(tableGroup35);
            this.table3.ColumnGroups.Add(tableGroup37);
            this.table3.ColumnGroups.Add(tableGroup39);
            this.table3.ColumnGroups.Add(tableGroup41);
            this.table3.ColumnGroups.Add(tableGroup43);
            this.table3.ColumnGroups.Add(tableGroup49);
            this.table3.DataSource = this.PartsInboundPlanDataSource;
            this.table3.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox31,
            this.textBox32,
            this.textBox33,
            this.textBox35,
            this.textBox37,
            this.textBox39,
            this.textBox43,
            this.textBox45,
            this.textBox46,
            this.textBox48,
            this.textBox44,
            this.textBox50,
            this.textBox66,
            this.textBox28,
            this.textBox67,
            this.textBox36,
            this.textBox68,
            this.textBox34,
            this.textBox69,
            this.textBox29,
            this.textBox70,
            this.textBox30,
            this.textBox71,
            this.textBox38,
            this.textBox72,
            this.textBox40,
            this.textBox73,
            this.textBox47,
            this.textBox77,
            this.textBox49});
            this.table3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.3999999463558197D), Telerik.Reporting.Drawing.Unit.Cm(7.6000003814697266D));
            this.table3.Name = "table3";
            tableGroup51.Groupings.AddRange(new Telerik.Reporting.Grouping[] {
            new Telerik.Reporting.Grouping(null)});
            tableGroup51.Name = "DetailGroup";
            this.table3.RowGroups.Add(tableGroup51);
            this.table3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.200000762939453D), Telerik.Reporting.Drawing.Unit.Cm(2.3969998359680176D));
            // 
            // textBox31
            // 
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.0890882015228271D), Telerik.Reporting.Drawing.Unit.Cm(0.94191664457321167D));
            this.textBox31.Style.Font.Bold = true;
            this.textBox31.Value = "打印时间：";
            // 
            // textBox32
            // 
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.2018530368804932D), Telerik.Reporting.Drawing.Unit.Cm(0.94191664457321167D));
            // 
            // textBox33
            // 
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9867312908172607D), Telerik.Reporting.Drawing.Unit.Cm(0.94191664457321167D));
            // 
            // textBox35
            // 
            this.textBox35.Name = "textBox35";
            this.textBox35.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.8119179010391235D), Telerik.Reporting.Drawing.Unit.Cm(0.94191664457321167D));
            this.textBox35.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox35.StyleName = "";
            this.textBox35.Value = "注：";
            // 
            // textBox37
            // 
            this.textBox37.Name = "textBox37";
            this.textBox37.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.4824748039245605D), Telerik.Reporting.Drawing.Unit.Cm(0.94191664457321167D));
            this.textBox37.StyleName = "";
            this.textBox37.Value = "= Now()";
            // 
            // textBox39
            // 
            this.textBox39.Name = "textBox39";
            this.textBox39.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.270003080368042D), Telerik.Reporting.Drawing.Unit.Cm(0.94191664457321167D));
            this.textBox39.StyleName = "";
            // 
            // textBox43
            // 
            this.textBox43.Name = "textBox43";
            this.textBox43.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.37642210721969604D), Telerik.Reporting.Drawing.Unit.Cm(0.94191664457321167D));
            this.textBox43.StyleName = "";
            this.textBox43.Value = "= PageCount";
            // 
            // textBox45
            // 
            this.textBox45.Name = "textBox45";
            this.textBox45.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.51791936159133911D), Telerik.Reporting.Drawing.Unit.Cm(0.94191664457321167D));
            this.textBox45.StyleName = "";
            this.textBox45.Value = "页";
            // 
            // textBox46
            // 
            this.textBox46.Name = "textBox46";
            this.textBox46.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.5983067750930786D), Telerik.Reporting.Drawing.Unit.Cm(0.94191664457321167D));
            this.textBox46.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox46.StyleName = "";
            this.textBox46.Value = "第";
            // 
            // textBox48
            // 
            this.textBox48.Name = "textBox48";
            this.textBox48.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.37268593907356262D), Telerik.Reporting.Drawing.Unit.Cm(0.94191664457321167D));
            this.textBox48.StyleName = "";
            this.textBox48.Value = "= PageNumber";
            // 
            // textBox44
            // 
            this.textBox44.Name = "textBox44";
            this.textBox44.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.46297526359558105D), Telerik.Reporting.Drawing.Unit.Cm(0.94191664457321167D));
            this.textBox44.StyleName = "";
            this.textBox44.Value = "共";
            // 
            // textBox50
            // 
            this.textBox50.Name = "textBox50";
            this.textBox50.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.0296225547790527D), Telerik.Reporting.Drawing.Unit.Cm(0.94191664457321167D));
            this.textBox50.StyleName = "";
            this.textBox50.Value = "页";
            // 
            // pageFooterSection1
            // 
            this.pageFooterSection1.Height = Telerik.Reporting.Drawing.Unit.Cm(3D);
            this.pageFooterSection1.Name = "pageFooterSection1";
            // 
            // barcode2
            // 
            this.barcode2.Name = "barcode2";
            this.barcode2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.6113724708557129D), Telerik.Reporting.Drawing.Unit.Cm(0.88899928331375122D));
            this.barcode2.StyleName = "";
            this.barcode2.Value = "= Fields.Code";
            // 
            // ReportPartsInboundPlan
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pageHeaderSection1,
            this.detail,
            this.pageFooterSection1});
            this.Name = "ReportPartsInboundPlan";
            this.PageSettings.Margins.Bottom = Telerik.Reporting.Drawing.Unit.Mm(9D);
            this.PageSettings.Margins.Left = Telerik.Reporting.Drawing.Unit.Pixel(19D);
            this.PageSettings.Margins.Right = Telerik.Reporting.Drawing.Unit.Pixel(19D);
            this.PageSettings.Margins.Top = Telerik.Reporting.Drawing.Unit.Mm(9D);
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            reportParameter1.Name = "partsInboundPlanId";
            reportParameter2.Name = "UserName";
            this.ReportParameters.Add(reportParameter1);
            this.ReportParameters.Add(reportParameter2);
            this.Style.BackgroundColor = System.Drawing.Color.White;
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
            this.Width = Telerik.Reporting.Drawing.Unit.Cm(20D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.PageHeaderSection pageHeaderSection1;
        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.PageFooterSection pageFooterSection1;
        private Telerik.Reporting.TextBox tbxReportTitle;
        private Telerik.Reporting.Table table1;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.TextBox textBox10;
        private Telerik.Reporting.TextBox textBox9;
        private Telerik.Reporting.TextBox textBox11;
        private Telerik.Reporting.Table table2;
        private Telerik.Reporting.TextBox textBox15;
        private Telerik.Reporting.TextBox textBox16;
        private Telerik.Reporting.TextBox textBox17;
        private Telerik.Reporting.TextBox textBox21;
        private Telerik.Reporting.TextBox textBox23;
        private Telerik.Reporting.TextBox textBox27;
        private Telerik.Reporting.TextBox textBox12;
        private Telerik.Reporting.TextBox textBox13;
        private Telerik.Reporting.TextBox textBox20;
        private Telerik.Reporting.TextBox textBox22;
        private Telerik.Reporting.TextBox textBox14;
        private Telerik.Reporting.TextBox textBox26;
        private Telerik.Reporting.Table table3;
        private Telerik.Reporting.TextBox textBox31;
        private Telerik.Reporting.TextBox textBox32;
        private Telerik.Reporting.TextBox textBox33;
        private Telerik.Reporting.TextBox textBox35;
        private Telerik.Reporting.TextBox textBox37;
        private Telerik.Reporting.TextBox textBox39;
        private Telerik.Reporting.TextBox textBox43;
        private Telerik.Reporting.TextBox textBox28;
        private Telerik.Reporting.TextBox textBox36;
        private Telerik.Reporting.TextBox textBox34;
        private Telerik.Reporting.TextBox textBox29;
        private Telerik.Reporting.TextBox textBox30;
        private Telerik.Reporting.TextBox textBox38;
        private Telerik.Reporting.TextBox textBox40;
        private Telerik.Reporting.ObjectDataSource PartsInboundPlanDataSource;
        private Telerik.Reporting.ObjectDataSource PartsInboundPlanDetailDataSource;
        private Telerik.Reporting.TextBox textBox52;
        private Telerik.Reporting.TextBox textBox53;
        private Telerik.Reporting.TextBox textBox54;
        private Telerik.Reporting.TextBox textBox55;
        private Telerik.Reporting.TextBox textBox56;
        private Telerik.Reporting.TextBox textBox51;
        private Telerik.Reporting.TextBox textBox57;
        private Telerik.Reporting.TextBox textBox58;
        private Telerik.Reporting.TextBox textBox60;
        private Telerik.Reporting.TextBox textBox61;
        private Telerik.Reporting.TextBox textBox62;
        private Telerik.Reporting.TextBox textBox63;
        private Telerik.Reporting.TextBox textBox65;
        private Telerik.Reporting.TextBox textBox45;
        private Telerik.Reporting.TextBox textBox46;
        private Telerik.Reporting.TextBox textBox48;
        private Telerik.Reporting.TextBox textBox44;
        private Telerik.Reporting.TextBox textBox50;
        private Telerik.Reporting.TextBox textBox66;
        private Telerik.Reporting.TextBox textBox67;
        private Telerik.Reporting.TextBox textBox68;
        private Telerik.Reporting.TextBox textBox69;
        private Telerik.Reporting.TextBox textBox70;
        private Telerik.Reporting.TextBox textBox71;
        private Telerik.Reporting.TextBox textBox72;
        private Telerik.Reporting.TextBox textBox47;
        private Telerik.Reporting.TextBox textBox73;
        private Telerik.Reporting.TextBox textBox49;
        private Telerik.Reporting.TextBox textBox77;
        private Telerik.Reporting.TextBox textBox25;
        private Telerik.Reporting.TextBox textBox24;
        private Telerik.Reporting.TextBox textBox18;
        private Telerik.Reporting.Barcode barcode2;
    }
}