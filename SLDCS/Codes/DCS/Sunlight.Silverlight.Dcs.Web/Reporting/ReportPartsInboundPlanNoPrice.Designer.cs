namespace Sunlight.Silverlight.Dcs.Web.Reporting {
    partial class ReportPartsInboundPlanNoPrice {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.ReportParameter reportParameter1 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter2 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            this.textBox12 = new Telerik.Reporting.TextBox();
            this.textBox30 = new Telerik.Reporting.TextBox();
            this.textBox24 = new Telerik.Reporting.TextBox();
            this.textBox13 = new Telerik.Reporting.TextBox();
            this.textBox20 = new Telerik.Reporting.TextBox();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.textBox22 = new Telerik.Reporting.TextBox();
            this.textBox26 = new Telerik.Reporting.TextBox();
            this.detail = new Telerik.Reporting.DetailSection();
            this.table2 = new Telerik.Reporting.Table();
            this.textBox15 = new Telerik.Reporting.TextBox();
            this.textBox16 = new Telerik.Reporting.TextBox();
            this.textBox21 = new Telerik.Reporting.TextBox();
            this.textBox23 = new Telerik.Reporting.TextBox();
            this.textBox27 = new Telerik.Reporting.TextBox();
            this.textBox25 = new Telerik.Reporting.TextBox();
            this.textBox31 = new Telerik.Reporting.TextBox();
            this.textBox7 = new Telerik.Reporting.TextBox();
            this.textBox9 = new Telerik.Reporting.TextBox();
            this.textBox34 = new Telerik.Reporting.TextBox();
            this.textBox29 = new Telerik.Reporting.TextBox();
            this.textBox19 = new Telerik.Reporting.TextBox();
            this.objectDataSource2 = new Telerik.Reporting.ObjectDataSource();
            this.objectDataSource1 = new Telerik.Reporting.ObjectDataSource();
            this.textBox8 = new Telerik.Reporting.TextBox();
            this.pageFooterSection1 = new Telerik.Reporting.PageFooterSection();
            this.textBox52 = new Telerik.Reporting.TextBox();
            this.textBox51 = new Telerik.Reporting.TextBox();
            this.textBox10 = new Telerik.Reporting.TextBox();
            this.textBox41 = new Telerik.Reporting.TextBox();
            this.textBox42 = new Telerik.Reporting.TextBox();
            this.textBox53 = new Telerik.Reporting.TextBox();
            this.textBox54 = new Telerik.Reporting.TextBox();
            this.textBox55 = new Telerik.Reporting.TextBox();
            this.textBox56 = new Telerik.Reporting.TextBox();
            this.textBox57 = new Telerik.Reporting.TextBox();
            this.textBox59 = new Telerik.Reporting.TextBox();
            this.textBox64 = new Telerik.Reporting.TextBox();
            this.textBox74 = new Telerik.Reporting.TextBox();
            this.textBox75 = new Telerik.Reporting.TextBox();
            this.textBox76 = new Telerik.Reporting.TextBox();
            this.textBox78 = new Telerik.Reporting.TextBox();
            this.textBox79 = new Telerik.Reporting.TextBox();
            this.pageHeaderSection1 = new Telerik.Reporting.PageHeaderSection();
            this.textBox28 = new Telerik.Reporting.TextBox();
            this.textBox32 = new Telerik.Reporting.TextBox();
            this.textBox33 = new Telerik.Reporting.TextBox();
            this.textBox18 = new Telerik.Reporting.TextBox();
            this.textBox36 = new Telerik.Reporting.TextBox();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.textBox11 = new Telerik.Reporting.TextBox();
            this.textBox37 = new Telerik.Reporting.TextBox();
            this.textBox39 = new Telerik.Reporting.TextBox();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.textBox35 = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // textBox12
            // 
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.93255525827407837D), Telerik.Reporting.Drawing.Unit.Cm(0.52916669845581055D));
            this.textBox12.Style.Font.Bold = false;
            this.textBox12.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox12.Value = "序号";
            // 
            // textBox30
            // 
            this.textBox30.Name = "textBox30";
            this.textBox30.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.74898099899292D), Telerik.Reporting.Drawing.Unit.Cm(0.52916669845581055D));
            this.textBox30.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox30.StyleName = "";
            this.textBox30.Value = "货位号";
            // 
            // textBox24
            // 
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.3042440414428711D), Telerik.Reporting.Drawing.Unit.Cm(0.52916669845581055D));
            this.textBox24.Style.Font.Bold = false;
            this.textBox24.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox24.StyleName = "";
            this.textBox24.Value = "配件图号";
            // 
            // textBox13
            // 
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.2184009552001953D), Telerik.Reporting.Drawing.Unit.Cm(0.52916669845581055D));
            this.textBox13.Style.Font.Bold = false;
            this.textBox13.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox13.Value = "配件名称";
            // 
            // textBox20
            // 
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.3688410520553589D), Telerik.Reporting.Drawing.Unit.Cm(0.52916669845581055D));
            this.textBox20.Style.Font.Bold = false;
            this.textBox20.StyleName = "";
            this.textBox20.Value = "计划量";
            // 
            // textBox6
            // 
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.2934978008270264D), Telerik.Reporting.Drawing.Unit.Cm(0.52916669845581055D));
            this.textBox6.StyleName = "";
            this.textBox6.Value = "检验量";
            // 
            // textBox22
            // 
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.92240637540817261D), Telerik.Reporting.Drawing.Unit.Cm(0.52916669845581055D));
            this.textBox22.Style.Font.Bold = false;
            this.textBox22.StyleName = "";
            this.textBox22.Value = "单位";
            // 
            // textBox26
            // 
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.2108732461929321D), Telerik.Reporting.Drawing.Unit.Cm(0.52916669845581055D));
            this.textBox26.Style.Font.Bold = false;
            this.textBox26.StyleName = "";
            this.textBox26.Value = "备注";
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Cm(1.9614661931991577D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.table2});
            this.detail.Name = "detail";
            this.detail.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.detail.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            // 
            // table2
            // 
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.932555079460144D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.74898099899292D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(5.3042440414428711D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(6.2184009552001953D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.3688410520553589D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.2934978008270264D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.92240655422210693D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.2108732461929321D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.699999988079071D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.44966641068458557D)));
            this.table2.Body.SetCellContent(0, 0, this.textBox15);
            this.table2.Body.SetCellContent(0, 3, this.textBox16);
            this.table2.Body.SetCellContent(0, 4, this.textBox21);
            this.table2.Body.SetCellContent(0, 6, this.textBox23);
            this.table2.Body.SetCellContent(0, 7, this.textBox27);
            this.table2.Body.SetCellContent(0, 2, this.textBox25);
            this.table2.Body.SetCellContent(0, 1, this.textBox31);
            this.table2.Body.SetCellContent(1, 0, this.textBox7);
            this.table2.Body.SetCellContent(1, 3, this.textBox35);
            this.table2.Body.SetCellContent(1, 1, this.textBox9, 1, 2);
            this.table2.Body.SetCellContent(1, 4, this.textBox34, 1, 3);
            this.table2.Body.SetCellContent(1, 7, this.textBox29);
            this.table2.Body.SetCellContent(0, 5, this.textBox19);
            tableGroup1.Name = "Group6";
            tableGroup1.ReportItem = this.textBox12;
            tableGroup2.Name = "Group12";
            tableGroup2.ReportItem = this.textBox30;
            tableGroup3.Name = "Group7";
            tableGroup3.ReportItem = this.textBox24;
            tableGroup4.Name = "Group8";
            tableGroup4.ReportItem = this.textBox13;
            tableGroup5.Name = "Group9";
            tableGroup5.ReportItem = this.textBox20;
            tableGroup6.Name = "group";
            tableGroup6.ReportItem = this.textBox6;
            tableGroup7.Name = "Group10";
            tableGroup7.ReportItem = this.textBox22;
            tableGroup8.Name = "Group13";
            tableGroup8.ReportItem = this.textBox26;
            this.table2.ColumnGroups.Add(tableGroup1);
            this.table2.ColumnGroups.Add(tableGroup2);
            this.table2.ColumnGroups.Add(tableGroup3);
            this.table2.ColumnGroups.Add(tableGroup4);
            this.table2.ColumnGroups.Add(tableGroup5);
            this.table2.ColumnGroups.Add(tableGroup6);
            this.table2.ColumnGroups.Add(tableGroup7);
            this.table2.ColumnGroups.Add(tableGroup8);
            this.table2.ColumnHeadersPrintOnEveryPage = true;
            this.table2.DataSource = this.objectDataSource2;
            this.table2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox15,
            this.textBox31,
            this.textBox25,
            this.textBox16,
            this.textBox21,
            this.textBox19,
            this.textBox23,
            this.textBox27,
            this.textBox7,
            this.textBox9,
            this.textBox35,
            this.textBox34,
            this.textBox29,
            this.textBox12,
            this.textBox30,
            this.textBox24,
            this.textBox13,
            this.textBox20,
            this.textBox6,
            this.textBox22,
            this.textBox26});
            this.table2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.9961594969499856E-05D), Telerik.Reporting.Drawing.Unit.Cm(0.25679954886436462D));
            this.table2.Name = "table2";
            tableGroup9.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup9.Name = "DetailGroup";
            tableGroup10.Name = "Group14";
            this.table2.RowGroups.Add(tableGroup9);
            this.table2.RowGroups.Add(tableGroup10);
            this.table2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.999799728393555D), Telerik.Reporting.Drawing.Unit.Cm(1.6788331270217896D));
            this.table2.Sortings.Add(new Telerik.Reporting.Sorting("=Fields.WarehouseAreaCode", Telerik.Reporting.SortDirection.Asc));
            this.table2.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.table2.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.table2.Style.Font.Name = "宋体";
            this.table2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            // 
            // textBox15
            // 
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.93255525827407837D), Telerik.Reporting.Drawing.Unit.Cm(0.699999988079071D));
            this.textBox15.Value = "= RowNumber()";
            // 
            // textBox16
            // 
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.2184009552001953D), Telerik.Reporting.Drawing.Unit.Cm(0.699999988079071D));
            this.textBox16.Value = "= Fields.SparePartName";
            // 
            // textBox21
            // 
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.3688410520553589D), Telerik.Reporting.Drawing.Unit.Cm(0.699999988079071D));
            this.textBox21.StyleName = "";
            this.textBox21.Value = "= Fields.PlannedAmount";
            // 
            // textBox23
            // 
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.92240637540817261D), Telerik.Reporting.Drawing.Unit.Cm(0.699999988079071D));
            this.textBox23.StyleName = "";
            this.textBox23.Value = "= Fields.Unit";
            // 
            // textBox27
            // 
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.2108732461929321D), Telerik.Reporting.Drawing.Unit.Cm(0.699999988079071D));
            this.textBox27.StyleName = "";
            this.textBox27.Value = "= Fields.Remark";
            // 
            // textBox25
            // 
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.3042440414428711D), Telerik.Reporting.Drawing.Unit.Cm(0.699999988079071D));
            this.textBox25.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox25.StyleName = "";
            this.textBox25.Value = "= Fields.SparePartCode";
            // 
            // textBox31
            // 
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.74898099899292D), Telerik.Reporting.Drawing.Unit.Cm(0.699999988079071D));
            this.textBox31.StyleName = "";
            this.textBox31.Value = "= Fields.WarehouseAreaCode";
            // 
            // textBox7
            // 
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.93255525827407837D), Telerik.Reporting.Drawing.Unit.Cm(0.44966641068458557D));
            this.textBox7.StyleName = "";
            // 
            // textBox9
            // 
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.0532245635986328D), Telerik.Reporting.Drawing.Unit.Cm(0.44966641068458557D));
            this.textBox9.StyleName = "";
            // 
            // textBox34
            // 
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.5847451686859131D), Telerik.Reporting.Drawing.Unit.Cm(0.44966641068458557D));
            this.textBox34.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox34.StyleName = "";
            // 
            // textBox29
            // 
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.2108732461929321D), Telerik.Reporting.Drawing.Unit.Cm(0.44966641068458557D));
            this.textBox29.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox29.Style.Visible = false;
            this.textBox29.StyleName = "";
            // 
            // textBox19
            // 
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.2934978008270264D), Telerik.Reporting.Drawing.Unit.Cm(0.699999988079071D));
            this.textBox19.StyleName = "";
            this.textBox19.Value = "= Fields.InspectedQuantity";
            // 
            // objectDataSource2
            // 
            this.objectDataSource2.DataMember = "GetPartsInboundPlanDetailByIdForPrint";
            this.objectDataSource2.DataSource = typeof(Sunlight.Silverlight.Dcs.Web.DcsDomainService);
            this.objectDataSource2.Name = "objectDataSource2";
            this.objectDataSource2.Parameters.AddRange(new Telerik.Reporting.ObjectDataSourceParameter[] {
            new Telerik.Reporting.ObjectDataSourceParameter("partsInboundPlanId", typeof(int), "=Parameters.partsInboundPlanId.Value")});
            // 
            // objectDataSource1
            // 
            this.objectDataSource1.DataMember = "GetPartsInboundPlanByIdForPrint";
            this.objectDataSource1.DataSource = typeof(Sunlight.Silverlight.Dcs.Web.DcsDomainService);
            this.objectDataSource1.Name = "objectDataSource1";
            this.objectDataSource1.Parameters.AddRange(new Telerik.Reporting.ObjectDataSourceParameter[] {
            new Telerik.Reporting.ObjectDataSourceParameter("partsInboundPlanId", typeof(int), "=Parameters.partsInboundPlanId.Value")});
            // 
            // textBox8
            // 
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(10.445978164672852D), Telerik.Reporting.Drawing.Unit.Cm(0.91016650199890137D));
            this.textBox8.StyleName = "";
            // 
            // pageFooterSection1
            // 
            this.pageFooterSection1.Height = Telerik.Reporting.Drawing.Unit.Cm(1.0953342914581299D);
            this.pageFooterSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox52,
            this.textBox51,
            this.textBox10,
            this.textBox41,
            this.textBox42,
            this.textBox53,
            this.textBox54,
            this.textBox55,
            this.textBox56,
            this.textBox57,
            this.textBox59,
            this.textBox64,
            this.textBox74,
            this.textBox75,
            this.textBox76,
            this.textBox78,
            this.textBox79});
            this.pageFooterSection1.Name = "pageFooterSection1";
            this.pageFooterSection1.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.pageFooterSection1.Style.Font.Name = "宋体";
            this.pageFooterSection1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            // 
            // textBox52
            // 
            this.textBox52.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(7D), Telerik.Reporting.Drawing.Unit.Cm(0.59533381462097168D));
            this.textBox52.Name = "textBox52";
            this.textBox52.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.0408608913421631D), Telerik.Reporting.Drawing.Unit.Cm(0.5000004768371582D));
            this.textBox52.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox52.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox52.Value = "注：";
            // 
            // textBox51
            // 
            this.textBox51.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(17.88279914855957D), Telerik.Reporting.Drawing.Unit.Cm(9.9477132607717067E-05D));
            this.textBox51.Name = "textBox51";
            this.textBox51.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.0333333015441895D), Telerik.Reporting.Drawing.Unit.Cm(0.5000004768371582D));
            this.textBox51.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox51.Value = "= Parameters.UserName.Value";
            // 
            // textBox10
            // 
            this.textBox10.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(16.431032180786133D), Telerik.Reporting.Drawing.Unit.Cm(9.9477132607717067E-05D));
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.4515676498413086D), Telerik.Reporting.Drawing.Unit.Cm(0.5000004768371582D));
            this.textBox10.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox10.Value = "打印人：";
            // 
            // textBox41
            // 
            this.textBox41.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.09555721282959D), Telerik.Reporting.Drawing.Unit.Cm(9.9477132607717067E-05D));
            this.textBox41.Name = "textBox41";
            this.textBox41.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.3352766036987305D), Telerik.Reporting.Drawing.Unit.Cm(0.5000004768371582D));
            this.textBox41.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox41.Value = "=Fields.CreateTime";
            // 
            // textBox42
            // 
            this.textBox42.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.297283172607422D), Telerik.Reporting.Drawing.Unit.Cm(9.9477132607717067E-05D));
            this.textBox42.Name = "textBox42";
            this.textBox42.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.7980741262435913D), Telerik.Reporting.Drawing.Unit.Cm(0.49999919533729553D));
            this.textBox42.Style.Font.Bold = false;
            this.textBox42.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox42.Value = "制单时间：";
            // 
            // textBox53
            // 
            this.textBox53.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(6.714902400970459D), Telerik.Reporting.Drawing.Unit.Cm(9.9477132607717067E-05D));
            this.textBox53.Name = "textBox53";
            this.textBox53.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.6110553741455078D), Telerik.Reporting.Drawing.Unit.Cm(0.5000004768371582D));
            this.textBox53.Style.Font.Bold = false;
            this.textBox53.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox53.Value = "制单人：";
            // 
            // textBox54
            // 
            this.textBox54.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.7891782522201538D), Telerik.Reporting.Drawing.Unit.Cm(0.59533381462097168D));
            this.textBox54.Name = "textBox54";
            this.textBox54.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.2123746871948242D), Telerik.Reporting.Drawing.Unit.Cm(0.49999919533729553D));
            this.textBox54.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox54.Value = "= Now()";
            // 
            // textBox55
            // 
            this.textBox55.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(8.32615852355957D), Telerik.Reporting.Drawing.Unit.Cm(9.9477132607717067E-05D));
            this.textBox55.Name = "textBox55";
            this.textBox55.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9709242582321167D), Telerik.Reporting.Drawing.Unit.Cm(0.5000004768371582D));
            this.textBox55.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox55.Value = "=Fields.CreatorName";
            // 
            // textBox56
            // 
            this.textBox56.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.7891782522201538D), Telerik.Reporting.Drawing.Unit.Cm(9.9477132607717067E-05D));
            this.textBox56.Name = "textBox56";
            this.textBox56.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.7884616851806641D), Telerik.Reporting.Drawing.Unit.Cm(0.49999919533729553D));
            this.textBox56.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox56.Value = "= Fields.StorageCompanyName";
            // 
            // textBox57
            // 
            this.textBox57.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(16.600000381469727D), Telerik.Reporting.Drawing.Unit.Cm(0.59533381462097168D));
            this.textBox57.Name = "textBox57";
            this.textBox57.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.53253436088562012D), Telerik.Reporting.Drawing.Unit.Cm(0.5000004768371582D));
            this.textBox57.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox57.Value = "第";
            // 
            // textBox59
            // 
            this.textBox59.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(17.132734298706055D), Telerik.Reporting.Drawing.Unit.Cm(0.59533381462097168D));
            this.textBox59.Name = "textBox59";
            this.textBox59.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.53253436088562012D), Telerik.Reporting.Drawing.Unit.Cm(0.5000004768371582D));
            this.textBox59.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox59.Value = "= PageNumber";
            // 
            // textBox64
            // 
            this.textBox64.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(17.746566772460938D), Telerik.Reporting.Drawing.Unit.Cm(0.59533381462097168D));
            this.textBox64.Name = "textBox64";
            this.textBox64.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.53253436088562012D), Telerik.Reporting.Drawing.Unit.Cm(0.5000004768371582D));
            this.textBox64.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox64.Value = "页";
            // 
            // textBox74
            // 
            this.textBox74.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(18.279300689697266D), Telerik.Reporting.Drawing.Unit.Cm(0.59533381462097168D));
            this.textBox74.Name = "textBox74";
            this.textBox74.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.53253436088562012D), Telerik.Reporting.Drawing.Unit.Cm(0.5000004768371582D));
            this.textBox74.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox74.Value = "共";
            // 
            // textBox75
            // 
            this.textBox75.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(18.812034606933594D), Telerik.Reporting.Drawing.Unit.Cm(0.59533381462097168D));
            this.textBox75.Name = "textBox75";
            this.textBox75.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.53253436088562012D), Telerik.Reporting.Drawing.Unit.Cm(0.5000004768371582D));
            this.textBox75.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox75.Value = "= PageCount";
            // 
            // textBox76
            // 
            this.textBox76.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(19.376399993896484D), Telerik.Reporting.Drawing.Unit.Cm(0.59533381462097168D));
            this.textBox76.Name = "textBox76";
            this.textBox76.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.53253436088562012D), Telerik.Reporting.Drawing.Unit.Cm(0.5000004768371582D));
            this.textBox76.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox76.Value = "页";
            // 
            // textBox78
            // 
            this.textBox78.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(0.59533381462097168D));
            this.textBox78.Name = "textBox78";
            this.textBox78.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.7889779806137085D), Telerik.Reporting.Drawing.Unit.Cm(0.49999919533729553D));
            this.textBox78.Style.Font.Bold = false;
            this.textBox78.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox78.Value = "打印时间：";
            // 
            // textBox79
            // 
            this.textBox79.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(9.9477132607717067E-05D));
            this.textBox79.Name = "textBox79";
            this.textBox79.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.7889777421951294D), Telerik.Reporting.Drawing.Unit.Cm(0.49999919533729553D));
            this.textBox79.Style.Font.Bold = false;
            this.textBox79.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox79.Value = "制单单位：";
            // 
            // pageHeaderSection1
            // 
            this.pageHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Cm(3.4000000953674316D);
            this.pageHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox28,
            this.textBox32,
            this.textBox33,
            this.textBox18,
            this.textBox36,
            this.textBox1,
            this.textBox2,
            this.textBox3,
            this.textBox11,
            this.textBox37,
            this.textBox39,
            this.textBox5,
            this.textBox4});
            this.pageHeaderSection1.Name = "pageHeaderSection1";
            this.pageHeaderSection1.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            // 
            // textBox28
            // 
            this.textBox28.Anchoring = ((Telerik.Reporting.AnchoringStyles)((((Telerik.Reporting.AnchoringStyles.Top | Telerik.Reporting.AnchoringStyles.Bottom) 
            | Telerik.Reporting.AnchoringStyles.Left) 
            | Telerik.Reporting.AnchoringStyles.Right)));
            this.textBox28.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.7089769840240479D), Telerik.Reporting.Drawing.Unit.Cm(0.27026483416557312D));
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(16.173624038696289D), Telerik.Reporting.Drawing.Unit.Cm(0.82953524589538574D));
            this.textBox28.Style.Font.Bold = false;
            this.textBox28.Style.Font.Italic = false;
            this.textBox28.Style.Font.Name = "宋体";
            this.textBox28.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(16D);
            this.textBox28.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Solid;
            this.textBox28.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox28.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox28.Value = "= Fields.BranchName+\"配件入库计划单\"";
            // 
            // textBox32
            // 
            this.textBox32.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.191508293151856D), Telerik.Reporting.Drawing.Unit.Cm(1.5000001192092896D));
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.6302108764648438D), Telerik.Reporting.Drawing.Unit.Cm(0.57149988412857056D));
            this.textBox32.Style.Font.Bold = false;
            this.textBox32.Style.Font.Name = "宋体";
            this.textBox32.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox32.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox32.StyleName = "";
            this.textBox32.Value = "入库计划单号：";
            // 
            // textBox33
            // 
            this.textBox33.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.821919441223145D), Telerik.Reporting.Drawing.Unit.Cm(1.5000001192092896D));
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.1779804229736328D), Telerik.Reporting.Drawing.Unit.Cm(0.57149988412857056D));
            this.textBox33.Style.Font.Name = "宋体";
            this.textBox33.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox33.StyleName = "";
            this.textBox33.Value = "= Fields.Code";
            // 
            // textBox18
            // 
            this.textBox18.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.054652992635965347D), Telerik.Reporting.Drawing.Unit.Cm(1.5000001192092896D));
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.7343249320983887D), Telerik.Reporting.Drawing.Unit.Cm(0.57149994373321533D));
            this.textBox18.Style.Font.Bold = false;
            this.textBox18.Style.Font.Name = "宋体";
            this.textBox18.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox18.Value = "供货单位：";
            // 
            // textBox36
            // 
            this.textBox36.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.7891782522201538D), Telerik.Reporting.Drawing.Unit.Cm(1.5000001192092896D));
            this.textBox36.Name = "textBox36";
            this.textBox36.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.0108213424682617D), Telerik.Reporting.Drawing.Unit.Cm(0.57149994373321533D));
            this.textBox36.Style.Font.Name = "宋体";
            this.textBox36.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox36.StyleName = "";
            this.textBox36.Value = "= Fields.CounterpartCompanyName";
            // 
            // textBox1
            // 
            this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.054652992635965347D), Telerik.Reporting.Drawing.Unit.Cm(2.0716006755828857D));
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.7343251705169678D), Telerik.Reporting.Drawing.Unit.Cm(0.57149982452392578D));
            this.textBox1.Style.Font.Bold = false;
            this.textBox1.Style.Font.Name = "宋体";
            this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox1.Value = "收货单位：";
            // 
            // textBox2
            // 
            this.textBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.7891782522201538D), Telerik.Reporting.Drawing.Unit.Cm(2.07170033454895D));
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.01062297821045D), Telerik.Reporting.Drawing.Unit.Cm(0.57149988412857056D));
            this.textBox2.Style.Font.Name = "宋体";
            this.textBox2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox2.StyleName = "";
            this.textBox2.Value = "= Fields.StorageCompanyName";
            // 
            // textBox3
            // 
            this.textBox3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.8000001907348633D), Telerik.Reporting.Drawing.Unit.Cm(2.07170033454895D));
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.8416167497634888D), Telerik.Reporting.Drawing.Unit.Cm(0.57149988412857056D));
            this.textBox3.Style.Font.Bold = false;
            this.textBox3.Style.Font.Name = "宋体";
            this.textBox3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox3.Value = "入库类型：";
            // 
            // textBox11
            // 
            this.textBox11.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.09111213684082D), Telerik.Reporting.Drawing.Unit.Cm(2.0716001987457275D));
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.7306089401245117D), Telerik.Reporting.Drawing.Unit.Cm(0.57149988412857056D));
            this.textBox11.Style.Font.Name = "宋体";
            this.textBox11.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox11.Value = "= Sunlight.Silverlight.Dcs.Web.Reporting.CommonConverter.PartsInboundCheckBillInb" +
    "oundTypeFormat(Fields.InboundType)";
            // 
            // textBox37
            // 
            this.textBox37.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.599124908447266D), Telerik.Reporting.Drawing.Unit.Cm(2.07170033454895D));
            this.textBox37.Name = "textBox37";
            this.textBox37.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.5334104299545288D), Telerik.Reporting.Drawing.Unit.Cm(0.57149988412857056D));
            this.textBox37.Style.Font.Bold = false;
            this.textBox37.Style.Font.Name = "宋体";
            this.textBox37.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox37.Value = "品牌：";
            // 
            // textBox39
            // 
            this.textBox39.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(17.421867370605469D), Telerik.Reporting.Drawing.Unit.Cm(2.0716001987457275D));
            this.textBox39.Name = "textBox39";
            this.textBox39.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.55696702003479D), Telerik.Reporting.Drawing.Unit.Cm(0.57149988412857056D));
            this.textBox39.Style.Font.Name = "宋体";
            this.textBox39.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox39.Value = "= Fields.PartsSalesCategorieName";
            // 
            // textBox5
            // 
            this.textBox5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.054652992635965347D), Telerik.Reporting.Drawing.Unit.Cm(2.6434006690979004D));
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.6451466083526611D), Telerik.Reporting.Drawing.Unit.Cm(0.55659955739974976D));
            this.textBox5.Style.Font.Name = "宋体";
            this.textBox5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox5.Value = "采购订单类型:";
            // 
            // textBox4
            // 
            this.textBox4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.6999998092651367D), Telerik.Reporting.Drawing.Unit.Cm(2.6434006690979004D));
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.0998015403747559D), Telerik.Reporting.Drawing.Unit.Cm(0.55659973621368408D));
            this.textBox4.Style.Font.Name = "宋体";
            this.textBox4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox4.Value = "= Fields.PartsPurchaseOrderType";
            // 
            // textBox35
            // 
            this.textBox35.Name = "textBox35";
            this.textBox35.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.2184009552001953D), Telerik.Reporting.Drawing.Unit.Cm(0.44966641068458557D));
            this.textBox35.StyleName = "";
            // 
            // ReportPartsInboundPlanNoPrice
            // 
            this.DataSource = this.objectDataSource1;
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.detail,
            this.pageFooterSection1,
            this.pageHeaderSection1});
            this.Name = "ReportPartsInboundPlan1";
            this.PageSettings.Landscape = true;
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Pixel(25D), Telerik.Reporting.Drawing.Unit.Pixel(19D), Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Custom;
            this.PageSettings.PaperSize = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(14D), Telerik.Reporting.Drawing.Unit.Cm(22D));
            reportParameter1.Name = "partsInboundPlanId";
            reportParameter1.Type = Telerik.Reporting.ReportParameterType.Integer;
            reportParameter2.Name = "UserName";
            this.ReportParameters.Add(reportParameter1);
            this.ReportParameters.Add(reportParameter2);
            this.Style.BackgroundColor = System.Drawing.Color.White;
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
            this.Width = Telerik.Reporting.Drawing.Unit.Cm(20D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.PageFooterSection pageFooterSection1;
        private Telerik.Reporting.ObjectDataSource objectDataSource1;
        private Telerik.Reporting.ObjectDataSource objectDataSource2;
        private Telerik.Reporting.Table table2;
        private Telerik.Reporting.TextBox textBox15;
        private Telerik.Reporting.TextBox textBox16;
        private Telerik.Reporting.TextBox textBox21;
        private Telerik.Reporting.TextBox textBox23;
        private Telerik.Reporting.TextBox textBox25;
        private Telerik.Reporting.TextBox textBox12;
        private Telerik.Reporting.TextBox textBox24;
        private Telerik.Reporting.TextBox textBox13;
        private Telerik.Reporting.TextBox textBox20;
        private Telerik.Reporting.TextBox textBox22;
        private Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.TextBox textBox52;
        private Telerik.Reporting.TextBox textBox51;
        private Telerik.Reporting.TextBox textBox10;
        private Telerik.Reporting.TextBox textBox41;
        private Telerik.Reporting.TextBox textBox42;
        private Telerik.Reporting.TextBox textBox53;
        private Telerik.Reporting.TextBox textBox54;
        private Telerik.Reporting.TextBox textBox55;
        private Telerik.Reporting.TextBox textBox56;
        private Telerik.Reporting.TextBox textBox57;
        private Telerik.Reporting.TextBox textBox59;
        private Telerik.Reporting.TextBox textBox64;
        private Telerik.Reporting.TextBox textBox74;
        private Telerik.Reporting.TextBox textBox75;
        private Telerik.Reporting.TextBox textBox76;
        private Telerik.Reporting.TextBox textBox78;
        private Telerik.Reporting.TextBox textBox79;
        private Telerik.Reporting.PageHeaderSection pageHeaderSection1;
        private Telerik.Reporting.TextBox textBox28;
        private Telerik.Reporting.TextBox textBox31;
        private Telerik.Reporting.TextBox textBox30;
        private Telerik.Reporting.TextBox textBox32;
        private Telerik.Reporting.TextBox textBox33;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.TextBox textBox9;
        private Telerik.Reporting.TextBox textBox34;
        private Telerik.Reporting.TextBox textBox18;
        private Telerik.Reporting.TextBox textBox36;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.TextBox textBox11;
        private Telerik.Reporting.TextBox textBox37;
        private Telerik.Reporting.TextBox textBox39;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.TextBox textBox19;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.TextBox textBox27;
        private Telerik.Reporting.TextBox textBox29;
        private Telerik.Reporting.TextBox textBox26;
        private Telerik.Reporting.TextBox textBox35;
    }
}