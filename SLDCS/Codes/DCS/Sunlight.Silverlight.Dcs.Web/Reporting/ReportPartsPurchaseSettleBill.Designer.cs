namespace Sunlight.Silverlight.Dcs.Web.Reporting {
    partial class ReportPartsPurchaseSettleBill {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup11 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup12 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup13 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup14 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup15 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup16 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup17 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup18 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup19 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup20 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup21 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup22 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup23 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup24 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup25 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup26 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup27 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup28 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup29 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup30 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup31 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup32 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup33 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup34 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup35 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup36 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup37 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup38 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup39 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup40 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.ReportParameter reportParameter1 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter2 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.textBox95 = new Telerik.Reporting.TextBox();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.textBox96 = new Telerik.Reporting.TextBox();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.textBox97 = new Telerik.Reporting.TextBox();
            this.textBox7 = new Telerik.Reporting.TextBox();
            this.textBox98 = new Telerik.Reporting.TextBox();
            this.textBox10 = new Telerik.Reporting.TextBox();
            this.textBox99 = new Telerik.Reporting.TextBox();
            this.textBox12 = new Telerik.Reporting.TextBox();
            this.textBox100 = new Telerik.Reporting.TextBox();
            this.textBox14 = new Telerik.Reporting.TextBox();
            this.textBox85 = new Telerik.Reporting.TextBox();
            this.textBox15 = new Telerik.Reporting.TextBox();
            this.textBox86 = new Telerik.Reporting.TextBox();
            this.textBox20 = new Telerik.Reporting.TextBox();
            this.textBox88 = new Telerik.Reporting.TextBox();
            this.textBox22 = new Telerik.Reporting.TextBox();
            this.textBox89 = new Telerik.Reporting.TextBox();
            this.textBox24 = new Telerik.Reporting.TextBox();
            this.textBox90 = new Telerik.Reporting.TextBox();
            this.textBox26 = new Telerik.Reporting.TextBox();
            this.textBox91 = new Telerik.Reporting.TextBox();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.textBox60 = new Telerik.Reporting.TextBox();
            this.textBox61 = new Telerik.Reporting.TextBox();
            this.pageHeaderSection1 = new Telerik.Reporting.PageHeaderSection();
            this.tbxReportTitle = new Telerik.Reporting.TextBox();
            this.detail = new Telerik.Reporting.DetailSection();
            this.table4 = new Telerik.Reporting.Table();
            this.textBox81 = new Telerik.Reporting.TextBox();
            this.textBox8 = new Telerik.Reporting.TextBox();
            this.PartsPurchaseSettleBillDataSource = new Telerik.Reporting.ObjectDataSource();
            this.table1 = new Telerik.Reporting.Table();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.textBox9 = new Telerik.Reporting.TextBox();
            this.textBox11 = new Telerik.Reporting.TextBox();
            this.textBox13 = new Telerik.Reporting.TextBox();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.table2 = new Telerik.Reporting.Table();
            this.textBox17 = new Telerik.Reporting.TextBox();
            this.textBox18 = new Telerik.Reporting.TextBox();
            this.textBox21 = new Telerik.Reporting.TextBox();
            this.textBox23 = new Telerik.Reporting.TextBox();
            this.textBox25 = new Telerik.Reporting.TextBox();
            this.textBox27 = new Telerik.Reporting.TextBox();
            this.textBox29 = new Telerik.Reporting.TextBox();
            this.textBox30 = new Telerik.Reporting.TextBox();
            this.textBox36 = new Telerik.Reporting.TextBox();
            this.textBox28 = new Telerik.Reporting.TextBox();
            this.textBox31 = new Telerik.Reporting.TextBox();
            this.PartsInboundCheckBillDetailDataSource = new Telerik.Reporting.ObjectDataSource();
            this.pageFooterSection1 = new Telerik.Reporting.PageFooterSection();
            this.textBox79 = new Telerik.Reporting.TextBox();
            this.textBox78 = new Telerik.Reporting.TextBox();
            this.textBox76 = new Telerik.Reporting.TextBox();
            this.textBox67 = new Telerik.Reporting.TextBox();
            this.textBox77 = new Telerik.Reporting.TextBox();
            this.textBox80 = new Telerik.Reporting.TextBox();
            this.textBox82 = new Telerik.Reporting.TextBox();
            this.textBox83 = new Telerik.Reporting.TextBox();
            this.textBox84 = new Telerik.Reporting.TextBox();
            this.textBox111 = new Telerik.Reporting.TextBox();
            this.textBox112 = new Telerik.Reporting.TextBox();
            this.textBox113 = new Telerik.Reporting.TextBox();
            this.textBox114 = new Telerik.Reporting.TextBox();
            this.textBox115 = new Telerik.Reporting.TextBox();
            this.textBox116 = new Telerik.Reporting.TextBox();
            this.textBox117 = new Telerik.Reporting.TextBox();
            this.textBox118 = new Telerik.Reporting.TextBox();
            this.table5 = new Telerik.Reporting.Table();
            this.textBox62 = new Telerik.Reporting.TextBox();
            this.textBox63 = new Telerik.Reporting.TextBox();
            this.textBox64 = new Telerik.Reporting.TextBox();
            this.table6 = new Telerik.Reporting.Table();
            this.textBox70 = new Telerik.Reporting.TextBox();
            this.table7 = new Telerik.Reporting.Table();
            this.textBox71 = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // textBox1
            // 
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.1806797981262207D), Telerik.Reporting.Drawing.Unit.Cm(0.571500301361084D));
            this.textBox1.Style.Font.Bold = false;
            this.textBox1.Value = "单位：";
            // 
            // textBox95
            // 
            this.textBox95.Name = "textBox95";
            this.textBox95.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.1806797981262207D), Telerik.Reporting.Drawing.Unit.Cm(0.21166670322418213D));
            this.textBox95.Style.Font.Bold = true;
            this.textBox95.StyleName = "";
            // 
            // textBox2
            // 
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.5340924263000488D), Telerik.Reporting.Drawing.Unit.Cm(0.571500301361084D));
            this.textBox2.Value = "= Fields.PartsSupplierName";
            // 
            // textBox96
            // 
            this.textBox96.Name = "textBox96";
            this.textBox96.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.5340924263000488D), Telerik.Reporting.Drawing.Unit.Cm(0.21166670322418213D));
            this.textBox96.StyleName = "";
            // 
            // textBox3
            // 
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.6274447441101074D), Telerik.Reporting.Drawing.Unit.Cm(0.571500301361084D));
            this.textBox3.Style.Font.Bold = false;
            this.textBox3.Value = "结算单号:";
            // 
            // textBox97
            // 
            this.textBox97.Name = "textBox97";
            this.textBox97.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.6274447441101074D), Telerik.Reporting.Drawing.Unit.Cm(0.21166670322418213D));
            this.textBox97.Style.Font.Bold = true;
            this.textBox97.StyleName = "";
            // 
            // textBox7
            // 
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.2913737297058105D), Telerik.Reporting.Drawing.Unit.Cm(0.571500301361084D));
            this.textBox7.StyleName = "";
            this.textBox7.Value = "= Fields.Code";
            // 
            // textBox98
            // 
            this.textBox98.Name = "textBox98";
            this.textBox98.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.2913737297058105D), Telerik.Reporting.Drawing.Unit.Cm(0.21166670322418213D));
            this.textBox98.StyleName = "";
            // 
            // textBox10
            // 
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.7416632175445557D), Telerik.Reporting.Drawing.Unit.Cm(0.571500301361084D));
            this.textBox10.Style.Font.Bold = false;
            this.textBox10.StyleName = "";
            this.textBox10.Value = "结算金额（元）：";
            // 
            // textBox99
            // 
            this.textBox99.Name = "textBox99";
            this.textBox99.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.7416632175445557D), Telerik.Reporting.Drawing.Unit.Cm(0.21166670322418213D));
            this.textBox99.Style.Font.Bold = true;
            this.textBox99.StyleName = "";
            // 
            // textBox12
            // 
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.6245462894439697D), Telerik.Reporting.Drawing.Unit.Cm(0.571500301361084D));
            this.textBox12.StyleName = "";
            this.textBox12.Value = "= Fields.TotalSettlementAmount";
            // 
            // textBox100
            // 
            this.textBox100.Name = "textBox100";
            this.textBox100.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.6245462894439697D), Telerik.Reporting.Drawing.Unit.Cm(0.21166670322418213D));
            this.textBox100.StyleName = "";
            // 
            // textBox14
            // 
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.8993732929229736D), Telerik.Reporting.Drawing.Unit.Cm(0.5609167218208313D));
            this.textBox14.Style.Font.Bold = false;
            this.textBox14.Value = "出入库单编号";
            // 
            // textBox85
            // 
            this.textBox85.Name = "textBox85";
            this.textBox85.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.8993732929229736D), Telerik.Reporting.Drawing.Unit.Cm(0.2116667628288269D));
            this.textBox85.Style.Font.Bold = true;
            this.textBox85.StyleName = "";
            // 
            // textBox15
            // 
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.9138622283935547D), Telerik.Reporting.Drawing.Unit.Cm(0.5609167218208313D));
            this.textBox15.Style.Font.Bold = false;
            this.textBox15.Value = "供应商图号";
            // 
            // textBox86
            // 
            this.textBox86.Name = "textBox86";
            this.textBox86.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.9138622283935547D), Telerik.Reporting.Drawing.Unit.Cm(0.2116667628288269D));
            this.textBox86.Style.Font.Bold = true;
            this.textBox86.StyleName = "";
            // 
            // textBox20
            // 
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.3643436431884766D), Telerik.Reporting.Drawing.Unit.Cm(0.56091678142547607D));
            this.textBox20.Style.Font.Bold = false;
            this.textBox20.StyleName = "";
            this.textBox20.Value = "配件名称";
            // 
            // textBox88
            // 
            this.textBox88.Name = "textBox88";
            this.textBox88.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.3643436431884766D), Telerik.Reporting.Drawing.Unit.Cm(0.2116667777299881D));
            this.textBox88.Style.Font.Bold = true;
            this.textBox88.StyleName = "";
            // 
            // textBox22
            // 
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.0494420528411865D), Telerik.Reporting.Drawing.Unit.Cm(0.5609167218208313D));
            this.textBox22.Style.Font.Bold = false;
            this.textBox22.StyleName = "";
            this.textBox22.Value = "数量";
            // 
            // textBox89
            // 
            this.textBox89.Name = "textBox89";
            this.textBox89.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.0494420528411865D), Telerik.Reporting.Drawing.Unit.Cm(0.2116667628288269D));
            this.textBox89.Style.Font.Bold = true;
            this.textBox89.StyleName = "";
            // 
            // textBox24
            // 
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8018326759338379D), Telerik.Reporting.Drawing.Unit.Cm(0.5609167218208313D));
            this.textBox24.Style.Font.Bold = false;
            this.textBox24.StyleName = "";
            this.textBox24.Value = "合同价格";
            // 
            // textBox90
            // 
            this.textBox90.Name = "textBox90";
            this.textBox90.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8018326759338379D), Telerik.Reporting.Drawing.Unit.Cm(0.2116667628288269D));
            this.textBox90.Style.Font.Bold = true;
            this.textBox90.StyleName = "";
            // 
            // textBox26
            // 
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8506243228912354D), Telerik.Reporting.Drawing.Unit.Cm(0.5609167218208313D));
            this.textBox26.Style.Font.Bold = false;
            this.textBox26.StyleName = "";
            this.textBox26.Value = "合同金额";
            // 
            // textBox91
            // 
            this.textBox91.Name = "textBox91";
            this.textBox91.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8506243228912354D), Telerik.Reporting.Drawing.Unit.Cm(0.2116667628288269D));
            this.textBox91.Style.Font.Bold = true;
            this.textBox91.StyleName = "";
            // 
            // textBox6
            // 
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.28927776217460632D), Telerik.Reporting.Drawing.Unit.Cm(0.24341666698455811D));
            // 
            // textBox60
            // 
            this.textBox60.Name = "textBox60";
            this.textBox60.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.28927776217460632D), Telerik.Reporting.Drawing.Unit.Cm(0.24341666698455811D));
            // 
            // textBox61
            // 
            this.textBox61.Name = "textBox61";
            this.textBox61.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.28927776217460632D), Telerik.Reporting.Drawing.Unit.Cm(0.24341666698455811D));
            // 
            // pageHeaderSection1
            // 
            this.pageHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Cm(2.7000000476837158D);
            this.pageHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.tbxReportTitle});
            this.pageHeaderSection1.Name = "pageHeaderSection1";
            // 
            // tbxReportTitle
            // 
            this.tbxReportTitle.Anchoring = ((Telerik.Reporting.AnchoringStyles)((((Telerik.Reporting.AnchoringStyles.Top | Telerik.Reporting.AnchoringStyles.Bottom) 
            | Telerik.Reporting.AnchoringStyles.Left) 
            | Telerik.Reporting.AnchoringStyles.Right)));
            this.tbxReportTitle.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(0.80000007152557373D));
            this.tbxReportTitle.Name = "tbxReportTitle";
            this.tbxReportTitle.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(20.387229919433594D), Telerik.Reporting.Drawing.Unit.Cm(1.0999996662139893D));
            this.tbxReportTitle.Style.Font.Bold = true;
            this.tbxReportTitle.Style.Font.Italic = false;
            this.tbxReportTitle.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(16D);
            this.tbxReportTitle.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Solid;
            this.tbxReportTitle.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.tbxReportTitle.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.tbxReportTitle.Value = "= Fields.BranchName+\"配件采购结算明细\"";
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Cm(5D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.table4,
            this.table1,
            this.table2});
            this.detail.KeepTogether = false;
            this.detail.Name = "detail";
            // 
            // table4
            // 
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.4119188785552979D)));
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.5542495250701904D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.69990026950836182D)));
            this.table4.Body.SetCellContent(0, 0, this.textBox81);
            this.table4.Body.SetCellContent(0, 1, this.textBox8);
            this.table4.ColumnGroups.Add(tableGroup1);
            this.table4.ColumnGroups.Add(tableGroup2);
            this.table4.DataSource = this.PartsPurchaseSettleBillDataSource;
            this.table4.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox81,
            this.textBox8});
            this.table4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.920963287353516D), Telerik.Reporting.Drawing.Unit.Cm(0.00010012308484874666D));
            this.table4.Name = "table4";
            tableGroup3.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup3.Name = "DetailGroup";
            this.table4.RowGroups.Add(tableGroup3);
            this.table4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.9661684036254883D), Telerik.Reporting.Drawing.Unit.Cm(0.69990026950836182D));
            this.table4.Style.Font.Name = "宋体";
            this.table4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            // 
            // textBox81
            // 
            this.textBox81.Name = "textBox81";
            this.textBox81.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.411919116973877D), Telerik.Reporting.Drawing.Unit.Cm(0.69990026950836182D));
            this.textBox81.Style.Font.Bold = false;
            this.textBox81.Value = "品牌：";
            // 
            // textBox8
            // 
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.5542492866516113D), Telerik.Reporting.Drawing.Unit.Cm(0.69990026950836182D));
            this.textBox8.Value = "= Fields.PartsSalesCategoryName";
            // 
            // PartsPurchaseSettleBillDataSource
            // 
            this.PartsPurchaseSettleBillDataSource.DataMember = "GetPartsPurchaseSettleBillForPrint";
            this.PartsPurchaseSettleBillDataSource.DataSource = typeof(Sunlight.Silverlight.Dcs.Web.DcsDomainService);
            this.PartsPurchaseSettleBillDataSource.Name = "PartsPurchaseSettleBillDataSource";
            this.PartsPurchaseSettleBillDataSource.Parameters.AddRange(new Telerik.Reporting.ObjectDataSourceParameter[] {
            new Telerik.Reporting.ObjectDataSourceParameter("partsPurchaseSettleBillId", typeof(int), "=Parameters.partsPurchaseSettleBillId.Value")});
            // 
            // table1
            // 
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.1806802749633789D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(7.534092903137207D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.6274453401565552D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(4.2913727760314941D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.7416634559631348D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.6245467662811279D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.57149994373321533D)));
            this.table1.Body.SetCellContent(0, 0, this.textBox4);
            this.table1.Body.SetCellContent(0, 3, this.textBox9);
            this.table1.Body.SetCellContent(0, 4, this.textBox11);
            this.table1.Body.SetCellContent(0, 5, this.textBox13);
            this.table1.Body.SetCellContent(0, 1, this.textBox5, 1, 2);
            tableGroup5.Name = "Group4";
            tableGroup5.ReportItem = this.textBox1;
            tableGroup4.ChildGroups.Add(tableGroup5);
            tableGroup4.ReportItem = this.textBox95;
            tableGroup7.Name = "Group5";
            tableGroup7.ReportItem = this.textBox2;
            tableGroup6.ChildGroups.Add(tableGroup7);
            tableGroup6.ReportItem = this.textBox96;
            tableGroup9.Name = "Group6";
            tableGroup9.ReportItem = this.textBox3;
            tableGroup8.ChildGroups.Add(tableGroup9);
            tableGroup8.ReportItem = this.textBox97;
            tableGroup11.Name = "Group7";
            tableGroup11.ReportItem = this.textBox7;
            tableGroup10.ChildGroups.Add(tableGroup11);
            tableGroup10.Name = "Group1";
            tableGroup10.ReportItem = this.textBox98;
            tableGroup13.Name = "Group8";
            tableGroup13.ReportItem = this.textBox10;
            tableGroup12.ChildGroups.Add(tableGroup13);
            tableGroup12.Name = "Group2";
            tableGroup12.ReportItem = this.textBox99;
            tableGroup15.Name = "Group9";
            tableGroup15.ReportItem = this.textBox12;
            tableGroup14.ChildGroups.Add(tableGroup15);
            tableGroup14.Name = "Group3";
            tableGroup14.ReportItem = this.textBox100;
            this.table1.ColumnGroups.Add(tableGroup4);
            this.table1.ColumnGroups.Add(tableGroup6);
            this.table1.ColumnGroups.Add(tableGroup8);
            this.table1.ColumnGroups.Add(tableGroup10);
            this.table1.ColumnGroups.Add(tableGroup12);
            this.table1.ColumnGroups.Add(tableGroup14);
            this.table1.DataSource = this.PartsPurchaseSettleBillDataSource;
            this.table1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox4,
            this.textBox5,
            this.textBox9,
            this.textBox11,
            this.textBox13,
            this.textBox95,
            this.textBox1,
            this.textBox96,
            this.textBox2,
            this.textBox97,
            this.textBox3,
            this.textBox98,
            this.textBox7,
            this.textBox99,
            this.textBox10,
            this.textBox100,
            this.textBox12});
            this.table1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.9961594969499856E-05D), Telerik.Reporting.Drawing.Unit.Cm(0.85946625471115112D));
            this.table1.Name = "table1";
            tableGroup16.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup16.Name = "DetailGroup";
            this.table1.RowGroups.Add(tableGroup16);
            this.table1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.999801635742188D), Telerik.Reporting.Drawing.Unit.Cm(1.3546669483184815D));
            this.table1.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.table1.Style.Font.Name = "宋体";
            this.table1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            // 
            // textBox4
            // 
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.1806797981262207D), Telerik.Reporting.Drawing.Unit.Cm(0.57149994373321533D));
            this.textBox4.Style.Font.Bold = false;
            this.textBox4.Value = "备注：";
            // 
            // textBox9
            // 
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.2913737297058105D), Telerik.Reporting.Drawing.Unit.Cm(0.57149994373321533D));
            this.textBox9.StyleName = "";
            // 
            // textBox11
            // 
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.7416632175445557D), Telerik.Reporting.Drawing.Unit.Cm(0.57149994373321533D));
            this.textBox11.StyleName = "";
            // 
            // textBox13
            // 
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.6245462894439697D), Telerik.Reporting.Drawing.Unit.Cm(0.57149994373321533D));
            this.textBox13.StyleName = "";
            // 
            // textBox5
            // 
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.1615381240844727D), Telerik.Reporting.Drawing.Unit.Cm(0.57149994373321533D));
            this.textBox5.Value = "= Fields.Remark";
            // 
            // table2
            // 
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.8993747234344482D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.9138593673706055D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(4.3643441200256348D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.0494420528411865D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.8018324375152588D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.8506243228912354D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.85724949836730957D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.47083333134651184D)));
            this.table2.Body.SetCellContent(0, 0, this.textBox17);
            this.table2.Body.SetCellContent(0, 1, this.textBox18);
            this.table2.Body.SetCellContent(0, 2, this.textBox21);
            this.table2.Body.SetCellContent(0, 3, this.textBox23);
            this.table2.Body.SetCellContent(0, 4, this.textBox25);
            this.table2.Body.SetCellContent(0, 5, this.textBox27);
            this.table2.Body.SetCellContent(1, 1, this.textBox29);
            this.table2.Body.SetCellContent(1, 2, this.textBox30);
            this.table2.Body.SetCellContent(1, 0, this.textBox36);
            this.table2.Body.SetCellContent(1, 3, this.textBox28, 1, 2);
            this.table2.Body.SetCellContent(1, 5, this.textBox31);
            tableGroup18.Name = "Group9";
            tableGroup18.ReportItem = this.textBox14;
            tableGroup17.ChildGroups.Add(tableGroup18);
            tableGroup17.ReportItem = this.textBox85;
            tableGroup20.Name = "Group10";
            tableGroup20.ReportItem = this.textBox15;
            tableGroup19.ChildGroups.Add(tableGroup20);
            tableGroup19.ReportItem = this.textBox86;
            tableGroup22.Name = "Group11";
            tableGroup22.ReportItem = this.textBox20;
            tableGroup21.ChildGroups.Add(tableGroup22);
            tableGroup21.Name = "Group1";
            tableGroup21.ReportItem = this.textBox88;
            tableGroup24.Name = "Group12";
            tableGroup24.ReportItem = this.textBox22;
            tableGroup23.ChildGroups.Add(tableGroup24);
            tableGroup23.Name = "Group2";
            tableGroup23.ReportItem = this.textBox89;
            tableGroup26.Name = "Group13";
            tableGroup26.ReportItem = this.textBox24;
            tableGroup25.ChildGroups.Add(tableGroup26);
            tableGroup25.Name = "Group3";
            tableGroup25.ReportItem = this.textBox90;
            tableGroup28.Name = "Group14";
            tableGroup28.ReportItem = this.textBox26;
            tableGroup27.ChildGroups.Add(tableGroup28);
            tableGroup27.Name = "Group4";
            tableGroup27.ReportItem = this.textBox91;
            this.table2.ColumnGroups.Add(tableGroup17);
            this.table2.ColumnGroups.Add(tableGroup19);
            this.table2.ColumnGroups.Add(tableGroup21);
            this.table2.ColumnGroups.Add(tableGroup23);
            this.table2.ColumnGroups.Add(tableGroup25);
            this.table2.ColumnGroups.Add(tableGroup27);
            this.table2.ColumnHeadersPrintOnEveryPage = true;
            this.table2.DataSource = this.PartsInboundCheckBillDetailDataSource;
            this.table2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox17,
            this.textBox18,
            this.textBox21,
            this.textBox23,
            this.textBox25,
            this.textBox27,
            this.textBox36,
            this.textBox29,
            this.textBox30,
            this.textBox28,
            this.textBox31,
            this.textBox85,
            this.textBox14,
            this.textBox86,
            this.textBox15,
            this.textBox88,
            this.textBox20,
            this.textBox89,
            this.textBox22,
            this.textBox90,
            this.textBox24,
            this.textBox91,
            this.textBox26});
            this.table2.KeepTogether = false;
            this.table2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.01136881485581398D), Telerik.Reporting.Drawing.Unit.Cm(2.8000001907348633D));
            this.table2.Name = "table2";
            tableGroup30.Name = "Group7";
            tableGroup29.ChildGroups.Add(tableGroup30);
            tableGroup29.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup29.Name = "DetailGroup";
            tableGroup32.Name = "group1";
            tableGroup31.ChildGroups.Add(tableGroup32);
            tableGroup31.Name = "group";
            this.table2.RowGroups.Add(tableGroup29);
            this.table2.RowGroups.Add(tableGroup31);
            this.table2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.879476547241211D), Telerik.Reporting.Drawing.Unit.Cm(2.1006662845611572D));
            this.table2.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.table2.Style.Font.Name = "宋体";
            this.table2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            // 
            // textBox17
            // 
            this.textBox17.CanShrink = true;
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.8993740081787109D), Telerik.Reporting.Drawing.Unit.Cm(0.85724949836730957D));
            this.textBox17.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox17.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox17.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox17.Value = "= Fields.Code";
            // 
            // textBox18
            // 
            this.textBox18.CanShrink = true;
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.9138627052307129D), Telerik.Reporting.Drawing.Unit.Cm(0.85724949836730957D));
            this.textBox18.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox18.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox18.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox18.Value = "= Fields.SupplierPartCode";
            // 
            // textBox21
            // 
            this.textBox21.CanShrink = true;
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.3643431663513184D), Telerik.Reporting.Drawing.Unit.Cm(0.85724949836730957D));
            this.textBox21.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox21.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox21.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox21.StyleName = "";
            this.textBox21.Value = "= Fields.PartName";
            // 
            // textBox23
            // 
            this.textBox23.CanShrink = true;
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.0494420528411865D), Telerik.Reporting.Drawing.Unit.Cm(0.85724949836730957D));
            this.textBox23.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox23.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox23.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox23.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox23.StyleName = "";
            this.textBox23.Value = "= Fields.Quantity";
            // 
            // textBox25
            // 
            this.textBox25.CanShrink = true;
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8018326759338379D), Telerik.Reporting.Drawing.Unit.Cm(0.85724949836730957D));
            this.textBox25.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox25.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox25.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox25.StyleName = "";
            this.textBox25.Value = "= Fields.SettlementPrice";
            // 
            // textBox27
            // 
            this.textBox27.CanShrink = true;
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8506243228912354D), Telerik.Reporting.Drawing.Unit.Cm(0.85724949836730957D));
            this.textBox27.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox27.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox27.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox27.StyleName = "";
            this.textBox27.Value = "= Fields.SettlementPrice * Fields.Quantity";
            // 
            // textBox29
            // 
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.9138622283935547D), Telerik.Reporting.Drawing.Unit.Cm(0.47083339095115662D));
            this.textBox29.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox29.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox29.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox29.StyleName = "";
            // 
            // textBox30
            // 
            this.textBox30.Name = "textBox30";
            this.textBox30.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.3643431663513184D), Telerik.Reporting.Drawing.Unit.Cm(0.47083339095115662D));
            this.textBox30.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox30.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox30.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox30.StyleName = "";
            // 
            // textBox36
            // 
            this.textBox36.Name = "textBox36";
            this.textBox36.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.8993732929229736D), Telerik.Reporting.Drawing.Unit.Cm(0.47083339095115662D));
            this.textBox36.Style.Font.Bold = false;
            this.textBox36.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox36.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox36.Value = "合计：";
            // 
            // textBox28
            // 
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.8512749671936035D), Telerik.Reporting.Drawing.Unit.Cm(0.47083333134651184D));
            this.textBox28.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox28.Value = "= Sum(Fields.Quantity)";
            // 
            // textBox31
            // 
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8506243228912354D), Telerik.Reporting.Drawing.Unit.Cm(0.47083333134651184D));
            this.textBox31.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox31.StyleName = "";
            this.textBox31.Value = "= Sum(Fields.SettlementPrice * Fields.Quantity)";
            // 
            // PartsInboundCheckBillDetailDataSource
            // 
            this.PartsInboundCheckBillDetailDataSource.DataMember = "GetVirtualInAndOutBoundDetailByPurchaseSettleBillId";
            this.PartsInboundCheckBillDetailDataSource.DataSource = "Sunlight.Silverlight.Dcs.Web.DcsDomainService, Sunlight.Silverlight.Dcs.Web, Vers" +
    "ion=1.0.0.0, Culture=neutral, PublicKeyToken=null";
            this.PartsInboundCheckBillDetailDataSource.Name = "PartsInboundCheckBillDetailDataSource";
            this.PartsInboundCheckBillDetailDataSource.Parameters.AddRange(new Telerik.Reporting.ObjectDataSourceParameter[] {
            new Telerik.Reporting.ObjectDataSourceParameter("partsPurchaseSettleBillId", typeof(int), "=Parameters.partsPurchaseSettleBillId.Value")});
            // 
            // pageFooterSection1
            // 
            this.pageFooterSection1.Height = Telerik.Reporting.Drawing.Unit.Cm(3D);
            this.pageFooterSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox79,
            this.textBox78,
            this.textBox76,
            this.textBox67,
            this.textBox77,
            this.textBox80,
            this.textBox82,
            this.textBox83,
            this.textBox84,
            this.textBox111,
            this.textBox112,
            this.textBox113,
            this.textBox114,
            this.textBox115,
            this.textBox116,
            this.textBox117,
            this.textBox118});
            this.pageFooterSection1.Name = "pageFooterSection1";
            this.pageFooterSection1.Style.Font.Name = "宋体";
            this.pageFooterSection1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            // 
            // textBox79
            // 
            this.textBox79.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(9.9477132607717067E-05D));
            this.textBox79.Name = "textBox79";
            this.textBox79.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9000000953674316D), Telerik.Reporting.Drawing.Unit.Cm(0.49999919533729553D));
            this.textBox79.Style.Font.Bold = false;
            this.textBox79.Value = "制单单位：";
            // 
            // textBox78
            // 
            this.textBox78.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(0.60000020265579224D));
            this.textBox78.Name = "textBox78";
            this.textBox78.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9000000953674316D), Telerik.Reporting.Drawing.Unit.Cm(0.49999919533729553D));
            this.textBox78.Style.Font.Bold = false;
            this.textBox78.Value = "打印时间：";
            // 
            // textBox76
            // 
            this.textBox76.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(19.354597091674805D), Telerik.Reporting.Drawing.Unit.Cm(0.60000020265579224D));
            this.textBox76.Name = "textBox76";
            this.textBox76.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.53253436088562012D), Telerik.Reporting.Drawing.Unit.Cm(0.5000004768371582D));
            this.textBox76.Value = "页";
            // 
            // textBox67
            // 
            this.textBox67.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(18.464015960693359D), Telerik.Reporting.Drawing.Unit.Cm(0.60000020265579224D));
            this.textBox67.Name = "textBox67";
            this.textBox67.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.89038199186325073D), Telerik.Reporting.Drawing.Unit.Cm(0.5000004768371582D));
            this.textBox67.Value = "= PageCount";
            // 
            // textBox77
            // 
            this.textBox77.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(17.931282043457031D), Telerik.Reporting.Drawing.Unit.Cm(0.60000020265579224D));
            this.textBox77.Name = "textBox77";
            this.textBox77.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.53253436088562012D), Telerik.Reporting.Drawing.Unit.Cm(0.5000004768371582D));
            this.textBox77.Value = "共";
            // 
            // textBox80
            // 
            this.textBox80.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(17.398548126220703D), Telerik.Reporting.Drawing.Unit.Cm(0.60000020265579224D));
            this.textBox80.Name = "textBox80";
            this.textBox80.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.53253436088562012D), Telerik.Reporting.Drawing.Unit.Cm(0.5000004768371582D));
            this.textBox80.Value = "页";
            // 
            // textBox82
            // 
            this.textBox82.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(16.654523849487305D), Telerik.Reporting.Drawing.Unit.Cm(0.60000020265579224D));
            this.textBox82.Name = "textBox82";
            this.textBox82.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.743823766708374D), Telerik.Reporting.Drawing.Unit.Cm(0.5000004768371582D));
            this.textBox82.Value = "= PageNumber";
            // 
            // textBox83
            // 
            this.textBox83.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(16.121789932250977D), Telerik.Reporting.Drawing.Unit.Cm(0.60000020265579224D));
            this.textBox83.Name = "textBox83";
            this.textBox83.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.53253436088562012D), Telerik.Reporting.Drawing.Unit.Cm(0.5000004768371582D));
            this.textBox83.Value = "第";
            // 
            // textBox84
            // 
            this.textBox84.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.9602844715118408D), Telerik.Reporting.Drawing.Unit.Cm(9.9477132607717067E-05D));
            this.textBox84.Name = "textBox84";
            this.textBox84.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.169522762298584D), Telerik.Reporting.Drawing.Unit.Cm(0.49999919533729553D));
            this.textBox84.Value = "= Fields.BranchName";
            // 
            // textBox111
            // 
            this.textBox111.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(8.6031932830810547D), Telerik.Reporting.Drawing.Unit.Cm(9.9477132607717067E-05D));
            this.textBox111.Name = "textBox111";
            this.textBox111.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9943689107894898D), Telerik.Reporting.Drawing.Unit.Cm(0.5000004768371582D));
            this.textBox111.Value = "=Fields.CreatorName";
            // 
            // textBox112
            // 
            this.textBox112.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.9602844715118408D), Telerik.Reporting.Drawing.Unit.Cm(0.60000020265579224D));
            this.textBox112.Name = "textBox112";
            this.textBox112.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.8800230026245117D), Telerik.Reporting.Drawing.Unit.Cm(0.49999919533729553D));
            this.textBox112.Value = "= Now()";
            // 
            // textBox113
            // 
            this.textBox113.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(7.130007266998291D), Telerik.Reporting.Drawing.Unit.Cm(9.9477132607717067E-05D));
            this.textBox113.Name = "textBox113";
            this.textBox113.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.4729853868484497D), Telerik.Reporting.Drawing.Unit.Cm(0.5000004768371582D));
            this.textBox113.Style.Font.Bold = false;
            this.textBox113.Value = "制单人：";
            // 
            // textBox114
            // 
            this.textBox114.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.597762107849121D), Telerik.Reporting.Drawing.Unit.Cm(9.9477132607717067E-05D));
            this.textBox114.Name = "textBox114";
            this.textBox114.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.7917376756668091D), Telerik.Reporting.Drawing.Unit.Cm(0.49999919533729553D));
            this.textBox114.Style.Font.Bold = false;
            this.textBox114.Value = "制单时间：";
            // 
            // textBox115
            // 
            this.textBox115.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.389700889587402D), Telerik.Reporting.Drawing.Unit.Cm(9.9477132607717067E-05D));
            this.textBox115.Name = "textBox115";
            this.textBox115.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.11054801940918D), Telerik.Reporting.Drawing.Unit.Cm(0.5000004768371582D));
            this.textBox115.Value = "=Fields.CreateTime";
            // 
            // textBox116
            // 
            this.textBox116.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(16.500448226928711D), Telerik.Reporting.Drawing.Unit.Cm(9.9477132607717067E-05D));
            this.textBox116.Name = "textBox116";
            this.textBox116.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.5980002880096436D), Telerik.Reporting.Drawing.Unit.Cm(0.5000004768371582D));
            this.textBox116.Value = "打印人：";
            // 
            // textBox117
            // 
            this.textBox117.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(18.098649978637695D), Telerik.Reporting.Drawing.Unit.Cm(9.9477132607717067E-05D));
            this.textBox117.Name = "textBox117";
            this.textBox117.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9012508392333984D), Telerik.Reporting.Drawing.Unit.Cm(0.5000004768371582D));
            this.textBox117.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox117.Value = "= Parameters.UserName.Value";
            // 
            // textBox118
            // 
            this.textBox118.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(6.8405075073242188D), Telerik.Reporting.Drawing.Unit.Cm(0.60000020265579224D));
            this.textBox118.Name = "textBox118";
            this.textBox118.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.0408608913421631D), Telerik.Reporting.Drawing.Unit.Cm(0.5000004768371582D));
            this.textBox118.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox118.Value = "注：";
            // 
            // table5
            // 
            this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.28927779197692871D)));
            this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.28927779197692871D)));
            this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.28927779197692871D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.24341666698455811D)));
            this.table5.Body.SetCellContent(0, 0, this.textBox62);
            this.table5.Body.SetCellContent(0, 1, this.textBox63);
            this.table5.Body.SetCellContent(0, 2, this.textBox64);
            tableGroup33.ReportItem = this.textBox6;
            tableGroup34.ReportItem = this.textBox60;
            tableGroup35.ReportItem = this.textBox61;
            this.table5.ColumnGroups.Add(tableGroup33);
            this.table5.ColumnGroups.Add(tableGroup34);
            this.table5.ColumnGroups.Add(tableGroup35);
            this.table5.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox62,
            this.textBox63,
            this.textBox64,
            this.textBox6,
            this.textBox60,
            this.textBox61});
            this.table5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(7.7000002861022949D), Telerik.Reporting.Drawing.Unit.Cm(5.8000998497009277D));
            this.table5.Name = "table5";
            tableGroup36.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup36.Name = "DetailGroup";
            this.table5.RowGroups.Add(tableGroup36);
            this.table5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.86783331632614136D), Telerik.Reporting.Drawing.Unit.Cm(0.48683333396911621D));
            // 
            // textBox62
            // 
            this.textBox62.Name = "textBox62";
            this.textBox62.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.28927776217460632D), Telerik.Reporting.Drawing.Unit.Cm(0.24341666698455811D));
            // 
            // textBox63
            // 
            this.textBox63.Name = "textBox63";
            this.textBox63.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.28927776217460632D), Telerik.Reporting.Drawing.Unit.Cm(0.24341666698455811D));
            // 
            // textBox64
            // 
            this.textBox64.Name = "textBox64";
            this.textBox64.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.28927776217460632D), Telerik.Reporting.Drawing.Unit.Cm(0.24341666698455811D));
            // 
            // table6
            // 
            this.table6.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.68438875675201416D)));
            this.table6.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.47624999284744263D)));
            this.table6.Body.SetCellContent(0, 0, this.textBox70);
            this.table6.ColumnGroups.Add(tableGroup37);
            this.table6.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox70});
            this.table6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(7.9000000953674316D), Telerik.Reporting.Drawing.Unit.Cm(5.4000997543334961D));
            this.table6.Name = "table6";
            tableGroup38.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup38.Name = "DetailGroup";
            this.table6.RowGroups.Add(tableGroup38);
            this.table6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.68438875675201416D), Telerik.Reporting.Drawing.Unit.Cm(0.47624999284744263D));
            // 
            // textBox70
            // 
            this.textBox70.Name = "textBox70";
            this.textBox70.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.68438887596130371D), Telerik.Reporting.Drawing.Unit.Cm(0.47624999284744263D));
            // 
            // table7
            // 
            this.table7.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.399999737739563D)));
            this.table7.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.49999982118606567D)));
            this.table7.Body.SetCellContent(0, 0, this.textBox71);
            this.table7.ColumnGroups.Add(tableGroup39);
            this.table7.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox71});
            this.table7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.7999998927116394D), Telerik.Reporting.Drawing.Unit.Cm(4.9009666442871094D));
            this.table7.Name = "table7";
            tableGroup40.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup40.Name = "DetailGroup";
            this.table7.RowGroups.Add(tableGroup40);
            this.table7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.399999737739563D), Telerik.Reporting.Drawing.Unit.Cm(0.49999982118606567D));
            // 
            // textBox71
            // 
            this.textBox71.Name = "textBox71";
            this.textBox71.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.399999737739563D), Telerik.Reporting.Drawing.Unit.Cm(0.49999982118606567D));
            // 
            // ReportPartsPurchaseSettleBill
            // 
            this.DataSource = this.PartsPurchaseSettleBillDataSource;
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pageHeaderSection1,
            this.detail,
            this.pageFooterSection1});
            this.Name = "ReportPartsPurchaseSettleBill";
            this.PageSettings.Landscape = true;
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Pixel(25D), Telerik.Reporting.Drawing.Unit.Pixel(19D), Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Custom;
            this.PageSettings.PaperSize = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(130D), Telerik.Reporting.Drawing.Unit.Mm(220D));
            reportParameter1.Name = "UserName";
            reportParameter2.Name = "partsPurchaseSettleBillId";
            reportParameter2.Type = Telerik.Reporting.ReportParameterType.Integer;
            this.ReportParameters.Add(reportParameter1);
            this.ReportParameters.Add(reportParameter2);
            this.Style.BackgroundColor = System.Drawing.Color.White;
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
            this.Width = Telerik.Reporting.Drawing.Unit.Cm(20.5D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.PageHeaderSection pageHeaderSection1;
        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.PageFooterSection pageFooterSection1;
        private Telerik.Reporting.TextBox tbxReportTitle;
        private Telerik.Reporting.Table table4;
        private Telerik.Reporting.TextBox textBox81;
        private Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.Table table1;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.TextBox textBox9;
        private Telerik.Reporting.TextBox textBox11;
        private Telerik.Reporting.TextBox textBox13;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.TextBox textBox10;
        private Telerik.Reporting.TextBox textBox12;
        private Telerik.Reporting.Table table2;
        private Telerik.Reporting.TextBox textBox21;
        private Telerik.Reporting.TextBox textBox23;
        private Telerik.Reporting.TextBox textBox25;
        private Telerik.Reporting.TextBox textBox27;
        private Telerik.Reporting.TextBox textBox14;
        private Telerik.Reporting.TextBox textBox15;
        private Telerik.Reporting.TextBox textBox20;
        private Telerik.Reporting.TextBox textBox22;
        private Telerik.Reporting.TextBox textBox24;
        private Telerik.Reporting.TextBox textBox26;
        private Telerik.Reporting.ObjectDataSource PartsPurchaseSettleBillDataSource;
        private Telerik.Reporting.ObjectDataSource PartsInboundCheckBillDetailDataSource;
        private Telerik.Reporting.TextBox textBox18;
        private Telerik.Reporting.Table table7;
        private Telerik.Reporting.TextBox textBox71;
        private Telerik.Reporting.Table table5;
        private Telerik.Reporting.TextBox textBox62;
        private Telerik.Reporting.TextBox textBox63;
        private Telerik.Reporting.TextBox textBox64;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.TextBox textBox60;
        private Telerik.Reporting.TextBox textBox61;
        private Telerik.Reporting.Table table6;
        private Telerik.Reporting.TextBox textBox70;
        private Telerik.Reporting.TextBox textBox17;
        private Telerik.Reporting.TextBox textBox95;
        private Telerik.Reporting.TextBox textBox96;
        private Telerik.Reporting.TextBox textBox97;
        private Telerik.Reporting.TextBox textBox98;
        private Telerik.Reporting.TextBox textBox99;
        private Telerik.Reporting.TextBox textBox100;
        private Telerik.Reporting.TextBox textBox85;
        private Telerik.Reporting.TextBox textBox86;
        private Telerik.Reporting.TextBox textBox88;
        private Telerik.Reporting.TextBox textBox89;
        private Telerik.Reporting.TextBox textBox90;
        private Telerik.Reporting.TextBox textBox91;
        private Telerik.Reporting.TextBox textBox79;
        private Telerik.Reporting.TextBox textBox78;
        private Telerik.Reporting.TextBox textBox76;
        private Telerik.Reporting.TextBox textBox67;
        private Telerik.Reporting.TextBox textBox77;
        private Telerik.Reporting.TextBox textBox80;
        private Telerik.Reporting.TextBox textBox82;
        private Telerik.Reporting.TextBox textBox83;
        private Telerik.Reporting.TextBox textBox84;
        private Telerik.Reporting.TextBox textBox111;
        private Telerik.Reporting.TextBox textBox112;
        private Telerik.Reporting.TextBox textBox113;
        private Telerik.Reporting.TextBox textBox114;
        private Telerik.Reporting.TextBox textBox115;
        private Telerik.Reporting.TextBox textBox116;
        private Telerik.Reporting.TextBox textBox117;
        private Telerik.Reporting.TextBox textBox118;
        private Telerik.Reporting.TextBox textBox29;
        private Telerik.Reporting.TextBox textBox30;
        private Telerik.Reporting.TextBox textBox36;
        private Telerik.Reporting.TextBox textBox28;
        private Telerik.Reporting.TextBox textBox31;
    }
}