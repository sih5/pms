namespace Sunlight.Silverlight.Dcs.Web.Reporting {
    partial class ReportPartsRequisitionSettleBill {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup11 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup12 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.ReportParameter reportParameter1 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.textBox8 = new Telerik.Reporting.TextBox();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.textBox18 = new Telerik.Reporting.TextBox();
            this.textBox16 = new Telerik.Reporting.TextBox();
            this.textBox14 = new Telerik.Reporting.TextBox();
            this.textBox12 = new Telerik.Reporting.TextBox();
            this.textBox10 = new Telerik.Reporting.TextBox();
            this.detail = new Telerik.Reporting.DetailSection();
            this.table1 = new Telerik.Reporting.Table();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.textBox11 = new Telerik.Reporting.TextBox();
            this.textBox13 = new Telerik.Reporting.TextBox();
            this.textBox15 = new Telerik.Reporting.TextBox();
            this.textBox17 = new Telerik.Reporting.TextBox();
            this.textBox19 = new Telerik.Reporting.TextBox();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.textBox21 = new Telerik.Reporting.TextBox();
            this.textBox22 = new Telerik.Reporting.TextBox();
            this.textBox23 = new Telerik.Reporting.TextBox();
            this.textBox24 = new Telerik.Reporting.TextBox();
            this.textBox25 = new Telerik.Reporting.TextBox();
            this.textBox32 = new Telerik.Reporting.TextBox();
            this.textBox7 = new Telerik.Reporting.TextBox();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.PartsRequisitionSettleDetailDataSource = new Telerik.Reporting.ObjectDataSource();
            this.textBox9 = new Telerik.Reporting.TextBox();
            this.PartsRequisitionSettleBillDataSource = new Telerik.Reporting.ObjectDataSource();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // textBox2
            // 
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.269536018371582D), Telerik.Reporting.Drawing.Unit.Cm(0.60934466123580933D));
            this.textBox2.Value = "出入库单号";
            // 
            // textBox8
            // 
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2D), Telerik.Reporting.Drawing.Unit.Cm(0.60934466123580933D));
            this.textBox8.Value = "图号";
            // 
            // textBox3
            // 
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.881131649017334D), Telerik.Reporting.Drawing.Unit.Cm(0.60934466123580933D));
            this.textBox3.StyleName = "";
            this.textBox3.Value = "配件名称";
            // 
            // textBox18
            // 
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.2533552646636963D), Telerik.Reporting.Drawing.Unit.Cm(0.60934460163116455D));
            this.textBox18.StyleName = "";
            this.textBox18.Value = "数量";
            // 
            // textBox16
            // 
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.4259966611862183D), Telerik.Reporting.Drawing.Unit.Cm(0.60934466123580933D));
            this.textBox16.StyleName = "";
            this.textBox16.Value = "价格";
            // 
            // textBox14
            // 
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9232614040374756D), Telerik.Reporting.Drawing.Unit.Cm(0.60934460163116455D));
            this.textBox14.StyleName = "";
            this.textBox14.Value = "总金额";
            // 
            // textBox12
            // 
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.4563711881637573D), Telerik.Reporting.Drawing.Unit.Cm(0.60934466123580933D));
            this.textBox12.StyleName = "";
            this.textBox12.Value = "计划价";
            // 
            // textBox10
            // 
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.482499361038208D), Telerik.Reporting.Drawing.Unit.Cm(0.60934460163116455D));
            this.textBox10.StyleName = "";
            this.textBox10.Value = "计划价总金额";
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Cm(8.6799993515014648D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.table1,
            this.textBox9});
            this.detail.Name = "detail";
            // 
            // table1
            // 
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(4.26953649520874D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.9999992847442627D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(4.88113260269165D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.253353476524353D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.4259966611862183D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.9232598543167114D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.4563709497451782D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.4824972152709961D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.715317964553833D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(1.0013196468353272D)));
            this.table1.Body.SetCellContent(0, 0, this.textBox5);
            this.table1.Body.SetCellContent(0, 7, this.textBox11);
            this.table1.Body.SetCellContent(0, 6, this.textBox13);
            this.table1.Body.SetCellContent(0, 5, this.textBox15);
            this.table1.Body.SetCellContent(0, 4, this.textBox17);
            this.table1.Body.SetCellContent(0, 3, this.textBox19);
            this.table1.Body.SetCellContent(0, 2, this.textBox4);
            this.table1.Body.SetCellContent(1, 2, this.textBox21);
            this.table1.Body.SetCellContent(1, 3, this.textBox22);
            this.table1.Body.SetCellContent(1, 4, this.textBox23);
            this.table1.Body.SetCellContent(1, 5, this.textBox24);
            this.table1.Body.SetCellContent(1, 6, this.textBox25);
            this.table1.Body.SetCellContent(1, 7, this.textBox32);
            this.table1.Body.SetCellContent(1, 1, this.textBox7);
            this.table1.Body.SetCellContent(0, 1, this.textBox1);
            this.table1.Body.SetCellContent(1, 0, this.textBox6);
            tableGroup1.Name = "Group8";
            tableGroup1.ReportItem = this.textBox2;
            tableGroup2.Name = "group";
            tableGroup2.ReportItem = this.textBox8;
            tableGroup3.Name = "Group9";
            tableGroup3.ReportItem = this.textBox3;
            tableGroup4.Name = "Group10";
            tableGroup4.ReportItem = this.textBox18;
            tableGroup5.Name = "Group11";
            tableGroup5.ReportItem = this.textBox16;
            tableGroup6.Name = "Group12";
            tableGroup6.ReportItem = this.textBox14;
            tableGroup7.Name = "Group13";
            tableGroup7.ReportItem = this.textBox12;
            tableGroup8.Name = "Group14";
            tableGroup8.ReportItem = this.textBox10;
            this.table1.ColumnGroups.Add(tableGroup1);
            this.table1.ColumnGroups.Add(tableGroup2);
            this.table1.ColumnGroups.Add(tableGroup3);
            this.table1.ColumnGroups.Add(tableGroup4);
            this.table1.ColumnGroups.Add(tableGroup5);
            this.table1.ColumnGroups.Add(tableGroup6);
            this.table1.ColumnGroups.Add(tableGroup7);
            this.table1.ColumnGroups.Add(tableGroup8);
            this.table1.DataSource = this.PartsRequisitionSettleDetailDataSource;
            this.table1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox5,
            this.textBox11,
            this.textBox13,
            this.textBox15,
            this.textBox17,
            this.textBox19,
            this.textBox4,
            this.textBox21,
            this.textBox22,
            this.textBox23,
            this.textBox24,
            this.textBox25,
            this.textBox32,
            this.textBox7,
            this.textBox1,
            this.textBox6,
            this.textBox2,
            this.textBox8,
            this.textBox3,
            this.textBox18,
            this.textBox16,
            this.textBox14,
            this.textBox12,
            this.textBox10});
            this.table1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.27561444044113159D), Telerik.Reporting.Drawing.Unit.Cm(3.2000000476837158D));
            this.table1.Name = "table1";
            tableGroup10.Name = "Group1";
            tableGroup9.ChildGroups.Add(tableGroup10);
            tableGroup9.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup9.Name = "DetailGroup";
            tableGroup12.Name = "Group3";
            tableGroup11.ChildGroups.Add(tableGroup12);
            tableGroup11.Name = "Group2";
            this.table1.RowGroups.Add(tableGroup9);
            this.table1.RowGroups.Add(tableGroup11);
            this.table1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.692146301269531D), Telerik.Reporting.Drawing.Unit.Cm(2.3259823322296143D));
            // 
            // textBox5
            // 
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.269536018371582D), Telerik.Reporting.Drawing.Unit.Cm(0.71531802415847778D));
            this.textBox5.Value = "= Fields.SourceCode";
            // 
            // textBox11
            // 
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.482499361038208D), Telerik.Reporting.Drawing.Unit.Cm(0.71531802415847778D));
            this.textBox11.StyleName = "";
            this.textBox11.Value = "= Fields.Quantity * Fields.CostPrice";
            // 
            // textBox13
            // 
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.4563711881637573D), Telerik.Reporting.Drawing.Unit.Cm(0.71531802415847778D));
            this.textBox13.StyleName = "";
            this.textBox13.Value = "= Fields.CostPrice";
            // 
            // textBox15
            // 
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9232614040374756D), Telerik.Reporting.Drawing.Unit.Cm(0.71531802415847778D));
            this.textBox15.StyleName = "";
            this.textBox15.Value = "= Fields.Quantity * Fields.SettlementPrice";
            // 
            // textBox17
            // 
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.4259966611862183D), Telerik.Reporting.Drawing.Unit.Cm(0.71531802415847778D));
            this.textBox17.StyleName = "";
            this.textBox17.Value = "= Fields.SettlementPrice";
            // 
            // textBox19
            // 
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.2533552646636963D), Telerik.Reporting.Drawing.Unit.Cm(0.71531802415847778D));
            this.textBox19.StyleName = "";
            this.textBox19.Value = "= Fields.Quantity";
            // 
            // textBox4
            // 
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.881131649017334D), Telerik.Reporting.Drawing.Unit.Cm(0.71531802415847778D));
            this.textBox4.StyleName = "";
            this.textBox4.Value = "= Fields.SparePartName";
            // 
            // textBox21
            // 
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.881131649017334D), Telerik.Reporting.Drawing.Unit.Cm(1.0013197660446167D));
            this.textBox21.StyleName = "";
            // 
            // textBox22
            // 
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.2533552646636963D), Telerik.Reporting.Drawing.Unit.Cm(1.0013197660446167D));
            this.textBox22.StyleName = "";
            // 
            // textBox23
            // 
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.4259966611862183D), Telerik.Reporting.Drawing.Unit.Cm(1.0013197660446167D));
            this.textBox23.StyleName = "";
            // 
            // textBox24
            // 
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9232614040374756D), Telerik.Reporting.Drawing.Unit.Cm(1.0013197660446167D));
            this.textBox24.StyleName = "";
            this.textBox24.Value = "= Sum(Fields.Quantity * Fields.SettlementPrice)";
            // 
            // textBox25
            // 
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.4563711881637573D), Telerik.Reporting.Drawing.Unit.Cm(1.0013197660446167D));
            this.textBox25.StyleName = "";
            // 
            // textBox32
            // 
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.482499361038208D), Telerik.Reporting.Drawing.Unit.Cm(1.0013197660446167D));
            this.textBox32.StyleName = "";
            this.textBox32.Value = "= Sum(Fields.Quantity * Fields.CostPrice)";
            // 
            // textBox7
            // 
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2D), Telerik.Reporting.Drawing.Unit.Cm(1.0013197660446167D));
            this.textBox7.StyleName = "";
            // 
            // textBox1
            // 
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2D), Telerik.Reporting.Drawing.Unit.Cm(0.71531802415847778D));
            this.textBox1.Value = "= Fields.SparePartCode";
            // 
            // textBox6
            // 
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.269536018371582D), Telerik.Reporting.Drawing.Unit.Cm(1.0013197660446167D));
            this.textBox6.StyleName = "";
            this.textBox6.Value = "合计：";
            // 
            // PartsRequisitionSettleDetailDataSource
            // 
            this.PartsRequisitionSettleDetailDataSource.DataMember = "GetPartsRequisitionSettleDetailForPrint";
            this.PartsRequisitionSettleDetailDataSource.DataSource = typeof(Sunlight.Silverlight.Dcs.Web.DcsDomainService);
            this.PartsRequisitionSettleDetailDataSource.Name = "PartsRequisitionSettleDetailDataSource";
            this.PartsRequisitionSettleDetailDataSource.Parameters.AddRange(new Telerik.Reporting.ObjectDataSourceParameter[] {
            new Telerik.Reporting.ObjectDataSourceParameter("PartsRequisitionSettleBillId", typeof(int), "=Parameters.paraPartsRequisitionSettleBillId.Value")});
            // 
            // textBox9
            // 
            this.textBox9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.5D), Telerik.Reporting.Drawing.Unit.Cm(0.7999998927116394D));
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.19999885559082D), Telerik.Reporting.Drawing.Unit.Cm(1.1000000238418579D));
            this.textBox9.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox9.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox9.Value = "= Fields.BranchName+\"（领用）内部结算单\"+Fields.Code";
            // 
            // PartsRequisitionSettleBillDataSource
            // 
            this.PartsRequisitionSettleBillDataSource.DataMember = "GetPartsRequisitionSettleBillPrint";
            this.PartsRequisitionSettleBillDataSource.DataSource = typeof(Sunlight.Silverlight.Dcs.Web.DcsDomainService);
            this.PartsRequisitionSettleBillDataSource.Name = "PartsRequisitionSettleBillDataSource";
            this.PartsRequisitionSettleBillDataSource.Parameters.AddRange(new Telerik.Reporting.ObjectDataSourceParameter[] {
            new Telerik.Reporting.ObjectDataSourceParameter("PartsRequisitionSettleBillId", typeof(int), "=Parameters.paraPartsRequisitionSettleBillId.Value")});
            // 
            // ReportPartsRequisitionSettleBill
            // 
            this.DataSource = this.PartsRequisitionSettleBillDataSource;
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.detail});
            this.Name = "ReportPartsRequisitionSettleBill";
            this.PageSettings.Landscape = true;
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Pixel(25D), Telerik.Reporting.Drawing.Unit.Pixel(19D), Telerik.Reporting.Drawing.Unit.Mm(9D), Telerik.Reporting.Drawing.Unit.Mm(9D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Custom;
            this.PageSettings.PaperSize = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(14D), Telerik.Reporting.Drawing.Unit.Cm(22D));
            reportParameter1.Name = "paraPartsRequisitionSettleBillId";
            reportParameter1.Type = Telerik.Reporting.ReportParameterType.Integer;
            reportParameter1.Value = "";
            this.ReportParameters.Add(reportParameter1);
            this.Style.BackgroundColor = System.Drawing.Color.White;
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
            this.Width = Telerik.Reporting.Drawing.Unit.Cm(20D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.Table table1;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.TextBox textBox11;
        private Telerik.Reporting.TextBox textBox13;
        private Telerik.Reporting.TextBox textBox15;
        private Telerik.Reporting.TextBox textBox17;
        private Telerik.Reporting.TextBox textBox19;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox18;
        private Telerik.Reporting.TextBox textBox16;
        private Telerik.Reporting.TextBox textBox14;
        private Telerik.Reporting.TextBox textBox12;
        private Telerik.Reporting.TextBox textBox10;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.ObjectDataSource PartsRequisitionSettleDetailDataSource;
        private Telerik.Reporting.ObjectDataSource PartsRequisitionSettleBillDataSource;
        private Telerik.Reporting.TextBox textBox21;
        private Telerik.Reporting.TextBox textBox22;
        private Telerik.Reporting.TextBox textBox23;
        private Telerik.Reporting.TextBox textBox24;
        private Telerik.Reporting.TextBox textBox25;
        private Telerik.Reporting.TextBox textBox32;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.TextBox textBox9;
    }
}