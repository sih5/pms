﻿namespace Sunlight.Silverlight.Dcs.Web.Reporting {
    partial class ReportPartsSalesSettlement {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup11 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup12 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup13 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup14 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup15 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup16 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup17 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup18 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup19 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup20 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup21 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup22 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.ReportParameter reportParameter1 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter2 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            this.textBox19 = new Telerik.Reporting.TextBox();
            this.textBox101 = new Telerik.Reporting.TextBox();
            this.textBox89 = new Telerik.Reporting.TextBox();
            this.textBox49 = new Telerik.Reporting.TextBox();
            this.pageHeaderSection1 = new Telerik.Reporting.PageHeaderSection();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.detail = new Telerik.Reporting.DetailSection();
            this.table1 = new Telerik.Reporting.Table();
            this.textBox18 = new Telerik.Reporting.TextBox();
            this.textBox24 = new Telerik.Reporting.TextBox();
            this.textBox25 = new Telerik.Reporting.TextBox();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.textBox7 = new Telerik.Reporting.TextBox();
            this.textBox9 = new Telerik.Reporting.TextBox();
            this.textBox11 = new Telerik.Reporting.TextBox();
            this.textBox15 = new Telerik.Reporting.TextBox();
            this.textBox17 = new Telerik.Reporting.TextBox();
            this.textBox26 = new Telerik.Reporting.TextBox();
            this.textBox27 = new Telerik.Reporting.TextBox();
            this.textBox28 = new Telerik.Reporting.TextBox();
            this.textBox29 = new Telerik.Reporting.TextBox();
            this.textBox30 = new Telerik.Reporting.TextBox();
            this.textBox32 = new Telerik.Reporting.TextBox();
            this.textBox8 = new Telerik.Reporting.TextBox();
            this.textBox10 = new Telerik.Reporting.TextBox();
            this.textBox34 = new Telerik.Reporting.TextBox();
            this.textBox43 = new Telerik.Reporting.TextBox();
            this.textBox46 = new Telerik.Reporting.TextBox();
            this.textBox48 = new Telerik.Reporting.TextBox();
            this.textBox51 = new Telerik.Reporting.TextBox();
            this.textBox52 = new Telerik.Reporting.TextBox();
            this.textBox53 = new Telerik.Reporting.TextBox();
            this.textBox56 = new Telerik.Reporting.TextBox();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.textBox57 = new Telerik.Reporting.TextBox();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.textBox47 = new Telerik.Reporting.TextBox();
            this.textBox42 = new Telerik.Reporting.TextBox();
            this.textBox61 = new Telerik.Reporting.TextBox();
            this.textBox20 = new Telerik.Reporting.TextBox();
            this.textBox59 = new Telerik.Reporting.TextBox();
            this.textBox16 = new Telerik.Reporting.TextBox();
            this.textBox36 = new Telerik.Reporting.TextBox();
            this.textBox33 = new Telerik.Reporting.TextBox();
            this.textBox14 = new Telerik.Reporting.TextBox();
            this.textBox58 = new Telerik.Reporting.TextBox();
            this.PartsSalesSettlementDataSource = new Telerik.Reporting.ObjectDataSource();
            this.table4 = new Telerik.Reporting.Table();
            this.textBox35 = new Telerik.Reporting.TextBox();
            this.textBox54 = new Telerik.Reporting.TextBox();
            this.textBox93 = new Telerik.Reporting.TextBox();
            this.textBox102 = new Telerik.Reporting.TextBox();
            this.PartsSalesSettlementDetailDataSource = new Telerik.Reporting.ObjectDataSource();
            this.pageFooterSection1 = new Telerik.Reporting.PageFooterSection();
            this.textBox12 = new Telerik.Reporting.TextBox();
            this.textBox13 = new Telerik.Reporting.TextBox();
            this.textBox21 = new Telerik.Reporting.TextBox();
            this.textBox22 = new Telerik.Reporting.TextBox();
            this.textBox23 = new Telerik.Reporting.TextBox();
            this.textBox31 = new Telerik.Reporting.TextBox();
            this.textBox39 = new Telerik.Reporting.TextBox();
            this.textBox40 = new Telerik.Reporting.TextBox();
            this.textBox41 = new Telerik.Reporting.TextBox();
            this.textBox50 = new Telerik.Reporting.TextBox();
            this.textBox60 = new Telerik.Reporting.TextBox();
            this.textBox62 = new Telerik.Reporting.TextBox();
            this.textBox63 = new Telerik.Reporting.TextBox();
            this.textBox64 = new Telerik.Reporting.TextBox();
            this.textBox65 = new Telerik.Reporting.TextBox();
            this.textBox66 = new Telerik.Reporting.TextBox();
            this.textBox67 = new Telerik.Reporting.TextBox();
            this.textBox68 = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // textBox19
            // 
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.1438350677490234D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox19.Style.Font.Bold = false;
            this.textBox19.Value = "出入库单编号";
            // 
            // textBox101
            // 
            this.textBox101.Name = "textBox101";
            this.textBox101.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.7158591747283936D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox101.Style.Font.Bold = false;
            this.textBox101.StyleName = "";
            this.textBox101.Value = "出入库金额";
            // 
            // textBox89
            // 
            this.textBox89.Name = "textBox89";
            this.textBox89.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.3599998950958252D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox89.Style.Font.Bold = false;
            this.textBox89.StyleName = "";
            this.textBox89.Value = "仓库编号";
            // 
            // textBox49
            // 
            this.textBox49.Name = "textBox49";
            this.textBox49.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.7700004577636719D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox49.Style.Font.Bold = false;
            this.textBox49.StyleName = "";
            this.textBox49.Value = "仓库名称";
            // 
            // pageHeaderSection1
            // 
            this.pageHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Cm(2D);
            this.pageHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox1});
            this.pageHeaderSection1.Name = "pageHeaderSection1";
            // 
            // textBox1
            // 
            this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.0045830952003598213D), Telerik.Reporting.Drawing.Unit.Cm(0.99989980459213257D));
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.9851131439209D), Telerik.Reporting.Drawing.Unit.Cm(1D));
            this.textBox1.Style.Font.Bold = true;
            this.textBox1.Style.Font.Name = "宋体";
            this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(15D);
            this.textBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox1.Value = "{Fields.SalesCompanyName}销售结算明细";
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Cm(3.9002001285552979D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.table1,
            this.table4});
            this.detail.Name = "detail";
            this.detail.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            // 
            // table1
            // 
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.4115715026855469D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.0105212926864624D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.0105212926864624D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.0105212926864624D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.0315639972686768D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.0210421085357666D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.8111436367034912D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.1172428131103516D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.3526259660720825D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.1922028064727783D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.0210421085357666D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.5D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.5D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.5D)));
            this.table1.Body.SetCellContent(4, 0, this.textBox18);
            this.table1.Body.SetCellContent(4, 6, this.textBox24);
            this.table1.Body.SetCellContent(4, 10, this.textBox25);
            this.table1.Body.SetCellContent(1, 0, this.textBox5);
            this.table1.Body.SetCellContent(1, 2, this.textBox6);
            this.table1.Body.SetCellContent(1, 3, this.textBox7);
            this.table1.Body.SetCellContent(1, 4, this.textBox9);
            this.table1.Body.SetCellContent(1, 5, this.textBox11);
            this.table1.Body.SetCellContent(1, 6, this.textBox15);
            this.table1.Body.SetCellContent(1, 10, this.textBox17);
            this.table1.Body.SetCellContent(0, 0, this.textBox26);
            this.table1.Body.SetCellContent(0, 2, this.textBox27);
            this.table1.Body.SetCellContent(0, 3, this.textBox28);
            this.table1.Body.SetCellContent(0, 4, this.textBox29);
            this.table1.Body.SetCellContent(0, 5, this.textBox30);
            this.table1.Body.SetCellContent(0, 6, this.textBox32);
            this.table1.Body.SetCellContent(3, 4, this.textBox8);
            this.table1.Body.SetCellContent(3, 5, this.textBox10);
            this.table1.Body.SetCellContent(2, 0, this.textBox34);
            this.table1.Body.SetCellContent(1, 9, this.textBox43);
            this.table1.Body.SetCellContent(4, 9, this.textBox46);
            this.table1.Body.SetCellContent(1, 8, this.textBox48);
            this.table1.Body.SetCellContent(4, 8, this.textBox51);
            this.table1.Body.SetCellContent(0, 7, this.textBox52);
            this.table1.Body.SetCellContent(1, 7, this.textBox53);
            this.table1.Body.SetCellContent(4, 7, this.textBox56);
            this.table1.Body.SetCellContent(0, 1, this.textBox3);
            this.table1.Body.SetCellContent(1, 1, this.textBox57);
            this.table1.Body.SetCellContent(3, 0, this.textBox2, 1, 2);
            this.table1.Body.SetCellContent(0, 8, this.textBox47);
            this.table1.Body.SetCellContent(0, 9, this.textBox42, 1, 2);
            this.table1.Body.SetCellContent(3, 2, this.textBox61, 1, 2);
            this.table1.Body.SetCellContent(3, 6, this.textBox20);
            this.table1.Body.SetCellContent(4, 1, this.textBox59, 1, 5);
            this.table1.Body.SetCellContent(2, 7, this.textBox16);
            this.table1.Body.SetCellContent(3, 7, this.textBox36);
            this.table1.Body.SetCellContent(3, 8, this.textBox33, 1, 3);
            this.table1.Body.SetCellContent(2, 8, this.textBox14, 1, 3);
            this.table1.Body.SetCellContent(2, 1, this.textBox58, 1, 6);
            tableGroup2.Name = "Group15";
            tableGroup4.Name = "Group6";
            tableGroup5.Name = "Group5";
            tableGroup6.Name = "Group4";
            tableGroup7.Name = "Group2";
            tableGroup8.Name = "Group14";
            tableGroup9.Name = "Group13";
            tableGroup10.Name = "Group12";
            tableGroup11.Name = "Group1";
            this.table1.ColumnGroups.Add(tableGroup1);
            this.table1.ColumnGroups.Add(tableGroup2);
            this.table1.ColumnGroups.Add(tableGroup3);
            this.table1.ColumnGroups.Add(tableGroup4);
            this.table1.ColumnGroups.Add(tableGroup5);
            this.table1.ColumnGroups.Add(tableGroup6);
            this.table1.ColumnGroups.Add(tableGroup7);
            this.table1.ColumnGroups.Add(tableGroup8);
            this.table1.ColumnGroups.Add(tableGroup9);
            this.table1.ColumnGroups.Add(tableGroup10);
            this.table1.ColumnGroups.Add(tableGroup11);
            this.table1.DataSource = this.PartsSalesSettlementDataSource;
            this.table1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox18,
            this.textBox24,
            this.textBox25,
            this.textBox5,
            this.textBox6,
            this.textBox7,
            this.textBox9,
            this.textBox11,
            this.textBox15,
            this.textBox17,
            this.textBox26,
            this.textBox27,
            this.textBox28,
            this.textBox29,
            this.textBox30,
            this.textBox32,
            this.textBox8,
            this.textBox10,
            this.textBox34,
            this.textBox43,
            this.textBox46,
            this.textBox48,
            this.textBox51,
            this.textBox52,
            this.textBox53,
            this.textBox56,
            this.textBox3,
            this.textBox57,
            this.textBox2,
            this.textBox47,
            this.textBox42,
            this.textBox61,
            this.textBox20,
            this.textBox59,
            this.textBox16,
            this.textBox36,
            this.textBox33,
            this.textBox14,
            this.textBox58});
            this.table1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.00010012308484874666D), Telerik.Reporting.Drawing.Unit.Cm(0.40000000596046448D));
            this.table1.Name = "table1";
            tableGroup13.Name = "Group9";
            tableGroup14.Name = "Group7";
            tableGroup15.Name = "Group11";
            tableGroup16.Name = "Group10";
            tableGroup17.Name = "Group8";
            tableGroup12.ChildGroups.Add(tableGroup13);
            tableGroup12.ChildGroups.Add(tableGroup14);
            tableGroup12.ChildGroups.Add(tableGroup15);
            tableGroup12.ChildGroups.Add(tableGroup16);
            tableGroup12.ChildGroups.Add(tableGroup17);
            tableGroup12.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup12.Name = "DetailGroup";
            this.table1.RowGroups.Add(tableGroup12);
            this.table1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.989999771118164D), Telerik.Reporting.Drawing.Unit.Cm(2.5D));
            this.table1.Style.Font.Name = "宋体";
            this.table1.Style.Font.Strikeout = false;
            this.table1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            // 
            // textBox18
            // 
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.4115713834762573D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox18.Style.Font.Bold = false;
            this.textBox18.StyleName = "";
            this.textBox18.Value = "备注：";
            // 
            // textBox24
            // 
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8111438751220703D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox24.StyleName = "";
            // 
            // textBox25
            // 
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.0210423469543457D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox25.StyleName = "";
            // 
            // textBox5
            // 
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.4115713834762573D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox5.StyleName = "";
            // 
            // textBox6
            // 
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.010521411895752D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox6.StyleName = "";
            // 
            // textBox7
            // 
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.010521411895752D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox7.StyleName = "";
            // 
            // textBox9
            // 
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.0315642356872559D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox9.StyleName = "";
            // 
            // textBox11
            // 
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.0210423469543457D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox11.StyleName = "";
            // 
            // textBox15
            // 
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8111438751220703D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox15.StyleName = "";
            // 
            // textBox17
            // 
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.0210423469543457D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox17.StyleName = "";
            // 
            // textBox26
            // 
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.4115713834762573D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox26.StyleName = "";
            // 
            // textBox27
            // 
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.010521411895752D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox27.StyleName = "";
            // 
            // textBox28
            // 
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.010521411895752D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox28.StyleName = "";
            // 
            // textBox29
            // 
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.0315642356872559D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox29.StyleName = "";
            // 
            // textBox30
            // 
            this.textBox30.Name = "textBox30";
            this.textBox30.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.0210423469543457D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox30.StyleName = "";
            // 
            // textBox32
            // 
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8111438751220703D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox32.StyleName = "";
            // 
            // textBox8
            // 
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.0315642356872559D), Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D));
            this.textBox8.Style.Font.Bold = false;
            this.textBox8.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox8.StyleName = "";
            this.textBox8.Value = "返利（元）：";
            // 
            // textBox10
            // 
            this.textBox10.Format = "{0:N2}";
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.0210423469543457D), Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D));
            this.textBox10.StyleName = "";
            this.textBox10.Value = "= Fields.RebateAmount";
            // 
            // textBox34
            // 
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.4115713834762573D), Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D));
            this.textBox34.Style.Font.Bold = false;
            this.textBox34.StyleName = "";
            this.textBox34.Value = "单位：";
            // 
            // textBox43
            // 
            this.textBox43.Name = "textBox43";
            this.textBox43.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.1922028064727783D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox43.StyleName = "";
            // 
            // textBox46
            // 
            this.textBox46.Name = "textBox46";
            this.textBox46.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.1922028064727783D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox46.StyleName = "";
            // 
            // textBox48
            // 
            this.textBox48.Name = "textBox48";
            this.textBox48.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.3526260852813721D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox48.StyleName = "";
            // 
            // textBox51
            // 
            this.textBox51.Name = "textBox51";
            this.textBox51.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.3526260852813721D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox51.StyleName = "";
            // 
            // textBox52
            // 
            this.textBox52.Name = "textBox52";
            this.textBox52.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1172428131103516D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox52.StyleName = "";
            // 
            // textBox53
            // 
            this.textBox53.Name = "textBox53";
            this.textBox53.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1172428131103516D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox53.StyleName = "";
            // 
            // textBox56
            // 
            this.textBox56.Name = "textBox56";
            this.textBox56.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1172428131103516D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox56.StyleName = "";
            // 
            // textBox3
            // 
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.010521411895752D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox3.StyleName = "";
            // 
            // textBox57
            // 
            this.textBox57.Name = "textBox57";
            this.textBox57.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.010521411895752D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox57.StyleName = "";
            // 
            // textBox2
            // 
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.4220926761627197D), Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D));
            this.textBox2.Style.Font.Bold = false;
            this.textBox2.StyleName = "";
            this.textBox2.Value = "合计（元）：";
            // 
            // textBox47
            // 
            this.textBox47.Name = "textBox47";
            this.textBox47.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.3526260852813721D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox47.Style.Font.Bold = false;
            this.textBox47.StyleName = "";
            this.textBox47.Value = "品牌：";
            // 
            // textBox42
            // 
            this.textBox42.Name = "textBox42";
            this.textBox42.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.2132449150085449D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox42.StyleName = "";
            this.textBox42.Value = "= Fields.PartsSalesCategoryName";
            // 
            // textBox61
            // 
            this.textBox61.Format = "{0:N2}";
            this.textBox61.Name = "textBox61";
            this.textBox61.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.0210428237915039D), Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D));
            this.textBox61.StyleName = "";
            this.textBox61.Value = "= Fields.RebateAmount + Fields.TotalSettlementAmount";
            // 
            // textBox20
            // 
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8111438751220703D), Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D));
            this.textBox20.Style.Font.Bold = true;
            this.textBox20.StyleName = "";
            // 
            // textBox59
            // 
            this.textBox59.Name = "textBox59";
            this.textBox59.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.0841703414917D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox59.StyleName = "";
            this.textBox59.Value = "= Fields.Remark";
            // 
            // textBox16
            // 
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1172428131103516D), Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D));
            this.textBox16.Style.Font.Bold = false;
            this.textBox16.StyleName = "";
            this.textBox16.Value = "结算单号：";
            // 
            // textBox36
            // 
            this.textBox36.Name = "textBox36";
            this.textBox36.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1172428131103516D), Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D));
            this.textBox36.Style.Font.Bold = false;
            this.textBox36.StyleName = "";
            this.textBox36.Value = "结算金额（元）：";
            // 
            // textBox33
            // 
            this.textBox33.Format = "{0:N2}";
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.5658712387084961D), Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D));
            this.textBox33.StyleName = "";
            this.textBox33.Value = "= Fields.TotalSettlementAmount";
            // 
            // textBox14
            // 
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.5658712387084961D), Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D));
            this.textBox14.StyleName = "";
            this.textBox14.Value = "= Fields.Code";
            // 
            // textBox58
            // 
            this.textBox58.Name = "textBox58";
            this.textBox58.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(10.895313262939453D), Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D));
            this.textBox58.StyleName = "";
            this.textBox58.Value = "= Fields.CustomerCompanyName";
            // 
            // PartsSalesSettlementDataSource
            // 
            this.PartsSalesSettlementDataSource.DataMember = "GetPartsSalesSettlementByIdForPrint";
            this.PartsSalesSettlementDataSource.DataSource = typeof(Sunlight.Silverlight.Dcs.Web.DcsDomainService);
            this.PartsSalesSettlementDataSource.Name = "PartsSalesSettlementDataSource";
            this.PartsSalesSettlementDataSource.Parameters.AddRange(new Telerik.Reporting.ObjectDataSourceParameter[] {
            new Telerik.Reporting.ObjectDataSourceParameter("PartsSalesSettlementId", typeof(int), "=Parameters.partsSalesSettlementId.Value")});
            // 
            // table4
            // 
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(6.1438355445861816D)));
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.7158589363098145D)));
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.3600001335144043D)));
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(7.7700004577636719D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.5D)));
            this.table4.Body.SetCellContent(0, 0, this.textBox35);
            this.table4.Body.SetCellContent(0, 3, this.textBox54);
            this.table4.Body.SetCellContent(0, 2, this.textBox93);
            this.table4.Body.SetCellContent(0, 1, this.textBox102);
            tableGroup18.ReportItem = this.textBox19;
            tableGroup19.Name = "Group6";
            tableGroup19.ReportItem = this.textBox101;
            tableGroup20.Name = "Group4";
            tableGroup20.ReportItem = this.textBox89;
            tableGroup21.Name = "Group2";
            tableGroup21.ReportItem = this.textBox49;
            this.table4.ColumnGroups.Add(tableGroup18);
            this.table4.ColumnGroups.Add(tableGroup19);
            this.table4.ColumnGroups.Add(tableGroup20);
            this.table4.ColumnGroups.Add(tableGroup21);
            this.table4.ColumnHeadersPrintOnEveryPage = true;
            this.table4.DataSource = this.PartsSalesSettlementDetailDataSource;
            this.table4.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox35,
            this.textBox54,
            this.textBox93,
            this.textBox102,
            this.textBox19,
            this.textBox101,
            this.textBox89,
            this.textBox49});
            this.table4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(2.9002001285552979D));
            this.table4.Name = "table4";
            tableGroup22.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup22.Name = "DetailGroup";
            this.table4.RowGroups.Add(tableGroup22);
            this.table4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.989694595336914D), Telerik.Reporting.Drawing.Unit.Cm(1D));
            this.table4.Sortings.Add(new Telerik.Reporting.Sorting("=Fields.SourceCode", Telerik.Reporting.SortDirection.Asc));
            this.table4.Style.Font.Name = "宋体";
            this.table4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            // 
            // textBox35
            // 
            this.textBox35.Name = "textBox35";
            this.textBox35.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.1438350677490234D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox35.Value = "= Fields.SourceCode";
            // 
            // textBox54
            // 
            this.textBox54.Name = "textBox54";
            this.textBox54.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.7700004577636719D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox54.StyleName = "";
            this.textBox54.Value = "= Fields.WarehouseName";
            // 
            // textBox93
            // 
            this.textBox93.Name = "textBox93";
            this.textBox93.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.3599998950958252D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox93.StyleName = "";
            this.textBox93.Value = "= Fields.Warehouse.Code";
            // 
            // textBox102
            // 
            this.textBox102.Name = "textBox102";
            this.textBox102.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.7158591747283936D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox102.StyleName = "";
            this.textBox102.Value = "= Fields.SettlementAmount";
            // 
            // PartsSalesSettlementDetailDataSource
            // 
            this.PartsSalesSettlementDetailDataSource.DataMember = "GetPartsSalesSettlementRefByIdForPrint";
            this.PartsSalesSettlementDetailDataSource.DataSource = typeof(Sunlight.Silverlight.Dcs.Web.DcsDomainService);
            this.PartsSalesSettlementDetailDataSource.Name = "PartsSalesSettlementDetailDataSource";
            this.PartsSalesSettlementDetailDataSource.Parameters.AddRange(new Telerik.Reporting.ObjectDataSourceParameter[] {
            new Telerik.Reporting.ObjectDataSourceParameter("PartsSalesSettlementId", typeof(int), "=Parameters.partsSalesSettlementId.Value")});
            // 
            // pageFooterSection1
            // 
            this.pageFooterSection1.Height = Telerik.Reporting.Drawing.Unit.Cm(2.0000002384185791D);
            this.pageFooterSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox12,
            this.textBox13,
            this.textBox21,
            this.textBox22,
            this.textBox23,
            this.textBox31,
            this.textBox39,
            this.textBox40,
            this.textBox41,
            this.textBox50,
            this.textBox60,
            this.textBox62,
            this.textBox63,
            this.textBox64,
            this.textBox65,
            this.textBox66,
            this.textBox67,
            this.textBox68});
            this.pageFooterSection1.Name = "pageFooterSection1";
            this.pageFooterSection1.Style.Font.Name = "宋体";
            this.pageFooterSection1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            // 
            // textBox12
            // 
            this.textBox12.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.1002004146575928D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.4851183891296387D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox12.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox12.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox12.Value = "= Fields.SalesCompanyName";
            // 
            // textBox13
            // 
            this.textBox13.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.0045830532908439636D), Telerik.Reporting.Drawing.Unit.Cm(0.00010012308484874666D));
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.0954170227050781D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox13.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox13.Style.Font.Bold = false;
            this.textBox13.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox13.Value = "制单单位：";
            // 
            // textBox21
            // 
            this.textBox21.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(6.5855193138122559D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.8742622137069702D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox21.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox21.Style.Font.Bold = false;
            this.textBox21.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox21.Value = "制单人：";
            // 
            // textBox22
            // 
            this.textBox22.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(8.4599819183349609D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.8742623329162598D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox22.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox22.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox22.Value = "= Fields.CreatorName";
            // 
            // textBox23
            // 
            this.textBox23.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.334444046020508D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.364722728729248D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox23.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox23.Style.Font.Bold = false;
            this.textBox23.Value = "制单时间：";
            // 
            // textBox31
            // 
            this.textBox31.Format = "{0:d}";
            this.textBox31.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.699366569519043D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.6500000953674316D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox31.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox31.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox31.Value = "= Fields.CreateTime.Date.Date.Date";
            // 
            // textBox39
            // 
            this.textBox39.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.349566459655762D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.textBox39.Name = "textBox39";
            this.textBox39.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.8700000047683716D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox39.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox39.Style.Font.Bold = false;
            this.textBox39.Value = "打印人：";
            // 
            // textBox40
            // 
            this.textBox40.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(17.219766616821289D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.textBox40.Name = "textBox40";
            this.textBox40.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.7702324390411377D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox40.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox40.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox40.Value = "= Parameters.UserName.Value";
            // 
            // textBox41
            // 
            this.textBox41.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(0.50030052661895752D));
            this.textBox41.Name = "textBox41";
            this.textBox41.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.0999999046325684D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox41.Style.Font.Bold = false;
            this.textBox41.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox41.Value = "打印时间:";
            // 
            // textBox50
            // 
            this.textBox50.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.1002004146575928D), Telerik.Reporting.Drawing.Unit.Cm(0.5003013014793396D));
            this.textBox50.Name = "textBox50";
            this.textBox50.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.4899997711181641D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox50.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox50.Value = "= Now()";
            // 
            // textBox60
            // 
            this.textBox60.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(6.590400218963623D), Telerik.Reporting.Drawing.Unit.Cm(0.5003013014793396D));
            this.textBox60.Name = "textBox60";
            this.textBox60.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.8700000047683716D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox60.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox60.Value = "注：";
            // 
            // textBox62
            // 
            this.textBox62.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(8.4642429351806641D), Telerik.Reporting.Drawing.Unit.Cm(0.50030213594436646D));
            this.textBox62.Name = "textBox62";
            this.textBox62.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.8700000047683716D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox62.Value = "";
            // 
            // textBox63
            // 
            this.textBox63.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.349566459655762D), Telerik.Reporting.Drawing.Unit.Cm(0.51238632202148438D));
            this.textBox63.Name = "textBox63";
            this.textBox63.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.62000000476837158D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox63.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox63.Value = "第";
            // 
            // textBox64
            // 
            this.textBox64.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.969765663146973D), Telerik.Reporting.Drawing.Unit.Cm(0.51238632202148438D));
            this.textBox64.Name = "textBox64";
            this.textBox64.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.86000001430511475D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox64.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox64.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox64.Value = "= PageNumber";
            // 
            // textBox65
            // 
            this.textBox65.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(16.829967498779297D), Telerik.Reporting.Drawing.Unit.Cm(0.51238632202148438D));
            this.textBox65.Name = "textBox65";
            this.textBox65.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.60000002384185791D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox65.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox65.Value = "页";
            // 
            // textBox66
            // 
            this.textBox66.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(19.092897415161133D), Telerik.Reporting.Drawing.Unit.Cm(0.51238632202148438D));
            this.textBox66.Name = "textBox66";
            this.textBox66.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.60000002384185791D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox66.Value = "页";
            // 
            // textBox67
            // 
            this.textBox67.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(18.232698440551758D), Telerik.Reporting.Drawing.Unit.Cm(0.51238632202148438D));
            this.textBox67.Name = "textBox67";
            this.textBox67.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.86000001430511475D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox67.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox67.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox67.Value = "= PageCount";
            // 
            // textBox68
            // 
            this.textBox68.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(17.502496719360352D), Telerik.Reporting.Drawing.Unit.Cm(0.51238632202148438D));
            this.textBox68.Name = "textBox68";
            this.textBox68.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.73000001907348633D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox68.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox68.Value = "共";
            // 
            // ReportPartsSalesSettlement
            // 
            this.DataSource = this.PartsSalesSettlementDataSource;
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pageHeaderSection1,
            this.detail,
            this.pageFooterSection1});
            this.Name = "ReportPartsSalesSettlement";
            this.PageSettings.Landscape = true;
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Pixel(25D), Telerik.Reporting.Drawing.Unit.Pixel(19D), Telerik.Reporting.Drawing.Unit.Mm(9D), Telerik.Reporting.Drawing.Unit.Mm(9D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Custom;
            this.PageSettings.PaperSize = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(14D), Telerik.Reporting.Drawing.Unit.Cm(22D));
            reportParameter1.Name = "partsSalesSettlementId";
            reportParameter2.Name = "UserName";
            this.ReportParameters.Add(reportParameter1);
            this.ReportParameters.Add(reportParameter2);
            this.Style.BackgroundColor = System.Drawing.Color.White;
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
            this.Width = Telerik.Reporting.Drawing.Unit.Cm(20.022350311279297D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.PageHeaderSection pageHeaderSection1;
        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.PageFooterSection pageFooterSection1;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.Table table1;
        private Telerik.Reporting.TextBox textBox18;
        private Telerik.Reporting.TextBox textBox24;
        private Telerik.Reporting.TextBox textBox25;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.TextBox textBox9;
        private Telerik.Reporting.TextBox textBox11;
        private Telerik.Reporting.TextBox textBox15;
        private Telerik.Reporting.TextBox textBox17;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.TextBox textBox10;
        private Telerik.Reporting.TextBox textBox34;
        private Telerik.Reporting.TextBox textBox43;
        private Telerik.Reporting.TextBox textBox46;
        private Telerik.Reporting.TextBox textBox48;
        private Telerik.Reporting.TextBox textBox51;
        private Telerik.Reporting.TextBox textBox53;
        private Telerik.Reporting.TextBox textBox56;
        private Telerik.Reporting.TextBox textBox26;
        private Telerik.Reporting.TextBox textBox27;
        private Telerik.Reporting.TextBox textBox28;
        private Telerik.Reporting.TextBox textBox29;
        private Telerik.Reporting.TextBox textBox30;
        private Telerik.Reporting.TextBox textBox32;
        private Telerik.Reporting.TextBox textBox52;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.TextBox textBox57;
        private Telerik.Reporting.TextBox textBox58;
        private Telerik.Reporting.TextBox textBox59;
        private Telerik.Reporting.TextBox textBox61;
        private Telerik.Reporting.TextBox textBox47;
        private Telerik.Reporting.TextBox textBox42;
        private Telerik.Reporting.ObjectDataSource PartsSalesSettlementDataSource;
        private Telerik.Reporting.ObjectDataSource PartsSalesSettlementDetailDataSource;
        private Telerik.Reporting.TextBox textBox20;
        private Telerik.Reporting.TextBox textBox16;
        private Telerik.Reporting.TextBox textBox36;
        private Telerik.Reporting.TextBox textBox33;
        private Telerik.Reporting.TextBox textBox14;
        private Telerik.Reporting.Table table4;
        private Telerik.Reporting.TextBox textBox35;
        private Telerik.Reporting.TextBox textBox54;
        private Telerik.Reporting.TextBox textBox93;
        private Telerik.Reporting.TextBox textBox102;
        private Telerik.Reporting.TextBox textBox19;
        private Telerik.Reporting.TextBox textBox101;
        private Telerik.Reporting.TextBox textBox89;
        private Telerik.Reporting.TextBox textBox49;
        private Telerik.Reporting.TextBox textBox12;
        private Telerik.Reporting.TextBox textBox13;
        private Telerik.Reporting.TextBox textBox21;
        private Telerik.Reporting.TextBox textBox22;
        private Telerik.Reporting.TextBox textBox23;
        private Telerik.Reporting.TextBox textBox31;
        private Telerik.Reporting.TextBox textBox39;
        private Telerik.Reporting.TextBox textBox40;
        private Telerik.Reporting.TextBox textBox41;
        private Telerik.Reporting.TextBox textBox50;
        private Telerik.Reporting.TextBox textBox60;
        private Telerik.Reporting.TextBox textBox62;
        private Telerik.Reporting.TextBox textBox63;
        private Telerik.Reporting.TextBox textBox64;
        private Telerik.Reporting.TextBox textBox65;
        private Telerik.Reporting.TextBox textBox66;
        private Telerik.Reporting.TextBox textBox67;
        private Telerik.Reporting.TextBox textBox68;
    }
}