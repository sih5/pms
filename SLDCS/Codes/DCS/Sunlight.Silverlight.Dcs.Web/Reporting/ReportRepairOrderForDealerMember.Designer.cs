﻿namespace Sunlight.Silverlight.Dcs.Web.Reporting {
    partial class ReportRepairOrderForDealerMember {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup11 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup12 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup13 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup14 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup15 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup16 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup17 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup18 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup19 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup20 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup21 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup22 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup23 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup24 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup25 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup26 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup27 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup28 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup29 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup30 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup31 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup32 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup33 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup34 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup35 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup36 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup37 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup38 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup39 = new Telerik.Reporting.TableGroup();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReportRepairOrderForDealerMember));
            Telerik.Reporting.TableGroup tableGroup40 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup41 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup42 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup43 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup44 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup45 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup46 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup47 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup48 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup49 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup50 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup51 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup52 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup53 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup54 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup55 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup56 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup57 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup58 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup59 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup60 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup61 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup62 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup63 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup64 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup65 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup66 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup67 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup68 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup69 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup70 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup71 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup72 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup73 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup74 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup75 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup76 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup77 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup78 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup79 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup80 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup81 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup82 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup83 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup84 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup85 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup86 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup87 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup88 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup89 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup90 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup91 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup92 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup93 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup94 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup95 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup96 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup97 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup98 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup99 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup100 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup101 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup102 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup103 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup104 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup105 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup106 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup107 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup108 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.ReportParameter reportParameter1 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter2 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            this.textBox97 = new Telerik.Reporting.TextBox();
            this.textBox35 = new Telerik.Reporting.TextBox();
            this.textBox36 = new Telerik.Reporting.TextBox();
            this.textBox37 = new Telerik.Reporting.TextBox();
            this.textBox41 = new Telerik.Reporting.TextBox();
            this.textBox43 = new Telerik.Reporting.TextBox();
            this.textBox47 = new Telerik.Reporting.TextBox();
            this.textBox49 = new Telerik.Reporting.TextBox();
            this.textBox98 = new Telerik.Reporting.TextBox();
            this.textBox34 = new Telerik.Reporting.TextBox();
            this.textBox51 = new Telerik.Reporting.TextBox();
            this.textBox81 = new Telerik.Reporting.TextBox();
            this.textBox52 = new Telerik.Reporting.TextBox();
            this.textBox91 = new Telerik.Reporting.TextBox();
            this.textBox53 = new Telerik.Reporting.TextBox();
            this.textBox92 = new Telerik.Reporting.TextBox();
            this.textBox57 = new Telerik.Reporting.TextBox();
            this.textBox93 = new Telerik.Reporting.TextBox();
            this.textBox59 = new Telerik.Reporting.TextBox();
            this.textBox94 = new Telerik.Reporting.TextBox();
            this.textBox61 = new Telerik.Reporting.TextBox();
            this.textBox95 = new Telerik.Reporting.TextBox();
            this.textBox63 = new Telerik.Reporting.TextBox();
            this.textBox96 = new Telerik.Reporting.TextBox();
            this.textBox108 = new Telerik.Reporting.TextBox();
            this.textBox109 = new Telerik.Reporting.TextBox();
            this.textBox110 = new Telerik.Reporting.TextBox();
            this.textBox114 = new Telerik.Reporting.TextBox();
            this.textBox116 = new Telerik.Reporting.TextBox();
            this.textBox118 = new Telerik.Reporting.TextBox();
            this.textBox146 = new Telerik.Reporting.TextBox();
            this.textBox154 = new Telerik.Reporting.TextBox();
            this.textBox160 = new Telerik.Reporting.TextBox();
            this.pageHeaderSection1 = new Telerik.Reporting.PageHeaderSection();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.detail = new Telerik.Reporting.DetailSection();
            this.table1 = new Telerik.Reporting.Table();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.textBox120 = new Telerik.Reporting.TextBox();
            this.textBox121 = new Telerik.Reporting.TextBox();
            this.RepairOrderForDealerDataSource = new Telerik.Reporting.ObjectDataSource();
            this.table2 = new Telerik.Reporting.Table();
            this.textBox9 = new Telerik.Reporting.TextBox();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.textBox8 = new Telerik.Reporting.TextBox();
            this.textBox11 = new Telerik.Reporting.TextBox();
            this.textBox7 = new Telerik.Reporting.TextBox();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.textBox10 = new Telerik.Reporting.TextBox();
            this.textBox12 = new Telerik.Reporting.TextBox();
            this.textBox13 = new Telerik.Reporting.TextBox();
            this.textBox14 = new Telerik.Reporting.TextBox();
            this.textBox15 = new Telerik.Reporting.TextBox();
            this.textBox16 = new Telerik.Reporting.TextBox();
            this.textBox17 = new Telerik.Reporting.TextBox();
            this.textBox18 = new Telerik.Reporting.TextBox();
            this.textBox19 = new Telerik.Reporting.TextBox();
            this.textBox20 = new Telerik.Reporting.TextBox();
            this.textBox21 = new Telerik.Reporting.TextBox();
            this.textBox22 = new Telerik.Reporting.TextBox();
            this.textBox23 = new Telerik.Reporting.TextBox();
            this.textBox24 = new Telerik.Reporting.TextBox();
            this.textBox124 = new Telerik.Reporting.TextBox();
            this.textBox125 = new Telerik.Reporting.TextBox();
            this.textBox126 = new Telerik.Reporting.TextBox();
            this.textBox127 = new Telerik.Reporting.TextBox();
            this.textBox128 = new Telerik.Reporting.TextBox();
            this.textBox130 = new Telerik.Reporting.TextBox();
            this.textBox131 = new Telerik.Reporting.TextBox();
            this.textBox132 = new Telerik.Reporting.TextBox();
            this.textBox133 = new Telerik.Reporting.TextBox();
            this.textBox134 = new Telerik.Reporting.TextBox();
            this.textBox129 = new Telerik.Reporting.TextBox();
            this.table3 = new Telerik.Reporting.Table();
            this.textBox29 = new Telerik.Reporting.TextBox();
            this.textBox27 = new Telerik.Reporting.TextBox();
            this.textBox26 = new Telerik.Reporting.TextBox();
            this.textBox25 = new Telerik.Reporting.TextBox();
            this.textBox28 = new Telerik.Reporting.TextBox();
            this.textBox30 = new Telerik.Reporting.TextBox();
            this.textBox31 = new Telerik.Reporting.TextBox();
            this.textBox32 = new Telerik.Reporting.TextBox();
            this.textBox33 = new Telerik.Reporting.TextBox();
            this.textBox45 = new Telerik.Reporting.TextBox();
            this.textBox46 = new Telerik.Reporting.TextBox();
            this.table4 = new Telerik.Reporting.Table();
            this.textBox38 = new Telerik.Reporting.TextBox();
            this.textBox39 = new Telerik.Reporting.TextBox();
            this.textBox40 = new Telerik.Reporting.TextBox();
            this.textBox42 = new Telerik.Reporting.TextBox();
            this.textBox44 = new Telerik.Reporting.TextBox();
            this.textBox48 = new Telerik.Reporting.TextBox();
            this.textBox50 = new Telerik.Reporting.TextBox();
            this.textBox105 = new Telerik.Reporting.TextBox();
            this.RepairOrderItemDetailDataSource = new Telerik.Reporting.ObjectDataSource();
            this.table5 = new Telerik.Reporting.Table();
            this.textBox54 = new Telerik.Reporting.TextBox();
            this.textBox55 = new Telerik.Reporting.TextBox();
            this.textBox56 = new Telerik.Reporting.TextBox();
            this.textBox58 = new Telerik.Reporting.TextBox();
            this.textBox60 = new Telerik.Reporting.TextBox();
            this.textBox62 = new Telerik.Reporting.TextBox();
            this.textBox64 = new Telerik.Reporting.TextBox();
            this.textBox101 = new Telerik.Reporting.TextBox();
            this.RepairOrderForDealerDetailDataSource = new Telerik.Reporting.ObjectDataSource();
            this.table6 = new Telerik.Reporting.Table();
            this.textBox72 = new Telerik.Reporting.TextBox();
            this.textBox66 = new Telerik.Reporting.TextBox();
            this.textBox67 = new Telerik.Reporting.TextBox();
            this.textBox68 = new Telerik.Reporting.TextBox();
            this.textBox69 = new Telerik.Reporting.TextBox();
            this.textBox71 = new Telerik.Reporting.TextBox();
            this.textBox70 = new Telerik.Reporting.TextBox();
            this.textBox74 = new Telerik.Reporting.TextBox();
            this.textBox76 = new Telerik.Reporting.TextBox();
            this.textBox77 = new Telerik.Reporting.TextBox();
            this.textBox79 = new Telerik.Reporting.TextBox();
            this.textBox80 = new Telerik.Reporting.TextBox();
            this.textBox82 = new Telerik.Reporting.TextBox();
            this.textBox83 = new Telerik.Reporting.TextBox();
            this.textBox73 = new Telerik.Reporting.TextBox();
            this.table8 = new Telerik.Reporting.Table();
            this.textBox103 = new Telerik.Reporting.TextBox();
            this.textBox104 = new Telerik.Reporting.TextBox();
            this.textBox106 = new Telerik.Reporting.TextBox();
            this.textBox100 = new Telerik.Reporting.TextBox();
            this.textBox122 = new Telerik.Reporting.TextBox();
            this.table7 = new Telerik.Reporting.Table();
            this.textBox111 = new Telerik.Reporting.TextBox();
            this.textBox112 = new Telerik.Reporting.TextBox();
            this.textBox113 = new Telerik.Reporting.TextBox();
            this.textBox115 = new Telerik.Reporting.TextBox();
            this.textBox117 = new Telerik.Reporting.TextBox();
            this.textBox119 = new Telerik.Reporting.TextBox();
            this.table9 = new Telerik.Reporting.Table();
            this.textBox85 = new Telerik.Reporting.TextBox();
            this.textBox86 = new Telerik.Reporting.TextBox();
            this.textBox87 = new Telerik.Reporting.TextBox();
            this.textBox88 = new Telerik.Reporting.TextBox();
            this.textBox89 = new Telerik.Reporting.TextBox();
            this.textBox90 = new Telerik.Reporting.TextBox();
            this.textBox140 = new Telerik.Reporting.TextBox();
            this.textBox141 = new Telerik.Reporting.TextBox();
            this.textBox142 = new Telerik.Reporting.TextBox();
            this.textBox143 = new Telerik.Reporting.TextBox();
            this.textBox144 = new Telerik.Reporting.TextBox();
            this.textBox145 = new Telerik.Reporting.TextBox();
            this.textBox148 = new Telerik.Reporting.TextBox();
            this.textBox149 = new Telerik.Reporting.TextBox();
            this.textBox150 = new Telerik.Reporting.TextBox();
            this.textBox151 = new Telerik.Reporting.TextBox();
            this.textBox152 = new Telerik.Reporting.TextBox();
            this.textBox153 = new Telerik.Reporting.TextBox();
            this.textBox164 = new Telerik.Reporting.TextBox();
            this.textBox165 = new Telerik.Reporting.TextBox();
            this.textBox166 = new Telerik.Reporting.TextBox();
            this.textBox167 = new Telerik.Reporting.TextBox();
            this.textBox168 = new Telerik.Reporting.TextBox();
            this.textBox169 = new Telerik.Reporting.TextBox();
            this.textBox135 = new Telerik.Reporting.TextBox();
            this.textBox147 = new Telerik.Reporting.TextBox();
            this.textBox75 = new Telerik.Reporting.TextBox();
            this.textBox78 = new Telerik.Reporting.TextBox();
            this.textBox137 = new Telerik.Reporting.TextBox();
            this.textBox84 = new Telerik.Reporting.TextBox();
            this.textBox155 = new Telerik.Reporting.TextBox();
            this.textBox156 = new Telerik.Reporting.TextBox();
            this.textBox157 = new Telerik.Reporting.TextBox();
            this.VirtualRepairOrderMaterialDetailDataSource = new Telerik.Reporting.ObjectDataSource();
            this.table10 = new Telerik.Reporting.Table();
            this.textBox139 = new Telerik.Reporting.TextBox();
            this.textBox65 = new Telerik.Reporting.TextBox();
            this.textBox107 = new Telerik.Reporting.TextBox();
            this.GetRepairMemberRightsDetails = new Telerik.Reporting.ObjectDataSource();
            this.objectDataSource1 = new Telerik.Reporting.ObjectDataSource();
            this.pageFooterSection1 = new Telerik.Reporting.PageFooterSection();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // textBox97
            // 
            this.textBox97.Name = "textBox97";
            this.textBox97.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.6500000953674316D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox97.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox97.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox97.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox97.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox97.StyleName = "";
            // 
            // textBox35
            // 
            this.textBox35.Name = "textBox35";
            this.textBox35.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.3010119199752808D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox35.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox35.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox35.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox35.Value = "序号";
            // 
            // textBox36
            // 
            this.textBox36.Name = "textBox36";
            this.textBox36.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.5299997329711914D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox36.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox36.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox36.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox36.Value = "维修项目名称";
            // 
            // textBox37
            // 
            this.textBox37.Name = "textBox37";
            this.textBox37.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.50156307220459D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox37.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox37.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox37.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox37.Value = "工时单价";
            // 
            // textBox41
            // 
            this.textBox41.Name = "textBox41";
            this.textBox41.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.7808948755264282D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox41.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox41.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox41.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox41.StyleName = "";
            this.textBox41.Value = "工时定额";
            // 
            // textBox43
            // 
            this.textBox43.Name = "textBox43";
            this.textBox43.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.0657527446746826D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox43.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox43.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox43.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox43.StyleName = "";
            this.textBox43.Value = "标准工时费";
            // 
            // textBox47
            // 
            this.textBox47.Name = "textBox47";
            this.textBox47.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9111988544464111D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox47.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox47.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox47.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox47.StyleName = "";
            this.textBox47.Value = "结算属性";
            // 
            // textBox49
            // 
            this.textBox49.Name = "textBox49";
            this.textBox49.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.179999828338623D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox49.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox49.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox49.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox49.StyleName = "";
            this.textBox49.Value = "实收工时费";
            // 
            // textBox98
            // 
            this.textBox98.Name = "textBox98";
            this.textBox98.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.6500000953674316D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox98.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox98.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox98.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox98.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox98.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox98.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox98.StyleName = "";
            // 
            // textBox34
            // 
            this.textBox34.CanShrink = false;
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.6500000953674316D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox34.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox34.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox34.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox34.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox34.Style.Font.Bold = true;
            this.textBox34.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox34.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox34.Value = "维修信息";
            // 
            // textBox51
            // 
            this.textBox51.Name = "textBox51";
            this.textBox51.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.3026063442230225D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox51.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox51.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox51.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox51.Value = "序号";
            // 
            // textBox81
            // 
            this.textBox81.Name = "textBox81";
            this.textBox81.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.3026063442230225D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox81.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox81.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox81.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox81.StyleName = "";
            // 
            // textBox52
            // 
            this.textBox52.Name = "textBox52";
            this.textBox52.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.53000020980835D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox52.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox52.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox52.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox52.Value = "配件名称";
            // 
            // textBox91
            // 
            this.textBox91.Name = "textBox91";
            this.textBox91.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.53000020980835D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox91.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox91.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox91.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox91.StyleName = "";
            // 
            // textBox53
            // 
            this.textBox53.Name = "textBox53";
            this.textBox53.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.4968748092651367D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox53.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox53.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox53.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox53.Value = "配件图号";
            // 
            // textBox92
            // 
            this.textBox92.Name = "textBox92";
            this.textBox92.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.4968748092651367D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox92.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox92.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox92.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox92.StyleName = "";
            // 
            // textBox57
            // 
            this.textBox57.Name = "textBox57";
            this.textBox57.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.7799999713897705D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox57.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox57.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox57.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox57.StyleName = "";
            this.textBox57.Value = "实收材料费";
            // 
            // textBox93
            // 
            this.textBox93.Name = "textBox93";
            this.textBox93.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.7799999713897705D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox93.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox93.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox93.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox93.StyleName = "";
            // 
            // textBox59
            // 
            this.textBox59.Name = "textBox59";
            this.textBox59.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.0901286602020264D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox59.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox59.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox59.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox59.StyleName = "";
            this.textBox59.Value = "数量";
            // 
            // textBox94
            // 
            this.textBox94.Name = "textBox94";
            this.textBox94.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.0901286602020264D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox94.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox94.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox94.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox94.StyleName = "";
            // 
            // textBox61
            // 
            this.textBox61.Name = "textBox61";
            this.textBox61.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9000000953674316D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox61.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox61.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox61.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox61.StyleName = "";
            this.textBox61.Value = "结算属性";
            // 
            // textBox95
            // 
            this.textBox95.Name = "textBox95";
            this.textBox95.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9000000953674316D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox95.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox95.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox95.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox95.StyleName = "";
            // 
            // textBox63
            // 
            this.textBox63.Name = "textBox63";
            this.textBox63.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.1699998378753662D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox63.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox63.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox63.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox63.StyleName = "";
            this.textBox63.Value = "实收材料费";
            // 
            // textBox96
            // 
            this.textBox96.Name = "textBox96";
            this.textBox96.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.1699998378753662D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox96.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox96.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox96.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox96.StyleName = "";
            // 
            // textBox108
            // 
            this.textBox108.CanShrink = true;
            this.textBox108.Name = "textBox108";
            this.textBox108.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.0499999523162842D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox108.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox108.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox108.Value = "服务站名称：";
            // 
            // textBox109
            // 
            this.textBox109.CanShrink = true;
            this.textBox109.Name = "textBox109";
            this.textBox109.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.1915273666381836D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox109.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox109.Value = "= Fields.DealerName";
            // 
            // textBox110
            // 
            this.textBox110.CanShrink = true;
            this.textBox110.Name = "textBox110";
            this.textBox110.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.0499999523162842D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox110.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox110.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox110.Value = "服务站地址：";
            // 
            // textBox114
            // 
            this.textBox114.CanShrink = true;
            this.textBox114.Name = "textBox114";
            this.textBox114.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.1000003814697266D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox114.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox114.StyleName = "";
            this.textBox114.Value = "= Fields.Dealer.Company.ContactAddress";
            // 
            // textBox116
            // 
            this.textBox116.CanShrink = true;
            this.textBox116.Name = "textBox116";
            this.textBox116.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.7400000095367432D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox116.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox116.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox116.StyleName = "";
            this.textBox116.Value = "联系电话：";
            // 
            // textBox118
            // 
            this.textBox118.CanShrink = true;
            this.textBox118.Name = "textBox118";
            this.textBox118.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.7992854118347168D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox118.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox118.StyleName = "";
            this.textBox118.Value = "=Fields.Dealer.Company.ContactMobile";
            // 
            // textBox146
            // 
            this.textBox146.Name = "textBox146";
            this.textBox146.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.6500000953674316D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox146.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox146.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox146.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox146.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox146.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox146.Style.Font.Bold = true;
            this.textBox146.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox146.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox146.StyleName = "";
            this.textBox146.Value = "会员信息";
            // 
            // textBox154
            // 
            this.textBox154.Name = "textBox154";
            this.textBox154.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.3325748443603516D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox154.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox154.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox154.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox154.Value = "会员权益";
            // 
            // textBox160
            // 
            this.textBox160.Name = "textBox160";
            this.textBox160.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.9378466606140137D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox160.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox160.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox160.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox160.StyleName = "";
            this.textBox160.Value = "是否使用";
            // 
            // pageHeaderSection1
            // 
            this.pageHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Cm(2D);
            this.pageHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox1});
            this.pageHeaderSection1.Name = "pageHeaderSection1";
            this.pageHeaderSection1.Style.Font.Name = "黑体";
            // 
            // textBox1
            // 
            this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(8.6999998092651367D), Telerik.Reporting.Drawing.Unit.Cm(1.1000000238418579D));
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.5D), Telerik.Reporting.Drawing.Unit.Cm(0.60000008344650269D));
            this.textBox1.Style.Font.Bold = true;
            this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(15D);
            this.textBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox1.Value = "结算单";
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Cm(17.20550537109375D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.table1,
            this.table2,
            this.table3,
            this.table4,
            this.table5,
            this.table6,
            this.table8,
            this.table7,
            this.table9,
            this.table10});
            this.detail.Name = "detail";
            this.detail.Style.Font.Name = "宋体";
            // 
            // table1
            // 
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.640418529510498D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(12.218340873718262D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.1058318614959717D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.9260411262512207D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.5D)));
            this.table1.Body.SetCellContent(0, 0, this.textBox3);
            this.table1.Body.SetCellContent(0, 1, this.textBox6);
            this.table1.Body.SetCellContent(0, 2, this.textBox120);
            this.table1.Body.SetCellContent(0, 3, this.textBox121);
            tableGroup3.Name = "Group1";
            tableGroup4.Name = "Group3";
            this.table1.ColumnGroups.Add(tableGroup1);
            this.table1.ColumnGroups.Add(tableGroup2);
            this.table1.ColumnGroups.Add(tableGroup3);
            this.table1.ColumnGroups.Add(tableGroup4);
            this.table1.DataSource = this.RepairOrderForDealerDataSource;
            this.table1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox3,
            this.textBox6,
            this.textBox120,
            this.textBox121});
            this.table1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.9921220680698752E-05D), Telerik.Reporting.Drawing.Unit.Cm(9.9921220680698752E-05D));
            this.table1.Name = "table1";
            tableGroup6.Name = "Group2";
            tableGroup5.ChildGroups.Add(tableGroup6);
            tableGroup5.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup5.Name = "DetailGroup";
            this.table1.RowGroups.Add(tableGroup5);
            this.table1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.890632629394531D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            // 
            // textBox3
            // 
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.6404170989990234D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox3.Style.Font.Bold = true;
            this.textBox3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox3.StyleName = "";
            this.textBox3.Value = "维修单编号：";
            // 
            // textBox6
            // 
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(12.21833610534668D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox6.StyleName = "";
            this.textBox6.Value = "= Fields.Code";
            // 
            // textBox120
            // 
            this.textBox120.Name = "textBox120";
            this.textBox120.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.1058330535888672D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox120.Style.Font.Bold = true;
            this.textBox120.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox120.StyleName = "";
            this.textBox120.Value = "打印时间：";
            // 
            // textBox121
            // 
            this.textBox121.Format = "{0:d}";
            this.textBox121.Name = "textBox121";
            this.textBox121.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9260413646698D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox121.StyleName = "";
            this.textBox121.Value = "= Now()";
            // 
            // RepairOrderForDealerDataSource
            // 
            this.RepairOrderForDealerDataSource.DataMember = "GetRepairOrderManagementByIdForPrint";
            this.RepairOrderForDealerDataSource.DataSource = typeof(Sunlight.Silverlight.Dcs.Web.DcsDomainService);
            this.RepairOrderForDealerDataSource.Name = "RepairOrderForDealerDataSource";
            this.RepairOrderForDealerDataSource.Parameters.AddRange(new Telerik.Reporting.ObjectDataSourceParameter[] {
            new Telerik.Reporting.ObjectDataSourceParameter("RepairOrderId", typeof(int), "= Parameters.paraRepairOrderId.Value")});
            // 
            // table2
            // 
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.6463990211486816D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.5155165195465088D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.3109393119812012D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(4.2872438430786133D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.61582612991333D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.5440711975097656D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.47883346676826477D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.77516680955886841D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.5D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.5D)));
            this.table2.Body.SetCellContent(0, 1, this.textBox9);
            this.table2.Body.SetCellContent(3, 1, this.textBox4);
            this.table2.Body.SetCellContent(2, 1, this.textBox8);
            this.table2.Body.SetCellContent(1, 1, this.textBox11);
            this.table2.Body.SetCellContent(0, 0, this.textBox7, 4, 1);
            this.table2.Body.SetCellContent(0, 2, this.textBox2);
            this.table2.Body.SetCellContent(1, 2, this.textBox5);
            this.table2.Body.SetCellContent(2, 2, this.textBox10);
            this.table2.Body.SetCellContent(3, 2, this.textBox12);
            this.table2.Body.SetCellContent(0, 5, this.textBox13);
            this.table2.Body.SetCellContent(1, 5, this.textBox14);
            this.table2.Body.SetCellContent(2, 5, this.textBox15);
            this.table2.Body.SetCellContent(3, 5, this.textBox16);
            this.table2.Body.SetCellContent(0, 4, this.textBox17);
            this.table2.Body.SetCellContent(1, 4, this.textBox18);
            this.table2.Body.SetCellContent(2, 4, this.textBox19);
            this.table2.Body.SetCellContent(3, 4, this.textBox20);
            this.table2.Body.SetCellContent(0, 3, this.textBox21);
            this.table2.Body.SetCellContent(1, 3, this.textBox22);
            this.table2.Body.SetCellContent(2, 3, this.textBox23);
            this.table2.Body.SetCellContent(3, 3, this.textBox24);
            this.table2.Body.SetCellContent(5, 1, this.textBox124);
            this.table2.Body.SetCellContent(5, 2, this.textBox125);
            this.table2.Body.SetCellContent(5, 3, this.textBox126);
            this.table2.Body.SetCellContent(5, 4, this.textBox127);
            this.table2.Body.SetCellContent(5, 5, this.textBox128);
            this.table2.Body.SetCellContent(4, 1, this.textBox130);
            this.table2.Body.SetCellContent(4, 2, this.textBox131);
            this.table2.Body.SetCellContent(4, 3, this.textBox132);
            this.table2.Body.SetCellContent(4, 4, this.textBox133);
            this.table2.Body.SetCellContent(4, 5, this.textBox134);
            this.table2.Body.SetCellContent(4, 0, this.textBox129, 2, 1);
            tableGroup9.Name = "Group5";
            tableGroup10.Name = "Group8";
            tableGroup11.Name = "Group7";
            tableGroup12.Name = "Group6";
            this.table2.ColumnGroups.Add(tableGroup7);
            this.table2.ColumnGroups.Add(tableGroup8);
            this.table2.ColumnGroups.Add(tableGroup9);
            this.table2.ColumnGroups.Add(tableGroup10);
            this.table2.ColumnGroups.Add(tableGroup11);
            this.table2.ColumnGroups.Add(tableGroup12);
            this.table2.DataSource = this.RepairOrderForDealerDataSource;
            this.table2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox7,
            this.textBox9,
            this.textBox2,
            this.textBox21,
            this.textBox17,
            this.textBox13,
            this.textBox11,
            this.textBox5,
            this.textBox22,
            this.textBox18,
            this.textBox14,
            this.textBox8,
            this.textBox10,
            this.textBox23,
            this.textBox19,
            this.textBox15,
            this.textBox4,
            this.textBox12,
            this.textBox24,
            this.textBox20,
            this.textBox16,
            this.textBox129,
            this.textBox130,
            this.textBox131,
            this.textBox132,
            this.textBox133,
            this.textBox134,
            this.textBox124,
            this.textBox125,
            this.textBox126,
            this.textBox127,
            this.textBox128});
            this.table2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.9921220680698752E-05D), Telerik.Reporting.Drawing.Unit.Cm(0.50030034780502319D));
            this.table2.Name = "table2";
            tableGroup14.Name = "Group1";
            tableGroup15.Name = "Group4";
            tableGroup16.Name = "Group3";
            tableGroup17.Name = "Group2";
            tableGroup13.ChildGroups.Add(tableGroup14);
            tableGroup13.ChildGroups.Add(tableGroup15);
            tableGroup13.ChildGroups.Add(tableGroup16);
            tableGroup13.ChildGroups.Add(tableGroup17);
            tableGroup13.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup13.Name = "DetailGroup";
            tableGroup19.Name = "group3";
            tableGroup18.ChildGroups.Add(tableGroup19);
            tableGroup18.Name = "group2";
            tableGroup21.Name = "group1";
            tableGroup20.ChildGroups.Add(tableGroup21);
            tableGroup20.Name = "group";
            this.table2.RowGroups.Add(tableGroup13);
            this.table2.RowGroups.Add(tableGroup18);
            this.table2.RowGroups.Add(tableGroup20);
            this.table2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.91999626159668D), Telerik.Reporting.Drawing.Unit.Cm(3.2540004253387451D));
            // 
            // textBox9
            // 
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.5155177116394043D), Telerik.Reporting.Drawing.Unit.Cm(0.49999994039535522D));
            this.textBox9.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox9.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox9.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox9.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox9.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox9.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox9.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox9.Value = "联系人";
            // 
            // textBox4
            // 
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.5155177116394043D), Telerik.Reporting.Drawing.Unit.Cm(0.77516686916351318D));
            this.textBox4.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox4.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox4.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox4.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox4.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox4.StyleName = "";
            this.textBox4.Value = "= Fields.VehicleInformation.ProductCategoryName";
            // 
            // textBox8
            // 
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.5155177116394043D), Telerik.Reporting.Drawing.Unit.Cm(0.47883340716362D));
            this.textBox8.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox8.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox8.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox8.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox8.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox8.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox8.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox8.StyleName = "";
            this.textBox8.Value = "车型名称";
            // 
            // textBox11
            // 
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.5155177116394043D), Telerik.Reporting.Drawing.Unit.Cm(0.49999994039535522D));
            this.textBox11.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox11.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox11.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox11.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox11.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox11.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox11.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox11.StyleName = "";
            this.textBox11.Value = "= Fields.VehicleContactPerson";
            // 
            // textBox7
            // 
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.6464006900787354D), Telerik.Reporting.Drawing.Unit.Cm(2.2539999485015869D));
            this.textBox7.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox7.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox7.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox7.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox7.Style.Font.Bold = true;
            this.textBox7.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox7.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox7.Value = "客户及\r\n车辆信息";
            // 
            // textBox2
            // 
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.3109393119812012D), Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D));
            this.textBox2.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox2.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox2.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox2.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox2.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox2.StyleName = "";
            this.textBox2.Value = "联系电话";
            // 
            // textBox5
            // 
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.3109393119812012D), Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D));
            this.textBox5.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox5.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox5.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox5.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox5.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox5.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox5.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox5.StyleName = "";
            this.textBox5.Value = "= Fields.ContactPhone";
            // 
            // textBox10
            // 
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.3109393119812012D), Telerik.Reporting.Drawing.Unit.Cm(0.47883340716362D));
            this.textBox10.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox10.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox10.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox10.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox10.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox10.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox10.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox10.StyleName = "";
            this.textBox10.Value = "销售日期";
            // 
            // textBox12
            // 
            this.textBox12.Format = "{0:d}";
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.3109393119812012D), Telerik.Reporting.Drawing.Unit.Cm(0.77516686916351318D));
            this.textBox12.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox12.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox12.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox12.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox12.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox12.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox12.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox12.StyleName = "";
            this.textBox12.Value = "= Fields.VehicleInformation.SalesDate";
            // 
            // textBox13
            // 
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.5440711975097656D), Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D));
            this.textBox13.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox13.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox13.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox13.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox13.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox13.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox13.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox13.StyleName = "";
            this.textBox13.Value = "品牌";
            // 
            // textBox14
            // 
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.5440711975097656D), Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D));
            this.textBox14.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox14.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox14.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox14.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox14.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox14.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox14.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox14.StyleName = "";
            this.textBox14.Value = "= Fields.ServiceProductLine.PartsSalesCategory.Name";
            // 
            // textBox15
            // 
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.5440711975097656D), Telerik.Reporting.Drawing.Unit.Cm(0.47883346676826477D));
            this.textBox15.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox15.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox15.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox15.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox15.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox15.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox15.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox15.StyleName = "";
            this.textBox15.Value = "方量";
            // 
            // textBox16
            // 
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.5440711975097656D), Telerik.Reporting.Drawing.Unit.Cm(0.77516680955886841D));
            this.textBox16.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox16.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox16.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox16.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox16.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox16.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox16.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox16.StyleName = "";
            this.textBox16.Value = "= Fields.Capacity";
            // 
            // textBox17
            // 
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.6158268451690674D), Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D));
            this.textBox17.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox17.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox17.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox17.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox17.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox17.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox17.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox17.StyleName = "";
            this.textBox17.Value = "车牌号";
            // 
            // textBox18
            // 
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.6158268451690674D), Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D));
            this.textBox18.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox18.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox18.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox18.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox18.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox18.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox18.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox18.StyleName = "";
            this.textBox18.Value = "= Fields.VehicleInformation.VehicleLicensePlate";
            // 
            // textBox19
            // 
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.6158268451690674D), Telerik.Reporting.Drawing.Unit.Cm(0.47883340716362D));
            this.textBox19.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox19.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox19.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox19.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox19.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox19.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox19.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox19.StyleName = "";
            this.textBox19.Value = "工作小时";
            // 
            // textBox20
            // 
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.6158268451690674D), Telerik.Reporting.Drawing.Unit.Cm(0.775166928768158D));
            this.textBox20.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox20.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox20.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox20.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox20.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox20.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox20.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox20.StyleName = "";
            this.textBox20.Value = "= Fields.WorkingHours";
            // 
            // textBox21
            // 
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.2872443199157715D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox21.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox21.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox21.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox21.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox21.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox21.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox21.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox21.StyleName = "";
            this.textBox21.Value = "出厂编号";
            // 
            // textBox22
            // 
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.2872443199157715D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox22.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox22.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox22.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox22.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox22.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox22.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox22.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox22.StyleName = "";
            this.textBox22.Value = "= Fields.VehicleInformation.SerialNumber";
            // 
            // textBox23
            // 
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.2872443199157715D), Telerik.Reporting.Drawing.Unit.Cm(0.47883331775665283D));
            this.textBox23.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox23.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox23.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox23.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox23.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox23.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox23.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox23.StyleName = "";
            this.textBox23.Value = "行驶里程";
            // 
            // textBox24
            // 
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.2872443199157715D), Telerik.Reporting.Drawing.Unit.Cm(0.77516663074493408D));
            this.textBox24.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox24.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox24.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox24.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox24.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox24.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox24.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox24.StyleName = "";
            this.textBox24.Value = "= Fields.Mileage";
            // 
            // textBox124
            // 
            this.textBox124.Name = "textBox124";
            this.textBox124.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.5155177116394043D), Telerik.Reporting.Drawing.Unit.Cm(0.49999991059303284D));
            this.textBox124.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox124.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox124.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox124.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox124.StyleName = "";
            this.textBox124.Value = "= Fields.MemberCode";
            // 
            // textBox125
            // 
            this.textBox125.Name = "textBox125";
            this.textBox125.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.3109393119812012D), Telerik.Reporting.Drawing.Unit.Cm(0.49999994039535522D));
            this.textBox125.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox125.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox125.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox125.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox125.StyleName = "";
            this.textBox125.Value = "= Fields.MemberName";
            // 
            // textBox126
            // 
            this.textBox126.Name = "textBox126";
            this.textBox126.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.2872438430786133D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox126.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox126.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox126.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox126.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox126.StyleName = "";
            this.textBox126.Value = "= Fields.MemberPhone";
            // 
            // textBox127
            // 
            this.textBox127.Name = "textBox127";
            this.textBox127.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.6158268451690674D), Telerik.Reporting.Drawing.Unit.Cm(0.49999994039535522D));
            this.textBox127.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox127.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox127.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox127.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox127.StyleName = "";
            this.textBox127.Value = "= Fields.MemberRank";
            // 
            // textBox128
            // 
            this.textBox128.Name = "textBox128";
            this.textBox128.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.5440711975097656D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox128.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox128.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox128.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox128.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox128.StyleName = "";
            this.textBox128.Value = "= Fields.ExistingIntegral";
            // 
            // textBox130
            // 
            this.textBox130.Name = "textBox130";
            this.textBox130.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.5155177116394043D), Telerik.Reporting.Drawing.Unit.Cm(0.49999991059303284D));
            this.textBox130.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox130.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox130.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox130.StyleName = "";
            this.textBox130.Value = "会员号";
            // 
            // textBox131
            // 
            this.textBox131.Name = "textBox131";
            this.textBox131.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.3109393119812012D), Telerik.Reporting.Drawing.Unit.Cm(0.49999994039535522D));
            this.textBox131.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox131.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox131.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox131.StyleName = "";
            this.textBox131.Value = "客户名称";
            // 
            // textBox132
            // 
            this.textBox132.Name = "textBox132";
            this.textBox132.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.2872438430786133D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox132.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox132.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox132.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox132.StyleName = "";
            this.textBox132.Value = "手机号码";
            // 
            // textBox133
            // 
            this.textBox133.Name = "textBox133";
            this.textBox133.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.6158268451690674D), Telerik.Reporting.Drawing.Unit.Cm(0.49999994039535522D));
            this.textBox133.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox133.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox133.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox133.StyleName = "";
            this.textBox133.Value = "会员等级";
            // 
            // textBox134
            // 
            this.textBox134.Name = "textBox134";
            this.textBox134.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.5440711975097656D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox134.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox134.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox134.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox134.StyleName = "";
            this.textBox134.Value = "现有积分";
            // 
            // textBox129
            // 
            this.textBox129.Name = "textBox129";
            this.textBox129.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.6464006900787354D), Telerik.Reporting.Drawing.Unit.Cm(1D));
            this.textBox129.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox129.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox129.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox129.Style.Font.Bold = true;
            this.textBox129.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox129.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox129.StyleName = "";
            this.textBox129.Value = "";
            // 
            // table3
            // 
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.65000057220459D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.5185413360595703D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.3114573955535889D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(4.2899975776672363D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.6160426139831543D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.5387496948242188D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.5D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.5D)));
            this.table3.Body.SetCellContent(0, 1, this.textBox29);
            this.table3.Body.SetCellContent(1, 1, this.textBox27);
            this.table3.Body.SetCellContent(0, 0, this.textBox26, 2, 1);
            this.table3.Body.SetCellContent(0, 2, this.textBox25);
            this.table3.Body.SetCellContent(1, 2, this.textBox28);
            this.table3.Body.SetCellContent(0, 3, this.textBox30);
            this.table3.Body.SetCellContent(1, 3, this.textBox31);
            this.table3.Body.SetCellContent(0, 4, this.textBox32);
            this.table3.Body.SetCellContent(1, 4, this.textBox33);
            this.table3.Body.SetCellContent(0, 5, this.textBox45);
            this.table3.Body.SetCellContent(1, 5, this.textBox46);
            tableGroup24.Name = "Group3";
            tableGroup25.Name = "Group4";
            tableGroup26.Name = "Group5";
            tableGroup27.Name = "Group6";
            this.table3.ColumnGroups.Add(tableGroup22);
            this.table3.ColumnGroups.Add(tableGroup23);
            this.table3.ColumnGroups.Add(tableGroup24);
            this.table3.ColumnGroups.Add(tableGroup25);
            this.table3.ColumnGroups.Add(tableGroup26);
            this.table3.ColumnGroups.Add(tableGroup27);
            this.table3.DataSource = this.RepairOrderForDealerDataSource;
            this.table3.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox26,
            this.textBox29,
            this.textBox25,
            this.textBox30,
            this.textBox32,
            this.textBox45,
            this.textBox27,
            this.textBox28,
            this.textBox31,
            this.textBox33,
            this.textBox46});
            this.table3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.9921220680698752E-05D), Telerik.Reporting.Drawing.Unit.Cm(4.7000002861022949D));
            this.table3.Name = "table3";
            tableGroup29.Name = "Group2";
            tableGroup30.Name = "Group1";
            tableGroup28.ChildGroups.Add(tableGroup29);
            tableGroup28.ChildGroups.Add(tableGroup30);
            tableGroup28.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup28.Name = "DetailGroup";
            this.table3.RowGroups.Add(tableGroup28);
            this.table3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.924789428710938D), Telerik.Reporting.Drawing.Unit.Cm(1D));
            // 
            // textBox29
            // 
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.5185408592224121D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox29.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox29.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox29.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox29.StyleName = "";
            this.textBox29.Value = "报修时间";
            // 
            // textBox27
            // 
            this.textBox27.Format = "{0:d}";
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.5185408592224121D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox27.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox27.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox27.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox27.StyleName = "";
            this.textBox27.Value = "= Fields.RepairRequestTime";
            // 
            // textBox26
            // 
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.6500000953674316D), Telerik.Reporting.Drawing.Unit.Cm(1D));
            this.textBox26.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox26.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox26.Style.Font.Bold = true;
            this.textBox26.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox26.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox26.StyleName = "";
            this.textBox26.Value = "作业信息";
            // 
            // textBox25
            // 
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.3114573955535889D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox25.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox25.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox25.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox25.StyleName = "";
            this.textBox25.Value = "完工时间";
            // 
            // textBox28
            // 
            this.textBox28.Format = "{0:d}";
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.3114573955535889D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox28.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox28.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox28.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox28.StyleName = "";
            this.textBox28.Value = "= Fields.FinishingTime";
            // 
            // textBox30
            // 
            this.textBox30.Name = "textBox30";
            this.textBox30.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.2899999618530273D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox30.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox30.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox30.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox30.StyleName = "";
            this.textBox30.Value = "结算时间";
            // 
            // textBox31
            // 
            this.textBox31.Format = "{0:d}";
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.2899999618530273D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox31.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox31.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox31.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox31.StyleName = "";
            this.textBox31.Value = "= Now()";
            // 
            // textBox32
            // 
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.6160423755645752D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox32.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox32.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox32.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox32.StyleName = "";
            this.textBox32.Value = "结算员";
            // 
            // textBox33
            // 
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.6160423755645752D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox33.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox33.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox33.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox33.StyleName = "";
            this.textBox33.Value = "= Parameters.UserName.Value";
            // 
            // textBox45
            // 
            this.textBox45.Name = "textBox45";
            this.textBox45.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.5387496948242188D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox45.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox45.StyleName = "";
            // 
            // textBox46
            // 
            this.textBox46.Name = "textBox46";
            this.textBox46.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.5387496948242188D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox46.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox46.StyleName = "";
            // 
            // table4
            // 
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.6500003337860107D)));
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.3010119199752808D)));
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(5.5299997329711914D)));
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.50156307220459D)));
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.7808952331542969D)));
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.0657527446746826D)));
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.9111989736557007D)));
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.179999828338623D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.5D)));
            this.table4.Body.SetCellContent(0, 1, this.textBox38);
            this.table4.Body.SetCellContent(0, 2, this.textBox39);
            this.table4.Body.SetCellContent(0, 3, this.textBox40);
            this.table4.Body.SetCellContent(0, 4, this.textBox42);
            this.table4.Body.SetCellContent(0, 5, this.textBox44);
            this.table4.Body.SetCellContent(0, 6, this.textBox48);
            this.table4.Body.SetCellContent(0, 7, this.textBox50);
            this.table4.Body.SetCellContent(0, 0, this.textBox105);
            tableGroup31.Name = "Group5";
            tableGroup31.ReportItem = this.textBox97;
            tableGroup32.ReportItem = this.textBox35;
            tableGroup33.ReportItem = this.textBox36;
            tableGroup34.ReportItem = this.textBox37;
            tableGroup35.Name = "Group1";
            tableGroup35.ReportItem = this.textBox41;
            tableGroup36.Name = "Group2";
            tableGroup36.ReportItem = this.textBox43;
            tableGroup37.Name = "Group3";
            tableGroup37.ReportItem = this.textBox47;
            tableGroup38.Name = "Group4";
            tableGroup38.ReportItem = this.textBox49;
            this.table4.ColumnGroups.Add(tableGroup31);
            this.table4.ColumnGroups.Add(tableGroup32);
            this.table4.ColumnGroups.Add(tableGroup33);
            this.table4.ColumnGroups.Add(tableGroup34);
            this.table4.ColumnGroups.Add(tableGroup35);
            this.table4.ColumnGroups.Add(tableGroup36);
            this.table4.ColumnGroups.Add(tableGroup37);
            this.table4.ColumnGroups.Add(tableGroup38);
            this.table4.DataSource = this.RepairOrderItemDetailDataSource;
            this.table4.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox105,
            this.textBox38,
            this.textBox39,
            this.textBox40,
            this.textBox42,
            this.textBox44,
            this.textBox48,
            this.textBox50,
            this.textBox97,
            this.textBox35,
            this.textBox36,
            this.textBox37,
            this.textBox41,
            this.textBox43,
            this.textBox47,
            this.textBox49});
            this.table4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.00020024616969749332D), Telerik.Reporting.Drawing.Unit.Cm(5.7000002861022949D));
            this.table4.Name = "table4";
            tableGroup39.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup39.Name = "DetailGroup";
            this.table4.RowGroups.Add(tableGroup39);
            this.table4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.920421600341797D), Telerik.Reporting.Drawing.Unit.Cm(1D));
            // 
            // textBox38
            // 
            this.textBox38.CanShrink = true;
            this.textBox38.Name = "textBox38";
            this.textBox38.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.3010119199752808D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox38.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox38.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox38.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox38.Value = "= RowNumber()";
            // 
            // textBox39
            // 
            this.textBox39.CanShrink = true;
            this.textBox39.Name = "textBox39";
            this.textBox39.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.5299997329711914D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox39.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox39.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox39.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox39.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox39.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox39.Value = "= Fields.RepairItemName";
            // 
            // textBox40
            // 
            this.textBox40.CanShrink = true;
            this.textBox40.Name = "textBox40";
            this.textBox40.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.50156307220459D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox40.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox40.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox40.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox40.Value = "= Fields.LaborUnitPrice";
            // 
            // textBox42
            // 
            this.textBox42.CanShrink = true;
            this.textBox42.Name = "textBox42";
            this.textBox42.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.7808948755264282D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox42.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox42.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox42.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox42.StyleName = "";
            this.textBox42.Value = "= Fields.DefaultLaborHour";
            // 
            // textBox44
            // 
            this.textBox44.CanShrink = true;
            this.textBox44.Name = "textBox44";
            this.textBox44.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.0657527446746826D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox44.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox44.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox44.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox44.StyleName = "";
            this.textBox44.Value = "= Fields.LaborCost";
            // 
            // textBox48
            // 
            this.textBox48.CanShrink = true;
            this.textBox48.Name = "textBox48";
            this.textBox48.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9111988544464111D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox48.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox48.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox48.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox48.StyleName = "";
            this.textBox48.Value = resources.GetString("textBox48.Value");
            // 
            // textBox50
            // 
            this.textBox50.CanShrink = true;
            this.textBox50.Name = "textBox50";
            this.textBox50.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.179999828338623D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox50.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox50.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox50.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox50.StyleName = "";
            this.textBox50.Value = resources.GetString("textBox50.Value");
            // 
            // textBox105
            // 
            this.textBox105.CanShrink = true;
            this.textBox105.Name = "textBox105";
            this.textBox105.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.6500000953674316D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox105.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox105.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox105.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox105.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox105.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox105.StyleName = "";
            // 
            // RepairOrderItemDetailDataSource
            // 
            this.RepairOrderItemDetailDataSource.DataMember = "GetRepairOrderItemDetailByIdForPrint";
            this.RepairOrderItemDetailDataSource.DataSource = typeof(Sunlight.Silverlight.Dcs.Web.DcsDomainService);
            this.RepairOrderItemDetailDataSource.Name = "RepairOrderItemDetailDataSource";
            this.RepairOrderItemDetailDataSource.Parameters.AddRange(new Telerik.Reporting.ObjectDataSourceParameter[] {
            new Telerik.Reporting.ObjectDataSourceParameter("RepairOrderId", typeof(int), "= Parameters.paraRepairOrderId.Value")});
            // 
            // table5
            // 
            this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.6500003337860107D)));
            this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.302606463432312D)));
            this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(5.5299997329711914D)));
            this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.496875524520874D)));
            this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.7799997329711914D)));
            this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.0901279449462891D)));
            this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.899999737739563D)));
            this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.1699998378753662D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D)));
            this.table5.Body.SetCellContent(0, 1, this.textBox54);
            this.table5.Body.SetCellContent(0, 2, this.textBox55);
            this.table5.Body.SetCellContent(0, 3, this.textBox56);
            this.table5.Body.SetCellContent(0, 4, this.textBox58);
            this.table5.Body.SetCellContent(0, 5, this.textBox60);
            this.table5.Body.SetCellContent(0, 6, this.textBox62);
            this.table5.Body.SetCellContent(0, 7, this.textBox64);
            this.table5.Body.SetCellContent(0, 0, this.textBox101);
            tableGroup41.Name = "Group13";
            tableGroup41.ReportItem = this.textBox98;
            tableGroup40.ChildGroups.Add(tableGroup41);
            tableGroup40.Name = "Group12";
            tableGroup40.ReportItem = this.textBox34;
            tableGroup43.Name = "Group5";
            tableGroup43.ReportItem = this.textBox51;
            tableGroup42.ChildGroups.Add(tableGroup43);
            tableGroup42.ReportItem = this.textBox81;
            tableGroup45.Name = "Group6";
            tableGroup45.ReportItem = this.textBox52;
            tableGroup44.ChildGroups.Add(tableGroup45);
            tableGroup44.ReportItem = this.textBox91;
            tableGroup47.Name = "Group7";
            tableGroup47.ReportItem = this.textBox53;
            tableGroup46.ChildGroups.Add(tableGroup47);
            tableGroup46.ReportItem = this.textBox92;
            tableGroup49.Name = "Group8";
            tableGroup49.ReportItem = this.textBox57;
            tableGroup48.ChildGroups.Add(tableGroup49);
            tableGroup48.Name = "Group1";
            tableGroup48.ReportItem = this.textBox93;
            tableGroup51.Name = "Group9";
            tableGroup51.ReportItem = this.textBox59;
            tableGroup50.ChildGroups.Add(tableGroup51);
            tableGroup50.Name = "Group2";
            tableGroup50.ReportItem = this.textBox94;
            tableGroup53.Name = "Group10";
            tableGroup53.ReportItem = this.textBox61;
            tableGroup52.ChildGroups.Add(tableGroup53);
            tableGroup52.Name = "Group3";
            tableGroup52.ReportItem = this.textBox95;
            tableGroup55.Name = "Group11";
            tableGroup55.ReportItem = this.textBox63;
            tableGroup54.ChildGroups.Add(tableGroup55);
            tableGroup54.Name = "Group4";
            tableGroup54.ReportItem = this.textBox96;
            this.table5.ColumnGroups.Add(tableGroup40);
            this.table5.ColumnGroups.Add(tableGroup42);
            this.table5.ColumnGroups.Add(tableGroup44);
            this.table5.ColumnGroups.Add(tableGroup46);
            this.table5.ColumnGroups.Add(tableGroup48);
            this.table5.ColumnGroups.Add(tableGroup50);
            this.table5.ColumnGroups.Add(tableGroup52);
            this.table5.ColumnGroups.Add(tableGroup54);
            this.table5.DataSource = this.RepairOrderForDealerDetailDataSource;
            this.table5.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox101,
            this.textBox54,
            this.textBox55,
            this.textBox56,
            this.textBox58,
            this.textBox60,
            this.textBox62,
            this.textBox64,
            this.textBox34,
            this.textBox98,
            this.textBox81,
            this.textBox51,
            this.textBox91,
            this.textBox52,
            this.textBox92,
            this.textBox53,
            this.textBox93,
            this.textBox57,
            this.textBox94,
            this.textBox59,
            this.textBox95,
            this.textBox61,
            this.textBox96,
            this.textBox63});
            this.table5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.00010012308484874666D), Telerik.Reporting.Drawing.Unit.Cm(6.7000002861022949D));
            this.table5.Name = "table5";
            tableGroup56.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup56.Name = "DetailGroup";
            this.table5.RowGroups.Add(tableGroup56);
            this.table5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.919609069824219D), Telerik.Reporting.Drawing.Unit.Cm(1.5D));
            // 
            // textBox54
            // 
            this.textBox54.CanShrink = true;
            this.textBox54.Name = "textBox54";
            this.textBox54.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.3026062250137329D), Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D));
            this.textBox54.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox54.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox54.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox54.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox54.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox54.Value = "= RowNumber()";
            // 
            // textBox55
            // 
            this.textBox55.CanShrink = true;
            this.textBox55.Name = "textBox55";
            this.textBox55.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.53000020980835D), Telerik.Reporting.Drawing.Unit.Cm(0.49999994039535522D));
            this.textBox55.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox55.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox55.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox55.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox55.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox55.Value = "= Fields.NewPartsName";
            // 
            // textBox56
            // 
            this.textBox56.CanShrink = true;
            this.textBox56.Name = "textBox56";
            this.textBox56.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.4968748092651367D), Telerik.Reporting.Drawing.Unit.Cm(0.49999994039535522D));
            this.textBox56.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox56.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox56.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox56.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox56.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox56.Value = "= Fields.NewPartsCode";
            // 
            // textBox58
            // 
            this.textBox58.CanShrink = true;
            this.textBox58.Name = "textBox58";
            this.textBox58.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.7799999713897705D), Telerik.Reporting.Drawing.Unit.Cm(0.49999994039535522D));
            this.textBox58.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox58.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox58.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox58.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox58.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox58.StyleName = "";
            this.textBox58.Value = "= Fields.ActuralMaterialCost";
            // 
            // textBox60
            // 
            this.textBox60.CanShrink = true;
            this.textBox60.Name = "textBox60";
            this.textBox60.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.0901286602020264D), Telerik.Reporting.Drawing.Unit.Cm(0.49999994039535522D));
            this.textBox60.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox60.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox60.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox60.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox60.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox60.StyleName = "";
            this.textBox60.Value = "= Fields.Quantity";
            // 
            // textBox62
            // 
            this.textBox62.CanShrink = true;
            this.textBox62.Name = "textBox62";
            this.textBox62.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9000000953674316D), Telerik.Reporting.Drawing.Unit.Cm(0.49999994039535522D));
            this.textBox62.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox62.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox62.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox62.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox62.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox62.StyleName = "";
            this.textBox62.Value = resources.GetString("textBox62.Value");
            // 
            // textBox64
            // 
            this.textBox64.CanShrink = true;
            this.textBox64.Name = "textBox64";
            this.textBox64.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.1699998378753662D), Telerik.Reporting.Drawing.Unit.Cm(0.49999994039535522D));
            this.textBox64.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox64.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox64.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox64.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox64.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox64.StyleName = "";
            this.textBox64.Value = resources.GetString("textBox64.Value");
            // 
            // textBox101
            // 
            this.textBox101.CanShrink = true;
            this.textBox101.Name = "textBox101";
            this.textBox101.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.6500000953674316D), Telerik.Reporting.Drawing.Unit.Cm(0.49999994039535522D));
            this.textBox101.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox101.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox101.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox101.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox101.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox101.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox101.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox101.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox101.StyleName = "";
            // 
            // RepairOrderForDealerDetailDataSource
            // 
            this.RepairOrderForDealerDetailDataSource.DataMember = "GetRepairOrderMaterialDetailByIdForPrint";
            this.RepairOrderForDealerDetailDataSource.DataSource = typeof(Sunlight.Silverlight.Dcs.Web.DcsDomainService);
            this.RepairOrderForDealerDetailDataSource.Name = "RepairOrderForDealerDetailDataSource";
            this.RepairOrderForDealerDetailDataSource.Parameters.AddRange(new Telerik.Reporting.ObjectDataSourceParameter[] {
            new Telerik.Reporting.ObjectDataSourceParameter("RepairOrderId", typeof(int), "= Parameters.paraRepairOrderId.Value")});
            // 
            // table6
            // 
            this.table6.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.6500003337860107D)));
            this.table6.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.4282059669494629D)));
            this.table6.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.6175532341003418D)));
            this.table6.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(4.1400008201599121D)));
            this.table6.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.8536226749420166D)));
            this.table6.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.0802180767059326D)));
            this.table6.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.1460562944412231D)));
            this.table6.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D)));
            this.table6.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D)));
            this.table6.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D)));
            this.table6.Body.SetCellContent(0, 1, this.textBox72);
            this.table6.Body.SetCellContent(1, 1, this.textBox66);
            this.table6.Body.SetCellContent(1, 2, this.textBox67);
            this.table6.Body.SetCellContent(2, 1, this.textBox68);
            this.table6.Body.SetCellContent(2, 2, this.textBox69);
            this.table6.Body.SetCellContent(0, 0, this.textBox71, 3, 1);
            this.table6.Body.SetCellContent(1, 3, this.textBox70);
            this.table6.Body.SetCellContent(2, 3, this.textBox74);
            this.table6.Body.SetCellContent(1, 4, this.textBox76);
            this.table6.Body.SetCellContent(2, 4, this.textBox77);
            this.table6.Body.SetCellContent(1, 5, this.textBox79);
            this.table6.Body.SetCellContent(2, 5, this.textBox80);
            this.table6.Body.SetCellContent(1, 6, this.textBox82);
            this.table6.Body.SetCellContent(2, 6, this.textBox83);
            this.table6.Body.SetCellContent(0, 2, this.textBox73, 1, 5);
            tableGroup60.Name = "Group4";
            tableGroup61.Name = "Group5";
            tableGroup62.Name = "Group6";
            tableGroup63.Name = "Group7";
            this.table6.ColumnGroups.Add(tableGroup57);
            this.table6.ColumnGroups.Add(tableGroup58);
            this.table6.ColumnGroups.Add(tableGroup59);
            this.table6.ColumnGroups.Add(tableGroup60);
            this.table6.ColumnGroups.Add(tableGroup61);
            this.table6.ColumnGroups.Add(tableGroup62);
            this.table6.ColumnGroups.Add(tableGroup63);
            this.table6.DataSource = this.RepairOrderForDealerDataSource;
            this.table6.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox71,
            this.textBox72,
            this.textBox73,
            this.textBox66,
            this.textBox67,
            this.textBox70,
            this.textBox76,
            this.textBox79,
            this.textBox82,
            this.textBox68,
            this.textBox69,
            this.textBox74,
            this.textBox77,
            this.textBox80,
            this.textBox83});
            this.table6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(8.2000007629394531D));
            this.table6.Name = "table6";
            tableGroup65.Name = "Group2";
            tableGroup66.Name = "Group1";
            tableGroup67.Name = "Group3";
            tableGroup64.ChildGroups.Add(tableGroup65);
            tableGroup64.ChildGroups.Add(tableGroup66);
            tableGroup64.ChildGroups.Add(tableGroup67);
            tableGroup64.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup64.Name = "DetailGroup";
            this.table6.RowGroups.Add(tableGroup64);
            this.table6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.915657043457031D), Telerik.Reporting.Drawing.Unit.Cm(1.5D));
            // 
            // textBox72
            // 
            this.textBox72.Name = "textBox72";
            this.textBox72.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.4282042980194092D), Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D));
            this.textBox72.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox72.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox72.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox72.StyleName = "";
            this.textBox72.Value = "外出故障地";
            // 
            // textBox66
            // 
            this.textBox66.Name = "textBox66";
            this.textBox66.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.4282042980194092D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox66.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox66.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox66.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox66.StyleName = "";
            this.textBox66.Value = "外出里程";
            // 
            // textBox67
            // 
            this.textBox67.Name = "textBox67";
            this.textBox67.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.6175520420074463D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox67.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox67.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox67.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox67.StyleName = "";
            this.textBox67.Value = "= Fields.OutRange";
            // 
            // textBox68
            // 
            this.textBox68.Name = "textBox68";
            this.textBox68.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.4282042980194092D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox68.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox68.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox68.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox68.StyleName = "";
            this.textBox68.Value = "应收外出服务费";
            // 
            // textBox69
            // 
            this.textBox69.Name = "textBox69";
            this.textBox69.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.6175520420074463D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox69.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox69.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox69.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox69.StyleName = "";
            this.textBox69.Value = "= Fields.OtherTrafficFee+Fields.TowCharge+Fields.FieldServiceCharge";
            // 
            // textBox71
            // 
            this.textBox71.CanShrink = true;
            this.textBox71.Name = "textBox71";
            this.textBox71.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.6500000953674316D), Telerik.Reporting.Drawing.Unit.Cm(1.5D));
            this.textBox71.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox71.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox71.Style.Font.Bold = true;
            this.textBox71.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox71.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox71.StyleName = "";
            this.textBox71.Value = "外出信息";
            // 
            // textBox70
            // 
            this.textBox70.Name = "textBox70";
            this.textBox70.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.1400008201599121D), Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D));
            this.textBox70.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox70.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox70.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox70.StyleName = "";
            this.textBox70.Value = "外出天数";
            // 
            // textBox74
            // 
            this.textBox74.Name = "textBox74";
            this.textBox74.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.1400008201599121D), Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D));
            this.textBox74.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox74.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox74.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox74.StyleName = "";
            this.textBox74.Value = "客户是否承担外出服务费";
            // 
            // textBox76
            // 
            this.textBox76.Name = "textBox76";
            this.textBox76.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.8536211252212524D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox76.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox76.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox76.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox76.StyleName = "";
            this.textBox76.Value = "= Fields.ServiceTripDuration";
            // 
            // textBox77
            // 
            this.textBox77.Name = "textBox77";
            this.textBox77.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.8536211252212524D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox77.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox77.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox77.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox77.StyleName = "";
            this.textBox77.Value = "= Sunlight.Silverlight.Dcs.Web.Reporting.CommonConverter.GetOutClaimSupplierName(" +
    "Fields.OutClaimSupplierName)";
            // 
            // textBox79
            // 
            this.textBox79.Name = "textBox79";
            this.textBox79.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.0802178382873535D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox79.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox79.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox79.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox79.StyleName = "";
            this.textBox79.Value = "外出人数";
            // 
            // textBox80
            // 
            this.textBox80.Name = "textBox80";
            this.textBox80.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.0802178382873535D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox80.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox80.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox80.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox80.StyleName = "";
            this.textBox80.Value = "实收外出服务费";
            // 
            // textBox82
            // 
            this.textBox82.Name = "textBox82";
            this.textBox82.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.1460561752319336D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox82.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox82.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox82.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox82.StyleName = "";
            this.textBox82.Value = "= Fields.ServiceTripPerson";
            // 
            // textBox83
            // 
            this.textBox83.Name = "textBox83";
            this.textBox83.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.1460561752319336D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox83.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox83.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox83.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox83.StyleName = "";
            this.textBox83.Value = "= Fields.OtherTrafficFee+Fields.TowCharge+Fields.FieldServiceCharge-(Fields.Membe" +
    "rRightsCost??0)";
            // 
            // textBox73
            // 
            this.textBox73.Name = "textBox73";
            this.textBox73.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(13.837448120117188D), Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D));
            this.textBox73.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox73.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox73.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox73.StyleName = "";
            this.textBox73.Value = "= Fields.RegionID";
            // 
            // table8
            // 
            this.table8.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.6500003337860107D)));
            this.table8.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(7.9400010108947754D)));
            this.table8.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(9.3333950042724609D)));
            this.table8.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.49999991059303284D)));
            this.table8.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.49999991059303284D)));
            this.table8.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.809499979019165D)));
            this.table8.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.49999991059303284D)));
            this.table8.Body.SetCellContent(0, 0, this.textBox103, 2, 1);
            this.table8.Body.SetCellContent(0, 1, this.textBox104, 2, 2);
            this.table8.Body.SetCellContent(3, 0, this.textBox106, 1, 3);
            this.table8.Body.SetCellContent(2, 0, this.textBox100);
            this.table8.Body.SetCellContent(2, 1, this.textBox122, 1, 2);
            this.table8.ColumnGroups.Add(tableGroup68);
            this.table8.ColumnGroups.Add(tableGroup69);
            this.table8.ColumnGroups.Add(tableGroup70);
            this.table8.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox103,
            this.textBox104,
            this.textBox100,
            this.textBox122,
            this.textBox106});
            this.table8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(13.195500373840332D));
            this.table8.Name = "table8";
            tableGroup72.Name = "Group2";
            tableGroup73.Name = "Group1";
            tableGroup74.Name = "Group3";
            tableGroup75.Name = "Group4";
            tableGroup71.ChildGroups.Add(tableGroup72);
            tableGroup71.ChildGroups.Add(tableGroup73);
            tableGroup71.ChildGroups.Add(tableGroup74);
            tableGroup71.ChildGroups.Add(tableGroup75);
            tableGroup71.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup71.Name = "DetailGroup";
            this.table8.RowGroups.Add(tableGroup71);
            this.table8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.923397064208984D), Telerik.Reporting.Drawing.Unit.Cm(2.3094997406005859D));
            // 
            // textBox103
            // 
            this.textBox103.Name = "textBox103";
            this.textBox103.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.6500000953674316D), Telerik.Reporting.Drawing.Unit.Cm(1D));
            this.textBox103.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox103.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox103.Style.Font.Bold = true;
            this.textBox103.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox103.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox103.StyleName = "";
            this.textBox103.Value = "其他事项";
            // 
            // textBox104
            // 
            this.textBox104.Name = "textBox104";
            this.textBox104.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(17.273399353027344D), Telerik.Reporting.Drawing.Unit.Cm(0.99999994039535522D));
            this.textBox104.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox104.StyleName = "";
            // 
            // textBox106
            // 
            this.textBox106.Name = "textBox106";
            this.textBox106.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.923398971557617D), Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D));
            this.textBox106.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox106.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox106.Style.Font.Bold = true;
            this.textBox106.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox106.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox106.StyleName = "";
            this.textBox106.Value = "说明:本单一式三联,用户凭此单第一联提车,请妥善保管。";
            // 
            // textBox100
            // 
            this.textBox100.Name = "textBox100";
            this.textBox100.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.6500000953674316D), Telerik.Reporting.Drawing.Unit.Cm(0.809499979019165D));
            this.textBox100.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox100.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox100.Style.Font.Bold = true;
            this.textBox100.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox100.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox100.StyleName = "";
            this.textBox100.Value = "用户意见及\r\n签字";
            // 
            // textBox122
            // 
            this.textBox122.Name = "textBox122";
            this.textBox122.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(17.273397445678711D), Telerik.Reporting.Drawing.Unit.Cm(0.809499979019165D));
            this.textBox122.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox122.Style.Font.Bold = true;
            this.textBox122.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox122.StyleName = "";
            // 
            // table7
            // 
            this.table7.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.0499997138977051D)));
            this.table7.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(5.1915245056152344D)));
            this.table7.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.0500001907348633D)));
            this.table7.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(6.09999942779541D)));
            this.table7.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.7400002479553223D)));
            this.table7.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.7992868423461914D)));
            this.table7.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.5D)));
            this.table7.Body.SetCellContent(0, 0, this.textBox111);
            this.table7.Body.SetCellContent(0, 1, this.textBox112);
            this.table7.Body.SetCellContent(0, 2, this.textBox113);
            this.table7.Body.SetCellContent(0, 3, this.textBox115);
            this.table7.Body.SetCellContent(0, 4, this.textBox117);
            this.table7.Body.SetCellContent(0, 5, this.textBox119);
            tableGroup76.ReportItem = this.textBox108;
            tableGroup77.ReportItem = this.textBox109;
            tableGroup78.ReportItem = this.textBox110;
            tableGroup79.Name = "Group1";
            tableGroup79.ReportItem = this.textBox114;
            tableGroup80.Name = "Group2";
            tableGroup80.ReportItem = this.textBox116;
            tableGroup81.Name = "Group3";
            tableGroup81.ReportItem = this.textBox118;
            this.table7.ColumnGroups.Add(tableGroup76);
            this.table7.ColumnGroups.Add(tableGroup77);
            this.table7.ColumnGroups.Add(tableGroup78);
            this.table7.ColumnGroups.Add(tableGroup79);
            this.table7.ColumnGroups.Add(tableGroup80);
            this.table7.ColumnGroups.Add(tableGroup81);
            this.table7.DataSource = this.RepairOrderForDealerDataSource;
            this.table7.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox111,
            this.textBox112,
            this.textBox113,
            this.textBox115,
            this.textBox117,
            this.textBox119,
            this.textBox108,
            this.textBox109,
            this.textBox110,
            this.textBox114,
            this.textBox116,
            this.textBox118});
            this.table7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(15.505504608154297D));
            this.table7.Name = "table7";
            tableGroup82.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup82.Name = "DetailGroup";
            this.table7.RowGroups.Add(tableGroup82);
            this.table7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.930810928344727D), Telerik.Reporting.Drawing.Unit.Cm(1D));
            // 
            // textBox111
            // 
            this.textBox111.Name = "textBox111";
            this.textBox111.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.0499999523162842D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            // 
            // textBox112
            // 
            this.textBox112.Name = "textBox112";
            this.textBox112.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.1915273666381836D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            // 
            // textBox113
            // 
            this.textBox113.Name = "textBox113";
            this.textBox113.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.0499999523162842D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            // 
            // textBox115
            // 
            this.textBox115.Name = "textBox115";
            this.textBox115.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.1000003814697266D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox115.StyleName = "";
            // 
            // textBox117
            // 
            this.textBox117.Name = "textBox117";
            this.textBox117.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.7400000095367432D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox117.StyleName = "";
            // 
            // textBox119
            // 
            this.textBox119.Name = "textBox119";
            this.textBox119.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.7992854118347168D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox119.StyleName = "";
            // 
            // table9
            // 
            this.table9.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.75636559724807739D)));
            this.table9.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.8939316272735596D)));
            this.table9.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.424821138381958D)));
            this.table9.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.6186933517456055D)));
            this.table9.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(4.139488697052002D)));
            this.table9.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.8542051315307617D)));
            this.table9.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.0696232318878174D)));
            this.table9.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.17287278175354D)));
            this.table9.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D)));
            this.table9.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D)));
            this.table9.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D)));
            this.table9.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D)));
            this.table9.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D)));
            this.table9.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D)));
            this.table9.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D)));
            this.table9.Body.SetCellContent(0, 2, this.textBox85);
            this.table9.Body.SetCellContent(0, 3, this.textBox86);
            this.table9.Body.SetCellContent(0, 4, this.textBox87);
            this.table9.Body.SetCellContent(0, 5, this.textBox88);
            this.table9.Body.SetCellContent(0, 6, this.textBox89);
            this.table9.Body.SetCellContent(0, 7, this.textBox90);
            this.table9.Body.SetCellContent(1, 2, this.textBox140);
            this.table9.Body.SetCellContent(1, 3, this.textBox141);
            this.table9.Body.SetCellContent(1, 4, this.textBox142);
            this.table9.Body.SetCellContent(1, 5, this.textBox143);
            this.table9.Body.SetCellContent(1, 6, this.textBox144);
            this.table9.Body.SetCellContent(1, 7, this.textBox145);
            this.table9.Body.SetCellContent(2, 2, this.textBox148);
            this.table9.Body.SetCellContent(2, 3, this.textBox149);
            this.table9.Body.SetCellContent(2, 4, this.textBox150);
            this.table9.Body.SetCellContent(2, 5, this.textBox151);
            this.table9.Body.SetCellContent(2, 6, this.textBox152);
            this.table9.Body.SetCellContent(2, 7, this.textBox153);
            this.table9.Body.SetCellContent(3, 2, this.textBox164);
            this.table9.Body.SetCellContent(3, 3, this.textBox165);
            this.table9.Body.SetCellContent(3, 4, this.textBox166);
            this.table9.Body.SetCellContent(3, 5, this.textBox167);
            this.table9.Body.SetCellContent(3, 6, this.textBox168);
            this.table9.Body.SetCellContent(3, 7, this.textBox169);
            this.table9.Body.SetCellContent(0, 1, this.textBox135, 2, 1);
            this.table9.Body.SetCellContent(2, 1, this.textBox147, 2, 1);
            this.table9.Body.SetCellContent(6, 1, this.textBox75);
            this.table9.Body.SetCellContent(6, 2, this.textBox78);
            this.table9.Body.SetCellContent(0, 0, this.textBox137, 7, 1);
            this.table9.Body.SetCellContent(6, 3, this.textBox84, 1, 5);
            this.table9.Body.SetCellContent(4, 1, this.textBox155, 2, 1);
            this.table9.Body.SetCellContent(4, 2, this.textBox156, 2, 1);
            this.table9.Body.SetCellContent(4, 3, this.textBox157, 2, 5);
            tableGroup83.Name = "group5";
            tableGroup84.Name = "group4";
            tableGroup88.Name = "Group1";
            tableGroup89.Name = "Group2";
            tableGroup90.Name = "Group3";
            this.table9.ColumnGroups.Add(tableGroup83);
            this.table9.ColumnGroups.Add(tableGroup84);
            this.table9.ColumnGroups.Add(tableGroup85);
            this.table9.ColumnGroups.Add(tableGroup86);
            this.table9.ColumnGroups.Add(tableGroup87);
            this.table9.ColumnGroups.Add(tableGroup88);
            this.table9.ColumnGroups.Add(tableGroup89);
            this.table9.ColumnGroups.Add(tableGroup90);
            this.table9.DataSource = this.VirtualRepairOrderMaterialDetailDataSource;
            this.table9.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox137,
            this.textBox135,
            this.textBox85,
            this.textBox86,
            this.textBox87,
            this.textBox88,
            this.textBox89,
            this.textBox90,
            this.textBox140,
            this.textBox141,
            this.textBox142,
            this.textBox143,
            this.textBox144,
            this.textBox145,
            this.textBox147,
            this.textBox148,
            this.textBox149,
            this.textBox150,
            this.textBox151,
            this.textBox152,
            this.textBox153,
            this.textBox164,
            this.textBox165,
            this.textBox166,
            this.textBox167,
            this.textBox168,
            this.textBox169,
            this.textBox155,
            this.textBox156,
            this.textBox157,
            this.textBox75,
            this.textBox78,
            this.textBox84});
            this.table9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(9.7002010345458984D));
            this.table9.Name = "table9";
            tableGroup91.Name = "Group4";
            tableGroup92.Name = "group6";
            tableGroup93.Name = "group7";
            tableGroup94.Name = "group9";
            tableGroup95.Name = "group8";
            tableGroup96.Name = "group11";
            tableGroup97.Name = "group10";
            this.table9.RowGroups.Add(tableGroup91);
            this.table9.RowGroups.Add(tableGroup92);
            this.table9.RowGroups.Add(tableGroup93);
            this.table9.RowGroups.Add(tableGroup94);
            this.table9.RowGroups.Add(tableGroup95);
            this.table9.RowGroups.Add(tableGroup96);
            this.table9.RowGroups.Add(tableGroup97);
            this.table9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.930002212524414D), Telerik.Reporting.Drawing.Unit.Cm(3.5D));
            // 
            // textBox85
            // 
            this.textBox85.Name = "textBox85";
            this.textBox85.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.4248208999633789D), Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D));
            this.textBox85.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox85.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox85.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox85.StyleName = "";
            this.textBox85.Value = "应收工时费小计";
            // 
            // textBox86
            // 
            this.textBox86.Name = "textBox86";
            this.textBox86.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.6186931133270264D), Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D));
            this.textBox86.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox86.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox86.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox86.StyleName = "";
            this.textBox86.Value = "= Fields.TotalActualLaborCost";
            // 
            // textBox87
            // 
            this.textBox87.Name = "textBox87";
            this.textBox87.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.1394882202148438D), Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D));
            this.textBox87.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox87.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox87.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox87.StyleName = "";
            this.textBox87.Value = "应收材料费小计";
            // 
            // textBox88
            // 
            this.textBox88.Name = "textBox88";
            this.textBox88.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.8542051315307617D), Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D));
            this.textBox88.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox88.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox88.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox88.StyleName = "";
            this.textBox88.Value = "= Fields.TotalActuralMaterialCost";
            // 
            // textBox89
            // 
            this.textBox89.Name = "textBox89";
            this.textBox89.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.0696229934692383D), Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D));
            this.textBox89.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox89.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox89.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox89.StyleName = "";
            this.textBox89.Value = "应收外出服务费";
            // 
            // textBox90
            // 
            this.textBox90.Name = "textBox90";
            this.textBox90.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.1728726625442505D), Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D));
            this.textBox90.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox90.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox90.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox90.StyleName = "";
            this.textBox90.Value = "= Fields.FieldServiceCharge";
            // 
            // textBox140
            // 
            this.textBox140.Name = "textBox140";
            this.textBox140.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.4248206615448D), Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D));
            this.textBox140.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox140.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox140.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox140.StyleName = "";
            this.textBox140.Value = "其他费用小计";
            // 
            // textBox141
            // 
            this.textBox141.Name = "textBox141";
            this.textBox141.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.6186928749084473D), Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D));
            this.textBox141.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox141.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox141.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox141.StyleName = "";
            this.textBox141.Value = "= Fields.OtherCost";
            // 
            // textBox142
            // 
            this.textBox142.Name = "textBox142";
            this.textBox142.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.1394882202148438D), Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D));
            this.textBox142.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox142.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox142.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox142.StyleName = "";
            this.textBox142.Value = "应收费用小计";
            // 
            // textBox143
            // 
            this.textBox143.Name = "textBox143";
            this.textBox143.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.8542050123214722D), Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D));
            this.textBox143.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox143.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox143.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox143.StyleName = "";
            this.textBox143.Value = "= Fields.Total";
            // 
            // textBox144
            // 
            this.textBox144.Name = "textBox144";
            this.textBox144.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.0696229934692383D), Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D));
            this.textBox144.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox144.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox144.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox144.StyleName = "";
            // 
            // textBox145
            // 
            this.textBox145.Name = "textBox145";
            this.textBox145.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.1728725433349609D), Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D));
            this.textBox145.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox145.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox145.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox145.StyleName = "";
            // 
            // textBox148
            // 
            this.textBox148.Name = "textBox148";
            this.textBox148.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.4248213768005371D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox148.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox148.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox148.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox148.StyleName = "";
            this.textBox148.Value = "积分抵扣费小计";
            // 
            // textBox149
            // 
            this.textBox149.Name = "textBox149";
            this.textBox149.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.6186935901641846D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox149.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox149.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox149.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox149.StyleName = "";
            this.textBox149.Value = "= Fields.MemberUsedIntegral";
            // 
            // textBox150
            // 
            this.textBox150.Name = "textBox150";
            this.textBox150.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.139488697052002D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox150.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox150.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox150.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox150.StyleName = "";
            this.textBox150.Value = "工时折扣抵扣费小计";
            // 
            // textBox151
            // 
            this.textBox151.Name = "textBox151";
            this.textBox151.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.8542052507400513D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox151.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox151.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox151.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox151.StyleName = "";
            this.textBox151.Value = "= Fields.CostDiscount";
            // 
            // textBox152
            // 
            this.textBox152.Name = "textBox152";
            this.textBox152.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.0696234703063965D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox152.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox152.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox152.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox152.StyleName = "";
            this.textBox152.Value = "免费救援抵扣费小计";
            // 
            // textBox153
            // 
            this.textBox153.Name = "textBox153";
            this.textBox153.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.17287278175354D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox153.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox153.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox153.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox153.StyleName = "";
            this.textBox153.Value = "= Fields.MemberRightsCost";
            // 
            // textBox164
            // 
            this.textBox164.Name = "textBox164";
            this.textBox164.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.424821138381958D), Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D));
            this.textBox164.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox164.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox164.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox164.StyleName = "";
            this.textBox164.Value = "会员抵扣费用小计";
            // 
            // textBox165
            // 
            this.textBox165.Name = "textBox165";
            this.textBox165.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.6186933517456055D), Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D));
            this.textBox165.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox165.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox165.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox165.StyleName = "";
            this.textBox165.Value = "= Fields.TotalMember";
            // 
            // textBox166
            // 
            this.textBox166.Name = "textBox166";
            this.textBox166.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.1394882202148438D), Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D));
            this.textBox166.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox166.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox166.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox166.StyleName = "";
            // 
            // textBox167
            // 
            this.textBox167.Name = "textBox167";
            this.textBox167.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.8542051315307617D), Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D));
            this.textBox167.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox167.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox167.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox167.StyleName = "";
            // 
            // textBox168
            // 
            this.textBox168.Name = "textBox168";
            this.textBox168.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.0696232318878174D), Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D));
            this.textBox168.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox168.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox168.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox168.StyleName = "";
            // 
            // textBox169
            // 
            this.textBox169.Name = "textBox169";
            this.textBox169.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.1728726625442505D), Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D));
            this.textBox169.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox169.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox169.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox169.StyleName = "";
            // 
            // textBox135
            // 
            this.textBox135.Name = "textBox135";
            this.textBox135.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.89393150806427D), Telerik.Reporting.Drawing.Unit.Cm(0.99999994039535522D));
            this.textBox135.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox135.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox135.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox135.StyleName = "";
            this.textBox135.Value = "应收费用";
            // 
            // textBox147
            // 
            this.textBox147.Name = "textBox147";
            this.textBox147.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.89393150806427D), Telerik.Reporting.Drawing.Unit.Cm(0.99999994039535522D));
            this.textBox147.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox147.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox147.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox147.StyleName = "";
            this.textBox147.Value = "会员抵扣费用";
            // 
            // textBox75
            // 
            this.textBox75.Name = "textBox75";
            this.textBox75.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.8939316272735596D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox75.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox75.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox75.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox75.StyleName = "";
            this.textBox75.Value = "实收费用";
            // 
            // textBox78
            // 
            this.textBox78.Name = "textBox78";
            this.textBox78.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.424821138381958D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox78.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox78.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox78.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox78.StyleName = "";
            this.textBox78.Value = "实收费用合计";
            // 
            // textBox137
            // 
            this.textBox137.Name = "textBox137";
            this.textBox137.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.75636559724807739D), Telerik.Reporting.Drawing.Unit.Cm(3.5D));
            this.textBox137.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox137.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox137.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox137.StyleName = "";
            this.textBox137.Value = "结算信息";
            // 
            // textBox84
            // 
            this.textBox84.Name = "textBox84";
            this.textBox84.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(13.854883193969727D), Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D));
            this.textBox84.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox84.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox84.StyleName = "";
            this.textBox84.Value = "= Fields.DeductionTotalAmount";
            // 
            // textBox155
            // 
            this.textBox155.Name = "textBox155";
            this.textBox155.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.8939317464828491D), Telerik.Reporting.Drawing.Unit.Cm(1.0000001192092896D));
            this.textBox155.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox155.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox155.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox155.StyleName = "";
            this.textBox155.Value = "红包抵扣费用";
            // 
            // textBox156
            // 
            this.textBox156.Name = "textBox156";
            this.textBox156.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.4248213768005371D), Telerik.Reporting.Drawing.Unit.Cm(1.0000001192092896D));
            this.textBox156.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox156.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox156.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox156.StyleName = "";
            this.textBox156.Value = "红包金额";
            // 
            // textBox157
            // 
            this.textBox157.Name = "textBox157";
            this.textBox157.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(13.854884147644043D), Telerik.Reporting.Drawing.Unit.Cm(1.0000001192092896D));
            this.textBox157.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox157.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox157.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox157.StyleName = "";
            this.textBox157.Value = "= Fields.RedPacketsUsedPrice";
            // 
            // VirtualRepairOrderMaterialDetailDataSource
            // 
            this.VirtualRepairOrderMaterialDetailDataSource.DataMember = "GetVirtualRepairOrderMaterialByRepairOrderId";
            this.VirtualRepairOrderMaterialDetailDataSource.DataSource = typeof(Sunlight.Silverlight.Dcs.Web.DcsDomainService);
            this.VirtualRepairOrderMaterialDetailDataSource.Name = "VirtualRepairOrderMaterialDetailDataSource";
            this.VirtualRepairOrderMaterialDetailDataSource.Parameters.AddRange(new Telerik.Reporting.ObjectDataSourceParameter[] {
            new Telerik.Reporting.ObjectDataSourceParameter("RepairOrderId", typeof(int), "= Parameters.paraRepairOrderId.Value")});
            // 
            // table10
            // 
            this.table10.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.6500003337860107D)));
            this.table10.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.3010119199752808D)));
            this.table10.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(5.5299997329711914D)));
            this.table10.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.50156307220459D)));
            this.table10.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.7808952331542969D)));
            this.table10.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.0657527446746826D)));
            this.table10.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.9111989736557007D)));
            this.table10.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.179999828338623D)));
            this.table10.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.5D)));
            this.table10.Body.SetCellContent(0, 0, this.textBox139);
            this.table10.Body.SetCellContent(0, 1, this.textBox65, 1, 3);
            this.table10.Body.SetCellContent(0, 4, this.textBox107, 1, 4);
            tableGroup98.Name = "Group5";
            tableGroup98.ReportItem = this.textBox146;
            tableGroup100.Name = "group12";
            tableGroup99.ChildGroups.Add(tableGroup100);
            tableGroup99.ChildGroups.Add(tableGroup101);
            tableGroup99.ChildGroups.Add(tableGroup102);
            tableGroup99.ReportItem = this.textBox154;
            tableGroup104.Name = "group13";
            tableGroup105.Name = "Group2";
            tableGroup106.Name = "Group3";
            tableGroup107.Name = "Group4";
            tableGroup103.ChildGroups.Add(tableGroup104);
            tableGroup103.ChildGroups.Add(tableGroup105);
            tableGroup103.ChildGroups.Add(tableGroup106);
            tableGroup103.ChildGroups.Add(tableGroup107);
            tableGroup103.Name = "Group1";
            tableGroup103.ReportItem = this.textBox160;
            this.table10.ColumnGroups.Add(tableGroup98);
            this.table10.ColumnGroups.Add(tableGroup99);
            this.table10.ColumnGroups.Add(tableGroup103);
            this.table10.DataSource = this.GetRepairMemberRightsDetails;
            this.table10.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox139,
            this.textBox65,
            this.textBox107,
            this.textBox146,
            this.textBox154,
            this.textBox160});
            this.table10.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(3.7000000476837158D));
            this.table10.Name = "table10";
            tableGroup108.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup108.Name = "DetailGroup";
            this.table10.RowGroups.Add(tableGroup108);
            this.table10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.920421600341797D), Telerik.Reporting.Drawing.Unit.Cm(1D));
            // 
            // textBox139
            // 
            this.textBox139.CanShrink = true;
            this.textBox139.Name = "textBox139";
            this.textBox139.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.6500000953674316D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox139.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox139.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox139.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox139.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox139.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox139.StyleName = "";
            // 
            // textBox65
            // 
            this.textBox65.CanShrink = true;
            this.textBox65.Name = "textBox65";
            this.textBox65.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.3325748443603516D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox65.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox65.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox65.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox65.Value = "= Fields.RightsItem";
            // 
            // textBox107
            // 
            this.textBox107.CanShrink = true;
            this.textBox107.Name = "textBox107";
            this.textBox107.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.9378466606140137D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox107.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox107.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox107.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox107.StyleName = "";
            this.textBox107.Value = "= Sunlight.Silverlight.Dcs.Web.Reporting.CommonConverter.GetBoolConvertFormat(CBo" +
    "ol(Fields.IsRights))";
            // 
            // GetRepairMemberRightsDetails
            // 
            this.GetRepairMemberRightsDetails.DataMember = "GetRepairMemberRightsDetailsForPrint";
            this.GetRepairMemberRightsDetails.DataSource = typeof(Sunlight.Silverlight.Dcs.Web.DcsDomainService);
            this.GetRepairMemberRightsDetails.Name = "GetRepairMemberRightsDetails";
            this.GetRepairMemberRightsDetails.Parameters.AddRange(new Telerik.Reporting.ObjectDataSourceParameter[] {
            new Telerik.Reporting.ObjectDataSourceParameter("repairOrderId", typeof(int), "= Parameters.paraRepairOrderId.Value")});
            // 
            // objectDataSource1
            // 
            this.objectDataSource1.DataMember = "GetRepairOrderItemDetailByIdForPrint";
            this.objectDataSource1.DataSource = typeof(Sunlight.Silverlight.Dcs.Web.DcsDomainService);
            this.objectDataSource1.Name = "objectDataSource1";
            this.objectDataSource1.Parameters.AddRange(new Telerik.Reporting.ObjectDataSourceParameter[] {
            new Telerik.Reporting.ObjectDataSourceParameter("RepairOrderId", typeof(int), "= Parameters.paraRepairOrderId.Value")});
            // 
            // pageFooterSection1
            // 
            this.pageFooterSection1.Height = Telerik.Reporting.Drawing.Unit.Cm(3D);
            this.pageFooterSection1.Name = "pageFooterSection1";
            // 
            // ReportRepairOrderForDealerMember
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pageHeaderSection1,
            this.detail,
            this.pageFooterSection1});
            this.Name = "ReportRepairOrderForDealer";
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Pixel(19D), Telerik.Reporting.Drawing.Unit.Pixel(19D), Telerik.Reporting.Drawing.Unit.Mm(9D), Telerik.Reporting.Drawing.Unit.Mm(9D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            reportParameter1.Name = "paraRepairOrderId";
            reportParameter1.Type = Telerik.Reporting.ReportParameterType.Integer;
            reportParameter2.Name = "UserName";
            this.ReportParameters.Add(reportParameter1);
            this.ReportParameters.Add(reportParameter2);
            this.Style.BackgroundColor = System.Drawing.Color.White;
            this.Style.Font.Name = "宋体";
            this.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
            this.Width = Telerik.Reporting.Drawing.Unit.Cm(33.299999237060547D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.PageHeaderSection pageHeaderSection1;
        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.PageFooterSection pageFooterSection1;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.Table table1;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.Table table2;
        private Telerik.Reporting.TextBox textBox9;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.TextBox textBox11;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.TextBox textBox10;
        private Telerik.Reporting.TextBox textBox12;
        private Telerik.Reporting.TextBox textBox13;
        private Telerik.Reporting.TextBox textBox14;
        private Telerik.Reporting.TextBox textBox15;
        private Telerik.Reporting.TextBox textBox16;
        private Telerik.Reporting.TextBox textBox17;
        private Telerik.Reporting.TextBox textBox18;
        private Telerik.Reporting.TextBox textBox19;
        private Telerik.Reporting.TextBox textBox20;
        private Telerik.Reporting.TextBox textBox21;
        private Telerik.Reporting.TextBox textBox22;
        private Telerik.Reporting.TextBox textBox23;
        private Telerik.Reporting.TextBox textBox24;
        private Telerik.Reporting.Table table3;
        private Telerik.Reporting.TextBox textBox29;
        private Telerik.Reporting.TextBox textBox27;
        private Telerik.Reporting.TextBox textBox26;
        private Telerik.Reporting.TextBox textBox25;
        private Telerik.Reporting.TextBox textBox28;
        private Telerik.Reporting.TextBox textBox30;
        private Telerik.Reporting.TextBox textBox31;
        private Telerik.Reporting.TextBox textBox32;
        private Telerik.Reporting.TextBox textBox33;
        private Telerik.Reporting.TextBox textBox45;
        private Telerik.Reporting.TextBox textBox46;
        private Telerik.Reporting.Table table4;
        private Telerik.Reporting.TextBox textBox38;
        private Telerik.Reporting.TextBox textBox39;
        private Telerik.Reporting.TextBox textBox40;
        private Telerik.Reporting.TextBox textBox42;
        private Telerik.Reporting.TextBox textBox44;
        private Telerik.Reporting.TextBox textBox48;
        private Telerik.Reporting.TextBox textBox50;
        private Telerik.Reporting.TextBox textBox35;
        private Telerik.Reporting.TextBox textBox36;
        private Telerik.Reporting.TextBox textBox37;
        private Telerik.Reporting.TextBox textBox41;
        private Telerik.Reporting.TextBox textBox43;
        private Telerik.Reporting.TextBox textBox47;
        private Telerik.Reporting.TextBox textBox49;
        private Telerik.Reporting.Table table5;
        private Telerik.Reporting.TextBox textBox54;
        private Telerik.Reporting.TextBox textBox55;
        private Telerik.Reporting.TextBox textBox56;
        private Telerik.Reporting.TextBox textBox58;
        private Telerik.Reporting.TextBox textBox60;
        private Telerik.Reporting.TextBox textBox62;
        private Telerik.Reporting.TextBox textBox64;
        private Telerik.Reporting.TextBox textBox51;
        private Telerik.Reporting.TextBox textBox52;
        private Telerik.Reporting.TextBox textBox53;
        private Telerik.Reporting.TextBox textBox57;
        private Telerik.Reporting.TextBox textBox59;
        private Telerik.Reporting.TextBox textBox61;
        private Telerik.Reporting.TextBox textBox63;
        private Telerik.Reporting.Table table6;
        private Telerik.Reporting.TextBox textBox72;
        private Telerik.Reporting.TextBox textBox73;
        private Telerik.Reporting.TextBox textBox66;
        private Telerik.Reporting.TextBox textBox67;
        private Telerik.Reporting.TextBox textBox69;
        private Telerik.Reporting.TextBox textBox71;
        private Telerik.Reporting.TextBox textBox70;
        private Telerik.Reporting.TextBox textBox74;
        private Telerik.Reporting.TextBox textBox76;
        private Telerik.Reporting.TextBox textBox77;
        private Telerik.Reporting.Table table8;
        private Telerik.Reporting.TextBox textBox103;
        private Telerik.Reporting.TextBox textBox104;
        private Telerik.Reporting.TextBox textBox100;
        private Telerik.Reporting.TextBox textBox106;
        private Telerik.Reporting.ObjectDataSource RepairOrderForDealerDataSource;
        private Telerik.Reporting.ObjectDataSource RepairOrderForDealerDetailDataSource;
        private Telerik.Reporting.Table table9;
        private Telerik.Reporting.TextBox textBox85;
        private Telerik.Reporting.TextBox textBox86;
        private Telerik.Reporting.TextBox textBox87;
        private Telerik.Reporting.TextBox textBox88;
        private Telerik.Reporting.TextBox textBox90;
        private Telerik.Reporting.TextBox textBox101;
        private Telerik.Reporting.TextBox textBox98;
        private Telerik.Reporting.TextBox textBox34;
        private Telerik.Reporting.TextBox textBox81;
        private Telerik.Reporting.TextBox textBox91;
        private Telerik.Reporting.TextBox textBox92;
        private Telerik.Reporting.TextBox textBox93;
        private Telerik.Reporting.TextBox textBox94;
        private Telerik.Reporting.TextBox textBox95;
        private Telerik.Reporting.TextBox textBox96;
        private Telerik.Reporting.TextBox textBox105;
        private Telerik.Reporting.TextBox textBox97;
        private Telerik.Reporting.Table table7;
        private Telerik.Reporting.TextBox textBox111;
        private Telerik.Reporting.TextBox textBox112;
        private Telerik.Reporting.TextBox textBox113;
        private Telerik.Reporting.TextBox textBox115;
        private Telerik.Reporting.TextBox textBox117;
        private Telerik.Reporting.TextBox textBox119;
        private Telerik.Reporting.TextBox textBox108;
        private Telerik.Reporting.TextBox textBox109;
        private Telerik.Reporting.TextBox textBox110;
        private Telerik.Reporting.TextBox textBox114;
        private Telerik.Reporting.TextBox textBox116;
        private Telerik.Reporting.TextBox textBox118;
        private Telerik.Reporting.TextBox textBox120;
        private Telerik.Reporting.TextBox textBox121;
        private Telerik.Reporting.TextBox textBox122;
        private Telerik.Reporting.TextBox textBox79;
        private Telerik.Reporting.TextBox textBox80;
        private Telerik.Reporting.TextBox textBox82;
        private Telerik.Reporting.TextBox textBox83;
        private Telerik.Reporting.TextBox textBox89;
        private Telerik.Reporting.ObjectDataSource RepairOrderItemDetailDataSource;
        private Telerik.Reporting.ObjectDataSource VirtualRepairOrderMaterialDetailDataSource;
        private Telerik.Reporting.TextBox textBox124;
        private Telerik.Reporting.TextBox textBox125;
        private Telerik.Reporting.TextBox textBox126;
        private Telerik.Reporting.TextBox textBox127;
        private Telerik.Reporting.TextBox textBox128;
        private Telerik.Reporting.TextBox textBox129;
        private Telerik.Reporting.TextBox textBox130;
        private Telerik.Reporting.TextBox textBox131;
        private Telerik.Reporting.TextBox textBox132;
        private Telerik.Reporting.TextBox textBox133;
        private Telerik.Reporting.TextBox textBox134;
        private Telerik.Reporting.TextBox textBox68;
        private Telerik.Reporting.TextBox textBox135;
        private Telerik.Reporting.TextBox textBox137;
        private Telerik.Reporting.TextBox textBox140;
        private Telerik.Reporting.TextBox textBox141;
        private Telerik.Reporting.TextBox textBox142;
        private Telerik.Reporting.TextBox textBox143;
        private Telerik.Reporting.TextBox textBox144;
        private Telerik.Reporting.TextBox textBox145;
        private Telerik.Reporting.TextBox textBox147;
        private Telerik.Reporting.TextBox textBox148;
        private Telerik.Reporting.TextBox textBox149;
        private Telerik.Reporting.TextBox textBox150;
        private Telerik.Reporting.TextBox textBox151;
        private Telerik.Reporting.TextBox textBox152;
        private Telerik.Reporting.TextBox textBox153;
        private Telerik.Reporting.TextBox textBox155;
        private Telerik.Reporting.TextBox textBox156;
        private Telerik.Reporting.TextBox textBox157;
        private Telerik.Reporting.TextBox textBox164;
        private Telerik.Reporting.TextBox textBox165;
        private Telerik.Reporting.TextBox textBox166;
        private Telerik.Reporting.TextBox textBox167;
        private Telerik.Reporting.TextBox textBox168;
        private Telerik.Reporting.TextBox textBox169;
        private Telerik.Reporting.TextBox textBox75;
        private Telerik.Reporting.TextBox textBox78;
        private Telerik.Reporting.TextBox textBox84;
        private Telerik.Reporting.Table table10;
        private Telerik.Reporting.TextBox textBox139;
        private Telerik.Reporting.TextBox textBox65;
        private Telerik.Reporting.TextBox textBox107;
        private Telerik.Reporting.TextBox textBox146;
        private Telerik.Reporting.TextBox textBox154;
        private Telerik.Reporting.TextBox textBox160;
        private Telerik.Reporting.ObjectDataSource objectDataSource1;
        private Telerik.Reporting.ObjectDataSource GetRepairMemberRightsDetails;
    }
}