namespace Sunlight.Silverlight.Dcs.Web.Reporting {
    partial class ReportSparePartLabel {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.Reporting.ReportParameter reportParameter1 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            this.detail = new Telerik.Reporting.DetailSection();
            this.panel1 = new Telerik.Reporting.Panel();
            this.barcode3 = new Telerik.Reporting.Barcode();
            this.barcode4 = new Telerik.Reporting.Barcode();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.textBox7 = new Telerik.Reporting.TextBox();
            this.textBox8 = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Cm(4D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.panel1});
            this.detail.Name = "detail";
            this.detail.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(1D);
            // 
            // panel1
            // 
            this.panel1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.barcode3,
            this.barcode4,
            this.textBox1,
            this.textBox4});
            this.panel1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(1D));
            this.panel1.Name = "panel1";
            this.panel1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.5D), Telerik.Reporting.Drawing.Unit.Cm(3D));
            // 
            // barcode3
            // 
            this.barcode3.BarAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.barcode3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.00010012308484874666D), Telerik.Reporting.Drawing.Unit.Cm(9.9961594969499856E-05D));
            this.barcode3.Name = "barcode3";
            this.barcode3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.89989972114563D), Telerik.Reporting.Drawing.Unit.Cm(1.2999001741409302D));
            this.barcode3.Stretch = true;
            this.barcode3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.barcode3.Value = "= Sunlight.Silverlight.Dcs.Web.Reporting.CommonConverter.GetSolidDate(Now().Year)" +
    "";
            // 
            // barcode4
            // 
            this.barcode4.BarAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.barcode4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(1.5000001192092896D));
            this.barcode4.Name = "barcode4";
            this.barcode4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.4998998641967773D), Telerik.Reporting.Drawing.Unit.Cm(1.2000000476837158D));
            this.barcode4.Stretch = true;
            this.barcode4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.barcode4.Value = "= Fields.SparePartCode";
            // 
            // textBox1
            // 
            this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.9001998901367188D), Telerik.Reporting.Drawing.Unit.Cm(9.9961594969499856E-05D));
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.5996999740600586D), Telerik.Reporting.Drawing.Unit.Cm(0.69989979267120361D));
            this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox1.Value = "= Fields.SparePart.Name";
            // 
            // textBox4
            // 
            this.textBox4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.9001998901367188D), Telerik.Reporting.Drawing.Unit.Cm(0.80010002851486206D));
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.69979989528656D), Telerik.Reporting.Drawing.Unit.Cm(0.49989986419677734D));
            this.textBox4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox4.Value = "=\"��λ��\"+  Fields.SparePart.MeasureUnit";
            // 
            // textBox7
            // 
            this.textBox7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(7.0273332595825195D), Telerik.Reporting.Drawing.Unit.Cm(0.35983332991600037D));
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.89999997615814209D), Telerik.Reporting.Drawing.Unit.Cm(0.59999984502792358D));
            this.textBox7.Value = "= Fields.SparePart.MeasureUnit";
            // 
            // textBox8
            // 
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.4000010490417481D), Telerik.Reporting.Drawing.Unit.Cm(0.59999984502792358D));
            this.textBox8.Value = "��λ��";
            // 
            // ReportSparePartLabel
            // 
            this.DataSource = null;
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.detail});
            this.Name = "ReportSparePartLabel";
            this.PageSettings.Landscape = false;
            this.PageSettings.Margins.Bottom = Telerik.Reporting.Drawing.Unit.Mm(0D);
            this.PageSettings.Margins.Left = Telerik.Reporting.Drawing.Unit.Mm(0D);
            this.PageSettings.Margins.Right = Telerik.Reporting.Drawing.Unit.Mm(0D);
            this.PageSettings.Margins.Top = Telerik.Reporting.Drawing.Unit.Mm(0D);
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Custom;
            this.PageSettings.PaperSize = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(100D), Telerik.Reporting.Drawing.Unit.Mm(40D));
            reportParameter1.MultiValue = true;
            reportParameter1.Name = "checkDetails";
            this.ReportParameters.Add(reportParameter1);
            this.Style.BackgroundColor = System.Drawing.Color.White;
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
            this.Width = Telerik.Reporting.Drawing.Unit.Cm(10D);
            this.NeedDataSource += new System.EventHandler(this.ReportSparePartLabel1_NeedDataSource);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.Panel panel1;
        private Telerik.Reporting.Barcode barcode3;
        private Telerik.Reporting.Barcode barcode4;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.TextBox textBox4;
    }
}