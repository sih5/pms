
using System.Collections.Generic;
using System.Linq;

namespace Sunlight.Silverlight.Dcs.Web.Reporting {
    using System;
    using Telerik.Reporting;

    /// <summary>
    /// Summary description for ReportSparePartLabel1.
    /// </summary>
    public partial class ReportSparePartLabel1 : Report {
        public ReportSparePartLabel1() {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }

        private void ReportSparePartLabel1_NeedDataSource(object sender, EventArgs e) {
            var report = (Telerik.Reporting.Processing.Report)sender;
            // 取参数值
            var inboundDetails = report.Parameters["InboundDetails"].Value as object[];
            Dictionary<int, int?> idss = new Dictionary<int, int?>();
            var ids = new List<int>();
            foreach(var inboundDetail in inboundDetails) {
                var aa = inboundDetail.ToString();
                var ss = aa.Split(',');
                var qq = ss[0];
                var q = ss[1];
                idss.Add(Convert.ToInt32(qq), Convert.ToInt32(q));
                ids.Add(Convert.ToInt32(qq));
            }
            // 获取客户端传入的配件入库计划单清单的Id集合
            // 调用服务端方法，查询清单数据
            var domainService = new DcsDomainService();
            var partsInboundPlanDetails = domainService.GetPartsInboundPlanDetailByIdForLabel(ids.ToArray()).OrderBy(r => r.SparePartId).ToArray();
            var details = new List<PartsInboundPlanDetail>();
            //修改清单记录的数量属性，为客户端传入的值
            foreach(var item in partsInboundPlanDetails) {
                int printNumber = Convert.ToInt32(idss[item.Id].Value);
                for(int i = 0; i < printNumber; i++) {
                    var planDetail = new PartsInboundPlanDetail();
                    planDetail.SparePartCode = item.SparePartCode;
                    planDetail.SparePartName = item.SparePartName;
                    planDetail.Unit = item.Unit;
                    planDetail.OverseasPartsFigure = item.OverseasPartsFigure;
                    details.Add(planDetail);
                }
            }

            // 将构造的结果集绑定到 DetailSection
            report.DataSource = details;

        }
    }
}