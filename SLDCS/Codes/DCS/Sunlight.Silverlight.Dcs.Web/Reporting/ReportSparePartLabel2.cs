

namespace Sunlight.Silverlight.Dcs.Web.Reporting {
    using System;
    using Telerik.Reporting;

    /// <summary>
    /// Summary description for ReportSparePartLabel2.
    /// </summary>
    public partial class ReportSparePartLabel2 : Report {
        public ReportSparePartLabel2() {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();
        }

        private void ReportSparePartLabel2_NeedDataSource(object sender, EventArgs e) {
            var domainService = new DcsDomainService();
            var report = (Telerik.Reporting.Processing.Report)sender;
            var parameter = report.Parameters["partDetails"].Value.ToString().Trim();
            var partIds = parameter.Split(',');
            var partId = new int[partIds.Length];
            for(var i = 0; i < partIds.Length; i++) {
                partId[i] = int.Parse(partIds[i]);
            }
            var data = domainService.GetSparePartByIds(partId);
            report.DataSource = data;
        }
    }
}