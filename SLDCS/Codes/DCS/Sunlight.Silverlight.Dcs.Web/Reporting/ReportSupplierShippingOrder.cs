namespace Sunlight.Silverlight.Dcs.Web.Reporting {
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using Telerik.Reporting;
    using Telerik.Reporting.Drawing;

    /// <summary>
    /// Summary description for ReportSupplierShippingOrder.
    /// </summary>
    public partial class ReportSupplierShippingOrder : Telerik.Reporting.Report {
        public ReportSupplierShippingOrder() {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }
    }
}