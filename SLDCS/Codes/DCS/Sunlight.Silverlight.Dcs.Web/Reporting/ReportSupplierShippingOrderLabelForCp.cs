namespace Sunlight.Silverlight.Dcs.Web.Reporting
{

    using Sunlight.Silverlight.Dcs.Web.Entities;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;

    /// <summary>
    /// Summary description for ReportSupplierShippingOrderLabelForCp.
    /// </summary>
    public partial class ReportSupplierShippingOrderLabelForCp : Telerik.Reporting.Report
    {
        public ReportSupplierShippingOrderLabelForCp()
        {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }
        private void ReportPartsInboundCheckLabelForCp_NeedDataSource(object sender, EventArgs e)
        {
            var report = (Telerik.Reporting.Processing.Report)sender;
            // 取参数值
            var pararPartsTags = report.Parameters["PararPartsTags"].Value.ToString().Replace("%2c", ",");

            //  var id = Convert.ToInt32(report.Parameters["PartsInboundCheckBillId"].Value);
            var ids = new List<int>();
            var idsStr = pararPartsTags.Split(',');
            for (int i = 0; i < idsStr.Count() - 1; i++)
            {
                ids.Add(Convert.ToInt32(idsStr[i]));
            }
            var billId = report.Parameters["SupplierShippingOrderId"].Value.ToString().Replace("%2c", ",");
            var billIds = new List<int>();
            var billIdStr = billId.Split(',');
            for (int i = 0; i < billIdStr.Count() - 1; i++)
            {
                billIds.Add(Convert.ToInt32(billIdStr[i]));
            }

            var domainService = new DcsDomainService();
            // 获取客户端传入的配件入库计划清单的Id集合
            // 调用服务端方法，查询清单数据
            var partsInboundPlanDetails = domainService.GetSupplierShippingOrderForPrintPcs(ids.ToArray(), billIds.ToArray()).OrderBy(r => r.SparePartId).ToArray();
            var details = new List<VirtualSupplierShippingDetail>();
            //修改清单记录的数量属性，为客户端传入的值
            foreach (var item in partsInboundPlanDetails)
            {
                int firPrintNumber = item.FirPrintNumber.Value;
                for (int i = 0; i < firPrintNumber; i++)
                {
                    var planDetail = new VirtualSupplierShippingDetail();
                    planDetail.SparePartCode = item.SparePartCode;
                    planDetail.SparePartName = item.SparePartName;
                    planDetail.MeasureUnit = item.MeasureUnit + "/" + item.FirMeasureUnit; ;
                    planDetail.PackingCoefficient = item.FirPackingCoefficient.Value;
                    planDetail.BachMunber = item.BachMunber == null ? "" : item.BachMunber;
                    if(planDetail.SparePartName.Length > 18) {
                        planDetail.SparePartName = planDetail.SparePartName.Substring(0, 18);
                        planDetail.SparePartName = planDetail.SparePartName + "...";
                    }
                    if(planDetail.BachMunber.Length > 12) {
                        planDetail.BachMunber = planDetail.BachMunber.Substring(planDetail.BachMunber.Length - 16, 16);
                    }
                    details.Add(planDetail);
                }
                int secPrintNumber = item.SecPrintNumber.Value;
                for (int i = 0; i < secPrintNumber; i++)
                {
                    var planDetail = new VirtualSupplierShippingDetail();
                    planDetail.SparePartCode = item.SparePartCode;
                    planDetail.SparePartName = item.SparePartName;
                    planDetail.MeasureUnit = item.MeasureUnit + "/" + item.SecMeasureUnit;
                    planDetail.PackingCoefficient = item.SecPackingCoefficient.Value;
                    planDetail.BachMunber = item.BachMunber == null ? "" : item.BachMunber;
                    if(planDetail.SparePartName.Length > 18) {
                        planDetail.SparePartName = planDetail.SparePartName.Substring(0, 18);
                        planDetail.SparePartName = planDetail.SparePartName + "...";
                    }
                    if(planDetail.BachMunber.Length > 12) {
                        planDetail.BachMunber = planDetail.BachMunber.Substring(planDetail.BachMunber.Length - 16, 16);
                    }
                    details.Add(planDetail);
                } int ThidPrintNumber = item.ThidPrintNumber.Value;
                for (int i = 0; i < ThidPrintNumber; i++)
                {
                    var planDetail = new VirtualSupplierShippingDetail();
                    planDetail.SparePartCode = item.SparePartCode;
                    planDetail.SparePartName = item.SparePartName;
                    planDetail.MeasureUnit = item.MeasureUnit + "/" + item.ThidMeasureUnit;
                    planDetail.PackingCoefficient = item.ThidPackingCoefficient.Value;
                    planDetail.BachMunber = item.BachMunber == null ? "" : item.BachMunber;
                    if(planDetail.SparePartName.Length > 18) {
                        planDetail.SparePartName = planDetail.SparePartName.Substring(0, 18);
                        planDetail.SparePartName = planDetail.SparePartName + "...";
                    }
                    if(planDetail.BachMunber.Length > 12) {
                        planDetail.BachMunber = planDetail.BachMunber.Substring(planDetail.BachMunber.Length - 16, 16);
                    }
                    details.Add(planDetail);
                }
            }
            //调用生成WMS标签序列号方法
            //domainService.InsertWMSLabelPrintingSerialList2(details);
            // 将构造的结果集绑定到 DetailSection
            report.DataSource = details;
        }


    }
}