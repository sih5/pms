using System.Collections.Generic;
using System.Linq;

namespace Sunlight.Silverlight.Dcs.Web.Reporting {

    /// <summary>
    /// Summary description for ReportUsedPartsLable1.
    /// </summary>
    public partial class ReportUsedPartsLable1 : Telerik.Reporting.Report {
        public ReportUsedPartsLable1() {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //            
        }

        //public IEnumerable<Wrapper> IndexData(IEnumerable<RepairOrderMaterialDetail> data) {
        //    int i = 0;
        //    return data.Select(o => new Wrapper {
        //        Index = i++,
        //        Data = o
        //    });
        //}

        public class Wrapper {
            public int Index {
                get;
                set;
            }
            //public RepairOrderMaterialDetail Data {
            //    get;
            //    set;
            //}
        }

        private void ReportUsedPartsLable1_NeedDataSource(object sender, System.EventArgs e) {
            var service = new DcsDomainService();
            var report = (Telerik.Reporting.Processing.Report)sender;
            int parameter = int.Parse(report.Parameters["paraRepairOrderId"].Value.ToString());
            //var data = service.GetRepairOrderMaterialDetailByIdForPrint(parameter);
            //this.list1.DataSource = IndexData(data);
        }

    }
}