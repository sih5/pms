using System.Collections.Generic;
using System.Linq;

namespace Sunlight.Silverlight.Dcs.Web.Reporting {
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using Telerik.Reporting;
    using Telerik.Reporting.Drawing;

    /// <summary>
    /// Summary description for ReportUsedPartsLable2.
    /// </summary>
    public partial class ReportUsedPartsLable2 : Telerik.Reporting.Report {
        public ReportUsedPartsLable2() {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }

        public IEnumerable<ReportUsedPartsLable2.Wrapper> IndexData(IEnumerable<UsedPartsShippingDetail> data) {
            int i = 0;
            return data.Select(o => new ReportUsedPartsLable2.Wrapper {
                Index = i++,
                Data = o
            });
        }
        public class Wrapper {
            public int Index {
                get;
                set;
            }
            public UsedPartsShippingDetail Data {
                get;
                set;
            }
        }

        private void ReportUsedPartsLable2_NeedDataSource(object sender, EventArgs e) {
            var service = new DcsDomainService();
            var report = (Telerik.Reporting.Processing.Report)sender;
            int parameter = int.Parse(report.Parameters["parausedPartsShippingOrderId"].Value.ToString());
            var data = service.GetUsedPartsShippingDetailWithLableForPrint(parameter);
            this.list1.DataSource = IndexData(data);
        }
       
    }
}