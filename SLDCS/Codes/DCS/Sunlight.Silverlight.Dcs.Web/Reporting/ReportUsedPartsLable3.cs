using System.Collections.Generic;
using System.Linq;

namespace Sunlight.Silverlight.Dcs.Web.Reporting {
    using System;

    /// <summary>
    /// Summary description for ReportUsedPartsLable3.
    /// </summary>
    public partial class ReportUsedPartsLable3 : Telerik.Reporting.Report {
        public ReportUsedPartsLable3() {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }
        public IEnumerable<ReportUsedPartsLable3.Wrapper> IndexData(IEnumerable<SsUsedPartsStorage> data) {
            int i = 0;
            return data.Select(o => new ReportUsedPartsLable3.Wrapper {
                Index = i++,
                Data = o
            });
        }
        public class Wrapper {
            public int Index {
                get;
                set;
            }
            public SsUsedPartsStorage Data {
                get;
                set;
            }
        }

        private void ReportUsedPartsLable3_NeedDataSource(object sender, EventArgs e) {
            var service = new DcsDomainService();
            var report = (Telerik.Reporting.Processing.Report)sender;
            string parameter = report.Parameters["paraSsUsedPartsStorageId"].Value.ToString().Trim();
            var aa = parameter.Split(',');
            int[] a = new int[aa.Length];
            for(int i = 0; i < aa.Length; i++) {
                a[i] = int.Parse(aa[i]);
            }
            var data = service.GetSsUsedPartsStorageWithLableForPrint(a);
            this.list1.DataSource = IndexData(data);
        }
    }
}