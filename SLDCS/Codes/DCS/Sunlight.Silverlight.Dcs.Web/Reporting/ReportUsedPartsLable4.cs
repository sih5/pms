using System.Collections.Generic;
using System.Linq;

namespace Sunlight.Silverlight.Dcs.Web.Reporting {
    using System;

    /// <summary>
    /// Summary description for ReportUsedPartsLable4.
    /// </summary>
    public partial class ReportUsedPartsLable4 : Telerik.Reporting.Report {
        public ReportUsedPartsLable4() {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }
        public IEnumerable<ReportUsedPartsLable4.Wrapper> IndexData(IEnumerable<UsedPartsStock> data) {
            int i = 0;
            return data.Select(o => new ReportUsedPartsLable4.Wrapper {
                Index = i++,
                Data = o
            });
        }
        public class Wrapper {
            public int Index {
                get;
                set;
            }
            public UsedPartsStock Data {
                get;
                set;
            }
        }

        private void ReportUsedPartsLable4_NeedDataSource(object sender, EventArgs e) {
            var service = new DcsDomainService();
            var report = (Telerik.Reporting.Processing.Report)sender;
            string parameter = report.Parameters["paraUsedPartsStockId"].Value.ToString().Trim();
            var aa = parameter.Split(',');
            int[] a = new int[aa.Length];
            for(int i = 0; i < aa.Length; i++) {
                a[i] = int.Parse(aa[i]);
            }
            var data = service.GetUsedPartsStockWithLableForPrint(a);
            this.list1.DataSource = IndexData(data);
        }

    }
}