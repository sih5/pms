using System.Collections.Generic;
using System.Linq;

namespace Sunlight.Silverlight.Dcs.Web.Reporting {
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using Telerik.Reporting;
    using Telerik.Reporting.Drawing;

    /// <summary>
    /// Summary description for ReportUsedPartsShippingOrder1.
    /// </summary>
    public partial class ReportUsedPartsShippingOrder1 : Telerik.Reporting.Report {
        public ReportUsedPartsShippingOrder1() {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }
        public IEnumerable<ReportUsedPartsShippingOrder1.Wrapper> IndexData(IEnumerable<UsedPartsShippingDetail> data) {
            int i = 0;
            return data.Select(o => new ReportUsedPartsShippingOrder1.Wrapper {
                Index = i++,
                Data = o
            });
        }
        public class Wrapper {
            public int Index {
                get;
                set;
            }
            public UsedPartsShippingDetail Data {
                get;
                set;
            }
        }

        private void ReportUsedPartsShippingOrder1_NeedDataSource(object sender, EventArgs e) {
            var service = new DcsDomainService();
            var report = (Telerik.Reporting.Processing.Report)sender;
            string parameter = report.Parameters["UsedPartsShippingDetailIds"].Value as string;
            if(!string.IsNullOrEmpty(parameter)) {
                string[] strids = parameter.Split(',');
                int[] ids = new int[parameter.Split(',').Length];
                for(int i = 0; i < strids.Length; i++) {
                    ids[i] = Convert.ToInt32(strids[i]);
                }
                var data = service.GetUsedPartsShippingDetailWithLableForChoosePrint(ids);
                this.list1.DataSource = IndexData(data);
            }
        }
    }
}