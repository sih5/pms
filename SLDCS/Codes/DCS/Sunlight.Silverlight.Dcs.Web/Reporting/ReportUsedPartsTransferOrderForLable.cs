using System.Collections.Generic;
using System.Linq;

namespace Sunlight.Silverlight.Dcs.Web.Reporting {
    using System;

    /// <summary>
    /// Summary description for ReportUsedPartsTransferOrderForLable.
    /// </summary>
    public partial class ReportUsedPartsTransferOrderForLable : Telerik.Reporting.Report {
        public ReportUsedPartsTransferOrderForLable() {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }
        public IEnumerable<ReportUsedPartsTransferOrderForLable.Wrapper> IndexData(IEnumerable<UsedPartsTransferDetail> data) {
            int i = 0;
            return data.Select(o => new ReportUsedPartsTransferOrderForLable.Wrapper {
                Index = i++,
                Data = o
            });
        }
        public class Wrapper {
            public int Index {
                get;
                set;
            }
            public UsedPartsTransferDetail Data {
                get;
                set;
            }
        }

        private void ReportUsedPartsTransferOrderForLable_NeedDataSource(object sender, EventArgs e) {
            var service = new DcsDomainService();
            var report = (Telerik.Reporting.Processing.Report)sender;
            int parameter = int.Parse(report.Parameters["usedPartsTransferOrderId"].Value.ToString());
            var data = service.GetUsedPartsTransferOrderWithLableForPrint(parameter);
            this.list1.DataSource = IndexData(data);

        }
    }
}