﻿namespace Sunlight.Silverlight.Dcs.Web.Reporting {
    partial class ReportVehicleDLRAccountFreezeBill {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup11 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup12 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup13 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.ReportParameter reportParameter1 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter2 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter3 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter4 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter5 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter6 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter7 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter8 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            this.pageHeaderSection1 = new Telerik.Reporting.PageHeaderSection();
            this.textBox13 = new Telerik.Reporting.TextBox();
            this.detail = new Telerik.Reporting.DetailSection();
            this.list3 = new Telerik.Reporting.List();
            this.textBox174 = new Telerik.Reporting.TextBox();
            this.textBox176 = new Telerik.Reporting.TextBox();
            this.textBox186 = new Telerik.Reporting.TextBox();
            this.textBox189 = new Telerik.Reporting.TextBox();
            this.textBox190 = new Telerik.Reporting.TextBox();
            this.textBox191 = new Telerik.Reporting.TextBox();
            this.textBox192 = new Telerik.Reporting.TextBox();
            this.textBox203 = new Telerik.Reporting.TextBox();
            this.textBox205 = new Telerik.Reporting.TextBox();
            this.textBox208 = new Telerik.Reporting.TextBox();
            this.textBox209 = new Telerik.Reporting.TextBox();
            this.textBox212 = new Telerik.Reporting.TextBox();
            this.textBox220 = new Telerik.Reporting.TextBox();
            this.textBox222 = new Telerik.Reporting.TextBox();
            this.textBox225 = new Telerik.Reporting.TextBox();
            this.textBox226 = new Telerik.Reporting.TextBox();
            this.textBox235 = new Telerik.Reporting.TextBox();
            this.textBox241 = new Telerik.Reporting.TextBox();
            this.textBox228 = new Telerik.Reporting.TextBox();
            this.textBox237 = new Telerik.Reporting.TextBox();
            this.DataSourceVehicleDLRAccountFreezeBill = new Telerik.Reporting.ObjectDataSource();
            this.pageFooterSection1 = new Telerik.Reporting.PageFooterSection();
            this.textBox4 = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeaderSection1
            // 
            this.pageHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Cm(1.7999999523162842D);
            this.pageHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox13});
            this.pageHeaderSection1.Name = "pageHeaderSection1";
            // 
            // textBox13
            // 
            this.textBox13.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.2999997138977051D), Telerik.Reporting.Drawing.Unit.Cm(0.49999994039535522D));
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(11.300000190734863D), Telerik.Reporting.Drawing.Unit.Cm(0.89999997615814209D));
            this.textBox13.Style.Font.Name = "微软雅黑";
            this.textBox13.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(20D);
            this.textBox13.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox13.Value = "账户冻结申请单";
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Cm(2.2999999523162842D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.list3});
            this.detail.Name = "detail";
            // 
            // list3
            // 
            this.list3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.001849889755249D)));
            this.list3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.016232967376709D)));
            this.list3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.5152666568756104D)));
            this.list3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.126460075378418D)));
            this.list3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.8468725681304932D)));
            this.list3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.4819457530975342D)));
            this.list3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.6924276351928711D)));
            this.list3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.2188446521759033D)));
            this.list3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.60999947786331177D)));
            this.list3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.7999998927116394D)));
            this.list3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.7999998927116394D)));
            this.list3.Body.SetCellContent(0, 0, this.textBox174);
            this.list3.Body.SetCellContent(0, 1, this.textBox176);
            this.list3.Body.SetCellContent(1, 0, this.textBox186);
            this.list3.Body.SetCellContent(1, 1, this.textBox189);
            this.list3.Body.SetCellContent(0, 2, this.textBox190);
            this.list3.Body.SetCellContent(1, 2, this.textBox191);
            this.list3.Body.SetCellContent(0, 3, this.textBox192);
            this.list3.Body.SetCellContent(1, 3, this.textBox203);
            this.list3.Body.SetCellContent(0, 4, this.textBox205);
            this.list3.Body.SetCellContent(1, 4, this.textBox208);
            this.list3.Body.SetCellContent(0, 5, this.textBox209);
            this.list3.Body.SetCellContent(1, 5, this.textBox212);
            this.list3.Body.SetCellContent(0, 7, this.textBox220);
            this.list3.Body.SetCellContent(1, 7, this.textBox222);
            this.list3.Body.SetCellContent(0, 6, this.textBox225);
            this.list3.Body.SetCellContent(1, 6, this.textBox226);
            this.list3.Body.SetCellContent(2, 5, this.textBox235);
            this.list3.Body.SetCellContent(2, 0, this.textBox241, 1, 2);
            this.list3.Body.SetCellContent(2, 2, this.textBox228, 1, 3);
            this.list3.Body.SetCellContent(2, 6, this.textBox237, 1, 2);
            tableGroup1.Name = "ColumnGroup";
            tableGroup2.Name = "Group1";
            tableGroup3.Name = "Group3";
            tableGroup4.Name = "Group5";
            tableGroup5.Name = "Group6";
            tableGroup6.Name = "Group8";
            tableGroup7.Name = "Group11";
            tableGroup8.Name = "Group10";
            this.list3.ColumnGroups.Add(tableGroup1);
            this.list3.ColumnGroups.Add(tableGroup2);
            this.list3.ColumnGroups.Add(tableGroup3);
            this.list3.ColumnGroups.Add(tableGroup4);
            this.list3.ColumnGroups.Add(tableGroup5);
            this.list3.ColumnGroups.Add(tableGroup6);
            this.list3.ColumnGroups.Add(tableGroup7);
            this.list3.ColumnGroups.Add(tableGroup8);
            this.list3.ColumnHeadersPrintOnEveryPage = false;
            this.list3.DataSource = this.DataSourceVehicleDLRAccountFreezeBill;
            this.list3.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox174,
            this.textBox176,
            this.textBox186,
            this.textBox189,
            this.textBox190,
            this.textBox191,
            this.textBox192,
            this.textBox203,
            this.textBox205,
            this.textBox208,
            this.textBox209,
            this.textBox212,
            this.textBox220,
            this.textBox222,
            this.textBox225,
            this.textBox226,
            this.textBox235,
            this.textBox241,
            this.textBox228,
            this.textBox237});
            this.list3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.00010012308484874666D), Telerik.Reporting.Drawing.Unit.Cm(9.9921220680698752E-05D));
            this.list3.Name = "list3";
            tableGroup9.Name = "Group2";
            tableGroup11.Name = "Group4";
            tableGroup10.ChildGroups.Add(tableGroup11);
            tableGroup10.Groupings.AddRange(new Telerik.Reporting.Grouping[] {
            new Telerik.Reporting.Grouping(null)});
            tableGroup10.Name = "DetailGroup";
            tableGroup13.Name = "Group12";
            tableGroup12.ChildGroups.Add(tableGroup13);
            tableGroup12.Name = "Group7";
            this.list3.RowGroups.Add(tableGroup9);
            this.list3.RowGroups.Add(tableGroup10);
            this.list3.RowGroups.Add(tableGroup12);
            this.list3.RowHeadersPrintOnEveryPage = false;
            this.list3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.899898529052734D), Telerik.Reporting.Drawing.Unit.Cm(2.2099993228912354D));
            this.list3.Style.Font.Name = "微软雅黑";
            // 
            // textBox174
            // 
            this.textBox174.Name = "textBox174";
            this.textBox174.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.001849889755249D), Telerik.Reporting.Drawing.Unit.Cm(0.60999953746795654D));
            this.textBox174.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox174.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox174.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox174.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox174.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox174.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox174.Style.Font.Name = "微软雅黑";
            this.textBox174.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox174.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox174.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox174.Style.Visible = true;
            this.textBox174.StyleName = "";
            this.textBox174.Value = "序号";
            // 
            // textBox176
            // 
            this.textBox176.Name = "textBox176";
            this.textBox176.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.016232967376709D), Telerik.Reporting.Drawing.Unit.Cm(0.60999953746795654D));
            this.textBox176.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox176.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox176.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox176.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox176.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox176.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox176.Style.Font.Name = "微软雅黑";
            this.textBox176.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox176.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox176.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox176.Style.Visible = true;
            this.textBox176.StyleName = "";
            this.textBox176.Value = "编号";
            // 
            // textBox186
            // 
            this.textBox186.Name = "textBox186";
            this.textBox186.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.001849889755249D), Telerik.Reporting.Drawing.Unit.Cm(0.7999998927116394D));
            this.textBox186.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox186.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox186.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox186.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox186.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox186.Style.Font.Name = "微软雅黑";
            this.textBox186.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox186.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox186.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox186.StyleName = "";
            this.textBox186.Value = "=Fields.Id";
            // 
            // textBox189
            // 
            this.textBox189.Name = "textBox189";
            this.textBox189.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.016232967376709D), Telerik.Reporting.Drawing.Unit.Cm(0.7999998927116394D));
            this.textBox189.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox189.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox189.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox189.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox189.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox189.Style.Font.Name = "微软雅黑";
            this.textBox189.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox189.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox189.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox189.StyleName = "";
            this.textBox189.Value = "=Fields.Code";
            // 
            // textBox190
            // 
            this.textBox190.Name = "textBox190";
            this.textBox190.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.5152661800384521D), Telerik.Reporting.Drawing.Unit.Cm(0.60999953746795654D));
            this.textBox190.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox190.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox190.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox190.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox190.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox190.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox190.Style.Font.Name = "微软雅黑";
            this.textBox190.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox190.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox190.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox190.Style.Visible = true;
            this.textBox190.StyleName = "";
            this.textBox190.Value = "经销商编号";
            // 
            // textBox191
            // 
            this.textBox191.Name = "textBox191";
            this.textBox191.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.5152661800384521D), Telerik.Reporting.Drawing.Unit.Cm(0.7999998927116394D));
            this.textBox191.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox191.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox191.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox191.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox191.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox191.Style.Font.Name = "微软雅黑";
            this.textBox191.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox191.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox191.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox191.StyleName = "";
            this.textBox191.Value = "=Fields.CustomerCompanyCode";
            // 
            // textBox192
            // 
            this.textBox192.Name = "textBox192";
            this.textBox192.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1264598369598389D), Telerik.Reporting.Drawing.Unit.Cm(0.60999953746795654D));
            this.textBox192.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox192.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox192.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox192.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox192.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox192.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox192.Style.Font.Name = "微软雅黑";
            this.textBox192.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox192.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox192.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox192.Style.Visible = true;
            this.textBox192.StyleName = "";
            this.textBox192.Value = "经销商名称";
            // 
            // textBox203
            // 
            this.textBox203.Format = "{0:C2}";
            this.textBox203.Name = "textBox203";
            this.textBox203.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1264598369598389D), Telerik.Reporting.Drawing.Unit.Cm(0.7999998927116394D));
            this.textBox203.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox203.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox203.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox203.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox203.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox203.Style.Font.Name = "微软雅黑";
            this.textBox203.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox203.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox203.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox203.StyleName = "";
            this.textBox203.Value = "=Fields.CustomerCompanyName";
            // 
            // textBox205
            // 
            this.textBox205.Name = "textBox205";
            this.textBox205.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8468723297119141D), Telerik.Reporting.Drawing.Unit.Cm(0.60999953746795654D));
            this.textBox205.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox205.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox205.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox205.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox205.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox205.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox205.Style.Font.Name = "微软雅黑";
            this.textBox205.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox205.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox205.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox205.Style.Visible = true;
            this.textBox205.StyleName = "";
            this.textBox205.Value = "资金类型";
            // 
            // textBox208
            // 
            this.textBox208.Format = "";
            this.textBox208.Name = "textBox208";
            this.textBox208.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8468723297119141D), Telerik.Reporting.Drawing.Unit.Cm(0.7999998927116394D));
            this.textBox208.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox208.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox208.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox208.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox208.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox208.Style.Font.Name = "微软雅黑";
            this.textBox208.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox208.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox208.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox208.StyleName = "";
            this.textBox208.Value = "=Fields.VehicleFundsType.Name";
            // 
            // textBox209
            // 
            this.textBox209.Name = "textBox209";
            this.textBox209.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.4819457530975342D), Telerik.Reporting.Drawing.Unit.Cm(0.60999953746795654D));
            this.textBox209.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox209.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox209.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox209.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox209.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox209.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox209.Style.Font.Name = "微软雅黑";
            this.textBox209.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox209.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox209.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox209.Style.Visible = true;
            this.textBox209.StyleName = "";
            this.textBox209.Value = "账户可用性";
            // 
            // textBox212
            // 
            this.textBox212.Format = "";
            this.textBox212.Name = "textBox212";
            this.textBox212.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.4819457530975342D), Telerik.Reporting.Drawing.Unit.Cm(0.7999998927116394D));
            this.textBox212.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox212.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox212.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox212.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox212.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox212.Style.Font.Name = "微软雅黑";
            this.textBox212.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox212.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox212.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox212.StyleName = "";
            this.textBox212.Value = "= Sunlight.Silverlight.Dcs.Web.Reporting.CommonConverter.GetAccountAvailabilityCo" +
    "nvertFormat(Fields.AccountAvailability)";
            // 
            // textBox220
            // 
            this.textBox220.Name = "textBox220";
            this.textBox220.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.2188446521759033D), Telerik.Reporting.Drawing.Unit.Cm(0.60999953746795654D));
            this.textBox220.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox220.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox220.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox220.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox220.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox220.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox220.Style.Font.Name = "微软雅黑";
            this.textBox220.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox220.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox220.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox220.Style.Visible = true;
            this.textBox220.StyleName = "";
            this.textBox220.Value = "创建时间";
            // 
            // textBox222
            // 
            this.textBox222.Format = "{0:d}";
            this.textBox222.Name = "textBox222";
            this.textBox222.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.2188446521759033D), Telerik.Reporting.Drawing.Unit.Cm(0.7999998927116394D));
            this.textBox222.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox222.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox222.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox222.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox222.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox222.Style.Font.Name = "微软雅黑";
            this.textBox222.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox222.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox222.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox222.StyleName = "";
            this.textBox222.Value = "=Fields.CreateTime";
            // 
            // textBox225
            // 
            this.textBox225.Name = "textBox225";
            this.textBox225.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.692427396774292D), Telerik.Reporting.Drawing.Unit.Cm(0.60999953746795654D));
            this.textBox225.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox225.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox225.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox225.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox225.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox225.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox225.Style.Font.Name = "微软雅黑";
            this.textBox225.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox225.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox225.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox225.Style.Visible = true;
            this.textBox225.StyleName = "";
            this.textBox225.Value = "创建人";
            // 
            // textBox226
            // 
            this.textBox226.Name = "textBox226";
            this.textBox226.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.692427396774292D), Telerik.Reporting.Drawing.Unit.Cm(0.7999998927116394D));
            this.textBox226.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox226.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox226.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox226.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox226.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox226.Style.Font.Name = "微软雅黑";
            this.textBox226.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox226.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox226.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox226.StyleName = "";
            this.textBox226.Value = "=Fields.CreatorName";
            // 
            // textBox235
            // 
            this.textBox235.Format = "{0:C2}";
            this.textBox235.Name = "textBox235";
            this.textBox235.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.4819457530975342D), Telerik.Reporting.Drawing.Unit.Cm(0.7999998927116394D));
            this.textBox235.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox235.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox235.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox235.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox235.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox235.Style.Font.Name = "微软雅黑";
            this.textBox235.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox235.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox235.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox235.StyleName = "";
            this.textBox235.Value = "打印时间:";
            // 
            // textBox241
            // 
            this.textBox241.Name = "textBox241";
            this.textBox241.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.018082857131958D), Telerik.Reporting.Drawing.Unit.Cm(0.7999998927116394D));
            this.textBox241.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox241.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox241.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox241.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox241.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox241.Style.Font.Name = "微软雅黑";
            this.textBox241.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox241.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox241.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox241.StyleName = "";
            this.textBox241.Value = "打印条数:";
            // 
            // textBox228
            // 
            this.textBox228.Name = "textBox228";
            this.textBox228.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.4885988235473633D), Telerik.Reporting.Drawing.Unit.Cm(0.7999998927116394D));
            this.textBox228.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox228.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox228.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox228.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox228.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox228.Style.Font.Name = "微软雅黑";
            this.textBox228.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox228.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox228.StyleName = "";
            this.textBox228.Value = "= Sunlight.Silverlight.Dcs.Web.Reporting.CommonConverter.TotalRecordsConverterFor" +
    "mat(Count(Fields.id))";
            // 
            // textBox237
            // 
            this.textBox237.Name = "textBox237";
            this.textBox237.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.9112720489501953D), Telerik.Reporting.Drawing.Unit.Cm(0.7999998927116394D));
            this.textBox237.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox237.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox237.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox237.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox237.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox237.Style.Font.Name = "微软雅黑";
            this.textBox237.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox237.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox237.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox237.StyleName = "";
            this.textBox237.Value = "= Now()";
            // 
            // DataSourceVehicleDLRAccountFreezeBill
            // 
            this.DataSourceVehicleDLRAccountFreezeBill.DataMember = "GetReportVehicleDLRAccountFreezeBillsWithDetails";
            this.DataSourceVehicleDLRAccountFreezeBill.DataSource = typeof(Sunlight.Silverlight.Dcs.Web.DcsDomainService);
            this.DataSourceVehicleDLRAccountFreezeBill.Name = "DataSourceVehicleDLRAccountFreezeBill";
            this.DataSourceVehicleDLRAccountFreezeBill.Parameters.AddRange(new Telerik.Reporting.ObjectDataSourceParameter[] {
            new Telerik.Reporting.ObjectDataSourceParameter("code", typeof(string), "=Parameters.paraCode.Value"),
            new Telerik.Reporting.ObjectDataSourceParameter("customerCompanyName", typeof(string), "=Parameters.ParaCustomerCompanyName.Value"),
            new Telerik.Reporting.ObjectDataSourceParameter("vehicleFundsTypeId", typeof(System.Nullable<int>), "=Parameters.paraVehicleFundsTypeId.Value"),
            new Telerik.Reporting.ObjectDataSourceParameter("status", typeof(System.Nullable<int>), "=Parameters.ParaStatus.Value"),
            new Telerik.Reporting.ObjectDataSourceParameter("statDateTime", typeof(System.Nullable<System.DateTime>), "=Parameters.paraStartDateTime.Value"),
            new Telerik.Reporting.ObjectDataSourceParameter("endDateTime", typeof(System.Nullable<System.DateTime>), "=Parameters.ParaEndateTime.Value"),
            new Telerik.Reporting.ObjectDataSourceParameter("statApproveTime", typeof(System.Nullable<System.DateTime>), "=Parameters.paraStartApproveTime.Value"),
            new Telerik.Reporting.ObjectDataSourceParameter("endApproveTime", typeof(System.Nullable<System.DateTime>), "=Parameters.ParaEndApproveTime.Value")});
            // 
            // pageFooterSection1
            // 
            this.pageFooterSection1.Height = Telerik.Reporting.Drawing.Unit.Cm(1.3000001907348633D);
            this.pageFooterSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox4});
            this.pageFooterSection1.Name = "pageFooterSection1";
            // 
            // textBox4
            // 
            this.textBox4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.6000003814697266D), Telerik.Reporting.Drawing.Unit.Cm(0.19999989867210388D));
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.0339269638061523D), Telerik.Reporting.Drawing.Unit.Cm(0.79999995231628418D));
            this.textBox4.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox4.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox4.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox4.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox4.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox4.Style.Font.Name = "微软雅黑";
            this.textBox4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox4.StyleName = "";
            this.textBox4.Value = "= Sunlight.Silverlight.Dcs.Web.Reporting.CommonConverter.CurrentPagesConverterFor" +
    "mat(PageNumber)";
            // 
            // ReportVehicleDLRAccountFreezeBill
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pageHeaderSection1,
            this.detail,
            this.pageFooterSection1});
            this.Name = "ReportVehicleDLRAccountFreezeBill";
            this.PageSettings.Margins.Bottom = Telerik.Reporting.Drawing.Unit.Mm(9D);
            this.PageSettings.Margins.Left = Telerik.Reporting.Drawing.Unit.Pixel(19D);
            this.PageSettings.Margins.Right = Telerik.Reporting.Drawing.Unit.Pixel(19D);
            this.PageSettings.Margins.Top = Telerik.Reporting.Drawing.Unit.Mm(9D);
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            reportParameter1.AllowNull = true;
            reportParameter1.Name = "paraCode";
            reportParameter2.AllowNull = true;
            reportParameter2.Name = "ParaCustomerCompanyName";
            reportParameter3.AllowNull = true;
            reportParameter3.Name = "paraVehicleFundsTypeId";
            reportParameter3.Type = Telerik.Reporting.ReportParameterType.Integer;
            reportParameter4.AllowNull = true;
            reportParameter4.Name = "ParaStatus";
            reportParameter4.Type = Telerik.Reporting.ReportParameterType.Integer;
            reportParameter5.AllowNull = true;
            reportParameter5.Name = "paraStartDateTime";
            reportParameter5.Type = Telerik.Reporting.ReportParameterType.DateTime;
            reportParameter6.AllowNull = true;
            reportParameter6.Name = "ParaEndateTime";
            reportParameter6.Type = Telerik.Reporting.ReportParameterType.DateTime;
            reportParameter7.AllowNull = true;
            reportParameter7.Name = "paraStartApproveTime";
            reportParameter7.Type = Telerik.Reporting.ReportParameterType.DateTime;
            reportParameter8.AllowNull = true;
            reportParameter8.Name = "ParaEndApproveTime";
            reportParameter8.Type = Telerik.Reporting.ReportParameterType.DateTime;
            this.ReportParameters.Add(reportParameter1);
            this.ReportParameters.Add(reportParameter2);
            this.ReportParameters.Add(reportParameter3);
            this.ReportParameters.Add(reportParameter4);
            this.ReportParameters.Add(reportParameter5);
            this.ReportParameters.Add(reportParameter6);
            this.ReportParameters.Add(reportParameter7);
            this.ReportParameters.Add(reportParameter8);
            this.Style.BackgroundColor = System.Drawing.Color.White;
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
            this.Width = Telerik.Reporting.Drawing.Unit.Cm(20.012096405029297D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.PageHeaderSection pageHeaderSection1;
        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.ObjectDataSource DataSourceVehicleDLRAccountFreezeBill;
        private Telerik.Reporting.TextBox textBox13;
        private Telerik.Reporting.List list3;
        private Telerik.Reporting.TextBox textBox174;
        private Telerik.Reporting.TextBox textBox176;
        private Telerik.Reporting.TextBox textBox186;
        private Telerik.Reporting.TextBox textBox189;
        private Telerik.Reporting.TextBox textBox190;
        private Telerik.Reporting.TextBox textBox191;
        private Telerik.Reporting.TextBox textBox192;
        private Telerik.Reporting.TextBox textBox203;
        private Telerik.Reporting.TextBox textBox205;
        private Telerik.Reporting.TextBox textBox208;
        private Telerik.Reporting.TextBox textBox209;
        private Telerik.Reporting.TextBox textBox212;
        private Telerik.Reporting.TextBox textBox220;
        private Telerik.Reporting.TextBox textBox222;
        private Telerik.Reporting.TextBox textBox225;
        private Telerik.Reporting.TextBox textBox226;
        private Telerik.Reporting.TextBox textBox235;
        private Telerik.Reporting.TextBox textBox241;
        private Telerik.Reporting.TextBox textBox228;
        private Telerik.Reporting.TextBox textBox237;
        private Telerik.Reporting.PageFooterSection pageFooterSection1;
        private Telerik.Reporting.TextBox textBox4;
    }
}