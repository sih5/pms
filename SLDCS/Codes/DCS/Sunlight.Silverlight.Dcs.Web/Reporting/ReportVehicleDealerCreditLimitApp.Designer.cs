namespace Sunlight.Silverlight.Dcs.Web.Reporting {
    partial class ReportVehicleDealerCreditLimitApp {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup11 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup12 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup13 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup14 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup15 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup16 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.ReportParameter reportParameter1 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter2 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter3 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter4 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter5 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter6 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter7 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter8 = new Telerik.Reporting.ReportParameter();
            this.pageHeaderSection1 = new Telerik.Reporting.PageHeaderSection();
            this.textBox13 = new Telerik.Reporting.TextBox();
            this.detail = new Telerik.Reporting.DetailSection();
            this.list3 = new Telerik.Reporting.List();
            this.textBox174 = new Telerik.Reporting.TextBox();
            this.textBox176 = new Telerik.Reporting.TextBox();
            this.textBox186 = new Telerik.Reporting.TextBox();
            this.textBox189 = new Telerik.Reporting.TextBox();
            this.textBox190 = new Telerik.Reporting.TextBox();
            this.textBox191 = new Telerik.Reporting.TextBox();
            this.textBox192 = new Telerik.Reporting.TextBox();
            this.textBox203 = new Telerik.Reporting.TextBox();
            this.textBox205 = new Telerik.Reporting.TextBox();
            this.textBox208 = new Telerik.Reporting.TextBox();
            this.textBox209 = new Telerik.Reporting.TextBox();
            this.textBox212 = new Telerik.Reporting.TextBox();
            this.textBox213 = new Telerik.Reporting.TextBox();
            this.textBox219 = new Telerik.Reporting.TextBox();
            this.textBox220 = new Telerik.Reporting.TextBox();
            this.textBox222 = new Telerik.Reporting.TextBox();
            this.textBox225 = new Telerik.Reporting.TextBox();
            this.textBox226 = new Telerik.Reporting.TextBox();
            this.textBox235 = new Telerik.Reporting.TextBox();
            this.textBox241 = new Telerik.Reporting.TextBox();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.textBox11 = new Telerik.Reporting.TextBox();
            this.textBox12 = new Telerik.Reporting.TextBox();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.textBox23 = new Telerik.Reporting.TextBox();
            this.textBox24 = new Telerik.Reporting.TextBox();
            this.textBox25 = new Telerik.Reporting.TextBox();
            this.textBox26 = new Telerik.Reporting.TextBox();
            this.textBox228 = new Telerik.Reporting.TextBox();
            this.textBox237 = new Telerik.Reporting.TextBox();
            this.dataSourceRpt = new Telerik.Reporting.ObjectDataSource();
            this.pageFooterSection1 = new Telerik.Reporting.PageFooterSection();
            this.textBox4 = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeaderSection1
            // 
            this.pageHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Cm(1.6000000238418579D);
            this.pageHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox13});
            this.pageHeaderSection1.Name = "pageHeaderSection1";
            // 
            // textBox13
            // 
            this.textBox13.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.2327003479003906D), Telerik.Reporting.Drawing.Unit.Cm(0.40000000596046448D));
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(11.300000190734863D), Telerik.Reporting.Drawing.Unit.Cm(0.89999997615814209D));
            this.textBox13.Style.Font.Name = "΢���ź�";
            this.textBox13.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(20D);
            this.textBox13.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox13.Value = "���ö�����뵥";
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Cm(3.2000000476837158D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.list3});
            this.detail.Name = "detail";
            // 
            // list3
            // 
            this.list3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.0074934959411621D)));
            this.list3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.0275909900665283D)));
            this.list3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.8206934928894043D)));
            this.list3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.5884480476379395D)));
            this.list3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.6247849464416504D)));
            this.list3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.2313439846038818D)));
            this.list3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.2313468456268311D)));
            this.list3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.2313439846038818D)));
            this.list3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.2313439846038818D)));
            this.list3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.60999971628189087D)));
            this.list3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.79999995231628418D)));
            this.list3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.79999995231628418D)));
            this.list3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.79999995231628418D)));
            this.list3.Body.SetCellContent(0, 0, this.textBox174);
            this.list3.Body.SetCellContent(0, 1, this.textBox176);
            this.list3.Body.SetCellContent(1, 0, this.textBox186);
            this.list3.Body.SetCellContent(1, 1, this.textBox189);
            this.list3.Body.SetCellContent(0, 2, this.textBox190);
            this.list3.Body.SetCellContent(1, 2, this.textBox191);
            this.list3.Body.SetCellContent(0, 3, this.textBox192);
            this.list3.Body.SetCellContent(1, 3, this.textBox203);
            this.list3.Body.SetCellContent(0, 4, this.textBox205);
            this.list3.Body.SetCellContent(1, 4, this.textBox208);
            this.list3.Body.SetCellContent(0, 5, this.textBox209);
            this.list3.Body.SetCellContent(1, 5, this.textBox212);
            this.list3.Body.SetCellContent(0, 6, this.textBox213);
            this.list3.Body.SetCellContent(1, 6, this.textBox219);
            this.list3.Body.SetCellContent(0, 8, this.textBox220);
            this.list3.Body.SetCellContent(1, 8, this.textBox222);
            this.list3.Body.SetCellContent(0, 7, this.textBox225);
            this.list3.Body.SetCellContent(1, 7, this.textBox226);
            this.list3.Body.SetCellContent(3, 5, this.textBox235);
            this.list3.Body.SetCellContent(3, 0, this.textBox241, 1, 2);
            this.list3.Body.SetCellContent(2, 2, this.textBox2);
            this.list3.Body.SetCellContent(2, 3, this.textBox11);
            this.list3.Body.SetCellContent(2, 4, this.textBox12);
            this.list3.Body.SetCellContent(2, 5, this.textBox1);
            this.list3.Body.SetCellContent(2, 6, this.textBox23);
            this.list3.Body.SetCellContent(2, 7, this.textBox24);
            this.list3.Body.SetCellContent(2, 8, this.textBox25);
            this.list3.Body.SetCellContent(2, 0, this.textBox26, 1, 2);
            this.list3.Body.SetCellContent(3, 2, this.textBox228, 1, 3);
            this.list3.Body.SetCellContent(3, 6, this.textBox237, 1, 3);
            tableGroup1.Name = "ColumnGroup";
            tableGroup2.Name = "Group1";
            tableGroup3.Name = "Group3";
            tableGroup4.Name = "Group5";
            tableGroup5.Name = "Group6";
            tableGroup6.Name = "Group8";
            tableGroup7.Name = "Group9";
            tableGroup8.Name = "Group11";
            tableGroup9.Name = "Group10";
            this.list3.ColumnGroups.Add(tableGroup1);
            this.list3.ColumnGroups.Add(tableGroup2);
            this.list3.ColumnGroups.Add(tableGroup3);
            this.list3.ColumnGroups.Add(tableGroup4);
            this.list3.ColumnGroups.Add(tableGroup5);
            this.list3.ColumnGroups.Add(tableGroup6);
            this.list3.ColumnGroups.Add(tableGroup7);
            this.list3.ColumnGroups.Add(tableGroup8);
            this.list3.ColumnGroups.Add(tableGroup9);
            this.list3.ColumnHeadersPrintOnEveryPage = true;
            this.list3.DataSource = this.dataSourceRpt;
            this.list3.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox174,
            this.textBox176,
            this.textBox186,
            this.textBox189,
            this.textBox190,
            this.textBox191,
            this.textBox192,
            this.textBox203,
            this.textBox205,
            this.textBox208,
            this.textBox209,
            this.textBox212,
            this.textBox213,
            this.textBox219,
            this.textBox220,
            this.textBox222,
            this.textBox225,
            this.textBox226,
            this.textBox235,
            this.textBox241,
            this.textBox2,
            this.textBox11,
            this.textBox12,
            this.textBox1,
            this.textBox23,
            this.textBox24,
            this.textBox25,
            this.textBox26,
            this.textBox228,
            this.textBox237});
            this.list3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(0.0793749988079071D));
            this.list3.Name = "list3";
            tableGroup10.Name = "Group2";
            tableGroup12.Name = "Group4";
            tableGroup11.ChildGroups.Add(tableGroup12);
            tableGroup11.Groupings.AddRange(new Telerik.Reporting.Grouping[] {
            new Telerik.Reporting.Grouping(null)});
            tableGroup11.Name = "DetailGroup";
            tableGroup14.Name = "Group14";
            tableGroup13.ChildGroups.Add(tableGroup14);
            tableGroup13.Name = "Group13";
            tableGroup16.Name = "Group12";
            tableGroup15.ChildGroups.Add(tableGroup16);
            tableGroup15.Name = "Group7";
            this.list3.RowGroups.Add(tableGroup10);
            this.list3.RowGroups.Add(tableGroup11);
            this.list3.RowGroups.Add(tableGroup13);
            this.list3.RowGroups.Add(tableGroup15);
            this.list3.RowHeadersPrintOnEveryPage = true;
            this.list3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.9943904876709D), Telerik.Reporting.Drawing.Unit.Cm(3.0099997520446777D));
            this.list3.Style.Font.Name = "΢���ź�";
            // 
            // textBox174
            // 
            this.textBox174.Name = "textBox174";
            this.textBox174.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.0074933767318726D), Telerik.Reporting.Drawing.Unit.Cm(0.6099998950958252D));
            this.textBox174.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox174.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox174.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox174.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox174.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox174.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox174.Style.Font.Name = "΢���ź�";
            this.textBox174.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox174.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox174.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox174.Style.Visible = true;
            this.textBox174.StyleName = "";
            this.textBox174.Value = "���";
            // 
            // textBox176
            // 
            this.textBox176.Name = "textBox176";
            this.textBox176.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.0275907516479492D), Telerik.Reporting.Drawing.Unit.Cm(0.60999995470047D));
            this.textBox176.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox176.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox176.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox176.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox176.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox176.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox176.Style.Font.Name = "΢���ź�";
            this.textBox176.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox176.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox176.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox176.Style.Visible = true;
            this.textBox176.StyleName = "";
            this.textBox176.Value = "���";
            // 
            // textBox186
            // 
            this.textBox186.Name = "textBox186";
            this.textBox186.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.0074933767318726D), Telerik.Reporting.Drawing.Unit.Cm(0.80000007152557373D));
            this.textBox186.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox186.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox186.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox186.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox186.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox186.Style.Font.Name = "΢���ź�";
            this.textBox186.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox186.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox186.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox186.StyleName = "";
            this.textBox186.Value = "=Fields.Id";
            // 
            // textBox189
            // 
            this.textBox189.Name = "textBox189";
            this.textBox189.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.0275907516479492D), Telerik.Reporting.Drawing.Unit.Cm(0.79999995231628418D));
            this.textBox189.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox189.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox189.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox189.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox189.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox189.Style.Font.Name = "΢���ź�";
            this.textBox189.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox189.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox189.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox189.StyleName = "";
            this.textBox189.Value = "=Fields.Code";
            // 
            // textBox190
            // 
            this.textBox190.Name = "textBox190";
            this.textBox190.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8206934928894043D), Telerik.Reporting.Drawing.Unit.Cm(0.6099998950958252D));
            this.textBox190.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox190.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox190.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox190.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox190.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox190.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox190.Style.Font.Name = "΢���ź�";
            this.textBox190.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox190.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox190.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox190.Style.Visible = true;
            this.textBox190.StyleName = "";
            this.textBox190.Value = "�����̱��";
            // 
            // textBox191
            // 
            this.textBox191.Name = "textBox191";
            this.textBox191.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8206934928894043D), Telerik.Reporting.Drawing.Unit.Cm(0.80000007152557373D));
            this.textBox191.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox191.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox191.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox191.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox191.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox191.Style.Font.Name = "΢���ź�";
            this.textBox191.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox191.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox191.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox191.StyleName = "";
            this.textBox191.Value = "=Fields.CustomerCompanyCode";
            // 
            // textBox192
            // 
            this.textBox192.Name = "textBox192";
            this.textBox192.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.5884473323822021D), Telerik.Reporting.Drawing.Unit.Cm(0.60999995470047D));
            this.textBox192.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox192.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox192.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox192.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox192.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox192.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox192.Style.Font.Name = "΢���ź�";
            this.textBox192.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox192.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox192.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox192.Style.Visible = true;
            this.textBox192.StyleName = "";
            this.textBox192.Value = "����������";
            // 
            // textBox203
            // 
            this.textBox203.Format = "{0:C2}";
            this.textBox203.Name = "textBox203";
            this.textBox203.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.5884473323822021D), Telerik.Reporting.Drawing.Unit.Cm(0.79999995231628418D));
            this.textBox203.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox203.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox203.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox203.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox203.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox203.Style.Font.Name = "΢���ź�";
            this.textBox203.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox203.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox203.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox203.StyleName = "";
            this.textBox203.Value = "=Fields.CustomerCompanyName";
            // 
            // textBox205
            // 
            this.textBox205.Name = "textBox205";
            this.textBox205.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.624783992767334D), Telerik.Reporting.Drawing.Unit.Cm(0.60999995470047D));
            this.textBox205.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox205.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox205.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox205.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox205.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox205.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox205.Style.Font.Name = "΢���ź�";
            this.textBox205.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox205.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox205.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox205.Style.Visible = true;
            this.textBox205.StyleName = "";
            this.textBox205.Value = "�ʽ�����";
            // 
            // textBox208
            // 
            this.textBox208.Format = "";
            this.textBox208.Name = "textBox208";
            this.textBox208.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.624783992767334D), Telerik.Reporting.Drawing.Unit.Cm(0.79999995231628418D));
            this.textBox208.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox208.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox208.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox208.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox208.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox208.Style.Font.Name = "΢���ź�";
            this.textBox208.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox208.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox208.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox208.StyleName = "";
            this.textBox208.Value = "=Fields.VehicleFundsType.Name";
            // 
            // textBox209
            // 
            this.textBox209.Name = "textBox209";
            this.textBox209.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.2313439846038818D), Telerik.Reporting.Drawing.Unit.Cm(0.60999995470047D));
            this.textBox209.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox209.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox209.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox209.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox209.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox209.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox209.Style.Font.Name = "΢���ź�";
            this.textBox209.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox209.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox209.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox209.Style.Visible = true;
            this.textBox209.StyleName = "";
            this.textBox209.Value = "�����ܶ�";
            // 
            // textBox212
            // 
            this.textBox212.Format = "{0:C2}";
            this.textBox212.Name = "textBox212";
            this.textBox212.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.2313439846038818D), Telerik.Reporting.Drawing.Unit.Cm(0.79999995231628418D));
            this.textBox212.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox212.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox212.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox212.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox212.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox212.Style.Font.Name = "΢���ź�";
            this.textBox212.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox212.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox212.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox212.StyleName = "";
            this.textBox212.Value = "=Fields.CredenceLimit";
            // 
            // textBox213
            // 
            this.textBox213.Name = "textBox213";
            this.textBox213.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.2313458919525146D), Telerik.Reporting.Drawing.Unit.Cm(0.60999995470047D));
            this.textBox213.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox213.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox213.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox213.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox213.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox213.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox213.Style.Font.Name = "΢���ź�";
            this.textBox213.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox213.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox213.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox213.Style.Visible = true;
            this.textBox213.StyleName = "";
            this.textBox213.Value = "ʵЧ����";
            // 
            // textBox219
            // 
            this.textBox219.Format = "{0:d}";
            this.textBox219.Name = "textBox219";
            this.textBox219.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.2313458919525146D), Telerik.Reporting.Drawing.Unit.Cm(0.79999995231628418D));
            this.textBox219.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox219.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox219.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox219.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox219.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox219.Style.Font.Name = "΢���ź�";
            this.textBox219.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox219.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox219.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox219.StyleName = "";
            this.textBox219.Value = "=Fields.ExpireDate";
            // 
            // textBox220
            // 
            this.textBox220.Name = "textBox220";
            this.textBox220.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.2313439846038818D), Telerik.Reporting.Drawing.Unit.Cm(0.60999995470047D));
            this.textBox220.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox220.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox220.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox220.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox220.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox220.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox220.Style.Font.Name = "΢���ź�";
            this.textBox220.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox220.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox220.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox220.Style.Visible = true;
            this.textBox220.StyleName = "";
            this.textBox220.Value = "����ʱ��";
            // 
            // textBox222
            // 
            this.textBox222.Format = "{0:d}";
            this.textBox222.Name = "textBox222";
            this.textBox222.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.2313439846038818D), Telerik.Reporting.Drawing.Unit.Cm(0.79999995231628418D));
            this.textBox222.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox222.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox222.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox222.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox222.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox222.Style.Font.Name = "΢���ź�";
            this.textBox222.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox222.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox222.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox222.StyleName = "";
            this.textBox222.Value = "=Fields.CreateTime";
            // 
            // textBox225
            // 
            this.textBox225.Name = "textBox225";
            this.textBox225.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.2313439846038818D), Telerik.Reporting.Drawing.Unit.Cm(0.60999995470047D));
            this.textBox225.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox225.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox225.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox225.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox225.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox225.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox225.Style.Font.Name = "΢���ź�";
            this.textBox225.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox225.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox225.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox225.Style.Visible = true;
            this.textBox225.StyleName = "";
            this.textBox225.Value = "������";
            // 
            // textBox226
            // 
            this.textBox226.Name = "textBox226";
            this.textBox226.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.2313439846038818D), Telerik.Reporting.Drawing.Unit.Cm(0.79999995231628418D));
            this.textBox226.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox226.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox226.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox226.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox226.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox226.Style.Font.Name = "΢���ź�";
            this.textBox226.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox226.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox226.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox226.StyleName = "";
            this.textBox226.Value = "=Fields.CreatorName";
            // 
            // textBox235
            // 
            this.textBox235.Format = "{0:C2}";
            this.textBox235.Name = "textBox235";
            this.textBox235.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.2313439846038818D), Telerik.Reporting.Drawing.Unit.Cm(0.79999995231628418D));
            this.textBox235.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox235.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox235.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox235.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox235.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox235.Style.Font.Name = "΢���ź�";
            this.textBox235.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox235.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox235.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox235.StyleName = "";
            this.textBox235.Value = "��ӡʱ��:";
            // 
            // textBox241
            // 
            this.textBox241.Name = "textBox241";
            this.textBox241.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.0350847244262695D), Telerik.Reporting.Drawing.Unit.Cm(0.79999995231628418D));
            this.textBox241.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox241.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox241.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox241.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox241.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox241.Style.Font.Name = "΢���ź�";
            this.textBox241.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox241.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox241.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox241.StyleName = "";
            this.textBox241.Value = "��ӡ����:";
            // 
            // textBox2
            // 
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8206934928894043D), Telerik.Reporting.Drawing.Unit.Cm(0.79999995231628418D));
            this.textBox2.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox2.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox2.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox2.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox2.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox2.Style.Font.Name = "΢���ź�";
            this.textBox2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox2.StyleName = "";
            // 
            // textBox11
            // 
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.5884480476379395D), Telerik.Reporting.Drawing.Unit.Cm(0.79999995231628418D));
            this.textBox11.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox11.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox11.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox11.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox11.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox11.Style.Font.Name = "΢���ź�";
            this.textBox11.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox11.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox11.StyleName = "";
            // 
            // textBox12
            // 
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.6247849464416504D), Telerik.Reporting.Drawing.Unit.Cm(0.79999995231628418D));
            this.textBox12.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox12.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox12.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox12.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox12.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox12.Style.Font.Name = "΢���ź�";
            this.textBox12.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox12.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox12.StyleName = "";
            // 
            // textBox1
            // 
            this.textBox1.Format = "{0:C2}";
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.2313439846038818D), Telerik.Reporting.Drawing.Unit.Cm(0.79999995231628418D));
            this.textBox1.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox1.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox1.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox1.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox1.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox1.Style.Font.Name = "΢���ź�";
            this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox1.StyleName = "";
            this.textBox1.Value = "= Sum(Fields.CredenceLimit)";
            // 
            // textBox23
            // 
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.2313468456268311D), Telerik.Reporting.Drawing.Unit.Cm(0.79999995231628418D));
            this.textBox23.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox23.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox23.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox23.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox23.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox23.Style.Font.Name = "΢���ź�";
            this.textBox23.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox23.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox23.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox23.StyleName = "";
            // 
            // textBox24
            // 
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.2313439846038818D), Telerik.Reporting.Drawing.Unit.Cm(0.79999995231628418D));
            this.textBox24.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox24.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox24.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox24.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox24.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox24.Style.Font.Name = "΢���ź�";
            this.textBox24.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox24.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox24.StyleName = "";
            // 
            // textBox25
            // 
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.2313439846038818D), Telerik.Reporting.Drawing.Unit.Cm(0.79999995231628418D));
            this.textBox25.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox25.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox25.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox25.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox25.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox25.Style.Font.Name = "΢���ź�";
            this.textBox25.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox25.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox25.StyleName = "";
            // 
            // textBox26
            // 
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.0350844860076904D), Telerik.Reporting.Drawing.Unit.Cm(0.79999995231628418D));
            this.textBox26.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox26.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox26.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox26.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox26.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox26.Style.Font.Name = "΢���ź�";
            this.textBox26.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox26.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox26.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox26.StyleName = "";
            this.textBox26.Value = "�ܼƣ�";
            // 
            // textBox228
            // 
            this.textBox228.Name = "textBox228";
            this.textBox228.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.0339269638061523D), Telerik.Reporting.Drawing.Unit.Cm(0.79999995231628418D));
            this.textBox228.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox228.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox228.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox228.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox228.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox228.Style.Font.Name = "΢���ź�";
            this.textBox228.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox228.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox228.StyleName = "";
            this.textBox228.Value = "= Sunlight.Silverlight.Dcs.Web.Reporting.CommonConverter.TotalRecordsConverterFor" +
    "mat(Count(Fields.id))";
            // 
            // textBox237
            // 
            this.textBox237.Name = "textBox237";
            this.textBox237.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.6940345764160156D), Telerik.Reporting.Drawing.Unit.Cm(0.79999995231628418D));
            this.textBox237.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox237.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox237.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox237.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox237.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox237.Style.Font.Name = "΢���ź�";
            this.textBox237.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox237.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox237.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox237.StyleName = "";
            this.textBox237.Value = "= Now()";
            // 
            // dataSourceRpt
            // 
            this.dataSourceRpt.DataMember = "GetReportVehicleDealerCreditLimitAppsWithVehicleFundsType";
            this.dataSourceRpt.DataSource = typeof(Sunlight.Silverlight.Dcs.Web.DcsDomainService);
            this.dataSourceRpt.Name = "dataSourceRpt";
            this.dataSourceRpt.Parameters.AddRange(new Telerik.Reporting.ObjectDataSourceParameter[] {
            new Telerik.Reporting.ObjectDataSourceParameter("code", typeof(string), "=Parameters.paraCode.Value"),
            new Telerik.Reporting.ObjectDataSourceParameter("customerCompanyName", typeof(string), "=Parameters.ParaName.Value"),
            new Telerik.Reporting.ObjectDataSourceParameter("vehicleFundsTypeId", typeof(System.Nullable<int>), "=Parameters.paraVehicleFundsTypeId.Value"),
            new Telerik.Reporting.ObjectDataSourceParameter("status", typeof(System.Nullable<int>), "=Parameters.ParaStatus.Value"),
            new Telerik.Reporting.ObjectDataSourceParameter("statDateTime", typeof(System.Nullable<System.DateTime>), "=Parameters.paraStatDateTime.Value"),
            new Telerik.Reporting.ObjectDataSourceParameter("endDateTime", typeof(System.Nullable<System.DateTime>), "=Parameters.ParaEndDateTime.Value"),
            new Telerik.Reporting.ObjectDataSourceParameter("statApproveTime", typeof(System.Nullable<System.DateTime>), "=Parameters.paraStatApproveTime.Value"),
            new Telerik.Reporting.ObjectDataSourceParameter("endApproveTime", typeof(System.Nullable<System.DateTime>), "=Parameters.ParaEndApproveTime.Value")});
            // 
            // pageFooterSection1
            // 
            this.pageFooterSection1.Height = Telerik.Reporting.Drawing.Unit.Cm(1.2999997138977051D);
            this.pageFooterSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox4});
            this.pageFooterSection1.Name = "pageFooterSection1";
            // 
            // textBox4
            // 
            this.textBox4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.09999942779541D), Telerik.Reporting.Drawing.Unit.Cm(0.19999989867210388D));
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.0339269638061523D), Telerik.Reporting.Drawing.Unit.Cm(0.79999995231628418D));
            this.textBox4.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox4.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox4.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox4.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox4.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox4.Style.Font.Name = "΢���ź�";
            this.textBox4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox4.StyleName = "";
            this.textBox4.Value = "= Sunlight.Silverlight.Dcs.Web.Reporting.CommonConverter.CurrentPagesConverterFor" +
    "mat(PageNumber)";
            // 
            // ReportVehicleDealerCreditLimitApp
            // 
            this.DataSource = null;
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pageHeaderSection1,
            this.detail,
            this.pageFooterSection1});
            this.Name = "ReportVehicleDealerCreditLimitApp";
            this.PageSettings.Margins.Bottom = Telerik.Reporting.Drawing.Unit.Mm(9D);
            this.PageSettings.Margins.Left = Telerik.Reporting.Drawing.Unit.Pixel(19D);
            this.PageSettings.Margins.Right = Telerik.Reporting.Drawing.Unit.Pixel(19D);
            this.PageSettings.Margins.Top = Telerik.Reporting.Drawing.Unit.Mm(9D);
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            reportParameter1.AllowNull = true;
            reportParameter1.Name = "paraCode";
            reportParameter2.AllowNull = true;
            reportParameter2.Name = "ParaName";
            reportParameter3.AllowNull = true;
            reportParameter3.Name = "paraVehicleFundsTypeId";
            reportParameter3.Type = Telerik.Reporting.ReportParameterType.Integer;
            reportParameter4.AllowNull = true;
            reportParameter4.Name = "ParaStatus";
            reportParameter4.Type = Telerik.Reporting.ReportParameterType.Integer;
            reportParameter5.AllowNull = true;
            reportParameter5.Name = "paraStatDateTime";
            reportParameter5.Type = Telerik.Reporting.ReportParameterType.DateTime;
            reportParameter6.AllowNull = true;
            reportParameter6.Name = "ParaEndDateTime";
            reportParameter6.Type = Telerik.Reporting.ReportParameterType.DateTime;
            reportParameter7.AllowNull = true;
            reportParameter7.Name = "paraStatApproveTime";
            reportParameter7.Type = Telerik.Reporting.ReportParameterType.DateTime;
            reportParameter8.AllowNull = true;
            reportParameter8.Name = "ParaEndApproveTime";
            reportParameter8.Type = Telerik.Reporting.ReportParameterType.Integer;
            this.ReportParameters.Add(reportParameter1);
            this.ReportParameters.Add(reportParameter2);
            this.ReportParameters.Add(reportParameter3);
            this.ReportParameters.Add(reportParameter4);
            this.ReportParameters.Add(reportParameter5);
            this.ReportParameters.Add(reportParameter6);
            this.ReportParameters.Add(reportParameter7);
            this.ReportParameters.Add(reportParameter8);
            this.Style.BackgroundColor = System.Drawing.Color.White;
            this.Width = Telerik.Reporting.Drawing.Unit.Cm(20D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.PageHeaderSection pageHeaderSection1;
        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.ObjectDataSource dataSourceRpt;
        private Telerik.Reporting.TextBox textBox13;
        private Telerik.Reporting.List list3;
        private Telerik.Reporting.TextBox textBox174;
        private Telerik.Reporting.TextBox textBox176;
        private Telerik.Reporting.TextBox textBox186;
        private Telerik.Reporting.TextBox textBox189;
        private Telerik.Reporting.TextBox textBox190;
        private Telerik.Reporting.TextBox textBox191;
        private Telerik.Reporting.TextBox textBox192;
        private Telerik.Reporting.TextBox textBox203;
        private Telerik.Reporting.TextBox textBox205;
        private Telerik.Reporting.TextBox textBox208;
        private Telerik.Reporting.TextBox textBox209;
        private Telerik.Reporting.TextBox textBox212;
        private Telerik.Reporting.TextBox textBox213;
        private Telerik.Reporting.TextBox textBox219;
        private Telerik.Reporting.TextBox textBox220;
        private Telerik.Reporting.TextBox textBox222;
        private Telerik.Reporting.TextBox textBox225;
        private Telerik.Reporting.TextBox textBox226;
        private Telerik.Reporting.TextBox textBox235;
        private Telerik.Reporting.TextBox textBox241;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox11;
        private Telerik.Reporting.TextBox textBox12;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.TextBox textBox23;
        private Telerik.Reporting.TextBox textBox24;
        private Telerik.Reporting.TextBox textBox25;
        private Telerik.Reporting.TextBox textBox26;
        private Telerik.Reporting.TextBox textBox228;
        private Telerik.Reporting.TextBox textBox237;
        private Telerik.Reporting.PageFooterSection pageFooterSection1;
        private Telerik.Reporting.TextBox textBox4;
    }
}