﻿namespace Sunlight.Silverlight.Dcs.Web.Reporting {
    partial class ReportVehiclePaymentBill {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup11 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup12 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup13 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup14 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup15 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup16 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup17 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup18 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.ReportParameter reportParameter1 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter2 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter3 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter4 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter5 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter6 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter7 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter8 = new Telerik.Reporting.ReportParameter();
            this.pageHeaderSection1 = new Telerik.Reporting.PageHeaderSection();
            this.textBox19 = new Telerik.Reporting.TextBox();
            this.detail = new Telerik.Reporting.DetailSection();
            this.list3 = new Telerik.Reporting.List();
            this.textBox174 = new Telerik.Reporting.TextBox();
            this.textBox176 = new Telerik.Reporting.TextBox();
            this.textBox186 = new Telerik.Reporting.TextBox();
            this.textBox189 = new Telerik.Reporting.TextBox();
            this.textBox190 = new Telerik.Reporting.TextBox();
            this.textBox191 = new Telerik.Reporting.TextBox();
            this.textBox192 = new Telerik.Reporting.TextBox();
            this.textBox203 = new Telerik.Reporting.TextBox();
            this.textBox205 = new Telerik.Reporting.TextBox();
            this.textBox208 = new Telerik.Reporting.TextBox();
            this.textBox209 = new Telerik.Reporting.TextBox();
            this.textBox212 = new Telerik.Reporting.TextBox();
            this.textBox213 = new Telerik.Reporting.TextBox();
            this.textBox219 = new Telerik.Reporting.TextBox();
            this.textBox220 = new Telerik.Reporting.TextBox();
            this.textBox222 = new Telerik.Reporting.TextBox();
            this.textBox225 = new Telerik.Reporting.TextBox();
            this.textBox226 = new Telerik.Reporting.TextBox();
            this.textBox235 = new Telerik.Reporting.TextBox();
            this.textBox241 = new Telerik.Reporting.TextBox();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.textBox24 = new Telerik.Reporting.TextBox();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.textBox7 = new Telerik.Reporting.TextBox();
            this.textBox228 = new Telerik.Reporting.TextBox();
            this.textBox237 = new Telerik.Reporting.TextBox();
            this.textBox8 = new Telerik.Reporting.TextBox();
            this.textBox9 = new Telerik.Reporting.TextBox();
            this.textBox10 = new Telerik.Reporting.TextBox();
            this.textBox36 = new Telerik.Reporting.TextBox();
            this.textBox37 = new Telerik.Reporting.TextBox();
            this.textBox38 = new Telerik.Reporting.TextBox();
            this.textBox39 = new Telerik.Reporting.TextBox();
            this.textBox40 = new Telerik.Reporting.TextBox();
            this.DataSourceVehiclePaymentBill = new Telerik.Reporting.ObjectDataSource();
            this.pageFooterSection1 = new Telerik.Reporting.PageFooterSection();
            this.textBox23 = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeaderSection1
            // 
            this.pageHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Cm(1.9318748712539673D);
            this.pageHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox19});
            this.pageHeaderSection1.Name = "pageHeaderSection1";
            // 
            // textBox19
            // 
            this.textBox19.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.2000002861022949D), Telerik.Reporting.Drawing.Unit.Cm(0.60000008344650269D));
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(11.300000190734863D), Telerik.Reporting.Drawing.Unit.Cm(0.89999997615814209D));
            this.textBox19.Style.Font.Name = "微软雅黑";
            this.textBox19.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(20D);
            this.textBox19.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox19.Value = "来款单";
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Cm(3.1681249141693115D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.list3});
            this.detail.Name = "detail";
            // 
            // list3
            // 
            this.list3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.79349035024642944D)));
            this.list3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.4942113161087036D)));
            this.list3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.9428068399429321D)));
            this.list3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.9225183725357056D)));
            this.list3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.6675453186035156D)));
            this.list3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.5917834043502808D)));
            this.list3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.7577873468399048D)));
            this.list3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.9953615665435791D)));
            this.list3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.6982321739196777D)));
            this.list3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.1269791126251221D)));
            this.list3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.9622002840042114D)));
            this.list3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.60999971628189087D)));
            this.list3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.79999995231628418D)));
            this.list3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.79999995231628418D)));
            this.list3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.79999995231628418D)));
            this.list3.Body.SetCellContent(0, 0, this.textBox174);
            this.list3.Body.SetCellContent(0, 1, this.textBox176);
            this.list3.Body.SetCellContent(1, 0, this.textBox186);
            this.list3.Body.SetCellContent(1, 1, this.textBox189);
            this.list3.Body.SetCellContent(0, 2, this.textBox190);
            this.list3.Body.SetCellContent(1, 2, this.textBox191);
            this.list3.Body.SetCellContent(0, 3, this.textBox192);
            this.list3.Body.SetCellContent(1, 3, this.textBox203);
            this.list3.Body.SetCellContent(0, 4, this.textBox205);
            this.list3.Body.SetCellContent(1, 4, this.textBox208);
            this.list3.Body.SetCellContent(0, 5, this.textBox209);
            this.list3.Body.SetCellContent(1, 5, this.textBox212);
            this.list3.Body.SetCellContent(0, 8, this.textBox213);
            this.list3.Body.SetCellContent(1, 8, this.textBox219);
            this.list3.Body.SetCellContent(0, 10, this.textBox220);
            this.list3.Body.SetCellContent(1, 10, this.textBox222);
            this.list3.Body.SetCellContent(0, 9, this.textBox225);
            this.list3.Body.SetCellContent(1, 9, this.textBox226);
            this.list3.Body.SetCellContent(3, 5, this.textBox235);
            this.list3.Body.SetCellContent(3, 0, this.textBox241, 1, 2);
            this.list3.Body.SetCellContent(2, 2, this.textBox2);
            this.list3.Body.SetCellContent(2, 3, this.textBox1);
            this.list3.Body.SetCellContent(2, 4, this.textBox3);
            this.list3.Body.SetCellContent(2, 5, this.textBox4);
            this.list3.Body.SetCellContent(2, 8, this.textBox5);
            this.list3.Body.SetCellContent(2, 9, this.textBox24);
            this.list3.Body.SetCellContent(2, 10, this.textBox6);
            this.list3.Body.SetCellContent(2, 0, this.textBox7, 1, 2);
            this.list3.Body.SetCellContent(3, 2, this.textBox228, 1, 3);
            this.list3.Body.SetCellContent(3, 8, this.textBox237, 1, 3);
            this.list3.Body.SetCellContent(0, 6, this.textBox8);
            this.list3.Body.SetCellContent(1, 6, this.textBox9);
            this.list3.Body.SetCellContent(2, 6, this.textBox10);
            this.list3.Body.SetCellContent(3, 6, this.textBox36);
            this.list3.Body.SetCellContent(0, 7, this.textBox37);
            this.list3.Body.SetCellContent(1, 7, this.textBox38);
            this.list3.Body.SetCellContent(2, 7, this.textBox39);
            this.list3.Body.SetCellContent(3, 7, this.textBox40);
            tableGroup1.Name = "ColumnGroup";
            tableGroup2.Name = "Group1";
            tableGroup3.Name = "Group3";
            tableGroup4.Name = "Group5";
            tableGroup5.Name = "Group6";
            tableGroup6.Name = "Group8";
            tableGroup7.Name = "Group15";
            tableGroup8.Name = "Group16";
            tableGroup9.Name = "Group9";
            tableGroup10.Name = "Group11";
            tableGroup11.Name = "Group10";
            this.list3.ColumnGroups.Add(tableGroup1);
            this.list3.ColumnGroups.Add(tableGroup2);
            this.list3.ColumnGroups.Add(tableGroup3);
            this.list3.ColumnGroups.Add(tableGroup4);
            this.list3.ColumnGroups.Add(tableGroup5);
            this.list3.ColumnGroups.Add(tableGroup6);
            this.list3.ColumnGroups.Add(tableGroup7);
            this.list3.ColumnGroups.Add(tableGroup8);
            this.list3.ColumnGroups.Add(tableGroup9);
            this.list3.ColumnGroups.Add(tableGroup10);
            this.list3.ColumnGroups.Add(tableGroup11);
            this.list3.ColumnHeadersPrintOnEveryPage = false;
            this.list3.DataSource = this.DataSourceVehiclePaymentBill;
            this.list3.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox174,
            this.textBox176,
            this.textBox186,
            this.textBox189,
            this.textBox190,
            this.textBox191,
            this.textBox192,
            this.textBox203,
            this.textBox205,
            this.textBox208,
            this.textBox209,
            this.textBox212,
            this.textBox213,
            this.textBox219,
            this.textBox220,
            this.textBox222,
            this.textBox225,
            this.textBox226,
            this.textBox235,
            this.textBox241,
            this.textBox2,
            this.textBox1,
            this.textBox3,
            this.textBox4,
            this.textBox5,
            this.textBox24,
            this.textBox6,
            this.textBox7,
            this.textBox228,
            this.textBox237,
            this.textBox8,
            this.textBox9,
            this.textBox10,
            this.textBox36,
            this.textBox37,
            this.textBox38,
            this.textBox39,
            this.textBox40});
            this.list3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(0.079999998211860657D));
            this.list3.Name = "list3";
            tableGroup12.Name = "Group2";
            tableGroup14.Name = "Group4";
            tableGroup13.ChildGroups.Add(tableGroup14);
            tableGroup13.Groupings.AddRange(new Telerik.Reporting.Grouping[] {
            new Telerik.Reporting.Grouping(null)});
            tableGroup13.Name = "DetailGroup";
            tableGroup16.Name = "Group14";
            tableGroup15.ChildGroups.Add(tableGroup16);
            tableGroup15.Name = "Group13";
            tableGroup18.Name = "Group12";
            tableGroup17.ChildGroups.Add(tableGroup18);
            tableGroup17.Name = "Group7";
            this.list3.RowGroups.Add(tableGroup12);
            this.list3.RowGroups.Add(tableGroup13);
            this.list3.RowGroups.Add(tableGroup15);
            this.list3.RowGroups.Add(tableGroup17);
            this.list3.RowHeadersPrintOnEveryPage = false;
            this.list3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.952917098999023D), Telerik.Reporting.Drawing.Unit.Cm(3.0099997520446777D));
            this.list3.Style.Font.Name = "微软雅黑";
            // 
            // textBox174
            // 
            this.textBox174.Name = "textBox174";
            this.textBox174.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.79349017143249512D), Telerik.Reporting.Drawing.Unit.Cm(0.60999971628189087D));
            this.textBox174.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox174.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox174.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox174.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox174.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox174.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox174.Style.Font.Name = "微软雅黑";
            this.textBox174.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox174.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox174.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox174.Style.Visible = true;
            this.textBox174.StyleName = "";
            this.textBox174.Value = "序号";
            // 
            // textBox176
            // 
            this.textBox176.Name = "textBox176";
            this.textBox176.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.49421226978302D), Telerik.Reporting.Drawing.Unit.Cm(0.60999971628189087D));
            this.textBox176.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox176.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox176.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox176.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox176.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox176.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox176.Style.Font.Name = "微软雅黑";
            this.textBox176.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox176.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox176.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox176.Style.Visible = true;
            this.textBox176.StyleName = "";
            this.textBox176.Value = "编号";
            // 
            // textBox186
            // 
            this.textBox186.Name = "textBox186";
            this.textBox186.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.79349017143249512D), Telerik.Reporting.Drawing.Unit.Cm(0.79999995231628418D));
            this.textBox186.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox186.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox186.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox186.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox186.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox186.Style.Font.Name = "微软雅黑";
            this.textBox186.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox186.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox186.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox186.StyleName = "";
            this.textBox186.Value = "= Fields.Id";
            // 
            // textBox189
            // 
            this.textBox189.Name = "textBox189";
            this.textBox189.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.49421226978302D), Telerik.Reporting.Drawing.Unit.Cm(0.79999995231628418D));
            this.textBox189.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox189.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox189.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox189.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox189.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox189.Style.Font.Name = "微软雅黑";
            this.textBox189.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox189.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox189.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox189.StyleName = "";
            this.textBox189.Value = "=Fields.Code";
            // 
            // textBox190
            // 
            this.textBox190.Name = "textBox190";
            this.textBox190.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9428073167800903D), Telerik.Reporting.Drawing.Unit.Cm(0.60999971628189087D));
            this.textBox190.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox190.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox190.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox190.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox190.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox190.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox190.Style.Font.Name = "微软雅黑";
            this.textBox190.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox190.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox190.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox190.Style.Visible = true;
            this.textBox190.StyleName = "";
            this.textBox190.Value = "经销商编号";
            // 
            // textBox191
            // 
            this.textBox191.Name = "textBox191";
            this.textBox191.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9428073167800903D), Telerik.Reporting.Drawing.Unit.Cm(0.79999995231628418D));
            this.textBox191.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox191.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox191.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox191.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox191.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox191.Style.Font.Name = "微软雅黑";
            this.textBox191.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox191.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox191.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox191.StyleName = "";
            this.textBox191.Value = "=Fields.CustomerCompanyCode";
            // 
            // textBox192
            // 
            this.textBox192.Name = "textBox192";
            this.textBox192.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9225189685821533D), Telerik.Reporting.Drawing.Unit.Cm(0.60999971628189087D));
            this.textBox192.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox192.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox192.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox192.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox192.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox192.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox192.Style.Font.Name = "微软雅黑";
            this.textBox192.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox192.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox192.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox192.Style.Visible = true;
            this.textBox192.StyleName = "";
            this.textBox192.Value = "经销商名称";
            // 
            // textBox203
            // 
            this.textBox203.Format = "";
            this.textBox203.Name = "textBox203";
            this.textBox203.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9225189685821533D), Telerik.Reporting.Drawing.Unit.Cm(0.800000011920929D));
            this.textBox203.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox203.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox203.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox203.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox203.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox203.Style.Font.Name = "微软雅黑";
            this.textBox203.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox203.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox203.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox203.StyleName = "";
            this.textBox203.Value = "=Fields.CustomerCompanyName";
            // 
            // textBox205
            // 
            this.textBox205.Name = "textBox205";
            this.textBox205.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.6675457954406738D), Telerik.Reporting.Drawing.Unit.Cm(0.60999971628189087D));
            this.textBox205.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox205.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox205.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox205.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox205.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox205.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox205.Style.Font.Name = "微软雅黑";
            this.textBox205.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox205.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox205.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox205.Style.Visible = true;
            this.textBox205.StyleName = "";
            this.textBox205.Value = "资金类型";
            // 
            // textBox208
            // 
            this.textBox208.Format = "";
            this.textBox208.Name = "textBox208";
            this.textBox208.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.6675457954406738D), Telerik.Reporting.Drawing.Unit.Cm(0.79999995231628418D));
            this.textBox208.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox208.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox208.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox208.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox208.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox208.Style.Font.Name = "微软雅黑";
            this.textBox208.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox208.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox208.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox208.StyleName = "";
            this.textBox208.Value = "=Fields.VehicleFundsType.Name";
            // 
            // textBox209
            // 
            this.textBox209.Name = "textBox209";
            this.textBox209.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.5917834043502808D), Telerik.Reporting.Drawing.Unit.Cm(0.60999971628189087D));
            this.textBox209.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox209.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox209.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox209.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox209.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox209.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox209.Style.Font.Name = "微软雅黑";
            this.textBox209.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox209.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox209.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox209.Style.Visible = true;
            this.textBox209.StyleName = "";
            this.textBox209.Value = "金额";
            // 
            // textBox212
            // 
            this.textBox212.Format = "{0:C2}";
            this.textBox212.Name = "textBox212";
            this.textBox212.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.5917834043502808D), Telerik.Reporting.Drawing.Unit.Cm(0.79999995231628418D));
            this.textBox212.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox212.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox212.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox212.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox212.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox212.Style.Font.Name = "微软雅黑";
            this.textBox212.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox212.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox212.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox212.StyleName = "";
            this.textBox212.Value = "=Fields.Amount";
            // 
            // textBox213
            // 
            this.textBox213.Name = "textBox213";
            this.textBox213.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.6982324123382568D), Telerik.Reporting.Drawing.Unit.Cm(0.60999971628189087D));
            this.textBox213.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox213.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox213.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox213.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox213.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox213.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox213.Style.Font.Name = "微软雅黑";
            this.textBox213.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox213.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox213.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox213.Style.Visible = true;
            this.textBox213.StyleName = "";
            this.textBox213.Value = "银行账号";
            // 
            // textBox219
            // 
            this.textBox219.Format = "";
            this.textBox219.Name = "textBox219";
            this.textBox219.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.6982324123382568D), Telerik.Reporting.Drawing.Unit.Cm(0.800000011920929D));
            this.textBox219.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox219.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox219.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox219.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox219.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox219.Style.Font.Name = "微软雅黑";
            this.textBox219.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox219.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox219.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox219.StyleName = "";
            this.textBox219.Value = "=Fields.VehicleBankAccount.BankAccountNumber";
            // 
            // textBox220
            // 
            this.textBox220.Name = "textBox220";
            this.textBox220.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9621996879577637D), Telerik.Reporting.Drawing.Unit.Cm(0.60999971628189087D));
            this.textBox220.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox220.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox220.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox220.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox220.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox220.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox220.Style.Font.Name = "微软雅黑";
            this.textBox220.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox220.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox220.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox220.Style.Visible = true;
            this.textBox220.StyleName = "";
            this.textBox220.Value = "来款凭证编号";
            // 
            // textBox222
            // 
            this.textBox222.Format = "";
            this.textBox222.Name = "textBox222";
            this.textBox222.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9621996879577637D), Telerik.Reporting.Drawing.Unit.Cm(0.800000011920929D));
            this.textBox222.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox222.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox222.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox222.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox222.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox222.Style.Font.Name = "微软雅黑";
            this.textBox222.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox222.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox222.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox222.StyleName = "";
            this.textBox222.Value = "=Fields.PaymentCertificateNumber";
            // 
            // textBox225
            // 
            this.textBox225.Name = "textBox225";
            this.textBox225.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1269793510437012D), Telerik.Reporting.Drawing.Unit.Cm(0.60999971628189087D));
            this.textBox225.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox225.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox225.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox225.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox225.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox225.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox225.Style.Font.Name = "微软雅黑";
            this.textBox225.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox225.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox225.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox225.Style.Visible = true;
            this.textBox225.StyleName = "";
            this.textBox225.Value = "银行名称";
            // 
            // textBox226
            // 
            this.textBox226.Name = "textBox226";
            this.textBox226.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1269793510437012D), Telerik.Reporting.Drawing.Unit.Cm(0.79999995231628418D));
            this.textBox226.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox226.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox226.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox226.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox226.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox226.Style.Font.Name = "微软雅黑";
            this.textBox226.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox226.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox226.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox226.StyleName = "";
            this.textBox226.Value = "=Fields.VehicleBankAccount.BankName";
            // 
            // textBox235
            // 
            this.textBox235.Format = "{0:C2}";
            this.textBox235.Name = "textBox235";
            this.textBox235.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.5917834043502808D), Telerik.Reporting.Drawing.Unit.Cm(0.79999995231628418D));
            this.textBox235.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox235.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox235.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox235.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox235.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox235.Style.Font.Name = "微软雅黑";
            this.textBox235.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox235.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox235.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox235.StyleName = "";
            this.textBox235.Value = "打印时间:";
            // 
            // textBox241
            // 
            this.textBox241.Name = "textBox241";
            this.textBox241.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.2877027988433838D), Telerik.Reporting.Drawing.Unit.Cm(0.79999995231628418D));
            this.textBox241.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox241.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox241.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox241.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox241.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox241.Style.Font.Name = "微软雅黑";
            this.textBox241.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox241.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox241.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox241.StyleName = "";
            this.textBox241.Value = "打印条数:";
            // 
            // textBox2
            // 
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9428073167800903D), Telerik.Reporting.Drawing.Unit.Cm(0.79999995231628418D));
            this.textBox2.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox2.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox2.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox2.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox2.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox2.Style.Font.Name = "微软雅黑";
            this.textBox2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox2.StyleName = "";
            this.textBox2.Value = "= Sunlight.Silverlight.Dcs.Web.Reporting.CommonConverter.TotalRecordsConverterFor" +
    "mat(Count(Fields.id))";
            // 
            // textBox1
            // 
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9225189685821533D), Telerik.Reporting.Drawing.Unit.Cm(0.800000011920929D));
            this.textBox1.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox1.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox1.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox1.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox1.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox1.Style.Font.Name = "微软雅黑";
            this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox1.StyleName = "";
            // 
            // textBox3
            // 
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.6675457954406738D), Telerik.Reporting.Drawing.Unit.Cm(0.79999995231628418D));
            this.textBox3.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox3.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox3.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox3.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox3.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox3.Style.Font.Name = "微软雅黑";
            this.textBox3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox3.StyleName = "";
            // 
            // textBox4
            // 
            this.textBox4.Format = "{0:C2}";
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.5917834043502808D), Telerik.Reporting.Drawing.Unit.Cm(0.79999995231628418D));
            this.textBox4.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox4.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox4.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox4.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox4.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox4.Style.Font.Name = "微软雅黑";
            this.textBox4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox4.StyleName = "";
            this.textBox4.Value = "= Sum(Fields.Amount)";
            // 
            // textBox5
            // 
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.6982324123382568D), Telerik.Reporting.Drawing.Unit.Cm(0.800000011920929D));
            this.textBox5.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox5.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox5.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox5.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox5.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox5.Style.Font.Name = "微软雅黑";
            this.textBox5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox5.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox5.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox5.StyleName = "";
            // 
            // textBox24
            // 
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1269793510437012D), Telerik.Reporting.Drawing.Unit.Cm(0.79999995231628418D));
            this.textBox24.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox24.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox24.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox24.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox24.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox24.Style.Font.Name = "微软雅黑";
            this.textBox24.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox24.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox24.StyleName = "";
            // 
            // textBox6
            // 
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9621996879577637D), Telerik.Reporting.Drawing.Unit.Cm(0.800000011920929D));
            this.textBox6.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox6.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox6.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox6.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox6.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox6.Style.Font.Name = "微软雅黑";
            this.textBox6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox6.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox6.StyleName = "";
            // 
            // textBox7
            // 
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.2877027988433838D), Telerik.Reporting.Drawing.Unit.Cm(0.79999995231628418D));
            this.textBox7.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox7.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox7.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox7.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox7.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox7.Style.Font.Name = "微软雅黑";
            this.textBox7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox7.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox7.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox7.StyleName = "";
            this.textBox7.Value = "总计：";
            // 
            // textBox228
            // 
            this.textBox228.Name = "textBox228";
            this.textBox228.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.5328717231750488D), Telerik.Reporting.Drawing.Unit.Cm(0.79999995231628418D));
            this.textBox228.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox228.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox228.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox228.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox228.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox228.Style.Font.Name = "微软雅黑";
            this.textBox228.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox228.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox228.StyleName = "";
            this.textBox228.Value = "";
            // 
            // textBox237
            // 
            this.textBox237.Name = "textBox237";
            this.textBox237.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.787412166595459D), Telerik.Reporting.Drawing.Unit.Cm(0.79999995231628418D));
            this.textBox237.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox237.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox237.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox237.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox237.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox237.Style.Font.Name = "微软雅黑";
            this.textBox237.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox237.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox237.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox237.StyleName = "";
            this.textBox237.Value = "= Now()";
            // 
            // textBox8
            // 
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.7577887773513794D), Telerik.Reporting.Drawing.Unit.Cm(0.60999971628189087D));
            this.textBox8.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox8.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox8.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox8.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox8.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox8.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox8.Style.Font.Name = "微软雅黑";
            this.textBox8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox8.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox8.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox8.StyleName = "";
            this.textBox8.Value = "来款方式";
            // 
            // textBox9
            // 
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.7577887773513794D), Telerik.Reporting.Drawing.Unit.Cm(0.800000011920929D));
            this.textBox9.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox9.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox9.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox9.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox9.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox9.Style.Font.Name = "微软雅黑";
            this.textBox9.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox9.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox9.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox9.StyleName = "";
            this.textBox9.Value = "= Fields.VehiclePaymentMethod.Name";
            // 
            // textBox10
            // 
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.7577887773513794D), Telerik.Reporting.Drawing.Unit.Cm(0.800000011920929D));
            this.textBox10.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox10.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox10.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox10.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox10.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox10.Style.Font.Name = "微软雅黑";
            this.textBox10.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox10.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox10.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox10.StyleName = "";
            // 
            // textBox36
            // 
            this.textBox36.Name = "textBox36";
            this.textBox36.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.7577887773513794D), Telerik.Reporting.Drawing.Unit.Cm(0.800000011920929D));
            this.textBox36.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox36.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox36.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox36.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox36.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox36.Style.Font.Name = "微软雅黑";
            this.textBox36.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox36.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox36.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox36.StyleName = "";
            // 
            // textBox37
            // 
            this.textBox37.Name = "textBox37";
            this.textBox37.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9953620433807373D), Telerik.Reporting.Drawing.Unit.Cm(0.60999971628189087D));
            this.textBox37.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox37.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox37.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox37.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox37.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox37.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox37.Style.Font.Name = "微软雅黑";
            this.textBox37.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox37.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox37.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox37.StyleName = "";
            this.textBox37.Value = "来款时间";
            // 
            // textBox38
            // 
            this.textBox38.Format = "{0:d}";
            this.textBox38.Name = "textBox38";
            this.textBox38.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9953620433807373D), Telerik.Reporting.Drawing.Unit.Cm(0.800000011920929D));
            this.textBox38.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox38.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox38.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox38.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox38.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox38.Style.Font.Name = "微软雅黑";
            this.textBox38.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox38.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox38.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox38.StyleName = "";
            this.textBox38.Value = "= Fields.TimeOfIncomingPayment";
            // 
            // textBox39
            // 
            this.textBox39.Name = "textBox39";
            this.textBox39.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9953620433807373D), Telerik.Reporting.Drawing.Unit.Cm(0.800000011920929D));
            this.textBox39.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox39.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox39.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox39.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox39.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox39.Style.Font.Name = "微软雅黑";
            this.textBox39.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox39.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox39.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox39.StyleName = "";
            // 
            // textBox40
            // 
            this.textBox40.Name = "textBox40";
            this.textBox40.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9953620433807373D), Telerik.Reporting.Drawing.Unit.Cm(0.800000011920929D));
            this.textBox40.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox40.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox40.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox40.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox40.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox40.Style.Font.Name = "微软雅黑";
            this.textBox40.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox40.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox40.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox40.StyleName = "";
            // 
            // DataSourceVehiclePaymentBill
            // 
            this.DataSourceVehiclePaymentBill.DataMember = "GetReportVehiclePaymentBillWithDetails";
            this.DataSourceVehiclePaymentBill.DataSource = typeof(Sunlight.Silverlight.Dcs.Web.DcsDomainService);
            this.DataSourceVehiclePaymentBill.Name = "DataSourceVehiclePaymentBill";
            this.DataSourceVehiclePaymentBill.Parameters.AddRange(new Telerik.Reporting.ObjectDataSourceParameter[] {
            new Telerik.Reporting.ObjectDataSourceParameter("customerCompanyName", typeof(string), "=Parameters.paracustomerCompanyName.Value"),
            new Telerik.Reporting.ObjectDataSourceParameter("vehiclePaymentMethodId", typeof(System.Nullable<int>), "=Parameters.paravehiclePaymentMethodId.Value"),
            new Telerik.Reporting.ObjectDataSourceParameter("vehicleFundsTypeId", typeof(System.Nullable<int>), "=Parameters.paravehicleFundsTypeId.Value"),
            new Telerik.Reporting.ObjectDataSourceParameter("status", typeof(System.Nullable<int>), "=Parameters.parastatus.Value"),
            new Telerik.Reporting.ObjectDataSourceParameter("statDateTime", typeof(System.Nullable<System.DateTime>), "=Parameters.parastartDateTime.Value"),
            new Telerik.Reporting.ObjectDataSourceParameter("endDateTime", typeof(System.Nullable<System.DateTime>), "=Parameters.paraendDateTime.Value"),
            new Telerik.Reporting.ObjectDataSourceParameter("statTimeOfIncomingPayment", typeof(System.Nullable<System.DateTime>), "=Parameters.parastatTimeOfIncomingPayment.Value"),
            new Telerik.Reporting.ObjectDataSourceParameter("endTimeOfIncomingPayment", typeof(System.Nullable<System.DateTime>), "=Parameters.paraendTimeOfIncomingPayment.Value")});
            // 
            // pageFooterSection1
            // 
            this.pageFooterSection1.Height = Telerik.Reporting.Drawing.Unit.Cm(1.6599996089935303D);
            this.pageFooterSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox23});
            this.pageFooterSection1.Name = "pageFooterSection1";
            this.pageFooterSection1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            // 
            // textBox23
            // 
            this.textBox23.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(6.7000002861022949D), Telerik.Reporting.Drawing.Unit.Cm(0.40000021457672119D));
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.0264649391174316D), Telerik.Reporting.Drawing.Unit.Cm(0.84125012159347534D));
            this.textBox23.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox23.StyleName = "";
            this.textBox23.Value = "= Sunlight.Silverlight.Dcs.Web.Reporting.CommonConverter.CurrentPagesConverterFor" +
    "mat(PageNumber)";
            // 
            // ReportVehiclePaymentBill
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pageHeaderSection1,
            this.detail,
            this.pageFooterSection1});
            this.Name = "ReportVehiclePaymentBills";
            this.PageSettings.Margins.Bottom = Telerik.Reporting.Drawing.Unit.Mm(9D);
            this.PageSettings.Margins.Left = Telerik.Reporting.Drawing.Unit.Pixel(19D);
            this.PageSettings.Margins.Right = Telerik.Reporting.Drawing.Unit.Pixel(19D);
            this.PageSettings.Margins.Top = Telerik.Reporting.Drawing.Unit.Mm(9D);
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            reportParameter1.AllowNull = true;
            reportParameter1.Name = "paracustomerCompanyName";
            reportParameter2.AllowNull = true;
            reportParameter2.Name = "paravehiclePaymentMethodId";
            reportParameter3.AllowNull = true;
            reportParameter3.Name = "paravehicleFundsTypeId";
            reportParameter4.AllowNull = true;
            reportParameter4.Name = "parastatus";
            reportParameter5.AllowNull = true;
            reportParameter5.Name = "parastartDateTime";
            reportParameter6.AllowNull = true;
            reportParameter6.Name = "paraendDateTime";
            reportParameter7.AllowNull = true;
            reportParameter7.Name = "parastatTimeOfIncomingPayment";
            reportParameter8.AllowNull = true;
            reportParameter8.Name = "paraendTimeOfIncomingPayment";
            this.ReportParameters.Add(reportParameter1);
            this.ReportParameters.Add(reportParameter2);
            this.ReportParameters.Add(reportParameter3);
            this.ReportParameters.Add(reportParameter4);
            this.ReportParameters.Add(reportParameter5);
            this.ReportParameters.Add(reportParameter6);
            this.ReportParameters.Add(reportParameter7);
            this.ReportParameters.Add(reportParameter8);
            this.Style.BackgroundColor = System.Drawing.Color.White;
            this.Width = Telerik.Reporting.Drawing.Unit.Cm(20D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.PageHeaderSection pageHeaderSection1;
        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.TextBox textBox19;
        private Telerik.Reporting.PageFooterSection pageFooterSection1;
        private Telerik.Reporting.TextBox textBox23;
        private Telerik.Reporting.List list3;
        private Telerik.Reporting.TextBox textBox174;
        private Telerik.Reporting.TextBox textBox176;
        private Telerik.Reporting.TextBox textBox186;
        private Telerik.Reporting.TextBox textBox189;
        private Telerik.Reporting.TextBox textBox190;
        private Telerik.Reporting.TextBox textBox191;
        private Telerik.Reporting.TextBox textBox192;
        private Telerik.Reporting.TextBox textBox203;
        private Telerik.Reporting.TextBox textBox205;
        private Telerik.Reporting.TextBox textBox208;
        private Telerik.Reporting.TextBox textBox209;
        private Telerik.Reporting.TextBox textBox212;
        private Telerik.Reporting.TextBox textBox213;
        private Telerik.Reporting.TextBox textBox219;
        private Telerik.Reporting.TextBox textBox220;
        private Telerik.Reporting.TextBox textBox222;
        private Telerik.Reporting.TextBox textBox225;
        private Telerik.Reporting.TextBox textBox226;
        private Telerik.Reporting.TextBox textBox235;
        private Telerik.Reporting.TextBox textBox241;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.TextBox textBox24;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.TextBox textBox228;
        private Telerik.Reporting.TextBox textBox237;
        private Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.TextBox textBox9;
        private Telerik.Reporting.TextBox textBox10;
        private Telerik.Reporting.TextBox textBox36;
        private Telerik.Reporting.TextBox textBox37;
        private Telerik.Reporting.TextBox textBox38;
        private Telerik.Reporting.TextBox textBox39;
        private Telerik.Reporting.TextBox textBox40;
        private Telerik.Reporting.ObjectDataSource DataSourceVehiclePaymentBill;
    }
}