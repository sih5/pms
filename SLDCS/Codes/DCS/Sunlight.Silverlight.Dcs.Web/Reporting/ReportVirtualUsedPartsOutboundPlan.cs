namespace Sunlight.Silverlight.Dcs.Web.Reporting {
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using Telerik.Reporting;
    using Telerik.Reporting.Drawing;

    /// <summary>
    /// Summary description for ReportVirtualUsedPartsOutboundPlan.
    /// </summary>
    public partial class ReportVirtualUsedPartsOutboundPlan : Telerik.Reporting.Report {
        public ReportVirtualUsedPartsOutboundPlan() {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }
    }
}