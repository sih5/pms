namespace Sunlight.Silverlight.Dcs.Web.Reporting {
    using System;

    /// <summary>
    /// Summary description for ReportWarehouseAreaLable.
    /// </summary>
    public partial class ReportWarehouseAreaLable : Telerik.Reporting.Report {
        public ReportWarehouseAreaLable() {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }

        private void ReportWarehouseAreaLable_NeedDataSource(object sender, EventArgs e) {
            var service = new DcsDomainService();
            var report = (Telerik.Reporting.Processing.Report)sender;
            string parameter = report.Parameters["paraWarehouseAreaId"].Value.ToString().Trim();
            var aa = parameter.Split(',');
            int[] a = new int[aa.Length];
            for(int i = 0; i < aa.Length; i++) {
                a[i] = int.Parse(aa[i]);
            }
            var data = service.GetWarehouseAreaWithLablePrint(a);
            report.DataSource = data;
        }
    }
}