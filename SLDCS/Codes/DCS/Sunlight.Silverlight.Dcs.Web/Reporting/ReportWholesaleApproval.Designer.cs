namespace Sunlight.Silverlight.Dcs.Web.Reporting {
    partial class ReportWholesaleApproval {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup11 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup12 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup13 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup14 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup15 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup16 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup17 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup18 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup19 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup20 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup21 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup22 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup23 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup24 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup25 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup26 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup27 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup28 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup29 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup30 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup31 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup32 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup33 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup34 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup35 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup36 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup37 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup38 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup39 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup40 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup41 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup42 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup43 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup44 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup45 = new Telerik.Reporting.TableGroup();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReportWholesaleApproval));
            Telerik.Reporting.TableGroup tableGroup46 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup47 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup48 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup49 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup50 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup51 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup52 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup53 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup54 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup55 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup56 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup57 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup58 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup59 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup60 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup61 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup62 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup63 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup64 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup65 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup66 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup67 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup68 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup69 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup70 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup71 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup72 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup73 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup74 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup75 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup76 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup77 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup78 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup79 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup80 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup81 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup82 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup83 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup84 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup85 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup86 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup87 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup88 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.ReportParameter reportParameter1 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter2 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            this.textBox107 = new Telerik.Reporting.TextBox();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.textBox7 = new Telerik.Reporting.TextBox();
            this.textBox42 = new Telerik.Reporting.TextBox();
            this.textBox125 = new Telerik.Reporting.TextBox();
            this.textBox136 = new Telerik.Reporting.TextBox();
            this.textBox127 = new Telerik.Reporting.TextBox();
            this.textBox126 = new Telerik.Reporting.TextBox();
            this.textBox130 = new Telerik.Reporting.TextBox();
            this.textBox128 = new Telerik.Reporting.TextBox();
            this.textBox132 = new Telerik.Reporting.TextBox();
            this.textBox129 = new Telerik.Reporting.TextBox();
            this.textBox134 = new Telerik.Reporting.TextBox();
            this.pageHeaderSection1 = new Telerik.Reporting.PageHeaderSection();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.detail = new Telerik.Reporting.DetailSection();
            this.table1 = new Telerik.Reporting.Table();
            this.textBox33 = new Telerik.Reporting.TextBox();
            this.textBox108 = new Telerik.Reporting.TextBox();
            this.textBox109 = new Telerik.Reporting.TextBox();
            this.textBox110 = new Telerik.Reporting.TextBox();
            this.textBox8 = new Telerik.Reporting.TextBox();
            this.textBox10 = new Telerik.Reporting.TextBox();
            this.textBox12 = new Telerik.Reporting.TextBox();
            this.textBox14 = new Telerik.Reporting.TextBox();
            this.textBox16 = new Telerik.Reporting.TextBox();
            this.textBox28 = new Telerik.Reporting.TextBox();
            this.textBox39 = new Telerik.Reporting.TextBox();
            this.textBox40 = new Telerik.Reporting.TextBox();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.textBox17 = new Telerik.Reporting.TextBox();
            this.textBox29 = new Telerik.Reporting.TextBox();
            this.textBox41 = new Telerik.Reporting.TextBox();
            this.textBox48 = new Telerik.Reporting.TextBox();
            this.textBox21 = new Telerik.Reporting.TextBox();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.textBox9 = new Telerik.Reporting.TextBox();
            this.textBox11 = new Telerik.Reporting.TextBox();
            this.textBox13 = new Telerik.Reporting.TextBox();
            this.textBox30 = new Telerik.Reporting.TextBox();
            this.textBox23 = new Telerik.Reporting.TextBox();
            this.textBox24 = new Telerik.Reporting.TextBox();
            this.textBox26 = new Telerik.Reporting.TextBox();
            this.textBox55 = new Telerik.Reporting.TextBox();
            this.textBox56 = new Telerik.Reporting.TextBox();
            this.textBox59 = new Telerik.Reporting.TextBox();
            this.textBox65 = new Telerik.Reporting.TextBox();
            this.textBox69 = new Telerik.Reporting.TextBox();
            this.textBox70 = new Telerik.Reporting.TextBox();
            this.textBox74 = new Telerik.Reporting.TextBox();
            this.textBox83 = new Telerik.Reporting.TextBox();
            this.textBox84 = new Telerik.Reporting.TextBox();
            this.textBox62 = new Telerik.Reporting.TextBox();
            this.textBox63 = new Telerik.Reporting.TextBox();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.textBox49 = new Telerik.Reporting.TextBox();
            this.textBox20 = new Telerik.Reporting.TextBox();
            this.textBox15 = new Telerik.Reporting.TextBox();
            this.textBox25 = new Telerik.Reporting.TextBox();
            this.textBox18 = new Telerik.Reporting.TextBox();
            this.textBox19 = new Telerik.Reporting.TextBox();
            this.textBox22 = new Telerik.Reporting.TextBox();
            this.textBox27 = new Telerik.Reporting.TextBox();
            this.textBox37 = new Telerik.Reporting.TextBox();
            this.textBox43 = new Telerik.Reporting.TextBox();
            this.textBox50 = new Telerik.Reporting.TextBox();
            this.DataSourceWholesaleApproval = new Telerik.Reporting.ObjectDataSource();
            this.table3 = new Telerik.Reporting.Table();
            this.textBox99 = new Telerik.Reporting.TextBox();
            this.textBox93 = new Telerik.Reporting.TextBox();
            this.textBox66 = new Telerik.Reporting.TextBox();
            this.textBox77 = new Telerik.Reporting.TextBox();
            this.textBox79 = new Telerik.Reporting.TextBox();
            this.textBox76 = new Telerik.Reporting.TextBox();
            this.textBox91 = new Telerik.Reporting.TextBox();
            this.textBox68 = new Telerik.Reporting.TextBox();
            this.textBox90 = new Telerik.Reporting.TextBox();
            this.textBox94 = new Telerik.Reporting.TextBox();
            this.textBox98 = new Telerik.Reporting.TextBox();
            this.textBox103 = new Telerik.Reporting.TextBox();
            this.textBox116 = new Telerik.Reporting.TextBox();
            this.textBox119 = new Telerik.Reporting.TextBox();
            this.textBox100 = new Telerik.Reporting.TextBox();
            this.textBox87 = new Telerik.Reporting.TextBox();
            this.DataSourceWholesaleApprovalPost = new Telerik.Reporting.ObjectDataSource();
            this.table2 = new Telerik.Reporting.Table();
            this.textBox67 = new Telerik.Reporting.TextBox();
            this.textBox71 = new Telerik.Reporting.TextBox();
            this.textBox113 = new Telerik.Reporting.TextBox();
            this.textBox89 = new Telerik.Reporting.TextBox();
            this.textBox133 = new Telerik.Reporting.TextBox();
            this.list3 = new Telerik.Reporting.List();
            this.textBox186 = new Telerik.Reporting.TextBox();
            this.textBox189 = new Telerik.Reporting.TextBox();
            this.textBox191 = new Telerik.Reporting.TextBox();
            this.textBox203 = new Telerik.Reporting.TextBox();
            this.textBox208 = new Telerik.Reporting.TextBox();
            this.textBox212 = new Telerik.Reporting.TextBox();
            this.textBox219 = new Telerik.Reporting.TextBox();
            this.textBox220 = new Telerik.Reporting.TextBox();
            this.textBox222 = new Telerik.Reporting.TextBox();
            this.textBox226 = new Telerik.Reporting.TextBox();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.textBox31 = new Telerik.Reporting.TextBox();
            this.textBox32 = new Telerik.Reporting.TextBox();
            this.textBox34 = new Telerik.Reporting.TextBox();
            this.textBox35 = new Telerik.Reporting.TextBox();
            this.textBox36 = new Telerik.Reporting.TextBox();
            this.textBox38 = new Telerik.Reporting.TextBox();
            this.textBox44 = new Telerik.Reporting.TextBox();
            this.textBox45 = new Telerik.Reporting.TextBox();
            this.textBox46 = new Telerik.Reporting.TextBox();
            this.textBox47 = new Telerik.Reporting.TextBox();
            this.textBox52 = new Telerik.Reporting.TextBox();
            this.textBox53 = new Telerik.Reporting.TextBox();
            this.textBox54 = new Telerik.Reporting.TextBox();
            this.textBox58 = new Telerik.Reporting.TextBox();
            this.textBox60 = new Telerik.Reporting.TextBox();
            this.textBox190 = new Telerik.Reporting.TextBox();
            this.textBox192 = new Telerik.Reporting.TextBox();
            this.textBox61 = new Telerik.Reporting.TextBox();
            this.textBox209 = new Telerik.Reporting.TextBox();
            this.textBox213 = new Telerik.Reporting.TextBox();
            this.textBox64 = new Telerik.Reporting.TextBox();
            this.textBox72 = new Telerik.Reporting.TextBox();
            this.pageFooterSection1 = new Telerik.Reporting.PageFooterSection();
            this.textBox73 = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // textBox107
            // 
            this.textBox107.Name = "textBox107";
            this.textBox107.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9841976165771484D), Telerik.Reporting.Drawing.Unit.Cm(0.79985183477401733D));
            this.textBox107.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox107.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox107.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox107.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox107.StyleName = "";
            this.textBox107.Value = "经销商名称";
            // 
            // textBox3
            // 
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.1999673843383789D), Telerik.Reporting.Drawing.Unit.Cm(0.79985183477401733D));
            this.textBox3.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox3.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox3.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(3D);
            this.textBox3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox3.Value = "= Fields.DealerName";
            // 
            // textBox7
            // 
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.0422234535217285D), Telerik.Reporting.Drawing.Unit.Cm(0.79985183477401733D));
            this.textBox7.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox7.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox7.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(3D);
            this.textBox7.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox7.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox7.StyleName = "";
            this.textBox7.Value = "经销商代码 ";
            // 
            // textBox42
            // 
            this.textBox42.Name = "textBox42";
            this.textBox42.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(10.689489364624023D), Telerik.Reporting.Drawing.Unit.Cm(0.79985183477401733D));
            this.textBox42.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox42.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox42.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(3D);
            this.textBox42.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox42.StyleName = "";
            this.textBox42.Value = "= Fields.DealerCode";
            // 
            // textBox125
            // 
            this.textBox125.Name = "textBox125";
            this.textBox125.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.8489222526550293D), Telerik.Reporting.Drawing.Unit.Cm(0.80000025033950806D));
            this.textBox125.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox125.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox125.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox125.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox125.StyleName = "";
            this.textBox125.Value = "员工购车可提交文件";
            // 
            // textBox136
            // 
            this.textBox136.Name = "textBox136";
            this.textBox136.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.85404068231582642D), Telerik.Reporting.Drawing.Unit.Cm(0.80000025033950806D));
            this.textBox136.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox136.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox136.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox136.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox136.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox136.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(20D);
            this.textBox136.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox136.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox136.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox136.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox136.StyleName = "";
            this.textBox136.Value = "□";
            // 
            // textBox127
            // 
            this.textBox127.Name = "textBox127";
            this.textBox127.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.8730783462524414D), Telerik.Reporting.Drawing.Unit.Cm(0.80000025033950806D));
            this.textBox127.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox127.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox127.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(3D);
            this.textBox127.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox127.StyleName = "";
            this.textBox127.Value = "社保证明";
            // 
            // textBox126
            // 
            this.textBox126.Name = "textBox126";
            this.textBox126.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.69168722629547119D), Telerik.Reporting.Drawing.Unit.Cm(0.80000025033950806D));
            this.textBox126.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox126.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox126.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox126.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox126.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox126.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(20D);
            this.textBox126.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox126.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox126.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox126.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox126.StyleName = "";
            this.textBox126.Value = "□";
            // 
            // textBox130
            // 
            this.textBox130.Name = "textBox130";
            this.textBox130.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.1262335777282715D), Telerik.Reporting.Drawing.Unit.Cm(0.80000025033950806D));
            this.textBox130.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox130.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox130.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(3D);
            this.textBox130.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox130.StyleName = "";
            this.textBox130.Value = "公积金证明";
            // 
            // textBox128
            // 
            this.textBox128.Name = "textBox128";
            this.textBox128.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.74794673919677734D), Telerik.Reporting.Drawing.Unit.Cm(0.80000025033950806D));
            this.textBox128.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox128.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox128.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox128.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox128.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox128.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(20D);
            this.textBox128.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox128.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox128.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox128.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox128.StyleName = "";
            this.textBox128.Value = "□";
            // 
            // textBox132
            // 
            this.textBox132.Name = "textBox132";
            this.textBox132.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.1824936866760254D), Telerik.Reporting.Drawing.Unit.Cm(0.80000025033950806D));
            this.textBox132.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox132.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox132.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox132.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox132.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox132.StyleName = "";
            this.textBox132.Value = "完税证明";
            // 
            // textBox129
            // 
            this.textBox129.Name = "textBox129";
            this.textBox129.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.4230209589004517D), Telerik.Reporting.Drawing.Unit.Cm(0.80000025033950806D));
            this.textBox129.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox129.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox129.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox129.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox129.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox129.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(20D);
            this.textBox129.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox129.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox129.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox129.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox129.StyleName = "";
            this.textBox129.Value = "□";
            // 
            // textBox134
            // 
            this.textBox134.Name = "textBox134";
            this.textBox134.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.1525764465332031D), Telerik.Reporting.Drawing.Unit.Cm(0.80000025033950806D));
            this.textBox134.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox134.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox134.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(3D);
            this.textBox134.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox134.StyleName = "";
            this.textBox134.Value = "其它";
            // 
            // pageHeaderSection1
            // 
            this.pageHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Cm(2D);
            this.pageHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox1});
            this.pageHeaderSection1.Name = "pageHeaderSection1";
            // 
            // textBox1
            // 
            this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(5.953125D), Telerik.Reporting.Drawing.Unit.Cm(0.44979164004325867D));
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.0079164505004883D), Telerik.Reporting.Drawing.Unit.Cm(1.0912585258483887D));
            this.textBox1.Style.Font.Bold = false;
            this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(20D);
            this.textBox1.Value = "长安马自达批售审批单  ";
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Cm(21.5D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.table1,
            this.table3,
            this.table2,
            this.list3});
            this.detail.Name = "detail";
            // 
            // table1
            // 
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.26320517063140869D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.7209925651550293D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.9999997615814209D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.1999677419662476D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.0422234535217285D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.0420440435409546D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.06854510307312D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.3000005483627319D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.9999998807907105D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.7999999523162842D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.1210073232650757D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.357892632484436D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.79985147714614868D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.79985147714614868D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.79985147714614868D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.79985147714614868D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.79985147714614868D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.93211877346038818D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(1.0643857717514038D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(1.0643863677978516D)));
            this.table1.Body.SetCellContent(1, 4, this.textBox33);
            this.table1.Body.SetCellContent(0, 0, this.textBox108, 1, 2);
            this.table1.Body.SetCellContent(1, 0, this.textBox109, 1, 2);
            this.table1.Body.SetCellContent(2, 0, this.textBox110, 1, 2);
            this.table1.Body.SetCellContent(3, 2, this.textBox8);
            this.table1.Body.SetCellContent(3, 3, this.textBox10);
            this.table1.Body.SetCellContent(3, 4, this.textBox12);
            this.table1.Body.SetCellContent(3, 8, this.textBox14);
            this.table1.Body.SetCellContent(3, 9, this.textBox16);
            this.table1.Body.SetCellContent(4, 9, this.textBox28);
            this.table1.Body.SetCellContent(5, 8, this.textBox39);
            this.table1.Body.SetCellContent(5, 9, this.textBox40);
            this.table1.Body.SetCellContent(3, 0, this.textBox2, 1, 2);
            this.table1.Body.SetCellContent(4, 0, this.textBox17, 1, 2);
            this.table1.Body.SetCellContent(5, 0, this.textBox29, 1, 2);
            this.table1.Body.SetCellContent(6, 0, this.textBox41, 1, 2);
            this.table1.Body.SetCellContent(7, 0, this.textBox48, 1, 2);
            this.table1.Body.SetCellContent(2, 2, this.textBox21);
            this.table1.Body.SetCellContent(2, 3, this.textBox5);
            this.table1.Body.SetCellContent(2, 4, this.textBox9);
            this.table1.Body.SetCellContent(2, 8, this.textBox11);
            this.table1.Body.SetCellContent(2, 9, this.textBox13);
            this.table1.Body.SetCellContent(1, 2, this.textBox30, 1, 2);
            this.table1.Body.SetCellContent(2, 6, this.textBox23);
            this.table1.Body.SetCellContent(3, 6, this.textBox24);
            this.table1.Body.SetCellContent(5, 6, this.textBox26);
            this.table1.Body.SetCellContent(2, 5, this.textBox55);
            this.table1.Body.SetCellContent(3, 5, this.textBox56);
            this.table1.Body.SetCellContent(5, 5, this.textBox59);
            this.table1.Body.SetCellContent(5, 10, this.textBox65);
            this.table1.Body.SetCellContent(2, 7, this.textBox69);
            this.table1.Body.SetCellContent(3, 7, this.textBox70);
            this.table1.Body.SetCellContent(5, 7, this.textBox74);
            this.table1.Body.SetCellContent(4, 11, this.textBox83);
            this.table1.Body.SetCellContent(5, 11, this.textBox84);
            this.table1.Body.SetCellContent(2, 10, this.textBox62, 1, 2);
            this.table1.Body.SetCellContent(3, 10, this.textBox63, 1, 2);
            this.table1.Body.SetCellContent(0, 2, this.textBox6, 1, 10);
            this.table1.Body.SetCellContent(1, 5, this.textBox49, 1, 7);
            this.table1.Body.SetCellContent(4, 3, this.textBox20, 1, 2);
            this.table1.Body.SetCellContent(4, 5, this.textBox15);
            this.table1.Body.SetCellContent(4, 6, this.textBox25, 1, 2);
            this.table1.Body.SetCellContent(4, 8, this.textBox18);
            this.table1.Body.SetCellContent(4, 10, this.textBox19);
            this.table1.Body.SetCellContent(4, 2, this.textBox22);
            this.table1.Body.SetCellContent(5, 2, this.textBox27);
            this.table1.Body.SetCellContent(5, 3, this.textBox37, 1, 2);
            this.table1.Body.SetCellContent(6, 2, this.textBox43, 1, 10);
            this.table1.Body.SetCellContent(7, 2, this.textBox50, 1, 10);
            tableGroup2.Name = "Group12";
            tableGroup1.ChildGroups.Add(tableGroup2);
            tableGroup1.ChildGroups.Add(tableGroup3);
            tableGroup1.Name = "Group5";
            tableGroup1.ReportItem = this.textBox107;
            tableGroup5.Name = "Group6";
            tableGroup6.Name = "Group1";
            tableGroup4.ChildGroups.Add(tableGroup5);
            tableGroup4.ChildGroups.Add(tableGroup6);
            tableGroup4.ReportItem = this.textBox3;
            tableGroup8.Name = "Group2";
            tableGroup7.ChildGroups.Add(tableGroup8);
            tableGroup7.Name = "Group11";
            tableGroup7.ReportItem = this.textBox7;
            tableGroup10.Name = "Group21";
            tableGroup11.Name = "Group19";
            tableGroup12.Name = "Group22";
            tableGroup13.Name = "Group3";
            tableGroup14.Name = "Group4";
            tableGroup15.Name = "Group18";
            tableGroup16.Name = "Group24";
            tableGroup9.ChildGroups.Add(tableGroup10);
            tableGroup9.ChildGroups.Add(tableGroup11);
            tableGroup9.ChildGroups.Add(tableGroup12);
            tableGroup9.ChildGroups.Add(tableGroup13);
            tableGroup9.ChildGroups.Add(tableGroup14);
            tableGroup9.ChildGroups.Add(tableGroup15);
            tableGroup9.ChildGroups.Add(tableGroup16);
            tableGroup9.Name = "Group20";
            tableGroup9.ReportItem = this.textBox42;
            this.table1.ColumnGroups.Add(tableGroup1);
            this.table1.ColumnGroups.Add(tableGroup4);
            this.table1.ColumnGroups.Add(tableGroup7);
            this.table1.ColumnGroups.Add(tableGroup9);
            this.table1.DataSource = this.DataSourceWholesaleApproval;
            this.table1.Docking = Telerik.Reporting.DockingStyle.Top;
            this.table1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox33,
            this.textBox108,
            this.textBox109,
            this.textBox110,
            this.textBox8,
            this.textBox10,
            this.textBox12,
            this.textBox14,
            this.textBox16,
            this.textBox28,
            this.textBox39,
            this.textBox40,
            this.textBox2,
            this.textBox17,
            this.textBox29,
            this.textBox41,
            this.textBox48,
            this.textBox21,
            this.textBox5,
            this.textBox9,
            this.textBox11,
            this.textBox13,
            this.textBox30,
            this.textBox23,
            this.textBox24,
            this.textBox26,
            this.textBox55,
            this.textBox56,
            this.textBox59,
            this.textBox65,
            this.textBox69,
            this.textBox70,
            this.textBox74,
            this.textBox83,
            this.textBox84,
            this.textBox62,
            this.textBox63,
            this.textBox6,
            this.textBox49,
            this.textBox20,
            this.textBox15,
            this.textBox25,
            this.textBox18,
            this.textBox19,
            this.textBox22,
            this.textBox27,
            this.textBox37,
            this.textBox43,
            this.textBox50,
            this.textBox107,
            this.textBox3,
            this.textBox7,
            this.textBox42});
            this.table1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.table1.Name = "table1";
            tableGroup18.Name = "Group7";
            tableGroup19.Name = "Group9";
            tableGroup20.Name = "Group8";
            tableGroup21.Name = "Group13";
            tableGroup22.Name = "Group14";
            tableGroup23.Name = "Group15";
            tableGroup24.Name = "Group16";
            tableGroup25.Name = "Group17";
            tableGroup17.ChildGroups.Add(tableGroup18);
            tableGroup17.ChildGroups.Add(tableGroup19);
            tableGroup17.ChildGroups.Add(tableGroup20);
            tableGroup17.ChildGroups.Add(tableGroup21);
            tableGroup17.ChildGroups.Add(tableGroup22);
            tableGroup17.ChildGroups.Add(tableGroup23);
            tableGroup17.ChildGroups.Add(tableGroup24);
            tableGroup17.ChildGroups.Add(tableGroup25);
            tableGroup17.Groupings.AddRange(new Telerik.Reporting.Grouping[] {
            new Telerik.Reporting.Grouping(null)});
            tableGroup17.Name = "DetailGroup";
            this.table1.RowGroups.Add(tableGroup17);
            this.table1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.915878295898438D), Telerik.Reporting.Drawing.Unit.Cm(7.8600001335144043D));
            this.table1.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table1.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table1.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table1.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table1.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.table1.Style.Font.Name = "微软雅黑";
            this.table1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(11D);
            // 
            // textBox33
            // 
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.0422234535217285D), Telerik.Reporting.Drawing.Unit.Cm(0.79985147714614868D));
            this.textBox33.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox33.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox33.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(3D);
            this.textBox33.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox33.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox33.StyleName = "";
            this.textBox33.Value = "邮编";
            // 
            // textBox108
            // 
            this.textBox108.Name = "textBox108";
            this.textBox108.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9841976165771484D), Telerik.Reporting.Drawing.Unit.Cm(0.79985147714614868D));
            this.textBox108.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox108.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox108.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox108.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox108.StyleName = "";
            this.textBox108.Value = "客户名称";
            // 
            // textBox109
            // 
            this.textBox109.Name = "textBox109";
            this.textBox109.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9841976165771484D), Telerik.Reporting.Drawing.Unit.Cm(0.79985147714614868D));
            this.textBox109.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox109.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox109.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox109.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox109.StyleName = "";
            this.textBox109.Value = "地址";
            // 
            // textBox110
            // 
            this.textBox110.Name = "textBox110";
            this.textBox110.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9841976165771484D), Telerik.Reporting.Drawing.Unit.Cm(0.79985147714614868D));
            this.textBox110.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox110.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox110.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox110.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox110.StyleName = "";
            this.textBox110.Value = "客户联系人";
            // 
            // textBox8
            // 
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.9999997615814209D), Telerik.Reporting.Drawing.Unit.Cm(0.79985147714614868D));
            this.textBox8.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox8.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox8.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(3D);
            this.textBox8.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox8.StyleName = "";
            this.textBox8.Value = "= Fields.KeyAccount.CompanyName";
            // 
            // textBox10
            // 
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.1999677419662476D), Telerik.Reporting.Drawing.Unit.Cm(0.79985147714614868D));
            this.textBox10.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox10.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox10.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(3D);
            this.textBox10.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox10.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox10.StyleName = "";
            this.textBox10.Value = "联系人";
            // 
            // textBox12
            // 
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.0422234535217285D), Telerik.Reporting.Drawing.Unit.Cm(0.79985147714614868D));
            this.textBox12.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox12.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox12.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(3D);
            this.textBox12.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox12.StyleName = "";
            this.textBox12.Value = "= Fields.KeyAccount.ContactPerson";
            // 
            // textBox14
            // 
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9999998807907105D), Telerik.Reporting.Drawing.Unit.Cm(0.79985147714614868D));
            this.textBox14.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox14.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox14.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox14.StyleName = "";
            this.textBox14.Value = "= Fields.KeyAccount.ContactPhone";
            // 
            // textBox16
            // 
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.7999999523162842D), Telerik.Reporting.Drawing.Unit.Cm(0.79985147714614868D));
            this.textBox16.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox16.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox16.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox16.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox16.StyleName = "";
            this.textBox16.Value = "传真";
            // 
            // textBox28
            // 
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.7999999523162842D), Telerik.Reporting.Drawing.Unit.Cm(0.79985147714614868D));
            this.textBox28.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox28.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox28.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox28.StyleName = "";
            this.textBox28.Value = "租凭";
            // 
            // textBox39
            // 
            this.textBox39.Name = "textBox39";
            this.textBox39.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9999998807907105D), Telerik.Reporting.Drawing.Unit.Cm(0.93211877346038818D));
            this.textBox39.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox39.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox39.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox39.StyleName = "";
            // 
            // textBox40
            // 
            this.textBox40.Name = "textBox40";
            this.textBox40.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.7999999523162842D), Telerik.Reporting.Drawing.Unit.Cm(0.93211877346038818D));
            this.textBox40.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox40.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox40.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox40.StyleName = "";
            this.textBox40.Value = "代交车\r\n经销商联系人";
            // 
            // textBox2
            // 
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9841976165771484D), Telerik.Reporting.Drawing.Unit.Cm(0.79985147714614868D));
            this.textBox2.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox2.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox2.StyleName = "";
            this.textBox2.Value = "招标单位/\r\n集中采购单位";
            // 
            // textBox17
            // 
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9841976165771484D), Telerik.Reporting.Drawing.Unit.Cm(0.79985147714614868D));
            this.textBox17.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox17.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox17.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox17.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox17.StyleName = "";
            this.textBox17.Value = "客户性质";
            // 
            // textBox29
            // 
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9841976165771484D), Telerik.Reporting.Drawing.Unit.Cm(0.93211877346038818D));
            this.textBox29.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox29.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox29.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox29.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox29.StyleName = "";
            this.textBox29.Value = "是否通过其它\r\n经销商代交车";
            // 
            // textBox41
            // 
            this.textBox41.Name = "textBox41";
            this.textBox41.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9841976165771484D), Telerik.Reporting.Drawing.Unit.Cm(1.0643857717514038D));
            this.textBox41.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox41.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox41.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox41.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox41.StyleName = "";
            this.textBox41.Value = "特殊配置需求";
            // 
            // textBox48
            // 
            this.textBox48.Name = "textBox48";
            this.textBox48.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9841976165771484D), Telerik.Reporting.Drawing.Unit.Cm(1.0643863677978516D));
            this.textBox48.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox48.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox48.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox48.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox48.StyleName = "";
            this.textBox48.Value = "项目说明";
            // 
            // textBox21
            // 
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.9999997615814209D), Telerik.Reporting.Drawing.Unit.Cm(0.79985147714614868D));
            this.textBox21.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox21.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox21.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(3D);
            this.textBox21.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox21.StyleName = "";
            this.textBox21.Value = "= Fields.KeyAccount.Customer.Name";
            // 
            // textBox5
            // 
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.1999677419662476D), Telerik.Reporting.Drawing.Unit.Cm(0.79985147714614868D));
            this.textBox5.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox5.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox5.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(3D);
            this.textBox5.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox5.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox5.StyleName = "";
            this.textBox5.Value = "职位";
            // 
            // textBox9
            // 
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.0422234535217285D), Telerik.Reporting.Drawing.Unit.Cm(0.79985147714614868D));
            this.textBox9.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox9.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox9.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(3D);
            this.textBox9.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox9.StyleName = "";
            this.textBox9.Value = "= Fields.KeyAccount.Customer.JobPosition";
            // 
            // textBox11
            // 
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9999998807907105D), Telerik.Reporting.Drawing.Unit.Cm(0.79985147714614868D));
            this.textBox11.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox11.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox11.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(3D);
            this.textBox11.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox11.StyleName = "";
            this.textBox11.Value = "= Fields.KeyAccount.Customer.CellPhoneNumber";
            // 
            // textBox13
            // 
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.7999999523162842D), Telerik.Reporting.Drawing.Unit.Cm(0.79985147714614868D));
            this.textBox13.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox13.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox13.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(3D);
            this.textBox13.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox13.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox13.StyleName = "";
            this.textBox13.Value = "传真";
            // 
            // textBox30
            // 
            this.textBox30.Name = "textBox30";
            this.textBox30.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.1999673843383789D), Telerik.Reporting.Drawing.Unit.Cm(0.79985147714614868D));
            this.textBox30.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox30.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox30.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(3D);
            this.textBox30.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox30.StyleName = "";
            this.textBox30.Value = "= Fields.KeyAccount.Customer.Address";
            // 
            // textBox23
            // 
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.06854510307312D), Telerik.Reporting.Drawing.Unit.Cm(0.79985147714614868D));
            this.textBox23.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox23.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox23.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(3D);
            this.textBox23.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox23.StyleName = "";
            this.textBox23.Value = "= Fields.KeyAccount.Customer.OfficePhoneNumber";
            // 
            // textBox24
            // 
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.06854510307312D), Telerik.Reporting.Drawing.Unit.Cm(0.79985147714614868D));
            this.textBox24.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox24.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox24.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(3D);
            this.textBox24.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox24.StyleName = "";
            this.textBox24.Value = "= Fields.KeyAccount.ContactJobPosition";
            // 
            // textBox26
            // 
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.06854510307312D), Telerik.Reporting.Drawing.Unit.Cm(0.93211877346038818D));
            this.textBox26.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox26.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox26.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(3D);
            this.textBox26.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox26.StyleName = "";
            // 
            // textBox55
            // 
            this.textBox55.Name = "textBox55";
            this.textBox55.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.0420440435409546D), Telerik.Reporting.Drawing.Unit.Cm(0.79985147714614868D));
            this.textBox55.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox55.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox55.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(3D);
            this.textBox55.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox55.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox55.StyleName = "";
            this.textBox55.Value = "电话";
            // 
            // textBox56
            // 
            this.textBox56.Name = "textBox56";
            this.textBox56.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.0420440435409546D), Telerik.Reporting.Drawing.Unit.Cm(0.79985147714614868D));
            this.textBox56.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox56.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox56.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(3D);
            this.textBox56.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox56.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox56.StyleName = "";
            this.textBox56.Value = "职位";
            // 
            // textBox59
            // 
            this.textBox59.Name = "textBox59";
            this.textBox59.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.0420440435409546D), Telerik.Reporting.Drawing.Unit.Cm(0.93211877346038818D));
            this.textBox59.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox59.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox59.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(3D);
            this.textBox59.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox59.StyleName = "";
            this.textBox59.Value = "代交车数量";
            // 
            // textBox65
            // 
            this.textBox65.Name = "textBox65";
            this.textBox65.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.1210073232650757D), Telerik.Reporting.Drawing.Unit.Cm(0.93211877346038818D));
            this.textBox65.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox65.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox65.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox65.StyleName = "";
            this.textBox65.Value = "联系人\r\n电话";
            // 
            // textBox69
            // 
            this.textBox69.Name = "textBox69";
            this.textBox69.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.3000005483627319D), Telerik.Reporting.Drawing.Unit.Cm(0.79985147714614868D));
            this.textBox69.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox69.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox69.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(3D);
            this.textBox69.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox69.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox69.StyleName = "";
            this.textBox69.Value = "手机";
            // 
            // textBox70
            // 
            this.textBox70.Name = "textBox70";
            this.textBox70.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.3000005483627319D), Telerik.Reporting.Drawing.Unit.Cm(0.79985147714614868D));
            this.textBox70.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox70.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox70.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(3D);
            this.textBox70.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox70.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox70.StyleName = "";
            this.textBox70.Value = "电话";
            // 
            // textBox74
            // 
            this.textBox74.Name = "textBox74";
            this.textBox74.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.3000005483627319D), Telerik.Reporting.Drawing.Unit.Cm(0.93211877346038818D));
            this.textBox74.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox74.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox74.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(3D);
            this.textBox74.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox74.StyleName = "";
            this.textBox74.Value = "代交车\r\n经销商名称";
            // 
            // textBox83
            // 
            this.textBox83.Name = "textBox83";
            this.textBox83.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.357892632484436D), Telerik.Reporting.Drawing.Unit.Cm(0.79985147714614868D));
            this.textBox83.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox83.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox83.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox83.StyleName = "";
            this.textBox83.Value = "个人";
            // 
            // textBox84
            // 
            this.textBox84.Name = "textBox84";
            this.textBox84.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.357892632484436D), Telerik.Reporting.Drawing.Unit.Cm(0.93211877346038818D));
            this.textBox84.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox84.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox84.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox84.StyleName = "";
            // 
            // textBox62
            // 
            this.textBox62.Name = "textBox62";
            this.textBox62.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.4788999557495117D), Telerik.Reporting.Drawing.Unit.Cm(0.79985147714614868D));
            this.textBox62.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox62.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox62.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(3D);
            this.textBox62.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox62.StyleName = "";
            this.textBox62.Value = "= Fields.KeyAccount.Customer.Fax";
            // 
            // textBox63
            // 
            this.textBox63.Name = "textBox63";
            this.textBox63.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.4788999557495117D), Telerik.Reporting.Drawing.Unit.Cm(0.79985147714614868D));
            this.textBox63.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox63.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox63.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox63.StyleName = "";
            this.textBox63.Value = "= Fields.KeyAccount.ContactFax";
            // 
            // textBox6
            // 
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(17.931680679321289D), Telerik.Reporting.Drawing.Unit.Cm(0.79985147714614868D));
            this.textBox6.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox6.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox6.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(3D);
            this.textBox6.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox6.Value = "= Fields.KeyAccount.Customer.Name";
            // 
            // textBox49
            // 
            this.textBox49.Name = "textBox49";
            this.textBox49.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(10.689489364624023D), Telerik.Reporting.Drawing.Unit.Cm(0.79985147714614868D));
            this.textBox49.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox49.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox49.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(3D);
            this.textBox49.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox49.StyleName = "";
            this.textBox49.Value = "= Fields.KeyAccount.Customer.PostCode";
            // 
            // textBox20
            // 
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.2421910762786865D), Telerik.Reporting.Drawing.Unit.Cm(0.79985147714614868D));
            this.textBox20.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox20.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox20.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(3D);
            this.textBox20.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox20.StyleName = "";
            this.textBox20.Value = "车改或组织内员工购车";
            // 
            // textBox15
            // 
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.0420440435409546D), Telerik.Reporting.Drawing.Unit.Cm(0.79985147714614868D));
            this.textBox15.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox15.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox15.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox15.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox15.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox15.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(20D);
            this.textBox15.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox15.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox15.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox15.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox15.StyleName = "";
            this.textBox15.Value = "□";
            // 
            // textBox25
            // 
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.3685457706451416D), Telerik.Reporting.Drawing.Unit.Cm(0.79985147714614868D));
            this.textBox25.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox25.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox25.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(3D);
            this.textBox25.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox25.StyleName = "";
            this.textBox25.Value = "企事业/政府/社团";
            // 
            // textBox18
            // 
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9999998807907105D), Telerik.Reporting.Drawing.Unit.Cm(0.79985147714614868D));
            this.textBox18.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox18.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox18.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox18.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox18.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox18.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(20D);
            this.textBox18.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox18.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox18.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox18.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox18.StyleName = "";
            this.textBox18.Value = "□";
            // 
            // textBox19
            // 
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.1210073232650757D), Telerik.Reporting.Drawing.Unit.Cm(0.79985147714614868D));
            this.textBox19.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox19.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox19.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox19.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox19.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox19.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(20D);
            this.textBox19.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox19.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox19.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox19.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox19.StyleName = "";
            this.textBox19.Value = "□";
            // 
            // textBox22
            // 
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.9999997615814209D), Telerik.Reporting.Drawing.Unit.Cm(0.79985147714614868D));
            this.textBox22.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox22.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox22.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox22.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox22.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox22.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(20D);
            this.textBox22.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox22.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox22.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox22.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox22.StyleName = "";
            this.textBox22.Value = "□";
            // 
            // textBox27
            // 
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.9999997615814209D), Telerik.Reporting.Drawing.Unit.Cm(0.93211877346038818D));
            this.textBox27.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox27.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox27.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox27.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox27.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox27.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(20D);
            this.textBox27.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox27.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox27.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox27.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox27.StyleName = "";
            this.textBox27.Value = "□□";
            // 
            // textBox37
            // 
            this.textBox37.Name = "textBox37";
            this.textBox37.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.2421910762786865D), Telerik.Reporting.Drawing.Unit.Cm(0.93211877346038818D));
            this.textBox37.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox37.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox37.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(3D);
            this.textBox37.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox37.StyleName = "";
            this.textBox37.Value = "是/否（后拦无需填写）";
            // 
            // textBox43
            // 
            this.textBox43.Name = "textBox43";
            this.textBox43.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(17.931680679321289D), Telerik.Reporting.Drawing.Unit.Cm(1.0643857717514038D));
            this.textBox43.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox43.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox43.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(3D);
            this.textBox43.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox43.StyleName = "";
            this.textBox43.Value = "= Fields.SpecialConfigRequirement";
            // 
            // textBox50
            // 
            this.textBox50.Name = "textBox50";
            this.textBox50.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(17.931680679321289D), Telerik.Reporting.Drawing.Unit.Cm(1.0643863677978516D));
            this.textBox50.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox50.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox50.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(3D);
            this.textBox50.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox50.StyleName = "";
            this.textBox50.Value = "（客户简介、本次采购规模、采购方式、车辆用途等情况）";
            // 
            // DataSourceWholesaleApproval
            // 
            this.DataSourceWholesaleApproval.DataMember = "GetWholesaleApprovalWithDetailsAndKeyAccountCustomer";
            this.DataSourceWholesaleApproval.DataSource = typeof(Sunlight.Silverlight.Dcs.Web.DcsDomainService);
            this.DataSourceWholesaleApproval.Name = "DataSourceWholesaleApproval";
            this.DataSourceWholesaleApproval.Parameters.AddRange(new Telerik.Reporting.ObjectDataSourceParameter[] {
            new Telerik.Reporting.ObjectDataSourceParameter("id", typeof(System.Nullable<int>), "=Parameters.paraid.Value")});
            // 
            // table3
            // 
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.81466561555862427D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.4292327165603638D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.74534898996353149D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.0404212474822998D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.26279497146606445D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.6434688568115234D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.8288047313690186D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.69878756999969482D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.6711318492889404D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.9878578782081604D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.88274407386779785D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.6448471546173096D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.5397297143936157D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.7236860990524292D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.9864816665649414D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(1.1703414916992188D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.93223202228546143D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.99993574619293213D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(3.4074914455413818D)));
            this.table3.Body.SetCellContent(1, 7, this.textBox99, 1, 3);
            this.table3.Body.SetCellContent(0, 0, this.textBox93, 1, 15);
            this.table3.Body.SetCellContent(2, 0, this.textBox66, 1, 5);
            this.table3.Body.SetCellContent(2, 5, this.textBox77, 1, 2);
            this.table3.Body.SetCellContent(2, 7, this.textBox79, 1, 3);
            this.table3.Body.SetCellContent(2, 10, this.textBox76, 1, 3);
            this.table3.Body.SetCellContent(2, 13, this.textBox91, 1, 2);
            this.table3.Body.SetCellContent(3, 0, this.textBox68, 1, 5);
            this.table3.Body.SetCellContent(3, 5, this.textBox90, 1, 2);
            this.table3.Body.SetCellContent(3, 7, this.textBox94, 1, 3);
            this.table3.Body.SetCellContent(3, 10, this.textBox98, 1, 3);
            this.table3.Body.SetCellContent(3, 13, this.textBox103, 1, 2);
            this.table3.Body.SetCellContent(1, 0, this.textBox116, 1, 5);
            this.table3.Body.SetCellContent(1, 5, this.textBox119, 1, 2);
            this.table3.Body.SetCellContent(1, 10, this.textBox100, 1, 3);
            this.table3.Body.SetCellContent(1, 13, this.textBox87, 1, 2);
            tableGroup26.Name = "Group12";
            tableGroup28.Name = "Group6";
            tableGroup29.Name = "Group1";
            tableGroup30.Name = "Group2";
            tableGroup31.Name = "Group21";
            tableGroup32.Name = "Group19";
            tableGroup33.Name = "Group22";
            tableGroup34.Name = "Group3";
            tableGroup35.Name = "Group4";
            tableGroup36.Name = "Group18";
            tableGroup37.Name = "Group24";
            tableGroup38.Name = "Group7";
            tableGroup39.Name = "Group8";
            tableGroup40.Name = "Group9";
            this.table3.ColumnGroups.Add(tableGroup26);
            this.table3.ColumnGroups.Add(tableGroup27);
            this.table3.ColumnGroups.Add(tableGroup28);
            this.table3.ColumnGroups.Add(tableGroup29);
            this.table3.ColumnGroups.Add(tableGroup30);
            this.table3.ColumnGroups.Add(tableGroup31);
            this.table3.ColumnGroups.Add(tableGroup32);
            this.table3.ColumnGroups.Add(tableGroup33);
            this.table3.ColumnGroups.Add(tableGroup34);
            this.table3.ColumnGroups.Add(tableGroup35);
            this.table3.ColumnGroups.Add(tableGroup36);
            this.table3.ColumnGroups.Add(tableGroup37);
            this.table3.ColumnGroups.Add(tableGroup38);
            this.table3.ColumnGroups.Add(tableGroup39);
            this.table3.ColumnGroups.Add(tableGroup40);
            this.table3.DataSource = this.DataSourceWholesaleApprovalPost;
            this.table3.Docking = Telerik.Reporting.DockingStyle.Bottom;
            this.table3.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox99,
            this.textBox93,
            this.textBox66,
            this.textBox77,
            this.textBox79,
            this.textBox76,
            this.textBox91,
            this.textBox68,
            this.textBox90,
            this.textBox94,
            this.textBox98,
            this.textBox103,
            this.textBox116,
            this.textBox119,
            this.textBox100,
            this.textBox87});
            this.table3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(14.989999771118164D));
            this.table3.Name = "table3";
            tableGroup42.Name = "Group14";
            tableGroup43.Name = "Group15";
            tableGroup44.Name = "Group5";
            tableGroup45.Name = "Group10";
            tableGroup41.ChildGroups.Add(tableGroup42);
            tableGroup41.ChildGroups.Add(tableGroup43);
            tableGroup41.ChildGroups.Add(tableGroup44);
            tableGroup41.ChildGroups.Add(tableGroup45);
            tableGroup41.Groupings.AddRange(new Telerik.Reporting.Grouping[] {
            new Telerik.Reporting.Grouping(null)});
            tableGroup41.Name = "DetailGroup";
            this.table3.RowGroups.Add(tableGroup41);
            this.table3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.899999618530273D), Telerik.Reporting.Drawing.Unit.Cm(6.5100002288818359D));
            this.table3.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table3.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table3.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table3.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table3.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.table3.Style.Font.Name = "微软雅黑";
            this.table3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(11D);
            // 
            // textBox99
            // 
            this.textBox99.Name = "textBox99";
            this.textBox99.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.3577771186828613D), Telerik.Reporting.Drawing.Unit.Cm(0.9322318434715271D));
            this.textBox99.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox99.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox99.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox99.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox99.StyleName = "";
            this.textBox99.Value = "大客户批售";
            // 
            // textBox93
            // 
            this.textBox93.Name = "textBox93";
            this.textBox93.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.900003433227539D), Telerik.Reporting.Drawing.Unit.Cm(1.1703413724899292D));
            this.textBox93.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox93.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox93.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox93.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox93.StyleName = "";
            this.textBox93.Value = "长安马自达审批";
            // 
            // textBox66
            // 
            this.textBox66.Name = "textBox66";
            this.textBox66.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.2924633026123047D), Telerik.Reporting.Drawing.Unit.Cm(0.99993574619293213D));
            this.textBox66.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox66.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox66.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox66.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox66.StyleName = "";
            this.textBox66.Value = "区域销售室经理";
            // 
            // textBox77
            // 
            this.textBox77.Name = "textBox77";
            this.textBox77.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.4722733497619629D), Telerik.Reporting.Drawing.Unit.Cm(0.99993574619293213D));
            this.textBox77.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox77.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox77.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(3D);
            this.textBox77.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox77.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox77.StyleName = "";
            this.textBox77.Value = "销售部总监/副总监";
            // 
            // textBox79
            // 
            this.textBox79.Name = "textBox79";
            this.textBox79.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.3577771186828613D), Telerik.Reporting.Drawing.Unit.Cm(0.99993574619293213D));
            this.textBox79.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox79.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox79.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(3D);
            this.textBox79.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox79.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox79.StyleName = "";
            this.textBox79.Value = "总经理/副总经理";
            // 
            // textBox76
            // 
            this.textBox76.Name = "textBox76";
            this.textBox76.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.0673208236694336D), Telerik.Reporting.Drawing.Unit.Cm(0.99993574619293213D));
            this.textBox76.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox76.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox76.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(3D);
            this.textBox76.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox76.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox76.StyleName = "";
            this.textBox76.Value = "财务负责人";
            // 
            // textBox91
            // 
            this.textBox91.Name = "textBox91";
            this.textBox91.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.7101676464080811D), Telerik.Reporting.Drawing.Unit.Cm(0.99993574619293213D));
            this.textBox91.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox91.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox91.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(3D);
            this.textBox91.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox91.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox91.StyleName = "";
            this.textBox91.Value = "财务副总裁";
            // 
            // textBox68
            // 
            this.textBox68.Name = "textBox68";
            this.textBox68.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.2924633026123047D), Telerik.Reporting.Drawing.Unit.Cm(3.4074907302856445D));
            this.textBox68.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox68.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox68.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox68.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox68.StyleName = "";
            this.textBox68.Value = "= Fields.Item + Sunlight.Silverlight.Dcs.Web.Reporting.CommonConverter.GetDateTim" +
    "eAndPost(\"区域销售室经理通过\")";
            // 
            // textBox90
            // 
            this.textBox90.Name = "textBox90";
            this.textBox90.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.4722733497619629D), Telerik.Reporting.Drawing.Unit.Cm(3.4074907302856445D));
            this.textBox90.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox90.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox90.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(3D);
            this.textBox90.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox90.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox90.StyleName = "";
            this.textBox90.Value = resources.GetString("textBox90.Value");
            // 
            // textBox94
            // 
            this.textBox94.Name = "textBox94";
            this.textBox94.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.3577771186828613D), Telerik.Reporting.Drawing.Unit.Cm(3.4074907302856445D));
            this.textBox94.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox94.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox94.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(3D);
            this.textBox94.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox94.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox94.StyleName = "";
            this.textBox94.Value = resources.GetString("textBox94.Value");
            // 
            // textBox98
            // 
            this.textBox98.Name = "textBox98";
            this.textBox98.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.0673208236694336D), Telerik.Reporting.Drawing.Unit.Cm(3.4074907302856445D));
            this.textBox98.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox98.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox98.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(3D);
            this.textBox98.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox98.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox98.StyleName = "";
            this.textBox98.Value = "= Fields.Item + Sunlight.Silverlight.Dcs.Web.Reporting.CommonConverter.GetDateTim" +
    "eAndPost(\"财务负责人通过\")";
            // 
            // textBox103
            // 
            this.textBox103.Name = "textBox103";
            this.textBox103.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.7101676464080811D), Telerik.Reporting.Drawing.Unit.Cm(3.4074907302856445D));
            this.textBox103.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox103.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox103.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(3D);
            this.textBox103.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox103.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox103.StyleName = "";
            this.textBox103.Value = "= Fields.Item + Sunlight.Silverlight.Dcs.Web.Reporting.CommonConverter.GetDateTim" +
    "eAndPost(\"财务副总裁通过\")";
            // 
            // textBox116
            // 
            this.textBox116.Name = "textBox116";
            this.textBox116.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.2924633026123047D), Telerik.Reporting.Drawing.Unit.Cm(0.9322318434715271D));
            this.textBox116.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox116.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox116.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox116.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox116.StyleName = "";
            this.textBox116.Value = "大区经理";
            // 
            // textBox119
            // 
            this.textBox119.Name = "textBox119";
            this.textBox119.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.4722733497619629D), Telerik.Reporting.Drawing.Unit.Cm(0.9322318434715271D));
            this.textBox119.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox119.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox119.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox119.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox119.StyleName = "";
            this.textBox119.Value = "= Fields.Item + Sunlight.Silverlight.Dcs.Web.Reporting.CommonConverter.GetDateTim" +
    "eAndPost(\"大区经理通过\")";
            // 
            // textBox100
            // 
            this.textBox100.Name = "textBox100";
            this.textBox100.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.0673208236694336D), Telerik.Reporting.Drawing.Unit.Cm(0.9322318434715271D));
            this.textBox100.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox100.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox100.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox100.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox100.StyleName = "";
            this.textBox100.Value = "= Fields.Item + Sunlight.Silverlight.Dcs.Web.Reporting.CommonConverter.GetDateTim" +
    "eAndPost(\"大区经理通过\")";
            // 
            // textBox87
            // 
            this.textBox87.Name = "textBox87";
            this.textBox87.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.7101676464080811D), Telerik.Reporting.Drawing.Unit.Cm(0.9322318434715271D));
            this.textBox87.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox87.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox87.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox87.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox87.StyleName = "";
            // 
            // DataSourceWholesaleApprovalPost
            // 
            this.DataSourceWholesaleApprovalPost.DataMember = "GetWholesaleApprovalAuditRecDateTime";
            this.DataSourceWholesaleApprovalPost.DataSource = typeof(Sunlight.Silverlight.Dcs.Web.DcsDomainService);
            this.DataSourceWholesaleApprovalPost.Name = "DataSourceWholesaleApprovalPost";
            this.DataSourceWholesaleApprovalPost.Parameters.AddRange(new Telerik.Reporting.ObjectDataSourceParameter[] {
            new Telerik.Reporting.ObjectDataSourceParameter("wholesaleApprovalId", typeof(System.Nullable<int>), "=Parameters.paraid.Value"),
            new Telerik.Reporting.ObjectDataSourceParameter("postAuditStatusId", typeof(System.Nullable<int>), "=Parameters.paraPostAuditStatusId.Value")});
            // 
            // table2
            // 
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.281282514333725D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(4.5676398277282715D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.85404074192047119D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.2823834419250488D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.5906948447227478D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.69168722629547119D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.1262335777282715D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.74794667959213257D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.1824936866760254D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.4230210781097412D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.1980006694793701D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.954575777053833D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.80000007152557373D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.80000007152557373D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.80000007152557373D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.80000007152557373D)));
            this.table2.Body.SetCellContent(0, 0, this.textBox67, 2, 2);
            this.table2.Body.SetCellContent(2, 0, this.textBox71, 2, 2);
            this.table2.Body.SetCellContent(0, 2, this.textBox113, 2, 4);
            this.table2.Body.SetCellContent(2, 2, this.textBox89, 2, 4);
            this.table2.Body.SetCellContent(0, 6, this.textBox133, 4, 6);
            tableGroup47.Name = "Group12";
            tableGroup46.ChildGroups.Add(tableGroup47);
            tableGroup46.ChildGroups.Add(tableGroup48);
            tableGroup46.Name = "Group5";
            tableGroup46.ReportItem = this.textBox125;
            tableGroup50.Name = "Group6";
            tableGroup49.ChildGroups.Add(tableGroup50);
            tableGroup49.ReportItem = this.textBox136;
            tableGroup52.Name = "Group1";
            tableGroup53.Name = "Group2";
            tableGroup51.ChildGroups.Add(tableGroup52);
            tableGroup51.ChildGroups.Add(tableGroup53);
            tableGroup51.Name = "Group10";
            tableGroup51.ReportItem = this.textBox127;
            tableGroup55.Name = "Group21";
            tableGroup54.ChildGroups.Add(tableGroup55);
            tableGroup54.Name = "Group20";
            tableGroup54.ReportItem = this.textBox126;
            tableGroup57.Name = "Group19";
            tableGroup56.ChildGroups.Add(tableGroup57);
            tableGroup56.Name = "Group23";
            tableGroup56.ReportItem = this.textBox130;
            tableGroup59.Name = "Group22";
            tableGroup58.ChildGroups.Add(tableGroup59);
            tableGroup58.Name = "Group25";
            tableGroup58.ReportItem = this.textBox128;
            tableGroup61.Name = "Group3";
            tableGroup60.ChildGroups.Add(tableGroup61);
            tableGroup60.Name = "Group26";
            tableGroup60.ReportItem = this.textBox132;
            tableGroup63.Name = "Group4";
            tableGroup62.ChildGroups.Add(tableGroup63);
            tableGroup62.Name = "Group27";
            tableGroup62.ReportItem = this.textBox129;
            tableGroup65.Name = "Group18";
            tableGroup66.Name = "Group24";
            tableGroup64.ChildGroups.Add(tableGroup65);
            tableGroup64.ChildGroups.Add(tableGroup66);
            tableGroup64.Name = "Group28";
            tableGroup64.ReportItem = this.textBox134;
            this.table2.ColumnGroups.Add(tableGroup46);
            this.table2.ColumnGroups.Add(tableGroup49);
            this.table2.ColumnGroups.Add(tableGroup51);
            this.table2.ColumnGroups.Add(tableGroup54);
            this.table2.ColumnGroups.Add(tableGroup56);
            this.table2.ColumnGroups.Add(tableGroup58);
            this.table2.ColumnGroups.Add(tableGroup60);
            this.table2.ColumnGroups.Add(tableGroup62);
            this.table2.ColumnGroups.Add(tableGroup64);
            this.table2.DataSource = this.DataSourceWholesaleApproval;
            this.table2.Docking = Telerik.Reporting.DockingStyle.Bottom;
            this.table2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox67,
            this.textBox71,
            this.textBox113,
            this.textBox89,
            this.textBox133,
            this.textBox125,
            this.textBox136,
            this.textBox127,
            this.textBox126,
            this.textBox130,
            this.textBox128,
            this.textBox132,
            this.textBox129,
            this.textBox134});
            this.table2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(10.990000724792481D));
            this.table2.Name = "table2";
            tableGroup68.Name = "Group7";
            tableGroup69.Name = "Group9";
            tableGroup70.Name = "Group8";
            tableGroup71.Name = "Group13";
            tableGroup67.ChildGroups.Add(tableGroup68);
            tableGroup67.ChildGroups.Add(tableGroup69);
            tableGroup67.ChildGroups.Add(tableGroup70);
            tableGroup67.ChildGroups.Add(tableGroup71);
            tableGroup67.Groupings.AddRange(new Telerik.Reporting.Grouping[] {
            new Telerik.Reporting.Grouping(null)});
            tableGroup67.Name = "DetailGroup";
            this.table2.RowGroups.Add(tableGroup67);
            this.table2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.899999618530273D), Telerik.Reporting.Drawing.Unit.Cm(4D));
            this.table2.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table2.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table2.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table2.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table2.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.table2.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.table2.Style.Font.Name = "微软雅黑";
            this.table2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(11D);
            // 
            // textBox67
            // 
            this.textBox67.Name = "textBox67";
            this.textBox67.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.8489222526550293D), Telerik.Reporting.Drawing.Unit.Cm(1.5999999046325684D));
            this.textBox67.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox67.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox67.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox67.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox67.StyleName = "";
            this.textBox67.Value = "经销商\r\n批售负责人";
            // 
            // textBox71
            // 
            this.textBox71.Name = "textBox71";
            this.textBox71.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.8489222526550293D), Telerik.Reporting.Drawing.Unit.Cm(1.5999999046325684D));
            this.textBox71.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox71.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox71.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox71.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox71.StyleName = "";
            this.textBox71.Value = "联系电话";
            // 
            // textBox113
            // 
            this.textBox113.Name = "textBox113";
            this.textBox113.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.4188065528869629D), Telerik.Reporting.Drawing.Unit.Cm(1.5999999046325684D));
            this.textBox113.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox113.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox113.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(3D);
            this.textBox113.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox113.Value = "= Fields.DealerKeyAccountResponsible";
            // 
            // textBox89
            // 
            this.textBox89.Name = "textBox89";
            this.textBox89.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.4188065528869629D), Telerik.Reporting.Drawing.Unit.Cm(1.5999999046325684D));
            this.textBox89.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox89.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox89.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(3D);
            this.textBox89.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox89.StyleName = "";
            this.textBox89.Value = "= Fields.DealerContactPhone";
            // 
            // textBox133
            // 
            this.textBox133.Name = "textBox133";
            this.textBox133.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(11.632271766662598D), Telerik.Reporting.Drawing.Unit.Cm(3.1999998092651367D));
            this.textBox133.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox133.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox133.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(3D);
            this.textBox133.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox133.StyleName = "";
            this.textBox133.Value = "经销商公章：";
            // 
            // list3
            // 
            this.list3.Bindings.Add(new Telerik.Reporting.Binding("DataSource", "=Fields.WholesaleApprovalDetails"));
            this.list3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.6874334812164307D)));
            this.list3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.833246111869812D)));
            this.list3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.0954424142837524D)));
            this.list3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.1276204586029053D)));
            this.list3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.4274768829345703D)));
            this.list3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.878024697303772D)));
            this.list3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.562105655670166D)));
            this.list3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.351482629776001D)));
            this.list3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.9570057392120361D)));
            this.list3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.9900808334350586D)));
            this.list3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.9900808334350586D)));
            this.list3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.52810919284820557D)));
            this.list3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.98857587575912476D)));
            this.list3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.80665779113769531D)));
            this.list3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.80665779113769531D)));
            this.list3.Body.SetCellContent(2, 0, this.textBox186);
            this.list3.Body.SetCellContent(2, 1, this.textBox189);
            this.list3.Body.SetCellContent(2, 2, this.textBox191);
            this.list3.Body.SetCellContent(2, 3, this.textBox203);
            this.list3.Body.SetCellContent(2, 4, this.textBox208);
            this.list3.Body.SetCellContent(2, 5, this.textBox212);
            this.list3.Body.SetCellContent(2, 6, this.textBox219);
            this.list3.Body.SetCellContent(1, 8, this.textBox220);
            this.list3.Body.SetCellContent(2, 8, this.textBox222);
            this.list3.Body.SetCellContent(2, 7, this.textBox226);
            this.list3.Body.SetCellContent(3, 2, this.textBox4);
            this.list3.Body.SetCellContent(3, 3, this.textBox31);
            this.list3.Body.SetCellContent(3, 4, this.textBox32);
            this.list3.Body.SetCellContent(3, 5, this.textBox34);
            this.list3.Body.SetCellContent(3, 6, this.textBox35);
            this.list3.Body.SetCellContent(3, 7, this.textBox36);
            this.list3.Body.SetCellContent(3, 8, this.textBox38);
            this.list3.Body.SetCellContent(3, 0, this.textBox44, 1, 2);
            this.list3.Body.SetCellContent(1, 10, this.textBox45);
            this.list3.Body.SetCellContent(2, 10, this.textBox46);
            this.list3.Body.SetCellContent(3, 10, this.textBox47);
            this.list3.Body.SetCellContent(1, 9, this.textBox52);
            this.list3.Body.SetCellContent(2, 9, this.textBox53);
            this.list3.Body.SetCellContent(3, 9, this.textBox54);
            this.list3.Body.SetCellContent(0, 0, this.textBox58, 2, 1);
            this.list3.Body.SetCellContent(0, 1, this.textBox60, 2, 1);
            this.list3.Body.SetCellContent(0, 2, this.textBox190, 2, 1);
            this.list3.Body.SetCellContent(0, 3, this.textBox192, 2, 1);
            this.list3.Body.SetCellContent(0, 4, this.textBox61, 2, 1);
            this.list3.Body.SetCellContent(0, 5, this.textBox209, 2, 1);
            this.list3.Body.SetCellContent(0, 6, this.textBox213, 2, 1);
            this.list3.Body.SetCellContent(0, 7, this.textBox64, 2, 1);
            this.list3.Body.SetCellContent(0, 8, this.textBox72, 1, 3);
            tableGroup72.Name = "ColumnGroup";
            tableGroup73.Name = "Group1";
            tableGroup74.Name = "Group3";
            tableGroup75.Name = "Group5";
            tableGroup76.Name = "Group6";
            tableGroup77.Name = "Group8";
            tableGroup78.Name = "Group9";
            tableGroup79.Name = "Group11";
            tableGroup80.Name = "Group10";
            tableGroup81.Name = "Group16";
            tableGroup82.Name = "Group15";
            this.list3.ColumnGroups.Add(tableGroup72);
            this.list3.ColumnGroups.Add(tableGroup73);
            this.list3.ColumnGroups.Add(tableGroup74);
            this.list3.ColumnGroups.Add(tableGroup75);
            this.list3.ColumnGroups.Add(tableGroup76);
            this.list3.ColumnGroups.Add(tableGroup77);
            this.list3.ColumnGroups.Add(tableGroup78);
            this.list3.ColumnGroups.Add(tableGroup79);
            this.list3.ColumnGroups.Add(tableGroup80);
            this.list3.ColumnGroups.Add(tableGroup81);
            this.list3.ColumnGroups.Add(tableGroup82);
            this.list3.ColumnHeadersPrintOnEveryPage = true;
            this.list3.DataSource = this.DataSourceWholesaleApproval;
            this.list3.Docking = Telerik.Reporting.DockingStyle.Fill;
            this.list3.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox186,
            this.textBox189,
            this.textBox191,
            this.textBox203,
            this.textBox208,
            this.textBox212,
            this.textBox219,
            this.textBox220,
            this.textBox222,
            this.textBox226,
            this.textBox4,
            this.textBox31,
            this.textBox32,
            this.textBox34,
            this.textBox35,
            this.textBox36,
            this.textBox38,
            this.textBox44,
            this.textBox45,
            this.textBox46,
            this.textBox47,
            this.textBox52,
            this.textBox53,
            this.textBox54,
            this.textBox58,
            this.textBox60,
            this.textBox190,
            this.textBox192,
            this.textBox61,
            this.textBox209,
            this.textBox213,
            this.textBox64,
            this.textBox72});
            this.list3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(7.8599996566772461D));
            this.list3.Name = "list3";
            tableGroup83.Name = "Group17";
            tableGroup84.Name = "Group2";
            tableGroup86.Name = "Group4";
            tableGroup85.ChildGroups.Add(tableGroup86);
            tableGroup85.Groupings.AddRange(new Telerik.Reporting.Grouping[] {
            new Telerik.Reporting.Grouping(null)});
            tableGroup85.Name = "DetailGroup";
            tableGroup88.Name = "Group14";
            tableGroup87.ChildGroups.Add(tableGroup88);
            tableGroup87.Name = "Group13";
            this.list3.RowGroups.Add(tableGroup83);
            this.list3.RowGroups.Add(tableGroup84);
            this.list3.RowGroups.Add(tableGroup85);
            this.list3.RowGroups.Add(tableGroup87);
            this.list3.RowHeadersPrintOnEveryPage = true;
            this.list3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.899999618530273D), Telerik.Reporting.Drawing.Unit.Cm(3.1300005912780762D));
            this.list3.Sortings.AddRange(new Telerik.Reporting.Sorting[] {
            new Telerik.Reporting.Sorting("Id", Telerik.Reporting.SortDirection.Asc)});
            this.list3.Style.Font.Name = "微软雅黑";
            // 
            // textBox186
            // 
            this.textBox186.Name = "textBox186";
            this.textBox186.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.6874334812164307D), Telerik.Reporting.Drawing.Unit.Cm(0.80665779113769531D));
            this.textBox186.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox186.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox186.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox186.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox186.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox186.Style.Font.Name = "微软雅黑";
            this.textBox186.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox186.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox186.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox186.StyleName = "";
            this.textBox186.Value = "= ProductCategoryName";
            // 
            // textBox189
            // 
            this.textBox189.Name = "textBox189";
            this.textBox189.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.833246111869812D), Telerik.Reporting.Drawing.Unit.Cm(0.80665779113769531D));
            this.textBox189.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox189.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox189.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox189.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox189.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox189.Style.Font.Name = "微软雅黑";
            this.textBox189.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox189.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox189.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox189.StyleName = "";
            this.textBox189.Value = "= Color";
            // 
            // textBox191
            // 
            this.textBox191.Name = "textBox191";
            this.textBox191.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.0954424142837524D), Telerik.Reporting.Drawing.Unit.Cm(0.80665779113769531D));
            this.textBox191.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox191.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox191.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox191.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox191.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox191.Style.Font.Name = "微软雅黑";
            this.textBox191.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox191.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox191.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox191.StyleName = "";
            this.textBox191.Value = "= Quantity";
            // 
            // textBox203
            // 
            this.textBox203.Format = "{0:C2}";
            this.textBox203.Name = "textBox203";
            this.textBox203.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.1276204586029053D), Telerik.Reporting.Drawing.Unit.Cm(0.80665779113769531D));
            this.textBox203.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox203.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox203.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox203.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox203.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox203.Style.Font.Name = "微软雅黑";
            this.textBox203.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox203.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox203.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox203.StyleName = "";
            this.textBox203.Value = "= SupplyDate";
            // 
            // textBox208
            // 
            this.textBox208.Format = "{0:C2}";
            this.textBox208.Name = "textBox208";
            this.textBox208.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.4274768829345703D), Telerik.Reporting.Drawing.Unit.Cm(0.80665779113769531D));
            this.textBox208.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox208.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox208.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox208.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox208.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox208.Style.Font.Name = "微软雅黑";
            this.textBox208.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox208.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox208.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox208.StyleName = "";
            this.textBox208.Value = "= RetailGuidePrice";
            // 
            // textBox212
            // 
            this.textBox212.Format = "{0:C2}";
            this.textBox212.Name = "textBox212";
            this.textBox212.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.878024697303772D), Telerik.Reporting.Drawing.Unit.Cm(0.80665779113769531D));
            this.textBox212.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox212.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox212.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox212.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox212.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox212.Style.Font.Name = "微软雅黑";
            this.textBox212.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox212.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox212.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox212.StyleName = "";
            this.textBox212.Value = "= WholesalePrice";
            // 
            // textBox219
            // 
            this.textBox219.Format = "{0:C2}";
            this.textBox219.Name = "textBox219";
            this.textBox219.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.5621057748794556D), Telerik.Reporting.Drawing.Unit.Cm(0.80665779113769531D));
            this.textBox219.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox219.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox219.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox219.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox219.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox219.Style.Font.Name = "微软雅黑";
            this.textBox219.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox219.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox219.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox219.StyleName = "";
            this.textBox219.Value = "=RetailGuidePrice - WholesalePrice";
            // 
            // textBox220
            // 
            this.textBox220.Format = "";
            this.textBox220.Name = "textBox220";
            this.textBox220.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9570058584213257D), Telerik.Reporting.Drawing.Unit.Cm(0.98857587575912476D));
            this.textBox220.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox220.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox220.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox220.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox220.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox220.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox220.Style.Font.Name = "微软雅黑";
            this.textBox220.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox220.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox220.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox220.Style.Visible = true;
            this.textBox220.StyleName = "";
            this.textBox220.Value = "正常批售\r\n返利支持";
            // 
            // textBox222
            // 
            this.textBox222.Format = "{0:C2}";
            this.textBox222.Name = "textBox222";
            this.textBox222.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9570058584213257D), Telerik.Reporting.Drawing.Unit.Cm(0.80665779113769531D));
            this.textBox222.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox222.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox222.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox222.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox222.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox222.Style.Font.Name = "微软雅黑";
            this.textBox222.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox222.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox222.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox222.StyleName = "";
            this.textBox222.Value = "= StandardWholesaleRebate";
            // 
            // textBox226
            // 
            this.textBox226.Format = "{0:P2}";
            this.textBox226.Name = "textBox226";
            this.textBox226.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.3514827489852905D), Telerik.Reporting.Drawing.Unit.Cm(0.80665779113769531D));
            this.textBox226.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox226.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox226.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox226.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox226.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox226.Style.Font.Name = "微软雅黑";
            this.textBox226.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox226.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox226.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox226.StyleName = "";
            this.textBox226.Value = "= (RetailGuidePrice - WholesalePrice) / RetailGuidePrice";
            // 
            // textBox4
            // 
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.0954424142837524D), Telerik.Reporting.Drawing.Unit.Cm(0.80665779113769531D));
            this.textBox4.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox4.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox4.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox4.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox4.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox4.Style.Font.Name = "微软雅黑";
            this.textBox4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox4.StyleName = "";
            this.textBox4.Value = "= Sum(Quantity)";
            // 
            // textBox31
            // 
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.1276204586029053D), Telerik.Reporting.Drawing.Unit.Cm(0.80665779113769531D));
            this.textBox31.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox31.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox31.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox31.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox31.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox31.Style.Font.Name = "微软雅黑";
            this.textBox31.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox31.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox31.StyleName = "";
            // 
            // textBox32
            // 
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.4274768829345703D), Telerik.Reporting.Drawing.Unit.Cm(0.80665779113769531D));
            this.textBox32.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox32.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox32.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox32.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox32.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox32.Style.Font.Name = "微软雅黑";
            this.textBox32.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox32.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox32.StyleName = "";
            // 
            // textBox34
            // 
            this.textBox34.Format = "{0:C2}";
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.878024697303772D), Telerik.Reporting.Drawing.Unit.Cm(0.80665779113769531D));
            this.textBox34.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox34.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox34.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox34.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox34.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox34.Style.Font.Name = "微软雅黑";
            this.textBox34.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox34.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox34.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox34.StyleName = "";
            // 
            // textBox35
            // 
            this.textBox35.Name = "textBox35";
            this.textBox35.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.5621057748794556D), Telerik.Reporting.Drawing.Unit.Cm(0.80665779113769531D));
            this.textBox35.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox35.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox35.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox35.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox35.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox35.Style.Font.Name = "微软雅黑";
            this.textBox35.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox35.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox35.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox35.StyleName = "";
            // 
            // textBox36
            // 
            this.textBox36.Name = "textBox36";
            this.textBox36.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.3514827489852905D), Telerik.Reporting.Drawing.Unit.Cm(0.80665779113769531D));
            this.textBox36.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox36.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox36.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox36.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox36.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox36.Style.Font.Name = "微软雅黑";
            this.textBox36.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox36.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox36.StyleName = "";
            // 
            // textBox38
            // 
            this.textBox38.Name = "textBox38";
            this.textBox38.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9570058584213257D), Telerik.Reporting.Drawing.Unit.Cm(0.80665779113769531D));
            this.textBox38.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox38.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox38.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox38.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox38.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox38.Style.Font.Name = "微软雅黑";
            this.textBox38.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox38.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox38.StyleName = "";
            // 
            // textBox44
            // 
            this.textBox44.Name = "textBox44";
            this.textBox44.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.5206794738769531D), Telerik.Reporting.Drawing.Unit.Cm(0.80665779113769531D));
            this.textBox44.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox44.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox44.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox44.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox44.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox44.Style.Font.Name = "微软雅黑";
            this.textBox44.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox44.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox44.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox44.StyleName = "";
            this.textBox44.Value = "总计：";
            // 
            // textBox45
            // 
            this.textBox45.Name = "textBox45";
            this.textBox45.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9900808334350586D), Telerik.Reporting.Drawing.Unit.Cm(0.98857587575912476D));
            this.textBox45.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox45.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox45.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox45.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox45.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox45.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox45.Style.Font.Name = "微软雅黑";
            this.textBox45.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox45.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox45.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox45.StyleName = "";
            this.textBox45.Value = "其它";
            // 
            // textBox46
            // 
            this.textBox46.Format = "{0:C2}";
            this.textBox46.Name = "textBox46";
            this.textBox46.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9900808334350586D), Telerik.Reporting.Drawing.Unit.Cm(0.80665779113769531D));
            this.textBox46.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox46.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox46.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox46.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox46.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox46.Style.Font.Name = "微软雅黑";
            this.textBox46.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox46.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox46.StyleName = "";
            this.textBox46.Value = "= OtherWholesaleReward";
            // 
            // textBox47
            // 
            this.textBox47.Name = "textBox47";
            this.textBox47.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9900808334350586D), Telerik.Reporting.Drawing.Unit.Cm(0.80665779113769531D));
            this.textBox47.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox47.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox47.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox47.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox47.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox47.Style.Font.Name = "微软雅黑";
            this.textBox47.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox47.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox47.StyleName = "";
            // 
            // textBox52
            // 
            this.textBox52.Name = "textBox52";
            this.textBox52.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9900808334350586D), Telerik.Reporting.Drawing.Unit.Cm(0.98857587575912476D));
            this.textBox52.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox52.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox52.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox52.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox52.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox52.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox52.Style.Font.Name = "微软雅黑";
            this.textBox52.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox52.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox52.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox52.StyleName = "";
            this.textBox52.Value = "特殊批售\r\n支持";
            // 
            // textBox53
            // 
            this.textBox53.Format = "{0:C2}";
            this.textBox53.Name = "textBox53";
            this.textBox53.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9900808334350586D), Telerik.Reporting.Drawing.Unit.Cm(0.80665779113769531D));
            this.textBox53.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox53.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox53.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox53.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox53.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox53.Style.Font.Name = "微软雅黑";
            this.textBox53.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox53.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox53.StyleName = "";
            this.textBox53.Value = "= SpecialWholesaleRebate";
            // 
            // textBox54
            // 
            this.textBox54.Name = "textBox54";
            this.textBox54.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9900808334350586D), Telerik.Reporting.Drawing.Unit.Cm(0.80665779113769531D));
            this.textBox54.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox54.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox54.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox54.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox54.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox54.Style.Font.Name = "微软雅黑";
            this.textBox54.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox54.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox54.StyleName = "";
            // 
            // textBox58
            // 
            this.textBox58.Name = "textBox58";
            this.textBox58.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.6874334812164307D), Telerik.Reporting.Drawing.Unit.Cm(1.5166850090026856D));
            this.textBox58.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox58.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox58.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox58.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox58.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox58.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox58.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Pixel(3D);
            this.textBox58.Style.Font.Name = "微软雅黑";
            this.textBox58.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox58.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox58.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox58.StyleName = "";
            this.textBox58.Value = "需求车型";
            // 
            // textBox60
            // 
            this.textBox60.Name = "textBox60";
            this.textBox60.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.833246111869812D), Telerik.Reporting.Drawing.Unit.Cm(1.5166850090026856D));
            this.textBox60.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox60.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox60.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox60.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox60.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox60.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox60.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Pixel(3D);
            this.textBox60.Style.Font.Name = "微软雅黑";
            this.textBox60.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox60.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox60.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox60.StyleName = "";
            this.textBox60.Value = "颜色";
            // 
            // textBox190
            // 
            this.textBox190.Name = "textBox190";
            this.textBox190.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.0954424142837524D), Telerik.Reporting.Drawing.Unit.Cm(1.5166850090026856D));
            this.textBox190.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox190.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox190.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox190.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox190.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox190.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox190.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Pixel(3D);
            this.textBox190.Style.Font.Name = "微软雅黑";
            this.textBox190.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox190.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox190.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox190.Style.Visible = true;
            this.textBox190.StyleName = "";
            this.textBox190.Value = "数量";
            // 
            // textBox192
            // 
            this.textBox192.Name = "textBox192";
            this.textBox192.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.1276204586029053D), Telerik.Reporting.Drawing.Unit.Cm(1.5166850090026856D));
            this.textBox192.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox192.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox192.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox192.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox192.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox192.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox192.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Pixel(3D);
            this.textBox192.Style.Font.Name = "微软雅黑";
            this.textBox192.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox192.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox192.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox192.Style.Visible = true;
            this.textBox192.StyleName = "";
            this.textBox192.Value = "供货期";
            // 
            // textBox61
            // 
            this.textBox61.Name = "textBox61";
            this.textBox61.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.4274768829345703D), Telerik.Reporting.Drawing.Unit.Cm(1.5166850090026856D));
            this.textBox61.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox61.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox61.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox61.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox61.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox61.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox61.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Pixel(3D);
            this.textBox61.Style.Font.Name = "微软雅黑";
            this.textBox61.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox61.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox61.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox61.Style.Visible = true;
            this.textBox61.StyleName = "";
            this.textBox61.Value = "长安马自达建议零售价";
            // 
            // textBox209
            // 
            this.textBox209.Name = "textBox209";
            this.textBox209.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.878024697303772D), Telerik.Reporting.Drawing.Unit.Cm(1.5166850090026856D));
            this.textBox209.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox209.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox209.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox209.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox209.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox209.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox209.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Pixel(3D);
            this.textBox209.Style.Font.Name = "微软雅黑";
            this.textBox209.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox209.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox209.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox209.Style.Visible = true;
            this.textBox209.StyleName = "";
            this.textBox209.Value = "批售成交价（开票价）";
            // 
            // textBox213
            // 
            this.textBox213.Format = "";
            this.textBox213.Name = "textBox213";
            this.textBox213.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.5621057748794556D), Telerik.Reporting.Drawing.Unit.Cm(1.5166850090026856D));
            this.textBox213.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox213.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox213.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox213.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox213.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox213.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox213.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Pixel(3D);
            this.textBox213.Style.Font.Name = "微软雅黑";
            this.textBox213.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox213.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox213.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox213.Style.Visible = true;
            this.textBox213.StyleName = "";
            this.textBox213.Value = "折让幅度";
            // 
            // textBox64
            // 
            this.textBox64.Format = "";
            this.textBox64.Name = "textBox64";
            this.textBox64.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.3514827489852905D), Telerik.Reporting.Drawing.Unit.Cm(1.5166850090026856D));
            this.textBox64.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox64.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox64.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox64.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox64.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox64.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox64.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Pixel(3D);
            this.textBox64.Style.Font.Name = "微软雅黑";
            this.textBox64.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox64.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox64.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox64.Style.Visible = true;
            this.textBox64.StyleName = "";
            this.textBox64.Value = "折让率";
            // 
            // textBox72
            // 
            this.textBox72.Name = "textBox72";
            this.textBox72.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.9371671676635742D), Telerik.Reporting.Drawing.Unit.Cm(0.52810919284820557D));
            this.textBox72.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox72.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox72.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox72.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox72.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox72.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox72.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Pixel(3D);
            this.textBox72.Style.Font.Name = "微软雅黑";
            this.textBox72.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox72.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox72.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox72.StyleName = "";
            this.textBox72.Value = "批售奖励";
            // 
            // pageFooterSection1
            // 
            this.pageFooterSection1.Height = Telerik.Reporting.Drawing.Unit.Cm(1D);
            this.pageFooterSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox73});
            this.pageFooterSection1.Name = "pageFooterSection1";
            // 
            // textBox73
            // 
            this.textBox73.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(8.90000057220459D), Telerik.Reporting.Drawing.Unit.Cm(0.19990058243274689D));
            this.textBox73.Name = "textBox73";
            this.textBox73.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.0339269638061523D), Telerik.Reporting.Drawing.Unit.Cm(0.79999995231628418D));
            this.textBox73.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox73.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox73.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox73.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox73.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox73.Style.Font.Name = "微软雅黑";
            this.textBox73.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox73.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox73.StyleName = "";
            this.textBox73.Value = "= Sunlight.Silverlight.Dcs.Web.Reporting.CommonConverter.CurrentPagesConverterFor" +
    "mat(PageNumber)";
            // 
            // ReportWholesaleApproval
            // 
            this.DataSource = this.DataSourceWholesaleApproval;
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pageHeaderSection1,
            this.detail,
            this.pageFooterSection1});
            this.Name = "ReportWholesaleApproval";
            this.PageSettings.Margins.Bottom = Telerik.Reporting.Drawing.Unit.Mm(9D);
            this.PageSettings.Margins.Left = Telerik.Reporting.Drawing.Unit.Pixel(19D);
            this.PageSettings.Margins.Right = Telerik.Reporting.Drawing.Unit.Pixel(19D);
            this.PageSettings.Margins.Top = Telerik.Reporting.Drawing.Unit.Mm(9D);
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            reportParameter1.Name = "paraid";
            reportParameter2.Name = "paraPostAuditStatusId";
            this.ReportParameters.Add(reportParameter1);
            this.ReportParameters.Add(reportParameter2);
            this.Style.BackgroundColor = System.Drawing.Color.White;
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
            this.Width = Telerik.Reporting.Drawing.Unit.Cm(19.899999618530273D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.PageHeaderSection pageHeaderSection1;
        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.PageFooterSection pageFooterSection1;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.TextBox textBox73;
        private Telerik.Reporting.Table table1;
        private Telerik.Reporting.TextBox textBox33;
        private Telerik.Reporting.TextBox textBox108;
        private Telerik.Reporting.TextBox textBox109;
        private Telerik.Reporting.TextBox textBox110;
        private Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.TextBox textBox10;
        private Telerik.Reporting.TextBox textBox12;
        private Telerik.Reporting.TextBox textBox14;
        private Telerik.Reporting.TextBox textBox16;
        private Telerik.Reporting.TextBox textBox28;
        private Telerik.Reporting.TextBox textBox39;
        private Telerik.Reporting.TextBox textBox40;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox17;
        private Telerik.Reporting.TextBox textBox29;
        private Telerik.Reporting.TextBox textBox41;
        private Telerik.Reporting.TextBox textBox48;
        private Telerik.Reporting.TextBox textBox21;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.TextBox textBox9;
        private Telerik.Reporting.TextBox textBox11;
        private Telerik.Reporting.TextBox textBox13;
        private Telerik.Reporting.TextBox textBox30;
        private Telerik.Reporting.TextBox textBox23;
        private Telerik.Reporting.TextBox textBox24;
        private Telerik.Reporting.TextBox textBox26;
        private Telerik.Reporting.TextBox textBox55;
        private Telerik.Reporting.TextBox textBox56;
        private Telerik.Reporting.TextBox textBox59;
        private Telerik.Reporting.TextBox textBox65;
        private Telerik.Reporting.TextBox textBox69;
        private Telerik.Reporting.TextBox textBox70;
        private Telerik.Reporting.TextBox textBox74;
        private Telerik.Reporting.TextBox textBox83;
        private Telerik.Reporting.TextBox textBox84;
        private Telerik.Reporting.TextBox textBox62;
        private Telerik.Reporting.TextBox textBox63;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.TextBox textBox49;
        private Telerik.Reporting.TextBox textBox20;
        private Telerik.Reporting.TextBox textBox15;
        private Telerik.Reporting.TextBox textBox25;
        private Telerik.Reporting.TextBox textBox18;
        private Telerik.Reporting.TextBox textBox19;
        private Telerik.Reporting.TextBox textBox22;
        private Telerik.Reporting.TextBox textBox27;
        private Telerik.Reporting.TextBox textBox37;
        private Telerik.Reporting.TextBox textBox43;
        private Telerik.Reporting.TextBox textBox50;
        private Telerik.Reporting.TextBox textBox107;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.TextBox textBox42;
        private Telerik.Reporting.Table table3;
        private Telerik.Reporting.TextBox textBox99;
        private Telerik.Reporting.TextBox textBox93;
        private Telerik.Reporting.TextBox textBox66;
        private Telerik.Reporting.TextBox textBox77;
        private Telerik.Reporting.TextBox textBox79;
        private Telerik.Reporting.TextBox textBox76;
        private Telerik.Reporting.TextBox textBox91;
        private Telerik.Reporting.TextBox textBox68;
        private Telerik.Reporting.TextBox textBox90;
        private Telerik.Reporting.TextBox textBox94;
        private Telerik.Reporting.TextBox textBox98;
        private Telerik.Reporting.TextBox textBox103;
        private Telerik.Reporting.TextBox textBox116;
        private Telerik.Reporting.TextBox textBox119;
        private Telerik.Reporting.TextBox textBox100;
        private Telerik.Reporting.TextBox textBox87;
        private Telerik.Reporting.Table table2;
        private Telerik.Reporting.TextBox textBox67;
        private Telerik.Reporting.TextBox textBox71;
        private Telerik.Reporting.TextBox textBox113;
        private Telerik.Reporting.TextBox textBox89;
        private Telerik.Reporting.TextBox textBox133;
        private Telerik.Reporting.TextBox textBox125;
        private Telerik.Reporting.TextBox textBox136;
        private Telerik.Reporting.TextBox textBox127;
        private Telerik.Reporting.TextBox textBox126;
        private Telerik.Reporting.TextBox textBox130;
        private Telerik.Reporting.TextBox textBox128;
        private Telerik.Reporting.TextBox textBox132;
        private Telerik.Reporting.TextBox textBox129;
        private Telerik.Reporting.TextBox textBox134;
        private Telerik.Reporting.List list3;
        private Telerik.Reporting.TextBox textBox186;
        private Telerik.Reporting.TextBox textBox189;
        private Telerik.Reporting.TextBox textBox191;
        private Telerik.Reporting.TextBox textBox203;
        private Telerik.Reporting.TextBox textBox208;
        private Telerik.Reporting.TextBox textBox212;
        private Telerik.Reporting.TextBox textBox219;
        private Telerik.Reporting.TextBox textBox220;
        private Telerik.Reporting.TextBox textBox222;
        private Telerik.Reporting.TextBox textBox226;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.TextBox textBox31;
        private Telerik.Reporting.TextBox textBox32;
        private Telerik.Reporting.TextBox textBox34;
        private Telerik.Reporting.TextBox textBox35;
        private Telerik.Reporting.TextBox textBox36;
        private Telerik.Reporting.TextBox textBox38;
        private Telerik.Reporting.TextBox textBox44;
        private Telerik.Reporting.TextBox textBox45;
        private Telerik.Reporting.TextBox textBox46;
        private Telerik.Reporting.TextBox textBox47;
        private Telerik.Reporting.TextBox textBox52;
        private Telerik.Reporting.TextBox textBox53;
        private Telerik.Reporting.TextBox textBox54;
        private Telerik.Reporting.TextBox textBox58;
        private Telerik.Reporting.TextBox textBox60;
        private Telerik.Reporting.TextBox textBox190;
        private Telerik.Reporting.TextBox textBox192;
        private Telerik.Reporting.TextBox textBox61;
        private Telerik.Reporting.TextBox textBox209;
        private Telerik.Reporting.TextBox textBox213;
        private Telerik.Reporting.TextBox textBox64;
        private Telerik.Reporting.TextBox textBox72;
        private Telerik.Reporting.ObjectDataSource DataSourceWholesaleApproval;
        private Telerik.Reporting.ObjectDataSource DataSourceWholesaleApprovalPost;
    }
}