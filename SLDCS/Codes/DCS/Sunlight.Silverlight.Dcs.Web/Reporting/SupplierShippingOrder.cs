namespace Sunlight.Silverlight.Dcs.Web.Reporting
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using Telerik.Reporting;
    using Telerik.Reporting.Drawing;

    /// <summary>
    /// Summary description for SupplierShippingOrder.
    /// </summary>
    public partial class SupplierShippingOrder : Telerik.Reporting.Report
    {
        public SupplierShippingOrder()
        {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }     
    }
}