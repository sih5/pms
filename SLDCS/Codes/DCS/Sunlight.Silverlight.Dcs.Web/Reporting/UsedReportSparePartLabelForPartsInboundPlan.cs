using System.Collections.Generic;
using System.Linq;

namespace Sunlight.Silverlight.Dcs.Web.Reporting {
    using System;

    /// <summary>
    /// Summary description for ReportSparePartLabelForPartsInboundPlan.
    /// </summary>
    public partial class UsedReportSparePartLabelForPartsInboundPlan : Telerik.Reporting.Report {
        public UsedReportSparePartLabelForPartsInboundPlan() {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }

        private int num = 0;

        private void ReportSparePartLabelForPartsInboundPlan_NeedDataSource(object sender, EventArgs e) {
            var report = (Telerik.Reporting.Processing.Report)sender;
            // 取参数值
            var pararPartsTags = report.Parameters["PararPartsTags"].Value as object[];
            Dictionary<int, int> idss = new Dictionary<int, int>();
            var ids = new List<int>();
            foreach(var pararPartsTag in pararPartsTags) {
                var aa = pararPartsTag.ToString();
                var ss = aa.Split(',');
                var qq = ss[0];
                var q = ss[1];
                idss.Add(Convert.ToInt32(qq), Convert.ToInt32(q));
                ids.Add(Convert.ToInt32(qq));
            }

            int totalnum = 0;
            foreach(var key in idss.Keys) {
                totalnum = totalnum + idss[key]; //获取打印配件总数量
            }
            var domainService = new DcsDomainService();
            //根据编码规则获取序列号
            var tempCodes = domainService.GetNumberOfParts(totalnum, "WMSLabelPrintingSerial");

            // 获取客户端传入的配件入库计划清单的Id集合
            // 调用服务端方法，查询清单数据
            var partsInboundPlanDetails = domainService.GetPartsInboundPlanDetailsByIdForLabel(ids.ToArray()).OrderBy(r => r.SparePartId).ToArray();
            var details = new List<PartsInboundPlanDetail>();
            //修改清单记录的数量属性，为客户端传入的值
            foreach(var item in partsInboundPlanDetails) {
                int printNumber = Convert.ToInt32(idss[item.Id]);
                for(int i = 0; i < printNumber; i++) {
                    var planDetail = new PartsInboundPlanDetail();
                    planDetail.SparePartId = item.SparePartId;
                    planDetail.SparePartCode = item.SparePartCode;
                    planDetail.SparePartName = item.SparePartName;
                    planDetail.PlannedAmount = item.PlannedAmount;
                    planDetail.EnglishName = item.SparePart.EnglishName;
                    planDetail.Specification = item.SparePart.Specification;
                    planDetail.NumCode = CommonConverter.GetSolidDateForSupplierPrintLabel(DateTime.Now.Year) + tempCodes[num];
                    details.Add(planDetail);
                    num++;
                }
            }
            //调用生成WMS标签序列号方法
            //domainService.InsertWMSLabelPrintingSerialList2(details);
            // 将构造的结果集绑定到 DetailSection
            report.DataSource = details;
        }
    }
}