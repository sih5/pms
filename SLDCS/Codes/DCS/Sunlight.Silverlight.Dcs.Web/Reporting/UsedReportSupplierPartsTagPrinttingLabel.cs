using System.Collections.Generic;
using System.Linq;

namespace Sunlight.Silverlight.Dcs.Web.Reporting {
    using System;

    /// <summary>
    /// Summary description for ReportSupplierPartsTagPrinttingLabel.
    /// </summary>
    public partial class UsedReportSupplierPartsTagPrinttingLabel : Telerik.Reporting.Report {
        public UsedReportSupplierPartsTagPrinttingLabel() {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }
        private int num = 0;

        private void ReportSupplierPartsTagPrinttingLabel_NeedDataSource(object sender, EventArgs e) {
            var report = (Telerik.Reporting.Processing.Report)sender;
            // 取参数值
            var pararPartsTags = report.Parameters["PararPartsTags"].Value as object[];
            Dictionary<int, int> idss = new Dictionary<int, int>();
            var ids = new List<int>();
            int printNum = 0;
            foreach(var pararPartsTag in pararPartsTags) {
                var aa = pararPartsTag.ToString();
                var ss = aa.Split(',');
                var qq = ss[0];
                var q = ss[1];
                var printnum = ss[2];
                idss.Add(Convert.ToInt32(qq), Convert.ToInt32(q));
                ids.Add(Convert.ToInt32(qq));
                printNum = Convert.ToInt32(printnum);
            }

            int totalnum = 0;
            foreach(var key in idss.Keys) {
                totalnum = totalnum + idss[key];//获取打印配件总数量
            }
            var domainService = new DcsDomainService();
            //根据编码规则获取序列号
            var tempCodes = domainService.GetNumberOfParts(totalnum, "WMSLabelPrintingSerial");

            var spareParts = domainService.GetSparePartByIdForSupplierPartsTagPrinttingLabel(ids.ToArray()).OrderBy(r => r.Id).ToArray();
            var details = new List<SparePart>();
            foreach(var item in spareParts) {
                int printNumber = Convert.ToInt32(idss[item.Id]);
                for(int i = 0; i < printNumber; i++) {
                    var planDetail = new SparePart();
                    planDetail.Id = item.Id;
                    planDetail.Code = item.Code;
                    planDetail.Name = item.Name;
                    planDetail.EnglishName = item.EnglishName;
                    planDetail.Specification = item.Specification;
                    planDetail.PrintNums = printNum;
                    planDetail.NumCode = CommonConverter.GetSolidDateForSupplierPrintLabel(DateTime.Now.Year) + tempCodes[num];
                    details.Add(planDetail);
                    num++;
                }
            }
            //调用生成WMS标签序列号方法
            //domainService.InsertWMSLabelPrintingSerialList3(details);
            // 将构造的结果集绑定到 DetailSection
            report.DataSource = details;
        }
        //#region 相关转换暂时隐藏
        //    private string GetNumber(string tempCode) {
        //        var code = tempCode.PadLeft(6, '0');
        //       // var code = tempCode.Substring(tempCode.Length - 7);
        //        switch(num.ToString().Length) {
        //            case 1:
        //                code = "00000" + num.ToString();
        //                break;
        //            case 2:
        //                code = "0000" + num.ToString();
        //                break;
        //            case 3:
        //                code = "000" + num.ToString();
        //                break;
        //            case 4:
        //                code = "00" + num.ToString();
        //                break;
        //            case 5:
        //                code = "0" + num.ToString();
        //                break;
        //            case 6:
        //                code = num.ToString();
        //                break;
        //        }
        //        num++;
        //        return code;
        //    }
        //#endregion
    }
}