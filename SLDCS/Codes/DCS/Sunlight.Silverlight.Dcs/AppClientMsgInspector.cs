﻿using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;
using System.Threading;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs {
    public class AppClientMsgInspector : IClientMessageInspector {
        //在soap或HTTP头文件中加入当前Culture
        public object BeforeSendRequest(ref Message request, IClientChannel channel) {
            //EnvelopeVersion.None 的版本不能加入Soap头文件
            if (request.Headers.MessageVersion.Envelope != EnvelopeVersion.None) {
                var header = MessageHeader.CreateHeader(GlobalVar.CULTURE_INFO_HEAD_LOCAL_NAME, GlobalVar.CULTYRE_INFO_HEADER_NAMESPACE, Thread.CurrentThread.CurrentUICulture.ToString());
                request.Headers.Add(header);
            }
            else {
                var property = request.Properties[HttpRequestMessageProperty.Name] as HttpRequestMessageProperty;
                if (property != null) {
                    property.Headers[GlobalVar.CULTURE_INFO_HEAD_LOCAL_NAME] = Thread.CurrentThread.CurrentUICulture.Name;
                }
            }
            return null;
        }

        public void AfterReceiveReply(ref Message reply, object correlationState) {
        }
    }
}
