﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.ReportViewer.Silverlight;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs {
    public class BasePrintWindow : RadWindow, INotifyPropertyChanged {
        public DcsDomainContext DcsDomainContext;

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public BasePrintWindow() {
            this.SetBinding(DataContextProperty, new Binding {
                RelativeSource = new RelativeSource(RelativeSourceMode.Self)
            });
            this.FontSize = 12;
            this.BasePrintWindow_Loaded();
            this.Closed += (sender, args) => Sunlight.Silverlight.ViewModel.ShellViewModel.Current.IsRibbonMinimized = false;
        }

        private void BasePrintWindow_Loaded() {
            var browserProperties = Application.Current.Host;
            var height = browserProperties.Content.ActualHeight;
            var width = browserProperties.Content.ActualWidth;
            this.Width = 920;
            this.Height = height;
            this.Left = (width / 2) - 455;
        }
    }

    public static class SunlightPrinter {
        private static readonly ReportViewer ReportViewer;
        private static readonly RadWindow ReportWindow = new RadWindow();

        static SunlightPrinter() {
            BasePrintWindowLoaded(ReportWindow);
            var viewer = new ReportViewer();
            viewer.Width = 1000.0;
            viewer.ReportServiceUri = new Uri("../ReportService.svc", UriKind.Relative);
            viewer.ViewMode = ViewMode.PrintPreview;
            ReportViewer = viewer;
            ReportWindow.Content = ReportViewer;
        }

        private static void BasePrintWindowLoaded(RadWindow window) {
            var browserProperties = Application.Current.Host;
            var height = browserProperties.Content.ActualHeight;
            var width = browserProperties.Content.ActualWidth;
            window.Width = 920;
            window.Height = height;
            window.Left = (width / 2) - 455;
        }

        /// <summary>
        /// 展示打印页面，如果<c>useHtml</c>为<c>true</c>，在浏览器中打开新的标签页，同时立即调用<c>windowClosed</c>
        /// <para>
        /// 如果<c>useHtml</c>为<c>false</c>，或者程序在浏览器外运行，则会打开Silverlight中的TelerikReport窗口，窗口关闭后调用<c>windowClosed</c>
        /// </para>
        /// 示例
        /// <code>
        /// SunlightPrinter.ShowPrinter(SalesUIStrings.DataManagementView_Title_ReportsPrintPreviewPage,
        ///                         "ReportRepairContractNew",
        ///                         null,
        ///                         true,
        ///                         new[] { new Tuple&lt;string, string&gt;("paraItemDetailSumPrice", entity.RepairContractItems.Sum(d =&gt; d.WorkingFee).ToString()),
        ///                                    new Tuple&lt;string,string&gt;("paraMaterialDetailSumPrice", entity.RepairContractItems.SelectMany(i =&gt; i.RepairContractMaterials).Sum(d=&gt;d.Price).ToString(CultureInfo.CurrentCulture)),
        ///                                    new Tuple&lt;string,string&gt;("paraRetailContractId", entity.Id.ToString()),
        ///                                    new Tuple&lt;string,string&gt;("paraServiceId", entity.CorporationId.ToString()),
        ///                                    new Tuple&lt;string,string&gt;("paraServiceAdvisorName", entity.ServiceAdvisorName),
        ///                                });
        /// </code>
        /// </summary>
        /// <param name="title">打印界面标题</param>
        /// <param name="reportName">报表名称，如果是trdx报表，是 SampleReport.trdx，如果是C#报表，则是报表类名称</param>
        /// <param name="windowClosed">打印的后续方法</param>
        /// <param name="useHtml">是否启用html模式</param>
        /// <param name="parameters">报表参数</param>
        public static void ShowPrinter(string title, string reportName, EventHandler<WindowClosedEventArgs> windowClosed, bool useHtml, params Tuple<string, string>[] parameters) {
            if(useHtml && !Application.Current.IsRunningOutOfBrowser) {
                ShowPrintInHtml(title, reportName, parameters);
            } else {
                ShowPrintInSilverlight(title, reportName, windowClosed, parameters);
            }
        }

        private static void ShowPrintInHtml(string title, string reportName, params Tuple<string, string>[] parameters) {
            var encodePrams = string.Format("report={0}&title={1}", reportName, HttpUtility.UrlEncode(title));
            if(parameters.Any())
                encodePrams += "&" + string.Join("&", from p in parameters
                                                      select string.Format("{0}={1}", p.Item1, HttpUtility.UrlEncode(p.Item2)));
            var base64Prams = Convert.ToBase64String(Encoding.UTF8.GetBytes(encodePrams));
            HtmlPage.Window.Navigate(Utils.MakeServerUri(string.Format("html5reportviewer.aspx?{0}", base64Prams)), "_blank");
        }

        /// <summary>
        /// 展示打印页面，会打开Silverlight中的TelerikReport窗口，窗口关闭后调用<c>windowClosed</c>
        /// 示例
        /// <code>
        /// SunlightPrinter.ShowPrintInSilverlight(SalesUIStrings.DataManagementView_Title_ReportsPrintPreviewPage,
        ///                         "ReportRepairContractNew",
        ///                         null,
        ///                         new[] { new Tuple&lt;string, string&gt;("paraItemDetailSumPrice", entity.RepairContractItems.Sum(d =&gt; d.WorkingFee).ToString()),
        ///                                    new Tuple&lt;string,string&gt;("paraMaterialDetailSumPrice", entity.RepairContractItems.SelectMany(i =&gt; i.RepairContractMaterials).Sum(d=&gt;d.Price).ToString(CultureInfo.CurrentCulture)),
        ///                                    new Tuple&lt;string,string&gt;("paraRetailContractId", entity.Id.ToString()),
        ///                                    new Tuple&lt;string,string&gt;("paraServiceId", entity.CorporationId.ToString()),
        ///                                    new Tuple&lt;string,string&gt;("paraServiceAdvisorName", entity.ServiceAdvisorName),
        ///                                });
        /// </code>
        /// </summary>
        /// <param name="title">打印界面标题</param>
        /// <param name="reportName">报表名称，如果是trdx报表，是 SampleReport.trdx，如果是C#报表，则是报表类名称</param>
        /// <param name="windowClosed">打印的后续方法</param>
        /// <param name="parameters">报表参数</param>
        public static void ShowPrintInSilverlight(string title, string reportName, EventHandler<WindowClosedEventArgs> windowClosed, params Tuple<string, string>[] parameters) {
            ApplyParametersEventHandler applyParameters = (s, e) => {
                foreach(var pair in parameters)
                    e.ParameterValues[pair.Item1] = pair.Item2;
            };
            ReportWindow.Header = title;
            ReportViewer.ApplyParameters += applyParameters;
            ReportViewer.Report = string.Format("Sunlight.Silverlight.Dcs.Web.Reporting.{0}, Sunlight.Silverlight.Dcs.Web", reportName);
            ReportWindow.Closed += (s, e) => {
                ReportViewer.ApplyParameters -= applyParameters;
                if(windowClosed != null) {
                    windowClosed(s, e);
                }
            };
            ReportWindow.ShowDialog();
        }
    }
}
