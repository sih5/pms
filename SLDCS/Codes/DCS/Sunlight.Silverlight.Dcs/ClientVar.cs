﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using Sunlight.Silverlight.Core.Model;

namespace Sunlight.Silverlight.Dcs {
    public static class ClientVar {
        /// <summary>
        /// 查询时间范围需要保留时间 23:59:59
        /// </summary>
        /// <param name="compositeFilter"></param>
        public static void ConvertTime(CompositeFilterItem compositeFilter) {
            if(compositeFilter.Filters.Any(filter => filter.GetType() == typeof(CompositeFilterItem))) {
                var timeFilter = compositeFilter.Filters.Where(filter => filter.GetType() == typeof(CompositeFilterItem)).ToArray();
                var filterItems = timeFilter as FilterItem[] ?? timeFilter.ToArray();
                if(filterItems.Any()) {
                    foreach(var filterItem in filterItems) {
                        if(((CompositeFilterItem)filterItem).Filters.Any()) {
                            for(var i = 1;i < ((CompositeFilterItem)filterItem).Filters.Count + 1;i++) {
                                switch(i % 2) {
                                    case 0:
                                        var tempEndDataTime = ((CompositeFilterItem)filterItem).Filters.ElementAt(i - 1).Value as DateTime?;
                                        if(tempEndDataTime.HasValue)
                                            ((CompositeFilterItem)filterItem).Filters.ElementAt(i - 1).Value = new DateTime(tempEndDataTime.Value.Year, tempEndDataTime.Value.Month, tempEndDataTime.Value.Day, 23, 59, 59);
                                        break;
                                    case 1:
                                        var tempStartDataTime = ((CompositeFilterItem)filterItem).Filters.ElementAt(i - 1).Value as DateTime?;
                                        if(tempStartDataTime.HasValue)
                                            ((CompositeFilterItem)filterItem).Filters.ElementAt(i - 1).Value = tempStartDataTime.Value.Year.CompareTo(1754) < 0 ? null : ((CompositeFilterItem)filterItem).Filters.ElementAt(i - 1).Value;
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                    }
                }
            }
        }

        public static ObservableCollection<KeyValuePair> GetMonth() {
            var result = new ObservableCollection<KeyValuePair>();
            for(int i = 1;i < 13;i++) {
                result.Add(new KeyValuePair {
                    Key = i,
                    Value = i.ToString()
                });
            }
            return result;
        }

        /// <summary>
        /// 报表用到的日期集合 年-月
        /// </summary>
        public static ObservableCollection<KeyValuePair> GetDateTimeArray(int startYear = 2012, int startMonth = 1) {
            var date = new ObservableCollection<KeyValuePair>();
            var t = DateTime.Now;
            var key = 0;
            for(var i = t.Year;i >= startYear;i--) {
                var j = i == t.Year ? t.Month : 12;
                var minMonth = i == startYear ? startMonth : 1;
                while(j >= minMonth) {
                    var moth = j < 10 ? "0" + j.ToString() : j.ToString();
                    date.Add(new KeyValuePair {
                        Key = key,
                        Value = i + "-" + moth
                    });
                    j--;
                    key++;
                }
            }
            return date;
        }

        /// <summary>
        /// 报表用到的日期集合 年份
        /// </summary>
        public static ObservableCollection<KeyValuePair> GetDateTimeYearArray(int startYear = 2012) {
            var date = new ObservableCollection<KeyValuePair>();
            var t = DateTime.Now;
            var key = 0;
            for(var i = t.Year;i >= startYear;i--) {
                date.Add(new KeyValuePair {
                    Key = key,
                    Value = i.ToString()
                });
                key++;
            }
            return date;
        }

        /// <summary>
        /// 自定义四舍五入
        /// </summary>
        public static decimal Round(decimal number, int decimals) {
            int mask = (int)Math.Pow(10, decimals + 1);
            int round = (int)(number * mask);
            int last = (int)(number * mask) % 10;
            if(number < 0) {
                var ss = -number;
                round = (int)(ss * mask);
                last = (int)(ss * mask) % 10;
            }
            decimal rtv = 0;
            if(last >= 5) {
                rtv = ((decimal)round + 10M - last) / mask;
                if(number < 0) {
                    rtv = -rtv;
                }
            } else {
                rtv = ((decimal)round - last) / mask;
                if(number < 0) {
                    rtv = -rtv;
                }
            }
            return rtv;
        }

        /// <summary>
        /// 客户账户历史记录查询用到的日期集合 年-月
        /// </summary>
        public static ObservableCollection<KeyValuePair> GetDateTimeArrayForTheDate(int startYear = 2015, int startMonth = 1) {
            var date = new ObservableCollection<KeyValuePair>();
            var t = DateTime.Now;
            var key = 0;
            for(var i = t.Year;i >= startYear;i--) {
                var j = i == t.Year ? t.Month : 12;
                var minMonth = i == startYear ? startMonth : 1;
                while(j >= minMonth) {
                    var moth = j < 10 ? "0" + j.ToString() : j.ToString();
                    date.Add(new KeyValuePair {
                        Key = key,
                        Value = i + "-" + moth
                    });
                    j--;
                    key++;
                }
            }
            return date;
        }

        public static string GetSysCode(string sysCode) {
            switch(sysCode) {
                case "1101":
                    return "DCS";
                case "2450":
                case "2470":
                    return "DCS";
                case "2230":
                case "240":
                    return "DCS";
                case "2290":
                case "2601":
                    return "DCS";
            }
            return "";
        }
    }
}
