﻿namespace Sunlight.Silverlight.Dcs {
    public static class CommonActionKeys {
        public const string ADD = "Add";
        public const string EDIT = "Edit";
        public const string ABANDON = "Abandon";
        public const string APPROVE = "Approve";
        public const string BATCHAPPROVAL = "BatchApproval";
        public const string AUDIT = "Audit";
        public const string EXPORT = "Export";
        public const string IMPORT = "Import";
        public const string BATCH_IMPORT = "BatchImport";
        public const string EXPORT_ALL = "ExportAll";
        public const string PRINT = "Print";
        public const string GCPRINT = "GCPrint";
        public const string FDPRINT = "FDPrint";
        public const string PRINTFORGC = "PrintForGC";
        public const string REVOCATION = "Revocation";
        public const string REPLY = "Reply";
        public const string UPLOAD = "Upload";
        public const string VIEW = "View";
        public const string REPLACE_REPAIR_CLASSIFY = "ReplaceRepairClassify";

        //手工设置状态
        public const string SET_STATUS = "SetStatus";
        // 停用
        public const string PAUSE = "Pause";
        //重新开始
        public const string RESUME = "Resume";
        //导入修改
        public const string IMPORTUPDATE = "ImportUpdate";
        //驳回
        public const string REJECT = "Reject";
        //结算成本查询
        public const string SETTLEMENTCOSTQUERY = "SettlementCostQuery";
        // 终止
        public const string TERMINATE = "Terminate";
        public const string CONFIRM = "Confirm";
        public const string SUBMIT = "Submit";
        public const string DETAIL = "Detail";
        public const string DELETE = "Delete";
        public const string DOWNLOAD = "Download";
        // 汇总
        public const string COLLECT = "Collect";
        // 生效
        public const string TAKE_EFFECT = "TakeEffect";
        //修改主单
        public const string MAINEDIT = "MainEdit";
        // 代审批
        public const string INSTEAD_APPROVE = "InsteadApprove";
        // 撤单
        public const string REVOKE = "Revoke";
        public const string MERGEEXPORT = "MergeExport";
        //定时导出
        public const string SCHEDULEREEXPORT = "SchedulerExport";
        //导出清单
        public const string MVSPicture = "MVSPicture";
        //查看照片
        public const string MVSVideo = "MVSVideo";
        //查看视频
        public const string MVSRoute = "MVSRoute";
        //查看外出轨迹
        public const string APPPicture = "APPPicture";
        //查看APP照片
        public const string APPRoute = "APPRoute";
        //查看APP外出轨迹
        //导出客户信息
        public const string EXPORTCUSTOMERINFO = "ExportCustomerInfo";
        //高级审批
        public const string SENIORAPPROVE = "SeniorApprove";
        //初审
        public const string INITIALAPPROVE = "InitialApprove";
        //终审
        public const string FINALAPPROVE = "FinalApprove";
        //高级审核
        public const string ADVANCEDAUDIT = "AdvancedAudit";
        public const string ABANDON_TERMINATE_EXPORT = ABANDON + TERMINATE + EXPORT;
        public const string ABANDON_TERMINATE_EXPORT_DETAIL = ABANDON + TERMINATE + EXPORT + DETAIL;
        public const string ADD_EDIT_ABANDON_IMPORT_EXPORT = ADD + EDIT + ABANDON + IMPORT + EXPORT;
        public const string AUDIT_TERMINATE = AUDIT + TERMINATE;
        public const string APPROVE_TERMINATE_EXPORT_MERGEEXPORT = APPROVE + TERMINATE + EXPORT + MERGEEXPORT;
        public const string DETAIL_MVS_PICTURE_MVS_VIDEO_MVS_ROUTE_EXPORT = DETAIL + MVSPicture + MVSVideo + MVSRoute + MERGEEXPORT + EXPORT;
        public const string DETAIL_MERGEEXPORT_EXPORT = DETAIL + MERGEEXPORT + EXPORT;
        public const string ADD_EDIT_ABANDON_APPROVE_BATCHAPPROVAL_DETAIL_EXPORT_EXPORTDETAIL = ADD + EDIT + ABANDON + APPROVE + BATCHAPPROVAL + DETAIL + EXPORT + EXPORTDETAIL;
        public const string ADD_EDIT_SUBMIT_ABANDON_EXPORT_DETAIL = ADD + EDIT + SUBMIT + ABANDON + EXPORT + DETAIL;
        public const string SCHEDULEREEXPORT_EXPORT = SCHEDULEREEXPORT + EXPORT;
        public const string EXPORTDETAIL = "ExportDetail";
        public const string EXPORT_MERGEEXPORT_PRINTFORGC = EXPORT + MERGEEXPORT + PRINTFORGC;
        public const string APPROVE_BATCHAPPROVAL_REJECT_ABANDON_TERMINATE_MERGEEXPORT = APPROVE + BATCHAPPROVAL + REJECT + ABANDON + TERMINATE + MERGEEXPORT;
        public const string APPROVE_SENIORAPPROVE_BATCHAPPROVAL_REJECT_ABANDON_TERMINATE_MERGEEXPORT = APPROVE + SENIORAPPROVE + BATCHAPPROVAL + REJECT + ABANDON + TERMINATE + MERGEEXPORT;
        public const string DETAIL_EXPORT_EXPORTDETAIL = DETAIL + EXPORT + EXPORTDETAIL;
        public const string APPROVE_REJECT_ABANDON_TERMINATE_MERGEEXPORT = APPROVE + REJECT + ABANDON + TERMINATE + MERGEEXPORT;
        public const string APPROVE_ABANDON_TERMINATE_MERGEEXPORT = APPROVE + ABANDON + TERMINATE + MERGEEXPORT;
        public const string EDIT_DETAIL = EDIT + DETAIL;
        public const string ADD_EDIT_ABANDON_SUBMIT_TERMINATE_EXPORT_PRINT_IMPORT_MERGEEXPORT = ADD + EDIT + ABANDON + SUBMIT + TERMINATE + EXPORT + PRINT + IMPORT + MERGEEXPORT;
        public const string ABANDON_EXPORT = ABANDON + EXPORT;
        public const string ADD_EDIT_SUBMIT_REVOKE_ABANDON_MERGEEXPORT = ADD + EDIT + SUBMIT + REVOKE + ABANDON + MERGEEXPORT;
        public const string EDIT_EXPORT = EDIT + EXPORT;
        public const string ADD_EDIT_EXPORT = ADD + EDIT + EXPORT;
        public const string ADD_EDIT = ADD + EDIT;
        public const string ADD_EDIT_DELETE_DETAIL = ADD + EDIT + DELETE + DETAIL;
        public const string ADD_EDIT_IMPORT_EXPORT = ADD + EDIT + IMPORT + EXPORT;
        public const string ADD_EDIT_DELETE_EXPORT = ADD + EDIT + DELETE + EXPORT;
        public const string ADD_EDIT_DELETE_DETAIL_EXPORT = ADD + EDIT + DELETE + DETAIL + EXPORT;
        public const string ADD_EDIT_PAUSE = ADD + EDIT + PAUSE;
        public const string ADD_EDIT_ABANDON = ADD + EDIT + ABANDON;
        public const string ADD_EDIT_MAINEDIT_ABANDON = ADD + EDIT + MAINEDIT + ABANDON;
        public const string ADD_EDIT_ABANDON_EXPORT = ADD + EDIT + ABANDON + EXPORT;
        public const string ADD_EDIT_ABANDON_EXPORT_IMPORT = ADD + EDIT + ABANDON + EXPORT + IMPORT;
        public const string ADD_EDIT_ABANDON_EXPORT_IMPORT_IMPORTUPDATE = ADD + EDIT + ABANDON + EXPORT + IMPORT + IMPORTUPDATE;
        public const string ADD_EDIT_SUBMIT_ABANDON_EXPORT_PRINT = ADD + EDIT + SUBMIT + ABANDON + EXPORT + PRINT;

        public const string ADD_EDIT_ABANDON_APPROVE_EXPORT_IMPORT = ADD + EDIT + ABANDON + APPROVE + EXPORT + IMPORT;
        public const string ADD_EDIT_ABANDON_DETAIL_IMPORT_EXPORT = ADD + EDIT + ABANDON + DETAIL + IMPORT + EXPORT;
        public const string EDIT_ABANDON_PAUSE_RESUME_EXPORT_IMPORT = EDIT + ABANDON + PAUSE + RESUME + EXPORT + IMPORT;
        public const string ADD_EDIT_ABANDON_PAUSE_RESUME_EXPORT_IMPORT = ADD + EDIT + ABANDON + PAUSE + RESUME + EXPORT + IMPORT;
        public const string ADD_EDIT_ABANDON_PAUSE_RESUME_EXPORT = ADD + EDIT + ABANDON + PAUSE + RESUME + EXPORT;
        public const string ADD_EDIT_ABANDON_PAUSE_RESUME_EXPORT_DETAIL = ADD + EDIT + ABANDON + PAUSE + RESUME + EXPORT + DETAIL;

        public const string ADD_EDIT_ABANDON_PAUSE_RESUME_EXPORT_IMPORT_DETAIL = ADD + EDIT + ABANDON + PAUSE + RESUME + EXPORT + DETAIL;
        public const string ADD_EDIT_ABANDON_PAUSE_RESUME = ADD + EDIT + ABANDON + PAUSE + RESUME;
        public const string ADD_EDIT_ABANDON_PAUSE_RESUME_IMPORT = ADD + EDIT + ABANDON + PAUSE + RESUME + IMPORT;
        public const string ADD_EDIT_ABANDON_PAUSE_RESUME_DETAIL = ADD + EDIT + ABANDON + PAUSE + RESUME + DETAIL;
        public const string ADD_EDIT_ABANDON_PAUSE_RESUME_DETAIL_EXPORTDETAIL = ADD + EDIT + ABANDON + PAUSE + RESUME + DETAIL + EXPORTDETAIL;
        public const string ADD_EDIT_APPROVE_EXPORT = ADD + EDIT + APPROVE + EXPORT;
        public const string APPROVE_TERMINATE_PRINT = APPROVE + TERMINATE + PRINT;
        public const string ADD_EDIT_EXPORT_PRINT = ADD + EDIT + EXPORT + PRINT;
        public const string EDIT_ABANDON_EXPORT = EDIT + ABANDON + EXPORT;
        public const string EDIT_ABANDON = EDIT + ABANDON;
        public const string ABANDON_EXPORT_PRINT = ABANDON + EXPORT + PRINT;
        public const string EXPORT_PRINT = EXPORT + PRINT;
        public const string EXPORT_MERGEEXPORT = EXPORT + MERGEEXPORT;
        public const string CONFIRM_EXPORT_PRINT = CONFIRM + EXPORT + PRINT;
        public const string CONFIRM_EXPORT_PRINT_MERGEEXPORT = CONFIRM + EXPORT + PRINT + MERGEEXPORT;
        public const string ADD_EDIT_ABANDON_APPROVE_EXPORT_PRINT_MERGEEXPORT = ADD + EDIT + ABANDON + APPROVE + EXPORT + PRINT + MERGEEXPORT;
        public const string CONFIRM_TERMINATE_EXPORT_PRINT_MERGEEXPORT_EDIT = CONFIRM + TERMINATE + EXPORT + PRINT + MERGEEXPORT + EDIT;
        public const string EDIT_EXPORT_PRINT = EDIT + EXPORT + PRINT;
        public const string ADD_EDIT_ABANDON_EXPORT_PRINT = ADD + EDIT + ABANDON + EXPORT + PRINT;
        public const string ADD_EDIT_ABANDON_APPROVE_PRINT = ADD + EDIT + ABANDON + APPROVE + PRINT;
        public const string APPROVE_EXPORT_PRINT = APPROVE + EXPORT + PRINT;
        public const string ADD_EDIT_ABANDON_APPROVE_IMPORT_PRINT = ADD + EDIT + ABANDON + APPROVE + IMPORT + PRINT;
        public const string ADD_EDIT_ABANDON_APPROVE_IMPORT_PRINT_EXPORT = ADD + EDIT + ABANDON + APPROVE + IMPORT + PRINT + EXPORT;
        public const string ADD_EDIT_ABANDON_APPROVE_EXPORT_PRINT = ADD + EDIT + ABANDON + APPROVE + EXPORT + PRINT;
        public const string ADD_EDIT_ABANDON_APPROVE_EXPORT_MERGEEXPORT_PRINT_GCPRINT = ADD + EDIT + ABANDON + APPROVE + EXPORT + MERGEEXPORT + PRINT + GCPRINT;
        public const string ADD_EDIT_ABANDON_APPROVE_EXPORT_MERGEEXPORT_PRINT = ADD + EDIT + ABANDON + APPROVE + EXPORT + MERGEEXPORT + PRINT;
        public const string ADD_EDIT_ABANDON_APPROVE_MERGEEXPORT_PRINT = ADD + EDIT + ABANDON + APPROVE + MERGEEXPORT + PRINT;
        public const string ADD_EDIT_ABANDON_EXPORT_MERGEEXPORT_PRINT = ADD + EDIT + ABANDON + EXPORT + MERGEEXPORT + PRINT;
        public const string ADD_EDIT_ABANDON_APPROVE_TERMINATE_EXPORT_PRINT = ADD + EDIT + ABANDON + APPROVE + TERMINATE + EXPORT + PRINT;
        public const string ADD_EDIT_ABANDON_APPROVE_TERMINATE_EXPORT = ADD + EDIT + ABANDON + APPROVE + TERMINATE + EXPORT;
        public const string ADD_EDIT_ABANDON_APPROVE_TERMINATE_EXPORT_PRINT_EXPORTDETAIL = ADD + EDIT + ABANDON + APPROVE + TERMINATE + EXPORT + PRINT + EXPORTDETAIL;
        public const string ADD_EDIT_ABANDON_SUBMIT_TERMINATE_EXPORT_PRINT = ADD + EDIT + ABANDON + SUBMIT + TERMINATE + EXPORT + PRINT;
        public const string ADD_EXPORT = ADD + EXPORT;
        public const string ADD_EDIT_ABANDON_APPROVE = ADD + EDIT + ABANDON + APPROVE;
        public const string ADD_EDIT_ABANDON_APPROVE_MERGEEXPORT = ADD + EDIT + ABANDON + APPROVE + MERGEEXPORT;
        public const string ADD_EDIT_ABANDON_SUBMIT = ADD + EDIT + ABANDON + SUBMIT;
        public const string ABANDON_APPROVE = ABANDON + APPROVE;
        public const string ABANDON_APPROVE_DETAIL = ABANDON + APPROVE + DETAIL;
        public const string ADD_EDIT_ABANDON_SUBMIT_DETAIL = ADD + EDIT + ABANDON + SUBMIT + DETAIL;
        public const string ABANDON_DETAIL = ABANDON + DETAIL;
        public const string ABANDON_DETAIL_EXPORT = ABANDON + DETAIL + EXPORT;
        public const string ABANDON_DETAIL_EXPORT_PRINT = ABANDON + DETAIL + EXPORT + PRINT;
        public const string APPROVE_ABANDON_DETAIL = APPROVE + ABANDON + DETAIL;
        public const string APPROVE_ABANDON_REJECT_DETAIL = APPROVE + ABANDON + REJECT + DETAIL;
        public const string ADD_EDIT_ABANDON_APPROVE_DETAIL = ADD + EDIT + ABANDON + APPROVE + DETAIL;
        public const string ADD_EDIT_APPROVE_ABANDON_EXPORT_MERGEEXPORT_IMPORT = ADD + EDIT + APPROVE + ABANDON + EXPORT + MERGEEXPORT + IMPORT;
        public const string ADD_EDIT_APPROVE_BATCHAPPROVAL_ABANDON_EXPORT_MERGEEXPORT = ADD + EDIT + APPROVE + BATCHAPPROVAL + ABANDON + EXPORT + MERGEEXPORT;
        public const string ADD_EDIT_APPROVE_BATCHAPPROVAL_ABANDON_DETAIL_IMPORT_EXPORT = ADD + EDIT + APPROVE + BATCHAPPROVAL + ABANDON + DETAIL + IMPORT + EXPORT;
        public const string ADD_EDIT_APPROVE_BATCHAPPROVAL_ABANDON_DETAIL_IMPORT = ADD + EDIT + APPROVE + BATCHAPPROVAL + ABANDON + DETAIL + IMPORT;
        public const string APPROVE_ABANDON_EXPORT_PRINT = APPROVE + ABANDON + EXPORT + PRINT;
        public const string DETAIL_TERMINATE = DETAIL + TERMINATE;
        public const string DETAIL_TERMINATE_MERGEEXPORT = DETAIL + TERMINATE + MERGEEXPORT;
        public const string ADD_EDIT_SUBMIT_EXPORT_PRINT = ADD + EDIT + SUBMIT + EXPORT + PRINT;
        public const string ADD_EDIT_APPROVE_DETAIL_EXPORT = ADD + EDIT + APPROVE + DETAIL + EXPORT;
        public const string ADD_TAKE_EFFECT_ABANDON = ADD + TAKE_EFFECT + ABANDON;
        public const string APPROVE_PRINT = APPROVE + PRINT;
        public const string ADD_EDIT_INSTEAD_APPROVE_ABANDON_EXPORT_PRINT = ADD + EDIT + INSTEAD_APPROVE + ABANDON + EXPORT + PRINT;
        public const string ADD_EDIT_SUBMIT_REVOKE_ABANDON = ADD + EDIT + SUBMIT + REVOKE + ABANDON;
        public const string APPROVE_ABANDON_TERMINATE = APPROVE + ABANDON + TERMINATE;
        public const string ADD_EDIT_APPROVE = ADD + EDIT + APPROVE;
        public const string ADD_ABANDON = ADD + ABANDON;
        public const string ADD_EDIT_SUBMIT_ABANDON_EXPORT = ADD + EDIT + SUBMIT + ABANDON + EXPORT;
        public const string ADD_EDIT_ABANDON_IMPORT = ADD + EDIT + ABANDON + IMPORT;
        public const string ABANDON_CONFIRM_EXPORT_ALL = ABANDON + CONFIRM + EXPORT_ALL;
        public const string ADD_EDIT_SUBMIT_EXPORT_ALL = ADD + EDIT + SUBMIT + EXPORT_ALL;
        public const string ADD_EDIT_ABANDON_SUBMIT_PRINT = ADD + EDIT + ABANDON + SUBMIT + PRINT;
        public const string ADD_EDIT_ABANDON_SUBMIT_AUDIT_APPROVE_EXPORT = ADD + EDIT + ABANDON + SUBMIT + AUDIT + APPROVE + EXPORT;
        public const string ADD_EDIT_ABANDON_SUBMIT_INITIALAPPROVE_FINALAPPROVE_EXPORT = ADD + EDIT + ABANDON + SUBMIT + INITIALAPPROVE + FINALAPPROVE + EXPORT;
        public const string ADD_EDIT_ABANDON_DETAIL = ADD + EDIT + ABANDON + DETAIL;
        public const string IMPORT_EXPORT = IMPORT + EXPORT;
        public const string ADD_ABANDON_APPROVE_EXPORT = ADD + ABANDON + APPROVE + EXPORT;
        public const string ADD_ABANDON_IMPORT_EXPORT = ADD + ABANDON + IMPORT + EXPORT;
        public const string ADD_APPROVE_ABANDON = ADD + APPROVE + ABANDON;
        public const string ADD_EDIT_CONFIRM = ADD + EDIT + CONFIRM;
        public const string IMPORT_ABANDON = IMPORT + ABANDON;
        public const string ADD_IMPORT = ADD + IMPORT;
        public const string ADD_EDIT_AUDIT_ABANDON = ADD + EDIT + AUDIT + ABANDON;
        public const string EDIT_IMPORT = EDIT + IMPORT;
        public const string ADD_EDIT_CONFIRM_IMPORT = ADD + EDIT + CONFIRM + IMPORT;
        public const string ADD_EDIT_IMPORT = ADD + EDIT + IMPORT;
        public const string APPROVE_REVOCATION_ABANDON = APPROVE + REVOCATION + ABANDON;
        public const string ADD_EDIT_SUBMIT_ABANDON_APPROVE_EXPORT = ADD + EDIT + SUBMIT + ABANDON + APPROVE + EXPORT;
        public const string APPROVE_ABANDON_EXPORT = APPROVE + ABANDON + EXPORT;
        public const string INITIALAPPROVE_APPROVE_FINALAPPROVE_ABANDON_EXPORT_REJECT_MERGEEXPORT_PRINT = INITIALAPPROVE + APPROVE + FINALAPPROVE + ABANDON + EXPORT + REJECT + MERGEEXPORT + PRINT;
        public const string INITIALAPPROVE_APPROVE_AUDIT_ABANDON_EXPORT_REJECT_MERGEEXPORT_PRINT = INITIALAPPROVE + APPROVE + AUDIT + ABANDON + EXPORT + REJECT + MERGEEXPORT + PRINT;
        public const string INITIALAPPROVE_APPROVE_AUDIT_ABANDON_EXPORT_REJECT_MERGEEXPORT_PRINT_COMFIRM_ADVANCEDAUDIT_SENIORAPPROVE = INITIALAPPROVE + APPROVE + AUDIT + ABANDON + EXPORT + REJECT + MERGEEXPORT + PRINT + CONFIRM + ADVANCEDAUDIT+SENIORAPPROVE;
        public const string INITIALAPPROVE_APPROVE_AUDIT_SENIORAPPROVE_ABANDON_EXPORT_REJECT_MERGEEXPORT_PRINT = INITIALAPPROVE + APPROVE + AUDIT+SENIORAPPROVE + ABANDON + EXPORT + REJECT + MERGEEXPORT + PRINT;
        public const string ADD_EDIT_ABANDON_APPROVE_EXPORT = ADD + EDIT + ABANDON + APPROVE + EXPORT;
        public const string ADD_EDIT_ABANDON_APPROVE_EXPORT_EXPORTDETAIL = ADD + EDIT + ABANDON + APPROVE + EXPORT + EXPORTDETAIL;
        public const string APPROVE_ABANDON_IMPORT_EXPORT = APPROVE + ABANDON + IMPORT + EXPORT;
        public const string ADD_EDIT_SUBMIT_DETAIL_ABANDON_APPROVE = ADD + EDIT + SUBMIT + DETAIL + ABANDON + APPROVE;
        public const string EDIT_ABANDON_SUBMIT_DETAIL_EXPORT = EDIT + ABANDON + SUBMIT + DETAIL + EXPORT;
        public const string ADD_EDIT_ABANDON_SUBMIT_DETAIL_EXPORT = ADD + EDIT + ABANDON + SUBMIT + DETAIL + EXPORT;
        public const string AUDIT_EXPORT = AUDIT + EXPORT;
        public const string ADD_EDIT_APPROVE_DETAIL_MERGEEXPORT = ADD + EDIT + APPROVE + DETAIL + MERGEEXPORT;
        public const string ADD_ABANDON_DETAIL = ADD + ABANDON + DETAIL;
        public const string ABANDON_APPROVE_MERGEEXPORT = ABANDON + APPROVE + MERGEEXPORT;
        public const string APPROVE_MERGEEXPORT_ABANDON = APPROVE + MERGEEXPORT + ABANDON;
        public const string APPROVE_MERGEEXPORT_REJECT_ABANDON = APPROVE + MERGEEXPORT + REJECT + ABANDON;
        public const string APPROVE_MERGEEXPORT_REJECT_TERMINATE_ABANDON = APPROVE + MERGEEXPORT + REJECT + TERMINATE + ABANDON;
        public const string INITIALAPPROVE_MERGEEXPORT_REJECT_TERMINATE_ABANDON = INITIALAPPROVE + MERGEEXPORT + REJECT + TERMINATE + ABANDON;
        public const string CONFIRM_TERMINATE_PRINT_MERGEEXPORT = CONFIRM + TERMINATE + PRINT + MERGEEXPORT;
        public const string APPROVE_TERMINATE_PRINT_MERGEEXPORT = APPROVE + TERMINATE + PRINT + MERGEEXPORT;
        public const string ADD_EDIT_ABANDON_APPROVE_DETAIL_EXPORT_MERGEEXPORT_IMPORT = ADD + EDIT + ABANDON + APPROVE + DETAIL + EXPORT + MERGEEXPORT + IMPORT;
        public const string ADD_EDIT_ABANDON_APPROVE_BATCHAPPROVAL_DETAIL_EXPORT_MERGEEXPORT_IMPORT = ADD + EDIT + ABANDON + APPROVE + BATCHAPPROVAL + DETAIL + EXPORT + MERGEEXPORT + IMPORT;
        public const string ADD_EDIT_ABANDON_APPROVE_BATCHAPPROVAL_DETAIL_EXPORT_MERGEEXPORT = ADD + EDIT + ABANDON + APPROVE + BATCHAPPROVAL + DETAIL + EXPORT + MERGEEXPORT;
        public const string SUBMIT_ABANDON_DETAIL = SUBMIT + ABANDON + DETAIL;
        public const string SUBMIT_ABANDON_DETAIL_EXPORT_PRINT = SUBMIT + ABANDON + DETAIL + EXPORT + PRINT;
        public const string SUBMIT_ABANDON_DETAIL_EXPORT = SUBMIT + ABANDON + DETAIL + EXPORT;
        public const string ADD_EDIT_DETAIL = ADD + EDIT + DETAIL;
        public const string EXPORT_EXPORTDETAIL_PRINT = EXPORT + EXPORTDETAIL + PRINT;
        public const string ADD_EDIT_ABANDON_APPROVE_BATCHAPPROVAL_DETAIL_EXPORT_EXPORTDETAIL_PRINT = ADD + EDIT + ABANDON + APPROVE + BATCHAPPROVAL + DETAIL + EXPORT + EXPORTDETAIL + PRINT;
        public const string ADD_EDIT_ABANDON_SUBMIT_MERGEEXPORT = ADD + EDIT + ABANDON + SUBMIT + MERGEEXPORT;
        public const string APPROVE_BATCHAPPROVAL_ABANDON_TERMINATE_MERGEEXPORT = APPROVE + BATCHAPPROVAL + ABANDON + TERMINATE + MERGEEXPORT;
        public const string ADD_EDIT_ABANDON_APPROVE_BATCHAPPROVAL_IMPORT_EXPORT = ADD + EDIT + ABANDON + APPROVE + BATCHAPPROVAL + IMPORT + EXPORT;
        public const string DETAIL_EXPORT_MVSPicture_MVSVideo_MVSRoute = DETAIL + EXPORT + MVSPicture + MVSVideo + MVSRoute;
        public const string EXPORT_MERGEEXPORT_PRINT = EXPORT + MERGEEXPORT + PRINT;
        public const string EXPORT_MERGEEXPORT_PRINT_FDPrint = EXPORT + MERGEEXPORT + PRINT + FDPRINT;
        public const string EXPORT_MERGEEXPORT_PRINT_FDPrint_EXPORTCUSTOMERINFO = EXPORT + MERGEEXPORT + PRINT + FDPRINT + EXPORTCUSTOMERINFO;
        public const string ADD_ABANDON_APPROVE_EXPORT_MERGEEXPORT = ADD + ABANDON + APPROVE + EXPORT + MERGEEXPORT;
        public const string ADD_ABANDON_APPROVE_BATCHAPPROVAL_EXPORT_MERGEEXPORT = ADD + ABANDON + APPROVE + BATCHAPPROVAL + EXPORT + MERGEEXPORT;
        public const string APPROVE_ABANDON_EXPORT_PRINT_REJECT = APPROVE + ABANDON + EXPORT + PRINT + REJECT;
        public const string ADD_EDIT_ABANDON_SUBMIT_TERMINATE_EXPORT_IMPORT_MERGEEXPORT = ADD + EDIT + ABANDON + SUBMIT + TERMINATE + EXPORT + IMPORT + MERGEEXPORT;
        public const string ADD_EDIT_SUBMIT_ABANDON_MERGEEXPORT = ADD + EDIT + SUBMIT + ABANDON + MERGEEXPORT;
        public const string ADD_EDIT_ABANDON_APPROVE_EXPORT_BATCHAPPROVAL = ADD + EDIT + ABANDON + APPROVE + EXPORT + BATCHAPPROVAL;
        public const string ADD_EDIT_ABANDON_APPROVE_BATCHAPPROVAL_EXPORT_IMPORT = ADD + EDIT + ABANDON + APPROVE + BATCHAPPROVAL + EXPORT + IMPORT;
        public const string ADD_EDIT_ABANDON_SUBMIT_MERGEEXPORT_IMPORT = ADD + EDIT + ABANDON + SUBMIT + MERGEEXPORT + IMPORT;
        public const string ADD_EDIT_ABANDON_SUBMIT_APPROVE_EXPORT = ADD + EDIT + ABANDON + SUBMIT + APPROVE + EXPORT;
        public const string EXPORT_MERGEEXPORT_PRINT_TERMINATE = EXPORT + MERGEEXPORT + PRINT + TERMINATE;
        public const string ADD_EDIT_ABANDON_APPROVE_INITIALAPPROVE_EXPORT_PRINT_MERGEEXPORT = ADD + EDIT + ABANDON + APPROVE + INITIALAPPROVE + EXPORT + PRINT + MERGEEXPORT;
        public const string ADD_EDIT_ABANDON_APPROVE_AUDIT_EXPORT_PRINT_MERGEEXPORT = ADD + EDIT + ABANDON + APPROVE + AUDIT + EXPORT + PRINT + MERGEEXPORT;
        public const string ADD_EDIT_ABANDON_APPROVE_FINALAPPROVE_EXPORT_IMPORT = ADD + EDIT + ABANDON + APPROVE + FINALAPPROVE + EXPORT + IMPORT;
        public const string ADD_EDIT_ABANDON_INITIALAPPROVE_APPROVE_MERGEEXPORT = ADD + EDIT + ABANDON + INITIALAPPROVE + APPROVE + MERGEEXPORT;
        public const string ADD_EDIT_ABANDON_INITIALAPPROVE_APPROVE_EXPORT_IMPORT = ADD + EDIT + ABANDON + INITIALAPPROVE + APPROVE + EXPORT + IMPORT;
        public const string ADD_EDIT_ABANDON_AUDIT_APPROVE_EXPORT_IMPORT_SUBMIT = ADD + EDIT + ABANDON + AUDIT + APPROVE + EXPORT + IMPORT + SUBMIT;
        public const string ABANDON_APPROVE_INITIALAPPROVE_FINALAPPROVE = ABANDON + APPROVE + INITIALAPPROVE + FINALAPPROVE;
        public const string ADD_EDIT_ABANDON_SUBMIT_APPROVE_EXPORT_EXPORTDETAIL = ADD + EDIT + ABANDON + SUBMIT + APPROVE + EXPORT + EXPORTDETAIL;
        public const string MAINEDIT_APPROVE_SENIORAPPROVE_ABANDON_TERMINATE_MERGEEXPORT = MAINEDIT + APPROVE + SENIORAPPROVE + ABANDON + TERMINATE + MERGEEXPORT;
        public const string ADD_EDIT_ABANDON_AUDIT_APPROVE_EXPORT_IMPORT = ADD + EDIT + ABANDON + AUDIT + APPROVE + EXPORT + IMPORT;
        public const string ADD_EDIT_ABANDON_SUBMIT_EXPORT_MERGEEXPORT_PRINT = ADD + EDIT + ABANDON + SUBMIT + EXPORT + MERGEEXPORT + PRINT;
        public const string ADD_EDIT_ABANDON_INITIALAPPROVE_AUDIT_APPROVE_EXPORT_IMPORT_SUBMIT = ADD + EDIT + ABANDON + INITIALAPPROVE+ AUDIT + APPROVE + EXPORT + IMPORT + SUBMIT;
        public const string ADD_EDIT_SUBMIT_ABANDON_INITIALAPPROVE_AUDIT_FINALAPPROVE_SENIORAPPROVE_MERGEEXPORT_IMPORT = ADD + EDIT + SUBMIT + ABANDON + INITIALAPPROVE + AUDIT + FINALAPPROVE + SENIORAPPROVE + MERGEEXPORT + IMPORT;
        public const string ADD_EDIT_SUBMIT_ABANDON_AUDIT_APPROVE = ADD + EDIT + SUBMIT + ABANDON + AUDIT + APPROVE;
        public const string ADD_EDIT_SUBMIT_ABANDON_INITIALAPPROVE_AUDIT_APPROVE_MERGEEXPORT = ADD + EDIT + SUBMIT + ABANDON + INITIALAPPROVE + AUDIT + APPROVE + MERGEEXPORT;
    }
}
