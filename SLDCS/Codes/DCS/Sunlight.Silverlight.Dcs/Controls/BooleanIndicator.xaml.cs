﻿using System;
using System.Windows;
using System.Windows.Media.Imaging;

namespace Sunlight.Silverlight.Dcs.Controls {
    public partial class BooleanIndicator {
        private static readonly Uri UriTrue = new Uri("/Sunlight.Silverlight.Dcs;component/Images/bool-true.png", UriKind.Relative);
        private static readonly Uri UriFalse = new Uri("/Sunlight.Silverlight.Dcs;component/Images/bool-false.png", UriKind.Relative);
        private static readonly Uri UriNull = new Uri("/Sunlight.Silverlight.Dcs;component/Images/bool-null.png", UriKind.Relative);
        public static readonly DependencyProperty ValueProperty = DependencyProperty.Register("Value", typeof(bool?), typeof(BooleanIndicator), new PropertyMetadata((o, args) => {
            var indicator = (BooleanIndicator)o;
            var value = (bool?)args.NewValue;
            if(value == null)
                indicator.MainImage.Source = new BitmapImage(UriNull);
            else if(value.Value)
                indicator.MainImage.Source = new BitmapImage(UriTrue);
            else
                indicator.MainImage.Source = new BitmapImage(UriFalse);
        }));

        public bool? Value {
            get {
                return (bool?)this.GetValue(ValueProperty);
            }
            set {
                this.SetValue(ValueProperty, value);
            }
        }

        public BooleanIndicator() {
            this.InitializeComponent();
            this.MainImage.Source = new BitmapImage(UriNull);
        }
    }
}
