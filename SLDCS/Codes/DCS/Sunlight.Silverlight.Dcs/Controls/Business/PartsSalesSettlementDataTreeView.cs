﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Controls.Business {
    public class PartsSalesSettlementDataTreeView : DcsDataTreeView {
        protected override void PopulateItemsHandler(IEnumerable<object> entities, int? id, ICollection<object> treeItems, Action<RadTreeViewItem> additionalHandler) {
            var dbRegion = entities.Cast<Region>();
            if(dbRegion == null)
                return;
            foreach(var region in dbRegion.Where(e => e.Type == (int)DcsRegionType.省)) {
                var item = new RadTreeViewItem {
                    Header = region.Name,
                    Tag = region
                };
                treeItems.Add(item);
                item.Items.Add(new RadTreeViewItem());
                if(additionalHandler != null)
                    additionalHandler(item);
            }
        }

        protected override int? TreeViewRootItemIdentity() {
            return null;
        }

        protected override void InitializeControl() {
            base.InitializeControl();
            this.IsLineEnable = true;
        }

        protected override RadTreeViewItem VisiableParentTreeItemHander(RadTreeViewItem parentItem, ObservableCollection<RadTreeViewItem> treeItemCollection) {
            if(parentItem.Visibility == Visibility.Collapsed)
                parentItem.Visibility = Visibility.Visible;
            var currentParentItem = treeItemCollection.SingleOrDefault(v => ((Region)v.Tag).Id == ((Region)parentItem.Tag).ParentId);
            return currentParentItem;
        }
    }
}
