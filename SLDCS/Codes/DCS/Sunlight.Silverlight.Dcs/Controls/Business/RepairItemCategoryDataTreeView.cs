﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Controls.Business {
    public class RepairItemCategoryDataTreeView : DcsDataTreeView {
        protected override void PopulateItemsHandler(IEnumerable<object> entities, int? id, ICollection<object> treeItems, System.Action<RadTreeViewItem> additionalHandler) {
            //foreach(var repairItemCategory in entities.Cast<RepairItemCategory>().Where(d => d.ParentId == id)) {
            //    var item = new RadTreeViewItem {
            //        Header = repairItemCategory.Name,
            //        Tag = repairItemCategory
            //    };
            //    ToolTipService.SetToolTip(item, repairItemCategory.Code);
            //    item.Items.Add(new RadTreeViewItem());
            //    treeItems.Add(item);

            //    if(additionalHandler != null)
            //        additionalHandler(item);
            //    this.PopulateItems(entities, repairItemCategory.Id, item.Items, additionalHandler);
            //}
        }

        protected override void InitializeControl() {
            base.InitializeControl();
            this.IsLineEnable = true;
        }

        protected override int? TreeViewRootItemIdentity() {
            return null;
        }

        public override string WatermarkText {
            get {
                return DcsUIStrings.TreeView_WatermarkText_RepairItemCategory_Code;
            }
        }

        protected override RadTreeViewItem VisiableParentTreeItemHander(RadTreeViewItem parentItem, ObservableCollection<RadTreeViewItem> treeItemCollection) {
            if(parentItem.Visibility == Visibility.Collapsed)
                parentItem.Visibility = Visibility.Visible;
            //var currentParentItem = treeItemCollection.SingleOrDefault(v => ((RepairItemCategory)v.Tag).Id == ((RepairItemCategory)parentItem.Tag).ParentId);
            //return currentParentItem;
            return null;
        }
    }
}
