﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Controls.Business {
    public class UsedPartsWarehouseAreaDataTreeView : WarehouseAreaDataTreeView {

        protected override void PopulateItemsHandler(IEnumerable<object> entities, int? id, ICollection<object> treeItems, System.Action<RadTreeViewItem> additionalHandler) {
            foreach(var warehouseArea in entities.Cast<UsedPartsWarehouseArea>().Where(d => d.ParentId == id)) {
                var item = new RadTreeViewItem {
                    Header = warehouseArea.Code,
                    Tag = warehouseArea
                };
                treeItems.Add(item);
                if(additionalHandler != null)
                    additionalHandler(item);
                this.PopulateItems(entities, warehouseArea.Id, item.Items, additionalHandler);
            }
        }

        protected override void InitializeControl() {
            base.InitializeControl();
            this.EnableSearchTextBox();
            this.IsLineEnable = true;
        }

        public override string WatermarkText {
            get {
                return DcsUIStrings.TreeView_WatermarkText_UsedPartsWarehouseArea_Code;
            }
        }
        protected override RadTreeViewItem VisiableParentTreeItemHander(RadTreeViewItem parentItem, System.Collections.ObjectModel.ObservableCollection<RadTreeViewItem> treeItemCollection) {
            if(parentItem.Visibility == Visibility.Collapsed)
                parentItem.Visibility = Visibility.Visible;
            var currentParentItem = treeItemCollection.SingleOrDefault(v => ((UsedPartsWarehouseArea)v.Tag).Id == ((UsedPartsWarehouseArea)parentItem.Tag).ParentId);
            return currentParentItem;
        }
    }
}
