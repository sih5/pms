﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Controls.Business {
    public class WarehouseAreaDataTreeView : DcsDataTreeView {

        private WarehouseArea currentSelectedItem;

        public WarehouseArea CurrentSelectedItem {
            get {
                return this.currentSelectedItem;
            }
            set {
                this.currentSelectedItem = value;
                this.OnPropertyChanged("CurrentSelectedItem");
            }
        }

        private void WarehouseAreaDataTreeView_OnTreeViewDataLoaded() {
            var firstNode = this.GetFirstTreeViewItem();
            if(firstNode != null && firstNode.Tag is WarehouseArea) {
                firstNode.IsSelected = true;
                this.CurrentSelectedItem = (firstNode.Tag as WarehouseArea);
            }
        }

        public WarehouseAreaDataTreeView() {
            this.OnTreeViewDataLoaded += this.WarehouseAreaDataTreeView_OnTreeViewDataLoaded;

        }

        protected override void PopulateItemsHandler(IEnumerable<object> entities, int? id, ICollection<object> treeItems, System.Action<RadTreeViewItem> additionalHandler) {
            foreach(var warehouseArea in entities.Cast<WarehouseArea>().Where(d => d.ParentId == id)) {
                var item = new RadTreeViewItem {
                    Header = warehouseArea.Code,
                    Tag = warehouseArea
                };
                item.Items.Add(new RadTreeViewItem());
                treeItems.Add(item);
                if(additionalHandler != null)
                    additionalHandler(item);
                this.PopulateItems(entities, warehouseArea.Id, item.Items, additionalHandler);
            }
        }

        protected override void InitializeControl() {
            base.InitializeControl();
            this.EnableSearchTextBox();
            this.IsLineEnable = true;
        }

        protected override int? TreeViewRootItemIdentity() {
            return null;
        }

        public override string WatermarkText {
            get {
                return DcsUIStrings.TreeView_WatermarkText_WarehouseArea_Code;
            }
        }

        protected override RadTreeViewItem VisiableParentTreeItemHander(RadTreeViewItem parentItem, System.Collections.ObjectModel.ObservableCollection<RadTreeViewItem> treeItemCollection) {
            if(parentItem.Visibility == Visibility.Collapsed)
                parentItem.Visibility = Visibility.Visible;
            var currentParentItem = treeItemCollection.SingleOrDefault(v => ((WarehouseArea)v.Tag).Id == ((WarehouseArea)parentItem.Tag).ParentId);
            return currentParentItem;
        }
    }
}
