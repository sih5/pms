﻿using System.Collections.Specialized;
using System.Windows.Data;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Controls {
    public class DcsComboBox : RadComboBox {
        private bool resettingSelectedValueBindingExpression;
        private BindingExpression selectedValueBindingExpression;

        /// <summary>
        ///     Resets the <see cref="SelectedValue" /> property binding expression to it's original value in order to trigger a refresh of the selected item.
        /// </summary>
        private void ResetSelectedValueBinding() {
            if(this.selectedValueBindingExpression == null || this.resettingSelectedValueBindingExpression)
                return;

            this.resettingSelectedValueBindingExpression = true;
            this.SetBinding(SelectedValueProperty, this.selectedValueBindingExpression.ParentBinding);
            this.resettingSelectedValueBindingExpression = false;
        }

        /// <summary>
        ///     Fixed as per workaround listed here https://connect.microsoft.com/VisualStudio/feedback/details/523394/silverlight-forum-combobox-selecteditem-binding
        /// </summary>
        /// <param name="e"> </param>
        protected override void OnItemsChanged(NotifyCollectionChangedEventArgs e) {
            this.selectedValueBindingExpression = this.GetBindingExpression(SelectedValueProperty);
            base.OnItemsChanged(e);
            this.ResetSelectedValueBinding();
        }

        public DcsComboBox() {
            this.SelectionChanged += (sender, args) => this.ResetSelectedValueBinding();
        }
    }
}
