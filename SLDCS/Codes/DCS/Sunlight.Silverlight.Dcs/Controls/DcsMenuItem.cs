﻿using System;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Controls {
    public class DcsMenuItem : RadMenuItem {
        public virtual Func<bool> CheckMenuVisiable {
            get;
            set;
        }
    }
}
