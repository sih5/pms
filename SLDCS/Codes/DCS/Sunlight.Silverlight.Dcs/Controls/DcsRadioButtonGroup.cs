﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core.Model;

namespace Sunlight.Silverlight.Dcs.Controls {
    public class DcsRadioButtonGroup : StackPanel {
        public static readonly DependencyProperty ItemsSourceProperty;
        public static readonly DependencyProperty SelectedValueProperty;
        public static readonly DependencyProperty GroupNameProperty;
        public static readonly DependencyProperty InnerMarginProperty;
        public static readonly DependencyProperty CheckedCommandProperty;

        private readonly IDictionary<object, RadioButton> radioButtons = new Dictionary<object, RadioButton>();

        static DcsRadioButtonGroup() {
            ItemsSourceProperty = DependencyProperty.Register("ItemsSource", typeof(ObservableCollection<KeyValuePair>), typeof(DcsRadioButtonGroup),
                new PropertyMetadata(null, OnItemsSourcePropertyChanged));
            SelectedValueProperty = DependencyProperty.Register("SelectedValue", typeof(object), typeof(DcsRadioButtonGroup),
                new PropertyMetadata(null, OnSelectedValuePropertyChanged));
            GroupNameProperty = DependencyProperty.Register("GroupName", typeof(string), typeof(DcsRadioButtonGroup),
                new PropertyMetadata(null));
            InnerMarginProperty = DependencyProperty.Register("InnerMargin", typeof(Thickness), typeof(DcsRadioButtonGroup),
                new PropertyMetadata(null));
            CheckedCommandProperty = DependencyProperty.Register("CheckedCommand", typeof(ICommand), typeof(DcsRadioButtonGroup),
               new PropertyMetadata(null));
        }

        private static void OnItemsSourcePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) {
            var dcsRadioButtonGroup = d as DcsRadioButtonGroup;
            if(dcsRadioButtonGroup != null)
                dcsRadioButtonGroup.OnItemsSourcePropertyChanged(e);
        }

        private void OnItemsSourcePropertyChanged(DependencyPropertyChangedEventArgs e) {
            if(e.NewValue is ObservableCollection<KeyValuePair>) {
                this.ItemsSource = e.NewValue as ObservableCollection<KeyValuePair>;
                if(this.ItemsSource == null)
                    return;
                if(string.IsNullOrWhiteSpace(this.GroupName))
                    this.GroupName = null;
                this.ItemsSource.CollectionChanged -= this.ItemsSourceCollectionChanged;
                this.ItemsSource.CollectionChanged += this.ItemsSourceCollectionChanged;
                this.radioButtons.Clear();
                this.Children.Clear();
                if(this.ItemsSource.Any())
                    this.CreateRadioButtonGroup(this.ItemsSource);
            }
        }

        private static void OnSelectedValuePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) {
            var dcsRadioButtonGroup = d as DcsRadioButtonGroup;
            if(dcsRadioButtonGroup != null)
                dcsRadioButtonGroup.OnSelectedValuePropertyChanged(e);
        }

        private void OnSelectedValuePropertyChanged(DependencyPropertyChangedEventArgs e) {
            if(e.NewValue == null)
                return;
            this.SelectedValue = e.NewValue;
            if(this.radioButtons.ContainsKey(e.NewValue))
                this.radioButtons[this.SelectedValue].IsChecked = true;
            else
                foreach(var radio in this.radioButtons.Values)
                    radio.IsChecked = false;
        }

        private void ItemsSourceCollectionChanged(object sender, NotifyCollectionChangedEventArgs e) {
            if(e.NewItems != null && e.NewItems.Count > 0)
                this.CreateRadioButtonGroup(e.NewItems);
        }

        private void CreateRadioButtonGroup(IList collection) {
            foreach(var item in collection.Cast<KeyValuePair>()) {
                var itemButton = new RadioButton();
                itemButton.Content = item.Value;
                itemButton.GroupName = this.GroupName;
                itemButton.IsEnabled = !(item.UserObject is bool) || (bool)item.UserObject;
                itemButton.Margin = InnerMargin;
                itemButton.Checked += this.ItemButtonChecked;
                this.radioButtons.Add(item.Key, itemButton);
                this.Children.Add(itemButton);
            }
            this.SetBinding(SelectedValueProperty, this.GetBindingExpression(SelectedValueProperty).ParentBinding);
        }

        private void ItemButtonChecked(object sender, RoutedEventArgs e) {
            var radioButton = sender as RadioButton;
            if(radioButton == null || this.radioButtons.All(button => button.Value.IsChecked.HasValue && !button.Value.IsChecked.Value)) {
                this.SelectedValue = default(int);
                return;
            }
            this.SelectedValue = this.radioButtons.First(button => button.Value.IsChecked.HasValue && button.Value.IsChecked.Value).Key;
            if(this.CheckedCommand != null && this.CheckedCommand.CanExecute(sender))
                this.CheckedCommand.Execute(sender);
        }

        public ObservableCollection<KeyValuePair> ItemsSource {
            get {
                var itemsSource = this.GetValue(ItemsSourceProperty) as ObservableCollection<KeyValuePair>;
                if(itemsSource == null) {
                    itemsSource = new ObservableCollection<KeyValuePair>();
                    itemsSource.CollectionChanged += this.ItemsSourceCollectionChanged;
                    SetValue(ItemsSourceProperty, itemsSource);
                }
                return itemsSource;
            }
            set {
                SetValue(ItemsSourceProperty, value);
            }
        }

        public object SelectedValue {
            get {
                return this.GetValue(SelectedValueProperty);
            }
            set {
                SetValue(SelectedValueProperty, value);
            }
        }

        public string GroupName {
            get {
                return this.GetValue(GroupNameProperty) != null ? this.GetValue(GroupNameProperty).ToString() : "";
            }
            set {
                this.SetValue(GroupNameProperty, string.IsNullOrWhiteSpace(value) ? System.Guid.NewGuid().ToString() : value);
            }
        }

        public Thickness InnerMargin {
            get {
                return this.GetValue(InnerMarginProperty) is Thickness ? (Thickness)this.GetValue(InnerMarginProperty) : new Thickness(0);
            }
            set {
                this.SetValue(InnerMarginProperty, value);
            }
        }

        public ICommand CheckedCommand {
            get {
                return this.GetValue(CheckedCommandProperty) is ICommand ? (ICommand)this.GetValue(CheckedCommandProperty) : null;
            }
            set {
                this.SetValue(CheckedCommandProperty, value);
            }
        }
    }
}
