﻿using System;
using System.Windows;
using Telerik.Windows.Controls;
using WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation;

namespace Sunlight.Silverlight.Dcs.Controls {
    /// <summary>
    /// DMS弹出窗体
    /// 
    /// 在构造时应向其Content属性赋值
    /// 
    /// 在初始化时，Content完全加载后可根据WindowStartupLocation重新定位弹出窗体。
    /// </summary>
    public class DmsPopupWindow : RadWindow {
        private bool relocationByContent;

        private void Content_Loaded(object sender, RoutedEventArgs e) {
            this.relocationByContent = true;
        }

        private void Content_Closed(object sender, PopupContentControlClosedEventArgs e) {
            this.DialogResult = e.DialogResult;
            if(!e.Cancel)
                this.Close();
        }

        private void ReCalcWindowsPos() {
            if(this.relocationByContent) {
                this.relocationByContent = false;
                if(this.WindowStartupLocation == WindowStartupLocation.CenterScreen ||
                    this.WindowStartupLocation == WindowStartupLocation.CenterOwner) {
                    double left = 0, top = 0;
                    if(this.WindowStartupLocation == WindowStartupLocation.CenterScreen) {
                        left = Application.Current.RootVisual.RenderSize.Width / 2 - this.ActualWidth / 2;
                        top = Application.Current.RootVisual.RenderSize.Height / 2 - this.ActualHeight / 2;
                    } else if(this.WindowStartupLocation == WindowStartupLocation.CenterOwner) {
                        left = this.Owner.RenderSize.Width / 2 - this.ActualWidth / 2;
                        top = this.Owner.RenderSize.Height / 2 - this.ActualHeight / 2;
                    }
                    this.Left = Math.Max(0, left);
                    this.Top = Math.Max(0, top);
                }
            }
            this.relocationByContent = true;
        }

        private void DmsPopupWindow_SizeChanged(object sender, SizeChangedEventArgs e) {
            this.ReCalcWindowsPos();
        }

        protected override void OnContentChanged(object oldContent, object newContent) {
            base.OnContentChanged(oldContent, newContent);
            if(newContent.GetType().GetInterface("IPopupContentControl", false) != null) {
                ((IPopupContentControl)newContent).Closed -= this.Content_Closed;
                ((IPopupContentControl)newContent).Closed += this.Content_Closed;
            }
            if(oldContent == null && newContent is FrameworkElement) {
                ((FrameworkElement)newContent).Loaded -= this.Content_Loaded;
                ((FrameworkElement)newContent).Loaded += this.Content_Loaded;
            }
        }

        public DmsPopupWindow() {
            this.Header = string.Empty;
            this.FontSize = 12;
            this.MaxWidth = Application.Current.RootVisual.RenderSize.Width;
            this.MaxHeight = Application.Current.RootVisual.RenderSize.Height;
            this.MinHeight = 300;
            this.MinWidth = 400;
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            this.IsRestricted = true;
            this.SizeChanged += this.DmsPopupWindow_SizeChanged;
            Application.Current.Host.Content.Resized += Content_Resized;
        }

        void Content_Resized(object sender, EventArgs e) {
            this.MaxWidth = Application.Current.RootVisual.RenderSize.Width;
            this.MaxHeight = Application.Current.RootVisual.RenderSize.Height;
            this.ReCalcWindowsPos();
        }

        //public DmsPopupWindow(IPopupContentControl contentControl)
        //    : this() {
        //    this.Content = contentControl;
        //}
    }
}