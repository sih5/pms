﻿using System.ComponentModel;

namespace Sunlight.Silverlight.Dcs.Controls {
    /// <summary>
    /// 弹出窗体内容控件的关闭事件参数
    /// </summary>
    public class PopupContentControlClosedEventArgs : CancelEventArgs {
        public bool DialogResult;

        public PopupContentControlClosedEventArgs(bool dialogResult) {
            this.DialogResult = dialogResult;
        }
    }
}