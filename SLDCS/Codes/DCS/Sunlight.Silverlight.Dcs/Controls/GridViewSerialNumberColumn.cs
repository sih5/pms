﻿using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Controls {
    public class GridViewSerialNumberColumn : GridViewColumn {
        private int currentIndex;

        public override FrameworkElement CreateCellEditElement(GridViewCell cell, object dataItem) {
            var element = base.CreateCellEditElement(cell, dataItem) ?? this.CreateEditor(cell);
            return element;
        }

        private FrameworkElement CreateEditor(GridViewCell cell) {
            var gridview = cell.ParentRow.ParentOfType<RadGridView>();
            var selectIndex = gridview.Items.IndexOf(cell.ParentRow.DataContext) + 1;
            return this.CreateTextBoxEditorWithBinding(selectIndex);
        }

        private FrameworkElement CreateTextBoxEditorWithBinding(int value) {
            var element = new TextBlock();
            element.Text = value.ToString(CultureInfo.InvariantCulture);
            return element;
        }

        public override FrameworkElement CreateCellElement(GridViewCell cell, object dataItem) {
            var content = cell.Content as FrameworkElement ?? new TextBlock();
            var gridview = cell.ParentRow.ParentOfType<RadGridView>();
            currentIndex = gridview.Items.IndexOf(cell.ParentRow.DataContext) + 1;
            content.SetValue(TextBlock.TextProperty, currentIndex.ToString(CultureInfo.InvariantCulture));
            return content;
        }
    }
}
