﻿using System;

namespace Sunlight.Silverlight.Dcs.Controls {
    /// <summary>
    /// 弹出窗体内容控件接口
    /// 
    /// 作为DmsPopupWindow的Content属性显示的内容控件应实现此接口，可在内容控件中控制弹出窗体的关闭效果。
    /// </summary>
    public interface IPopupContentControl {
        event EventHandler<PopupContentControlClosedEventArgs> Closed;
    }
}