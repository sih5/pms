﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.Controls {
    public abstract class MultiDataQueryWindowBase : QueryWindowBase, IBaseView {
        private IDictionary<string, DataGridViewBase> dataGridViews;
        private QueryPanelBase queryPanel;
        protected Entity selectedEntityByDoubleClick;

        protected UserControlBase customerView;

        public new event EventHandler SelectionChanged;

        public new event EventHandler SelectionDecided;

        protected virtual void DataGridView_RowDoubleClick(object sender, DataGridViewRowDoubleClickEventArgs e) {
            this.selectedEntityByDoubleClick = e.Row.DataContext as Entity;
            if(this.selectedEntityByDoubleClick != null) {
                this.RaiseSelectionChanged();
                this.RaiseSelectionDecided();
            }
        }

        private void DataGridView_SelectionChanged(object sender, EventArgs e) {
            this.selectedEntityByDoubleClick = null;
            this.RaiseSelectionChanged();
        }

        protected virtual FilterItem OnRequestFilterItem(string gridViewKey, FilterItem filterItem) {
            return filterItem;
        }

        protected sealed override FilterItem OnRequestFilterItem(FilterItem filterItem) {
            return base.OnRequestFilterItem(filterItem);
        }

        protected virtual void QueryPanel_ExecutingQuery(object sender, FilterPanelRibbonGroup.ExecutingQueryEventArgs e) {
            if(this.DataGridViewses == null)
                return;
            foreach(var gridViewComponent in this.DataGridViewses) {
                gridViewComponent.Value.FilterItem = this.OnRequestFilterItem(gridViewComponent.Key, e.Filter);
                gridViewComponent.Value.ExecuteQueryDelayed();
            }
        }

        protected virtual void RaiseSelectionChanged() {
            EventHandler selectionChanged = this.SelectionChanged;
            if(selectionChanged != null) {
                selectionChanged.Invoke(this, EventArgs.Empty);
            }
        }

        protected virtual void RaiseSelectionDecided() {
            EventHandler selectionDecided = this.SelectionDecided;
            if(selectionDecided != null) {
                selectionDecided.Invoke(this, EventArgs.Empty);
            }
        }

        protected IDictionary<string, DataGridViewBase> DataGridViewses {
            get {
                if((this.dataGridViews == null) && this.DataGridViewKeies != null && this.DataGridViewKeies.Length > 0 &&
                    this.DataGridViewKeies.Any(content => !string.IsNullOrEmpty(content))) {
                    dataGridViews = new Dictionary<string, DataGridViewBase>();
                    foreach(var key in this.DataGridViewKeies) {
                        var gridView = DI.GetDataGridView(key);
                        if(gridView == null)
                            continue;
                        gridView.SelectionChanged += this.DataGridView_SelectionChanged;
                        gridView.RowDoubleClick += this.DataGridView_RowDoubleClick;
                        dataGridViews.Add(key, gridView);
                    }
                }
                return this.dataGridViews;
            }
        }

        protected virtual IEnumerable<Entity> SelectedEntitiesProcessing(IEnumerable<Entity> entities) {
            return entities;
        }

        public abstract string[] DataGridViewKeies {
            get;
        }

        public virtual object GetReturnData() {
            return null;
        }

        protected new QueryPanelBase QueryPanel {
            get {
                if((this.queryPanel == null) && !string.IsNullOrEmpty(this.QueryPanelKey)) {
                    this.queryPanel = DI.GetQueryPanel(this.QueryPanelKey);
                    this.queryPanel.ExecutingQuery += this.QueryPanel_ExecutingQuery;
                }
                return this.queryPanel;
            }
        }

        protected virtual UserControlBase CustomerView {
            get {
                return null;
            }
        }

        public override string DataGridViewKey {
            get {
                return "";
            }
        }

        public abstract string[] GridViewTitles {
            get;
        }

        public new IEnumerable<Entity> SelectedEntities {
            get {
                if(this.selectedEntityByDoubleClick != null) {
                    return SelectedEntitiesProcessing(new[] {
                        this.selectedEntityByDoubleClick
                    });
                }
                if(this.DataGridViewses != null)
                    return SelectedEntitiesProcessing((IEnumerable<Entity>)this.DataGridViewses.Values.Select(gridView => gridView.SelectedEntities));
                return Enumerable.Empty<Entity>();
            }
        }

        protected IEnumerable<Entity> GetSelectedEntitiesProcessing(string dataGridViewKey) {
            if(this.selectedEntityByDoubleClick != null)
                return new[] {
                    this.selectedEntityByDoubleClick
                };
            if(this.DataGridViewses != null)
                return this.DataGridViewses[dataGridViewKey].SelectedEntities;
            return Enumerable.Empty<Entity>();
        }
    }
}
