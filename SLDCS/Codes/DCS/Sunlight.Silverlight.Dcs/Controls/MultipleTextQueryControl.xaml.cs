﻿using System;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;

namespace Sunlight.Silverlight.Dcs.Controls {
    public partial class MultipleTextQueryControl : IFilterPanelControl {
        private string ColumnName {
            get;
            set;
        }

        private Type ColumnType {
            get;
            set;
        }

        private MultipleTextQueryControl() {
            InitializeComponent();
        }

        public MultipleTextQueryControl(Type columnType, string columnName)
            : this() {
            this.ColumnType = columnType;
            this.ColumnName = columnName;
        }

        public FilterItem GetFilter() {
            return new FilterItem(this.ColumnName, this.ColumnType, FilterOperator.IsContainedIn, this.GetFilterValue());
        }

        /// <summary>
        ///     若结果集为空，则返回 NULL，表示忽略这个查询条件
        /// </summary>
        /// <returns></returns>
        public object GetFilterValue() {
            object value = this.MainTextBox.Items;
            if(this.MainTextBox.Items.Count == 0) {
                value = null;
            }
            return value;
        }

        /// <summary>
        ///     对这个查询条件控件赋予默认值
        /// </summary>
        /// <param name="value"></param>
        public void SetFilterValue(object value) {
            this.MainTextBox.SetValue(value);
        }
    }
}
