﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs {
    public class DcsDataEditPanelBase : UserControlBase, IDataEditPanel {
        private ViewInitializer initializer;
        private KeyValueManager keyValueManager;

        protected ViewInitializer Initializer {
            get {
                return this.initializer ?? (this.initializer = new ViewInitializer(this));
            }
        }

        protected KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        private void RefreshTextBlockBindings() {
            foreach(var textBlock in this.FindChildrenByType<TextBlock>()) {
                var expression = textBlock.GetBindingExpression(TextBlock.TextProperty);
                if(expression == null)
                    continue;
                var binding = expression.ParentBinding;
                if(binding.Converter is KeyValueItemConverter) {
                    textBlock.Text = string.Empty;
                    textBlock.SetBinding(TextBlock.TextProperty, binding);
                }
            }
        }

        private void Initialize() {
            this.KeyValueManager.LoadData(this.RefreshTextBlockBindings);
        }

        public DcsDataEditPanelBase() {
            this.Initializer.Register(this.Initialize);
        }

        protected FrameworkElement CreateHorizontalLine(int columnIndex = 0, int rowIndex = 0, int columnSpan = 0, int rowSpan = 0) {
            var result = new Rectangle {
                Height = 1,
                Fill = new SolidColorBrush(Color.FromArgb(0xFF, 0xA0, 0xA0, 0xA0)),
                Margin = new Thickness(8, 2, 8, 5)
            };
            result.SetValue(Grid.ColumnProperty, columnIndex);
            result.SetValue(Grid.RowProperty, rowIndex);
            if(columnSpan > default(int))
                result.SetValue(Grid.ColumnSpanProperty, columnSpan);
            if(rowSpan > default(int))
                result.SetValue(Grid.RowSpanProperty, rowSpan);
            return result;
        }

        protected FrameworkElement CreateVerticalLine(int gridColumnIndex = 0, int gridRowIndex = 0, int columnSpan = 0, int rowSpan = 0) {
            var result = new Rectangle {
                Width = 1,
                Fill = new SolidColorBrush(Color.FromArgb(0xFF, 0xA0, 0xA0, 0xA0)),
                Margin = new Thickness(8, 0, 8, 0)
            };
            result.SetValue(Grid.ColumnProperty, gridColumnIndex);
            result.SetValue(Grid.RowProperty, gridRowIndex);
            if(columnSpan > default(int))
                result.SetValue(Grid.ColumnSpanProperty, columnSpan);
            if(rowSpan > default(int))
                result.SetValue(Grid.RowSpanProperty, rowSpan);
            return result;
        }
    }
}
