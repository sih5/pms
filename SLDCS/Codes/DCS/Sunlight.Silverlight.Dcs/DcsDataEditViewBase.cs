﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs {
    public class DcsDataEditViewBase : DataEditViewBase {
        private bool uiCreated;
        private Border contentContainer;
        private TextBlock titleText;
        private string title;
        private readonly IDictionary<ButtonItem, RadButton> buttons = new Dictionary<ButtonItem, RadButton>();
        private StackPanel buttonStackPanel;
        private RadButton saveButton, cancelButton;
        private Border dataEditViewBorder;


        protected bool isConent {
            set {
                if(value)
                    dataEditViewBorder.HorizontalAlignment = HorizontalAlignment.Center;
            }
        }

        protected new DcsDomainContext DomainContext {
            get {
                return base.DomainContext as DcsDomainContext;
            }
            set {
                base.DomainContext = value;
            }
        }

        protected virtual string BusinessName {
            get {
                return null;
            }
        }

        protected virtual string Title {
            get {
                return title;
            }
            set {
                this.title = value;
                this.titleText.Text = this.GetTitleText();
            }
        }

        public new UIElement Content {
            get {
                return this.uiCreated ? this.contentContainer.Child : base.Content;
            }
            set {
                if(this.uiCreated)
                    this.contentContainer.Child = value;
                else
                    base.Content = value;
            }
        }

        protected virtual void CreateUILayout() {
            if(this.uiCreated)
                return;
            this.uiCreated = true;

            // 合并按钮的资源字典
            this.Resources.MergedDictionaries.Add(new ResourceDictionary {
                Source = new Uri("/Sunlight.Silverlight.Dcs;component/Styles/FlatButton.xaml", UriKind.Relative),
            });

            // 创建标题栏内的布局容器
            var titleGrid = new Grid();
            titleGrid.ColumnDefinitions.Add(new ColumnDefinition());
            titleGrid.ColumnDefinitions.Add(new ColumnDefinition {
                Width = GridLength.Auto
            });
            // 创建标题文本
            titleGrid.Children.Add(this.titleText = new TextBlock {
                Margin = new Thickness(8, 5, 8, 5),
                FontSize = 18,
                Text = this.GetTitleText(),
            });
            // 创建操作按钮的布局容器
            var stackPanel = new StackPanel {
                Orientation = Orientation.Horizontal,
                HorizontalAlignment = HorizontalAlignment.Right,
                Margin = new Thickness(0, 1, 15, 1),
            };
            stackPanel.SetValue(Grid.ColumnProperty, 1);
            titleGrid.Children.Add(stackPanel);
            buttonStackPanel = new StackPanel {
                Orientation = Orientation.Horizontal,
                HorizontalAlignment = HorizontalAlignment.Right,
            };
            stackPanel.Children.Add(buttonStackPanel);
            if(buttons.Keys.Count > 0) {
                foreach(var otherButton in this.buttons.Values) {
                    otherButton.Margin = new Thickness(5, 0, 0, 0);
                    this.buttonStackPanel.Children.Add(otherButton);
                }
            }
            // 创建保存按钮
            saveButton = new RadButton {
                Style = (Style)this.Resources["SubmitButton"],
                Command = this.SubmitCommand,
                Margin = new Thickness(5, 0, 0, 0),
                Visibility = Visibility.Visible
            };
            stackPanel.Children.Add(saveButton);
            // 创建取消按钮
            cancelButton = new RadButton {
                Margin = new Thickness(5, 0, 0, 0),
                Style = (Style)this.Resources["RejectButton"],
                Command = this.CancelCommand,
                Visibility = Visibility.Visible
            };
            stackPanel.Children.Add(cancelButton);
            // 只有在Load之后，RadButton才会根据Command.CanExecute设置IsEnabled属性值。
            foreach(var btn in stackPanel.FindChildrenByType<RadButton>()) {
                btn.Loaded += (sender, args) => this.NotifyCommandsCanExecuteChanged();
            }

            // 创建用于承载内容的容器
            var busyIndicator = new RadBusyIndicator {
                DisplayAfter = TimeSpan.Zero,
                Content = this.contentContainer = new Border {
                    BorderThickness = new Thickness(1),
                    BorderBrush = new SolidColorBrush(Color.FromArgb(0xFF, 0xC0, 0xC0, 0xC0)),
                },
            };
            busyIndicator.SetBinding(RadBusyIndicator.IsBusyProperty, new Binding("IsBusy") {
                Source = base.DomainContext,
                Mode = BindingMode.OneWay,
            });
            busyIndicator.SetValue(Grid.RowProperty, 1);

            // 创建整个页面的根布局
            var root = new Grid {
                Background = new SolidColorBrush(Colors.White)
            };
            root.RowDefinitions.Add(new RowDefinition {
                Height = GridLength.Auto
            });
            root.RowDefinitions.Add(new RowDefinition());
            root.Children.Add(new Border {
                BorderThickness = new Thickness(1, 1, 1, 0),
                BorderBrush = new SolidColorBrush(Color.FromArgb(0xFF, 0xC0, 0xC0, 0xC0)),
                Background = new SolidColorBrush(Color.FromArgb(0xFF, 0xED, 0xED, 0xED)),
                Child = titleGrid
            });
            root.Children.Add(busyIndicator);
            dataEditViewBorder = new Border {
                Child = root,
            };
            var originalContent = base.Content;
            base.Content = dataEditViewBorder;
            this.contentContainer.Child = originalContent;
            this.InitailizedUI();
        }

        private void RefreshButtons(bool isShow) {
            this.buttonStackPanel.Children.Clear();
            foreach(var kv in this.buttons.Where(kv => kv.Value == null).ToList())
                this.buttons[kv.Key] = this.CreateButton(kv.Key, isShow);
            foreach(var button in this.buttons.Values)
                this.buttonStackPanel.Children.Add(button);
        }

        private RadButton CreateButton(ButtonItem item, bool isShow) {
            var result = new RadButton {
                Style = (Style)this.Resources["Flat"],
                Width = 26,
                Height = 26,
                Margin = new Thickness(5, 0, 0, 0),
                Background = new SolidColorBrush(Color.FromArgb(255, 135, 206, 235)),
                Content = new Image {
                    Source = new BitmapImage(item.Icon),
                    HorizontalAlignment = HorizontalAlignment.Center,
                    VerticalAlignment = VerticalAlignment.Center,
                    Stretch = Stretch.Fill,
                }
            };
            if(item.Action != null)
                result.Click += (sender, args) => item.Action();
            else if(item.Command != null) {
                result.Command = item.Command;
                result.CommandParameter = item.CommandParameter;
            }
            if(isShow)
                ToolTipService.SetToolTip(result, item.Title);
            return result;
        }

        protected virtual string GetTitleText() {
            return string.IsNullOrEmpty(this.Title) ? string.Format(this.EditState == DataEditState.New ? DcsUIStrings.DataEditView_Add : DcsUIStrings.DataEditView_Edit, this.BusinessName) : this.Title;
        }

        protected DcsDataEditViewBase() {
            this.Initializer.Register(this.CreateUILayout);
        }

        /// <summary>
        /// 重置当前状态，在提交成功后或退出当前编辑界面前被调用。
        /// </summary>
        protected virtual void Reset() {

        }

        protected override void OnEditSubmitted() {
            this.Reset();
            base.OnEditSubmitted();
        }

        protected override void OnEditCancelled() {
            this.Reset();
            base.OnEditCancelled();
        }

        protected virtual void InitailizedUI() {

        }

        protected FrameworkElement CreateVerticalLine(int gridColumnIndex = 0, int gridRowIndex = 0, int columnSpan = 0, int rowSpan = 0) {
            var result = new Rectangle {
                Width = 1,
                Fill = new SolidColorBrush(Color.FromArgb(0xFF, 0xA0, 0xA0, 0xA0)),
                Margin = new Thickness(8, 0, 8, 0)
            };
            result.SetValue(Grid.ColumnProperty, gridColumnIndex);
            result.SetValue(Grid.RowProperty, gridRowIndex);
            if(columnSpan > default(int))
                result.SetValue(Grid.ColumnSpanProperty, columnSpan);
            if(rowSpan > default(int))
                result.SetValue(Grid.RowSpanProperty, rowSpan);
            return result;
        }

        protected FrameworkElement CreateHorizontalLine(int columnIndex = 0, int rowIndex = 0, int columnSpan = 0, int rowSpan = 0) {
            var result = new Rectangle {
                Height = 1,
                Fill = new SolidColorBrush(Color.FromArgb(0xFF, 0xA0, 0xA0, 0xA0)),
                Margin = new Thickness(8, 2, 8, 5)
            };
            result.SetValue(Grid.ColumnProperty, columnIndex);
            result.SetValue(Grid.RowProperty, rowIndex);
            if(columnSpan > default(int))
                result.SetValue(Grid.ColumnSpanProperty, columnSpan);
            if(rowSpan > default(int))
                result.SetValue(Grid.RowSpanProperty, rowSpan);
            return result;
        }

        protected override void OnEditStateChanged() {
            if(this.titleText != null)
                this.titleText.Text = this.GetTitleText();
        }

        public void RegisterButton(ButtonItem button, bool isShow = false) {
            this.buttons[button] = null;
            this.RefreshButtons(isShow);
        }

        public void UnregisterButton(ButtonItem button, bool isShow = false) {
            if(this.buttons.Remove(button))
                this.RefreshButtons(isShow);
        }

        public void HideSaveButton() {
            this.saveButton.Visibility = Visibility.Collapsed;
        }

        public void ShowSaveButton() {
            this.saveButton.Visibility = Visibility.Visible;
        }

        public void HideCancelButton() {
            this.cancelButton.Visibility = Visibility.Collapsed;
        }

        public void ShowCancelButton() {
            this.cancelButton.Visibility = Visibility.Visible;
        }
    }
}
