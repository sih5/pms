﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.DomainServices;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Dcs {
    public abstract class DcsDataGridViewBase : DataGridViewBase {
        private const string EXPORT_EXTENSION = "xls";
        private bool keyValuesLoaded;
        private KeyValueManager keyValueManager;

        protected KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        private void Initialize() {
            this.KeyValueManager.LoadData(() => {
                this.RebindKeyValueColumns();
                this.keyValuesLoaded = true;
                this.NotifyOfPropertyChange("CanExecuteQuery");
            });
            this.DomainDataSource.LoadingData += DomainDataSourceOnLoadingData;
        }
        private void DomainDataSourceOnLoadingData(object sender, LoadingDataEventArgs loadingDataEventArgs) {
            if(this.Entities == null)
                return;
            var first = this.Entities.FirstOrDefault();
            if(first == null)
                return;
            EntitySet entitySet;
            if(!this.DomainContext.EntityContainer.TryGetEntitySet(first.GetType(), out entitySet))
                return;
            if(entitySet.Count == 0)
                return;
            foreach(var entity in this.Entities)
                try {
                    entitySet.Detach(entity);
                } catch(Exception e) {
                    Console.WriteLine(e);
                }
        }
        private void ExportData(GridViewExportOptions options) {
            var dialog = new SaveFileDialog {
                DefaultExt = EXPORT_EXTENSION,
                Filter = string.Format("{0} (*.{1})|*.{1}", DcsUIStrings.ExcelDocument, EXPORT_EXTENSION),
            };
            var result = dialog.ShowDialog();
            if(result.HasValue && result.Value)
                using(var stream = dialog.OpenFile())
                    this.GridView.Export(stream, options);
        }

        protected DcsDataGridViewBase() {
            this.RegisterCanExecuteQueryDependency(() => this.keyValuesLoaded);
            this.Initializer.Register(this.Initialize);
            this.Resources.MergedDictionaries.Add(new ResourceDictionary {
                Source = new Uri("/Sunlight.Silverlight.Dcs;component/Styles/GridViewStyle.xaml", UriKind.Relative),
            });
        }

        protected override void OnControlsCreated() {
            //
        }

        protected override string OnRequestQueryName() {
            return null;
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            throw new NotSupportedException();
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            return this.FilterItem == null ? null : this.FilterItem.ToFilterDescriptor();
        }

        internal void SetButtonCommandTargetToGridView(RadButton button) {
            if(button == null)
                throw new ArgumentNullException("button");
            button.CommandTarget = this.GridView;
        }

        public void ExportData(bool showHeaders = true, bool showFooters = true, bool showGroupFooters = true) {
            this.ExportData(new GridViewExportOptions {
                Format = ExportFormat.ExcelML,
                ShowColumnHeaders = showHeaders,
                ShowColumnFooters = showFooters,
                ShowGroupFooters = showGroupFooters,
            });
        }

        public void ExportAllLocalData(bool showHeaders = true, bool showFooters = true, bool showGroupFooters = true) {
            EntitySet entitySet;
            if(!this.DomainContext.EntityContainer.TryGetEntitySet(this.EntityType, out entitySet))
                return;

            this.ExportData(new GridViewExportOptions {
                Format = ExportFormat.ExcelML,
                ShowColumnHeaders = showHeaders,
                ShowColumnFooters = showFooters,
                ShowGroupFooters = showGroupFooters,
                Items = entitySet,
            });
        }
        public Dictionary<string, object> GetColumnItems {
            get {
                if(this.GridView == null)
                    return null;
                var columnItems = new Dictionary<string, object>();
                foreach(var columnItem in this.GridView.Columns) {
                    if(columnItem is GridViewSelectColumn)
                        continue;
                    columnItems.Add(columnItem.UniqueName, columnItem.Header);
                }
                return columnItems;
            }
        }
        public void CancelEdit() {
            this.GridView.CancelEdit();
        }
    }
}
