﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs {
    public abstract class DcsDataManagementViewBase : DataManagementViewBase {
        private string[] actionPanelKeys;
        protected const string DATA_GRID_VIEW = "_DataGridView_";
        protected const string DATA_EDIT_VIEW = "_DataEditView_";
        protected const string WORKFLOW_VIEW = "_WorkflowView_";

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return null;
            }
        }

        protected override IEnumerable<string> ActionPanelKeys {
            get {
                if(this.actionPanelKeys == null) {
                    var attribute = this.GetType().GetCustomAttributes(typeof(PageMetaAttribute), false).Cast<PageMetaAttribute>().SingleOrDefault();
                    this.actionPanelKeys = attribute == null || attribute.ActionPanelKeys == null ? new string[0] : attribute.ActionPanelKeys;
                }
                return this.actionPanelKeys;
            }
        }

        protected override IEnumerable<RadRibbonTab> CustomRibbonTabs {
            get {
                return null;
            }
        }

        public int Id {
            get {
                return Convert.ToInt32(this.UniqueId);
            }
        }

        private void DataGridView_SelectionChanged(object sender, EventArgs e) {
            this.CheckActionsCanExecute();
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            return false;
        }

        protected override void OnViewSwitched(string key, UserControl view) {
            var dataGridView = view as DcsDataGridViewBase;
            if(dataGridView != null) {
                dataGridView.SelectionChanged -= this.DataGridView_SelectionChanged;
                dataGridView.SelectionChanged += this.DataGridView_SelectionChanged;
            }
            this.CheckActionsCanExecute();
        }
    }
}
