﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using Sunlight.Silverlight.Controls;
using Sunlight.Silverlight.Dcs.Controls;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.DragDrop;

namespace Sunlight.Silverlight.Dcs {
    public abstract partial class DcsDataTreeView : INotifyPropertyChanged {
        private ObservableCollection<RadTreeViewItem> treeItemCollection;
        public event TreeViewDropQuery OnTreeViewDropQuery;
        public event TreeViewDropInfo OnTreeViewDropInfo;
        public event TreeViewDoubleClick OnTreeViewDoubleClick;
        public event TreeViewOnChecked OnTreeViewChecked;
        public event TreeViewOnItemClick OnTreeViewItemClick;
        public event TreeViewDataLoaded OnTreeViewDataLoaded;
        public event TreeViewItemExpanded OnTreeViewItemExpanded;
        //public event RoutedEventHandler OnTreeViewItemMenuClicked;
        public event PropertyChangedEventHandler PropertyChanged;
        private DcsDomainContext domainContext;
        private Visibility isVisibility = Visibility.Collapsed;
        private int selectedItemId;
        private ObservableCollection<DcsMenuItem> menuItems;
        private bool isLineEnable, isDragDropEnable, isOptionElements, isExpanderFirstNode = true, isInitialize;

        protected DcsDataTreeView() {
            this.InitializeComponent();
            this.DataContext = this;
            this.txtSearchBox.SearchTextChanged += this.TreeFilter_SearchTextChanged;
            this.RadMenuItems.CollectionChanged += this.RadMenuItems_CollectionChanged;
            //this.DataTreeView.AddHandler(RadMenuItem.ClickEvent, new RoutedEventHandler(OnMenuItemClicked));
            this.Loaded += this.DcsDataTreeView_Loaded;
        }

        private void DcsDataTreeView_Loaded(object sender, RoutedEventArgs e) {
            if(!isInitialize)
                this.Initialize();
        }

        //TODO:将该方法定义为延迟调用
        /// <summary>
        ///     初始化页面 信息树
        /// </summary>
        private void Initialize() {
            this.InitializeControl();
            if(this.IsDragDropEnable && this.TreeViewDataExchangeDestination != null) {
                //RadDragAndDropManager.SetAllowDrop(this.TreeViewDataExchangeDestination, this.IsDragDropEnable);
                //RadDragAndDropManager.AddDropQueryHandler(this.TreeViewDataExchangeDestination, this.DataTreeView_OnDropQuery);
                //RadDragAndDropManager.AddDropInfoHandler(this.TreeViewDataExchangeDestination, this.DataTreeView_OnDropInfo);
            }
            this.DataTreeView.ItemDoubleClick += this.DataTreeView_DoubleClick;
            this.DataTreeView.Checked += this.DataTreeView_Checked;
            this.DataTreeView.ItemClick += this.DataTreeView_ItemClick;
            this.DataTreeView.Expanded += this.DataTreeView_Expanded;
            this.ContextMenu.Opening += this.ContextMenu_Opening;
            this.isInitialize = true;
        }

        private void ContextMenu_Opening(object sender, RadRoutedEventArgs e) {
            if(!RadMenuItems.Any())
                return;
            foreach(var menuItem in RadMenuItems) {
                if(menuItem.CheckMenuVisiable != null)
                    menuItem.Visibility = menuItem.CheckMenuVisiable.Invoke() ? Visibility.Visible : Visibility.Collapsed;
                else
                    menuItem.Visibility = Visibility.Visible;
            }
        }

        private void RadMenuItems_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e) {
            this.OnPropertyChanged("IsEnableMenu");
        }

        private void TreeFilter_SearchTextChanged(object sender, EventArgs e) {
            var searcher = sender as SearchTextBox;
            if(searcher == null)
                return;
            var searchText = searcher.SearchText;
            if(string.IsNullOrWhiteSpace(searchText)) {
                this.DataTreeView.Items.Clear();
                this.PopulateItems(this.TreeItemCollection.Select(treeItem => treeItem.Tag), null, this.DataTreeView.Items, null);
                return;
            }
            var searchCollection = new ObservableCollection<RadTreeViewItem>(this.TreeItemCollection);
            foreach(var item in searchCollection)
                item.Visibility = Visibility.Collapsed;
            foreach(var item in searchCollection.Where(treeItem => treeItem.Header is string && treeItem.Header.ToString().Contains(searchText))) {
                item.Visibility = Visibility.Visible;
                this.VisiableParentTreeItem(item);
            }
            this.DataTreeView.Items.Clear();
            this.PopulateItems(searchCollection.Where(treeItem => treeItem.Visibility == Visibility.Visible).Select(treeItem => treeItem.Tag), null, this.DataTreeView.Items, null);
            this.DataTreeView.ExpandAll();
        }

        private void VisiableParentTreeItem(RadTreeViewItem parentItem) {
            var currentParentItem = this.VisiableParentTreeItemHander(parentItem, this.TreeItemCollection);
            if(currentParentItem != null)
                this.VisiableParentTreeItem(currentParentItem);
        }

        //private void OnMenuItemClicked(object sender, RoutedEventArgs e) {
        //    this.OnTreeViewItemMenuClicked(sender, e);
        //}

        private RadTreeViewItem GetTreeViewItem(int id) {
            if(id != default(int) && this.TreeItemCollection != null && this.TreeItemCollection.Any()) {
                var item = this.TreeItemCollection.FirstOrDefault(treeItem => treeItem.Tag is Entity && ((int)(treeItem.Tag as Entity).GetIdentity()) == id);
                if(item != null)
                    return item;
            }
            return null;
        }

        //private void ContextMenuOpened(object sender, RoutedEventArgs e) {
        //    var treeView = sender as RadContextMenu;
        //    if(treeView == null)
        //        return;
        //    var treeViewItem = treeView.GetClickedElement<RadTreeViewItem>();

        //    if(treeViewItem == null) {
        //        (sender as RadContextMenu).IsOpen = false;
        //        return;
        //    }

        //    if(!treeViewItem.IsSelected) {
        //        DataTreeView.SelectedItems.Clear();
        //        DataTreeView.SelectedItems.Add(treeViewItem.Item);
        //    }
        //}

        //private string ExpandAllParent(RadTreeViewItem childTreeItem) {
        //    if(childTreeItem != null)
        //        if(childTreeItem.ParentItem != null)
        //            return ExpandAllParent(childTreeItem.ParentItem) + "|" + childTreeItem.Header;
        //    return "";
        //}

        protected virtual void InitializeControl() {
        }

        protected virtual void DataTreeView_Expanded(object sender, RadRoutedEventArgs e) {
            if(this.OnTreeViewItemExpanded != null)
                this.OnTreeViewItemExpanded(sender, e);
        }

        protected virtual void DataTreeView_Checked(object sender, RadRoutedEventArgs e) {
            if(this.OnTreeViewChecked != null)
                this.OnTreeViewChecked(sender, e);
        }

        protected virtual void DataTreeView_DoubleClick(object sender, RadRoutedEventArgs e) {
            if(this.OnTreeViewDoubleClick != null)
                this.OnTreeViewDoubleClick(sender, e);
        }

        protected virtual void DataTreeView_ItemClick(object sender, RadRoutedEventArgs e) {
            if(this.OnTreeViewItemClick != null)
                this.OnTreeViewItemClick(sender, e);
        }

        protected virtual void DataTreeView_OnDropQuery(object sender, DragDropQueryEventArgs e) {
            if(this.OnTreeViewDropQuery != null)
                this.OnTreeViewDropQuery(sender, e);
        }

        protected virtual void DataTreeView_OnDropInfo(object sender, DragDropEventArgs e) {
            if(this.OnTreeViewDropInfo != null)
                this.OnTreeViewDropInfo(sender, e);
        }

        protected void OnPropertyChanged(string propertyName) {
            if(this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        protected ObservableCollection<RadTreeViewItem> TreeItemCollection {
            get {
                return this.treeItemCollection ?? (this.treeItemCollection = new ObservableCollection<RadTreeViewItem>());
            }
        }

        protected abstract RadTreeViewItem VisiableParentTreeItemHander(RadTreeViewItem parentItem, ObservableCollection<RadTreeViewItem> treeItemCollection);

        protected void PopulateItems(IEnumerable<object> entities, int? id, ICollection<object> treeItems, Action<RadTreeViewItem> additionalHandler) {
            if(entities == null)
                return;
            this.PopulateItemsHandler(entities, id, treeItems, additionalHandler);
        }

        protected void LoadTreeViewData() {
            if(EntityQuery == null)
                return;
            this.DomainContext.Load(EntityQuery, LoadBehavior.RefreshCurrent, loadOp => {
                this.TreeItemCollection.Clear();
                this.DataTreeView.Items.Clear();
                this.PopulateItems(loadOp.Entities, this.TreeViewRootItemIdentity(), this.DataTreeView.Items, item => this.TreeItemCollection.Add(item));
                if(this.DataTreeView.Items.Cast<RadTreeViewItem>().Any())
                    this.DataTreeView.Items.Cast<RadTreeViewItem>().First().IsExpanded = this.IsExpanderFirstNode;
                if(this.OnTreeViewDataLoaded != null)
                    this.OnTreeViewDataLoaded();
            }, null);
        }

        protected abstract void PopulateItemsHandler(IEnumerable<object> entities, int? id, ICollection<object> treeItems, Action<RadTreeViewItem> additionalHandler);

        protected abstract int? TreeViewRootItemIdentity();

        /// <summary>
        ///   设置树检索框上的浮动提示信息
        /// </summary>
        public virtual string WatermarkText {
            get {
                return null;
            }
        }

        /// <summary>
        /// 清空树节点
        /// </summary>
        public void ClearTreeViewItem() {
            this.TreeItemCollection.Clear();
            this.DataTreeView.Items.Clear();
        }

        /// <summary>
        /// 作废树节点(标记为节点不可用)
        /// </summary>
        /// <param name="id"></param>
        public void CancelTreeItem(int id) {
            var treeItem = this.GetTreeViewItem(id);
            if(treeItem == null)
                return;
            this.TreeItemCollection.Remove(treeItem);
            this.DataTreeView.Items.Clear();
            this.PopulateItems(this.TreeItemCollection.Select(e => e.Tag), this.TreeViewRootItemIdentity(), this.DataTreeView.Items, null);
        }

        /// <summary>
        ///     查询数据库用的DomainContext 外部传入
        /// </summary>
        public DcsDomainContext DomainContext {
            get {
                return this.domainContext ?? (this.domainContext = new DcsDomainContext());
            }
            set {
                this.domainContext = value;
            }
        }

        /// <summary>
        ///     是否有复选框
        /// </summary>
        public bool IsOptionElements {
            get {
                return this.isOptionElements;
            }
            set {
                this.isOptionElements = value;
                this.OnPropertyChanged("IsOptionElements");
            }
        }

        /// <summary>
        ///     是否有节点连接线
        /// </summary>
        public bool IsLineEnable {
            get {
                return this.isLineEnable;
            }
            set {
                this.isLineEnable = value;
                this.OnPropertyChanged("IsLineEnable");
            }
        }

        /// <summary>
        ///     是否启用菜单
        /// </summary>
        public bool IsEnableMenu {
            get {
                return this.RadMenuItems.Any();
            }
        }

        /// <summary>
        ///     重新加载树数据
        /// </summary>
        public void RefreshDataTree() {
            this.LoadTreeViewData();
        }

        /// <summary>
        ///     是否显示 过滤查询框
        /// </summary>
        public Visibility IsVisibility {
            get {
                return this.isVisibility;
            }
            private set {
                this.isVisibility = value;
                this.OnPropertyChanged("IsVisibility");
            }
        }

        public bool IsDragDropEnable {
            get {
                return this.isDragDropEnable;
            }
            set {
                this.isDragDropEnable = value;
                this.OnPropertyChanged("IsDragDropEnable");
            }
        }

        /// <summary>
        ///     当树组件第一个节点下有数据的时候，自动展开第一级节点
        /// </summary>
        public bool IsExpanderFirstNode {
            get {
                return this.isExpanderFirstNode;
            }
            set {
                this.isExpanderFirstNode = value;
                this.OnPropertyChanged("IsExpanderFirstNode");
            }
        }

        public object GetObjectById(int id) {
            return this.GetTreeViewItem(id);
        }

        public int SelectedItemId {
            get {
                return this.selectedItemId;
            }
            set {
                this.selectedItemId = value;
                var item = this.GetTreeViewItem(value);
                if(item == null)
                    return;
                this.DataTreeView.ExpandAll();
                this.DataTreeView.SelectedItem = item;
            }
        }

        public object SelectedItem {
            get {
                return this.DataTreeView.SelectedItem;
            }
        }

        ///// <summary>
        /////     界面加载完成后，判断是否启用了Option框。没有启用的话，收起所有节点
        ///// </summary>
        ///// <param name="sender"> </param>
        ///// <param name="routedEventArgs"> </param>
        //public void TreeViewCollapseNode(object sender, RoutedEventArgs routedEventArgs) {
        //    if(!this.IsOptionElements)
        //        this.DataTreeView.CollapseAll();
        //}

        public DependencyObject TreeViewDataExchangeDestination {
            get;
            set;
        }

        /// <summary>
        ///     启用 过滤查询框
        /// </summary>
        public void EnableSearchTextBox() {
            this.IsVisibility = Visibility.Visible;
        }

        /// <summary>
        ///     获取树组件中第一个节点
        /// </summary>
        /// <returns></returns>
        public RadTreeViewItem GetFirstTreeViewItem() {
            if(this.DataTreeView.Items.Cast<RadTreeViewItem>().Any())
                return this.DataTreeView.Items.Cast<RadTreeViewItem>().First();
            return null;
        }

        public EntityQuery EntityQuery {
            get;
            set;
        }

        public ObservableCollection<DcsMenuItem> RadMenuItems {
            get {
                return this.menuItems ?? (this.menuItems = new ObservableCollection<DcsMenuItem>());
            }
        }

        public delegate void TreeViewDropQuery(object sender, DragDropQueryEventArgs e);

        public delegate void TreeViewDropInfo(object sender, DragDropEventArgs e);

        public delegate void TreeViewDoubleClick(object sender, RadRoutedEventArgs e);

        public delegate void TreeViewOnChecked(object sender, RadRoutedEventArgs e);

        public delegate void TreeViewOnItemClick(object sender, RadRoutedEventArgs e);

        public delegate void TreeViewOnUnchecked(object sender, RadRoutedEventArgs e);

        public delegate void TreeViewItemExpanded(object sender, RadRoutedEventArgs e);

        public delegate void TreeViewDataLoaded();
    }
}
