﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs {
    public partial class DcsDetailDataEditView {
        private bool initialized;
        private List<Tuple<object, Lazy<DataGridViewBase>>> dataGridViews;
        private readonly Dictionary<ButtonItem, RadButton> buttons;
        private ButtonItem insertButton, deleteButton;
        private ICommand convertDataForImport;

        public ButtonItem InsertButton {
            get {
                return this.insertButton ?? (this.insertButton = new ButtonItem {
                    Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/add.png", UriKind.Relative),
                    Command = RadGridViewCommands.BeginInsert,
                });
            }
        }

        public ButtonItem DeleteButton {
            get {
                return this.deleteButton ?? (this.deleteButton = new ButtonItem {
                    Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/delete.png", UriKind.Relative),
                    Command = RadGridViewCommands.Delete,
                });
            }
        }

        private void DcsDetailDataEditView_Loaded(object sender, RoutedEventArgs e) {
            if(this.initialized)
                return;
            this.initialized = true;

            if(this.dataGridViews == null)
                return;

            if(this.ConvertDataForGridView != null) {
                var importButton = new ButtonItem {
                    Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/import.png", UriKind.Relative),
                    Command = this.ConvertDataForImport
                };
                this.buttons.Add(importButton, this.CreateButton(importButton, false));
                this.RadUpload.UploadServiceUrl = DcsUtils.GetUploadHandlerUrl().ToString();
                this.RadUpload.FilesSelected -= this.RadUpload_FilesSelected;
                this.RadUpload.FilesSelected += this.RadUpload_FilesSelected;
                //this.RadUpload.FileUploadStarting -= this.RadUpload_FileUploadStarting;
                //this.RadUpload.FileUploadStarting += this.RadUpload_FileUploadStarting;
                this.RadUpload.UploadFinished -= this.RadUpload_UploadFinished;
                this.RadUpload.UploadFinished += this.RadUpload_UploadFinished;
            }

            foreach(var dataGridView in this.dataGridViews) {
                var view = dataGridView.Item2.Value;
                if(view == null)
                    continue;

                view.Margin = new Thickness(2);
                this.MainTabControl.Items.Add(new RadTabItem {
                    Margin = new Thickness(this.MainTabControl.HasItems ? 4 : (buttons.Count * 8 + buttons.Values.Sum(v => v.Width)), 0, 0, 0),
                    Header = dataGridView.Item1,
                    Content = view,
                });
            }

            if(this.MainTabControl.HasItems)
                this.MainTabControl.SelectedIndex = 0;

            this.RefreshButtons(false);
        }

        private void RadUpload_FilesSelected(object sender, FilesSelectedEventArgs e) {
            if(!e.SelectedFiles.Any())
                try {
                    //尝试读取，出错说明文件已打开
                    using(e.SelectedFiles[0].File.OpenRead()) {
                    }
                } catch(Exception ex) {
                    UIHelper.ShowAlertMessage(ex.Message);
                    e.Handled = true;
                }
        }

        //private void RadUpload_FileUploadStarting(object sender, FileUploadStartingEventArgs e) {
        //    //取当前登录用户的HashCode来标记上传文件的唯一性
        //    e.UploadData.FileName = BaseApp.Current.CurrentUserData.GetHashCode().ToString(CultureInfo.InvariantCulture) + "_" + e.UploadData.FileName;
        //}

        private void RadUpload_UploadFinished(object sender, RoutedEventArgs e) {
            var upload = sender as RadUpload;
            if(upload != null && upload.CurrentSession != null && upload.CurrentSession.CurrentFile != null) {
                upload.CancelUpload();
                //取当前登录用户的HashCode来标记上传文件的唯一性
                //this.ExecuteExcelImport(BaseApp.Current.CurrentUserData.GetHashCode().ToString(CultureInfo.InvariantCulture) + "_" + upload.CurrentSession.CurrentFile.Name);
                DcsUtils.Confirm(DcsUIStrings.DataEditView_Confirm_WillClearDetailData, () => this.ExecuteExcelImport(upload.CurrentSession.CurrentFile.Name));
            }
        }

        private RadButton CreateButton(ButtonItem buttonItem, bool IsDisplayTitle) {
            var result = new RadButton();
            result.Style = IsDisplayTitle ? null : (Style)this.Resources["ToolbarButton"];
            if(IsDisplayTitle) {
                result.Content = buttonItem.Title;
                result.Height = 20;
                Margin = new Thickness(4, 0, 4, 0);
                result.Background = new SolidColorBrush(Colors.DarkGray);
                result.Width = buttonItem.Title.Length * 13;
                result.HorizontalAlignment = HorizontalAlignment.Left;
            } else {
                result.Content = new Image {
                    Source = new BitmapImage(buttonItem.Icon),
                    Width = 16,
                    Height = 16,
                    Stretch = Stretch.Fill
                };
            }
            if(buttonItem.Action != null)
                result.Click += (sender, args) => buttonItem.Action();
            else if(buttonItem.Command != null) {
                result.Command = buttonItem.Command;
                result.CommandParameter = buttonItem.CommandParameter;
            }
            if(!IsDisplayTitle)
                ToolTipService.SetToolTip(result, buttonItem.Title);
            return result;
        }

        private void RefreshButtons(bool isDisplayTitle) {
            this.ButtonPanel.Children.Clear();
            foreach(var kv in this.buttons.Where(kv => kv.Value == null).ToList())
                this.buttons[kv.Key] = this.CreateButton(kv.Key, isDisplayTitle);
            foreach(var button in this.buttons.Values)
                this.ButtonPanel.Children.Add(button);
        }

        private void MainTabControl_SelectionChanged(object sender, RadSelectionChangedEventArgs e) {
            var insert = this.buttons.ContainsKey(this.InsertButton) ? this.buttons[this.InsertButton] : null;
            var delete = this.buttons.ContainsKey(this.DeleteButton) ? this.buttons[this.DeleteButton] : null;

            var dataGridView = this.MainTabControl.SelectedContent as DcsDataGridViewBase;
            if(dataGridView == null) {
                if(insert != null)
                    insert.CommandTarget = null;
                if(delete != null)
                    delete.CommandTarget = null;
                return;
            }
            if(insert != null)
                dataGridView.SetButtonCommandTargetToGridView(insert);
            if(delete != null)
                dataGridView.SetButtonCommandTargetToGridView(delete);
        }

        private void SelectedImportFile() {
            this.RadUpload.ShowFileDialog();
        }

        private void ExecuteExcelImport(string fileName) {
            this.GridViewLoading.IsBusy = true;
            //执行Excel读取
            var dataGridView = this.MainTabControl.SelectedContent as DcsDataGridViewBase;
            if(dataGridView == null)
                return;
            var validationErrors = this.ConvertDataForGridView.Invoke(fileName);
            this.GridViewLoading.IsBusy = false;
        }

        protected ICommand ConvertDataForImport {
            get {
                return this.convertDataForImport ?? (this.convertDataForImport = new Core.Command.DelegateCommand(this.SelectedImportFile));
            }
        }

        public DcsDetailDataEditView() {
            this.InitializeComponent();
            this.Loaded += this.DcsDetailDataEditView_Loaded;
            this.buttons = new Dictionary<ButtonItem, RadButton>();
            this.buttons.Add(this.InsertButton, this.CreateButton(this.InsertButton, false));
            this.buttons.Add(this.DeleteButton, this.CreateButton(this.DeleteButton, false));
            this.GridViewLoading.BusyContent = new TextBlock {
                FontSize = 13,
                Margin = new Thickness(10),
                Text = DcsUIStrings.Loading
            };
        }

        public virtual Func<string, List<object>> ConvertDataForGridView {
            get;
            set;
        }

        public void Register(string title, Uri icon, DataGridViewBase dataGridView) {
            this.Register(title, icon, () => dataGridView);
        }

        public void Register(string title, Uri icon, Func<DataGridViewBase> dataGridViewFactory) {
            if(this.dataGridViews == null)
                this.dataGridViews = new List<Tuple<object, Lazy<DataGridViewBase>>>();

            object header;
            if(icon == null)
                header = title;
            else
                header = BusinessViewUtils.MakeTabHeader(icon, title);

            this.dataGridViews.Add(Tuple.Create(header, new Lazy<DataGridViewBase>(dataGridViewFactory)));
        }

        public void RegisterButton(ButtonItem button, bool isDisplayTitle = false) {
            this.buttons[button] = null;
            this.RefreshButtons(isDisplayTitle);
        }

        public void UnregisterButton(ButtonItem button, bool isDisplayTitle = false) {
            //if(this.buttons == null || !this.buttons.ContainsKey(button))
            //    return;
            if(this.buttons.Remove(button))
                this.RefreshButtons(isDisplayTitle);
        }

        public void RefreshGridHeaderMargin() {
            var tabItem = this.MainTabControl.Items.First() as RadTabItem;
            if(tabItem == null)
                return;
            tabItem.Margin = new Thickness(buttons.Count * 8 + buttons.Values.Sum(v => v.Width), 0, 0, 0);
        }
    }
}
