﻿using Sunlight.Silverlight.Dcs.Resources;

namespace Sunlight.Silverlight.Dcs {
    public class DcsDetailDataViewBase : DcsDataEditViewBase {

        protected override void InitailizedUI() {
            base.InitailizedUI();
            this.HideSaveButton();
        }

        protected override string GetTitleText() {
            return string.IsNullOrWhiteSpace(this.BusinessName) ? this.Title : string.Format(DcsUIStrings.DataEditView_Detail, this.BusinessName);
        }
    }
}
