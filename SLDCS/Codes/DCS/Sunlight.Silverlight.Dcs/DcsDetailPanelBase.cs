﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs {
    public class DcsDetailPanelBase : UserControlBase, IDetailPanel, INotifyPropertyChanged {
        private KeyValueManager keyValueManager;
        private bool keyValuesLoaded;
        private ViewInitializer initializer;

        protected KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        protected ViewInitializer Initializer {
            get {
                return this.initializer ?? (this.initializer = new ViewInitializer(this));
            }
        }

        public bool KeyValuesLoaded {
            get {
                return this.keyValuesLoaded;
            }
            private set {
                this.keyValuesLoaded = value;
                this.NotifyPropertyChanged("KeyValuesLoaded");
            }
        }

        protected void NotifyPropertyChanged(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public virtual string Title {
            get {
                return null;
            }
        }

        public virtual Uri Icon {
            get {
                return null;
            }
        }

        public DcsDetailPanelBase() {
            this.Initializer.Register(this.Initialize);
            this.PropertyChanged += this.DcsDetailPanelBase_PropertyChanged;
        }

        private void DcsDetailPanelBase_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            if(e.PropertyName == "KeyValuesLoaded") {
                foreach(var textBox in this.ChildrenOfType<TextBox>()) {
                    var expression = textBox.GetBindingExpression(TextBox.TextProperty);
                    if(expression == null)
                        continue;
                    var binding = expression.ParentBinding;
                    if(!(binding.Converter is KeyValueItemConverter))
                        continue;
                    textBox.SetBinding(TextBox.TextProperty, binding);
                }
            }
        }

        private void Initialize() {
            this.KeyValueManager.LoadData(() => {
                this.KeyValuesLoaded = true;
            });
        }

        protected FrameworkElement CreateHorizontalLine(int columnIndex = 0, int rowIndex = 0, int columnSpan = 0, int rowSpan = 0) {
            var result = new Rectangle {
                Height = 1,
                Fill = new SolidColorBrush(Color.FromArgb(0xFF, 0xA0, 0xA0, 0xA0)),
                Margin = new Thickness(8, 2, 8, 5)
            };
            result.SetValue(Grid.ColumnProperty, columnIndex);
            result.SetValue(Grid.RowProperty, rowIndex);
            if(columnSpan > default(int))
                result.SetValue(Grid.ColumnSpanProperty, columnSpan);
            if(rowSpan > default(int))
                result.SetValue(Grid.RowSpanProperty, rowSpan);
            return result;
        }

        protected FrameworkElement CreateVerticalLine(int gridColumnIndex = 0, int gridRowIndex = 0, int columnSpan = 0, int rowSpan = 0) {
            var result = new Rectangle {
                Width = 1,
                Fill = new SolidColorBrush(Color.FromArgb(0xFF, 0xA0, 0xA0, 0xA0)),
                Margin = new Thickness(8, 0, 8, 0)
            };
            result.SetValue(Grid.ColumnProperty, gridColumnIndex);
            result.SetValue(Grid.RowProperty, gridRowIndex);
            if(columnSpan > default(int))
                result.SetValue(Grid.ColumnSpanProperty, columnSpan);
            if(rowSpan > default(int))
                result.SetValue(Grid.RowSpanProperty, rowSpan);
            return result;
        }
    }
}
