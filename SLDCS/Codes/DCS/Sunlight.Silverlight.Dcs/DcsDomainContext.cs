﻿using System;
using System.ComponentModel;
using System.ServiceModel;
using System.ServiceModel.DomainServices.Client;
using System.Threading;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class DcsDomainContext {
        private static EndpointAddress altAddress;
        private static bool AltAddressInitialized;
        private int activeInvokeCount;

        public bool IsInvoking {
            get {
                return this.activeInvokeCount > 0;
            }
        }

        public bool IsBusy {
            get {
                return IsLoading || IsSubmitting || this.IsInvoking;
            }
        }

        /// <summary>
        /// 若 App 的初始参数中包含有 Dcs 服务端地址，则采用此地址与对应 DcsDomainService 通信。
        /// </summary>
        public static EndpointAddress AltAddress {
            get {
                if (!AltAddressInitialized) {
                    var altServerAddress = BaseApp.Current.Resources[DcsUtils.DCS_SERVER_ADDRESS_KEY] as Uri;
                    Uri address;
                    if (Uri.TryCreate(altServerAddress, "/ClientBin/Sunlight-Silverlight-Dcs-Web-DcsDomainService.svc/binary", out address))
                        altAddress = new EndpointAddress(address);

                    AltAddressInitialized = true;
                }
                return altAddress;
            }
        }

        partial void OnCreated() {
            var endpoint = ((WebDomainClient<IDcsDomainServiceContract>)this.DomainClient).ChannelFactory.Endpoint;
            endpoint.Binding.SendTimeout = TimeSpan.FromMinutes(5);
            if (AltAddress != null)
                endpoint.Address = AltAddress;
            this.PropertyChanged += OnPropertyChanged;
            //endpoint.Behaviors.Add(AppClientMsgSettingBehavior);
        }

        private static readonly AppClientMsgSettingBehavior AppClientMsgSettingBehavior = new AppClientMsgSettingBehavior();

        private void OnPropertyChanged(object sender, PropertyChangedEventArgs e) {
            if (e.PropertyName == "IsLoading" || e.PropertyName == "IsSubmitting" || e.PropertyName == "IsInvoking") {
                this.RaisePropertyChanged("IsBusy");
            }
        }

        private void IncrementInvokeCount() {
            Interlocked.Increment(ref this.activeInvokeCount);
            if (this.activeInvokeCount == 1)
                this.RaisePropertyChanged("IsInvoking");
        }

        private void DecrementInvokeCount() {
            Interlocked.Decrement(ref this.activeInvokeCount);
            if (this.activeInvokeCount == 0)
                this.RaisePropertyChanged("IsInvoking");
        }

        private void InvokeOperation_Completed(object sender, EventArgs e) {
            this.DecrementInvokeCount();
            var operation = sender as OperationBase;
            if (operation != null)
                operation.Completed -= this.InvokeOperation_Completed;
        }

        public override InvokeOperation InvokeOperation(string operationName, Type returnType, System.Collections.Generic.IDictionary<string, object> parameters, bool hasSideEffects, Action<InvokeOperation> callback, object userState) {
            this.IncrementInvokeCount();
            var result = base.InvokeOperation(operationName, returnType, parameters, hasSideEffects, callback, userState);
            result.Completed += this.InvokeOperation_Completed;
            return result;
        }

        public override InvokeOperation<TValue> InvokeOperation<TValue>(string operationName, Type returnType, System.Collections.Generic.IDictionary<string, object> parameters, bool hasSideEffects, Action<InvokeOperation<TValue>> callback, object userState) {
            this.IncrementInvokeCount();
            var result = base.InvokeOperation(operationName, returnType, parameters, hasSideEffects, callback, userState);
            result.Completed += this.InvokeOperation_Completed;
            return result;
        }
    }
}
