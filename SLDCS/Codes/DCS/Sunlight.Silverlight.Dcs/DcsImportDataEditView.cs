﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;

namespace Sunlight.Silverlight.Dcs {
    public class DcsImportDataEditView : DcsDetailDataViewBase, INotifyPropertyChanged {
        protected readonly RadUpload uploader = new RadUpload();
        private ICommand uploadFileCommand, exportFileCommand, importFileCommand, clearUpCommand;
        private string selectedFileName;
        private ExcelServiceClient excelServiceClient;
        private object errorData;
        private bool canImportUse, canExportUse;
        public event PropertyChangedEventHandler PropertyChanged;

        protected override sealed void InitailizedUI() {
            base.InitailizedUI();
            this.DataContext = this;
            this.uploader.Filter = "Excel File|*.xlsx";
            this.uploader.FilterIndex = 0;
            this.uploader.IsAppendFilesEnabled = false;
            this.uploader.IsAutomaticUpload = this.IsAutomaticUploadFile;
            this.uploader.MaxFileCount = 1;
            this.uploader.MaxFileSize = 30000000;
            this.uploader.MaxUploadSize = 10000000;
            this.uploader.OverwriteExistingFiles = true;
            this.uploader.UploadServiceUrl = DcsUtils.GetUploadHandlerUrl().ToString();
            this.uploader.AdditionalPostFields.Add("Category", GlobalVar.UPLOAD_IMPORTFILE_DIR);
            this.uploader.FilesSelected -= this.Uploader_FilesSelected;
            this.uploader.FilesSelected += this.Uploader_FilesSelected;
            this.uploader.FileUploadStarting -= this.Uploader_FileUploadStarting;
            this.uploader.FileUploadStarting += this.Uploader_FileUploadStarting;
            this.uploader.FileUploaded -= uploader_FileUploaded;
            this.uploader.FileUploaded += uploader_FileUploaded;
            this.uploader.UploadFinished -= this.Uploader_UploadFinished;
            this.uploader.UploadFinished += this.Uploader_UploadFinished;
            this.Loaded -= DcsImportDataEditView_Loaded;
            this.Loaded += DcsImportDataEditView_Loaded;
        }

        private void DcsImportDataEditView_Loaded(object sender, RoutedEventArgs e) {
            this.HasImportingError = this.CanExportUse = this.CanImportUse = false;
            this.ErrorFileName = string.Empty;
        }

        private void Uploader_FileUploadStarting(object sender, FileUploadStartingEventArgs e) {
            this.SelectedFileName = e.UploadData.FileName;
            //取当前登录用户的HashCode来标记上传文件的唯一性
            //暂不考虑并发
            //e.UploadData.FileName = BaseApp.Current.CurrentUserData.GetHashCode().ToString(CultureInfo.InvariantCulture) + "_" + e.UploadData.FileName;
            UIHelper.ShowNotification(DcsUIStrings.DataEditView_Notification_PerparingImport);
        }

        private void Uploader_FilesSelected(object sender, FilesSelectedEventArgs e) {
            if(e.SelectedFiles.Any())
                try {
                    //尝试读取，出错说明文件已打开
                    using(e.SelectedFiles[0].File.OpenRead()) {
                    }
                    this.HasImportingError = (e.SelectedFiles.Any() && !string.IsNullOrWhiteSpace(e.SelectedFiles[0].Name));
                    if(HasImportingError) {
                        this.SelectedFileName = e.SelectedFiles[0].Name;
                    } else
                        e.Handled = true;
                } catch(Exception) {
                    UIHelper.ShowAlertMessage("请关闭选择的文件后再进行导入");
                    e.Handled = true;
                }
        }

        private void uploader_FileUploaded(object sender, FileUploadedEventArgs e) {
            this.UploadReturnFilePath = e.HandlerData.CustomData["Path"].ToString();
        }

        private void Uploader_UploadFinished(object sender, RoutedEventArgs e) {
            var upload = sender as RadUpload;
            if(upload == null || upload.CurrentSession == null || upload.CurrentSession.CurrentFile == null)
                return;
            upload.CancelUpload();
            if(this.UploadFileSuccessedProcessing == null)
                return;
            Action importAction = () => {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Notification_StartingImport);
                //取当前登录用户的HashCode来标记上传文件的唯一性
                //this.UploadFileSuccessedProcessing.Invoke(BaseApp.Current.CurrentUserData.GetHashCode().ToString(CultureInfo.InvariantCulture) + "_" + upload.CurrentSession.CurrentFile.Name);
                //暂不考虑并发
                this.UploadFileSuccessedProcessing.Invoke(this.UploadReturnFilePath);
            };
            if(this.IsAlertClearData && this.ImportErrorDataGridView != null)
                DcsUtils.Confirm(DcsUIStrings.DataEditView_Confirm_WillClearDetailData, importAction);
            else
                importAction.Invoke();
        }

        private void ShowBrowserFileDialog() {
            if(this.uploader != null)
                this.uploader.ShowFileDialog();
        }

        private void ExportValidationInfo() {
            if(this.ExportValidationInfoProcessing != null)
                this.ExportValidationInfoProcessing.Invoke();
            else if(this.CanExportUse && this.ImportErrorDataGridView != null)
                ((DcsDataGridViewBase)this.ImportErrorDataGridView).ExportData();
        }

        private void UploadFile() {
            if(this.IsAutomaticUploadFile) {
                if(this.IsAlertClearData && this.ImportErrorDataGridView != null)
                    DcsUtils.Confirm(DcsUIStrings.DataEditView_Confirm_WillClearDetailData, this.uploader.StartUpload);
                else
                    this.uploader.StartUpload();
            }
        }

        private void ClearUp() {
            this.ErrorData = null;
            this.HasImportingError = this.CanExportUse = this.CanImportUse = false;
            this.UploadFileName = this.SelectedFileName = this.UploadReturnFilePath = string.Empty;
        }

        protected override string GetTitleText() {
            return string.IsNullOrWhiteSpace(this.BusinessName) ? this.Title : string.Format(DcsUIStrings.DataEditView_Import, this.BusinessName);
        }

        /// <summary>
        /// 文件上传成功后，调用该方法进行上传后的处理。不支持异步调用
        /// <param>上传文件名 <name>fileName</name> </param>
        /// </summary>
        protected Action<string> UploadFileSuccessedProcessing;

        /// <summary>
        /// 定义特殊的异常信息导出处理
        /// </summary>
        protected Action ExportValidationInfoProcessing;

        protected virtual DataGridViewBase ImportErrorDataGridView {
            get {
                return null;
            }
        }

        protected void ExportFile(string errorFileName = null) {
            if(string.IsNullOrWhiteSpace(errorFileName)) {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Validation_NeedOutputErrorFileName);
                return;
            }
            this.ErrorFileName = errorFileName;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(ErrorFileName));
        }

        //private void client_OpenReadCompleted(object sender, OpenReadCompletedEventArgs e) {
        //    UIHelper.ShowNotification(DcsUIStrings.DataEditView_Notification_ExportedErrorData);
        //}

        protected ExcelServiceClient ExcelServiceClient {
            get {
                return this.excelServiceClient ?? (this.excelServiceClient = new ExcelServiceClient());
            }
        }

        protected virtual bool IsAutomaticUploadFile {
            get {
                return false;
            }
        }

        protected virtual void OnPropertyChanged(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        protected bool HasImportingError {
            get;
            set;
        }

        protected string ErrorFileName {
            get;
            set;
        }

        protected string UploadReturnFilePath {
            get;
            private set;
        }

        protected void ImportComplete() {
            this.CanImportUse = false;
            this.UploadFileName = this.SelectedFileName = string.Empty;
        }

        protected override void OnEditCancelled() {
            base.OnEditCancelled();
            this.ClearUp();
        }

        protected string UploadFileName {
            get;
            set;
        }

        protected bool IsAlertClearData {
            get;
            set;
        }

        public bool CanImportUse {
            get {
                return this.canImportUse;
            }
            set {
                this.canImportUse = value;
                this.OnPropertyChanged("CanImportUse");
            }
        }

        public string SelectedFileName {
            get {
                return this.selectedFileName;
            }
            set {
                this.selectedFileName = value;
                this.CanImportUse = !string.IsNullOrWhiteSpace(this.selectedFileName);
                this.OnPropertyChanged("SelectedFileName");
            }
        }

        public object ErrorData {
            get {
                return this.errorData;
            }
            set {
                this.errorData = value;
                this.CanExportUse = this.errorData != null;
                this.OnPropertyChanged("ErrorData");
            }
        }

        public bool CanExportUse {
            get {
                return this.canExportUse;
            }
            set {
                this.canExportUse = value;
                this.OnPropertyChanged("CanExportUse");
            }
        }

        public ICommand UploadFileCommand {
            get {
                return this.uploadFileCommand ?? (this.uploadFileCommand = new DelegateCommand(this.ShowBrowserFileDialog));
            }
        }

        public ICommand ExportFileCommand {
            get {
                return this.exportFileCommand ?? (this.exportFileCommand = new DelegateCommand(this.ExportValidationInfo));
            }
        }

        public ICommand ImportFileCommand {
            get {
                return this.importFileCommand ?? (this.importFileCommand = new DelegateCommand(this.UploadFile));
            }
        }

        public ICommand ClearUpCommand {
            get {
                return this.clearUpCommand ?? (this.clearUpCommand = new DelegateCommand(this.ClearUp));
            }
        }
    }
}
