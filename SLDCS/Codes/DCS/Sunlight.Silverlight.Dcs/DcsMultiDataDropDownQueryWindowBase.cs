﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Sunlight.Silverlight.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.Controls;
using Sunlight.Silverlight.Dcs.Resources;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs {
    public abstract class DcsMultiDataDropDownQueryWindowBase : MultiDataQueryWindowBase {
        private FilterItem additionalFilterItem;
        private FilterItem defaultFilterItem;

        public abstract string Title {
            get;
        }

        private void CreateUI() {
            var grid = new Grid();
            grid.RowDefinitions.Add(new RowDefinition {
                Height = GridLength.Auto
            });
            grid.RowDefinitions.Add(new RowDefinition {
                Height = GridLength.Auto
            });
            grid.RowDefinitions.Add(new RowDefinition());

            grid.Children.Add(new Border {
                Height = 28,
                Background = new LinearGradientBrush {
                    StartPoint = new Point(0.5, 0),
                    EndPoint = new Point(0.5, 1),
                    GradientStops = new GradientStopCollection {
                        new GradientStop {
                            Offset = 0,
                            Color = Color.FromArgb(0xFF, 0x83, 0x83, 0x83)
                        },
                        new GradientStop {
                            Offset = 0.4,
                            Color = Color.FromArgb(0xFF, 0x4D, 0x4D, 0x4D)
                        },
                        new GradientStop {
                            Offset = 0.4,
                            Color = Color.FromArgb(0xFF, 0x23, 0x23, 0x23)
                        },
                        new GradientStop {
                            Offset = 1,
                            Color = Color.FromArgb(0xFF, 0x58, 0x58, 0x58)
                        },
                    },
                },
            });

            var titleText = new TextBlock {
                HorizontalAlignment = HorizontalAlignment.Left,
                VerticalAlignment = VerticalAlignment.Center,
                Margin = new Thickness(8, 0, 8, 0),
                FontSize = 13,
                Foreground = new SolidColorBrush(Colors.White),
            };
            titleText.SetBinding(TextBlock.TextProperty, new Binding("Title") {
                Source = this,
                Mode = BindingMode.OneTime,
            });
            grid.Children.Add(titleText);

            var closeImage = new Image {
                Width = 15,
                Height = 15,
                HorizontalAlignment = HorizontalAlignment.Right,
                VerticalAlignment = VerticalAlignment.Center,
                Margin = new Thickness(0, 0, 8, 0),
                Cursor = Cursors.Hand,
                Stretch = Stretch.Fill,
                Source = new BitmapImage(new Uri("/Sunlight.Silverlight.Dcs;component/Images/dropdown-close.png", UriKind.Relative)),
            };
            closeImage.MouseLeftButtonDown += (sender, args) => {
                var button = this.ParentOfType<RadDropDownButton>();
                if(button != null)
                    button.IsOpen = false;
            };
            grid.Children.Add(closeImage);

            if(this.QueryPanel != null) {
                this.QueryPanel.HorizontalAlignment = HorizontalAlignment.Left;
                this.QueryPanel.SetValue(Grid.RowProperty, 1);
                grid.Children.Add(this.QueryPanel);
            }

            if(this.DataGridViewses.Count > 0 && this.GridViewTitles.Count() == this.DataGridViewses.Count) {
                var tabControl = new TabControl();
                for(int viewCount = 0; viewCount < this.DataGridViewses.Count; viewCount++) {
                    var tabItem = new TabItem();
                    if(this.GridViewTitles != null && this.GridViewTitles.Count() > viewCount)
                        tabItem.Header = this.GridViewTitles[viewCount];
                    tabItem.Content = this.DataGridViewses.ElementAt(viewCount).Value;
                    tabControl.Items.Add(tabItem);
                }
                if(tabControl.Items.Any()) {
                    tabControl.SetValue(Grid.RowProperty, 2);
                    grid.Children.Add(tabControl);
                }
            } else if(this.DataGridViewses.Count == 1 && !this.GridViewTitles.Any()) {
                var gridView = this.DataGridViewses.Values.ElementAt(0);
                gridView.SetValue(Grid.RowProperty, 2);
                grid.Children.Add(gridView);
            }

            if(this.AdditionalComponent != null) {
                this.AdditionalComponent.SetValue(Grid.RowProperty, 2);
                this.AdditionalComponent.HorizontalAlignment = HorizontalAlignment.Right;
                this.AdditionalComponent.VerticalAlignment = VerticalAlignment.Top;
                grid.Children.Add(this.AdditionalComponent);
            }

            this.Content = grid;
        }

        private void RefreshQueryResult() {
            if(this.DataGridView.Entities != null)
                this.QueryPanel.ExecuteQuery();
        }

        private void DcsDropDownQueryWindowBase_SelectionDecided(object sender, EventArgs e) {
            var dropDownTextBox = this.ParentOfType<DropDownTextBox>();
            if(dropDownTextBox != null)
                dropDownTextBox.IsOpen = false;
        }

        protected DcsMultiDataDropDownQueryWindowBase() {
            this.SelectionDecided += this.DcsDropDownQueryWindowBase_SelectionDecided;
            this.Initializer.Register(this.CreateUI);
            this.Content = new TextBlock {
                FontSize = 13,
                Margin = new Thickness(20),
                Text = DcsUIStrings.Loading,
            };
        }

        protected void SetDefaultFilterItem(FilterItem filterItem) {
            this.defaultFilterItem = filterItem;
        }

        protected void SetDefaultQueryItemValue(string queryItemGroupName, string queryItemName, object value) {
            this.QueryPanel.SetValue(queryItemGroupName, queryItemName, value);
        }

        protected void SetDefaultQueryItemEnable(string queryItemGroupName, string queryItemName, bool value) {
            this.QueryPanel.SetEnabled(queryItemGroupName, queryItemName, value);
        }

        protected override FilterItem OnRequestFilterItem(string gridViewKey, FilterItem filterItem) {
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.LogicalOperator = LogicalOperator.And;
            if(filterItem != null) {
                var originalFilterItem = filterItem as CompositeFilterItem;
                if(originalFilterItem != null)
                    foreach(var filter in originalFilterItem.Filters)
                        compositeFilterItem.Filters.Add(filter);
                else
                    compositeFilterItem.Filters.Add(filterItem);
            }
            if(this.defaultFilterItem != null)
                compositeFilterItem.Filters.Add(this.defaultFilterItem);

            if(this.additionalFilterItem != null) {
                compositeFilterItem.Filters.Add(this.additionalFilterItem);
                return compositeFilterItem;
            }
            return base.OnRequestFilterItem(gridViewKey, compositeFilterItem);
        }

        protected virtual Grid AdditionalComponent {
            get;
            set;
        }

        public override object ExchangeData(IBaseView sender, string subject, params object[] contents) {
            switch(subject) {
                case "SetQueryItemValue": {
                        if(contents.Length == 3) {
                            this.QueryPanel.SetValue(contents[0] as string, contents[1] as string, contents[2]);
                            this.RefreshQueryResult();
                            return true;
                        }
                        throw new ArgumentException(String.Format(DcsUIStrings.QueryWindow_ExchangeData_ExceptionMessage_Contents, "SetQueryItemValue", 3), "contents");
                    }
                case "SetQueryItemEnabled": {
                        if(contents.Length == 3) {
                            this.QueryPanel.SetEnabled(contents[0] as string, contents[1] as string, (bool)contents[2]);
                            return true;
                        }
                        throw new ArgumentException(String.Format(DcsUIStrings.QueryWindow_ExchangeData_ExceptionMessage_Contents, "SetQueryItemEnabled", 3), "contents");
                    }
                case "SetAdditionalFilterItem": {
                        if(contents.Length == 1) {
                            if(this.additionalFilterItem == null && contents[0] == null)
                                return false;
                            if(this.additionalFilterItem != null && this.additionalFilterItem.Equals(contents[0] as FilterItem))
                                return false;
                            this.additionalFilterItem = contents[0] as FilterItem;
                            this.RefreshQueryResult();
                            return true;
                        }
                        throw new ArgumentException(String.Format(DcsUIStrings.QueryWindow_ExchangeData_ExceptionMessage_Contents, "SetAdditionalFilterItem", 1), "contents");
                    }
                case "ExecuteQuery": {
                        this.QueryPanel.ExecuteQuery();
                        return true;
                    }
                case "RefreshQueryResult": {
                        this.RefreshQueryResult();
                        return true;
                    }
                default:
                    throw new ArgumentException(DcsUIStrings.QueryWindow_ExchangeData_Exception_Subject, "subject");
            }
        }
    }
}
