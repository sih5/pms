﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.Controls;
using Sunlight.Silverlight.Dcs.Resources;

namespace Sunlight.Silverlight.Dcs {
    public abstract class DcsMultiDataQueryWindowBase : MultiDataQueryWindowBase {
        private FilterItem additionalFilterItem;
        private FilterItem defaultFilterItem;

        protected DcsMultiDataQueryWindowBase() {
            this.Initializer.Register(this.CreateUI);
            this.Content = new TextBlock {
                FontSize = 13,
                Margin = new Thickness(20),
                Text = DcsUIStrings.Loading
            };
        }

        //为解决多选窗体可以处理传入参数，将传入参数FilterItem暴露给子类
        protected FilterItem AdditionalFilterItem {
            get {
                return additionalFilterItem;
            }
        }

        protected FilterItem DefaultFilterItem {
            get {
                return defaultFilterItem;
            }
        }

        protected virtual void CreateUI() {
            var grid = new Grid();
            grid.RowDefinitions.Add(new RowDefinition {
                Height = GridLength.Auto
            });
            grid.RowDefinitions.Add(new RowDefinition());

            if(this.QueryPanel != null) {
                this.QueryPanel.HorizontalAlignment = HorizontalAlignment.Left;
                grid.Children.Add(this.QueryPanel);
            }

            if(this.DataGridViewses.Count > 1) {
                var tabControl = new TabControl();
                for(int viewCount = 0; viewCount < this.DataGridViewses.Count; viewCount++) {
                    var tabItem = new TabItem();
                    if(this.GridViewTitles != null && this.GridViewTitles.Count() > viewCount)
                        tabItem.Header = this.GridViewTitles[viewCount];
                    tabItem.Content = this.DataGridViewses.ElementAt(viewCount).Value;
                    tabControl.Items.Add(tabItem);
                }
                if(tabControl.Items.Any()) {
                    tabControl.SetValue(Grid.RowProperty, 1);
                    grid.Children.Add(tabControl);
                }
            } else if(this.DataGridViewses.Count == 1) {
                var gridView = this.DataGridViewses.Values.ElementAt(0);
                gridView.SetValue(Grid.RowProperty, 1);
                grid.Children.Add(gridView);
            }

            if(this.AdditionalComponent != null) {
                this.AdditionalComponent.SetValue(Grid.RowProperty, 1);
                this.AdditionalComponent.Margin = new Thickness(0, 2, 0, 0);
                this.AdditionalComponent.HorizontalAlignment = HorizontalAlignment.Right;
                this.AdditionalComponent.VerticalAlignment = VerticalAlignment.Top;
                grid.Children.Add(this.AdditionalComponent);
            }

            this.Content = grid;
        }

        private void RefreshQueryResult() {
            this.QueryPanel.ExecuteQuery();
        }

        protected void SetDefaultFilterItem(FilterItem filterItem) {
            this.defaultFilterItem = filterItem;
        }

        protected void SetDefaultQueryItemValue(string queryItemGroupName, string queryItemName, object value) {
            this.QueryPanel.SetValue(queryItemGroupName, queryItemName, value);
        }

        protected void SetDefaultQueryItemEnable(string queryItemGroupName, string queryItemName, bool value) {
            this.QueryPanel.SetEnabled(queryItemGroupName, queryItemName, value);
        }

        protected override FilterItem OnRequestFilterItem(string gridViewKey, FilterItem filterItem) {
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.LogicalOperator = LogicalOperator.And;
            if(filterItem != null) {
                var originalFilterItem = filterItem as CompositeFilterItem;
                if(originalFilterItem != null)
                    foreach(var filter in originalFilterItem.Filters)
                        compositeFilterItem.Filters.Add(filter);
                else
                    compositeFilterItem.Filters.Add(filterItem);
            }
            if(this.defaultFilterItem != null)
                compositeFilterItem.Filters.Add(this.defaultFilterItem);

            if(this.additionalFilterItem != null) {
                compositeFilterItem.Filters.Add(this.additionalFilterItem);
                return compositeFilterItem;
            }
            return base.OnRequestFilterItem(gridViewKey, compositeFilterItem);
        }

        protected virtual Grid AdditionalComponent {
            get;
            set;
        }

        public override object ExchangeData(IBaseView sender, string subject, params object[] contents) {
            switch(subject) {
                case "SetQueryItemValue": {
                        if(contents.Length == 3) {
                            this.QueryPanel.SetValue(contents[0] as string, contents[1] as string, contents[2]);
                            return true;
                        }
                        throw new ArgumentException(String.Format(DcsUIStrings.QueryWindow_ExchangeData_ExceptionMessage_Contents, "SetQueryItemValue", 3), "contents");
                    }
                case "SetQueryItemEnabled": {
                        if(contents.Length == 3) {
                            this.QueryPanel.SetEnabled(contents[0] as string, contents[1] as string, (bool)contents[2]);
                            return true;
                        }
                        throw new ArgumentException(String.Format(DcsUIStrings.QueryWindow_ExchangeData_ExceptionMessage_Contents, "SetQueryItemEnabled", 3), "contents");
                    }
                case "SetAdditionalFilterItem": {
                        if(contents.Length == 1) {
                            if(this.additionalFilterItem == null && contents[0] == null)
                                return false;
                            if(this.additionalFilterItem != null && this.additionalFilterItem.Equals(contents[0] as FilterItem))
                                return false;
                            this.additionalFilterItem = contents[0] as FilterItem;
                            return true;
                        }
                        throw new ArgumentException(String.Format(DcsUIStrings.QueryWindow_ExchangeData_ExceptionMessage_Contents, "SetAdditionalFilterItem", 1), "contents");
                    }
                case "ExecuteQuery": {
                        this.QueryPanel.ExecuteQuery();
                        return true;
                    }
                case "RefreshQueryResult": {
                        this.RefreshQueryResult();
                        return true;
                    }
                default:
                    throw new ArgumentException(DcsUIStrings.QueryWindow_ExchangeData_Exception_Subject, "subject");
            }
        }
    }
}
