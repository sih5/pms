﻿


using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;

namespace Sunlight.Silverlight.Dcs {
    public abstract class DcsMultiPopupsQueryWindowBase : QueryWindowBase {
        private Button confirmButton;
        private FilterItem defaultFilterItem;
        private FilterItem additionalFilterItem;

        protected DcsMultiPopupsQueryWindowBase() {
            this.Initializer.Register(this.CreateUI);
            this.Content = new TextBlock {
                FontSize = 13,
                Margin = new Thickness(20),
                Text = DcsUIStrings.Loading
            };
        }

        public new event EventHandler SelectionDecided;

        private void CreateUI() {
            var grid = new Grid();
            grid.RowDefinitions.Add(new RowDefinition {
                Height = GridLength.Auto
            });
            grid.RowDefinitions.Add(new RowDefinition());

            var panelGrid = new Grid();
            if(this.QueryPanel != null) {
                panelGrid.ColumnDefinitions.Add(new ColumnDefinition {
                    Width = GridLength.Auto
                });
                //this.QueryPanel.HorizontalAlignment = HorizontalAlignment.Left;
                panelGrid.Children.Add(this.QueryPanel);
            }
            panelGrid.ColumnDefinitions.Add(new ColumnDefinition {
                Width = GridLength.Auto
            });
            var confirmButtonGrid = new Grid();
            confirmButtonGrid.RowDefinitions.Add(new RowDefinition {
                Height = GridLength.Auto
            });
            confirmButtonGrid.RowDefinitions.Add(new RowDefinition {
                Height = GridLength.Auto
            });
            confirmButtonGrid.RowDefinitions.Add(new RowDefinition {
                Height = new GridLength(12)
            });
            var confirmText = new TextBlock {
                Text = DcsUIStrings.QueryWindow_Save
            };
            confirmText.SetValue(Grid.RowProperty, 1);
            var image = new BitmapImage(Utils.MakeServerUri("Client/Dcs/Images/Operations/confirm.png"));
            var imageElement = new Image {
                Source = image
            };
            confirmButtonGrid.Children.Add(imageElement);
            confirmButtonGrid.Children.Add(confirmText);
            confirmButton = new Button {
                Content = confirmButtonGrid,
                Background = new SolidColorBrush(new Color {
                    A = 255,
                    B = 255,
                    G = 255,
                    R = 255
                }),
                BorderThickness = new Thickness(0),
                Command = new DelegateCommand(SelectedEntitiesDecided)
            };
            if(panelGrid.ColumnDefinitions.Any()) {
                confirmButton.SetValue(Grid.ColumnProperty, panelGrid.ColumnDefinitions.Count - 1);
                panelGrid.Children.Add(confirmButton);
            }

            var scrollViewer = new ScrollViewer();
            scrollViewer.Content = panelGrid;
            scrollViewer.VerticalScrollBarVisibility = ScrollBarVisibility.Disabled;
            scrollViewer.HorizontalScrollBarVisibility = ScrollBarVisibility.Auto;
            scrollViewer.HorizontalAlignment = HorizontalAlignment.Left;
            grid.Children.Add(scrollViewer);

            if(this.DataGridView != null) {
                this.DataGridView.SetValue(Grid.RowProperty, 1);
                grid.Children.Add(this.DataGridView);
            }

            this.Content = grid;
            var parent = this.ParentOfType<RadWindow>();
            if(parent == null)
                return;
            if(double.IsNaN(parent.Width))
                parent.Width = 1000;
            if(double.IsNaN(parent.Height))
                parent.Height = 600;
        }

        private void SelectedEntitiesDecided() {
            this.RaiseSelectionDecided();
        }

        protected virtual void RaiseSelectionDecided() {
            var selectionDecided = this.SelectionDecided;
            if(selectionDecided != null) {
                selectionDecided.Invoke(this, EventArgs.Empty);
            }
        }

        private void RefreshQueryResult() {
            if(this.DataGridView.Entities != null)
                this.QueryPanel.ExecuteQuery();
        }

        protected void SetDefaultFilterItem(FilterItem filterItem) {
            this.defaultFilterItem = filterItem;
        }

        protected void SetDefaultQueryItemValue(string queryItemGroupName, string queryItemName, object value) {
            this.QueryPanel.SetValue(queryItemGroupName, queryItemName, value);
        }

        protected void SetDefaultQueryItemEnable(string queryItemGroupName, string queryItemName, bool value) {
            this.QueryPanel.SetEnabled(queryItemGroupName, queryItemName, value);
        }

        public override object ExchangeData(IBaseView sender, string subject, params object[] contents) {
            switch(subject) {
                case "SetQueryItemValue": {
                        if(contents.Length == 3) {
                            this.QueryPanel.SetValue(contents[0] as string, contents[1] as string, contents[2]);
                            this.RefreshQueryResult();
                            return true;
                        }
                        throw new ArgumentException(String.Format(DcsUIStrings.QueryWindow_ExchangeData_ExceptionMessage_Contents, "SetQueryItemValue", 3), "contents");
                    }
                case "SetQueryItemEnabled": {
                        if(contents.Length == 3) {
                            this.QueryPanel.SetEnabled(contents[0] as string, contents[1] as string, (bool)contents[2]);
                            return true;
                        }
                        throw new ArgumentException(String.Format(DcsUIStrings.QueryWindow_ExchangeData_ExceptionMessage_Contents, "SetQueryItemEnabled", 3), "contents");
                    }
                case "SetAdditionalFilterItem": {
                        if(contents.Length == 1) {
                            if(this.additionalFilterItem == null && contents[0] == null)
                                return false;
                            if(this.additionalFilterItem != null && this.additionalFilterItem.Equals(contents[0] as FilterItem))
                                return false;
                            this.additionalFilterItem = contents[0] as FilterItem;
                            this.RefreshQueryResult();
                            return true;
                        }
                        throw new ArgumentException(String.Format(DcsUIStrings.QueryWindow_ExchangeData_ExceptionMessage_Contents, "SetAdditionalFilterItem", 1), "contents");
                    }
                case "ExecuteQuery": {
                        this.QueryPanel.ExecuteQuery();
                        return true;
                    }
                case "RefreshQueryResult": {
                        this.RefreshQueryResult();
                        return true;
                    }
                default:
                    throw new ArgumentException(DcsUIStrings.QueryWindow_ExchangeData_Exception_Subject, "subject");
            }
        }

        protected override FilterItem OnRequestFilterItem(FilterItem filterItem) {
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.LogicalOperator = LogicalOperator.And;
            if(filterItem != null) {
                var originalFilterItem = filterItem as CompositeFilterItem;
                if(originalFilterItem != null)
                    foreach(var filter in originalFilterItem.Filters)
                        compositeFilterItem.Filters.Add(filter);
                else
                    compositeFilterItem.Filters.Add(filterItem);
            }
            if(this.defaultFilterItem != null)
                compositeFilterItem.Filters.Add(this.defaultFilterItem);

            if(this.additionalFilterItem != null) {
                compositeFilterItem.Filters.Add(this.additionalFilterItem);
                return compositeFilterItem;
            }
            return base.OnRequestFilterItem(compositeFilterItem);
        }
    }
}
