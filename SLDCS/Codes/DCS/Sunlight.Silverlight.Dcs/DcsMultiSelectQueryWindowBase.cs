﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Resources;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;

namespace Sunlight.Silverlight.Dcs {
    public abstract class DcsMultiSelectQueryWindowBase : DcsMultiDataQueryWindowBase {
        private Button confirmButton;
        private bool isQueryMode;

        private void SelectedEntitiesDecided() {
            this.RaiseSelectionChanged();
            this.RaiseSelectionDecided();
        }

        protected override void CreateUI() {
            var grid = new Grid();
            grid.RowDefinitions.Add(new RowDefinition {
                Height = GridLength.Auto
            });
            grid.RowDefinitions.Add(new RowDefinition());

            var panelGrid = new Grid();
            if(this.QueryPanel != null) {
                panelGrid.ColumnDefinitions.Add(new ColumnDefinition {
                    Width = GridLength.Auto
                });
                this.QueryPanel.HorizontalAlignment = HorizontalAlignment.Left;
                panelGrid.Children.Add(this.QueryPanel);
            }
            panelGrid.ColumnDefinitions.Add(new ColumnDefinition {
                Width = GridLength.Auto
            });
            var confirmButtonGrid = new Grid();
            confirmButtonGrid.RowDefinitions.Add(new RowDefinition {
                Height = GridLength.Auto
            });
            confirmButtonGrid.RowDefinitions.Add(new RowDefinition {
                Height = GridLength.Auto
            });
            confirmButtonGrid.RowDefinitions.Add(new RowDefinition {
                Height = new GridLength(12)
            });
            var confirmText = new TextBlock {
                Text = DcsUIStrings.QueryWindow_Save
            };
            confirmText.SetValue(Grid.RowProperty, 1);
            var image = new BitmapImage(Utils.MakeServerUri("Client/Dcs/Images/Operations/confirm.png"));
            var imageElement = new Image {
                Source = image
            };
            confirmButtonGrid.Children.Add(imageElement);
            confirmButtonGrid.Children.Add(confirmText);
            confirmButton = new Button {
                Content = confirmButtonGrid,
                Background = new SolidColorBrush(new Color {
                    A = 255,
                    B = 255,
                    G = 255,
                    R = 255
                }),
                BorderThickness = new Thickness(0),
                Command = new DelegateCommand(SelectedEntitiesDecided)
            };
            if(this.isQueryMode)
                this.QueryMode();
            else
                this.SelectMode();
            if(panelGrid.ColumnDefinitions.Any()) {
                confirmButton.SetValue(Grid.ColumnProperty, panelGrid.ColumnDefinitions.Count - 1);
                panelGrid.Children.Add(confirmButton);
            }
            grid.Children.Add(panelGrid);

            if(this.DataGridViewses != null) {
                if(this.DataGridViewses.Count > 1) {
                    var tabControl = new TabControl();
                    for(int viewCount = 0; viewCount < this.DataGridViewses.Count; viewCount++) {
                        var tabItem = new TabItem();
                        if(this.GridViewTitles != null && this.GridViewTitles.Count() > viewCount)
                            tabItem.Header = this.GridViewTitles[viewCount];
                        tabItem.Content = this.DataGridViewses.ElementAt(viewCount).Value;
                        tabControl.Items.Add(tabItem);
                    }
                    if(tabControl.Items.Any()) {
                        tabControl.SetValue(Grid.RowProperty, 1);
                        grid.Children.Add(tabControl);
                    }
                } else if(this.DataGridViewses.Count == 1) {
                    var gridView = this.DataGridViewses.Values.ElementAt(0);
                    gridView.SetValue(Grid.RowProperty, 1);
                    grid.Children.Add(gridView);
                }
            } else if(this.CustomerView != null) {
                this.CustomerView.SetValue(Grid.RowProperty, 1);
                grid.Children.Add(this.CustomerView);
            }

            if(this.AdditionalComponent != null) {
                this.AdditionalComponent.SetValue(Grid.RowProperty, 1);
                this.AdditionalComponent.Margin = new Thickness(0, 2, 0, 0);
                this.AdditionalComponent.HorizontalAlignment = HorizontalAlignment.Right;
                this.AdditionalComponent.VerticalAlignment = VerticalAlignment.Top;
                grid.Children.Add(this.AdditionalComponent);
            }

            this.Content = grid;
        }

        public abstract new IEnumerable<Entity> SelectedEntities {
            get;
        }

        public void QueryMode() {
            if(this.confirmButton == null)
                this.isQueryMode = true;
            else
                this.confirmButton.Visibility = Visibility.Collapsed;
        }

        public void SelectMode() {
            if(this.confirmButton == null)
                this.isQueryMode = false;
            else
                this.confirmButton.Visibility = Visibility.Visible;
        }
    }
}
