﻿using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs {
    public abstract class DcsQueryPanelBase : QueryPanelBase {
        private KeyValueManager keyValueManager;

        protected KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        private void Initialize() {
            this.KeyValueManager.LoadData();
        }

        protected DcsQueryPanelBase() {
            this.Initializer.Register(this.Initialize);
        }
    }
}
