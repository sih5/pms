﻿using System;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs {
    public abstract class DcsQueryWindowBase : QueryWindowBase {
        private FilterItem additionalFilterItem;
        private FilterItem defaultFilterItem;
        protected bool QueryPanelVisuable = true;

        private void CreateUI() {
            var grid = new Grid();
            grid.RowDefinitions.Add(new RowDefinition {
                Height = GridLength.Auto
            });
            grid.RowDefinitions.Add(new RowDefinition());

            if(this.QueryPanel != null) {
                var scrollViewer = new ScrollViewer();
                scrollViewer.Content = this.QueryPanel;
                scrollViewer.VerticalScrollBarVisibility = ScrollBarVisibility.Disabled;
                scrollViewer.HorizontalScrollBarVisibility = ScrollBarVisibility.Auto;
                scrollViewer.HorizontalAlignment = HorizontalAlignment.Left;
                grid.Children.Add(scrollViewer);
            }

            if(this.DataGridView != null) {
                this.DataGridView.SetValue(Grid.RowProperty, 1);
                grid.Children.Add(this.DataGridView);
            }

            this.Content = grid;
            var parent = this.ParentOfType<RadWindow>();
            if(parent == null)
                return;
            if(double.IsNaN(parent.Width))
                parent.Width = 1000;
            if(double.IsNaN(parent.Height))
                parent.Height = 600;
        }
        private void RefreshQueryResult() {
            if(this.DataGridView != null)
                this.QueryPanel.ExecuteQuery();
        }

        protected DcsQueryWindowBase() {
            this.Initializer.Register(this.CreateUI);
            this.Content = new TextBlock {
                FontSize = 13,
                Margin = new Thickness(20),
                Text = DcsUIStrings.Loading,
            };
        }

        protected override FilterItem OnRequestFilterItem(FilterItem filterItem) {
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.LogicalOperator = LogicalOperator.And;
            if(filterItem != null) {
                var originalFilterItem = filterItem as CompositeFilterItem;
                if(originalFilterItem != null)
                    foreach(var filter in originalFilterItem.Filters)
                        compositeFilterItem.Filters.Add(filter);
                else
                    compositeFilterItem.Filters.Add(filterItem);
            }
            if(this.defaultFilterItem != null)
                compositeFilterItem.Filters.Add(this.defaultFilterItem);

            if(this.additionalFilterItem != null) {
                compositeFilterItem.Filters.Add(this.additionalFilterItem);
                return compositeFilterItem;
            }
            return base.OnRequestFilterItem(compositeFilterItem);
        }

        protected void SetDefaultFilterItem(FilterItem filterItem) {
            this.defaultFilterItem = filterItem;
        }

        protected void SetDefaultQueryItemValue(string queryItemGroupName, string queryItemName, object value) {
            this.QueryPanel.SetValue(queryItemGroupName, queryItemName, value);
        }

        protected void SetDefaultQueryItemEnable(string queryItemGroupName, string queryItemName, bool value) {
            this.QueryPanel.SetEnabled(queryItemGroupName, queryItemName, value);
        }

        public override object ExchangeData(IBaseView sender, string subject, params object[] contents) {
            switch(subject) {
                case "SetQueryItemValue": {
                        if(contents.Length == 3) {
                            this.QueryPanel.SetValue(contents[0] as string, contents[1] as string, contents[2]);
                            this.RefreshQueryResult();
                            return true;
                        }
                        throw new ArgumentException(String.Format(DcsUIStrings.QueryWindow_ExchangeData_ExceptionMessage_Contents, "SetQueryItemValue", 3), "contents");
                    }
                case "SetQueryItemValueNotQuery": {
                        if(contents.Length == 3) {
                            this.QueryPanel.SetValue(contents[0] as string, contents[1] as string, contents[2]);
                            // this.RefreshQueryResult();//这个方法将进行一次查询
                            return true;
                        }
                        throw new ArgumentException(String.Format(DcsUIStrings.QueryWindow_ExchangeData_ExceptionMessage_Contents, "SetQueryItemValue", 3), "contents");
                    }
                case "SetQueryItemEnabled": {
                        if(contents.Length == 3) {
                            this.QueryPanel.SetEnabled(contents[0] as string, contents[1] as string, (bool)contents[2]);
                            return true;
                        }
                        throw new ArgumentException(String.Format(DcsUIStrings.QueryWindow_ExchangeData_ExceptionMessage_Contents, "SetQueryItemEnabled", 3), "contents");
                    }
                case "SetAdditionalFilterItem":
                case "SetAdditionalFilterItemNotQuery": {
                        if(contents.Length != 1)
                            throw new ArgumentException(String.Format(DcsUIStrings.QueryWindow_ExchangeData_ExceptionMessage_Contents, subject, 1), "contents");
                        if(this.additionalFilterItem == null && contents[0] == null)
                            return false;
                        if(this.additionalFilterItem != null && this.additionalFilterItem.Equals(contents[0] as FilterItem))
                            return false;
                        this.additionalFilterItem = contents[0] as FilterItem;
                        if("SetAdditionalFilterItem" == subject)
                            this.RefreshQueryResult();
                        return true;
                    }
                case "ExecuteQuery": {
                        this.QueryPanel.ExecuteQuery();
                        return true;
                    }
                case "RefreshQueryResult": {
                        this.RefreshQueryResult();
                        return true;
                    }
                default:
                    throw new ArgumentException(DcsUIStrings.QueryWindow_ExchangeData_Exception_Subject, "subject");
            }
        }

        public void Close() {
            var parent = this.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }
    }
}
