﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs {
    public static class DcsUtils {
        private const string DEFAULT_CATEGORY = "DCS";
        internal const string DCS_SERVER_ADDRESS_KEY = "DcsServerAddress";
        internal static readonly Dictionary<string, KeyValuePair[]> KeyValueItems = new Dictionary<string, KeyValuePair[]>();
        internal static readonly object KeyValueItemsLock = new object();

        private static void ShowDomainServiceOperationWindowInternal(object context) {
            (new RadWindow {
                Header = ((BaseApp)Application.Current).ApplicationName,
                WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen,
                IsTopmost = true,
                ResizeMode = ResizeMode.NoResize,
                CanClose = true,
                CanMove = true,
                Content = new DomainServiceOperationView {
                    Margin = new Thickness(10),
                    DataContext = context,
                },
            }).ShowDialog();
        }

        public static void GetKeyValuePairs(IDictionary<string, ObservableCollection<KeyValuePair>> collections) {
            GetKeyValuePairs(DEFAULT_CATEGORY, collections, null);
        }

        public static void GetKeyValuePairs(IDictionary<string, ObservableCollection<KeyValuePair>> collections, Action callback) {
            GetKeyValuePairs(DEFAULT_CATEGORY, collections, callback);
        }

        public static void GetKeyValuePairs(string category, IDictionary<string, ObservableCollection<KeyValuePair>> collections) {
            GetKeyValuePairs(category, collections, null);
        }

        public static void GetKeyValuePairs(string category, IDictionary<string, ObservableCollection<KeyValuePair>> collections, Action callback) {
            if(collections == null)
                throw new ArgumentNullException("collections");

            ShellViewModel.Current.IsBusy = true;

            var namesToQuery = new List<string>();
            foreach(var kv in collections.Where(kv => kv.Value != null)) {
                bool success;
                KeyValuePair[] keyValueItems;
                lock(KeyValueItemsLock)
                    success = KeyValueItems.TryGetValue(String.Concat(category, "|", kv.Key), out keyValueItems);
                if(success) {
                    kv.Value.Clear();
                    foreach(var keyValueItem in keyValueItems)
                        kv.Value.Add(keyValueItem);
                } else
                    namesToQuery.Add(kv.Key);
            }
            if(!namesToQuery.Any()) {
                try {
                    if(callback != null)
                        callback();
                } finally {
                    ShellViewModel.Current.IsBusy = false;
                }
                return;
            }

            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetKeyValueItemsByMultipleNamesQuery(category, namesToQuery.ToArray(), null), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var name in namesToQuery) {
                    var keyValueItems = loadOp.Entities.Where(e => e.Name == name).Select(e => new KeyValuePair {
                        Key = e.Key,
                        Value = e.Value,
                    }).OrderBy(kvp => kvp.Key).ToArray();
                    lock(KeyValueItemsLock) {
                        var cacheKey = String.Concat(category, "|", name);
                        if(!KeyValueItems.ContainsKey(cacheKey))
                            KeyValueItems.Add(cacheKey, keyValueItems);
                    }
                    var collection = collections[name];
                    collection.Clear();
                    foreach(var keyValueItem in keyValueItems)
                        collection.Add(keyValueItem);
                }
                try {
                    if(callback != null)
                        callback();
                } finally {
                    ShellViewModel.Current.IsBusy = false;
                }
            }, null);
        }

        internal static IEnumerable<KeyValuePair> GetKeyValuePairs(string cacheKey) {
            KeyValuePair[] result;
            return KeyValueItems.TryGetValue(cacheKey, out result) ? result : Enumerable.Empty<KeyValuePair>();
        }

        /// <summary>
        ///     根据提交操作中服务端返回的验证信息拼接出验证结果字符串。
        /// </summary>
        /// <param name="submitOperation"> 提交操作 </param>
        public static void ShowSubmitError(SubmitOperation submitOperation) {
            string errorMessage;
            if(submitOperation.EntitiesInError.Any())
                errorMessage = string.Join("\r\n", from entityInError in submitOperation.EntitiesInError
                                                   from validationError in entityInError.ValidationErrors
                                                   where entityInError.HasValidationErrors
                                                   select validationError.ErrorMessage);
            else
                errorMessage = submitOperation.Error.Message;
            UIHelper.ShowAlertMessage(errorMessage);
        }

        /// <summary>
        ///     根据加载操作中服务端返回的验证信息拼接出验证结果字符串。
        /// </summary>
        /// <param name="loadOperation"> 加载操作 </param>
        public static void ShowLoadError(LoadOperation loadOperation) {
            string errorMessage;
            if(loadOperation.ValidationErrors.Any())
                errorMessage = string.Join("\r\n", from validationError in loadOperation.ValidationErrors
                                                   select validationError.ErrorMessage);
            else
                errorMessage = loadOperation.Error.Message;
            UIHelper.ShowAlertMessage(errorMessage);
        }

        public static void ShowDomainServiceOperationWindow(Exception ex) {
            ShowDomainServiceOperationWindowInternal(ex);
        }

        public static void ShowDomainServiceOperationWindow(OperationBase operation) {
            ShowDomainServiceOperationWindowInternal(operation);
        }

        public static void Confirm(string text, Action action) {
            if(action == null)
                throw new ArgumentNullException("action");

            var parameters = new DialogParameters {
                Header = BaseApp.Current.ApplicationName,
                Content = text,
            };
            parameters.Closed += (sender, args) => {
                if(args.DialogResult.HasValue && args.DialogResult.Value)
                    action();
            };
            RadWindow.Confirm(parameters);
        }

        public static void Confirm(string text, Action yesAction, Action noAction) {
            if(yesAction == null)
                throw new ArgumentNullException("yesAction");
            if(noAction == null)
                throw new ArgumentNullException("noAction");
            var parameters = new DialogParameters {
                Header = BaseApp.Current.ApplicationName,
                Content = text,
            };
            parameters.Closed += (sender, args) => {
                if(args.DialogResult.HasValue && args.DialogResult.Value)
                    yesAction();
                else
                    noAction();
            };
            RadWindow.Confirm(parameters);
        }

        public static void Confirm(string text, Action yesAction, Action noAction, string okButtonContent, string cancelButtonContent) {
            if(yesAction == null)
                throw new ArgumentNullException("yesAction");
            if(noAction == null)
                throw new ArgumentNullException("noAction");
            var parameters = new DialogParameters {
                Header = BaseApp.Current.ApplicationName,
                Content = text,
                OkButtonContent = okButtonContent,
                CancelButtonContent = cancelButtonContent
            };
            parameters.Closed += (sender, args) => {
                if(args.DialogResult.HasValue && args.DialogResult.Value)
                    yesAction();
                else if(args.DialogResult.HasValue && (bool)args.DialogResult.Value == false)
                    noAction();
            };
            RadWindow.Confirm(parameters);
        }

        public static CompositeFilterItem ToCompositeFilterItem(FilterItem filterItem) {
            var newCompositeFilterItem = new CompositeFilterItem();
            if(filterItem is CompositeFilterItem)
                foreach(var filter in ((CompositeFilterItem)filterItem).Filters)
                    newCompositeFilterItem.Filters.Add(filter);
            else if(filterItem != null)
                newCompositeFilterItem.Filters.Add(filterItem);
            return newCompositeFilterItem;
        }

        /// <summary>
        /// 获取服务端文件的下载路径
        /// </summary>
        /// <param name="path">相对于服务端存储附件根目录的路径</param>
        /// <returns></returns>
        public static Uri GetDownloadFileUrl(string path) {
            if(BaseApp.Current.Resources[DCS_SERVER_ADDRESS_KEY] != null) {
                return new Uri(string.Format("{0}{1}{2}", BaseApp.Current.Resources[DCS_SERVER_ADDRESS_KEY], GlobalVar.DOWNLOAD_FILE_URL_PREFIX, path));
            } else
                return Core.Utils.MakeServerUri(string.Format("{0}{1}", GlobalVar.DOWNLOAD_FILE_URL_PREFIX, path));
        }

        /// <summary>
        /// 获取服务端文件的下载路径
        /// </summary>
        /// <param name="path">相对于服务端存储附件根目录的路径</param>
        /// <param name="ftpServerKey">相对于服务端存储附件Ftp配置Key</param>
        /// <returns></returns>
        public static Uri GetDownloadFileUrl(string path, string ftpServerKey) {
            if(BaseApp.Current.Resources[DCS_SERVER_ADDRESS_KEY] != null) {
                return new Uri(string.Format("{0}{1}{2}&FtpServerKey={3}", BaseApp.Current.Resources[DCS_SERVER_ADDRESS_KEY], GlobalVar.DOWNLOAD_FILE_URL_PREFIX, path, ftpServerKey));
            } else
                return Core.Utils.MakeServerUri(string.Format("{0}{1}&FtpServerKey={2}", GlobalVar.DOWNLOAD_FILE_URL_PREFIX, path, ftpServerKey));
        }

        /// <summary>
        /// 获取服务端上传文件处理程序的路径
        /// </summary>
        /// <returns></returns>
        public static Uri GetUploadHandlerUrl() {
            if(BaseApp.Current.Resources[DCS_SERVER_ADDRESS_KEY] != null)
                return new Uri(string.Format("{0}{1}", BaseApp.Current.Resources[DCS_SERVER_ADDRESS_KEY], GlobalVar.UPLOAD_FILE_URL_PERFIX));
            else
                return Core.Utils.MakeServerUri(GlobalVar.UPLOAD_FILE_URL_PERFIX);
        }

        /// <summary>
        /// 获取头像文件的下载路径
        /// </summary>
        /// <param name="photo">用户的头像文件存储路径</param>
        /// <returns></returns>
        public static Uri GetPersonnelPhotoUri(string photo) {
            if(string.IsNullOrEmpty(photo))
                return null;
            return photo == GlobalVar.DEFAULT_AVATAR_URL ? Core.Utils.MakeServerUri(photo) : GetDownloadFileUrl(photo);
        }

        /// <summary>
        ///     将服务端上的相对路径转换为服务端上的绝对路径，通常用于客户端下载服务端文件。
        /// </summary>
        /// <param name="relativePath"> 服务端的相对路径，因为程序可能部署在虚拟目录下，所以不要以“/”符号开头。 </param>
        /// <returns> 与该相对路径所对应的绝对路径。 </returns>
        /// <exception cref="InvalidOperationException">当前应用程序没有运行于某个网站环境之下。</exception>
        public static Uri MakeServerUri(string relativePath) {
            if(BaseApp.Current.Resources[DCS_SERVER_ADDRESS_KEY] != null) {
                return new Uri(string.Format("{0}{1}", BaseApp.Current.Resources[DCS_SERVER_ADDRESS_KEY], relativePath));
            } else {
                return Core.Utils.MakeServerUri(relativePath);
            }

        }

        public static void ShowInvokeError(InvokeOperation inVokeOperation) {
            string errorMessage;
            if(inVokeOperation.ValidationErrors.Any())
                errorMessage = string.Join("\r\n", from validationError in inVokeOperation.ValidationErrors
                                                   select validationError.ErrorMessage);
            else
                errorMessage = inVokeOperation.Error.Message;
            UIHelper.ShowAlertMessage(errorMessage);
        }
    }
}
