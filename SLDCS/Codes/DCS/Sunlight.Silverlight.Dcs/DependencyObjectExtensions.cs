﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;

namespace Sunlight.Silverlight.Dcs {
    public static class DependencyObjectExtensions {
        public static IEnumerable<T> FindChildrenByType<T>(this DependencyObject source) where T : DependencyObject {
            var dependencyObjects = new Queue<DependencyObject>();
            dependencyObjects.Enqueue(source);
            while(dependencyObjects.Count > 0) {
                var dependencyObject = dependencyObjects.Dequeue();
                var num = 0;
                while(num < VisualTreeHelper.GetChildrenCount(dependencyObject)) {
                    var child = VisualTreeHelper.GetChild(dependencyObject, num);
                    var t = child as T;
                    if(t != null) {
                        yield return t;
                    }
                    dependencyObjects.Enqueue(child);
                    num = num + 1;
                }
            }
        }
    }
}
