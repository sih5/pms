#首先得执行命令 powershell Set-ExecutionPolicy Unrestricted
$input = Read-Host "请输入端口号"
echo "开始生成代理类..."
$path="..\..\DCS\Sunlight.Silverlight.Dcs"
cd $path
Start-Process -FilePath "C:\Program Files (x86)\Microsoft SDKs\Silverlight\v5.0\Tools\SlSvcUtil.exe" -ArgumentList "http://localhost:$input/ExcelService.svc /edb /n:""*,Sunlight.Silverlight.Dcs"" /r:""C:\Program Files (x86)\Microsoft Silverlight\5.1.50918.0\System.Windows.dll"" /o:ExcelServiceClient.cs" -NoNewWindow -Wait
echo "删除配置文件"

echo "配置文件信息加入生成的代理文件中"
$file=get-childitem ExcelServiceClient.cs
$content=[IO.File]::ReadAllText($file,[Text.Encoding]::UTF8)
$regex = [regex]"[ ]*public ExcelServiceClient\(\)\r\n[ ]*{\r\n[ ]*}\r\n"
$content=$regex.Replace($content,"        public ExcelServiceClient()
            : base(new BasicHttpBinding(Application.Current.Host.Source.Scheme == Uri.UriSchemeHttps ? BasicHttpSecurityMode.Transport : BasicHttpSecurityMode.None) {
                MaxBufferSize = Int32.MaxValue,
                MaxReceivedMessageSize = Int32.MaxValue,
				SendTimeout = new TimeSpan(0, 10, 0)
            }, new EndpointAddress(DcsUtils.MakeServerUri(""ExcelService.svc""))) {
            this.Endpoint.Behaviors.Add(new AppClientMsgSettingBehavior());
        }
        ");
        
$regex = [regex]"namespace Sunlight.Silverlight.Dcs\r\n{"
$content=$regex.Replace($content,"namespace Sunlight.Silverlight.Dcs {
    using System;
    using System.ServiceModel;
    using System.Windows;
    using Sunlight.Silverlight.Core;");
[IO.File]::WriteAllText($file,$content,[Text.Encoding]::UTF8)
echo "结束"
Start-Sleep -s 300
