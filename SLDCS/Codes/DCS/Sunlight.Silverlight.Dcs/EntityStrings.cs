﻿using System.Linq;
using Sunlight.Silverlight.Core.Model;

namespace Sunlight.Silverlight.Dcs {
    public class EntityStrings : ModelBase {
        public void Refresh(params string[] propertyNames) {
            this.NotifyOfPropertiesChange(propertyNames.Length == 0 ? this.GetType().GetProperties().Select(p => p.Name).ToArray() : propertyNames);
        }

        public string PartsBranch_BranchName {
            get {
                return Web.Resources.EntityStrings.PartsBranch_BranchName;
            }
        }

        public string PartsBranch_IsDirectSupply {
            get {
                return Web.Resources.EntityStrings.PartsBranch_IsDirectSupply;
            }
        }

        public string PartsBranch_IsOrderable {
            get {
                return Web.Resources.EntityStrings.PartsBranch_IsOrderable;
            }
        }

        public string PartsBranch_IsSalable {
            get {
                return Web.Resources.EntityStrings.PartsBranch_IsSalable;
            }
        }

        public string PartsBranch_IsService {
            get {
                return Web.Resources.EntityStrings.PartsBranch_IsService;
            }
        }

        public string PartsBranch_PartABC {
            get {
                return Web.Resources.EntityStrings.PartsBranch_PartABC;
            }
        }

        public string PartsBranch_PartCode {
            get {
                return Web.Resources.EntityStrings.PartsBranch_PartCode;
            }
        }

        public string PartsBranch_PartName {
            get {
                return Web.Resources.EntityStrings.PartsBranch_PartName;
            }
        }

        public string PartsBranch_PurchaseCycle {
            get {
                return Web.Resources.EntityStrings.PartsBranch_PurchaseCycle;
            }
        }

        public string PartsBranch_PurchaseRoute {
            get {
                return Web.Resources.EntityStrings.PartsBranch_PurchaseRoute;
            }
        }

        public string _Common_AbandonerName {
            get {
                return Web.Resources.EntityStrings._Common_AbandonerName;
            }
        }

        public string _Common_AbandonTime {
            get {
                return Web.Resources.EntityStrings._Common_AbandonTime;
            }
        }

        public string _Common_CreateTime {
            get {
                return Web.Resources.EntityStrings._Common_CreateTime;
            }
        }

        public string _Common_CreatorId {
            get {
                return Web.Resources.EntityStrings._Common_CreatorId;
            }
        }

        public string _Common_CreatorName {
            get {
                return Web.Resources.EntityStrings._Common_CreatorName;
            }
        }

        public string _Common_ModifierId {
            get {
                return Web.Resources.EntityStrings._Common_ModifierId;
            }
        }

        public string _Common_ModifierName {
            get {
                return Web.Resources.EntityStrings._Common_ModifierName;
            }
        }

        public string _Common_ModifyTime {
            get {
                return Web.Resources.EntityStrings._Common_ModifyTime;
            }
        }

        public string _Common_Status {
            get {
                return Web.Resources.EntityStrings._Common_Status;
            }
        }

        public string PartsBranch_MinSaleQuantity {
            get {
                return Web.Resources.EntityStrings.PartsBranch_MinSaleQuantity;
            }
        }

        public string _Common_Remark {
            get {
                return Web.Resources.EntityStrings._Common_Remark;
            }
        }

        public string PartsReplacement_NewPartCode {
            get {
                return Web.Resources.EntityStrings.PartsReplacement_NewPartCode;
            }
        }

        public string PartsReplacement_NewPartName {
            get {
                return Web.Resources.EntityStrings.PartsReplacement_NewPartName;
            }
        }

        public string PartsReplacement_NewPartQuantity {
            get {
                return Web.Resources.EntityStrings.PartsReplacement_NewPartQuantity;
            }
        }

        public string PartsReplacement_OldPartCode {
            get {
                return Web.Resources.EntityStrings.PartsReplacement_OldPartCode;
            }
        }

        public string PartsReplacement_OldPartName {
            get {
                return Web.Resources.EntityStrings.PartsReplacement_OldPartName;
            }
        }

        public string PartsReplacement_OldPartQuantity {
            get {
                return Web.Resources.EntityStrings.PartsReplacement_OldPartQuantity;
            }
        }

        public string PartsReplacement_ReplacementType {
            get {
                return Web.Resources.EntityStrings.PartsReplacement_ReplacementType;
            }
        }

        public string SparePart_CADCode {
            get {
                return Web.Resources.EntityStrings.SparePart_CADCode;
            }
        }

        public string SparePart_CADName {
            get {
                return Web.Resources.EntityStrings.SparePart_CADName;
            }
        }

        public string SparePart_Feature {
            get {
                return Web.Resources.EntityStrings.SparePart_Feature;
            }
        }

        public string SparePart_LossType {
            get {
                return Web.Resources.EntityStrings.SparePart_LossType;
            }
        }

        public string SparePart_MeasureUnit {
            get {
                return Web.Resources.EntityStrings.SparePart_MeasureUnit;
            }
        }

        public string SparePart_PartType {
            get {
                return Web.Resources.EntityStrings.SparePart_PartType;
            }
        }

        public string SparePart_Specification {
            get {
                return Web.Resources.EntityStrings.SparePart_Specification;
            }
        }

        public string SparePart_Code {
            get {
                return Web.Resources.EntityStrings.SparePart_Code;
            }
        }

        public string SparePart_Name {
            get {
                return Web.Resources.EntityStrings.SparePart_Name;
            }
        }

        public string ABCStrategy_BranchName {
            get {
                return Web.Resources.EntityStrings.ABCStrategy_BranchName;
            }
        }

        public string ABCStrategy_Category {
            get {
                return Web.Resources.EntityStrings.ABCStrategy_Category;
            }
        }

        public string ABCStrategy_Percentage {
            get {
                return Web.Resources.EntityStrings.ABCStrategy_Percentage;
            }
        }

        public string ABCStrategy_ReferenceBaseType {
            get {
                return Web.Resources.EntityStrings.ABCStrategy_ReferenceBaseType;
            }
        }

        public string ABCStrategy_SamplingDuration {
            get {
                return Web.Resources.EntityStrings.ABCStrategy_SamplingDuration;
            }
        }

        public string PartsSupplier_BusinessManager {
            get {
                return Web.Resources.EntityStrings.PartsSupplier_BusinessManager;
            }
        }

        public string PartsSupplier_Code {
            get {
                return Web.Resources.EntityStrings.PartsSupplier_Code;
            }
        }

        public string PartsSupplier_DutyPhone {
            get {
                return Web.Resources.EntityStrings.PartsSupplier_DutyPhone;
            }
        }

        public string PartsSupplier_ManagerMobile {
            get {
                return Web.Resources.EntityStrings.PartsSupplier_ManagerMobile;
            }
        }

        public string PartsSupplier_Name {
            get {
                return Web.Resources.EntityStrings.PartsSupplier_Name;
            }
        }

        public string PartsSupplier_QuickCode {
            get {
                return Web.Resources.EntityStrings.PartsSupplier_QuickCode;
            }
        }

        public string PartsSupplier_ShortName {
            get {
                return Web.Resources.EntityStrings.PartsSupplier_ShortName;
            }
        }

        public string PartsSupplier_SupplierType {
            get {
                return Web.Resources.EntityStrings.PartsSupplier_SupplierType;
            }
        }

        public string PartsSupplier_TaxpayerKind {
            get {
                return Web.Resources.EntityStrings.PartsSupplier_TaxpayerKind;
            }
        }

        public string PartsSupplierRelation_EconomicalBatch {
            get {
                return Web.Resources.EntityStrings.PartsSupplierRelation_EconomicalBatch;
            }
        }

        public string PartsSupplierRelation_IsPrimary {
            get {
                return Web.Resources.EntityStrings.PartsSupplierRelation_IsPrimary;
            }
        }

        public string PartsSupplierRelation_MinBatch {
            get {
                return Web.Resources.EntityStrings.PartsSupplierRelation_MinBatch;
            }
        }

        public string PartsSupplierRelation_PurchasePercentage {
            get {
                return Web.Resources.EntityStrings.PartsSupplierRelation_PurchasePercentage;
            }
        }

        public string PartsSupplierRelation_SupplierPartCode {
            get {
                return Web.Resources.EntityStrings.PartsSupplierRelation_SupplierPartCode;
            }
        }

        public string PartsSupplierRelation_SupplierPartName {
            get {
                return Web.Resources.EntityStrings.PartsSupplierRelation_SupplierPartName;
            }
        }

        public string PartsPurchasePricingDetail_PartCode {
            get {
                return Web.Resources.EntityStrings.PartsPurchasePricingDetail_PartCode;
            }
        }

        public string PartsPurchasePricingDetail_PartName {
            get {
                return Web.Resources.EntityStrings.PartsPurchasePricingDetail_PartName;
            }
        }

        public string PartsPurchasePricingDetail_SupplierCode {
            get {
                return Web.Resources.EntityStrings.PartsPurchasePricingDetail_SupplierCode;
            }
        }

        public string PartsPurchasePricingDetail_SupplierName {
            get {
                return Web.Resources.EntityStrings.PartsPurchasePricingDetail_SupplierName;
            }
        }

        public string PartsPurchasePricingDetail_ValidFrom {
            get {
                return Web.Resources.EntityStrings.PartsPurchasePricingDetail_ValidFrom;
            }
        }

        public string PartsPurchasePricingChange_Code {
            get {
                return Web.Resources.EntityStrings.PartsPurchasePricingChange_Code;
            }
        }

        public string PartsPurchasePricingDetail_Price {
            get {
                return Web.Resources.EntityStrings.PartsPurchasePricingDetail_Price;
            }
        }

        public string PartsPurchasePricingDetail_ValidTo {
            get {
                return Web.Resources.EntityStrings.PartsPurchasePricingDetail_ValidTo;
            }
        }

        public string _Common_ApproverName {
            get {
                return Web.Resources.EntityStrings._Common_ApproverName;
            }
        }

        public string _Common_ApproveTime {
            get {
                return Web.Resources.EntityStrings._Common_ApproveTime;
            }
        }

        public string SparePart_EnglishName {
            get {
                return Web.Resources.EntityStrings.SparePart_EnglishName;
            }
        }

        public string SparePart_Height {
            get {
                return Web.Resources.EntityStrings.SparePart_Height;
            }
        }

        public string SparePart_IsManagedByBatch {
            get {
                return Web.Resources.EntityStrings.SparePart_IsManagedByBatch;
            }
        }

        public string SparePart_IsManagedBySerial {
            get {
                return Web.Resources.EntityStrings.SparePart_IsManagedBySerial;
            }
        }

        public string SparePart_Length {
            get {
                return Web.Resources.EntityStrings.SparePart_Length;
            }
        }

        public string SparePart_Material {
            get {
                return Web.Resources.EntityStrings.SparePart_Material;
            }
        }

        public string SparePart_PinyinCode {
            get {
                return Web.Resources.EntityStrings.SparePart_PinyinCode;
            }
        }

        public string SparePart_ReferenceCode {
            get {
                return Web.Resources.EntityStrings.SparePart_ReferenceCode;
            }
        }

        public string SparePart_ReferenceName {
            get {
                return Web.Resources.EntityStrings.SparePart_ReferenceName;
            }
        }

        public string SparePart_Volume {
            get {
                return Web.Resources.EntityStrings.SparePart_Volume;
            }
        }

        public string SparePart_Weight {
            get {
                return Web.Resources.EntityStrings.SparePart_Weight;
            }
        }

        public string SparePart_Width {
            get {
                return Web.Resources.EntityStrings.SparePart_Width;
            }
        }

        public string CombinedPart_Quantity {
            get {
                return Web.Resources.EntityStrings.CombinedPart_Quantity;
            }
        }

        public string SparePart_ChildCombinedParts {
            get {
                return Web.Resources.EntityStrings.SparePart_ChildCombinedParts;
            }
        }

        public string PartsPurchasePricingChange_PartsPurchasePricingDetails {
            get {
                return Web.Resources.EntityStrings.PartsPurchasePricingChange_PartsPurchasePricingDetails;
            }
        }

        public string PartsPurchasePricingDetail_ReferencePrice {
            get {
                return Web.Resources.EntityStrings.PartsPurchasePricingDetail_ReferencePrice;
            }
        }

        public string Dealer_Code {
            get {
                return Web.Resources.EntityStrings.Dealer_Code;
            }
        }

        public string Dealer_Name {
            get {
                return Web.Resources.EntityStrings.Dealer_Name;
            }
        }

        public string Dealer_ShortName {
            get {
                return Web.Resources.EntityStrings.Dealer_ShortName;
            }
        }

        public string CompanyAddress_Usage {
            get {
                return Web.Resources.EntityStrings.CompanyAddress_Usage;
            }
        }

        public string CompanyAddress_DetailAddress {
            get {
                return Web.Resources.EntityStrings.CompanyAddress_DetailAddress;
            }
        }

        public string DealerKeyPosition_PositionName {
            get {
                return Web.Resources.EntityStrings.DealerKeyPosition_PositionName;
            }
        }

        public string DealerKeyPosition_Responsibility {
            get {
                return Web.Resources.EntityStrings.DealerKeyPosition_Responsibility;
            }
        }

        public string Branch_Name {
            get {
                return Web.Resources.EntityStrings.Branch_Name;
            }
        }

        public string DealerKeyEmployee_Name {
            get {
                return Web.Resources.EntityStrings.DealerKeyEmployee_Name;
            }
        }

        public string DealerKeyEmployeeHistory_FilerName {
            get {
                return Web.Resources.EntityStrings.DealerKeyEmployeeHistory_FilerName;
            }
        }

        public string DealerKeyEmployeeHistory_FileTime {
            get {
                return Web.Resources.EntityStrings.DealerKeyEmployeeHistory_FileTime;
            }
        }

        public string DealerKeyEmployeeHistory_Name {
            get {
                return Web.Resources.EntityStrings.DealerKeyEmployeeHistory_Name;
            }
        }

        public string DealerServiceExt_BrandScope {
            get {
                return Web.Resources.EntityStrings.DealerServiceExt_BrandScope;
            }
        }

        public string DealerServiceExt_CompetitiveBrandScope {
            get {
                return Web.Resources.EntityStrings.DealerServiceExt_CompetitiveBrandScope;
            }
        }

        public string DealerServiceExt_DangerousRepairQualification {
            get {
                return Web.Resources.EntityStrings.DealerServiceExt_DangerousRepairQualification;
            }
        }

        public string DealerServiceExt_Manager {
            get {
                return Web.Resources.EntityStrings.DealerServiceExt_Manager;
            }
        }

        public string DealerServiceExt_ParkingArea {
            get {
                return Web.Resources.EntityStrings.DealerServiceExt_ParkingArea;
            }
        }

        public string DealerServiceExt_PartWarehouseArea {
            get {
                return Web.Resources.EntityStrings.DealerServiceExt_PartWarehouseArea;
            }
        }

        public string DealerServiceExt_ReceptionRoomArea {
            get {
                return Web.Resources.EntityStrings.DealerServiceExt_ReceptionRoomArea;
            }
        }

        public string DealerServiceExt_RepairingArea {
            get {
                return Web.Resources.EntityStrings.DealerServiceExt_RepairingArea;
            }
        }

        public string DealerServiceExt_RepairQualification {
            get {
                return Web.Resources.EntityStrings.DealerServiceExt_RepairQualification;
            }
        }

        public string ChannelCapability_Name {
            get {
                return Web.Resources.EntityStrings.ChannelCapability_Name;
            }
        }

        public string DealerServiceExt_EmployeeNumber {
            get {
                return Web.Resources.EntityStrings.DealerServiceExt_EmployeeNumber;
            }
        }

        public string DealerServiceExt_HasBranch {
            get {
                return Web.Resources.EntityStrings.DealerServiceExt_HasBranch;
            }
        }

        public string DealerServiceExt_ManagerMail {
            get {
                return Web.Resources.EntityStrings.DealerServiceExt_ManagerMail;
            }
        }

        public string DealerServiceExt_ManagerMobile {
            get {
                return Web.Resources.EntityStrings.DealerServiceExt_ManagerMobile;
            }
        }

        public string DealerServiceExt_ManagerPhoneNumber {
            get {
                return Web.Resources.EntityStrings.DealerServiceExt_ManagerPhoneNumber;
            }
        }

        public string DealerServiceInfo_Fax {
            get {
                return Web.Resources.EntityStrings.DealerServiceInfo_Fax;
            }
        }

        public string DealerServiceInfo_Fix {
            get {
                return Web.Resources.EntityStrings.DealerServiceInfo_Fix;
            }
        }

        public string DealerServiceInfo_HotLine {
            get {
                return Web.Resources.EntityStrings.DealerServiceInfo_HotLine;
            }
        }

        public string DealerServiceInfo_RepairAuthorityGrade {
            get {
                return Web.Resources.EntityStrings.DealerServiceInfo_RepairAuthorityGrade;
            }
        }

        public string ServiceTripPriceGrade_Code {
            get {
                return Web.Resources.EntityStrings.ServiceTripPriceGrade_Code;
            }
        }

        public string PartsManagementCostGrade_Code {
            get {
                return Web.Resources.EntityStrings.PartsManagementCostGrade_Code;
            }
        }

        public string LaborHourUnitPriceGrade_Code {
            get {
                return Web.Resources.EntityStrings.LaborHourUnitPriceGrade_Code;
            }
        }

        public string DealerKeyEmployee_Address {
            get {
                return Web.Resources.EntityStrings.DealerKeyEmployee_Address;
            }
        }

        public string DealerKeyEmployee_Mail {
            get {
                return Web.Resources.EntityStrings.DealerKeyEmployee_Mail;
            }
        }

        public string DealerKeyEmployee_PhoneNumber {
            get {
                return Web.Resources.EntityStrings.DealerKeyEmployee_PhoneNumber;
            }
        }

        public string DealerKeyEmployee_PostCode {
            get {
                return Web.Resources.EntityStrings.DealerKeyEmployee_PostCode;
            }
        }

        public string DealerKeyEmployee_Sex {
            get {
                return Web.Resources.EntityStrings.DealerKeyEmployee_Sex;
            }
        }

        public string Dealer_DealerKeyEmployees {
            get {
                return Web.Resources.EntityStrings.Dealer_DealerKeyEmployees;
            }
        }

        public string DealerKeyEmployee_KeyPositionId {
            get {
                return Web.Resources.EntityStrings.DealerKeyEmployee_KeyPositionId;
            }
        }

        public string TiledRegion_CityName {
            get {
                return Web.Resources.EntityStrings.TiledRegion_CityName;
            }
        }

        public string TiledRegion_CountyName {
            get {
                return Web.Resources.EntityStrings.TiledRegion_CountyName;
            }
        }

        public string TiledRegion_ProvinceName {
            get {
                return Web.Resources.EntityStrings.TiledRegion_ProvinceName;
            }
        }

        public string TiledRegion_RegionName {
            get {
                return Web.Resources.EntityStrings.TiledRegion_RegionName;
            }
        }

        public string MarketingDepartment_Address {
            get {
                return Web.Resources.EntityStrings.MarketingDepartment_Address;
            }
        }

        public string MarketingDepartment_BusinessType {
            get {
                return Web.Resources.EntityStrings.MarketingDepartment_BusinessType;
            }
        }

        public string MarketingDepartment_Code {
            get {
                return Web.Resources.EntityStrings.MarketingDepartment_Code;
            }
        }

        public string MarketingDepartment_Fax {
            get {
                return Web.Resources.EntityStrings.MarketingDepartment_Fax;
            }
        }

        public string MarketingDepartment_Name {
            get {
                return Web.Resources.EntityStrings.MarketingDepartment_Name;
            }
        }

        public string MarketingDepartment_Phone {
            get {
                return Web.Resources.EntityStrings.MarketingDepartment_Phone;
            }
        }

        public string MarketingDepartment_PostCode {
            get {
                return Web.Resources.EntityStrings.MarketingDepartment_PostCode;
            }
        }

        public string MarketingDepartment_DealerMarketDptRelations {
            get {
                return Web.Resources.EntityStrings.MarketingDepartment_DealerMarketDptRelations;
            }
        }

        public string _Common_CorporationName {
            get {
                return Web.Resources.EntityStrings._Common_CorporationName;
            }
        }

        public string Personnel_Name {
            get {
                return Web.Resources.EntityStrings.Personnel_Name;
            }
        }

        public string Personnel_LoginId {
            get {
                return Web.Resources.EntityStrings.Personnel_LoginId;
            }
        }

        public string _Common_CorporationId {
            get {
                return Web.Resources.EntityStrings._Common_CorporationId;
            }
        }

        public string WarehouseAreaCategory_Category {
            get {
                return Web.Resources.EntityStrings.WarehouseAreaCategory_Category;
            }
        }

        public string WarehouseArea_AreaKind {
            get {
                return Web.Resources.EntityStrings.WarehouseArea_AreaKind;
            }
        }

        public string WarehouseArea_Code {
            get {
                return Web.Resources.EntityStrings.WarehouseArea_Code;
            }
        }

        public string Warehouse_Name {
            get {
                return Web.Resources.EntityStrings.Warehouse_Name;
            }
        }

        public string Warehouse_Address {
            get {
                return Web.Resources.EntityStrings.Warehouse_Address;
            }
        }

        public string Warehouse_Code {
            get {
                return Web.Resources.EntityStrings.Warehouse_Code;
            }
        }

        public string Warehouse_Contact {
            get {
                return Web.Resources.EntityStrings.Warehouse_Contact;
            }
        }

        public string Warehouse_Email {
            get {
                return Web.Resources.EntityStrings.Warehouse_Email;
            }
        }

        public string Warehouse_Fax {
            get {
                return Web.Resources.EntityStrings.Warehouse_Fax;
            }
        }

        public string Warehouse_PhoneNumber {
            get {
                return Web.Resources.EntityStrings.Warehouse_PhoneNumber;
            }
        }

        public string Warehouse_StorageCenter {
            get {
                return Web.Resources.EntityStrings.Warehouse_StorageCenter;
            }
        }

        public string Warehouse_StorageStrategy {
            get {
                return Web.Resources.EntityStrings.Warehouse_StorageStrategy;
            }
        }

        public string Warehouse_BranchId {
            get {
                return Web.Resources.EntityStrings.Warehouse_BranchId;
            }
        }

        public string Warehouse_WarehouseOperators {
            get {
                return Web.Resources.EntityStrings.Warehouse_WarehouseOperators;
            }
        }

        public string WarehouseArea_WarehouseAreaManagers {
            get {
                return Web.Resources.EntityStrings.WarehouseArea_WarehouseAreaManagers;
            }
        }

        public string WarrantyPolicy_ApplicationScope {
            get {
                return Web.Resources.EntityStrings.WarrantyPolicy_ApplicationScope;
            }
        }

        public string WarrantyPolicy_Category {
            get {
                return Web.Resources.EntityStrings.WarrantyPolicy_Category;
            }
        }

        public string WarrantyPolicy_Code {
            get {
                return Web.Resources.EntityStrings.WarrantyPolicy_Code;
            }
        }

        public string WarrantyPolicy_ExecutionDate {
            get {
                return Web.Resources.EntityStrings.WarrantyPolicy_ExecutionDate;
            }
        }

        public string WarrantyPolicy_Name {
            get {
                return Web.Resources.EntityStrings.WarrantyPolicy_Name;
            }
        }

        public string WarrantyPolicy_ReleaseDate {
            get {
                return Web.Resources.EntityStrings.WarrantyPolicy_ReleaseDate;
            }
        }

        public string VehicleWarrantyTerm_Code {
            get {
                return Web.Resources.EntityStrings.VehicleWarrantyTerm_Code;
            }
        }

        public string VehicleWarrantyTerm_Name {
            get {
                return Web.Resources.EntityStrings.VehicleWarrantyTerm_Name;
            }
        }

        public string VehicleWarrantyTerm_PartsPriceCap {
            get {
                return Web.Resources.EntityStrings.VehicleWarrantyTerm_PartsPriceCap;
            }
        }

        public string PartsWarrantyCategory_Code {
            get {
                return Web.Resources.EntityStrings.PartsWarrantyCategory_Code;
            }
        }

        public string PartsWarrantyCategory_Name {
            get {
                return Web.Resources.EntityStrings.PartsWarrantyCategory_Name;
            }
        }

        public string VehicleMainteTerm_Code {
            get {
                return Web.Resources.EntityStrings.VehicleMainteTerm_Code;
            }
        }

        public string VehicleMainteTerm_LaborCost {
            get {
                return Web.Resources.EntityStrings.VehicleMainteTerm_LaborCost;
            }
        }

        public string VehicleMainteTerm_MaterialCost {
            get {
                return Web.Resources.EntityStrings.VehicleMainteTerm_MaterialCost;
            }
        }

        public string VehicleMainteTerm_Name {
            get {
                return Web.Resources.EntityStrings.VehicleMainteTerm_Name;
            }
        }

        public string VehicleMainteTerm_OtherCost {
            get {
                return Web.Resources.EntityStrings.VehicleMainteTerm_OtherCost;
            }
        }

        public string VehicleMainteTerm_PartsPriceCap {
            get {
                return Web.Resources.EntityStrings.VehicleMainteTerm_PartsPriceCap;
            }
        }

        public string UsedPartsWarehouse_Address {
            get {
                return Web.Resources.EntityStrings.UsedPartsWarehouse_Address;
            }
        }

        public string UsedPartsWarehouse_Code {
            get {
                return Web.Resources.EntityStrings.UsedPartsWarehouse_Code;
            }
        }

        public string UsedPartsWarehouse_Contact {
            get {
                return Web.Resources.EntityStrings.UsedPartsWarehouse_Contact;
            }
        }

        public string UsedPartsWarehouse_ContactPhone {
            get {
                return Web.Resources.EntityStrings.UsedPartsWarehouse_ContactPhone;
            }
        }

        public string UsedPartsWarehouse_Email {
            get {
                return Web.Resources.EntityStrings.UsedPartsWarehouse_Email;
            }
        }

        public string UsedPartsWarehouse_Fax {
            get {
                return Web.Resources.EntityStrings.UsedPartsWarehouse_Fax;
            }
        }

        public string UsedPartsWarehouse_Name {
            get {
                return Web.Resources.EntityStrings.UsedPartsWarehouse_Name;
            }
        }

        public string UsedPartsWarehouse_UsedPartsWarehouseStaffs {
            get {
                return Web.Resources.EntityStrings.UsedPartsWarehouse_UsedPartsWarehouseStaffs;
            }
        }

        public string UsedPartsWarehouse_StoragePolicy {
            get {
                return Web.Resources.EntityStrings.UsedPartsWarehouse_StoragePolicy;
            }
        }

        public string UsedPartsStock_ResponsibleUnitName {
            get {
                return Web.Resources.EntityStrings.UsedPartsStock_ResponsibleUnitName;
            }
        }

        public string UsedPartsStock_ClaimBillCode {
            get {
                return Web.Resources.EntityStrings.UsedPartsStock_ClaimBillCode;
            }
        }

        public string UsedPartsStock_ClaimBillType {
            get {
                return Web.Resources.EntityStrings.UsedPartsStock_ClaimBillType;
            }
        }

        public string UsedPartsStock_FaultyPartsSupplierCode {
            get {
                return Web.Resources.EntityStrings.UsedPartsStock_FaultyPartsSupplierCode;
            }
        }

        public string UsedPartsStock_FaultyPartsSupplierName {
            get {
                return Web.Resources.EntityStrings.UsedPartsStock_FaultyPartsSupplierName;
            }
        }

        public string UsedPartsStock_IfFaultyParts {
            get {
                return Web.Resources.EntityStrings.UsedPartsStock_IfFaultyParts;
            }
        }

        public string UsedPartsStock_LockedQuantity {
            get {
                return Web.Resources.EntityStrings.UsedPartsStock_LockedQuantity;
            }
        }

        public string UsedPartsStock_ResponsibleUnitCode {
            get {
                return Web.Resources.EntityStrings.UsedPartsStock_ResponsibleUnitCode;
            }
        }

        public string UsedPartsStock_StorageQuantity {
            get {
                return Web.Resources.EntityStrings.UsedPartsStock_StorageQuantity;
            }
        }

        public string UsedPartsStock_UsedPartsBarCode {
            get {
                return Web.Resources.EntityStrings.UsedPartsStock_UsedPartsBarCode;
            }
        }

        public string UsedPartsStock_UsedPartsBatchNumber {
            get {
                return Web.Resources.EntityStrings.UsedPartsStock_UsedPartsBatchNumber;
            }
        }

        public string UsedPartsStock_UsedPartsCode {
            get {
                return Web.Resources.EntityStrings.UsedPartsStock_UsedPartsCode;
            }
        }

        public string UsedPartsStock_UsedPartsName {
            get {
                return Web.Resources.EntityStrings.UsedPartsStock_UsedPartsName;
            }
        }

        public string UsedPartsStock_UsedPartsSerialNumber {
            get {
                return Web.Resources.EntityStrings.UsedPartsStock_UsedPartsSerialNumber;
            }
        }

        public string UsedPartsStock_UsedPartsSupplierCode {
            get {
                return Web.Resources.EntityStrings.UsedPartsStock_UsedPartsSupplierCode;
            }
        }

        public string UsedPartsStock_UsedPartsSupplierName {
            get {
                return Web.Resources.EntityStrings.UsedPartsStock_UsedPartsSupplierName;
            }
        }

        public string UsedPartsWarehouseArea_IfDefaultStoragePosition {
            get {
                return Web.Resources.EntityStrings.UsedPartsWarehouseArea_IfDefaultStoragePosition;
            }
        }

        public string UsedPartsWarehouseArea_StorageAreaType {
            get {
                return Web.Resources.EntityStrings.UsedPartsWarehouseArea_StorageAreaType;
            }
        }

        public string UsedPartsWarehouseArea_StorageCategory {
            get {
                return Web.Resources.EntityStrings.UsedPartsWarehouseArea_StorageCategory;
            }
        }

        public string UsedPartsWarehouseArea_UsedPartsWarehouseManagers {
            get {
                return Web.Resources.EntityStrings.UsedPartsWarehouseArea_UsedPartsWarehouseManagers;
            }
        }

        public string SsUsedPartsStorage_ClaimBillCode {
            get {
                return Web.Resources.EntityStrings.SsUsedPartsStorage_ClaimBillCode;
            }
        }

        public string SsUsedPartsStorage_FaultyPartsCode {
            get {
                return Web.Resources.EntityStrings.SsUsedPartsStorage_FaultyPartsCode;
            }
        }

        public string SsUsedPartsStorage_FaultyPartsName {
            get {
                return Web.Resources.EntityStrings.SsUsedPartsStorage_FaultyPartsName;
            }
        }

        public string SsUsedPartsStorage_FaultyPartsSupplierCode {
            get {
                return Web.Resources.EntityStrings.SsUsedPartsStorage_FaultyPartsSupplierCode;
            }
        }

        public string SsUsedPartsStorage_FaultyPartsSupplierName {
            get {
                return Web.Resources.EntityStrings.SsUsedPartsStorage_FaultyPartsSupplierName;
            }
        }

        public string SsUsedPartsStorage_IfFaultyParts {
            get {
                return Web.Resources.EntityStrings.SsUsedPartsStorage_IfFaultyParts;
            }
        }

        public string SsUsedPartsStorage_NewPartsBatchNumber {
            get {
                return Web.Resources.EntityStrings.SsUsedPartsStorage_NewPartsBatchNumber;
            }
        }

        public string SsUsedPartsStorage_NewPartsCode {
            get {
                return Web.Resources.EntityStrings.SsUsedPartsStorage_NewPartsCode;
            }
        }

        public string SsUsedPartsStorage_NewPartsName {
            get {
                return Web.Resources.EntityStrings.SsUsedPartsStorage_NewPartsName;
            }
        }

        public string SsUsedPartsStorage_NewPartsSecurityNumber {
            get {
                return Web.Resources.EntityStrings.SsUsedPartsStorage_NewPartsSecurityNumber;
            }
        }

        public string SsUsedPartsStorage_NewPartsSerialNumber {
            get {
                return Web.Resources.EntityStrings.SsUsedPartsStorage_NewPartsSerialNumber;
            }
        }

        public string SsUsedPartsStorage_NewPartsSupplierCode {
            get {
                return Web.Resources.EntityStrings.SsUsedPartsStorage_NewPartsSupplierCode;
            }
        }

        public string SsUsedPartsStorage_NewPartsSupplierName {
            get {
                return Web.Resources.EntityStrings.SsUsedPartsStorage_NewPartsSupplierName;
            }
        }

        public string SsUsedPartsStorage_PartsManagementCost {
            get {
                return Web.Resources.EntityStrings.SsUsedPartsStorage_PartsManagementCost;
            }
        }

        public string SsUsedPartsStorage_ResponsibleUnitCode {
            get {
                return Web.Resources.EntityStrings.SsUsedPartsStorage_ResponsibleUnitCode;
            }
        }

        public string SsUsedPartsStorage_ResponsibleUnitName {
            get {
                return Web.Resources.EntityStrings.SsUsedPartsStorage_ResponsibleUnitName;
            }
        }

        public string SsUsedPartsStorage_Shipped {
            get {
                return Web.Resources.EntityStrings.SsUsedPartsStorage_Shipped;
            }
        }

        public string SsUsedPartsStorage_UnitPrice {
            get {
                return Web.Resources.EntityStrings.SsUsedPartsStorage_UnitPrice;
            }
        }

        public string SsUsedPartsStorage_UsedPartsBarCode {
            get {
                return Web.Resources.EntityStrings.SsUsedPartsStorage_UsedPartsBarCode;
            }
        }

        public string SsUsedPartsStorage_UsedPartsBatchNumber {
            get {
                return Web.Resources.EntityStrings.SsUsedPartsStorage_UsedPartsBatchNumber;
            }
        }

        public string SsUsedPartsStorage_UsedPartsCode {
            get {
                return Web.Resources.EntityStrings.SsUsedPartsStorage_UsedPartsCode;
            }
        }

        public string SsUsedPartsStorage_UsedPartsName {
            get {
                return Web.Resources.EntityStrings.SsUsedPartsStorage_UsedPartsName;
            }
        }

        public string SsUsedPartsStorage_UsedPartsReturnPolicy {
            get {
                return Web.Resources.EntityStrings.SsUsedPartsStorage_UsedPartsReturnPolicy;
            }
        }

        public string SsUsedPartsStorage_UsedPartsSerialNumber {
            get {
                return Web.Resources.EntityStrings.SsUsedPartsStorage_UsedPartsSerialNumber;
            }
        }

        public string SsUsedPartsStorage_UsedPartsSupplierCode {
            get {
                return Web.Resources.EntityStrings.SsUsedPartsStorage_UsedPartsSupplierCode;
            }
        }

        public string SsUsedPartsStorage_UsedPartsSupplierName {
            get {
                return Web.Resources.EntityStrings.SsUsedPartsStorage_UsedPartsSupplierName;
            }
        }

        public string RepairItem_Code {
            get {
                return Web.Resources.EntityStrings.RepairItem_Code;
            }
        }

        public string RepairItem_JobType {
            get {
                return Web.Resources.EntityStrings.RepairItem_JobType;
            }
        }

        public string RepairItem_Name {
            get {
                return Web.Resources.EntityStrings.RepairItem_Name;
            }
        }

        public string RepairItem_QuickCode {
            get {
                return Web.Resources.EntityStrings.RepairItem_QuickCode;
            }
        }

        public string RepairItem_RepairQualificationGrade {
            get {
                return Web.Resources.EntityStrings.RepairItem_RepairQualificationGrade;
            }
        }

        public string RepairItem_SettlementProperty {
            get {
                return Web.Resources.EntityStrings.RepairItem_SettlementProperty;
            }
        }

        public string PartsWarrantyTerm_PartsWarrantyType {
            get {
                return Web.Resources.EntityStrings.PartsWarrantyTerm_PartsWarrantyType;
            }
        }

        public string PartsWarrantyTerm_ReturnPolicy {
            get {
                return Web.Resources.EntityStrings.PartsWarrantyTerm_ReturnPolicy;
            }
        }

        public string PartsWarrantyTerm_WarrantyMileage {
            get {
                return Web.Resources.EntityStrings.PartsWarrantyTerm_WarrantyMileage;
            }
        }

        public string PartsWarrantyTerm_WarrantyPeriod {
            get {
                return Web.Resources.EntityStrings.PartsWarrantyTerm_WarrantyPeriod;
            }
        }

        public string WarrantyPolicy_MainteTermInvolved {
            get {
                return Web.Resources.EntityStrings.WarrantyPolicy_MainteTermInvolved;
            }
        }

        public string WarrantyPolicy_PartsWarrantyTermInvolved {
            get {
                return Web.Resources.EntityStrings.WarrantyPolicy_PartsWarrantyTermInvolved;
            }
        }

        public string WarrantyPolicy_RepairTermInvolved {
            get {
                return Web.Resources.EntityStrings.WarrantyPolicy_RepairTermInvolved;
            }
        }

        public string VehicleMainteRepairItem_DefaultLaborHour {
            get {
                return Web.Resources.EntityStrings.VehicleMainteRepairItem_DefaultLaborHour;
            }
        }

        public string VehicleMainteRepairItem_IfObliged {
            get {
                return Web.Resources.EntityStrings.VehicleMainteRepairItem_IfObliged;
            }
        }

        public string UsedPartsShippingOrder_ActualArrivalDate {
            get {
                return Web.Resources.EntityStrings.UsedPartsShippingOrder_ActualArrivalDate;
            }
        }

        public string UsedPartsShippingOrder_BranchName {
            get {
                return Web.Resources.EntityStrings.UsedPartsShippingOrder_BranchName;
            }
        }

        public string UsedPartsShippingOrder_Code {
            get {
                return Web.Resources.EntityStrings.UsedPartsShippingOrder_Code;
            }
        }

        public string UsedPartsShippingOrder_DealerCode {
            get {
                return Web.Resources.EntityStrings.UsedPartsShippingOrder_DealerCode;
            }
        }

        public string UsedPartsShippingOrder_DealerName {
            get {
                return Web.Resources.EntityStrings.UsedPartsShippingOrder_DealerName;
            }
        }

        public string UsedPartsShippingOrder_DestinationAddress {
            get {
                return Web.Resources.EntityStrings.UsedPartsShippingOrder_DestinationAddress;
            }
        }

        public string UsedPartsShippingOrder_Dispatcher {
            get {
                return Web.Resources.EntityStrings.UsedPartsShippingOrder_Dispatcher;
            }
        }

        public string UsedPartsShippingOrder_DriverPhone {
            get {
                return Web.Resources.EntityStrings.UsedPartsShippingOrder_DriverPhone;
            }
        }

        public string UsedPartsShippingOrder_LogisticCompanyCode {
            get {
                return Web.Resources.EntityStrings.UsedPartsShippingOrder_LogisticCompanyCode;
            }
        }

        public string UsedPartsShippingOrder_LogisticCompanyName {
            get {
                return Web.Resources.EntityStrings.UsedPartsShippingOrder_LogisticCompanyName;
            }
        }

        public string UsedPartsShippingOrder_Operator {
            get {
                return Web.Resources.EntityStrings.UsedPartsShippingOrder_Operator;
            }
        }

        public string UsedPartsShippingOrder_OperatorPhone {
            get {
                return Web.Resources.EntityStrings.UsedPartsShippingOrder_OperatorPhone;
            }
        }

        public string UsedPartsShippingOrder_ReceptionRemark {
            get {
                return Web.Resources.EntityStrings.UsedPartsShippingOrder_ReceptionRemark;
            }
        }

        public string UsedPartsShippingOrder_RequestedArrivalDate {
            get {
                return Web.Resources.EntityStrings.UsedPartsShippingOrder_RequestedArrivalDate;
            }
        }

        public string UsedPartsShippingOrder_ShippingDate {
            get {
                return Web.Resources.EntityStrings.UsedPartsShippingOrder_ShippingDate;
            }
        }

        public string UsedPartsShippingOrder_ShippingMethod {
            get {
                return Web.Resources.EntityStrings.UsedPartsShippingOrder_ShippingMethod;
            }
        }

        public string UsedPartsShippingOrder_Supervisor {
            get {
                return Web.Resources.EntityStrings.UsedPartsShippingOrder_Supervisor;
            }
        }

        public string UsedPartsShippingOrder_SupervisorPhone {
            get {
                return Web.Resources.EntityStrings.UsedPartsShippingOrder_SupervisorPhone;
            }
        }

        public string UsedPartsShippingOrder_TotalAmount {
            get {
                return Web.Resources.EntityStrings.UsedPartsShippingOrder_TotalAmount;
            }
        }

        public string UsedPartsShippingOrder_TransportationVehicle {
            get {
                return Web.Resources.EntityStrings.UsedPartsShippingOrder_TransportationVehicle;
            }
        }

        public string UsedPartsShippingOrder_TransportDriver {
            get {
                return Web.Resources.EntityStrings.UsedPartsShippingOrder_TransportDriver;
            }
        }

        public string UsedPartsShippingOrder_WarehouseContact {
            get {
                return Web.Resources.EntityStrings.UsedPartsShippingOrder_WarehouseContact;
            }
        }

        public string UsedPartsShippingOrder_WarehousePhone {
            get {
                return Web.Resources.EntityStrings.UsedPartsShippingOrder_WarehousePhone;
            }
        }

        public string UsedPartsShippingDetail_ClaimBillCode {
            get {
                return Web.Resources.EntityStrings.UsedPartsShippingDetail_ClaimBillCode;
            }
        }

        public string UsedPartsShippingDetail_FaultyPartsCode {
            get {
                return Web.Resources.EntityStrings.UsedPartsShippingDetail_FaultyPartsCode;
            }
        }

        public string UsedPartsShippingDetail_FaultyPartsName {
            get {
                return Web.Resources.EntityStrings.UsedPartsShippingDetail_FaultyPartsName;
            }
        }

        public string UsedPartsShippingDetail_FaultyPartsSupplierName {
            get {
                return Web.Resources.EntityStrings.UsedPartsShippingDetail_FaultyPartsSupplierName;
            }
        }

        public string UsedPartsShippingDetail_IfFaultyParts {
            get {
                return Web.Resources.EntityStrings.UsedPartsShippingDetail_IfFaultyParts;
            }
        }

        public string UsedPartsShippingDetail_PartsManagementCost {
            get {
                return Web.Resources.EntityStrings.UsedPartsShippingDetail_PartsManagementCost;
            }
        }

        public string UsedPartsShippingDetail_ReceptionRemark {
            get {
                return Web.Resources.EntityStrings.UsedPartsShippingDetail_ReceptionRemark;
            }
        }

        public string UsedPartsShippingDetail_ReceptionStatus {
            get {
                return Web.Resources.EntityStrings.UsedPartsShippingDetail_ReceptionStatus;
            }
        }

        public string UsedPartsShippingDetail_ShippingRemark {
            get {
                return Web.Resources.EntityStrings.UsedPartsShippingDetail_ShippingRemark;
            }
        }

        public string UsedPartsShippingDetail_UnitPrice {
            get {
                return Web.Resources.EntityStrings.UsedPartsShippingDetail_UnitPrice;
            }
        }

        public string UsedPartsShippingDetail_UsedPartsBarCode {
            get {
                return Web.Resources.EntityStrings.UsedPartsShippingDetail_UsedPartsBarCode;
            }
        }

        public string UsedPartsShippingOrder_UsedPartsShippingDetails {
            get {
                return Web.Resources.EntityStrings.UsedPartsShippingOrder_UsedPartsShippingDetails;
            }
        }

        public string UsedPartsReturnPolicyHistory_ClaimBillCode {
            get {
                return Web.Resources.EntityStrings.UsedPartsReturnPolicyHistory_ClaimBillCode;
            }
        }

        public string UsedPartsReturnPolicyHistory_FaultyPartsCode {
            get {
                return Web.Resources.EntityStrings.UsedPartsReturnPolicyHistory_FaultyPartsCode;
            }
        }

        public string UsedPartsReturnPolicyHistory_FaultyPartsName {
            get {
                return Web.Resources.EntityStrings.UsedPartsReturnPolicyHistory_FaultyPartsName;
            }
        }

        public string UsedPartsReturnPolicyHistory_FaultyPartsSupplierCode {
            get {
                return Web.Resources.EntityStrings.UsedPartsReturnPolicyHistory_FaultyPartsSupplierCode;
            }
        }

        public string UsedPartsReturnPolicyHistory_FaultyPartsSupplierName {
            get {
                return Web.Resources.EntityStrings.UsedPartsReturnPolicyHistory_FaultyPartsSupplierName;
            }
        }

        public string UsedPartsReturnPolicyHistory_IfFaultyParts {
            get {
                return Web.Resources.EntityStrings.UsedPartsReturnPolicyHistory_IfFaultyParts;
            }
        }

        public string UsedPartsReturnPolicyHistory_NewPartsBatchNumber {
            get {
                return Web.Resources.EntityStrings.UsedPartsReturnPolicyHistory_NewPartsBatchNumber;
            }
        }

        public string UsedPartsReturnPolicyHistory_NewPartsSerialNumber {
            get {
                return Web.Resources.EntityStrings.UsedPartsReturnPolicyHistory_NewPartsSerialNumber;
            }
        }

        public string UsedPartsReturnPolicyHistory_NewPartsSupplierCode {
            get {
                return Web.Resources.EntityStrings.UsedPartsReturnPolicyHistory_NewPartsSupplierCode;
            }
        }

        public string UsedPartsReturnPolicyHistory_NewPartsSupplierName {
            get {
                return Web.Resources.EntityStrings.UsedPartsReturnPolicyHistory_NewPartsSupplierName;
            }
        }

        public string UsedPartsReturnPolicyHistory_PartsManagementCost {
            get {
                return Web.Resources.EntityStrings.UsedPartsReturnPolicyHistory_PartsManagementCost;
            }
        }

        public string UsedPartsReturnPolicyHistory_ResponsibleUnitCode {
            get {
                return Web.Resources.EntityStrings.UsedPartsReturnPolicyHistory_ResponsibleUnitCode;
            }
        }

        public string UsedPartsReturnPolicyHistory_ResponsibleUnitName {
            get {
                return Web.Resources.EntityStrings.UsedPartsReturnPolicyHistory_ResponsibleUnitName;
            }
        }

        public string UsedPartsReturnPolicyHistory_Shipped {
            get {
                return Web.Resources.EntityStrings.UsedPartsReturnPolicyHistory_Shipped;
            }
        }

        public string UsedPartsReturnPolicyHistory_UnitPrice {
            get {
                return Web.Resources.EntityStrings.UsedPartsReturnPolicyHistory_UnitPrice;
            }
        }

        public string UsedPartsReturnPolicyHistory_UsedPartsBarCode {
            get {
                return Web.Resources.EntityStrings.UsedPartsReturnPolicyHistory_UsedPartsBarCode;
            }
        }

        public string UsedPartsReturnPolicyHistory_UsedPartsBatchNumber {
            get {
                return Web.Resources.EntityStrings.UsedPartsReturnPolicyHistory_UsedPartsBatchNumber;
            }
        }

        public string UsedPartsReturnPolicyHistory_UsedPartsReturnPolicy {
            get {
                return Web.Resources.EntityStrings.UsedPartsReturnPolicyHistory_UsedPartsReturnPolicy;
            }
        }

        public string UsedPartsReturnPolicyHistory_UsedPartsSerialNumber {
            get {
                return Web.Resources.EntityStrings.UsedPartsReturnPolicyHistory_UsedPartsSerialNumber;
            }
        }

        public string UsedPartsReturnPolicyHistory_UsedPartsSupplierCode {
            get {
                return Web.Resources.EntityStrings.UsedPartsReturnPolicyHistory_UsedPartsSupplierCode;
            }
        }

        public string UsedPartsReturnPolicyHistory_UsedPartsSupplierName {
            get {
                return Web.Resources.EntityStrings.UsedPartsReturnPolicyHistory_UsedPartsSupplierName;
            }
        }

        public string SsUsedPartsDisposalBill_ApprovalComment {
            get {
                return Web.Resources.EntityStrings.SsUsedPartsDisposalBill_ApprovalComment;
            }
        }

        public string SsUsedPartsDisposalBill_BranchName {
            get {
                return Web.Resources.EntityStrings.SsUsedPartsDisposalBill_BranchName;
            }
        }

        public string SsUsedPartsDisposalBill_Code {
            get {
                return Web.Resources.EntityStrings.SsUsedPartsDisposalBill_Code;
            }
        }

        public string SsUsedPartsDisposalBill_SsUsedPartsDisposalDetails {
            get {
                return Web.Resources.EntityStrings.SsUsedPartsDisposalBill_SsUsedPartsDisposalDetails;
            }
        }

        public string SsUsedPartsDisposalBill_ProcessTime {
            get {
                return Web.Resources.EntityStrings.SsUsedPartsDisposalBill_ProcessTime;
            }
        }

        public string SsUsedPartsDisposalBill_TotalAmount {
            get {
                return Web.Resources.EntityStrings.SsUsedPartsDisposalBill_TotalAmount;
            }
        }

        public string SsUsedPartsDisposalBill_UsedPartsDisposalMethod {
            get {
                return Web.Resources.EntityStrings.SsUsedPartsDisposalBill_UsedPartsDisposalMethod;
            }
        }

        public string SsUsedPartsDisposalDetail_ClaimBillCode {
            get {
                return Web.Resources.EntityStrings.SsUsedPartsDisposalDetail_ClaimBillCode;
            }
        }

        public string SsUsedPartsDisposalDetail_FaultyPartsCode {
            get {
                return Web.Resources.EntityStrings.SsUsedPartsDisposalDetail_FaultyPartsCode;
            }
        }

        public string SsUsedPartsDisposalDetail_FaultyPartsName {
            get {
                return Web.Resources.EntityStrings.SsUsedPartsDisposalDetail_FaultyPartsName;
            }
        }

        public string SsUsedPartsDisposalDetail_FaultyPartsSupplierName {
            get {
                return Web.Resources.EntityStrings.SsUsedPartsDisposalDetail_FaultyPartsSupplierName;
            }
        }

        public string SsUsedPartsDisposalDetail_IfFaultyParts {
            get {
                return Web.Resources.EntityStrings.SsUsedPartsDisposalDetail_IfFaultyParts;
            }
        }

        public string SsUsedPartsDisposalDetail_PartsManagementCost {
            get {
                return Web.Resources.EntityStrings.SsUsedPartsDisposalDetail_PartsManagementCost;
            }
        }

        public string SsUsedPartsDisposalDetail_UnitPrice {
            get {
                return Web.Resources.EntityStrings.SsUsedPartsDisposalDetail_UnitPrice;
            }
        }

        public string SsUsedPartsDisposalDetail_UsedPartsBarCode {
            get {
                return Web.Resources.EntityStrings.SsUsedPartsDisposalDetail_UsedPartsBarCode;
            }
        }

        public string SsUsedPartsDisposalDetail_UsedPartsBatchNumber {
            get {
                return Web.Resources.EntityStrings.SsUsedPartsDisposalDetail_UsedPartsBatchNumber;
            }
        }

        public string SsUsedPartsDisposalDetail_UsedPartsReturnPolicy {
            get {
                return Web.Resources.EntityStrings.SsUsedPartsDisposalDetail_UsedPartsReturnPolicy;
            }
        }

        public string SsUsedPartsDisposalDetail_UsedPartsSerialNumber {
            get {
                return Web.Resources.EntityStrings.SsUsedPartsDisposalDetail_UsedPartsSerialNumber;
            }
        }

        public string SsUsedPartsDisposalBill_DealerCode {
            get {
                return Web.Resources.EntityStrings.SsUsedPartsDisposalBill_DealerCode;
            }
        }

        public string VehicleMainteTerm_VehicleMainteRepairItems {
            get {
                return Web.Resources.EntityStrings.VehicleMainteTerm_VehicleMainteRepairItems;
            }
        }

        public string SsUsedPartsDisposalBill_DealerName {
            get {
                return Web.Resources.EntityStrings.SsUsedPartsDisposalBill_DealerName;
            }
        }

        public string WarrantyPolicy_PartsWarrantyTerms {
            get {
                return Web.Resources.EntityStrings.WarrantyPolicy_PartsWarrantyTerms;
            }
        }

        public string LogisticCompany_Code {
            get {
                return Web.Resources.EntityStrings.LogisticCompany_Code;
            }
        }

        public string LogisticCompany_Name {
            get {
                return Web.Resources.EntityStrings.LogisticCompany_Name;
            }
        }

        public string UsedPartsShippingDetail_UsedPartsWarehouseAreaCode {
            get {
                return Web.Resources.EntityStrings.UsedPartsShippingDetail_UsedPartsWarehouseAreaCode;
            }
        }

        public string UsedPartsInboundDetail_ConfirmedAmount {
            get {
                return Web.Resources.EntityStrings.UsedPartsInboundDetail_ConfirmedAmount;
            }
        }

        public string UsedPartsInboundOrder_Operator {
            get {
                return Web.Resources.EntityStrings.UsedPartsInboundOrder_Operator;
            }
        }

        public string UsedPartsInboundDetail_PlannedAmount {
            get {
                return Web.Resources.EntityStrings.UsedPartsInboundDetail_PlannedAmount;
            }
        }

        public string UsedPartsInboundDetail_UsedPartsBarCode {
            get {
                return Web.Resources.EntityStrings.UsedPartsInboundDetail_UsedPartsBarCode;
            }
        }

        public string UsedPartsInboundDetail_UsedPartsBatchNumber {
            get {
                return Web.Resources.EntityStrings.UsedPartsInboundDetail_UsedPartsBatchNumber;
            }
        }

        public string UsedPartsOutboundDetail_ConfirmedAmount {
            get {
                return Web.Resources.EntityStrings.UsedPartsOutboundDetail_ConfirmedAmount;
            }
        }

        public string UsedPartsInboundDetail_UsedPartsSerialNumber {
            get {
                return Web.Resources.EntityStrings.UsedPartsInboundDetail_UsedPartsSerialNumber;
            }
        }

        public string UsedPartsOutboundDetail_PlannedAmount {
            get {
                return Web.Resources.EntityStrings.UsedPartsOutboundDetail_PlannedAmount;
            }
        }

        public string UsedPartsInboundOrder_UsedPartsInboundDetails {
            get {
                return Web.Resources.EntityStrings.UsedPartsInboundOrder_UsedPartsInboundDetails;
            }
        }

        public string UsedPartsInboundDetail_SettlementPrice {
            get {
                return Web.Resources.EntityStrings.UsedPartsInboundDetail_SettlementPrice;
            }
        }

        public string UsedPartsInboundDetail_CostPrice {
            get {
                return Web.Resources.EntityStrings.UsedPartsInboundDetail_CostPrice;
            }
        }

        public string VirtualUsedPartsInboundPlanDetail_ConfirmedAmount {
            get {
                return Web.Resources.EntityStrings.VirtualUsedPartsInboundPlanDetail_ConfirmedAmount;
            }
        }

        public string VirtualUsedPartsInboundPlanDetail_PlannedAmount {
            get {
                return Web.Resources.EntityStrings.VirtualUsedPartsInboundPlanDetail_PlannedAmount;
            }
        }

        public string UsedPartsInboundDetail_Quantity {
            get {
                return Web.Resources.EntityStrings.UsedPartsInboundDetail_Quantity;
            }
        }

        public string UsedPartsInboundOrder_Code {
            get {
                return Web.Resources.EntityStrings.UsedPartsInboundOrder_Code;
            }
        }

        public string UsedPartsInboundOrder_IfAlreadySettled {
            get {
                return Web.Resources.EntityStrings.UsedPartsInboundOrder_IfAlreadySettled;
            }
        }

        public string UsedPartsInboundOrder_IfBillable {
            get {
                return Web.Resources.EntityStrings.UsedPartsInboundOrder_IfBillable;
            }
        }

        public string UsedPartsInboundOrder_InboundType {
            get {
                return Web.Resources.EntityStrings.UsedPartsInboundOrder_InboundType;
            }
        }

        public string UsedPartsInboundOrder_SourceCode {
            get {
                return Web.Resources.EntityStrings.UsedPartsInboundOrder_SourceCode;
            }
        }

        public string UsedPartsLogisticLossBill_DealerCode {
            get {
                return Web.Resources.EntityStrings.UsedPartsLogisticLossBill_DealerCode;
            }
        }

        public string VirtualUsedPartsInboundPlanDetail_SourceCode {
            get {
                return Web.Resources.EntityStrings.VirtualUsedPartsInboundPlanDetail_SourceCode;
            }
        }

        public string UsedPartsLogisticLossBill_DealerName {
            get {
                return Web.Resources.EntityStrings.UsedPartsLogisticLossBill_DealerName;
            }
        }

        public string VirtualUsedPartsInboundPlanDetail_InboundAmount {
            get {
                return Web.Resources.EntityStrings.VirtualUsedPartsInboundPlanDetail_InboundAmount;
            }
        }

        public string UsedPartsLogisticLossBill_LogisticCompanyCode {
            get {
                return Web.Resources.EntityStrings.UsedPartsLogisticLossBill_LogisticCompanyCode;
            }
        }

        public string UsedPartsLogisticLossBill_LogisticCompanyName {
            get {
                return Web.Resources.EntityStrings.UsedPartsLogisticLossBill_LogisticCompanyName;
            }
        }

        public string VirtualUsedPartsInboundPlanDetail_UsedPartsBarCode {
            get {
                return Web.Resources.EntityStrings.VirtualUsedPartsInboundPlanDetail_UsedPartsBarCode;
            }
        }

        public string UsedPartsLogisticLossBill_TotalAmount {
            get {
                return Web.Resources.EntityStrings.UsedPartsLogisticLossBill_TotalAmount;
            }
        }

        public string VirtualUsedPartsInboundPlanDetail_UsedPartsBatchNumber {
            get {
                return Web.Resources.EntityStrings.VirtualUsedPartsInboundPlanDetail_UsedPartsBatchNumber;
            }
        }

        public string UsedPartsLogisticLossBill_UsedPartsLogisticLossDetails {
            get {
                return Web.Resources.EntityStrings.UsedPartsLogisticLossBill_UsedPartsLogisticLossDetails;
            }
        }

        public string VirtualUsedPartsInboundPlanDetail_UsedPartsSerialNumber {
            get {
                return Web.Resources.EntityStrings.VirtualUsedPartsInboundPlanDetail_UsedPartsSerialNumber;
            }
        }

        public string UsedPartsLogisticLossDetail_ClaimBillCode {
            get {
                return Web.Resources.EntityStrings.UsedPartsLogisticLossDetail_ClaimBillCode;
            }
        }

        public string VirtualUsedPartsInboundPlan_InboundStatus {
            get {
                return Web.Resources.EntityStrings.VirtualUsedPartsInboundPlan_InboundStatus;
            }
        }

        public string UsedPartsLogisticLossDetail_PartsManagementCost {
            get {
                return Web.Resources.EntityStrings.UsedPartsLogisticLossDetail_PartsManagementCost;
            }
        }

        public string VirtualUsedPartsInboundPlan_InboundType {
            get {
                return Web.Resources.EntityStrings.VirtualUsedPartsInboundPlan_InboundType;
            }
        }

        public string UsedPartsLogisticLossDetail_ProcessStatus {
            get {
                return Web.Resources.EntityStrings.UsedPartsLogisticLossDetail_ProcessStatus;
            }
        }

        public string VirtualUsedPartsInboundPlan_SourceBillApprovalTime {
            get {
                return Web.Resources.EntityStrings.VirtualUsedPartsInboundPlan_SourceBillApprovalTime;
            }
        }

        public string VirtualUsedPartsInboundPlan_UsedPartsWarehouseCode {
            get {
                return Web.Resources.EntityStrings.VirtualUsedPartsInboundPlan_UsedPartsWarehouseCode;
            }
        }

        public string UsedPartsLogisticLossDetail_UsedPartsBarCode {
            get {
                return Web.Resources.EntityStrings.UsedPartsLogisticLossDetail_UsedPartsBarCode;
            }
        }

        public string VirtualUsedPartsInboundPlan_UsedPartsWarehouseName {
            get {
                return Web.Resources.EntityStrings.VirtualUsedPartsInboundPlan_UsedPartsWarehouseName;
            }
        }

        public string UsedPartsLogisticLossBill_TotalQuantity {
            get {
                return Web.Resources.EntityStrings.UsedPartsLogisticLossBill_TotalQuantity;
            }
        }

        public string UsedPartsTransferDetail_ClaimBillType {
            get {
                return Web.Resources.EntityStrings.UsedPartsTransferDetail_ClaimBillType;
            }
        }

        public string UsedPartsTransferDetail_ClaimBillCode {
            get {
                return Web.Resources.EntityStrings.UsedPartsTransferDetail_ClaimBillCode;
            }
        }

        public string UsedPartsTransferDetail_ConfirmedAmount {
            get {
                return Web.Resources.EntityStrings.UsedPartsTransferDetail_ConfirmedAmount;
            }
        }

        public string UsedPartsTransferDetail_FaultyPartsSupplierName {
            get {
                return Web.Resources.EntityStrings.UsedPartsTransferDetail_FaultyPartsSupplierName;
            }
        }

        public string UsedPartsTransferDetail_IfFaultyParts {
            get {
                return Web.Resources.EntityStrings.UsedPartsTransferDetail_IfFaultyParts;
            }
        }

        public string UsedPartsTransferDetail_PlannedAmount {
            get {
                return Web.Resources.EntityStrings.UsedPartsTransferDetail_PlannedAmount;
            }
        }

        public string UsedPartsTransferDetail_Price {
            get {
                return Web.Resources.EntityStrings.UsedPartsTransferDetail_Price;
            }
        }

        public string UsedPartsTransferDetail_ResponsibleUnitName {
            get {
                return Web.Resources.EntityStrings.UsedPartsTransferDetail_ResponsibleUnitName;
            }
        }

        public string UsedPartsTransferDetail_UsedPartsBarCode {
            get {
                return Web.Resources.EntityStrings.UsedPartsTransferDetail_UsedPartsBarCode;
            }
        }

        public string UsedPartsTransferDetail_UsedPartsBatchNumber {
            get {
                return Web.Resources.EntityStrings.UsedPartsTransferDetail_UsedPartsBatchNumber;
            }
        }

        public string UsedPartsTransferDetail_UsedPartsCode {
            get {
                return Web.Resources.EntityStrings.UsedPartsTransferDetail_UsedPartsCode;
            }
        }

        public string UsedPartsTransferDetail_UsedPartsName {
            get {
                return Web.Resources.EntityStrings.UsedPartsTransferDetail_UsedPartsName;
            }
        }

        public string UsedPartsTransferDetail_UsedPartsSerialNumber {
            get {
                return Web.Resources.EntityStrings.UsedPartsTransferDetail_UsedPartsSerialNumber;
            }
        }

        public string UsedPartsTransferDetail_UsedPartsSupplierName {
            get {
                return Web.Resources.EntityStrings.UsedPartsTransferDetail_UsedPartsSupplierName;
            }
        }

        public string UsedPartsTransferOrder_Code {
            get {
                return Web.Resources.EntityStrings.UsedPartsTransferOrder_Code;
            }
        }

        public string UsedPartsTransferOrder_DestinationWarehouseCode {
            get {
                return Web.Resources.EntityStrings.UsedPartsTransferOrder_DestinationWarehouseCode;
            }
        }

        public string UsedPartsTransferOrder_DestinationWarehouseName {
            get {
                return Web.Resources.EntityStrings.UsedPartsTransferOrder_DestinationWarehouseName;
            }
        }

        public string UsedPartsTransferOrder_InboundStatus {
            get {
                return Web.Resources.EntityStrings.UsedPartsTransferOrder_InboundStatus;
            }
        }

        public string UsedPartsTransferOrder_OriginWarehouseCode {
            get {
                return Web.Resources.EntityStrings.UsedPartsTransferOrder_OriginWarehouseCode;
            }
        }

        public string UsedPartsTransferOrder_OriginWarehouseName {
            get {
                return Web.Resources.EntityStrings.UsedPartsTransferOrder_OriginWarehouseName;
            }
        }

        public string UsedPartsTransferOrder_OutboundStatus {
            get {
                return Web.Resources.EntityStrings.UsedPartsTransferOrder_OutboundStatus;
            }
        }

        public string UsedPartsTransferOrder_UsedPartsTransferDetails {
            get {
                return Web.Resources.EntityStrings.UsedPartsTransferOrder_UsedPartsTransferDetails;
            }
        }

        public string _Common_SerialNumber {
            get {
                return Web.Resources.EntityStrings._Common_SerialNumber;
            }
        }

        public string UsedPartsOutboundDetail_UsedPartsBarCode {
            get {
                return Web.Resources.EntityStrings.UsedPartsOutboundDetail_UsedPartsBarCode;
            }
        }

        public string UsedPartsOutboundDetail_UsedPartsBatchNumber {
            get {
                return Web.Resources.EntityStrings.UsedPartsOutboundDetail_UsedPartsBatchNumber;
            }
        }

        public string UsedPartsOutboundDetail_UsedPartsSerialNumber {
            get {
                return Web.Resources.EntityStrings.UsedPartsOutboundDetail_UsedPartsSerialNumber;
            }
        }

        public string UsedPartsOutboundOrder_Operator {
            get {
                return Web.Resources.EntityStrings.UsedPartsOutboundOrder_Operator;
            }
        }

        public string UsedPartsOutboundOrder_UsedPartsOutboundDetails {
            get {
                return Web.Resources.EntityStrings.UsedPartsOutboundOrder_UsedPartsOutboundDetails;
            }
        }

        public string UsedPartsDisposalBill_Code {
            get {
                return Web.Resources.EntityStrings.UsedPartsDisposalBill_Code;
            }
        }

        public string UsedPartsDisposalBill_Operator {
            get {
                return Web.Resources.EntityStrings.UsedPartsDisposalBill_Operator;
            }
        }

        public string UsedPartsDisposalBill_OutboundStatus {
            get {
                return Web.Resources.EntityStrings.UsedPartsDisposalBill_OutboundStatus;
            }
        }

        public string UsedPartsDisposalBill_RelatedCompanyName {
            get {
                return Web.Resources.EntityStrings.UsedPartsDisposalBill_RelatedCompanyName;
            }
        }

        public string UsedPartsDisposalBill_ResidualValue {
            get {
                return Web.Resources.EntityStrings.UsedPartsDisposalBill_ResidualValue;
            }
        }

        public string UsedPartsDisposalBill_Supervisor {
            get {
                return Web.Resources.EntityStrings.UsedPartsDisposalBill_Supervisor;
            }
        }

        public string UsedPartsDisposalBill_TotalAmount {
            get {
                return Web.Resources.EntityStrings.UsedPartsDisposalBill_TotalAmount;
            }
        }

        public string UsedPartsDisposalBill_UsedPartsDisposalMethod {
            get {
                return Web.Resources.EntityStrings.UsedPartsDisposalBill_UsedPartsDisposalMethod;
            }
        }

        public string UsedPartsDisposalBill_UsedPartsWarehouseCode {
            get {
                return Web.Resources.EntityStrings.UsedPartsDisposalBill_UsedPartsWarehouseCode;
            }
        }

        public string UsedPartsDisposalBill_UsedPartsWarehouseName {
            get {
                return Web.Resources.EntityStrings.UsedPartsDisposalBill_UsedPartsWarehouseName;
            }
        }

        public string UsedPartsDisposalDetail_ConfirmedAmount {
            get {
                return Web.Resources.EntityStrings.UsedPartsDisposalDetail_ConfirmedAmount;
            }
        }

        public string UsedPartsDisposalDetail_OutboundAmount {
            get {
                return Web.Resources.EntityStrings.UsedPartsDisposalDetail_OutboundAmount;
            }
        }

        public string UsedPartsDisposalDetail_PlannedAmount {
            get {
                return Web.Resources.EntityStrings.UsedPartsDisposalDetail_PlannedAmount;
            }
        }

        public string UsedPartsDisposalDetail_Price {
            get {
                return Web.Resources.EntityStrings.UsedPartsDisposalDetail_Price;
            }
        }

        public string UsedPartsDisposalDetail_UsedPartsBarCode {
            get {
                return Web.Resources.EntityStrings.UsedPartsDisposalDetail_UsedPartsBarCode;
            }
        }

        public string UsedPartsDisposalDetail_UsedPartsBatchNumber {
            get {
                return Web.Resources.EntityStrings.UsedPartsDisposalDetail_UsedPartsBatchNumber;
            }
        }

        public string UsedPartsDisposalDetail_UsedPartsCode {
            get {
                return Web.Resources.EntityStrings.UsedPartsDisposalDetail_UsedPartsCode;
            }
        }

        public string UsedPartsDisposalDetail_UsedPartsName {
            get {
                return Web.Resources.EntityStrings.UsedPartsDisposalDetail_UsedPartsName;
            }
        }

        public string UsedPartsDisposalDetail_UsedPartsSerialNumber {
            get {
                return Web.Resources.EntityStrings.UsedPartsDisposalDetail_UsedPartsSerialNumber;
            }
        }

        public string UsedPartsLoanBill_Code {
            get {
                return Web.Resources.EntityStrings.UsedPartsLoanBill_Code;
            }
        }

        public string UsedPartsLoanBill_IfReturned {
            get {
                return Web.Resources.EntityStrings.UsedPartsLoanBill_IfReturned;
            }
        }

        public string UsedPartsLoanBill_InboundStatus {
            get {
                return Web.Resources.EntityStrings.UsedPartsLoanBill_InboundStatus;
            }
        }

        public string UsedPartsLoanBill_LoanRequestor {
            get {
                return Web.Resources.EntityStrings.UsedPartsLoanBill_LoanRequestor;
            }
        }

        public string UsedPartsLoanBill_OutboundStatus {
            get {
                return Web.Resources.EntityStrings.UsedPartsLoanBill_OutboundStatus;
            }
        }

        public string UsedPartsLoanBill_PlannedReturnTime {
            get {
                return Web.Resources.EntityStrings.UsedPartsLoanBill_PlannedReturnTime;
            }
        }

        public string UsedPartsLoanBill_RelatedCompanyName {
            get {
                return Web.Resources.EntityStrings.UsedPartsLoanBill_RelatedCompanyName;
            }
        }

        public string UsedPartsLoanBill_ReturnExecutor {
            get {
                return Web.Resources.EntityStrings.UsedPartsLoanBill_ReturnExecutor;
            }
        }

        public string UsedPartsLoanBill_UsedPartsWarehouseCode {
            get {
                return Web.Resources.EntityStrings.UsedPartsLoanBill_UsedPartsWarehouseCode;
            }
        }

        public string UsedPartsLoanBill_UsedPartsWarehouseName {
            get {
                return Web.Resources.EntityStrings.UsedPartsLoanBill_UsedPartsWarehouseName;
            }
        }

        public string UsedPartsLoanDetail_ConfirmedAmount {
            get {
                return Web.Resources.EntityStrings.UsedPartsLoanDetail_ConfirmedAmount;
            }
        }

        public string UsedPartsLoanDetail_InboundAmount {
            get {
                return Web.Resources.EntityStrings.UsedPartsLoanDetail_InboundAmount;
            }
        }

        public string UsedPartsLoanDetail_OutboundAmount {
            get {
                return Web.Resources.EntityStrings.UsedPartsLoanDetail_OutboundAmount;
            }
        }

        public string UsedPartsLoanDetail_PlannedAmount {
            get {
                return Web.Resources.EntityStrings.UsedPartsLoanDetail_PlannedAmount;
            }
        }

        public string UsedPartsLoanDetail_Price {
            get {
                return Web.Resources.EntityStrings.UsedPartsLoanDetail_Price;
            }
        }

        public string UsedPartsLoanDetail_UsedPartsBarCode {
            get {
                return Web.Resources.EntityStrings.UsedPartsLoanDetail_UsedPartsBarCode;
            }
        }

        public string UsedPartsLoanDetail_UsedPartsBatchNumber {
            get {
                return Web.Resources.EntityStrings.UsedPartsLoanDetail_UsedPartsBatchNumber;
            }
        }

        public string UsedPartsLoanDetail_UsedPartsCode {
            get {
                return Web.Resources.EntityStrings.UsedPartsLoanDetail_UsedPartsCode;
            }
        }

        public string UsedPartsLoanDetail_UsedPartsName {
            get {
                return Web.Resources.EntityStrings.UsedPartsLoanDetail_UsedPartsName;
            }
        }

        public string UsedPartsLoanDetail_UsedPartsSerialNumber {
            get {
                return Web.Resources.EntityStrings.UsedPartsLoanDetail_UsedPartsSerialNumber;
            }
        }

        public string UsedPartsLoanBill_UsedPartsLoanDetails {
            get {
                return Web.Resources.EntityStrings.UsedPartsLoanBill_UsedPartsLoanDetails;
            }
        }

        public string UsedPartsOutboundDetail_SettlementPrice {
            get {
                return Web.Resources.EntityStrings.UsedPartsOutboundDetail_SettlementPrice;
            }
        }

        public string UsedPartsOutboundDetail_CostPrice {
            get {
                return Web.Resources.EntityStrings.UsedPartsOutboundDetail_CostPrice;
            }
        }

        public string UsedPartsOutboundDetail_Quantity {
            get {
                return Web.Resources.EntityStrings.UsedPartsOutboundDetail_Quantity;
            }
        }

        public string UsedPartsOutboundOrder_Code {
            get {
                return Web.Resources.EntityStrings.UsedPartsOutboundOrder_Code;
            }
        }

        public string UsedPartsOutboundOrder_IfAlreadySettled {
            get {
                return Web.Resources.EntityStrings.UsedPartsOutboundOrder_IfAlreadySettled;
            }
        }

        public string UsedPartsOutboundOrder_IfBillable {
            get {
                return Web.Resources.EntityStrings.UsedPartsOutboundOrder_IfBillable;
            }
        }

        public string UsedPartsOutboundOrder_OutboundType {
            get {
                return Web.Resources.EntityStrings.UsedPartsOutboundOrder_OutboundType;
            }
        }

        public string UsedPartsOutboundOrder_SourceCode {
            get {
                return Web.Resources.EntityStrings.UsedPartsOutboundOrder_SourceCode;
            }
        }

        public string UsedPartsWarehouseArea_Code {
            get {
                return Web.Resources.EntityStrings.UsedPartsWarehouseArea_Code;
            }
        }

        public string UsedPartsShiftDetail_Quantity {
            get {
                return Web.Resources.EntityStrings.UsedPartsShiftDetail_Quantity;
            }
        }

        public string UsedPartsShiftDetail_UsedPartsBarCode {
            get {
                return Web.Resources.EntityStrings.UsedPartsShiftDetail_UsedPartsBarCode;
            }
        }

        public string UsedPartsShiftDetail_UsedPartsBatchNumber {
            get {
                return Web.Resources.EntityStrings.UsedPartsShiftDetail_UsedPartsBatchNumber;
            }
        }

        public string UsedPartsShiftDetail_UsedPartsSerialNumber {
            get {
                return Web.Resources.EntityStrings.UsedPartsShiftDetail_UsedPartsSerialNumber;
            }
        }

        public string UsedPartsShiftOrder_Code {
            get {
                return Web.Resources.EntityStrings.UsedPartsShiftOrder_Code;
            }
        }

        public string UsedPartsShiftOrder_UsedPartsShiftDetails {
            get {
                return Web.Resources.EntityStrings.UsedPartsShiftOrder_UsedPartsShiftDetails;
            }
        }

        public string UsedPartsShiftOrder_UsedPartsWarehouseCode {
            get {
                return Web.Resources.EntityStrings.UsedPartsShiftOrder_UsedPartsWarehouseCode;
            }
        }

        public string UsedPartsShiftOrder_UsedPartsWarehouseName {
            get {
                return Web.Resources.EntityStrings.UsedPartsShiftOrder_UsedPartsWarehouseName;
            }
        }

        public string UsedPartsShiftDetail_DestiStoragePositionCode {
            get {
                return Web.Resources.EntityStrings.UsedPartsShiftDetail_DestiStoragePositionCode;
            }
        }

        public string PartsBranch_ProductLifeCycle {
            get {
                return Web.Resources.EntityStrings.PartsBranch_ProductLifeCycle;
            }
        }

        public string PartsBranch_DedicatedProvision {
            get {
                return Web.Resources.EntityStrings.PartsBranch_DedicatedProvision;
            }
        }

        public string PartsBranch_PackingAmount {
            get {
                return Web.Resources.EntityStrings.PartsBranch_PackingAmount;
            }
        }

        public string PartsBranch_PackingSpecification {
            get {
                return Web.Resources.EntityStrings.PartsBranch_PackingSpecification;
            }
        }

        public string ServiceTripPriceGrade_RegionDescription {
            get {
                return Web.Resources.EntityStrings.ServiceTripPriceGrade_RegionDescription;
            }
        }

        public string ServiceTripPriceRate_ServiceTripType {
            get {
                return Web.Resources.EntityStrings.ServiceTripPriceRate_ServiceTripType;
            }
        }

        public string ServiceTripPriceGrade_ServiceTripPriceRates {
            get {
                return Web.Resources.EntityStrings.ServiceTripPriceGrade_ServiceTripPriceRates;
            }
        }

        public string LaborHourUnitPriceGrade_Name {
            get {
                return Web.Resources.EntityStrings.LaborHourUnitPriceGrade_Name;
            }
        }

        public string LaborHourUnitPriceRate_Code {
            get {
                return Web.Resources.EntityStrings.LaborHourUnitPriceRate_Code;
            }
        }

        public string LaborHourUnitPriceRate_Price {
            get {
                return Web.Resources.EntityStrings.LaborHourUnitPriceRate_Price;
            }
        }

        public string LaborHourUnitPriceGrade_LaborHourUnitPriceRates {
            get {
                return Web.Resources.EntityStrings.LaborHourUnitPriceGrade_LaborHourUnitPriceRates;
            }
        }

        public string PartsManagementCostRate_InvoiceType {
            get {
                return Web.Resources.EntityStrings.PartsManagementCostRate_InvoiceType;
            }
        }

        public string PartsManagementCostRate_Rate {
            get {
                return Web.Resources.EntityStrings.PartsManagementCostRate_Rate;
            }
        }

        public string PartsManagementCostRate_PartsPriceLowerLimit {
            get {
                return Web.Resources.EntityStrings.PartsManagementCostRate_PartsPriceLowerLimit;
            }
        }

        public string PartsManagementCostRate_PartsPriceUpperLimit {
            get {
                return Web.Resources.EntityStrings.PartsManagementCostRate_PartsPriceUpperLimit;
            }
        }

        public string PartsManagementCostGrade_PartsManagementCostRates {
            get {
                return Web.Resources.EntityStrings.PartsManagementCostGrade_PartsManagementCostRates;
            }
        }

        public string PartsManagementCostGrade_Name {
            get {
                return Web.Resources.EntityStrings.PartsManagementCostGrade_Name;
            }
        }

        public string ServiceActivity_Code {
            get {
                return Web.Resources.EntityStrings.ServiceActivity_Code;
            }
        }

        public string ServiceActivity_Name {
            get {
                return Web.Resources.EntityStrings.ServiceActivity_Name;
            }
        }

        public string RepairItemAffiServProdLine_DefaultLaborHour {
            get {
                return Web.Resources.EntityStrings.RepairItemAffiServProdLine_DefaultLaborHour;
            }
        }

        public string RepairItemCategory_RepairItems {
            get {
                return Web.Resources.EntityStrings.RepairItemCategory_RepairItems;
            }
        }

        public string ServiceActivity_Type {
            get {
                return Web.Resources.EntityStrings.ServiceActivity_Type;
            }
        }

        public string MalfCatAffiVehiCat_MalfunctionCategoryId {
            get {
                return Web.Resources.EntityStrings.MalfCatAffiVehiCat_MalfunctionCategoryId;
            }
        }

        public string ServiceActivityCondition_AttributeName {
            get {
                return Web.Resources.EntityStrings.ServiceActivityCondition_AttributeName;
            }
        }

        public string ServiceActivityCondition_DataType {
            get {
                return Web.Resources.EntityStrings.ServiceActivityCondition_DataType;
            }
        }

        public string MalfCatAffiVehiCat_VehicleCategoryId {
            get {
                return Web.Resources.EntityStrings.MalfCatAffiVehiCat_VehicleCategoryId;
            }
        }

        public string MalfunctionCategory_Malfunctions {
            get {
                return Web.Resources.EntityStrings.MalfunctionCategory_Malfunctions;
            }
        }

        public string ServiceActivityCondition_MathOperator {
            get {
                return Web.Resources.EntityStrings.ServiceActivityCondition_MathOperator;
            }
        }

        public string ServiceActivityCondition_Value {
            get {
                return Web.Resources.EntityStrings.ServiceActivityCondition_Value;
            }
        }

        public string ServiceActivityMaterialDetail_Quantity {
            get {
                return Web.Resources.EntityStrings.ServiceActivityMaterialDetail_Quantity;
            }
        }

        public string Malfunction_Code {
            get {
                return Web.Resources.EntityStrings.Malfunction_Code;
            }
        }

        public string Malfunction_Description {
            get {
                return Web.Resources.EntityStrings.Malfunction_Description;
            }
        }

        public string Malfunction_QuickCode {
            get {
                return Web.Resources.EntityStrings.Malfunction_QuickCode;
            }
        }

        public string ServiceActivityQuota_Quota {
            get {
                return Web.Resources.EntityStrings.ServiceActivityQuota_Quota;
            }
        }

        public string ServiceActivityQuota_QuotaUsed {
            get {
                return Web.Resources.EntityStrings.ServiceActivityQuota_QuotaUsed;
            }
        }

        public string ServiceActivityVehicleList_AlreadyUsed {
            get {
                return Web.Resources.EntityStrings.ServiceActivityVehicleList_AlreadyUsed;
            }
        }

        public string ServiceActivityVehicleList_VIN {
            get {
                return Web.Resources.EntityStrings.ServiceActivityVehicleList_VIN;
            }
        }

        public string ServiceActivity_ApplicationScope {
            get {
                return Web.Resources.EntityStrings.ServiceActivity_ApplicationScope;
            }
        }

        public string ServiceActivity_AutomaticApproval {
            get {
                return Web.Resources.EntityStrings.ServiceActivity_AutomaticApproval;
            }
        }

        public string ServiceActivity_Content {
            get {
                return Web.Resources.EntityStrings.ServiceActivity_Content;
            }
        }

        public string ServiceActivity_DealerQuotaLimit {
            get {
                return Web.Resources.EntityStrings.ServiceActivity_DealerQuotaLimit;
            }
        }

        public string ServiceActivity_EndDate {
            get {
                return Web.Resources.EntityStrings.ServiceActivity_EndDate;
            }
        }

        public string ServiceActivity_StartDate {
            get {
                return Web.Resources.EntityStrings.ServiceActivity_StartDate;
            }
        }

        public string ServiceActivityRepairItem_DefaultLaborHour {
            get {
                return Web.Resources.EntityStrings.ServiceActivityRepairItem_DefaultLaborHour;
            }
        }

        public string ServiceActivity_ServiceActivityConditions {
            get {
                return Web.Resources.EntityStrings.ServiceActivity_ServiceActivityConditions;
            }
        }

        public string ServiceActivity_ServiceActivityQuotas {
            get {
                return Web.Resources.EntityStrings.ServiceActivity_ServiceActivityQuotas;
            }
        }

        public string ServiceActivity_ServiceActivityRepairItems {
            get {
                return Web.Resources.EntityStrings.ServiceActivity_ServiceActivityRepairItems;
            }
        }

        public string ServiceActivity_ServiceActivityVehicleLists {
            get {
                return Web.Resources.EntityStrings.ServiceActivity_ServiceActivityVehicleLists;
            }
        }

        public string ResponsibleUnit_Code {
            get {
                return Web.Resources.EntityStrings.ResponsibleUnit_Code;
            }
        }

        public string ResponsibleUnit_Name {
            get {
                return Web.Resources.EntityStrings.ResponsibleUnit_Name;
            }
        }

        public string ServiceActivity_BillingMethod {
            get {
                return Web.Resources.EntityStrings.ServiceActivity_BillingMethod;
            }
        }

        public string VehicleInformation_ChassisSerialNumber {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_ChassisSerialNumber;
            }
        }

        public string VehicleInformation_EngineSerialNumber {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_EngineSerialNumber;
            }
        }

        public string VehicleInformation_FabricationDate {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_FabricationDate;
            }
        }

        public string VehicleInformation_GearSerialNumber {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_GearSerialNumber;
            }
        }

        public string VehicleInformation_OutOfFactoryDate {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_OutOfFactoryDate;
            }
        }

        public string VehicleInformation_ProductCode {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_ProductCode;
            }
        }

        public string VehicleInformation_SalesDate {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_SalesDate;
            }
        }

        public string VehicleInformation_TradeMarkBrand {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_TradeMarkBrand;
            }
        }

        public string VehicleInformation_VIN {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_VIN;
            }
        }

        public string PartsSalesOrderType_Code {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrderType_Code;
            }
        }

        public string PartsSalesOrderType_Name {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrderType_Name;
            }
        }

        public string PartsSalesCategory_Code {
            get {
                return Web.Resources.EntityStrings.PartsSalesCategory_Code;
            }
        }

        public string PartsSalesCategory_Name {
            get {
                return Web.Resources.EntityStrings.PartsSalesCategory_Name;
            }
        }

        public string CustomerInformation_ContactPerson {
            get {
                return Web.Resources.EntityStrings.CustomerInformation_ContactPerson;
            }
        }

        public string CustomerInformation_ContactPhone {
            get {
                return Web.Resources.EntityStrings.CustomerInformation_ContactPhone;
            }
        }

        public string PartsRetailGuidePrice_SparePartCode {
            get {
                return Web.Resources.EntityStrings.PartsRetailGuidePrice_SparePartCode;
            }
        }

        public string PartsRetailGuidePrice_SparePartName {
            get {
                return Web.Resources.EntityStrings.PartsRetailGuidePrice_SparePartName;
            }
        }

        public string PartsRetailGuidePriceHistory_ExpireTime {
            get {
                return Web.Resources.EntityStrings.PartsRetailGuidePriceHistory_ExpireTime;
            }
        }

        public string PartsRetailGuidePriceHistory_RetailGuidePrice {
            get {
                return Web.Resources.EntityStrings.PartsRetailGuidePriceHistory_RetailGuidePrice;
            }
        }

        public string PartsRetailGuidePriceHistory_SparePartCode {
            get {
                return Web.Resources.EntityStrings.PartsRetailGuidePriceHistory_SparePartCode;
            }
        }

        public string PartsRetailGuidePriceHistory_SparePartName {
            get {
                return Web.Resources.EntityStrings.PartsRetailGuidePriceHistory_SparePartName;
            }
        }

        public string PartsRetailGuidePriceHistory_ValidationTime {
            get {
                return Web.Resources.EntityStrings.PartsRetailGuidePriceHistory_ValidationTime;
            }
        }

        public string PartsRetailGuidePrice_ExpireTime {
            get {
                return Web.Resources.EntityStrings.PartsRetailGuidePrice_ExpireTime;
            }
        }

        public string PartsRetailGuidePrice_RetailGuidePrice {
            get {
                return Web.Resources.EntityStrings.PartsRetailGuidePrice_RetailGuidePrice;
            }
        }

        public string PartsRetailGuidePrice_ValidationTime {
            get {
                return Web.Resources.EntityStrings.PartsRetailGuidePrice_ValidationTime;
            }
        }

        public string CustomerOrderPriceGradeHistory_Coefficient {
            get {
                return Web.Resources.EntityStrings.CustomerOrderPriceGradeHistory_Coefficient;
            }
        }

        public string CustomerOrderPriceGradeHistory_CustomerCompanyCode {
            get {
                return Web.Resources.EntityStrings.CustomerOrderPriceGradeHistory_CustomerCompanyCode;
            }
        }

        public string CustomerOrderPriceGradeHistory_CustomerCompanyName {
            get {
                return Web.Resources.EntityStrings.CustomerOrderPriceGradeHistory_CustomerCompanyName;
            }
        }

        public string CustomerOrderPriceGrade_Coefficient {
            get {
                return Web.Resources.EntityStrings.CustomerOrderPriceGrade_Coefficient;
            }
        }

        public string CustomerOrderPriceGrade_CustomerCompanyCode {
            get {
                return Web.Resources.EntityStrings.CustomerOrderPriceGrade_CustomerCompanyCode;
            }
        }

        public string CustomerOrderPriceGrade_CustomerCompanyName {
            get {
                return Web.Resources.EntityStrings.CustomerOrderPriceGrade_CustomerCompanyName;
            }
        }

        public string PartsSpecialTreatyPrice_DealerCode {
            get {
                return Web.Resources.EntityStrings.PartsSpecialTreatyPrice_DealerCode;
            }
        }

        public string PartsSpecialTreatyPrice_DealerName {
            get {
                return Web.Resources.EntityStrings.PartsSpecialTreatyPrice_DealerName;
            }
        }

        public string PartsSpecialTreatyPrice_ExpireTime {
            get {
                return Web.Resources.EntityStrings.PartsSpecialTreatyPrice_ExpireTime;
            }
        }

        public string PartsSpecialTreatyPrice_PartsSalesCategoryCode {
            get {
                return Web.Resources.EntityStrings.PartsSpecialTreatyPrice_PartsSalesCategoryCode;
            }
        }

        public string PartsSpecialTreatyPrice_PartsSalesCategoryName {
            get {
                return Web.Resources.EntityStrings.PartsSpecialTreatyPrice_PartsSalesCategoryName;
            }
        }

        public string PartsSpecialTreatyPrice_SparePartCode {
            get {
                return Web.Resources.EntityStrings.PartsSpecialTreatyPrice_SparePartCode;
            }
        }

        public string PartsSpecialTreatyPrice_SparePartName {
            get {
                return Web.Resources.EntityStrings.PartsSpecialTreatyPrice_SparePartName;
            }
        }

        public string PartsSpecialTreatyPrice_TreatyPrice {
            get {
                return Web.Resources.EntityStrings.PartsSpecialTreatyPrice_TreatyPrice;
            }
        }

        public string PartsSpecialTreatyPrice_ValidationTime {
            get {
                return Web.Resources.EntityStrings.PartsSpecialTreatyPrice_ValidationTime;
            }
        }

        public string PartsSalesCategory_PartsSalesOrderTypes {
            get {
                return Web.Resources.EntityStrings.PartsSalesCategory_PartsSalesOrderTypes;
            }
        }

        public string PartsSalesPriceHistory_PartsSalesCategoryCode {
            get {
                return Web.Resources.EntityStrings.PartsSalesPriceHistory_PartsSalesCategoryCode;
            }
        }

        public string PartsSalesPriceHistory_PartsSalesCategoryName {
            get {
                return Web.Resources.EntityStrings.PartsSalesPriceHistory_PartsSalesCategoryName;
            }
        }

        public string PartsSalesPriceHistory_PriceType {
            get {
                return Web.Resources.EntityStrings.PartsSalesPriceHistory_PriceType;
            }
        }

        public string PartsSalesPriceHistory_SalesPrice {
            get {
                return Web.Resources.EntityStrings.PartsSalesPriceHistory_SalesPrice;
            }
        }

        public string PartsSalesPriceHistory_SparePartCode {
            get {
                return Web.Resources.EntityStrings.PartsSalesPriceHistory_SparePartCode;
            }
        }

        public string PartsSalesPriceHistory_SparePartName {
            get {
                return Web.Resources.EntityStrings.PartsSalesPriceHistory_SparePartName;
            }
        }

        public string PartsSalesPrice_PartsSalesCategoryCode {
            get {
                return Web.Resources.EntityStrings.PartsSalesPrice_PartsSalesCategoryCode;
            }
        }

        public string PartsSalesPrice_PartsSalesCategoryName {
            get {
                return Web.Resources.EntityStrings.PartsSalesPrice_PartsSalesCategoryName;
            }
        }

        public string PartsSalesPrice_PriceType {
            get {
                return Web.Resources.EntityStrings.PartsSalesPrice_PriceType;
            }
        }

        public string PartsSalesPrice_SalesPrice {
            get {
                return Web.Resources.EntityStrings.PartsSalesPrice_SalesPrice;
            }
        }

        public string PartsSalesPrice_SparePartCode {
            get {
                return Web.Resources.EntityStrings.PartsSalesPrice_SparePartCode;
            }
        }

        public string PartsSalesPrice_SparePartName {
            get {
                return Web.Resources.EntityStrings.PartsSalesPrice_SparePartName;
            }
        }

        public string PartsSalesPriceHistory_ExpireTime {
            get {
                return Web.Resources.EntityStrings.PartsSalesPriceHistory_ExpireTime;
            }
        }

        public string PartsSalesPriceHistory_ValidationTime {
            get {
                return Web.Resources.EntityStrings.PartsSalesPriceHistory_ValidationTime;
            }
        }

        public string PartsSalesPrice_ExpireTime {
            get {
                return Web.Resources.EntityStrings.PartsSalesPrice_ExpireTime;
            }
        }

        public string PartsSalesPrice_ValidationTime {
            get {
                return Web.Resources.EntityStrings.PartsSalesPrice_ValidationTime;
            }
        }

        public string SupplierShippingDetail_MeasureUnit {
            get {
                return Web.Resources.EntityStrings.SupplierShippingDetail_MeasureUnit;
            }
        }

        public string SupplierShippingDetail_SparePartCode {
            get {
                return Web.Resources.EntityStrings.SupplierShippingDetail_SparePartCode;
            }
        }

        public string SupplierShippingDetail_SparePartName {
            get {
                return Web.Resources.EntityStrings.SupplierShippingDetail_SparePartName;
            }
        }

        public string SupplierShippingOrder_ArrivalDate {
            get {
                return Web.Resources.EntityStrings.SupplierShippingOrder_ArrivalDate;
            }
        }

        public string SupplierShippingOrder_BranchName {
            get {
                return Web.Resources.EntityStrings.SupplierShippingOrder_BranchName;
            }
        }

        public string SupplierShippingOrder_Code {
            get {
                return Web.Resources.EntityStrings.SupplierShippingOrder_Code;
            }
        }

        public string SupplierShippingOrder_DeliveryBillNumber {
            get {
                return Web.Resources.EntityStrings.SupplierShippingOrder_DeliveryBillNumber;
            }
        }

        public string SupplierShippingOrder_Driver {
            get {
                return Web.Resources.EntityStrings.SupplierShippingOrder_Driver;
            }
        }

        public string SupplierShippingOrder_IfDirectProvision {
            get {
                return Web.Resources.EntityStrings.SupplierShippingOrder_IfDirectProvision;
            }
        }

        public string SupplierShippingOrder_LogisticCompany {
            get {
                return Web.Resources.EntityStrings.SupplierShippingOrder_LogisticCompany;
            }
        }

        public string SupplierShippingOrder_PartsPurchaseOrderCode {
            get {
                return Web.Resources.EntityStrings.SupplierShippingOrder_PartsPurchaseOrderCode;
            }
        }

        public string SupplierShippingOrder_PartsSupplierCode {
            get {
                return Web.Resources.EntityStrings.SupplierShippingOrder_PartsSupplierCode;
            }
        }

        public string SupplierShippingOrder_PartsSupplierName {
            get {
                return Web.Resources.EntityStrings.SupplierShippingOrder_PartsSupplierName;
            }
        }

        public string SupplierShippingOrder_Phone {
            get {
                return Web.Resources.EntityStrings.SupplierShippingOrder_Phone;
            }
        }

        public string SupplierShippingOrder_ReceivingAddress {
            get {
                return Web.Resources.EntityStrings.SupplierShippingOrder_ReceivingAddress;
            }
        }

        public string SupplierShippingOrder_ReceivingCompanyName {
            get {
                return Web.Resources.EntityStrings.SupplierShippingOrder_ReceivingCompanyName;
            }
        }

        public string SupplierShippingOrder_ReceivingWarehouseName {
            get {
                return Web.Resources.EntityStrings.SupplierShippingOrder_ReceivingWarehouseName;
            }
        }

        public string PartsPurchaseOrder_BranchName {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseOrder_BranchName;
            }
        }

        public string PartsPurchaseOrder_Code {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseOrder_Code;
            }
        }

        public string PartsPurchaseOrder_ConfirmationRemark {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseOrder_ConfirmationRemark;
            }
        }

        public string PartsPurchaseOrder_IfDirectProvision {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseOrder_IfDirectProvision;
            }
        }

        public string PartsPurchaseOrder_OrderType {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseOrder_OrderType;
            }
        }

        public string PartsPurchaseOrder_ReceivingAddress {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseOrder_ReceivingAddress;
            }
        }

        public string PartsPurchaseOrder_ReceivingCompanyName {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseOrder_ReceivingCompanyName;
            }
        }

        public string PartsPurchaseOrder_RequestedDeliveryTime {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseOrder_RequestedDeliveryTime;
            }
        }

        public string PartsPurchaseOrder_ShippingMethod {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseOrder_ShippingMethod;
            }
        }

        public string PartsPurchaseOrder_TotalAmount {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseOrder_TotalAmount;
            }
        }

        public string PartsPurchaseOrder_WarehouseName {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseOrder_WarehouseName;
            }
        }

        public string PartsPurchaseOrder_PartsSupplierCode {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseOrder_PartsSupplierCode;
            }
        }

        public string PartsPurchaseOrder_SubmitterName {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseOrder_SubmitterName;
            }
        }

        public string PartsPurchaseOrder_CloserName {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseOrder_CloserName;
            }
        }

        public string PartsPurchaseOrder_SubmitTime {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseOrder_SubmitTime;
            }
        }

        public string PartsPurchaseOrder_CloseTime {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseOrder_CloseTime;
            }
        }

        public string PartsPurchaseOrder_PartsSupplierName {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseOrder_PartsSupplierName;
            }
        }

        public string SupplierShippingOrder_ShippingDate {
            get {
                return Web.Resources.EntityStrings.SupplierShippingOrder_ShippingDate;
            }
        }

        public string SupplierShippingOrder_ShippingMethod {
            get {
                return Web.Resources.EntityStrings.SupplierShippingOrder_ShippingMethod;
            }
        }

        public string SupplierShippingOrder_SupplierShippingDetails {
            get {
                return Web.Resources.EntityStrings.SupplierShippingOrder_SupplierShippingDetails;
            }
        }

        public string SupplierShippingOrder_VehicleLicensePlate {
            get {
                return Web.Resources.EntityStrings.SupplierShippingOrder_VehicleLicensePlate;
            }
        }

        public string PartsPurchaseOrderDetail_ConfirmationRemark {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseOrderDetail_ConfirmationRemark;
            }
        }

        public string PartsPurchaseOrderDetail_ConfirmedAmount {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseOrderDetail_ConfirmedAmount;
            }
        }

        public string SsUsedPartsDisposalDetail_UsedPartsCode {
            get {
                return Web.Resources.EntityStrings.SsUsedPartsDisposalDetail_UsedPartsCode;
            }
        }

        public string SsUsedPartsDisposalDetail_UsedPartsName {
            get {
                return Web.Resources.EntityStrings.SsUsedPartsDisposalDetail_UsedPartsName;
            }
        }

        public string PartsPurchaseOrderDetail_MeasureUnit {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseOrderDetail_MeasureUnit;
            }
        }

        public string PartsPurchaseOrderDetail_OrderAmount {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseOrderDetail_OrderAmount;
            }
        }

        public string PartsPurchaseOrderDetail_PackingAmount {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseOrderDetail_PackingAmount;
            }
        }

        public string PartsPurchaseOrderDetail_PackingSpecification {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseOrderDetail_PackingSpecification;
            }
        }

        public string PartsPurchaseOrderDetail_PromisedDeliveryTime {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseOrderDetail_PromisedDeliveryTime;
            }
        }

        public string PartsPurchaseOrderDetail_ShippingAmount {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseOrderDetail_ShippingAmount;
            }
        }

        public string PartsPurchaseOrderDetail_SparePartCode {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseOrderDetail_SparePartCode;
            }
        }

        public string PartsPurchaseOrderDetail_SparePartName {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseOrderDetail_SparePartName;
            }
        }

        public string PartsPurchaseOrderDetail_SupplierPartCode {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseOrderDetail_SupplierPartCode;
            }
        }

        public string PartsPurchaseOrderDetail_UnitPrice {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseOrderDetail_UnitPrice;
            }
        }

        public string SupplierShippingDetail_Quantity {
            get {
                return Web.Resources.EntityStrings.SupplierShippingDetail_Quantity;
            }
        }

        public string SupplierShippingDetail_SupplierPartCode {
            get {
                return Web.Resources.EntityStrings.SupplierShippingDetail_SupplierPartCode;
            }
        }

        public string ServiceActivity_ShippingMethod {
            get {
                return Web.Resources.EntityStrings.ServiceActivity_ShippingMethod;
            }
        }

        public string PartsPurchaseOrder_PartsPurchaseOrderDetails {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseOrder_PartsPurchaseOrderDetails;
            }
        }

        public string SupplierShippingDetail_PendingQuantity {
            get {
                return Web.Resources.EntityStrings.SupplierShippingDetail_PendingQuantity;
            }
        }

        public string RepairClaimAppDetail_Quantity {
            get {
                return Web.Resources.EntityStrings.RepairClaimAppDetail_Quantity;
            }
        }

        public string RepairClaimAppDetail_SparePartCode {
            get {
                return Web.Resources.EntityStrings.RepairClaimAppDetail_SparePartCode;
            }
        }

        public string RepairClaimAppDetail_SparePartName {
            get {
                return Web.Resources.EntityStrings.RepairClaimAppDetail_SparePartName;
            }
        }

        public string RepairClaimApplication_ApplicationType {
            get {
                return Web.Resources.EntityStrings.RepairClaimApplication_ApplicationType;
            }
        }

        public string RepairClaimApplication_EstimatedLaborCost {
            get {
                return Web.Resources.EntityStrings.RepairClaimApplication_EstimatedLaborCost;
            }
        }

        public string RepairClaimApplication_EstimatedMaterialCost {
            get {
                return Web.Resources.EntityStrings.RepairClaimApplication_EstimatedMaterialCost;
            }
        }

        public string RepairClaimApplication_MalfunctionReason {
            get {
                return Web.Resources.EntityStrings.RepairClaimApplication_MalfunctionReason;
            }
        }

        public string RepairClaimApplication_MalfunctionTime {
            get {
                return Web.Resources.EntityStrings.RepairClaimApplication_MalfunctionTime;
            }
        }

        public string RepairClaimApplication_OtherCost {
            get {
                return Web.Resources.EntityStrings.RepairClaimApplication_OtherCost;
            }
        }

        public string RepairClaimApplication_RepairClaimAppDetails {
            get {
                return Web.Resources.EntityStrings.RepairClaimApplication_RepairClaimAppDetails;
            }
        }

        public string RepairClaimApplication_TotalAmount {
            get {
                return Web.Resources.EntityStrings.RepairClaimApplication_TotalAmount;
            }
        }

        public string RepairClaimApplication_VehicleLicensePlate {
            get {
                return Web.Resources.EntityStrings.RepairClaimApplication_VehicleLicensePlate;
            }
        }

        public string PartsClaimApplicationDetail_Quantity {
            get {
                return Web.Resources.EntityStrings.PartsClaimApplicationDetail_Quantity;
            }
        }

        public string PartsClaimApplicationDetail_SparePartCode {
            get {
                return Web.Resources.EntityStrings.PartsClaimApplicationDetail_SparePartCode;
            }
        }

        public string PartsClaimApplicationDetail_SparePartName {
            get {
                return Web.Resources.EntityStrings.PartsClaimApplicationDetail_SparePartName;
            }
        }

        public string PartsClaimApplication_EstimatedLaborCost {
            get {
                return Web.Resources.EntityStrings.PartsClaimApplication_EstimatedLaborCost;
            }
        }

        public string PartsClaimApplication_EstimatedMaterialCost {
            get {
                return Web.Resources.EntityStrings.PartsClaimApplication_EstimatedMaterialCost;
            }
        }

        public string PartsClaimApplication_MalfunctionReason {
            get {
                return Web.Resources.EntityStrings.PartsClaimApplication_MalfunctionReason;
            }
        }

        public string PartsClaimApplication_MalfunctionTime {
            get {
                return Web.Resources.EntityStrings.PartsClaimApplication_MalfunctionTime;
            }
        }

        public string PartsClaimApplication_OtherCost {
            get {
                return Web.Resources.EntityStrings.PartsClaimApplication_OtherCost;
            }
        }

        public string PartsClaimApplication_PartsClaimApplicationDetails {
            get {
                return Web.Resources.EntityStrings.PartsClaimApplication_PartsClaimApplicationDetails;
            }
        }

        public string PartsClaimApplication_TotalAmount {
            get {
                return Web.Resources.EntityStrings.PartsClaimApplication_TotalAmount;
            }
        }

        public string PartsClaimApplication_VehicleLicensePlate {
            get {
                return Web.Resources.EntityStrings.PartsClaimApplication_VehicleLicensePlate;
            }
        }

        public string RepairClaimBill_DealerCode {
            get {
                return Web.Resources.EntityStrings.RepairClaimBill_DealerCode;
            }
        }

        public string RepairClaimBill_DealerName {
            get {
                return Web.Resources.EntityStrings.RepairClaimBill_DealerName;
            }
        }

        public string RepairClaimBill_LaborCost {
            get {
                return Web.Resources.EntityStrings.RepairClaimBill_LaborCost;
            }
        }

        public string RepairClaimBill_MaterialCost {
            get {
                return Web.Resources.EntityStrings.RepairClaimBill_MaterialCost;
            }
        }

        public string RepairClaimBill_OtherCost {
            get {
                return Web.Resources.EntityStrings.RepairClaimBill_OtherCost;
            }
        }

        public string RepairClaimBill_PartsManagementCost {
            get {
                return Web.Resources.EntityStrings.RepairClaimBill_PartsManagementCost;
            }
        }

        public string RepairClaimBill_TotalAmount {
            get {
                return Web.Resources.EntityStrings.RepairClaimBill_TotalAmount;
            }
        }

        public string RepairClaimBill_VehicleLicensePlate {
            get {
                return Web.Resources.EntityStrings.RepairClaimBill_VehicleLicensePlate;
            }
        }

        public string RepairClaimBill_VIN {
            get {
                return Web.Resources.EntityStrings.RepairClaimBill_VIN;
            }
        }

        public string ServiceProductLine_Name {
            get {
                return Web.Resources.EntityStrings.ServiceProductLine_Name;
            }
        }

        public string ServiceTripClaimApplication_Code {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimApplication_Code;
            }
        }

        public string ServiceTripClaimApplication_EstimatedFieldServiceCharge {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimApplication_EstimatedFieldServiceCharge;
            }
        }

        public string ServiceTripClaimApplication_EstimatedTowCharge {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimApplication_EstimatedTowCharge;
            }
        }

        public string ServiceTripClaimApplication_FieldLocation {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimApplication_FieldLocation;
            }
        }

        public string ServiceTripClaimApplication_MalfunctionTime {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimApplication_MalfunctionTime;
            }
        }

        public string ServiceTripClaimApplication_OtherCost {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimApplication_OtherCost;
            }
        }

        public string ServiceTripClaimApplication_ServiceTripReason {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimApplication_ServiceTripReason;
            }
        }

        public string ServiceTripClaimApplication_ServiceTripType {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimApplication_ServiceTripType;
            }
        }

        public string ServiceTripClaimApplication_TotalAmount {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimApplication_TotalAmount;
            }
        }

        public string ServiceTripClaimApplication_VehicleLicensePlate {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimApplication_VehicleLicensePlate;
            }
        }

        public string ServiceTripClaimApplication_VIN {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimApplication_VIN;
            }
        }

        public string ServiceTripClaimBill_Code {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimBill_Code;
            }
        }

        public string ServiceTripClaimBill_DealerCode {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimBill_DealerCode;
            }
        }

        public string ServiceTripClaimBill_DealerName {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimBill_DealerName;
            }
        }

        public string ServiceTripClaimBill_FieldServiceCharge {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimBill_FieldServiceCharge;
            }
        }

        public string ServiceTripClaimBill_OtherCost {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimBill_OtherCost;
            }
        }

        public string ServiceTripClaimBill_ServiceTripClaimAppCode {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimBill_ServiceTripClaimAppCode;
            }
        }

        public string ServiceTripClaimBill_ServiceTripReason {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimBill_ServiceTripReason;
            }
        }

        public string ServiceTripClaimBill_ServiceTripTime {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimBill_ServiceTripTime;
            }
        }

        public string ServiceTripClaimBill_ServiceTripType {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimBill_ServiceTripType;
            }
        }

        public string ServiceTripClaimBill_TotalAmount {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimBill_TotalAmount;
            }
        }

        public string ServiceTripClaimBill_TowCharge {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimBill_TowCharge;
            }
        }

        public string ServiceTripClaimBill_VehicleLicensePlate {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimBill_VehicleLicensePlate;
            }
        }

        public string ServiceTripClaimBill_VIN {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimBill_VIN;
            }
        }

        public string ServiceTripClaimBill_SettlementStatus {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimBill_SettlementStatus;
            }
        }

        public string _Common_AbandonerId {
            get {
                return Web.Resources.EntityStrings._Common_AbandonerId;
            }
        }

        public string _Common_ApproverId {
            get {
                return Web.Resources.EntityStrings._Common_ApproverId;
            }
        }

        public string _Common_CloserId {
            get {
                return Web.Resources.EntityStrings._Common_CloserId;
            }
        }

        public string _Common_CloseTime {
            get {
                return Web.Resources.EntityStrings._Common_CloseTime;
            }
        }

        public string _Common_CloserName {
            get {
                return Web.Resources.EntityStrings._Common_CloserName;
            }
        }

        public string _Common_ConfirmationTime {
            get {
                return Web.Resources.EntityStrings._Common_ConfirmationTime;
            }
        }

        public string _Common_ConfirmorId {
            get {
                return Web.Resources.EntityStrings._Common_ConfirmorId;
            }
        }

        public string _Common_ConfirmorName {
            get {
                return Web.Resources.EntityStrings._Common_ConfirmorName;
            }
        }

        public string _Common_SubmitterId {
            get {
                return Web.Resources.EntityStrings._Common_SubmitterId;
            }
        }

        public string _Common_SubmitterName {
            get {
                return Web.Resources.EntityStrings._Common_SubmitterName;
            }
        }

        public string _Common_SubmitTime {
            get {
                return Web.Resources.EntityStrings._Common_SubmitTime;
            }
        }

        public string VehicleInformation_OldVIN {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_OldVIN;
            }
        }

        public string VehicleInformation_VehicleLicensePlate {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_VehicleLicensePlate;
            }
        }

        public string _Common_FinalApproverId {
            get {
                return Web.Resources.EntityStrings._Common_FinalApproverId;
            }
        }

        public string _Common_FinalApproverName {
            get {
                return Web.Resources.EntityStrings._Common_FinalApproverName;
            }
        }

        public string _Common_FinalApproveTime {
            get {
                return Web.Resources.EntityStrings._Common_FinalApproveTime;
            }
        }

        public string _Common_InitialApproverId {
            get {
                return Web.Resources.EntityStrings._Common_InitialApproverId;
            }
        }

        public string _Common_InitialApproverName {
            get {
                return Web.Resources.EntityStrings._Common_InitialApproverName;
            }
        }

        public string _Common_InitialApproveTime {
            get {
                return Web.Resources.EntityStrings._Common_InitialApproveTime;
            }
        }

        public string PartsLogisticBatchItemDetail_BatchNumber {
            get {
                return Web.Resources.EntityStrings.PartsLogisticBatchItemDetail_BatchNumber;
            }
        }

        public string _Common_ApproveCommentHistory {
            get {
                return Web.Resources.EntityStrings._Common_ApproveCommentHistory;
            }
        }

        public string _Common_FinalApproveComment {
            get {
                return Web.Resources.EntityStrings._Common_FinalApproveComment;
            }
        }

        public string _Common_InitialApproveComment {
            get {
                return Web.Resources.EntityStrings._Common_InitialApproveComment;
            }
        }

        public string RepairClaimBill_ClaimBillCode {
            get {
                return Web.Resources.EntityStrings.RepairClaimBill_ClaimBillCode;
            }
        }

        public string RepairClaimBill_ServiceActivityCode {
            get {
                return Web.Resources.EntityStrings.RepairClaimBill_ServiceActivityCode;
            }
        }

        public string RepairClaimBill_ServiceActivityContent {
            get {
                return Web.Resources.EntityStrings.RepairClaimBill_ServiceActivityContent;
            }
        }

        public string PartsClaimOrder_Code {
            get {
                return Web.Resources.EntityStrings.PartsClaimOrder_Code;
            }
        }

        public string PartsClaimOrder_DealerCode {
            get {
                return Web.Resources.EntityStrings.PartsClaimOrder_DealerCode;
            }
        }

        public string PartsClaimOrder_DealerName {
            get {
                return Web.Resources.EntityStrings.PartsClaimOrder_DealerName;
            }
        }

        public string PartsClaimOrder_LaborCost {
            get {
                return Web.Resources.EntityStrings.PartsClaimOrder_LaborCost;
            }
        }

        public string PartsClaimOrder_MaterialCost {
            get {
                return Web.Resources.EntityStrings.PartsClaimOrder_MaterialCost;
            }
        }

        public string PartsClaimOrder_OtherCost {
            get {
                return Web.Resources.EntityStrings.PartsClaimOrder_OtherCost;
            }
        }

        public string PartsClaimOrder_PartsManagementCost {
            get {
                return Web.Resources.EntityStrings.PartsClaimOrder_PartsManagementCost;
            }
        }

        public string PartsClaimOrder_TotalAmount {
            get {
                return Web.Resources.EntityStrings.PartsClaimOrder_TotalAmount;
            }
        }

        public string PartsClaimOrder_VehicleLicensePlate {
            get {
                return Web.Resources.EntityStrings.PartsClaimOrder_VehicleLicensePlate;
            }
        }

        public string PartsClaimOrder_VIN {
            get {
                return Web.Resources.EntityStrings.PartsClaimOrder_VIN;
            }
        }

        public string RepairClaimApplication_Motive {
            get {
                return Web.Resources.EntityStrings.RepairClaimApplication_Motive;
            }
        }

        public string RepairClaimApplication_ServiceActivityCode {
            get {
                return Web.Resources.EntityStrings.RepairClaimApplication_ServiceActivityCode;
            }
        }

        public string RepairClaimApplication_VIN {
            get {
                return Web.Resources.EntityStrings.RepairClaimApplication_VIN;
            }
        }

        public string ServiceTripClaimApplication_RepairRequestTime {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimApplication_RepairRequestTime;
            }
        }

        public string RepairClaimBill_SettlementStatus {
            get {
                return Web.Resources.EntityStrings.RepairClaimBill_SettlementStatus;
            }
        }

        public string RepairClaimBill_UsedPartsDisposalStatus {
            get {
                return Web.Resources.EntityStrings.RepairClaimBill_UsedPartsDisposalStatus;
            }
        }

        public string RepairClaimItemDetail_ApproveStatus {
            get {
                return Web.Resources.EntityStrings.RepairClaimItemDetail_ApproveStatus;
            }
        }

        public string RepairClaimApplication_RepairRequestTime {
            get {
                return Web.Resources.EntityStrings.RepairClaimApplication_RepairRequestTime;
            }
        }

        public string RepairClaimApplication_ContactAddress {
            get {
                return Web.Resources.EntityStrings.RepairClaimApplication_ContactAddress;
            }
        }

        public string RepairClaimApplication_ContactPhone {
            get {
                return Web.Resources.EntityStrings.RepairClaimApplication_ContactPhone;
            }
        }

        public string RepairClaimApplication_EstimatedFinishingTime {
            get {
                return Web.Resources.EntityStrings.RepairClaimApplication_EstimatedFinishingTime;
            }
        }

        public string RepairClaimApplication_MalfunctionLocation {
            get {
                return Web.Resources.EntityStrings.RepairClaimApplication_MalfunctionLocation;
            }
        }

        public string RepairClaimApplication_Mileage {
            get {
                return Web.Resources.EntityStrings.RepairClaimApplication_Mileage;
            }
        }

        public string RepairClaimApplication_OtherCostReason {
            get {
                return Web.Resources.EntityStrings.RepairClaimApplication_OtherCostReason;
            }
        }

        public string RepairClaimApplication_OutOfFactoryDate {
            get {
                return Web.Resources.EntityStrings.RepairClaimApplication_OutOfFactoryDate;
            }
        }

        public string RepairClaimApplication_RepairRequestOrderCode {
            get {
                return Web.Resources.EntityStrings.RepairClaimApplication_RepairRequestOrderCode;
            }
        }

        public string RepairClaimApplication_SalesDate {
            get {
                return Web.Resources.EntityStrings.RepairClaimApplication_SalesDate;
            }
        }

        public string RepairClaimApplication_SalesStatus {
            get {
                return Web.Resources.EntityStrings.RepairClaimApplication_SalesStatus;
            }
        }

        public string RepairClaimApplication_VehicleContactPerson {
            get {
                return Web.Resources.EntityStrings.RepairClaimApplication_VehicleContactPerson;
            }
        }

        public string RepairClaimApplication_ClaimStatus {
            get {
                return Web.Resources.EntityStrings.RepairClaimApplication_ClaimStatus;
            }
        }

        public string PartsClaimApplicationDetail_Price {
            get {
                return Web.Resources.EntityStrings.PartsClaimApplicationDetail_Price;
            }
        }

        public string PartsClaimApplication_Code {
            get {
                return Web.Resources.EntityStrings.PartsClaimApplication_Code;
            }
        }

        public string PartsClaimApplication_RepairRequestTime {
            get {
                return Web.Resources.EntityStrings.PartsClaimApplication_RepairRequestTime;
            }
        }

        public string RepairClaimBill_ContactAddress {
            get {
                return Web.Resources.EntityStrings.RepairClaimBill_ContactAddress;
            }
        }

        public string RepairClaimBill_ContactPhone {
            get {
                return Web.Resources.EntityStrings.RepairClaimBill_ContactPhone;
            }
        }

        public string RepairClaimBill_OtherCostReason {
            get {
                return Web.Resources.EntityStrings.RepairClaimBill_OtherCostReason;
            }
        }

        public string RepairClaimBill_OutOfFactoryDate {
            get {
                return Web.Resources.EntityStrings.RepairClaimBill_OutOfFactoryDate;
            }
        }

        public string RepairClaimBill_SalesDate {
            get {
                return Web.Resources.EntityStrings.RepairClaimBill_SalesDate;
            }
        }

        public string RepairClaimBill_VehicleContactPerson {
            get {
                return Web.Resources.EntityStrings.RepairClaimBill_VehicleContactPerson;
            }
        }

        public string RepairClaimBill_Mileage {
            get {
                return Web.Resources.EntityStrings.RepairClaimBill_Mileage;
            }
        }

        public string RepairClaimBill_SalesStatus {
            get {
                return Web.Resources.EntityStrings.RepairClaimBill_SalesStatus;
            }
        }

        public string RepairClaimMaterialDetail_MaterialCost {
            get {
                return Web.Resources.EntityStrings.RepairClaimMaterialDetail_MaterialCost;
            }
        }

        public string PartsClaimMaterialDetail_MaterialCost {
            get {
                return Web.Resources.EntityStrings.PartsClaimMaterialDetail_MaterialCost;
            }
        }

        public string PartsClaimMaterialDetail_NewPartsBatchNumber {
            get {
                return Web.Resources.EntityStrings.PartsClaimMaterialDetail_NewPartsBatchNumber;
            }
        }

        public string PartsClaimMaterialDetail_NewPartsCode {
            get {
                return Web.Resources.EntityStrings.PartsClaimMaterialDetail_NewPartsCode;
            }
        }

        public string PartsClaimMaterialDetail_NewPartsName {
            get {
                return Web.Resources.EntityStrings.PartsClaimMaterialDetail_NewPartsName;
            }
        }

        public string PartsClaimMaterialDetail_NewPartsSerialNumber {
            get {
                return Web.Resources.EntityStrings.PartsClaimMaterialDetail_NewPartsSerialNumber;
            }
        }

        public string PartsClaimMaterialDetail_UnitPrice {
            get {
                return Web.Resources.EntityStrings.PartsClaimMaterialDetail_UnitPrice;
            }
        }

        public string PartsClaimMaterialDetail_UsedPartsBatchNumber {
            get {
                return Web.Resources.EntityStrings.PartsClaimMaterialDetail_UsedPartsBatchNumber;
            }
        }

        public string RepairClaimMaterialDetail_UsedPartsBatchNumber {
            get {
                return Web.Resources.EntityStrings.RepairClaimMaterialDetail_UsedPartsBatchNumber;
            }
        }

        public string PartsClaimMaterialDetail_UsedPartsCode {
            get {
                return Web.Resources.EntityStrings.PartsClaimMaterialDetail_UsedPartsCode;
            }
        }

        public string PartsClaimMaterialDetail_UsedPartsName {
            get {
                return Web.Resources.EntityStrings.PartsClaimMaterialDetail_UsedPartsName;
            }
        }

        public string PartsClaimMaterialDetail_UsedPartsSerialNumber {
            get {
                return Web.Resources.EntityStrings.PartsClaimMaterialDetail_UsedPartsSerialNumber;
            }
        }

        public string RepairClaimMaterialDetail_UsedPartsSerialNumber {
            get {
                return Web.Resources.EntityStrings.RepairClaimMaterialDetail_UsedPartsSerialNumber;
            }
        }

        public string RepairClaimBill_RepairClaimItemDetails {
            get {
                return Web.Resources.EntityStrings.RepairClaimBill_RepairClaimItemDetails;
            }
        }

        public string RepairClaimItemDetail_RepairClaimMaterialDetails {
            get {
                return Web.Resources.EntityStrings.RepairClaimItemDetail_RepairClaimMaterialDetails;
            }
        }

        public string RepairClaimItemDetail_DefaultLaborHour {
            get {
                return Web.Resources.EntityStrings.RepairClaimItemDetail_DefaultLaborHour;
            }
        }

        public string RepairClaimItemDetail_LaborCost {
            get {
                return Web.Resources.EntityStrings.RepairClaimItemDetail_LaborCost;
            }
        }

        public string RepairClaimItemDetail_LaborUnitPrice {
            get {
                return Web.Resources.EntityStrings.RepairClaimItemDetail_LaborUnitPrice;
            }
        }

        public string RepairClaimMaterialDetail_NewPartsSecurityNumber {
            get {
                return Web.Resources.EntityStrings.RepairClaimMaterialDetail_NewPartsSecurityNumber;
            }
        }

        public string RepairClaimBill_FinishingTime {
            get {
                return Web.Resources.EntityStrings.RepairClaimBill_FinishingTime;
            }
        }

        public string RepairClaimBill_MalfunctionReason {
            get {
                return Web.Resources.EntityStrings.RepairClaimBill_MalfunctionReason;
            }
        }

        public string RepairClaimBill_RepairContractCode {
            get {
                return Web.Resources.EntityStrings.RepairClaimBill_RepairContractCode;
            }
        }

        public string RepairClaimBill_RepairRequestTime {
            get {
                return Web.Resources.EntityStrings.RepairClaimBill_RepairRequestTime;
            }
        }

        public string ServiceTripClaimBill_ClaimSupplierName {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimBill_ClaimSupplierName;
            }
        }

        public string ServiceTripClaimBill_ContactAddress {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimBill_ContactAddress;
            }
        }

        public string ServiceTripClaimBill_ContactPhone {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimBill_ContactPhone;
            }
        }

        public string ServiceTripClaimBill_FieldLocation {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimBill_FieldLocation;
            }
        }

        public string ServiceTripClaimBill_FinishingTime {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimBill_FinishingTime;
            }
        }

        public string ServiceTripClaimBill_Mileage {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimBill_Mileage;
            }
        }

        public string ServiceTripClaimBill_OtherCostReason {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimBill_OtherCostReason;
            }
        }

        public string ServiceTripClaimBill_OutOfFactoryDate {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimBill_OutOfFactoryDate;
            }
        }

        public string ServiceTripClaimBill_RepairRequestTime {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimBill_RepairRequestTime;
            }
        }

        public string ServiceTripClaimBill_SalesDate {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimBill_SalesDate;
            }
        }

        public string ServiceTripClaimBill_SalesStatus {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimBill_SalesStatus;
            }
        }

        public string ServiceTripClaimBill_ServiceTripDistance {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimBill_ServiceTripDistance;
            }
        }

        public string ServiceTripClaimBill_ServiceTripDuration {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimBill_ServiceTripDuration;
            }
        }

        public string ServiceTripClaimBill_ServiceTripPerson {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimBill_ServiceTripPerson;
            }
        }

        public string ServiceTripClaimBill_ServiceTripRegion {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimBill_ServiceTripRegion;
            }
        }

        public string ServiceTripClaimBill_VehicleContactPerson {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimBill_VehicleContactPerson;
            }
        }

        public string ServiceTripClaimBill_ApprovalComment {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimBill_ApprovalComment;
            }
        }

        public string RepairClaimBill_ServiceActivityType {
            get {
                return Web.Resources.EntityStrings.RepairClaimBill_ServiceActivityType;
            }
        }

        public string RepairClaimBill_ServiceDepartmentComment {
            get {
                return Web.Resources.EntityStrings.RepairClaimBill_ServiceDepartmentComment;
            }
        }

        public string RepairClaimAppDetail_Price {
            get {
                return Web.Resources.EntityStrings.RepairClaimAppDetail_Price;
            }
        }

        public string PartsClaimApplication_ClaimStatus {
            get {
                return Web.Resources.EntityStrings.PartsClaimApplication_ClaimStatus;
            }
        }

        public string PartsClaimApplication_ContactAddress {
            get {
                return Web.Resources.EntityStrings.PartsClaimApplication_ContactAddress;
            }
        }

        public string PartsClaimApplication_ContactPhone {
            get {
                return Web.Resources.EntityStrings.PartsClaimApplication_ContactPhone;
            }
        }

        public string PartsClaimApplication_EstimatedFinishingTime {
            get {
                return Web.Resources.EntityStrings.PartsClaimApplication_EstimatedFinishingTime;
            }
        }

        public string PartsClaimApplication_MalfunctionLocation {
            get {
                return Web.Resources.EntityStrings.PartsClaimApplication_MalfunctionLocation;
            }
        }

        public string PartsClaimApplication_Mileage {
            get {
                return Web.Resources.EntityStrings.PartsClaimApplication_Mileage;
            }
        }

        public string PartsClaimApplication_Motive {
            get {
                return Web.Resources.EntityStrings.PartsClaimApplication_Motive;
            }
        }

        public string PartsClaimApplication_OtherCostReason {
            get {
                return Web.Resources.EntityStrings.PartsClaimApplication_OtherCostReason;
            }
        }

        public string PartsClaimApplication_OutOfFactoryDate {
            get {
                return Web.Resources.EntityStrings.PartsClaimApplication_OutOfFactoryDate;
            }
        }

        public string PartsClaimApplication_RelatedBusinessBillCode {
            get {
                return Web.Resources.EntityStrings.PartsClaimApplication_RelatedBusinessBillCode;
            }
        }

        public string PartsClaimApplication_Remark {
            get {
                return Web.Resources.EntityStrings.PartsClaimApplication_Remark;
            }
        }

        public string PartsClaimApplication_RepairRequestOrderCode {
            get {
                return Web.Resources.EntityStrings.PartsClaimApplication_RepairRequestOrderCode;
            }
        }

        public string PartsClaimApplication_SalesDate {
            get {
                return Web.Resources.EntityStrings.PartsClaimApplication_SalesDate;
            }
        }

        public string PartsClaimApplication_SalesStatus {
            get {
                return Web.Resources.EntityStrings.PartsClaimApplication_SalesStatus;
            }
        }

        public string PartsClaimApplication_Status {
            get {
                return Web.Resources.EntityStrings.PartsClaimApplication_Status;
            }
        }

        public string PartsClaimApplication_VehicleContactPerson {
            get {
                return Web.Resources.EntityStrings.PartsClaimApplication_VehicleContactPerson;
            }
        }

        public string PartsClaimApplication_VIN {
            get {
                return Web.Resources.EntityStrings.PartsClaimApplication_VIN;
            }
        }

        public string PartsClaimApplication_WarrantyStartTime {
            get {
                return Web.Resources.EntityStrings.PartsClaimApplication_WarrantyStartTime;
            }
        }

        public string PartsClaimOrder_ClaimType {
            get {
                return Web.Resources.EntityStrings.PartsClaimOrder_ClaimType;
            }
        }

        public string PartsClaimOrder_ContactAddress {
            get {
                return Web.Resources.EntityStrings.PartsClaimOrder_ContactAddress;
            }
        }

        public string PartsClaimOrder_ContactPhone {
            get {
                return Web.Resources.EntityStrings.PartsClaimOrder_ContactPhone;
            }
        }

        public string PartsClaimOrder_EngineModel {
            get {
                return Web.Resources.EntityStrings.PartsClaimOrder_EngineModel;
            }
        }

        public string PartsClaimOrder_EngineSerialNumber {
            get {
                return Web.Resources.EntityStrings.PartsClaimOrder_EngineSerialNumber;
            }
        }

        public string PartsClaimOrder_FaultyPartsName {
            get {
                return Web.Resources.EntityStrings.PartsClaimOrder_FaultyPartsName;
            }
        }

        public string PartsClaimOrder_FaultyPartsSupplierName {
            get {
                return Web.Resources.EntityStrings.PartsClaimOrder_FaultyPartsSupplierName;
            }
        }

        public string PartsClaimOrder_FinishingTime {
            get {
                return Web.Resources.EntityStrings.PartsClaimOrder_FinishingTime;
            }
        }

        public string PartsClaimOrder_MalfunctionCode {
            get {
                return Web.Resources.EntityStrings.PartsClaimOrder_MalfunctionCode;
            }
        }

        public string PartsClaimOrder_MalfunctionReason {
            get {
                return Web.Resources.EntityStrings.PartsClaimOrder_MalfunctionReason;
            }
        }

        public string PartsClaimOrder_Mileage {
            get {
                return Web.Resources.EntityStrings.PartsClaimOrder_Mileage;
            }
        }

        public string PartsClaimOrder_OtherCostReason {
            get {
                return Web.Resources.EntityStrings.PartsClaimOrder_OtherCostReason;
            }
        }

        public string PartsClaimOrder_OutOfFactoryDate {
            get {
                return Web.Resources.EntityStrings.PartsClaimOrder_OutOfFactoryDate;
            }
        }

        public string PartsClaimOrder_PartsClaimApplicationCode {
            get {
                return Web.Resources.EntityStrings.PartsClaimOrder_PartsClaimApplicationCode;
            }
        }

        public string PartsClaimOrder_RelatedBusinessBillCode {
            get {
                return Web.Resources.EntityStrings.PartsClaimOrder_RelatedBusinessBillCode;
            }
        }

        public string PartsClaimOrder_RepairContractCode {
            get {
                return Web.Resources.EntityStrings.PartsClaimOrder_RepairContractCode;
            }
        }

        public string PartsClaimOrder_RepairRequestTime {
            get {
                return Web.Resources.EntityStrings.PartsClaimOrder_RepairRequestTime;
            }
        }

        public string PartsClaimOrder_SalesDate {
            get {
                return Web.Resources.EntityStrings.PartsClaimOrder_SalesDate;
            }
        }

        public string PartsClaimOrder_SalesStatus {
            get {
                return Web.Resources.EntityStrings.PartsClaimOrder_SalesStatus;
            }
        }

        public string PartsClaimOrder_VehicleContactPerson {
            get {
                return Web.Resources.EntityStrings.PartsClaimOrder_VehicleContactPerson;
            }
        }

        public string PartsClaimOrder_WarrantyStartTime {
            get {
                return Web.Resources.EntityStrings.PartsClaimOrder_WarrantyStartTime;
            }
        }

        public string RepairClaimApplication_BonusExtensionDescription {
            get {
                return Web.Resources.EntityStrings.RepairClaimApplication_BonusExtensionDescription;
            }
        }

        public string RepairClaimMaterialDetail_NewPartsBatchNumber {
            get {
                return Web.Resources.EntityStrings.RepairClaimMaterialDetail_NewPartsBatchNumber;
            }
        }

        public string RepairClaimMaterialDetail_NewPartsCode {
            get {
                return Web.Resources.EntityStrings.RepairClaimMaterialDetail_NewPartsCode;
            }
        }

        public string RepairClaimMaterialDetail_NewPartsName {
            get {
                return Web.Resources.EntityStrings.RepairClaimMaterialDetail_NewPartsName;
            }
        }

        public string RepairClaimMaterialDetail_NewPartsSerialNumber {
            get {
                return Web.Resources.EntityStrings.RepairClaimMaterialDetail_NewPartsSerialNumber;
            }
        }

        public string RepairClaimMaterialDetail_UnitPrice {
            get {
                return Web.Resources.EntityStrings.RepairClaimMaterialDetail_UnitPrice;
            }
        }

        public string RepairClaimMaterialDetail_UsedPartsCode {
            get {
                return Web.Resources.EntityStrings.RepairClaimMaterialDetail_UsedPartsCode;
            }
        }

        public string RepairClaimMaterialDetail_UsedPartsName {
            get {
                return Web.Resources.EntityStrings.RepairClaimMaterialDetail_UsedPartsName;
            }
        }

        public string PartsClaimPrice_Feature {
            get {
                return Web.Resources.EntityStrings.PartsClaimPrice_Feature;
            }
        }

        public string ExpenseAdjustmentBill_Code {
            get {
                return Web.Resources.EntityStrings.ExpenseAdjustmentBill_Code;
            }
        }

        public string ExpenseAdjustmentBill_DealerCode {
            get {
                return Web.Resources.EntityStrings.ExpenseAdjustmentBill_DealerCode;
            }
        }

        public string ExpenseAdjustmentBill_DealerName {
            get {
                return Web.Resources.EntityStrings.ExpenseAdjustmentBill_DealerName;
            }
        }

        public string ExpenseAdjustmentBill_SettlementStatus {
            get {
                return Web.Resources.EntityStrings.ExpenseAdjustmentBill_SettlementStatus;
            }
        }

        public string ExpenseAdjustmentBill_SourceCode {
            get {
                return Web.Resources.EntityStrings.ExpenseAdjustmentBill_SourceCode;
            }
        }

        public string ExpenseAdjustmentBill_SourceType {
            get {
                return Web.Resources.EntityStrings.ExpenseAdjustmentBill_SourceType;
            }
        }

        public string ExpenseAdjustmentBill_TransactionCategory {
            get {
                return Web.Resources.EntityStrings.ExpenseAdjustmentBill_TransactionCategory;
            }
        }

        public string ExpenseAdjustmentBill_DealerContactPerson {
            get {
                return Web.Resources.EntityStrings.ExpenseAdjustmentBill_DealerContactPerson;
            }
        }

        public string ExpenseAdjustmentBill_DealerPhoneNumber {
            get {
                return Web.Resources.EntityStrings.ExpenseAdjustmentBill_DealerPhoneNumber;
            }
        }

        public string ExpenseAdjustmentBill_DebitOrReplenish {
            get {
                return Web.Resources.EntityStrings.ExpenseAdjustmentBill_DebitOrReplenish;
            }
        }

        public string ExpenseAdjustmentBill_TransactionAmount {
            get {
                return Web.Resources.EntityStrings.ExpenseAdjustmentBill_TransactionAmount;
            }
        }

        public string ExpenseAdjustmentBill_TransactionReason {
            get {
                return Web.Resources.EntityStrings.ExpenseAdjustmentBill_TransactionReason;
            }
        }

        public string PartsClaimPrice_MeasureUnit {
            get {
                return Web.Resources.EntityStrings.PartsClaimPrice_MeasureUnit;
            }
        }

        public string PartsClaimPrice_PartsSupplierCode {
            get {
                return Web.Resources.EntityStrings.PartsClaimPrice_PartsSupplierCode;
            }
        }

        public string PartsClaimPrice_PartsSupplierName {
            get {
                return Web.Resources.EntityStrings.PartsClaimPrice_PartsSupplierName;
            }
        }

        public string PartsClaimPrice_PartsWarrantyCategoryName {
            get {
                return Web.Resources.EntityStrings.PartsClaimPrice_PartsWarrantyCategoryName;
            }
        }

        public string PartsClaimPrice_SalesPrice {
            get {
                return Web.Resources.EntityStrings.PartsClaimPrice_SalesPrice;
            }
        }

        public string PartsClaimPrice_Specification {
            get {
                return Web.Resources.EntityStrings.PartsClaimPrice_Specification;
            }
        }

        public string PartsClaimPrice_UsedPartsCode {
            get {
                return Web.Resources.EntityStrings.PartsClaimPrice_UsedPartsCode;
            }
        }

        public string PartsClaimPrice_UsedPartsName {
            get {
                return Web.Resources.EntityStrings.PartsClaimPrice_UsedPartsName;
            }
        }

        public string RepairClaimApplication_DealerCode {
            get {
                return Web.Resources.EntityStrings.RepairClaimApplication_DealerCode;
            }
        }

        public string RepairClaimApplication_DealerName {
            get {
                return Web.Resources.EntityStrings.RepairClaimApplication_DealerName;
            }
        }

        public string RepairClaimMaterialDetail_PartsManagementCost {
            get {
                return Web.Resources.EntityStrings.RepairClaimMaterialDetail_PartsManagementCost;
            }
        }

        public string PartsClaimOrder_SupplierConfirmComment {
            get {
                return Web.Resources.EntityStrings.PartsClaimOrder_SupplierConfirmComment;
            }
        }

        public string PartsClaimOrder_SupplierConfirmorName {
            get {
                return Web.Resources.EntityStrings.PartsClaimOrder_SupplierConfirmorName;
            }
        }

        public string PartsClaimOrder_SupplierConfirmStatus {
            get {
                return Web.Resources.EntityStrings.PartsClaimOrder_SupplierConfirmStatus;
            }
        }

        public string PartsClaimOrder_SupplierConfirmTime {
            get {
                return Web.Resources.EntityStrings.PartsClaimOrder_SupplierConfirmTime;
            }
        }

        public string PartsClaimOrder_ServiceDepartmentComment {
            get {
                return Web.Resources.EntityStrings.PartsClaimOrder_ServiceDepartmentComment;
            }
        }

        public string PartsClaimOrder_SettlementStatus {
            get {
                return Web.Resources.EntityStrings.PartsClaimOrder_SettlementStatus;
            }
        }

        public string PartsClaimOrder_UsedPartsDisposalStatus {
            get {
                return Web.Resources.EntityStrings.PartsClaimOrder_UsedPartsDisposalStatus;
            }
        }

        public string ServiceTripClaimApplication_ClaimStatus {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimApplication_ClaimStatus;
            }
        }

        public string ServiceTripClaimApplication_ContactAddress {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimApplication_ContactAddress;
            }
        }

        public string ServiceTripClaimApplication_ContactPhone {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimApplication_ContactPhone;
            }
        }

        public string ServiceTripClaimApplication_EstimatedFinishingTime {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimApplication_EstimatedFinishingTime;
            }
        }

        public string ServiceTripClaimApplication_Motive {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimApplication_Motive;
            }
        }

        public string ServiceTripClaimApplication_OtherCostReason {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimApplication_OtherCostReason;
            }
        }

        public string ServiceTripClaimApplication_OutOfFactoryDate {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimApplication_OutOfFactoryDate;
            }
        }

        public string ServiceTripClaimApplication_RepairRequestOrderCode {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimApplication_RepairRequestOrderCode;
            }
        }

        public string ServiceTripClaimApplication_SalesDate {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimApplication_SalesDate;
            }
        }

        public string ServiceTripClaimApplication_ServiceTripDistance {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimApplication_ServiceTripDistance;
            }
        }

        public string ServiceTripClaimApplication_ServiceTripDuration {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimApplication_ServiceTripDuration;
            }
        }

        public string ServiceTripClaimApplication_ServiceTripPerson {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimApplication_ServiceTripPerson;
            }
        }

        public string ServiceTripClaimApplication_VehicleContactPerson {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimApplication_VehicleContactPerson;
            }
        }

        public string PartsClaimRepairItemDetail_ApproveStatus {
            get {
                return Web.Resources.EntityStrings.PartsClaimRepairItemDetail_ApproveStatus;
            }
        }

        public string PartsClaimRepairItemDetail_DefaultLaborHour {
            get {
                return Web.Resources.EntityStrings.PartsClaimRepairItemDetail_DefaultLaborHour;
            }
        }

        public string PartsClaimRepairItemDetail_LaborCost {
            get {
                return Web.Resources.EntityStrings.PartsClaimRepairItemDetail_LaborCost;
            }
        }

        public string PartsClaimRepairItemDetail_LaborUnitPrice {
            get {
                return Web.Resources.EntityStrings.PartsClaimRepairItemDetail_LaborUnitPrice;
            }
        }

        public string PartsClaimMaterialDetail_NewPartsSecurityNumber {
            get {
                return Web.Resources.EntityStrings.PartsClaimMaterialDetail_NewPartsSecurityNumber;
            }
        }

        public string PartsClaimMaterialDetail_PartsManagementCost {
            get {
                return Web.Resources.EntityStrings.PartsClaimMaterialDetail_PartsManagementCost;
            }
        }

        public string PartsClaimRepairItemDetail_PartsClaimMaterialDetails {
            get {
                return Web.Resources.EntityStrings.PartsClaimRepairItemDetail_PartsClaimMaterialDetails;
            }
        }

        public string RepairClaimBill_SupplierConfirmComment {
            get {
                return Web.Resources.EntityStrings.RepairClaimBill_SupplierConfirmComment;
            }
        }

        public string RepairClaimBill_SupplierConfirmorName {
            get {
                return Web.Resources.EntityStrings.RepairClaimBill_SupplierConfirmorName;
            }
        }

        public string RepairClaimBill_SupplierConfirmStatus {
            get {
                return Web.Resources.EntityStrings.RepairClaimBill_SupplierConfirmStatus;
            }
        }

        public string RepairClaimBill_SupplierConfirmTime {
            get {
                return Web.Resources.EntityStrings.RepairClaimBill_SupplierConfirmTime;
            }
        }

        public string RepairClaimApplication_ServiceActivityContent {
            get {
                return Web.Resources.EntityStrings.RepairClaimApplication_ServiceActivityContent;
            }
        }

        public string RepairClaimApplication_ServiceActivityType {
            get {
                return Web.Resources.EntityStrings.RepairClaimApplication_ServiceActivityType;
            }
        }

        public string PartsClaimMaterialDetail_UsedPartsBarCode {
            get {
                return Web.Resources.EntityStrings.PartsClaimMaterialDetail_UsedPartsBarCode;
            }
        }

        public string PartsClaimMaterialDetail_UsedPartsDisposalStatus {
            get {
                return Web.Resources.EntityStrings.PartsClaimMaterialDetail_UsedPartsDisposalStatus;
            }
        }

        public string PartsClaimMaterialDetail_UsedPartsReturnPolicy {
            get {
                return Web.Resources.EntityStrings.PartsClaimMaterialDetail_UsedPartsReturnPolicy;
            }
        }

        public string PartsClaimPrice_PartsWarrantyType {
            get {
                return Web.Resources.EntityStrings.PartsClaimPrice_PartsWarrantyType;
            }
        }

        public string PartsClaimPrice_PartType {
            get {
                return Web.Resources.EntityStrings.PartsClaimPrice_PartType;
            }
        }

        public string PartsClaimPrice_UsedPartsReturnPolicy {
            get {
                return Web.Resources.EntityStrings.PartsClaimPrice_UsedPartsReturnPolicy;
            }
        }

        public string RepairClaimMaterialDetail_UsedPartsBarCode {
            get {
                return Web.Resources.EntityStrings.RepairClaimMaterialDetail_UsedPartsBarCode;
            }
        }

        public string Product_Code {
            get {
                return Web.Resources.EntityStrings.Product_Code;
            }
        }

        public string Product_Name {
            get {
                return Web.Resources.EntityStrings.Product_Name;
            }
        }

        public string Product_ParameterDescription {
            get {
                return Web.Resources.EntityStrings.Product_ParameterDescription;
            }
        }

        public string ServiceProductLine_Code {
            get {
                return Web.Resources.EntityStrings.ServiceProductLine_Code;
            }
        }

        public string ServiceTripClaimApplication_DealerCode {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimApplication_DealerCode;
            }
        }

        public string ServiceTripClaimApplication_DealerName {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimApplication_DealerName;
            }
        }

        public string ResponsibleUnit_Abbreviation {
            get {
                return Web.Resources.EntityStrings.ResponsibleUnit_Abbreviation;
            }
        }

        public string ResponsibleUnit_ClaimRange {
            get {
                return Web.Resources.EntityStrings.ResponsibleUnit_ClaimRange;
            }
        }

        public string Branch_MainBrand {
            get {
                return Web.Resources.EntityStrings.Branch_MainBrand;
            }
        }

        public string Company_Code {
            get {
                return Web.Resources.EntityStrings.Company_Code;
            }
        }

        public string Company_Name {
            get {
                return Web.Resources.EntityStrings.Company_Name;
            }
        }

        public string Company_ShortName {
            get {
                return Web.Resources.EntityStrings.Company_ShortName;
            }
        }

        public string Company_ContactAddress {
            get {
                return Web.Resources.EntityStrings.Company_ContactAddress;
            }
        }

        public string Company_ContactMail {
            get {
                return Web.Resources.EntityStrings.Company_ContactMail;
            }
        }

        public string Company_ContactPerson {
            get {
                return Web.Resources.EntityStrings.Company_ContactPerson;
            }
        }

        public string Company_ContactPhone {
            get {
                return Web.Resources.EntityStrings.Company_ContactPhone;
            }
        }

        public string Company_FoundDate {
            get {
                return Web.Resources.EntityStrings.Company_FoundDate;
            }
        }

        public string CompanyInvoiceInfo_BankName {
            get {
                return Web.Resources.EntityStrings.CompanyInvoiceInfo_BankName;
            }
        }

        public string CompanyInvoiceInfo_InvoiceAmountQuota {
            get {
                return Web.Resources.EntityStrings.CompanyInvoiceInfo_InvoiceAmountQuota;
            }
        }

        public string CompanyInvoiceInfo_InvoiceTitle {
            get {
                return Web.Resources.EntityStrings.CompanyInvoiceInfo_InvoiceTitle;
            }
        }

        public string CompanyInvoiceInfo_TaxpayerQualification {
            get {
                return Web.Resources.EntityStrings.CompanyInvoiceInfo_TaxpayerQualification;
            }
        }

        public string CompanyInvoiceInfo_TaxRegisteredNumber {
            get {
                return Web.Resources.EntityStrings.CompanyInvoiceInfo_TaxRegisteredNumber;
            }
        }

        public string Company_BusinessAddress {
            get {
                return Web.Resources.EntityStrings.Company_BusinessAddress;
            }
        }

        public string Company_BusinessScope {
            get {
                return Web.Resources.EntityStrings.Company_BusinessScope;
            }
        }

        public string Company_ContactPostCode {
            get {
                return Web.Resources.EntityStrings.Company_ContactPostCode;
            }
        }

        public string Company_CorporateNature {
            get {
                return Web.Resources.EntityStrings.Company_CorporateNature;
            }
        }

        public string Company_Fax {
            get {
                return Web.Resources.EntityStrings.Company_Fax;
            }
        }

        public string Company_IdDocumentNumber {
            get {
                return Web.Resources.EntityStrings.Company_IdDocumentNumber;
            }
        }

        public string Company_IdDocumentType {
            get {
                return Web.Resources.EntityStrings.Company_IdDocumentType;
            }
        }

        public string Company_LegalRepresentative {
            get {
                return Web.Resources.EntityStrings.Company_LegalRepresentative;
            }
        }

        public string Company_RegisterCapital {
            get {
                return Web.Resources.EntityStrings.Company_RegisterCapital;
            }
        }

        public string Company_RegisterCode {
            get {
                return Web.Resources.EntityStrings.Company_RegisterCode;
            }
        }

        public string Company_RegisterDate {
            get {
                return Web.Resources.EntityStrings.Company_RegisterDate;
            }
        }

        public string Company_RegisterName {
            get {
                return Web.Resources.EntityStrings.Company_RegisterName;
            }
        }

        public string CompanyInvoiceInfo_BankAccount {
            get {
                return Web.Resources.EntityStrings.CompanyInvoiceInfo_BankAccount;
            }
        }

        public string CompanyInvoiceInfo_CompanyCode {
            get {
                return Web.Resources.EntityStrings.CompanyInvoiceInfo_CompanyCode;
            }
        }

        public string CompanyInvoiceInfo_CompanyName {
            get {
                return Web.Resources.EntityStrings.CompanyInvoiceInfo_CompanyName;
            }
        }

        public string Agency_Code {
            get {
                return Web.Resources.EntityStrings.Agency_Code;
            }
        }

        public string Agency_Name {
            get {
                return Web.Resources.EntityStrings.Agency_Name;
            }
        }

        public string Agency_ShortName {
            get {
                return Web.Resources.EntityStrings.Agency_ShortName;
            }
        }

        public string Branch_Abbreviation {
            get {
                return Web.Resources.EntityStrings.Branch_Abbreviation;
            }
        }

        public string VehicleCategory_Code {
            get {
                return Web.Resources.EntityStrings.VehicleCategory_Code;
            }
        }

        public string VehicleCategory_Name {
            get {
                return Web.Resources.EntityStrings.VehicleCategory_Name;
            }
        }

        public string Product_Color {
            get {
                return Web.Resources.EntityStrings.Product_Color;
            }
        }

        public string PartsSalesCategory_IsForClaim {
            get {
                return Web.Resources.EntityStrings.PartsSalesCategory_IsForClaim;
            }
        }

        public string LogisticCompany_Abbreviation {
            get {
                return Web.Resources.EntityStrings.LogisticCompany_Abbreviation;
            }
        }

        public string LogisticCompany_LogisticCompanyServiceRanges {
            get {
                return Web.Resources.EntityStrings.LogisticCompany_LogisticCompanyServiceRanges;
            }
        }

        public string VirtualPartsStock_BatchNumber {
            get {
                return Web.Resources.EntityStrings.VirtualPartsStock_BatchNumber;
            }
        }

        public string VirtualPartsStock_SparePartCode {
            get {
                return Web.Resources.EntityStrings.VirtualPartsStock_SparePartCode;
            }
        }

        public string VirtualPartsStock_SparePartName {
            get {
                return Web.Resources.EntityStrings.VirtualPartsStock_SparePartName;
            }
        }

        public string VirtualPartsStock_WarehouseAreaCode {
            get {
                return Web.Resources.EntityStrings.VirtualPartsStock_WarehouseAreaCode;
            }
        }

        public string OverstockPartsAdjustDetail_InboundAmount {
            get {
                return Web.Resources.EntityStrings.OverstockPartsAdjustDetail_InboundAmount;
            }
        }

        public string OverstockPartsAdjustDetail_OutboundAmount {
            get {
                return Web.Resources.EntityStrings.OverstockPartsAdjustDetail_OutboundAmount;
            }
        }

        public string OverstockPartsAdjustDetail_ShippingAmount {
            get {
                return Web.Resources.EntityStrings.OverstockPartsAdjustDetail_ShippingAmount;
            }
        }

        public string PartsOutboundPlanDetail_PlannedAmount {
            get {
                return Web.Resources.EntityStrings.PartsOutboundPlanDetail_PlannedAmount;
            }
        }

        public string PartsOutboundPlanDetail_Price {
            get {
                return Web.Resources.EntityStrings.PartsOutboundPlanDetail_Price;
            }
        }

        public string PartsOutboundPlanDetail_SparePartCode {
            get {
                return Web.Resources.EntityStrings.PartsOutboundPlanDetail_SparePartCode;
            }
        }

        public string PartsOutboundPlanDetail_SparePartName {
            get {
                return Web.Resources.EntityStrings.PartsOutboundPlanDetail_SparePartName;
            }
        }

        public string PartsOutboundPlan_CounterpartCompanyCode {
            get {
                return Web.Resources.EntityStrings.PartsOutboundPlan_CounterpartCompanyCode;
            }
        }

        public string PartsOutboundPlan_CounterpartCompanyName {
            get {
                return Web.Resources.EntityStrings.PartsOutboundPlan_CounterpartCompanyName;
            }
        }

        public string PartsOutboundPlan_OutboundType {
            get {
                return Web.Resources.EntityStrings.PartsOutboundPlan_OutboundType;
            }
        }

        public string PartsOutboundPlan_SourceCode {
            get {
                return Web.Resources.EntityStrings.PartsOutboundPlan_SourceCode;
            }
        }

        public string OverstockPartsAdjustDetail_ShelvesAmount {
            get {
                return Web.Resources.EntityStrings.OverstockPartsAdjustDetail_ShelvesAmount;
            }
        }

        public string PartsShiftOrderDetail_BatchNumber {
            get {
                return Web.Resources.EntityStrings.PartsShiftOrderDetail_BatchNumber;
            }
        }

        public string PartsShiftOrderDetail_Remark {
            get {
                return Web.Resources.EntityStrings.PartsShiftOrderDetail_Remark;
            }
        }

        public string PartsShiftOrderDetail_SparePartCode {
            get {
                return Web.Resources.EntityStrings.PartsShiftOrderDetail_SparePartCode;
            }
        }

        public string PartsShiftOrderDetail_SparePartName {
            get {
                return Web.Resources.EntityStrings.PartsShiftOrderDetail_SparePartName;
            }
        }

        public string PartsInboundCheckBillDetail_BatchInformation {
            get {
                return Web.Resources.EntityStrings.PartsInboundCheckBillDetail_BatchInformation;
            }
        }

        public string PartsInboundCheckBillDetail_PlannedAmount {
            get {
                return Web.Resources.EntityStrings.PartsInboundCheckBillDetail_PlannedAmount;
            }
        }

        public string PartsInboundPlan_CounterpartCompanyCode {
            get {
                return Web.Resources.EntityStrings.PartsInboundPlan_CounterpartCompanyCode;
            }
        }

        public string PartsInboundPlan_CounterpartCompanyName {
            get {
                return Web.Resources.EntityStrings.PartsInboundPlan_CounterpartCompanyName;
            }
        }

        public string PartsInboundPlan_InboundType {
            get {
                return Web.Resources.EntityStrings.PartsInboundPlan_InboundType;
            }
        }

        public string PartsInboundPlan_SourceCode {
            get {
                return Web.Resources.EntityStrings.PartsInboundPlan_SourceCode;
            }
        }

        public string PartsInboundPlanDetail_InspectedQuantity {
            get {
                return Web.Resources.EntityStrings.PartsInboundPlanDetail_InspectedQuantity;
            }
        }

        public string PartsInboundPlanDetail_PlannedAmount {
            get {
                return Web.Resources.EntityStrings.PartsInboundPlanDetail_PlannedAmount;
            }
        }

        public string PartsInboundPlanDetail_Price {
            get {
                return Web.Resources.EntityStrings.PartsInboundPlanDetail_Price;
            }
        }

        public string PartsInboundPlanDetail_SparePartCode {
            get {
                return Web.Resources.EntityStrings.PartsInboundPlanDetail_SparePartCode;
            }
        }

        public string PartsInboundPlanDetail_SparePartName {
            get {
                return Web.Resources.EntityStrings.PartsInboundPlanDetail_SparePartName;
            }
        }

        public string PartsInboundPlan_PartsInboundPlanDetails {
            get {
                return Web.Resources.EntityStrings.PartsInboundPlan_PartsInboundPlanDetails;
            }
        }

        public string PartsOutboundBillDetail_SettlementPrice {
            get {
                return Web.Resources.EntityStrings.PartsOutboundBillDetail_SettlementPrice;
            }
        }

        public string PartsOutboundBillDetail_OutboundAmount {
            get {
                return Web.Resources.EntityStrings.PartsOutboundBillDetail_OutboundAmount;
            }
        }

        public string PartsOutboundBillDetail_SparePartCode {
            get {
                return Web.Resources.EntityStrings.PartsOutboundBillDetail_SparePartCode;
            }
        }

        public string PartsOutboundBillDetail_SparePartName {
            get {
                return Web.Resources.EntityStrings.PartsOutboundBillDetail_SparePartName;
            }
        }

        public string PartsOutboundBillDetail_WarehouseAreaCode {
            get {
                return Web.Resources.EntityStrings.PartsOutboundBillDetail_WarehouseAreaCode;
            }
        }

        public string PartsOutboundBill_CounterpartCompanyCode {
            get {
                return Web.Resources.EntityStrings.PartsOutboundBill_CounterpartCompanyCode;
            }
        }

        public string PartsOutboundBill_CounterpartCompanyName {
            get {
                return Web.Resources.EntityStrings.PartsOutboundBill_CounterpartCompanyName;
            }
        }

        public string PartsOutboundBill_OutboundType {
            get {
                return Web.Resources.EntityStrings.PartsOutboundBill_OutboundType;
            }
        }

        public string PartsOutboundBill_SettlementStatus {
            get {
                return Web.Resources.EntityStrings.PartsOutboundBill_SettlementStatus;
            }
        }

        public string PartsOutboundBillDetail_BatchNumber {
            get {
                return Web.Resources.EntityStrings.PartsOutboundBillDetail_BatchNumber;
            }
        }

        public string PartsOutboundBill_PartsOutboundBillDetails {
            get {
                return Web.Resources.EntityStrings.PartsOutboundBill_PartsOutboundBillDetails;
            }
        }

        public string VirtualPartsStock_InboundTime {
            get {
                return Web.Resources.EntityStrings.VirtualPartsStock_InboundTime;
            }
        }

        public string VirtualPartsStock_UsableQuantity {
            get {
                return Web.Resources.EntityStrings.VirtualPartsStock_UsableQuantity;
            }
        }

        public string VirtualPartsStock_WarehouseAreaCategory {
            get {
                return Web.Resources.EntityStrings.VirtualPartsStock_WarehouseAreaCategory;
            }
        }

        public string PartsOutboundPlanDetail_CurrentOutboundAmount {
            get {
                return Web.Resources.EntityStrings.PartsOutboundPlanDetail_CurrentOutboundAmount;
            }
        }

        public string OverstockPartsStock_CityName {
            get {
                return Web.Resources.EntityStrings.OverstockPartsStock_CityName;
            }
        }

        public string OverstockPartsStock_ProvinceName {
            get {
                return Web.Resources.EntityStrings.OverstockPartsStock_ProvinceName;
            }
        }

        public string OverstockPartsStock_SparePartCode {
            get {
                return Web.Resources.EntityStrings.OverstockPartsStock_SparePartCode;
            }
        }

        public string OverstockPartsStock_SparePartName {
            get {
                return Web.Resources.EntityStrings.OverstockPartsStock_SparePartName;
            }
        }

        public string OverstockPartsStock_StorageCompanyCode {
            get {
                return Web.Resources.EntityStrings.OverstockPartsStock_StorageCompanyCode;
            }
        }

        public string OverstockPartsStock_StorageCompanyName {
            get {
                return Web.Resources.EntityStrings.OverstockPartsStock_StorageCompanyName;
            }
        }

        public string OverstockPartsStock_StorageCompanyType {
            get {
                return Web.Resources.EntityStrings.OverstockPartsStock_StorageCompanyType;
            }
        }

        public string OverstockPartsStock_UsableQuantity {
            get {
                return Web.Resources.EntityStrings.OverstockPartsStock_UsableQuantity;
            }
        }

        public string OverstockPartsAdjustBill_AccomplishTime {
            get {
                return Web.Resources.EntityStrings.OverstockPartsAdjustBill_AccomplishTime;
            }
        }

        public string OverstockPartsAdjustDetail_Quantity {
            get {
                return Web.Resources.EntityStrings.OverstockPartsAdjustDetail_Quantity;
            }
        }

        public string OverstockPartsAdjustDetail_SparePartCode {
            get {
                return Web.Resources.EntityStrings.OverstockPartsAdjustDetail_SparePartCode;
            }
        }

        public string OverstockPartsAdjustDetail_SparePartName {
            get {
                return Web.Resources.EntityStrings.OverstockPartsAdjustDetail_SparePartName;
            }
        }

        public string OverstockPartsAdjustDetail_TransferInPrice {
            get {
                return Web.Resources.EntityStrings.OverstockPartsAdjustDetail_TransferInPrice;
            }
        }

        public string OverstockPartsAdjustDetail_TransferOutPrice {
            get {
                return Web.Resources.EntityStrings.OverstockPartsAdjustDetail_TransferOutPrice;
            }
        }

        public string OverstockPartsAdjustBill_AccomplisherName {
            get {
                return Web.Resources.EntityStrings.OverstockPartsAdjustBill_AccomplisherName;
            }
        }

        public string OverstockPartsAdjustBill_BranchName {
            get {
                return Web.Resources.EntityStrings.OverstockPartsAdjustBill_BranchName;
            }
        }

        public string OverstockPartsAdjustBill_Code {
            get {
                return Web.Resources.EntityStrings.OverstockPartsAdjustBill_Code;
            }
        }

        public string OverstockPartsAdjustBill_SalesUnitName {
            get {
                return Web.Resources.EntityStrings.OverstockPartsAdjustBill_SalesUnitName;
            }
        }

        public string OverstockPartsAdjustBill_TransferReason {
            get {
                return Web.Resources.EntityStrings.OverstockPartsAdjustBill_TransferReason;
            }
        }

        public string PartsInboundCheckBillDetail_SettlementPrice {
            get {
                return Web.Resources.EntityStrings.PartsInboundCheckBillDetail_SettlementPrice;
            }
        }

        public string PartsInboundCheckBillDetail_InspectedQuantity {
            get {
                return Web.Resources.EntityStrings.PartsInboundCheckBillDetail_InspectedQuantity;
            }
        }

        public string PartsInboundCheckBillDetail_SparePartCode {
            get {
                return Web.Resources.EntityStrings.PartsInboundCheckBillDetail_SparePartCode;
            }
        }

        public string PartsInboundCheckBillDetail_SparePartName {
            get {
                return Web.Resources.EntityStrings.PartsInboundCheckBillDetail_SparePartName;
            }
        }

        public string PartsInboundCheckBillDetail_WarehouseAreaCode {
            get {
                return Web.Resources.EntityStrings.PartsInboundCheckBillDetail_WarehouseAreaCode;
            }
        }

        public string PartsInboundCheckBill_CounterpartCompanyCode {
            get {
                return Web.Resources.EntityStrings.PartsInboundCheckBill_CounterpartCompanyCode;
            }
        }

        public string PartsInboundCheckBill_CounterpartCompanyName {
            get {
                return Web.Resources.EntityStrings.PartsInboundCheckBill_CounterpartCompanyName;
            }
        }

        public string PartsInboundCheckBill_InboundType {
            get {
                return Web.Resources.EntityStrings.PartsInboundCheckBill_InboundType;
            }
        }

        public string PartsInboundCheckBill_SettlementStatus {
            get {
                return Web.Resources.EntityStrings.PartsInboundCheckBill_SettlementStatus;
            }
        }

        public string PartsInboundPackingDetail_BatchNumber {
            get {
                return Web.Resources.EntityStrings.PartsInboundPackingDetail_BatchNumber;
            }
        }

        public string PartsInboundPackingDetail_PackingMaterialQuantity {
            get {
                return Web.Resources.EntityStrings.PartsInboundPackingDetail_PackingMaterialQuantity;
            }
        }

        public string PartsInboundPackingDetail_PackingSpecification {
            get {
                return Web.Resources.EntityStrings.PartsInboundPackingDetail_PackingSpecification;
            }
        }

        public string PartsInboundPackingDetail_SparePartCode {
            get {
                return Web.Resources.EntityStrings.PartsInboundPackingDetail_SparePartCode;
            }
        }

        public string PartsInboundPackingDetail_SparePartName {
            get {
                return Web.Resources.EntityStrings.PartsInboundPackingDetail_SparePartName;
            }
        }

        public string PartsInboundPackingDetail_WarehouseAreaCode {
            get {
                return Web.Resources.EntityStrings.PartsInboundPackingDetail_WarehouseAreaCode;
            }
        }

        public string OverstockPartsAppDetail_SparePartCode {
            get {
                return Web.Resources.EntityStrings.OverstockPartsAppDetail_SparePartCode;
            }
        }

        public string OverstockPartsAppDetail_SparePartName {
            get {
                return Web.Resources.EntityStrings.OverstockPartsAppDetail_SparePartName;
            }
        }

        public string OverstockPartsApp_Code {
            get {
                return Web.Resources.EntityStrings.OverstockPartsApp_Code;
            }
        }

        public string OverstockPartsApp_OverstockPartsAppDetails {
            get {
                return Web.Resources.EntityStrings.OverstockPartsApp_OverstockPartsAppDetails;
            }
        }

        public string PartsInboundCheckBill_PartsInboundCheckBillDetails {
            get {
                return Web.Resources.EntityStrings.PartsInboundCheckBill_PartsInboundCheckBillDetails;
            }
        }

        public string PartsInboundCheckBill_PartsInboundPackingDetails {
            get {
                return Web.Resources.EntityStrings.PartsInboundCheckBill_PartsInboundPackingDetails;
            }
        }

        public string PartsInboundPackingDetail_Quantity {
            get {
                return Web.Resources.EntityStrings.PartsInboundPackingDetail_Quantity;
            }
        }

        public string PartsShiftOrderDetail_DestWarehouseAreaCategory {
            get {
                return Web.Resources.EntityStrings.PartsShiftOrderDetail_DestWarehouseAreaCategory;
            }
        }

        public string PartsShiftOrderDetail_OriginalWarehouseAreaCategory {
            get {
                return Web.Resources.EntityStrings.PartsShiftOrderDetail_OriginalWarehouseAreaCategory;
            }
        }

        public string PartsShiftOrder_Type {
            get {
                return Web.Resources.EntityStrings.PartsShiftOrder_Type;
            }
        }

        public string OverstockPartsAdjustDetail_SourceStockAmount {
            get {
                return Web.Resources.EntityStrings.OverstockPartsAdjustDetail_SourceStockAmount;
            }
        }

        public string WarehouseAreaWithPartsStock_AreaKind {
            get {
                return Web.Resources.EntityStrings.WarehouseAreaWithPartsStock_AreaKind;
            }
        }

        public string WarehouseAreaWithPartsStock_SparePartCode {
            get {
                return Web.Resources.EntityStrings.WarehouseAreaWithPartsStock_SparePartCode;
            }
        }

        public string WarehouseAreaWithPartsStock_SparePartName {
            get {
                return Web.Resources.EntityStrings.WarehouseAreaWithPartsStock_SparePartName;
            }
        }

        public string WarehouseAreaWithPartsStock_WarehouseAreaCode {
            get {
                return Web.Resources.EntityStrings.WarehouseAreaWithPartsStock_WarehouseAreaCode;
            }
        }

        public string PartsOutboundPlan_PartsOutboundPlanDetails {
            get {
                return Web.Resources.EntityStrings.PartsOutboundPlan_PartsOutboundPlanDetails;
            }
        }

        public string PartsShiftOrderDetail_DestWarehouseAreaCode {
            get {
                return Web.Resources.EntityStrings.PartsShiftOrderDetail_DestWarehouseAreaCode;
            }
        }

        public string PartsShiftOrderDetail_Quantity {
            get {
                return Web.Resources.EntityStrings.PartsShiftOrderDetail_Quantity;
            }
        }

        public string Dealer_DealerServiceInfoes {
            get {
                return Web.Resources.EntityStrings.Dealer_DealerServiceInfoes;
            }
        }

        public string PartsPacking_SparePartCode {
            get {
                return Web.Resources.EntityStrings.PartsPacking_SparePartCode;
            }
        }

        public string PartsPacking_SparePartName {
            get {
                return Web.Resources.EntityStrings.PartsPacking_SparePartName;
            }
        }

        public string PartsPacking_WarehouseAreaCode {
            get {
                return Web.Resources.EntityStrings.PartsPacking_WarehouseAreaCode;
            }
        }

        public string PartsStockBatchDetail_BatchNumber {
            get {
                return Web.Resources.EntityStrings.PartsStockBatchDetail_BatchNumber;
            }
        }

        public string PartsStockBatchDetail_Quantity {
            get {
                return Web.Resources.EntityStrings.PartsStockBatchDetail_Quantity;
            }
        }

        public string VirtualPartsStock_WarehouseCode {
            get {
                return Web.Resources.EntityStrings.VirtualPartsStock_WarehouseCode;
            }
        }

        public string VirtualPartsStock_WarehouseName {
            get {
                return Web.Resources.EntityStrings.VirtualPartsStock_WarehouseName;
            }
        }

        public string PartsSalesOrder_RequestedDeliveryTime {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrder_RequestedDeliveryTime;
            }
        }

        public string PartsSalesOrder_RequestedShippingTime {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrder_RequestedShippingTime;
            }
        }

        public string PartsSalesOrder_SecondLevelOrderType {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrder_SecondLevelOrderType;
            }
        }

        public string PartsSalesOrder_TotalAmount {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrder_TotalAmount;
            }
        }

        public string PartsSalesOrder_ContactPerson {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrder_ContactPerson;
            }
        }

        public string PartsSalesOrder_ContactPhone {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrder_ContactPhone;
            }
        }

        public string PartsSalesOrder_ReceivingAddress {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrder_ReceivingAddress;
            }
        }

        public string PartsSalesOrderProcess_ApprovalComment {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrderProcess_ApprovalComment;
            }
        }

        public string PartsSalesOrderProcess_ShippingInformationRemark {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrderProcess_ShippingInformationRemark;
            }
        }

        public string PartsSalesOrderProcess_ShippingMethod {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrderProcess_ShippingMethod;
            }
        }

        public string PartsPurReturnOrderDetail_MeasureUnit {
            get {
                return Web.Resources.EntityStrings.PartsPurReturnOrderDetail_MeasureUnit;
            }
        }

        public string PartsPurReturnOrderDetail_Quantity {
            get {
                return Web.Resources.EntityStrings.PartsPurReturnOrderDetail_Quantity;
            }
        }

        public string PartsPurReturnOrderDetail_SparePartCode {
            get {
                return Web.Resources.EntityStrings.PartsPurReturnOrderDetail_SparePartCode;
            }
        }

        public string PartsPurReturnOrderDetail_SparePartName {
            get {
                return Web.Resources.EntityStrings.PartsPurReturnOrderDetail_SparePartName;
            }
        }

        public string PartsPurReturnOrderDetail_UnitPrice {
            get {
                return Web.Resources.EntityStrings.PartsPurReturnOrderDetail_UnitPrice;
            }
        }

        public string PartsPurReturnOrder_BranchName {
            get {
                return Web.Resources.EntityStrings.PartsPurReturnOrder_BranchName;
            }
        }

        public string PartsPurReturnOrder_Code {
            get {
                return Web.Resources.EntityStrings.PartsPurReturnOrder_Code;
            }
        }

        public string PartsPurReturnOrder_InvoiceRequirement {
            get {
                return Web.Resources.EntityStrings.PartsPurReturnOrder_InvoiceRequirement;
            }
        }

        public string PartsPurReturnOrder_PartsPurchaseOrderCode {
            get {
                return Web.Resources.EntityStrings.PartsPurReturnOrder_PartsPurchaseOrderCode;
            }
        }

        public string PartsPurReturnOrder_PartsSupplierCode {
            get {
                return Web.Resources.EntityStrings.PartsPurReturnOrder_PartsSupplierCode;
            }
        }

        public string PartsPurReturnOrder_PartsSupplierName {
            get {
                return Web.Resources.EntityStrings.PartsPurReturnOrder_PartsSupplierName;
            }
        }

        public string PartsPurReturnOrder_ReturnReason {
            get {
                return Web.Resources.EntityStrings.PartsPurReturnOrder_ReturnReason;
            }
        }

        public string PartsPurReturnOrder_TotalAmount {
            get {
                return Web.Resources.EntityStrings.PartsPurReturnOrder_TotalAmount;
            }
        }

        public string PartsPurReturnOrder_WarehouseAddress {
            get {
                return Web.Resources.EntityStrings.PartsPurReturnOrder_WarehouseAddress;
            }
        }

        public string PartsPurReturnOrder_WarehouseName {
            get {
                return Web.Resources.EntityStrings.PartsPurReturnOrder_WarehouseName;
            }
        }

        public string DepartmentInformation_BranchName {
            get {
                return Web.Resources.EntityStrings.DepartmentInformation_BranchName;
            }
        }

        public string DepartmentInformation_Code {
            get {
                return Web.Resources.EntityStrings.DepartmentInformation_Code;
            }
        }

        public string DepartmentInformation_ContactPhone {
            get {
                return Web.Resources.EntityStrings.DepartmentInformation_ContactPhone;
            }
        }

        public string DepartmentInformation_Name {
            get {
                return Web.Resources.EntityStrings.DepartmentInformation_Name;
            }
        }

        public string DepartmentInformation_Responsible {
            get {
                return Web.Resources.EntityStrings.DepartmentInformation_Responsible;
            }
        }

        public string InternalAllocationBill_Operator {
            get {
                return Web.Resources.EntityStrings.InternalAllocationBill_Operator;
            }
        }

        public string InternalAllocationBill_Reason {
            get {
                return Web.Resources.EntityStrings.InternalAllocationBill_Reason;
            }
        }

        public string InternalAllocationBill_TotalAmount {
            get {
                return Web.Resources.EntityStrings.InternalAllocationBill_TotalAmount;
            }
        }

        public string InternalAllocationBill_DepartmentName {
            get {
                return Web.Resources.EntityStrings.InternalAllocationBill_DepartmentName;
            }
        }

        public string PartsSalesOrderProcessDetail_CurrentFulfilledQuantity {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrderProcessDetail_CurrentFulfilledQuantity;
            }
        }

        public string PartsSalesOrderProcessDetail_NewPartCode {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrderProcessDetail_NewPartCode;
            }
        }

        public string PartsSalesOrderProcessDetail_NewPartName {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrderProcessDetail_NewPartName;
            }
        }

        public string PartsSalesOrderProcessDetail_OrderedQuantity {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrderProcessDetail_OrderedQuantity;
            }
        }

        public string PartsSalesOrderProcessDetail_OrderProcessMethod {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrderProcessDetail_OrderProcessMethod;
            }
        }

        public string PartsSalesOrderProcessDetail_OrderProcessStatus {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrderProcessDetail_OrderProcessStatus;
            }
        }

        public string PartsSalesOrderProcessDetail_SparePartCode {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrderProcessDetail_SparePartCode;
            }
        }

        public string PartsSalesOrderProcessDetail_SparePartName {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrderProcessDetail_SparePartName;
            }
        }

        public string PartsSalesOrderProcessDetail_UnfulfilledQuantity {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrderProcessDetail_UnfulfilledQuantity;
            }
        }

        public string PartsSalesOrderProcessDetail_OrderPrice {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrderProcessDetail_OrderPrice;
            }
        }

        public string PartsSalesReturnBillDetail_ApproveQuantity {
            get {
                return Web.Resources.EntityStrings.PartsSalesReturnBillDetail_ApproveQuantity;
            }
        }

        public string PartsSalesReturnBillDetail_BatchNumber {
            get {
                return Web.Resources.EntityStrings.PartsSalesReturnBillDetail_BatchNumber;
            }
        }

        public string PartsSalesReturnBillDetail_MeasureUnit {
            get {
                return Web.Resources.EntityStrings.PartsSalesReturnBillDetail_MeasureUnit;
            }
        }

        public string PartsSalesReturnBillDetail_OriginalOrderPrice {
            get {
                return Web.Resources.EntityStrings.PartsSalesReturnBillDetail_OriginalOrderPrice;
            }
        }

        public string PartsSalesReturnBillDetail_ReturnedQuantity {
            get {
                return Web.Resources.EntityStrings.PartsSalesReturnBillDetail_ReturnedQuantity;
            }
        }

        public string PartsSalesReturnBillDetail_ReturnPrice {
            get {
                return Web.Resources.EntityStrings.PartsSalesReturnBillDetail_ReturnPrice;
            }
        }

        public string PartsSalesReturnBillDetail_SparePartCode {
            get {
                return Web.Resources.EntityStrings.PartsSalesReturnBillDetail_SparePartCode;
            }
        }

        public string PartsSalesReturnBillDetail_SparePartName {
            get {
                return Web.Resources.EntityStrings.PartsSalesReturnBillDetail_SparePartName;
            }
        }

        public string PartsSalesReturnBill_BlueInvoiceNumber {
            get {
                return Web.Resources.EntityStrings.PartsSalesReturnBill_BlueInvoiceNumber;
            }
        }

        public string PartsSalesReturnBill_Code {
            get {
                return Web.Resources.EntityStrings.PartsSalesReturnBill_Code;
            }
        }

        public string PartsSalesReturnBill_ContactPerson {
            get {
                return Web.Resources.EntityStrings.PartsSalesReturnBill_ContactPerson;
            }
        }

        public string PartsSalesReturnBill_ContactPhone {
            get {
                return Web.Resources.EntityStrings.PartsSalesReturnBill_ContactPhone;
            }
        }

        public string PartsSalesReturnBill_ReturnReason {
            get {
                return Web.Resources.EntityStrings.PartsSalesReturnBill_ReturnReason;
            }
        }

        public string PartsSalesReturnBill_ReturnType {
            get {
                return Web.Resources.EntityStrings.PartsSalesReturnBill_ReturnType;
            }
        }

        public string PartsSalesReturnBill_WarehouseName {
            get {
                return Web.Resources.EntityStrings.PartsSalesReturnBill_WarehouseName;
            }
        }

        public string PartsSupplierRelation_BranchCode {
            get {
                return Web.Resources.EntityStrings.PartsSupplierRelation_BranchCode;
            }
        }

        public string PartsSupplierRelation_BranchName {
            get {
                return Web.Resources.EntityStrings.PartsSupplierRelation_BranchName;
            }
        }

        public string PartsPurReturnOrder_PartsPurReturnOrderDetails {
            get {
                return Web.Resources.EntityStrings.PartsPurReturnOrder_PartsPurReturnOrderDetails;
            }
        }

        public string InternalAllocationBill_BranchName {
            get {
                return Web.Resources.EntityStrings.InternalAllocationBill_BranchName;
            }
        }

        public string InternalAllocationBill_Code {
            get {
                return Web.Resources.EntityStrings.InternalAllocationBill_Code;
            }
        }

        public string InternalAllocationBill_InternalAllocationDetails {
            get {
                return Web.Resources.EntityStrings.InternalAllocationBill_InternalAllocationDetails;
            }
        }

        public string InternalAllocationDetail_MeasureUnit {
            get {
                return Web.Resources.EntityStrings.InternalAllocationDetail_MeasureUnit;
            }
        }

        public string InternalAllocationDetail_Quantity {
            get {
                return Web.Resources.EntityStrings.InternalAllocationDetail_Quantity;
            }
        }

        public string InternalAllocationDetail_SparePartCode {
            get {
                return Web.Resources.EntityStrings.InternalAllocationDetail_SparePartCode;
            }
        }

        public string InternalAllocationDetail_SparePartName {
            get {
                return Web.Resources.EntityStrings.InternalAllocationDetail_SparePartName;
            }
        }

        public string InternalAllocationDetail_UnitPrice {
            get {
                return Web.Resources.EntityStrings.InternalAllocationDetail_UnitPrice;
            }
        }

        public string PartsSalesOrderProcessDetail_MeasureUnit {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrderProcessDetail_MeasureUnit;
            }
        }

        public string InternalAcquisitionBill_Operator {
            get {
                return Web.Resources.EntityStrings.InternalAcquisitionBill_Operator;
            }
        }

        public string InternalAcquisitionBill_Reason {
            get {
                return Web.Resources.EntityStrings.InternalAcquisitionBill_Reason;
            }
        }

        public string InternalAcquisitionBill_TotalAmount {
            get {
                return Web.Resources.EntityStrings.InternalAcquisitionBill_TotalAmount;
            }
        }

        public string InternalAcquisitionDetail_MeasureUnit {
            get {
                return Web.Resources.EntityStrings.InternalAcquisitionDetail_MeasureUnit;
            }
        }

        public string InternalAcquisitionDetail_Quantity {
            get {
                return Web.Resources.EntityStrings.InternalAcquisitionDetail_Quantity;
            }
        }

        public string InternalAcquisitionDetail_SerialNumber {
            get {
                return Web.Resources.EntityStrings.InternalAcquisitionDetail_SerialNumber;
            }
        }

        public string InternalAcquisitionDetail_SparePartCode {
            get {
                return Web.Resources.EntityStrings.InternalAcquisitionDetail_SparePartCode;
            }
        }

        public string InternalAcquisitionDetail_SparePartName {
            get {
                return Web.Resources.EntityStrings.InternalAcquisitionDetail_SparePartName;
            }
        }

        public string InternalAcquisitionDetail_UnitPrice {
            get {
                return Web.Resources.EntityStrings.InternalAcquisitionDetail_UnitPrice;
            }
        }

        public string InternalAcquisitionBill_BranchName {
            get {
                return Web.Resources.EntityStrings.InternalAcquisitionBill_BranchName;
            }
        }

        public string InternalAcquisitionBill_Code {
            get {
                return Web.Resources.EntityStrings.InternalAcquisitionBill_Code;
            }
        }

        public string InternalAcquisitionBill_DepartmentName {
            get {
                return Web.Resources.EntityStrings.InternalAcquisitionBill_DepartmentName;
            }
        }

        public string InternalAcquisitionBill_InternalAcquisitionDetails {
            get {
                return Web.Resources.EntityStrings.InternalAcquisitionBill_InternalAcquisitionDetails;
            }
        }

        public string SalesUnit_Code {
            get {
                return Web.Resources.EntityStrings.SalesUnit_Code;
            }
        }

        public string SalesUnit_Name {
            get {
                return Web.Resources.EntityStrings.SalesUnit_Name;
            }
        }

        public string PartsSalesOrder_Code {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrder_Code;
            }
        }

        public string PartsSalesOrder_SalesActivityDiscountAmount {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrder_SalesActivityDiscountAmount;
            }
        }

        public string PartsSalesOrder_SalesActivityDiscountRate {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrder_SalesActivityDiscountRate;
            }
        }

        public string PartsSalesOrder_SalesUnitName {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrder_SalesUnitName;
            }
        }

        public string PartsSalesOrder_ShippingMethod {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrder_ShippingMethod;
            }
        }

        public string PartsSalesOrder_ApprovalComment {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrder_ApprovalComment;
            }
        }

        public string PartsSalesOrder_CancelOperatorName {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrder_CancelOperatorName;
            }
        }

        public string PartsSalesOrder_CancelTime {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrder_CancelTime;
            }
        }

        public string PartsSalesOrder_IfDirectProvision {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrder_IfDirectProvision;
            }
        }

        public string PartsSalesOrder_ReceivingCompanyName {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrder_ReceivingCompanyName;
            }
        }

        public string PartsSalesOrder_SubmitCompanyName {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrder_SubmitCompanyName;
            }
        }

        public string PartsSalesOrder_InvoiceReceiveCompanyName {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrder_InvoiceReceiveCompanyName;
            }
        }

        public string PartsSalesOrder_PartsSalesOrderDetails {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrder_PartsSalesOrderDetails;
            }
        }

        public string PartsSalesOrderDetail_ApproveQuantity {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrderDetail_ApproveQuantity;
            }
        }

        public string PartsSalesOrderDetail_CustOrderPriceGradeCoefficient {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrderDetail_CustOrderPriceGradeCoefficient;
            }
        }

        public string PartsSalesOrderDetail_DiscountedPrice {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrderDetail_DiscountedPrice;
            }
        }

        public string PartsSalesOrderDetail_EstimatedFulfillTime {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrderDetail_EstimatedFulfillTime;
            }
        }

        public string PartsSalesOrderDetail_OrderedQuantity {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrderDetail_OrderedQuantity;
            }
        }

        public string PartsSalesOrderDetail_OrderPrice {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrderDetail_OrderPrice;
            }
        }

        public string PartsSalesOrderDetail_OrderSum {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrderDetail_OrderSum;
            }
        }

        public string PartsSalesOrderDetail_OriginalPrice {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrderDetail_OriginalPrice;
            }
        }

        public string PartsSalesOrderDetail_SparePartCode {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrderDetail_SparePartCode;
            }
        }

        public string PartsSalesOrderDetail_SparePartName {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrderDetail_SparePartName;
            }
        }

        public string PartsSalesOrderProcess_PartsSalesOrderProcessDetails {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrderProcess_PartsSalesOrderProcessDetails;
            }
        }

        public string PartsSalesOrderProcess_BillStatusAfterProcess {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrderProcess_BillStatusAfterProcess;
            }
        }

        public string PartsSalesOrderProcess_BillStatusBeforeProcess {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrderProcess_BillStatusBeforeProcess;
            }
        }

        public string PartsSalesOrderProcess_RequestedDeliveryTime {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrderProcess_RequestedDeliveryTime;
            }
        }

        public string PartsSalesOrderProcess_RequestedShippingTime {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrderProcess_RequestedShippingTime;
            }
        }

        public string PartsSalesOrderProcessDetail_WarehouseName {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrderProcessDetail_WarehouseName;
            }
        }

        public string PartsSalesReturnBill_InvoiceRequirement {
            get {
                return Web.Resources.EntityStrings.PartsSalesReturnBill_InvoiceRequirement;
            }
        }

        public string PartsSalesReturnBill_PartsSalesReturnBillDetails {
            get {
                return Web.Resources.EntityStrings.PartsSalesReturnBill_PartsSalesReturnBillDetails;
            }
        }

        public string PartsPlannedPrice_PlannedPrice {
            get {
                return Web.Resources.EntityStrings.PartsPlannedPrice_PlannedPrice;
            }
        }

        public string Personnel_CellNumber {
            get {
                return Web.Resources.EntityStrings.Personnel_CellNumber;
            }
        }

        public string CustomerAccount_AccountBalance {
            get {
                return Web.Resources.EntityStrings.CustomerAccount_AccountBalance;
            }
        }

        public string CustomerAccount_PendingAmount {
            get {
                return Web.Resources.EntityStrings.CustomerAccount_PendingAmount;
            }
        }

        public string CustomerCredenceAmount_ShippedProductValue {
            get {
                return Web.Resources.EntityStrings.CustomerCredenceAmount_ShippedProductValue;
            }
        }

        public string CustomerAccount_ShippedProductValue {
            get {
                return Web.Resources.EntityStrings.CustomerAccount_ShippedProductValue;
            }
        }

        public string PartsSalesOrder_PartsSalesOrderProcesses {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrder_PartsSalesOrderProcesses;
            }
        }

        public string RepairItemCategory_Code {
            get {
                return Web.Resources.EntityStrings.RepairItemCategory_Code;
            }
        }

        public string RepairItemCategory_Name {
            get {
                return Web.Resources.EntityStrings.RepairItemCategory_Name;
            }
        }

        public string PlannedPriceAppDetail_AmountDifference {
            get {
                return Web.Resources.EntityStrings.PlannedPriceAppDetail_AmountDifference;
            }
        }

        public string PlannedPriceAppDetail_PriceBeforeChange {
            get {
                return Web.Resources.EntityStrings.PlannedPriceAppDetail_PriceBeforeChange;
            }
        }

        public string PlannedPriceAppDetail_Quantity {
            get {
                return Web.Resources.EntityStrings.PlannedPriceAppDetail_Quantity;
            }
        }

        public string PlannedPriceAppDetail_RequestedPrice {
            get {
                return Web.Resources.EntityStrings.PlannedPriceAppDetail_RequestedPrice;
            }
        }

        public string PlannedPriceAppDetail_SparePartCode {
            get {
                return Web.Resources.EntityStrings.PlannedPriceAppDetail_SparePartCode;
            }
        }

        public string PlannedPriceAppDetail_SparePartName {
            get {
                return Web.Resources.EntityStrings.PlannedPriceAppDetail_SparePartName;
            }
        }

        public string PlannedPriceApp_ActualExecutionTime {
            get {
                return Web.Resources.EntityStrings.PlannedPriceApp_ActualExecutionTime;
            }
        }

        public string PlannedPriceApp_AmountAfterChange {
            get {
                return Web.Resources.EntityStrings.PlannedPriceApp_AmountAfterChange;
            }
        }

        public string PlannedPriceApp_AmountBeforeChange {
            get {
                return Web.Resources.EntityStrings.PlannedPriceApp_AmountBeforeChange;
            }
        }

        public string PlannedPriceApp_AmountDifference {
            get {
                return Web.Resources.EntityStrings.PlannedPriceApp_AmountDifference;
            }
        }

        public string PlannedPriceApp_PlannedExecutionTime {
            get {
                return Web.Resources.EntityStrings.PlannedPriceApp_PlannedExecutionTime;
            }
        }

        public string PlannedPriceApp_PlannedPriceAppDetails {
            get {
                return Web.Resources.EntityStrings.PlannedPriceApp_PlannedPriceAppDetails;
            }
        }

        public string PlannedPriceApp_RecordStatus {
            get {
                return Web.Resources.EntityStrings.PlannedPriceApp_RecordStatus;
            }
        }

        public string WarehouseCostChangeBill_AmountAfterChange {
            get {
                return Web.Resources.EntityStrings.WarehouseCostChangeBill_AmountAfterChange;
            }
        }

        public string WarehouseCostChangeBill_AmountBeforeChange {
            get {
                return Web.Resources.EntityStrings.WarehouseCostChangeBill_AmountBeforeChange;
            }
        }

        public string WarehouseCostChangeBill_AmountDifference {
            get {
                return Web.Resources.EntityStrings.WarehouseCostChangeBill_AmountDifference;
            }
        }

        public string WarehouseCostChangeBill_Code {
            get {
                return Web.Resources.EntityStrings.WarehouseCostChangeBill_Code;
            }
        }

        public string WarehouseCostChangeBill_WarehouseName {
            get {
                return Web.Resources.EntityStrings.WarehouseCostChangeBill_WarehouseName;
            }
        }

        public string WarehouseCostChangeDetail_AmountDifference {
            get {
                return Web.Resources.EntityStrings.WarehouseCostChangeDetail_AmountDifference;
            }
        }

        public string WarehouseCostChangeDetail_PriceBeforeChange {
            get {
                return Web.Resources.EntityStrings.WarehouseCostChangeDetail_PriceBeforeChange;
            }
        }

        public string WarehouseCostChangeDetail_Quantity {
            get {
                return Web.Resources.EntityStrings.WarehouseCostChangeDetail_Quantity;
            }
        }

        public string WarehouseCostChangeDetail_SparePartCode {
            get {
                return Web.Resources.EntityStrings.WarehouseCostChangeDetail_SparePartCode;
            }
        }

        public string WarehouseCostChangeDetail_SparePartName {
            get {
                return Web.Resources.EntityStrings.WarehouseCostChangeDetail_SparePartName;
            }
        }

        public string SsClaimSettlementBill_Code {
            get {
                return Web.Resources.EntityStrings.SsClaimSettlementBill_Code;
            }
        }

        public string SsClaimSettlementBill_ComplementAmount {
            get {
                return Web.Resources.EntityStrings.SsClaimSettlementBill_ComplementAmount;
            }
        }

        public string SsClaimSettlementBill_DebitAmount {
            get {
                return Web.Resources.EntityStrings.SsClaimSettlementBill_DebitAmount;
            }
        }

        public string SsClaimSettlementBill_FieldServiceExpense {
            get {
                return Web.Resources.EntityStrings.SsClaimSettlementBill_FieldServiceExpense;
            }
        }

        public string SsClaimSettlementBill_LaborCost {
            get {
                return Web.Resources.EntityStrings.SsClaimSettlementBill_LaborCost;
            }
        }

        public string SsClaimSettlementBill_MaterialCost {
            get {
                return Web.Resources.EntityStrings.SsClaimSettlementBill_MaterialCost;
            }
        }

        public string SsClaimSettlementBill_OtherCost {
            get {
                return Web.Resources.EntityStrings.SsClaimSettlementBill_OtherCost;
            }
        }

        public string SsClaimSettlementBill_PartsManagementCost {
            get {
                return Web.Resources.EntityStrings.SsClaimSettlementBill_PartsManagementCost;
            }
        }

        public string SsClaimSettlementBill_SettlementEndTime {
            get {
                return Web.Resources.EntityStrings.SsClaimSettlementBill_SettlementEndTime;
            }
        }

        public string SsClaimSettlementBill_SettlementStartTime {
            get {
                return Web.Resources.EntityStrings.SsClaimSettlementBill_SettlementStartTime;
            }
        }

        public string SsClaimSettlementBill_TaxRate {
            get {
                return Web.Resources.EntityStrings.SsClaimSettlementBill_TaxRate;
            }
        }

        public string SsClaimSettlementBill_TotalAmount {
            get {
                return Web.Resources.EntityStrings.SsClaimSettlementBill_TotalAmount;
            }
        }

        public string SsClaimSettlementDetail_ComplementAmount {
            get {
                return Web.Resources.EntityStrings.SsClaimSettlementDetail_ComplementAmount;
            }
        }

        public string SsClaimSettlementDetail_DebitAmount {
            get {
                return Web.Resources.EntityStrings.SsClaimSettlementDetail_DebitAmount;
            }
        }

        public string SsClaimSettlementDetail_FieldServiceExpense {
            get {
                return Web.Resources.EntityStrings.SsClaimSettlementDetail_FieldServiceExpense;
            }
        }

        public string SsClaimSettlementDetail_LaborCost {
            get {
                return Web.Resources.EntityStrings.SsClaimSettlementDetail_LaborCost;
            }
        }

        public string SsClaimSettlementDetail_MaterialCost {
            get {
                return Web.Resources.EntityStrings.SsClaimSettlementDetail_MaterialCost;
            }
        }

        public string SsClaimSettlementDetail_OtherCost {
            get {
                return Web.Resources.EntityStrings.SsClaimSettlementDetail_OtherCost;
            }
        }

        public string SsClaimSettlementDetail_PartsManagementCost {
            get {
                return Web.Resources.EntityStrings.SsClaimSettlementDetail_PartsManagementCost;
            }
        }

        public string SsClaimSettlementDetail_SourceCode {
            get {
                return Web.Resources.EntityStrings.SsClaimSettlementDetail_SourceCode;
            }
        }

        public string SsClaimSettlementDetail_SourceType {
            get {
                return Web.Resources.EntityStrings.SsClaimSettlementDetail_SourceType;
            }
        }

        public string SsClaimSettlementDetail_TotalAmount {
            get {
                return Web.Resources.EntityStrings.SsClaimSettlementDetail_TotalAmount;
            }
        }

        public string SsClaimSettleInstruction_BranchCode {
            get {
                return Web.Resources.EntityStrings.SsClaimSettleInstruction_BranchCode;
            }
        }

        public string SsClaimSettleInstruction_BranchName {
            get {
                return Web.Resources.EntityStrings.SsClaimSettleInstruction_BranchName;
            }
        }

        public string SsClaimSettleInstruction_ExecutionResult {
            get {
                return Web.Resources.EntityStrings.SsClaimSettleInstruction_ExecutionResult;
            }
        }

        public string SsClaimSettleInstruction_ExecutionTime {
            get {
                return Web.Resources.EntityStrings.SsClaimSettleInstruction_ExecutionTime;
            }
        }

        public string SsClaimSettleInstruction_SettlementEndTime {
            get {
                return Web.Resources.EntityStrings.SsClaimSettleInstruction_SettlementEndTime;
            }
        }

        public string SsClaimSettleInstruction_SettlementStartTime {
            get {
                return Web.Resources.EntityStrings.SsClaimSettleInstruction_SettlementStartTime;
            }
        }

        public string PartsSalesOrderDetail_MeasureUnit {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrderDetail_MeasureUnit;
            }
        }

        public string PartsRetailOrder_CustomerAddress {
            get {
                return Web.Resources.EntityStrings.PartsRetailOrder_CustomerAddress;
            }
        }

        public string PartsRetailOrder_CustomerCellPhone {
            get {
                return Web.Resources.EntityStrings.PartsRetailOrder_CustomerCellPhone;
            }
        }

        public string PartsRetailOrder_CustomerName {
            get {
                return Web.Resources.EntityStrings.PartsRetailOrder_CustomerName;
            }
        }

        public string PartsRetailOrder_SalesUnitName {
            get {
                return Web.Resources.EntityStrings.PartsRetailOrder_SalesUnitName;
            }
        }

        public string PartsRetailOrder_WarehouseName {
            get {
                return Web.Resources.EntityStrings.PartsRetailOrder_WarehouseName;
            }
        }

        public string PartsRetailOrder_PaymentMethod {
            get {
                return Web.Resources.EntityStrings.PartsRetailOrder_PaymentMethod;
            }
        }

        public string PartsSalesReturnBill_ReturnCompanyName {
            get {
                return Web.Resources.EntityStrings.PartsSalesReturnBill_ReturnCompanyName;
            }
        }

        public string PartsSalesReturnBill_TotalAmount {
            get {
                return Web.Resources.EntityStrings.PartsSalesReturnBill_TotalAmount;
            }
        }

        public string SsClaimSettlementBill_DealerCode {
            get {
                return Web.Resources.EntityStrings.SsClaimSettlementBill_DealerCode;
            }
        }

        public string SsClaimSettlementBill_DealerName {
            get {
                return Web.Resources.EntityStrings.SsClaimSettlementBill_DealerName;
            }
        }

        public string PartsStock_Quantity {
            get {
                return Web.Resources.EntityStrings.PartsStock_Quantity;
            }
        }

        public string PartsRetailOrder_InvoiceOperatorName {
            get {
                return Web.Resources.EntityStrings.PartsRetailOrder_InvoiceOperatorName;
            }
        }

        public string PartsRetailOrder_InvoiceTime {
            get {
                return Web.Resources.EntityStrings.PartsRetailOrder_InvoiceTime;
            }
        }

        public string PartsRetailOrder_DiscountedAmount {
            get {
                return Web.Resources.EntityStrings.PartsRetailOrder_DiscountedAmount;
            }
        }

        public string PartsRetailOrderDetail_DiscountAmount {
            get {
                return Web.Resources.EntityStrings.PartsRetailOrderDetail_DiscountAmount;
            }
        }

        public string PartsRetailOrderDetail_DiscountedAmount {
            get {
                return Web.Resources.EntityStrings.PartsRetailOrderDetail_DiscountedAmount;
            }
        }

        public string PartsRetailOrderDetail_DiscountedPrice {
            get {
                return Web.Resources.EntityStrings.PartsRetailOrderDetail_DiscountedPrice;
            }
        }

        public string PartsRetailOrderDetail_Quantity {
            get {
                return Web.Resources.EntityStrings.PartsRetailOrderDetail_Quantity;
            }
        }

        public string PartsRetailOrderDetail_SalesPrice {
            get {
                return Web.Resources.EntityStrings.PartsRetailOrderDetail_SalesPrice;
            }
        }

        public string PartsRetailOrderDetail_SparePartCode {
            get {
                return Web.Resources.EntityStrings.PartsRetailOrderDetail_SparePartCode;
            }
        }

        public string PartsRetailOrderDetail_SparePartName {
            get {
                return Web.Resources.EntityStrings.PartsRetailOrderDetail_SparePartName;
            }
        }

        public string PartsRetailOrder_OriginalAmount {
            get {
                return Web.Resources.EntityStrings.PartsRetailOrder_OriginalAmount;
            }
        }

        public string PartsRetailOrder_CustomerPhone {
            get {
                return Web.Resources.EntityStrings.PartsRetailOrder_CustomerPhone;
            }
        }

        public string PartsRetailOrder_DiscountAmount {
            get {
                return Web.Resources.EntityStrings.PartsRetailOrder_DiscountAmount;
            }
        }

        public string PartsRetailOrder_DiscountRate {
            get {
                return Web.Resources.EntityStrings.PartsRetailOrder_DiscountRate;
            }
        }

        public string PartsTransferOrderDetail_ConfirmedAmount {
            get {
                return Web.Resources.EntityStrings.PartsTransferOrderDetail_ConfirmedAmount;
            }
        }

        public string PartsTransferOrderDetail_PlannedAmount {
            get {
                return Web.Resources.EntityStrings.PartsTransferOrderDetail_PlannedAmount;
            }
        }

        public string PartsTransferOrderDetail_SparePartCode {
            get {
                return Web.Resources.EntityStrings.PartsTransferOrderDetail_SparePartCode;
            }
        }

        public string PartsTransferOrderDetail_SparePartName {
            get {
                return Web.Resources.EntityStrings.PartsTransferOrderDetail_SparePartName;
            }
        }

        public string PartsTransferOrder_ShippingMethod {
            get {
                return Web.Resources.EntityStrings.PartsTransferOrder_ShippingMethod;
            }
        }

        public string PartsTransferOrder_Type {
            get {
                return Web.Resources.EntityStrings.PartsTransferOrder_Type;
            }
        }

        public string PartsPurchaseSettleBill_BranchName {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseSettleBill_BranchName;
            }
        }

        public string PartsPurchaseSettleBill_CostAmountDifference {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseSettleBill_CostAmountDifference;
            }
        }

        public string PartsPurchaseSettleBill_InvoiceAmountDifference {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseSettleBill_InvoiceAmountDifference;
            }
        }

        public string PartsPurchaseSettleBill_OffsettedSettlementBillCode {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseSettleBill_OffsettedSettlementBillCode;
            }
        }

        public string PartsPurchaseSettleBill_PartsSupplierCode {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseSettleBill_PartsSupplierCode;
            }
        }

        public string PartsPurchaseSettleBill_PartsSupplierName {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseSettleBill_PartsSupplierName;
            }
        }

        public string PartsPurchaseSettleBill_SettlementPath {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseSettleBill_SettlementPath;
            }
        }

        public string PartsPurchaseSettleBill_Tax {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseSettleBill_Tax;
            }
        }

        public string PartsPurchaseSettleBill_TaxRate {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseSettleBill_TaxRate;
            }
        }

        public string PartsPurchaseSettleBill_TotalSettlementAmount {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseSettleBill_TotalSettlementAmount;
            }
        }

        public string PartsPurchaseSettleBill_WarehouseName {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseSettleBill_WarehouseName;
            }
        }

        public string PartsPurchaseSettleDetail_QuantityToSettle {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseSettleDetail_QuantityToSettle;
            }
        }

        public string PartsPurchaseSettleDetail_SettlementAmount {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseSettleDetail_SettlementAmount;
            }
        }

        public string PartsPurchaseSettleDetail_SettlementPrice {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseSettleDetail_SettlementPrice;
            }
        }

        public string PartsPurchaseSettleDetail_SparePartCode {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseSettleDetail_SparePartCode;
            }
        }

        public string PartsPurchaseSettleDetail_SparePartName {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseSettleDetail_SparePartName;
            }
        }

        public string InvoiceInformation_Code {
            get {
                return Web.Resources.EntityStrings.InvoiceInformation_Code;
            }
        }

        public string InvoiceInformation_InvoiceAmount {
            get {
                return Web.Resources.EntityStrings.InvoiceInformation_InvoiceAmount;
            }
        }

        public string InvoiceInformation_InvoiceCode {
            get {
                return Web.Resources.EntityStrings.InvoiceInformation_InvoiceCode;
            }
        }

        public string InvoiceInformation_InvoiceCompanyCode {
            get {
                return Web.Resources.EntityStrings.InvoiceInformation_InvoiceCompanyCode;
            }
        }

        public string InvoiceInformation_InvoiceCompanyName {
            get {
                return Web.Resources.EntityStrings.InvoiceInformation_InvoiceCompanyName;
            }
        }

        public string InvoiceInformation_InvoiceDate {
            get {
                return Web.Resources.EntityStrings.InvoiceInformation_InvoiceDate;
            }
        }

        public string InvoiceInformation_InvoiceNumber {
            get {
                return Web.Resources.EntityStrings.InvoiceInformation_InvoiceNumber;
            }
        }

        public string InvoiceInformation_InvoicePurpose {
            get {
                return Web.Resources.EntityStrings.InvoiceInformation_InvoicePurpose;
            }
        }

        public string InvoiceInformation_InvoiceReceiveCompanyCode {
            get {
                return Web.Resources.EntityStrings.InvoiceInformation_InvoiceReceiveCompanyCode;
            }
        }

        public string InvoiceInformation_InvoiceReceiveCompanyName {
            get {
                return Web.Resources.EntityStrings.InvoiceInformation_InvoiceReceiveCompanyName;
            }
        }

        public string InvoiceInformation_InvoiceTax {
            get {
                return Web.Resources.EntityStrings.InvoiceInformation_InvoiceTax;
            }
        }

        public string InvoiceInformation_SourceCode {
            get {
                return Web.Resources.EntityStrings.InvoiceInformation_SourceCode;
            }
        }

        public string InvoiceInformation_SourceType {
            get {
                return Web.Resources.EntityStrings.InvoiceInformation_SourceType;
            }
        }

        public string InvoiceInformation_TaxRate {
            get {
                return Web.Resources.EntityStrings.InvoiceInformation_TaxRate;
            }
        }

        public string InvoiceInformation_Type {
            get {
                return Web.Resources.EntityStrings.InvoiceInformation_Type;
            }
        }

        public string PartsRetailReturnBillDetail_OriginalOrderPrice {
            get {
                return Web.Resources.EntityStrings.PartsRetailReturnBillDetail_OriginalOrderPrice;
            }
        }

        public string PartsRetailReturnBillDetail_Quantity {
            get {
                return Web.Resources.EntityStrings.PartsRetailReturnBillDetail_Quantity;
            }
        }

        public string PartsRetailReturnBillDetail_ReturnPrice {
            get {
                return Web.Resources.EntityStrings.PartsRetailReturnBillDetail_ReturnPrice;
            }
        }

        public string PartsRetailReturnBillDetail_SparePartCode {
            get {
                return Web.Resources.EntityStrings.PartsRetailReturnBillDetail_SparePartCode;
            }
        }

        public string PartsRetailReturnBillDetail_SparePartName {
            get {
                return Web.Resources.EntityStrings.PartsRetailReturnBillDetail_SparePartName;
            }
        }

        public string PartsRetailReturnBill_Code {
            get {
                return Web.Resources.EntityStrings.PartsRetailReturnBill_Code;
            }
        }

        public string PartsRetailReturnBill_CustomerAddress {
            get {
                return Web.Resources.EntityStrings.PartsRetailReturnBill_CustomerAddress;
            }
        }

        public string PartsRetailReturnBill_CustomerCellPhone {
            get {
                return Web.Resources.EntityStrings.PartsRetailReturnBill_CustomerCellPhone;
            }
        }

        public string PartsRetailReturnBill_CustomerName {
            get {
                return Web.Resources.EntityStrings.PartsRetailReturnBill_CustomerName;
            }
        }

        public string PartsRetailReturnBill_CustomerPhone {
            get {
                return Web.Resources.EntityStrings.PartsRetailReturnBill_CustomerPhone;
            }
        }

        public string PartsRetailReturnBill_ReturnReason {
            get {
                return Web.Resources.EntityStrings.PartsRetailReturnBill_ReturnReason;
            }
        }

        public string PartsRetailReturnBill_SourceCode {
            get {
                return Web.Resources.EntityStrings.PartsRetailReturnBill_SourceCode;
            }
        }

        public string PartsRetailReturnBill_TotalAmount {
            get {
                return Web.Resources.EntityStrings.PartsRetailReturnBill_TotalAmount;
            }
        }

        public string PartsRetailReturnBill_WarehouseName {
            get {
                return Web.Resources.EntityStrings.PartsRetailReturnBill_WarehouseName;
            }
        }

        public string UsedPartsDisposalBill_UsedPartsDisposalDetails {
            get {
                return Web.Resources.EntityStrings.UsedPartsDisposalBill_UsedPartsDisposalDetails;
            }
        }

        public string PartsPurchaseRtnSettleBill_PartsSupplierCode {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseRtnSettleBill_PartsSupplierCode;
            }
        }

        public string PartsPurchaseRtnSettleBill_PartsSupplierName {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseRtnSettleBill_PartsSupplierName;
            }
        }

        public string PartsPurchaseRtnSettleBill_Tax {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseRtnSettleBill_Tax;
            }
        }

        public string PartsPurchaseRtnSettleBill_TaxRate {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseRtnSettleBill_TaxRate;
            }
        }

        public string PartsPurchaseRtnSettleBill_WarehouseName {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseRtnSettleBill_WarehouseName;
            }
        }

        public string PartsPurchaseRtnSettleBill_InvoicePath {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseRtnSettleBill_InvoicePath;
            }
        }

        public string SsUsedPartsStorage_BranchName {
            get {
                return Web.Resources.EntityStrings.SsUsedPartsStorage_BranchName;
            }
        }

        public string PartsPurchaseSettleBill_PartsPurchaseSettleDetails {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseSettleBill_PartsPurchaseSettleDetails;
            }
        }

        public string PartsPurchaseRtnSettleBill_TotalSettlementAmount {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseRtnSettleBill_TotalSettlementAmount;
            }
        }

        public string PartsPurchaseRtnSettleBill_BranchName {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseRtnSettleBill_BranchName;
            }
        }

        public string PartsPurchaseRtnSettleBill_Code {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseRtnSettleBill_Code;
            }
        }

        public string PartsPurchaseRtnSettleBill_InvoiceAmountDifference {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseRtnSettleBill_InvoiceAmountDifference;
            }
        }

        public string PartsPurchaseRtnSettleBill_OffsettedSettlementBillCode {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseRtnSettleBill_OffsettedSettlementBillCode;
            }
        }

        public string PartsPurchaseRtnSettleBill_SettlementPath {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseRtnSettleBill_SettlementPath;
            }
        }

        public string PartsPurchaseRtnSettleDetail_QuantityToSettle {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseRtnSettleDetail_QuantityToSettle;
            }
        }

        public string PartsPurchaseRtnSettleDetail_SettlementAmount {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseRtnSettleDetail_SettlementAmount;
            }
        }

        public string PartsPurchaseRtnSettleDetail_SettlementPrice {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseRtnSettleDetail_SettlementPrice;
            }
        }

        public string PartsPurchaseRtnSettleDetail_SparePartCode {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseRtnSettleDetail_SparePartCode;
            }
        }

        public string PartsPurchaseRtnSettleDetail_SparePartName {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseRtnSettleDetail_SparePartName;
            }
        }

        public string PartsTransferOrder_ReceivingAddress {
            get {
                return Web.Resources.EntityStrings.PartsTransferOrder_ReceivingAddress;
            }
        }

        public string PartsShippingOrder_Code {
            get {
                return Web.Resources.EntityStrings.PartsShippingOrder_Code;
            }
        }

        public string PartsShippingOrder_DeliveryBillNumber {
            get {
                return Web.Resources.EntityStrings.PartsShippingOrder_DeliveryBillNumber;
            }
        }

        public string PartsShippingOrder_LogisticCompanyCode {
            get {
                return Web.Resources.EntityStrings.PartsShippingOrder_LogisticCompanyCode;
            }
        }

        public string PartsShippingOrder_LogisticCompanyName {
            get {
                return Web.Resources.EntityStrings.PartsShippingOrder_LogisticCompanyName;
            }
        }

        public string PartsShippingOrder_ReceivingAddress {
            get {
                return Web.Resources.EntityStrings.PartsShippingOrder_ReceivingAddress;
            }
        }

        public string PartsShippingOrder_ReceivingCompanyCode {
            get {
                return Web.Resources.EntityStrings.PartsShippingOrder_ReceivingCompanyCode;
            }
        }

        public string PartsShippingOrder_ReceivingCompanyName {
            get {
                return Web.Resources.EntityStrings.PartsShippingOrder_ReceivingCompanyName;
            }
        }

        public string PartsShippingOrder_RequestedArrivalDate {
            get {
                return Web.Resources.EntityStrings.PartsShippingOrder_RequestedArrivalDate;
            }
        }

        public string PartsShippingOrder_ShippingCompanyCode {
            get {
                return Web.Resources.EntityStrings.PartsShippingOrder_ShippingCompanyCode;
            }
        }

        public string PartsShippingOrder_ShippingCompanyName {
            get {
                return Web.Resources.EntityStrings.PartsShippingOrder_ShippingCompanyName;
            }
        }

        public string PartsShippingOrder_ShippingDate {
            get {
                return Web.Resources.EntityStrings.PartsShippingOrder_ShippingDate;
            }
        }

        public string PartsShippingOrder_ShippingMethod {
            get {
                return Web.Resources.EntityStrings.PartsShippingOrder_ShippingMethod;
            }
        }

        public string PartsShippingOrder_TransportCost {
            get {
                return Web.Resources.EntityStrings.PartsShippingOrder_TransportCost;
            }
        }

        public string PartsShippingOrder_TransportCostSettleStatus {
            get {
                return Web.Resources.EntityStrings.PartsShippingOrder_TransportCostSettleStatus;
            }
        }

        public string PartsShippingOrder_TransportDriverPhone {
            get {
                return Web.Resources.EntityStrings.PartsShippingOrder_TransportDriverPhone;
            }
        }

        public string PartsShippingOrder_Type {
            get {
                return Web.Resources.EntityStrings.PartsShippingOrder_Type;
            }
        }

        public string PartsShippingOrder_Volume {
            get {
                return Web.Resources.EntityStrings.PartsShippingOrder_Volume;
            }
        }

        public string PartsShippingOrder_Weight {
            get {
                return Web.Resources.EntityStrings.PartsShippingOrder_Weight;
            }
        }

        public string PartsShippingOrderDetail_ConfirmedAmount {
            get {
                return Web.Resources.EntityStrings.PartsShippingOrderDetail_ConfirmedAmount;
            }
        }

        public string PartsShippingOrderDetail_InTransitDamageLossAmount {
            get {
                return Web.Resources.EntityStrings.PartsShippingOrderDetail_InTransitDamageLossAmount;
            }
        }

        public string PartsShippingOrderDetail_SettlementPrice {
            get {
                return Web.Resources.EntityStrings.PartsShippingOrderDetail_SettlementPrice;
            }
        }

        public string PartsShippingOrderDetail_ShippingAmount {
            get {
                return Web.Resources.EntityStrings.PartsShippingOrderDetail_ShippingAmount;
            }
        }

        public string PartsShippingOrderDetail_SparePartCode {
            get {
                return Web.Resources.EntityStrings.PartsShippingOrderDetail_SparePartCode;
            }
        }

        public string PartsShippingOrderDetail_SparePartName {
            get {
                return Web.Resources.EntityStrings.PartsShippingOrderDetail_SparePartName;
            }
        }

        public string PartsShippingOrder_ConfirmedReceptionTime {
            get {
                return Web.Resources.EntityStrings.PartsShippingOrder_ConfirmedReceptionTime;
            }
        }

        public string PartsShippingOrder_ConsigneeName {
            get {
                return Web.Resources.EntityStrings.PartsShippingOrder_ConsigneeName;
            }
        }

        public string PartsShippingOrder_InsuranceFee {
            get {
                return Web.Resources.EntityStrings.PartsShippingOrder_InsuranceFee;
            }
        }

        public string PartsShippingOrder_ReceiptConfirmorName {
            get {
                return Web.Resources.EntityStrings.PartsShippingOrder_ReceiptConfirmorName;
            }
        }

        public string PartsShippingOrder_ReceiptConfirmTime {
            get {
                return Web.Resources.EntityStrings.PartsShippingOrder_ReceiptConfirmTime;
            }
        }

        public string PartsShippingOrder_TransportDriver {
            get {
                return Web.Resources.EntityStrings.PartsShippingOrder_TransportDriver;
            }
        }

        public string PartsShippingOrder_TransportMileage {
            get {
                return Web.Resources.EntityStrings.PartsShippingOrder_TransportMileage;
            }
        }

        public string PartsShippingOrder_TransportVehiclePlate {
            get {
                return Web.Resources.EntityStrings.PartsShippingOrder_TransportVehiclePlate;
            }
        }

        public string PaymentBeneficiaryList_Amount {
            get {
                return Web.Resources.EntityStrings.PaymentBeneficiaryList_Amount;
            }
        }

        public string PaymentBeneficiaryList_BillNumber {
            get {
                return Web.Resources.EntityStrings.PaymentBeneficiaryList_BillNumber;
            }
        }

        public string PaymentBeneficiaryList_Operator {
            get {
                return Web.Resources.EntityStrings.PaymentBeneficiaryList_Operator;
            }
        }

        public string PaymentBeneficiaryList_PaymentBillCode {
            get {
                return Web.Resources.EntityStrings.PaymentBeneficiaryList_PaymentBillCode;
            }
        }

        public string PaymentBeneficiaryList_Status {
            get {
                return Web.Resources.EntityStrings.PaymentBeneficiaryList_Status;
            }
        }

        public string PaymentBeneficiaryList_Summary {
            get {
                return Web.Resources.EntityStrings.PaymentBeneficiaryList_Summary;
            }
        }

        public string CustomerTransferBill_Amount {
            get {
                return Web.Resources.EntityStrings.CustomerTransferBill_Amount;
            }
        }

        public string CustomerTransferBill_CredentialDocument {
            get {
                return Web.Resources.EntityStrings.CustomerTransferBill_CredentialDocument;
            }
        }

        public string CustomerTransferBill_Summary {
            get {
                return Web.Resources.EntityStrings.CustomerTransferBill_Summary;
            }
        }

        public string CustomerTransferBill_Operator {
            get {
                return Web.Resources.EntityStrings.CustomerTransferBill_Operator;
            }
        }

        public string PartsShippingOrder_ReceivingWarehouseCode {
            get {
                return Web.Resources.EntityStrings.PartsShippingOrder_ReceivingWarehouseCode;
            }
        }

        public string PartsShippingOrder_ReceivingWarehouseName {
            get {
                return Web.Resources.EntityStrings.PartsShippingOrder_ReceivingWarehouseName;
            }
        }

        public string PartsOutboundPlanDetail_OutboundFulfillment {
            get {
                return Web.Resources.EntityStrings.PartsOutboundPlanDetail_OutboundFulfillment;
            }
        }

        public string PartsShippingOrder_PartsShippingOrderRefs {
            get {
                return Web.Resources.EntityStrings.PartsShippingOrder_PartsShippingOrderRefs;
            }
        }

        public string PaymentBill_Amount {
            get {
                return Web.Resources.EntityStrings.PaymentBill_Amount;
            }
        }

        public string PaymentBill_BillNumber {
            get {
                return Web.Resources.EntityStrings.PaymentBill_BillNumber;
            }
        }

        public string PaymentBill_Code {
            get {
                return Web.Resources.EntityStrings.PaymentBill_Code;
            }
        }

        public string PaymentBill_Operator {
            get {
                return Web.Resources.EntityStrings.PaymentBill_Operator;
            }
        }

        public string PaymentBill_PaymentMethod {
            get {
                return Web.Resources.EntityStrings.PaymentBill_PaymentMethod;
            }
        }

        public string PaymentBill_Summary {
            get {
                return Web.Resources.EntityStrings.PaymentBill_Summary;
            }
        }

        public string DealerVehicleMonthQuota_DealerCode {
            get {
                return Web.Resources.EntityStrings.DealerVehicleMonthQuota_DealerCode;
            }
        }

        public string DealerVehicleMonthQuota_DealerName {
            get {
                return Web.Resources.EntityStrings.DealerVehicleMonthQuota_DealerName;
            }
        }

        public string DealerVehicleMonthQuota_MonthOfPlan {
            get {
                return Web.Resources.EntityStrings.DealerVehicleMonthQuota_MonthOfPlan;
            }
        }

        public string DealerVehicleMonthQuota_YearOfPlan {
            get {
                return Web.Resources.EntityStrings.DealerVehicleMonthQuota_YearOfPlan;
            }
        }

        public string OEMVehicleMonthQuota_MonthOfPlan {
            get {
                return Web.Resources.EntityStrings.OEMVehicleMonthQuota_MonthOfPlan;
            }
        }

        public string OEMVehicleMonthQuota_YearOfPlan {
            get {
                return Web.Resources.EntityStrings.OEMVehicleMonthQuota_YearOfPlan;
            }
        }

        public string DealerRegionManagerAffi_DealerCode {
            get {
                return Web.Resources.EntityStrings.DealerRegionManagerAffi_DealerCode;
            }
        }

        public string DealerRegionManagerAffi_DealerName {
            get {
                return Web.Resources.EntityStrings.DealerRegionManagerAffi_DealerName;
            }
        }

        public string DealerRegionManagerAffi_RegionManager {
            get {
                return Web.Resources.EntityStrings.DealerRegionManagerAffi_RegionManager;
            }
        }

        public string VehicleOrderDetail_Quantity {
            get {
                return Web.Resources.EntityStrings.VehicleOrderDetail_Quantity;
            }
        }

        public string VehicleOrderPlan_MonthOfPlan {
            get {
                return Web.Resources.EntityStrings.VehicleOrderPlan_MonthOfPlan;
            }
        }

        public string VehicleOrderPlan_YearOfPlan {
            get {
                return Web.Resources.EntityStrings.VehicleOrderPlan_YearOfPlan;
            }
        }

        public string VehicleOrderPlan_DealerCode {
            get {
                return Web.Resources.EntityStrings.VehicleOrderPlan_DealerCode;
            }
        }

        public string VehicleOrderPlan_DealerName {
            get {
                return Web.Resources.EntityStrings.VehicleOrderPlan_DealerName;
            }
        }

        public string VehicleOrder_Balance {
            get {
                return Web.Resources.EntityStrings.VehicleOrder_Balance;
            }
        }

        public string VehicleOrder_OrderType {
            get {
                return Web.Resources.EntityStrings.VehicleOrder_OrderType;
            }
        }

        public string VehicleOrderSchedule_MonthOfOrder {
            get {
                return Web.Resources.EntityStrings.VehicleOrderSchedule_MonthOfOrder;
            }
        }

        public string VehicleOrderSchedule_OrderPlanToPODate {
            get {
                return Web.Resources.EntityStrings.VehicleOrderSchedule_OrderPlanToPODate;
            }
        }

        public string VehicleOrderSchedule_OrderSubmitDeadline {
            get {
                return Web.Resources.EntityStrings.VehicleOrderSchedule_OrderSubmitDeadline;
            }
        }

        public string VehicleOrderSchedule_YearOfOrder {
            get {
                return Web.Resources.EntityStrings.VehicleOrderSchedule_YearOfOrder;
            }
        }

        public string VehicleOrder_DealerCode {
            get {
                return Web.Resources.EntityStrings.VehicleOrder_DealerCode;
            }
        }

        public string VehicleOrder_DealerName {
            get {
                return Web.Resources.EntityStrings.VehicleOrder_DealerName;
            }
        }

        public string VehicleOrder_MonthOfPlan {
            get {
                return Web.Resources.EntityStrings.VehicleOrder_MonthOfPlan;
            }
        }

        public string VehicleOrder_YearOfPlan {
            get {
                return Web.Resources.EntityStrings.VehicleOrder_YearOfPlan;
            }
        }

        public string VehicleOrderDetail_Price {
            get {
                return Web.Resources.EntityStrings.VehicleOrderDetail_Price;
            }
        }

        public string VehicleOrderDetail_ProductCode {
            get {
                return Web.Resources.EntityStrings.VehicleOrderDetail_ProductCode;
            }
        }

        public string PartsSalesRtnSettlement_CustomerCompanyCode {
            get {
                return Web.Resources.EntityStrings.PartsSalesRtnSettlement_CustomerCompanyCode;
            }
        }

        public string PartsSalesRtnSettlement_CustomerCompanyName {
            get {
                return Web.Resources.EntityStrings.PartsSalesRtnSettlement_CustomerCompanyName;
            }
        }

        public string PartsSalesRtnSettlement_InvoiceAmountDifference {
            get {
                return Web.Resources.EntityStrings.PartsSalesRtnSettlement_InvoiceAmountDifference;
            }
        }

        public string PartsSalesRtnSettlement_InvoicePath {
            get {
                return Web.Resources.EntityStrings.PartsSalesRtnSettlement_InvoicePath;
            }
        }

        public string PartsSalesRtnSettlement_OffsettedSettlementBillCode {
            get {
                return Web.Resources.EntityStrings.PartsSalesRtnSettlement_OffsettedSettlementBillCode;
            }
        }

        public string PartsSalesRtnSettlement_SettlementPath {
            get {
                return Web.Resources.EntityStrings.PartsSalesRtnSettlement_SettlementPath;
            }
        }

        public string PartsSalesRtnSettlement_Tax {
            get {
                return Web.Resources.EntityStrings.PartsSalesRtnSettlement_Tax;
            }
        }

        public string PartsSalesRtnSettlement_TaxRate {
            get {
                return Web.Resources.EntityStrings.PartsSalesRtnSettlement_TaxRate;
            }
        }

        public string PartsSalesRtnSettlement_TotalSettlementAmount {
            get {
                return Web.Resources.EntityStrings.PartsSalesRtnSettlement_TotalSettlementAmount;
            }
        }

        public string PartsSalesRtnSettlementDetail_QuantityToSettle {
            get {
                return Web.Resources.EntityStrings.PartsSalesRtnSettlementDetail_QuantityToSettle;
            }
        }

        public string PartsSalesRtnSettlementDetail_SettlementAmount {
            get {
                return Web.Resources.EntityStrings.PartsSalesRtnSettlementDetail_SettlementAmount;
            }
        }

        public string PartsSalesRtnSettlementDetail_SparePartCode {
            get {
                return Web.Resources.EntityStrings.PartsSalesRtnSettlementDetail_SparePartCode;
            }
        }

        public string PartsSalesRtnSettlementDetail_SparePartName {
            get {
                return Web.Resources.EntityStrings.PartsSalesRtnSettlementDetail_SparePartName;
            }
        }

        public string PartsSalesRtnSettlementRef_SettlementAmount {
            get {
                return Web.Resources.EntityStrings.PartsSalesRtnSettlementRef_SettlementAmount;
            }
        }

        public string MalfunctionCategory_Code {
            get {
                return Web.Resources.EntityStrings.MalfunctionCategory_Code;
            }
        }

        public string MalfunctionCategory_Name {
            get {
                return Web.Resources.EntityStrings.MalfunctionCategory_Name;
            }
        }

        public string VehicleModelAffiProduct_Color {
            get {
                return Web.Resources.EntityStrings.VehicleModelAffiProduct_Color;
            }
        }

        public string VehicleModelAffiProduct_ProductCode {
            get {
                return Web.Resources.EntityStrings.VehicleModelAffiProduct_ProductCode;
            }
        }

        public string VehicleOrderPlan_VehicleOrderPlanDetail {
            get {
                return Web.Resources.EntityStrings.VehicleOrderPlan_VehicleOrderPlanDetail;
            }
        }

        public string VehicleOrderPlanDetail_ProductCode {
            get {
                return Web.Resources.EntityStrings.VehicleOrderPlanDetail_ProductCode;
            }
        }

        public string PartsSalesRtnSettlementRef_SourceCode {
            get {
                return Web.Resources.EntityStrings.PartsSalesRtnSettlementRef_SourceCode;
            }
        }

        public string PartsSalesRtnSettlementRef_SourceBillCreateTime {
            get {
                return Web.Resources.EntityStrings.PartsSalesRtnSettlementRef_SourceBillCreateTime;
            }
        }

        public string _Common_InvoiceRegistrationOperator {
            get {
                return Web.Resources.EntityStrings._Common_InvoiceRegistrationOperator;
            }
        }

        public string _Common_InvoiceRegistrationOperatorId {
            get {
                return Web.Resources.EntityStrings._Common_InvoiceRegistrationOperatorId;
            }
        }

        public string _Common_InvoiceRegistrationTime {
            get {
                return Web.Resources.EntityStrings._Common_InvoiceRegistrationTime;
            }
        }

        public string PartsSalesSettlement_CustomerCompanyCode {
            get {
                return Web.Resources.EntityStrings.PartsSalesSettlement_CustomerCompanyCode;
            }
        }

        public string PartsSalesSettlement_CustomerCompanyName {
            get {
                return Web.Resources.EntityStrings.PartsSalesSettlement_CustomerCompanyName;
            }
        }

        public string PartsSalesSettlement_OffsettedSettlementBillCode {
            get {
                return Web.Resources.EntityStrings.PartsSalesSettlement_OffsettedSettlementBillCode;
            }
        }

        public string PartsSalesSettlement_RebateAmount {
            get {
                return Web.Resources.EntityStrings.PartsSalesSettlement_RebateAmount;
            }
        }

        public string PartsSalesSettlement_SettlementPath {
            get {
                return Web.Resources.EntityStrings.PartsSalesSettlement_SettlementPath;
            }
        }

        public string PartsSalesSettlement_Tax {
            get {
                return Web.Resources.EntityStrings.PartsSalesSettlement_Tax;
            }
        }

        public string PartsSalesSettlement_TaxRate {
            get {
                return Web.Resources.EntityStrings.PartsSalesSettlement_TaxRate;
            }
        }

        public string PartsSalesSettlement_TotalSettlementAmount {
            get {
                return Web.Resources.EntityStrings.PartsSalesSettlement_TotalSettlementAmount;
            }
        }

        public string PartsSalesSettlementDetail_QuantityToSettle {
            get {
                return Web.Resources.EntityStrings.PartsSalesSettlementDetail_QuantityToSettle;
            }
        }

        public string PartsSalesSettlementDetail_SettlementAmount {
            get {
                return Web.Resources.EntityStrings.PartsSalesSettlementDetail_SettlementAmount;
            }
        }

        public string PartsSalesSettlementDetail_SettlementPrice {
            get {
                return Web.Resources.EntityStrings.PartsSalesSettlementDetail_SettlementPrice;
            }
        }

        public string PartsSalesSettlementDetail_SparePartCode {
            get {
                return Web.Resources.EntityStrings.PartsSalesSettlementDetail_SparePartCode;
            }
        }

        public string PartsSalesSettlementDetail_SparePartName {
            get {
                return Web.Resources.EntityStrings.PartsSalesSettlementDetail_SparePartName;
            }
        }

        public string PartsSalesSettlementRef_SettlementAmount {
            get {
                return Web.Resources.EntityStrings.PartsSalesSettlementRef_SettlementAmount;
            }
        }

        public string PartsSalesSettlementRef_SourceCode {
            get {
                return Web.Resources.EntityStrings.PartsSalesSettlementRef_SourceCode;
            }
        }

        public string PartsSalesSettlementRef_SourceType {
            get {
                return Web.Resources.EntityStrings.PartsSalesSettlementRef_SourceType;
            }
        }

        public string PartsSalesSettlement_PartsSalesSettlementRefs {
            get {
                return Web.Resources.EntityStrings.PartsSalesSettlement_PartsSalesSettlementRefs;
            }
        }

        public string PartsSalesSettlement_RebateBalance {
            get {
                return Web.Resources.EntityStrings.PartsSalesSettlement_RebateBalance;
            }
        }

        public string PartsSalesSettlement_RebateMethod {
            get {
                return Web.Resources.EntityStrings.PartsSalesSettlement_RebateMethod;
            }
        }

        public string PartsSalesSettlementDetail_DiscountedPrice {
            get {
                return Web.Resources.EntityStrings.PartsSalesSettlementDetail_DiscountedPrice;
            }
        }

        public string JumpShutoffDay_JumpShutoffDate {
            get {
                return Web.Resources.EntityStrings.JumpShutoffDay_JumpShutoffDate;
            }
        }

        public string JumpShutoffDay_MonthOfOrder {
            get {
                return Web.Resources.EntityStrings.JumpShutoffDay_MonthOfOrder;
            }
        }

        public string JumpShutoffDay_YearOfOrder {
            get {
                return Web.Resources.EntityStrings.JumpShutoffDay_YearOfOrder;
            }
        }

        public string VehShipplanApprovalChangeRec_AdjustmentType {
            get {
                return Web.Resources.EntityStrings.VehShipplanApprovalChangeRec_AdjustmentType;
            }
        }

        public string VehicleShipplanApprovalDetail_ProductCode {
            get {
                return Web.Resources.EntityStrings.VehicleShipplanApprovalDetail_ProductCode;
            }
        }

        public string ProductionPlan_MonthOfOrder {
            get {
                return Web.Resources.EntityStrings.ProductionPlan_MonthOfOrder;
            }
        }

        public string ProductionPlan_ProductCategoryCode {
            get {
                return Web.Resources.EntityStrings.ProductionPlan_ProductCategoryCode;
            }
        }

        public string ProductionPlan_ProductCode {
            get {
                return Web.Resources.EntityStrings.ProductionPlan_ProductCode;
            }
        }

        public string ProductionPlan_YearOfOrder {
            get {
                return Web.Resources.EntityStrings.ProductionPlan_YearOfOrder;
            }
        }

        public string VehicleOrderPlan_VehicleOrderPlanDetails {
            get {
                return Web.Resources.EntityStrings.VehicleOrderPlan_VehicleOrderPlanDetails;
            }
        }

        public string PartsSalesSettlement_PartsSalesSettlementDetails {
            get {
                return Web.Resources.EntityStrings.PartsSalesSettlement_PartsSalesSettlementDetails;
            }
        }

        public string VehicleShipplanApprovalDetail_VIN {
            get {
                return Web.Resources.EntityStrings.VehicleShipplanApprovalDetail_VIN;
            }
        }

        public string VehicleShipplanApproval_DealerCode {
            get {
                return Web.Resources.EntityStrings.VehicleShipplanApproval_DealerCode;
            }
        }

        public string VehicleShipplanApproval_DealerName {
            get {
                return Web.Resources.EntityStrings.VehicleShipplanApproval_DealerName;
            }
        }

        public string VehicleAvailableResource_ProductCode {
            get {
                return Web.Resources.EntityStrings.VehicleAvailableResource_ProductCode;
            }
        }

        public string VehicleAvailableResource_VIN {
            get {
                return Web.Resources.EntityStrings.VehicleAvailableResource_VIN;
            }
        }

        public string VehicleAvailableResource_ProductName {
            get {
                return Web.Resources.EntityStrings.VehicleAvailableResource_ProductName;
            }
        }

        public string Product_CanBeOrdered {
            get {
                return Web.Resources.EntityStrings.Product_CanBeOrdered;
            }
        }

        public string Product_Configuration {
            get {
                return Web.Resources.EntityStrings.Product_Configuration;
            }
        }

        public string Product_EmissionStandard {
            get {
                return Web.Resources.EntityStrings.Product_EmissionStandard;
            }
        }

        public string Product_EngineCylinder {
            get {
                return Web.Resources.EntityStrings.Product_EngineCylinder;
            }
        }

        public string Product_IfOption {
            get {
                return Web.Resources.EntityStrings.Product_IfOption;
            }
        }

        public string Product_IfPurchasable {
            get {
                return Web.Resources.EntityStrings.Product_IfPurchasable;
            }
        }

        public string Product_ManualAutomatic {
            get {
                return Web.Resources.EntityStrings.Product_ManualAutomatic;
            }
        }

        public string Product_OrderCycle {
            get {
                return Web.Resources.EntityStrings.Product_OrderCycle;
            }
        }

        public string Product_ProductLifeCycle {
            get {
                return Web.Resources.EntityStrings.Product_ProductLifeCycle;
            }
        }

        public string Product_Version {
            get {
                return Web.Resources.EntityStrings.Product_Version;
            }
        }

        public string VehicleSery_ProductCode {
            get {
                return Web.Resources.EntityStrings.VehicleSery_ProductCode;
            }
        }

        public string VehicleSery_RegionCode {
            get {
                return Web.Resources.EntityStrings.VehicleSery_RegionCode;
            }
        }

        public string VehicleSery_VehicleSeriesCode {
            get {
                return Web.Resources.EntityStrings.VehicleSery_VehicleSeriesCode;
            }
        }

        public string VehicleInformation_RolloutDate {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_RolloutDate;
            }
        }

        public string VehicleShipplanApprovalDetail_Price {
            get {
                return Web.Resources.EntityStrings.VehicleShipplanApprovalDetail_Price;
            }
        }

        public string VehicleShipplanApproval_VehicleShipplanApprovalDetails {
            get {
                return Web.Resources.EntityStrings.VehicleShipplanApproval_VehicleShipplanApprovalDetails;
            }
        }

        public string VehicleOrderDetail_EstimatedShippingDate {
            get {
                return Web.Resources.EntityStrings.VehicleOrderDetail_EstimatedShippingDate;
            }
        }

        public string ProductCategory_Code {
            get {
                return Web.Resources.EntityStrings.ProductCategory_Code;
            }
        }

        public string ProductCategory_Name {
            get {
                return Web.Resources.EntityStrings.ProductCategory_Name;
            }
        }

        public string RepairClaimItemDetail_RepairItemName {
            get {
                return Web.Resources.EntityStrings.RepairClaimItemDetail_RepairItemName;
            }
        }

        public string DealerVehicleStock_ProductCode {
            get {
                return Web.Resources.EntityStrings.DealerVehicleStock_ProductCode;
            }
        }

        public string DealerVehicleStock_VehicleWarehouseName {
            get {
                return Web.Resources.EntityStrings.DealerVehicleStock_VehicleWarehouseName;
            }
        }

        public string DealerVehicleStock_VIN {
            get {
                return Web.Resources.EntityStrings.DealerVehicleStock_VIN;
            }
        }

        public string DealerForecast_ForecastDate {
            get {
                return Web.Resources.EntityStrings.DealerForecast_ForecastDate;
            }
        }

        public string DealerForecastDetail_EngineCylinder {
            get {
                return Web.Resources.EntityStrings.DealerForecastDetail_EngineCylinder;
            }
        }

        public string DealerForecastDetail_RetailForecastAmount {
            get {
                return Web.Resources.EntityStrings.DealerForecastDetail_RetailForecastAmount;
            }
        }

        public string DealerForecastDetail_VehicleModel {
            get {
                return Web.Resources.EntityStrings.DealerForecastDetail_VehicleModel;
            }
        }

        public string DealerForecastDetail_WagonType {
            get {
                return Web.Resources.EntityStrings.DealerForecastDetail_WagonType;
            }
        }

        public string DealerForecastDetail_WholesaleForecastAmount {
            get {
                return Web.Resources.EntityStrings.DealerForecastDetail_WholesaleForecastAmount;
            }
        }

        public string DealerForecast_DealerCode {
            get {
                return Web.Resources.EntityStrings.DealerForecast_DealerCode;
            }
        }

        public string DealerForecast_DealerName {
            get {
                return Web.Resources.EntityStrings.DealerForecast_DealerName;
            }
        }

        public string VehiclePreAllocationRef_DealerCode {
            get {
                return Web.Resources.EntityStrings.VehiclePreAllocationRef_DealerCode;
            }
        }

        public string VehiclePreAllocationRef_DealerName {
            get {
                return Web.Resources.EntityStrings.VehiclePreAllocationRef_DealerName;
            }
        }

        public string VehiclePreAllocationRef_ExpectedShippingDate {
            get {
                return Web.Resources.EntityStrings.VehiclePreAllocationRef_ExpectedShippingDate;
            }
        }

        public string VehiclePreAllocationRef_OrderType {
            get {
                return Web.Resources.EntityStrings.VehiclePreAllocationRef_OrderType;
            }
        }

        public string VehiclePreAllocationRef_ProductCode {
            get {
                return Web.Resources.EntityStrings.VehiclePreAllocationRef_ProductCode;
            }
        }

        public string VehiclePreAllocationRef_ProductionPlanSON {
            get {
                return Web.Resources.EntityStrings.VehiclePreAllocationRef_ProductionPlanSON;
            }
        }

        public string VehiclePreAllocationRef_RolloutDate {
            get {
                return Web.Resources.EntityStrings.VehiclePreAllocationRef_RolloutDate;
            }
        }

        public string VehiclePreAllocationRef_VehicleOrderSON {
            get {
                return Web.Resources.EntityStrings.VehiclePreAllocationRef_VehicleOrderSON;
            }
        }

        public string VehicleWarehouse_Address {
            get {
                return Web.Resources.EntityStrings.VehicleWarehouse_Address;
            }
        }

        public string VehicleWarehouse_Code {
            get {
                return Web.Resources.EntityStrings.VehicleWarehouse_Code;
            }
        }

        public string VehicleWarehouse_ContactPerson {
            get {
                return Web.Resources.EntityStrings.VehicleWarehouse_ContactPerson;
            }
        }

        public string VehicleWarehouse_ContactPhone {
            get {
                return Web.Resources.EntityStrings.VehicleWarehouse_ContactPhone;
            }
        }

        public string VehicleWarehouse_Name {
            get {
                return Web.Resources.EntityStrings.VehicleWarehouse_Name;
            }
        }

        public string VehicleWarehouse_StoragePolicy {
            get {
                return Web.Resources.EntityStrings.VehicleWarehouse_StoragePolicy;
            }
        }

        public string VehicleWarehouse_Type {
            get {
                return Web.Resources.EntityStrings.VehicleWarehouse_Type;
            }
        }

        public string VehicleWarehouse_Email {
            get {
                return Web.Resources.EntityStrings.VehicleWarehouse_Email;
            }
        }

        public string Region_PostCode {
            get {
                return Web.Resources.EntityStrings.Region_PostCode;
            }
        }

        public string VehicleReturnOrderDetail_OriginalSalesPrice {
            get {
                return Web.Resources.EntityStrings.VehicleReturnOrderDetail_OriginalSalesPrice;
            }
        }

        public string VehicleReturnOrderDetail_ProductCategoryCode {
            get {
                return Web.Resources.EntityStrings.VehicleReturnOrderDetail_ProductCategoryCode;
            }
        }

        public string VehicleReturnOrderDetail_ProductCode {
            get {
                return Web.Resources.EntityStrings.VehicleReturnOrderDetail_ProductCode;
            }
        }

        public string VehicleReturnOrderDetail_ReturnPrice {
            get {
                return Web.Resources.EntityStrings.VehicleReturnOrderDetail_ReturnPrice;
            }
        }

        public string VehicleReturnOrder_Code {
            get {
                return Web.Resources.EntityStrings.VehicleReturnOrder_Code;
            }
        }

        public string VehicleReturnOrder_ReturnReason {
            get {
                return Web.Resources.EntityStrings.VehicleReturnOrder_ReturnReason;
            }
        }

        public string Product_ColorCode {
            get {
                return Web.Resources.EntityStrings.Product_ColorCode;
            }
        }

        public string DealerTransactionBill_DealerBuyerName {
            get {
                return Web.Resources.EntityStrings.DealerTransactionBill_DealerBuyerName;
            }
        }

        public string DealerTransactionBill_DealerSellerName {
            get {
                return Web.Resources.EntityStrings.DealerTransactionBill_DealerSellerName;
            }
        }

        public string DealerTransactionDetail_ProductCode {
            get {
                return Web.Resources.EntityStrings.DealerTransactionDetail_ProductCode;
            }
        }

        public string DealerTransactionDetail_Quantity {
            get {
                return Web.Resources.EntityStrings.DealerTransactionDetail_Quantity;
            }
        }

        public string VehicleModelAffiProduct_ProductName {
            get {
                return Web.Resources.EntityStrings.VehicleModelAffiProduct_ProductName;
            }
        }

        public string VehicleReturnOrder_DealerCode {
            get {
                return Web.Resources.EntityStrings.VehicleReturnOrder_DealerCode;
            }
        }

        public string VehicleReturnOrder_DealerName {
            get {
                return Web.Resources.EntityStrings.VehicleReturnOrder_DealerName;
            }
        }

        public string CredenceApplication_ApplyCondition {
            get {
                return Web.Resources.EntityStrings.CredenceApplication_ApplyCondition;
            }
        }

        public string CredenceApplication_CredenceLimit {
            get {
                return Web.Resources.EntityStrings.CredenceApplication_CredenceLimit;
            }
        }

        public string CredenceApplication_CredencePurpose {
            get {
                return Web.Resources.EntityStrings.CredenceApplication_CredencePurpose;
            }
        }

        public string CredenceApplication_ExpireDate {
            get {
                return Web.Resources.EntityStrings.CredenceApplication_ExpireDate;
            }
        }

        public string CredenceApplication_FinancingContractCode {
            get {
                return Web.Resources.EntityStrings.CredenceApplication_FinancingContractCode;
            }
        }

        public string CredenceApplication_ValidationDate {
            get {
                return Web.Resources.EntityStrings.CredenceApplication_ValidationDate;
            }
        }

        public string CredenceApplication_GuarantorCompanyName {
            get {
                return Web.Resources.EntityStrings.CredenceApplication_GuarantorCompanyName;
            }
        }

        public string CustomerAccountHisDetail_BusinessType {
            get {
                return Web.Resources.EntityStrings.CustomerAccountHisDetail_BusinessType;
            }
        }

        public string CustomerAccountHisDetail_ChangeAmount {
            get {
                return Web.Resources.EntityStrings.CustomerAccountHisDetail_ChangeAmount;
            }
        }

        public string CustomerAccountHisDetail_ProcessDate {
            get {
                return Web.Resources.EntityStrings.CustomerAccountHisDetail_ProcessDate;
            }
        }

        public string CustomerAccountHisDetail_SourceCode {
            get {
                return Web.Resources.EntityStrings.CustomerAccountHisDetail_SourceCode;
            }
        }

        public string CustomerAccountHisDetail_Summary {
            get {
                return Web.Resources.EntityStrings.CustomerAccountHisDetail_Summary;
            }
        }

        public string ChannelCapability_ChannelGrade {
            get {
                return Web.Resources.EntityStrings.ChannelCapability_ChannelGrade;
            }
        }

        public string ChannelCapability_Description {
            get {
                return Web.Resources.EntityStrings.ChannelCapability_Description;
            }
        }

        public string ChannelCapability_IsPart {
            get {
                return Web.Resources.EntityStrings.ChannelCapability_IsPart;
            }
        }

        public string ChannelCapability_IsSale {
            get {
                return Web.Resources.EntityStrings.ChannelCapability_IsSale;
            }
        }

        public string ChannelCapability_IsService {
            get {
                return Web.Resources.EntityStrings.ChannelCapability_IsService;
            }
        }

        public string ChannelCapability_IsSurvey {
            get {
                return Web.Resources.EntityStrings.ChannelCapability_IsSurvey;
            }
        }

        public string ChannelCapability_Remark {
            get {
                return Web.Resources.EntityStrings.ChannelCapability_Remark;
            }
        }

        public string VehicleRetailSuggestedPrice_ExecutionTime {
            get {
                return Web.Resources.EntityStrings.VehicleRetailSuggestedPrice_ExecutionTime;
            }
        }

        public string VehicleRetailSuggestedPrice_ProductCode {
            get {
                return Web.Resources.EntityStrings.VehicleRetailSuggestedPrice_ProductCode;
            }
        }

        public string VehicleRetailSuggestedPrice_ProductName {
            get {
                return Web.Resources.EntityStrings.VehicleRetailSuggestedPrice_ProductName;
            }
        }

        public string VINChangeRecord_NewVIN {
            get {
                return Web.Resources.EntityStrings.VINChangeRecord_NewVIN;
            }
        }

        public string VINChangeRecord_OriginalVIN {
            get {
                return Web.Resources.EntityStrings.VINChangeRecord_OriginalVIN;
            }
        }

        public string VINChangeRecord_RolloutDateAfterChange {
            get {
                return Web.Resources.EntityStrings.VINChangeRecord_RolloutDateAfterChange;
            }
        }

        public string VINChangeRecord_RolloutDateBeforeChange {
            get {
                return Web.Resources.EntityStrings.VINChangeRecord_RolloutDateBeforeChange;
            }
        }

        public string VehicleInformation_AnnoucementNumber {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_AnnoucementNumber;
            }
        }

        public string VehicleInformation_CertificateNumber {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_CertificateNumber;
            }
        }

        public string VehicleInformation_InvoiceDate {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_InvoiceDate;
            }
        }

        public string VehicleInformation_KeyCode {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_KeyCode;
            }
        }

        public string VehicleInformation_LicensePlateAcquisitionTime {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_LicensePlateAcquisitionTime;
            }
        }

        public string VehicleInformation_LicensePlateOwner {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_LicensePlateOwner;
            }
        }

        public string VehicleInformation_ProductCategoryCode {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_ProductCategoryCode;
            }
        }

        public string VehicleInformation_ProductCategoryName {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_ProductCategoryName;
            }
        }

        public string VehicleInformation_ProductionPlanSON {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_ProductionPlanSON;
            }
        }

        public string VehicleInformation_ProductName {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_ProductName;
            }
        }

        public string VehicleInformation_PublishedRolloutDate {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_PublishedRolloutDate;
            }
        }

        public string VehicleInformation_ResponsibleUnitName {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_ResponsibleUnitName;
            }
        }

        public string VehicleInformation_SalesDealerCode {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_SalesDealerCode;
            }
        }

        public string VehicleInformation_SalesDealerName {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_SalesDealerName;
            }
        }

        public string VehicleInformation_SalesInvoiceNumber {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_SalesInvoiceNumber;
            }
        }

        public string VehicleInformation_SON {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_SON;
            }
        }

        public string VehicleInformation_VehicleType {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_VehicleType;
            }
        }

        public string VINChangeRecord_AdjustmentTime {
            get {
                return Web.Resources.EntityStrings.VINChangeRecord_AdjustmentTime;
            }
        }

        public string PartsSalesOrderProcessDetail_WarehouseCode {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrderProcessDetail_WarehouseCode;
            }
        }

        public string VehicleWholesalePactPrice_DealerCode {
            get {
                return Web.Resources.EntityStrings.VehicleWholesalePactPrice_DealerCode;
            }
        }

        public string VehicleWholesalePactPrice_DealerName {
            get {
                return Web.Resources.EntityStrings.VehicleWholesalePactPrice_DealerName;
            }
        }

        public string VehicleWholesalePactPrice_ProductCode {
            get {
                return Web.Resources.EntityStrings.VehicleWholesalePactPrice_ProductCode;
            }
        }

        public string VehicleWholesalePactPrice_ProductName {
            get {
                return Web.Resources.EntityStrings.VehicleWholesalePactPrice_ProductName;
            }
        }

        public string VehicleWholesalePactPrice_TreatyPrice {
            get {
                return Web.Resources.EntityStrings.VehicleWholesalePactPrice_TreatyPrice;
            }
        }

        public string VehicleWholesalePactPrice_Code {
            get {
                return Web.Resources.EntityStrings.VehicleWholesalePactPrice_Code;
            }
        }

        public string VehicleWholesalePactPrice_VehicleSalesTypeName {
            get {
                return Web.Resources.EntityStrings.VehicleWholesalePactPrice_VehicleSalesTypeName;
            }
        }

        public string PartsSalesOrder_PartsSalesOrderTypeName {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrder_PartsSalesOrderTypeName;
            }
        }

        public string VehicleAvailableResource_IsAvailable {
            get {
                return Web.Resources.EntityStrings.VehicleAvailableResource_IsAvailable;
            }
        }

        public string VehicleAvailableResource_LockStatus {
            get {
                return Web.Resources.EntityStrings.VehicleAvailableResource_LockStatus;
            }
        }

        public string VehicleWholesalePrice_BranchName {
            get {
                return Web.Resources.EntityStrings.VehicleWholesalePrice_BranchName;
            }
        }

        public string VehicleWholesalePrice_Code {
            get {
                return Web.Resources.EntityStrings.VehicleWholesalePrice_Code;
            }
        }

        public string VehicleWholesalePrice_ProductCode {
            get {
                return Web.Resources.EntityStrings.VehicleWholesalePrice_ProductCode;
            }
        }

        public string VehicleWholesalePrice_ProductName {
            get {
                return Web.Resources.EntityStrings.VehicleWholesalePrice_ProductName;
            }
        }

        public string VehicleWholesalePrice_VehicleSalesTypeName {
            get {
                return Web.Resources.EntityStrings.VehicleWholesalePrice_VehicleSalesTypeName;
            }
        }

        public string VehRetailPriceChangeApp_BranchCode {
            get {
                return Web.Resources.EntityStrings.VehRetailPriceChangeApp_BranchCode;
            }
        }

        public string VehRetailPriceChangeApp_BranchName {
            get {
                return Web.Resources.EntityStrings.VehRetailPriceChangeApp_BranchName;
            }
        }

        public string VehRetailPriceChangeApp_Code {
            get {
                return Web.Resources.EntityStrings.VehRetailPriceChangeApp_Code;
            }
        }

        public string VehRetailPriceChangeApp_ExecutedImmediately {
            get {
                return Web.Resources.EntityStrings.VehRetailPriceChangeApp_ExecutedImmediately;
            }
        }

        public string VehRetailPriceChangeApp_ExecutionDate {
            get {
                return Web.Resources.EntityStrings.VehRetailPriceChangeApp_ExecutionDate;
            }
        }

        public string AccountGroup_Code {
            get {
                return Web.Resources.EntityStrings.AccountGroup_Code;
            }
        }

        public string AccountGroup_Name {
            get {
                return Web.Resources.EntityStrings.AccountGroup_Name;
            }
        }

        public string VehRetailPriceChangeAppDetail_PriceAfterChange {
            get {
                return Web.Resources.EntityStrings.VehRetailPriceChangeAppDetail_PriceAfterChange;
            }
        }

        public string VehRetailPriceChangeAppDetail_PriceBeforeChange {
            get {
                return Web.Resources.EntityStrings.VehRetailPriceChangeAppDetail_PriceBeforeChange;
            }
        }

        public string VehRetailPriceChangeAppDetail_ProductCode {
            get {
                return Web.Resources.EntityStrings.VehRetailPriceChangeAppDetail_ProductCode;
            }
        }

        public string VehRetailPriceChangeAppDetail_ProductName {
            get {
                return Web.Resources.EntityStrings.VehRetailPriceChangeAppDetail_ProductName;
            }
        }

        public string VehicleShippingOrder_DealerCode {
            get {
                return Web.Resources.EntityStrings.VehicleShippingOrder_DealerCode;
            }
        }

        public string VehicleShippingDetail_ProductCategoryCode {
            get {
                return Web.Resources.EntityStrings.VehicleShippingDetail_ProductCategoryCode;
            }
        }

        public string VehicleShippingDetail_ProductCode {
            get {
                return Web.Resources.EntityStrings.VehicleShippingDetail_ProductCode;
            }
        }

        public string VehicleShippingDetail_ShippingDate {
            get {
                return Web.Resources.EntityStrings.VehicleShippingDetail_ShippingDate;
            }
        }

        public string VehicleShippingDetail_VehicleReceptionStatus {
            get {
                return Web.Resources.EntityStrings.VehicleShippingDetail_VehicleReceptionStatus;
            }
        }

        public string VehicleShippingOrder_DealerName {
            get {
                return Web.Resources.EntityStrings.VehicleShippingOrder_DealerName;
            }
        }

        public string VehicleShippingOrder_ShippingMethod {
            get {
                return Web.Resources.EntityStrings.VehicleShippingOrder_ShippingMethod;
            }
        }

        public string VehicleShippingOrder_VehicleShippingDetails {
            get {
                return Web.Resources.EntityStrings.VehicleShippingOrder_VehicleShippingDetails;
            }
        }

        public string VehicleReturnOrderDetail_VIN {
            get {
                return Web.Resources.EntityStrings.VehicleReturnOrderDetail_VIN;
            }
        }

        public string VehPifaPriceChangeAppDetail_PriceAfterChange {
            get {
                return Web.Resources.EntityStrings.VehPifaPriceChangeAppDetail_PriceAfterChange;
            }
        }

        public string VehPifaPriceChangeAppDetail_PriceBeforeChange {
            get {
                return Web.Resources.EntityStrings.VehPifaPriceChangeAppDetail_PriceBeforeChange;
            }
        }

        public string VehPifaPriceChangeAppDetail_ProductCode {
            get {
                return Web.Resources.EntityStrings.VehPifaPriceChangeAppDetail_ProductCode;
            }
        }

        public string VehPifaPriceChangeAppDetail_ProductName {
            get {
                return Web.Resources.EntityStrings.VehPifaPriceChangeAppDetail_ProductName;
            }
        }

        public string VehPifaPriceChangeApp_BranchCode {
            get {
                return Web.Resources.EntityStrings.VehPifaPriceChangeApp_BranchCode;
            }
        }

        public string VehPifaPriceChangeApp_BranchName {
            get {
                return Web.Resources.EntityStrings.VehPifaPriceChangeApp_BranchName;
            }
        }

        public string VehPifaPriceChangeApp_Code {
            get {
                return Web.Resources.EntityStrings.VehPifaPriceChangeApp_Code;
            }
        }

        public string VehPifaPriceChangeApp_ExecutionDate {
            get {
                return Web.Resources.EntityStrings.VehPifaPriceChangeApp_ExecutionDate;
            }
        }

        public string VehPifaPriceChangeApp_VehPifaPriceChangeAppDetails {
            get {
                return Web.Resources.EntityStrings.VehPifaPriceChangeApp_VehPifaPriceChangeAppDetails;
            }
        }

        public string VehPifaPriceChangeApp_VehicleSalesTypeName {
            get {
                return Web.Resources.EntityStrings.VehPifaPriceChangeApp_VehicleSalesTypeName;
            }
        }

        public string VehPifaPriceChangeApp_ExecutedImmediately {
            get {
                return Web.Resources.EntityStrings.VehPifaPriceChangeApp_ExecutedImmediately;
            }
        }

        public string CustomerOpenAccountApp_Operator {
            get {
                return Web.Resources.EntityStrings.CustomerOpenAccountApp_Operator;
            }
        }

        public string VehicleShippingOrderChangeRec_NewVIN {
            get {
                return Web.Resources.EntityStrings.VehicleShippingOrderChangeRec_NewVIN;
            }
        }

        public string VehicleShippingOrderChangeRec_OriginalVIN {
            get {
                return Web.Resources.EntityStrings.VehicleShippingOrderChangeRec_OriginalVIN;
            }
        }

        public string PartsSalesReturnBill_SalesUnitName {
            get {
                return Web.Resources.EntityStrings.PartsSalesReturnBill_SalesUnitName;
            }
        }

        public string PartsSalesReturnBill_PartsSalesOrderCode {
            get {
                return Web.Resources.EntityStrings.PartsSalesReturnBill_PartsSalesOrderCode;
            }
        }

        public string Warehouse_Type {
            get {
                return Web.Resources.EntityStrings.Warehouse_Type;
            }
        }

        public string PartsSalesReturnBill_BranchName {
            get {
                return Web.Resources.EntityStrings.PartsSalesReturnBill_BranchName;
            }
        }

        public string VehicleDLRStartSecurityDeposit_DealerCode {
            get {
                return Web.Resources.EntityStrings.VehicleDLRStartSecurityDeposit_DealerCode;
            }
        }

        public string VehicleDLRStartSecurityDeposit_Debtor {
            get {
                return Web.Resources.EntityStrings.VehicleDLRStartSecurityDeposit_Debtor;
            }
        }

        public string VehicleDLRStartSecurityDeposit_Lender {
            get {
                return Web.Resources.EntityStrings.VehicleDLRStartSecurityDeposit_Lender;
            }
        }

        public string VehicleDealerCreditLimitApp_Code {
            get {
                return Web.Resources.EntityStrings.VehicleDealerCreditLimitApp_Code;
            }
        }

        public string VehicleDealerCreditLimitApp_CredenceLimit {
            get {
                return Web.Resources.EntityStrings.VehicleDealerCreditLimitApp_CredenceLimit;
            }
        }

        public string VehicleDealerCreditLimitApp_CustomerCompanyCode {
            get {
                return Web.Resources.EntityStrings.VehicleDealerCreditLimitApp_CustomerCompanyCode;
            }
        }

        public string VehicleDealerCreditLimitApp_CustomerCompanyName {
            get {
                return Web.Resources.EntityStrings.VehicleDealerCreditLimitApp_CustomerCompanyName;
            }
        }

        public string VehicleDealerCreditLimitApp_ExpireDate {
            get {
                return Web.Resources.EntityStrings.VehicleDealerCreditLimitApp_ExpireDate;
            }
        }

        public string VehicleBankAccount_BankAccountNumber {
            get {
                return Web.Resources.EntityStrings.VehicleBankAccount_BankAccountNumber;
            }
        }

        public string VehicleBankAccount_BankName {
            get {
                return Web.Resources.EntityStrings.VehicleBankAccount_BankName;
            }
        }

        public string VehicleBankAccount_CustomerCompanyCode {
            get {
                return Web.Resources.EntityStrings.VehicleBankAccount_CustomerCompanyCode;
            }
        }

        public string VehicleBankAccount_CustomerCompanyName {
            get {
                return Web.Resources.EntityStrings.VehicleBankAccount_CustomerCompanyName;
            }
        }

        public string VehicleBankAccount_Purpose {
            get {
                return Web.Resources.EntityStrings.VehicleBankAccount_Purpose;
            }
        }

        public string VehiclePaymentBill_Amount {
            get {
                return Web.Resources.EntityStrings.VehiclePaymentBill_Amount;
            }
        }

        public string VehiclePaymentBill_CertificateSummary {
            get {
                return Web.Resources.EntityStrings.VehiclePaymentBill_CertificateSummary;
            }
        }

        public string VehiclePaymentBill_PaymentCertificateNumber {
            get {
                return Web.Resources.EntityStrings.VehiclePaymentBill_PaymentCertificateNumber;
            }
        }

        public string VehicleCustAccountHisDetail_BusinessType {
            get {
                return Web.Resources.EntityStrings.VehicleCustAccountHisDetail_BusinessType;
            }
        }

        public string VehicleCustAccountHisDetail_ChangeAmount {
            get {
                return Web.Resources.EntityStrings.VehicleCustAccountHisDetail_ChangeAmount;
            }
        }

        public string VehicleCustAccountHisDetail_ProcessDate {
            get {
                return Web.Resources.EntityStrings.VehicleCustAccountHisDetail_ProcessDate;
            }
        }

        public string VehicleCustAccountHisDetail_SourceCode {
            get {
                return Web.Resources.EntityStrings.VehicleCustAccountHisDetail_SourceCode;
            }
        }

        public string VehicleCustomerAccount_CustomerCredenceAmount {
            get {
                return Web.Resources.EntityStrings.VehicleCustomerAccount_CustomerCredenceAmount;
            }
        }

        public string VehicleCustomerAccount_PendingAmount {
            get {
                return Web.Resources.EntityStrings.VehicleCustomerAccount_PendingAmount;
            }
        }

        public string VehiclePaymentMethod_Code {
            get {
                return Web.Resources.EntityStrings.VehiclePaymentMethod_Code;
            }
        }

        public string VehiclePaymentMethod_Name {
            get {
                return Web.Resources.EntityStrings.VehiclePaymentMethod_Name;
            }
        }

        public string VehiclePaymentBill_BillNumber {
            get {
                return Web.Resources.EntityStrings.VehiclePaymentBill_BillNumber;
            }
        }

        public string VehiclePaymentBill_Code {
            get {
                return Web.Resources.EntityStrings.VehiclePaymentBill_Code;
            }
        }

        public string VehiclePaymentBill_TimeOfIncomingPayment {
            get {
                return Web.Resources.EntityStrings.VehiclePaymentBill_TimeOfIncomingPayment;
            }
        }

        public string VehiclePaymentBill_TimeOfRecord {
            get {
                return Web.Resources.EntityStrings.VehiclePaymentBill_TimeOfRecord;
            }
        }

        public string VehicleDLRAccountFreezeBill_AccountAvailability {
            get {
                return Web.Resources.EntityStrings.VehicleDLRAccountFreezeBill_AccountAvailability;
            }
        }

        public string VehicleDLRAccountFreezeBill_Code {
            get {
                return Web.Resources.EntityStrings.VehicleDLRAccountFreezeBill_Code;
            }
        }

        public string VehicleAccountReceivableBill_BillReceptionDate {
            get {
                return Web.Resources.EntityStrings.VehicleAccountReceivableBill_BillReceptionDate;
            }
        }

        public string VehicleAccountReceivableBill_CustomerCompanyCode {
            get {
                return Web.Resources.EntityStrings.VehicleAccountReceivableBill_CustomerCompanyCode;
            }
        }

        public string VehicleAccountReceivableBill_CustomerCompanyName {
            get {
                return Web.Resources.EntityStrings.VehicleAccountReceivableBill_CustomerCompanyName;
            }
        }

        public string VehicleAccountReceivableBill_DaysOfInterest {
            get {
                return Web.Resources.EntityStrings.VehicleAccountReceivableBill_DaysOfInterest;
            }
        }

        public string VehicleAccountReceivableBill_DueDate {
            get {
                return Web.Resources.EntityStrings.VehicleAccountReceivableBill_DueDate;
            }
        }

        public string VehicleAccountReceivableBill_InterestFreePeriod {
            get {
                return Web.Resources.EntityStrings.VehicleAccountReceivableBill_InterestFreePeriod;
            }
        }

        public string VehicleAccountReceivableBill_InterestRefundAmount {
            get {
                return Web.Resources.EntityStrings.VehicleAccountReceivableBill_InterestRefundAmount;
            }
        }

        public string VehicleAccountReceivableBill_IssueBank {
            get {
                return Web.Resources.EntityStrings.VehicleAccountReceivableBill_IssueBank;
            }
        }

        public string VehicleAccountReceivableBill_IssueCompany {
            get {
                return Web.Resources.EntityStrings.VehicleAccountReceivableBill_IssueCompany;
            }
        }

        public string VehicleAccountReceivableBill_IssueCompanyAccount {
            get {
                return Web.Resources.EntityStrings.VehicleAccountReceivableBill_IssueCompanyAccount;
            }
        }

        public string VehicleAccountReceivableBill_IssueDate {
            get {
                return Web.Resources.EntityStrings.VehicleAccountReceivableBill_IssueDate;
            }
        }

        public string VehicleAccountReceivableBill_MaturityInterest {
            get {
                return Web.Resources.EntityStrings.VehicleAccountReceivableBill_MaturityInterest;
            }
        }

        public string VehicleAccountReceivableBill_MaturityInterestRate {
            get {
                return Web.Resources.EntityStrings.VehicleAccountReceivableBill_MaturityInterestRate;
            }
        }

        public string VehicleAccountReceivableBill_MoneyOrderNumber {
            get {
                return Web.Resources.EntityStrings.VehicleAccountReceivableBill_MoneyOrderNumber;
            }
        }

        public string VehicleAccountReceivableBill_NoteDenomination {
            get {
                return Web.Resources.EntityStrings.VehicleAccountReceivableBill_NoteDenomination;
            }
        }

        public string VehicleAccountReceivableBill_PaymentDate {
            get {
                return Web.Resources.EntityStrings.VehicleAccountReceivableBill_PaymentDate;
            }
        }

        public string VehicleAccountReceivableBill_PaymentOperatorName {
            get {
                return Web.Resources.EntityStrings.VehicleAccountReceivableBill_PaymentOperatorName;
            }
        }

        public string VehicleAccountReceivableBill_PaymentReceptionBank {
            get {
                return Web.Resources.EntityStrings.VehicleAccountReceivableBill_PaymentReceptionBank;
            }
        }

        public string VehicleAccountReceivableBill_PaymentReceptionBankAccount {
            get {
                return Web.Resources.EntityStrings.VehicleAccountReceivableBill_PaymentReceptionBankAccount;
            }
        }

        public string VehicleAccountReceivableBill_PaymentReceptionDate {
            get {
                return Web.Resources.EntityStrings.VehicleAccountReceivableBill_PaymentReceptionDate;
            }
        }

        public string VehicleAccountReceivableBill_PaymentReceptionOperatorrName {
            get {
                return Web.Resources.EntityStrings.VehicleAccountReceivableBill_PaymentReceptionOperatorrName;
            }
        }

        public string VehicleAccountReceivableBill_TransferOutCompany {
            get {
                return Web.Resources.EntityStrings.VehicleAccountReceivableBill_TransferOutCompany;
            }
        }

        public string VehicleAccountReceivableBill_TransferOutOperatorrName {
            get {
                return Web.Resources.EntityStrings.VehicleAccountReceivableBill_TransferOutOperatorrName;
            }
        }

        public string VehicleAccountReceivableBill_TransferOutType {
            get {
                return Web.Resources.EntityStrings.VehicleAccountReceivableBill_TransferOutType;
            }
        }

        public string VehicleAccountReceivableBill_Type {
            get {
                return Web.Resources.EntityStrings.VehicleAccountReceivableBill_Type;
            }
        }

        public string VehicleAccountReceivableBill_TransferOutTime {
            get {
                return Web.Resources.EntityStrings.VehicleAccountReceivableBill_TransferOutTime;
            }
        }

        public string VehicleDLRStartSecurityDeposit_CurrentBalance {
            get {
                return Web.Resources.EntityStrings.VehicleDLRStartSecurityDeposit_CurrentBalance;
            }
        }

        public string VehicleFundsType_Code {
            get {
                return Web.Resources.EntityStrings.VehicleFundsType_Code;
            }
        }

        public string VehicleFundsType_Name {
            get {
                return Web.Resources.EntityStrings.VehicleFundsType_Name;
            }
        }

        public string VehiclePreAllocationRef_MonthOfOrder {
            get {
                return Web.Resources.EntityStrings.VehiclePreAllocationRef_MonthOfOrder;
            }
        }

        public string VehiclePreAllocationRef_ProductCategoryCode {
            get {
                return Web.Resources.EntityStrings.VehiclePreAllocationRef_ProductCategoryCode;
            }
        }

        public string VehiclePreAllocationRef_YearOfOrder {
            get {
                return Web.Resources.EntityStrings.VehiclePreAllocationRef_YearOfOrder;
            }
        }

        public string ProductionPlan_RolloutDate {
            get {
                return Web.Resources.EntityStrings.ProductionPlan_RolloutDate;
            }
        }

        public string PartsSalesOrderProcess_CurrentFulfilledAmount {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrderProcess_CurrentFulfilledAmount;
            }
        }

        public string VehicleSery_ProductCategoryCode {
            get {
                return Web.Resources.EntityStrings.VehicleSery_ProductCategoryCode;
            }
        }

        public string UsedPartsReturnDetail_ClaimBillCode {
            get {
                return Web.Resources.EntityStrings.UsedPartsReturnDetail_ClaimBillCode;
            }
        }

        public string UsedPartsReturnDetail_ConfirmedAmount {
            get {
                return Web.Resources.EntityStrings.UsedPartsReturnDetail_ConfirmedAmount;
            }
        }

        public string UsedPartsReturnDetail_OutboundAmount {
            get {
                return Web.Resources.EntityStrings.UsedPartsReturnDetail_OutboundAmount;
            }
        }

        public string UsedPartsReturnDetail_PlannedAmount {
            get {
                return Web.Resources.EntityStrings.UsedPartsReturnDetail_PlannedAmount;
            }
        }

        public string UsedPartsReturnDetail_UsedPartsBarCode {
            get {
                return Web.Resources.EntityStrings.UsedPartsReturnDetail_UsedPartsBarCode;
            }
        }

        public string UsedPartsReturnDetail_UsedPartsBatchNumber {
            get {
                return Web.Resources.EntityStrings.UsedPartsReturnDetail_UsedPartsBatchNumber;
            }
        }

        public string UsedPartsReturnDetail_UsedPartsSerialNumber {
            get {
                return Web.Resources.EntityStrings.UsedPartsReturnDetail_UsedPartsSerialNumber;
            }
        }

        public string UsedPartsReturnOrder_Code {
            get {
                return Web.Resources.EntityStrings.UsedPartsReturnOrder_Code;
            }
        }

        public string UsedPartsReturnOrder_Operator {
            get {
                return Web.Resources.EntityStrings.UsedPartsReturnOrder_Operator;
            }
        }

        public string UsedPartsReturnOrder_OutboundWarehouseCode {
            get {
                return Web.Resources.EntityStrings.UsedPartsReturnOrder_OutboundWarehouseCode;
            }
        }

        public string UsedPartsReturnOrder_OutboundWarehouseName {
            get {
                return Web.Resources.EntityStrings.UsedPartsReturnOrder_OutboundWarehouseName;
            }
        }

        public string UsedPartsReturnOrder_ReturnOfficeCode {
            get {
                return Web.Resources.EntityStrings.UsedPartsReturnOrder_ReturnOfficeCode;
            }
        }

        public string UsedPartsReturnOrder_ReturnOfficeName {
            get {
                return Web.Resources.EntityStrings.UsedPartsReturnOrder_ReturnOfficeName;
            }
        }

        public string UsedPartsReturnOrder_ReturnType {
            get {
                return Web.Resources.EntityStrings.UsedPartsReturnOrder_ReturnType;
            }
        }

        public string UsedPartsReturnOrder_TotalAmount {
            get {
                return Web.Resources.EntityStrings.UsedPartsReturnOrder_TotalAmount;
            }
        }

        public string UsedPartsReturnOrder_TotalQuantity {
            get {
                return Web.Resources.EntityStrings.UsedPartsReturnOrder_TotalQuantity;
            }
        }

        public string UsedPartsReturnOrder_UsedPartsReturnDetails {
            get {
                return Web.Resources.EntityStrings.UsedPartsReturnOrder_UsedPartsReturnDetails;
            }
        }

        public string PartsRequisitionSettleBill_TotalSettlementAmount {
            get {
                return Web.Resources.EntityStrings.PartsRequisitionSettleBill_TotalSettlementAmount;
            }
        }

        public string PartsRequisitionSettleDetail_Quantity {
            get {
                return Web.Resources.EntityStrings.PartsRequisitionSettleDetail_Quantity;
            }
        }

        public string PartsRequisitionSettleDetail_SettlementPrice {
            get {
                return Web.Resources.EntityStrings.PartsRequisitionSettleDetail_SettlementPrice;
            }
        }

        public string PartsRequisitionSettleDetail_SparePartCode {
            get {
                return Web.Resources.EntityStrings.PartsRequisitionSettleDetail_SparePartCode;
            }
        }

        public string PartsRequisitionSettleDetail_SparePartName {
            get {
                return Web.Resources.EntityStrings.PartsRequisitionSettleDetail_SparePartName;
            }
        }

        public string PartsRequisitionSettleRef_SourceCode {
            get {
                return Web.Resources.EntityStrings.PartsRequisitionSettleRef_SourceCode;
            }
        }

        public string PartsRequisitionSettleRef_SourceType {
            get {
                return Web.Resources.EntityStrings.PartsRequisitionSettleRef_SourceType;
            }
        }

        public string PartsRequisitionSettleRef_SettlementAmount {
            get {
                return Web.Resources.EntityStrings.PartsRequisitionSettleRef_SettlementAmount;
            }
        }

        public string RegionConfuguration_Code {
            get {
                return Web.Resources.EntityStrings.RegionConfuguration_Code;
            }
        }

        public string RegionConfuguration_Name {
            get {
                return Web.Resources.EntityStrings.RegionConfuguration_Name;
            }
        }

        public string KeyAccount_CompanyScale {
            get {
                return Web.Resources.EntityStrings.KeyAccount_CompanyScale;
            }
        }

        public string KeyAccount_DealerCode {
            get {
                return Web.Resources.EntityStrings.KeyAccount_DealerCode;
            }
        }

        public string KeyAccount_DealerName {
            get {
                return Web.Resources.EntityStrings.KeyAccount_DealerName;
            }
        }

        public string KeyAccount_IsConfirmed {
            get {
                return Web.Resources.EntityStrings.KeyAccount_IsConfirmed;
            }
        }

        public string KeyAccount_KeyAccountCode {
            get {
                return Web.Resources.EntityStrings.KeyAccount_KeyAccountCode;
            }
        }

        public string Customer_CustomerType {
            get {
                return Web.Resources.EntityStrings.Customer_CustomerType;
            }
        }

        public string WholesaleApproval_Code {
            get {
                return Web.Resources.EntityStrings.WholesaleApproval_Code;
            }
        }

        public string AuditHierarchySetting_BusinessDomain {
            get {
                return Web.Resources.EntityStrings.AuditHierarchySetting_BusinessDomain;
            }
        }

        public string AuditHierarchySetting_PositionCode {
            get {
                return Web.Resources.EntityStrings.AuditHierarchySetting_PositionCode;
            }
        }

        public string AuditHierarchySetting_PositionName {
            get {
                return Web.Resources.EntityStrings.AuditHierarchySetting_PositionName;
            }
        }

        public string AuditHierarchySetting_AuditRoleAffiPerconnels {
            get {
                return Web.Resources.EntityStrings.AuditHierarchySetting_AuditRoleAffiPerconnels;
            }
        }

        public string AuditHierarchySetting_PostAuditStatus {
            get {
                return Web.Resources.EntityStrings.AuditHierarchySetting_PostAuditStatus;
            }
        }

        public string PostAuditStatu_Name {
            get {
                return Web.Resources.EntityStrings.PostAuditStatu_Name;
            }
        }

        public string VehicleOrderPlanSummary_DealerCode {
            get {
                return Web.Resources.EntityStrings.VehicleOrderPlanSummary_DealerCode;
            }
        }

        public string VehicleOrderPlanSummary_DealerName {
            get {
                return Web.Resources.EntityStrings.VehicleOrderPlanSummary_DealerName;
            }
        }

        public string VehicleOrderPlanSummary_MonthOfOrder {
            get {
                return Web.Resources.EntityStrings.VehicleOrderPlanSummary_MonthOfOrder;
            }
        }

        public string VehicleOrderPlanSummary_ProductCode {
            get {
                return Web.Resources.EntityStrings.VehicleOrderPlanSummary_ProductCode;
            }
        }

        public string VehicleOrderPlanSummary_UploadType {
            get {
                return Web.Resources.EntityStrings.VehicleOrderPlanSummary_UploadType;
            }
        }

        public string VehicleOrderPlanSummary_YearOfOrder {
            get {
                return Web.Resources.EntityStrings.VehicleOrderPlanSummary_YearOfOrder;
            }
        }

        public string WholesaleRewardApp_WholesaleApprovalCode {
            get {
                return Web.Resources.EntityStrings.WholesaleRewardApp_WholesaleApprovalCode;
            }
        }

        public string WholesaleRewardAppDetail_AppliedRewardAmount {
            get {
                return Web.Resources.EntityStrings.WholesaleRewardAppDetail_AppliedRewardAmount;
            }
        }

        public string WholesaleRewardAppDetail_ApprovedRewardAmount {
            get {
                return Web.Resources.EntityStrings.WholesaleRewardAppDetail_ApprovedRewardAmount;
            }
        }

        public string WholesaleRewardAppDetail_InvoiceCode {
            get {
                return Web.Resources.EntityStrings.WholesaleRewardAppDetail_InvoiceCode;
            }
        }

        public string WholesaleRewardAppDetail_InvoiceTitle {
            get {
                return Web.Resources.EntityStrings.WholesaleRewardAppDetail_InvoiceTitle;
            }
        }

        public string WholesaleRewardAppDetail_MarketGuidePrice {
            get {
                return Web.Resources.EntityStrings.WholesaleRewardAppDetail_MarketGuidePrice;
            }
        }

        public string WholesaleRewardAppDetail_ProductCategoryCode {
            get {
                return Web.Resources.EntityStrings.WholesaleRewardAppDetail_ProductCategoryCode;
            }
        }

        public string WholesaleRewardAppDetail_TransactionPrice {
            get {
                return Web.Resources.EntityStrings.WholesaleRewardAppDetail_TransactionPrice;
            }
        }

        public string WholesaleRewardAppDetail_VehicleDeliveryTime {
            get {
                return Web.Resources.EntityStrings.WholesaleRewardAppDetail_VehicleDeliveryTime;
            }
        }

        public string WholesaleRewardAppDetail_VIN {
            get {
                return Web.Resources.EntityStrings.WholesaleRewardAppDetail_VIN;
            }
        }

        public string WholesaleRewardApp_WholesaleRewardAppDetails {
            get {
                return Web.Resources.EntityStrings.WholesaleRewardApp_WholesaleRewardAppDetails;
            }
        }

        public string BusinessObject_Name {
            get {
                return Web.Resources.EntityStrings.BusinessObject_Name;
            }
        }

        public string MultiLevelAuditConfig_Code {
            get {
                return Web.Resources.EntityStrings.MultiLevelAuditConfig_Code;
            }
        }

        public string MultiLevelAuditConfig_InitialStatus {
            get {
                return Web.Resources.EntityStrings.MultiLevelAuditConfig_InitialStatus;
            }
        }

        public string MultiLevelAuditConfig_Name {
            get {
                return Web.Resources.EntityStrings.MultiLevelAuditConfig_Name;
            }
        }

        public string KeyAccount_CompanyName {
            get {
                return Web.Resources.EntityStrings.KeyAccount_CompanyName;
            }
        }

        public string KeyAccount_ContactCellPhone {
            get {
                return Web.Resources.EntityStrings.KeyAccount_ContactCellPhone;
            }
        }

        public string KeyAccount_ContactFax {
            get {
                return Web.Resources.EntityStrings.KeyAccount_ContactFax;
            }
        }

        public string KeyAccount_ContactPerson {
            get {
                return Web.Resources.EntityStrings.KeyAccount_ContactPerson;
            }
        }

        public string KeyAccount_ContactPhone {
            get {
                return Web.Resources.EntityStrings.KeyAccount_ContactPhone;
            }
        }

        public string WholesaleRewardApp_DealerCode {
            get {
                return Web.Resources.EntityStrings.WholesaleRewardApp_DealerCode;
            }
        }

        public string WholesaleRewardApp_DealerName {
            get {
                return Web.Resources.EntityStrings.WholesaleRewardApp_DealerName;
            }
        }

        public string Customer_Code {
            get {
                return Web.Resources.EntityStrings.Customer_Code;
            }
        }

        public string Customer_Gender {
            get {
                return Web.Resources.EntityStrings.Customer_Gender;
            }
        }

        public string Customer_Name {
            get {
                return Web.Resources.EntityStrings.Customer_Name;
            }
        }

        public string WholesaleApprovalDetail_AgentDeliveryDealerContact {
            get {
                return Web.Resources.EntityStrings.WholesaleApprovalDetail_AgentDeliveryDealerContact;
            }
        }

        public string WholesaleApprovalDetail_AgentDeliveryDealerName {
            get {
                return Web.Resources.EntityStrings.WholesaleApprovalDetail_AgentDeliveryDealerName;
            }
        }

        public string WholesaleApprovalDetail_Color {
            get {
                return Web.Resources.EntityStrings.WholesaleApprovalDetail_Color;
            }
        }

        public string WholesaleApprovalDetail_DeliveryAgentPhoneNumber {
            get {
                return Web.Resources.EntityStrings.WholesaleApprovalDetail_DeliveryAgentPhoneNumber;
            }
        }

        public string WholesaleApprovalDetail_DiscountAmount {
            get {
                return Web.Resources.EntityStrings.WholesaleApprovalDetail_DiscountAmount;
            }
        }

        public string WholesaleApprovalDetail_IfAgentDeliverDealerNeeded {
            get {
                return Web.Resources.EntityStrings.WholesaleApprovalDetail_IfAgentDeliverDealerNeeded;
            }
        }

        public string WholesaleApprovalDetail_ProductCategoryCode {
            get {
                return Web.Resources.EntityStrings.WholesaleApprovalDetail_ProductCategoryCode;
            }
        }

        public string WholesaleApprovalDetail_ProductCode {
            get {
                return Web.Resources.EntityStrings.WholesaleApprovalDetail_ProductCode;
            }
        }

        public string WholesaleApprovalDetail_Quantity {
            get {
                return Web.Resources.EntityStrings.WholesaleApprovalDetail_Quantity;
            }
        }

        public string WholesaleApprovalDetail_RetailGuidePrice {
            get {
                return Web.Resources.EntityStrings.WholesaleApprovalDetail_RetailGuidePrice;
            }
        }

        public string WholesaleApprovalDetail_SupplyDate {
            get {
                return Web.Resources.EntityStrings.WholesaleApprovalDetail_SupplyDate;
            }
        }

        public string WholesaleApprovalDetail_WholesalePrice {
            get {
                return Web.Resources.EntityStrings.WholesaleApprovalDetail_WholesalePrice;
            }
        }

        public string WholesaleApprovalDetail_DiscountRate {
            get {
                return Web.Resources.EntityStrings.WholesaleApprovalDetail_DiscountRate;
            }
        }

        public string WholesaleApproval_WholesaleApprovalDetails {
            get {
                return Web.Resources.EntityStrings.WholesaleApproval_WholesaleApprovalDetails;
            }
        }

        public string PartsClaimPrice_WarrantyPolicyCategory {
            get {
                return Web.Resources.EntityStrings.PartsClaimPrice_WarrantyPolicyCategory;
            }
        }

        public string PartsTransferOrder_PartsTransferOrderDetails {
            get {
                return Web.Resources.EntityStrings.PartsTransferOrder_PartsTransferOrderDetails;
            }
        }

        public string WholesaleApproval_DealerCode {
            get {
                return Web.Resources.EntityStrings.WholesaleApproval_DealerCode;
            }
        }

        public string WholesaleApproval_DealerName {
            get {
                return Web.Resources.EntityStrings.WholesaleApproval_DealerName;
            }
        }

        public string Customer_Address {
            get {
                return Web.Resources.EntityStrings.Customer_Address;
            }
        }

        public string Customer_CompanyName {
            get {
                return Web.Resources.EntityStrings.Customer_CompanyName;
            }
        }

        public string Customer_Email {
            get {
                return Web.Resources.EntityStrings.Customer_Email;
            }
        }

        public string Customer_Fax {
            get {
                return Web.Resources.EntityStrings.Customer_Fax;
            }
        }

        public string Customer_PostCode {
            get {
                return Web.Resources.EntityStrings.Customer_PostCode;
            }
        }

        public string KeyAccount_ContactJobPosition {
            get {
                return Web.Resources.EntityStrings.KeyAccount_ContactJobPosition;
            }
        }

        public string Warehouse_StorageCompanyType {
            get {
                return Web.Resources.EntityStrings.Warehouse_StorageCompanyType;
            }
        }

        public string VehicleInformation_Mileage {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_Mileage;
            }
        }

        public string VehicleInformation_SellPrice {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_SellPrice;
            }
        }

        public string MultiLevelAuditConfigDetail_IsSpecialOperation {
            get {
                return Web.Resources.EntityStrings.MultiLevelAuditConfigDetail_IsSpecialOperation;
            }
        }

        public string KeyAccount_CustomerProperty {
            get {
                return Web.Resources.EntityStrings.KeyAccount_CustomerProperty;
            }
        }

        public string KeyAccount_IfTenderingOrCentralPurchase {
            get {
                return Web.Resources.EntityStrings.KeyAccount_IfTenderingOrCentralPurchase;
            }
        }

        public string WholesaleApproval_DealerKeyAccountResponsible {
            get {
                return Web.Resources.EntityStrings.WholesaleApproval_DealerKeyAccountResponsible;
            }
        }

        public string WholesaleRewardApp_CustomerDetailCategory {
            get {
                return Web.Resources.EntityStrings.WholesaleRewardApp_CustomerDetailCategory;
            }
        }

        public string WholesaleApprovalDetail_AgentVehicleDeliverAppCity {
            get {
                return Web.Resources.EntityStrings.WholesaleApprovalDetail_AgentVehicleDeliverAppCity;
            }
        }

        public string WholesaleApprovalDetail_AgentVehicleDeliverAppDate {
            get {
                return Web.Resources.EntityStrings.WholesaleApprovalDetail_AgentVehicleDeliverAppDate;
            }
        }

        public string WholesaleApprovalDetail_OtherWholesaleReward {
            get {
                return Web.Resources.EntityStrings.WholesaleApprovalDetail_OtherWholesaleReward;
            }
        }

        public string WholesaleApproval_ApprovalComment {
            get {
                return Web.Resources.EntityStrings.WholesaleApproval_ApprovalComment;
            }
        }

        public string WholesaleApproval_DealerContactPhone {
            get {
                return Web.Resources.EntityStrings.WholesaleApproval_DealerContactPhone;
            }
        }

        public string WholesaleApproval_IfSpecialConfig {
            get {
                return Web.Resources.EntityStrings.WholesaleApproval_IfSpecialConfig;
            }
        }

        public string WholesaleApproval_SpecialConfigRequirement {
            get {
                return Web.Resources.EntityStrings.WholesaleApproval_SpecialConfigRequirement;
            }
        }

        public string DealerRegionManagerAffi_SalesRegion {
            get {
                return Web.Resources.EntityStrings.DealerRegionManagerAffi_SalesRegion;
            }
        }

        public string KeyAccount_CurrentPurchaseScale {
            get {
                return Web.Resources.EntityStrings.KeyAccount_CurrentPurchaseScale;
            }
        }

        public string KeyAccount_CustomerSummary {
            get {
                return Web.Resources.EntityStrings.KeyAccount_CustomerSummary;
            }
        }

        public string KeyAccount_ItemOtherDescription {
            get {
                return Web.Resources.EntityStrings.KeyAccount_ItemOtherDescription;
            }
        }

        public string KeyAccount_PurchaseMethod {
            get {
                return Web.Resources.EntityStrings.KeyAccount_PurchaseMethod;
            }
        }

        public string KeyAccount_PurchasePurpose {
            get {
                return Web.Resources.EntityStrings.KeyAccount_PurchasePurpose;
            }
        }

        public string WholesaleApproval_EmployeePurchaseDocOtherType {
            get {
                return Web.Resources.EntityStrings.WholesaleApproval_EmployeePurchaseDocOtherType;
            }
        }

        public string WholesaleApproval_EmployeePurchaseDocType {
            get {
                return Web.Resources.EntityStrings.WholesaleApproval_EmployeePurchaseDocType;
            }
        }

        public string SpecialVehicleChangeRecord_NewVIN {
            get {
                return Web.Resources.EntityStrings.SpecialVehicleChangeRecord_NewVIN;
            }
        }

        public string SpecialVehicleChangeRecord_OriginalVIN {
            get {
                return Web.Resources.EntityStrings.SpecialVehicleChangeRecord_OriginalVIN;
            }
        }

        public string VehicleShipplanApproval_Amount {
            get {
                return Web.Resources.EntityStrings.VehicleShipplanApproval_Amount;
            }
        }

        public string VehicleShipplanApprovalDetail_ProductCategoryCode {
            get {
                return Web.Resources.EntityStrings.VehicleShipplanApprovalDetail_ProductCategoryCode;
            }
        }

        public string PartsSupplier_PurchasingCycle {
            get {
                return Web.Resources.EntityStrings.PartsSupplier_PurchasingCycle;
            }
        }

        public string PartsShippingOrder_IsTransportLosses {
            get {
                return Web.Resources.EntityStrings.PartsShippingOrder_IsTransportLosses;
            }
        }

        public string PartsShippingOrder_TransportLossesDisposeStatus {
            get {
                return Web.Resources.EntityStrings.PartsShippingOrder_TransportLossesDisposeStatus;
            }
        }

        public string PartsShippingOrderDetail_TransportLossesDisposeMethod {
            get {
                return Web.Resources.EntityStrings.PartsShippingOrderDetail_TransportLossesDisposeMethod;
            }
        }

        public string PartsShippingOrder_BillingMethod {
            get {
                return Web.Resources.EntityStrings.PartsShippingOrder_BillingMethod;
            }
        }

        public string PartsShippingOrder_InsuranceRate {
            get {
                return Web.Resources.EntityStrings.PartsShippingOrder_InsuranceRate;
            }
        }

        public string PartsShippingOrder_TransportCostRate {
            get {
                return Web.Resources.EntityStrings.PartsShippingOrder_TransportCostRate;
            }
        }

        public string PartsSalesOrder_IfCanNewPart {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrder_IfCanNewPart;
            }
        }

        public string PartsBranch_PartsWarhouseManageGranularity {
            get {
                return Web.Resources.EntityStrings.PartsBranch_PartsWarhouseManageGranularity;
            }
        }

        public string PartsBranch_IsOrderToBranch {
            get {
                return Web.Resources.EntityStrings.PartsBranch_IsOrderToBranch;
            }
        }

        public string PartsBranch_PartsWarrantyCategoryCode {
            get {
                return Web.Resources.EntityStrings.PartsBranch_PartsWarrantyCategoryCode;
            }
        }

        public string PartsBranch_PartsWarrantyCategoryName {
            get {
                return Web.Resources.EntityStrings.PartsBranch_PartsWarrantyCategoryName;
            }
        }

        public string PartsOuterPurchaseChange_AbandonComment {
            get {
                return Web.Resources.EntityStrings.PartsOuterPurchaseChange_AbandonComment;
            }
        }

        public string PartsOuterPurchaseChange_Amount {
            get {
                return Web.Resources.EntityStrings.PartsOuterPurchaseChange_Amount;
            }
        }

        public string PartsOuterPurchaseChange_ApproveComment {
            get {
                return Web.Resources.EntityStrings.PartsOuterPurchaseChange_ApproveComment;
            }
        }

        public string PartsOuterPurchaseChange_BranchName {
            get {
                return Web.Resources.EntityStrings.PartsOuterPurchaseChange_BranchName;
            }
        }

        public string PartsOuterPurchaseChange_Code {
            get {
                return Web.Resources.EntityStrings.PartsOuterPurchaseChange_Code;
            }
        }

        public string PartsOuterPurchaseChange_CustomerCompanyCode {
            get {
                return Web.Resources.EntityStrings.PartsOuterPurchaseChange_CustomerCompanyCode;
            }
        }

        public string PartsOuterPurchaseChange_CustomerCompanyNace {
            get {
                return Web.Resources.EntityStrings.PartsOuterPurchaseChange_CustomerCompanyNace;
            }
        }

        public string PartsOuterPurchaseChange_OuterPurchaseComment {
            get {
                return Web.Resources.EntityStrings.PartsOuterPurchaseChange_OuterPurchaseComment;
            }
        }

        public string PartsOuterPurchaseChange_PartsSalesCategoryName {
            get {
                return Web.Resources.EntityStrings.PartsOuterPurchaseChange_PartsSalesCategoryName;
            }
        }

        public string PartsOuterPurchaseChange_SourceCategoryr {
            get {
                return Web.Resources.EntityStrings.PartsOuterPurchaseChange_SourceCategoryr;
            }
        }

        public string PartsOuterPurchaseChange_SourceCode {
            get {
                return Web.Resources.EntityStrings.PartsOuterPurchaseChange_SourceCode;
            }
        }

        public string PartsPurchaseOrderDetail_PendingConfirmedAmount {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseOrderDetail_PendingConfirmedAmount;
            }
        }

        public string PartsPurchasePricing_PriceType {
            get {
                return Web.Resources.EntityStrings.PartsPurchasePricing_PriceType;
            }
        }

        public string PartsPurchasePricing_ValidFrom {
            get {
                return Web.Resources.EntityStrings.PartsPurchasePricing_ValidFrom;
            }
        }

        public string PartsPurchasePricing_ValidTo {
            get {
                return Web.Resources.EntityStrings.PartsPurchasePricing_ValidTo;
            }
        }

        public string PartsOuterPurchaselist_OuterPurchasePrice {
            get {
                return Web.Resources.EntityStrings.PartsOuterPurchaselist_OuterPurchasePrice;
            }
        }

        public string PartsOuterPurchaselist_PartsCode {
            get {
                return Web.Resources.EntityStrings.PartsOuterPurchaselist_PartsCode;
            }
        }

        public string PartsOuterPurchaselist_PartsName {
            get {
                return Web.Resources.EntityStrings.PartsOuterPurchaselist_PartsName;
            }
        }

        public string PartsOuterPurchaselist_Quantity {
            get {
                return Web.Resources.EntityStrings.PartsOuterPurchaselist_Quantity;
            }
        }

        public string PartsOuterPurchaselist_Supplier {
            get {
                return Web.Resources.EntityStrings.PartsOuterPurchaselist_Supplier;
            }
        }

        public string PartsOuterPurchaselist_TradePrice {
            get {
                return Web.Resources.EntityStrings.PartsOuterPurchaselist_TradePrice;
            }
        }

        public string PartsSalesPriceChange_Code {
            get {
                return Web.Resources.EntityStrings.PartsSalesPriceChange_Code;
            }
        }

        public string PartsSalesPriceChange_IfClaim {
            get {
                return Web.Resources.EntityStrings.PartsSalesPriceChange_IfClaim;
            }
        }

        public string OverstockPartsAppDetail_DealPrice {
            get {
                return Web.Resources.EntityStrings.OverstockPartsAppDetail_DealPrice;
            }
        }

        public string PartsSalesPriceChangeDetail_ExpireTime {
            get {
                return Web.Resources.EntityStrings.PartsSalesPriceChangeDetail_ExpireTime;
            }
        }

        public string PartsSalesPriceChangeDetail_PriceType {
            get {
                return Web.Resources.EntityStrings.PartsSalesPriceChangeDetail_PriceType;
            }
        }

        public string PartsSalesPriceChangeDetail_RetailGuidePrice {
            get {
                return Web.Resources.EntityStrings.PartsSalesPriceChangeDetail_RetailGuidePrice;
            }
        }

        public string PartsSalesPriceChangeDetail_SalesPrice {
            get {
                return Web.Resources.EntityStrings.PartsSalesPriceChangeDetail_SalesPrice;
            }
        }

        public string PartsSalesPriceChangeDetail_SparePartCode {
            get {
                return Web.Resources.EntityStrings.PartsSalesPriceChangeDetail_SparePartCode;
            }
        }

        public string PartsSalesPriceChangeDetail_SparePartName {
            get {
                return Web.Resources.EntityStrings.PartsSalesPriceChangeDetail_SparePartName;
            }
        }

        public string PartsSalesPriceChangeDetail_ValidationTime {
            get {
                return Web.Resources.EntityStrings.PartsSalesPriceChangeDetail_ValidationTime;
            }
        }

        public string PartsPurchaseOrderType_Code {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseOrderType_Code;
            }
        }

        public string PartsPurchaseOrderType_Name {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseOrderType_Name;
            }
        }

        public string BranchSupplierRelation_BranchCode {
            get {
                return Web.Resources.EntityStrings.BranchSupplierRelation_BranchCode;
            }
        }

        public string BranchSupplierRelation_BranchName {
            get {
                return Web.Resources.EntityStrings.BranchSupplierRelation_BranchName;
            }
        }

        public string BranchSupplierRelation_PurchasingCycle {
            get {
                return Web.Resources.EntityStrings.BranchSupplierRelation_PurchasingCycle;
            }
        }

        public string BranchSupplierRelation_SupplierCode {
            get {
                return Web.Resources.EntityStrings.BranchSupplierRelation_SupplierCode;
            }
        }

        public string BranchSupplierRelation_SupplierName {
            get {
                return Web.Resources.EntityStrings.BranchSupplierRelation_SupplierName;
            }
        }

        public string PartsSalesPriceChange_PartsSalesCategoryCode {
            get {
                return Web.Resources.EntityStrings.PartsSalesPriceChange_PartsSalesCategoryCode;
            }
        }

        public string PartsSalesPriceChange_PartsSalesCategoryName {
            get {
                return Web.Resources.EntityStrings.PartsSalesPriceChange_PartsSalesCategoryName;
            }
        }

        public string PartsSalesPriceChange_Rejecter {
            get {
                return Web.Resources.EntityStrings.PartsSalesPriceChange_Rejecter;
            }
        }

        public string PartsSalesPriceChange_RejectTime {
            get {
                return Web.Resources.EntityStrings.PartsSalesPriceChange_RejectTime;
            }
        }

        public string DealerPartsRetailOrder_BranchName {
            get {
                return Web.Resources.EntityStrings.DealerPartsRetailOrder_BranchName;
            }
        }

        public string DealerPartsRetailOrder_ApprovalComment {
            get {
                return Web.Resources.EntityStrings.DealerPartsRetailOrder_ApprovalComment;
            }
        }

        public string DealerPartsRetailOrder_Customer {
            get {
                return Web.Resources.EntityStrings.DealerPartsRetailOrder_Customer;
            }
        }

        public string DealerPartsRetailOrder_TotalAmount {
            get {
                return Web.Resources.EntityStrings.DealerPartsRetailOrder_TotalAmount;
            }
        }

        public string DealerRetailOrderDetail_PartsCode {
            get {
                return Web.Resources.EntityStrings.DealerRetailOrderDetail_PartsCode;
            }
        }

        public string DealerRetailOrderDetail_PartsName {
            get {
                return Web.Resources.EntityStrings.DealerRetailOrderDetail_PartsName;
            }
        }

        public string DealerRetailOrderDetail_Price {
            get {
                return Web.Resources.EntityStrings.DealerRetailOrderDetail_Price;
            }
        }

        public string DealerRetailOrderDetail_Quantity {
            get {
                return Web.Resources.EntityStrings.DealerRetailOrderDetail_Quantity;
            }
        }

        public string DealerPartsStock_Quantity {
            get {
                return Web.Resources.EntityStrings.DealerPartsStock_Quantity;
            }
        }

        public string DealerPartsStock_SparePartCode {
            get {
                return Web.Resources.EntityStrings.DealerPartsStock_SparePartCode;
            }
        }

        public string DealerPartsStock_SparePartName {
            get {
                return Web.Resources.EntityStrings.DealerPartsStock_SparePartName;
            }
        }

        public string PartsPurchasePricingDetail_PriceType {
            get {
                return Web.Resources.EntityStrings.PartsPurchasePricingDetail_PriceType;
            }
        }

        public string PartsSalesPrice_IfClaim {
            get {
                return Web.Resources.EntityStrings.PartsSalesPrice_IfClaim;
            }
        }

        public string Warehouse_WmsInterface {
            get {
                return Web.Resources.EntityStrings.Warehouse_WmsInterface;
            }
        }

        public string PartsTransferOrderDetail_Price {
            get {
                return Web.Resources.EntityStrings.PartsTransferOrderDetail_Price;
            }
        }

        public string PartsTransferOrder_TotalAmount {
            get {
                return Web.Resources.EntityStrings.PartsTransferOrder_TotalAmount;
            }
        }

        public string PartsRebateType_Code {
            get {
                return Web.Resources.EntityStrings.PartsRebateType_Code;
            }
        }

        public string PartsRebateType_Name {
            get {
                return Web.Resources.EntityStrings.PartsRebateType_Name;
            }
        }

        public string PartsRebateApplication_Amount {
            get {
                return Web.Resources.EntityStrings.PartsRebateApplication_Amount;
            }
        }

        public string PartsRebateApplication_ApprovalComment {
            get {
                return Web.Resources.EntityStrings.PartsRebateApplication_ApprovalComment;
            }
        }

        public string PartsRebateApplication_Code {
            get {
                return Web.Resources.EntityStrings.PartsRebateApplication_Code;
            }
        }

        public string PartsRebateApplication_CustomerCompanyCode {
            get {
                return Web.Resources.EntityStrings.PartsRebateApplication_CustomerCompanyCode;
            }
        }

        public string PartsRebateApplication_CustomerCompanyName {
            get {
                return Web.Resources.EntityStrings.PartsRebateApplication_CustomerCompanyName;
            }
        }

        public string PartsRebateApplication_Motive {
            get {
                return Web.Resources.EntityStrings.PartsRebateApplication_Motive;
            }
        }

        public string PartsRebateApplication_RebateDirection {
            get {
                return Web.Resources.EntityStrings.PartsRebateApplication_RebateDirection;
            }
        }

        public string PartsRebateChangeDetail_Amount {
            get {
                return Web.Resources.EntityStrings.PartsRebateChangeDetail_Amount;
            }
        }

        public string PartsRebateChangeDetail_SourceCode {
            get {
                return Web.Resources.EntityStrings.PartsRebateChangeDetail_SourceCode;
            }
        }

        public string PartsRebateChangeDetail_SourceType {
            get {
                return Web.Resources.EntityStrings.PartsRebateChangeDetail_SourceType;
            }
        }

        public string PartsRebateChangeDetail_Summary {
            get {
                return Web.Resources.EntityStrings.PartsRebateChangeDetail_Summary;
            }
        }

        public string FinancialSnapshotSet_Code {
            get {
                return Web.Resources.EntityStrings.FinancialSnapshotSet_Code;
            }
        }

        public string FinancialSnapshotSet_SnapshotTime {
            get {
                return Web.Resources.EntityStrings.FinancialSnapshotSet_SnapshotTime;
            }
        }

        public string FinancialSnapshotSet_Memo {
            get {
                return Web.Resources.EntityStrings.FinancialSnapshotSet_Memo;
            }
        }

        public string DealerPartsInventoryDetail_CurrentStorage {
            get {
                return Web.Resources.EntityStrings.DealerPartsInventoryDetail_CurrentStorage;
            }
        }

        public string DealerPartsInventoryDetail_SparePartCode {
            get {
                return Web.Resources.EntityStrings.DealerPartsInventoryDetail_SparePartCode;
            }
        }

        public string DealerPartsInventoryDetail_SparePartName {
            get {
                return Web.Resources.EntityStrings.DealerPartsInventoryDetail_SparePartName;
            }
        }

        public string DealerPartsInventoryDetail_StorageAfterInventory {
            get {
                return Web.Resources.EntityStrings.DealerPartsInventoryDetail_StorageAfterInventory;
            }
        }

        public string DealerPartsInventoryDetail_StorageDifference {
            get {
                return Web.Resources.EntityStrings.DealerPartsInventoryDetail_StorageDifference;
            }
        }

        public string PartsInventoryBill_PartsInventoryDetails {
            get {
                return Web.Resources.EntityStrings.PartsInventoryBill_PartsInventoryDetails;
            }
        }

        public string PartsInventoryBill_InitiatorName {
            get {
                return Web.Resources.EntityStrings.PartsInventoryBill_InitiatorName;
            }
        }

        public string PartsInventoryBill_InventoryReason {
            get {
                return Web.Resources.EntityStrings.PartsInventoryBill_InventoryReason;
            }
        }

        public string PartsInventoryBill_Status {
            get {
                return Web.Resources.EntityStrings.PartsInventoryBill_Status;
            }
        }

        public string PartsInventoryBill_WarehouseAreaCode {
            get {
                return Web.Resources.EntityStrings.PartsInventoryBill_WarehouseAreaCode;
            }
        }

        public string PartsInventoryDetail_WarehouseAreaCode {
            get {
                return Web.Resources.EntityStrings.PartsInventoryDetail_WarehouseAreaCode;
            }
        }

        public string AgencyDealerRelation_AgencyCode {
            get {
                return Web.Resources.EntityStrings.AgencyDealerRelation_AgencyCode;
            }
        }

        public string AgencyDealerRelation_AgencyName {
            get {
                return Web.Resources.EntityStrings.AgencyDealerRelation_AgencyName;
            }
        }

        public string AgencyDealerRelation_DealerCode {
            get {
                return Web.Resources.EntityStrings.AgencyDealerRelation_DealerCode;
            }
        }

        public string AgencyDealerRelation_DealerName {
            get {
                return Web.Resources.EntityStrings.AgencyDealerRelation_DealerName;
            }
        }

        public string EngineProductLine_EngineCode {
            get {
                return Web.Resources.EntityStrings.EngineProductLine_EngineCode;
            }
        }

        public string EngineProductLine_EngineName {
            get {
                return Web.Resources.EntityStrings.EngineProductLine_EngineName;
            }
        }

        public string PartsInventoryDetail_CurrentBatchNumber {
            get {
                return Web.Resources.EntityStrings.PartsInventoryDetail_CurrentBatchNumber;
            }
        }

        public string PartsInventoryDetail_CurrentStorage {
            get {
                return Web.Resources.EntityStrings.PartsInventoryDetail_CurrentStorage;
            }
        }

        public string PartsInventoryDetail_NewBatchNumber {
            get {
                return Web.Resources.EntityStrings.PartsInventoryDetail_NewBatchNumber;
            }
        }

        public string PartsInventoryDetail_SparePartCode {
            get {
                return Web.Resources.EntityStrings.PartsInventoryDetail_SparePartCode;
            }
        }

        public string PartsInventoryDetail_SparePartName {
            get {
                return Web.Resources.EntityStrings.PartsInventoryDetail_SparePartName;
            }
        }

        public string PartsInventoryDetail_StorageAfterInventory {
            get {
                return Web.Resources.EntityStrings.PartsInventoryDetail_StorageAfterInventory;
            }
        }

        public string PartsInventoryDetail_StorageDifference {
            get {
                return Web.Resources.EntityStrings.PartsInventoryDetail_StorageDifference;
            }
        }

        public string PartsInventoryBill_WarehouseAreaCategory {
            get {
                return Web.Resources.EntityStrings.PartsInventoryBill_WarehouseAreaCategory;
            }
        }

        public string GradeCoefficient_Coefficient {
            get {
                return Web.Resources.EntityStrings.GradeCoefficient_Coefficient;
            }
        }

        public string GradeCoefficient_Grade {
            get {
                return Web.Resources.EntityStrings.GradeCoefficient_Grade;
            }
        }

        public string ServiceTripClaimApplication_FirstClassStationCode {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimApplication_FirstClassStationCode;
            }
        }

        public string DealerServiceInfo_OutServiceradii {
            get {
                return Web.Resources.EntityStrings.DealerServiceInfo_OutServiceradii;
            }
        }

        public string ServiceTripClaimApplication_CityName {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimApplication_CityName;
            }
        }

        public string ServiceTripClaimApplication_DetailedAddress {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimApplication_DetailedAddress;
            }
        }

        public string ServiceTripClaimApplication_ProvinceName {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimApplication_ProvinceName;
            }
        }

        public string ServiceTripClaimApplication_TrafficWay {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimApplication_TrafficWay;
            }
        }

        public string _Common__CheckerName {
            get {
                return Web.Resources.EntityStrings._Common__CheckerName;
            }
        }

        public string _Common__CheckTime {
            get {
                return Web.Resources.EntityStrings._Common__CheckTime;
            }
        }

        public string RepairClaimApplication_Capacity {
            get {
                return Web.Resources.EntityStrings.RepairClaimApplication_Capacity;
            }
        }

        public string RepairClaimApplication_WorkingHours {
            get {
                return Web.Resources.EntityStrings.RepairClaimApplication_WorkingHours;
            }
        }

        public string ServiceTripClaimApplication_RejectReason {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimApplication_RejectReason;
            }
        }

        public string ServiceTripClaimApplication_SupplyApproveComment {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimApplication_SupplyApproveComment;
            }
        }

        public string ServiceTripClaimApplication_CountyName {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimApplication_CountyName;
            }
        }

        public string CompanyTransferOrder_Code {
            get {
                return Web.Resources.EntityStrings.CompanyTransferOrder_Code;
            }
        }

        public string CompanyTransferOrder_Type {
            get {
                return Web.Resources.EntityStrings.CompanyTransferOrder_Type;
            }
        }

        public string CompanyTransferOrderDetail_ConfirmedAmount {
            get {
                return Web.Resources.EntityStrings.CompanyTransferOrderDetail_ConfirmedAmount;
            }
        }

        public string CompanyTransferOrderDetail_PartsCode {
            get {
                return Web.Resources.EntityStrings.CompanyTransferOrderDetail_PartsCode;
            }
        }

        public string CompanyTransferOrderDetail_PartsName {
            get {
                return Web.Resources.EntityStrings.CompanyTransferOrderDetail_PartsName;
            }
        }

        public string CompanyTransferOrderDetail_PlannedAmount {
            get {
                return Web.Resources.EntityStrings.CompanyTransferOrderDetail_PlannedAmount;
            }
        }

        public string CompanyTransferOrderDetail_Price {
            get {
                return Web.Resources.EntityStrings.CompanyTransferOrderDetail_Price;
            }
        }

        public string CompanyTransferOrder_ReceivingAddress {
            get {
                return Web.Resources.EntityStrings.CompanyTransferOrder_ReceivingAddress;
            }
        }

        public string CompanyTransferOrder_TotalAmount {
            get {
                return Web.Resources.EntityStrings.CompanyTransferOrder_TotalAmount;
            }
        }

        public string ServiceTripClaimBill_TrafficWay {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimBill_TrafficWay;
            }
        }

        public string ServiceTripClaimBill_Capacity {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimBill_Capacity;
            }
        }

        public string ServiceTripClaimBill_CityName {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimBill_CityName;
            }
        }

        public string ServiceTripClaimBill_CountyName {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimBill_CountyName;
            }
        }

        public string ServiceTripClaimBill_DetailedAddress {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimBill_DetailedAddress;
            }
        }

        public string ServiceTripClaimBill_OutVehicleLicensePlate {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimBill_OutVehicleLicensePlate;
            }
        }

        public string ServiceTripClaimBill_ProvinceName {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimBill_ProvinceName;
            }
        }

        public string ServiceTripClaimBill_SettleDistance {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimBill_SettleDistance;
            }
        }

        public string ServiceTripClaimBill_WorkingHours {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimBill_WorkingHours;
            }
        }

        public string TemporarySupplier_ContactAddress {
            get {
                return Web.Resources.EntityStrings.TemporarySupplier_ContactAddress;
            }
        }

        public string TemporarySupplier_ContactPerson {
            get {
                return Web.Resources.EntityStrings.TemporarySupplier_ContactPerson;
            }
        }

        public string TemporarySupplier_ContactPhone {
            get {
                return Web.Resources.EntityStrings.TemporarySupplier_ContactPhone;
            }
        }

        public string ServiceTripClaimBill_FinalApproverComment {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimBill_FinalApproverComment;
            }
        }

        public string ServiceTripClaimBill_InitialApproverComment {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimBill_InitialApproverComment;
            }
        }

        public string ServiceTripClaimBill_RejectQty {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimBill_RejectQty;
            }
        }

        public string ServiceTripClaimBill_SupplierApproveComment {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimBill_SupplierApproveComment;
            }
        }

        public string ServiceTripClaimBill_RejectReason {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimBill_RejectReason;
            }
        }

        public string ServiceTripClaimBill_AbandonerName {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimBill_AbandonerName;
            }
        }

        public string ServiceTripClaimBill_FinalApproverName {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimBill_FinalApproverName;
            }
        }

        public string ServiceTripClaimBill_FinalApproverTime {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimBill_FinalApproverTime;
            }
        }

        public string ServiceTripClaimBill_InitialApproverName {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimBill_InitialApproverName;
            }
        }

        public string ServiceTripClaimBill_InitialApproveTime {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimBill_InitialApproveTime;
            }
        }

        public string ServiceTripClaimBill_SupplierApproverName {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimBill_SupplierApproverName;
            }
        }

        public string ServiceTripClaimBill_SupplierApproveTime {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimBill_SupplierApproveTime;
            }
        }

        public string PartsPurchaseOrderType_Remark {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseOrderType_Remark;
            }
        }

        public string RepairClaimBill_Capacity {
            get {
                return Web.Resources.EntityStrings.RepairClaimBill_Capacity;
            }
        }

        public string RepairClaimBill_WorkingHours {
            get {
                return Web.Resources.EntityStrings.RepairClaimBill_WorkingHours;
            }
        }

        public string PartsSalesOrder_SalesCategoryName {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrder_SalesCategoryName;
            }
        }

        public string Company_CityName {
            get {
                return Web.Resources.EntityStrings.Company_CityName;
            }
        }

        public string Company_CountyName {
            get {
                return Web.Resources.EntityStrings.Company_CountyName;
            }
        }

        public string Company_ProvinceName {
            get {
                return Web.Resources.EntityStrings.Company_ProvinceName;
            }
        }

        public string RepairClaimBill_RejectStatus {
            get {
                return Web.Resources.EntityStrings.RepairClaimBill_RejectStatus;
            }
        }

        public string CompanyAddress_ContactPerson {
            get {
                return Web.Resources.EntityStrings.CompanyAddress_ContactPerson;
            }
        }

        public string CompanyAddress_ContactPhone {
            get {
                return Web.Resources.EntityStrings.CompanyAddress_ContactPhone;
            }
        }

        public string SubDealer_Code {
            get {
                return Web.Resources.EntityStrings.SubDealer_Code;
            }
        }

        public string SubDealer_Manager {
            get {
                return Web.Resources.EntityStrings.SubDealer_Manager;
            }
        }

        public string SubDealer_ManagerMail {
            get {
                return Web.Resources.EntityStrings.SubDealer_ManagerMail;
            }
        }

        public string SubDealer_ManagerMobile {
            get {
                return Web.Resources.EntityStrings.SubDealer_ManagerMobile;
            }
        }

        public string SubDealer_ManagerPhoneNumber {
            get {
                return Web.Resources.EntityStrings.SubDealer_ManagerPhoneNumber;
            }
        }

        public string SubDealer_Name {
            get {
                return Web.Resources.EntityStrings.SubDealer_Name;
            }
        }

        public string RepairOrder_Code {
            get {
                return Web.Resources.EntityStrings.RepairOrder_Code;
            }
        }

        public string RepairOrder_RejectStatus {
            get {
                return Web.Resources.EntityStrings.RepairOrder_RejectStatus;
            }
        }

        public string RepairOrder_VehicleLicensePlate {
            get {
                return Web.Resources.EntityStrings.RepairOrder_VehicleLicensePlate;
            }
        }

        public string RepairOrder_LaborCost {
            get {
                return Web.Resources.EntityStrings.RepairOrder_LaborCost;
            }
        }

        public string RepairOrder_OtherCost {
            get {
                return Web.Resources.EntityStrings.RepairOrder_OtherCost;
            }
        }

        public string RepairOrder_PartsManagementCost {
            get {
                return Web.Resources.EntityStrings.RepairOrder_PartsManagementCost;
            }
        }

        public string RepairOrder_TotalAmount {
            get {
                return Web.Resources.EntityStrings.RepairOrder_TotalAmount;
            }
        }

        public string RepairOrder_MaterialCost {
            get {
                return Web.Resources.EntityStrings.RepairOrder_MaterialCost;
            }
        }

        public string PartsSalesOrder_BranchName {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrder_BranchName;
            }
        }

        public string PartsInboundCheckBill_PartsSalesCategoryName {
            get {
                return Web.Resources.EntityStrings.PartsInboundCheckBill_PartsSalesCategoryName;
            }
        }

        public string RepairOrder_OutOfFactoryDate {
            get {
                return Web.Resources.EntityStrings.RepairOrder_OutOfFactoryDate;
            }
        }

        public string RepairOrder_SalesDate {
            get {
                return Web.Resources.EntityStrings.RepairOrder_SalesDate;
            }
        }

        public string RepairOrder_SalesStatus {
            get {
                return Web.Resources.EntityStrings.RepairOrder_SalesStatus;
            }
        }

        public string RepairOrder_BridgeType {
            get {
                return Web.Resources.EntityStrings.RepairOrder_BridgeType;
            }
        }

        public string RepairOrder_FinishingTime {
            get {
                return Web.Resources.EntityStrings.RepairOrder_FinishingTime;
            }
        }

        public string RepairOrder_RepairRequestTime {
            get {
                return Web.Resources.EntityStrings.RepairOrder_RepairRequestTime;
            }
        }

        public string RepairOrder_ContactAddress {
            get {
                return Web.Resources.EntityStrings.RepairOrder_ContactAddress;
            }
        }

        public string RepairOrder_ContactPhone {
            get {
                return Web.Resources.EntityStrings.RepairOrder_ContactPhone;
            }
        }

        public string RepairOrder_EngineApplicationArea {
            get {
                return Web.Resources.EntityStrings.RepairOrder_EngineApplicationArea;
            }
        }

        public string RepairOrder_EngineDamagedCondition {
            get {
                return Web.Resources.EntityStrings.RepairOrder_EngineDamagedCondition;
            }
        }

        public string RepairOrder_EngineWarrantyProperties {
            get {
                return Web.Resources.EntityStrings.RepairOrder_EngineWarrantyProperties;
            }
        }

        public string RepairOrder_RepairType {
            get {
                return Web.Resources.EntityStrings.RepairOrder_RepairType;
            }
        }

        public string RepairOrder_EngineWarrantyStartDate {
            get {
                return Web.Resources.EntityStrings.RepairOrder_EngineWarrantyStartDate;
            }
        }

        public string RepairOrder_Capacity {
            get {
                return Web.Resources.EntityStrings.RepairOrder_Capacity;
            }
        }

        public string RepairOrder_CarActuality {
            get {
                return Web.Resources.EntityStrings.RepairOrder_CarActuality;
            }
        }

        public string RepairOrder_CityName {
            get {
                return Web.Resources.EntityStrings.RepairOrder_CityName;
            }
        }

        public string RepairOrder_CountyName {
            get {
                return Web.Resources.EntityStrings.RepairOrder_CountyName;
            }
        }

        public string RepairOrder_CustomOpinion {
            get {
                return Web.Resources.EntityStrings.RepairOrder_CustomOpinion;
            }
        }

        public string RepairOrder_DealMethod {
            get {
                return Web.Resources.EntityStrings.RepairOrder_DealMethod;
            }
        }

        public string RepairOrder_DealResult {
            get {
                return Web.Resources.EntityStrings.RepairOrder_DealResult;
            }
        }

        public string RepairOrder_DetailedAddress {
            get {
                return Web.Resources.EntityStrings.RepairOrder_DetailedAddress;
            }
        }

        public string RepairOrder_EngineClaim {
            get {
                return Web.Resources.EntityStrings.RepairOrder_EngineClaim;
            }
        }

        public string RepairOrder_EngineCode {
            get {
                return Web.Resources.EntityStrings.RepairOrder_EngineCode;
            }
        }

        public string RepairOrder_GoTime {
            get {
                return Web.Resources.EntityStrings.RepairOrder_GoTime;
            }
        }

        public string RepairOrder_Mileage {
            get {
                return Web.Resources.EntityStrings.RepairOrder_Mileage;
            }
        }

        public string RepairOrder_OutPersonCode {
            get {
                return Web.Resources.EntityStrings.RepairOrder_OutPersonCode;
            }
        }

        public string RepairOrder_OutRange {
            get {
                return Web.Resources.EntityStrings.RepairOrder_OutRange;
            }
        }

        public string RepairOrder_ProvinceName {
            get {
                return Web.Resources.EntityStrings.RepairOrder_ProvinceName;
            }
        }

        public string RepairOrder_ReturnTime {
            get {
                return Web.Resources.EntityStrings.RepairOrder_ReturnTime;
            }
        }

        public string RepairOrder_ServiceTripDuration {
            get {
                return Web.Resources.EntityStrings.RepairOrder_ServiceTripDuration;
            }
        }

        public string RepairOrder_ServiceTripPerson {
            get {
                return Web.Resources.EntityStrings.RepairOrder_ServiceTripPerson;
            }
        }

        public string RepairOrder_ServiceTripType {
            get {
                return Web.Resources.EntityStrings.RepairOrder_ServiceTripType;
            }
        }

        public string RepairOrder_TrafficWay {
            get {
                return Web.Resources.EntityStrings.RepairOrder_TrafficWay;
            }
        }

        public string RepairOrder_USETRANSPORTPATH {
            get {
                return Web.Resources.EntityStrings.RepairOrder_USETRANSPORTPATH;
            }
        }

        public string RepairOrder_WorkingHours {
            get {
                return Web.Resources.EntityStrings.RepairOrder_WorkingHours;
            }
        }

        public string RepairOrderFaultReason_CauseOtherMalfunction {
            get {
                return Web.Resources.EntityStrings.RepairOrderFaultReason_CauseOtherMalfunction;
            }
        }

        public string RepairOrderFaultReason_MalfunctionReason {
            get {
                return Web.Resources.EntityStrings.RepairOrderFaultReason_MalfunctionReason;
            }
        }

        public string RepairOrderFaultReason_OtherCost {
            get {
                return Web.Resources.EntityStrings.RepairOrderFaultReason_OtherCost;
            }
        }

        public string RepairOrderFaultReason_OtherCostReason {
            get {
                return Web.Resources.EntityStrings.RepairOrderFaultReason_OtherCostReason;
            }
        }

        public string RepairOrderFaultReason_SettleAttribute {
            get {
                return Web.Resources.EntityStrings.RepairOrderFaultReason_SettleAttribute;
            }
        }

        public string RepairOrderItemDetail_DefaultLaborHour {
            get {
                return Web.Resources.EntityStrings.RepairOrderItemDetail_DefaultLaborHour;
            }
        }

        public string RepairOrderItemDetail_LaborCost {
            get {
                return Web.Resources.EntityStrings.RepairOrderItemDetail_LaborCost;
            }
        }

        public string RepairOrderItemDetail_LaborUnitPrice {
            get {
                return Web.Resources.EntityStrings.RepairOrderItemDetail_LaborUnitPrice;
            }
        }

        public string RepairOrderMaterialDetail_MaterialCost {
            get {
                return Web.Resources.EntityStrings.RepairOrderMaterialDetail_MaterialCost;
            }
        }

        public string RepairOrderMaterialDetail_NewPartsCode {
            get {
                return Web.Resources.EntityStrings.RepairOrderMaterialDetail_NewPartsCode;
            }
        }

        public string RepairOrderMaterialDetail_NewPartsName {
            get {
                return Web.Resources.EntityStrings.RepairOrderMaterialDetail_NewPartsName;
            }
        }

        public string RepairOrderMaterialDetail_NewPartsSecurityNumber {
            get {
                return Web.Resources.EntityStrings.RepairOrderMaterialDetail_NewPartsSecurityNumber;
            }
        }

        public string RepairOrderMaterialDetail_PartsManagementCost {
            get {
                return Web.Resources.EntityStrings.RepairOrderMaterialDetail_PartsManagementCost;
            }
        }

        public string RepairOrderMaterialDetail_PartSource {
            get {
                return Web.Resources.EntityStrings.RepairOrderMaterialDetail_PartSource;
            }
        }

        public string RepairOrderMaterialDetail_UsedPartsCode {
            get {
                return Web.Resources.EntityStrings.RepairOrderMaterialDetail_UsedPartsCode;
            }
        }

        public string RepairOrderMaterialDetail_UsedPartsName {
            get {
                return Web.Resources.EntityStrings.RepairOrderMaterialDetail_UsedPartsName;
            }
        }

        public string MalfunctionView_Code {
            get {
                return Web.Resources.EntityStrings.MalfunctionView_Code;
            }
        }

        public string MalfunctionView_ParentCode {
            get {
                return Web.Resources.EntityStrings.MalfunctionView_ParentCode;
            }
        }

        public string MalfunctionView_ParentName {
            get {
                return Web.Resources.EntityStrings.MalfunctionView_ParentName;
            }
        }

        public string Branch_Code {
            get {
                return Web.Resources.EntityStrings.Branch_Code;
            }
        }

        public string MalfunctionView_MalfunctionCategoryCode {
            get {
                return Web.Resources.EntityStrings.MalfunctionView_MalfunctionCategoryCode;
            }
        }

        public string MalfunctionView_MalfunctionCategoryName {
            get {
                return Web.Resources.EntityStrings.MalfunctionView_MalfunctionCategoryName;
            }
        }

        public string VehicleLinkman_CellPhoneNumber {
            get {
                return Web.Resources.EntityStrings.VehicleLinkman_CellPhoneNumber;
            }
        }

        public string VehicleLinkman_Gender {
            get {
                return Web.Resources.EntityStrings.VehicleLinkman_Gender;
            }
        }

        public string VehicleLinkman_LinkmanType {
            get {
                return Web.Resources.EntityStrings.VehicleLinkman_LinkmanType;
            }
        }

        public string VehicleLinkman_Name {
            get {
                return Web.Resources.EntityStrings.VehicleLinkman_Name;
            }
        }

        public string RepairItemView_Code {
            get {
                return Web.Resources.EntityStrings.RepairItemView_Code;
            }
        }

        public string RepairItemView_Name {
            get {
                return Web.Resources.EntityStrings.RepairItemView_Name;
            }
        }

        public string RepairItemView_ParentCode {
            get {
                return Web.Resources.EntityStrings.RepairItemView_ParentCode;
            }
        }

        public string RepairItemView_ParentName {
            get {
                return Web.Resources.EntityStrings.RepairItemView_ParentName;
            }
        }

        public string RepairItemLaborHourView_Code {
            get {
                return Web.Resources.EntityStrings.RepairItemLaborHourView_Code;
            }
        }

        public string RepairItemLaborHourView_DefaultLaborHour {
            get {
                return Web.Resources.EntityStrings.RepairItemLaborHourView_DefaultLaborHour;
            }
        }

        public string RepairItemLaborHourView_Name {
            get {
                return Web.Resources.EntityStrings.RepairItemLaborHourView_Name;
            }
        }

        public string RepairItemLaborHourView_ParentCode {
            get {
                return Web.Resources.EntityStrings.RepairItemLaborHourView_ParentCode;
            }
        }

        public string RepairItemLaborHourView_ParentName {
            get {
                return Web.Resources.EntityStrings.RepairItemLaborHourView_ParentName;
            }
        }

        public string RepairItemLaborHourView_RepairItemCategoryCode {
            get {
                return Web.Resources.EntityStrings.RepairItemLaborHourView_RepairItemCategoryCode;
            }
        }

        public string RepairItemLaborHourView_RepairItemCategoryName {
            get {
                return Web.Resources.EntityStrings.RepairItemLaborHourView_RepairItemCategoryName;
            }
        }

        public string SparePart_PackingAmount {
            get {
                return Web.Resources.EntityStrings.SparePart_PackingAmount;
            }
        }

        public string SparePart_PackingSpecification {
            get {
                return Web.Resources.EntityStrings.SparePart_PackingSpecification;
            }
        }

        public string SparePart_PartsInPackingCode {
            get {
                return Web.Resources.EntityStrings.SparePart_PartsInPackingCode;
            }
        }

        public string SparePart_PartsOutPackingCode {
            get {
                return Web.Resources.EntityStrings.SparePart_PartsOutPackingCode;
            }
        }

        public string PartsBranch_PartsReturnPolicy {
            get {
                return Web.Resources.EntityStrings.PartsBranch_PartsReturnPolicy;
            }
        }

        public string SparePartHistory_CADCode {
            get {
                return Web.Resources.EntityStrings.SparePartHistory_CADCode;
            }
        }

        public string SparePartHistory_CADName {
            get {
                return Web.Resources.EntityStrings.SparePartHistory_CADName;
            }
        }

        public string SparePartHistory_Code {
            get {
                return Web.Resources.EntityStrings.SparePartHistory_Code;
            }
        }

        public string SparePartHistory_EnglishName {
            get {
                return Web.Resources.EntityStrings.SparePartHistory_EnglishName;
            }
        }

        public string SparePartHistory_Height {
            get {
                return Web.Resources.EntityStrings.SparePartHistory_Height;
            }
        }

        public string SparePartHistory_Length {
            get {
                return Web.Resources.EntityStrings.SparePartHistory_Length;
            }
        }

        public string SparePartHistory_LossType {
            get {
                return Web.Resources.EntityStrings.SparePartHistory_LossType;
            }
        }

        public string SparePartHistory_Material {
            get {
                return Web.Resources.EntityStrings.SparePartHistory_Material;
            }
        }

        public string SparePartHistory_MeasureUnit {
            get {
                return Web.Resources.EntityStrings.SparePartHistory_MeasureUnit;
            }
        }

        public string SparePartHistory_Name {
            get {
                return Web.Resources.EntityStrings.SparePartHistory_Name;
            }
        }

        public string SparePartHistory_PackingAmount {
            get {
                return Web.Resources.EntityStrings.SparePartHistory_PackingAmount;
            }
        }

        public string SparePartHistory_PackingSpecification {
            get {
                return Web.Resources.EntityStrings.SparePartHistory_PackingSpecification;
            }
        }

        public string SparePartHistory_PartsInPackingCode {
            get {
                return Web.Resources.EntityStrings.SparePartHistory_PartsInPackingCode;
            }
        }

        public string SparePartHistory_PartsOutPackingCode {
            get {
                return Web.Resources.EntityStrings.SparePartHistory_PartsOutPackingCode;
            }
        }

        public string SparePartHistory_PartType {
            get {
                return Web.Resources.EntityStrings.SparePartHistory_PartType;
            }
        }

        public string SparePartHistory_PinyinCode {
            get {
                return Web.Resources.EntityStrings.SparePartHistory_PinyinCode;
            }
        }

        public string SparePartHistory_Specification {
            get {
                return Web.Resources.EntityStrings.SparePartHistory_Specification;
            }
        }

        public string SparePartHistory_Volume {
            get {
                return Web.Resources.EntityStrings.SparePartHistory_Volume;
            }
        }

        public string SparePartHistory_Weight {
            get {
                return Web.Resources.EntityStrings.SparePartHistory_Weight;
            }
        }

        public string SparePartHistory_Width {
            get {
                return Web.Resources.EntityStrings.SparePartHistory_Width;
            }
        }

        public string RepairOrder_FieldServiceCharge {
            get {
                return Web.Resources.EntityStrings.RepairOrder_FieldServiceCharge;
            }
        }

        public string RepairOrder_TowCharge {
            get {
                return Web.Resources.EntityStrings.RepairOrder_TowCharge;
            }
        }

        public string RepairOrder_OutVehicleLicensePlate {
            get {
                return Web.Resources.EntityStrings.RepairOrder_OutVehicleLicensePlate;
            }
        }

        public string CompanyInvoiceInfo_InvoiceType {
            get {
                return Web.Resources.EntityStrings.CompanyInvoiceInfo_InvoiceType;
            }
        }

        public string RepairClaimApplication_ChangeAssemblyReason {
            get {
                return Web.Resources.EntityStrings.RepairClaimApplication_ChangeAssemblyReason;
            }
        }

        public string PreSaleCheckOrder_DriverName {
            get {
                return Web.Resources.EntityStrings.PreSaleCheckOrder_DriverName;
            }
        }

        public string PreSaleCheckOrder_DriverTel {
            get {
                return Web.Resources.EntityStrings.PreSaleCheckOrder_DriverTel;
            }
        }

        public string PreSaleCheckOrder_Cost {
            get {
                return Web.Resources.EntityStrings.PreSaleCheckOrder_Cost;
            }
        }

        public string PreSalesCheckDetail_Category {
            get {
                return Web.Resources.EntityStrings.PreSalesCheckDetail_Category;
            }
        }

        public string PreSalesCheckDetail_CheckItem {
            get {
                return Web.Resources.EntityStrings.PreSalesCheckDetail_CheckItem;
            }
        }

        public string PreSalesCheckDetail_FaultPattern {
            get {
                return Web.Resources.EntityStrings.PreSalesCheckDetail_FaultPattern;
            }
        }

        public string PreSaleCheckOrder_Code {
            get {
                return Web.Resources.EntityStrings.PreSaleCheckOrder_Code;
            }
        }

        public string PreSaleCheckOrder_VehicleLicensePlate {
            get {
                return Web.Resources.EntityStrings.PreSaleCheckOrder_VehicleLicensePlate;
            }
        }

        public string VehicleLinkman_Address {
            get {
                return Web.Resources.EntityStrings.VehicleLinkman_Address;
            }
        }

        public string VehicleLinkman_Email {
            get {
                return Web.Resources.EntityStrings.VehicleLinkman_Email;
            }
        }

        public string PreSaleItem_CheckItem {
            get {
                return Web.Resources.EntityStrings.PreSaleItem_CheckItem;
            }
        }

        public string PreSaleItem_FaultPattern {
            get {
                return Web.Resources.EntityStrings.PreSaleItem_FaultPattern;
            }
        }

        public string PreSaleItem_Category {
            get {
                return Web.Resources.EntityStrings.PreSaleItem_Category;
            }
        }

        public string RepairClaimBill_RejectReason {
            get {
                return Web.Resources.EntityStrings.RepairClaimBill_RejectReason;
            }
        }

        public string RepairClaimBill_EngineModel {
            get {
                return Web.Resources.EntityStrings.RepairClaimBill_EngineModel;
            }
        }

        public string RepairClaimBill_GearModel {
            get {
                return Web.Resources.EntityStrings.RepairClaimBill_GearModel;
            }
        }

        public string RepairClaimBill_InitialApprovertComment {
            get {
                return Web.Resources.EntityStrings.RepairClaimBill_InitialApprovertComment;
            }
        }

        public string PartsWarrantyTerm_Capacity {
            get {
                return Web.Resources.EntityStrings.PartsWarrantyTerm_Capacity;
            }
        }

        public string PartsWarrantyTerm_WorkingHours {
            get {
                return Web.Resources.EntityStrings.PartsWarrantyTerm_WorkingHours;
            }
        }

        public string PartsWarrantyStandard_Capacity {
            get {
                return Web.Resources.EntityStrings.PartsWarrantyStandard_Capacity;
            }
        }

        public string PartsWarrantyStandard_Code {
            get {
                return Web.Resources.EntityStrings.PartsWarrantyStandard_Code;
            }
        }

        public string PartsWarrantyStandard_Name {
            get {
                return Web.Resources.EntityStrings.PartsWarrantyStandard_Name;
            }
        }

        public string PartsWarrantyStandard_PartsWarrantyType {
            get {
                return Web.Resources.EntityStrings.PartsWarrantyStandard_PartsWarrantyType;
            }
        }

        public string PartsWarrantyStandard_WarrantyMileage {
            get {
                return Web.Resources.EntityStrings.PartsWarrantyStandard_WarrantyMileage;
            }
        }

        public string PartsWarrantyStandard_WarrantyPeriod {
            get {
                return Web.Resources.EntityStrings.PartsWarrantyStandard_WarrantyPeriod;
            }
        }

        public string PartsWarrantyStandard_WorkingHours {
            get {
                return Web.Resources.EntityStrings.PartsWarrantyStandard_WorkingHours;
            }
        }

        public string RepairClaimBill_FinalApproverComment {
            get {
                return Web.Resources.EntityStrings.RepairClaimBill_FinalApproverComment;
            }
        }

        public string RepairClaimBill_RejectQty {
            get {
                return Web.Resources.EntityStrings.RepairClaimBill_RejectQty;
            }
        }

        public string PartsInventoryDetail_Ifcover {
            get {
                return Web.Resources.EntityStrings.PartsInventoryDetail_Ifcover;
            }
        }

        public string SparePart_LastSubstitute {
            get {
                return Web.Resources.EntityStrings.SparePart_LastSubstitute;
            }
        }

        public string SparePart_NextSubstitute {
            get {
                return Web.Resources.EntityStrings.SparePart_NextSubstitute;
            }
        }

        public string RepairOrderMaterialDetail_Quantity {
            get {
                return Web.Resources.EntityStrings.RepairOrderMaterialDetail_Quantity;
            }
        }

        public string RepairWorkOrder_Code {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrder_Code;
            }
        }

        public string RepairWorkOrder_DealerCode {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrder_DealerCode;
            }
        }

        public string RepairWorkOrder_ContactAddress {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrder_ContactAddress;
            }
        }

        public string RepairWorkOrder_ContactPhone {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrder_ContactPhone;
            }
        }

        public string RepairWorkOrder_DispatchingCode {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrder_DispatchingCode;
            }
        }

        public string RepairWorkOrder_VehicleContactPerson {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrder_VehicleContactPerson;
            }
        }

        public string RepairWorkOrder_DealerName {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrder_DealerName;
            }
        }

        public string ServiceActivityDealerVehicle_AlreadyUsed {
            get {
                return Web.Resources.EntityStrings.ServiceActivityDealerVehicle_AlreadyUsed;
            }
        }

        public string ServiceActivity_ServiceActivityDealerVehicles {
            get {
                return Web.Resources.EntityStrings.ServiceActivity_ServiceActivityDealerVehicles;
            }
        }

        public string HauDistanceInfor_HauDistance {
            get {
                return Web.Resources.EntityStrings.HauDistanceInfor_HauDistance;
            }
        }

        public string SparePartHistory_LastSubstitute {
            get {
                return Web.Resources.EntityStrings.SparePartHistory_LastSubstitute;
            }
        }

        public string SparePartHistory_NextSubstitute {
            get {
                return Web.Resources.EntityStrings.SparePartHistory_NextSubstitute;
            }
        }

        public string PartsManagementCostRate_LowerLimitAmount {
            get {
                return Web.Resources.EntityStrings.PartsManagementCostRate_LowerLimitAmount;
            }
        }

        public string PartsManagementCostRate_UpperLimitAmount {
            get {
                return Web.Resources.EntityStrings.PartsManagementCostRate_UpperLimitAmount;
            }
        }

        public string PartsBranch_ReferenceCode {
            get {
                return Web.Resources.EntityStrings.PartsBranch_ReferenceCode;
            }
        }

        public string PartsBranch_StockMaximum {
            get {
                return Web.Resources.EntityStrings.PartsBranch_StockMaximum;
            }
        }

        public string PartsBranch_StockMinimum {
            get {
                return Web.Resources.EntityStrings.PartsBranch_StockMinimum;
            }
        }

        public string PartsBranch_LossType {
            get {
                return Web.Resources.EntityStrings.PartsBranch_LossType;
            }
        }

        public string SecondClassStationPlan_Code {
            get {
                return Web.Resources.EntityStrings.SecondClassStationPlan_Code;
            }
        }

        public string SecondClassStationPlan_FirstClassStationCode {
            get {
                return Web.Resources.EntityStrings.SecondClassStationPlan_FirstClassStationCode;
            }
        }

        public string SecondClassStationPlan_FirstClassStationName {
            get {
                return Web.Resources.EntityStrings.SecondClassStationPlan_FirstClassStationName;
            }
        }

        public string SecondClassStationPlan_SecondClassStationCode {
            get {
                return Web.Resources.EntityStrings.SecondClassStationPlan_SecondClassStationCode;
            }
        }

        public string SecondClassStationPlan_SecondClassStationName {
            get {
                return Web.Resources.EntityStrings.SecondClassStationPlan_SecondClassStationName;
            }
        }

        public string SecondClassStationPlanDetail_Quantity {
            get {
                return Web.Resources.EntityStrings.SecondClassStationPlanDetail_Quantity;
            }
        }

        public string SecondClassStationPlanDetail_SparePartCode {
            get {
                return Web.Resources.EntityStrings.SecondClassStationPlanDetail_SparePartCode;
            }
        }

        public string SecondClassStationPlanDetail_SparePartName {
            get {
                return Web.Resources.EntityStrings.SecondClassStationPlanDetail_SparePartName;
            }
        }

        public string RepairOrder_DealerName {
            get {
                return Web.Resources.EntityStrings.RepairOrder_DealerName;
            }
        }

        public string PartsReplacementHistory_NewPartCode {
            get {
                return Web.Resources.EntityStrings.PartsReplacementHistory_NewPartCode;
            }
        }

        public string PartsReplacementHistory_NewPartName {
            get {
                return Web.Resources.EntityStrings.PartsReplacementHistory_NewPartName;
            }
        }

        public string PartsReplacementHistory_OldPartCode {
            get {
                return Web.Resources.EntityStrings.PartsReplacementHistory_OldPartCode;
            }
        }

        public string PartsReplacementHistory_OldPartName {
            get {
                return Web.Resources.EntityStrings.PartsReplacementHistory_OldPartName;
            }
        }

        public string PartsBranchHistory_BranchName {
            get {
                return Web.Resources.EntityStrings.PartsBranchHistory_BranchName;
            }
        }

        public string PartsBranchHistory_LossType {
            get {
                return Web.Resources.EntityStrings.PartsBranchHistory_LossType;
            }
        }

        public string PartsBranchHistory_MinSaleQuantity {
            get {
                return Web.Resources.EntityStrings.PartsBranchHistory_MinSaleQuantity;
            }
        }

        public string PartsBranchHistory_PartABC {
            get {
                return Web.Resources.EntityStrings.PartsBranchHistory_PartABC;
            }
        }

        public string PartsBranchHistory_PartCode {
            get {
                return Web.Resources.EntityStrings.PartsBranchHistory_PartCode;
            }
        }

        public string PartsBranchHistory_PartName {
            get {
                return Web.Resources.EntityStrings.PartsBranchHistory_PartName;
            }
        }

        public string PartsBranchHistory_PartsReturnPolicy {
            get {
                return Web.Resources.EntityStrings.PartsBranchHistory_PartsReturnPolicy;
            }
        }

        public string PartsBranchHistory_ProductLifeCycle {
            get {
                return Web.Resources.EntityStrings.PartsBranchHistory_ProductLifeCycle;
            }
        }

        public string PartsBranchHistory_ReferenceCode {
            get {
                return Web.Resources.EntityStrings.PartsBranchHistory_ReferenceCode;
            }
        }

        public string PartsBranchHistory_StockMaximum {
            get {
                return Web.Resources.EntityStrings.PartsBranchHistory_StockMaximum;
            }
        }

        public string PartsBranchHistory_StockMinimum {
            get {
                return Web.Resources.EntityStrings.PartsBranchHistory_StockMinimum;
            }
        }

        public string Notification_Code {
            get {
                return Web.Resources.EntityStrings.Notification_Code;
            }
        }

        public string Notification_Content {
            get {
                return Web.Resources.EntityStrings.Notification_Content;
            }
        }

        public string Notification_Title {
            get {
                return Web.Resources.EntityStrings.Notification_Title;
            }
        }

        public string Notification_Top {
            get {
                return Web.Resources.EntityStrings.Notification_Top;
            }
        }

        public string Notification_TopTime {
            get {
                return Web.Resources.EntityStrings.Notification_TopTime;
            }
        }

        public string Notification_Type {
            get {
                return Web.Resources.EntityStrings.Notification_Type;
            }
        }

        public string Notification_TopModifierName {
            get {
                return Web.Resources.EntityStrings.Notification_TopModifierName;
            }
        }

        public string MalfunctionBrandRelation_IsApplication {
            get {
                return Web.Resources.EntityStrings.MalfunctionBrandRelation_IsApplication;
            }
        }

        public string MalfunctionBrandRelation_MalfunctionCode {
            get {
                return Web.Resources.EntityStrings.MalfunctionBrandRelation_MalfunctionCode;
            }
        }

        public string MalfunctionBrandRelation_MalfunctionName {
            get {
                return Web.Resources.EntityStrings.MalfunctionBrandRelation_MalfunctionName;
            }
        }

        public string PartsExchange_ExchangeCode {
            get {
                return Web.Resources.EntityStrings.PartsExchange_ExchangeCode;
            }
        }

        public string PartsExchange_ExchangeName {
            get {
                return Web.Resources.EntityStrings.PartsExchange_ExchangeName;
            }
        }

        public string PartsExchangeHistory_ExchangeCode {
            get {
                return Web.Resources.EntityStrings.PartsExchangeHistory_ExchangeCode;
            }
        }

        public string PartsExchangeHistory_ExchangeName {
            get {
                return Web.Resources.EntityStrings.PartsExchangeHistory_ExchangeName;
            }
        }

        public string PartsSalesOrder_SourceBillCode {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrder_SourceBillCode;
            }
        }

        public string WarrantyPolicy_BranchName {
            get {
                return Web.Resources.EntityStrings.WarrantyPolicy_BranchName;
            }
        }

        public string PartsPurchaseOrder_OriginalRequirementBillCode {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseOrder_OriginalRequirementBillCode;
            }
        }

        public string PartsShippingOrder_OriginalRequirementBillCode {
            get {
                return Web.Resources.EntityStrings.PartsShippingOrder_OriginalRequirementBillCode;
            }
        }

        public string SupplierShippingOrder_OriginalRequirementBillCode {
            get {
                return Web.Resources.EntityStrings.SupplierShippingOrder_OriginalRequirementBillCode;
            }
        }

        public string DealerPartsRetailOrder_Address {
            get {
                return Web.Resources.EntityStrings.DealerPartsRetailOrder_Address;
            }
        }

        public string DealerPartsRetailOrder_CustomerCellPhone {
            get {
                return Web.Resources.EntityStrings.DealerPartsRetailOrder_CustomerCellPhone;
            }
        }

        public string DealerPartsRetailOrder_CustomerPhone {
            get {
                return Web.Resources.EntityStrings.DealerPartsRetailOrder_CustomerPhone;
            }
        }

        public string RetailOrderCustomer_Address {
            get {
                return Web.Resources.EntityStrings.RetailOrderCustomer_Address;
            }
        }

        public string RetailOrderCustomer_CustomerCellPhone {
            get {
                return Web.Resources.EntityStrings.RetailOrderCustomer_CustomerCellPhone;
            }
        }

        public string RetailOrderCustomer_CustomerName {
            get {
                return Web.Resources.EntityStrings.RetailOrderCustomer_CustomerName;
            }
        }

        public string RetailOrderCustomer_CustomerPhone {
            get {
                return Web.Resources.EntityStrings.RetailOrderCustomer_CustomerPhone;
            }
        }

        public string TemporarySupplier_BranchName {
            get {
                return Web.Resources.EntityStrings.TemporarySupplier_BranchName;
            }
        }

        public string CompanyAddress_CityName {
            get {
                return Web.Resources.EntityStrings.CompanyAddress_CityName;
            }
        }

        public string CompanyAddress_CountyName {
            get {
                return Web.Resources.EntityStrings.CompanyAddress_CountyName;
            }
        }

        public string CompanyAddress_ProvinceName {
            get {
                return Web.Resources.EntityStrings.CompanyAddress_ProvinceName;
            }
        }

        public string VehicleMainteTerm_Capacity {
            get {
                return Web.Resources.EntityStrings.VehicleMainteTerm_Capacity;
            }
        }

        public string VehicleMainteTerm_MainteType {
            get {
                return Web.Resources.EntityStrings.VehicleMainteTerm_MainteType;
            }
        }

        public string VehicleMainteTerm_WorkingHours {
            get {
                return Web.Resources.EntityStrings.VehicleMainteTerm_WorkingHours;
            }
        }

        public string CompanyInvoiceInfo_TaxRegisteredAddress {
            get {
                return Web.Resources.EntityStrings.CompanyInvoiceInfo_TaxRegisteredAddress;
            }
        }

        public string CompanyInvoiceInfo_ContactNumber {
            get {
                return Web.Resources.EntityStrings.CompanyInvoiceInfo_ContactNumber;
            }
        }

        public string CompanyInvoiceInfo_Fax {
            get {
                return Web.Resources.EntityStrings.CompanyInvoiceInfo_Fax;
            }
        }

        public string CompanyInvoiceInfo_InvoiceTax {
            get {
                return Web.Resources.EntityStrings.CompanyInvoiceInfo_InvoiceTax;
            }
        }

        public string CompanyInvoiceInfo_Linkman {
            get {
                return Web.Resources.EntityStrings.CompanyInvoiceInfo_Linkman;
            }
        }

        public string ServiceTripArea_Name {
            get {
                return Web.Resources.EntityStrings.ServiceTripArea_Name;
            }
        }

        public string ServiceTripArea_PartsSalesCategoryName {
            get {
                return Web.Resources.EntityStrings.ServiceTripArea_PartsSalesCategoryName;
            }
        }

        public string ServiceTripArea_SubsidyPrice {
            get {
                return Web.Resources.EntityStrings.ServiceTripArea_SubsidyPrice;
            }
        }

        public string ServiceTripArea_Code {
            get {
                return Web.Resources.EntityStrings.ServiceTripArea_Code;
            }
        }

        public string Company_CityLevel {
            get {
                return Web.Resources.EntityStrings.Company_CityLevel;
            }
        }

        public string UsedPartsShippingDetail_SubDealerCode {
            get {
                return Web.Resources.EntityStrings.UsedPartsShippingDetail_SubDealerCode;
            }
        }

        public string UsedPartsShippingDetail_SubDealerName {
            get {
                return Web.Resources.EntityStrings.UsedPartsShippingDetail_SubDealerName;
            }
        }

        public string DealerServiceExt_OwnerCompany {
            get {
                return Web.Resources.EntityStrings.DealerServiceExt_OwnerCompany;
            }
        }

        public string DealerServiceExt_MainBusinessAreas {
            get {
                return Web.Resources.EntityStrings.DealerServiceExt_MainBusinessAreas;
            }
        }

        public string DealerServiceExt_TrafficRestrictionsdescribe {
            get {
                return Web.Resources.EntityStrings.DealerServiceExt_TrafficRestrictionsdescribe;
            }
        }

        public string DealerServiceExt_AndBusinessAreas {
            get {
                return Web.Resources.EntityStrings.DealerServiceExt_AndBusinessAreas;
            }
        }

        public string DealerServiceExt_BuildTime {
            get {
                return Web.Resources.EntityStrings.DealerServiceExt_BuildTime;
            }
        }

        public string DealerServiceExt_GeographicPosition {
            get {
                return Web.Resources.EntityStrings.DealerServiceExt_GeographicPosition;
            }
        }

        public string DealerServiceExt_PartReserveAmount {
            get {
                return Web.Resources.EntityStrings.DealerServiceExt_PartReserveAmount;
            }
        }

        public string Dealer_Manager {
            get {
                return Web.Resources.EntityStrings.Dealer_Manager;
            }
        }

        public string OverstockPartsApp_ContactPerson {
            get {
                return Web.Resources.EntityStrings.OverstockPartsApp_ContactPerson;
            }
        }

        public string OverstockPartsApp_ContactPhone {
            get {
                return Web.Resources.EntityStrings.OverstockPartsApp_ContactPhone;
            }
        }

        public string PartsSalesCategory_IsNotWarranty {
            get {
                return Web.Resources.EntityStrings.PartsSalesCategory_IsNotWarranty;
            }
        }

        public string DealerPartsRetailOrder_RetailOrderType {
            get {
                return Web.Resources.EntityStrings.DealerPartsRetailOrder_RetailOrderType;
            }
        }

        public string DealerPartsRetailOrder_SubDealerName {
            get {
                return Web.Resources.EntityStrings.DealerPartsRetailOrder_SubDealerName;
            }
        }

        public string RepairItemAffiServProdLine_ProductLineType {
            get {
                return Web.Resources.EntityStrings.RepairItemAffiServProdLine_ProductLineType;
            }
        }

        public string RepairItemProdLineHistory_DefaultLaborHour {
            get {
                return Web.Resources.EntityStrings.RepairItemProdLineHistory_DefaultLaborHour;
            }
        }

        public string DealerServiceInfo_BusinessDivision {
            get {
                return Web.Resources.EntityStrings.DealerServiceInfo_BusinessDivision;
            }
        }

        public string DealerServiceInfo_AccreditTime {
            get {
                return Web.Resources.EntityStrings.DealerServiceInfo_AccreditTime;
            }
        }

        public string DealerServiceInfo_Area {
            get {
                return Web.Resources.EntityStrings.DealerServiceInfo_Area;
            }
        }

        public string DealerServiceInfo_CancellingDate {
            get {
                return Web.Resources.EntityStrings.DealerServiceInfo_CancellingDate;
            }
        }

        public string DealerServiceInfo_InvoiceTypeInvoiceRatio {
            get {
                return Web.Resources.EntityStrings.DealerServiceInfo_InvoiceTypeInvoiceRatio;
            }
        }

        public string DealerServiceInfo_IsOnDuty {
            get {
                return Web.Resources.EntityStrings.DealerServiceInfo_IsOnDuty;
            }
        }

        public string DealerServiceInfo_LaborCostCostInvoiceType {
            get {
                return Web.Resources.EntityStrings.DealerServiceInfo_LaborCostCostInvoiceType;
            }
        }

        public string DealerServiceInfo_LaborCostInvoiceRatio {
            get {
                return Web.Resources.EntityStrings.DealerServiceInfo_LaborCostInvoiceRatio;
            }
        }

        public string DealerServiceInfo_MaterialCostInvoiceType {
            get {
                return Web.Resources.EntityStrings.DealerServiceInfo_MaterialCostInvoiceType;
            }
        }

        public string DealerServiceInfo_RegionType {
            get {
                return Web.Resources.EntityStrings.DealerServiceInfo_RegionType;
            }
        }

        public string DealerServiceInfo_ServicePermission {
            get {
                return Web.Resources.EntityStrings.DealerServiceInfo_ServicePermission;
            }
        }

        public string DealerInvoiceInformation_AbandonerName {
            get {
                return Web.Resources.EntityStrings.DealerInvoiceInformation_AbandonerName;
            }
        }

        public string DealerInvoiceInformation_AbandonTime {
            get {
                return Web.Resources.EntityStrings.DealerInvoiceInformation_AbandonTime;
            }
        }

        public string DealerInvoiceInformation_ApproverName {
            get {
                return Web.Resources.EntityStrings.DealerInvoiceInformation_ApproverName;
            }
        }

        public string DealerInvoiceInformation_ApproverTime {
            get {
                return Web.Resources.EntityStrings.DealerInvoiceInformation_ApproverTime;
            }
        }

        public string DealerInvoiceInformation_ClaimSettlementBillCode {
            get {
                return Web.Resources.EntityStrings.DealerInvoiceInformation_ClaimSettlementBillCode;
            }
        }

        public string DealerInvoiceInformation_CreateTime {
            get {
                return Web.Resources.EntityStrings.DealerInvoiceInformation_CreateTime;
            }
        }

        public string DealerInvoiceInformation_CreatorName {
            get {
                return Web.Resources.EntityStrings.DealerInvoiceInformation_CreatorName;
            }
        }

        public string DealerInvoiceInformation_DealerCode {
            get {
                return Web.Resources.EntityStrings.DealerInvoiceInformation_DealerCode;
            }
        }

        public string DealerInvoiceInformation_DealerName {
            get {
                return Web.Resources.EntityStrings.DealerInvoiceInformation_DealerName;
            }
        }

        public string DealerInvoiceInformation_InitialApproverName {
            get {
                return Web.Resources.EntityStrings.DealerInvoiceInformation_InitialApproverName;
            }
        }

        public string DealerInvoiceInformation_InitialApproverTime {
            get {
                return Web.Resources.EntityStrings.DealerInvoiceInformation_InitialApproverTime;
            }
        }

        public string DealerInvoiceInformation_InvoiceCode {
            get {
                return Web.Resources.EntityStrings.DealerInvoiceInformation_InvoiceCode;
            }
        }

        public string DealerInvoiceInformation_InvoiceDate {
            get {
                return Web.Resources.EntityStrings.DealerInvoiceInformation_InvoiceDate;
            }
        }

        public string DealerInvoiceInformation_InvoiceFee {
            get {
                return Web.Resources.EntityStrings.DealerInvoiceInformation_InvoiceFee;
            }
        }

        public string DealerInvoiceInformation_InvoiceNumber {
            get {
                return Web.Resources.EntityStrings.DealerInvoiceInformation_InvoiceNumber;
            }
        }

        public string DealerInvoiceInformation_InvoiceTaxFee {
            get {
                return Web.Resources.EntityStrings.DealerInvoiceInformation_InvoiceTaxFee;
            }
        }

        public string DealerInvoiceInformation_ModifierName {
            get {
                return Web.Resources.EntityStrings.DealerInvoiceInformation_ModifierName;
            }
        }

        public string DealerInvoiceInformation_ModifyTime {
            get {
                return Web.Resources.EntityStrings.DealerInvoiceInformation_ModifyTime;
            }
        }

        public string DealerInvoiceInformation_RejectName {
            get {
                return Web.Resources.EntityStrings.DealerInvoiceInformation_RejectName;
            }
        }

        public string DealerInvoiceInformation_RejectTime {
            get {
                return Web.Resources.EntityStrings.DealerInvoiceInformation_RejectTime;
            }
        }

        public string DealerInvoiceInformation_Remark {
            get {
                return Web.Resources.EntityStrings.DealerInvoiceInformation_Remark;
            }
        }

        public string DealerInvoiceInformation_Status {
            get {
                return Web.Resources.EntityStrings.DealerInvoiceInformation_Status;
            }
        }

        public string DealerInvoiceInformation_TaxRate {
            get {
                return Web.Resources.EntityStrings.DealerInvoiceInformation_TaxRate;
            }
        }

        public string OriginUsedPartsWarehouse_Name {
            get {
                return Web.Resources.EntityStrings.OriginUsedPartsWarehouse_Name;
            }
        }

        public string UsedPartsDistanceInfor_UsedPartsDistanceType {
            get {
                return Web.Resources.EntityStrings.UsedPartsDistanceInfor_UsedPartsDistanceType;
            }
        }

        public string VehicleInformation_SerialNumber {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_SerialNumber;
            }
        }

        public string UsedPartsDistanceInfor_AbandonerName {
            get {
                return Web.Resources.EntityStrings.UsedPartsDistanceInfor_AbandonerName;
            }
        }

        public string UsedPartsDistanceInfor_AbandonTime {
            get {
                return Web.Resources.EntityStrings.UsedPartsDistanceInfor_AbandonTime;
            }
        }

        public string UsedPartsDistanceInfor_CreateTime {
            get {
                return Web.Resources.EntityStrings.UsedPartsDistanceInfor_CreateTime;
            }
        }

        public string UsedPartsDistanceInfor_CreatorName {
            get {
                return Web.Resources.EntityStrings.UsedPartsDistanceInfor_CreatorName;
            }
        }

        public string UsedPartsDistanceInfor_ModifierName {
            get {
                return Web.Resources.EntityStrings.UsedPartsDistanceInfor_ModifierName;
            }
        }

        public string UsedPartsDistanceInfor_ModifyTime {
            get {
                return Web.Resources.EntityStrings.UsedPartsDistanceInfor_ModifyTime;
            }
        }

        public string UsedPartsDistanceInfor_Remark {
            get {
                return Web.Resources.EntityStrings.UsedPartsDistanceInfor_Remark;
            }
        }

        public string UsedPartsDistanceInfor_ShippingDistance {
            get {
                return Web.Resources.EntityStrings.UsedPartsDistanceInfor_ShippingDistance;
            }
        }

        public string UsedPartsDistanceInfor_Status {
            get {
                return Web.Resources.EntityStrings.UsedPartsDistanceInfor_Status;
            }
        }

        public string Customer_Age {
            get {
                return Web.Resources.EntityStrings.Customer_Age;
            }
        }

        public string Customer_Birthdate {
            get {
                return Web.Resources.EntityStrings.Customer_Birthdate;
            }
        }

        public string Customer_BusinessType {
            get {
                return Web.Resources.EntityStrings.Customer_BusinessType;
            }
        }

        public string Customer_CellPhoneNumber {
            get {
                return Web.Resources.EntityStrings.Customer_CellPhoneNumber;
            }
        }

        public string Customer_DrivingSeniority {
            get {
                return Web.Resources.EntityStrings.Customer_DrivingSeniority;
            }
        }

        public string Customer_EducationLevel {
            get {
                return Web.Resources.EntityStrings.Customer_EducationLevel;
            }
        }

        public string Customer_HomePhoneNumber {
            get {
                return Web.Resources.EntityStrings.Customer_HomePhoneNumber;
            }
        }

        public string Customer_IdDocumentNumber {
            get {
                return Web.Resources.EntityStrings.Customer_IdDocumentNumber;
            }
        }

        public string Customer_IdDocumentType {
            get {
                return Web.Resources.EntityStrings.Customer_IdDocumentType;
            }
        }

        public string Customer_Income {
            get {
                return Web.Resources.EntityStrings.Customer_Income;
            }
        }

        public string Customer_JobPosition {
            get {
                return Web.Resources.EntityStrings.Customer_JobPosition;
            }
        }

        public string Customer_OccupationType {
            get {
                return Web.Resources.EntityStrings.Customer_OccupationType;
            }
        }

        public string Customer_OfficePhoneNumber {
            get {
                return Web.Resources.EntityStrings.Customer_OfficePhoneNumber;
            }
        }

        public string RetainedCustomer_FamilyComposition {
            get {
                return Web.Resources.EntityStrings.RetainedCustomer_FamilyComposition;
            }
        }

        public string RetainedCustomer_FamilyIncome {
            get {
                return Web.Resources.EntityStrings.RetainedCustomer_FamilyIncome;
            }
        }

        public string RetainedCustomer_IfAdAccepted {
            get {
                return Web.Resources.EntityStrings.RetainedCustomer_IfAdAccepted;
            }
        }

        public string RetainedCustomer_IfMarried {
            get {
                return Web.Resources.EntityStrings.RetainedCustomer_IfMarried;
            }
        }

        public string RetainedCustomer_IfTextAccepted {
            get {
                return Web.Resources.EntityStrings.RetainedCustomer_IfTextAccepted;
            }
        }

        public string RetainedCustomer_IfVIP {
            get {
                return Web.Resources.EntityStrings.RetainedCustomer_IfVIP;
            }
        }

        public string RetainedCustomer_IfVisitBackNeeded {
            get {
                return Web.Resources.EntityStrings.RetainedCustomer_IfVisitBackNeeded;
            }
        }

        public string RetainedCustomer_Location {
            get {
                return Web.Resources.EntityStrings.RetainedCustomer_Location;
            }
        }

        public string RetainedCustomer_PlateOwner {
            get {
                return Web.Resources.EntityStrings.RetainedCustomer_PlateOwner;
            }
        }

        public string RetainedCustomer_VIPType {
            get {
                return Web.Resources.EntityStrings.RetainedCustomer_VIPType;
            }
        }

        public string RetainedCustomer_WeddingDay {
            get {
                return Web.Resources.EntityStrings.RetainedCustomer_WeddingDay;
            }
        }

        public string Customer_RegionName {
            get {
                return Web.Resources.EntityStrings.Customer_RegionName;
            }
        }

        public string SupplierOpenAccountApp_AccountInitialDeposit {
            get {
                return Web.Resources.EntityStrings.SupplierOpenAccountApp_AccountInitialDeposit;
            }
        }

        public string SupplierOpenAccountApp_Operator {
            get {
                return Web.Resources.EntityStrings.SupplierOpenAccountApp_Operator;
            }
        }

        public string SupplierOpenAccountApp_Code {
            get {
                return Web.Resources.EntityStrings.SupplierOpenAccountApp_Code;
            }
        }

        public string ServiceProdLineProduct_BrandId {
            get {
                return Web.Resources.EntityStrings.ServiceProdLineProduct_BrandId;
            }
        }

        public string ServiceProdLineProduct_ServiceProductLineCode {
            get {
                return Web.Resources.EntityStrings.ServiceProdLineProduct_ServiceProductLineCode;
            }
        }

        public string ServiceProdLineProduct_ServiceProductLineName {
            get {
                return Web.Resources.EntityStrings.ServiceProdLineProduct_ServiceProductLineName;
            }
        }

        public string ServiceProdLineProduct_SubBrandCode {
            get {
                return Web.Resources.EntityStrings.ServiceProdLineProduct_SubBrandCode;
            }
        }

        public string ServiceProdLineProduct_SubBrandName {
            get {
                return Web.Resources.EntityStrings.ServiceProdLineProduct_SubBrandName;
            }
        }

        public string ServiceProdLineProduct_TerraceCode {
            get {
                return Web.Resources.EntityStrings.ServiceProdLineProduct_TerraceCode;
            }
        }

        public string ServiceProdLineProduct_TerraceName {
            get {
                return Web.Resources.EntityStrings.ServiceProdLineProduct_TerraceName;
            }
        }

        public string ServiceProdLineProduct_VehicleBrandCode {
            get {
                return Web.Resources.EntityStrings.ServiceProdLineProduct_VehicleBrandCode;
            }
        }

        public string ServiceProdLineProduct_VehicleBrandName {
            get {
                return Web.Resources.EntityStrings.ServiceProdLineProduct_VehicleBrandName;
            }
        }

        public string ServiceProdLineProduct_VehicleProductLine {
            get {
                return Web.Resources.EntityStrings.ServiceProdLineProduct_VehicleProductLine;
            }
        }

        public string ServiceProdLineProduct_VehicleProductName {
            get {
                return Web.Resources.EntityStrings.ServiceProdLineProduct_VehicleProductName;
            }
        }

        public string VehicleMaintenancePoilcy_Capacity {
            get {
                return Web.Resources.EntityStrings.VehicleMaintenancePoilcy_Capacity;
            }
        }

        public string VehicleMaintenancePoilcy_DaysLowerLimit {
            get {
                return Web.Resources.EntityStrings.VehicleMaintenancePoilcy_DaysLowerLimit;
            }
        }

        public string VehicleMaintenancePoilcy_DaysUpperLimit {
            get {
                return Web.Resources.EntityStrings.VehicleMaintenancePoilcy_DaysUpperLimit;
            }
        }

        public string VehicleMaintenancePoilcy_IfEmployed {
            get {
                return Web.Resources.EntityStrings.VehicleMaintenancePoilcy_IfEmployed;
            }
        }

        public string VehicleMaintenancePoilcy_MainteType {
            get {
                return Web.Resources.EntityStrings.VehicleMaintenancePoilcy_MainteType;
            }
        }

        public string VehicleMaintenancePoilcy_MaxMileage {
            get {
                return Web.Resources.EntityStrings.VehicleMaintenancePoilcy_MaxMileage;
            }
        }

        public string VehicleMaintenancePoilcy_MinMileage {
            get {
                return Web.Resources.EntityStrings.VehicleMaintenancePoilcy_MinMileage;
            }
        }

        public string VehicleMaintenancePoilcy_VehicleMainteTermCode {
            get {
                return Web.Resources.EntityStrings.VehicleMaintenancePoilcy_VehicleMainteTermCode;
            }
        }

        public string VehicleMaintenancePoilcy_VehicleMainteTermName {
            get {
                return Web.Resources.EntityStrings.VehicleMaintenancePoilcy_VehicleMainteTermName;
            }
        }

        public string VehicleMaintenancePoilcy_WorkingHours {
            get {
                return Web.Resources.EntityStrings.VehicleMaintenancePoilcy_WorkingHours;
            }
        }

        public string VehicleWarrantyCard_Code {
            get {
                return Web.Resources.EntityStrings.VehicleWarrantyCard_Code;
            }
        }

        public string VehicleWarrantyCard_CustomerName {
            get {
                return Web.Resources.EntityStrings.VehicleWarrantyCard_CustomerName;
            }
        }

        public string VehicleWarrantyCard_ProductCode {
            get {
                return Web.Resources.EntityStrings.VehicleWarrantyCard_ProductCode;
            }
        }

        public string VehicleWarrantyCard_VIN {
            get {
                return Web.Resources.EntityStrings.VehicleWarrantyCard_VIN;
            }
        }

        public string VehicleWarrantyCard_WarrantyPolicyCategory {
            get {
                return Web.Resources.EntityStrings.VehicleWarrantyCard_WarrantyPolicyCategory;
            }
        }

        public string VehicleWarrantyCard_WarrantyStartDate {
            get {
                return Web.Resources.EntityStrings.VehicleWarrantyCard_WarrantyStartDate;
            }
        }

        public string VehicleWarrantyCard_IdDocumentNumber {
            get {
                return Web.Resources.EntityStrings.VehicleWarrantyCard_IdDocumentNumber;
            }
        }

        public string VehicleWarrantyCard_IdDocumentType {
            get {
                return Web.Resources.EntityStrings.VehicleWarrantyCard_IdDocumentType;
            }
        }

        public string VehicleWarrantyCard_SalesDate {
            get {
                return Web.Resources.EntityStrings.VehicleWarrantyCard_SalesDate;
            }
        }

        public string DealerServiceExt_VehicleDockingStations {
            get {
                return Web.Resources.EntityStrings.DealerServiceExt_VehicleDockingStations;
            }
        }

        public string DealerServiceExt_VehicleTravelRoute {
            get {
                return Web.Resources.EntityStrings.DealerServiceExt_VehicleTravelRoute;
            }
        }

        public string DealerServiceExt_VehicleUseSpeciality {
            get {
                return Web.Resources.EntityStrings.DealerServiceExt_VehicleUseSpeciality;
            }
        }

        public string RepairOrderMaterialDetail_NewPartsSupplierCode {
            get {
                return Web.Resources.EntityStrings.RepairOrderMaterialDetail_NewPartsSupplierCode;
            }
        }

        public string RepairOrderMaterialDetail_NewPartsSupplierName {
            get {
                return Web.Resources.EntityStrings.RepairOrderMaterialDetail_NewPartsSupplierName;
            }
        }

        public string RepairOrderMaterialDetail_UsedPartsSupplierCode {
            get {
                return Web.Resources.EntityStrings.RepairOrderMaterialDetail_UsedPartsSupplierCode;
            }
        }

        public string RepairOrderMaterialDetail_UsedPartsSupplierName {
            get {
                return Web.Resources.EntityStrings.RepairOrderMaterialDetail_UsedPartsSupplierName;
            }
        }

        public string RepairClaimApplication_FaultyPartsCode {
            get {
                return Web.Resources.EntityStrings.RepairClaimApplication_FaultyPartsCode;
            }
        }

        public string RepairClaimApplication_FaultyPartsName {
            get {
                return Web.Resources.EntityStrings.RepairClaimApplication_FaultyPartsName;
            }
        }

        public string RepairClaimApplication_FaultyPartsSupplierCode {
            get {
                return Web.Resources.EntityStrings.RepairClaimApplication_FaultyPartsSupplierCode;
            }
        }

        public string RepairClaimApplication_FaultyPartsSupplierName {
            get {
                return Web.Resources.EntityStrings.RepairClaimApplication_FaultyPartsSupplierName;
            }
        }

        public string RepairClaimApplication_MalfunctionDescription {
            get {
                return Web.Resources.EntityStrings.RepairClaimApplication_MalfunctionDescription;
            }
        }

        public string RepairClaimApplication_HandleAdvise {
            get {
                return Web.Resources.EntityStrings.RepairClaimApplication_HandleAdvise;
            }
        }

        public string RepairClaimApplication_VehicleMaintenancePoilcyName {
            get {
                return Web.Resources.EntityStrings.RepairClaimApplication_VehicleMaintenancePoilcyName;
            }
        }

        public string RepairClaimApplication_VehicleWarrantyCardCode {
            get {
                return Web.Resources.EntityStrings.RepairClaimApplication_VehicleWarrantyCardCode;
            }
        }

        public string RepairClaimApplication_ApplicationTel {
            get {
                return Web.Resources.EntityStrings.RepairClaimApplication_ApplicationTel;
            }
        }

        public string SsClaimSettlementBill_InvoiceAmountDifference {
            get {
                return Web.Resources.EntityStrings.SsClaimSettlementBill_InvoiceAmountDifference;
            }
        }

        public string PartsSalesOrder_IsDebt {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrder_IsDebt;
            }
        }

        public string PartsSalesOrder_CustomerType {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrder_CustomerType;
            }
        }

        public string ServiceTripClaimApplication_Capacity {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimApplication_Capacity;
            }
        }

        public string ServiceTripClaimApplication_Mileage {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimApplication_Mileage;
            }
        }

        public string ServiceTripClaimApplication_WorkingHours {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimApplication_WorkingHours;
            }
        }

        public string _Common_CheckerNameZS {
            get {
                return Web.Resources.EntityStrings._Common_CheckerNameZS;
            }
        }

        public string _Common_CheckTimeZS {
            get {
                return Web.Resources.EntityStrings._Common_CheckTimeZS;
            }
        }

        public string ServiceActivity_Text_HeadPiece {
            get {
                return Web.Resources.EntityStrings.ServiceActivity_Text_HeadPiece;
            }
        }

        public string ServiceActivity_Text_HeadPiece_Supplier {
            get {
                return Web.Resources.EntityStrings.ServiceActivity_Text_HeadPiece_Supplier;
            }
        }

        public string PartsSalesOrderDetail_IfCanNewPart {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrderDetail_IfCanNewPart;
            }
        }

        public string PartsSalesOrder_WarehouseId {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrder_WarehouseId;
            }
        }

        public string ScheduleExportState_FilePath {
            get {
                return Web.Resources.EntityStrings.ScheduleExportState_FilePath;
            }
        }

        public string ScheduleExportState_ScheduleTime {
            get {
                return Web.Resources.EntityStrings.ScheduleExportState_ScheduleTime;
            }
        }

        public string SubDealer_Address {
            get {
                return Web.Resources.EntityStrings.SubDealer_Address;
            }
        }

        public string DealerPartsRetailOrder_SalesCategoryName {
            get {
                return Web.Resources.EntityStrings.DealerPartsRetailOrder_SalesCategoryName;
            }
        }

        public string UsedPartsDisposalDetail_ClaimBillCode {
            get {
                return Web.Resources.EntityStrings.UsedPartsDisposalDetail_ClaimBillCode;
            }
        }

        public string UsedPartsDisposalDetail_ClaimBillType {
            get {
                return Web.Resources.EntityStrings.UsedPartsDisposalDetail_ClaimBillType;
            }
        }

        public string UsedPartsDisposalDetail_FaultyPartsCode {
            get {
                return Web.Resources.EntityStrings.UsedPartsDisposalDetail_FaultyPartsCode;
            }
        }

        public string UsedPartsDisposalDetail_FaultyPartsSupplierCode {
            get {
                return Web.Resources.EntityStrings.UsedPartsDisposalDetail_FaultyPartsSupplierCode;
            }
        }

        public string UsedPartsDisposalDetail_FaultyPartsSupplierName {
            get {
                return Web.Resources.EntityStrings.UsedPartsDisposalDetail_FaultyPartsSupplierName;
            }
        }

        public string UsedPartsDisposalDetail_IfFaultyParts {
            get {
                return Web.Resources.EntityStrings.UsedPartsDisposalDetail_IfFaultyParts;
            }
        }

        public string UsedPartsDisposalDetail_ResponsibleUnitCode {
            get {
                return Web.Resources.EntityStrings.UsedPartsDisposalDetail_ResponsibleUnitCode;
            }
        }

        public string UsedPartsDisposalDetail_UsedPartsSupplierCode {
            get {
                return Web.Resources.EntityStrings.UsedPartsDisposalDetail_UsedPartsSupplierCode;
            }
        }

        public string UsedPartsDisposalDetail_UsedPartsSupplierName {
            get {
                return Web.Resources.EntityStrings.UsedPartsDisposalDetail_UsedPartsSupplierName;
            }
        }

        public string UsedPartsReturnDetail_ClaimBillType {
            get {
                return Web.Resources.EntityStrings.UsedPartsReturnDetail_ClaimBillType;
            }
        }

        public string UsedPartsReturnDetail_FaultyPartsCode {
            get {
                return Web.Resources.EntityStrings.UsedPartsReturnDetail_FaultyPartsCode;
            }
        }

        public string UsedPartsReturnDetail_IfFaultyParts {
            get {
                return Web.Resources.EntityStrings.UsedPartsReturnDetail_IfFaultyParts;
            }
        }

        public string UsedPartsReturnDetail_ResponsibleUnitCode {
            get {
                return Web.Resources.EntityStrings.UsedPartsReturnDetail_ResponsibleUnitCode;
            }
        }

        public string UsedPartsReturnDetail_FaultyPartsSupplierCode {
            get {
                return Web.Resources.EntityStrings.UsedPartsReturnDetail_FaultyPartsSupplierCode;
            }
        }

        public string UsedPartsReturnDetail_FaultyPartsSupplierName {
            get {
                return Web.Resources.EntityStrings.UsedPartsReturnDetail_FaultyPartsSupplierName;
            }
        }

        public string UsedPartsReturnDetail_UsedPartsSupplierCode {
            get {
                return Web.Resources.EntityStrings.UsedPartsReturnDetail_UsedPartsSupplierCode;
            }
        }

        public string UsedPartsReturnDetail_UsedPartsSupplierName {
            get {
                return Web.Resources.EntityStrings.UsedPartsReturnDetail_UsedPartsSupplierName;
            }
        }

        public string UsedPartsLoanDetail_ClaimBillCode {
            get {
                return Web.Resources.EntityStrings.UsedPartsLoanDetail_ClaimBillCode;
            }
        }

        public string UsedPartsLoanDetail_ClaimBillType {
            get {
                return Web.Resources.EntityStrings.UsedPartsLoanDetail_ClaimBillType;
            }
        }

        public string UsedPartsLoanDetail_FaultyPartsSupplierCode {
            get {
                return Web.Resources.EntityStrings.UsedPartsLoanDetail_FaultyPartsSupplierCode;
            }
        }

        public string UsedPartsLoanDetail_FaultyPartsSupplierName {
            get {
                return Web.Resources.EntityStrings.UsedPartsLoanDetail_FaultyPartsSupplierName;
            }
        }

        public string UsedPartsLoanDetail_IfFaultyParts {
            get {
                return Web.Resources.EntityStrings.UsedPartsLoanDetail_IfFaultyParts;
            }
        }

        public string UsedPartsLoanDetail_ResponsibleUnitCode {
            get {
                return Web.Resources.EntityStrings.UsedPartsLoanDetail_ResponsibleUnitCode;
            }
        }

        public string UsedPartsLoanDetail_UsedPartsSupplierCode {
            get {
                return Web.Resources.EntityStrings.UsedPartsLoanDetail_UsedPartsSupplierCode;
            }
        }

        public string UsedPartsLoanDetail_UsedPartsSupplierName {
            get {
                return Web.Resources.EntityStrings.UsedPartsLoanDetail_UsedPartsSupplierName;
            }
        }

        public string UsedPartsLoanDetail_FaultyPartsCode {
            get {
                return Web.Resources.EntityStrings.UsedPartsLoanDetail_FaultyPartsCode;
            }
        }

        public string UsedPartsTransferDetail_FaultyPartsCode {
            get {
                return Web.Resources.EntityStrings.UsedPartsTransferDetail_FaultyPartsCode;
            }
        }

        public string UsedPartsStock_FaultyPartsCode {
            get {
                return Web.Resources.EntityStrings.UsedPartsStock_FaultyPartsCode;
            }
        }

        public string SsUsedPartsStorage_ClaimBillCreateTime {
            get {
                return Web.Resources.EntityStrings.SsUsedPartsStorage_ClaimBillCreateTime;
            }
        }

        public string UsedPartsLogisticLossDetail_ClaimBillCreateTime {
            get {
                return Web.Resources.EntityStrings.UsedPartsLogisticLossDetail_ClaimBillCreateTime;
            }
        }

        public string UsedPartsShippingDetail_ClaimBillCreateTime {
            get {
                return Web.Resources.EntityStrings.UsedPartsShippingDetail_ClaimBillCreateTime;
            }
        }

        public string PartsPurchaseSettleRef_WarehouseName {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseSettleRef_WarehouseName;
            }
        }

        public string PartsSalesSettlementRef_WarehouseName {
            get {
                return Web.Resources.EntityStrings.PartsSalesSettlementRef_WarehouseName;
            }
        }

        public string PartsSalesRtnSettlementRef_WarehouseName {
            get {
                return Web.Resources.EntityStrings.PartsSalesRtnSettlementRef_WarehouseName;
            }
        }

        public string RepairClaimBill_RepairType {
            get {
                return Web.Resources.EntityStrings.RepairClaimBill_RepairType;
            }
        }

        public string SubDealer_DealerManager {
            get {
                return Web.Resources.EntityStrings.SubDealer_DealerManager;
            }
        }

        public string PartsClaimOrder_PartsRetailOrderCode {
            get {
                return Web.Resources.EntityStrings.PartsClaimOrder_PartsRetailOrderCode;
            }
        }

        public string PartsClaimOrder_ClaimReqTime {
            get {
                return Web.Resources.EntityStrings.PartsClaimOrder_ClaimReqTime;
            }
        }

        public string PartsClaimMaterialDetail_NewPartsSupplierCode {
            get {
                return Web.Resources.EntityStrings.PartsClaimMaterialDetail_NewPartsSupplierCode;
            }
        }

        public string PartsClaimMaterialDetail_NewPartsSupplierName {
            get {
                return Web.Resources.EntityStrings.PartsClaimMaterialDetail_NewPartsSupplierName;
            }
        }

        public string PartsClaimMaterialDetail_PartsSalesOrderCode {
            get {
                return Web.Resources.EntityStrings.PartsClaimMaterialDetail_PartsSalesOrderCode;
            }
        }

        public string PartsClaimMaterialDetail_UsedPartsSupplierCode {
            get {
                return Web.Resources.EntityStrings.PartsClaimMaterialDetail_UsedPartsSupplierCode;
            }
        }

        public string PartsClaimMaterialDetail_UsedPartsSupplierName {
            get {
                return Web.Resources.EntityStrings.PartsClaimMaterialDetail_UsedPartsSupplierName;
            }
        }

        public string WarehouseCostChangeDetail_PriceAfterChange {
            get {
                return Web.Resources.EntityStrings.WarehouseCostChangeDetail_PriceAfterChange;
            }
        }

        public string PartsPurchaseOrder_ApproveTime {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseOrder_ApproveTime;
            }
        }

        public string PartsPurchaseOrder_FirstApproveTime {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseOrder_FirstApproveTime;
            }
        }

        public string SupplierShippingOrder_ConfirmationTime {
            get {
                return Web.Resources.EntityStrings.SupplierShippingOrder_ConfirmationTime;
            }
        }

        public string SupplierShippingOrder_FirstConfirmationTime {
            get {
                return Web.Resources.EntityStrings.SupplierShippingOrder_FirstConfirmationTime;
            }
        }

        public string ServiceTripClaimBill_OutServiceCarUnitPrice {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimBill_OutServiceCarUnitPrice;
            }
        }

        public string ServiceTripClaimBill_OutSubsidyPrice {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimBill_OutSubsidyPrice;
            }
        }

        public string PartsSupplierRelationHistory_EconomicalBatch {
            get {
                return Web.Resources.EntityStrings.PartsSupplierRelationHistory_EconomicalBatch;
            }
        }

        public string PartsSupplierRelationHistory_IsPrimary {
            get {
                return Web.Resources.EntityStrings.PartsSupplierRelationHistory_IsPrimary;
            }
        }

        public string PartsSupplierRelationHistory_MinBatch {
            get {
                return Web.Resources.EntityStrings.PartsSupplierRelationHistory_MinBatch;
            }
        }

        public string PartsSupplierRelationHistory_PartsSalesCategoryName {
            get {
                return Web.Resources.EntityStrings.PartsSupplierRelationHistory_PartsSalesCategoryName;
            }
        }

        public string PartsSupplierRelationHistory_PurchasePercentage {
            get {
                return Web.Resources.EntityStrings.PartsSupplierRelationHistory_PurchasePercentage;
            }
        }

        public string PartsSupplierRelationHistory_Remark {
            get {
                return Web.Resources.EntityStrings.PartsSupplierRelationHistory_Remark;
            }
        }

        public string PartsSupplierRelationHistory_Status {
            get {
                return Web.Resources.EntityStrings.PartsSupplierRelationHistory_Status;
            }
        }

        public string PartsSupplierRelationHistory_SupplierPartCode {
            get {
                return Web.Resources.EntityStrings.PartsSupplierRelationHistory_SupplierPartCode;
            }
        }

        public string PartsSupplierRelationHistory_SupplierPartName {
            get {
                return Web.Resources.EntityStrings.PartsSupplierRelationHistory_SupplierPartName;
            }
        }

        public string SalesRegion_RegionCode {
            get {
                return Web.Resources.EntityStrings.SalesRegion_RegionCode;
            }
        }

        public string SalesRegion_RegionName {
            get {
                return Web.Resources.EntityStrings.SalesRegion_RegionName;
            }
        }

        public string PartsOuterPurchaseChange_RejectComment {
            get {
                return Web.Resources.EntityStrings.PartsOuterPurchaseChange_RejectComment;
            }
        }

        public string PartsOuterPurchaseChange_RejecterName {
            get {
                return Web.Resources.EntityStrings.PartsOuterPurchaseChange_RejecterName;
            }
        }

        public string PartsOuterPurchaseChange_RejectTime {
            get {
                return Web.Resources.EntityStrings.PartsOuterPurchaseChange_RejectTime;
            }
        }

        public string PartsSalesOrder_StopComment {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrder_StopComment;
            }
        }

        public string PartsSalesOrder_RejectComment {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrder_RejectComment;
            }
        }

        public string PartsSalesReturnBill_RejectComment {
            get {
                return Web.Resources.EntityStrings.PartsSalesReturnBill_RejectComment;
            }
        }

        public string PartsOutboundPlan_OrderApproveComment {
            get {
                return Web.Resources.EntityStrings.PartsOutboundPlan_OrderApproveComment;
            }
        }

        public string DealerPartsInventoryBill_CancelReason {
            get {
                return Web.Resources.EntityStrings.DealerPartsInventoryBill_CancelReason;
            }
        }

        public string PersonSalesCenterLink_PersonType {
            get {
                return Web.Resources.EntityStrings.PersonSalesCenterLink_PersonType;
            }
        }

        public string PartsTransferOrder_AbandonComment {
            get {
                return Web.Resources.EntityStrings.PartsTransferOrder_AbandonComment;
            }
        }

        public string PartsOutboundPlan_StopComment {
            get {
                return Web.Resources.EntityStrings.PartsOutboundPlan_StopComment;
            }
        }

        public string PartsOutboundPlan_Stoper {
            get {
                return Web.Resources.EntityStrings.PartsOutboundPlan_Stoper;
            }
        }

        public string PartsOutboundPlan_StopTime {
            get {
                return Web.Resources.EntityStrings.PartsOutboundPlan_StopTime;
            }
        }

        public string PartsSalesRtnSettlement_Discount {
            get {
                return Web.Resources.EntityStrings.PartsSalesRtnSettlement_Discount;
            }
        }

        public string PartsSalesRtnSettlement_DiscountRate {
            get {
                return Web.Resources.EntityStrings.PartsSalesRtnSettlement_DiscountRate;
            }
        }

        public string PreSaleCheckOrder_SerialNumber {
            get {
                return Web.Resources.EntityStrings.PreSaleCheckOrder_SerialNumber;
            }
        }

        public string VehicleInformation_ACMODEL {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_ACMODEL;
            }
        }

        public string VehicleInformation_ACNUMBER {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_ACNUMBER;
            }
        }

        public string VehicleInformation_ARMSUPPORTPUMPMODEL {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_ARMSUPPORTPUMPMODEL;
            }
        }

        public string VehicleInformation_ARMSUPPORTPUMPNUMBER {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_ARMSUPPORTPUMPNUMBER;
            }
        }

        public string VehicleInformation_CCMODEL {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_CCMODEL;
            }
        }

        public string VehicleInformation_CCNUMBER {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_CCNUMBER;
            }
        }

        public string VehicleInformation_CPPMODEL {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_CPPMODEL;
            }
        }

        public string VehicleInformation_CPPNUMBER {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_CPPNUMBER;
            }
        }

        public string VehicleInformation_CSMODEL {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_CSMODEL;
            }
        }

        public string VehicleInformation_CSNUMBER {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_CSNUMBER;
            }
        }

        public string VehicleInformation_DMMODEL {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_DMMODEL;
            }
        }

        public string VehicleInformation_DMNUMBER {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_DMNUMBER;
            }
        }

        public string VehicleInformation_ECCMODEL {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_ECCMODEL;
            }
        }

        public string VehicleInformation_ECCNUMBER {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_ECCNUMBER;
            }
        }

        public string VehicleInformation_GASTANKMODEL {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_GASTANKMODEL;
            }
        }

        public string VehicleInformation_GASTANKNUMBER {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_GASTANKNUMBER;
            }
        }

        public string VehicleInformation_GEARPUMPMODEL {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_GEARPUMPMODEL;
            }
        }

        public string VehicleInformation_GEARPUMPNUMBER {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_GEARPUMPNUMBER;
            }
        }

        public string VehicleInformation_INCLINEDBELTMODEL {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_INCLINEDBELTMODEL;
            }
        }

        public string VehicleInformation_INCLINEDBELTNUMBER {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_INCLINEDBELTNUMBER;
            }
        }

        public string VehicleInformation_IPCMODEL {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_IPCMODEL;
            }
        }

        public string VehicleInformation_IPCNUMBER {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_IPCNUMBER;
            }
        }

        public string VehicleInformation_MCMODEL {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_MCMODEL;
            }
        }

        public string VehicleInformation_MCNUMBER {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_MCNUMBER;
            }
        }

        public string VehicleInformation_MIXERMODEL {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_MIXERMODEL;
            }
        }

        public string VehicleInformation_MIXERNUMBER {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_MIXERNUMBER;
            }
        }

        public string VehicleInformation_MOTORMODEL {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_MOTORMODEL;
            }
        }

        public string VehicleInformation_MOTORNUMBER {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_MOTORNUMBER;
            }
        }

        public string VehicleInformation_MSINOILPUMPMODEL {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_MSINOILPUMPMODEL;
            }
        }

        public string VehicleInformation_MSINOILPUMPNUMBER {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_MSINOILPUMPNUMBER;
            }
        }

        public string VehicleInformation_OILPUMPMODEL {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_OILPUMPMODEL;
            }
        }

        public string VehicleInformation_OILPUMPNUMBER {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_OILPUMPNUMBER;
            }
        }

        public string VehicleInformation_RADIATORMODEL {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_RADIATORMODEL;
            }
        }

        public string VehicleInformation_RADIATORNUMBER {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_RADIATORNUMBER;
            }
        }

        public string VehicleInformation_REDUCERMODEL {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_REDUCERMODEL;
            }
        }

        public string VehicleInformation_REDUCERNUMBER {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_REDUCERNUMBER;
            }
        }

        public string VehicleInformation_ROLLINGREDUCERMODEL {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_ROLLINGREDUCERMODEL;
            }
        }

        public string VehicleInformation_ROLLINGREDUCERNUMBER {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_ROLLINGREDUCERNUMBER;
            }
        }

        public string VehicleInformation_SCMODEL {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_SCMODEL;
            }
        }

        public string VehicleInformation_SCNUMBER {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_SCNUMBER;
            }
        }

        public string VehicleInformation_TRANSFERCASEMODEL {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_TRANSFERCASEMODEL;
            }
        }

        public string VehicleInformation_TRANSFERCASENUMBER {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_TRANSFERCASENUMBER;
            }
        }

        public string PartsSalesReturnBill_StopComment {
            get {
                return Web.Resources.EntityStrings.PartsSalesReturnBill_StopComment;
            }
        }

        public string PartsSalesReturnBill_StopName {
            get {
                return Web.Resources.EntityStrings.PartsSalesReturnBill_StopName;
            }
        }

        public string PartsSalesReturnBill_StopTime {
            get {
                return Web.Resources.EntityStrings.PartsSalesReturnBill_StopTime;
            }
        }

        public string UsedPartsInboundOrder_TotalAmount {
            get {
                return Web.Resources.EntityStrings.UsedPartsInboundOrder_TotalAmount;
            }
        }

        public string UsedPartsInboundOrder_TotalQuantity {
            get {
                return Web.Resources.EntityStrings.UsedPartsInboundOrder_TotalQuantity;
            }
        }

        public string SsClaimSettlementBill_PreSaleAmount {
            get {
                return Web.Resources.EntityStrings.SsClaimSettlementBill_PreSaleAmount;
            }
        }

        public string SsClaimSettlementDetail_PreSaleAmount {
            get {
                return Web.Resources.EntityStrings.SsClaimSettlementDetail_PreSaleAmount;
            }
        }

        public string _Common_ProcessAmount {
            get {
                return Web.Resources.EntityStrings._Common_ProcessAmount;
            }
        }

        public string PartsPurchaseOrder_PlanSource {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseOrder_PlanSource;
            }
        }

        public string SupplierShippingOrder_PlanDeliveryTime {
            get {
                return Web.Resources.EntityStrings.SupplierShippingOrder_PlanDeliveryTime;
            }
        }

        public string PartsPurchaseOrder_AbandonOrStopReason {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseOrder_AbandonOrStopReason;
            }
        }

        public string DealerPartsRetailOrder_CustomerUnit {
            get {
                return Web.Resources.EntityStrings.DealerPartsRetailOrder_CustomerUnit;
            }
        }

        public string PartDeleaveInformation_DeleavePartCode {
            get {
                return Web.Resources.EntityStrings.PartDeleaveInformation_DeleavePartCode;
            }
        }

        public string PartDeleaveInformation_DeleavePartName {
            get {
                return Web.Resources.EntityStrings.PartDeleaveInformation_DeleavePartName;
            }
        }

        public string PartDeleaveInformation_OldPartCode {
            get {
                return Web.Resources.EntityStrings.PartDeleaveInformation_OldPartCode;
            }
        }

        public string PreSaleCheckOrder_Drive {
            get {
                return Web.Resources.EntityStrings.PreSaleCheckOrder_Drive;
            }
        }

        public string PreSaleCheckOrder_InternalCode {
            get {
                return Web.Resources.EntityStrings.PreSaleCheckOrder_InternalCode;
            }
        }

        public string PreSaleCheckOrder_ProductCategory {
            get {
                return Web.Resources.EntityStrings.PreSaleCheckOrder_ProductCategory;
            }
        }

        public string PreSaleCheckOrder_Revision {
            get {
                return Web.Resources.EntityStrings.PreSaleCheckOrder_Revision;
            }
        }

        public string PreSaleCheckOrder_Series {
            get {
                return Web.Resources.EntityStrings.PreSaleCheckOrder_Series;
            }
        }

        public string PreSalesCheckDetail_ClaimUnit {
            get {
                return Web.Resources.EntityStrings.PreSalesCheckDetail_ClaimUnit;
            }
        }

        public string PreSalesCheckDetail_FaultDescription {
            get {
                return Web.Resources.EntityStrings.PreSalesCheckDetail_FaultDescription;
            }
        }

        public string PreSalesCheckDetail_RepairMethod {
            get {
                return Web.Resources.EntityStrings.PreSalesCheckDetail_RepairMethod;
            }
        }

        public string SupplierShippingOrder_PlanSource {
            get {
                return Web.Resources.EntityStrings.SupplierShippingOrder_PlanSource;
            }
        }

        public string RepairMethodStandard_RepairMethodCode {
            get {
                return Web.Resources.EntityStrings.RepairMethodStandard_RepairMethodCode;
            }
        }

        public string RepairMethodStandard_RepairMethodDescription {
            get {
                return Web.Resources.EntityStrings.RepairMethodStandard_RepairMethodDescription;
            }
        }

        public string RepairMethodStandard_RepairMethodName {
            get {
                return Web.Resources.EntityStrings.RepairMethodStandard_RepairMethodName;
            }
        }

        public string FaultModeStandard_FaultModeCode {
            get {
                return Web.Resources.EntityStrings.FaultModeStandard_FaultModeCode;
            }
        }

        public string FaultModeStandard_FaultModeDescription {
            get {
                return Web.Resources.EntityStrings.FaultModeStandard_FaultModeDescription;
            }
        }

        public string FaultModeStandard_FaultModeName {
            get {
                return Web.Resources.EntityStrings.FaultModeStandard_FaultModeName;
            }
        }

        public string Company_LegalRepresentTel {
            get {
                return Web.Resources.EntityStrings.Company_LegalRepresentTel;
            }
        }

        public string QTSDataQuery_Assembly_date {
            get {
                return Web.Resources.EntityStrings.QTSDataQuery_Assembly_date;
            }
        }

        public string QTSDataQuery_Bind_date {
            get {
                return Web.Resources.EntityStrings.QTSDataQuery_Bind_date;
            }
        }

        public string QTSDataQuery_Cscode {
            get {
                return Web.Resources.EntityStrings.QTSDataQuery_Cscode;
            }
        }

        public string QTSDataQuery_Factory {
            get {
                return Web.Resources.EntityStrings.QTSDataQuery_Factory;
            }
        }

        public string QTSDataQuery_Mapid {
            get {
                return Web.Resources.EntityStrings.QTSDataQuery_Mapid;
            }
        }

        public string QTSDataQuery_Mapname {
            get {
                return Web.Resources.EntityStrings.QTSDataQuery_Mapname;
            }
        }

        public string QTSDataQuery_Patch {
            get {
                return Web.Resources.EntityStrings.QTSDataQuery_Patch;
            }
        }

        public string QTSDataQuery_Produceline {
            get {
                return Web.Resources.EntityStrings.QTSDataQuery_Produceline;
            }
        }

        public string QTSDataQuery_Zcbmcode {
            get {
                return Web.Resources.EntityStrings.QTSDataQuery_Zcbmcode;
            }
        }

        public string DealerPartsSalesPriceHistory_DealerSalesPrice {
            get {
                return Web.Resources.EntityStrings.DealerPartsSalesPriceHistory_DealerSalesPrice;
            }
        }

        public string DealerPartsSalesPriceHistory_PartsSalesCategoryName {
            get {
                return Web.Resources.EntityStrings.DealerPartsSalesPriceHistory_PartsSalesCategoryName;
            }
        }

        public string DealerPartsSalesPriceHistory_SparePartCode {
            get {
                return Web.Resources.EntityStrings.DealerPartsSalesPriceHistory_SparePartCode;
            }
        }

        public string DealerPartsSalesPriceHistory_SparePartName {
            get {
                return Web.Resources.EntityStrings.DealerPartsSalesPriceHistory_SparePartName;
            }
        }

        public string DealerPartsSalesPrice_DealerSalesPrice {
            get {
                return Web.Resources.EntityStrings.DealerPartsSalesPrice_DealerSalesPrice;
            }
        }

        public string DealerPartsSalesPrice_PartsSalesCategoryName {
            get {
                return Web.Resources.EntityStrings.DealerPartsSalesPrice_PartsSalesCategoryName;
            }
        }

        public string DealerPartsSalesPrice_SparePartCode {
            get {
                return Web.Resources.EntityStrings.DealerPartsSalesPrice_SparePartCode;
            }
        }

        public string DealerPartsSalesPrice_SparePartName {
            get {
                return Web.Resources.EntityStrings.DealerPartsSalesPrice_SparePartName;
            }
        }

        public string PartsSalesPriceChangeDetail_DealerSalesPrice {
            get {
                return Web.Resources.EntityStrings.PartsSalesPriceChangeDetail_DealerSalesPrice;
            }
        }

        public string MarketABQualityInformation_ApproveCommentHistory {
            get {
                return Web.Resources.EntityStrings.MarketABQualityInformation_ApproveCommentHistory;
            }
        }

        public string MarketABQualityInformation_ApprovertComment {
            get {
                return Web.Resources.EntityStrings.MarketABQualityInformation_ApprovertComment;
            }
        }

        public string MarketABQualityInformation_CargoTypes {
            get {
                return Web.Resources.EntityStrings.MarketABQualityInformation_CargoTypes;
            }
        }

        public string MarketABQualityInformation_Code {
            get {
                return Web.Resources.EntityStrings.MarketABQualityInformation_Code;
            }
        }

        public string MarketABQualityInformation_FailureTimes {
            get {
                return Web.Resources.EntityStrings.MarketABQualityInformation_FailureTimes;
            }
        }

        public string MarketABQualityInformation_FaultDate {
            get {
                return Web.Resources.EntityStrings.MarketABQualityInformation_FaultDate;
            }
        }

        public string MarketABQualityInformation_FaultDescription {
            get {
                return Web.Resources.EntityStrings.MarketABQualityInformation_FaultDescription;
            }
        }

        public string MarketABQualityInformation_FaultReason {
            get {
                return Web.Resources.EntityStrings.MarketABQualityInformation_FaultReason;
            }
        }

        public string MarketABQualityInformation_FaultyPartsCode {
            get {
                return Web.Resources.EntityStrings.MarketABQualityInformation_FaultyPartsCode;
            }
        }

        public string MarketABQualityInformation_FaultyPartsName {
            get {
                return Web.Resources.EntityStrings.MarketABQualityInformation_FaultyPartsName;
            }
        }

        public string MarketABQualityInformation_FaultyPartsSupplierCode {
            get {
                return Web.Resources.EntityStrings.MarketABQualityInformation_FaultyPartsSupplierCode;
            }
        }

        public string MarketABQualityInformation_FaultyPartsSupplierName {
            get {
                return Web.Resources.EntityStrings.MarketABQualityInformation_FaultyPartsSupplierName;
            }
        }

        public string MarketABQualityInformation_HaveMeasures {
            get {
                return Web.Resources.EntityStrings.MarketABQualityInformation_HaveMeasures;
            }
        }

        public string MarketABQualityInformation_InformationPhone {
            get {
                return Web.Resources.EntityStrings.MarketABQualityInformation_InformationPhone;
            }
        }

        public string MarketABQualityInformation_InitialApprovertComment {
            get {
                return Web.Resources.EntityStrings.MarketABQualityInformation_InitialApprovertComment;
            }
        }

        public string MarketABQualityInformation_Mileage {
            get {
                return Web.Resources.EntityStrings.MarketABQualityInformation_Mileage;
            }
        }

        public string MarketABQualityInformation_OutOfFactoryDate {
            get {
                return Web.Resources.EntityStrings.MarketABQualityInformation_OutOfFactoryDate;
            }
        }

        public string MarketABQualityInformation_ProductCategoryCode {
            get {
                return Web.Resources.EntityStrings.MarketABQualityInformation_ProductCategoryCode;
            }
        }

        public string MarketABQualityInformation_RoadConditions {
            get {
                return Web.Resources.EntityStrings.MarketABQualityInformation_RoadConditions;
            }
        }

        public string MarketABQualityInformation_SalesDate {
            get {
                return Web.Resources.EntityStrings.MarketABQualityInformation_SalesDate;
            }
        }

        public string MarketABQualityInformation_Status {
            get {
                return Web.Resources.EntityStrings.MarketABQualityInformation_Status;
            }
        }

        public string MarketABQualityInformation_VIN {
            get {
                return Web.Resources.EntityStrings.MarketABQualityInformation_VIN;
            }
        }

        public string SalesUnit_BusinessCode {
            get {
                return Web.Resources.EntityStrings.SalesUnit_BusinessCode;
            }
        }

        public string SalesUnit_BusinessName {
            get {
                return Web.Resources.EntityStrings.SalesUnit_BusinessName;
            }
        }

        public string BranchSupplierRelation_BusinessCode {
            get {
                return Web.Resources.EntityStrings.BranchSupplierRelation_BusinessCode;
            }
        }

        public string BranchSupplierRelation_BusinessName {
            get {
                return Web.Resources.EntityStrings.BranchSupplierRelation_BusinessName;
            }
        }

        public string DealerServiceInfo_BusinessCode {
            get {
                return Web.Resources.EntityStrings.DealerServiceInfo_BusinessCode;
            }
        }

        public string DealerServiceInfo_BusinessName {
            get {
                return Web.Resources.EntityStrings.DealerServiceInfo_BusinessName;
            }
        }

        public string PreOrder_Code {
            get {
                return Web.Resources.EntityStrings.PreOrder_Code;
            }
        }

        public string PreOrder_DealerCode {
            get {
                return Web.Resources.EntityStrings.PreOrder_DealerCode;
            }
        }

        public string PreOrder_PartsSalesCategoryCode {
            get {
                return Web.Resources.EntityStrings.PreOrder_PartsSalesCategoryCode;
            }
        }

        public string PreOrder_PartName {
            get {
                return Web.Resources.EntityStrings.PreOrder_PartName;
            }
        }

        public string RepairWorkOrder_ArrivalDate {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrder_ArrivalDate;
            }
        }

        public string RepairWorkOrder_ClosedLoopTime {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrder_ClosedLoopTime;
            }
        }

        public string RepairWorkOrder_ComplaintClassification {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrder_ComplaintClassification;
            }
        }

        public string RepairWorkOrder_ConfirmDate {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrder_ConfirmDate;
            }
        }

        public string RepairWorkOrder_Dispatching {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrder_Dispatching;
            }
        }

        public string RepairWorkOrder_EngineerProcessingContent {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrder_EngineerProcessingContent;
            }
        }

        public string RepairWorkOrder_EstimatedFinishingTime {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrder_EstimatedFinishingTime;
            }
        }

        public string RepairWorkOrder_FeedBackNumber {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrder_FeedBackNumber;
            }
        }

        public string RepairWorkOrder_FinishingTime {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrder_FinishingTime;
            }
        }

        public string RepairWorkOrder_IsCallCenterSupport {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrder_IsCallCenterSupport;
            }
        }

        public string RepairWorkOrder_PositionofFault {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrder_PositionofFault;
            }
        }

        public string RepairWorkOrder_WorkOrderContent {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrder_WorkOrderContent;
            }
        }

        public string SupplierExpenseAdjustBill_Code {
            get {
                return Web.Resources.EntityStrings.SupplierExpenseAdjustBill_Code;
            }
        }

        public string SupplierExpenseAdjustBill_DealerCode {
            get {
                return Web.Resources.EntityStrings.SupplierExpenseAdjustBill_DealerCode;
            }
        }

        public string SupplierExpenseAdjustBill_DealerName {
            get {
                return Web.Resources.EntityStrings.SupplierExpenseAdjustBill_DealerName;
            }
        }

        public string SupplierExpenseAdjustBill_DebitOrReplenish {
            get {
                return Web.Resources.EntityStrings.SupplierExpenseAdjustBill_DebitOrReplenish;
            }
        }

        public string SupplierExpenseAdjustBill_SettlementStatus {
            get {
                return Web.Resources.EntityStrings.SupplierExpenseAdjustBill_SettlementStatus;
            }
        }

        public string SupplierExpenseAdjustBill_SourceCode {
            get {
                return Web.Resources.EntityStrings.SupplierExpenseAdjustBill_SourceCode;
            }
        }

        public string SupplierExpenseAdjustBill_SourceType {
            get {
                return Web.Resources.EntityStrings.SupplierExpenseAdjustBill_SourceType;
            }
        }

        public string SupplierExpenseAdjustBill_SupplierAddress {
            get {
                return Web.Resources.EntityStrings.SupplierExpenseAdjustBill_SupplierAddress;
            }
        }

        public string SupplierExpenseAdjustBill_SupplierContactPerson {
            get {
                return Web.Resources.EntityStrings.SupplierExpenseAdjustBill_SupplierContactPerson;
            }
        }

        public string SupplierExpenseAdjustBill_SupplierPhoneNumber {
            get {
                return Web.Resources.EntityStrings.SupplierExpenseAdjustBill_SupplierPhoneNumber;
            }
        }

        public string SupplierExpenseAdjustBill_TransactionAmount {
            get {
                return Web.Resources.EntityStrings.SupplierExpenseAdjustBill_TransactionAmount;
            }
        }

        public string SupplierExpenseAdjustBill_TransactionCategory {
            get {
                return Web.Resources.EntityStrings.SupplierExpenseAdjustBill_TransactionCategory;
            }
        }

        public string SupplierExpenseAdjustBill_TransactionReason {
            get {
                return Web.Resources.EntityStrings.SupplierExpenseAdjustBill_TransactionReason;
            }
        }

        public string SupplierExpenseAdjustBill_SupplierCode {
            get {
                return Web.Resources.EntityStrings.SupplierExpenseAdjustBill_SupplierCode;
            }
        }

        public string SupplierExpenseAdjustBill_SupplierName {
            get {
                return Web.Resources.EntityStrings.SupplierExpenseAdjustBill_SupplierName;
            }
        }

        public string RepairOrderMaterialDetail_MaterialType {
            get {
                return Web.Resources.EntityStrings.RepairOrderMaterialDetail_MaterialType;
            }
        }

        public string DealerMobileNumberList_VideoMobileNumber {
            get {
                return Web.Resources.EntityStrings.DealerMobileNumberList_VideoMobileNumber;
            }
        }

        public string RepairOrder_MVSOutCoordinate {
            get {
                return Web.Resources.EntityStrings.RepairOrder_MVSOutCoordinate;
            }
        }

        public string RepairOrder_MVSOutDistance {
            get {
                return Web.Resources.EntityStrings.RepairOrder_MVSOutDistance;
            }
        }

        public string RepairOrder_MVSOutRange {
            get {
                return Web.Resources.EntityStrings.RepairOrder_MVSOutRange;
            }
        }

        public string RepairOrder_MVSOutTime {
            get {
                return Web.Resources.EntityStrings.RepairOrder_MVSOutTime;
            }
        }

        public string RepairOrder_VideoMobileNumber {
            get {
                return Web.Resources.EntityStrings.RepairOrder_VideoMobileNumber;
            }
        }

        public string PartsInventoryBill_RejectComment {
            get {
                return Web.Resources.EntityStrings.PartsInventoryBill_RejectComment;
            }
        }

        public string MarketABQualityInformation_Drive {
            get {
                return Web.Resources.EntityStrings.MarketABQualityInformation_Drive;
            }
        }

        public string MarketABQualityInformation_FailureSerialNumberDetail {
            get {
                return Web.Resources.EntityStrings.MarketABQualityInformation_FailureSerialNumberDetail;
            }
        }

        public string MarketABQualityInformation_FailureType {
            get {
                return Web.Resources.EntityStrings.MarketABQualityInformation_FailureType;
            }
        }

        public string MarketABQualityInformation_Revision {
            get {
                return Web.Resources.EntityStrings.MarketABQualityInformation_Revision;
            }
        }

        public string MarketABQualityInformation_Series {
            get {
                return Web.Resources.EntityStrings.MarketABQualityInformation_Series;
            }
        }

        public string SupplierClaimSettleBill_Code {
            get {
                return Web.Resources.EntityStrings.SupplierClaimSettleBill_Code;
            }
        }

        public string SupplierClaimSettleBill_ComplementAmount {
            get {
                return Web.Resources.EntityStrings.SupplierClaimSettleBill_ComplementAmount;
            }
        }

        public string SupplierClaimSettleBill_DebitAmount {
            get {
                return Web.Resources.EntityStrings.SupplierClaimSettleBill_DebitAmount;
            }
        }

        public string SupplierClaimSettleBill_FieldServiceExpense {
            get {
                return Web.Resources.EntityStrings.SupplierClaimSettleBill_FieldServiceExpense;
            }
        }

        public string SupplierClaimSettleBill_LaborCost {
            get {
                return Web.Resources.EntityStrings.SupplierClaimSettleBill_LaborCost;
            }
        }

        public string SupplierClaimSettleBill_MaterialCost {
            get {
                return Web.Resources.EntityStrings.SupplierClaimSettleBill_MaterialCost;
            }
        }

        public string SupplierClaimSettleBill_OtherCost {
            get {
                return Web.Resources.EntityStrings.SupplierClaimSettleBill_OtherCost;
            }
        }

        public string SupplierClaimSettleBill_PartsManagementCost {
            get {
                return Web.Resources.EntityStrings.SupplierClaimSettleBill_PartsManagementCost;
            }
        }

        public string SupplierClaimSettleBill_ProductLineType {
            get {
                return Web.Resources.EntityStrings.SupplierClaimSettleBill_ProductLineType;
            }
        }

        public string SupplierClaimSettleBill_SettlementEndTime {
            get {
                return Web.Resources.EntityStrings.SupplierClaimSettleBill_SettlementEndTime;
            }
        }

        public string SupplierClaimSettleBill_SettlementStartTime {
            get {
                return Web.Resources.EntityStrings.SupplierClaimSettleBill_SettlementStartTime;
            }
        }

        public string SupplierClaimSettleBill_SupplierCode {
            get {
                return Web.Resources.EntityStrings.SupplierClaimSettleBill_SupplierCode;
            }
        }

        public string SupplierClaimSettleBill_SupplierName {
            get {
                return Web.Resources.EntityStrings.SupplierClaimSettleBill_SupplierName;
            }
        }

        public string SupplierClaimSettleBill_TotalAmount {
            get {
                return Web.Resources.EntityStrings.SupplierClaimSettleBill_TotalAmount;
            }
        }

        public string SupplierClaimSettleDetail_ComplementAmount {
            get {
                return Web.Resources.EntityStrings.SupplierClaimSettleDetail_ComplementAmount;
            }
        }

        public string SupplierClaimSettleDetail_DebitAmount {
            get {
                return Web.Resources.EntityStrings.SupplierClaimSettleDetail_DebitAmount;
            }
        }

        public string SupplierClaimSettleDetail_FieldServiceExpense {
            get {
                return Web.Resources.EntityStrings.SupplierClaimSettleDetail_FieldServiceExpense;
            }
        }

        public string SupplierClaimSettleDetail_LaborCost {
            get {
                return Web.Resources.EntityStrings.SupplierClaimSettleDetail_LaborCost;
            }
        }

        public string SupplierClaimSettleDetail_MaterialCost {
            get {
                return Web.Resources.EntityStrings.SupplierClaimSettleDetail_MaterialCost;
            }
        }

        public string SupplierClaimSettleDetail_OtherCost {
            get {
                return Web.Resources.EntityStrings.SupplierClaimSettleDetail_OtherCost;
            }
        }

        public string SupplierClaimSettleDetail_PartsManagementCost {
            get {
                return Web.Resources.EntityStrings.SupplierClaimSettleDetail_PartsManagementCost;
            }
        }

        public string SupplierClaimSettleDetail_SourceCode {
            get {
                return Web.Resources.EntityStrings.SupplierClaimSettleDetail_SourceCode;
            }
        }

        public string SupplierClaimSettleDetail_SourceType {
            get {
                return Web.Resources.EntityStrings.SupplierClaimSettleDetail_SourceType;
            }
        }

        public string SupplierClaimSettleDetail_TotalAmount {
            get {
                return Web.Resources.EntityStrings.SupplierClaimSettleDetail_TotalAmount;
            }
        }

        public string RepairOrderSupplierDetail_LaborCost {
            get {
                return Web.Resources.EntityStrings.RepairOrderSupplierDetail_LaborCost;
            }
        }

        public string RepairOrderSupplierDetail_MaterialCost {
            get {
                return Web.Resources.EntityStrings.RepairOrderSupplierDetail_MaterialCost;
            }
        }

        public string RepairOrderSupplierDetail_OtherCost {
            get {
                return Web.Resources.EntityStrings.RepairOrderSupplierDetail_OtherCost;
            }
        }

        public string RepairOrderSupplierDetail_PartsManagementCost {
            get {
                return Web.Resources.EntityStrings.RepairOrderSupplierDetail_PartsManagementCost;
            }
        }

        public string RepairOrderSupplierDetail_TotalAmount {
            get {
                return Web.Resources.EntityStrings.RepairOrderSupplierDetail_TotalAmount;
            }
        }

        public string BranchSupplierRelation_IfHaveOutFee {
            get {
                return Web.Resources.EntityStrings.BranchSupplierRelation_IfHaveOutFee;
            }
        }

        public string BranchSupplierRelation_IfSPByLabor {
            get {
                return Web.Resources.EntityStrings.BranchSupplierRelation_IfSPByLabor;
            }
        }

        public string BranchSupplierRelation_LaborCoefficient {
            get {
                return Web.Resources.EntityStrings.BranchSupplierRelation_LaborCoefficient;
            }
        }

        public string BranchSupplierRelation_LaborUnitPrice {
            get {
                return Web.Resources.EntityStrings.BranchSupplierRelation_LaborUnitPrice;
            }
        }

        public string BranchSupplierRelation_OldPartTransCoefficient {
            get {
                return Web.Resources.EntityStrings.BranchSupplierRelation_OldPartTransCoefficient;
            }
        }

        public string BranchSupplierRelation_OutServiceCarUnitPrice {
            get {
                return Web.Resources.EntityStrings.BranchSupplierRelation_OutServiceCarUnitPrice;
            }
        }

        public string BranchSupplierRelation_OutSubsidyPrice {
            get {
                return Web.Resources.EntityStrings.BranchSupplierRelation_OutSubsidyPrice;
            }
        }

        public string SecondClassStationPlanDetail_Price {
            get {
                return Web.Resources.EntityStrings.SecondClassStationPlanDetail_Price;
            }
        }

        public string SecondClassStationPlan_TotalAmount {
            get {
                return Web.Resources.EntityStrings.SecondClassStationPlan_TotalAmount;
            }
        }

        public string PreSaleCheckOrder_submittedName {
            get {
                return Web.Resources.EntityStrings.PreSaleCheckOrder_submittedName;
            }
        }

        public string PreSaleCheckOrder_SubmitTime {
            get {
                return Web.Resources.EntityStrings.PreSaleCheckOrder_SubmitTime;
            }
        }

        public string DealerServiceInfo_PartReserveAmount {
            get {
                return Web.Resources.EntityStrings.DealerServiceInfo_PartReserveAmount;
            }
        }

        public string DealerServiceInfo_SaleandServicesiteLayout {
            get {
                return Web.Resources.EntityStrings.DealerServiceInfo_SaleandServicesiteLayout;
            }
        }

        public string Warehouse_IsCentralizedPurchase {
            get {
                return Web.Resources.EntityStrings.Warehouse_IsCentralizedPurchase;
            }
        }

        public string AccountPayableHistoryDetail_AfterChangeAmount {
            get {
                return Web.Resources.EntityStrings.AccountPayableHistoryDetail_AfterChangeAmount;
            }
        }

        public string AccountPayableHistoryDetail_BeforeChangeAmount {
            get {
                return Web.Resources.EntityStrings.AccountPayableHistoryDetail_BeforeChangeAmount;
            }
        }

        public string AccountPayableHistoryDetail_Credit {
            get {
                return Web.Resources.EntityStrings.AccountPayableHistoryDetail_Credit;
            }
        }

        public string AccountPayableHistoryDetail_Debit {
            get {
                return Web.Resources.EntityStrings.AccountPayableHistoryDetail_Debit;
            }
        }

        public string CustomerAccountHisDetail_AfterChangeAmount {
            get {
                return Web.Resources.EntityStrings.CustomerAccountHisDetail_AfterChangeAmount;
            }
        }

        public string CustomerAccountHisDetail_BeforeChangeAmount {
            get {
                return Web.Resources.EntityStrings.CustomerAccountHisDetail_BeforeChangeAmount;
            }
        }

        public string CustomerAccountHisDetail_Credit {
            get {
                return Web.Resources.EntityStrings.CustomerAccountHisDetail_Credit;
            }
        }

        public string CustomerAccountHisDetail_Debit {
            get {
                return Web.Resources.EntityStrings.CustomerAccountHisDetail_Debit;
            }
        }

        public string PartsInboundCheckBill_Objid {
            get {
                return Web.Resources.EntityStrings.PartsInboundCheckBill_Objid;
            }
        }

        public string PartsOutboundBill_InterfaceRecordId {
            get {
                return Web.Resources.EntityStrings.PartsOutboundBill_InterfaceRecordId;
            }
        }

        public string PartsShippingOrder_InterfaceRecordId {
            get {
                return Web.Resources.EntityStrings.PartsShippingOrder_InterfaceRecordId;
            }
        }

        public string PartsTransferOrderDetail_PlannPrice {
            get {
                return Web.Resources.EntityStrings.PartsTransferOrderDetail_PlannPrice;
            }
        }

        public string PartsTransferOrder_TotalPlanAmount {
            get {
                return Web.Resources.EntityStrings.PartsTransferOrder_TotalPlanAmount;
            }
        }

        public string SparePart_MInPackingAmount {
            get {
                return Web.Resources.EntityStrings.SparePart_MInPackingAmount;
            }
        }

        public string PartsSalesOrder_SalesUnitOwnerCompanyCode {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrder_SalesUnitOwnerCompanyCode;
            }
        }

        public string PartsSalesOrder_SalesUnitOwnerCompanyName {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrder_SalesUnitOwnerCompanyName;
            }
        }

        public string RepairOrder_VehicleUse {
            get {
                return Web.Resources.EntityStrings.RepairOrder_VehicleUse;
            }
        }

        public string PartsOutboundPlan_ReceivingWarehouseCode {
            get {
                return Web.Resources.EntityStrings.PartsOutboundPlan_ReceivingWarehouseCode;
            }
        }

        public string PartsOutboundPlan_ReceivingWarehouseName {
            get {
                return Web.Resources.EntityStrings.PartsOutboundPlan_ReceivingWarehouseName;
            }
        }

        public string DTMRepairContract_CellNumber {
            get {
                return Web.Resources.EntityStrings.DTMRepairContract_CellNumber;
            }
        }

        public string DTMRepairContract_Code {
            get {
                return Web.Resources.EntityStrings.DTMRepairContract_Code;
            }
        }

        public string DTMRepairContract_CustomerName {
            get {
                return Web.Resources.EntityStrings.DTMRepairContract_CustomerName;
            }
        }

        public string DTMRepairContract_ExpectedFinishTime {
            get {
                return Web.Resources.EntityStrings.DTMRepairContract_ExpectedFinishTime;
            }
        }

        public string DTMRepairContract_PlateNumber {
            get {
                return Web.Resources.EntityStrings.DTMRepairContract_PlateNumber;
            }
        }

        public string DTMRepairContract_RepairType {
            get {
                return Web.Resources.EntityStrings.DTMRepairContract_RepairType;
            }
        }

        public string DTMRepairContract_Vin {
            get {
                return Web.Resources.EntityStrings.DTMRepairContract_Vin;
            }
        }

        public string DTMRepairContract_Address {
            get {
                return Web.Resources.EntityStrings.DTMRepairContract_Address;
            }
        }

        public string DTMRepairContract_ClaimStatus {
            get {
                return Web.Resources.EntityStrings.DTMRepairContract_ClaimStatus;
            }
        }

        public string DTMRepairContract_MaintenanceStatus {
            get {
                return Web.Resources.EntityStrings.DTMRepairContract_MaintenanceStatus;
            }
        }

        public string DTMRepairContract_MaterialFee {
            get {
                return Web.Resources.EntityStrings.DTMRepairContract_MaterialFee;
            }
        }

        public string DTMRepairContract_Mileage {
            get {
                return Web.Resources.EntityStrings.DTMRepairContract_Mileage;
            }
        }

        public string DTMRepairContract_OtherFee {
            get {
                return Web.Resources.EntityStrings.DTMRepairContract_OtherFee;
            }
        }

        public string DTMRepairContract_OutletName {
            get {
                return Web.Resources.EntityStrings.DTMRepairContract_OutletName;
            }
        }

        public string DTMRepairContract_VehicleRepairCellPhone {
            get {
                return Web.Resources.EntityStrings.DTMRepairContract_VehicleRepairCellPhone;
            }
        }

        public string DTMRepairContract_VehicleRepairName {
            get {
                return Web.Resources.EntityStrings.DTMRepairContract_VehicleRepairName;
            }
        }

        public string DTMRepairContract_WorkingFee {
            get {
                return Web.Resources.EntityStrings.DTMRepairContract_WorkingFee;
            }
        }

        public string DTMRepairContractItem_AccountingProperty {
            get {
                return Web.Resources.EntityStrings.DTMRepairContractItem_AccountingProperty;
            }
        }

        public string DTMRepairContractItem_ClaimStatus {
            get {
                return Web.Resources.EntityStrings.DTMRepairContractItem_ClaimStatus;
            }
        }

        public string DTMRepairContractItem_Hours {
            get {
                return Web.Resources.EntityStrings.DTMRepairContractItem_Hours;
            }
        }

        public string DTMRepairContractItem_Price {
            get {
                return Web.Resources.EntityStrings.DTMRepairContractItem_Price;
            }
        }

        public string DTMRepairContractItem_RepairItemCode {
            get {
                return Web.Resources.EntityStrings.DTMRepairContractItem_RepairItemCode;
            }
        }

        public string DTMRepairContractItem_RepairItemName {
            get {
                return Web.Resources.EntityStrings.DTMRepairContractItem_RepairItemName;
            }
        }

        public string DTMRepairContractItem_Repairman {
            get {
                return Web.Resources.EntityStrings.DTMRepairContractItem_Repairman;
            }
        }

        public string DTMRepairContractItem_RepairReason {
            get {
                return Web.Resources.EntityStrings.DTMRepairContractItem_RepairReason;
            }
        }

        public string DTMRepairContractItem_WorkingFee {
            get {
                return Web.Resources.EntityStrings.DTMRepairContractItem_WorkingFee;
            }
        }

        public string DTMRepairContract_CompletionTime {
            get {
                return Web.Resources.EntityStrings.DTMRepairContract_CompletionTime;
            }
        }

        public string DTMRepairContract_CustomerComplaint {
            get {
                return Web.Resources.EntityStrings.DTMRepairContract_CustomerComplaint;
            }
        }

        public string DTMRepairContract_Description {
            get {
                return Web.Resources.EntityStrings.DTMRepairContract_Description;
            }
        }

        public string DTMRepairContract_EngineCode {
            get {
                return Web.Resources.EntityStrings.DTMRepairContract_EngineCode;
            }
        }

        public string DTMRepairContract_FuelAmount {
            get {
                return Web.Resources.EntityStrings.DTMRepairContract_FuelAmount;
            }
        }

        public string DTMRepairContract_IsOutService {
            get {
                return Web.Resources.EntityStrings.DTMRepairContract_IsOutService;
            }
        }

        public string DTMRepairContract_RepairTime {
            get {
                return Web.Resources.EntityStrings.DTMRepairContract_RepairTime;
            }
        }

        public string DTMRepairContract_TechnicalServiceActivityCode {
            get {
                return Web.Resources.EntityStrings.DTMRepairContract_TechnicalServiceActivityCode;
            }
        }

        public string DTMRepairContract_VehicleObjects {
            get {
                return Web.Resources.EntityStrings.DTMRepairContract_VehicleObjects;
            }
        }

        public string DTMRepairContractMaterial_AccountingProperty {
            get {
                return Web.Resources.EntityStrings.DTMRepairContractMaterial_AccountingProperty;
            }
        }

        public string DTMRepairContractMaterial_Amount {
            get {
                return Web.Resources.EntityStrings.DTMRepairContractMaterial_Amount;
            }
        }

        public string DTMRepairContractMaterial_ClaimStatus {
            get {
                return Web.Resources.EntityStrings.DTMRepairContractMaterial_ClaimStatus;
            }
        }

        public string DTMRepairContractMaterial_MaterialFee {
            get {
                return Web.Resources.EntityStrings.DTMRepairContractMaterial_MaterialFee;
            }
        }

        public string DTMRepairContractMaterial_MeasureUnitName {
            get {
                return Web.Resources.EntityStrings.DTMRepairContractMaterial_MeasureUnitName;
            }
        }

        public string DTMRepairContractMaterial_OldSparePartSupplierCode {
            get {
                return Web.Resources.EntityStrings.DTMRepairContractMaterial_OldSparePartSupplierCode;
            }
        }

        public string DTMRepairContractMaterial_OldSparePartSupplierName {
            get {
                return Web.Resources.EntityStrings.DTMRepairContractMaterial_OldSparePartSupplierName;
            }
        }

        public string DTMRepairContractMaterial_Price {
            get {
                return Web.Resources.EntityStrings.DTMRepairContractMaterial_Price;
            }
        }

        public string DTMRepairContractMaterial_SparePartCode {
            get {
                return Web.Resources.EntityStrings.DTMRepairContractMaterial_SparePartCode;
            }
        }

        public string DTMRepairContractMaterial_SparePartName {
            get {
                return Web.Resources.EntityStrings.DTMRepairContractMaterial_SparePartName;
            }
        }

        public string DTMRepairContractMaterial_UsedPartsBarCode {
            get {
                return Web.Resources.EntityStrings.DTMRepairContractMaterial_UsedPartsBarCode;
            }
        }

        public string Dtp_vehicleinformation_HANDLESTATUS {
            get {
                return Web.Resources.EntityStrings.Dtp_vehicleinformation_HANDLESTATUS;
            }
        }

        public string Dtp_vehicleinformation_SYSCODE {
            get {
                return Web.Resources.EntityStrings.Dtp_vehicleinformation_SYSCODE;
            }
        }

        public string Dtp_vehicleinformation_THEDATE {
            get {
                return Web.Resources.EntityStrings.Dtp_vehicleinformation_THEDATE;
            }
        }

        public string Dtp_vehicleinformation_VIN {
            get {
                return Web.Resources.EntityStrings.Dtp_vehicleinformation_VIN;
            }
        }

        public string PartsTransferOrder_OriginalBillCode {
            get {
                return Web.Resources.EntityStrings.PartsTransferOrder_OriginalBillCode;
            }
        }

        public string PartsPurchaseOrder_InStatus {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseOrder_InStatus;
            }
        }

        public string PartsInboundPlan_OriginalRequirementBillCode {
            get {
                return Web.Resources.EntityStrings.PartsInboundPlan_OriginalRequirementBillCode;
            }
        }

        public string SAPInvoiceInfo_BusinessType {
            get {
                return Web.Resources.EntityStrings.SAPInvoiceInfo_BusinessType;
            }
        }

        public string SAPInvoiceInfo_Code {
            get {
                return Web.Resources.EntityStrings.SAPInvoiceInfo_Code;
            }
        }

        public string SAPInvoiceInfo_CreateTime {
            get {
                return Web.Resources.EntityStrings.SAPInvoiceInfo_CreateTime;
            }
        }

        public string SAPInvoiceInfo_InvoiceNumber {
            get {
                return Web.Resources.EntityStrings.SAPInvoiceInfo_InvoiceNumber;
            }
        }

        public string SAPInvoiceInfo_Message {
            get {
                return Web.Resources.EntityStrings.SAPInvoiceInfo_Message;
            }
        }

        public string SAPInvoiceInfo_Status {
            get {
                return Web.Resources.EntityStrings.SAPInvoiceInfo_Status;
            }
        }

        public string SAPInvoiceInfo_InvoiceTax {
            get {
                return Web.Resources.EntityStrings.SAPInvoiceInfo_InvoiceTax;
            }
        }

        public string SAPInvoiceInfo_InvoiceAmount {
            get {
                return Web.Resources.EntityStrings.SAPInvoiceInfo_InvoiceAmount;
            }
        }

        public string PMSEPCBRANDMAPPING_EPCBrandCode {
            get {
                return Web.Resources.EntityStrings.PMSEPCBRANDMAPPING_EPCBrandCode;
            }
        }

        public string PMSEPCBRANDMAPPING_EPCBrandId {
            get {
                return Web.Resources.EntityStrings.PMSEPCBRANDMAPPING_EPCBrandId;
            }
        }

        public string PMSEPCBRANDMAPPING_EPCBrandName {
            get {
                return Web.Resources.EntityStrings.PMSEPCBRANDMAPPING_EPCBrandName;
            }
        }

        public string PMSEPCBRANDMAPPING_PMSBrandCode {
            get {
                return Web.Resources.EntityStrings.PMSEPCBRANDMAPPING_PMSBrandCode;
            }
        }

        public string PMSEPCBRANDMAPPING_PMSBrandName {
            get {
                return Web.Resources.EntityStrings.PMSEPCBRANDMAPPING_PMSBrandName;
            }
        }

        public string PMSEPCBRANDMAPPING_Syscode {
            get {
                return Web.Resources.EntityStrings.PMSEPCBRANDMAPPING_Syscode;
            }
        }

        public string PartsSalePriceIncreaseRate_GroupCode {
            get {
                return Web.Resources.EntityStrings.PartsSalePriceIncreaseRate_GroupCode;
            }
        }

        public string PartsSalePriceIncreaseRate_GroupName {
            get {
                return Web.Resources.EntityStrings.PartsSalePriceIncreaseRate_GroupName;
            }
        }

        public string PartsSalePriceIncreaseRate_IncreaseRate {
            get {
                return Web.Resources.EntityStrings.PartsSalePriceIncreaseRate_IncreaseRate;
            }
        }

        public string PartsSalePriceIncreaseRate_IsDefault {
            get {
                return Web.Resources.EntityStrings.PartsSalePriceIncreaseRate_IsDefault;
            }
        }

        public string PMSDMSBRANDMAPPING_DMSBrandCode {
            get {
                return Web.Resources.EntityStrings.PMSDMSBRANDMAPPING_DMSBrandCode;
            }
        }

        public string PMSDMSBRANDMAPPING_DMSBrandId {
            get {
                return Web.Resources.EntityStrings.PMSDMSBRANDMAPPING_DMSBrandId;
            }
        }

        public string PMSDMSBRANDMAPPING_DMSBrandName {
            get {
                return Web.Resources.EntityStrings.PMSDMSBRANDMAPPING_DMSBrandName;
            }
        }

        public string PMSDMSBRANDMAPPING_PMSBrandCode {
            get {
                return Web.Resources.EntityStrings.PMSDMSBRANDMAPPING_PMSBrandCode;
            }
        }

        public string PMSDMSBRANDMAPPING_PMSBrandId {
            get {
                return Web.Resources.EntityStrings.PMSDMSBRANDMAPPING_PMSBrandId;
            }
        }

        public string PMSDMSBRANDMAPPING_PMSBrandName {
            get {
                return Web.Resources.EntityStrings.PMSDMSBRANDMAPPING_PMSBrandName;
            }
        }

        public string PMSDMSBRANDMAPPING_Syscode {
            get {
                return Web.Resources.EntityStrings.PMSDMSBRANDMAPPING_Syscode;
            }
        }

        public string PartsInboundCheckBill_GPMSPurOrderCode {
            get {
                return Web.Resources.EntityStrings.PartsInboundCheckBill_GPMSPurOrderCode;
            }
        }

        public string PartsPurchaseOrder_GPMSPurOrderCode {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseOrder_GPMSPurOrderCode;
            }
        }

        public string PartsOutboundBill_ContractCode {
            get {
                return Web.Resources.EntityStrings.PartsOutboundBill_ContractCode;
            }
        }

        public string PartsOutboundBill_GPMSPurOrderCode {
            get {
                return Web.Resources.EntityStrings.PartsOutboundBill_GPMSPurOrderCode;
            }
        }

        public string PartsOutboundBill_OutboundPackPlanCode {
            get {
                return Web.Resources.EntityStrings.PartsOutboundBill_OutboundPackPlanCode;
            }
        }

        public string PartsTransferOrder_GPMSPurOrderCode {
            get {
                return Web.Resources.EntityStrings.PartsTransferOrder_GPMSPurOrderCode;
            }
        }

        public string VehicleMainteProductCategory_ProductCategoryCode {
            get {
                return Web.Resources.EntityStrings.VehicleMainteProductCategory_ProductCategoryCode;
            }
        }

        public string VehicleMainteProductCategory_ProductCategoryName {
            get {
                return Web.Resources.EntityStrings.VehicleMainteProductCategory_ProductCategoryName;
            }
        }

        public string SsClaimSettlementBill_SettleMethods {
            get {
                return Web.Resources.EntityStrings.SsClaimSettlementBill_SettleMethods;
            }
        }

        public string SsClaimSettlementBill_SettleType {
            get {
                return Web.Resources.EntityStrings.SsClaimSettlementBill_SettleType;
            }
        }

        public string SsClaimSettleInstruction_SettleMethods {
            get {
                return Web.Resources.EntityStrings.SsClaimSettleInstruction_SettleMethods;
            }
        }

        public string Notification_PublishDep {
            get {
                return Web.Resources.EntityStrings.Notification_PublishDep;
            }
        }

        public string RepairClaimApplication_Applicant {
            get {
                return Web.Resources.EntityStrings.RepairClaimApplication_Applicant;
            }
        }

        public string PartsSupplierRelation_ArrivalReplenishmentCycle {
            get {
                return Web.Resources.EntityStrings.PartsSupplierRelation_ArrivalReplenishmentCycle;
            }
        }

        public string PartsSupplierRelation_EmergencyArrivalPeriod {
            get {
                return Web.Resources.EntityStrings.PartsSupplierRelation_EmergencyArrivalPeriod;
            }
        }

        public string PartsSupplierRelation_MonthlyArrivalPeriod {
            get {
                return Web.Resources.EntityStrings.PartsSupplierRelation_MonthlyArrivalPeriod;
            }
        }

        public string SparePart_GroupABCCategory {
            get {
                return Web.Resources.EntityStrings.SparePart_GroupABCCategory;
            }
        }

        public string PartsShippingOrder_LogisticArrivalDate {
            get {
                return Web.Resources.EntityStrings.PartsShippingOrder_LogisticArrivalDate;
            }
        }

        public string SupplierShippingOrder_LogisticArrivalDate {
            get {
                return Web.Resources.EntityStrings.SupplierShippingOrder_LogisticArrivalDate;
            }
        }

        public string PartsSalesOrder_City {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrder_City;
            }
        }

        public string PartsSalesOrder_Province {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrder_Province;
            }
        }

        public string VehicleMainteProductCategory_MaterialCost {
            get {
                return Web.Resources.EntityStrings.VehicleMainteProductCategory_MaterialCost;
            }
        }

        public string VehicleMainteProductCategory_Type1 {
            get {
                return Web.Resources.EntityStrings.VehicleMainteProductCategory_Type1;
            }
        }

        public string VehicleMainteProductCategory_Price {
            get {
                return Web.Resources.EntityStrings.VehicleMainteProductCategory_Price;
            }
        }

        public string VehicleMainteProductCategory_Price2 {
            get {
                return Web.Resources.EntityStrings.VehicleMainteProductCategory_Price2;
            }
        }

        public string VehicleMainteProductCategory_Quantity {
            get {
                return Web.Resources.EntityStrings.VehicleMainteProductCategory_Quantity;
            }
        }

        public string VehicleMainteProductCategory_Quantity2 {
            get {
                return Web.Resources.EntityStrings.VehicleMainteProductCategory_Quantity2;
            }
        }

        public string VehicleMainteProductCategory_Type1Amount {
            get {
                return Web.Resources.EntityStrings.VehicleMainteProductCategory_Type1Amount;
            }
        }

        public string VehicleMainteProductCategory_Type2 {
            get {
                return Web.Resources.EntityStrings.VehicleMainteProductCategory_Type2;
            }
        }

        public string VehicleMainteProductCategory_Type2Amount {
            get {
                return Web.Resources.EntityStrings.VehicleMainteProductCategory_Type2Amount;
            }
        }

        public string LocketUnitQuery_Code {
            get {
                return Web.Resources.EntityStrings.LocketUnitQuery_Code;
            }
        }

        public string LocketUnitQuery_CounterpartCompanyCode {
            get {
                return Web.Resources.EntityStrings.LocketUnitQuery_CounterpartCompanyCode;
            }
        }

        public string LocketUnitQuery_CounterpartCompanyName {
            get {
                return Web.Resources.EntityStrings.LocketUnitQuery_CounterpartCompanyName;
            }
        }

        public string LocketUnitQuery_LocketQty {
            get {
                return Web.Resources.EntityStrings.LocketUnitQuery_LocketQty;
            }
        }

        public string LocketUnitQuery_OutboundType {
            get {
                return Web.Resources.EntityStrings.LocketUnitQuery_OutboundType;
            }
        }

        public string LocketUnitQuery_SparePartCode {
            get {
                return Web.Resources.EntityStrings.LocketUnitQuery_SparePartCode;
            }
        }

        public string LocketUnitQuery_SparePartName {
            get {
                return Web.Resources.EntityStrings.LocketUnitQuery_SparePartName;
            }
        }

        public string LocketUnitQuery_Status {
            get {
                return Web.Resources.EntityStrings.LocketUnitQuery_Status;
            }
        }

        public string LocketUnitQuery_WarehouseCode {
            get {
                return Web.Resources.EntityStrings.LocketUnitQuery_WarehouseCode;
            }
        }

        public string LocketUnitQuery_WarehouseName {
            get {
                return Web.Resources.EntityStrings.LocketUnitQuery_WarehouseName;
            }
        }

        public string RepairOrder_DealerCode {
            get {
                return Web.Resources.EntityStrings.RepairOrder_DealerCode;
            }
        }

        public string PreOrder_PartCode {
            get {
                return Web.Resources.EntityStrings.PreOrder_PartCode;
            }
        }

        public string WMSLabelPrintingSerial_PartCode {
            get {
                return Web.Resources.EntityStrings.WMSLabelPrintingSerial_PartCode;
            }
        }

        public string WMSLabelPrintingSerial_PartName {
            get {
                return Web.Resources.EntityStrings.WMSLabelPrintingSerial_PartName;
            }
        }

        public string WMSLabelPrintingSerial_Serial {
            get {
                return Web.Resources.EntityStrings.WMSLabelPrintingSerial_Serial;
            }
        }

        public string RepairClaimApplication_ProductCategoryName {
            get {
                return Web.Resources.EntityStrings.RepairClaimApplication_ProductCategoryName;
            }
        }

        public string UsedPartsInboundOrder_SettlementStatus {
            get {
                return Web.Resources.EntityStrings.UsedPartsInboundOrder_SettlementStatus;
            }
        }

        public string MarketABQualityInformation_Emission {
            get {
                return Web.Resources.EntityStrings.MarketABQualityInformation_Emission;
            }
        }

        public string MarketABQualityInformation_FailureMode {
            get {
                return Web.Resources.EntityStrings.MarketABQualityInformation_FailureMode;
            }
        }

        public string InternalAcquisitionDetail_SalesPrice {
            get {
                return Web.Resources.EntityStrings.InternalAcquisitionDetail_SalesPrice;
            }
        }

        public string InternalAllocationDetail_SalesPrice {
            get {
                return Web.Resources.EntityStrings.InternalAllocationDetail_SalesPrice;
            }
        }

        public string SupplierPlanArrear_SupplierCode {
            get {
                return Web.Resources.EntityStrings.SupplierPlanArrear_SupplierCode;
            }
        }

        public string SupplierPlanArrear_SupplierCompanyCode {
            get {
                return Web.Resources.EntityStrings.SupplierPlanArrear_SupplierCompanyCode;
            }
        }

        public string SupplierPlanArrear_SupplierCompanyName {
            get {
                return Web.Resources.EntityStrings.SupplierPlanArrear_SupplierCompanyName;
            }
        }

        public string SupplierPlanArrear_PlannedAmountOwed {
            get {
                return Web.Resources.EntityStrings.SupplierPlanArrear_PlannedAmountOwed;
            }
        }

        public string SupplierPreApprovedLoan_PreApprovedCode {
            get {
                return Web.Resources.EntityStrings.SupplierPreApprovedLoan_PreApprovedCode;
            }
        }

        public string SupplierPreAppLoanDetail_ApprovalAmount {
            get {
                return Web.Resources.EntityStrings.SupplierPreAppLoanDetail_ApprovalAmount;
            }
        }

        public string SupplierPreAppLoanDetail_BankCode {
            get {
                return Web.Resources.EntityStrings.SupplierPreAppLoanDetail_BankCode;
            }
        }

        public string SupplierPreAppLoanDetail_BankName {
            get {
                return Web.Resources.EntityStrings.SupplierPreAppLoanDetail_BankName;
            }
        }

        public string SupplierPreAppLoanDetail_ExceedAmount {
            get {
                return Web.Resources.EntityStrings.SupplierPreAppLoanDetail_ExceedAmount;
            }
        }

        public string SupplierPreAppLoanDetail_IsOverstock {
            get {
                return Web.Resources.EntityStrings.SupplierPreAppLoanDetail_IsOverstock;
            }
        }

        public string SupplierPreAppLoanDetail_SupplierCode {
            get {
                return Web.Resources.EntityStrings.SupplierPreAppLoanDetail_SupplierCode;
            }
        }

        public string SupplierPreAppLoanDetail_SupplierCompanyCode {
            get {
                return Web.Resources.EntityStrings.SupplierPreAppLoanDetail_SupplierCompanyCode;
            }
        }

        public string SupplierPreAppLoanDetail_SupplierCompanyName {
            get {
                return Web.Resources.EntityStrings.SupplierPreAppLoanDetail_SupplierCompanyName;
            }
        }

        public string SupplierPreApprovedLoan_ApprovalAmount {
            get {
                return Web.Resources.EntityStrings.SupplierPreApprovedLoan_ApprovalAmount;
            }
        }

        public string SupplierPreApprovedLoan_ExceedAmount {
            get {
                return Web.Resources.EntityStrings.SupplierPreApprovedLoan_ExceedAmount;
            }
        }

        public string PartsBranch_PartsAttribution {
            get {
                return Web.Resources.EntityStrings.PartsBranch_PartsAttribution;
            }
        }

        public string PartsOutboundPlan_ShippingMethod {
            get {
                return Web.Resources.EntityStrings.PartsOutboundPlan_ShippingMethod;
            }
        }

        public string SupClaimSettleInstruction_BranchCode {
            get {
                return Web.Resources.EntityStrings.SupClaimSettleInstruction_BranchCode;
            }
        }

        public string SupClaimSettleInstruction_BranchName {
            get {
                return Web.Resources.EntityStrings.SupClaimSettleInstruction_BranchName;
            }
        }

        public string SupClaimSettleInstruction_SettlementEndTime {
            get {
                return Web.Resources.EntityStrings.SupClaimSettleInstruction_SettlementEndTime;
            }
        }

        public string SupClaimSettleInstruction_SettlementStartTime {
            get {
                return Web.Resources.EntityStrings.SupClaimSettleInstruction_SettlementStartTime;
            }
        }

        public string SupClaimSettleInstruction_ExecutionResult {
            get {
                return Web.Resources.EntityStrings.SupClaimSettleInstruction_ExecutionResult;
            }
        }

        public string SupClaimSettleInstruction_ExecutionTime {
            get {
                return Web.Resources.EntityStrings.SupClaimSettleInstruction_ExecutionTime;
            }
        }

        public string SupplierPreAppLoanDetail_ActualPaidAmount {
            get {
                return Web.Resources.EntityStrings.SupplierPreAppLoanDetail_ActualPaidAmount;
            }
        }

        public string SupplierPreApprovedLoan_ActualPaidAmount {
            get {
                return Web.Resources.EntityStrings.SupplierPreApprovedLoan_ActualPaidAmount;
            }
        }

        public string DealerServiceInfoHistory_AAccreditTime {
            get {
                return Web.Resources.EntityStrings.DealerServiceInfoHistory_AAccreditTime;
            }
        }

        public string DealerServiceInfoHistory_AArea {
            get {
                return Web.Resources.EntityStrings.DealerServiceInfoHistory_AArea;
            }
        }

        public string DealerServiceInfoHistory_ABusinessCode {
            get {
                return Web.Resources.EntityStrings.DealerServiceInfoHistory_ABusinessCode;
            }
        }

        public string DealerServiceInfoHistory_ABusinessDivision {
            get {
                return Web.Resources.EntityStrings.DealerServiceInfoHistory_ABusinessDivision;
            }
        }

        public string DealerServiceInfoHistory_ABusinessName {
            get {
                return Web.Resources.EntityStrings.DealerServiceInfoHistory_ABusinessName;
            }
        }

        public string DealerServiceInfoHistory_AFax {
            get {
                return Web.Resources.EntityStrings.DealerServiceInfoHistory_AFax;
            }
        }

        public string DealerServiceInfoHistory_AFix {
            get {
                return Web.Resources.EntityStrings.DealerServiceInfoHistory_AFix;
            }
        }

        public string DealerServiceInfoHistory_AHotLine {
            get {
                return Web.Resources.EntityStrings.DealerServiceInfoHistory_AHotLine;
            }
        }

        public string DealerServiceInfoHistory_AInvoiceTypeInvoiceRatio {
            get {
                return Web.Resources.EntityStrings.DealerServiceInfoHistory_AInvoiceTypeInvoiceRatio;
            }
        }

        public string DealerServiceInfoHistory_AIsOnDuty {
            get {
                return Web.Resources.EntityStrings.DealerServiceInfoHistory_AIsOnDuty;
            }
        }

        public string DealerServiceInfoHistory_ALaborCostCostInvoiceType {
            get {
                return Web.Resources.EntityStrings.DealerServiceInfoHistory_ALaborCostCostInvoiceType;
            }
        }

        public string DealerServiceInfoHistory_ALaborCostInvoiceRatio {
            get {
                return Web.Resources.EntityStrings.DealerServiceInfoHistory_ALaborCostInvoiceRatio;
            }
        }

        public string DealerServiceInfoHistory_AMaterialCostInvoiceType {
            get {
                return Web.Resources.EntityStrings.DealerServiceInfoHistory_AMaterialCostInvoiceType;
            }
        }

        public string DealerServiceInfoHistory_AOutServiceradii {
            get {
                return Web.Resources.EntityStrings.DealerServiceInfoHistory_AOutServiceradii;
            }
        }

        public string DealerServiceInfoHistory_APartReserveAmount {
            get {
                return Web.Resources.EntityStrings.DealerServiceInfoHistory_APartReserveAmount;
            }
        }

        public string DealerServiceInfoHistory_ARegionType {
            get {
                return Web.Resources.EntityStrings.DealerServiceInfoHistory_ARegionType;
            }
        }

        public string DealerServiceInfoHistory_ARemark {
            get {
                return Web.Resources.EntityStrings.DealerServiceInfoHistory_ARemark;
            }
        }

        public string DealerServiceInfoHistory_ARepairAuthorityGrade {
            get {
                return Web.Resources.EntityStrings.DealerServiceInfoHistory_ARepairAuthorityGrade;
            }
        }

        public string DealerServiceInfoHistory_ASaleandServicesiteLayout {
            get {
                return Web.Resources.EntityStrings.DealerServiceInfoHistory_ASaleandServicesiteLayout;
            }
        }

        public string DealerServiceInfoHistory_AServicePermission {
            get {
                return Web.Resources.EntityStrings.DealerServiceInfoHistory_AServicePermission;
            }
        }

        public string DealerServiceInfoHistory_AServiceStationType {
            get {
                return Web.Resources.EntityStrings.DealerServiceInfoHistory_AServiceStationType;
            }
        }

        public string DealerServiceInfoHistory_AStatus {
            get {
                return Web.Resources.EntityStrings.DealerServiceInfoHistory_AStatus;
            }
        }

        public string DealerServiceInfoHistory_BusinessCode {
            get {
                return Web.Resources.EntityStrings.DealerServiceInfoHistory_BusinessCode;
            }
        }

        public string DealerServiceInfoHistory_BusinessName {
            get {
                return Web.Resources.EntityStrings.DealerServiceInfoHistory_BusinessName;
            }
        }

        public string DealerServiceInfoHistory_MaterialCostInvoiceType {
            get {
                return Web.Resources.EntityStrings.DealerServiceInfoHistory_MaterialCostInvoiceType;
            }
        }

        public string DealerServiceInfoHistory_PartReserveAmount {
            get {
                return Web.Resources.EntityStrings.DealerServiceInfoHistory_PartReserveAmount;
            }
        }

        public string DealerServiceInfoHistory_SaleandServicesiteLayout {
            get {
                return Web.Resources.EntityStrings.DealerServiceInfoHistory_SaleandServicesiteLayout;
            }
        }

        public string DealerHistory_AAndBusinessAreas {
            get {
                return Web.Resources.EntityStrings.DealerHistory_AAndBusinessAreas;
            }
        }

        public string DealerHistory_ABankAccount {
            get {
                return Web.Resources.EntityStrings.DealerHistory_ABankAccount;
            }
        }

        public string DealerHistory_ABankName {
            get {
                return Web.Resources.EntityStrings.DealerHistory_ABankName;
            }
        }

        public string DealerHistory_ABrandScope {
            get {
                return Web.Resources.EntityStrings.DealerHistory_ABrandScope;
            }
        }

        public string DealerHistory_ABuildTime {
            get {
                return Web.Resources.EntityStrings.DealerHistory_ABuildTime;
            }
        }

        public string DealerHistory_ACompetitiveBrandScope {
            get {
                return Web.Resources.EntityStrings.DealerHistory_ACompetitiveBrandScope;
            }
        }

        public string DealerHistory_AContactNumber {
            get {
                return Web.Resources.EntityStrings.DealerHistory_AContactNumber;
            }
        }

        public string DealerHistory_ADangerousRepairQualification {
            get {
                return Web.Resources.EntityStrings.DealerHistory_ADangerousRepairQualification;
            }
        }

        public string DealerHistory_AEmployeeNumber {
            get {
                return Web.Resources.EntityStrings.DealerHistory_AEmployeeNumber;
            }
        }

        public string DealerHistory_AFax {
            get {
                return Web.Resources.EntityStrings.DealerHistory_AFax;
            }
        }

        public string DealerHistory_AGeographicPosition {
            get {
                return Web.Resources.EntityStrings.DealerHistory_AGeographicPosition;
            }
        }

        public string DealerHistory_AHasBranch {
            get {
                return Web.Resources.EntityStrings.DealerHistory_AHasBranch;
            }
        }

        public string DealerHistory_AInvoiceAmountQuota {
            get {
                return Web.Resources.EntityStrings.DealerHistory_AInvoiceAmountQuota;
            }
        }

        public string DealerHistory_AInvoiceTax {
            get {
                return Web.Resources.EntityStrings.DealerHistory_AInvoiceTax;
            }
        }

        public string DealerHistory_AInvoiceTitle {
            get {
                return Web.Resources.EntityStrings.DealerHistory_AInvoiceTitle;
            }
        }

        public string DealerHistory_AInvoiceType {
            get {
                return Web.Resources.EntityStrings.DealerHistory_AInvoiceType;
            }
        }

        public string DealerHistory_AIsPartsSalesInvoice {
            get {
                return Web.Resources.EntityStrings.DealerHistory_AIsPartsSalesInvoice;
            }
        }

        public string DealerHistory_AIsVehicleSalesInvoice {
            get {
                return Web.Resources.EntityStrings.DealerHistory_AIsVehicleSalesInvoice;
            }
        }

        public string DealerHistory_ALinkman {
            get {
                return Web.Resources.EntityStrings.DealerHistory_ALinkman;
            }
        }

        public string DealerHistory_AMainBusinessAreas {
            get {
                return Web.Resources.EntityStrings.DealerHistory_AMainBusinessAreas;
            }
        }

        public string DealerHistory_AManager {
            get {
                return Web.Resources.EntityStrings.DealerHistory_AManager;
            }
        }

        public string DealerHistory_AManagerMail {
            get {
                return Web.Resources.EntityStrings.DealerHistory_AManagerMail;
            }
        }

        public string DealerHistory_AManagerMobile {
            get {
                return Web.Resources.EntityStrings.DealerHistory_AManagerMobile;
            }
        }

        public string DealerHistory_AManagerPhoneNumber {
            get {
                return Web.Resources.EntityStrings.DealerHistory_AManagerPhoneNumber;
            }
        }

        public string DealerHistory_AName {
            get {
                return Web.Resources.EntityStrings.DealerHistory_AName;
            }
        }

        public string DealerHistory_AndBusinessAreas {
            get {
                return Web.Resources.EntityStrings.DealerHistory_AndBusinessAreas;
            }
        }

        public string DealerHistory_AOwnerCompany {
            get {
                return Web.Resources.EntityStrings.DealerHistory_AOwnerCompany;
            }
        }

        public string DealerHistory_AParkingArea {
            get {
                return Web.Resources.EntityStrings.DealerHistory_AParkingArea;
            }
        }

        public string DealerHistory_APartWarehouseArea {
            get {
                return Web.Resources.EntityStrings.DealerHistory_APartWarehouseArea;
            }
        }

        public string DealerHistory_AReceptionRoomArea {
            get {
                return Web.Resources.EntityStrings.DealerHistory_AReceptionRoomArea;
            }
        }

        public string DealerHistory_ARepairingArea {
            get {
                return Web.Resources.EntityStrings.DealerHistory_ARepairingArea;
            }
        }

        public string DealerHistory_ARepairQualification {
            get {
                return Web.Resources.EntityStrings.DealerHistory_ARepairQualification;
            }
        }

        public string DealerHistory_AShortName {
            get {
                return Web.Resources.EntityStrings.DealerHistory_AShortName;
            }
        }

        public string DealerHistory_AStatus {
            get {
                return Web.Resources.EntityStrings.DealerHistory_AStatus;
            }
        }

        public string DealerHistory_ATaxpayerQualification {
            get {
                return Web.Resources.EntityStrings.DealerHistory_ATaxpayerQualification;
            }
        }

        public string DealerHistory_ATaxRegisteredAddress {
            get {
                return Web.Resources.EntityStrings.DealerHistory_ATaxRegisteredAddress;
            }
        }

        public string DealerHistory_ATaxRegisteredNumber {
            get {
                return Web.Resources.EntityStrings.DealerHistory_ATaxRegisteredNumber;
            }
        }

        public string DealerHistory_ATaxRegisteredPhone {
            get {
                return Web.Resources.EntityStrings.DealerHistory_ATaxRegisteredPhone;
            }
        }

        public string DealerHistory_ATrafficRestrictionsdescribe {
            get {
                return Web.Resources.EntityStrings.DealerHistory_ATrafficRestrictionsdescribe;
            }
        }

        public string DealerHistory_BankAccount {
            get {
                return Web.Resources.EntityStrings.DealerHistory_BankAccount;
            }
        }

        public string DealerHistory_BankName {
            get {
                return Web.Resources.EntityStrings.DealerHistory_BankName;
            }
        }

        public string DealerHistory_BrandScope {
            get {
                return Web.Resources.EntityStrings.DealerHistory_BrandScope;
            }
        }

        public string DealerHistory_BuildTime {
            get {
                return Web.Resources.EntityStrings.DealerHistory_BuildTime;
            }
        }

        public string DealerHistory_Code {
            get {
                return Web.Resources.EntityStrings.DealerHistory_Code;
            }
        }

        public string DealerHistory_CompetitiveBrandScope {
            get {
                return Web.Resources.EntityStrings.DealerHistory_CompetitiveBrandScope;
            }
        }

        public string DealerHistory_ContactNumber {
            get {
                return Web.Resources.EntityStrings.DealerHistory_ContactNumber;
            }
        }

        public string DealerHistory_DangerousRepairQualification {
            get {
                return Web.Resources.EntityStrings.DealerHistory_DangerousRepairQualification;
            }
        }

        public string DealerHistory_EmployeeNumber {
            get {
                return Web.Resources.EntityStrings.DealerHistory_EmployeeNumber;
            }
        }

        public string DealerHistory_Fax {
            get {
                return Web.Resources.EntityStrings.DealerHistory_Fax;
            }
        }

        public string DealerHistory_GeographicPosition {
            get {
                return Web.Resources.EntityStrings.DealerHistory_GeographicPosition;
            }
        }

        public string DealerHistory_HasBranch {
            get {
                return Web.Resources.EntityStrings.DealerHistory_HasBranch;
            }
        }

        public string DealerHistory_InvoiceAmountQuota {
            get {
                return Web.Resources.EntityStrings.DealerHistory_InvoiceAmountQuota;
            }
        }

        public string DealerHistory_InvoiceTax {
            get {
                return Web.Resources.EntityStrings.DealerHistory_InvoiceTax;
            }
        }

        public string DealerHistory_InvoiceTitle {
            get {
                return Web.Resources.EntityStrings.DealerHistory_InvoiceTitle;
            }
        }

        public string DealerHistory_InvoiceType {
            get {
                return Web.Resources.EntityStrings.DealerHistory_InvoiceType;
            }
        }

        public string DealerHistory_IsPartsSalesInvoice {
            get {
                return Web.Resources.EntityStrings.DealerHistory_IsPartsSalesInvoice;
            }
        }

        public string DealerHistory_IsVehicleSalesInvoice {
            get {
                return Web.Resources.EntityStrings.DealerHistory_IsVehicleSalesInvoice;
            }
        }

        public string DealerHistory_Linkman {
            get {
                return Web.Resources.EntityStrings.DealerHistory_Linkman;
            }
        }

        public string DealerHistory_MainBusinessAreas {
            get {
                return Web.Resources.EntityStrings.DealerHistory_MainBusinessAreas;
            }
        }

        public string DealerHistory_Manager {
            get {
                return Web.Resources.EntityStrings.DealerHistory_Manager;
            }
        }

        public string DealerHistory_ManagerMail {
            get {
                return Web.Resources.EntityStrings.DealerHistory_ManagerMail;
            }
        }

        public string DealerHistory_ManagerMobile {
            get {
                return Web.Resources.EntityStrings.DealerHistory_ManagerMobile;
            }
        }

        public string DealerHistory_ManagerPhoneNumber {
            get {
                return Web.Resources.EntityStrings.DealerHistory_ManagerPhoneNumber;
            }
        }

        public string DealerHistory_Name {
            get {
                return Web.Resources.EntityStrings.DealerHistory_Name;
            }
        }

        public string DealerHistory_OwnerCompany {
            get {
                return Web.Resources.EntityStrings.DealerHistory_OwnerCompany;
            }
        }

        public string DealerHistory_ParkingArea {
            get {
                return Web.Resources.EntityStrings.DealerHistory_ParkingArea;
            }
        }

        public string DealerHistory_PartWarehouseArea {
            get {
                return Web.Resources.EntityStrings.DealerHistory_PartWarehouseArea;
            }
        }

        public string DealerHistory_ReceptionRoomArea {
            get {
                return Web.Resources.EntityStrings.DealerHistory_ReceptionRoomArea;
            }
        }

        public string DealerHistory_RepairingArea {
            get {
                return Web.Resources.EntityStrings.DealerHistory_RepairingArea;
            }
        }

        public string DealerHistory_RepairQualification {
            get {
                return Web.Resources.EntityStrings.DealerHistory_RepairQualification;
            }
        }

        public string DealerHistory_ShortName {
            get {
                return Web.Resources.EntityStrings.DealerHistory_ShortName;
            }
        }

        public string DealerHistory_TaxpayerQualification {
            get {
                return Web.Resources.EntityStrings.DealerHistory_TaxpayerQualification;
            }
        }

        public string DealerHistory_TaxRegisteredAddress {
            get {
                return Web.Resources.EntityStrings.DealerHistory_TaxRegisteredAddress;
            }
        }

        public string DealerHistory_TaxRegisteredNumber {
            get {
                return Web.Resources.EntityStrings.DealerHistory_TaxRegisteredNumber;
            }
        }

        public string DealerHistory_TaxRegisteredPhone {
            get {
                return Web.Resources.EntityStrings.DealerHistory_TaxRegisteredPhone;
            }
        }

        public string DealerHistory_TrafficRestrictionsdescribe {
            get {
                return Web.Resources.EntityStrings.DealerHistory_TrafficRestrictionsdescribe;
            }
        }

        public string ServiceTripClaimApplication_FaultyPartsCode {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimApplication_FaultyPartsCode;
            }
        }

        public string ServiceTripClaimApplication_FaultyPartsName {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimApplication_FaultyPartsName;
            }
        }

        public string ServiceTripClaimApplication_FaultyPartsSupplierCode {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimApplication_FaultyPartsSupplierCode;
            }
        }

        public string ServiceTripClaimApplication_FaultyPartsSupplierName {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimApplication_FaultyPartsSupplierName;
            }
        }

        public string AuthenticationType_AuthenticationTypeName {
            get {
                return Web.Resources.EntityStrings.AuthenticationType_AuthenticationTypeName;
            }
        }

        public string TrainingType_TrainingTypeName {
            get {
                return Web.Resources.EntityStrings.TrainingType_TrainingTypeName;
            }
        }

        public string DealerPerTrainAut_AuthenticationTime {
            get {
                return Web.Resources.EntityStrings.DealerPerTrainAut_AuthenticationTime;
            }
        }

        public string DealerPerTrainAut_DealerCode {
            get {
                return Web.Resources.EntityStrings.DealerPerTrainAut_DealerCode;
            }
        }

        public string DealerPerTrainAut_DealerName {
            get {
                return Web.Resources.EntityStrings.DealerPerTrainAut_DealerName;
            }
        }

        public string DealerPerTrainAut_Name {
            get {
                return Web.Resources.EntityStrings.DealerPerTrainAut_Name;
            }
        }

        public string DealerPerTrainAut_TrainingTime {
            get {
                return Web.Resources.EntityStrings.DealerPerTrainAut_TrainingTime;
            }
        }

        public string PartsClaimOrderNew_PartsRetailOrderCode {
            get {
                return Web.Resources.EntityStrings.PartsClaimOrderNew_PartsRetailOrderCode;
            }
        }

        public string PartsClaimOrderNew_PartsSalesOrderCode {
            get {
                return Web.Resources.EntityStrings.PartsClaimOrderNew_PartsSalesOrderCode;
            }
        }

        public string PartsClaimOrderNew_RepairOrderCode {
            get {
                return Web.Resources.EntityStrings.PartsClaimOrderNew_RepairOrderCode;
            }
        }

        public string PartsBranch_PartsMaterialManageCost {
            get {
                return Web.Resources.EntityStrings.PartsBranch_PartsMaterialManageCost;
            }
        }

        public string PartsBranch_PartsWarrantyLong {
            get {
                return Web.Resources.EntityStrings.PartsBranch_PartsWarrantyLong;
            }
        }

        public string PartsClaimOrderNew_ApproveComment {
            get {
                return Web.Resources.EntityStrings.PartsClaimOrderNew_ApproveComment;
            }
        }

        public string PartsClaimOrderNew_CancelName {
            get {
                return Web.Resources.EntityStrings.PartsClaimOrderNew_CancelName;
            }
        }

        public string PartsClaimOrderNew_CancelTime {
            get {
                return Web.Resources.EntityStrings.PartsClaimOrderNew_CancelTime;
            }
        }

        public string PartsClaimOrderNew_CDCReturnWarehouseName {
            get {
                return Web.Resources.EntityStrings.PartsClaimOrderNew_CDCReturnWarehouseName;
            }
        }

        public string PartsClaimOrderNew_Code {
            get {
                return Web.Resources.EntityStrings.PartsClaimOrderNew_Code;
            }
        }

        public string PartsClaimOrderNew_CustomerContactPerson {
            get {
                return Web.Resources.EntityStrings.PartsClaimOrderNew_CustomerContactPerson;
            }
        }

        public string PartsClaimOrderNew_CustomerContactPhone {
            get {
                return Web.Resources.EntityStrings.PartsClaimOrderNew_CustomerContactPhone;
            }
        }

        public string PartsClaimOrderNew_IsOutWarranty {
            get {
                return Web.Resources.EntityStrings.PartsClaimOrderNew_IsOutWarranty;
            }
        }

        public string PartsClaimOrderNew_LaborCost {
            get {
                return Web.Resources.EntityStrings.PartsClaimOrderNew_LaborCost;
            }
        }

        public string PartsClaimOrderNew_MalfunctionDescription {
            get {
                return Web.Resources.EntityStrings.PartsClaimOrderNew_MalfunctionDescription;
            }
        }

        public string PartsClaimOrderNew_MaterialCost {
            get {
                return Web.Resources.EntityStrings.PartsClaimOrderNew_MaterialCost;
            }
        }

        public string PartsClaimOrderNew_MaterialManagementCost {
            get {
                return Web.Resources.EntityStrings.PartsClaimOrderNew_MaterialManagementCost;
            }
        }

        public string PartsClaimOrderNew_OtherCost {
            get {
                return Web.Resources.EntityStrings.PartsClaimOrderNew_OtherCost;
            }
        }

        public string PartsClaimOrderNew_PartsSaleLong {
            get {
                return Web.Resources.EntityStrings.PartsClaimOrderNew_PartsSaleLong;
            }
        }

        public string PartsClaimOrderNew_PartsSaleTime {
            get {
                return Web.Resources.EntityStrings.PartsClaimOrderNew_PartsSaleTime;
            }
        }

        public string PartsClaimOrderNew_PartsWarrantyLong {
            get {
                return Web.Resources.EntityStrings.PartsClaimOrderNew_PartsWarrantyLong;
            }
        }

        public string PartsClaimOrderNew_RDCReceiptSituation {
            get {
                return Web.Resources.EntityStrings.PartsClaimOrderNew_RDCReceiptSituation;
            }
        }

        public string PartsClaimOrderNew_RejectName {
            get {
                return Web.Resources.EntityStrings.PartsClaimOrderNew_RejectName;
            }
        }

        public string PartsClaimOrderNew_RejectQty {
            get {
                return Web.Resources.EntityStrings.PartsClaimOrderNew_RejectQty;
            }
        }

        public string PartsClaimOrderNew_RejectStatus {
            get {
                return Web.Resources.EntityStrings.PartsClaimOrderNew_RejectStatus;
            }
        }

        public string PartsClaimOrderNew_RejectTime {
            get {
                return Web.Resources.EntityStrings.PartsClaimOrderNew_RejectTime;
            }
        }

        public string PartsClaimOrderNew_RepairRequestTime {
            get {
                return Web.Resources.EntityStrings.PartsClaimOrderNew_RepairRequestTime;
            }
        }

        public string PartsClaimOrderNew_ReturnWarehouseCode {
            get {
                return Web.Resources.EntityStrings.PartsClaimOrderNew_ReturnWarehouseCode;
            }
        }

        public string PartsClaimOrderNew_ReturnWarehouseName {
            get {
                return Web.Resources.EntityStrings.PartsClaimOrderNew_ReturnWarehouseName;
            }
        }

        public string PartsClaimOrderNew_RWarehouseCompanyCode {
            get {
                return Web.Resources.EntityStrings.PartsClaimOrderNew_RWarehouseCompanyCode;
            }
        }

        public string PartsClaimOrderNew_SalesWarehouseCode {
            get {
                return Web.Resources.EntityStrings.PartsClaimOrderNew_SalesWarehouseCode;
            }
        }

        public string PartsClaimOrderNew_SalesWarehouseName {
            get {
                return Web.Resources.EntityStrings.PartsClaimOrderNew_SalesWarehouseName;
            }
        }

        public string PartsClaimOrderNew_TotalAmount {
            get {
                return Web.Resources.EntityStrings.PartsClaimOrderNew_TotalAmount;
            }
        }

        public string MarketABQualityInformation_CountersignatureCreateTime {
            get {
                return Web.Resources.EntityStrings.MarketABQualityInformation_CountersignatureCreateTime;
            }
        }

        public string MarketABQualityInformation_CountersignatureCreatorName {
            get {
                return Web.Resources.EntityStrings.MarketABQualityInformation_CountersignatureCreatorName;
            }
        }

        public string RepairClaimApplication_BusinessCode {
            get {
                return Web.Resources.EntityStrings.RepairClaimApplication_BusinessCode;
            }
        }

        public string ServiceTripClaimBillWithOtherInfo_ClaimSupplierCode {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimBillWithOtherInfo_ClaimSupplierCode;
            }
        }

        public string ServiceTripClaimBillWithOtherInfo_ClaimSupplierName {
            get {
                return Web.Resources.EntityStrings.ServiceTripClaimBillWithOtherInfo_ClaimSupplierName;
            }
        }

        public string ServiceTripPriceGrade_Name {
            get {
                return Web.Resources.EntityStrings.ServiceTripPriceGrade_Name;
            }
        }

        public string VirtualMarketABQualityInformation_ApproveCommentHistory {
            get {
                return Web.Resources.EntityStrings.VirtualMarketABQualityInformation_ApproveCommentHistory;
            }
        }

        public string VirtualMarketABQualityInformation_ApproverName {
            get {
                return Web.Resources.EntityStrings.VirtualMarketABQualityInformation_ApproverName;
            }
        }

        public string VirtualMarketABQualityInformation_ApproveTime {
            get {
                return Web.Resources.EntityStrings.VirtualMarketABQualityInformation_ApproveTime;
            }
        }

        public string VirtualMarketABQualityInformation_BranchName {
            get {
                return Web.Resources.EntityStrings.VirtualMarketABQualityInformation_BranchName;
            }
        }

        public string VirtualMarketABQualityInformation_CargoTypes {
            get {
                return Web.Resources.EntityStrings.VirtualMarketABQualityInformation_CargoTypes;
            }
        }

        public string VirtualMarketABQualityInformation_Code {
            get {
                return Web.Resources.EntityStrings.VirtualMarketABQualityInformation_Code;
            }
        }

        public string VirtualMarketABQualityInformation_CompanyCode {
            get {
                return Web.Resources.EntityStrings.VirtualMarketABQualityInformation_CompanyCode;
            }
        }

        public string VirtualMarketABQualityInformation_CompanyName {
            get {
                return Web.Resources.EntityStrings.VirtualMarketABQualityInformation_CompanyName;
            }
        }

        public string VirtualMarketABQualityInformation_CountersignatureCreateTime {
            get {
                return Web.Resources.EntityStrings.VirtualMarketABQualityInformation_CountersignatureCreateTime;
            }
        }

        public string VirtualMarketABQualityInformation_CountersignatureCreatorName {
            get {
                return Web.Resources.EntityStrings.VirtualMarketABQualityInformation_CountersignatureCreatorName;
            }
        }

        public string VirtualMarketABQualityInformation_CreateTime {
            get {
                return Web.Resources.EntityStrings.VirtualMarketABQualityInformation_CreateTime;
            }
        }

        public string VirtualMarketABQualityInformation_CreatorName {
            get {
                return Web.Resources.EntityStrings.VirtualMarketABQualityInformation_CreatorName;
            }
        }

        public string VirtualMarketABQualityInformation_DealerId {
            get {
                return Web.Resources.EntityStrings.VirtualMarketABQualityInformation_DealerId;
            }
        }

        public string VirtualMarketABQualityInformation_Drive {
            get {
                return Web.Resources.EntityStrings.VirtualMarketABQualityInformation_Drive;
            }
        }

        public string VirtualMarketABQualityInformation_Emission {
            get {
                return Web.Resources.EntityStrings.VirtualMarketABQualityInformation_Emission;
            }
        }

        public string VirtualMarketABQualityInformation_EngineModel {
            get {
                return Web.Resources.EntityStrings.VirtualMarketABQualityInformation_EngineModel;
            }
        }

        public string VirtualMarketABQualityInformation_FailureMode {
            get {
                return Web.Resources.EntityStrings.VirtualMarketABQualityInformation_FailureMode;
            }
        }

        public string VirtualMarketABQualityInformation_FailureSerialNumberDetail {
            get {
                return Web.Resources.EntityStrings.VirtualMarketABQualityInformation_FailureSerialNumberDetail;
            }
        }

        public string VirtualMarketABQualityInformation_FailureTimes {
            get {
                return Web.Resources.EntityStrings.VirtualMarketABQualityInformation_FailureTimes;
            }
        }

        public string VirtualMarketABQualityInformation_FailureType {
            get {
                return Web.Resources.EntityStrings.VirtualMarketABQualityInformation_FailureType;
            }
        }

        public string VirtualMarketABQualityInformation_FaultDate {
            get {
                return Web.Resources.EntityStrings.VirtualMarketABQualityInformation_FaultDate;
            }
        }

        public string VirtualMarketABQualityInformation_FaultDescription {
            get {
                return Web.Resources.EntityStrings.VirtualMarketABQualityInformation_FaultDescription;
            }
        }

        public string VirtualMarketABQualityInformation_FaultReason {
            get {
                return Web.Resources.EntityStrings.VirtualMarketABQualityInformation_FaultReason;
            }
        }

        public string VirtualMarketABQualityInformation_FaultyPartsCode {
            get {
                return Web.Resources.EntityStrings.VirtualMarketABQualityInformation_FaultyPartsCode;
            }
        }

        public string VirtualMarketABQualityInformation_FaultyPartsName {
            get {
                return Web.Resources.EntityStrings.VirtualMarketABQualityInformation_FaultyPartsName;
            }
        }

        public string VirtualMarketABQualityInformation_FaultyPartsSupplierCode {
            get {
                return Web.Resources.EntityStrings.VirtualMarketABQualityInformation_FaultyPartsSupplierCode;
            }
        }

        public string VirtualMarketABQualityInformation_FaultyPartsSupplierName {
            get {
                return Web.Resources.EntityStrings.VirtualMarketABQualityInformation_FaultyPartsSupplierName;
            }
        }

        public string VirtualMarketABQualityInformation_HaveMeasures {
            get {
                return Web.Resources.EntityStrings.VirtualMarketABQualityInformation_HaveMeasures;
            }
        }

        public string VirtualMarketABQualityInformation_InformationPhone {
            get {
                return Web.Resources.EntityStrings.VirtualMarketABQualityInformation_InformationPhone;
            }
        }

        public string VirtualMarketABQualityInformation_InitialApproverName {
            get {
                return Web.Resources.EntityStrings.VirtualMarketABQualityInformation_InitialApproverName;
            }
        }

        public string VirtualMarketABQualityInformation_InitialApproverTime {
            get {
                return Web.Resources.EntityStrings.VirtualMarketABQualityInformation_InitialApproverTime;
            }
        }

        public string VirtualMarketABQualityInformation_Mileage {
            get {
                return Web.Resources.EntityStrings.VirtualMarketABQualityInformation_Mileage;
            }
        }

        public string VirtualMarketABQualityInformation_ModifyTime {
            get {
                return Web.Resources.EntityStrings.VirtualMarketABQualityInformation_ModifyTime;
            }
        }

        public string VirtualMarketABQualityInformation_OutOfFactoryDate {
            get {
                return Web.Resources.EntityStrings.VirtualMarketABQualityInformation_OutOfFactoryDate;
            }
        }

        public string VirtualMarketABQualityInformation_PartsSalesCategoryId {
            get {
                return Web.Resources.EntityStrings.VirtualMarketABQualityInformation_PartsSalesCategoryId;
            }
        }

        public string VirtualMarketABQualityInformation_PartsSalesCategoryName {
            get {
                return Web.Resources.EntityStrings.VirtualMarketABQualityInformation_PartsSalesCategoryName;
            }
        }

        public string VirtualMarketABQualityInformation_ProductCategoryCode {
            get {
                return Web.Resources.EntityStrings.VirtualMarketABQualityInformation_ProductCategoryCode;
            }
        }

        public string VirtualMarketABQualityInformation_RejectName {
            get {
                return Web.Resources.EntityStrings.VirtualMarketABQualityInformation_RejectName;
            }
        }

        public string VirtualMarketABQualityInformation_RejectTime {
            get {
                return Web.Resources.EntityStrings.VirtualMarketABQualityInformation_RejectTime;
            }
        }

        public string VirtualMarketABQualityInformation_Remark {
            get {
                return Web.Resources.EntityStrings.VirtualMarketABQualityInformation_Remark;
            }
        }

        public string VirtualMarketABQualityInformation_RoadConditions {
            get {
                return Web.Resources.EntityStrings.VirtualMarketABQualityInformation_RoadConditions;
            }
        }

        public string VirtualMarketABQualityInformation_SalesDate {
            get {
                return Web.Resources.EntityStrings.VirtualMarketABQualityInformation_SalesDate;
            }
        }

        public string VirtualMarketABQualityInformation_ServiceProductLineName {
            get {
                return Web.Resources.EntityStrings.VirtualMarketABQualityInformation_ServiceProductLineName;
            }
        }

        public string VirtualMarketABQualityInformation_Status {
            get {
                return Web.Resources.EntityStrings.VirtualMarketABQualityInformation_Status;
            }
        }

        public string VirtualMarketABQualityInformation_VIN {
            get {
                return Web.Resources.EntityStrings.VirtualMarketABQualityInformation_VIN;
            }
        }

        public string Agency_IsAutoTranslate {
            get {
                return Web.Resources.EntityStrings.Agency_IsAutoTranslate;
            }
        }

        public string PartsSalesOrder_IsTransSuccess {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrder_IsTransSuccess;
            }
        }

        public string PartsSalesOrder_ReceivingWarehouseName {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrder_ReceivingWarehouseName;
            }
        }

        public string DealerHistory_ABusinessAddress {
            get {
                return Web.Resources.EntityStrings.DealerHistory_ABusinessAddress;
            }
        }

        public string DealerHistory_ABusinessScope {
            get {
                return Web.Resources.EntityStrings.DealerHistory_ABusinessScope;
            }
        }

        public string DealerHistory_ACityLevel {
            get {
                return Web.Resources.EntityStrings.DealerHistory_ACityLevel;
            }
        }

        public string DealerHistory_ACityName {
            get {
                return Web.Resources.EntityStrings.DealerHistory_ACityName;
            }
        }

        public string DealerHistory_ACompanyFax {
            get {
                return Web.Resources.EntityStrings.DealerHistory_ACompanyFax;
            }
        }

        public string DealerHistory_AContactMail {
            get {
                return Web.Resources.EntityStrings.DealerHistory_AContactMail;
            }
        }

        public string DealerHistory_AContactMobile {
            get {
                return Web.Resources.EntityStrings.DealerHistory_AContactMobile;
            }
        }

        public string DealerHistory_AContactPerson {
            get {
                return Web.Resources.EntityStrings.DealerHistory_AContactPerson;
            }
        }

        public string DealerHistory_AContactPhone {
            get {
                return Web.Resources.EntityStrings.DealerHistory_AContactPhone;
            }
        }

        public string DealerHistory_AContactPostCode {
            get {
                return Web.Resources.EntityStrings.DealerHistory_AContactPostCode;
            }
        }

        public string DealerHistory_ACorporateNature {
            get {
                return Web.Resources.EntityStrings.DealerHistory_ACorporateNature;
            }
        }

        public string DealerHistory_ACountyName {
            get {
                return Web.Resources.EntityStrings.DealerHistory_ACountyName;
            }
        }

        public string DealerHistory_ACustomerCode {
            get {
                return Web.Resources.EntityStrings.DealerHistory_ACustomerCode;
            }
        }

        public string DealerHistory_AFixedAsset {
            get {
                return Web.Resources.EntityStrings.DealerHistory_AFixedAsset;
            }
        }

        public string DealerHistory_AFoundDate {
            get {
                return Web.Resources.EntityStrings.DealerHistory_AFoundDate;
            }
        }

        public string DealerHistory_AIdDocumentNumber {
            get {
                return Web.Resources.EntityStrings.DealerHistory_AIdDocumentNumber;
            }
        }

        public string DealerHistory_AIdDocumentType {
            get {
                return Web.Resources.EntityStrings.DealerHistory_AIdDocumentType;
            }
        }

        public string DealerHistory_ALegalRepresentative {
            get {
                return Web.Resources.EntityStrings.DealerHistory_ALegalRepresentative;
            }
        }

        public string DealerHistory_ALegalRepresentTel {
            get {
                return Web.Resources.EntityStrings.DealerHistory_ALegalRepresentTel;
            }
        }

        public string DealerHistory_AProvinceName {
            get {
                return Web.Resources.EntityStrings.DealerHistory_AProvinceName;
            }
        }

        public string DealerHistory_ARegisterCapital {
            get {
                return Web.Resources.EntityStrings.DealerHistory_ARegisterCapital;
            }
        }

        public string DealerHistory_ARegisterCode {
            get {
                return Web.Resources.EntityStrings.DealerHistory_ARegisterCode;
            }
        }

        public string DealerHistory_ARegisterDate {
            get {
                return Web.Resources.EntityStrings.DealerHistory_ARegisterDate;
            }
        }

        public string DealerHistory_ARegisteredAddress {
            get {
                return Web.Resources.EntityStrings.DealerHistory_ARegisteredAddress;
            }
        }

        public string DealerHistory_ARegisterName {
            get {
                return Web.Resources.EntityStrings.DealerHistory_ARegisterName;
            }
        }

        public string DealerHistory_ASupplierCode {
            get {
                return Web.Resources.EntityStrings.DealerHistory_ASupplierCode;
            }
        }

        public string DealerHistory_BusinessAddress {
            get {
                return Web.Resources.EntityStrings.DealerHistory_BusinessAddress;
            }
        }

        public string DealerHistory_BusinessScope {
            get {
                return Web.Resources.EntityStrings.DealerHistory_BusinessScope;
            }
        }

        public string DealerHistory_CityLevel {
            get {
                return Web.Resources.EntityStrings.DealerHistory_CityLevel;
            }
        }

        public string DealerHistory_CityName {
            get {
                return Web.Resources.EntityStrings.DealerHistory_CityName;
            }
        }

        public string DealerHistory_CompanyFax {
            get {
                return Web.Resources.EntityStrings.DealerHistory_CompanyFax;
            }
        }

        public string DealerHistory_ContactMail {
            get {
                return Web.Resources.EntityStrings.DealerHistory_ContactMail;
            }
        }

        public string DealerHistory_ContactMobile {
            get {
                return Web.Resources.EntityStrings.DealerHistory_ContactMobile;
            }
        }

        public string DealerHistory_ContactPerson {
            get {
                return Web.Resources.EntityStrings.DealerHistory_ContactPerson;
            }
        }

        public string DealerHistory_ContactPhone {
            get {
                return Web.Resources.EntityStrings.DealerHistory_ContactPhone;
            }
        }

        public string DealerHistory_ContactPostCode {
            get {
                return Web.Resources.EntityStrings.DealerHistory_ContactPostCode;
            }
        }

        public string DealerHistory_CorporateNature {
            get {
                return Web.Resources.EntityStrings.DealerHistory_CorporateNature;
            }
        }

        public string DealerHistory_CountyName {
            get {
                return Web.Resources.EntityStrings.DealerHistory_CountyName;
            }
        }

        public string DealerHistory_CustomerCode {
            get {
                return Web.Resources.EntityStrings.DealerHistory_CustomerCode;
            }
        }

        public string DealerHistory_FixedAsset {
            get {
                return Web.Resources.EntityStrings.DealerHistory_FixedAsset;
            }
        }

        public string DealerHistory_FoundDate {
            get {
                return Web.Resources.EntityStrings.DealerHistory_FoundDate;
            }
        }

        public string DealerHistory_IdDocumentNumber {
            get {
                return Web.Resources.EntityStrings.DealerHistory_IdDocumentNumber;
            }
        }

        public string DealerHistory_IdDocumentType {
            get {
                return Web.Resources.EntityStrings.DealerHistory_IdDocumentType;
            }
        }

        public string DealerHistory_LegalRepresentative {
            get {
                return Web.Resources.EntityStrings.DealerHistory_LegalRepresentative;
            }
        }

        public string DealerHistory_LegalRepresentTel {
            get {
                return Web.Resources.EntityStrings.DealerHistory_LegalRepresentTel;
            }
        }

        public string DealerHistory_ProvinceName {
            get {
                return Web.Resources.EntityStrings.DealerHistory_ProvinceName;
            }
        }

        public string DealerHistory_RegisterCapital {
            get {
                return Web.Resources.EntityStrings.DealerHistory_RegisterCapital;
            }
        }

        public string DealerHistory_RegisterCode {
            get {
                return Web.Resources.EntityStrings.DealerHistory_RegisterCode;
            }
        }

        public string DealerHistory_RegisterDate {
            get {
                return Web.Resources.EntityStrings.DealerHistory_RegisterDate;
            }
        }

        public string DealerHistory_RegisteredAddress {
            get {
                return Web.Resources.EntityStrings.DealerHistory_RegisteredAddress;
            }
        }

        public string DealerHistory_RegisterName {
            get {
                return Web.Resources.EntityStrings.DealerHistory_RegisterName;
            }
        }

        public string DealerHistory_SupplierCode {
            get {
                return Web.Resources.EntityStrings.DealerHistory_SupplierCode;
            }
        }

        public string FaultyPartsSupplierAssembly_Code {
            get {
                return Web.Resources.EntityStrings.FaultyPartsSupplierAssembly_Code;
            }
        }

        public string FaultyPartsSupplierAssembly_Name {
            get {
                return Web.Resources.EntityStrings.FaultyPartsSupplierAssembly_Name;
            }
        }

        public string PartsPurReturnOrder_OutStatus {
            get {
                return Web.Resources.EntityStrings.PartsPurReturnOrder_OutStatus;
            }
        }

        public string InternalAcquisitionBill_PartsPurchaseOrderTypeId {
            get {
                return Web.Resources.EntityStrings.InternalAcquisitionBill_PartsPurchaseOrderTypeId;
            }
        }

        public string InternalAcquisitionBill_RequestedDeliveryTime {
            get {
                return Web.Resources.EntityStrings.InternalAcquisitionBill_RequestedDeliveryTime;
            }
        }

        public string AutoApproveStrategy_ClaimTimeOutLong {
            get {
                return Web.Resources.EntityStrings.AutoApproveStrategy_ClaimTimeOutLong;
            }
        }

        public string AutoApproveStrategy_ForcedMainteTime {
            get {
                return Web.Resources.EntityStrings.AutoApproveStrategy_ForcedMainteTime;
            }
        }

        public string AutoApproveStrategy_IsAssociatedApplication {
            get {
                return Web.Resources.EntityStrings.AutoApproveStrategy_IsAssociatedApplication;
            }
        }

        public string AutoApproveStrategy_IsNoMaterialFee {
            get {
                return Web.Resources.EntityStrings.AutoApproveStrategy_IsNoMaterialFee;
            }
        }

        public string AutoApproveStrategy_RepeatedRepairTime {
            get {
                return Web.Resources.EntityStrings.AutoApproveStrategy_RepeatedRepairTime;
            }
        }

        public string AutoApproveStrategy_ServiceFee {
            get {
                return Web.Resources.EntityStrings.AutoApproveStrategy_ServiceFee;
            }
        }

        public string AppointFaultReasonDtl_FaultReasonCode {
            get {
                return Web.Resources.EntityStrings.AppointFaultReasonDtl_FaultReasonCode;
            }
        }

        public string AppointFaultReasonDtl_FaultReasonName {
            get {
                return Web.Resources.EntityStrings.AppointFaultReasonDtl_FaultReasonName;
            }
        }

        public string AppointRepairTypeDtl_RepairType {
            get {
                return Web.Resources.EntityStrings.AppointRepairTypeDtl_RepairType;
            }
        }

        public string ServiceProductLineView_ProductLineCode {
            get {
                return Web.Resources.EntityStrings.ServiceProductLineView_ProductLineCode;
            }
        }

        public string ServiceProductLineView_ProductLineName {
            get {
                return Web.Resources.EntityStrings.ServiceProductLineView_ProductLineName;
            }
        }

        public string ServiceProductLineView_ProductLineType {
            get {
                return Web.Resources.EntityStrings.ServiceProductLineView_ProductLineType;
            }
        }

        public string CompanyDetail_Code {
            get {
                return Web.Resources.EntityStrings.CompanyDetail_Code;
            }
        }

        public string CompanyDetail_Name {
            get {
                return Web.Resources.EntityStrings.CompanyDetail_Name;
            }
        }

        public string CompanyDetail_Type {
            get {
                return Web.Resources.EntityStrings.CompanyDetail_Type;
            }
        }

        public string SparePartHistory_IMSCompressionNumber {
            get {
                return Web.Resources.EntityStrings.SparePartHistory_IMSCompressionNumber;
            }
        }

        public string SparePartHistory_IMSManufacturerNumber {
            get {
                return Web.Resources.EntityStrings.SparePartHistory_IMSManufacturerNumber;
            }
        }

        public string SparePart_IMSCompressionNumber {
            get {
                return Web.Resources.EntityStrings.SparePart_IMSCompressionNumber;
            }
        }

        public string SparePart_IMSManufacturerNumber {
            get {
                return Web.Resources.EntityStrings.SparePart_IMSManufacturerNumber;
            }
        }

        public string PartsBranch_PlannedPriceCategory {
            get {
                return Web.Resources.EntityStrings.PartsBranch_PlannedPriceCategory;
            }
        }

        public string ABCStrategy_YeardOutBoundEndTime {
            get {
                return Web.Resources.EntityStrings.ABCStrategy_YeardOutBoundEndTime;
            }
        }

        public string ABCStrategy_YeardOutBoundStartTime {
            get {
                return Web.Resources.EntityStrings.ABCStrategy_YeardOutBoundStartTime;
            }
        }

        public string PartsSupplierRelation_DefaultOrderWarehouseName {
            get {
                return Web.Resources.EntityStrings.PartsSupplierRelation_DefaultOrderWarehouseName;
            }
        }

        public string PartsSupplierRelation_OrderCycle {
            get {
                return Web.Resources.EntityStrings.PartsSupplierRelation_OrderCycle;
            }
        }

        public string ProductLifeCycleCategory_CreatorEndTime {
            get {
                return Web.Resources.EntityStrings.ProductLifeCycleCategory_CreatorEndTime;
            }
        }

        public string ProductLifeCycleCategory_CreatorStartTime {
            get {
                return Web.Resources.EntityStrings.ProductLifeCycleCategory_CreatorStartTime;
            }
        }

        public string ProductLifeCycleCategory_CycleCategory {
            get {
                return Web.Resources.EntityStrings.ProductLifeCycleCategory_CycleCategory;
            }
        }

        public string PlanPriceCategory_BranchName {
            get {
                return Web.Resources.EntityStrings.PlanPriceCategory_BranchName;
            }
        }

        public string PlanPriceCategory_PriceCategory {
            get {
                return Web.Resources.EntityStrings.PlanPriceCategory_PriceCategory;
            }
        }

        public string PlanPriceCategory_PriceMax {
            get {
                return Web.Resources.EntityStrings.PlanPriceCategory_PriceMax;
            }
        }

        public string PlanPriceCategory_PriceMin {
            get {
                return Web.Resources.EntityStrings.PlanPriceCategory_PriceMin;
            }
        }

        public string PartsServiceLevel_BranchName {
            get {
                return Web.Resources.EntityStrings.PartsServiceLevel_BranchName;
            }
        }

        public string PartsServiceLevel_EndDuration {
            get {
                return Web.Resources.EntityStrings.PartsServiceLevel_EndDuration;
            }
        }

        public string PartsServiceLevel_PartABC {
            get {
                return Web.Resources.EntityStrings.PartsServiceLevel_PartABC;
            }
        }

        public string PartsServiceLevel_PlannedPriceType {
            get {
                return Web.Resources.EntityStrings.PartsServiceLevel_PlannedPriceType;
            }
        }

        public string PartsServiceLevel_ServiceLevel {
            get {
                return Web.Resources.EntityStrings.PartsServiceLevel_ServiceLevel;
            }
        }

        public string PartsServiceLevel_StartDuration {
            get {
                return Web.Resources.EntityStrings.PartsServiceLevel_StartDuration;
            }
        }

        public string IMSPurchaseLoginInfo_Code {
            get {
                return Web.Resources.EntityStrings.IMSPurchaseLoginInfo_Code;
            }
        }

        public string IMSPurchaseLoginInfo_ErrorMsg {
            get {
                return Web.Resources.EntityStrings.IMSPurchaseLoginInfo_ErrorMsg;
            }
        }

        public string IMSPurchaseLoginInfo_FileName {
            get {
                return Web.Resources.EntityStrings.IMSPurchaseLoginInfo_FileName;
            }
        }

        public string IMSPurchaseLoginInfo_Status {
            get {
                return Web.Resources.EntityStrings.IMSPurchaseLoginInfo_Status;
            }
        }

        public string IMSPurchaseLoginInfo_SyncTime {
            get {
                return Web.Resources.EntityStrings.IMSPurchaseLoginInfo_SyncTime;
            }
        }

        public string IMSShippingLoginInfo_DelNote {
            get {
                return Web.Resources.EntityStrings.IMSShippingLoginInfo_DelNote;
            }
        }

        public string IMSShippingLoginInfo_ERRORMESSAGE {
            get {
                return Web.Resources.EntityStrings.IMSShippingLoginInfo_ERRORMESSAGE;
            }
        }

        public string IMSShippingLoginInfo_FILENAME {
            get {
                return Web.Resources.EntityStrings.IMSShippingLoginInfo_FILENAME;
            }
        }

        public string IMSShippingLoginInfo_OrderNo {
            get {
                return Web.Resources.EntityStrings.IMSShippingLoginInfo_OrderNo;
            }
        }

        public string IMSShippingLoginInfo_SYNCSTATUS {
            get {
                return Web.Resources.EntityStrings.IMSShippingLoginInfo_SYNCSTATUS;
            }
        }

        public string IMSShippingLoginInfo_SYNCTIME {
            get {
                return Web.Resources.EntityStrings.IMSShippingLoginInfo_SYNCTIME;
            }
        }

        public string PartConPurchasePlan_ActualOrderWarehouse {
            get {
                return Web.Resources.EntityStrings.PartConPurchasePlan_ActualOrderWarehouse;
            }
        }

        public string PartConPurchasePlan_ActualPurchase {
            get {
                return Web.Resources.EntityStrings.PartConPurchasePlan_ActualPurchase;
            }
        }

        public string PartConPurchasePlan_ActualSupplierCode {
            get {
                return Web.Resources.EntityStrings.PartConPurchasePlan_ActualSupplierCode;
            }
        }

        public string PartConPurchasePlan_AdvicePurchase {
            get {
                return Web.Resources.EntityStrings.PartConPurchasePlan_AdvicePurchase;
            }
        }

        public string PartConPurchasePlan_Average {
            get {
                return Web.Resources.EntityStrings.PartConPurchasePlan_Average;
            }
        }

        public string PartConPurchasePlan_ConArrivalCycle {
            get {
                return Web.Resources.EntityStrings.PartConPurchasePlan_ConArrivalCycle;
            }
        }

        public string PartConPurchasePlan_DateT {
            get {
                return Web.Resources.EntityStrings.PartConPurchasePlan_DateT;
            }
        }

        public string PartConPurchasePlan_DayAvgFor12M {
            get {
                return Web.Resources.EntityStrings.PartConPurchasePlan_DayAvgFor12M;
            }
        }

        public string PartConPurchasePlan_DayAvgFor1M {
            get {
                return Web.Resources.EntityStrings.PartConPurchasePlan_DayAvgFor1M;
            }
        }

        public string PartConPurchasePlan_DayAvgFor3M {
            get {
                return Web.Resources.EntityStrings.PartConPurchasePlan_DayAvgFor3M;
            }
        }

        public string PartConPurchasePlan_DayAvgFor6M {
            get {
                return Web.Resources.EntityStrings.PartConPurchasePlan_DayAvgFor6M;
            }
        }

        public string PartConPurchasePlan_DFOrderWarehouse {
            get {
                return Web.Resources.EntityStrings.PartConPurchasePlan_DFOrderWarehouse;
            }
        }

        public string PartConPurchasePlan_EconomicalBatch {
            get {
                return Web.Resources.EntityStrings.PartConPurchasePlan_EconomicalBatch;
            }
        }

        public string PartConPurchasePlan_ExPurchaseInLine {
            get {
                return Web.Resources.EntityStrings.PartConPurchasePlan_ExPurchaseInLine;
            }
        }

        public string PartConPurchasePlan_ExStock {
            get {
                return Web.Resources.EntityStrings.PartConPurchasePlan_ExStock;
            }
        }

        public string PartConPurchasePlan_MinPurchaseQuantity {
            get {
                return Web.Resources.EntityStrings.PartConPurchasePlan_MinPurchaseQuantity;
            }
        }

        public string PartConPurchasePlan_Near12MOutFrequency {
            get {
                return Web.Resources.EntityStrings.PartConPurchasePlan_Near12MOutFrequency;
            }
        }

        public string PartConPurchasePlan_OrderCycle {
            get {
                return Web.Resources.EntityStrings.PartConPurchasePlan_OrderCycle;
            }
        }

        public string PartConPurchasePlan_OrderQuantity1 {
            get {
                return Web.Resources.EntityStrings.PartConPurchasePlan_OrderQuantity1;
            }
        }

        public string PartConPurchasePlan_OrderQuantity2 {
            get {
                return Web.Resources.EntityStrings.PartConPurchasePlan_OrderQuantity2;
            }
        }

        public string PartConPurchasePlan_OrderQuantity3 {
            get {
                return Web.Resources.EntityStrings.PartConPurchasePlan_OrderQuantity3;
            }
        }

        public string PartConPurchasePlan_OrderQuantityFor12M {
            get {
                return Web.Resources.EntityStrings.PartConPurchasePlan_OrderQuantityFor12M;
            }
        }

        public string PartConPurchasePlan_OrderQuantityFor3M {
            get {
                return Web.Resources.EntityStrings.PartConPurchasePlan_OrderQuantityFor3M;
            }
        }

        public string PartConPurchasePlan_OrderQuantityFor6M {
            get {
                return Web.Resources.EntityStrings.PartConPurchasePlan_OrderQuantityFor6M;
            }
        }

        public string PartConPurchasePlan_PartABC {
            get {
                return Web.Resources.EntityStrings.PartConPurchasePlan_PartABC;
            }
        }

        public string PartConPurchasePlan_PartCode {
            get {
                return Web.Resources.EntityStrings.PartConPurchasePlan_PartCode;
            }
        }

        public string PartConPurchasePlan_PartName {
            get {
                return Web.Resources.EntityStrings.PartConPurchasePlan_PartName;
            }
        }

        public string PartConPurchasePlan_PartsSalesCategoryName {
            get {
                return Web.Resources.EntityStrings.PartConPurchasePlan_PartsSalesCategoryName;
            }
        }

        public string PartConPurchasePlan_PlannedPrice {
            get {
                return Web.Resources.EntityStrings.PartConPurchasePlan_PlannedPrice;
            }
        }

        public string PartConPurchasePlan_PlannedPriceType {
            get {
                return Web.Resources.EntityStrings.PartConPurchasePlan_PlannedPriceType;
            }
        }

        public string PartConPurchasePlan_PrimarySupplierCode {
            get {
                return Web.Resources.EntityStrings.PartConPurchasePlan_PrimarySupplierCode;
            }
        }

        public string PartConPurchasePlan_PruductLifeCycle {
            get {
                return Web.Resources.EntityStrings.PartConPurchasePlan_PruductLifeCycle;
            }
        }

        public string PartConPurchasePlan_PurchaseInLine {
            get {
                return Web.Resources.EntityStrings.PartConPurchasePlan_PurchaseInLine;
            }
        }

        public string PartConPurchasePlan_PurchaseMethod1 {
            get {
                return Web.Resources.EntityStrings.PartConPurchasePlan_PurchaseMethod1;
            }
        }

        public string PartConPurchasePlan_PurchaseMethod2 {
            get {
                return Web.Resources.EntityStrings.PartConPurchasePlan_PurchaseMethod2;
            }
        }

        public string PartConPurchasePlan_PurchaseMethod3 {
            get {
                return Web.Resources.EntityStrings.PartConPurchasePlan_PurchaseMethod3;
            }
        }

        public string PartConPurchasePlan_PurchaseMethod4 {
            get {
                return Web.Resources.EntityStrings.PartConPurchasePlan_PurchaseMethod4;
            }
        }

        public string PartConPurchasePlan_PurchaseToBeConfirm {
            get {
                return Web.Resources.EntityStrings.PartConPurchasePlan_PurchaseToBeConfirm;
            }
        }

        public string PartConPurchasePlan_RePurchaseInLine {
            get {
                return Web.Resources.EntityStrings.PartConPurchasePlan_RePurchaseInLine;
            }
        }

        public string PartConPurchasePlan_ReStock {
            get {
                return Web.Resources.EntityStrings.PartConPurchasePlan_ReStock;
            }
        }

        public string PartConPurchasePlan_SalesPending {
            get {
                return Web.Resources.EntityStrings.PartConPurchasePlan_SalesPending;
            }
        }

        public string PartConPurchasePlan_TheoryMaxStock {
            get {
                return Web.Resources.EntityStrings.PartConPurchasePlan_TheoryMaxStock;
            }
        }

        public string PartConPurchasePlan_TransferInLine {
            get {
                return Web.Resources.EntityStrings.PartConPurchasePlan_TransferInLine;
            }
        }

        public string PartConPurchasePlan_ValidStock {
            get {
                return Web.Resources.EntityStrings.PartConPurchasePlan_ValidStock;
            }
        }

        public string PartNotConPurchasePlan_DefaultOrderWarehouseName {
            get {
                return Web.Resources.EntityStrings.PartNotConPurchasePlan_DefaultOrderWarehouseName;
            }
        }

        public string PartNotConPurchasePlan_EgAdvicePurchase {
            get {
                return Web.Resources.EntityStrings.PartNotConPurchasePlan_EgAdvicePurchase;
            }
        }

        public string PartNotConPurchasePlan_EmergencyArrivalPeriod {
            get {
                return Web.Resources.EntityStrings.PartNotConPurchasePlan_EmergencyArrivalPeriod;
            }
        }

        public string PartNotConPurchasePlan_PartsExchangeOLnums {
            get {
                return Web.Resources.EntityStrings.PartNotConPurchasePlan_PartsExchangeOLnums;
            }
        }

        public string PartNotConPurchasePlan_ExStock {
            get {
                return Web.Resources.EntityStrings.PartNotConPurchasePlan_ExStock;
            }
        }

        public string PartNotConPurchasePlan_IsMeetEmergencyPlan {
            get {
                return Web.Resources.EntityStrings.PartNotConPurchasePlan_IsMeetEmergencyPlan;
            }
        }

        public string PartNotConPurchasePlan_IsMeetSupplementPlan {
            get {
                return Web.Resources.EntityStrings.PartNotConPurchasePlan_IsMeetSupplementPlan;
            }
        }

        public string PartNotConPurchasePlan_LastMonthOutFrequency {
            get {
                return Web.Resources.EntityStrings.PartNotConPurchasePlan_LastMonthOutFrequency;
            }
        }

        public string PartNotConPurchasePlan_MonthlyArrivalPeriod {
            get {
                return Web.Resources.EntityStrings.PartNotConPurchasePlan_MonthlyArrivalPeriod;
            }
        }

        public string PartNotConPurchasePlan_DeliveryTime {
            get {
                return Web.Resources.EntityStrings.PartNotConPurchasePlan_DeliveryTime;
            }
        }

        public string PartNotConPurchasePlan_OrderCycle {
            get {
                return Web.Resources.EntityStrings.PartNotConPurchasePlan_OrderCycle;
            }
        }

        public string PartNotConPurchasePlan_OrderQuantity1 {
            get {
                return Web.Resources.EntityStrings.PartNotConPurchasePlan_OrderQuantity1;
            }
        }

        public string PartNotConPurchasePlan_ORDERQUANTITY2 {
            get {
                return Web.Resources.EntityStrings.PartNotConPurchasePlan_ORDERQUANTITY2;
            }
        }

        public string PartNotConPurchasePlan_ORDERQUANTITY3 {
            get {
                return Web.Resources.EntityStrings.PartNotConPurchasePlan_ORDERQUANTITY3;
            }
        }

        public string PartNotConPurchasePlan_PartABC {
            get {
                return Web.Resources.EntityStrings.PartNotConPurchasePlan_PartABC;
            }
        }

        public string PartNotConPurchasePlan_PlannedPrice {
            get {
                return Web.Resources.EntityStrings.PartNotConPurchasePlan_PlannedPrice;
            }
        }

        public string PartNotConPurchasePlan_PlannedPriceCategory {
            get {
                return Web.Resources.EntityStrings.PartNotConPurchasePlan_PlannedPriceCategory;
            }
        }

        public string PartNotConPurchasePlan_SupplierPartCode {
            get {
                return Web.Resources.EntityStrings.PartNotConPurchasePlan_SupplierPartCode;
            }
        }

        public string PartNotConPurchasePlan_ProductLifeCycle {
            get {
                return Web.Resources.EntityStrings.PartNotConPurchasePlan_ProductLifeCycle;
            }
        }

        public string PartNotConPurchasePlan_PurchaseOLnums {
            get {
                return Web.Resources.EntityStrings.PartNotConPurchasePlan_PurchaseOLnums;
            }
        }

        public string PartNotConPurchasePlan_PCnums {
            get {
                return Web.Resources.EntityStrings.PartNotConPurchasePlan_PCnums;
            }
        }

        public string PartNotConPurchasePlan_ReplacePurchaseOLnums {
            get {
                return Web.Resources.EntityStrings.PartNotConPurchasePlan_ReplacePurchaseOLnums;
            }
        }

        public string PartNotConPurchasePlan_Replacementnums {
            get {
                return Web.Resources.EntityStrings.PartNotConPurchasePlan_Replacementnums;
            }
        }

        public string PartNotConPurchasePlan_RsAdvicePurchase {
            get {
                return Web.Resources.EntityStrings.PartNotConPurchasePlan_RsAdvicePurchase;
            }
        }

        public string PartNotConPurchasePlan_SafeStock {
            get {
                return Web.Resources.EntityStrings.PartNotConPurchasePlan_SafeStock;
            }
        }

        public string PartNotConPurchasePlan_SLNCnums {
            get {
                return Web.Resources.EntityStrings.PartNotConPurchasePlan_SLNCnums;
            }
        }

        public string PartNotConPurchasePlan_TransferOLnums {
            get {
                return Web.Resources.EntityStrings.PartNotConPurchasePlan_TransferOLnums;
            }
        }

        public string PartNotConPurchasePlan_AllValidStocknums {
            get {
                return Web.Resources.EntityStrings.PartNotConPurchasePlan_AllValidStocknums;
            }
        }

        public string PartNotConPurchasePlan_DAYAVGFOR1M {
            get {
                return Web.Resources.EntityStrings.PartNotConPurchasePlan_DAYAVGFOR1M;
            }
        }

        public string PartNotConPurchasePlan_FREQUENCY1 {
            get {
                return Web.Resources.EntityStrings.PartNotConPurchasePlan_FREQUENCY1;
            }
        }

        public string PartNotConPurchasePlan_OrderQuantityFor12M {
            get {
                return Web.Resources.EntityStrings.PartNotConPurchasePlan_OrderQuantityFor12M;
            }
        }

        public string PartNotConPurchasePlan_PartsExchangenums {
            get {
                return Web.Resources.EntityStrings.PartNotConPurchasePlan_PartsExchangenums;
            }
        }

        public string InternalAcquisitionBill_InStatus {
            get {
                return Web.Resources.EntityStrings.InternalAcquisitionBill_InStatus;
            }
        }

        public string LoadingDetail_FaultyPartsAssemblyName {
            get {
                return Web.Resources.EntityStrings.LoadingDetail_FaultyPartsAssemblyName;
            }
        }

        public string LoadingDetail_ResponsibleUnitCode {
            get {
                return Web.Resources.EntityStrings.LoadingDetail_ResponsibleUnitCode;
            }
        }

        public string LoadingDetail_SerialNumber {
            get {
                return Web.Resources.EntityStrings.LoadingDetail_SerialNumber;
            }
        }

        public string ERPDelivery_SyncTime {
            get {
                return Web.Resources.EntityStrings.ERPDelivery_SyncTime;
            }
        }

        public string ERPDelivery_SyncStatus {
            get {
                return Web.Resources.EntityStrings.ERPDelivery_SyncStatus;
            }
        }

        public string ERPDeliveryDetail_ErrorMessage {
            get {
                return Web.Resources.EntityStrings.ERPDeliveryDetail_ErrorMessage;
            }
        }

        public string ERPDelivery_Warehouse_Name {
            get {
                return Web.Resources.EntityStrings.ERPDelivery_Warehouse_Name;
            }
        }

        public string DealerPartsStockQueryView_CustomerType {
            get {
                return Web.Resources.EntityStrings.DealerPartsStockQueryView_CustomerType;
            }
        }

        public string DealerPartsStockQueryView_IsStoreMoreThanZero {
            get {
                return Web.Resources.EntityStrings.DealerPartsStockQueryView_IsStoreMoreThanZero;
            }
        }

        public string DealerPartsStockQueryView_PartName {
            get {
                return Web.Resources.EntityStrings.DealerPartsStockQueryView_PartName;
            }
        }

        public string DealerPartsStockQueryView_ProvinceName {
            get {
                return Web.Resources.EntityStrings.DealerPartsStockQueryView_ProvinceName;
            }
        }

        public string DealerPartsStockQueryView_SalesPrice {
            get {
                return Web.Resources.EntityStrings.DealerPartsStockQueryView_SalesPrice;
            }
        }

        public string DealerPartsStockQueryView_SalesPriceAmount {
            get {
                return Web.Resources.EntityStrings.DealerPartsStockQueryView_SalesPriceAmount;
            }
        }

        public string DealerPartsStockQueryView_StockMaximum {
            get {
                return Web.Resources.EntityStrings.DealerPartsStockQueryView_StockMaximum;
            }
        }

        public string RepairClaimApplication_RepairType {
            get {
                return Web.Resources.EntityStrings.RepairClaimApplication_RepairType;
            }
        }

        public string Customer_AddressType {
            get {
                return Web.Resources.EntityStrings.Customer_AddressType;
            }
        }

        public string Customer_PhoneNumber {
            get {
                return Web.Resources.EntityStrings.Customer_PhoneNumber;
            }
        }

        public string PartsInboundPlanDetail_SpareOrderRemark {
            get {
                return Web.Resources.EntityStrings.PartsInboundPlanDetail_SpareOrderRemark;
            }
        }

        public string PartsInboundCheckBillDetail_SpareOrderRemark {
            get {
                return Web.Resources.EntityStrings.PartsInboundCheckBillDetail_SpareOrderRemark;
            }
        }

        public string ReturnVisitQuestDetail_Choice {
            get {
                return Web.Resources.EntityStrings.ReturnVisitQuestDetail_Choice;
            }
        }

        public string ReturnVisitQuestDetail_ChoiceScore {
            get {
                return Web.Resources.EntityStrings.ReturnVisitQuestDetail_ChoiceScore;
            }
        }

        public string ReturnVisitQuestDetail_QUEST {
            get {
                return Web.Resources.EntityStrings.ReturnVisitQuestDetail_QUEST;
            }
        }

        public string ReturnVisitQuestDetail_TextResponse {
            get {
                return Web.Resources.EntityStrings.ReturnVisitQuestDetail_TextResponse;
            }
        }

        public string ReturnVisitQuest_Channel {
            get {
                return Web.Resources.EntityStrings.ReturnVisitQuest_Channel;
            }
        }

        public string ReturnVisitQuest_CreateTime {
            get {
                return Web.Resources.EntityStrings.ReturnVisitQuest_CreateTime;
            }
        }

        public string ReturnVisitQuest_CreatorName {
            get {
                return Web.Resources.EntityStrings.ReturnVisitQuest_CreatorName;
            }
        }

        public string ReturnVisitQuest_DealerCode {
            get {
                return Web.Resources.EntityStrings.ReturnVisitQuest_DealerCode;
            }
        }

        public string ReturnVisitQuest_DealerName {
            get {
                return Web.Resources.EntityStrings.ReturnVisitQuest_DealerName;
            }
        }

        public string ReturnVisitQuest_Description {
            get {
                return Web.Resources.EntityStrings.ReturnVisitQuest_Description;
            }
        }

        public string ReturnVisitQuest_FailReason {
            get {
                return Web.Resources.EntityStrings.ReturnVisitQuest_FailReason;
            }
        }

        public string ReturnVisitQuest_IsConnection {
            get {
                return Web.Resources.EntityStrings.ReturnVisitQuest_IsConnection;
            }
        }

        public string ReturnVisitQuest_IsTrue {
            get {
                return Web.Resources.EntityStrings.ReturnVisitQuest_IsTrue;
            }
        }

        public string ReturnVisitQuest_OrderId {
            get {
                return Web.Resources.EntityStrings.ReturnVisitQuest_OrderId;
            }
        }

        public string ReturnVisitQuest_PartsSalesCategoryCode {
            get {
                return Web.Resources.EntityStrings.ReturnVisitQuest_PartsSalesCategoryCode;
            }
        }

        public string ReturnVisitQuest_QuestsessionId {
            get {
                return Web.Resources.EntityStrings.ReturnVisitQuest_QuestsessionId;
            }
        }

        public string ReturnVisitQuest_Remark {
            get {
                return Web.Resources.EntityStrings.ReturnVisitQuest_Remark;
            }
        }

        public string ReturnVisitQuest_RevisitDays {
            get {
                return Web.Resources.EntityStrings.ReturnVisitQuest_RevisitDays;
            }
        }

        public string ReturnVisitQuest_Score {
            get {
                return Web.Resources.EntityStrings.ReturnVisitQuest_Score;
            }
        }

        public string ReturnVisitQuest_Status {
            get {
                return Web.Resources.EntityStrings.ReturnVisitQuest_Status;
            }
        }

        public string ReturnVisitQuest_SurverName {
            get {
                return Web.Resources.EntityStrings.ReturnVisitQuest_SurverName;
            }
        }

        public string ReturnVisitQuest_Type {
            get {
                return Web.Resources.EntityStrings.ReturnVisitQuest_Type;
            }
        }

        public string QueryPanel_Title_DispatchingCode {
            get {
                return Web.Resources.EntityStrings.QueryPanel_Title_DispatchingCode;
            }
        }

        public string RepairWorkOrderMd_AccountName {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrderMd_AccountName;
            }
        }

        public string RepairWorkOrderMd_ActiviteNum {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrderMd_ActiviteNum;
            }
        }

        public string RepairWorkOrderMd_ArriverName {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrderMd_ArriverName;
            }
        }

        public string RepairWorkOrderMd_ArriveStationerName {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrderMd_ArriveStationerName;
            }
        }

        public string RepairWorkOrderMd_ArriveStationTime {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrderMd_ArriveStationTime;
            }
        }

        public string RepairWorkOrderMd_ArriveTime {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrderMd_ArriveTime;
            }
        }

        public string RepairWorkOrderMd_CannotHandleReason {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrderMd_CannotHandleReason;
            }
        }

        public string RepairWorkOrderMd_ChewId {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrderMd_ChewId;
            }
        }

        public string RepairWorkOrderMd_Code {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrderMd_Code;
            }
        }

        public string RepairWorkOrderMd_ContactCellPhone {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrderMd_ContactCellPhone;
            }
        }

        public string RepairWorkOrderMd_ContactName {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrderMd_ContactName;
            }
        }

        public string RepairWorkOrderMd_CreateTime {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrderMd_CreateTime;
            }
        }

        public string RepairWorkOrderMd_CreatorName {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrderMd_CreatorName;
            }
        }

        public string RepairWorkOrderMd_DealerCode {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrderMd_DealerCode;
            }
        }

        public string RepairWorkOrderMd_DealerName {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrderMd_DealerName;
            }
        }

        public string RepairWorkOrderMd_Description {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrderMd_Description;
            }
        }

        public string RepairWorkOrderMd_DetailType {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrderMd_DetailType;
            }
        }

        public string RepairWorkOrderMd_DriveMile {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrderMd_DriveMile;
            }
        }

        public string RepairWorkOrderMd_Expect {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrderMd_Expect;
            }
        }

        public string RepairWorkOrderMd_ExpectTime {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrderMd_ExpectTime;
            }
        }

        public string RepairWorkOrderMd_FalutGround {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrderMd_FalutGround;
            }
        }

        public string RepairWorkOrderMd_FaultStmptom {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrderMd_FaultStmptom;
            }
        }

        public string RepairWorkOrderMd_FeedBackTime {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrderMd_FeedBackTime;
            }
        }

        public string RepairWorkOrderMd_FimishName {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrderMd_FimishName;
            }
        }

        public string RepairWorkOrderMd_FimishTime {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrderMd_FimishTime;
            }
        }

        public string RepairWorkOrderMd_HandleDepartment {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrderMd_HandleDepartment;
            }
        }

        public string RepairWorkOrderMd_HandlePerson {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrderMd_HandlePerson;
            }
        }

        public string RepairWorkOrderMd_IsCanFinish {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrderMd_IsCanFinish;
            }
        }

        public string RepairWorkOrderMd_IsUsed {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrderMd_IsUsed;
            }
        }

        public string RepairWorkOrderMd_LeavingName {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrderMd_LeavingName;
            }
        }

        public string RepairWorkOrderMd_LeavingTime {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrderMd_LeavingTime;
            }
        }

        public string RepairWorkOrderMd_ModifierName {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrderMd_ModifierName;
            }
        }

        public string RepairWorkOrderMd_ModifyTime {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrderMd_ModifyTime;
            }
        }

        public string RepairWorkOrderMd_PartArriveTime {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrderMd_PartArriveTime;
            }
        }

        public string RepairWorkOrderMd_PartsCodeName {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrderMd_PartsCodeName;
            }
        }

        public string RepairWorkOrderMd_PartsSalesCategoryCode {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrderMd_PartsSalesCategoryCode;
            }
        }

        public string RepairWorkOrderMd_PartsSalesOrderCode {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrderMd_PartsSalesOrderCode;
            }
        }

        public string RepairWorkOrderMd_PartsSourceChannel {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrderMd_PartsSourceChannel;
            }
        }

        public string RepairWorkOrderMd_ProcessContent {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrderMd_ProcessContent;
            }
        }

        public string RepairWorkOrderMd_ProcessMode {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrderMd_ProcessMode;
            }
        }

        public string RepairWorkOrderMd_ReceiverName {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrderMd_ReceiverName;
            }
        }

        public string RepairWorkOrderMd_ReceiveTime {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrderMd_ReceiveTime;
            }
        }

        public string RepairWorkOrderMd_RefuseReason {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrderMd_RefuseReason;
            }
        }

        public string RepairWorkOrderMd_RejectorName {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrderMd_RejectorName;
            }
        }

        public string RepairWorkOrderMd_RejectTime {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrderMd_RejectTime;
            }
        }

        public string RepairWorkOrderMd_Remark {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrderMd_Remark;
            }
        }

        public string RepairWorkOrderMd_RepairCategory {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrderMd_RepairCategory;
            }
        }

        public string RepairWorkOrderMd_RepairOrderCode {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrderMd_RepairOrderCode;
            }
        }

        public string RepairWorkOrderMd_RevokeName {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrderMd_RevokeName;
            }
        }

        public string RepairWorkOrderMd_RevokeTime {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrderMd_RevokeTime;
            }
        }

        public string RepairWorkOrderMd_SalesCategoryName {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrderMd_SalesCategoryName;
            }
        }

        public string RepairWorkOrderMd_SendTime {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrderMd_SendTime;
            }
        }

        public string RepairWorkOrderMd_SourceChannel {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrderMd_SourceChannel;
            }
        }

        public string RepairWorkOrderMd_Srnum {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrderMd_Srnum;
            }
        }

        public string RepairWorkOrderMd_StartRepairName {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrderMd_StartRepairName;
            }
        }

        public string RepairWorkOrderMd_StartRepairTime {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrderMd_StartRepairTime;
            }
        }

        public string RepairWorkOrderMd_Status {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrderMd_Status;
            }
        }

        public string RepairWorkOrderMd_SubBrand {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrderMd_SubBrand;
            }
        }

        public string RepairWorkOrderMd_SubCompany {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrderMd_SubCompany;
            }
        }

        public string RepairWorkOrderMd_SubType {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrderMd_SubType;
            }
        }

        public string RepairWorkOrderMd_Type {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrderMd_Type;
            }
        }

        public string RepairWorkOrderMd_UrgencyLevel {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrderMd_UrgencyLevel;
            }
        }

        public string RepairWorkOrderMd_UtcdateTime {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrderMd_UtcdateTime;
            }
        }

        public string RepairWorkOrderMd_VinCode {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrderMd_VinCode;
            }
        }

        public string DealerKeyPosition_IsKeyPositions {
            get {
                return Web.Resources.EntityStrings.DealerKeyPosition_IsKeyPositions;
            }
        }

        public string PartsShippingOrder_ArrivalMode {
            get {
                return Web.Resources.EntityStrings.PartsShippingOrder_ArrivalMode;
            }
        }

        public string PartsShippingOrder_ArrivalTime {
            get {
                return Web.Resources.EntityStrings.PartsShippingOrder_ArrivalTime;
            }
        }

        public string PartsShippingOrder_CargoTerminalMsg {
            get {
                return Web.Resources.EntityStrings.PartsShippingOrder_CargoTerminalMsg;
            }
        }

        public string PartsShippingOrder_ExpressCompany {
            get {
                return Web.Resources.EntityStrings.PartsShippingOrder_ExpressCompany;
            }
        }

        public string PartsShippingOrder_FlowFeedback {
            get {
                return Web.Resources.EntityStrings.PartsShippingOrder_FlowFeedback;
            }
        }

        public string PartsShippingOrder_QueryURL {
            get {
                return Web.Resources.EntityStrings.PartsShippingOrder_QueryURL;
            }
        }

        public string PartsShippingOrder_ShippingMsg {
            get {
                return Web.Resources.EntityStrings.PartsShippingOrder_ShippingMsg;
            }
        }

        public string PartsShippingOrder_ShippingTime {
            get {
                return Web.Resources.EntityStrings.PartsShippingOrder_ShippingTime;
            }
        }

        public string PartsShippingOrder_GPSCode {
            get {
                return Web.Resources.EntityStrings.PartsShippingOrder_GPSCode;
            }
        }

        public string OrderApproveWeekday_PartsSalesCategoryName {
            get {
                return Web.Resources.EntityStrings.OrderApproveWeekday_PartsSalesCategoryName;
            }
        }

        public string OrderApproveWeekday_ProvinceName {
            get {
                return Web.Resources.EntityStrings.OrderApproveWeekday_ProvinceName;
            }
        }

        public string OrderApproveWeekday_WeeksName {
            get {
                return Web.Resources.EntityStrings.OrderApproveWeekday_WeeksName;
            }
        }

        public string WarehouseSequence_CustomerCompanyCode {
            get {
                return Web.Resources.EntityStrings.WarehouseSequence_CustomerCompanyCode;
            }
        }

        public string WarehouseSequence_CustomerCompanyName {
            get {
                return Web.Resources.EntityStrings.WarehouseSequence_CustomerCompanyName;
            }
        }

        public string WarehouseSequence_DefaultOutWarehouseName {
            get {
                return Web.Resources.EntityStrings.WarehouseSequence_DefaultOutWarehouseName;
            }
        }

        public string WarehouseSequence_DefaultWarehouseName {
            get {
                return Web.Resources.EntityStrings.WarehouseSequence_DefaultWarehouseName;
            }
        }

        public string WarehouseSequence_FirstOutWarehouseName {
            get {
                return Web.Resources.EntityStrings.WarehouseSequence_FirstOutWarehouseName;
            }
        }

        public string WarehouseSequence_FirstWarehouseName {
            get {
                return Web.Resources.EntityStrings.WarehouseSequence_FirstWarehouseName;
            }
        }

        public string WarehouseSequence_IsAutoApprove {
            get {
                return Web.Resources.EntityStrings.WarehouseSequence_IsAutoApprove;
            }
        }

        public string WarehouseSequence_PartsSalesCategoryName {
            get {
                return Web.Resources.EntityStrings.WarehouseSequence_PartsSalesCategoryName;
            }
        }

        public string WarehouseSequence_SecoundOutWarehouseName {
            get {
                return Web.Resources.EntityStrings.WarehouseSequence_SecoundOutWarehouseName;
            }
        }

        public string WarehouseSequence_SecoundWarehouseName {
            get {
                return Web.Resources.EntityStrings.WarehouseSequence_SecoundWarehouseName;
            }
        }

        public string WarehouseSequence_ThirdOutWarehouseName {
            get {
                return Web.Resources.EntityStrings.WarehouseSequence_ThirdOutWarehouseName;
            }
        }

        public string WarehouseSequence_ThirdWarehouseName {
            get {
                return Web.Resources.EntityStrings.WarehouseSequence_ThirdWarehouseName;
            }
        }

        public string PartsBranch_AutoApproveUpLimit {
            get {
                return Web.Resources.EntityStrings.PartsBranch_AutoApproveUpLimit;
            }
        }

        public string PartsSalesOrder_IsAutoApprove {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrder_IsAutoApprove;
            }
        }

        public string PartsSalesOrder_AutoApproveComment {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrder_AutoApproveComment;
            }
        }

        public string PartsSalesOrder_AutoApproveTime {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrder_AutoApproveTime;
            }
        }

        public string DealerBusinessPermit_EngineRepairPower {
            get {
                return Web.Resources.EntityStrings.DealerBusinessPermit_EngineRepairPower;
            }
        }

        public string IntegralClaimBill_BonusPointsStatus {
            get {
                return Web.Resources.EntityStrings.IntegralClaimBill_BonusPointsStatus;
            }
        }

        public string IntegralClaimBill_BranchCode {
            get {
                return Web.Resources.EntityStrings.IntegralClaimBill_BranchCode;
            }
        }

        public string IntegralClaimBill_BranchName {
            get {
                return Web.Resources.EntityStrings.IntegralClaimBill_BranchName;
            }
        }

        public string IntegralClaimBill_BrandName {
            get {
                return Web.Resources.EntityStrings.IntegralClaimBill_BrandName;
            }
        }

        public string IntegralClaimBill_CellPhoneNumber {
            get {
                return Web.Resources.EntityStrings.IntegralClaimBill_CellPhoneNumber;
            }
        }

        public string IntegralClaimBill_CreateTime {
            get {
                return Web.Resources.EntityStrings.IntegralClaimBill_CreateTime;
            }
        }

        public string IntegralClaimBill_CreatorName {
            get {
                return Web.Resources.EntityStrings.IntegralClaimBill_CreatorName;
            }
        }

        public string IntegralClaimBill_DealerBusinessCode {
            get {
                return Web.Resources.EntityStrings.IntegralClaimBill_DealerBusinessCode;
            }
        }

        public string IntegralClaimBill_DealerCode {
            get {
                return Web.Resources.EntityStrings.IntegralClaimBill_DealerCode;
            }
        }

        public string IntegralClaimBill_DealerName {
            get {
                return Web.Resources.EntityStrings.IntegralClaimBill_DealerName;
            }
        }

        public string IntegralClaimBill_Deductible {
            get {
                return Web.Resources.EntityStrings.IntegralClaimBill_Deductible;
            }
        }

        public string IntegralClaimBill_DeductibleLaborCost {
            get {
                return Web.Resources.EntityStrings.IntegralClaimBill_DeductibleLaborCost;
            }
        }

        public string IntegralClaimBill_DeductibleMaterialCost {
            get {
                return Web.Resources.EntityStrings.IntegralClaimBill_DeductibleMaterialCost;
            }
        }

        public string IntegralClaimBill_IntegralClaimBillCode {
            get {
                return Web.Resources.EntityStrings.IntegralClaimBill_IntegralClaimBillCode;
            }
        }

        public string IntegralClaimBill_IntegralClaimBillType {
            get {
                return Web.Resources.EntityStrings.IntegralClaimBill_IntegralClaimBillType;
            }
        }

        public string IntegralClaimBill_IntegralStatus {
            get {
                return Web.Resources.EntityStrings.IntegralClaimBill_IntegralStatus;
            }
        }

        public string IntegralClaimBill_Memo {
            get {
                return Web.Resources.EntityStrings.IntegralClaimBill_Memo;
            }
        }

        public string IntegralClaimBill_ModifierName {
            get {
                return Web.Resources.EntityStrings.IntegralClaimBill_ModifierName;
            }
        }

        public string IntegralClaimBill_ModifyTime {
            get {
                return Web.Resources.EntityStrings.IntegralClaimBill_ModifyTime;
            }
        }

        public string IntegralClaimBill_OutFeeGrade {
            get {
                return Web.Resources.EntityStrings.IntegralClaimBill_OutFeeGrade;
            }
        }

        public string IntegralClaimBill_RedPacketsCode {
            get {
                return Web.Resources.EntityStrings.IntegralClaimBill_RedPacketsCode;
            }
        }

        public string IntegralClaimBill_RedPacketsPhone {
            get {
                return Web.Resources.EntityStrings.IntegralClaimBill_RedPacketsPhone;
            }
        }

        public string IntegralClaimBill_RepairCode {
            get {
                return Web.Resources.EntityStrings.IntegralClaimBill_RepairCode;
            }
        }

        public string IntegralClaimBill_SettlementStatus {
            get {
                return Web.Resources.EntityStrings.IntegralClaimBill_SettlementStatus;
            }
        }

        public string IntegralClaimBill_UsedIntegral {
            get {
                return Web.Resources.EntityStrings.IntegralClaimBill_UsedIntegral;
            }
        }

        public string IntegralClaimBill_VIP_Code {
            get {
                return Web.Resources.EntityStrings.IntegralClaimBill_VIP_Code;
            }
        }

        public string IntegralClaimBill_VIP_Name {
            get {
                return Web.Resources.EntityStrings.IntegralClaimBill_VIP_Name;
            }
        }

        public string RepairItemAffiServProdLine_EngineRepairPower {
            get {
                return Web.Resources.EntityStrings.RepairItemAffiServProdLine_EngineRepairPower;
            }
        }

        public string PartsSalesOrder_ERPOrderCode {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrder_ERPOrderCode;
            }
        }

        public string VehicleInformation_CustomerCategory {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_CustomerCategory;
            }
        }

        public string PartsShippingOrder_Logistic {
            get {
                return Web.Resources.EntityStrings.PartsShippingOrder_Logistic;
            }
        }

        public string PartsSalesOrder_ERPSourceOrderCode {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrder_ERPSourceOrderCode;
            }
        }

        public string PartsSaleOrder_IsBarter {
            get {
                return Web.Resources.EntityStrings.PartsSaleOrder_IsBarter;
            }
        }

        public string PartsSalesReturnBill_IsBarter {
            get {
                return Web.Resources.EntityStrings.PartsSalesReturnBill_IsBarter;
            }
        }

        public string PartsSalesOrder_IsBarter {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrder_IsBarter;
            }
        }

        public string LogisticCompany_StorageCenter {
            get {
                return Web.Resources.EntityStrings.LogisticCompany_StorageCenter;
            }
        }

        public string DealerInvoiceInformation_InitialApproverComment {
            get {
                return Web.Resources.EntityStrings.DealerInvoiceInformation_InitialApproverComment;
            }
        }

        public string DealerInvoiceInformation_IntegralClaimInvoiceInfo {
            get {
                return Web.Resources.EntityStrings.DealerInvoiceInformation_IntegralClaimInvoiceInfo;
            }
        }

        public string DealerInvoiceInformation_RejectComment {
            get {
                return Web.Resources.EntityStrings.DealerInvoiceInformation_RejectComment;
            }
        }

        public string PartsPurchaseOrder_IfInnerDirectProvision {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseOrder_IfInnerDirectProvision;
            }
        }

        public string Warehouse_IfSync {
            get {
                return Web.Resources.EntityStrings.Warehouse_IfSync;
            }
        }

        public string Warehouse_IsQualityWarehouse {
            get {
                return Web.Resources.EntityStrings.Warehouse_IsQualityWarehouse;
            }
        }

        public string PartsSalesPriceChangeDetail_IsUpsideDown {
            get {
                return Web.Resources.EntityStrings.PartsSalesPriceChangeDetail_IsUpsideDown;
            }
        }

        public string DataEditPanel_Text_Logistics_ApproveTime {
            get {
                return Web.Resources.EntityStrings.DataEditPanel_Text_Logistics_ApproveTime;
            }
        }

        public string DataEditPanel_Text_Logistics_LogisticName {
            get {
                return Web.Resources.EntityStrings.DataEditPanel_Text_Logistics_LogisticName;
            }
        }

        public string DataEditPanel_Text_Logistics_RequestedArrivalDate {
            get {
                return Web.Resources.EntityStrings.DataEditPanel_Text_Logistics_RequestedArrivalDate;
            }
        }

        public string DataEditPanel_Text_Logistics_ShippingDate {
            get {
                return Web.Resources.EntityStrings.DataEditPanel_Text_Logistics_ShippingDate;
            }
        }

        public string DataEditPanel_Text_Logistics_ShippingMethod {
            get {
                return Web.Resources.EntityStrings.DataEditPanel_Text_Logistics_ShippingMethod;
            }
        }

        public string DataEditPanel_Text_Logistics_TransportDriverPhone {
            get {
                return Web.Resources.EntityStrings.DataEditPanel_Text_Logistics_TransportDriverPhone;
            }
        }

        public string DataEditPanel_Text_Logistics_WarehouseName {
            get {
                return Web.Resources.EntityStrings.DataEditPanel_Text_Logistics_WarehouseName;
            }
        }

        public string _Common_提交出错 {
            get {
                return Web.Resources.EntityStrings._Common_提交出错;
            }
        }

        public string ProductStandard_AbandonerName {
            get {
                return Web.Resources.EntityStrings.ProductStandard_AbandonerName;
            }
        }

        public string ProductStandard_AbandonTime {
            get {
                return Web.Resources.EntityStrings.ProductStandard_AbandonTime;
            }
        }

        public string ProductStandard_CreateTime {
            get {
                return Web.Resources.EntityStrings.ProductStandard_CreateTime;
            }
        }

        public string ProductStandard_CreatorName {
            get {
                return Web.Resources.EntityStrings.ProductStandard_CreatorName;
            }
        }

        public string ProductStandard_ModifierName {
            get {
                return Web.Resources.EntityStrings.ProductStandard_ModifierName;
            }
        }

        public string ProductStandard_ModifyTime {
            get {
                return Web.Resources.EntityStrings.ProductStandard_ModifyTime;
            }
        }

        public string ProductStandard_Remark {
            get {
                return Web.Resources.EntityStrings.ProductStandard_Remark;
            }
        }

        public string ProductStandard_StandardCode {
            get {
                return Web.Resources.EntityStrings.ProductStandard_StandardCode;
            }
        }

        public string ProductStandard_StandardName {
            get {
                return Web.Resources.EntityStrings.ProductStandard_StandardName;
            }
        }

        public string ProductStandard_Status {
            get {
                return Web.Resources.EntityStrings.ProductStandard_Status;
            }
        }

        public string TrainingType_TrainingCode {
            get {
                return Web.Resources.EntityStrings.TrainingType_TrainingCode;
            }
        }

        public string TrainingType_TrainingName {
            get {
                return Web.Resources.EntityStrings.TrainingType_TrainingName;
            }
        }

        public string TrainingType_TrainingTypeCode {
            get {
                return Web.Resources.EntityStrings.TrainingType_TrainingTypeCode;
            }
        }

        public string AuthenticationType_AuthenticationTypeCode {
            get {
                return Web.Resources.EntityStrings.AuthenticationType_AuthenticationTypeCode;
            }
        }

        public string SparePart_StandardCode {
            get {
                return Web.Resources.EntityStrings.SparePart_StandardCode;
            }
        }

        public string SparePart_StandardName {
            get {
                return Web.Resources.EntityStrings.SparePart_StandardName;
            }
        }

        public string PartsExchangeGroup_AbandonerName {
            get {
                return Web.Resources.EntityStrings.PartsExchangeGroup_AbandonerName;
            }
        }

        public string PartsExchangeGroup_AbandonTime {
            get {
                return Web.Resources.EntityStrings.PartsExchangeGroup_AbandonTime;
            }
        }

        public string PartsExchangeGroup_CreateTime {
            get {
                return Web.Resources.EntityStrings.PartsExchangeGroup_CreateTime;
            }
        }

        public string PartsExchangeGroup_CreatorName {
            get {
                return Web.Resources.EntityStrings.PartsExchangeGroup_CreatorName;
            }
        }

        public string PartsExchangeGroup_ExchangeCode {
            get {
                return Web.Resources.EntityStrings.PartsExchangeGroup_ExchangeCode;
            }
        }

        public string PartsExchangeGroup_ExGroupCode {
            get {
                return Web.Resources.EntityStrings.PartsExchangeGroup_ExGroupCode;
            }
        }

        public string PartsExchangeGroup_ModifierName {
            get {
                return Web.Resources.EntityStrings.PartsExchangeGroup_ModifierName;
            }
        }

        public string PartsExchangeGroup_ModifyTime {
            get {
                return Web.Resources.EntityStrings.PartsExchangeGroup_ModifyTime;
            }
        }

        public string PartsExchangeGroup_Remark {
            get {
                return Web.Resources.EntityStrings.PartsExchangeGroup_Remark;
            }
        }

        public string PartsExchangeGroup_Status {
            get {
                return Web.Resources.EntityStrings.PartsExchangeGroup_Status;
            }
        }

        public string CAReconciliationList_Abstract {
            get {
                return Web.Resources.EntityStrings.CAReconciliationList_Abstract;
            }
        }

        public string CAReconciliationList_Money {
            get {
                return Web.Resources.EntityStrings.CAReconciliationList_Money;
            }
        }

        public string CAReconciliationList_ReconciliationDirection {
            get {
                return Web.Resources.EntityStrings.CAReconciliationList_ReconciliationDirection;
            }
        }

        public string CAReconciliationList_Time {
            get {
                return Web.Resources.EntityStrings.CAReconciliationList_Time;
            }
        }

        public string CAReconciliation_CorporationCode {
            get {
                return Web.Resources.EntityStrings.CAReconciliation_CorporationCode;
            }
        }

        public string CAReconciliation_CorporationName {
            get {
                return Web.Resources.EntityStrings.CAReconciliation_CorporationName;
            }
        }

        public string CAReconciliation_PartsSalesCategoryName {
            get {
                return Web.Resources.EntityStrings.CAReconciliation_PartsSalesCategoryName;
            }
        }

        public string CAReconciliation_ReconciliationName {
            get {
                return Web.Resources.EntityStrings.CAReconciliation_ReconciliationName;
            }
        }

        public string CAReconciliation_ReconciliationTime {
            get {
                return Web.Resources.EntityStrings.CAReconciliation_ReconciliationTime;
            }
        }

        public string CAReconciliation_StatementCode {
            get {
                return Web.Resources.EntityStrings.CAReconciliation_StatementCode;
            }
        }

        public string CAReconciliation_YeBalance {
            get {
                return Web.Resources.EntityStrings.CAReconciliation_YeBalance;
            }
        }

        public string ExtendedWarrantyOrderList_AgioType {
            get {
                return Web.Resources.EntityStrings.ExtendedWarrantyOrderList_AgioType;
            }
        }

        public string ExtendedWarrantyOrderList_CustomerRetailPrice {
            get {
                return Web.Resources.EntityStrings.ExtendedWarrantyOrderList_CustomerRetailPrice;
            }
        }

        public string ExtendedWarrantyOrderList_DiscountedPrice {
            get {
                return Web.Resources.EntityStrings.ExtendedWarrantyOrderList_DiscountedPrice;
            }
        }

        public string ExtendedWarrantyOrderList_DiscountRate {
            get {
                return Web.Resources.EntityStrings.ExtendedWarrantyOrderList_DiscountRate;
            }
        }

        public string ExtendedWarrantyOrderList_EndDate {
            get {
                return Web.Resources.EntityStrings.ExtendedWarrantyOrderList_EndDate;
            }
        }

        public string ExtendedWarrantyOrderList_EndMileage {
            get {
                return Web.Resources.EntityStrings.ExtendedWarrantyOrderList_EndMileage;
            }
        }

        public string ExtendedWarrantyOrderList_ExtensionCoverage {
            get {
                return Web.Resources.EntityStrings.ExtendedWarrantyOrderList_ExtensionCoverage;
            }
        }

        public string ExtendedWarrantyOrderList_SalesPrice {
            get {
                return Web.Resources.EntityStrings.ExtendedWarrantyOrderList_SalesPrice;
            }
        }

        public string ExtendedWarrantyOrderList_StartMileage {
            get {
                return Web.Resources.EntityStrings.ExtendedWarrantyOrderList_StartMileage;
            }
        }

        public string ExtendedWarrantyOrderList_TotalAmount {
            get {
                return Web.Resources.EntityStrings.ExtendedWarrantyOrderList_TotalAmount;
            }
        }

        public string ExtendedWarrantyOrderList_ValidFrom {
            get {
                return Web.Resources.EntityStrings.ExtendedWarrantyOrderList_ValidFrom;
            }
        }

        public string ExtendedWarrantyOrder_CarInvoicePhotos {
            get {
                return Web.Resources.EntityStrings.ExtendedWarrantyOrder_CarInvoicePhotos;
            }
        }

        public string ExtendedWarrantyOrder_CarSalesDate {
            get {
                return Web.Resources.EntityStrings.ExtendedWarrantyOrder_CarSalesDate;
            }
        }

        public string ExtendedWarrantyOrder_CellNumber {
            get {
                return Web.Resources.EntityStrings.ExtendedWarrantyOrder_CellNumber;
            }
        }

        public string ExtendedWarrantyOrder_CheckerResult {
            get {
                return Web.Resources.EntityStrings.ExtendedWarrantyOrder_CheckerResult;
            }
        }

        public string ExtendedWarrantyOrder_City {
            get {
                return Web.Resources.EntityStrings.ExtendedWarrantyOrder_City;
            }
        }

        public string ExtendedWarrantyOrder_CustomerName {
            get {
                return Web.Resources.EntityStrings.ExtendedWarrantyOrder_CustomerName;
            }
        }

        public string ExtendedWarrantyOrder_DealerCode {
            get {
                return Web.Resources.EntityStrings.ExtendedWarrantyOrder_DealerCode;
            }
        }

        public string ExtendedWarrantyOrder_DealerName {
            get {
                return Web.Resources.EntityStrings.ExtendedWarrantyOrder_DealerName;
            }
        }

        public string ExtendedWarrantyOrder_DealerTotalAmount {
            get {
                return Web.Resources.EntityStrings.ExtendedWarrantyOrder_DealerTotalAmount;
            }
        }

        public string ExtendedWarrantyOrder_DetailAddress {
            get {
                return Web.Resources.EntityStrings.ExtendedWarrantyOrder_DetailAddress;
            }
        }

        public string ExtendedWarrantyOrder_DetailedReasons {
            get {
                return Web.Resources.EntityStrings.ExtendedWarrantyOrder_DetailedReasons;
            }
        }

        public string ExtendedWarrantyOrder_Email {
            get {
                return Web.Resources.EntityStrings.ExtendedWarrantyOrder_Email;
            }
        }

        public string ExtendedWarrantyOrder_EngineCylinder {
            get {
                return Web.Resources.EntityStrings.ExtendedWarrantyOrder_EngineCylinder;
            }
        }

        public string ExtendedWarrantyOrder_EngineModel {
            get {
                return Web.Resources.EntityStrings.ExtendedWarrantyOrder_EngineModel;
            }
        }

        public string ExtendedWarrantyOrder_ExtendedWarrantyOrderCode {
            get {
                return Web.Resources.EntityStrings.ExtendedWarrantyOrder_ExtendedWarrantyOrderCode;
            }
        }

        public string ExtendedWarrantyOrder_IDNumer {
            get {
                return Web.Resources.EntityStrings.ExtendedWarrantyOrder_IDNumer;
            }
        }

        public string ExtendedWarrantyOrder_IDPhoto {
            get {
                return Web.Resources.EntityStrings.ExtendedWarrantyOrder_IDPhoto;
            }
        }

        public string ExtendedWarrantyOrder_IDType {
            get {
                return Web.Resources.EntityStrings.ExtendedWarrantyOrder_IDType;
            }
        }

        public string ExtendedWarrantyOrder_MemberRank {
            get {
                return Web.Resources.EntityStrings.ExtendedWarrantyOrder_MemberRank;
            }
        }

        public string ExtendedWarrantyOrder_Mileage {
            get {
                return Web.Resources.EntityStrings.ExtendedWarrantyOrder_Mileage;
            }
        }

        public string ExtendedWarrantyOrder_MileagePhotos {
            get {
                return Web.Resources.EntityStrings.ExtendedWarrantyOrder_MileagePhotos;
            }
        }

        public string ExtendedWarrantyOrder_PartsSalesCategoryName {
            get {
                return Web.Resources.EntityStrings.ExtendedWarrantyOrder_PartsSalesCategoryName;
            }
        }

        public string ExtendedWarrantyOrder_ProtocolPhotos {
            get {
                return Web.Resources.EntityStrings.ExtendedWarrantyOrder_ProtocolPhotos;
            }
        }

        public string ExtendedWarrantyOrder_Province {
            get {
                return Web.Resources.EntityStrings.ExtendedWarrantyOrder_Province;
            }
        }

        public string ExtendedWarrantyOrder_PurchaseAmount {
            get {
                return Web.Resources.EntityStrings.ExtendedWarrantyOrder_PurchaseAmount;
            }
        }

        public string ExtendedWarrantyOrder_Region {
            get {
                return Web.Resources.EntityStrings.ExtendedWarrantyOrder_Region;
            }
        }

        public string ExtendedWarrantyOrder_Remark {
            get {
                return Web.Resources.EntityStrings.ExtendedWarrantyOrder_Remark;
            }
        }

        public string ExtendedWarrantyOrder_SalesDate {
            get {
                return Web.Resources.EntityStrings.ExtendedWarrantyOrder_SalesDate;
            }
        }

        public string ExtendedWarrantyOrder_SellPersonnel {
            get {
                return Web.Resources.EntityStrings.ExtendedWarrantyOrder_SellPersonnel;
            }
        }

        public string ExtendedWarrantyOrder_ServiceProductLine {
            get {
                return Web.Resources.EntityStrings.ExtendedWarrantyOrder_ServiceProductLine;
            }
        }

        public string ExtendedWarrantyOrder_Status {
            get {
                return Web.Resources.EntityStrings.ExtendedWarrantyOrder_Status;
            }
        }

        public string ExtendedWarrantyOrder_TotalAmount {
            get {
                return Web.Resources.EntityStrings.ExtendedWarrantyOrder_TotalAmount;
            }
        }

        public string ExtendedWarrantyOrder_TravelPermitPhoto {
            get {
                return Web.Resources.EntityStrings.ExtendedWarrantyOrder_TravelPermitPhoto;
            }
        }

        public string ExtendedWarrantyOrder_VehicleLicensePlate {
            get {
                return Web.Resources.EntityStrings.ExtendedWarrantyOrder_VehicleLicensePlate;
            }
        }

        public string ExtendedWarrantyOrder_VIN {
            get {
                return Web.Resources.EntityStrings.ExtendedWarrantyOrder_VIN;
            }
        }

        public string DeferredDiscountType_AbandonerName {
            get {
                return Web.Resources.EntityStrings.DeferredDiscountType_AbandonerName;
            }
        }

        public string DeferredDiscountType_AbandonerTime {
            get {
                return Web.Resources.EntityStrings.DeferredDiscountType_AbandonerTime;
            }
        }

        public string DeferredDiscountType_CreatorName {
            get {
                return Web.Resources.EntityStrings.DeferredDiscountType_CreatorName;
            }
        }

        public string DeferredDiscountType_CreatorTime {
            get {
                return Web.Resources.EntityStrings.DeferredDiscountType_CreatorTime;
            }
        }

        public string DeferredDiscountType_ExtendedWarrantyAgio {
            get {
                return Web.Resources.EntityStrings.DeferredDiscountType_ExtendedWarrantyAgio;
            }
        }

        public string DeferredDiscountType_ExtendedWarrantyAgioType {
            get {
                return Web.Resources.EntityStrings.DeferredDiscountType_ExtendedWarrantyAgioType;
            }
        }

        public string DeferredDiscountType_ExtendedWarrantyProductName {
            get {
                return Web.Resources.EntityStrings.DeferredDiscountType_ExtendedWarrantyProductName;
            }
        }

        public string DeferredDiscountType_ModifierName {
            get {
                return Web.Resources.EntityStrings.DeferredDiscountType_ModifierName;
            }
        }

        public string DeferredDiscountType_ModifierTime {
            get {
                return Web.Resources.EntityStrings.DeferredDiscountType_ModifierTime;
            }
        }

        public string DeferredDiscountType_PartsSalesCategory {
            get {
                return Web.Resources.EntityStrings.DeferredDiscountType_PartsSalesCategory;
            }
        }

        public string DeferredDiscountType_Status {
            get {
                return Web.Resources.EntityStrings.DeferredDiscountType_Status;
            }
        }

        public string ExtendedWarrantyProduct_AbandonerName {
            get {
                return Web.Resources.EntityStrings.ExtendedWarrantyProduct_AbandonerName;
            }
        }

        public string ExtendedWarrantyProduct_AbandonerTime {
            get {
                return Web.Resources.EntityStrings.ExtendedWarrantyProduct_AbandonerTime;
            }
        }

        public string ExtendedWarrantyProduct_AttachmentUpload {
            get {
                return Web.Resources.EntityStrings.ExtendedWarrantyProduct_AttachmentUpload;
            }
        }

        public string ExtendedWarrantyProduct_AutomaticAuditPeriod {
            get {
                return Web.Resources.EntityStrings.ExtendedWarrantyProduct_AutomaticAuditPeriod;
            }
        }

        public string ExtendedWarrantyProduct_CreatorName {
            get {
                return Web.Resources.EntityStrings.ExtendedWarrantyProduct_CreatorName;
            }
        }

        public string ExtendedWarrantyProduct_CreatorTime {
            get {
                return Web.Resources.EntityStrings.ExtendedWarrantyProduct_CreatorTime;
            }
        }

        public string ExtendedWarrantyProduct_CustomerRetailPrice {
            get {
                return Web.Resources.EntityStrings.ExtendedWarrantyProduct_CustomerRetailPrice;
            }
        }

        public string ExtendedWarrantyProduct_ExtendedWarrantyMileage {
            get {
                return Web.Resources.EntityStrings.ExtendedWarrantyProduct_ExtendedWarrantyMileage;
            }
        }

        public string ExtendedWarrantyProduct_ExtendedWarrantyMileageRat {
            get {
                return Web.Resources.EntityStrings.ExtendedWarrantyProduct_ExtendedWarrantyMileageRat;
            }
        }

        public string ExtendedWarrantyProduct_ExtendedWarrantyProductCode {
            get {
                return Web.Resources.EntityStrings.ExtendedWarrantyProduct_ExtendedWarrantyProductCode;
            }
        }

        public string ExtendedWarrantyProduct_ExtendedWarrantyProductName {
            get {
                return Web.Resources.EntityStrings.ExtendedWarrantyProduct_ExtendedWarrantyProductName;
            }
        }

        public string ExtendedWarrantyProduct_ExtendedWarrantyTerm {
            get {
                return Web.Resources.EntityStrings.ExtendedWarrantyProduct_ExtendedWarrantyTerm;
            }
        }

        public string ExtendedWarrantyProduct_ExtendedWarrantyTermRat {
            get {
                return Web.Resources.EntityStrings.ExtendedWarrantyProduct_ExtendedWarrantyTermRat;
            }
        }

        public string ExtendedWarrantyProduct_ExtensionCoverage {
            get {
                return Web.Resources.EntityStrings.ExtendedWarrantyProduct_ExtensionCoverage;
            }
        }

        public string ExtendedWarrantyProduct_ModifierName {
            get {
                return Web.Resources.EntityStrings.ExtendedWarrantyProduct_ModifierName;
            }
        }

        public string ExtendedWarrantyProduct_ModifierTime {
            get {
                return Web.Resources.EntityStrings.ExtendedWarrantyProduct_ModifierTime;
            }
        }

        public string ExtendedWarrantyProduct_OutExtensionCoverage {
            get {
                return Web.Resources.EntityStrings.ExtendedWarrantyProduct_OutExtensionCoverage;
            }
        }

        public string ExtendedWarrantyProduct_PartsSalesCategory {
            get {
                return Web.Resources.EntityStrings.ExtendedWarrantyProduct_PartsSalesCategory;
            }
        }

        public string ExtendedWarrantyProduct_PayEndTimeCondition {
            get {
                return Web.Resources.EntityStrings.ExtendedWarrantyProduct_PayEndTimeCondition;
            }
        }

        public string ExtendedWarrantyProduct_PayEndTimeMileage {
            get {
                return Web.Resources.EntityStrings.ExtendedWarrantyProduct_PayEndTimeMileage;
            }
        }

        public string ExtendedWarrantyProduct_PayStartTimeCondition {
            get {
                return Web.Resources.EntityStrings.ExtendedWarrantyProduct_PayStartTimeCondition;
            }
        }

        public string ExtendedWarrantyProduct_PayStartTimeMileage {
            get {
                return Web.Resources.EntityStrings.ExtendedWarrantyProduct_PayStartTimeMileage;
            }
        }

        public string ExtendedWarrantyProduct_ServiceProductLine {
            get {
                return Web.Resources.EntityStrings.ExtendedWarrantyProduct_ServiceProductLine;
            }
        }

        public string ExtendedWarrantyProduct_Status {
            get {
                return Web.Resources.EntityStrings.ExtendedWarrantyProduct_Status;
            }
        }

        public string ExtendedWarrantyProduct_TradePrice {
            get {
                return Web.Resources.EntityStrings.ExtendedWarrantyProduct_TradePrice;
            }
        }

        public string CAReconciliation_AccountGroupName {
            get {
                return Web.Resources.EntityStrings.CAReconciliation_AccountGroupName;
            }
        }

        public string ExtendedWarrantyOrder_SerialNumber {
            get {
                return Web.Resources.EntityStrings.ExtendedWarrantyOrder_SerialNumber;
            }
        }

        public string MarketABQualityInformation_BatchNumber {
            get {
                return Web.Resources.EntityStrings.MarketABQualityInformation_BatchNumber;
            }
        }

        public string MarketABQualityInformation_CarryWeight {
            get {
                return Web.Resources.EntityStrings.MarketABQualityInformation_CarryWeight;
            }
        }

        public string MarketABQualityInformation_FaultyDescription {
            get {
                return Web.Resources.EntityStrings.MarketABQualityInformation_FaultyDescription;
            }
        }

        public string MarketABQualityInformation_FaultyLocation {
            get {
                return Web.Resources.EntityStrings.MarketABQualityInformation_FaultyLocation;
            }
        }

        public string MarketABQualityInformation_FaultyMileage {
            get {
                return Web.Resources.EntityStrings.MarketABQualityInformation_FaultyMileage;
            }
        }

        public string MarketABQualityInformation_FaultyNature {
            get {
                return Web.Resources.EntityStrings.MarketABQualityInformation_FaultyNature;
            }
        }

        public string MarketABQualityInformation_FaultyQuantity {
            get {
                return Web.Resources.EntityStrings.MarketABQualityInformation_FaultyQuantity;
            }
        }

        public string MarketABQualityInformation_ManagePhone {
            get {
                return Web.Resources.EntityStrings.MarketABQualityInformation_ManagePhone;
            }
        }

        public string MarketABQualityInformation_MessageLevel {
            get {
                return Web.Resources.EntityStrings.MarketABQualityInformation_MessageLevel;
            }
        }

        public string MarketABQualityInformation_StationMasterPhone {
            get {
                return Web.Resources.EntityStrings.MarketABQualityInformation_StationMasterPhone;
            }
        }

        public string FDWarrantyMessage_baogaojianshu {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_baogaojianshu;
            }
        }

        public string FDWarrantyMessage_baoxiushijian {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_baoxiushijian;
            }
        }

        public string FDWarrantyMessage_bukuanjine {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_bukuanjine;
            }
        }

        public string FDWarrantyMessage_cailiaofei {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_cailiaofei;
            }
        }

        public string FDWarrantyMessage_chanpinbianhao {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_chanpinbianhao;
            }
        }

        public string FDWarrantyMessage_chanpinxianleixing {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_chanpinxianleixing;
            }
        }

        public string FDWarrantyMessage_chelianglianxiren {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_chelianglianxiren;
            }
        }

        public string FDWarrantyMessage_chepaihao {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_chepaihao;
            }
        }

        public string FDWarrantyMessage_chexing {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_chexing;
            }
        }

        public string FDWarrantyMessage_chuangjianriqi {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_chuangjianriqi;
            }
        }

        public string FDWarrantyMessage_chuangjianshijian {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_chuangjianshijian;
            }
        }

        public string FDWarrantyMessage_chuchangbianhao {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_chuchangbianhao;
            }
        }

        public string FDWarrantyMessage_chuchangriqi {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_chuchangriqi;
            }
        }

        public string FDWarrantyMessage_danjuzhuangtai {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_danjuzhuangtai;
            }
        }

        public string FDWarrantyMessage_erjifuwuzhanbianhao {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_erjifuwuzhanbianhao;
            }
        }

        public string FDWarrantyMessage_erjifuwuzhandianziyouxiang {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_erjifuwuzhandianziyouxiang;
            }
        }

        public string FDWarrantyMessage_erjifuwuzhandizhi {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_erjifuwuzhandizhi;
            }
        }

        public string FDWarrantyMessage_erjifuwuzhangudingdianhua {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_erjifuwuzhangudingdianhua;
            }
        }

        public string FDWarrantyMessage_erjifuwuzhanjingli {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_erjifuwuzhanjingli;
            }
        }

        public string FDWarrantyMessage_erjifuwuzhanmingcheng {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_erjifuwuzhanmingcheng;
            }
        }

        public string FDWarrantyMessage_erjifuwuzhanqiyeleixing {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_erjifuwuzhanqiyeleixing;
            }
        }

        public string FDWarrantyMessage_erjifuwuzhanzhuangtai {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_erjifuwuzhanzhuangtai;
            }
        }

        public string FDWarrantyMessage_fadongjibianhao {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_fadongjibianhao;
            }
        }

        public string FDWarrantyMessage_fadongjixinghao {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_fadongjixinghao;
            }
        }

        public string FDWarrantyMessage_feiyongfenlei {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_feiyongfenlei;
            }
        }

        public string FDWarrantyMessage_feiyongheji {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_feiyongheji;
            }
        }

        public string FDWarrantyMessage_fengongsibianhao {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_fengongsibianhao;
            }
        }

        public string FDWarrantyMessage_fengongsimingcheng {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_fengongsimingcheng;
            }
        }

        public string FDWarrantyMessage_fengongsishenheshijian {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_fengongsishenheshijian;
            }
        }

        public string FDWarrantyMessage_fuwuchanpinxian {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_fuwuchanpinxian;
            }
        }

        public string FDWarrantyMessage_fuwuhuodongbianhao {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_fuwuhuodongbianhao;
            }
        }

        public string FDWarrantyMessage_fuwuhuodongleixing {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_fuwuhuodongleixing;
            }
        }

        public string FDWarrantyMessage_fuwuhuodongmingcheng {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_fuwuhuodongmingcheng;
            }
        }

        public string FDWarrantyMessage_fuwuhuodongneirong {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_fuwuhuodongneirong;
            }
        }

        public string FDWarrantyMessage_fuwuzhanbianhao {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_fuwuzhanbianhao;
            }
        }

        public string FDWarrantyMessage_fuwuzhanchengliriqi {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_fuwuzhanchengliriqi;
            }
        }

        public string FDWarrantyMessage_fuwuzhanchuanzhen {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_fuwuzhanchuanzhen;
            }
        }

        public string FDWarrantyMessage_fuwuzhandianziyouxiang {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_fuwuzhandianziyouxiang;
            }
        }

        public string FDWarrantyMessage_fuwuzhandizhi {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_fuwuzhandizhi;
            }
        }

        public string FDWarrantyMessage_fuwuzhangudingdianhua {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_fuwuzhangudingdianhua;
            }
        }

        public string FDWarrantyMessage_fuwuzhanjingli {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_fuwuzhanjingli;
            }
        }

        public string FDWarrantyMessage_fuwuzhanlianxiren {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_fuwuzhanlianxiren;
            }
        }

        public string FDWarrantyMessage_fuwuzhanmingcheng {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_fuwuzhanmingcheng;
            }
        }

        public string FDWarrantyMessage_fuwuzhanqiyeleixing {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_fuwuzhanqiyeleixing;
            }
        }

        public string FDWarrantyMessage_fuwuzhanqubianma {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_fuwuzhanqubianma;
            }
        }

        public string FDWarrantyMessage_fuwuzhanqumingcheng {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_fuwuzhanqumingcheng;
            }
        }

        public string FDWarrantyMessage_fuwuzhanshengbianma {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_fuwuzhanshengbianma;
            }
        }

        public string FDWarrantyMessage_fuwuzhanshengmingcheng {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_fuwuzhanshengmingcheng;
            }
        }

        public string FDWarrantyMessage_fuwuzhanshibianma {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_fuwuzhanshibianma;
            }
        }

        public string FDWarrantyMessage_fuwuzhanshimingcheng {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_fuwuzhanshimingcheng;
            }
        }

        public string FDWarrantyMessage_fuwuzhanyouzhengbianma {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_fuwuzhanyouzhengbianma;
            }
        }

        public string FDWarrantyMessage_fuwuzhanzhuangtai {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_fuwuzhanzhuangtai;
            }
        }

        public string FDWarrantyMessage_fuwuzhanzhucemingcheng {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_fuwuzhanzhucemingcheng;
            }
        }

        public string FDWarrantyMessage_fuwuzhanzhucezhenghao {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_fuwuzhanzhucezhenghao;
            }
        }

        public string FDWarrantyMessage_gaosugonglutuochefei {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_gaosugonglutuochefei;
            }
        }

        public string FDWarrantyMessage_gengxinshijian {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_gengxinshijian;
            }
        }

        public string FDWarrantyMessage_gonggaohao {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_gonggaohao;
            }
        }

        public string FDWarrantyMessage_gongkuangleibie {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_gongkuangleibie;
            }
        }

        public string FDWarrantyMessage_gongshifei {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_gongshifei;
            }
        }

        public string FDWarrantyMessage_gongyingshangquerenyijian {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_gongyingshangquerenyijian;
            }
        }

        public string FDWarrantyMessage_gongyingshangquerenzhuangtai {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_gongyingshangquerenzhuangtai;
            }
        }

        public string FDWarrantyMessage_goumairiqi {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_goumairiqi;
            }
        }

        public string FDWarrantyMessage_guzhangbeizhu {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_guzhangbeizhu;
            }
        }

        public string FDWarrantyMessage_guzhangdaima {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_guzhangdaima;
            }
        }

        public string FDWarrantyMessage_guzhangjianqingtuiyunfei {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_guzhangjianqingtuiyunfei;
            }
        }

        public string FDWarrantyMessage_guzhangmoshi {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_guzhangmoshi;
            }
        }

        public string FDWarrantyMessage_guzhangxianxiangmiaoshu {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_guzhangxianxiangmiaoshu;
            }
        }

        public string FDWarrantyMessage_huoshoujianmingcheng {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_huoshoujianmingcheng;
            }
        }

        public string FDWarrantyMessage_huoshoujianshengchanchangjia {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_huoshoujianshengchanchangjia;
            }
        }

        public string FDWarrantyMessage_huoshoujiansuoshuzongcheng {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_huoshoujiansuoshuzongcheng;
            }
        }

        public string FDWarrantyMessage_huoshoujiantuhao {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_huoshoujiantuhao;
            }
        }

        public string FDWarrantyMessage_isshiyebusuopei {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_isshiyebusuopei;
            }
        }

        public string FDWarrantyMessage_iszerengongyingshangsuopei {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_iszerengongyingshangsuopei;
            }
        }

        public string FDWarrantyMessage_jiesuanshijian {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_jiesuanshijian;
            }
        }

        public string FDWarrantyMessage_jiesuanzhuangtai {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_jiesuanzhuangtai;
            }
        }

        public string FDWarrantyMessage_kehudizhi {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_kehudizhi;
            }
        }

        public string FDWarrantyMessage_kehuleixing {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_kehuleixing;
            }
        }

        public string FDWarrantyMessage_koukuanjine {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_koukuanjine;
            }
        }

        public string FDWarrantyMessage_kuaibaodaihao {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_kuaibaodaihao;
            }
        }

        public string FDWarrantyMessage_peijianguanlifei {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_peijianguanlifei;
            }
        }

        public string FDWarrantyMessage_peijiansuoshuzongcheng {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_peijiansuoshuzongcheng;
            }
        }

        public string FDWarrantyMessage_pinpai {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_pinpai;
            }
        }

        public string FDWarrantyMessage_pizhunren {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_pizhunren;
            }
        }

        public string FDWarrantyMessage_qitafeiyong {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_qitafeiyong;
            }
        }

        public string FDWarrantyMessage_qudongxingshi {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_qudongxingshi;
            }
        }

        public string FDWarrantyMessage_shejicheliang {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_shejicheliang;
            }
        }

        public string FDWarrantyMessage_shejijine {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_shejijine;
            }
        }

        public string FDWarrantyMessage_shendanleixing {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_shendanleixing;
            }
        }

        public string FDWarrantyMessage_shichangABkuaibaobianhao {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_shichangABkuaibaobianhao;
            }
        }

        public string FDWarrantyMessage_shichangbu {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_shichangbu;
            }
        }

        public string FDWarrantyMessage_shichangbuyijian {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_shichangbuyijian;
            }
        }

        public string FDWarrantyMessage_shouciguzhanglicheng {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_shouciguzhanglicheng;
            }
        }

        public string FDWarrantyMessage_suopeigongyingshangbianhao {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_suopeigongyingshangbianhao;
            }
        }

        public string FDWarrantyMessage_suopeigongyingshangmingcheng {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_suopeigongyingshangmingcheng;
            }
        }

        public string FDWarrantyMessage_Vin {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_Vin;
            }
        }

        public string FDWarrantyMessage_waichufuwufei {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_waichufuwufei;
            }
        }

        public string FDWarrantyMessage_waichugonglishu {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_waichugonglishu;
            }
        }

        public string FDWarrantyMessage_waichuleixing {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_waichuleixing;
            }
        }

        public string FDWarrantyMessage_waichulicheng {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_waichulicheng;
            }
        }

        public string FDWarrantyMessage_waichulutubuzhudanjia {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_waichulutubuzhudanjia;
            }
        }

        public string FDWarrantyMessage_waichuqitafeiyongshuoming {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_waichuqitafeiyongshuoming;
            }
        }

        public string FDWarrantyMessage_waichurenshu {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_waichurenshu;
            }
        }

        public string FDWarrantyMessage_waichushenheyijian {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_waichushenheyijian;
            }
        }

        public string FDWarrantyMessage_waichusuopeibeizhuxinxi {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_waichusuopeibeizhuxinxi;
            }
        }

        public string FDWarrantyMessage_waichusuopeishenqingdanhao {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_waichusuopeishenqingdanhao;
            }
        }

        public string FDWarrantyMessage_waichutianshu {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_waichutianshu;
            }
        }

        public string FDWarrantyMessage_waichuyuanyin {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_waichuyuanyin;
            }
        }

        public string FDWarrantyMessage_weixiuleixing {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_weixiuleixing;
            }
        }

        public string FDWarrantyMessage_weixiushuxing {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_weixiushuxing;
            }
        }

        public string FDWarrantyMessage_weixiusuopeishenqingdanhao {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_weixiusuopeishenqingdanhao;
            }
        }

        public string FDWarrantyMessage_weixiusuopeishenqingleixing {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_weixiusuopeishenqingleixing;
            }
        }

        public string FDWarrantyMessage_wenjianbianhao {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_wenjianbianhao;
            }
        }

        public string FDWarrantyMessage_wentimiaoshu {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_wentimiaoshu;
            }
        }

        public string FDWarrantyMessage_xingshilicheng {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_xingshilicheng;
            }
        }

        public string FDWarrantyMessage_yewubianhao {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_yewubianhao;
            }
        }

        public string FDWarrantyMessage_zerendanweibianma {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_zerendanweibianma;
            }
        }

        public string FDWarrantyMessage_zerendanweimingcheng {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_zerendanweimingcheng;
            }
        }

        public string FDWarrantyMessage_zhengchechanpinxian {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_zhengchechanpinxian;
            }
        }

        public string FDWarrantyMessage_zhenggaiweixiufangan {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_zhenggaiweixiufangan;
            }
        }

        public string FDWarrantyMessage_zhiliangbuyijian {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_zhiliangbuyijian;
            }
        }

        public string FDWarrantyMessage_zhongshenren {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_zhongshenren;
            }
        }

        public string FDWarrantyMessage_zhongshenshijian {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_zhongshenshijian;
            }
        }

        public string FDWarrantyMessage_zhongshenyijian {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_zhongshenyijian;
            }
        }

        public string FDWarrantyMessage_zhuangtai {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_zhuangtai;
            }
        }

        public string FDWarrantyMessage_zongchengjianmingcheng {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_zongchengjianmingcheng;
            }
        }

        public string FDWarrantyMessage_ {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_;
            }
        }

        public string FDWarrantyMessage_suopeidanbianhao {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_suopeidanbianhao;
            }
        }

        public string FDWarrantyMessage_waichusuopeidanzhuangtai {
            get {
                return Web.Resources.EntityStrings.FDWarrantyMessage_waichusuopeidanzhuangtai;
            }
        }

        public string AgencyLogisticCompany_AbandonerName {
            get {
                return Web.Resources.EntityStrings.AgencyLogisticCompany_AbandonerName;
            }
        }

        public string AgencyLogisticCompany_AbandonerTime {
            get {
                return Web.Resources.EntityStrings.AgencyLogisticCompany_AbandonerTime;
            }
        }

        public string AgencyLogisticCompany_Code {
            get {
                return Web.Resources.EntityStrings.AgencyLogisticCompany_Code;
            }
        }

        public string AgencyLogisticCompany_CreateTime {
            get {
                return Web.Resources.EntityStrings.AgencyLogisticCompany_CreateTime;
            }
        }

        public string AgencyLogisticCompany_CreatorName {
            get {
                return Web.Resources.EntityStrings.AgencyLogisticCompany_CreatorName;
            }
        }

        public string AgencyLogisticCompany_ModifierName {
            get {
                return Web.Resources.EntityStrings.AgencyLogisticCompany_ModifierName;
            }
        }

        public string AgencyLogisticCompany_ModifyTime {
            get {
                return Web.Resources.EntityStrings.AgencyLogisticCompany_ModifyTime;
            }
        }

        public string AgencyLogisticCompany_Name {
            get {
                return Web.Resources.EntityStrings.AgencyLogisticCompany_Name;
            }
        }

        public string AgencyLogisticCompany_Remark {
            get {
                return Web.Resources.EntityStrings.AgencyLogisticCompany_Remark;
            }
        }

        public string AgencyLogisticCompany_Status {
            get {
                return Web.Resources.EntityStrings.AgencyLogisticCompany_Status;
            }
        }

        public string AgencyRetailerList_ConfirmedAmount {
            get {
                return Web.Resources.EntityStrings.AgencyRetailerList_ConfirmedAmount;
            }
        }

        public string AgencyRetailerList_OrderedQuantity {
            get {
                return Web.Resources.EntityStrings.AgencyRetailerList_OrderedQuantity;
            }
        }

        public string AgencyRetailerList_OrderSum {
            get {
                return Web.Resources.EntityStrings.AgencyRetailerList_OrderSum;
            }
        }

        public string AgencyRetailerList_SparePartCode {
            get {
                return Web.Resources.EntityStrings.AgencyRetailerList_SparePartCode;
            }
        }

        public string AgencyRetailerList_SparePartName {
            get {
                return Web.Resources.EntityStrings.AgencyRetailerList_SparePartName;
            }
        }

        public string AgencyRetailerList_UnitPrice {
            get {
                return Web.Resources.EntityStrings.AgencyRetailerList_UnitPrice;
            }
        }

        public string AgencyRetailerOrder_BranchName {
            get {
                return Web.Resources.EntityStrings.AgencyRetailerOrder_BranchName;
            }
        }

        public string AgencyRetailerOrder_ConfirmorTime {
            get {
                return Web.Resources.EntityStrings.AgencyRetailerOrder_ConfirmorTime;
            }
        }

        public string AgencyRetailerOrder_PartsSalesCategoryName {
            get {
                return Web.Resources.EntityStrings.AgencyRetailerOrder_PartsSalesCategoryName;
            }
        }

        public string AgencyRetailerOrder_ShippingCompanyCode {
            get {
                return Web.Resources.EntityStrings.AgencyRetailerOrder_ShippingCompanyCode;
            }
        }

        public string AgencyRetailerOrder_ShippingCompanyName {
            get {
                return Web.Resources.EntityStrings.AgencyRetailerOrder_ShippingCompanyName;
            }
        }

        public string AgencyRetailerOrder_ShippingStatus {
            get {
                return Web.Resources.EntityStrings.AgencyRetailerOrder_ShippingStatus;
            }
        }

        public string AgencyRetailerOrder_VehiclePartsHandleOrderCode {
            get {
                return Web.Resources.EntityStrings.AgencyRetailerOrder_VehiclePartsHandleOrderCode;
            }
        }

        public string AgencyRetailerOrder_WarehouseCode {
            get {
                return Web.Resources.EntityStrings.AgencyRetailerOrder_WarehouseCode;
            }
        }

        public string AgencyRetailerOrder_WarehouseName {
            get {
                return Web.Resources.EntityStrings.AgencyRetailerOrder_WarehouseName;
            }
        }

        public string AgencyRetailerOrder_CloserName {
            get {
                return Web.Resources.EntityStrings.AgencyRetailerOrder_CloserName;
            }
        }

        public string AgencyRetailerOrder_CloserTime {
            get {
                return Web.Resources.EntityStrings.AgencyRetailerOrder_CloserTime;
            }
        }

        public string AgencyRetailerOrder_Code {
            get {
                return Web.Resources.EntityStrings.AgencyRetailerOrder_Code;
            }
        }

        public string AgencyPartsShippingOrder_Code {
            get {
                return Web.Resources.EntityStrings.AgencyPartsShippingOrder_Code;
            }
        }

        public string AgencyPartsShippingOrder_ConfirmedReceptionTime {
            get {
                return Web.Resources.EntityStrings.AgencyPartsShippingOrder_ConfirmedReceptionTime;
            }
        }

        public string AgencyPartsShippingOrder_DeliveryBillNumber {
            get {
                return Web.Resources.EntityStrings.AgencyPartsShippingOrder_DeliveryBillNumber;
            }
        }

        public string AgencyPartsShippingOrder_GPSCode {
            get {
                return Web.Resources.EntityStrings.AgencyPartsShippingOrder_GPSCode;
            }
        }

        public string AgencyPartsShippingOrder_InterfaceRecordId {
            get {
                return Web.Resources.EntityStrings.AgencyPartsShippingOrder_InterfaceRecordId;
            }
        }

        public string AgencyPartsShippingOrder_LogisticArrivalDate {
            get {
                return Web.Resources.EntityStrings.AgencyPartsShippingOrder_LogisticArrivalDate;
            }
        }

        public string AgencyPartsShippingOrder_LogisticCompanyCode {
            get {
                return Web.Resources.EntityStrings.AgencyPartsShippingOrder_LogisticCompanyCode;
            }
        }

        public string AgencyPartsShippingOrder_LogisticCompanyName {
            get {
                return Web.Resources.EntityStrings.AgencyPartsShippingOrder_LogisticCompanyName;
            }
        }

        public string AgencyPartsShippingOrder_OriginalRequirementBillCode {
            get {
                return Web.Resources.EntityStrings.AgencyPartsShippingOrder_OriginalRequirementBillCode;
            }
        }

        public string AgencyPartsShippingOrder_ReceivingAddress {
            get {
                return Web.Resources.EntityStrings.AgencyPartsShippingOrder_ReceivingAddress;
            }
        }

        public string AgencyPartsShippingOrder_ReceivingCompanyCode {
            get {
                return Web.Resources.EntityStrings.AgencyPartsShippingOrder_ReceivingCompanyCode;
            }
        }

        public string AgencyPartsShippingOrder_ReceivingCompanyName {
            get {
                return Web.Resources.EntityStrings.AgencyPartsShippingOrder_ReceivingCompanyName;
            }
        }

        public string AgencyPartsShippingOrder_ReceivingWarehouseName {
            get {
                return Web.Resources.EntityStrings.AgencyPartsShippingOrder_ReceivingWarehouseName;
            }
        }

        public string AgencyPartsShippingOrder_RequestedArrivalDate {
            get {
                return Web.Resources.EntityStrings.AgencyPartsShippingOrder_RequestedArrivalDate;
            }
        }

        public string AgencyPartsShippingOrder_ShippingCompanyCode {
            get {
                return Web.Resources.EntityStrings.AgencyPartsShippingOrder_ShippingCompanyCode;
            }
        }

        public string AgencyPartsShippingOrder_ShippingCompanyName {
            get {
                return Web.Resources.EntityStrings.AgencyPartsShippingOrder_ShippingCompanyName;
            }
        }

        public string AgencyPartsShippingOrder_ShippingMethod {
            get {
                return Web.Resources.EntityStrings.AgencyPartsShippingOrder_ShippingMethod;
            }
        }

        public string AgencyPartsShippingOrder_TransportDriver {
            get {
                return Web.Resources.EntityStrings.AgencyPartsShippingOrder_TransportDriver;
            }
        }

        public string AgencyPartsShippingOrder_TransportDriverPhone {
            get {
                return Web.Resources.EntityStrings.AgencyPartsShippingOrder_TransportDriverPhone;
            }
        }

        public string AgencyPartsShippingOrder_TransportMileage {
            get {
                return Web.Resources.EntityStrings.AgencyPartsShippingOrder_TransportMileage;
            }
        }

        public string AgencyPartsShippingOrder_Type {
            get {
                return Web.Resources.EntityStrings.AgencyPartsShippingOrder_Type;
            }
        }

        public string AgencyPartsShippingOrderDetail_ConfirmedAmount {
            get {
                return Web.Resources.EntityStrings.AgencyPartsShippingOrderDetail_ConfirmedAmount;
            }
        }

        public string AgencyPartsShippingOrderDetail_DifferenceClassification {
            get {
                return Web.Resources.EntityStrings.AgencyPartsShippingOrderDetail_DifferenceClassification;
            }
        }

        public string AgencyPartsShippingOrderDetail_InTransitDamageLossAmount {
            get {
                return Web.Resources.EntityStrings.AgencyPartsShippingOrderDetail_InTransitDamageLossAmount;
            }
        }

        public string AgencyPartsShippingOrderDetail_SettlementPrice {
            get {
                return Web.Resources.EntityStrings.AgencyPartsShippingOrderDetail_SettlementPrice;
            }
        }

        public string AgencyPartsShippingOrderDetail_ShippingAmount {
            get {
                return Web.Resources.EntityStrings.AgencyPartsShippingOrderDetail_ShippingAmount;
            }
        }

        public string AgencyPartsShippingOrderDetail_SparePartCode {
            get {
                return Web.Resources.EntityStrings.AgencyPartsShippingOrderDetail_SparePartCode;
            }
        }

        public string AgencyPartsShippingOrderDetail_SparePartName {
            get {
                return Web.Resources.EntityStrings.AgencyPartsShippingOrderDetail_SparePartName;
            }
        }

        public string AgencyPartsShippingOrderDetail_TransportLossesDisposeMethod {
            get {
                return Web.Resources.EntityStrings.AgencyPartsShippingOrderDetail_TransportLossesDisposeMethod;
            }
        }

        public string Malfunction_ISISG {
            get {
                return Web.Resources.EntityStrings.Malfunction_ISISG;
            }
        }

        public string VehicleInformation_BOUND_TIME {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_BOUND_TIME;
            }
        }

        public string VehicleInformation_COMM_CODE {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_COMM_CODE;
            }
        }

        public string VehicleInformation_DEVICE_TYPE {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_DEVICE_TYPE;
            }
        }

        public string VehicleInformation_DID {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_DID;
            }
        }

        public string VehicleInformation_EXT_SIMCODE {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_EXT_SIMCODE;
            }
        }

        public string VehicleInformation_IS_FINANCE {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_IS_FINANCE;
            }
        }

        public string VehicleInformation_SN {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_SN;
            }
        }

        public string VehicleInformation_STD_SIMCODE {
            get {
                return Web.Resources.EntityStrings.VehicleInformation_STD_SIMCODE;
            }
        }

        public string Notification_ReplyFilePath {
            get {
                return Web.Resources.EntityStrings.Notification_ReplyFilePath;
            }
        }

        public string RepairOrder_TrailerMileage {
            get {
                return Web.Resources.EntityStrings.RepairOrder_TrailerMileage;
            }
        }

        public string DealerPartsTransferOrder_Code {
            get {
                return Web.Resources.EntityStrings.DealerPartsTransferOrder_Code;
            }
        }

        public string DealerPartsTransferOrder_SourceCode {
            get {
                return Web.Resources.EntityStrings.DealerPartsTransferOrder_SourceCode;
            }
        }

        public string DealerPartsTransferOrder_Status {
            get {
                return Web.Resources.EntityStrings.DealerPartsTransferOrder_Status;
            }
        }

        public string DealerPartsTransferOrder_SumNoWarrantyPrice {
            get {
                return Web.Resources.EntityStrings.DealerPartsTransferOrder_SumNoWarrantyPrice;
            }
        }

        public string DealerPartsTransferOrder_SumWarrantyPrice {
            get {
                return Web.Resources.EntityStrings.DealerPartsTransferOrder_SumWarrantyPrice;
            }
        }

        public string DealerPartsTransOrderDetail_Amount {
            get {
                return Web.Resources.EntityStrings.DealerPartsTransOrderDetail_Amount;
            }
        }

        public string DealerPartsTransOrderDetail_DiffPrice {
            get {
                return Web.Resources.EntityStrings.DealerPartsTransOrderDetail_DiffPrice;
            }
        }

        public string DealerPartsTransOrderDetail_NoWarrantyPrice {
            get {
                return Web.Resources.EntityStrings.DealerPartsTransOrderDetail_NoWarrantyPrice;
            }
        }

        public string DealerPartsTransOrderDetail_SparePartCode {
            get {
                return Web.Resources.EntityStrings.DealerPartsTransOrderDetail_SparePartCode;
            }
        }

        public string DealerPartsTransOrderDetail_SparePartName {
            get {
                return Web.Resources.EntityStrings.DealerPartsTransOrderDetail_SparePartName;
            }
        }

        public string DealerPartsTransOrderDetail_WarrantyPrice {
            get {
                return Web.Resources.EntityStrings.DealerPartsTransOrderDetail_WarrantyPrice;
            }
        }

        public string ExpressVehicleList_BatchNumber {
            get {
                return Web.Resources.EntityStrings.ExpressVehicleList_BatchNumber;
            }
        }

        public string ExpressVehicleList_FaultTime {
            get {
                return Web.Resources.EntityStrings.ExpressVehicleList_FaultTime;
            }
        }

        public string ExpressVehicleList_FaultyMileage {
            get {
                return Web.Resources.EntityStrings.ExpressVehicleList_FaultyMileage;
            }
        }

        public string ExpressVehicleList_OutOfFactoryDate {
            get {
                return Web.Resources.EntityStrings.ExpressVehicleList_OutOfFactoryDate;
            }
        }

        public string ExpressVehicleList_PartsSalesCategoryName {
            get {
                return Web.Resources.EntityStrings.ExpressVehicleList_PartsSalesCategoryName;
            }
        }

        public string ExpressVehicleList_SalesDate {
            get {
                return Web.Resources.EntityStrings.ExpressVehicleList_SalesDate;
            }
        }

        public string ExpressVehicleList_VehicleCode {
            get {
                return Web.Resources.EntityStrings.ExpressVehicleList_VehicleCode;
            }
        }

        public string ExpressVehicleList_VIN {
            get {
                return Web.Resources.EntityStrings.ExpressVehicleList_VIN;
            }
        }

        public string ExtendedSparePartList_ReqPart {
            get {
                return Web.Resources.EntityStrings.ExtendedSparePartList_ReqPart;
            }
        }

        public string ExtendedSparePartList_SparePartCode {
            get {
                return Web.Resources.EntityStrings.ExtendedSparePartList_SparePartCode;
            }
        }

        public string ExtendedSparePartList_SparePartName {
            get {
                return Web.Resources.EntityStrings.ExtendedSparePartList_SparePartName;
            }
        }

        public string CAReconciliation_AfterAdjustmentAmount {
            get {
                return Web.Resources.EntityStrings.CAReconciliation_AfterAdjustmentAmount;
            }
        }

        public string CAReconciliation_CustomerAccountAmount {
            get {
                return Web.Resources.EntityStrings.CAReconciliation_CustomerAccountAmount;
            }
        }

        public string CAReconciliation_NoArrivalAdd {
            get {
                return Web.Resources.EntityStrings.CAReconciliation_NoArrivalAdd;
            }
        }

        public string CAReconciliation_NoArrivalReduce {
            get {
                return Web.Resources.EntityStrings.CAReconciliation_NoArrivalReduce;
            }
        }

        public string IntegralClaimBill_ApprovalComment {
            get {
                return Web.Resources.EntityStrings.IntegralClaimBill_ApprovalComment;
            }
        }

        public string IntegralClaimBill_CheckerName {
            get {
                return Web.Resources.EntityStrings.IntegralClaimBill_CheckerName;
            }
        }

        public string IntegralClaimBill_CheckTime {
            get {
                return Web.Resources.EntityStrings.IntegralClaimBill_CheckTime;
            }
        }

        public string IntegralClaimBill_RejectComment {
            get {
                return Web.Resources.EntityStrings.IntegralClaimBill_RejectComment;
            }
        }

        public string RepairOrder_TestResults {
            get {
                return Web.Resources.EntityStrings.RepairOrder_TestResults;
            }
        }

        public string FundRedPostVehicleMsg_BrandName {
            get {
                return Web.Resources.EntityStrings.FundRedPostVehicleMsg_BrandName;
            }
        }

        public string FundRedPostVehicleMsg_CellPhoneNumber {
            get {
                return Web.Resources.EntityStrings.FundRedPostVehicleMsg_CellPhoneNumber;
            }
        }

        public string FundRedPostVehicleMsg_CustomerName {
            get {
                return Web.Resources.EntityStrings.FundRedPostVehicleMsg_CustomerName;
            }
        }

        public string FundRedPostVehicleMsg_ExpireDate {
            get {
                return Web.Resources.EntityStrings.FundRedPostVehicleMsg_ExpireDate;
            }
        }

        public string FundRedPostVehicleMsg_LimitPrice {
            get {
                return Web.Resources.EntityStrings.FundRedPostVehicleMsg_LimitPrice;
            }
        }

        public string FundRedPostVehicleMsg_LssuingUnit {
            get {
                return Web.Resources.EntityStrings.FundRedPostVehicleMsg_LssuingUnit;
            }
        }

        public string FundRedPostVehicleMsg_RedPostAmount {
            get {
                return Web.Resources.EntityStrings.FundRedPostVehicleMsg_RedPostAmount;
            }
        }

        public string FundRedPostVehicleMsg_RedPostCode {
            get {
                return Web.Resources.EntityStrings.FundRedPostVehicleMsg_RedPostCode;
            }
        }

        public string FundRedPostVehicleMsg_RedPostDescribe {
            get {
                return Web.Resources.EntityStrings.FundRedPostVehicleMsg_RedPostDescribe;
            }
        }

        public string FundRedPostVehicleMsg_RedPostName {
            get {
                return Web.Resources.EntityStrings.FundRedPostVehicleMsg_RedPostName;
            }
        }

        public string FundRedPostVehicleMsg_RepairContractCode {
            get {
                return Web.Resources.EntityStrings.FundRedPostVehicleMsg_RepairContractCode;
            }
        }

        public string FundRedPostVehicleMsg_ValidationDate {
            get {
                return Web.Resources.EntityStrings.FundRedPostVehicleMsg_ValidationDate;
            }
        }

        public string SubDealer_BusinessAddressLatitude {
            get {
                return Web.Resources.EntityStrings.SubDealer_BusinessAddressLatitude;
            }
        }

        public string SubDealer_BusinessAddressLongitude {
            get {
                return Web.Resources.EntityStrings.SubDealer_BusinessAddressLongitude;
            }
        }

        public string RepairWorkOrderMd_MemberType {
            get {
                return Web.Resources.EntityStrings.RepairWorkOrderMd_MemberType;
            }
        }

        public string PartsInboundPlan_PlanDeliveryTime {
            get {
                return Web.Resources.EntityStrings.PartsInboundPlan_PlanDeliveryTime;
            }
        }

        public string PartsInboundCheckBillDetail_IsPacking {
            get {
                return Web.Resources.EntityStrings.PartsInboundCheckBillDetail_IsPacking;
            }
        }

        public string PartsInboundCheckBillDetail_ThisInspectedQuantity {
            get {
                return Web.Resources.EntityStrings.PartsInboundCheckBillDetail_ThisInspectedQuantity;
            }
        }

        public string CustomerDirectSpareList_CustomerCode {
            get {
                return Web.Resources.EntityStrings.CustomerDirectSpareList_CustomerCode;
            }
        }

        public string CustomerDirectSpareList_CustomerName {
            get {
                return Web.Resources.EntityStrings.CustomerDirectSpareList_CustomerName;
            }
        }

        public string CustomerDirectSpareList_IfDirectProvision {
            get {
                return Web.Resources.EntityStrings.CustomerDirectSpareList_IfDirectProvision;
            }
        }

        public string CustomerDirectSpareList_MeasureUnit {
            get {
                return Web.Resources.EntityStrings.CustomerDirectSpareList_MeasureUnit;
            }
        }

        public string CustomerDirectSpareList_PartsSalesCategoryName {
            get {
                return Web.Resources.EntityStrings.CustomerDirectSpareList_PartsSalesCategoryName;
            }
        }

        public string CustomerDirectSpareList_SparePartCode {
            get {
                return Web.Resources.EntityStrings.CustomerDirectSpareList_SparePartCode;
            }
        }

        public string CustomerDirectSpareList_SparePartName {
            get {
                return Web.Resources.EntityStrings.CustomerDirectSpareList_SparePartName;
            }
        }

        public string SalesUnit_SubmitCompanyId {
            get {
                return Web.Resources.EntityStrings.SalesUnit_SubmitCompanyId;
            }
        }

        public string CustomerSupplyInitialFeeSet_CustomerCode {
            get {
                return Web.Resources.EntityStrings.CustomerSupplyInitialFeeSet_CustomerCode;
            }
        }

        public string CustomerSupplyInitialFeeSet_CustomerName {
            get {
                return Web.Resources.EntityStrings.CustomerSupplyInitialFeeSet_CustomerName;
            }
        }

        public string CustomerSupplyInitialFeeSet_InitialFee {
            get {
                return Web.Resources.EntityStrings.CustomerSupplyInitialFeeSet_InitialFee;
            }
        }

        public string CustomerSupplyInitialFeeSet_PartsSalesCategoryName {
            get {
                return Web.Resources.EntityStrings.CustomerSupplyInitialFeeSet_PartsSalesCategoryName;
            }
        }

        public string CustomerSupplyInitialFeeSet_SupplierCode {
            get {
                return Web.Resources.EntityStrings.CustomerSupplyInitialFeeSet_SupplierCode;
            }
        }

        public string CustomerSupplyInitialFeeSet_SupplierName {
            get {
                return Web.Resources.EntityStrings.CustomerSupplyInitialFeeSet_SupplierName;
            }
        }

        public string PartsSalesOrderDetail_MInSaleingAmount {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrderDetail_MInSaleingAmount;
            }
        }

        public string ExportCustomerInfo_CustomerCode {
            get {
                return Web.Resources.EntityStrings.ExportCustomerInfo_CustomerCode;
            }
        }

        public string ExportCustomerInfo_CustomerName {
            get {
                return Web.Resources.EntityStrings.ExportCustomerInfo_CustomerName;
            }
        }

        public string ExportCustomerInfo_PriceType {
            get {
                return Web.Resources.EntityStrings.ExportCustomerInfo_PriceType;
            }
        }

        public string ExportCustomerInfo_InvoiceCompanyName {
            get {
                return Web.Resources.EntityStrings.ExportCustomerInfo_InvoiceCompanyName;
            }
        }

        public string PartsSalesOrderDetail_ABCStrategy {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrderDetail_ABCStrategy;
            }
        }

        public string PartsSalesOrderDetail_PriceTypeName {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrderDetail_PriceTypeName;
            }
        }

        public string PartsSalesPriceChangeDetail_PriceTypeName {
            get {
                return Web.Resources.EntityStrings.PartsSalesPriceChangeDetail_PriceTypeName;
            }
        }

        public string BorrowBill_BorrowDepartmentName {
            get {
                return Web.Resources.EntityStrings.BorrowBill_BorrowDepartmentName;
            }
        }

        public string BorrowBill_BorrowName {
            get {
                return Web.Resources.EntityStrings.BorrowBill_BorrowName;
            }
        }

        public string BorrowBill_Code {
            get {
                return Web.Resources.EntityStrings.BorrowBill_Code;
            }
        }

        public string BorrowBill_ContactMethod {
            get {
                return Web.Resources.EntityStrings.BorrowBill_ContactMethod;
            }
        }

        public string BorrowBill_ExpectReturnTime {
            get {
                return Web.Resources.EntityStrings.BorrowBill_ExpectReturnTime;
            }
        }

        public string BorrowBill_OutBoundTime {
            get {
                return Web.Resources.EntityStrings.BorrowBill_OutBoundTime;
            }
        }

        public string BorrowBill_Status {
            get {
                return Web.Resources.EntityStrings.BorrowBill_Status;
            }
        }

        public string BorrowBill_Type {
            get {
                return Web.Resources.EntityStrings.BorrowBill_Type;
            }
        }

        public string BorrowBill_WarehouseCode {
            get {
                return Web.Resources.EntityStrings.BorrowBill_WarehouseCode;
            }
        }

        public string BorrowBill_WarehouseName {
            get {
                return Web.Resources.EntityStrings.BorrowBill_WarehouseName;
            }
        }

        public string BorrowBill_ReturnerName {
            get {
                return Web.Resources.EntityStrings.BorrowBill_ReturnerName;
            }
        }

        public string BorrowBill_ReturnTime {
            get {
                return Web.Resources.EntityStrings.BorrowBill_ReturnTime;
            }
        }

        public string BorrowBillDetail_BorrowQty {
            get {
                return Web.Resources.EntityStrings.BorrowBillDetail_BorrowQty;
            }
        }

        public string BorrowBillDetail_ConfirmedQty {
            get {
                return Web.Resources.EntityStrings.BorrowBillDetail_ConfirmedQty;
            }
        }

        public string BorrowBillDetail_ConfirmingQty {
            get {
                return Web.Resources.EntityStrings.BorrowBillDetail_ConfirmingQty;
            }
        }

        public string BorrowBillDetail_MeasureUnit {
            get {
                return Web.Resources.EntityStrings.BorrowBillDetail_MeasureUnit;
            }
        }

        public string BorrowBillDetail_OutboundAmount {
            get {
                return Web.Resources.EntityStrings.BorrowBillDetail_OutboundAmount;
            }
        }

        public string BorrowBillDetail_SihCode {
            get {
                return Web.Resources.EntityStrings.BorrowBillDetail_SihCode;
            }
        }

        public string BorrowBillDetail_SparePartCode {
            get {
                return Web.Resources.EntityStrings.BorrowBillDetail_SparePartCode;
            }
        }

        public string BorrowBillDetail_SparePartName {
            get {
                return Web.Resources.EntityStrings.BorrowBillDetail_SparePartName;
            }
        }

        public string BorrowBill_Purpose {
            get {
                return Web.Resources.EntityStrings.BorrowBill_Purpose;
            }
        }

        public string DealerHistory_Distance {
            get {
                return Web.Resources.EntityStrings.DealerHistory_Distance;
            }
        }

        public string DealerHistory_PostCode {
            get {
                return Web.Resources.EntityStrings.DealerHistory_PostCode;
            }
        }

        public string Dealer_SatationPeople {
            get {
                return Web.Resources.EntityStrings.Dealer_SatationPeople;
            }
        }

        public string Export_Title_No {
            get {
                return Web.Resources.EntityStrings.Export_Title_No;
            }
        }

        public string Export_Title_Yes {
            get {
                return Web.Resources.EntityStrings.Export_Title_Yes;
            }
        }

        public string Export_Validation_Dealer_Validation56 {
            get {
                return Web.Resources.EntityStrings.Export_Validation_Dealer_Validation56;
            }
        }

        public string Export_Validation_Dealer_Validation57 {
            get {
                return Web.Resources.EntityStrings.Export_Validation_Dealer_Validation57;
            }
        }

        public string PartsPurchaseOrderFinishOnTime_Item {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseOrderFinishOnTime_Item;
            }
        }

        public string PartsPurchaseOrderFinishOutTime_Item {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseOrderFinishOutTime_Item;
            }
        }

        public string PartsPurchaseOrderFinishOutTime_Item_14 {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseOrderFinishOutTime_Item_14;
            }
        }

        public string PartsPurchaseOrderFinishOutTime_Item_21 {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseOrderFinishOutTime_Item_21;
            }
        }

        public string PartsPurchaseOrderFinishOutTime_Item_22 {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseOrderFinishOutTime_Item_22;
            }
        }

        public string PartsPurchaseOrderFinishOutTime_Item_23 {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseOrderFinishOutTime_Item_23;
            }
        }

        public string PartsPurchaseOrderFinishOutTime_Item_24 {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseOrderFinishOutTime_Item_24;
            }
        }

        public string PartsPurchaseOrderFinishOutTime_Item_7 {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseOrderFinishOutTime_Item_7;
            }
        }

        public string PartsPurchaseOrderFinish_Date {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseOrderFinish_Date;
            }
        }

        public string PartsPurchaseOrderFinish_Item {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseOrderFinish_Item;
            }
        }

        public string PartsPurchaseOrderFinish_Month {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseOrderFinish_Month;
            }
        }

        public string PartsPurchaseOrderOutTime_Item {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseOrderOutTime_Item;
            }
        }

        public string PartsSalesOrderAndPartsOutboundPlanASAP_InterfaceId {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrderAndPartsOutboundPlanASAP_InterfaceId;
            }
        }

        public string PartsSalesOrderAndPartsOutboundPlanASAP_Message {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrderAndPartsOutboundPlanASAP_Message;
            }
        }

        public string PartsSalesOrderAndPartsOutboundPlanASAP_ModifyTime {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrderAndPartsOutboundPlanASAP_ModifyTime;
            }
        }

        public string PartsSalesOrderAndPartsOutboundPlanASAP_Status {
            get {
                return Web.Resources.EntityStrings.PartsSalesOrderAndPartsOutboundPlanASAP_Status;
            }
        }

        public string PartsPurchaseOrderFinishOutTime_Item_25 {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseOrderFinishOutTime_Item_25;
            }
        }

        public string OverstockPartsRecommendBill_CenterName {
            get {
                return Web.Resources.EntityStrings.OverstockPartsRecommendBill_CenterName;
            }
        }

        public string OverstockPartsRecommendBill_CreateTime {
            get {
                return Web.Resources.EntityStrings.OverstockPartsRecommendBill_CreateTime;
            }
        }

        public string OverstockPartsRecommendBill_DealerCode {
            get {
                return Web.Resources.EntityStrings.OverstockPartsRecommendBill_DealerCode;
            }
        }

        public string OverstockPartsRecommendBill_DealerName {
            get {
                return Web.Resources.EntityStrings.OverstockPartsRecommendBill_DealerName;
            }
        }

        public string OverstockPartsRecommendBill_SparePartCode {
            get {
                return Web.Resources.EntityStrings.OverstockPartsRecommendBill_SparePartCode;
            }
        }

        public string OverstockPartsRecommendBill_SparePartName {
            get {
                return Web.Resources.EntityStrings.OverstockPartsRecommendBill_SparePartName;
            }
        }

        public string PartsPurchaseOrderFinishOnTime_Items {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseOrderFinishOnTime_Items;
            }
        }

        public string PartsPurchaseOrderFinishOutTime_Items {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseOrderFinishOutTime_Items;
            }
        }

        public string PartsPurchaseOrderFinishOutTime_Item_14s {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseOrderFinishOutTime_Item_14s;
            }
        }

        public string PartsPurchaseOrderFinishOutTime_Item_21s {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseOrderFinishOutTime_Item_21s;
            }
        }

        public string PartsPurchaseOrderFinishOutTime_Item_22s {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseOrderFinishOutTime_Item_22s;
            }
        }

        public string PartsPurchaseOrderFinishOutTime_Item_23s {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseOrderFinishOutTime_Item_23s;
            }
        }

        public string PartsPurchaseOrderFinishOutTime_Item_25s {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseOrderFinishOutTime_Item_25s;
            }
        }

        public string PartsPurchaseOrderFinishOutTime_Item_7s {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseOrderFinishOutTime_Item_7s;
            }
        }

        public string PartsPurchaseOrderFinish_Items {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseOrderFinish_Items;
            }
        }

        public string PartsPurchaseOrderOutTime_Items {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseOrderOutTime_Items;
            }
        }

        public string String1 {
            get {
                return Web.Resources.EntityStrings.String1;
            }
        }

        public string PartsPurchaseOrder_IsTransSap {
            get {
                return Web.Resources.EntityStrings.PartsPurchaseOrder_IsTransSap;
            }
        }

    }
}
