﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Sunlight.Silverlight.Dcs {
    public class KeyRegularExpression {
        public const string chineseChar = @"[\u4E00-\u9FFF]+";
        public const string CELL_PHONE_NUMBER_REGEX = @"([\d])\1{7}";
        public const string EMAIL_REGEX = @"^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$";
        public const string ID_DOCUMENT_NUMBER_REGEX = @"([\d])\1{3}";
        public const string ID_DOCUMENT_NUMBER_BEFORE = @"([\d])\1{6}";
        public const string PLATE_NUMBER_REGEX = @"^[\u4E00-\u9FFF]{1}.{6}$";
        //public const string NAME_REGEX = @"^[\u4E00-\u9FFFa]$";
        public const string NAME_REGEX = @"^[\u4E00-\u9FFFa-zA-Z][\u4E00-\u9FFFa-zA-Z\\.]*[\u4E00-\u9FFFa-zA-Z]$";
        public const string NAME_REGEX2 = @"^([\u4e00-\u9fa5]+|([a-zA-Z]+\s?)+)$";
        public const string SPECIAL_CHARACTER = "[`~!@#$^&*=|{}':;'\"\\[\\]<>/? ]+";
        public const string IS_NUMBER = "^\\d+$";
        public const string POSTCODE = "^[0-9]{1}\\d{5}$";
        public const string ALL_DIGITAL_DISPLAY = "[^\\d]";
    }
}
