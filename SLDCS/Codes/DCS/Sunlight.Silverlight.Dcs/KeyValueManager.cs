﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Sunlight.Silverlight.Core.Model;

namespace Sunlight.Silverlight.Dcs {
    public sealed class KeyValueManager {
        private Dictionary<string, ObservableCollection<KeyValuePair>> kvPairs;

        public ObservableCollection<KeyValuePair> this[string name] {
            get {
                if(this.kvPairs == null || !this.kvPairs.ContainsKey(name))
                    return null;

                var result = this.kvPairs[name];
                if(result == null)
                    return this.kvPairs[name] = new ObservableCollection<KeyValuePair>();
                return result;
            }
        }

        public ObservableCollection<KeyValuePair> this[Type enumType] {
            get {
                var result = new ObservableCollection<KeyValuePair>();
                foreach(int value in Enum.GetValues(enumType))
                    result.Add(new KeyValuePair {
                        Key = value,
                        Value = Enum.GetName(enumType, value)
                    });
                return result;
            }
        }

        private void CreateAllInstances() {
            if(this.kvPairs == null)
                return;

            var names = this.kvPairs.Keys.Where(key => this.kvPairs[key] == null).ToArray();
            foreach(var name in names)
                this.kvPairs[name] = new ObservableCollection<KeyValuePair>();
        }

        public void Register(params string[] names) {
            if(names.Length == 0)
                return;

            if(this.kvPairs == null)
                this.kvPairs = new Dictionary<string, ObservableCollection<KeyValuePair>>(names.Length);
            foreach(var name in names.Where(name => !this.kvPairs.ContainsKey(name)))
                this.kvPairs.Add(name, null);
        }

        public void LoadData(Action callback = null) {
            if(this.kvPairs == null) {
                if(callback != null)
                    callback();
                return;
            }
            this.CreateAllInstances();
            DcsUtils.GetKeyValuePairs(this.kvPairs, callback);
        }
    }
}
