﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.ReportViewer.Silverlight;

namespace Sunlight.Silverlight.Dcs.Print {
    public partial class AgencyPartsShippingOrderPrintWindow {
        public AgencyPartsShippingOrderPrintWindow() {
            InitializeComponent();
            this.ReportViewer.ReportServiceUri = DcsUtils.MakeServerUri("ReportService.svc");
        }

        private AgencyPartsShippingOrder agencyPartsShippingOrder;
        public AgencyPartsShippingOrder AgencyPartsShippingOrder {
            get {
                return this.agencyPartsShippingOrder;
            }
            set {
                this.agencyPartsShippingOrder = value;
                this.OnPropertyChanged("AgencyPartsShippingOrder");
            }
        }

        private void ReportViewer1_ApplyParameters(object sender, ApplyParametersEventArgs args) {
            args.ParameterValues["agencyPartsShippingOrder"] = agencyPartsShippingOrder.Id;
            args.ParameterValues["UserName"] = BaseApp.Current.CurrentUserData.UserName;
        }
    }
}
