﻿using Sunlight.Silverlight.Dcs.Web;
using Telerik.ReportViewer.Silverlight;

namespace Sunlight.Silverlight.Dcs.Print {
    public partial class AgentsPartsOutboundBillPrintWindow {
        public AgentsPartsOutboundBillPrintWindow() {
            InitializeComponent();
            this.ReportViewer.ReportServiceUri = DcsUtils.MakeServerUri("ReportService.svc");
        }
        private AgencyPartsOutboundBill agencyPartsOutboundBill;
        public AgencyPartsOutboundBill AgencyPartsOutboundBill {
            get {
                return this.agencyPartsOutboundBill;
            }
            set {
                this.agencyPartsOutboundBill = value;
                this.OnPropertyChanged("PartsOutboundBill");
            }
        }

        private void ReportViewer1_ApplyParameters(object sender, ApplyParametersEventArgs args) {
            args.ParameterValues["parapartsOutboundBillId"] = this.agencyPartsOutboundBill.Id;
            args.ParameterValues["UserName"] = BaseApp.Current.CurrentUserData.UserName;
        }
    }
}
