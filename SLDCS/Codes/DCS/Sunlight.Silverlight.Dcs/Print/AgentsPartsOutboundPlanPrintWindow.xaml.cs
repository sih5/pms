﻿using Sunlight.Silverlight.Dcs.Web;
using Telerik.ReportViewer.Silverlight;

namespace Sunlight.Silverlight.Dcs.Print {
    public partial class AgentsPartsOutboundPlanPrintWindow {
        public AgentsPartsOutboundPlanPrintWindow() {
            InitializeComponent();
            this.ReportViewer.ReportServiceUri = DcsUtils.MakeServerUri("ReportService.svc");
        }
        private AgencyPartsOutboundPlan agencyPartsOutboundPlan;

        public AgencyPartsOutboundPlan AgencyPartsOutboundPlan {
            get {
                return this.agencyPartsOutboundPlan;
            }
            set {
                this.agencyPartsOutboundPlan = value;
                this.OnPropertyChanged("PartsOutboundPlan");
            }
        }
        private void ReportViewer1_ApplyParameters(object sender, ApplyParametersEventArgs args) {
            args.ParameterValues["paraPartsOutboundPlanId"] = agencyPartsOutboundPlan.Id;
            args.ParameterValues["UserName"] = BaseApp.Current.CurrentUserData.UserName;
        }

    }
}

