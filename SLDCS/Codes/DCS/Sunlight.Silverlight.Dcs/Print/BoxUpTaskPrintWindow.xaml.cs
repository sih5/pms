﻿using Sunlight.Silverlight.Dcs.Web;
using Telerik.ReportViewer.Silverlight;

namespace Sunlight.Silverlight.Dcs.Print
{
    public partial class BoxUpTaskPrintWindow
    {
        public BoxUpTaskPrintWindow()
        {
            InitializeComponent();
            this.ReportViewer.ReportServiceUri = DcsUtils.MakeServerUri("ReportService.svc");
        }
        private VirtualBoxUpTask virtualBoxUpTask;
        public VirtualBoxUpTask VirtualBoxUpTask
        {
            get
            {
                return this.virtualBoxUpTask;
            }
            set
            {
                this.virtualBoxUpTask = value;
                this.OnPropertyChanged("VirtualBoxUpTask");
            }
        }

        private void ReportViewer1_ApplyParameters(object sender, ApplyParametersEventArgs args)
        {
            args.ParameterValues["boxUpTaskId"] = virtualBoxUpTask.Id;
            args.ParameterValues["UserName"] = BaseApp.Current.CurrentUserData.UserName;
        }
    }
}
