﻿using Sunlight.Silverlight.Dcs.Web;
using Telerik.ReportViewer.Silverlight;

namespace Sunlight.Silverlight.Dcs.Print {
    public partial class CAReconciliationPrintWindow {
        public CAReconciliationPrintWindow() {
            InitializeComponent();
            this.reportViewer.ReportServiceUri = DcsUtils.MakeServerUri("ReportService.svc");
        }
        private CAReconciliation cAReconciliation;
        public CAReconciliation CAReconciliation {
            get {
                return this.cAReconciliation;
            }
            set {
                this.cAReconciliation = value;
                this.OnPropertyChanged("CAReconciliation");
            }
        }

        private void ReportViewer1_ApplyParameters(object sender, ApplyParametersEventArgs args) {
            args.ParameterValues["cAReconciliationId"] = cAReconciliation.Id;
        }
    }
}
