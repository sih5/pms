﻿using Sunlight.Silverlight.Dcs.Web;
using Telerik.ReportViewer.Silverlight;

namespace Sunlight.Silverlight.Dcs.Print {
    public partial class ExtendedWarrantyOrderPrintWindow {

        public ExtendedWarrantyOrderPrintWindow() {
            InitializeComponent();
            this.reportViewer.ReportServiceUri = DcsUtils.MakeServerUri("ReportService.svc");
        }

        private ExtendedWarrantyOrder extendedWarrantyOrder;
        public ExtendedWarrantyOrder ExtendedWarrantyOrder {
            get {
                return this.extendedWarrantyOrder;
            }
            set {
                this.extendedWarrantyOrder = value;
                this.OnPropertyChanged("ExtendedWarrantyOrder");
            }
        }
        private void ReportViewer1_ApplyParameters(object sender, ApplyParametersEventArgs args) {
            args.ParameterValues["extendedWarrantyOrderId"] = extendedWarrantyOrder.Id;
        }

    }
}
