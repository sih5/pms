﻿using Sunlight.Silverlight.Dcs.Web;
using Telerik.ReportViewer.Silverlight;

namespace Sunlight.Silverlight.Dcs.Print {
    public partial class InvoiceInformationPrintWindow {
        public InvoiceInformationPrintWindow() {
            InitializeComponent();
            this.reportViewer.ReportServiceUri = DcsUtils.MakeServerUri("ReportService.svc");
        }

        private PartsSalesSettlementEx partsSalesSettlementEx;
        public PartsSalesSettlementEx PartsSalesSettlementEx {
            get {
                return this.partsSalesSettlementEx;
            }
            set {
                this.partsSalesSettlementEx = value;
                this.OnPropertyChanged("PartsSalesSettlementEx");
            }
        }
        private void ReportViewer1_ApplyParameters(object sender, ApplyParametersEventArgs args) {
            args.ParameterValues["partsSalesSettlementId"] = partsSalesSettlementEx.Id;
        }
    }
}
