﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.ReportViewer.Silverlight;

namespace Sunlight.Silverlight.Dcs.Print {
    public partial class OMWMSPrintLablePrintWindow {
        public OMWMSPrintLablePrintWindow() {
            InitializeComponent();
            this.ReportViewer.ReportServiceUri = DcsUtils.MakeServerUri("ReportService.svc");
        }

        private ObservableCollection<SparePart> spareParts;

        public ObservableCollection<SparePart> SpareParts {
            get {
                return spareParts ?? (this.spareParts = new ObservableCollection<SparePart>());
            }
            set {
                this.spareParts = value;
            }
        }

        private void ReportViewer1_ApplyParameters(object sender, ApplyParametersEventArgs args) {
            foreach(var sparePart in SpareParts) {
                if(sparePart.PrintNumber.ToString() == string.Empty) {
                    sparePart.PrintNumber = 0;
                }
                if(sparePart.PrintNums.ToString() == string.Empty) {
                    sparePart.PrintNums = 1;
                }
            }
            var partsQuantity = SpareParts.Select(detail => detail.Id + "," + detail.PrintNumber + "," + detail.PrintNums).ToList();
            var details = new object[partsQuantity.Count()];
            for(var i = 0;i < partsQuantity.Count();i++) {
                details[i] = partsQuantity[i];
            }
            args.ParameterValues["PararPartsTags"] = details;
        }
    }
}
