﻿using Telerik.ReportViewer.Silverlight;

namespace Sunlight.Silverlight.Dcs.Print {
    public partial class PartsBranchPrintLabel4PrintWindow {
        public PartsBranchPrintLabel4PrintWindow() {
            InitializeComponent();
            this.ReportViewer.ReportServiceUri = DcsUtils.MakeServerUri("ReportService.svc");
        }
        private string partsBranch;
        public string PartsBranch {
            get {
                return this.partsBranch;
            }
            set {
                this.partsBranch = value;
                this.OnPropertyChanged("PartsBranch");
            }
        }

        private void ReportViewer1_ApplyParameters(object sender, ApplyParametersEventArgs args) {
            args.ParameterValues["partDetails"] = partsBranch;
        }

    }
}
