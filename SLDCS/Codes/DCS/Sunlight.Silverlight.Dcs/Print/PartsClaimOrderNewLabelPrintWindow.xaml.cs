﻿
using Telerik.ReportViewer.Silverlight;


namespace Sunlight.Silverlight.Dcs.Print {
    public partial class PartsClaimOrderNewLabelPrintWindow {
        public PartsClaimOrderNewLabelPrintWindow() {
            InitializeComponent();
            this.ReportViewer.ReportServiceUri = DcsUtils.MakeServerUri("ReportService.svc");
        }

        private string partsClaimOrderNewId;

        public string PartsClaimOrderNewId {
            get {
                return this.partsClaimOrderNewId;
            }
            set {
                this.partsClaimOrderNewId = value;
                this.OnPropertyChanged("PartsClaimOrderNewId");
            }
        }

        private void ReportViewer1_ApplyParameters(object sender, ApplyParametersEventArgs args) {
            args.ParameterValues["partsClaimOrderNewId"] = PartsClaimOrderNewId;
            args.ParameterValues["UserName"] = BaseApp.Current.CurrentUserData.UserName;
        }
    }
}
