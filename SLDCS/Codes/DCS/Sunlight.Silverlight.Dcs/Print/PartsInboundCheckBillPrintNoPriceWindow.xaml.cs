﻿using Sunlight.Silverlight.Dcs.Web;
using Telerik.ReportViewer.Silverlight;

namespace Sunlight.Silverlight.Dcs.Print {
    public partial class PartsInboundCheckBillPrintNoPriceWindow {
        public PartsInboundCheckBillPrintNoPriceWindow() {
            InitializeComponent();
            this.ReportViewer.ReportServiceUri = DcsUtils.MakeServerUri("ReportService.svc");

        }
        private PartsInboundCheckBill partsInboundCheckBillNoPrice;
        public PartsInboundCheckBill PartsInboundCheckBillNoPrice {
            get {
                return this.partsInboundCheckBillNoPrice;
            }
            set {
                this.partsInboundCheckBillNoPrice = value;
                this.OnPropertyChanged("PartsInboundCheckBill");
            }
        }

        private void ReportViewer1_ApplyParameters(object sender, ApplyParametersEventArgs args) {
            args.ParameterValues["paraPartsInboundCheckBillId"] = partsInboundCheckBillNoPrice.Id;
            args.ParameterValues["UserName"] = BaseApp.Current.CurrentUserData.UserName;
        }
    }
}
