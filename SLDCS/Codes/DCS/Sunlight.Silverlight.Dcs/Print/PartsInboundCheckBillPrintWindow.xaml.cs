﻿using Sunlight.Silverlight.Dcs.Web;
using Telerik.ReportViewer.Silverlight;

namespace Sunlight.Silverlight.Dcs.Print {
    public partial class PartsInboundCheckBillPrintWindow {
        public PartsInboundCheckBillPrintWindow() {
            InitializeComponent();
            this.ReportViewer.ReportServiceUri = DcsUtils.MakeServerUri("ReportService.svc");

        }
        private int partsInboundCheckBillId;
        public int PartsInboundCheckBillId {
            get {
                return this.partsInboundCheckBillId;
            }
            set {
                this.partsInboundCheckBillId = value;
                this.OnPropertyChanged("PartsInboundCheckBillId");
            }
        }

        private void ReportViewer1_ApplyParameters(object sender, ApplyParametersEventArgs args) {
            args.ParameterValues["paraPartsInboundCheckBillId"] = PartsInboundCheckBillId;
            args.ParameterValues["UserName"] = BaseApp.Current.CurrentUserData.UserName;
        }
    }
}
