﻿using Sunlight.Silverlight.Dcs.Web;
using Telerik.ReportViewer.Silverlight;

namespace Sunlight.Silverlight.Dcs.Print {
    public partial class PartsInboundPlanNoPriceMPrintWindow {
        public PartsInboundPlanNoPriceMPrintWindow() {
            InitializeComponent();
            this.ReportViewer.ReportServiceUri = DcsUtils.MakeServerUri("ReportService.svc");
        }

        private PartsInboundPlan partsInboundPlan;

        public PartsInboundPlan PartsInboundPlan {
            get {
                return this.partsInboundPlan;
            }
            set {
                this.partsInboundPlan = value;
                this.OnPropertyChanged("PartsInboundPlan");
            }
        }

        private void ReportViewer1_ApplyParameters(object sender, ApplyParametersEventArgs args) {
            args.ParameterValues["partsInboundPlanId"] = partsInboundPlan.Id;
            args.ParameterValues["UserName"] = BaseApp.Current.CurrentUserData.UserName;
        }
    }
}
