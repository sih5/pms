﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.ReportViewer.Silverlight;

namespace Sunlight.Silverlight.Dcs.Print {
    public partial class PartsInboundPlanOrderPrintWmsOMLabelPrintWindow {
        public PartsInboundPlanOrderPrintWmsOMLabelPrintWindow() {
            InitializeComponent();
            this.ReportViewer.ReportServiceUri = DcsUtils.MakeServerUri("ReportService.svc");
        }

        private ObservableCollection<PartsInboundPlanDetail> partsInboundPlanDetails;

        public ObservableCollection<PartsInboundPlanDetail> PartsInboundPlanDetails {
            get {
                return partsInboundPlanDetails ?? (this.partsInboundPlanDetails = new ObservableCollection<PartsInboundPlanDetail>());
            }
            set {
                this.partsInboundPlanDetails = value;
            }
        }

        private void ReportViewer1_ApplyParameters(object sender, ApplyParametersEventArgs args) {
            foreach(var partsInboundPlanDetail in PartsInboundPlanDetails) {
                if(partsInboundPlanDetail.PrintNumber.ToString() == string.Empty) {
                    partsInboundPlanDetail.PrintNumber = 0;
                }
            }
            var partsQuantity = PartsInboundPlanDetails.Select(detail => detail.Id + "," + detail.PrintNumber).ToList();
            var details = new object[partsQuantity.Count()];
            for(var i = 0; i < partsQuantity.Count(); i++) {
                details[i] = partsQuantity[i];
            }
            args.ParameterValues["PararPartsTags"] = details;
        }
    }
}

