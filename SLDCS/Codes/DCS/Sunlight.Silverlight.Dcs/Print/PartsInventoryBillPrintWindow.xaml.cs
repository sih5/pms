﻿using Sunlight.Silverlight.Dcs.Web;
using Telerik.ReportViewer.Silverlight;

namespace Sunlight.Silverlight.Dcs.Print {
    public partial class PartsInventoryBillPrintWindow {
        public PartsInventoryBillPrintWindow() {
            InitializeComponent();
            this.ReportViewer.ReportServiceUri = DcsUtils.MakeServerUri("ReportService.svc");

        }
        private PartsInventoryBill partsInventoryBill;

        public PartsInventoryBill PartsInventoryBill {
            get {
                return this.partsInventoryBill;
            }
            set {
                this.partsInventoryBill = value;
                this.OnPropertyChanged("PartsInventoryBill");
            }
        }

        private void ReportViewer1_ApplyParameters(object sender, ApplyParametersEventArgs args) {
            args.ParameterValues["partsInventoryBillId"] = PartsInventoryBill.Id;
            args.ParameterValues["UserName"] = BaseApp.Current.CurrentUserData.UserName;
        }
    }
}
