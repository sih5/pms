﻿using Sunlight.Silverlight.Dcs.Web;
using Telerik.ReportViewer.Silverlight;

namespace Sunlight.Silverlight.Dcs.Print {
    public partial class PartsOutboundBillForGCPrintWindow {
        public PartsOutboundBillForGCPrintWindow() {
            InitializeComponent();
            this.reportViewer.ReportServiceUri = DcsUtils.MakeServerUri("ReportService.svc");
        }
        private PartsOutboundBillWithOtherInfo partsOutboundBill;
        public PartsOutboundBillWithOtherInfo PartsOutboundBill {
            get {
                return this.partsOutboundBill;
            }
            set {
                this.partsOutboundBill = value;
                this.OnPropertyChanged("PartsOutboundBill");
            }
        }

        private void ReportViewer1_ApplyParameters(object sender, ApplyParametersEventArgs args) {
            args.ParameterValues["partsOutboundBillId"] = partsOutboundBill.PartsOutboundBillId;
            args.ParameterValues["UserName"] = BaseApp.Current.CurrentUserData.UserName;
        }
    }
}
