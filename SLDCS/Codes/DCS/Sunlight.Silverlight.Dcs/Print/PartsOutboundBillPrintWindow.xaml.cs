﻿using Sunlight.Silverlight.Dcs.Web;
using Telerik.ReportViewer.Silverlight;

namespace Sunlight.Silverlight.Dcs.Print {
    public partial class PartsOutboundBillPrintWindow {
        public PartsOutboundBillPrintWindow() {
            InitializeComponent();
            this.ReportViewer.ReportServiceUri = DcsUtils.MakeServerUri("ReportService.svc");
        }
        private PartsOutboundBill partsOutboundBill;
        public PartsOutboundBill PartsOutboundBill {
            get {
                return this.partsOutboundBill;
            }
            set {
                this.partsOutboundBill = value;
                this.OnPropertyChanged("PartsOutboundBill");
            }
        }

        private void ReportViewer1_ApplyParameters(object sender, ApplyParametersEventArgs args) {
            args.ParameterValues["parapartsOutboundBillId"] = partsOutboundBill.Id;
            args.ParameterValues["UserName"] = BaseApp.Current.CurrentUserData.UserName;
        }
    }
}
