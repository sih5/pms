﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.ReportViewer.Silverlight;

namespace Sunlight.Silverlight.Dcs.Print {
    public partial class PartsOutboundPlanNotPartitionPrintWindow {
        public PartsOutboundPlanNotPartitionPrintWindow() {
            InitializeComponent();
            this.ReportViewer.ReportServiceUri = DcsUtils.MakeServerUri("ReportService.svc");
        }

        private PartsOutboundPlan partsInboundPlan;

        public PartsOutboundPlan PartsOutboundPlan {
            get {
                return this.partsInboundPlan;
            }
            set {
                this.partsInboundPlan = value;
                this.OnPropertyChanged("PartsOutboundPlan");
            }
        }

        private void ReportViewer1_ApplyParameters(object sender, ApplyParametersEventArgs args) {
            args.ParameterValues["partsOutboundPlanId"] = PartsOutboundPlan.Id;
            args.ParameterValues["UserName"] = BaseApp.Current.CurrentUserData.UserName;
        }
    }
}
