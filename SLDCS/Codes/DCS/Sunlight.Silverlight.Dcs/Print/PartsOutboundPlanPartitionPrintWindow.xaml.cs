﻿
using Sunlight.Silverlight.Dcs.Web;
using Telerik.ReportViewer.Silverlight;

namespace Sunlight.Silverlight.Dcs.Print {
    public partial class PartsOutboundPlanPartitionPrintWindow {
        public PartsOutboundPlanPartitionPrintWindow() {
            InitializeComponent();
            this.ReportViewer.ReportServiceUri = DcsUtils.MakeServerUri("ReportService.svc");
        }

        private PartsOutboundPlan partsInboundPlan;

        public PartsOutboundPlan PartsOutboundPlan {
            get {
                return this.partsInboundPlan;
            }
            set {
                this.partsInboundPlan = value;
                this.OnPropertyChanged("PartsOutboundPlan");
            }
        }

        private void ReportViewer1_ApplyParameters(object sender, ApplyParametersEventArgs args) {
            args.ParameterValues["partsOutboundPlanId"] = PartsOutboundPlan.Id;
            args.ParameterValues["UserName"] = BaseApp.Current.CurrentUserData.UserName;
        }
    }
}
