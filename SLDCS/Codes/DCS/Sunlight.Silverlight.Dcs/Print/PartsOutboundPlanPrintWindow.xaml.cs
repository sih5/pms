﻿using Sunlight.Silverlight.Dcs.Web;
using Telerik.ReportViewer.Silverlight;

namespace Sunlight.Silverlight.Dcs.Print {
    public partial class PartsOutboundPlanPrintWindow {
        public PartsOutboundPlanPrintWindow() {
            InitializeComponent();
            this.ReportViewer.ReportServiceUri = DcsUtils.MakeServerUri("ReportService.svc");
        }
        private PartsOutboundPlan partsOutboundPlan;

        public PartsOutboundPlan PartsOutboundPlan {
            get {
                return this.partsOutboundPlan;
            }
            set {
                this.partsOutboundPlan = value;
                this.OnPropertyChanged("PartsOutboundPlan");
            }
        }
        private void ReportViewer1_ApplyParameters(object sender, ApplyParametersEventArgs args) {
            args.ParameterValues["paraPartsOutboundPlanId"] = partsOutboundPlan.Id;
            args.ParameterValues["UserName"] = BaseApp.Current.CurrentUserData.UserName;
        }

    }
}

