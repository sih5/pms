﻿using Sunlight.Silverlight.Dcs.Web;
using Telerik.ReportViewer.Silverlight;

namespace Sunlight.Silverlight.Dcs.Print {
    public partial class PartsOuterPurchaseChangePrintWindow {
        public PartsOuterPurchaseChangePrintWindow() {
            InitializeComponent();
            this.ReportViewer.ReportServiceUri = DcsUtils.MakeServerUri("ReportService.svc");
        }

        private PartsOuterPurchaseChange partsOuterPurchaseChange;

        public PartsOuterPurchaseChange PartsOuterPurchaseChange {
            get {
                return this.partsOuterPurchaseChange;
            }
            set {
                this.partsOuterPurchaseChange = value;
                this.OnPropertyChanged("PartsOuterPurchaseChange");
            }
        }

        private void ReportViewer1_ApplyParameters(object sender, ApplyParametersEventArgs args) {
            args.ParameterValues["partsOuterPurchaseChangeId"] = PartsOuterPurchaseChange.Id;
            args.ParameterValues["UserName"] = BaseApp.Current.CurrentUserData.UserName;
        }
    }
}
