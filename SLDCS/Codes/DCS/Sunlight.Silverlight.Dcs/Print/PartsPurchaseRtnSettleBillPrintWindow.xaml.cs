﻿using Sunlight.Silverlight.Dcs.Web;
using Telerik.ReportViewer.Silverlight;

namespace Sunlight.Silverlight.Dcs.Print {
    public partial class PartsPurchaseRtnSettleBillPrintWindow {
        public PartsPurchaseRtnSettleBillPrintWindow() {
            InitializeComponent();
            this.ReportViewer.ReportServiceUri = DcsUtils.MakeServerUri("ReportService.svc");
        }
        private VirtualPartsPurchaseRtnSettleBillWithBusinessCode partsPurchaseRtnSettleBill;
        public VirtualPartsPurchaseRtnSettleBillWithBusinessCode PartsPurchaseRtnSettleBill {
            get {
                return this.partsPurchaseRtnSettleBill;
            }
            set {
                this.partsPurchaseRtnSettleBill = value;
                this.OnPropertyChanged("PartsPurchaseRtnSettleBill");
            }
        }

        private void ReportViewer1_ApplyParameters(object sender, ApplyParametersEventArgs args) {
            args.ParameterValues["partsPurchaseRtnSettleBillId"] = partsPurchaseRtnSettleBill.Id;
            args.ParameterValues["PartsSalesCategoryName"] = partsPurchaseRtnSettleBill.PartsSalesCategoryName;
            args.ParameterValues["UserName"] = BaseApp.Current.CurrentUserData.UserName;
        }
    }
}
