﻿using Sunlight.Silverlight.Dcs.Web;
using Telerik.ReportViewer.Silverlight;

namespace Sunlight.Silverlight.Dcs.Print {
    public partial class PartsPurchaseSettleBillPrintForGCWindow {

        public PartsPurchaseSettleBillPrintForGCWindow() {
            InitializeComponent();
            this.ReportViewer.ReportServiceUri = DcsUtils.MakeServerUri("ReportService.svc");
        }
        private PartsPurchaseSettleBill partsPurchaseSettleBill;

        public PartsPurchaseSettleBill PartsPurchaseSettleBill {
            get {
                return this.partsPurchaseSettleBill;
            }
            set {
                this.partsPurchaseSettleBill = value;
                this.OnPropertyChanged("PartsPurchaseSettleBill");
            }
        }

        private void ReportViewer1_ApplyParameters(object sender, ApplyParametersEventArgs args) {
            args.ParameterValues["partsPurchaseSettleBillId"] = partsPurchaseSettleBill.Id;
            args.ParameterValues["UserName"] = BaseApp.Current.CurrentUserData.UserName;
        }
    }
}

