﻿using Sunlight.Silverlight.Dcs.Web;
using Telerik.ReportViewer.Silverlight;

namespace Sunlight.Silverlight.Dcs.Print {
    public partial class PartsRequisitionSettleBillPrintWindow {
        public PartsRequisitionSettleBillPrintWindow() {
            InitializeComponent();
            this.ReportViewer.ReportServiceUri = DcsUtils.MakeServerUri("ReportService.svc");
        }
        private PartsRequisitionSettleBillExport partsRequisitionSettleBill;
        public PartsRequisitionSettleBillExport PartsRequisitionSettleBill {
            get {
                return this.partsRequisitionSettleBill;
            }
            set {
                this.partsRequisitionSettleBill = value;
                this.OnPropertyChanged("PartsRequisitionSettleBill");
            }
        }

        private void ReportViewer1_ApplyParameters(object sender, ApplyParametersEventArgs args) {
            args.ParameterValues["paraPartsRequisitionSettleBillId"] = PartsRequisitionSettleBill.Id;
        }
    }
}
