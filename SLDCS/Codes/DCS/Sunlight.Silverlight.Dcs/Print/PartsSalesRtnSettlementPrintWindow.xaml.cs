﻿using Sunlight.Silverlight.Dcs.Web;
using Telerik.ReportViewer.Silverlight;

namespace Sunlight.Silverlight.Dcs.Print {
    public partial class PartsSalesRtnSettlementPrintWindow {
        public PartsSalesRtnSettlementPrintWindow() {
            InitializeComponent();
            this.ReportViewer.ReportServiceUri = DcsUtils.MakeServerUri("ReportService.svc");
        }
        //private PartsSalesRtnSettlement partsSalesRtnSettlement;

        //public PartsSalesRtnSettlement PartsSalesRtnSettlement {
        //    get {
        //        return this.partsSalesRtnSettlement;
        //    }
        //    set {
        //        this.partsSalesRtnSettlement = value;
        //        this.OnPropertyChanged("PartsSalesRtnSettlement");
        //    }
        //}
        private PartsSalesRtnSettlementEx partsSalesRtnSettlementEx;
        public PartsSalesRtnSettlementEx PartsSalesRtnSettlementEx {
            get {
                return this.partsSalesRtnSettlementEx;
            }
            set {
                this.partsSalesRtnSettlementEx = value;
                this.OnPropertyChanged("PartsSalesRtnSettlementEx");
            }
        }

        private void ReportViewer1_ApplyParameters(object sender, ApplyParametersEventArgs args) {
            //args.ParameterValues["paraPartsSalesRtnSettlementId"] = partsSalesRtnSettlement.Id;
            args.ParameterValues["paraPartsSalesRtnSettlementId"] = partsSalesRtnSettlementEx.Id;
            args.ParameterValues["UserName"] = BaseApp.Current.CurrentUserData.UserName;
        }
    }
}

