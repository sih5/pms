﻿using Sunlight.Silverlight.Dcs.Web;
using Telerik.ReportViewer.Silverlight;

namespace Sunlight.Silverlight.Dcs.Print {
    public partial class PartsSalesSettlementPrintWindow {
        public PartsSalesSettlementPrintWindow() {
            InitializeComponent();
            this.ReportViewer.ReportServiceUri = DcsUtils.MakeServerUri("ReportService.svc");
        }

        private PartsSalesSettlementEx partsSalesSettlement;
        public PartsSalesSettlementEx PartsSalesSettlement {
            get {
                return this.partsSalesSettlement;
            }
            set {
                this.partsSalesSettlement = value;
                this.OnPropertyChanged("PartsSalesSettlement");
            }
        }

        private void ReportViewer1_ApplyParameters(object sender, ApplyParametersEventArgs args) {
            args.ParameterValues["partsSalesSettlementId"] = partsSalesSettlement.Id;
            args.ParameterValues["UserName"] = BaseApp.Current.CurrentUserData.UserName;
        }
    }
}
