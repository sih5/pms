﻿using Sunlight.Silverlight.Dcs.Web;
using Telerik.ReportViewer.Silverlight;

namespace Sunlight.Silverlight.Dcs.Print {
    public partial class PartsShiftOrderPrintWindow {
        public PartsShiftOrderPrintWindow() {
            InitializeComponent();
            this.ReportViewer.ReportServiceUri = DcsUtils.MakeServerUri("ReportService.svc");
        }

        private PartsShiftOrder partsShiftOrder;
        public PartsShiftOrder PartsShiftOrder {
            get {
                return this.partsShiftOrder;
            }
            set {
                this.partsShiftOrder = value;
                this.OnPropertyChanged("PartsShiftOrder");
            }
        }

        private void ReportViewer1_ApplyParameters(object sender, ApplyParametersEventArgs args) {
            args.ParameterValues["partsShiftOrderId"] = partsShiftOrder.Id;
            args.ParameterValues["UserName"] = BaseApp.Current.CurrentUserData.UserName;
        }
    }
}
