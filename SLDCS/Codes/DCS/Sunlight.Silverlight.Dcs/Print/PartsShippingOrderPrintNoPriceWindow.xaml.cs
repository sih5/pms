﻿using Sunlight.Silverlight.Dcs.Web;
using Telerik.ReportViewer.Silverlight;

namespace Sunlight.Silverlight.Dcs.Print {
    public partial class PartsShippingOrderPrintNoPriceWindow {
        public PartsShippingOrderPrintNoPriceWindow() {
            InitializeComponent();
            this.ReportViewer.ReportServiceUri = DcsUtils.MakeServerUri("ReportService.svc");
        }

        private PartsShippingOrder partsShippingOrderNoPrice;
        public PartsShippingOrder PartsShippingOrderNoPrice {
            get {
                return this.partsShippingOrderNoPrice;
            }
            set {
                this.partsShippingOrderNoPrice = value;
                this.OnPropertyChanged("PartsShippingOrder");
            }
        }

        private void ReportViewer1_ApplyParameters(object sender, ApplyParametersEventArgs args) {
            args.ParameterValues["partsShippingOrderId"] = partsShippingOrderNoPrice.Id;
            args.ParameterValues["UserName"] = BaseApp.Current.CurrentUserData.UserName;
        }
    }
}
