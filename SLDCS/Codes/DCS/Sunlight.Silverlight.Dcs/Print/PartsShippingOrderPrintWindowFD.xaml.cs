﻿using Sunlight.Silverlight.Dcs.Web;
using Telerik.ReportViewer.Silverlight;

namespace Sunlight.Silverlight.Dcs.Print {
    public partial class PartsShippingOrderPrintWindowFD {
        public PartsShippingOrderPrintWindowFD() {
            InitializeComponent();
            this.ReportViewer.ReportServiceUri = DcsUtils.MakeServerUri("ReportService.svc");
        }

        private PartsShippingOrder partsShippingOrder;
        public PartsShippingOrder PartsShippingOrder {
            get {
                return this.partsShippingOrder;
            }
            set {
                this.partsShippingOrder = value;
                this.OnPropertyChanged("PartsShippingOrder");
            }
        }

        private void ReportViewer1_ApplyParameters(object sender, ApplyParametersEventArgs args) {
            args.ParameterValues["partsShippingOrderId"] = partsShippingOrder.Id;
            args.ParameterValues["UserName"] = BaseApp.Current.CurrentUserData.UserName;
        }
    }
}
