﻿using Sunlight.Silverlight.Dcs.Web;
using Telerik.ReportViewer.Silverlight;

namespace Sunlight.Silverlight.Dcs.Print
{
    public partial class PickingTaskPrintWindow 
    {
        public PickingTaskPrintWindow()
        {
            InitializeComponent();
            this.ReportViewer.ReportServiceUri = DcsUtils.MakeServerUri("ReportService.svc");
        }
        private PickingTaskQuery pickingTaskQuery;
        public PickingTaskQuery PickingTaskQuery
        {
            get
            {
                return this.pickingTaskQuery;
            }
            set
            {
                this.pickingTaskQuery = value;
                this.OnPropertyChanged("PickingTaskQuery");
            }
        }

        private void ReportViewer1_ApplyParameters(object sender, ApplyParametersEventArgs args)
        {
            args.ParameterValues["pickingTaskId"] = pickingTaskQuery.Id;
            args.ParameterValues["UserName"] = BaseApp.Current.CurrentUserData.UserName;
        }
    }
}
