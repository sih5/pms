﻿using Telerik.ReportViewer.Silverlight;

namespace Sunlight.Silverlight.Dcs.Print {
    public partial class RepairClaimApplicationPrintWindow {
        public RepairClaimApplicationPrintWindow() {
            InitializeComponent();
            this.ReportViewer.ReportServiceUri = DcsUtils.MakeServerUri("ReportService.svc");
        }

        private int repairClaimApplicationId;
        public int RepairClaimApplicationId {
            get {
                return this.repairClaimApplicationId;
            }
            set {
                this.repairClaimApplicationId = value;
                this.OnPropertyChanged("RepairClaimApplicationId");
            }
        }

        private void ReportViewer1_ApplyParameters(object sender, ApplyParametersEventArgs args) {
            args.ParameterValues["repairClaimApplicationId"] = RepairClaimApplicationId;
        }
    }
}
