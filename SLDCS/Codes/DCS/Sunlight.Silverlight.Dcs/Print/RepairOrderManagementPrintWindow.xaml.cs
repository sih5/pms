﻿using Sunlight.Silverlight.Dcs.Web;
using Telerik.ReportViewer.Silverlight;
namespace Sunlight.Silverlight.Dcs.Print {
    public partial class RepairOrderManagementPrintWindow {
        public RepairOrderManagementPrintWindow() {
            InitializeComponent();
            this.ReportViewer.ReportServiceUri = DcsUtils.MakeServerUri("ReportService.svc");
        }
        private int repairOrderId;
        public int RepairOrderId {
            get {
                return this.repairOrderId;
            }
            set {
                this.repairOrderId = value;
                this.OnPropertyChanged("RepairOrderId");
            }
        }
        private void ReportViewer1_ApplyParameters(object sender, ApplyParametersEventArgs args) {
            args.ParameterValues["paraRepairOrderId"] = RepairOrderId;
        }
    }
}

