﻿
namespace Sunlight.Silverlight.Dcs.Print {
    public partial class RepairOrderPrintNewPrintModeWindow {
        public RepairOrderPrintNewPrintModeWindow() {
            InitializeComponent();
            this.ReportViewer.ReportServiceUri = DcsUtils.MakeServerUri("ReportService.svc");
        }
    }
}
