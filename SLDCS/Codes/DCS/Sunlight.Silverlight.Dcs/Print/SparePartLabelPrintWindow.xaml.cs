﻿using System.Collections.ObjectModel;
using System.Linq;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.ReportViewer.Silverlight;

namespace Sunlight.Silverlight.Dcs.Print {
    public partial class SparePartLabelPrintWindow {
        public SparePartLabelPrintWindow() {
            InitializeComponent();
            this.ReportViewer.ReportServiceUri = DcsUtils.MakeServerUri("ReportService.svc");
        }

        private ObservableCollection<PartsInboundCheckBillDetail> partsInboundCheckBillDetail;
        public ObservableCollection<PartsInboundCheckBillDetail> PartsInboundCheckBillDetail {
            get {
                return partsInboundCheckBillDetail ?? (this.partsInboundCheckBillDetail = new ObservableCollection<PartsInboundCheckBillDetail>());
            }
            set {
                this.partsInboundCheckBillDetail = value;
            }
        }


        private void ReportViewer1_ApplyParameters(object sender, ApplyParametersEventArgs args) {
            foreach(var partsInboundCheckBillDetail in PartsInboundCheckBillDetail) {
                if(partsInboundCheckBillDetail.PrintNumber.ToString() == string.Empty) {
                    partsInboundCheckBillDetail.PrintNumber = 0;
                }
            }
            var partsQuantity = PartsInboundCheckBillDetail.Select(detail => detail.Id + "," + detail.PrintNumber).ToList();
            var details = new object[partsQuantity.Count()];
            for(var i = 0; i < partsQuantity.Count(); i++) {
                details[i] = partsQuantity[i];
            }
            args.ParameterValues["checkDetails"] = details;
        }
    }
}