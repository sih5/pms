﻿using Telerik.ReportViewer.Silverlight;


namespace Sunlight.Silverlight.Dcs.Print {
    public partial class SsUsedPartsStorageForLablePrintWindow {
        public SsUsedPartsStorageForLablePrintWindow() {
            InitializeComponent();
            this.ReportViewer.ReportServiceUri = DcsUtils.MakeServerUri("ReportService.svc");
        }
        private string ssUsedPartsStorage;
        public string SsUsedPartsStorage {
            get {
                return this.ssUsedPartsStorage;
            }
            set {
                this.ssUsedPartsStorage = value;
                this.OnPropertyChanged("SsUsedPartsStorage");
            }
        }
        private void ReportViewer1_ApplyParameters(object sender, ApplyParametersEventArgs args) {
            args.ParameterValues["paraSsUsedPartsStorageId"] = ssUsedPartsStorage;
        }
    }
}

