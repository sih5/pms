﻿using Sunlight.Silverlight.Dcs.Web;
using Telerik.ReportViewer.Silverlight;

namespace Sunlight.Silverlight.Dcs.Print {
    public partial class SupplierPreApprovedLoanDetailPrintWindow {
        public SupplierPreApprovedLoanDetailPrintWindow() {
            InitializeComponent();
            this.ReportViewer.ReportServiceUri = DcsUtils.MakeServerUri("ReportService.svc");
        }
        private SupplierPreApprovedLoan supplierPreApprovedLoan;

        public SupplierPreApprovedLoan SupplierPreApprovedLoan {
            get {
                return this.supplierPreApprovedLoan;
            }
            set {
                this.supplierPreApprovedLoan = value;
                this.OnPropertyChanged("SupplierPreApprovedLoan");
            }
        }
        private void ReportViewer1_ApplyParameters(object sender, ApplyParametersEventArgs args) {
            args.ParameterValues["supplierPreApprovedLoanId"] = SupplierPreApprovedLoan.Id;
            args.ParameterValues["UserName"] = BaseApp.Current.CurrentUserData.UserName;
        }

    }
}