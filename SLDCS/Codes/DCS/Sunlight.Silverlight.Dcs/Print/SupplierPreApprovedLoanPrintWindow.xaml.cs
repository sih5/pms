﻿using Sunlight.Silverlight.Dcs.Web;
using Telerik.ReportViewer.Silverlight;

namespace Sunlight.Silverlight.Dcs.Print {
    public partial class SupplierPreApprovedLoanPrintWindow {
        public SupplierPreApprovedLoanPrintWindow() {
            InitializeComponent();
            this.ReportViewer.ReportServiceUri = DcsUtils.MakeServerUri("ReportService.svc");
        }
        private SupplierPreApprovedLoan supplierPreApprovedLoan;
        private string printType;

        public string PrintType {
            get {
                return this.printType;
            }
            set {
                this.printType = value;
                this.OnPropertyChanged("PrintType");
            }
        }

        public SupplierPreApprovedLoan SupplierPreApprovedLoan {
            get {
                return this.supplierPreApprovedLoan;
            }
            set {
                this.supplierPreApprovedLoan = value;
                this.OnPropertyChanged("SupplierPreApprovedLoan");
            }
        }
        private void ReportViewer1_ApplyParameters(object sender, ApplyParametersEventArgs args) {
            args.ParameterValues["supplierPreApprovedLoanId"] = supplierPreApprovedLoan.Id;
            args.ParameterValues["printType"] = printType;
        }
    }
}

