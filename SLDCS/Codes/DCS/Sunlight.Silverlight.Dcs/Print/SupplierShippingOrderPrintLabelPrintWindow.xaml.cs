﻿using System.Collections.ObjectModel;
using System.Linq;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.ReportViewer.Silverlight;

namespace Sunlight.Silverlight.Dcs.Print {
    public partial class SupplierShippingOrderPrintLabelPrintWindow {
        public SupplierShippingOrderPrintLabelPrintWindow() {
            InitializeComponent();
            this.ReportViewer.ReportServiceUri = DcsUtils.MakeServerUri("ReportService.svc");
        }

        private ObservableCollection<SupplierShippingDetail> supplierShippingDetails;
        public ObservableCollection<SupplierShippingDetail> SupplierShippingDetails {
            get {
                return supplierShippingDetails ?? (this.supplierShippingDetails = new ObservableCollection<SupplierShippingDetail>());
            }
            set {
                this.supplierShippingDetails = value;
            }
        }

        private void ReportViewer1_ApplyParameters(object sender, ApplyParametersEventArgs args) {
            foreach(var supplierShippingDetail in SupplierShippingDetails) {
                if(supplierShippingDetail.PrintNumber.ToString() == string.Empty) {
                    supplierShippingDetail.PrintNumber = 0;
                }
            }
            var partsQuantity = SupplierShippingDetails.Select(detail => detail.Id + "," + detail.PrintNumber).ToList();
            var details = new object[partsQuantity.Count()];
            for(var i = 0; i < partsQuantity.Count(); i++) {
                details[i] = partsQuantity[i];
            }
            args.ParameterValues["PararPartsTags"] = details;
        }
    }
}

