﻿using Sunlight.Silverlight.Dcs.Web;
using Telerik.ReportViewer.Silverlight;

namespace Sunlight.Silverlight.Dcs.Print {
    public partial class SupplierShippingOrderPrintNoPriceWindow {
        public SupplierShippingOrderPrintNoPriceWindow() {
            InitializeComponent();
            this.ReportViewer.ReportServiceUri = DcsUtils.MakeServerUri("ReportService.svc");
        }

        private SupplierShippingOrder supplierShippingOrderNoPrice;

        public SupplierShippingOrder SupplierShippingOrderNoPrice {
            get {
                return this.supplierShippingOrderNoPrice;
            }
            set {
                this.supplierShippingOrderNoPrice = value;
                this.OnPropertyChanged("SupplierShippingOrder");
            }
        }
        private void ReportViewer1_ApplyParameters(object sender, ApplyParametersEventArgs args) {
            args.ParameterValues["paraSupplierShippingOrderId"] = supplierShippingOrderNoPrice.Id;
            args.ParameterValues["UserName"] = BaseApp.Current.CurrentUserData.UserName;
        }
    }
}

