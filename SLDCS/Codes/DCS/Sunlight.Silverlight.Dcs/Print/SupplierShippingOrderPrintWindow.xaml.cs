﻿using Sunlight.Silverlight.Dcs.Web;
using Telerik.ReportViewer.Silverlight;

namespace Sunlight.Silverlight.Dcs.Print {
    public partial class SupplierShippingOrderPrintWindow {
        public SupplierShippingOrderPrintWindow() {
            InitializeComponent();
            this.ReportViewer.ReportServiceUri = DcsUtils.MakeServerUri("ReportService.svc");
        }

        private SupplierShippingOrder supplierShippingOrder;

        public SupplierShippingOrder SupplierShippingOrder {
            get {
                return this.supplierShippingOrder;
            }
            set {
                this.supplierShippingOrder = value;
                this.OnPropertyChanged("SupplierShippingOrder");
            }
        }
        private void ReportViewer1_ApplyParameters(object sender, ApplyParametersEventArgs args) {
            args.ParameterValues["paraSupplierShippingOrderId"] = supplierShippingOrder.Id;
            args.ParameterValues["UserName"] = BaseApp.Current.CurrentUserData.UserName;
        }
    }
}

