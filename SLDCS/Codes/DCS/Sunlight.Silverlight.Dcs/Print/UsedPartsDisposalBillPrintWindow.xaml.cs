﻿using Sunlight.Silverlight.Dcs.Web;
using Telerik.ReportViewer.Silverlight;

namespace Sunlight.Silverlight.Dcs.Print {
    public partial class UsedPartsDisposalBillPrintWindow {
        public UsedPartsDisposalBillPrintWindow() {
            InitializeComponent();
            this.ReportViewer.ReportServiceUri = DcsUtils.MakeServerUri("ReportService.svc");
        }

        private UsedPartsDisposalBill usedPartsDisposalBill;

        public UsedPartsDisposalBill UsedPartsDisposalBill {
            get {
                return this.usedPartsDisposalBill;
            }
            set {
                this.usedPartsDisposalBill = value;
                this.OnPropertyChanged("UsedPartsDisposalBill");
            }
        }

        private void ReportViewer1_ApplyParameters(object sender, ApplyParametersEventArgs args) {
            args.ParameterValues["paraUsedPartsDisposalBillId"] = usedPartsDisposalBill.Id;
            args.ParameterValues["UserName"] = BaseApp.Current.CurrentUserData.UserName;
        }
    }
}

