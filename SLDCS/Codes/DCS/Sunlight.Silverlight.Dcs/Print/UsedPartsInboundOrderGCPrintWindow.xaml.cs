﻿using Sunlight.Silverlight.Dcs.Web;
using Telerik.ReportViewer.Silverlight;

namespace Sunlight.Silverlight.Dcs.Print {
    public partial class UsedPartsInboundOrderGCPrintWindow {
        public UsedPartsInboundOrderGCPrintWindow() {
            InitializeComponent();
            this.ReportViewer.ReportServiceUri = DcsUtils.MakeServerUri("ReportService.svc");
        }

        private VirtualUsedPartsInboundOrder usedPartsInboundOrder;
        public VirtualUsedPartsInboundOrder UsedPartsInboundOrder {
            get {
                return this.usedPartsInboundOrder;
            }
            set {
                this.usedPartsInboundOrder = value;
                this.OnPropertyChanged("UsedPartsInboundOrder");
            }
        }

        private void ReportViewer1_ApplyParameters(object sender, ApplyParametersEventArgs args) {
            args.ParameterValues["usedPartsInboundOrderId"] = usedPartsInboundOrder.Id;
            args.ParameterValues["UserName"] = BaseApp.Current.CurrentUserData.UserName;
        }
    }
}
