﻿using Telerik.ReportViewer.Silverlight;

namespace Sunlight.Silverlight.Dcs.Print {
    public partial class UsedPartsInboundOrderPrintWindow {
        private int usedPartsInboundOrderId;

        public UsedPartsInboundOrderPrintWindow() {
            InitializeComponent();
            this.ReportViewer.ReportServiceUri = DcsUtils.MakeServerUri("ReportService.svc");
        }

        public int UsedPartsInboundOrderId {
            get {
                return this.usedPartsInboundOrderId;
            }
            set {
                this.usedPartsInboundOrderId = value;
                this.OnPropertyChanged("UsedPartsInboundOrderId");
            }
        }

        private void ReportViewer1_ApplyParameters(object sender, ApplyParametersEventArgs args) {
            args.ParameterValues["usedPartsInboundOrderId"] = usedPartsInboundOrderId;
            args.ParameterValues["UserName"] = BaseApp.Current.CurrentUserData.UserName;
        }

    }
}
