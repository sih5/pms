﻿using Sunlight.Silverlight.Dcs.Web;
using Telerik.ReportViewer.Silverlight;

namespace Sunlight.Silverlight.Dcs.Print {
    public partial class UsedPartsOutboundOrderPrintWindow {
        public UsedPartsOutboundOrderPrintWindow() {
            InitializeComponent();
            this.ReportViewer.ReportServiceUri = DcsUtils.MakeServerUri("ReportService.svc");
        }

        private UsedPartsOutboundOrder usedPartsOutboundOrder;
        public UsedPartsOutboundOrder UsedPartsOutboundOrder {
            get {
                return this.usedPartsOutboundOrder;
            }
            set {
                this.usedPartsOutboundOrder = value;
                this.OnPropertyChanged("UsedPartsOutboundOrder");
            }
        }

        private void ReportViewer1_ApplyParameters(object sender, ApplyParametersEventArgs args) {
            args.ParameterValues["usedPartsOutboundOrderId"] = usedPartsOutboundOrder.Id;
            args.ParameterValues["UserName"] = BaseApp.Current.CurrentUserData.UserName;
        }
    }
}
