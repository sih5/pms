﻿using Sunlight.Silverlight.Dcs.Web;
using Telerik.ReportViewer.Silverlight;


namespace Sunlight.Silverlight.Dcs.Print {
    public partial class UsedPartsReturnOrderPrintWindow {
        public UsedPartsReturnOrderPrintWindow() {
            InitializeComponent();
            this.ReportViewer.ReportServiceUri = DcsUtils.MakeServerUri("ReportService.svc");
        }
        private UsedPartsReturnOrder usedPartsReturnOrder;
        public UsedPartsReturnOrder UsedPartsReturnOrder {
            get {
                return this.usedPartsReturnOrder;
            }
            set {
                this.usedPartsReturnOrder = value;
                this.OnPropertyChanged("UsedPartsReturnOrder");
            }
        }
        private void ReportViewer1_ApplyParameters(object sender, ApplyParametersEventArgs args) {
            args.ParameterValues["paraUsedPartsReturnOrderId"] = usedPartsReturnOrder.Id;
        }

    }
}

