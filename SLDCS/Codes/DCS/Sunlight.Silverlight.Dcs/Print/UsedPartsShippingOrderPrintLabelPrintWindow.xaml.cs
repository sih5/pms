﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.ReportViewer.Silverlight;

namespace Sunlight.Silverlight.Dcs.Print {
    public partial class UsedPartsShippingOrderPrintLabelPrintWindow {
        public UsedPartsShippingOrderPrintLabelPrintWindow() {
            InitializeComponent();
            this.ReportViewer.ReportServiceUri = DcsUtils.MakeServerUri("ReportService.svc");
        }

        private IEnumerable<UsedPartsShippingDetail> usedPartsShippingDetails;
        public IEnumerable<UsedPartsShippingDetail> UsedPartsShippingDetails {
            get {
                return usedPartsShippingDetails ?? (this.usedPartsShippingDetails = new ObservableCollection<UsedPartsShippingDetail>());
            }
            set {
                this.usedPartsShippingDetails = value;
            }
        }

        private void ReportViewer1_ApplyParameters(object sender, ApplyParametersEventArgs args) {
            string details = null;
            for(var i = 0; i < this.UsedPartsShippingDetails.ToList().Count; i++) {
                if(i == this.UsedPartsShippingDetails.ToList().Count - 1) {
                    details += this.UsedPartsShippingDetails.ToList()[i].GetIdentity();
                } else {
                    details += this.UsedPartsShippingDetails.ToList()[i].GetIdentity() + ",";
                }
            }
            args.ParameterValues["UsedPartsShippingDetailIds"] = details;
        }

    }
}
