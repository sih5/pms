﻿using Telerik.ReportViewer.Silverlight;

namespace Sunlight.Silverlight.Dcs.Print {
    public partial class UsedPartsShippingOrderPrintWindow {
        public UsedPartsShippingOrderPrintWindow() {
            InitializeComponent();
            this.ReportViewer.ReportServiceUri = DcsUtils.MakeServerUri("ReportService.svc");
        }

        private int usedPartsShippingOrderId;

        public int UsedPartsShippingOrderId {
            get {
                return this.usedPartsShippingOrderId;
            }
            set {
                this.usedPartsShippingOrderId = value;
                this.OnPropertyChanged("UsedPartsShippingOrderId");
            }
        }
        private void ReportViewer1_ApplyParameters(object sender, ApplyParametersEventArgs args) {
            args.ParameterValues["usedPartsShippingOrderId"] = UsedPartsShippingOrderId;
            args.ParameterValues["UserName"] = BaseApp.Current.CurrentUserData.UserName;
        }
    }
}
