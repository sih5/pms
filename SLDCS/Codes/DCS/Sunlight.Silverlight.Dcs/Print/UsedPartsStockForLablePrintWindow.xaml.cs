﻿using Telerik.ReportViewer.Silverlight;

namespace Sunlight.Silverlight.Dcs.Print {
    public partial class UsedPartsStockForLablePrintWindow {
        public UsedPartsStockForLablePrintWindow() {
            InitializeComponent();
            this.ReportViewer.ReportServiceUri = DcsUtils.MakeServerUri("ReportService.svc");
        }
        private string usedPartsStock;
        public string UsedPartsStock {
            get {
                return this.usedPartsStock;
            }
            set {
                this.usedPartsStock = value;
                this.OnPropertyChanged("UsedPartsStock");
            }
        }
        private void ReportViewer1_ApplyParameters(object sender, ApplyParametersEventArgs args) {
            args.ParameterValues["paraUsedPartsStockId"] = usedPartsStock;
        }
    }
}
