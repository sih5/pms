﻿using Sunlight.Silverlight.Dcs.Web;
using Telerik.ReportViewer.Silverlight;

namespace Sunlight.Silverlight.Dcs.Print {
    public partial class UsedPartsTransferOrderForLablePrintWindow {
        public UsedPartsTransferOrderForLablePrintWindow() {
            InitializeComponent();
            this.ReportViewer.ReportServiceUri = DcsUtils.MakeServerUri("ReportService.svc");
        }

        private UsedPartsTransferOrder usedPartsTransferOrder;
        public UsedPartsTransferOrder UsedPartsTransferOrder {
            get {
                return this.usedPartsTransferOrder;
            }
            set {
                this.usedPartsTransferOrder = value;
                this.OnPropertyChanged("UsedPartsTransferOrder");
            }
        }
        private void ReportViewer1_ApplyParameters(object sender, ApplyParametersEventArgs args) {
            args.ParameterValues["usedPartsTransferOrderId"] = usedPartsTransferOrder.Id;
        }
    }
}

