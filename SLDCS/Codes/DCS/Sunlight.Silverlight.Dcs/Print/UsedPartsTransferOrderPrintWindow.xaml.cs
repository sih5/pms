﻿
using Sunlight.Silverlight.Dcs.Web;
using Telerik.ReportViewer.Silverlight;

namespace Sunlight.Silverlight.Dcs.Print {
    public partial class UsedPartsTransferOrderPrintWindow {
        public UsedPartsTransferOrderPrintWindow() {
            InitializeComponent();
            this.ReportViewer.ReportServiceUri = DcsUtils.MakeServerUri("ReportService.svc");

        }
        private UsedPartsTransferOrder usedPartsTransferOrder;

        public UsedPartsTransferOrder UsedPartsTransferOrder {
            get {
                return this.usedPartsTransferOrder;
            }
            set {
                this.usedPartsTransferOrder = value;
                this.OnPropertyChanged("UsedPartsTransferOrder");
            }
        }

        private void ReportViewer1_ApplyParameters(object sender, ApplyParametersEventArgs args) {
            args.ParameterValues["usedPartsTransferOrderId"] = UsedPartsTransferOrder.Id;
            args.ParameterValues["UserName"] = BaseApp.Current.CurrentUserData.UserName;
        }
    }
}
