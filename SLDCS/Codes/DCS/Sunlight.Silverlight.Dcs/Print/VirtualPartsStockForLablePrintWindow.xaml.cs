﻿using Telerik.ReportViewer.Silverlight;

namespace Sunlight.Silverlight.Dcs.Print {
    public partial class VirtualPartsStockForLablePrintWindow {
        public VirtualPartsStockForLablePrintWindow() {
            InitializeComponent();
            this.ReportViewer.ReportServiceUri = DcsUtils.MakeServerUri("ReportService.svc");
        }

        private string virtualPartsStock;
        public string VirtualPartsStock {
            get {
                return this.virtualPartsStock;
            }
            set {
                this.virtualPartsStock = value;
                this.OnPropertyChanged("VirtualPartsStock");
            }
        }
        private void ReportViewer1_ApplyParameters(object sender, ApplyParametersEventArgs args) {
            args.ParameterValues["paraPartsStockId"] = virtualPartsStock;
        }

    }
}
