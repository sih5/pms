﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.ReportViewer.Silverlight;

namespace Sunlight.Silverlight.Dcs.Print {
    public partial class VirtualUsedPartsOutboundPlanPrintWindow {
        public VirtualUsedPartsOutboundPlanPrintWindow() {
            InitializeComponent();
            this.ReportViewer.ReportServiceUri = DcsUtils.MakeServerUri("ReportService.svc");
        }

        private UsedPartsOutboundOrder usedPartsOutboundOrder;
        public UsedPartsOutboundOrder UsedPartsOutboundOrder {
            get {
                return this.usedPartsOutboundOrder;
            }
            set {
                this.usedPartsOutboundOrder = value;
                this.OnPropertyChanged("UsedPartsOutboundOrder");
            }
        }

        private void ReportViewer1_ApplyParameters(object sender, ApplyParametersEventArgs args) {
            args.ParameterValues["usedPartsOutboundOrderId"] = usedPartsOutboundOrder.Id;
            args.ParameterValues["UserName"] = BaseApp.Current.CurrentUserData.UserName;
        }

    }
}
