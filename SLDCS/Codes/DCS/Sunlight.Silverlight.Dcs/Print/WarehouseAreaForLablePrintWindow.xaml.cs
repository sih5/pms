﻿using Telerik.ReportViewer.Silverlight;

namespace Sunlight.Silverlight.Dcs.Print {
    public partial class WarehouseAreaForLablePrintWindow {
        public WarehouseAreaForLablePrintWindow() {
            InitializeComponent();
            this.ReportViewer.ReportServiceUri = DcsUtils.MakeServerUri("ReportService.svc");
        }

        private string warehouseArea;
        public string WarehouseArea {
            get {
                return this.warehouseArea;
            }
            set {
                this.warehouseArea = value;
                this.OnPropertyChanged("WarehouseArea");
            }
        }
        private void ReportViewer1_ApplyParameters(object sender, ApplyParametersEventArgs args) {
            args.ParameterValues["paraWarehouseAreaId"] = warehouseArea;
        }
    }
}
