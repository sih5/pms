﻿using System.Reflection;
using System.Resources;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Sunlight.Silverlight.Dcs")]
[assembly: AssemblyCompany("上海晨阑数据技术有限公司")]
[assembly: AssemblyProduct("晨阑Silverlight产品平台")]
[assembly: AssemblyCopyright("版权所有(c) 上海晨阑数据技术有限公司")]
[assembly: NeutralResourcesLanguage("zh-CN")]
[assembly: ComVisible(false)]
[assembly: Guid("3e2a7837-74b1-416f-9eea-8390eb214e90")]
[assembly: AssemblyVersion("2012.1.6.*")]
