﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media.Imaging;
using Sunlight.Silverlight.Dcs.View.Custom;

namespace Sunlight.Silverlight.Dcs {
    public sealed class KeyValueItemConverter : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            if(value == null || parameter == null)
                return null;
            var t = System.Convert.ToInt32(value);
            var result = DcsUtils.GetKeyValuePairs(parameter as string).FirstOrDefault(kvp => kvp.Key == t);
            if(parameter is string && parameter.ToString().IndexOf("DCS|", StringComparison.Ordinal) > -1)
                return result == null ? "" : result.Value;
            return result == null ? value.ToString() : result.Value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            throw new NotImplementedException();
        }
    }

    public class HierarchyConverter : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            var item = value as DataItem;
            if(item != null)
                return item.Owner.Where(i => i.ParentId == item.Id);
            var items = value as DataItemCollection;
            if(items != null)
                return items.Where(i => i.ParentId == 0);
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            throw new NotImplementedException();
        }
    }

    public class CompareInt32Converter : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            var parameterValue = 0;
            int.TryParse(parameter.ToString(), out parameterValue);
            if(value != null && value is int)
                return (int)value == parameterValue;
            return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            throw new NotImplementedException();
        }
    }

    public sealed class BooleanToVisibilityConverter : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            if(value == null)
                return null;
            return value is bool && (bool)value ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            if(value == null || !(value is Visibility))
                return null;
            var visibility = (Visibility)value;
            if(visibility == Visibility.Visible)
                return true;
            if(visibility == Visibility.Collapsed)
                return false;
            return null;
        }
    }

    public sealed class ImageConverter : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            if(value == null)
                return null;

            var result = new BitmapImage(DcsUtils.GetDownloadFileUrl((string)value));
            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            throw new NotImplementedException();
        }

        public sealed class DownloadFileUriConverter : IValueConverter {
            public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
                if(value == null || string.IsNullOrEmpty((string)value))
                    return null;

                return DcsUtils.GetDownloadFileUrl((string)value);
            }

            public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
                throw new NotImplementedException();
            }
        }

        public sealed class FileNameConverter : IValueConverter {
            public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
                if(value == null || string.IsNullOrEmpty((string)value))
                    return null;

                return Path.GetFileName((string)value);
            }

            public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
                throw new NotImplementedException();
            }
        }
    }

    public sealed class AvatarUriConverter : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            var photo = value as string;
            return DcsUtils.GetPersonnelPhotoUri(photo);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            throw new NotImplementedException();
        }
    }
    public sealed class IsEnableConverter : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            var isenabled = !(bool)value;
            return isenabled;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            throw new NotImplementedException();
        }
    }
}
