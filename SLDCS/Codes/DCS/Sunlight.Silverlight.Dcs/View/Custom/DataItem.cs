﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;

namespace Sunlight.Silverlight.Dcs.View.Custom {
    public class DataItem {
        public int Id {
            get;
            set;
        }

        public int ParentId {
            get;
            set;
        }

        public string Text {
            get;
            set;
        }

        public Entity Entity {
            get;
            set;
        }

        public DataItemCollection Owner {
            get;
            protected set;
        }

        internal void SetOwner(DataItemCollection collection) {
            this.Owner = collection;
        }

        public List<DataItem> Children {
            get {
                var children = new List<DataItem>();
                if(this.Owner != null)
                    children = this.Owner.Where(item => item.ParentId == this.Id).ToList();
                return children;
            }
        }

        public List<DataItem> AllChildren {
            get {
                var allChildren = this.Children.ToList();
                foreach(var childItem in this.Children) {
                    allChildren.AddRange(childItem.AllChildren);
                }
                return allChildren;
            }
        }
    }
}
