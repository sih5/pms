﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.ComponentModel;
using System.Linq;
namespace Sunlight.Silverlight.Dcs.Web {
    public partial class ABCSetting {      
        /// <summary>
        /// 销售频次
        /// </summary>
        private string saleFrequency;
        public string SaleFrequency {
            get {
                if(this.SaleFrequencyFrom.HasValue) {
                    return this.SaleFrequencyFrom + "%—" + this.SaleFrequencyTo+"%";
                } else  return "";
            }
            set {
                this.saleFrequency = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("SaleFrequency"));
            }
        }
        /// <summary>
        /// 销售额
        /// </summary>
        private string saleFee;
        public string SaleFee {
            get {
                if(this.SaleFeeFrom.HasValue) {
                    return this.SaleFeeFrom + "%--" + this.SaleFeeTo + "%";
                } else return "";
            }
            set {
                this.saleFee = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("SaleFee"));
            }
        }
    }
}
