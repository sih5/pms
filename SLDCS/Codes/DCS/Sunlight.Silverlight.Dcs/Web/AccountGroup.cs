﻿
using System.ComponentModel;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class AccountGroup {

        private string companyName;

        public string CompanyName {
            get {
                return this.companyName;
            }
            set {
                this.companyName = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("CompanyName"));
            }
        }
    }
}
