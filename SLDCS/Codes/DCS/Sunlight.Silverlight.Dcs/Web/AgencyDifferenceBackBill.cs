﻿
using System.ComponentModel;
using System.Linq;
namespace Sunlight.Silverlight.Dcs.Web {
    public partial class AgencyDifferenceBackBill {
        private bool? isUplodFile;
        public bool? IsUplodFile {
            get {
                return isUplodFile = this.Path != null && this.Path.Length > 0;
            }
            set {
                this.isUplodFile = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("IsUplodFile"));
            }
        }
    }
}
