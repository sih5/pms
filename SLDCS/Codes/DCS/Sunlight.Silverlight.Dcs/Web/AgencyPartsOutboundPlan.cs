﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class AgencyPartsOutboundPlan {
        /// <summary>
        /// 总金额
        /// </summary>
        private decimal totalAmount;
        public decimal TotalAmount {
            get {
                return this.APartsOutboundPlanDetails.Sum(r => r.Price * r.PlannedAmount);

            }
            set {
                this.totalAmount = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("TotalAmount"));
            }
        }
    }
}
