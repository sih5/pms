﻿using System.ComponentModel;
using System.Linq;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class AgencyPartsShippingOrder {
        /// <summary>
        /// 总金额
        /// </summary>
        private decimal totalAmount;
        private string erpSourceOrderCodeQuery;
        public decimal TotalAmount {
            get {
                return this.APartsShippingOrderDetails.Sum(r => r.SettlementPrice * r.ShippingAmount);

            }
            set {
                this.totalAmount = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("TotalAmount"));
            }
        }

        public string ClosedLoop {
            get {
                if(this.Status == (int)DcsPartsShippingOrderStatus.回执确认 || this.Status == (int)DcsPartsShippingOrderStatus.收货确认 || this.Status == (int)DcsPartsShippingOrderStatus.作废) {
                    return "闭环";
                }
                return "未闭环";
            }
        }

        public string ERPSourceOrderCodeQuery {
            get {
                return this.erpSourceOrderCodeQuery;
            }
            set {
                this.erpSourceOrderCodeQuery = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("ERPSourceOrderCodeQuery"));
            }
        }
    }
}
