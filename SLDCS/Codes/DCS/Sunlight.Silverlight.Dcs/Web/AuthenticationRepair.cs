﻿
using System;

namespace Sunlight.Silverlight.Dcs.Web {
    public class AuthenticationRepair {
        public int? PartsSalesCategoryId {
            get;
            set;
        }
        public int? ServiceProductLineId {
            get;
            set;
        }
        public string DealerCode {
            get;
            set;
        }
        public string DealerName {
            get;
            set;
        }
        public string RepairWorkerName {
            get;
            set;
        }
        public string MalfunctionCode {
            get;
            set;
        }
        public int? RepairClaimStatus {
            get;
            set;
        }
        public string EngineModel {
            get;
            set;
        }
        public string TrainingName {
            get;
            set;
        }
        public DateTime? TrainingTime {
            get;
            set;
        }
        public string RepairItemCode {
            get;
            set;
        }
        public string RepairItemName {
            get;
            set;
        }
        public DateTime? RepairRequestTime {
            get;
            set;
        }
    }
}
