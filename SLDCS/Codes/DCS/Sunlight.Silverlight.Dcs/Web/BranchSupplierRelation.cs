﻿using System.ComponentModel;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class BranchSupplierRelation {
        private int sequeueNumber;
        public int SequeueNumber {
            get {
                return this.sequeueNumber;
            }
            set {
                this.sequeueNumber = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("SequeueNumber"));
            }
        }
    }
}
