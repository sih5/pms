﻿using System.ComponentModel;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class CompanyAddress {
        private int provinceId;
        public int ProvinceId {
            get {
                return this.provinceId;
            }
            set {
                this.provinceId = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("ProvinceId"));
            }
        }
        private int cityId;
        public int CityId {
            get {
                return this.cityId;
            }
            set {
                this.cityId = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("CityId"));
            }
        }
        private int countyId;
        public int CountyId {
            get {
                return this.countyId;
            }
            set {
                this.countyId = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("CountyId"));
            }
        }
    }
}
