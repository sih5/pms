﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class CompanyInvoiceInfo {
        private int? compDocumentType;
        /// <summary>
        /// 企业证件类型
        /// </summary>
        public int? CompDocumentType {
            get {
                return this.compDocumentType;
            }
            set {
                this.compDocumentType = value;
                //this.OnPropertyChanged(new PropertyChangedEventArgs("CompDocumentType"));
            }
        }
    }
}
