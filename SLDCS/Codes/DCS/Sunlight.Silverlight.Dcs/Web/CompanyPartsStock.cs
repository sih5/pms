﻿using System.ComponentModel;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class CompanyPartsStock {
        /// <summary>
        ///  库存满足数量
        /// </summary>
        private int usableStock;
        public int UsableStock {
            get {
                return this.usableStock;
            }
            set {
                this.usableStock = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("UsableStock"));
            }
        }

        /// <summary>
        ///  库存不满足数量
        /// </summary>
        private int unUsableStock;
        public int UnUsableStock {
            get {
                return this.unUsableStock;
            }
            set {
                this.unUsableStock = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("UnUsableStock"));
            }
        }

        /// <summary>
        /// 订货数量
        /// </summary>
        private int orderQuantity;
        public int OrderQuantity {
            get {
                return this.orderQuantity;
            }
            set {
                this.orderQuantity = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("OrderQuantity"));
            }
        }


    }
}
