﻿using System.ComponentModel;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class CompanyTransferOrder {
        private int? agencyWarehouseId;
        public int? AgencyWarehouseId {
            get {
                return this.agencyWarehouseId;
            }
            set {
                this.agencyWarehouseId = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("AgencyWarehouseId"));
            }
        }
    }
}
