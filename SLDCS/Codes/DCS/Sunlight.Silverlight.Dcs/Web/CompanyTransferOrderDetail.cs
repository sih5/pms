﻿
using System.ComponentModel;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class CompanyTransferOrderDetail {
        private int currentOutboundAmount;
        public int CurrentOutboundAmount {
            get {
                return this.currentOutboundAmount;
            }
            set {
                this.currentOutboundAmount = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("CurrentOutboundAmount"));
            }
        }
    }
}
