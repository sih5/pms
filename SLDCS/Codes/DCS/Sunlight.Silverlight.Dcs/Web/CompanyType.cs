﻿
namespace Sunlight.Silverlight.Dcs.Web {
    public class CompanyType {
        public string Type {
            get;
            set;
        }

        public int TypeValue {
            get;
            set;
        }

        public bool IsCheck {
            get;
            set;
        }
    }
}
