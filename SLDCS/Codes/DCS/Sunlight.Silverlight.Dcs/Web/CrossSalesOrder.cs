﻿
using System.ComponentModel;
using System.Linq;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class CrossSalesOrder {
        private bool? isUplodFile;
        private string receivingNames;
        private string receivingPhones;
        private string receivingAddresss;
        public bool? IsUplodFile {
            get {
                return isUplodFile = this.Path != null && this.Path.Length > 0;
            }
            set {
                this.isUplodFile = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("IsUplodFile"));
            }
        }
        public string ReceivingNames {
            get {
                return receivingNames = BaseApp.Current.CurrentUserData.EnterpriseId == this.SubCompanyId ? "" : this.ReceivingName;
            }
            set {
                this.receivingNames = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("ReceivingNames"));
            }
        }
        public string ReceivingPhones {
            get {
                return receivingPhones = BaseApp.Current.CurrentUserData.EnterpriseId == this.SubCompanyId ? "" : this.ReceivingPhone;
            }
            set {
                this.receivingPhones = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("ReceivingPhones"));
            }
        }
        public string ReceivingAddresss {
            get {
                return receivingAddresss = BaseApp.Current.CurrentUserData.EnterpriseId == this.SubCompanyId ? "" : this.ReceivingAddress;
            }
            set {
                this.receivingAddresss = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("ReceivingAddresss"));
            }
        }
    }
}
