﻿
using System;
using System.ComponentModel;
using System.Linq;
namespace Sunlight.Silverlight.Dcs.Web {
    public partial class Customer {
        private int provinceId;
        public int ProvinceId {
            get {
                return this.provinceId;
            }
            set {
                this.provinceId = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("ProvinceId"));
            }
        }
        private int cityId;
        public int CityId {
            get {
                return this.cityId;
            }
            set {
                this.cityId = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("CityId"));
            }
        }
        private int countyId;
        public int CountyId {
            get {
                return this.countyId;
            }
            set {
                this.countyId = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("CountyId"));
            }
        }
        public string VehicleLinkmanModifyName {
		 get {
                //return this.Dealer.DealerServiceInfoes.FirstOrDefault(r => r.PartsSalesCategoryId == this.PartsSalesCategoryId && r.Status == (int)DcsMasterDataStatus.有效) == null ? null : this.Dealer.DealerServiceInfoes.First(r => r.PartsSalesCategoryId == this.PartsSalesCategoryId && r.Status == (int)DcsMasterDataStatus.有效).BusinessCode;
                //return this.RetainedCustomers.FirstOrDefault() == null ? null : this.RetainedCustomers.FirstOrDefault().VehicleLinkmen.FirstOrDefault() == null ? null : this.RetainedCustomers.FirstOrDefault().VehicleLinkmen.FirstOrDefault().ModifierName;
             return null;
            }
        }
        public DateTime? VehicleLinkmanModifyTime {
            get {
                //return this.Dealer.DealerServiceInfoes.FirstOrDefault(r => r.PartsSalesCategoryId == this.PartsSalesCategoryId && r.Status == (int)DcsMasterDataStatus.有效) == null ? null : this.Dealer.DealerServiceInfoes.First(r => r.PartsSalesCategoryId == this.PartsSalesCategoryId && r.Status == (int)DcsMasterDataStatus.有效).BusinessCode;
                //return this.RetainedCustomers.FirstOrDefault() == null ? null : this.RetainedCustomers.FirstOrDefault().VehicleLinkmen.FirstOrDefault() == null ? null : this.RetainedCustomers.FirstOrDefault().VehicleLinkmen.FirstOrDefault().ModifyTime;
                return null;
            }
        }
    }
}