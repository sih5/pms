﻿using System.ComponentModel;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class CustomerAccount {
        private decimal lockBalance;

        /// <summary>
        /// 锁定金额
        /// </summary>
        public decimal LockBalance {
            get {
                return this.ShippedProductValue + this.PendingAmount;
            }
            set {
                this.lockBalance = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("LockBalance"));
            }
        }

        private decimal balance;

        /// <summary>
        /// 账户余额
        /// </summary>
        public decimal Balance {
            get {
                return this.AccountBalance - this.ShippedProductValue;
            }
            set {
                this.balance = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("Balance"));
            }
        }

        /// <summary>
        /// 可用金额
        /// </summary>
        private decimal usableBalance;
        public decimal UsableBalance {
            get {
                return this.AccountBalance + this.CustomerCredenceAmount - this.ShippedProductValue - this.PendingAmount;
            }
            set {
                this.usableBalance = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("UsableBalance"));
            }
        }

        /// <summary>
        /// 信用金额
        /// </summary>
        private decimal credenceAmount;
        public decimal CredenceAmount {
            get {
                return this.CustomerCredenceAmount + this.TempCreditTotalFee ?? 0;
            }
            set {
                this.credenceAmount = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("CredenceAmount"));
            }
        }
    }
}
