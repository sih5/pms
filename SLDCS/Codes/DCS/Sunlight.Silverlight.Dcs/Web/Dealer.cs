﻿using System.Linq;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class Dealer {

        public DealerServiceInfo DealerServiceInfo {
            get {
                return this.DealerServiceInfoes.FirstOrDefault() ?? new DealerServiceInfo();
            }
        }
       
    }
}
