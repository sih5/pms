﻿

using System.ComponentModel;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class DealerBusinessPermit {
        public string serviceProductLineCode;
        public string ServiceProductLineCode {
            get {
                //if(this.ProductLineType != default(int) && (EngineProductLine != null || ServiceProductLine != null)) {
                //    return this.ProductLineType == (int)DcsServiceProductLineViewProductLineType.发动机产品线 ? this.EngineProductLine.EngineCode : this.ServiceProductLine.Code;
                //}
                return serviceProductLineCode;
            }
            set {
                this.serviceProductLineCode = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("ServiceProductLineCode"));
            }
        }
        public string serviceProductLineName;
        public string ServiceProductLineName {
            get {
                //if(this.ProductLineType != default(int) && (EngineProductLine != null || ServiceProductLine != null)) {
                //    return this.ProductLineType == (int)DcsServiceProductLineViewProductLineType.发动机产品线 ? this.EngineProductLine.EngineName : this.ServiceProductLine.Name;
                //}
                return serviceProductLineName;
            }
            set {
                this.serviceProductLineName = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("ServiceProductLineName"));
            }
        }

        public string PartsSalesCategoryName {
            get;
            set;

        }
    }
}
