﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class DealerMobileNumberList {
        private string dealerCode;
        private string dealerName;
        private int serialNumber;
        /// <summary>
        /// 服务站编号
        /// </summary>
        public string DealerCode {
            get {
                return this.dealerCode;
            }
            set {
                this.dealerCode = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("DealerCode"));
            }
        }

        /// <summary>
        /// 服务站名称
        /// </summary>
        public string DealerName {
            get {
                return this.dealerName;
            }
            set {
                this.dealerName = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("DealerName"));
            }
        }

        [Display(Name = "_Common_SerialNumber", ResourceType = typeof(Resources.EntityStrings))]
        public int SerialNumber {
            get {
                return this.serialNumber;
            }
            set {
                this.serialNumber = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("SerialNumber"));
            }
        }

    }
}
