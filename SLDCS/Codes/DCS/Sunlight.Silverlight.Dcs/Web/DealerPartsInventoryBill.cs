﻿
using System.ComponentModel;
using System.Linq;
namespace Sunlight.Silverlight.Dcs.Web {
    public partial class DealerPartsInventoryBill {
        public decimal? SumCostBeforeInventory {
            get {
                return this.DealerPartsInventoryDetails.Sum(r => (r.DealerPrice ??0) * r.CurrentStorage);
            }
        }

        public decimal? SumCostAfterInventory {
            get {
                return this.DealerPartsInventoryDetails.Sum(r => (r.DealerPrice ??0)  * r.StorageAfterInventory); 
            }
        }
        public decimal? SumCostDifference {
            get {
                return this.DealerPartsInventoryDetails.Sum(r => (r.DealerPrice ??0)  * r.StorageDifference); 
            }
        }
        public bool? IsUplodFile {
            get {
                return this.Path != null && this.Path.Length > 0;
            }
            
        }
    }
}
