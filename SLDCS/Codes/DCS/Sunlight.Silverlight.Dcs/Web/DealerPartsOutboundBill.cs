﻿using System.ComponentModel;
using System.Linq;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class DealerPartsOutboundBill {
        /// <summary>
        /// 总金额
        /// </summary>
        private decimal totalAmount;
        public decimal TotalAmount {
            get {
                return this.OriginalPrice* this.Quantity;

            }
            set {
                this.totalAmount = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("TotalAmount"));
            }
        }
    }
}
