﻿using System.ComponentModel;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class DealerPartsTransOrderDetail {

        private decimal sumWarrantyPrice;
        public decimal SumWarrantyPrice {
            get {
                return this.sumWarrantyPrice;
            }
            set {
                this.sumWarrantyPrice = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("SumWarrantyPrice"));
            }
        }
        private decimal sumNoWarrantyPrice;
        public decimal SumNoWarrantyPrice {
            get {
                return this.sumNoWarrantyPrice;
            }
            set {
                this.sumNoWarrantyPrice = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("SumNoWarrantyPrice"));
            }
        }

        private int serialNumber;
        public int SerialNumber {
            get {
                return this.serialNumber;
            }
            set {
                this.serialNumber = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("SerialNumber"));
            }
        }
    }
}
