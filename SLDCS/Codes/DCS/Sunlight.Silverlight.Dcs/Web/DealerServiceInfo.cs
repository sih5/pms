﻿using System.ComponentModel;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class DealerServiceInfo {
        private string marketingDepartmentName;

        public string MarketingDepartmentName {
            get {
                return this.marketingDepartmentName;
            }
            set {
                this.marketingDepartmentName = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("MarketingDepartmentName"));
            }
        }
    }
}
