﻿using System.ComponentModel;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class ExtendedWarrantyOrder {
        public int cityId;
        private bool isApproveYes;
        public bool IsApproveYes {
            get {
                return isApproveYes;
            }
            set {
                this.isApproveYes = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("IsApproveYes"));
            }
        }

        private bool isApproveNo;
        public bool IsApproveNo {
            get {
                return isApproveNo;
            }
            set {
                this.isApproveNo = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("IsApproveNo"));
            }
        }
        private bool isApproveBack;
        public bool IsApproveBack {
            get {
                return isApproveBack;
            }
            set {
                this.isApproveBack = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("IsApproveBack"));
            }
        }

        public int provinceId {
            get;
            set;
        }

        public int CityId {
            get {

                return this.cityId;
            }
            set {
                this.cityId = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("CityId"));
            }
        }

        public int ProvinceId {
            get {

                return this.provinceId;
            }
            set {
                this.provinceId = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("ProvinceId"));
            }
        }

        public string MailingAddress {
            get {

                return this.Province + this.City + this.Region;
            }

        }

        /// <summary>
        /// 是否存在 会员购买延保产品折扣
        /// </summary>

        private bool isExistYBCPZK;
        public bool IsExistYBCPZK {
            get {
                return isExistYBCPZK;
            }
            set {
                this.isExistYBCPZK = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("IsExistYBCPZK"));
            }
        }


        //权益折扣
        private decimal discountNum;
        public decimal DiscountNum {
            get {
                return discountNum;
            }
            set {
                this.discountNum = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("DiscountNum"));
            }
        }
    }
}