﻿using System;
using System.ComponentModel;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class ExtendedWarrantyOrderList {
        private string extendedWarrantyProductName;
        /// <summary>
        /// 延保产品名称
        /// </summary>
        public string ExtendedWarrantyProductName {
            get {
                return this.ExtendedWarrantyProduct == null ? this.extendedWarrantyProductName : this.ExtendedWarrantyProduct.ExtendedWarrantyProductName;
            }
            set {
                this.extendedWarrantyProductName = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("ExtendedWarrantyProductName"));
            }
        }
        private int? extendedWarrantyMileage;
        /// <summary>
        /// 延保里程
        /// </summary>
        public int? ExtendedWarrantyMileage {
            get {
                return this.ExtendedWarrantyProduct == null ? this.extendedWarrantyMileage : this.ExtendedWarrantyProduct.ExtendedWarrantyMileage;
            }
            set {
                this.extendedWarrantyMileage = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("ExtendedWarrantyMileage"));
            }
        }

        private int extendedWarrantyTerm;
        /// <summary>
        /// 延保期限
        /// </summary>
        public int ExtendedWarrantyTerm {
            get {
                return this.ExtendedWarrantyProduct == null ? this.extendedWarrantyTerm : (this.ExtendedWarrantyProduct.ExtendedWarrantyTerm == null ? 0 : (int)this.ExtendedWarrantyProduct.ExtendedWarrantyTerm);
            }
            set {
                this.extendedWarrantyTerm = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("ExtendedWarrantyTerm"));
            }
        }

        private int serialNumber;
        public int SerialNumber {
            get {
                return this.serialNumber;
            }
            set {
                this.serialNumber = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("SerialNumber"));
            }
        }

        public DateTime? GetEndDate() {
            if(this.ValidFrom == null)
                return null;
            return ((DateTime)this.ValidFrom).AddMonths(this.ExtendedWarrantyTerm);

        }

        public int? GetEndMileage() {
            if(this.StartMileage == null || this.ExtendedWarrantyMileage == null)
                return null;
            return this.StartMileage + this.ExtendedWarrantyMileage;
        }
    }
}