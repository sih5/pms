﻿using System;

namespace Sunlight.Silverlight.Dcs.Web {
    public class FileEntityes {
        public string FileName {
            get;
            set;
        }

        public string FilePath {
            get;
            set;
        }

        public string FtpServerKey {
            get;
            set;
        }

        public Uri FileLink {
            get;
            set;
        }
        public FileEntityes(string imageName, string imagePath) {
            this.FileName = imageName;
            this.FilePath = imagePath;
        }

        public FileEntityes() {
          
        }
        public FileEntityes(string imageName, string imagePath, string ftpServerKey) {
            this.FileName = imageName;
            this.FilePath = imagePath;
            this.FtpServerKey = ftpServerKey;
        }

        public FileEntityes(string imageName, string imagePath, string ftpServerKey,Uri imageLink) {
            this.FileName = imageName;
            this.FilePath = imagePath;
            this.FileLink = imageLink;
            this.FtpServerKey = ftpServerKey;
        }
    }
}
