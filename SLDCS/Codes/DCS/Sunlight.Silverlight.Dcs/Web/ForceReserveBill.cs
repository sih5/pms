﻿using System;
using System.ComponentModel;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class ForceReserveBill {
        /// <summary> 
        /// 是否上传附件 
        /// </summary> 
        public bool? IsAttach {
            get {
                if(this.Path != null && this.Path.Length > 0) {
                    return true;

                } else {
                    return false;

                }
            }
        }
        public int ItemQty {
            get {
                return this.ForceReserveBillDetails.Count;
            }
        }
        public decimal? FeeAmount {
            get {
                decimal? sum=0;
                foreach(var item  in this.ForceReserveBillDetails){
                    sum += (item.ReserveFee)??0;
                }
                return sum;
            }

        }
    }
}