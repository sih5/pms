﻿using System;
using System.ComponentModel;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class InternalAcquisitionDetail {
        private string referenceCodeQuery;
        public string ReferenceCodeQuery {
            get {
                return this.referenceCodeQuery;
            }
            set {
                this.referenceCodeQuery = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("ReferenceCodeQuery"));
            }
        }
    }
}
