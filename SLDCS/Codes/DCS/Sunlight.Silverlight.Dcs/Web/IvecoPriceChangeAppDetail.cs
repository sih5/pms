﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class IvecoPriceChangeAppDetail {
        private decimal? salesPrice;
        /// <summary>
        /// 企业证件类型
        /// </summary>
        public decimal? SalesPrice {
            get {
                return this.salesPrice;
            }
            set {
                this.salesPrice = value;
            }
        }
    }
}
