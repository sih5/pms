﻿
using System;
using System.ComponentModel;
using System.Linq;
namespace Sunlight.Silverlight.Dcs.Web {
    /// <summary>
    /// 物流信息主单
    /// </summary>
    public partial class Logistic {
        private int detialSignStatus;
        /// <summary>
        /// 清单签收状态
        /// </summary>
        public int DetialSignStatus {
            get {
                return this.detialSignStatus;
            }
            set {
                this.detialSignStatus = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("DetialSignStatus"));
            }
        }
    }
}
