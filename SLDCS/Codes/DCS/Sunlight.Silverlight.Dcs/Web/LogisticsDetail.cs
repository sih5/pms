﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
namespace Sunlight.Silverlight.Dcs.Web {
    public partial class LogisticsDetail {
        private int number;
        public int Number {
            get {
                return this.number;
            }
            set {
                this.number = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("Number"));
            }
        }
    }
}
