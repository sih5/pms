﻿using System.ComponentModel;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class MarketABQualityInformation {
        private int isPassed;
        public int IsPassed {
            get {
                return this.isPassed;
            }
            set {
                this.isPassed = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("IsPassed"));
            }
        }
        private string branchName;
        public string BranchName {
            get {
                return this.branchName;
            }
            set {
                this.branchName = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("BranchName"));
            }
        }

        private string partsSalesCategoryName;
        public string PartsSalesCategoryName {
            get {
                return this.partsSalesCategoryName;
            }
            set {
                this.partsSalesCategoryName = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("PartsSalesCategoryName"));
            }
        }
        public string DealerCode {
            get {
                return BaseApp.Current.CurrentUserData.EnterpriseCode;
            }
        }
        public string DealerName {
            get {
                return BaseApp.Current.CurrentUserData.EnterpriseName;
            }
        }
    }
}
