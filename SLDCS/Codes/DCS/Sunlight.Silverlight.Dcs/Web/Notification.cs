﻿
namespace Sunlight.Silverlight.Dcs.Web {
    public partial class Notification {
        public string HavePath {
            get {
                if(string.IsNullOrWhiteSpace(this.Path))
                    return "无";
                else {
                    return "有";
                }
            }

        }
    }
}
