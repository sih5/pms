﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class OutboundAndInboundBill {
        private int serialNumber;

        [Display(Name = "_Common_SerialNumber", ResourceType = typeof(Resources.EntityStrings))]
        public int SerialNumber {
            get {
                return this.serialNumber;
            }
            set {
                this.serialNumber = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("SerialNumber"));
            }
        }

        private string entityType;
        /// <summary>
        /// 数据状态。采购结算管理，时候用到
        /// </summary>
        public string EntityType {
            get {
                return this.entityType;
            }
            set {
                this.entityType = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("EntityType"));
            }
        }
    }
}
