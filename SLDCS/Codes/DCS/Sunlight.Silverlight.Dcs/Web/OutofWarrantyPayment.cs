﻿
using System.ComponentModel;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class OutofWarrantyPayment {
        public int SequeueNumber {
            get;
            set;
        }

        private string responsibleUnitName;

        public string ResponsibleUnitName {
            get {
                return this.responsibleUnitName;
            }
            set {
                this.responsibleUnitName = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("ResponsibleUnitName"));
            }
        }
        private string sourceTypeName;

        public string SourceTypeName {
            get {
                return this.sourceTypeName;
            }
            set {
                this.sourceTypeName = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("SourceTypeName"));
            }
        }

        private string serviceProductLineName;

        public string ServiceProductLineName {
            get {
                return this.serviceProductLineName;
            }
            set {
                this.serviceProductLineName = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("ServiceProductLineName"));
            }
        }
        //星级系数
        private string gradeCoefficientName;

        public string GradeCoefficientName {
            get {
                return this.gradeCoefficientName;
            }
            set {
                this.gradeCoefficientName = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("GradeCoefficientName"));
            }
        }
    }
}
