﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Sunlight.Silverlight.Dcs.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class OverstockPartsAppDetail {

        private int serialNumber;

        [Display(Name = "_Common_SerialNumber", ResourceType = typeof(Resources.EntityStrings))]
        public int SerialNumber {
            get {
                return this.serialNumber;
            }
            set {
                this.serialNumber = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("SerialNumber"));
            }
        }

        public string ConfirmResult {
            get {
                if(this.IfConfirmed.HasValue)
                    if(this.IfConfirmed.Value)
                        return DcsUIStrings.OverstockPartsAppDetail_ConfirmResult_OverstockPart;
                    else
                        return DcsUIStrings.OverstockPartsAppDetail_ConfirmResult_NonOverstockPart;
                return null;
            }
        }

        private decimal partsSalesPrice;

        public decimal PartsSalesPrice {
            get {
                return this.partsSalesPrice;
            }
            set {
                this.partsSalesPrice = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("PartsSalesPrice"));
            }
        }


    }
}
