﻿
using System.ComponentModel;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class PartsBranch {
        private string increaseRateGroupCode;
        /// <summary>
        /// 加价率分组编号
        /// </summary>
        public string IncreaseRateGroupCode {
            get {
                return this.increaseRateGroupCode;
            }
            set {
                this.increaseRateGroupCode = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("IncreaseRateGroupCode"));
            }
        }
    }
}
