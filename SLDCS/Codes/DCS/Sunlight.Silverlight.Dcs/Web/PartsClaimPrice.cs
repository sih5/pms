﻿using System.ComponentModel;


namespace Sunlight.Silverlight.Dcs.Web {
    public partial class PartsClaimPrice {
        private int selectNumber;
        /// <summary>
        /// 选择数量
        /// </summary>
        public int SelectNumber {
            get {
                return this.selectNumber == 0 ? 1 : this.selectNumber;
            }
            set {
                this.selectNumber = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("SelectNumber"));
            }
        }
    }
}
