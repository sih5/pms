﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
namespace Sunlight.Silverlight.Dcs.Web {
    public partial class PartsExchange{
         private int serialNumber;
         private string partsSalesCategoryName;

        [Display(Name = "_Common_SerialNumber", ResourceType = typeof(Resources.EntityStrings))]
        public int SequeueNumber {
            get {
                return this.serialNumber;
            }
            set {
                this.serialNumber = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("SequeueNumber"));
            }
        }

        /// <summary>
        /// 品牌
        /// </summary>
        public string PartsSalesCategoryName {
            get {
                return this.partsSalesCategoryName;
            }
            set {
                this.partsSalesCategoryName = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("PartsSalesCategoryName"));
            }
        }

        /// <summary>
        /// 销售价
        /// </summary>
        private decimal? salesPrice;
        public decimal? SalesPrice {
            get {
                return this.salesPrice;
            }
            set {
                this.salesPrice = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("SalesPrice"));
            }
        }

        /// <summary>
        /// 零售指导价
        /// </summary>
        private decimal? retailGuidePrice;
        public decimal? RetailGuidePrice {
            get {
                return this.retailGuidePrice;
            }
            set {
                this.retailGuidePrice = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("RetailGuidePrice"));
            }
        }
    }
}
