﻿using System.ComponentModel;
using System.Linq;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class PartsInboundCheckBill {
        /// <summary>
        /// 总金额
        /// </summary>
        private decimal totalAmount;
        public decimal TotalAmount {
            get {
                return this.PartsInboundCheckBillDetails.Sum(r => r.SettlementPrice * r.InspectedQuantity);

            }
            set {
                this.totalAmount = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("TotalAmount"));
            }
        }

        /// <summary>
        /// 计划价总金额
        /// </summary>
        private decimal plannedPriceTotalAmount;
        public decimal PlannedPriceTotalAmount {
            get {
                return this.PartsInboundCheckBillDetails.Sum(r => r.CostPrice * r.InspectedQuantity);

            }
            set {
                this.plannedPriceTotalAmount = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("PlannedPriceTotalAmount"));
            }
        }


        /// <summary>
        /// 品牌名称
        /// </summary>
        private string partsSalesCategoryName;
        public string PartsSalesCategoryName {
            get {
                return this.partsSalesCategoryName;
            }
            set {
                this.partsSalesCategoryName = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("PartsSalesCategoryName"));
            }
        }

        public string PartsPurchaseOrderTypeNameQuery {
            get;
            set;
        }
        /// <summary>
        /// 计划来源
        /// </summary>
        private string planSourceQuery;
        public string PlanSourceQuery {
            get {
                return this.planSourceQuery;
            }
            set {
                this.planSourceQuery = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("PlanSourceQuery"));
            }
        }

        /// <summary>
        /// 入库计划单号
        /// </summary>
        private string partsInboundPlanCode;
        public string PartsInboundPlanCode {
            get {
                return this.partsInboundPlanCode;
            }
            set {
                this.partsInboundPlanCode = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("PartsInboundPlanCode"));
            }
        }

        /// <summary>
        /// 发运单号
        /// </summary>
        private string sourceCode;
        public string SourceCode {
            get {
                return this.sourceCode;
            }
            set {
                this.sourceCode = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("SourceCode"));
            }
        }
    }
}
