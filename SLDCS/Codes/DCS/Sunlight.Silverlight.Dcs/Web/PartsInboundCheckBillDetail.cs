﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class PartsInboundCheckBillDetail {
        private int plannedAmount;
        private string batchInformation;

        [Display(Name = "PartsInboundCheckBillDetail_PlannedAmount", ResourceType = typeof(Resources.EntityStrings))]
        public int PlannedAmount {
            get {
                return this.plannedAmount;
            }
            set {
                this.plannedAmount = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("PlannedAmount"));
            }
        }

        [Display(Name = "PartsInboundCheckBillDetail_BatchInformation", ResourceType = typeof(Resources.EntityStrings))]
        public string BatchInformation {
            get {
                return this.batchInformation;
            }
            set {
                this.batchInformation = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("BatchInformation"));
            }
        }

        /// <summary>
        /// 源单据编号 虚拟扩展字段
        /// 1.用于配件采购业务下， 分公司配件采购退货管理 节点。选择 配件检验入库单 时，同时带回 配件采购订单编号
        /// </summary>
        public string OriginalRequirementBillCode {
            get;
            set;
        }

        private int? returnableQuantity;
        public int? ReturnableQuantity {
            get {
                return this.returnableQuantity;
            }
            set {
                this.returnableQuantity = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("ReturnableQuantity"));
            }
        }

        private int allInspectedQuantity;
        public int AllInspectedQuantity {
            get {
                return this.allInspectedQuantity;
            }
            set {
                this.allInspectedQuantity = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("AllInspectedQuantity"));
            }
        }
    }
}
