﻿using System.ComponentModel;
using System.Linq;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class PartsInboundPlan {
        /// <summary>
        /// 总金额
        /// </summary>
        private decimal totalAmount;
        public decimal TotalAmount {
            get {
                return this.PartsInboundPlanDetails.Sum(r => r.Price * r.PlannedAmount);

            }
            set {
                this.totalAmount = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("TotalAmount"));
            }
        }



        /// <summary>
        /// 计划来源
        /// </summary>
        private string planSource1;
        public string PlanSource1 {
            get {
                return this.planSource1;

            }
            set {
                this.planSource1 = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("PlanSource1"));
            }
        }

        /// <summary>
        /// 采购订单类型名称
        /// </summary>
        private string partsPurchaseOrderTypeName1;
        public string PartsPurchaseOrderTypeName1 {
           get {
                return this.partsPurchaseOrderTypeName1;

            }
            set {
                this.partsPurchaseOrderTypeName1 = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("PartsPurchaseOrderTypeName1"));
            }
        }
    }
}
