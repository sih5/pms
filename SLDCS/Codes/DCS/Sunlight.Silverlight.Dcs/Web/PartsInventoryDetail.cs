﻿
using System.ComponentModel;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class PartsInventoryDetail {
        /// <summary>
        /// 显示盘点后库存
        /// </summary>
        public int? NewStorageAfterInventory {
            get {
                if(this.CostPrice.HasValue) {
                    return this.StorageAfterInventory;
                }
                return null;
            }

        }
        /// <summary>
        /// 显示库存差异
        /// </summary>
        public int? NewStorageDifference {
            get {
                if(this.CostPrice.HasValue) {
                    return this.StorageDifference;
                }
                return null;
            }

        }
    }
}
