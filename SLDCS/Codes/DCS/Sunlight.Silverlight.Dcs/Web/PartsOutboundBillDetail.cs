﻿
using System.ComponentModel;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class PartsOutboundBillDetail {

        /// <summary>
        /// 源单据编号 虚拟扩展字段
        /// 1.用于配件销售业务下， 配件销售退货提报 节点。选择 配件出库单 时，同时带回 配件销售订单编号
        /// </summary>
        public string OriginalRequirementBillCode {
            get;
            set;
        }
        /// <summary>
        /// 源单据编号 虚拟扩展字段
        /// 1.用于配件销售业务下， 配件销售退货提报 节点。选择 配件出库单 时，同时带回 订单联系人
        /// </summary>
        public string ContactPerson {
            get;
            set;
        }
        /// <summary>
        /// 源单据编号 虚拟扩展字段
        /// 1.用于配件销售业务下， 配件销售退货提报 节点。选择 配件出库单 时，同时带回 订单联系电话
        /// </summary>
        public string ContactPhone {
            get;
            set;
        }

        public string ERPSourceOrderCode {
            get;
            set;
        }
        /// <summary>
        /// 零售订单 扩展实体，将弹出框中的零售订单 通过清单带回
        /// 1.用于配件销售业务下， 配件零售退货管理 节点。选择 配件出库单 时，同时带回 配件零售订单
        /// </summary>
        public PartsRetailOrder PartsRetailOrder {
            get;
            set;
        }

        private int warehouseAreaCategory;
        /// <summary>
        /// 库区用途
        /// </summary>
        public int WarehouseAreaCategory {
            get {
                return this.warehouseAreaCategory;
            }
            set {
                this.warehouseAreaCategory = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("WarehouseAreaCategory"));
            }
        }
        private int usableQuantity;

        /// <summary>
        /// 可用库存
        /// </summary>
        public int UsableQuantity {
            get {
                return this.usableQuantity;
            }
            set {
                this.usableQuantity = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("UsableQuantity"));
            }
        }

    }
}
