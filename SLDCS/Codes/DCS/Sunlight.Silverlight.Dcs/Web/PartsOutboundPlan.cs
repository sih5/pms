﻿using System.ComponentModel;
using System.Linq;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class PartsOutboundPlan {
        /// <summary>
        /// 总金额
        /// </summary>
        private decimal totalAmount;
        public decimal TotalAmount {
            get {
                return this.PartsOutboundPlanDetails.Sum(r => r.Price * r.PlannedAmount);

            }
            set {
                this.totalAmount = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("TotalAmount"));
            }
        }
        private decimal totalCount;
        public decimal TotalCount {
            get {
                return this.PartsOutboundPlanDetails.Count();

            }
            set {
                this.totalCount = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("TotalCount"));
            }
        }
    }
}
