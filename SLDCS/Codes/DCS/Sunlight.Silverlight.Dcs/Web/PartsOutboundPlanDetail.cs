﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class PartsOutboundPlanDetail {
        private int serialNumber;
        private int currentOutboundAmount;

        [Display(Name = "_Common_SerialNumber", ResourceType = typeof(Resources.EntityStrings))]
        public int SerialNumber {
            get {
                return this.serialNumber;
            }
            set {
                this.serialNumber = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("SerialNumber"));
            }
        }

        [Display(Name = "PartsOutboundPlanDetail_CurrentOutboundAmount", ResourceType = typeof(Resources.EntityStrings))]
        public int CurrentOutboundAmount {
            get {
                return this.currentOutboundAmount;
            }
            set {
                this.currentOutboundAmount = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("CurrentOutboundAmount"));
            }
        }
    }
}
