﻿using System.ComponentModel;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class PartsOuterPurchaseChange {
        private string marketDepartmentName;
        public string MarketDepartmentName {
            get {
                return marketDepartmentName;
            }
            set {
                this.marketDepartmentName = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("MarketDepartmentName"));
            }
        }
        /// <summary>
        /// 是否上传附件
        /// </summary>
        public bool? IsUplodFile {
            get {
                if(this.Path != null && this.Path.Length > 0) {
                    return true;

                } else {
                    return false;

                }
            }
        }
    }
}
