﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class PartsPurchaseOrder {
        private string sparePartCode;
        private string sparePartName;
        private string detailSupplierPartCode;
        private string confirmedAmount;
        private int quantity;
        private string supplierShippingOrderCode;
        private DateTime shippingDate;
        private int inspectedQuantity;
        private decimal detailUnitPrice;
        private string partsInboundCheckBillCode;
        private DateTime partsInboundCheckBillCreateTime;
        private string contactPerson;
        private string contactPhone;

        /// <summary>
        /// 采购清单单价（统购分销生成采购使用）
        /// </summary>
        public decimal DetailUnitPrice {
            get {
                return detailUnitPrice;
            }
            set {
                this.detailUnitPrice = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("DetailUnitPrice"));
            }
        }
        /// <summary>
        ///联系人
        /// </summary>
        public string ContactPerson
        {
            get
            {
                return contactPerson;
            }
            set
            {
                this.contactPerson = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("ContactPerson"));
            }
        }
        /// <summary>
        /// 联系电话
        /// </summary>
        public string ContactPhone
        {
            get
            {
                return contactPhone;
            }
            set
            {
                this.contactPhone = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("ContactPhone"));
            }
        }

        /// <summary>
        /// 采购清单供应商图号（统购分销生成采购使用）
        /// </summary>
        public string DetailSupplierPartCode {
            get {
                return detailSupplierPartCode;
            }
            set {
                this.detailSupplierPartCode = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("DetailSupplierPartCode"));
            }
        }

        /// <summary>
        /// 配件编号
        /// </summary>
        [Display(Name = "PartsPurchaseOrderDetail_SparePartCode", ResourceType = typeof(Resources.EntityStrings))]
        public string SparePartCode {
            get {
                return sparePartCode;
            }
            set {
                this.sparePartCode = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("SparePartCode"));
            }
        }

        /// <summary>
        /// 配件名称
        /// </summary>
        [Display(Name = "PartsPurchaseOrderDetail_SparePartName", ResourceType = typeof(Resources.EntityStrings))]
        public string SparePartName {
            get {
                return sparePartName;
            }
            set {
                this.sparePartName = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("SparePartName"));
            }
        }

        /// <summary>
        /// 配件编号
        /// </summary>
        [Display(Name = "PartsPurchaseOrderDetail_ConfirmedAmount", ResourceType = typeof(Resources.EntityStrings))]
        public string ConfirmedAmount {
            get {
                return confirmedAmount;
            }
            set {
                this.confirmedAmount = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("ConfirmedAmount"));
            }
        }

        /// <summary>
        /// 发运数量
        /// </summary>
        [Display(Name = "SupplierShippingDetail_Quantity", ResourceType = typeof(Resources.EntityStrings))]
        public int Quantity {
            get {
                return quantity;
            }
            set {
                this.quantity = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("Quantity"));
            }
        }

        /// <summary>
        /// 发运单编号
        /// </summary>
        [Display(Name = "SupplierShippingOrder_Code", ResourceType = typeof(Resources.EntityStrings))]
        public string SupplierShippingOrderCode {
            get {
                return supplierShippingOrderCode;
            }
            set {
                this.supplierShippingOrderCode = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("Code"));
            }
        }

        /// <summary>
        /// 发运时间
        /// </summary>
        [Display(Name = "SupplierShippingOrder_ShippingDate", ResourceType = typeof(Resources.EntityStrings))]
        public DateTime ShippingDate {
            get {
                return shippingDate;
            }
            set {
                this.shippingDate = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("ShippingDate"));
            }
        }

        /// <summary>
        /// 入库数量
        /// </summary>
        [Display(Name = "PartsInboundCheckBillDetail_InspectedQuantity", ResourceType = typeof(Resources.EntityStrings))]
        public int InspectedQuantity {
            get {
                return inspectedQuantity;
            }
            set {
                this.inspectedQuantity = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("InspectedQuantity"));
            }
        }

        /// <summary>
        /// 入库单编号
        /// </summary>
        [Display(Name = "PartsInboundCheckBill_Code", ResourceType = typeof(Resources.EntityStrings))]
        public string PartsInboundCheckBillCode {
            get {
                return partsInboundCheckBillCode;
            }
            set {
                this.partsInboundCheckBillCode = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("Code"));
            }
        }

        /// <summary>
        /// 入库时间
        /// </summary>
        [Display(Name = "PartsInboundCheckBill_CreateTime", ResourceType = typeof(Resources.EntityStrings))]
        public DateTime PartsInboundCheckBillCreateTime {
            get {
                return partsInboundCheckBillCreateTime;
            }
            set {
                this.partsInboundCheckBillCreateTime = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("CreateTime"));
            }
        }
    }
}
