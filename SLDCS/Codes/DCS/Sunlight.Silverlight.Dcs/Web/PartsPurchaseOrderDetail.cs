﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class PartsPurchaseOrderDetail {
        private int pendingConfirmedAmount;
        [Display(Name = "PartsPurchaseOrderDetail_PendingConfirmedAmount", ResourceType = typeof(Resources.EntityStrings))]
        public int PendingConfirmedAmount {
            get {
                return pendingConfirmedAmount;
            }
            set {
                this.pendingConfirmedAmount = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("PendingConfirmedAmount"));
            }
        }

        private int minPackingAmount;
        public int MinPackingAmount {
            get {
                return minPackingAmount;
            }
            set {
                this.minPackingAmount = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("MinPackingAmount"));
            }
        }

        private string partsPurchaseOrderTypeName;
         public string PartsPurchaseOrderTypeName
        {
            get {
                return partsPurchaseOrderTypeName;
            }
            set {
                this.partsPurchaseOrderTypeName = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("PartsPurchaseOrderTypeName"));
            }
        }
    }
}
