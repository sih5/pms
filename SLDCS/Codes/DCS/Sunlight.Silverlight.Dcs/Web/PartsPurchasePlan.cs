﻿
using System.ComponentModel;
using System.Linq;
namespace Sunlight.Silverlight.Dcs.Web {
    public partial class PartsPurchasePlan {
        private decimal feeSum;
        private int planAmountSum;
        private bool? isUplodFile;
        //计划总数量
        public int PlanAmountSum {
            get {
                return this.PartsPurchasePlanDetails.Sum(r => r.PlanAmount);
            }
            set {
                this.planAmountSum = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("PlanAmountSum"));
            }
        }

        //计划总金额
        public decimal FeeSum {
            get {
                return this.PartsPurchasePlanDetails.Sum(r => r.PlanAmount * r.Price);
            }
            set {
                this.feeSum = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("FeeSum"));
            }
        }

        public bool? IsUplodFile {
            get {
                return isUplodFile = this.Path != null && this.Path.Length > 0;
            }
            set {
                this.isUplodFile = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("IsUplodFile"));
            }
        }
    }
}
