﻿
using System.ComponentModel;
using System.Linq;
namespace Sunlight.Silverlight.Dcs.Web {
    public partial class PartsPurchasePlanDetail {
        private int? packingCoefficient;
        /// <summary>
        /// 包装数量
        /// </summary>
        public int? PackingCoefficient {
            get {
                return packingCoefficient;
            }
            set {
                this.packingCoefficient = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("PackingCoefficient"));
            }
        }

        private int? limitQty;
        public int? LimitQty {
            get {
                return limitQty;
            }
            set {
                this.limitQty = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("LimitQty"));
            }
        }

        private int? usedQty;
        public int? UsedQty {
            get {
                return usedQty;
            }
            set {
                this.usedQty = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("UsedQty"));
            }
        }

    }
}
