﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web
{
    public partial class PartsPurchasePricingChange
    {
        private ObservableCollection<PartsPurchasePricingDetail> partsPurchasePricingDetailOrigs;
        /// <summary>
        /// 修改配件采购价变更申请时使用
        /// </summary>
        [DataMemberAttribute]
        public ObservableCollection<PartsPurchasePricingDetail> PartsPurchasePricingDetailOrigs
        {
           get {
                return this.partsPurchasePricingDetailOrigs ?? (this.partsPurchasePricingDetailOrigs = new ObservableCollection<PartsPurchasePricingDetail>());
            }
            set {
                this.partsPurchasePricingDetailOrigs = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("PartsPurchasePricingDetailOrigs"));
            }
        }

        /// <summary>
        /// 审批意见 （初审，审核，审批三个界面通用）
        /// </summary>
        private string approveMemo;
        public string ApproveMemo
        {
            get
            {
                return approveMemo;
            }
            set
            {
                this.approveMemo = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("ApproveMemo"));
            }
        }
        /// <summary>
        /// 是否上传附件
        /// </summary>
        private bool? isUplodFile;
        public bool? IsUplodFile {
            get {
                return isUplodFile = this.Path != null && this.Path.Length > 0;
            }
            set {
                this.isUplodFile = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("IsUplodFile"));
            }
        }
    }
}
