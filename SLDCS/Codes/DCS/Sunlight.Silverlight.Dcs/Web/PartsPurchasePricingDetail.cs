﻿
using System;
using System.ComponentModel;
namespace Sunlight.Silverlight.Dcs.Web {
    public partial class PartsPurchasePricingDetail {
        private decimal ratio;
        private string overseasPartsFigureQuery = string.Empty;
        private string priceRate;
        public decimal Ratio {
            get {
                return ratio;
            }
            set {
                ratio = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("Ratio"));
            }
        }
        public string OverseasPartsFigureQuery {
            get {
                return overseasPartsFigureQuery;
            }
            set {
                overseasPartsFigureQuery = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("OverseasPartsFigureQuery"));
            }
        }
        //价格浮动比率=（价格-参考价格）/参考价格*100%
        public string PriceRate {
            get {
                if(Convert.ToDecimal(this.ReferencePrice) == 0) {
                    return "0";
                } else {
                    return Math.Round((((this.Price - Convert.ToDecimal(this.ReferencePrice)) / Convert.ToDecimal(this.ReferencePrice)) * 100),2) + "%";
                    //return (Math.Round((this.Price) / Convert.ToDecimal(this.ReferencePrice),2) * 100) + "%";
                }
            }
            set {
                priceRate = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("PriceRate"));
            }
        }
        
    }
}
