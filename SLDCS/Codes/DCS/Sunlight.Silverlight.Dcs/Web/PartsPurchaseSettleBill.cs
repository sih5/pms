﻿
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class PartsPurchaseSettleBill {
        //是否可登记多发票（发票登记界面用于判断是否可登记多发票）
        private bool? mutInvoiceStrategy;
        public bool? MutInvoiceStrategy {
            get {
                return this.mutInvoiceStrategy;
            }
            set {
                this.mutInvoiceStrategy = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("MutInvoiceStrategy"));
            }
        }

        private int serialNumber;
        [Display(Name = "_Common_SerialNumber", ResourceType = typeof(Resources.EntityStrings))]
        public int SerialNumber {
            get {
                return this.serialNumber;
            }
            set {
                this.serialNumber = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("SerialNumber"));
            }
        }
    }
}
