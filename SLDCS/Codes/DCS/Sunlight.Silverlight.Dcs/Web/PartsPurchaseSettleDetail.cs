﻿using System.ComponentModel;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class PartsPurchaseSettleDetail {
        private int billType;

        /// <summary>
        /// 原始单据类型。合并清单用到
        /// </summary>
        public int BillType {
            get {
                return this.billType;
            }
            set {
                this.billType = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("BillType"));
            }
        }
    }
}
