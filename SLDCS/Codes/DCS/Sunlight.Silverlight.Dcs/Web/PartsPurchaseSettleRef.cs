﻿using System.ComponentModel;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class PartsPurchaseSettleRef {
        public string originalRequirementBillCode;

        public string OriginalRequirementBillCode {
            get {
                if(this.SourceType == (int)DcsPartsPurchaseSettleRefSourceType.配件入库检验单) {
                    if(this.PartsInboundCheckBill != null && this.PartsInboundCheckBill.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件采购订单) {
                        originalRequirementBillCode = this.PartsInboundCheckBill.OriginalRequirementBillCode;
                    }
                }
                if(this.SourceType == (int)DcsPartsPurchaseSettleRefSourceType.配件出库单) {
                    if(this.PartsOutboundBill != null && this.PartsOutboundBill.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件采购订单) {
                        originalRequirementBillCode = this.PartsOutboundBill.OriginalRequirementBillCode;
                    }
                }
                return originalRequirementBillCode;
            }
            set {
                this.originalRequirementBillCode = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("OriginalRequirementBillCode"));
            }
        }
    }
}
