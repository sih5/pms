﻿using System.ComponentModel;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class PartsReplacement {
        //配件参图号
        private string partReferenceCode;
        public string PartReferenceCode {
            get {
                return this.partReferenceCode;
            }
            set {
                this.partReferenceCode = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("PartReferenceCode"));
            }
        }
    }
}
