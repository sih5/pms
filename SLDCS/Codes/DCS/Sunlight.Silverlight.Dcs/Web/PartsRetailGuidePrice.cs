﻿
using System.ComponentModel;
namespace Sunlight.Silverlight.Dcs.Web {
    public partial class PartsRetailGuidePrice {
        private decimal ratio;

        public decimal Ratio {
            get {
                return ratio;
            }
            set {
                ratio = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("Ratio"));
            }
        }
    }
}
