﻿using System.ComponentModel;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class PartsSalesOrder {
        private string warehouseCode, centerWarehouseCode ,centerCompanyCode;
        private string warehouseName, centerWarehouseName,centerCompanyName;
        private int supplierCompanyId;
        private string supplierCompanyCode;
        private string supplierCompanyName;
        private int jsupplierCompanyId;
        private string jsupplierCompanyCode;
        private string jsupplierCompanyName;
        private int supplierWarehouseId;
        private string supplierWarehouseCode;
        private string supplierWarehouseName;
        private bool isReport;
        private bool isCreatePurchseOrder;
        private int transferredWarehouseId, centerWarehouseId ,centerCompanyId;
        private string transferredWarehouseName;
        private int processMethod;
        private int? outWarehouseId;
        private int distributionOWarehouseId;
        private string distributionOWarehouseName;
        private string distributionOWarehouseCode;
        private int distributionIWarehouseId;
        private string distributionIWarehouseName;
        private string distributionIWarehouseCode;
        private int salesUnitOwnerCompanyType;
        private int? cityid;
        private int centerSupplierId;
        private string centerSupplierCode;
        private string centerSupplierName;


        /// <summary>
        /// 供应商企业Id
        /// </summary>
        public int CenterSupplierId {
            get {
                return this.centerSupplierId;
            }
            set {
                this.centerSupplierId = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("CenterSupplierId"));
            }
        }
        /// <summary>
        /// 供应商企业编号
        /// </summary>
        public string CenterSupplierCode {
            get {
                return this.centerSupplierCode;
            }
            set {
                this.centerSupplierCode = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("CenterSupplierCode"));
            }
        }

        /// <summary>
        /// 供应商企业名称
        /// </summary>
        public string CenterSupplierName {
            get {
                return this.centerSupplierName;
            }
            set {
                this.centerSupplierName = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("CenterSupplierName"));
            }
        }

        /// <summary>
        /// 中心库企业名称
        /// </summary>
        public string CenterCompanyName {
            get {
                return this.centerCompanyName;
            }
            set {
                this.centerCompanyName = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("CenterCompanyName"));
            }
        }
        /// <summary>
        /// 中心库企业Id
        /// </summary>
        public int CenterCompanyId {
            get {
                return this.centerCompanyId;
            }
            set {
                this.centerCompanyId = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("CenterCompanyId"));
            }
        }
        /// <summary>
        /// 中心库企业编号
        /// </summary>
        public string CenterCompanyCode {
            get {
                return this.centerCompanyCode;
            }
            set {
                this.centerCompanyCode = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("CenterCompanyCode"));
            }
        }
        /// <summary>
        /// 中心库仓库名称
        /// </summary>
        public string CenterWarehouseName {
            get {
                return this.centerWarehouseName;
            }
            set {
                this.centerWarehouseName = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("CenterWarehouseName"));
            }
        }
        /// <summary>
        /// 中心库仓库Id
        /// </summary>
        public int CenterWarehouseId {
            get {
                return this.centerWarehouseId;
            }
            set {
                this.centerWarehouseId = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("CenterWarehouseId"));
            }
        }
        /// <summary>
        /// 中心库仓库编号
        /// </summary>
        public string CenterWarehouseCode {
            get {
                return this.centerWarehouseCode;
            }
            set {
                this.centerWarehouseCode = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("CenterWarehouseCode"));
            }
        }

        /// <summary>
        /// 城市Id
        /// </summary>
        public int? CityId
        {
            get
            {
                return this.cityid;
            }
            set
            {
                this.cityid = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("CityId"));
            }
        }
        /// <summary>
        /// 销售组织隶属企业类型
        /// </summary>
        public int SalesUnitOwnerCompanyType {
            get {
                return this.salesUnitOwnerCompanyType;
            }
            set {
                this.salesUnitOwnerCompanyType = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("SalesUnitOwnerCompanyType"));
            }
        }

        /// <summary>
        /// 出库仓库编号
        /// </summary>
        public string WarehouseCode {
            get {
                return this.warehouseCode;
            }
            set {
                this.warehouseCode = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("WarehouseCode"));
            }
        }


        /// <summary>
        /// 出库仓库Id
        /// </summary>
        public int? OutWarehouseId {
            get {
                return this.outWarehouseId;
            }
            set {
                this.outWarehouseId = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("OutWarehouseId"));
            }
        }

        /// <summary>
        /// 出库仓库名称
        /// </summary>
        public string WarehouseName {
            get {
                return this.warehouseName;
            }
            set {
                this.warehouseName = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("WarehouseName"));
            }
        }

        /// <summary>
        /// 统购分销出库仓库Id
        /// </summary>
        public int DistributionOWarehouseId {
            get {
                return this.distributionOWarehouseId;
            }
            set {
                this.distributionOWarehouseId = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("DistributionOWarehouseId"));
            }
        }

        public int eCommercePlatformOutWarehouseId;
        /// <summary>
        /// 电商审核出库仓库Id
        /// </summary>
        public int ECommercePlatformOutWarehouseId {
            get {
                return this.eCommercePlatformOutWarehouseId;
            }
            set {
                this.eCommercePlatformOutWarehouseId = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("ECommercePlatformOutWarehouseId"));
            }
        }



        public string eCommercePlatformOutWarehouseCode;
        /// <summary>
        /// 电商审核出库仓库
        /// </summary>
        public string ECommercePlatformOutWarehouseCode {
            get {
                return this.eCommercePlatformOutWarehouseCode;
            }
            set {
                this.eCommercePlatformOutWarehouseCode = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("ECommercePlatformOutWarehouseCode"));
            }
        }



        public string eCommercePlatformOutWarehouseName;
        /// <summary>
        /// 电商审核出库仓库
        /// </summary>
        public string ECommercePlatformOutWarehouseName {
            get {
                return this.eCommercePlatformOutWarehouseName;
            }
            set {
                this.eCommercePlatformOutWarehouseName = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("ECommercePlatformOutWarehouseName"));
            }
        }

        public string eCommercePlatformSalesCategoryName;
  
        public string ECommercePlatformSalesCategoryName {
            get {
                return this.eCommercePlatformSalesCategoryName;
            }
            set {
                this.eCommercePlatformSalesCategoryName = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("ECommercePlatformSalesCategoryName"));
            }
        }



        /// <summary>
        /// 统购分销出库仓库编号
        /// </summary>
        public string DistributionOWarehouseCode {
            get {
                return this.distributionOWarehouseCode;
            }
            set {
                this.distributionOWarehouseCode = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("DistributionOWarehouseCode"));
            }
        }

        /// <summary>
        ///统购分销出库仓库名称
        /// </summary>
        public string DistributionOWarehouseName {
            get {
                return this.distributionOWarehouseName;
            }
            set {
                this.distributionOWarehouseName = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("DistributionOWarehouseName"));
            }
        }

        /// <summary>
        /// 统购分销本品牌入库仓库id
        /// </summary>
        public int DistributionIWarehouseId {
            get {
                return this.distributionIWarehouseId;
            }
            set {
                this.distributionIWarehouseId = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("DistributionIWarehouseId"));
            }
        }

        /// <summary>
        /// 统购分销本品牌入库仓库编号
        /// </summary>
        public string DistributionIWarehouseCode {
            get {
                return this.distributionIWarehouseCode;
            }
            set {
                this.distributionIWarehouseCode = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("SupplierCompanyCode"));
            }
        }

        /// <summary>
        /// 统购分销本品牌入库仓库名称
        /// </summary>
        public string DistributionIWarehouseName {
            get {
                return this.distributionIWarehouseName;
            }
            set {
                this.distributionIWarehouseName = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("DistributionIWarehouseName"));
            }
        }

        /// <summary>
        /// 是否生成配件采购订单
        /// </summary>
        public bool IsCreatePurchseOrder {
            get {
                return this.isCreatePurchseOrder;
            }
            set {
                this.isCreatePurchseOrder = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("IsCreatePurchseOrder"));
            }
        }

        /// <summary>
        /// 供货单位Id
        /// </summary>
        public int SupplierCompanyId {
            get {
                return this.supplierCompanyId;
            }
            set {
                this.supplierCompanyId = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("SupplierCompanyId"));
            }
        }

        /// <summary>
        /// 供货单位编号
        /// </summary>
        public string SupplierCompanyCode {
            get {
                return this.supplierCompanyCode;
            }
            set {
                this.supplierCompanyCode = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("SupplierCompanyCode"));
            }
        }


        /// <summary>
        /// 供货单位名称
        /// </summary>
        public string SupplierCompanyName {
            get {
                return this.supplierCompanyName;
            }
            set {
                this.supplierCompanyName = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("SupplierCompanyName"));
            }
        }


        /// <summary>
        /// 积压件供货单位Id
        /// </summary>
        public int JSupplierCompanyId {
            get {
                return this.jsupplierCompanyId;
            }
            set {
                this.jsupplierCompanyId = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("JSupplierCompanyId"));
            }
        }

        /// <summary>
        /// 积压件供货单位编号
        /// </summary>
        public string JSupplierCompanyCode {
            get {
                return this.jsupplierCompanyCode;
            }
            set {
                this.jsupplierCompanyCode = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("JSupplierCompanyCode"));
            }
        }


        /// <summary>
        /// 积压件供货单位名称
        /// </summary>
        public string JSupplierCompanyName {
            get {
                return this.jsupplierCompanyName;
            }
            set {
                this.jsupplierCompanyName = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("JSupplierCompanyName"));
            }
        }
        /// <summary>
        /// 供货单位仓库Id
        /// </summary>
        public int SupplierWarehouseId {
            get {
                return this.supplierWarehouseId;
            }
            set {
                this.supplierWarehouseId = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("SupplierWarehouseId"));
            }
        }

        /// <summary>
        /// 供货单位仓库编号
        /// </summary>
        public string SupplierWarehouseCode {
            get {
                return this.supplierWarehouseCode;
            }
            set {
                this.supplierWarehouseCode = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("SupplierWarehouseCode"));
            }
        }

        /// <summary>
        /// 供货单位仓库名称
        /// </summary>
        public string SupplierWarehouseName {
            get {
                return this.supplierWarehouseName;
            }
            set {
                this.supplierWarehouseName = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("SupplierWarehouseName"));
            }
        }

        /// <summary>
        /// 调入仓库Id
        /// </summary>
        public int TransferredWarehouseId {
            get {
                return this.transferredWarehouseId;
            }
            set {
                this.transferredWarehouseId = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("TransferredWarehouseId"));
            }
        }

        /// <summary>
        /// 调入仓库名称
        /// </summary>
        public string TransferredWarehouseName {
            get {
                return this.transferredWarehouseName;
            }
            set {
                this.transferredWarehouseName = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("TransferredWarehouseName"));
            }
        }

        public int ProcessMethod {
            get {
                return this.processMethod;
            }
            set {
                this.processMethod = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("ProcessMethod"));
            }
        }
        /// <summary>
        /// 是否向代理库提报
        /// </summary>
        public bool IsReport {
            get {
                return this.isReport;
            }
            set {
                this.isReport = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("IsReport"));
            }
        }

        /// <summary>
        /// 收货仓库编号
        /// </summary>
        public string ReceivingWarehouseCode {
            get {
                return this.Warehouse != null ? Warehouse.Code : "";
            }
        }

        private int insideSupplyingId;
        /// <summary>
        /// 内部直供供应商Id
        /// </summary>
        public int InsideSupplyingId {
            get {
                return this.insideSupplyingId;
            }
            set {
                this.insideSupplyingId = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("InsideSupplyingId"));
            }
        }
    }
}
