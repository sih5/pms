﻿using System.ComponentModel;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class PartsSalesOrderDetail {
        //private int unfulfilledQuantity;
        private int currentFulfilledQuantity ;
        private int? minSaleQuantity;
        private int stockQuantity;
        private int supplierWarehouseId;
        private string supplierWarehouseCode;
        private string supplierWarehouseName;

        /// <summary>
        /// 未满足数量
        /// </summary>
        //public int UnfulfilledQuantity {
        //    get {
        //        return unfulfilledQuantity;
        //    }
        //    set {
        //        this.unfulfilledQuantity = value;
        //        this.OnPropertyChanged(new PropertyChangedEventArgs("UnfulfilledQuantity"));
        //    }
        //}

        /// <summary>
        /// 本次满足数量
        /// </summary>
        public int CurrentFulfilledQuantity {
            get {
                return currentFulfilledQuantity;
            }
            set {
                this.currentFulfilledQuantity = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("CurrentFulfilledQuantity"));
            }
        }

        /// <summary>
        /// 库存数量
        /// </summary>
        public int StockQuantity {
            get {
                return stockQuantity;
            }
            set {
                this.stockQuantity = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("StockQuantity"));
            }
        }

        /// <summary>
        /// 货源仓库ID
        /// </summary>
        public int SupplierWarehouseId {
            get {
                return supplierWarehouseId;
            }
            set {
                this.supplierWarehouseId = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("SupplierWarehouseId"));
            }
        }

        /// <summary>
        /// 货源仓库编号
        /// </summary>
        public string SupplierWarehouseCode {
            get {
                return this.supplierWarehouseCode;
            }
            set {
                this.supplierWarehouseCode = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("SupplierWarehouseCode"));
            }
        }

        /// <summary>
        /// 货源仓库
        /// </summary>
        public string SupplierWarehouseName {
            get {
                return supplierWarehouseName;
            }
            set {
                this.supplierWarehouseName = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("SupplierWarehouseName"));
            }
        }

        /// <summary>
        /// 保底库存
        /// </summary>
        public int? MinSaleQuantity {
            get {
                return minSaleQuantity;
            }
            set {
                this.minSaleQuantity = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("MinSaleQuantity"));
            }
        }
        
    }
}
