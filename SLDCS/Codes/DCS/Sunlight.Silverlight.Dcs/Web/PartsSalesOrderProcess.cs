﻿using System.ComponentModel;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class PartsSalesOrderProcess {
        private int warehouseId;
        private int partsSalesCategoryId;
        private bool isTransSap;

        public int PartsSalesCategoryId {
            get {
                return this.partsSalesCategoryId;
            }
            set {
                this.partsSalesCategoryId = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("PartsSalesCategoryId"));
            }
        }


        public int WarehouseId {
            get {
                return this.warehouseId;
            }
            set {
                this.warehouseId = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("WarehouseId"));
            }
        }

        public bool IsTransSap {
            get {
                return this.isTransSap;
            }
            set {
                this.isTransSap = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("IsTransSap"));
            }
        }
    }
}
