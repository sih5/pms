﻿
namespace Sunlight.Silverlight.Dcs.Web {
    public partial class PartsSalesOrderProcessDetail {
        /// <summary>
        /// 仓储企业ID
        /// </summary>
        public int? StorageCompanyId {
            get;
            set;
        }

        /// <summary>
        /// 未满足数量
        /// </summary>
        public int UnfulfilledQuantity {
            get;
            set;
        }

        /// <summary>
        /// 厂商
        /// </summary>
        public string Factury {
            get;
            set;
        }
    }
}
