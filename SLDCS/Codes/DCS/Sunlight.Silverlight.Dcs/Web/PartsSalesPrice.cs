﻿using System.ComponentModel;
using System.Linq;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class PartsSalesPrice {
        /// <summary>
        /// 零售指导价
        /// </summary>
        private decimal retailGuidePrice;
        public decimal RetailGuidePrice {
            get {
                return this.SparePart != null && this.SparePart.PartsRetailGuidePrices.Any() ? this.SparePart.PartsRetailGuidePrices.First().RetailGuidePrice : 0;
            }
            set {
                this.retailGuidePrice = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("RetailGuidePrice"));
            }
        }
    }
}
