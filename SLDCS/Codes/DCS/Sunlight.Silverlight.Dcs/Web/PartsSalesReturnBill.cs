﻿
using System.ComponentModel;
using System.Linq;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class PartsSalesReturnBill {
        private int? partsSalesOrderId;
        private string partsSalesOrderCode;
        //记录选中销售组织下的配件销售类型Id
        public int PartsSalesCategoryId {
            get;
            set;
        }
        /// <summary>
        /// 退货总金额
        /// </summary>
        public decimal ApproveAmount {
            get {
                if(this.ReturnType != (int)DcsPartsSalesReturn_ReturnType.特殊退货 && this.DiscountRate != decimal.Zero) {
                    return this.PartsSalesReturnBillDetails.Sum(r => (r.ApproveQuantity ?? 0) * (r.ReturnPrice * (1 - this.DiscountRate.Value)));

                } else {
                    return this.PartsSalesReturnBillDetails.Sum(r => (r.ApproveQuantity ?? 0) * r.ReturnPrice);

                }
            }
        }
        /// <summary>
        /// 是否上传附件
        /// </summary>
        public bool? IsUplodFile {
            get {
                if(this.Path != null && this.Path.Length > 0) {
                    return true;

                } else {
                    return false;

                }
            }
        }
        /// <summary>
        /// 配件销售订单Id
        /// </summary>
        public int? PartsSalesOrderId {
            get {
                return this.partsSalesOrderId;
            }
            set {
                this.partsSalesOrderId = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("PartsSalesOrderId"));
            }
        }

        /// <summary>
        /// 配件销售订单编号
        /// </summary>
        public string PartsSalesOrderCode {
            get {
                return this.partsSalesOrderCode;
            }
            set {
                this.partsSalesOrderCode = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("PartsSalesOrderCode"));
            }
        }
    }
}
