﻿using System.ComponentModel;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class PartsSalesSettlement {
        private decimal rebateBalance;

        // 返利余额
        public decimal RebateBalance {
            get {
                return this.rebateBalance;
            }
            set {
                this.rebateBalance = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("RebateBalance"));
            }
        }
        private int? isOil;

        // 是否油品订单
        public int? IsOil {
            get {
                return this.isOil;
            }
            set {
                this.isOil = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("IsOil"));
            }
        }
    }
}
