﻿using System.ComponentModel;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class PartsSalesSettlementDetail {
        private decimal originalPrice;

        // 原始单据价格
        public decimal OriginalPrice {
            get {
                return this.originalPrice;
            }
            set {
                this.originalPrice = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("OriginalPrice"));
            }
        }
    }
}
