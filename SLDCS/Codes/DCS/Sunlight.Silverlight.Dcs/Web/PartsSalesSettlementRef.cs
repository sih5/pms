﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class PartsSalesSettlementRef {
        private int serialNumber;

        [Display(Name = "_Common_SerialNumber", ResourceType = typeof(Resources.EntityStrings))]
        public int SerialNumber {
            get {
                return this.serialNumber;
            }
            set {
                this.serialNumber = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("SerialNumber"));
            }
        }

        private string partsSalesOrderTypeName;

        public string PartsSalesOrderTypeName {
            get {
                return this.partsSalesOrderTypeName;
            }
            set {
                this.partsSalesOrderTypeName = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("PartsSalesOrderTypeName"));
            }
        }
    }
}
