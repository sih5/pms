﻿
using System;
using System.ComponentModel;
using System.Linq;
namespace Sunlight.Silverlight.Dcs.Web {
    public partial class PartsShiftOrder {
        private bool? isUp;
        private bool? isDown;
        private bool? isUplodFile;
        private DateTime? closeTime;
        public bool? IsUp {
            get {
                return this.PartsShiftOrderDetails.Any(t=>t.DownShelfQty > t.UpShelfQty || (t.DownShelfQty > 0 && (t.UpShelfQty == null || t.UpShelfQty==0)));
            }
            set {
                this.isUp = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("IsUp"));
            }
        }
        public bool? IsDown {
            get {
                return this.PartsShiftOrderDetails.Any(t => t.Quantity > t.DownShelfQty || (t.DownShelfQty == null || t.DownShelfQty == 0));
            }
            set {
                this.isDown = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("IsDown"));
            }
        }
        public bool? IsUplodFile {
            get {
                return isUplodFile = this.Path != null && this.Path.Length > 0;
            }
            set {
                this.isUplodFile = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("IsUplodFile"));
            }
        }
        public DateTime? CloseTime {
            get {
                if(this.Status == (int)DcsPartsShiftOrderStatus.终止) {
                    return closeTime = this.ModifyTime;
                } else return closeTime = null;

            }
            set {
                this.closeTime = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("DateTime"));
            }
        }
    }
}
