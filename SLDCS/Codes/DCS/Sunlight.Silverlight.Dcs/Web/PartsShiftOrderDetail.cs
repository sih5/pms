﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class PartsShiftOrderDetail {

        private int shelvesAmount;
        private int sourceStockAmount;
        private string measureUnit;
        [Display(Name = "OverstockPartsAdjustDetail_ShelvesAmount", ResourceType = typeof(Resources.EntityStrings))]
        public int ShelvesAmount {
            get {
                return this.shelvesAmount;
            }
            set {
                this.shelvesAmount = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("ShelvesAmount"));
            }
        }

        [Display(Name = "OverstockPartsAdjustDetail_SourceStockAmount", ResourceType = typeof(Resources.EntityStrings))]
        public int SourceStockAmount {
            get {
                return this.sourceStockAmount;
            }
            set {
                this.sourceStockAmount = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("SourceStockAmount"));
            }
        }
        public string MeasureUnit {
            get {
                return this.measureUnit;
            }
            set {
                this.measureUnit = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("MeasureUnit"));
            }
        }
    }
}
