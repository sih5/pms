﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class PartsShippingOrder {
        /// <summary>
        /// 总金额
        /// </summary>
        private decimal totalAmount;
        private string erpSourceOrderCodeQuery;
        private string containerNumber;
        public decimal TotalAmount {
            get {
                return this.PartsShippingOrderDetails.Sum(r => r.SettlementPrice * r.ShippingAmount);

            }
            set {
                this.totalAmount = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("TotalAmount"));
            }
        }

        public string ClosedLoop {
            get {
                if(this.Status == (int)DcsPartsShippingOrderStatus.回执确认 || this.Status == (int)DcsPartsShippingOrderStatus.收货确认 || this.Status == (int)DcsPartsShippingOrderStatus.作废) {
                    return "闭环";
                }
                return "未闭环";
            }
        }

        public string ERPSourceOrderCodeQuery {
            get {
                return this.erpSourceOrderCodeQuery;
            }
            set {
                this.erpSourceOrderCodeQuery = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("ERPSourceOrderCodeQuery"));
            }
        }

        public string ContainerNumber {
            get {
                return this.PartsShippingOrderDetails.Select(r => r.ContainerNumber).FirstOrDefault();
            }
            set {
                this.containerNumber = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("ContainerNumber"));
            }
        }

        private ObservableCollection<BoxUpTaskOrPicking> boxUpTaskOrPickings;

        public ObservableCollection<BoxUpTaskOrPicking> BoxUpTaskOrPickings {
            get {
                return this.boxUpTaskOrPickings ?? (this.boxUpTaskOrPickings = new ObservableCollection<BoxUpTaskOrPicking>());
            }
            set {
                this.boxUpTaskOrPickings = value;
            }
        }
    }
}
