﻿
namespace Sunlight.Silverlight.Dcs.Web {
    public partial class PartsTransferOrder {
        public int DestPartsSalesCategoryId {
            get;
            set;
        }

        public int OriginPartsSalesCategoryId {
            get;
            set;
        }
    }
}
