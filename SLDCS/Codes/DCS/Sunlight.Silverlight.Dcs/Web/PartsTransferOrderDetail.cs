﻿using System.ComponentModel;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class PartsTransferOrderDetail {
        private int outboundAmount;
        private int inboundAmount;
        private int shippingAmount;
        private int serialNumber;
        private int originWarehouseStock;
        private int destinWarehouseStock;
        /// <summary>
        /// 清单计划金额=计划量*计划价(新建单据)||（审批状态的单据）确认量*计划价
        /// </summary>
        public decimal? DetailPlanAmount {
            get {
                return this.PartsTransferOrder.Status == (int)DcsPartsTransferOrderStatus.新建 ? this.PlannPrice * this.PlannedAmount : this.PlannPrice * this.ConfirmedAmount;
            }
        }


        public int SerialNumber {
            get {
                return this.serialNumber;
            }
            set {
                this.serialNumber = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("SerialNumber"));
            }
        }
        /// <summary>
        /// 源仓库计划价
        /// </summary>
        public decimal Oprice {
            get;
            set;
        }
        /// <summary>
        /// 目标仓库计划价
        /// </summary>
        public decimal Dprice {
            get;
            set;
        }

        public int OutboundAmount {
            get {
                return this.outboundAmount;
            }
            set {
                this.outboundAmount = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("OutboundAmount"));
            }
        }

        public int InboundAmount {
            get {
                return this.inboundAmount;
            }
            set {
                this.inboundAmount = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("InboundAmount"));
            }
        }

        public int ShippingAmount {
            get {
                return this.shippingAmount;
            }
            set {
                this.shippingAmount = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("ShippingAmount"));
            }
        }

        public int OriginWarehouseStock {
            get {
                return this.originWarehouseStock;
            }
            set {
                this.originWarehouseStock = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("OriginWarehouseStock"));
            }
        }

        public int DestinWarehouseStock {
            get {
                return this.destinWarehouseStock;
            }
            set {
                this.destinWarehouseStock = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("DestinWarehouseStock"));
            }
        }

        public int? MaxPlannedAmount {
            get;
            set;
        }

        public int? PartsSalesCategoryId {
            get;
            set;
        }
    }
}
