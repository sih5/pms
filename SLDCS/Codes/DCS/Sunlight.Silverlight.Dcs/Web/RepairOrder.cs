﻿using System;
using System.ComponentModel;
using System.Linq;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class RepairOrder {
        //private decimal? rightsCost;
        //public decimal? RightsCost {
        //    get {
        //        this.rightsCost = 0;
        //        if(this.IntegralClaimBills.Any()) {
        //            if(this.IntegralClaimBills.FirstOrDefault().IntegralClaimBillDetails.Any()) {
        //                foreach(var item in this.IntegralClaimBills.FirstOrDefault().IntegralClaimBillDetails) {
        //                    if(item.RightsItem == "免费救援" && item.IsRights == true) {
        //                        this.rightsCost= this.IntegralClaimBills.FirstOrDefault().FieldTotalAmount;
        //                    } 
        //                }
        //            } 
        //        }
        //        return this.rightsCost;
        //    }
        //    set {
        //        this.rightsCost = value;
        //        this.OnPropertyChanged(new PropertyChangedEventArgs("RightsCost"));
        //    }
        //}

        private decimal? deductionTotalAmount;

        public decimal? DeductionTotalAmount {
            get
            {
                this.deductionTotalAmount = 0;// this.TotalAmount;
                //if (this.IntegralClaimBills.Any())
                //{
                //    var memClaimBill = this.IntegralClaimBills.Where(r => !string.IsNullOrEmpty(r.MemberCode)).FirstOrDefault();
                //    if (memClaimBill != null)
                //        this.deductionTotalAmount = memClaimBill.DeductionTotalAmount;
                //    var redClaimBill = this.IntegralClaimBills.Where(r => !string.IsNullOrEmpty(r.RedPacketsCode)).FirstOrDefault();
                //    if (redClaimBill != null)
                //        this.deductionTotalAmount = redClaimBill.DeductionTotalAmount;
                //}
                return this.deductionTotalAmount;
            }
            set
            {
                this.deductionTotalAmount = value;
                //this.OnPropertyChanged(new PropertyChangedEventArgs("DeductionTotalAmount"));
            }
        }

        private bool? isNoOrder;
        public bool? IsNoOrder {
            get
            {
                //if (this.RepairWorkOrder == "无派工单")
                //    this.isNoOrder = true;
                //else
                //    this.isNoOrder = false;
                return this.isNoOrder;
            }
            set
            {
                this.isNoOrder = value;
                //this.OnPropertyChanged(new PropertyChangedEventArgs("IsNoOrder"));
            }
        }

        private decimal? memberUsedPrice;
        public decimal? MemberUsedPrice {
            get {
                this.memberUsedPrice = 0;
                //if(this.IntegralClaimBills.Any()) {
                //    if(this.IntegralClaimBills.Any()) {
                //        var memClaimBill = this.IntegralClaimBills.Where(r => !string.IsNullOrEmpty(r.MemberCode)).FirstOrDefault();
                //        if(memClaimBill != null)
                //            this.memberUsedPrice = memClaimBill.MemberUsedPrice;
                //    }
                //}
                return this.memberUsedPrice;
            }
            set {
                this.memberUsedPrice = value;
                //this.OnPropertyChanged(new PropertyChangedEventArgs("MemberUsedPrice"));
            }
        }

        private decimal? costDiscount;
        public decimal? CostDiscount {
            get {
                this.costDiscount = 0;
                //if(this.IntegralClaimBills.Any()) {
                //    if(this.IntegralClaimBills.Any()) {
                //        var memClaimBill = this.IntegralClaimBills.Where(r => !string.IsNullOrEmpty(r.MemberCode)).FirstOrDefault();
                //        if(memClaimBill != null)
                //            this.costDiscount = memClaimBill.CostDiscount;
                //    }
                //}
                return this.costDiscount;
            }
            set {
                this.costDiscount = value;
                //this.OnPropertyChanged(new PropertyChangedEventArgs("CostDiscount"));
            }
        }

        private decimal? memberRightsCost;
        public decimal? MemberRightsCost {
            get {
                this.memberRightsCost = 0;
                //if(this.IntegralClaimBills.Any()) {
                //    if(this.IntegralClaimBills.Any()) {
                //        var memClaimBill = this.IntegralClaimBills.Where(r => !string.IsNullOrEmpty(r.MemberCode)).FirstOrDefault();
                //        if(memClaimBill != null)
                //            this.memberRightsCost = memClaimBill.MemberRightsCost;
                //    }
                //}
                return this.memberRightsCost;
            }
            set {
                this.memberRightsCost = value;
                //this.OnPropertyChanged(new PropertyChangedEventArgs("MemberRightsCost"));
            }
        }
        private decimal? redPacketsUsedPrice;
        public decimal? RedPacketsUsedPrice {
            get {
                this.redPacketsUsedPrice = 0;
                //if(this.IntegralClaimBills.Any()) {
                //    if(this.IntegralClaimBills.Any()) {
                //        var memClaimBill = this.IntegralClaimBills.Where(r => !string.IsNullOrEmpty(r.RedPacketsCode)).FirstOrDefault();
                //        if(memClaimBill != null)
                //            this.redPacketsUsedPrice = memClaimBill.RedPacketsUsedPrice;
                //    }
                //}
                return this.redPacketsUsedPrice;
            }
            set {
                this.redPacketsUsedPrice = value;
                //this.OnPropertyChanged(new PropertyChangedEventArgs("RedPacketsUsedPrice"));
            }
        }

        private decimal? discount;
        public decimal? Discount {
            get {
                this.discount = 0;
                //if(this.IntegralClaimBills.Any()) {
                //    if(this.IntegralClaimBills.Any()) {
                //        var memClaimBill = this.IntegralClaimBills.Where(r => !string.IsNullOrEmpty(r.MemberCode)).FirstOrDefault();
                //        if(memClaimBill != null)
                //            this.discount = memClaimBill.Discount;
                //    }
                //}
                return this.discount;
            }
            set {
                this.discount = value;
                //this.OnPropertyChanged(new PropertyChangedEventArgs("Discount"));
            }
        }


        private decimal? usedPrice;
        public decimal? UsedPrice {
            get {
                this.usedPrice = 0;
                //if(this.IntegralClaimBills.Any()) {
                //    if(this.IntegralClaimBills.Any()) {
                //        var memClaimBill = this.IntegralClaimBills.Where(r => !string.IsNullOrEmpty(r.MemberCode)).FirstOrDefault();
                //        if(memClaimBill != null)
                //            this.usedPrice = memClaimBill.MemberUsedPrice;
                //    }
                //}
                return this.usedPrice;
            }
            set {
                this.usedPrice = value;
                //this.OnPropertyChanged(new PropertyChangedEventArgs("UsedPrice"));
            }
        }
        private DateTime? repairClaimBillCreateTime1;
        public DateTime? RepairClaimBillCreateTime1 {
            get {
                this.repairClaimBillCreateTime1 = null;
                //if(this.RepairClaimBill.Any()) {
                //    this.repairClaimBillCreateTime1 = this.RepairClaimBill.FirstOrDefault().CreateTime;
                //}
                return this.repairClaimBillCreateTime1;
            }
        }

        private int? customerIdentity;
        public int? CustomerIdentity {
            get {
                return this.customerIdentity;
            }
            set {
                this.customerIdentity = value;
                //this.OnPropertyChanged(new PropertyChangedEventArgs("CustomerIdentity"));
            }
        }

        private int? customerNature;
        public int? CustomerNature {
            get {
                return this.customerNature;
            }
            set {
                this.customerNature = value;
                //this.OnPropertyChanged(new PropertyChangedEventArgs("CustomerNature"));
            }

        }

        //private Decimal? outFeeGrade;
        //public Decimal? OutFeeGrade {
        //    get {
        //        this.outFeeGrade = this.TotalAmount;
        //        if(this.IntegralClaimBills.Any()) {
        //            this.outFeeGrade = this.IntegralClaimBills.FirstOrDefault().OutFeeGrade;
        //        }
        //        return this.outFeeGrade;
        //    }
        //    set {
        //        this.outFeeGrade = value;
        //        this.OnPropertyChanged(new PropertyChangedEventArgs("OutFeeGrade"));
        //    }
        //}

    }
}
