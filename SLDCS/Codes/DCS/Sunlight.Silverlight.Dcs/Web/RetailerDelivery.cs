﻿using System;
using System.Linq;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class Retailer_Delivery {
        public int? SyncStatus {
            get {
                return this.Retailer_DeliveryDetail.Select(r => r.SyncStatus).FirstOrDefault();
            }
        }
        public DateTime? SyncTime {
            get {
                return this.Retailer_DeliveryDetail.Select(r => r.SyncTime).FirstOrDefault();
            }
        }
    }
}
