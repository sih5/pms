﻿using System.ComponentModel;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class RetainedCustomerVehicleList {
        private string cityName;
        private string provinceName;
        private string custorName;

        public string CityName {
            get {
                return this.cityName;
            }
            set {
                this.cityName = value;
                //this.OnPropertyChanged(new PropertyChangedEventArgs("CityName"));
            }
        }

        public string ProvinceName {
            get {
                return this.provinceName;
            }
            set {
                this.provinceName = value;
                //this.OnPropertyChanged(new PropertyChangedEventArgs("ProvinceName"));
            }
        }

        public string CustorName {
            get {
                return this.custorName;
            }
            set {
                this.custorName = value;
                //this.OnPropertyChanged(new PropertyChangedEventArgs("custorName"));
            }
        }
    }
}
