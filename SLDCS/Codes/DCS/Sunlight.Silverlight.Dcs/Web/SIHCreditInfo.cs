﻿using System;
using System.ComponentModel;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class SIHCreditInfo {
        private int? marketingDepartmentId;
        public int? MarketingDepartmentId {
            get {
                return this.marketingDepartmentId;
            }
            set {
                this.marketingDepartmentId = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("MarketingDepartmentId"));
            }
        }
    }
}
