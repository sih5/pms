﻿using System;
using System.ComponentModel;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class SIHCreditInfoDetail {
        private DateTime? validationTimeCheck;
        public DateTime? ValidationTimeCheck {
            get {
                return this.validationTimeCheck;
            }
            set {
                this.validationTimeCheck = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("ValidationTimeCheck"));
            }
        }

        private DateTime? expireTimeCheck;
        public DateTime? ExpireTimeCheck {
            get {
                return this.expireTimeCheck;
            }
            set {
                this.expireTimeCheck = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("ExpireTimeCheck"));
            }
        }
        
    }
}
