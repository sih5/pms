
using System.ComponentModel;
namespace Sunlight.Silverlight.Dcs.Web {
    public partial class SecondClassStationPlanDetail {
        private int sequeueNumber;
        public int SequeueNumber {
            get {
                return sequeueNumber;
            }
            set {
                sequeueNumber = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("SequeueNumber"));
            }
        }

        private int dealerPartsStockQuantity;
        public int DealerPartsStockQuantity {
            get {
                return this.dealerPartsStockQuantity;
            }
            set {
                this.dealerPartsStockQuantity = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("DealerPartsStockQuantity"));
            }
        }
    }
}
