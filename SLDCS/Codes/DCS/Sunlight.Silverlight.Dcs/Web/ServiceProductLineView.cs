﻿using System.ComponentModel;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class ServiceProductLineView {
        private int id;
        public int Id {
            get {
                return this.id;
            }
            set {
                this.id = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("Id"));
            }
        }
    }
}
