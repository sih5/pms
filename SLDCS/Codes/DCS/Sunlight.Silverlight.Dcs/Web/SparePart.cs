﻿using System.Linq;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class SparePart {

        public PartsBranch PartsBranch {
            get {
                return this.PartsBranches.FirstOrDefault();
            }
        }

        public PartsSupplierRelation PartsSupplierRelation {
            get {
                return this.PartsSupplierRelations.FirstOrDefault();
            }
        }

        public PartsPurchasePricingDetail PartsPurchasePricingDetail {
            get {
                return this.PartsPurchasePricingDetails.FirstOrDefault();
            }
        }

        public PartsPurchasePricing PartsPurchasePricing {
            get {
                return this.PartsPurchasePricings.FirstOrDefault();
            }
        }

        public PartsSalesPrice PartsSalesPrice {
            get {
                return this.PartsSalesPrices.FirstOrDefault();
            }
        }
    }
}
