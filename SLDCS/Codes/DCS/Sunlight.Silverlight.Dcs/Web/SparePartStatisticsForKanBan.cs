﻿using System.ServiceModel.DomainServices.Client;

namespace Sunlight.Silverlight.Dcs.Web {

    /// <summary>
    /// SparePart Statistics VO
    /// </summary>
    public class SparePartStatisticsForKanBan : Entity {
        private int sparePartId;
        private string sparePartCode;
        private string sparePartName;
        private int partsTransferAmount;
        private int partsOutboundAmount;
        private int partsShippingAmount;
        private int partsInboundAmount;
        private int partsAdjustAmount;

        public int SparePartId {
            get {
                return this.sparePartId;
            }
            set {
                this.sparePartId = value;
                this.RaisePropertyChanged("SparePartId");
            }
        }

        public string SparePartCode {
            get {
                return this.sparePartCode;
            }
            set {
                this.sparePartCode = value;
                this.RaisePropertyChanged("SparePartCode");
            }
        }

        public string SparePartName {
            get {
                return this.sparePartName;
            }
            set {
                this.sparePartName = value;
                this.RaisePropertyChanged("SparePartName");
            }
        }

        public int PartsTransferAmount {
            get {
                return this.partsTransferAmount;
            }
            set {
                this.partsTransferAmount = value;
                this.RaisePropertyChanged("PartsTransferAmount");
            }
        }

        public int PartsOutboundAmount {
            get {
                return this.partsOutboundAmount;
            }
            set {
                this.partsOutboundAmount = value;
                this.RaisePropertyChanged("PartsOutboundAmount");
            }
        }

        public int PartsShippingAmount {
            get {
                return this.partsShippingAmount;
            }
            set {
                this.partsShippingAmount = value;
                this.RaisePropertyChanged("PartsShippingAmount");
            }
        }

        public int PartsInboundAmount {
            get {
                return this.partsInboundAmount;
            }
            set {
                this.partsInboundAmount = value;
                this.RaisePropertyChanged("PartsInboundAmount");
            }
        }

        public int PartsAdjustAmount {
            get {
                return this.partsAdjustAmount;
            }
            set {
                this.partsAdjustAmount = value;
                this.RaisePropertyChanged("PartsAdjustAmount");
            }
        }
    }
}
