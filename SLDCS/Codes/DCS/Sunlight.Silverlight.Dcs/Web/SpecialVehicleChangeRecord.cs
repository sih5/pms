﻿

using System.ComponentModel;
namespace Sunlight.Silverlight.Dcs.Web {
    public partial class SpecialVehicleChangeRecord {
        private string productCode;
        private string productCategoryCode;
        /// <summary>
        /// 产品编号
        /// </summary>
        public string ProductCode {
            get {
                return this.productCode;
            }
            set {
                this.productCode = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("ProductCode"));
            }
        }

        /// <summary>
        /// 车型编号
        /// </summary>
        public string ProductCategoryCode {
            get {
                return this.productCategoryCode;
            }
            set {
                this.productCategoryCode = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("ProductCategoryCode"));
            }
        }
    }
}
