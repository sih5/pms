﻿
using System.ComponentModel;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class SubDealer {
        private string dealerCode;
        private string dealerName;
        /// <summary>
        /// 服务站编号
        /// </summary>
        public string DealerCode {
            get {
                return this.dealerCode;
            }
            set {
                this.dealerCode = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("DealerCode"));
            }
        }

        /// <summary>
        /// 服务站名称
        /// </summary>
        public string DealerName {
            get {
                return this.dealerName;
            }
            set {
                this.dealerName = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("DealerName"));
            }
        }

    }
}
