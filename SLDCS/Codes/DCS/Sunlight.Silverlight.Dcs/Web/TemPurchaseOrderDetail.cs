﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class TemPurchaseOrderDetail {
        private int pendingConfirmedAmount;
        [Display(Name = "PartsPurchaseOrderDetail_PendingConfirmedAmount", ResourceType = typeof(Resources.EntityStrings))]
        public int PendingConfirmedAmount {
            get {
                return pendingConfirmedAmount;
            }
            set {
                this.pendingConfirmedAmount = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("PendingConfirmedAmount"));
            }
        }
    }
}
