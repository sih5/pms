﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class TemShippingOrderDetail {

        private int pendingQuantity;

        [Display(Name = "TemShippingOrderDetail_PendingQuantity", ResourceType = typeof(Resources.EntityStrings))]
        public int PendingQuantity {
            get {
                return this.pendingQuantity;
            }
            set {
                this.pendingQuantity = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("PendingQuantity"));
            }
        }
    }
}
