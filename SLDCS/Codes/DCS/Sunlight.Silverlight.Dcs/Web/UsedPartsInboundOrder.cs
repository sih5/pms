﻿using System.Linq;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class UsedPartsInboundOrder {
        /// <summary>
        /// 旧件激励金额
        /// </summary>
        public decimal UsedPartsEncourageAmount {
            get {
                return this.UsedPartsInboundDetails.Sum(r => (r.UsedPartsEncourageAmount ?? 0));
            }
        }
    }
}
