﻿using System.ComponentModel;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class UsedPartsLogisticLossBill {
        /// <summary>
        /// 物流损失总金额
        /// </summary>
        private decimal logisticLossTotalAmount;
        public decimal LogisticLossTotalAmount {
            get {
                return this.logisticLossTotalAmount;
            }
            set {
                this.logisticLossTotalAmount = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("LogisticLossTotalAmount"));
            }
        }
    }
}
