﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class UsedPartsOutboundDetail {
        private int serialNumber;

        [Display(Name = "_Common_SerialNumber", ResourceType = typeof(Resources.EntityStrings))]
        public int SerialNumber {
            get {
                return this.serialNumber;
            }
            set {
                this.serialNumber = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("SerialNumber"));
            }
        }

        [Display(Name = "UsedPartsOutboundDetail_PlannedAmount", ResourceType = typeof(Resources.EntityStrings))]
        public int? PlannedAmount {
            get;
            set;
        }

        [Display(Name = "UsedPartsOutboundDetail_ConfirmedAmount", ResourceType = typeof(Resources.EntityStrings))]
        public int? ConfirmedAmount {
            get;
            set;
        }
    }
}
