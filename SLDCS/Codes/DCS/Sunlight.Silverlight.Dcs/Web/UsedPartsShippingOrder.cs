﻿
using System.Linq;
namespace Sunlight.Silverlight.Dcs.Web {
    public partial class UsedPartsShippingOrder {
        public int DetailCount {
            get {
                return this.UsedPartsShippingDetails.Count;
            }
        }

        public decimal SumUnitPrice {
            get {
                return this.UsedPartsShippingDetails.Sum(r => r.UnitPrice * r.Quantity);
            }
        }
    }
}
