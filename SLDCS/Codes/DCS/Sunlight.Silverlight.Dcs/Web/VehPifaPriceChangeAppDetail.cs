﻿using System.ComponentModel;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class VehPifaPriceChangeAppDetail {
        private decimal accountDifference;

        public decimal AccountDifference {
            get {
                if(this.accountDifference > 0)
                    return this.accountDifference;
                return (decimal)(this.PriceBeforeChange - this.PriceAfterChange);
            }
            set {
                this.accountDifference = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("AccountDifference"));
            }
        }
    }
}
