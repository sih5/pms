﻿using System.ComponentModel;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class VehRetailPriceChangeAppDetail {
        private decimal difference;

        public decimal Difference {
            get {
                return this.difference;
            }
            set {
                this.difference = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("Difference"));
            }
        }
    }
}
