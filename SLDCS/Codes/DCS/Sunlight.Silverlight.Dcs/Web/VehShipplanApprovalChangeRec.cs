﻿using System.ComponentModel;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class VehShipplanApprovalChangeRec {
        private string productCode;
        private string productCategoryName;

        public string ProductCode {
            get {
                return this.productCode;
            }
            set {
                this.productCode = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("ProductCode"));
            }
        }

        public string ProductCategoryName {
            get {
                return this.productCategoryName;
            }
            set {
                this.productCategoryName = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("ProductCategoryName"));
            }
        }
    }
}
