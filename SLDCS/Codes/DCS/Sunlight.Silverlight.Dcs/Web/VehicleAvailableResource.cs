﻿using System.ComponentModel;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class VehicleAvailableResource {
        private int adjustmentType;

        public int AdjustmentType {
            get {
                return this.adjustmentType;
            }
            set {
                this.adjustmentType = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("AdjustmentType"));
            }
        }
    }
}
