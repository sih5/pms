﻿using System.ComponentModel;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class VehicleCustomerAccount {
        private decimal availableBalance;

        /// <summary>
        /// 发车审批管理，主界面查询，由于服务端返回的是整个Bo，并没有可用金额
        /// </summary>
        public decimal AvailableBalance {
            get {
                return this.CustomerCredenceAmount + this.AccountBalance;
            }
            set {
                this.availableBalance = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("availableBalance"));
            }
        }
    }
}
