﻿using System.Linq;
namespace Sunlight.Silverlight.Dcs.Web {
    public partial class VehicleModelAffiProduct {
        public decimal WholesalePrice {
            get;
            set;
        }

        /// <summary>
        /// 整车零售指导价格
        /// </summary>
        public decimal GuidePrice {
            get {
                return this.Product.VehicleRetailSuggestedPrices.Any() ? this.Product.VehicleRetailSuggestedPrices.First().Price : 0;
            }
        }
    }
}
