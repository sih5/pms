﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
namespace Sunlight.Silverlight.Dcs.Web {
    public partial class VehicleOrder {
        private decimal balance;
        private string vehicleFundsTypeName;
        private ObservableCollection<VehicleOrderDetailCollect> vehicleOrderDetailCollects;

        [Display(Name = "VehicleOrder_Balance", ResourceType = typeof(Resources.EntityStrings))]
        public decimal Balance {
            get {
                return this.balance;
            }
            set {
                this.balance = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("Balance"));
            }
        }

        public string VehicleFundsTypeName {
            get {
                return this.vehicleFundsTypeName;
            }
            set {
                this.vehicleFundsTypeName = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("VehicleFundsTypeName"));
            }
        }

        public ObservableCollection<VehicleOrderDetailCollect> VehicleOrderDetailCollects {
            get {
                return this.vehicleOrderDetailCollects ?? (this.vehicleOrderDetailCollects = new ObservableCollection<VehicleOrderDetailCollect>());
            }
            set {
                this.vehicleOrderDetailCollects = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("VehicleOrderDetailCollects"));
            }
        }
    }
}
