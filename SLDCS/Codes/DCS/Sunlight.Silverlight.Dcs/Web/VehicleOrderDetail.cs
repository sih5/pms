﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
namespace Sunlight.Silverlight.Dcs.Web {
    public partial class VehicleOrderDetail {
        private DateTime? shippingDate;

        public DateTime? ShippingDate {
            get {
                return this.shippingDate;
            }
            set {
                this.shippingDate = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("ShippingDate"));
            }
        }
    }
}
