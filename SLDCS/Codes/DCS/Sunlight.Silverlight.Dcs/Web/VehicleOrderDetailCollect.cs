﻿using System.ComponentModel;

namespace Sunlight.Silverlight.Dcs.Web {
    public class VehicleOrderDetailCollect : INotifyPropertyChanged {
        private int productId;
        private string productCode;
        private string productName;
        private string productCategoryCode;
        private string productCategoryName;
        private int quantity;
        private decimal price;

        public int ProductId {
            get {
                return this.productId;
            }
            set {
                this.productId = value;
                this.OnPropertyChanged("ProductId");
            }
        }

        public string ProductCode {
            get {
                return this.productCode;
            }
            set {
                this.productCode = value;
                this.OnPropertyChanged("ProductCode");
            }
        }

        public string ProductName {
            get {
                return this.productName;
            }
            set {
                this.productName = value;
                this.OnPropertyChanged("ProductName");
            }
        }

        public string ProductCategoryCode {
            get {
                return this.productCategoryCode;
            }
            set {
                this.productCategoryCode = value;
                this.OnPropertyChanged("ProductCategoryCode");
            }
        }

        public string ProductCategoryName {
            get {
                return this.productCategoryName;
            }
            set {
                this.productCategoryName = value;
                this.OnPropertyChanged("ProductCategoryName");
            }
        }

        public int Quantity {
            get {
                return this.quantity;
            }
            set {
                this.quantity = value;
                this.OnPropertyChanged("Quantity");
            }
        }

        public decimal Price {
            get {
                return this.price;
            }
            set {
                this.price = value;
                this.OnPropertyChanged("Price");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string property) {
            if(this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
        }
    }
}
