﻿using System.ComponentModel;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class VehiclePaymentBill {
        private string bankAccountNumber;

        public string BankAccountNumber {
            get {
                if(this.VehicleBankAccount != null)
                    return VehicleBankAccount.BankAccountNumber;
                return this.bankAccountNumber;
            }
            set {
                this.bankAccountNumber = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("BankAccountNumber"));
            }
        }
    }
}
