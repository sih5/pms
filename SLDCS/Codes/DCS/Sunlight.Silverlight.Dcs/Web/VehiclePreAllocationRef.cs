﻿using System.ComponentModel;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class VehiclePreAllocationRef {
        private string vehiclePreAllocationRefDealerCode;

        // 经销商编号
        public string VehiclePreAllocationRefDealerCode {
            get {
                return this.vehiclePreAllocationRefDealerCode;
            }
            set {
                this.vehiclePreAllocationRefDealerCode = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("VehiclePreAllocationRefDealerCode"));
            }
        }
    }
}
