﻿using System.ComponentModel;
using System.Linq;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class VehicleShippingOrderChangeRec {
        private string productCode;

        // 产品编号
        public string ProductCode {
            get {
                if(this.VehicleShippingOrder != null && this.VehicleShippingOrder.VehicleShippingDetails.Any())
                    return this.VehicleShippingOrder.VehicleShippingDetails.First().ProductCode;
                return this.productCode;
            }
            set {
                this.productCode = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("ProductCode"));
            }
        }

        private string productCategoryName;

        // 车型
        public string ProductCategoryName {
            get {
                return this.productCategoryName;
            }
            set {
                this.productCategoryName = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("ProductCategoryName"));
            }
        }
    }
}
