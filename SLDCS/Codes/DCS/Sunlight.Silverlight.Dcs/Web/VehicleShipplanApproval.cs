﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class VehicleShipplanApproval {
        private int vehicleWarehouseId;

        public int VehicleWarehouseId {
            get {
                return this.vehicleWarehouseId;
            }
            set {
                this.vehicleWarehouseId = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("VehicleWarehouseId"));
            }
        }
    }
}
