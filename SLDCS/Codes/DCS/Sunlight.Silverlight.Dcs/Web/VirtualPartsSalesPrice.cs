﻿using System;
using System.Net;
using System.Windows;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class VirtualPartsSalesPrice {
        public string IsUpsideDown {
            get {
                return this.SalesPrice >= this.PurchasePrice ? "否" : "是";
            }
        }
    }
}
