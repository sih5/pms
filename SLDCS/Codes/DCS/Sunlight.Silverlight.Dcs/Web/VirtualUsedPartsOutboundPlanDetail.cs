﻿using System.ComponentModel.DataAnnotations;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class VirtualUsedPartsOutboundPlanDetail {
        [Display(Name = "_Common_SerialNumber", ResourceType = typeof(Resources.EntityStrings))]
        public int SerialNumber {
            get;
            set;
        }
    }
}
