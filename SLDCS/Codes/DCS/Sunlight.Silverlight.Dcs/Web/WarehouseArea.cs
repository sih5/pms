﻿
using System.ComponentModel;

namespace Sunlight.Silverlight.Dcs.Web {
    public partial class WarehouseArea {
        private string warehouseName;
        public string WarehouseName {
            get {
                return this.warehouseName;
            }
            set {
                this.warehouseName = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("WarehouseName"));
            }
        }

        private int storageCompanyId;
        public int StorageCompanyId {
            get {
                return this.storageCompanyId;
            }
            set {
                this.storageCompanyId = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("StorageCompanyId"));
            }
        }
    }
}
