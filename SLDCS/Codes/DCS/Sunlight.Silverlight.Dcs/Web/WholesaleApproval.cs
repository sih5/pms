﻿
using System.ComponentModel;
namespace Sunlight.Silverlight.Dcs.Web {
    public partial class WholesaleApproval {
        private string keyAccountCode;
        private string customerName;
        private int customerType;
        private string customerAddress;
        private string postCode;
        private string contactPerson;
        private string contactJobPosition;
        private string contactPhone;
        private string contactCellPhone;
        private string contactFax;
        private string companyName;
        private int? customerProperty;
        private bool? ifTenderingOrCentralPurchase;

        /// <summary>
        /// 大客户代码
        /// </summary>
        public string KeyAccountCode {
            get {
                return this.KeyAccount != null ? this.KeyAccount.KeyAccountCode : this.keyAccountCode;
            }
            set {
                this.keyAccountCode = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("KeyAccountCode"));
            }
        }

        ///// <summary>
        ///// 客户编号
        ///// </summary>
        //public string CustomerCode {
        //    get {
        //        return this.KeyAccount != null ? this.KeyAccount.Customer != null ? this.KeyAccount.Customer.Code : this.customerCode : this.customerCode;
        //    }
        //    set {
        //        this.customerCode = value;
        //        this.OnPropertyChanged(new PropertyChangedEventArgs("CustomerCode"));
        //    }
        //}

        /// <summary>
        /// 客户名称
        /// </summary>
        public string CustomerName {
            get {
                return this.KeyAccount != null ? this.KeyAccount.Customer != null ? this.KeyAccount.Customer.Name : this.customerName : this.customerName;
            }
            set {
                this.customerName = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("CustomerName"));
            }
        }

        /// <summary>
        /// 客户类型
        /// </summary>
        public int CustomerType {
            get {
                return this.KeyAccount != null ? this.KeyAccount.Customer != null ? this.KeyAccount.Customer.CustomerType : this.customerType : this.customerType;
            }
            set {
                this.customerType = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("CustomerType"));
            }
        }

        /// <summary>
        /// 地址
        /// </summary>
        public string CustomerAddress {
            get {
                return this.KeyAccount != null ? this.KeyAccount.Customer != null ? this.KeyAccount.Customer.Address : this.customerAddress : this.customerAddress;
            }
            set {
                this.customerAddress = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("CustomerAddress"));
            }
        }

        /// <summary>
        /// 邮编
        /// </summary>
        public string CustomerPostCode {
            get {
                return this.KeyAccount != null ? this.KeyAccount.Customer != null ? this.KeyAccount.Customer.PostCode : this.postCode : this.postCode;
            }
            set {
                this.postCode = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("CustomerPostCode"));
            }
        }

        /// <summary>
        /// 联系人
        /// </summary>
        public string ContactPerson {
            get {
                return this.KeyAccount != null ? this.KeyAccount.ContactPerson : this.contactPerson;
            }
            set {
                this.contactPerson = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("ContactPerson"));
            }
        }

        /// <summary>
        /// 联系人职位
        /// </summary>
        public string ContactJobPosition {
            get {
                return this.KeyAccount != null ? this.KeyAccount.ContactJobPosition : this.contactJobPosition;
            }
            set {
                this.contactJobPosition = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("ContactJobPosition"));
            }
        }

        /// <summary>
        /// 联系人电话
        /// </summary>
        public string ContactPhone {
            get {
                return this.KeyAccount != null ? this.KeyAccount.ContactPhone : this.contactPhone;
            }
            set {
                this.contactPhone = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("ContactPhone"));
            }
        }

        /// <summary>
        /// 联系人手机号
        /// </summary>
        public string ContactCellPhone {
            get {
                return this.KeyAccount != null ? this.KeyAccount.ContactCellPhone : this.contactCellPhone;
            }
            set {
                this.contactCellPhone = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("ContactCellPhone"));
            }
        }

        /// <summary>
        /// 联系人传真
        /// </summary>
        public string ContactFax {
            get {
                return this.KeyAccount != null ? this.KeyAccount.ContactFax : this.contactFax;
            }
            set {
                this.contactFax = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("ContactFax"));
            }
        }

        /// <summary>
        /// 联系人公司
        /// </summary>
        public string CompanyName {
            get {
                return this.KeyAccount != null ? this.KeyAccount.CompanyName : this.companyName;
            }
            set {
                this.companyName = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("CompanyName"));
            }
        }

        public int? CustomerProperty {
            get {
                return this.KeyAccount != null ? this.KeyAccount.CustomerProperty : this.customerProperty;
            }
            set {
                this.customerProperty = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("CustomerProperty"));
            }
        }

        public bool? IfTenderingOrCentralPurchase {
            get {
                return this.KeyAccount != null ? this.KeyAccount.IfTenderingOrCentralPurchase : this.ifTenderingOrCentralPurchase;
            }
            set {
                this.ifTenderingOrCentralPurchase = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("IfTenderingOrCentralPurchase"));
            }
        }
    }
}
