﻿namespace Sunlight.Silverlight.Dcs {
    public static class WorkflowActionKey {
        public const string INITIALAPPROVE = "InitialApprove";
        public const string FINALAPPROVE = "FinalApprove";
        public const string COUNTERSIGN = "Countersign";

        public const string INITIALAPPROVE_FINALAPPROVE = INITIALAPPROVE + FINALAPPROVE;
        public const string INITIALAPPROVE_COUNTERSIGN = INITIALAPPROVE + COUNTERSIGN;
    }
}
