﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Globalization;
using System.IO;
using System.Web.Caching;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using Sunlight.Silverlight.Web.Extensions.Storages;
#if SqlServer
using System.Data.SqlClient;
#else
using Devart.Data.Oracle;
#endif

namespace Sunlight.Silverlight.Web.Extensions.ExcelOperators {
    public delegate string GetFieldTitleHandle(string fieldName);

    public delegate string GetBoolValueHandle(string fieldName, Boolean value);

    public delegate string GetEnumValueHandle(string fieldName, int value);

    public class ExcelExport : IDisposable {
        private const int SHEET_ROW_NUMBER = 60000;
        private readonly DbConnection conn;
        private readonly List<string> fieldTitles;
        private readonly DbCommand sqlCmd;
        private string aFileName;

        public ExcelExport(string excelFileName, string connectionString, string connectProvider, string exportSql) {
            if(string.IsNullOrEmpty(connectionString.Trim()))
                throw new ArgumentNullException("connectionString", "不能为空字符串");
            if(string.IsNullOrEmpty(connectionString.Trim()))
                throw new ArgumentNullException("connectProvider", "不能为空字符串");
            if(string.IsNullOrEmpty(exportSql.Trim()))
                throw new ArgumentNullException("exportSql", "查询SQL不能为空字符串");
            if(string.IsNullOrEmpty(excelFileName.Trim()))
                throw new ArgumentNullException("excelFileName", "必须指定Excel文件名");
            this.FileName = excelFileName;
            this.fieldTitles = new List<string>();

#if SqlServer
            this.sqlCmd = new SqlCommand(exportSql, new SqlConnection(connectionString));
#else
            sqlCmd = new OracleCommand(exportSql, new OracleConnection(connectionString));
#endif
            this.conn = this.sqlCmd.Connection;
            this.conn.Open();
        }

        public ExcelExport(string excelFileName, DbCommand aCommand) {
            if(string.IsNullOrEmpty(excelFileName.Trim()))
                throw new ArgumentNullException("excelFileName", "必须指定Excel文件名");
            this.FileName = excelFileName;
            if(this.sqlCmd == null) {
                throw new ArgumentNullException("aCommand", "必须指定SQLCommand类的实例");
            }
            this.sqlCmd = aCommand;
        }

        public ExcelExport(string excelFileName) {
            this.FileName = excelFileName;
        }

        public ExcelExportHelper ExportHelper {
            get;
            set;
        }

        public string FileName {
            get {
                return this.aFileName;
            }
            set {
                this.aFileName = value.Trim();
                while(File.Exists(this.aFileName)) {
                    this.aFileName = Path.GetDirectoryName(this.aFileName) + Path.GetFileNameWithoutExtension(this.aFileName) + "_" + DateTime.Now.ToString("yyyyMMdd") + "_" + DateTime.Now.ToString("hhmmss") + ".xlsx";
                }
            }
        }

        #region IDisposable Members

        public void Dispose() {
            if(this.conn != null) {
                this.conn.Close();
                this.sqlCmd.Dispose();
                this.conn.Dispose();
            }
        }
        #endregion

        /// <summary>
        /// 获取字段标题事件
        /// </summary>        
        public event GetFieldTitleHandle OnGetFieldTitle;

        /// <summary>
        /// 获得布尔值自定义值
        /// </summary>       
        public event GetBoolValueHandle OnGetBoolValue;

        /// <summary>
        /// 获得枚举自定义值
        /// </summary>        
        public event GetEnumValueHandle OnGetEnumValue;

        public Boolean Export() {
            if(this.sqlCmd == null) {
                throw new DatabaseNotEnabledForNotificationException("数据源未就绪。");
            }
            var reader = this.sqlCmd.ExecuteReader();
            if(!reader.HasRows)
                throw new DataException("查询无数据");

            try {
                IWorkbook wb = new XSSFWorkbook();
                ISheet st = wb.CreateSheet();

                int iRow = 0;
                while(reader.Read()) {
                    IRow row = st.CreateRow(iRow);
                    if(iRow == 0) {
                        for(int i = 0; i < reader.FieldCount; i++) {
                            string sName = "";
                            if(this.ExportHelper != null)
                                sName = this.ExportHelper.GetColumnHeader(reader.GetName(i));
                            else {
                                if((this.fieldTitles.IndexOf(reader.GetName(i)) == -1)) {
                                    sName = reader.GetName(i);
                                    if(this.OnGetFieldTitle != null)
                                        sName = this.OnGetFieldTitle(sName);
                                }
                            }
                            row.CreateCell(i).SetCellValue(sName);
                        }
                        iRow++;
                        row = st.CreateRow(iRow);
                    }
                    for(int i = 0; i < reader.FieldCount; i++) {
                        string retvalue = "";
                        var fieldType = reader.GetFieldType(i);
                        if(fieldType != null)
                            switch(fieldType.Name) {
                                case "String":
                                    row.CreateCell(i).SetCellValue(reader.GetString(i));
                                    break;
                                case "Char":
                                    row.CreateCell(i).SetCellValue(reader.GetChar(i));
                                    break;
                                case "Boolean":
                                    if(this.ExportHelper != null)
                                        retvalue = this.ExportHelper.GetBoolEnum(reader.GetName(i), reader.GetBoolean(i));
                                    else
                                        retvalue = reader.GetBoolean(i) ? "是" : "否";
                                    if(this.OnGetBoolValue != null)
                                        retvalue = this.OnGetBoolValue(reader.GetName(i), reader.GetBoolean(i));
                                    row.CreateCell(i).SetCellValue(retvalue);
                                    break;
                                case "Int16":
                                    if(this.ExportHelper != null)
                                        retvalue = this.ExportHelper.GetEnumValue(reader.GetName(i), reader.GetInt16(i));
                                    if(this.OnGetEnumValue != null)
                                        retvalue = this.OnGetEnumValue(reader.GetName(i), reader.GetInt16(i));
                                    if(retvalue.Equals(reader.GetInt16(i).ToString(CultureInfo.InvariantCulture)))
                                        row.CreateCell(i).SetCellValue(reader.GetInt16(i));
                                    else
                                        row.CreateCell(i).SetCellValue(retvalue);
                                    break;
                                case "Int32":
                                    if(this.ExportHelper != null)
                                        retvalue = this.ExportHelper.GetEnumValue(reader.GetName(i), reader.GetInt32(i));
                                    if(this.OnGetEnumValue != null)
                                        retvalue = this.OnGetEnumValue(reader.GetName(i), reader.GetInt32(i));
                                    if(retvalue.Equals(reader.GetInt32(i).ToString(CultureInfo.InvariantCulture)))
                                        row.CreateCell(i).SetCellValue(reader.GetInt32(i));
                                    else
                                        row.CreateCell(i).SetCellValue(retvalue);
                                    break;
                                case "Int64":
                                    row.CreateCell(i).SetCellValue(reader.GetInt64(i));
                                    break;
                                case "DateTime":
                                    row.CreateCell(i).SetCellValue(reader.GetDateTime(i).ToLongDateString());
                                    break;
                                case "Byte":
                                    if(this.ExportHelper != null)
                                        retvalue = this.ExportHelper.GetEnumValue(reader.GetName(i), reader.GetByte(i));
                                    if(this.OnGetEnumValue != null)
                                        retvalue = this.OnGetEnumValue(reader.GetName(i), reader.GetByte(i));
                                    if(retvalue.Equals(reader.GetByte(i).ToString(CultureInfo.InvariantCulture)))
                                        row.CreateCell(i).SetCellValue(reader.GetByte(i));
                                    else
                                        row.CreateCell(i).SetCellValue(retvalue);
                                    break;
                                case "Float":
                                    row.CreateCell(i).SetCellValue(reader.GetFloat(i));
                                    break;
                                case "Decimal":
                                    row.CreateCell(i).SetCellValue((Double)reader.GetDecimal(i));
                                    break;
                                case "Double":
                                    row.CreateCell(i).SetCellValue(reader.GetDouble(i));
                                    break;
                                case "Guid":
                                    row.CreateCell(i).SetCellValue(reader.GetGuid(i).ToString());
                                    break;
                                default:
                                    row.CreateCell(i).SetCellValue(reader.GetString(i));
                                    break;
                            }
                    }
                    iRow++;
                }
                reader.Close();
                using(var storage = StorageProvider.GetStorage(this.FileName,null)) {
                    using(var stream = storage.GetStreamForWrite())
                        wb.Write(stream);
                }
            } catch {
                return false;
            }
            return true;
        }

        //对获取的数据值，不做枚举转义处理
        public void ExportByRow(Func<int, object[]> getRowValue) {
            if(getRowValue == null)
                throw new Exception("未定义数据获取方法");
            IWorkbook wb = new XSSFWorkbook();
            var st = wb.CreateSheet();
            var dateCellStyle = wb.CreateCellStyle();
            dateCellStyle.DataFormat = wb.CreateDataFormat().GetFormat("yyyy年m月d日");

            var iRow = 0;
            var sheetRow = 0;
            while(true) {
                var rowData = getRowValue(sheetRow++);


                if(rowData == null)
                    break;
                if(iRow >= SHEET_ROW_NUMBER) {
                    iRow = 0;
                    st = wb.CreateSheet();
                }
                var row = st.CreateRow(iRow++);
                for(var i = 0; i < rowData.Length; i++) {
                    if(rowData[i] == null)
                        continue;
                    switch(rowData[i].GetType().Name) {
                        case "Float":
                        case "Decimal":
                        case "Int32":
                        case "Double":
                            row.CreateCell(i).SetCellValue(Convert.ToDouble(rowData[i]));
                            break;
                        case "DateTime":
                            var cell = row.CreateCell(i);
                            cell.SetCellValue(Convert.ToDateTime(rowData[i]));
                            cell.CellStyle = dateCellStyle;
                            break;
                        case "Boolean":
                            row.CreateCell(i).SetCellValue(Convert.ToBoolean(rowData[i]));
                            break;

                        case "StyleDataField":
                            StyleDataField f = (StyleDataField)rowData[i];
                            if(f.FieldData == null)
                                continue;
                            ICell cell2 = row.CreateCell(i);
                            ICellStyle mydateCellStyle = wb.CreateCellStyle();
                            switch(f.FieldData.GetType().Name) {
                                case "Float":
                                case "Decimal":
                                case "Int32":
                                case "Double":
                                    cell2.SetCellValue(Convert.ToDouble(f.FieldData));
                                    break;
                                case "DateTime":
                                    cell2.SetCellValue(Convert.ToDateTime(f.FieldData));
                                    break;
                                case "Boolean":
                                    cell2.SetCellValue(Convert.ToBoolean(f.FieldData));
                                    break;
                                default:
                                    cell2.SetCellValue(f.FieldData.ToString());
                                    break;
                            }
                            cell2.CellStyle = mydateCellStyle;
                            mydateCellStyle.DataFormat = wb.CreateDataFormat().GetFormat(f.Style);
                            break;
                        default:
                            row.CreateCell(i).SetCellValue(rowData[i].ToString());
                            break;
                    }
                }
            }
            using(var storage = StorageProvider.GetStorage(this.FileName,null)) {
                using(var stream = storage.GetStreamForWrite())
                    wb.Write(stream);
            }
        }
    }
}