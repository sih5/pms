﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using Sunlight.Silverlight.Web.Extensions.Storages;

namespace Sunlight.Silverlight.Web.Extensions.ExcelOperators {
    public class ExcelNoDataException : Exception {
        public ExcelNoDataException()
            : base("Excel文件无数据") {
        }
    }

    public delegate string ParseValueHandle(ExcelColumns source, string valueString);

    public class ExcelColumns {
        private readonly string sName;

        /// <summary>
        /// Excel列名
        /// </summary>
        public string Name {
            get {
                return this.sName;
            }
        }

        /// <summary>
        /// 绑定字段名
        /// </summary>
        public string FieldName {
            get;
            set;
        }

        /// <summary>
        /// Excel列索引值
        /// </summary>
        public int Index {
            get;
            set;
        }

        public Type FieldType {
            get;
            set;
        }

        public ExcelColumns(string columnName, string fieldName, int index, Type fieldType) {
            if(string.IsNullOrEmpty(columnName))
                throw new ArgumentNullException("columnName");
            if(index < 0)
                throw new ArgumentOutOfRangeException("index");
            if(string.IsNullOrEmpty(fieldName))
                throw new ArgumentNullException("fieldName");
            this.sName = columnName;
            this.Index = index;
            this.FieldName = fieldName;
            this.FieldType = fieldType;
        }
    }

    public class ExcelImport : IDisposable {
        private readonly ISheet sheet;
        public event ParseValueHandle OnParseValue;

        public ISheet Sheet {
            get {
                return this.sheet;
            }
        }

        /// <summary>
        /// 最后列号
        /// </summary>
        public int LastRowNum {
            get {
                return Sheet.LastRowNum;
            }
        }
        /// <summary>
        /// 字段列表
        /// </summary>
        public List<ExcelColumns> Columns {
            get;
            set;
        }

        public ExcelImportHelper ImportHelper {
            get;
            set;
        }

        private int IndexOfColumnName(string columnName) {
            for(var i = 0; i < this.Columns.Count; i++) {
                if(columnName.Equals(this.Columns[i].Name, StringComparison.OrdinalIgnoreCase))
                    return i;
            }
            return -1;
        }

        private string GetCellValue(ICell cell) {
            if(cell == null)
                return null;
            switch(cell.CellType) {
                case CellType.Numeric:
                    return DateUtil.IsCellDateFormatted(cell) || DateUtil.IsCellInternalDateFormatted(cell) ? cell.DateCellValue.ToString(CultureInfo.InvariantCulture) : cell.NumericCellValue.ToString(CultureInfo.InvariantCulture);
                case CellType.Boolean:
                    return cell.BooleanCellValue.ToString();
                case CellType.Error:
                    throw new Exception(string.Format("Excel中第{0}行，第{1}列值有误！", cell.RowIndex + 1, cell.ColumnIndex + 1));
                case CellType.Blank:
                    return null;
                default:
                    return (cell.StringCellValue.StartsWith("'") ? cell.StringCellValue.Substring(1) : cell.StringCellValue).Trim();
            }
        }

        public int IndexOfFieldName(string fieldName) {
            for(var i = 0; i <= this.Columns.Count - 1; i++) {
                if(fieldName.Equals(this.Columns[i].FieldName, StringComparison.OrdinalIgnoreCase))
                    return i;
            }
            return -1;
        }

        public List<Dictionary<string, string>> LoadExcel() {
            if(this.Columns.Count == 0)
                throw new Exception("请先指定Excel列对应的字段。");
            var data = new List<Dictionary<string, string>>();
            for(var i = 0; i <= this.sheet.LastRowNum; i++) {
                var row = this.sheet.GetRow(i);
                var rowData = new Dictionary<string, string>();
                foreach(var column in this.Columns) {
                    if(column.Index == -1)
                        continue;
                    var cell = row.GetCell(column.Index);
                    if(cell == null)
                        break;
                    string sValue;

                    if(this.OnParseValue != null) {
                        switch(cell.CellType) {
                            case CellType.Numeric:
                                sValue = DateUtil.IsCellDateFormatted(cell) ? cell.DateCellValue.ToString(CultureInfo.InvariantCulture) : cell.NumericCellValue.ToString(CultureInfo.InvariantCulture);
                                sValue = this.OnParseValue(column, sValue) ?? sValue;
                                break;
                            case CellType.Boolean:
                                sValue = this.OnParseValue(column, cell.BooleanCellValue.ToString()) ?? cell.BooleanCellValue.ToString();
                                break;
                            case CellType.Error:
                            case CellType.Blank:
                                throw new Exception(string.Format("Excel中第{0}行，第{1}列值有误！", i, column.Index));
                            default:
                                sValue = this.OnParseValue(column, cell.StringCellValue.Trim()) ?? cell.StringCellValue.Trim();
                                break;
                        }
                    } else {
                        switch(cell.CellType) {
                            case CellType.Numeric:
                                sValue = DateUtil.IsCellDateFormatted(cell) ? cell.DateCellValue.ToString(CultureInfo.InvariantCulture) : cell.NumericCellValue.ToString(CultureInfo.InvariantCulture);
                                break;
                            case CellType.Boolean:
                                sValue = cell.BooleanCellValue.ToString();
                                break;
                            case CellType.Error:
                            case CellType.Blank:
                                throw new Exception(string.Format("Excel中第{0}行，第{1}列值有误！", i, column.Index));
                            default:
                                sValue = cell.StringCellValue;
                                break;
                        }
                    }
                    rowData.Add(column.FieldName, sValue);
                }
                data.Add(rowData);
            }
            return data;
        }

        public int LoadExcelRow(Func<Dictionary<string, string>, bool> funcLoadExcelRow) {
            if(this.Columns.Count == 0)
                throw new Exception("请先指定Excel列对应的字段。");
            if(funcLoadExcelRow == null)
                throw new Exception("未定义导入数据处理方法");
            var result = this.sheet.LastRowNum - 1;
            for(var i = 1; i <= this.sheet.LastRowNum; i++) {
                var row = this.sheet.GetRow(i);
                if(row == null)
                    continue;
                var rowData = new Dictionary<string, string>();
                var hasValue = false;
                foreach(var column in this.Columns) {
                    if(column.Index == -1)
                        continue;
                    var sValue = this.GetCellValue(row.GetCell(column.Index));
                    if(!hasValue && !string.IsNullOrEmpty(sValue))
                        hasValue = true;
                    rowData.Add(column.FieldName, sValue);
                }
                if(!hasValue) {
                    result = i - 1;
                    break;
                }
                if(funcLoadExcelRow(rowData))
                    break;
            }
            return result;
        }

        public int LoadSingleRow(Func<string[], bool> funcLoadSingleRow) {
            if(funcLoadSingleRow == null)
                throw new Exception("未定义导入数据处理方法");
            var result = this.sheet.LastRowNum - 1;
            for(var i = 0; i <= this.sheet.LastRowNum; i++) {
                var row = this.sheet.GetRow(i);
                if(row == null)
                    continue;
                var rowData = new List<string>();
                var hasValue = false;
                foreach(var cell in row.Cells) {
                    var sValue = this.GetCellValue(cell);
                    if(!hasValue && !string.IsNullOrEmpty(sValue))
                        hasValue = true;
                    rowData.Add(sValue);
                }
                if(!hasValue) {
                    result = i;
                    break;
                }
                if(funcLoadSingleRow(rowData.ToArray()))
                    break;
            }
            return result;
        }

        /// <summary>
        /// 指定数据表对应列
        /// </summary>   
        /// <param name="columnName">Excel列名</param>
        /// <param name="fieldName">字段列</param>
        public void AddColumnDataSource(string columnName, string fieldName) {
            this.AddColumnDataSource(columnName, fieldName, null);
        }

        /// <summary>
        /// 指定数据表对应列
        /// </summary>   
        /// <param name="columnName">Excel列名</param>
        /// <param name="fieldName">字段列</param>
        public void AddColumnDataSourceTryCatch(string columnName, string fieldName) {
            this.AddColumnDataSourceTryCatch(columnName, fieldName, null);
        }

        public void AddColumnDataSource(string columnName, string fieldName, Type fieldType) {
            if(string.IsNullOrEmpty(columnName))
                throw new ArgumentNullException("columnName");
            if(string.IsNullOrEmpty(fieldName))
                throw new ArgumentNullException("fieldName");
            var cName = columnName.ToUpper();
            var fName = fieldName.ToUpper();
            var excelIndex = this.ExcelIndexOfName(cName);
            if(excelIndex == -1)
                throw new Exception(string.Format("名为{0}的数据列不存在", cName));
            var index = this.IndexOfColumnName(cName);
            if(index != -1)
                throw new Exception(string.Format("名为{0}的数据列已定义对应字段", cName));
            index = this.IndexOfFieldName(fName);
            if(index != -1)
                throw new Exception(string.Format("名为{0}的字段已定义对应数据列", fName));
            this.Columns.Add(new ExcelColumns(columnName, fieldName, excelIndex, fieldType));
        }

        public void AddColumnDataSourceTryCatch(string columnName, string fieldName, Type fieldType) {
            if(string.IsNullOrEmpty(columnName))
                throw new ArgumentNullException("columnName");
            if(string.IsNullOrEmpty(fieldName))
                throw new ArgumentNullException("fieldName");
            var cName = columnName.ToUpper();
            var fName = fieldName.ToUpper();
            var excelIndex = this.ExcelIndexOfName(cName);
            if(excelIndex == -1)
                return;
            var index = this.IndexOfColumnName(cName);
            if(index != -1)
                return;
            index = this.IndexOfFieldName(fName);
            if(index != -1)
                return;
            this.Columns.Add(new ExcelColumns(columnName, fieldName, excelIndex, fieldType));
        }

        public ExcelImport(string excelFileName, string connectionStr)
            : this(excelFileName) {
            if(string.IsNullOrEmpty(connectionStr))
                throw new ArgumentNullException("connectionStr");
            this.ImportHelper = new ExcelImportHelper(connectionStr);
        }

        public ExcelImport(string excelFileName) {
            if(string.IsNullOrEmpty(excelFileName))
                throw new ArgumentException("未指定Excel文件名。", excelFileName);
            this.Columns = new List<ExcelColumns>();
            using(var storage = StorageProvider.GetStorage(excelFileName,null)) {
                using(var xlsF = storage.GetSeekableStreamForRead()) {
                    IWorkbook wb;
                    string fileExt = Path.GetExtension(excelFileName);
                    if(fileExt == ".xls") {
                        wb = new HSSFWorkbook(xlsF);
                    } else if(fileExt == ".xlsx") {
                        wb = new XSSFWorkbook(xlsF);
                    } else {
                        return;
                    }
                    this.sheet = wb.GetSheetAt(0);
                    if(this.sheet == null)
                        throw new Exception("找不到符合条件的Sheet");
                    if(this.sheet.LastRowNum == 0)
                        throw new ExcelNoDataException();
                }
            }
            this.ImportHelper = new ExcelImportHelper();
        }

        public ExcelImport(FileStream excelFileName, string sheetName = "") {
            this.Columns = new List<ExcelColumns>();
            using(var xlsF = excelFileName) {
                IWorkbook wb = new HSSFWorkbook(xlsF);
                this.sheet = string.IsNullOrEmpty(sheetName) ? wb.GetSheetAt(0) : wb.GetSheet(sheetName);
                if(this.sheet == null)
                    throw new Exception("找不到符合条件的Sheet");
                if(this.sheet.LastRowNum == 0)
                    throw new ExcelNoDataException();
            }
            this.ImportHelper = new ExcelImportHelper();
        }

        public int ExcelIndexOfName(string columnName) {
            var index = -1;
            var row = this.sheet.GetRow(0);
            for(int i = 0; i <= row.LastCellNum - 1; i++) {
                var cell = row.GetCell(i);
                if(cell == null)
                    continue;
                if(cell.StringCellValue.Trim().Equals(columnName, StringComparison.OrdinalIgnoreCase)) {
                    if(index == -1)
                        index = i;
                    else
                        throw new Exception(string.Format("存在多个名为“{0}”的数据列", columnName));
                }
            }
            return index;
        }

        public void Dispose() {
            if(this.Columns != null)
                this.Columns.Clear();
        }
    }
}