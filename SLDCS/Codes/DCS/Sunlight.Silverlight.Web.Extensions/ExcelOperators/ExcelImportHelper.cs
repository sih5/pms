﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
#if SqlServer
using System.Data.SqlClient;
#else
using Devart.Data.Oracle;
#endif

namespace Sunlight.Silverlight.Web.Extensions.ExcelOperators {
    public class ExcelImportHelper {
        private readonly Dictionary<string, Dictionary<string, int>> eNumList;
        private readonly Dictionary<string, Dictionary<string, bool>> eNumBool;
        private readonly string connectionStr;

        public ExcelImportHelper() {
            this.eNumList = new Dictionary<string, Dictionary<string, int>>();
            this.eNumBool = new Dictionary<string, Dictionary<string, bool>>();
        }

        public ExcelImportHelper(string connectionStr)
            : this() {
            this.connectionStr = connectionStr;
        }

        /// <summary>
        /// 添加枚举替换值
        /// </summary>
        /// <param name="fieldName">替换字段</param>
        /// <param name="valuePair">替换值(第一个值为Excel实际值，第二个为替换值)</param>
        public void AddEnum(string fieldName, params KeyValuePair<string, int>[] valuePair) {
            if(string.IsNullOrEmpty(fieldName) || valuePair.Length <= 0)
                return;
            fieldName = fieldName.ToLower();
            if(!this.eNumList.ContainsKey(fieldName))
                this.eNumList.Add(fieldName, new Dictionary<string, int>());
            var eNum = this.eNumList[fieldName];
            foreach(var value in valuePair) {
                var sKey = string.IsNullOrEmpty(value.Key) ? value.Value.ToString(CultureInfo.InvariantCulture) : value.Key;
                eNum[sKey] = value.Value;
            }
        }

        public void AddBooleanEnum(string fieldName, string trueValue, string falseValue) {
            if(string.IsNullOrEmpty(fieldName))
                return;
            if(string.IsNullOrEmpty(trueValue))
                trueValue = "真";
            if(string.IsNullOrEmpty(falseValue))
                falseValue = "假";
            if(this.eNumBool.ContainsKey(fieldName.ToLower())) {
                this.eNumBool[fieldName.ToLower()][trueValue] = true;
                this.eNumBool[fieldName.ToLower()][falseValue] = false;
            } else {
                this.eNumBool.Add(fieldName.ToLower(), new Dictionary<string, bool> {
                    {
                        trueValue, true
                    }, {
                        falseValue, false
                    }
                });
            }
        }

        /// <summary>
        /// 从数据库调入枚举值列表
        /// </summary>
        /// <param name="keyValueTableName">枚举值表名</param>
        /// <param name="fieldPair">枚举值列表（第一个参数为目标表中被替换的字段名，第二个参数为枚举值表名里枚举名称限制值）</param>
        /// <param name="key">枚举值限定条件</param>
        /// <returns></returns>
        public Boolean LoadEnumFromDb(int? key, string keyValueTableName = "KeyValueItem", params KeyValuePair<string, string>[] fieldPair) {
            if(fieldPair.Length <= 0 || string.IsNullOrEmpty(this.connectionStr.Trim()))
                return false;
            if(string.IsNullOrEmpty(this.connectionStr))
                throw new Exception("未设置字典项来源数据库");
            if(string.IsNullOrWhiteSpace(keyValueTableName))
                keyValueTableName = "KeyValueItem";
            DbConnection conn = null;
            try {
#if SqlServer
                var connection = new SqlConnection(this.connectionStr);
                var cmd = new SqlCommand("", connection);
                const string SQL = "SELECT [Key],Value FROM {0} WHERE Name='{1}'";
                conn = connection;
#else
                var connection = new OracleConnection(connectionStr);
                var cmd = new OracleCommand("", connection);
                string sqlWithKey = null;
                const string SQL = "SELECT Key,Value FROM {0} WHERE Name='{1}'";
                if(key.HasValue) {
                    sqlWithKey = "select key,value from {0} where name='{1}' and key in {2}";
                }
                conn = connection;
#endif
                conn.Open();
                foreach(var value in fieldPair) {
                    if(string.IsNullOrEmpty(value.Key) || string.IsNullOrEmpty(value.Value))
                        continue;
                    cmd.CommandText = string.Format(SQL, keyValueTableName, value.Value);
                    if(key.HasValue) {
                        cmd.CommandText = string.Format(sqlWithKey, keyValueTableName, value.Value, key);
                    }
                    var reader = cmd.ExecuteReader();
                    var tmpValue = new Dictionary<string, int>();
                    while(reader.Read()) {
                        tmpValue.Add(reader.GetString(1).ToLower(), reader.GetInt32(0));
                    }
                    this.eNumList.Add(value.Key.ToLower(), tmpValue);
                    reader.Close();
                }
            } catch(Exception) {
                if(conn != null)
                    conn.Close();
                throw;
            }
            return true;
        }


        public int? GetEnumValue(string fieldName, string enumKey) {
            if(string.IsNullOrEmpty(enumKey))
                return null;
            if(!this.eNumList.ContainsKey(fieldName.ToLower()))
                return null;
            var tmpDict = this.eNumList[fieldName.ToLower()];
            if(!tmpDict.ContainsKey(enumKey))
                return null;
            return tmpDict[enumKey];
        }

        public Boolean? GetBoolEnum(string fieldName, string value) {
            if(string.IsNullOrEmpty(value))
                return null;
            if(!this.eNumBool.ContainsKey(fieldName))
                return null;
            return this.eNumBool[fieldName][value];
        }
    }
}