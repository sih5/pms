﻿using System.Collections.Generic;
using System.Linq;
using NPOI.HSSF.Util;
using NPOI.XSSF.UserModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Sunlight.Silverlight.Web.Extensions.Storages;

namespace Sunlight.Silverlight.Web.Extensions.ExcelOperators {
    /// <summary>
    ///     导入的模板文件
    /// </summary>
    public class ImportTemplate {
        /// <summary>
        ///     列定义
        /// </summary>
        public IList<ImportTemplateColumn> Columns {
            get;
            private set;
        }

        public ImportTemplate(IList<ImportTemplateColumn> columns) {
            this.Columns = columns;
        }

        /// <summary>
        ///     根据列定义生成 Excel 文件
        /// </summary>
        /// <param name="filePath">文件路径</param>
        public void GenerateExcelFile(string filePath) {
            var workbook = new XSSFWorkbook();

            // 必填项样式
            var font = workbook.CreateFont();
            font.Color = HSSFColor.Red.Index;
            var requiredCellStyle = workbook.CreateCellStyle();
            requiredCellStyle.SetFont(font);

            // 生成 Excel
            var sheet = workbook.CreateSheet();
            var columnHeaderRow = sheet.CreateRow(0);
            for(var i = 0; i < this.Columns.Count(); i++) {
                var column = this.Columns[i];
                var cell = columnHeaderRow.CreateCell(i);
                cell.SetCellValue(column.Name);
                if(column.IsRequired)
                    cell.CellStyle = requiredCellStyle;
            }

            // 保存到存储系统
            using(var storage = StorageProvider.GetStorage(filePath,null)) {
                using(var stream = storage.GetStreamForWrite())
                    workbook.Write(stream);
            }
        }
    }
}
