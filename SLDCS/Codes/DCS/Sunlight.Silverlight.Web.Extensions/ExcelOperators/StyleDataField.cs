﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sunlight.Silverlight.Web.Extensions.ExcelOperators
{
    public class StyleDataField
    {
        public object FieldData
        {
            get;
            set;
        }

        public string Style
        {
            get;
            set;
        }

        public StyleDataField(){
        
        }

        public StyleDataField(object fieldData,string style)
        {
            FieldData = fieldData;
            Style = style;
        }

    }
}
