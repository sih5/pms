﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Sunlight.Silverlight.Web {
    public static class ExtendFunctions {
        public static bool In<T>(this T t, params T[] paramTs) {
            return paramTs.Any(i => i.Equals(t));
        }

        /// <summary>
        /// 指示指定的字符串是 null 还是 System.String.Empty 字符串。
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static bool IsNullOrEmpty(this string s) {
            return string.IsNullOrEmpty(s);
        }
    }
}