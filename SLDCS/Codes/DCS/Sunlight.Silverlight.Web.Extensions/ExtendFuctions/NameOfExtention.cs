﻿using System;
using System.Linq.Expressions;

namespace Sunlight.Silverlight.Web {
    public static class NameOfExtention {
        public static string Nameof<T, E>(this Expression<Func<T, E>> accessor) {
            return Nameof(accessor.Body);
        }

        /// <summary>
        /// 返回参数或字段的名称
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="accessor"></param>
        /// <returns></returns>
        /// <code>
        /// // 获取dealerId的名称
        /// var dealerId = 1000;
        /// NameOfExtention.Nameof(() =&gt; dealerId);
        /// </code>
        public static string Nameof<T>(this Expression<Func<T>> accessor) {
            return Nameof(accessor.Body);
        }

        /// <summary>
        /// 返回属性的名称
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="E"></typeparam>
        /// <param name="obj"></param>
        /// <param name="propertyAccessor"></param>
        /// <returns></returns>
        public static string Nameof<T, E>(this T obj, Expression<Func<T, E>> propertyAccessor) {
            return Nameof(propertyAccessor.Body);
        }
        private static string Nameof(Expression expression) {
            if(expression.NodeType != ExpressionType.MemberAccess)
                return null;
            var memberExpression = expression as MemberExpression;
            return memberExpression == null ? null : memberExpression.Member.Name;
        }
    }
}
