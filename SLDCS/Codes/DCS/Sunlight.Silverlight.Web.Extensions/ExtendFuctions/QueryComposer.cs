﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace System.Linq {
    /// <summary>
    /// Copy System.ServiceModel.DomainServices.Server.QueryComposer
    /// 程序集 System.ServiceModel.DomainServices.Server.dll, v4.0.30319
    /// 仅公开方法Compose，并增加source是否为空的判断
    /// </summary>
    public static class QueryComposer {
        // Methods
        /// <summary>
        /// 返回合并后查询约束的IQueryable
        /// </summary>
        /// <param name="source">数据源</param>
        /// <param name="query">待合并查询约束</param>
        /// <returns>
        /// 如果query不为空，返回合并后查询约束的IQueryable
        /// 如果query为空，返回source
        /// </returns>
        public static IQueryable Compose(IQueryable source, IQueryable query) {
            return query == null ? source : new QueryRebaser().Rebase(source, query);
        }

        /// <summary>
        /// 合并客户端的所有条件，即 Where、排序、Skip和Take，示例在 http://wiki.sdt-int.com/pages/viewpage.action?pageId=9635380
        /// </summary>
        /// <typeparam name="TSource">数据源类型</typeparam>
        /// <typeparam name="TFilter">客户端条件关联的对象类型</typeparam>
        /// <param name="source">数据源</param>
        /// <param name="filterProperty">从数据源中获取符合客户端条件的对象</param>
        /// <param name="query">客户端的条件</param>
        /// <returns>过滤后的数据</returns>
        public static IQueryable<TSource> ComposeAllFilter<TSource, TFilter>(this IQueryable<TSource> source,
            Expression<Func<TSource, TFilter>> filterProperty, IQueryable query) {
            if(query == null)
                return source;
            //if(filterProperty.Body.NodeType != ExpressionType.MemberAccess)
            //    throw new NotSupportedException("表达式必须是对属性的访问");
            return new PropertyModifier<TSource>(true).Rebase(source, filterProperty, query);
        }

        /// <summary>
        /// 合并客户端除分页外的所有条件，示例在 http://wiki.sdt-int.com/pages/viewpage.action?pageId=9635380
        /// </summary>
        /// <typeparam name="TSource">数据源类型</typeparam>
        /// <typeparam name="TFilter">客户端条件关联的对象类型</typeparam>
        /// <param name="source">数据源</param>
        /// <param name="filterProperty">从数据源中获取符合客户端条件的对象</param>
        /// <param name="query">客户端的条件</param>
        /// <returns>过滤后的数据</returns>
        public static IQueryable<TSource> ComposeWithoutPaging<TSource, TFilter>(this IQueryable<TSource> source,
            Expression<Func<TSource, TFilter>> filterProperty, IQueryable query) {
            if(query == null)
                return source;
            //if(filterProperty.Body.NodeType != ExpressionType.MemberAccess)
            //    throw new NotSupportedException("表达式必须是对属性的访问");
            return new PropertyModifier<TSource>(false).Rebase(source, filterProperty, query);
        }

        /// <summary>
        /// 只合并客户端的分页条件，示例在 http://wiki.sdt-int.com/pages/viewpage.action?pageId=9635380
        /// </summary>
        /// <typeparam name="TSource">数据源类型</typeparam>
        /// <param name="source">数据源</param>
        /// <param name="query">客户端的条件</param>
        /// <returns>过滤后的数据</returns>
        public static IQueryable<TSource> ComposeOnlyPaging<TSource>(this IQueryable<TSource> source, IQueryable query) {
            if(query == null)
                return source;
            var expression = query.Expression as MethodCallExpression;
            int? takeCount = null, skipCount = null;
            while(expression != null && expression.Method.DeclaringType == typeof(Queryable)) {
                if(expression.Method.Name.Equals("take", StringComparison.OrdinalIgnoreCase)) {
                    takeCount = (int)((ConstantExpression)expression.Arguments[1]).Value;
                } else if(expression.Method.Name.Equals("skip", StringComparison.OrdinalIgnoreCase)) {
                    skipCount = (int)((ConstantExpression)expression.Arguments[1]).Value;
                }
                expression = expression.Arguments[0] as MethodCallExpression;
            }
            if(skipCount.HasValue)
                source = source.Skip(skipCount.Value);
            if(takeCount.HasValue)
                source = source.Take(takeCount.Value);
            return source;
        }

        //private static bool TryComposeWithLimit(IEnumerable results, DomainOperationEntry queryOperation, out IEnumerable limitedResults) {
        //    int resultLimit = ((QueryAttribute)queryOperation.OperationAttribute).ResultLimit;
        //    if(resultLimit > 0) {
        //        IQueryable source = results.AsQueryable();
        //        IQueryable query = Array.CreateInstance(queryOperation.AssociatedType, 0).AsQueryable();
        //        query = query.Provider.CreateQuery(Expression.Call(typeof(Queryable), "Take", new Type[] {
        //                query.ElementType
        //            }, new Expression[] {
        //                query.Expression, Expression.Constant(resultLimit)
        //            }));
        //        limitedResults = Compose(source, query);
        //        return true;
        //    }
        //    limitedResults = null;
        //    return false;
        //}

        private static bool TryComposeWithoutPaging(IQueryable query, out IQueryable countQuery) {
            MethodCallExpression expression = query.Expression as MethodCallExpression;
            Expression expression2 = null;
            if(((expression != null) && (expression.Method.DeclaringType == typeof(Queryable))) && expression.Method.Name.Equals("take", StringComparison.OrdinalIgnoreCase)) {
                expression2 = expression.Arguments[0];
                expression = expression2 as MethodCallExpression;
                if(((expression != null) && (expression.Method.DeclaringType == typeof(Queryable))) && expression.Method.Name.Equals("skip", StringComparison.OrdinalIgnoreCase)) {
                    expression2 = expression.Arguments[0];
                }
            }
            countQuery = null;
            if(expression2 != null) {
                countQuery = query.Provider.CreateQuery(expression2);
                return true;
            }
            return false;
        }

        /// <summary>
        /// 扩展方法，判断查询树是否包含除take、skip、order之外的条件
        /// </summary>
        /// <param name="query">待校验查询树</param>
        /// <returns>是包含条件</returns>
        public static bool HaveFilterWithoutPagingAndOrder(IQueryable query) {
            if(query == null)
                return false;
            MethodCallExpression expression = query.Expression as MethodCallExpression;
            Expression expression2 = null;
            var notExist = true;
            while(notExist && (expression != null && expression.Method.DeclaringType == typeof(Queryable))) {
                notExist = expression.Method.Name.Equals("take", StringComparison.OrdinalIgnoreCase)
                    || expression.Method.Name.Equals("skip", StringComparison.OrdinalIgnoreCase)
                    || expression.Method.Name.Equals("orderby", StringComparison.OrdinalIgnoreCase);
                if(notExist) {
                    expression2 = expression.Arguments[0];
                    expression = expression2 as MethodCallExpression;
                }
            }
            return !notExist;
        }

        // Nested Types
        private class QueryRebaser : ExpressionVisitor {
            // Methods
            public IQueryable Rebase(IQueryable source, IQueryable query) {
                this.root = source.Expression;
                var expression = this.Visit(query.Expression);
                return source.Provider.CreateQuery(expression);
            }

            // Fields
            private Expression root;

            protected override Expression VisitMethodCall(MethodCallExpression m) {
                if((m.Arguments.Count > 0) &&
                   (m.Arguments[0].NodeType == ExpressionType.Constant) &&
                   ((ConstantExpression)m.Arguments[0]).Value is IQueryable) {
                    var list = new List<Expression> { this.root };
                    list.AddRange(m.Arguments.Skip(1));
                    return Expression.Call(m.Method, list.ToArray());
                }
                return base.VisitMethodCall(m);
            }
        }

        class PropertyModifier<TSource> : ExpressionVisitor {

            // Fields
            private Expression root;

            private LambdaExpression fact;

            private readonly bool usePageing;

            internal PropertyModifier(bool usePageing) {
                this.usePageing = usePageing;
            }

            internal IQueryable<TSource> Rebase<TFact>(IQueryable<TSource> source, Expression<Func<TSource, TFact>> filterProperty, IQueryable query) {
                this.root = source.Expression;
                this.fact = filterProperty;
                var expression = this.Visit(query.Expression);
                return source.Provider.CreateQuery<TSource>(expression);
            }

            protected override Expression VisitMember(MemberExpression node) {
                // 根节点访问属性时，用实际的表达式替换，如 param0.Address.City == "Oxnard", param0为SaleOrder
                // 换成 t.saleOrder.Address.City == "Oxnard", t为服务端查询的临时实体
                if(node.Expression.Type == fact.ReturnType && node.Expression.NodeType == ExpressionType.Parameter)
                    return Expression.MakeMemberAccess(this.fact.Body, node.Member);
                return base.VisitMember(node);
            }

            protected override Expression VisitMethodCall(MethodCallExpression m) {
                if(m.Arguments.Count != 2)
                    return base.VisitMethodCall(m);
                var list = new List<Expression>();
                list.Add(m.Arguments[0].NodeType == ExpressionType.Constant ? this.root : this.Visit(m.Arguments[0]));
                var secondParam = this.Visit(m.Arguments[1]);
                var ua = secondParam as UnaryExpression;
                if(ua != null && ua.Operand.NodeType == ExpressionType.Lambda)
                    secondParam = Expression.Lambda(((LambdaExpression)ua.Operand).Body, this.fact.Parameters[0]);
                list.Add(secondParam);
                MethodInfo methodInfo;
                // 客户端目前提供的方法参考 https://msdn.microsoft.com/en-us/library/system.servicemodel.domainservices.client.entityqueryable(v=vs.91).aspx
                switch(m.Method.Name) {
                    case "Where":
                        methodInfo = GetWhereMethodInfo();
                        break;
                    case "OrderBy":
                    case "OrderByDescending":
                    case "ThenBy":
                    case "ThenByDescending":
                        methodInfo = GetMethodInfo(m.Method.Name, ((LambdaExpression)secondParam).ReturnType);
                        break;
                    default:
                        if(!usePageing)
                            return list[0];
                        // Skip 和 Take，只有一个方法定义
                        methodInfo = typeof(Queryable).GetMethod(m.Method.Name).MakeGenericMethod(typeof(TSource));
                        break;
                }
                return Expression.Call(methodInfo, list.ToArray());
            }

            /// <summary>
            /// 获取方法 Queryable.Where&lt;TSource,Func&lt;TSource,bool&gt;&gt;
            /// </summary>
            /// <returns></returns>
            private MethodInfo GetWhereMethodInfo() {
                MethodInfo result = null;
                var methodInfoes = typeof(Queryable).GetMethods().Where(m => m.Name == "Where" && m.IsGenericMethod);
                foreach(var method in methodInfoes) {
                    var pas = method.GetParameters().Single(p => p.Name == "predicate");
                    // Where的predicate有三参数版本，这里取二参数版
                    if(pas.ParameterType.GenericTypeArguments.First().GenericTypeArguments[1] == typeof(bool))
                        result = method.MakeGenericMethod(typeof(TSource));
                }
                return result;
            }

            /// <summary>
            /// 获取 OrderBy、OrderByDescending、ThenBy、ThenByDescending 的二参数版本
            /// </summary>
            /// <param name="methodName"></param>
            /// <param name="returnType"></param>
            /// <returns></returns>
            private MethodInfo GetMethodInfo(string methodName, Type returnType) {
                return typeof(Queryable)
                    .GetMethods()
                    .Single(m => m.Name == methodName && m.IsGenericMethod && m.GetParameters().Length == 2)
                    .MakeGenericMethod(typeof(TSource), returnType);
            }
        }
    }
}