﻿using System.Collections.Generic;
using System.Linq.Expressions;

namespace System.Linq {
    public static class WhereByMultiOrExtend {
        /// <summary>
        /// 多个or条件过滤， 如 Boms.Where(b=> (b.Code == "a" and b.Name == "A") or (b.Code == "b" and b.Name == "B"))
        /// <para> filters 可以是查询出来的。</para>
        /// </summary>
        /// <typeparam name="T">被过滤的数据类型</typeparam>
        /// <typeparam name="F">条件的类型</typeparam>
        /// <param name="entitySet">数据集</param>
        /// <param name="filters">过滤条件集合</param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        /// 例如：
        /// <c>
        /// var filters = new Dictionary&lt;string, string&gt;();
        /// filters.Add("C70GCCC4A004YAC", "1.0");
        /// filters.Add("C30DABC1Q003KAF", "1.0");
        /// ObjectContext.DomEditions.WhereByMultiOr(filters.ToList(), (b, f) =&gt; b.ProductCode == f.Key &amp;&amp; b.BatchCode == f.Value);
        /// </c>
        public static IQueryable<T> WhereByMultiOr<T, F>(this IQueryable<T> entitySet, IEnumerable<F> filters, Expression<Func<T, F, bool>> predicate) {
            var innerFilters = filters.ToArray();
            if(innerFilters == null || !innerFilters.Any())
                throw new ArgumentOutOfRangeException("filters");
            //条件参数数据常量化，所以参数只保留 被过滤的数据
            var pe = predicate.Parameters.First();
            var orElseExpressions = innerFilters.Select(filter => new ParameterModifier<F>().Modify(predicate, filter)).ToList();
            var predicateBody = orElseExpressions.First();
            if(orElseExpressions.Count > 1) {
                for(var i = 1; i < orElseExpressions.Count; i++) {
                    predicateBody = Expression.OrElse(predicateBody, orElseExpressions[i]);
                }
            }

            var whereCallExpression = Expression.Call(
                typeof(Queryable),
                "Where",
                new[] { typeof(T) },
                entitySet.Expression,
                Expression.Lambda<Func<T, bool>>(predicateBody, pe)
            );
            return entitySet.Provider.CreateQuery<T>(whereCallExpression);
        }

        public class ParameterModifier<F> : ExpressionVisitor {

            private F localParam;

            /// <summary>
            /// 将predicate中用到的F里的数据作为常量放到表达式里。如 F 是 KeyValueItem， key="abc"， value = "1.0"，
            /// <para> 则表达式 (b, f) =&gt; b.ProductCode == f.Key &amp;&amp; b.BatchCode == f.Value)</para>
            /// 变成 (b.ProductCode == "abc" &amp;&amp; b.BatchCode == "1.0")
            /// </summary>
            /// <param name="expression">表达式</param>
            /// <param name="param">条件对象</param>
            /// <returns></returns>
            public Expression Modify(Expression expression, F param) {
                localParam = param;
                return Visit(expression);
            }

            protected override Expression VisitBinary(BinaryExpression node) {
                var left = Visit(node.Left);
                var right = Visit(node.Right);
                string memberName;
                if(CheckForParameter(left))
                    left = Expression.Constant(localParam);
                else if(CheckForProperty(left, out memberName))
                    left = Expression.Constant(localParam.GetType().GetProperty(memberName).GetValue(localParam, null));
                if(CheckForParameter(right))
                    right = Expression.Constant(localParam);
                else if(CheckForProperty(right, out memberName))
                    right = Expression.Constant(localParam.GetType().GetProperty(memberName).GetValue(localParam, null));
                return Expression.MakeBinary(node.NodeType, left, right);
            }

            /// <summary>
            /// 常量访问判断
            /// </summary>
            /// <param name="e"></param>
            /// <returns></returns>
            private static bool CheckForParameter(Expression e) {
                var pe = e as ParameterExpression;
                return pe != null;
            }

            /// <summary>
            /// 属性访问判断
            /// </summary>
            /// <param name="e"></param>
            /// <param name="memberName"></param>
            /// <returns></returns>
            private static bool CheckForProperty(Expression e, out string memberName) {
                memberName = string.Empty;
                var me = e as MemberExpression;
                if(me == null || me.Expression.Type != typeof(F))
                    return false;
                memberName = me.Member.Name;
                return true;
            }

            /// <summary>
            /// 因为要去掉参数，这里只取Body
            /// </summary>
            /// <typeparam name="T"></typeparam>
            /// <param name="node"></param>
            /// <returns></returns>
            protected override Expression VisitLambda<T>(Expression<T> node) {
                return this.Visit(node.Body);
            }
        }
    }
}