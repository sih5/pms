﻿using System;
using System.Globalization;
using System.IO;
using System.Web;
using System.Web.Configuration;

namespace Sunlight.Silverlight.Web.Extensions.Storages {
    public abstract class BaseStorage : IDisposable {
        private const string TEMP_CATEGORY = "Temp";
        protected const string FTPDEFAULTSERVER = "FtpDefaultServer";
        private const int FOLDERS_NUMBER = 1000;

        private string category;

        /// <summary>
        ///     文件分类，不会为空，默认返回值为 <see cref="TEMP_CATEGORY" />
        /// </summary>
        protected string Category {
            set {
                this.category = string.IsNullOrEmpty(value) ? TEMP_CATEGORY : value;
            }
            get {
                if(string.IsNullOrEmpty(this.category))
                    this.category = TEMP_CATEGORY;
                return this.category;
            }
        }

        /// <summary>
        ///   文件所在FTP服务器配置Key不会为空
        /// </summary>
        protected string FtpServerKey;
        /// <summary>
        ///     文件所属企业编号，允许为空
        /// </summary>
        protected string EnterpriseCode;

        /// <summary>
        ///     文件上传日期,年-月,允许为空
        /// </summary>
        protected string DateTime;

        /// <summary>
        ///     文件名，不应为空
        /// </summary>
        protected string FileName;

        protected bool isOldVersion = false;

        protected string Subfolder {
            get {
                return isOldVersion ? "" : (Math.Abs(FileName.GetHashCode()) % FOLDERS_NUMBER).ToString(CultureInfo.InvariantCulture);
            }
        }

        /// <summary>
        ///    获取系统默认指定的Ftp服务器key
        /// </summary>
        /// <returns>返回系统默认Ftp服务器key</returns>
        public string GetFtpDefaultServer() {
            string ftpDefaultKey = WebConfigurationManager.AppSettings[FTPDEFAULTSERVER];
            if(string.IsNullOrEmpty(ftpDefaultKey))
                throw new HttpException(503, string.Format("服务端参数[{0}]设置错误，请联系管理员。", FTPDEFAULTSERVER));
            return ftpDefaultKey;
        }

        /// <summary>
        ///     在附件写入前，准备用于存储附件的目录。
        /// </summary>
        /// <returns>准备操作的结果，true 为成功，false 为失败。</returns>
        public abstract bool PrepareStorageFolder();

        /// <summary>
        ///     获取附件的文件名。
        /// </summary>
        /// <returns>附件的文件名</returns>
        public abstract string GetFileName();

        /// <summary>
        ///     获取附件应存储的目标目录。
        /// </summary>
        /// <returns></returns>
        public abstract string GetTargetFolder();

        /// <summary>
        ///     获取附件相对于根目录的相对路径。
        /// </summary>
        /// <returns>文件的相对路径</returns>
        public abstract string GetRelativePath();

        /// <summary>
        ///     保存文件的某个数据块
        /// </summary>
        /// <param name="position">数据块所应写入的位置</param>
        /// <param name="buffer">数据块的内容</param>
        /// <param name="contentLength">数据块的长度</param>
        /// <returns>操作结果</returns>
        public abstract bool SaveChunkData(long position, byte[] buffer, int contentLength);

        /// <summary>
        ///     完成文件所有数据块的保存操作。
        /// </summary>
        /// <returns>操作结果</returns>
        public abstract bool FinishSaveChunkData();

        /// <summary>
        ///     获取用于文件读取的流，不支持 <see cref="Stream" />.<see cref="Stream.Seek" /> 操作。
        /// </summary>
        /// <returns></returns>
        public abstract Stream GetStreamForRead();

        /// <summary>
        ///     获取用于文件写入的流。
        /// </summary>
        /// <returns></returns>
        public abstract Stream GetStreamForWrite();

        /// <summary>
        ///     获取用于文件读取的流，支持 <see cref="Stream" />.<see cref="Stream.Seek" /> 操作。
        /// </summary>
        /// <remarks>
        ///     此方法将文件内容读取到内存的方式来支持检索，故应谨慎使用。
        /// </remarks>
        /// <returns></returns>
        public abstract Stream GetSeekableStreamForRead();

        /// <summary>
        ///     释放被占用的资源
        /// </summary>
        public abstract void Dispose();
    }
}
