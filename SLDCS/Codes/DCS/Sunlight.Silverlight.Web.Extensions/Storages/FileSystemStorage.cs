﻿using System;
using System.Globalization;
using System.IO;
using System.Web;
using System.Web.Configuration;

namespace Sunlight.Silverlight.Web.Extensions.Storages {
    internal class FileSystemStorage : BaseStorage {
        private const string SETTING_KEY = "AttachmentFolder";
        private string rootDir;

        private string RootDir {
            get {
                if(string.IsNullOrEmpty(this.rootDir)) {
                    var directory = WebConfigurationManager.AppSettings[SETTING_KEY];
                    if(string.IsNullOrWhiteSpace(directory) && !Path.IsPathRooted(directory))
                        this.rootDir = string.Empty;
                    else
                        this.rootDir = directory;
                    if(!this.rootDir.EndsWith(Path.DirectorySeparatorChar.ToString(CultureInfo.InvariantCulture))
                        && !this.rootDir.EndsWith(Path.AltDirectorySeparatorChar.ToString(CultureInfo.InvariantCulture)))
                        this.rootDir = this.rootDir + Path.DirectorySeparatorChar;
                }
                return this.rootDir;
            }
        }

        /// <summary>
        /// 实际存储文件的位置
        /// </summary>
        private string StorageTargetFolder {
            get {
                return Path.Combine(this.RootDir, this.Category, this.EnterpriseCode, this.Subfolder);
            }
        }

        private string StorageFilePath {
            get {
                return Path.Combine(StorageTargetFolder, this.FileName);
            }
        }

        public FileSystemStorage(string category, string enterpriseCode, string fileName) {
            this.Category = category;
            this.EnterpriseCode = enterpriseCode;
            this.FileName = fileName;

            if(this.RootDir == null)
                throw new HttpException(503, string.Format("服务端未配置参数[{0}]，请联系管理员。", SETTING_KEY));
            if(string.IsNullOrWhiteSpace(this.RootDir))
                throw new HttpException(503, string.Format("服务端参数[{0}]不允许为空，请联系管理员。", SETTING_KEY));
        }

        public override bool PrepareStorageFolder() {
            var folderPath = StorageTargetFolder;
            if(!Directory.Exists(folderPath))
                Directory.CreateDirectory(folderPath);
            return Directory.Exists(folderPath);
        }

        public override string GetFileName() {
            return this.FileName;
        }

        public override string GetTargetFolder() {
            return Path.Combine(this.RootDir, this.Category, this.EnterpriseCode);
        }

        public override string GetRelativePath() {
            var relativePath = new Uri(this.RootDir).MakeRelativeUri(new Uri(Path.Combine(this.GetTargetFolder(), this.FileName)));
            return Uri.UnescapeDataString(relativePath.ToString());
        }

        public override bool SaveChunkData(long position, byte[] buffer, int contentLength) {
            using(var targetStream = File.OpenWrite(this.StorageFilePath)) {
                targetStream.Position = position;
                targetStream.Write(buffer, 0, contentLength);
            }
            return true;
        }

        public override bool FinishSaveChunkData() {
            return true;
        }

        public override Stream GetStreamForRead() {
            if(!File.Exists(this.StorageFilePath) && !isOldVersion)
                isOldVersion = true;
            return File.OpenRead(this.StorageFilePath);
        }

        public override Stream GetStreamForWrite() {
            this.PrepareStorageFolder();
            return File.OpenWrite(this.StorageFilePath);
        }

        public override Stream GetSeekableStreamForRead() {
            return GetStreamForRead();
        }

        public override void Dispose() {
        }
    }
}
