﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Configuration;

namespace Sunlight.Silverlight.Web.Extensions.Storages {
    internal class FtpStorage : BaseStorage {
        private const string USERNAME = "FtpUserName";
        private const string PASSWORD = "FtpPassword";

        private string server;

        private string Server {
            get {
                if(string.IsNullOrEmpty(server)) {
                    var ftp = WebConfigurationManager.AppSettings[this.FtpServerKey];
                    if(string.IsNullOrWhiteSpace(ftp) && !Uri.IsWellFormedUriString(ftp, UriKind.Absolute))
                        this.server = string.Empty;
                    else
                        this.server = ftp;
                    if(!this.server.EndsWith(Path.AltDirectorySeparatorChar.ToString(CultureInfo.InvariantCulture)))
                        this.server = this.server + Path.AltDirectorySeparatorChar.ToString(CultureInfo.InvariantCulture);
                }
                return server;
            }
        }

        private readonly string userName = WebConfigurationManager.AppSettings[USERNAME];

        private readonly string password = WebConfigurationManager.AppSettings[PASSWORD];

        private int targetFolderUriIndex;
        private Uri[] uris;

        /// <summary>
        /// 用于保存Ftp文件对应的每层目录的Uri，首层为Ftp主目录，最后一层为文件存储完整路径
        /// </summary>
        private Uri[] Uris {
            get {
                if(uris == null) {
                    var uriList = new List<Uri>();
                    uriList.Add(new Uri(this.Server));
                    uriList.Add(new Uri(uriList.Last(), this.Category + "/"));
                    if(!string.IsNullOrEmpty(DateTime))
                        uriList.Add(new Uri(uriList.Last(), this.DateTime + "/"));
                    if(!string.IsNullOrEmpty(EnterpriseCode))
                        uriList.Add(new Uri(uriList.Last(), this.EnterpriseCode + "/"));

                    targetFolderUriIndex = uriList.Count - 1;

                    if(!string.IsNullOrEmpty(Subfolder))
                        uriList.Add(new Uri(uriList.Last(), this.Subfolder + "/"));
                    uriList.Add(new Uri(uriList.Last(), this.FileName));
                    uris = uriList.ToArray();
                }
                return uris;
            }
        }

        /// <summary>
        /// 需要Dispose的WebClient
        /// </summary>
        private readonly List<WebClient> webClients = new List<WebClient>();

        private void CreateDirectoryIfNotExist(Uri uri) {
            try {
                var request = (FtpWebRequest)WebRequest.Create(uri);
                request.Method = WebRequestMethods.Ftp.MakeDirectory;
                request.Credentials = new NetworkCredential(this.userName, this.password);
                using(var response = (FtpWebResponse)request.GetResponse()) {
                }
            } catch(WebException) {
                //文件已存在报错
                //using(var response = (FtpWebResponse)ex.Response) {
                //    Debug.Write(response.StatusCode, "FTP");
                //}
            }
        }

        public FtpStorage(string category, string dateTime, string enterpriseCode, string fileName, string ftpServerKey) {
            this.Category = category;
            this.EnterpriseCode = enterpriseCode;
            this.FileName = fileName;
            this.DateTime = dateTime;
            this.FtpServerKey = ftpServerKey;
            if(string.IsNullOrEmpty(ftpServerKey)) {
                this.FtpServerKey = GetFtpDefaultServer();
            }
            if(string.IsNullOrEmpty(this.FtpServerKey))
                throw new HttpException(503, string.Format("服务端参数[{0}]设置错误，请联系管理员。", FtpServerKey));
            if(string.IsNullOrEmpty(this.userName))
                throw new HttpException(503, string.Format("服务端未配置参数[{0}]，请联系管理员。", USERNAME));
            if(string.IsNullOrEmpty(this.password))
                throw new HttpException(503, string.Format("服务端未配置参数[{0}]，请联系管理员。", PASSWORD));
        }

        public override bool PrepareStorageFolder() {
            for(var i = 1; i < Uris.Length - 1; i++)
                CreateDirectoryIfNotExist(Uris[i]);
            return true;
        }

        public override string GetFileName() {
            return this.FileName;
        }

        public override string GetTargetFolder() {
            return Uris[targetFolderUriIndex].ToString();
        }

        public override string GetRelativePath() {
            var relativePath = Uris[0].MakeRelativeUri(new Uri(Uris[targetFolderUriIndex], this.FileName));
            return Uri.UnescapeDataString(relativePath.ToString());
        }

        public override bool SaveChunkData(long position, byte[] buffer, int contentLength) {
            using(var webClient = new WebClient()) {
                webClient.Credentials = new NetworkCredential(this.userName, this.password);
                var method = position == 0 ? WebRequestMethods.Ftp.UploadFile : WebRequestMethods.Ftp.AppendFile;
                using(var stream = webClient.OpenWrite(Uris[Uris.Length - 1], method)) {
                    stream.Write(buffer, 0, contentLength);
                }
            }
            return true;
        }

        public override bool FinishSaveChunkData() {
            return true;
        }

        public override Stream GetStreamForRead() {
            var webClient = new WebClient {
                Credentials = new NetworkCredential(this.userName, this.password)
            };
            webClients.Add(webClient);

            try {
                return webClient.OpenRead(Uris[Uris.Length - 1]);
            } catch(WebException) {
                isOldVersion = true;
                uris = null;
                return webClient.OpenRead(Uris[Uris.Length - 1]);
            }
        }

        public override Stream GetStreamForWrite() {
            PrepareStorageFolder();
            var webClient = new WebClient {
                Credentials = new NetworkCredential(this.userName, this.password)
            };
            webClients.Add(webClient);
            return webClient.OpenWrite(Uris[Uris.Length - 1], WebRequestMethods.Ftp.UploadFile);
        }

        public override Stream GetSeekableStreamForRead() {
            using(var stream = GetStreamForRead()) {
                var memoryStream = new MemoryStream();
                stream.CopyTo(memoryStream);
                memoryStream.Position = 0;
                return memoryStream;
            }
        }

        public override void Dispose() {
            foreach(var webClient in webClients)
                webClient.Dispose();
        }
    }
}
