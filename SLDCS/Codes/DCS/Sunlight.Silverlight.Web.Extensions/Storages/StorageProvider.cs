﻿using System.IO;
using System.Web.Configuration;

namespace Sunlight.Silverlight.Web.Extensions.Storages {
    public class StorageProvider {
        ///// <summary>
        /////     根据网站的配置文件来返回用于存储文件的 Storage。
        ///// </summary>
        ///// <param name="path">文件的相对路径</param>
        ///// <returns>文件此附件的 Storage</returns>
        //public static BaseStorage GetStorage(string path) {
        //    var param = path.Split('/', '\\');
        //    //判断文件文类
        //    if(param[0] == "Products") {
        //        return GetStorage(param.Length > 1 ? param[0] : "", param.Length > 2 ? param[1] : "", param.Length > 3 ? param[2] : "", Path.GetFileName(path), null);
        //    }
        //    return GetStorage(param.Length > 1 ? param[0] : "", string.Empty, param.Length > 2 ? param[1] : "", Path.GetFileName(path), null);
        //}
        /// <summary>
        ///     根据网站的配置文件来返回用于存储文件的 Storage。
        /// </summary>
        /// <param name="path">文件的相对路径</param>
        /// <param name="ftpServerkey">文件所在ftp配置key,如果为空则默认取配置的默认FtpDefaultServer</param>
        /// <returns>文件此附件的 Storage</returns>
        public static BaseStorage GetStorage(string path, string ftpServerkey) {
            var param = path.Split('/', '\\');
            //判断文件文类
            if(param[0] == "Products") {
                return GetStorage(param.Length > 1 ? param[0] : "", param.Length > 2 ? param[1] : "", param.Length > 3 ? param[2] : "", Path.GetFileName(path), ftpServerkey);
            }
            return GetStorage(param.Length > 1 ? param[0] : "", string.Empty, param.Length > 2 ? param[1] : "", Path.GetFileName(path), ftpServerkey);
        }

        /// <summary>
        ///     根据网站的配置文件来返回用于存储文件的 Storage，默认为 <see cref="FileSystemStorage" />。
        /// </summary>
        /// <param name="category">文件分类</param>
        /// <param name="dateTime"></param>
        /// <param name="enterpriseCode">文件所属企业编号</param>
        /// <param name="fileName">文件名</param>
        /// <param name="ftpServerKey">文件所在ftp配置key</param>
        /// <returns>存储此文件的 Storage</returns>
        public static BaseStorage GetStorage(string category, string dateTime, string enterpriseCode, string fileName, string ftpServerKey) {
            switch(WebConfigurationManager.AppSettings["DeploymentType"]) {
                case "FTP":
                    return new FtpStorage(category, dateTime, enterpriseCode, fileName, ftpServerKey);
                default:
                    return new FileSystemStorage(category, enterpriseCode, fileName);
            }
        }
    }
}
