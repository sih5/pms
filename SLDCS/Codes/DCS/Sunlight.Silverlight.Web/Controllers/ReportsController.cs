namespace Sunlight.Silverlight.Web.Controllers {
    using System.IO;
    using System.Web;
    using Telerik.Reporting.Cache.File;
    using Telerik.Reporting.Services;
    using Telerik.Reporting.Services.WebApi;

    //The class name determines the service URL.
    //ReportsController class name defines /api/report/ service URL.
    public class ReportsController : ReportsControllerBase {
        static ReportServiceConfiguration configurationInstance;

        static ReportsController() {
            //This is the folder that contains the XML (trdx) report definitions
            //In this case this is the Reports folder
            var appPath = HttpContext.Current.Server.MapPath("~/");
            var reportsPath = Path.Combine(appPath, "Reports");

            //Add resolver for trdx report definitions,
            //then add resolver for class report definitions as fallback resolver;
            //finally create the resolver and use it in the ReportServiceConfiguration instance.
            var resolver = new ReportFileResolver(reportsPath)
                .AddFallbackResolver(new ReportTypeResolver());

            //Setup the ReportServiceConfiguration
            configurationInstance = new ReportServiceConfiguration {
                HostAppId = "Html5App",
                Storage = new FileStorage(),
                ReportResolver = resolver,
                // ReportSharingTimeout = 0,
                // ClientSessionTimeout = 15,
            };
        }

        public ReportsController() {
            //Initialize the service configuration
            this.ReportServiceConfiguration = configurationInstance;
        }

        private bool IsUserAuthorized() {
            return this.User != null && this.User.Identity.IsAuthenticated;
        }

        public override System.Net.Http.HttpResponseMessage GetDocumentInfo(string clientID, string instanceID, string documentID) {
            if(!this.IsUserAuthorized())
                throw new HttpException(401, "身份认证已失效，请登录系统后再次尝试。");
            return base.GetDocumentInfo(clientID, instanceID, documentID);
        }
    }
}