﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<%@ Import Namespace="System.Globalization" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Net.NetworkInformation" %>
<script runat="server">
    const string XAP_PATH = "ClientBin/SPMS.xap";
    private string GetXapUrl() {
        var xapFilePath = this.Server.MapPath(XAP_PATH);
        return string.Format("{0}?{1}", XAP_PATH, File.Exists(xapFilePath) ? File.GetLastWriteTime(xapFilePath).ToString("yyyyMMddHHmmss") : string.Empty);
    }

    private string GetXapModifiedDate() {
        var xapFilePath = this.Server.MapPath(XAP_PATH);
        var fileInfo = new FileInfo(xapFilePath);
        return fileInfo.LastWriteTime.ToString("MMdd");

    }

    private string GetInitParams() {
        var initParams = new Dictionary<string, string>(2);
        initParams.Add("Update", this.GetXapModifiedDate());
        string userIp = "";
        string url = Request.Url.AbsoluteUri; 
        //userIp = this.Request.ServerVariables["HTTP_VIA"] != null ? this.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString(CultureInfo.InvariantCulture) : this.Request.ServerVariables["REMOTE_ADDR"].ToString(CultureInfo.InvariantCulture);
        userIp = !string.IsNullOrEmpty(this.Request.ServerVariables["HTTP_X_FORWARDED_FOR"]) ? this.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString(CultureInfo.InvariantCulture) : this.Request.ServerVariables["REMOTE_ADDR"].ToString(CultureInfo.InvariantCulture);
        
        initParams.Add("IP", userIp);
        initParams.Add("IdmSupplierAddress", System.Web.Configuration.WebConfigurationManager.AppSettings["IdmSupplierAddress"]);
        initParams.Add("IdmBranchAddress", System.Web.Configuration.WebConfigurationManager.AppSettings["IdmBranchAddress"]);
        initParams.Add("MacAddPassword", ConfigurationManager.AppSettings["MacPassword"]);
        initParams.Add("Url", url);
        return string.Join(",", initParams.Select(kv => kv.Key + "=" + kv.Value));
    }
    
</script>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Loading</title>
    <style type="text/css">
        html, body {
            height: 100%;
            overflow: hidden;
        }

        body {
            padding: 0;
            margin: 0;
        }

        #silverlightControlHost {
            height: 100%;
            text-align: center;
        }
    </style>
    <script type="text/javascript" src="Silverlight.js"> </script>
    <script type="text/javascript">
        function appLoad(sender, args) {
            var xamlObject = document.getElementById("silverlightObject");
            if (xamlObject != null)
                xamlObject.focus();
        }

        function onSilverlightError(sender, args) {
            var appSource = "";
            if (sender != null && sender != 0) {
                appSource = sender.getHost().Source;
            }

            var errorType = args.ErrorType;
            var iErrorCode = args.ErrorCode;

            if (errorType == "ImageError" || errorType == "MediaError") {
                return;
            }

            var errMsg = "Silverlight 应用程序中未处理的错误 " + appSource + "\n";

            errMsg += "代码: " + iErrorCode + "    \n";
            errMsg += "类别: " + errorType + "       \n";
            errMsg += "消息: " + args.ErrorMessage + "     \n";

            if (errorType == "ParserError") {
                errMsg += "文件: " + args.xamlFile + "     \n";
                errMsg += "行: " + args.lineNumber + "     \n";
                errMsg += "位置: " + args.charPosition + "     \n";
            } else if (errorType == "RuntimeError") {
                if (args.lineNumber != 0) {
                    errMsg += "行: " + args.lineNumber + "     \n";
                    errMsg += "位置: " + args.charPosition + "     \n";
                }
                errMsg += "方法名称: " + args.methodName + "     \n";
            }

            throw new Error(errMsg);
        }
    </script>
</head>
<body>
    <form id="form1" runat="server" style="height: 100%">
        <div id="silverlightControlHost">
            <object data="data:application/x-silverlight-2," type="application/x-silverlight-2" id="silverlightObject"
                width="100%" height="100%">
                <param name="source" value="<%=this.GetXapUrl()%>" />
                <param name="initparams" value="UpdateDate=<%=this.GetInitParams()%>" />
                <param name="onError" value="onSilverlightError" />
                <param name="background" value="white" />
                <param name="minRuntimeVersion" value="5.0.61118.0" />
                <param name="autoUpgrade" value="true" />
                <param name="enableHtmlAccess" value="true" />
                <param name="enableGPUAcceleration" value="true" />
                <param name="enableFrameRateCounter" value="false" />
                <param name="onLoad" value="appLoad" />
                <a href="http://go.microsoft.com/fwlink/?LinkID=149156&v=5.0.61118.0" style="text-decoration: none">
                    <img src="http://go.microsoft.com/fwlink/?LinkId=161376" alt="获取 Microsoft Silverlight"
                        style="border-style: none" />
                </a>
            </object>
            <iframe id="_sl_historyFrame" style="visibility: hidden; height: 0; width: 0; border: 0"></iframe>
        </div>
    </form>
</body>
</html>
