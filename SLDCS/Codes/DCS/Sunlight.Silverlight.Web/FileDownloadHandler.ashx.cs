﻿using System;
using System.IO;
using System.Net;
using System.Web;
using System.Web.Configuration;
using System.Web.SessionState;
using Sunlight.Silverlight.Web.Extensions.Storages;

namespace Sunlight.Silverlight.Web {

    /// <summary>
    ///     负责处理下载文件的请求
    /// </summary>
    public class FileDownloadHandler : IHttpHandler, IReadOnlySessionState {

        private HttpContext Context {
            get;
            set;
        }

        public bool IsReusable {
            get {
                return false;
            }
        }

        private bool IsUserAuthorized() {
            return this.Context != null && this.Context.User != null && this.Context.User.Identity.IsAuthenticated;
        }

        public void ProcessRequest(HttpContext context) {
            this.Context = context;
            try {
                if(!this.IsUserAuthorized())
                    throw new HttpException(401, "身份认证已失效，请登录系统后再次尝试下载文件。");


                var ftpServerKey = context.Request.QueryString["FtpServerKey"] ?? string.Empty;

                var path = context.Request.QueryString["Path"] ?? string.Empty;
                if(string.IsNullOrEmpty(path))
                    throw new HttpException(400, "请使用Path参数指定需下载的文件路径");

                var fileName = Path.GetFileName(path);
                if(!string.IsNullOrEmpty(fileName) && (context.Request.Browser.Browser == "IE" || context.Request.Browser.Browser == "InternetExplorer"))
                    fileName = Uri.EscapeDataString(fileName);
                context.Response.Clear();
                context.Response.AddHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
                context.Response.ContentType = "application/octet-stream";

                using(var storage = StorageProvider.GetStorage(path, ftpServerKey)) {
                    using(var stream = storage.GetStreamForRead()) {
                        //context.Response.AddHeader("Content-Length", stream.Length.ToString(CultureInfo.InvariantCulture));
                        stream.CopyTo(context.Response.OutputStream);
                    }
                }
            } catch(HttpException ex) {
                context.Response.StatusCode = ex.GetHttpCode();
                context.Response.StatusDescription = ex.Message;
            } catch(DirectoryNotFoundException) {
                context.Response.StatusCode = (int)HttpStatusCode.NotFound;
                context.Response.StatusDescription = string.Format("您所要下载的文件不存在。");
            } catch(FileNotFoundException) {
                context.Response.StatusCode = (int)HttpStatusCode.NotFound;
                context.Response.StatusDescription = string.Format("您所要下载的文件不存在。");
            } catch(Exception ex) {
                context.Response.StatusCode = 503;
                context.Response.StatusDescription = ex.Message;
            } finally {
                context.ApplicationInstance.CompleteRequest();
            }
        }
    }
}