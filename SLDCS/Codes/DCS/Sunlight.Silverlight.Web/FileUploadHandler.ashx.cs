﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Web.Extensions.Storages;
using Telerik.Windows;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Web {

    /// <summary>
    ///     负责处理上传文件的请求
    /// </summary>
    public class FileUploadHandler : RadUploadHandler {
        private MD5CryptoServiceProvider md5Hasher;
        private BaseStorage storage;

        private readonly string[] pathCategoryItems = {
            "Personnels",
            "Products",
            "ImportedFiles"
        };

        private MD5CryptoServiceProvider MD5Hasher {
            get {
                return this.md5Hasher ?? (this.md5Hasher = new MD5CryptoServiceProvider());
            }
        }

        private string fileName;

        private string FileName {
            get {
                if(string.IsNullOrEmpty(fileName)) {
                    fileName = this.GetQueryParameter(RadUploadConstants.ParamNameFileName);
                    if(this.IsNewFileRequest())
                        this.ResultChunkTag = DateTime.Now.ToString("yyMMdd_HHmmss_fff");
                    else if(this.FormChunkTag != null)
                        this.ResultChunkTag = this.FormChunkTag;
                    if(this.ResultChunkTag != null)
                        fileName = string.Format("{0}_{1}{2}", Path.GetFileNameWithoutExtension(fileName), this.ResultChunkTag, Path.GetExtension(fileName));
                }
                return fileName;
            }
        }

        private string GetMd5Hash(string input) {
            var data = this.MD5Hasher.ComputeHash(Encoding.Default.GetBytes(input));
            return BitConverter.ToString(data, 4, 8).Replace("-", "");
        }

        public override void Initialize() {
            base.Initialize();
            var category = this.GetQueryParameter("Category") ?? string.Empty;
            if(!string.IsNullOrEmpty(category) && !pathCategoryItems.Contains(category))
                category = this.GetMd5Hash(category);
            var enterpriseCode = Utils.GetCurrentUserInfo().EnterpriseCode;
            var dateTime = "";
            if(!string.IsNullOrEmpty(category) && category == "Products") {
                dateTime = DateTime.Now.ToString("yyyy-MM");
            }
            storage = StorageProvider.GetStorage(category, dateTime, enterpriseCode, this.FileName, null);
        }

        public override string GetFileName() {
            return storage.GetFileName();
        }

        public override string GetTargetFolder() {
            return storage.GetTargetFolder();
        }

        public override Dictionary<string, object> GetAssociatedData() {
            var associatedData = base.GetAssociatedData();
            if(this.IsFinalFileRequest())
                associatedData.Add("Path", storage.GetRelativePath());
            associatedData.Add("FtpServerKey", storage.GetFtpDefaultServer());
            return associatedData;
        }

        public override bool SaveChunkData(string filePath, long position, byte[] buffer, int contentLength, out int savedBytes) {
            if(!HttpContext.Current.User.Identity.IsAuthenticated) {
                this.AddReturnFileParam(RadUploadConstants.ParamNameMessage, "身份认证失效，请登录系统后再次尝试上传文件。");
                this.AddReturnFileParam(RadUploadConstants.ParamNameSuccess, false);
                this.AddReturnFileParam(RadUploadConstants.ParamNameFileName, this.GetQueryParameter(RadUploadConstants.ParamNameFileName));
                this.AddReturnFileParam(RadUploadConstants.ParamNameFinalFileRequest, true);
                savedBytes = -1;
                return false;
            }

            if(!storage.PrepareStorageFolder()) {
                this.AddReturnFileParam(RadUploadConstants.ParamNameMessage, "服务端创建上传目录失败，请联系管理员。");
                this.AddReturnFileParam(RadUploadConstants.ParamNameSuccess, false);
                this.AddReturnFileParam(RadUploadConstants.ParamNameFileName, this.GetQueryParameter(RadUploadConstants.ParamNameFileName));
                this.AddReturnFileParam(RadUploadConstants.ParamNameFinalFileRequest, true);
                savedBytes = -1;
                return false;
            }

            try {
                storage.SaveChunkData(position, buffer, contentLength);
                savedBytes = contentLength;
            } catch(Exception e) {
                savedBytes = -1;
                this.AddReturnFileParam("RadUAG_message", string.Format("Cannot save the file: [{0}]", e.Message));
            }

            if(this.IsFinalUploadRequest())
                storage.FinishSaveChunkData();
            return true;
        }
    }
}