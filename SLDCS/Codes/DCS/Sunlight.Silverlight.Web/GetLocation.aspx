﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GetLocation.aspx.cs" Inherits="Sunlight.Silverlight.Web.GetLocation" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <style type="text/css">
        .alignCenter
        {
            align-content: center;
            text-align:center;
            font-size:xx-large
        }

        .TextBoxStyle{
            font-size:xx-large;
            height: 48px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
       <div class="alignCenter">
            纬度：<input type="text" runat="server" id="hdText2" class="TextBoxStyle" />
            <br />
           <br />
            经度：<input type="text" runat="server" id="hdText3"  class="TextBoxStyle" /><br />
            <br />
        </div>
    </form>
    <script>
        navigator.geolocation.getCurrentPosition( // 该函数有如下三个参数
            function (pos) { // 如果成果则执行该回调函数

                var hdText2 = document.getElementById("hdText2");
                var hdText3 = document.getElementById("hdText3");
                try {

                    hdText2.value = pos.coords.latitude;
                    hdText3.value = pos.coords.longitude;




                } catch (error) {
                    // alert(' 请点击按钮，插入经纬度' + error);
                }
            }, function (err) { // 如果失败则执行该回调函数



            }, { // 附带参数

                enableHighAccuracy: false, // 提高精度(耗费资源)

                timeout: 3000, // 超过timeout则调用失败的回调函数

                maximumAge: 1000 // 获取到的地理信息的有效期，超过有效期则重新获取一次位置信息

            }

        );

        function getQueryString(name) {
            var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
            var r = window.location.search.substr(1).match(reg);
            if (r != null) return unescape(r[2]); return null;
        }
    </script>
</body>
</html>