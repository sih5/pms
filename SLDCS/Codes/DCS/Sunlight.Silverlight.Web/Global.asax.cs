﻿using System;
using System.Web;
using System.Web.Http;
using Sunlight.Silverlight.Security.Web;
using Sunlight.Silverlight.Shell.Web;
using Telerik.Reporting.Services.WebApi;

namespace Sunlight.Silverlight.Web {
    public class Global : HttpApplication {
        protected void Application_Start(object sender, EventArgs e) {
            ReportsControllerConfiguration.RegisterRoutes(GlobalConfiguration.Configuration);
            SecurityDomainService.SystemType = SystemTypes.Dcs;

            //if(ConfigurationManager.AppSettings["DeploymentType"] == "Azure")
            LicenseService.IsEnabled = false;
        }
    }
}
