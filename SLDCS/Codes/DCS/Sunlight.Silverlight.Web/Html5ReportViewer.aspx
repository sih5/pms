<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Telerik HTML5 Report Viewer</title>

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <script src="ReportViewer/js/jquery-1.9.1.min.js"></script>
    <link href="ReportViewer/js/kendo.common.min.css" rel="stylesheet" />
    <link href="ReportViewer/js/kendo.blueopal.min.css" rel="stylesheet" />

    <!--kendo.web.min.js or kendo.all.min.js can be used as well instead of the following custom Kendo UI-->
    <script src="ReportViewer/js/kendo.subset.2015.3.930.min.js"></script>

    <script src="ReportViewer/js/telerikReportViewer-9.2.15.1105.min.js"></script>
    <style>
        #reportViewer1 {
            position: absolute;
            left: 5px;
            right: 5px;
            top: 5px;
            bottom: 5px;
            overflow: hidden;
            font-family: Verdana, Arial;
        }
    </style>
</head>
<body>

    <div id="reportViewer1">
        loading...
    </div>

    <script type="text/javascript">
        $(document).ready(function () {
            var decUrl = window.atob(window.location.search.substr(1));
            var params = window.decodeURI(decUrl).split('&');
            // var params = window.location.search.substr(1).split('&');
            var paramsLen = params.length;
            var reprotParams = {};
            var report = null;
            for (var i = 0; i < paramsLen; i++) {
                var splitParam = params[i].split('=');
                if (splitParam.length > 0) {
                    if (splitParam[0] === 'report') {
                        report = splitParam[1];
                        if (report.indexOf('.trdx') === -1)
                            report = 'Sunlight.Silverlight.Dcs.Web.Reporting.' + report + ', Sunlight.Silverlight.Dcs.Web';
                    } else if (splitParam[0] === 'title') {
                        document.title = splitParam[1];
                    } else
                        reprotParams[splitParam[0]] = splitParam[1];
                }
            }

            $("#reportViewer1")
                .telerik_ReportViewer({

                    // The URL of the service which will serve reports.
                    // The URL corresponds to the name of the controller class (ReportsController).
                    // For more information on how to configure the service please check http://www.telerik.com/help/reporting/telerik-reporting-rest-conception.html.
                    serviceUrl: "api/reports/",

                    // The URL for the report viewer template. The template can be edited -
                    // new functionalities can be added and unneeded ones can be removed.
                    // For more information please check http://www.telerik.com/help/reporting/html5-report-viewer-templates.html.
                    templateUrl: 'ReportViewer/templates/telerikReportViewerTemplate-9.2.15.1105.html',

                    //ReportSource - report description
                    reportSource: {

                        // The report can be set to a report file name (trdx report definition)
                        // or CLR type name (report class definition).
                        report: report,

                        // Parameters name value dictionary
                        //parameters: {
                        //    retailContractId: $.getUrlParam("retailContractId"),
                        //}
                        parameters: reprotParams,
                    },

                    // Specifies whether the viewer is in interactive or print preview mode.
                    // PRINT_PREVIEW - Displays the paginated report as if it is printed on paper. Interactivity is not enabled.
                    // INTERACTIVE - Displays the report in its original width and height without paging. Additionally interactivity is enabled.
                    viewMode: telerikReportViewer.ViewModes.PRINT_PREVIEW,

                    // Sets the scale mode of the viewer.
                    // Three modes exist currently:
                    // FIT_PAGE - The whole report will fit on the page (will zoom in or out), regardless of its width and height.
                    // FIT_PAGE_WIDTH - The report will be zoomed in or out so that the width of the screen and the width of the report match.
                    // SPECIFIC - Uses the scale to zoom in and out the report.
                    scaleMode: telerikReportViewer.ScaleModes.SPECIFIC,

                    // Zoom in and out the report using the scale
                    // 1.0 is equal to 100%, i.e. the original size of the report
                    scale: 1.0,

                    ready: function () {
                        //this.refreshReport();
                    },
                });
        });
    </script>

</body>
</html>