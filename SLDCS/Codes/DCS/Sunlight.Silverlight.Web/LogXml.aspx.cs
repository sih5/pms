﻿using System;
using Sunlight.Silverlight.Log.Web;

namespace Sunlight.Silverlight.Web {
    public partial class LogXml : System.Web.UI.Page {
        protected void Page_Load(object sender, EventArgs e) {

        }

        private static LogDomainService service;

        public LogDomainService Service {
            get {
                return service ?? (service = new LogDomainService());
            }
        }

        public string WriteXml(string type, string logInfoId, string logDetailId) {
            if(string.IsNullOrEmpty(logInfoId))
                return null;
            int typeValue;
            if(!int.TryParse(type, out typeValue))
                return null;
            switch(typeValue) {
                case 0: return Service.GetLogInfoParam(logInfoId);
                case 1: return Service.GetLogInfoQueryResult(logInfoId);
                case 2:
                    int logDetailIdValue;
                    return (!int.TryParse(logDetailId, out logDetailIdValue) && logDetailIdValue <= 0) ? null : Service.GetLogInfoDetailParam(logInfoId, logDetailIdValue);
                default: return null;
            }

        }
    }
}