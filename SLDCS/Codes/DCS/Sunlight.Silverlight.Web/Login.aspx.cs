﻿using System;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web.UI;
using System.Xml.Linq;

namespace Sunlight.Silverlight.Web {

    public partial class Login : Page {

        private static string HashPassword(string password) {
            var data = Encoding.UTF8.GetBytes(password ?? string.Empty);
            using(var sha1 = new SHA1Managed())
                data = sha1.ComputeHash(data);
            var sbResult = new StringBuilder(data.Length * 2);
            foreach(var b in data)
                sbResult.AppendFormat("{0:X2}", b);
            return sbResult.ToString();
        }

        protected void Page_Load(object sender, EventArgs e) {
            if(this.Request.ApplicationPath != null && this.Request.QueryString["EnterpriseCode"] != null && this.Request.QueryString["UserName"] != null && this.Request.QueryString["Password"] != null) {
                var request = (HttpWebRequest)WebRequest.Create(this.Request.Url.Scheme + "://" + this.Request.Url.Authority +
                                                                this.Request.ApplicationPath.TrimEnd('/') + "/ClientBin/Sunlight-Silverlight-Security-Web-SecurityDomainService.svc/binary/Login");
                WcfBinaryCode wbc = new WcfBinaryCode();
                var xmlStr = string.Format(@"<Login xmlns=""http://tempuri.org/"">
    <userName>{1}</userName>
    <password>{2}</password>
    <isPersistent>false</isPersistent>
    <customData>&lt;LoginParameters&gt;
    &lt;EnterpriseCode&gt;{0}&lt;/EnterpriseCode&gt;
&lt;/LoginParameters&gt;</customData>
</Login>", this.Request.QueryString["EnterpriseCode"], this.Request.QueryString["UserName"], HashPassword(this.Request.QueryString["Password"]));

                var data = wbc.EncodeBinaryXML(xmlStr);
                request.Method = "POST";
                request.ContentType = "application/msbin1";
                request.ContentLength = data.Length;

                using(var stream = request.GetRequestStream()) {
                    stream.Write(data, 0, data.Length);
                }

                var response = (HttpWebResponse)request.GetResponse();
                var responseStream = response.GetResponseStream();
                byte[] bytes = new byte[int.Parse(response.Headers["Content-Length"])];
                if(responseStream != null) {
                    responseStream.Read(bytes, 0, bytes.Length);
                    var result = wbc.DecodeBinaryXML(bytes);
                    var xml = XDocument.Parse(result);
                    XNamespace aw = "http://schemas.datacontract.org/2004/07/Sunlight.Silverlight.Security.Web";
                    var totalCount = xml.Descendants(aw + "AuthenticationUser").Count();
                    if(totalCount == 1) {
                        this.Response.AddHeader("Set-Cookie", response.Headers["Set-Cookie"]);
                        if(this.Response.IsClientConnected) {
                            // If still connected, redirect
                            // to another page.
                            var redirect = this.Request.QueryString["Redirect"];
                            if(redirect != null) {
                                this.Response.Redirect("#" + redirect, false);
                            } else {
                                this.Response.Redirect("#/home", false);
                            }
                        } else {
                            // If the browser is not connected
                            // stop all response processing.
                            Context.ApplicationInstance.CompleteRequest();
                        }
                    } else {
                        resultTextBox.Text = "用户名或密码错误!";
                    }
                } else {
                    resultTextBox.Text = "登录失败!";
                }
            } else {
                resultTextBox.Text = "参数不正确!";
            }
        }
    }
}