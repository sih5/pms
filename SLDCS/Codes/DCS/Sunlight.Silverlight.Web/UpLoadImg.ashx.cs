﻿using System;
using System.IO;
using System.Web;

namespace Sunlight.Silverlight.Web {

    /// <summary>
    /// UpLoadImg上传图片
    /// </summary>
    public class UpLoadImg : IHttpHandler {

        public void ProcessRequest(HttpContext context) {
            Stream fistream = context.Request.InputStream;
            try {
                string filePath = context.Request.QueryString["filePath"];
                var buffer = new byte[4096];
                using(FileStream fi = File.Create(context.Server.MapPath("~/Client/DCS/Images/Product/" + filePath), 4096)) {
                    int byteread;
                    while((byteread = fistream.Read(buffer, 0, buffer.Length)) > 0) {
                        fi.Write(buffer, 0, byteread);
                    }
                }
            } catch(Exception e) {
                context.Response.Write("上传失败, 错误信息:" + e.Message);
            } finally {
                fistream.Dispose();
            }
        }

        public bool IsReusable {
            get {
                return false;
            }
        }
    }
}