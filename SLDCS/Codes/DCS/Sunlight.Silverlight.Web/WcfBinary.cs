﻿using System;
using System.IO;
using System.Text;
using System.Xml;

namespace Sunlight.Silverlight.Web {

    public class WcfBinaryCode {
        private WcfBinaryCodec m_wcfBinaryCodec = new WcfBinaryCodec(Encoding.UTF8);

        public string DecodeBinaryXML(byte[] encodedXML) {
            return encodedXML == null ? "" : this.m_wcfBinaryCodec.DecodeBinaryXML(encodedXML, false);
        }

        public byte[] EncodeBinaryXML(string xml) {
            return String.IsNullOrEmpty(xml.Trim()) ? null : this.m_wcfBinaryCodec.EncodeBinaryXML(xml);
        }
    }

    public class WcfBinaryCodec {

        public WcfBinaryCodec() {
        }

        public WcfBinaryCodec(Encoding encoding) {
            m_encoding = encoding;
        }

        private Encoding m_encoding = Encoding.UTF8;

        /// <summary>
        /// Decode a bytestream that was encoded by WCF's BinaryEncodingBindingElement.  Will throw if the bytestream does
        /// not decode properly or the result is not valid XML.  I/O streams are flushed but not closed.
        /// </summary>
        /// <param name="xmlOutput"></param>
        /// <param name="explodeNewlines">if true, the returned string will be nicely indented according to
        /// element depth, and each attribute will be placed on its own line</param>
        /// <param name="binaryInput"></param>
        /// <returns></returns>
        public void DecodeBinaryXML(Stream binaryInput, Stream xmlOutput, bool? explodeNewlines) {
            // defaults
            var explode = explodeNewlines ?? false;

            // parse bytestream into the XML DOM
            var doc = new XmlDocument();
            using(var binaryReader = XmlDictionaryReader.CreateBinaryReader(binaryInput, WcfDictionaryBuilder.Dict, XmlDictionaryReaderQuotas.Max)) {
                doc.Load(binaryReader);
            }

            // write document to the output stream with customized settings
            var settings = new XmlWriterSettings {
                CheckCharacters = false,
                CloseOutput = false,
                ConformanceLevel = ConformanceLevel.Auto,
                Encoding = m_encoding,
                Indent = explode,
                IndentChars = "\t",
                NewLineChars = Environment.NewLine,
                NewLineHandling = explode ? NewLineHandling.Replace : NewLineHandling.None,
                NewLineOnAttributes = explode
            };
            using(var writer = XmlWriter.Create(xmlOutput, settings)) {
                doc.Save(writer);
                writer.Flush();
                xmlOutput.Flush();
            }
        }

        public string DecodeBinaryXML(byte[] binaryInput, bool? explodeNewLines) {
            var input = new MemoryStream(binaryInput);
            var output = new MemoryStream();
            DecodeBinaryXML(input, output, explodeNewLines);
            output.Seek(0, SeekOrigin.Begin);
            return new StreamReader(output, m_encoding).ReadToEnd();
        }

        /// <summary>
        /// Encode a text stream into a binary XML stream compatible with WCF's BinaryEncodingBindingElement.  Will throw if
        /// the input stream cannot be parsed into an XML document.  I/O streams are flushed but not closed.
        /// </summary>
        /// <param name="xmlInput"></param>
        /// <param name="binaryOutput"></param>
        public void EncodeBinaryXML(Stream xmlInput, Stream binaryOutput) {
            // parse string into the XML DOM
            var doc = new XmlDocument();
            doc.Load(xmlInput);

            // write bytestream
            using(var binaryWriter = XmlDictionaryWriter.CreateBinaryWriter(binaryOutput, WcfDictionaryBuilder.Dict, null, false)) {
                doc.Save(binaryWriter);
                binaryWriter.Flush();
                binaryOutput.Flush();
            }
        }

        public byte[] EncodeBinaryXML(string xmlInput) {
            var input = new MemoryStream(m_encoding.GetBytes(xmlInput));
            var output = new MemoryStream();
            EncodeBinaryXML(input, output);
            return output.ToArray();
        }
    }

    public static class WcfDictionaryBuilder {

        public static XmlDictionary Dict {
            get;
            private set;
        }

        static WcfDictionaryBuilder() {
            Dict = new XmlDictionary();
            Dict.Add("mustUnderstand");
            Dict.Add("Envelope");
            Dict.Add("http://www.w3.org/2003/05/soap-envelope");
            Dict.Add("http://www.w3.org/2005/08/addressing");
            Dict.Add("Header");
            Dict.Add("Action");
            Dict.Add("To");
            Dict.Add("Body");
            Dict.Add("Algorithm");
            Dict.Add("RelatesTo");
            Dict.Add("http://www.w3.org/2005/08/addressing/anonymous");
            Dict.Add("URI");
            Dict.Add("Reference");
            Dict.Add("MessageID");
            Dict.Add("Id");
            Dict.Add("Identifier");
            Dict.Add("http://schemas.xmlsoap.org/ws/2005/02/rm");
            Dict.Add("Transforms");
            Dict.Add("Transform");
            Dict.Add("DigestMethod");
            Dict.Add("Address");
            Dict.Add("ReplyTo");
            Dict.Add("SequenceAcknowledgement");
            Dict.Add("AcknowledgementRange");
            Dict.Add("Upper");
            Dict.Add("Lower");
            Dict.Add("BufferRemaining");
            Dict.Add("http://schemas.microsoft.com/ws/2006/05/rm");
            Dict.Add("http://schemas.xmlsoap.org/ws/2005/02/rm/SequenceAcknowledgement");
            Dict.Add("SecurityTokenReference");
            Dict.Add("Sequence");
            Dict.Add("MessageNumber");
            Dict.Add("http://www.w3.org/2000/09/xmldsig#");
            Dict.Add("http://www.w3.org/2000/09/xmldsig#enveloped-signature");
            Dict.Add("KeyInfo");
            Dict.Add("http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
            Dict.Add("http://www.w3.org/2001/04/xmlenc#");
            Dict.Add("http://schemas.xmlsoap.org/ws/2005/02/sc");
            Dict.Add("DerivedKeyToken");
            Dict.Add("Nonce");
            Dict.Add("Signature");
            Dict.Add("SignedInfo");
            Dict.Add("CanonicalizationMethod");
            Dict.Add("SignatureMethod");
            Dict.Add("SignatureValue");
            Dict.Add("DataReference");
            Dict.Add("EncryptedData");
            Dict.Add("EncryptionMethod");
            Dict.Add("CipherData");
            Dict.Add("CipherValue");
            Dict.Add("http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd");
            Dict.Add("Security");
            Dict.Add("Timestamp");
            Dict.Add("Created");
            Dict.Add("Expires");
            Dict.Add("Length");
            Dict.Add("ReferenceList");
            Dict.Add("ValueType");
            Dict.Add("Type");
            Dict.Add("EncryptedHeader");
            Dict.Add("http://docs.oasis-open.org/wss/oasis-wss-wssecurity-secext-1.1.xsd");
            Dict.Add("RequestSecurityTokenResponseCollection");
            Dict.Add("http://schemas.xmlsoap.org/ws/2005/02/trust");
            Dict.Add("http://schemas.xmlsoap.org/ws/2005/02/trust#BinarySecret");
            Dict.Add("http://schemas.microsoft.com/ws/2006/02/transactions");
            Dict.Add("s");
            Dict.Add("Fault");
            Dict.Add("MustUnderstand");
            Dict.Add("role");
            Dict.Add("relay");
            Dict.Add("Code");
            Dict.Add("Reason");
            Dict.Add("Text");
            Dict.Add("Node");
            Dict.Add("Role");
            Dict.Add("Detail");
            Dict.Add("Value");
            Dict.Add("Subcode");
            Dict.Add("NotUnderstood");
            Dict.Add("qname");
            Dict.Add("");
            Dict.Add("From");
            Dict.Add("FaultTo");
            Dict.Add("EndpointReference");
            Dict.Add("PortType");
            Dict.Add("ServiceName");
            Dict.Add("PortName");
            Dict.Add("ReferenceProperties");
            Dict.Add("RelationshipType");
            Dict.Add("Reply");
            Dict.Add("a");
            Dict.Add("http://schemas.xmlsoap.org/ws/2006/02/addressingidentity");
            Dict.Add("Identity");
            Dict.Add("Spn");
            Dict.Add("Upn");
            Dict.Add("Rsa");
            Dict.Add("Dns");
            Dict.Add("X509v3Certificate");
            Dict.Add("http://www.w3.org/2005/08/addressing/fault");
            Dict.Add("ReferenceParameters");
            Dict.Add("IsReferenceParameter");
            Dict.Add("http://www.w3.org/2005/08/addressing/reply");
            Dict.Add("http://www.w3.org/2005/08/addressing/none");
            Dict.Add("Metadata");
            Dict.Add("http://schemas.xmlsoap.org/ws/2004/08/addressing");
            Dict.Add("http://schemas.xmlsoap.org/ws/2004/08/addressing/role/anonymous");
            Dict.Add("http://schemas.xmlsoap.org/ws/2004/08/addressing/fault");
            Dict.Add("http://schemas.xmlsoap.org/ws/2004/06/addressingex");
            Dict.Add("RedirectTo");
            Dict.Add("Via");
            Dict.Add("http://www.w3.org/2001/10/xml-exc-c14n#");
            Dict.Add("PrefixList");
            Dict.Add("InclusiveNamespaces");
            Dict.Add("ec");
            Dict.Add("SecurityContextToken");
            Dict.Add("Generation");
            Dict.Add("Label");
            Dict.Add("Offset");
            Dict.Add("Properties");
            Dict.Add("Cookie");
            Dict.Add("wsc");
            Dict.Add("http://schemas.xmlsoap.org/ws/2004/04/sc");
            Dict.Add("http://schemas.xmlsoap.org/ws/2004/04/security/sc/dk");
            Dict.Add("http://schemas.xmlsoap.org/ws/2004/04/security/sc/sct");
            Dict.Add("http://schemas.xmlsoap.org/ws/2004/04/security/trust/RST/SCT");
            Dict.Add("http://schemas.xmlsoap.org/ws/2004/04/security/trust/RSTR/SCT");
            Dict.Add("RenewNeeded");
            Dict.Add("BadContextToken");
            Dict.Add("c");
            Dict.Add("http://schemas.xmlsoap.org/ws/2005/02/sc/dk");
            Dict.Add("http://schemas.xmlsoap.org/ws/2005/02/sc/sct");
            Dict.Add("http://schemas.xmlsoap.org/ws/2005/02/trust/RST/SCT");
            Dict.Add("http://schemas.xmlsoap.org/ws/2005/02/trust/RSTR/SCT");
            Dict.Add("http://schemas.xmlsoap.org/ws/2005/02/trust/RST/SCT/Renew");
            Dict.Add("http://schemas.xmlsoap.org/ws/2005/02/trust/RSTR/SCT/Renew");
            Dict.Add("http://schemas.xmlsoap.org/ws/2005/02/trust/RST/SCT/Cancel");
            Dict.Add("http://schemas.xmlsoap.org/ws/2005/02/trust/RSTR/SCT/Cancel");
            Dict.Add("http://www.w3.org/2001/04/xmlenc#aes128-cbc");
            Dict.Add("http://www.w3.org/2001/04/xmlenc#kw-aes128");
            Dict.Add("http://www.w3.org/2001/04/xmlenc#aes192-cbc");
            Dict.Add("http://www.w3.org/2001/04/xmlenc#kw-aes192");
            Dict.Add("http://www.w3.org/2001/04/xmlenc#aes256-cbc");
            Dict.Add("http://www.w3.org/2001/04/xmlenc#kw-aes256");
            Dict.Add("http://www.w3.org/2001/04/xmlenc#des-cbc");
            Dict.Add("http://www.w3.org/2000/09/xmldsig#dsa-sha1");
            Dict.Add("http://www.w3.org/2001/10/xml-exc-c14n#WithComments");
            Dict.Add("http://www.w3.org/2000/09/xmldsig#hmac-sha1");
            Dict.Add("http://www.w3.org/2001/04/xmldsig-more#hmac-sha256");
            Dict.Add("http://schemas.xmlsoap.org/ws/2005/02/sc/dk/p_sha1");
            Dict.Add("http://www.w3.org/2001/04/xmlenc#ripemd160");
            Dict.Add("http://www.w3.org/2001/04/xmlenc#rsa-oaep-mgf1p");
            Dict.Add("http://www.w3.org/2000/09/xmldsig#rsa-sha1");
            Dict.Add("http://www.w3.org/2001/04/xmldsig-more#rsa-sha256");
            Dict.Add("http://www.w3.org/2001/04/xmlenc#rsa-1_5");
            Dict.Add("http://www.w3.org/2000/09/xmldsig#sha1");
            Dict.Add("http://www.w3.org/2001/04/xmlenc#sha256");
            Dict.Add("http://www.w3.org/2001/04/xmlenc#sha512");
            Dict.Add("http://www.w3.org/2001/04/xmlenc#tripledes-cbc");
            Dict.Add("http://www.w3.org/2001/04/xmlenc#kw-tripledes");
            Dict.Add("http://schemas.xmlsoap.org/2005/02/trust/tlsnego#TLS_Wrap");
            Dict.Add("http://schemas.xmlsoap.org/2005/02/trust/spnego#GSS_Wrap");
            Dict.Add("http://schemas.microsoft.com/ws/2006/05/security");
            Dict.Add("dnse");
            Dict.Add("o");
            Dict.Add("Password");
            Dict.Add("PasswordText");
            Dict.Add("Username");
            Dict.Add("UsernameToken");
            Dict.Add("BinarySecurityToken");
            Dict.Add("EncodingType");
            Dict.Add("KeyIdentifier");
            Dict.Add("http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary");
            Dict.Add("http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#HexBinary");
            Dict.Add("http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Text");
            Dict.Add("http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-x509-token-profile-1.0#X509SubjectKeyIdentifier");
            Dict.Add("http://docs.oasis-open.org/wss/oasis-wss-kerberos-token-profile-1.1#GSS_Kerberosv5_AP_REQ");
            Dict.Add("http://docs.oasis-open.org/wss/oasis-wss-kerberos-token-profile-1.1#GSS_Kerberosv5_AP_REQ1510");
            Dict.Add("http://docs.oasis-open.org/wss/oasis-wss-saml-token-profile-1.0#SAMLAssertionID");
            Dict.Add("Assertion");
            Dict.Add("urn:oasis:names:tc:SAML:1.0:assertion");
            Dict.Add("http://docs.oasis-open.org/wss/oasis-wss-rel-token-profile-1.0.pdf#license");
            Dict.Add("FailedAuthentication");
            Dict.Add("InvalidSecurityToken");
            Dict.Add("InvalidSecurity");
            Dict.Add("k");
            Dict.Add("SignatureConfirmation");
            Dict.Add("TokenType");
            Dict.Add("http://docs.oasis-open.org/wss/oasis-wss-soap-message-security-1.1#ThumbprintSHA1");
            Dict.Add("http://docs.oasis-open.org/wss/oasis-wss-soap-message-security-1.1#EncryptedKey");
            Dict.Add("http://docs.oasis-open.org/wss/oasis-wss-soap-message-security-1.1#EncryptedKeySHA1");
            Dict.Add("http://docs.oasis-open.org/wss/oasis-wss-saml-token-profile-1.1#SAMLV1.1");
            Dict.Add("http://docs.oasis-open.org/wss/oasis-wss-saml-token-profile-1.1#SAMLV2.0");
            Dict.Add("http://docs.oasis-open.org/wss/oasis-wss-saml-token-profile-1.1#SAMLID");
            Dict.Add("AUTH-HASH");
            Dict.Add("RequestSecurityTokenResponse");
            Dict.Add("KeySize");
            Dict.Add("RequestedTokenReference");
            Dict.Add("AppliesTo");
            Dict.Add("Authenticator");
            Dict.Add("CombinedHash");
            Dict.Add("BinaryExchange");
            Dict.Add("Lifetime");
            Dict.Add("RequestedSecurityToken");
            Dict.Add("Entropy");
            Dict.Add("RequestedProofToken");
            Dict.Add("ComputedKey");
            Dict.Add("RequestSecurityToken");
            Dict.Add("RequestType");
            Dict.Add("Context");
            Dict.Add("BinarySecret");
            Dict.Add("http://schemas.xmlsoap.org/ws/2005/02/trust/spnego");
            Dict.Add("http://schemas.xmlsoap.org/ws/2005/02/trust/tlsnego");
            Dict.Add("wst");
            Dict.Add("http://schemas.xmlsoap.org/ws/2004/04/trust");
            Dict.Add("http://schemas.xmlsoap.org/ws/2004/04/security/trust/RST/Issue");
            Dict.Add("http://schemas.xmlsoap.org/ws/2004/04/security/trust/RSTR/Issue");
            Dict.Add("http://schemas.xmlsoap.org/ws/2004/04/security/trust/Issue");
            Dict.Add("http://schemas.xmlsoap.org/ws/2004/04/security/trust/CK/PSHA1");
            Dict.Add("http://schemas.xmlsoap.org/ws/2004/04/security/trust/SymmetricKey");
            Dict.Add("http://schemas.xmlsoap.org/ws/2004/04/security/trust/Nonce");
            Dict.Add("KeyType");
            Dict.Add("http://schemas.xmlsoap.org/ws/2004/04/trust/SymmetricKey");
            Dict.Add("http://schemas.xmlsoap.org/ws/2004/04/trust/PublicKey");
            Dict.Add("Claims");
            Dict.Add("InvalidRequest");
            Dict.Add("RequestFailed");
            Dict.Add("SignWith");
            Dict.Add("EncryptWith");
            Dict.Add("EncryptionAlgorithm");
            Dict.Add("CanonicalizationAlgorithm");
            Dict.Add("ComputedKeyAlgorithm");
            Dict.Add("UseKey");
            Dict.Add("http://schemas.microsoft.com/net/2004/07/secext/WS-SPNego");
            Dict.Add("http://schemas.microsoft.com/net/2004/07/secext/TLSNego");
            Dict.Add("t");
            Dict.Add("http://schemas.xmlsoap.org/ws/2005/02/trust/RST/Issue");
            Dict.Add("http://schemas.xmlsoap.org/ws/2005/02/trust/RSTR/Issue");
            Dict.Add("http://schemas.xmlsoap.org/ws/2005/02/trust/Issue");
            Dict.Add("http://schemas.xmlsoap.org/ws/2005/02/trust/SymmetricKey");
            Dict.Add("http://schemas.xmlsoap.org/ws/2005/02/trust/CK/PSHA1");
            Dict.Add("http://schemas.xmlsoap.org/ws/2005/02/trust/Nonce");
            Dict.Add("RenewTarget");
            Dict.Add("CancelTarget");
            Dict.Add("RequestedTokenCancelled");
            Dict.Add("RequestedAttachedReference");
            Dict.Add("RequestedUnattachedReference");
            Dict.Add("IssuedTokens");
            Dict.Add("http://schemas.xmlsoap.org/ws/2005/02/trust/Renew");
            Dict.Add("http://schemas.xmlsoap.org/ws/2005/02/trust/Cancel");
            Dict.Add("http://schemas.xmlsoap.org/ws/2005/02/trust/PublicKey");
            Dict.Add("Access");
            Dict.Add("AccessDecision");
            Dict.Add("Advice");
            Dict.Add("AssertionID");
            Dict.Add("AssertionIDReference");
            Dict.Add("Attribute");
            Dict.Add("AttributeName");
            Dict.Add("AttributeNamespace");
            Dict.Add("AttributeStatement");
            Dict.Add("AttributeValue");
            Dict.Add("Audience");
            Dict.Add("AudienceRestrictionCondition");
            Dict.Add("AuthenticationInstant");
            Dict.Add("AuthenticationMethod");
            Dict.Add("AuthenticationStatement");
            Dict.Add("AuthorityBinding");
            Dict.Add("AuthorityKind");
            Dict.Add("AuthorizationDecisionStatement");
            Dict.Add("Binding");
            Dict.Add("Condition");
            Dict.Add("Conditions");
            Dict.Add("Decision");
            Dict.Add("DoNotCacheCondition");
            Dict.Add("Evidence");
            Dict.Add("IssueInstant");
            Dict.Add("Issuer");
            Dict.Add("Location");
            Dict.Add("MajorVersion");
            Dict.Add("MinorVersion");
            Dict.Add("NameIdentifier");
            Dict.Add("Format");
            Dict.Add("NameQualifier");
            Dict.Add("Namespace");
            Dict.Add("NotBefore");
            Dict.Add("NotOnOrAfter");
            Dict.Add("saml");
            Dict.Add("Statement");
            Dict.Add("Subject");
            Dict.Add("SubjectConfirmation");
            Dict.Add("SubjectConfirmationData");
            Dict.Add("ConfirmationMethod");
            Dict.Add("urn:oasis:names:tc:SAML:1.0:cm:holder-of-key");
            Dict.Add("urn:oasis:names:tc:SAML:1.0:cm:sender-vouches");
            Dict.Add("SubjectLocality");
            Dict.Add("DNSAddress");
            Dict.Add("IPAddress");
            Dict.Add("SubjectStatement");
            Dict.Add("urn:oasis:names:tc:SAML:1.0:am:unspecified");
            Dict.Add("xmlns");
            Dict.Add("Resource");
            Dict.Add("UserName");
            Dict.Add("urn:oasis:names:tc:SAML:1.1:nameid-format:WindowsDomainQualifiedName");
            Dict.Add("EmailName");
            Dict.Add("urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress");
            Dict.Add("u");
            Dict.Add("ChannelInstance");
            Dict.Add("http://schemas.microsoft.com/ws/2005/02/duplex");
            Dict.Add("Encoding");
            Dict.Add("MimeType");
            Dict.Add("CarriedKeyName");
            Dict.Add("Recipient");
            Dict.Add("EncryptedKey");
            Dict.Add("KeyReference");
            Dict.Add("e");
            Dict.Add("http://www.w3.org/2001/04/xmlenc#Element");
            Dict.Add("http://www.w3.org/2001/04/xmlenc#Content");
            Dict.Add("KeyName");
            Dict.Add("MgmtData");
            Dict.Add("KeyValue");
            Dict.Add("RSAKeyValue");
            Dict.Add("Modulus");
            Dict.Add("Exponent");
            Dict.Add("X509Data");
            Dict.Add("X509IssuerSerial");
            Dict.Add("X509IssuerName");
            Dict.Add("X509SerialNumber");
            Dict.Add("X509Certificate");
            Dict.Add("AckRequested");
            Dict.Add("http://schemas.xmlsoap.org/ws/2005/02/rm/AckRequested");
            Dict.Add("AcksTo");
            Dict.Add("Accept");
            Dict.Add("CreateSequence");
            Dict.Add("http://schemas.xmlsoap.org/ws/2005/02/rm/CreateSequence");
            Dict.Add("CreateSequenceRefused");
            Dict.Add("CreateSequenceResponse");
            Dict.Add("http://schemas.xmlsoap.org/ws/2005/02/rm/CreateSequenceResponse");
            Dict.Add("FaultCode");
            Dict.Add("InvalidAcknowledgement");
            Dict.Add("LastMessage");
            Dict.Add("http://schemas.xmlsoap.org/ws/2005/02/rm/LastMessage");
            Dict.Add("LastMessageNumberExceeded");
            Dict.Add("MessageNumberRollover");
            Dict.Add("Nack");
            Dict.Add("netrm");
            Dict.Add("Offer");
            Dict.Add("r");
            Dict.Add("SequenceFault");
            Dict.Add("SequenceTerminated");
            Dict.Add("TerminateSequence");
            Dict.Add("http://schemas.xmlsoap.org/ws/2005/02/rm/TerminateSequence");
            Dict.Add("UnknownSequence");
            Dict.Add("http://schemas.microsoft.com/ws/2006/02/tx/oletx");
            Dict.Add("oletx");
            Dict.Add("OleTxTransaction");
            Dict.Add("PropagationToken");
            Dict.Add("http://schemas.xmlsoap.org/ws/2004/10/wscoor");
            Dict.Add("wscoor");
            Dict.Add("CreateCoordinationContext");
            Dict.Add("CreateCoordinationContextResponse");
            Dict.Add("CoordinationContext");
            Dict.Add("CurrentContext");
            Dict.Add("CoordinationType");
            Dict.Add("RegistrationService");
            Dict.Add("Register");
            Dict.Add("RegisterResponse");
            Dict.Add("ProtocolIdentifier");
            Dict.Add("CoordinatorProtocolService");
            Dict.Add("ParticipantProtocolService");
            Dict.Add("http://schemas.xmlsoap.org/ws/2004/10/wscoor/CreateCoordinationContext");
            Dict.Add("http://schemas.xmlsoap.org/ws/2004/10/wscoor/CreateCoordinationContextResponse");
            Dict.Add("http://schemas.xmlsoap.org/ws/2004/10/wscoor/Register");
            Dict.Add("http://schemas.xmlsoap.org/ws/2004/10/wscoor/RegisterResponse");
            Dict.Add("http://schemas.xmlsoap.org/ws/2004/10/wscoor/fault");
            Dict.Add("ActivationCoordinatorPortType");
            Dict.Add("RegistrationCoordinatorPortType");
            Dict.Add("InvalidState");
            Dict.Add("InvalidProtocol");
            Dict.Add("InvalidParameters");
            Dict.Add("NoActivity");
            Dict.Add("ContextRefused");
            Dict.Add("AlreadyRegistered");
            Dict.Add("http://schemas.xmlsoap.org/ws/2004/10/wsat");
            Dict.Add("wsat");
            Dict.Add("http://schemas.xmlsoap.org/ws/2004/10/wsat/Completion");
            Dict.Add("http://schemas.xmlsoap.org/ws/2004/10/wsat/Durable2PC");
            Dict.Add("http://schemas.xmlsoap.org/ws/2004/10/wsat/Volatile2PC");
            Dict.Add("Prepare");
            Dict.Add("Prepared");
            Dict.Add("ReadOnly");
            Dict.Add("Commit");
            Dict.Add("Rollback");
            Dict.Add("Committed");
            Dict.Add("Aborted");
            Dict.Add("Replay");
            Dict.Add("http://schemas.xmlsoap.org/ws/2004/10/wsat/Commit");
            Dict.Add("http://schemas.xmlsoap.org/ws/2004/10/wsat/Rollback");
            Dict.Add("http://schemas.xmlsoap.org/ws/2004/10/wsat/Committed");
            Dict.Add("http://schemas.xmlsoap.org/ws/2004/10/wsat/Aborted");
            Dict.Add("http://schemas.xmlsoap.org/ws/2004/10/wsat/Prepare");
            Dict.Add("http://schemas.xmlsoap.org/ws/2004/10/wsat/Prepared");
            Dict.Add("http://schemas.xmlsoap.org/ws/2004/10/wsat/ReadOnly");
            Dict.Add("http://schemas.xmlsoap.org/ws/2004/10/wsat/Replay");
            Dict.Add("http://schemas.xmlsoap.org/ws/2004/10/wsat/fault");
            Dict.Add("CompletionCoordinatorPortType");
            Dict.Add("CompletionParticipantPortType");
            Dict.Add("CoordinatorPortType");
            Dict.Add("ParticipantPortType");
            Dict.Add("InconsistentInternalState");
            Dict.Add("mstx");
            Dict.Add("Enlistment");
            Dict.Add("protocol");
            Dict.Add("LocalTransactionId");
            Dict.Add("IsolationLevel");
            Dict.Add("IsolationFlags");
            Dict.Add("Description");
            Dict.Add("Loopback");
            Dict.Add("RegisterInfo");
            Dict.Add("ContextId");
            Dict.Add("TokenId");
            Dict.Add("AccessDenied");
            Dict.Add("InvalidPolicy");
            Dict.Add("CoordinatorRegistrationFailed");
            Dict.Add("TooManyEnlistments");
            Dict.Add("Disabled");
            Dict.Add("ActivityId");
            Dict.Add("http://schemas.microsoft.com/2004/09/ServiceModel/Diagnostics");
            Dict.Add("http://docs.oasis-open.org/wss/oasis-wss-kerberos-token-profile-1.1#Kerberosv5APREQSHA1");
            Dict.Add("http://schemas.xmlsoap.org/ws/2002/12/policy");
            Dict.Add("FloodMessage");
            Dict.Add("LinkUtility");
            Dict.Add("Hops");
            Dict.Add("http://schemas.microsoft.com/net/2006/05/peer/HopCount");
            Dict.Add("PeerVia");
            Dict.Add("http://schemas.microsoft.com/net/2006/05/peer");
            Dict.Add("PeerFlooder");
            Dict.Add("PeerTo");
            Dict.Add("http://schemas.microsoft.com/ws/2005/05/routing");
            Dict.Add("PacketRoutable");
            Dict.Add("http://schemas.microsoft.com/ws/2005/05/addressing/none");
            Dict.Add("http://schemas.microsoft.com/ws/2005/05/envelope/none");
            Dict.Add("http://www.w3.org/2001/XMLSchema-instance");
            Dict.Add("http://www.w3.org/2001/XMLSchema");
            Dict.Add("nil");
            Dict.Add("type");
            Dict.Add("char");
            Dict.Add("boolean");
            Dict.Add("byte");
            Dict.Add("unsignedByte");
            Dict.Add("short");
            Dict.Add("unsignedShort");
            Dict.Add("int");
            Dict.Add("unsignedInt");
            Dict.Add("long");
            Dict.Add("unsignedLong");
            Dict.Add("float");
            Dict.Add("double");
            Dict.Add("decimal");
            Dict.Add("dateTime");
            Dict.Add("string");
            Dict.Add("base64Binary");
            Dict.Add("anyType");
            Dict.Add("duration");
            Dict.Add("guid");
            Dict.Add("anyURI");
            Dict.Add("QName");
            Dict.Add("time");
            Dict.Add("date");
            Dict.Add("hexBinary");
            Dict.Add("gYearMonth");
            Dict.Add("gYear");
            Dict.Add("gMonthDay");
            Dict.Add("gDay");
            Dict.Add("gMonth");
            Dict.Add("integer");
            Dict.Add("positiveInteger");
            Dict.Add("negativeInteger");
            Dict.Add("nonPositiveInteger");
            Dict.Add("nonNegativeInteger");
            Dict.Add("normalizedString");
            Dict.Add("ConnectionLimitReached");
            Dict.Add("http://schemas.xmlsoap.org/soap/envelope/");
            Dict.Add("Actor");
            Dict.Add("Faultcode");
            Dict.Add("Faultstring");
            Dict.Add("Faultactor");
            Dict.Add("Detail");
        }
    }
}