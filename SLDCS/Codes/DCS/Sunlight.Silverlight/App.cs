﻿using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Markup;
using System.Xml.Linq;
using Sunlight.Silverlight.Shell;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight {
    public sealed class App : BaseApp {
        public override string ApplicationName {
            get {
                return Silverlight.Resources.UIStrings.ApplicationName;
            }
        }

        public override SystemTypes SystemType {
            get {
                return SystemTypes.Dcs;
            }
        }

        private readonly string shortThemeName = "Office_Black";

        private string GetResourceXaml(string sourceName) {
            var resourcePath = string.Format("/Telerik.Windows.Themes.{0};component/Themes/{1}.xaml", this.shortThemeName, sourceName);
            var resourceInfo = GetResourceStream(new Uri(resourcePath, UriKind.Relative));
            if(resourceInfo == null)
                throw new FileNotFoundException(resourcePath);
            string xaml;
            using(var reader = new StreamReader(resourceInfo.Stream))
                xaml = reader.ReadToEnd();
            return xaml;
        }

        /// <summary>
        /// 替换选中Tab页的颜色,此颜色只适合Summer主题 --去除tab也颜色变更
        /// </summary>
        private void ModifyRadTabSelectdItemStyle() {
            var xaml = this.GetResourceXaml("Telerik.Windows.Controls.Navigation");
            var dic = (ResourceDictionary)XamlReader.Load(xaml);
            this.Resources.MergedDictionaries.Add(dic);
        }

        /// <summary>
        /// 放到这里是为了解决换主题时资源不兼容的问题。
        /// </summary>
        private void AddNotFocusableButtonStyle() {
            var xaml = this.GetResourceXaml("Telerik.Windows.Controls");
            var pattern = "<{0}.*{1}[^>]*[^/]>[^<>]*(((?'Open'<)[^<>]*)+((?'-Open'>)[^<>]*)+)*?(?(Open)(?!))</{0}>";
            var buttonXaml = Regex.Match(xaml, string.Format(pattern, "Style", "RadButtonStyle")).Value.Replace("RadButtonStyle", "NotFocusableButtonStyle");
            //Silverlight5中，不支持CodeBehind创建ControlTemplate，只好用下面的代码来实现
            var notFocusableButtonStyle = Regex.Replace(buttonXaml, string.Format(pattern, "VisualState", "Unfocused"), "<VisualState x:Name=\"Unfocused\" />");
            notFocusableButtonStyle = Regex.Replace(notFocusableButtonStyle, string.Format(pattern, "VisualState", "Focused"), "<VisualState x:Name=\"Focused\" />");

            this.Resources.MergedDictionaries.Add((ResourceDictionary)XamlReader.Load(
                "<ResourceDictionary xmlns=\"http://schemas.microsoft.com/winfx/2006/xaml/presentation\" xmlns:x=\"http://schemas.microsoft.com/winfx/2006/xaml\" xmlns:telerik=\"http://schemas.telerik.com/2008/xaml/presentation\">"
                + notFocusableButtonStyle + "</ResourceDictionary>"
                ));
        }

        private void ModifyRibbonViewStyle() {
            //RadControls 主题中的 ScrollViewer 设为 Disabled 这样菜单才会换行
            //xaml = Regex.Replace(xaml, @"(?<=TabControlTemplate.*RadTabControl[^=]*.*)\{TemplateBinding\sScrollViewer\.HorizontalScrollBarVisibility\}", "Disabled");
            #region Rad 2015Q3中RadTab中左右箭头滚动速度偏慢，这里加一下速，可根据用户的要求经行调整
            var xaml = GetResourceXaml("Telerik.Windows.Controls.RibbonView");
            var xDoc = XDocument.Parse(xaml);
            XNamespace pr = "http://schemas.microsoft.com/winfx/2006/xaml/presentation";
            XNamespace xa = "http://schemas.microsoft.com/winfx/2006/xaml";
            var radScrollStyle = xDoc.Elements().First().Elements(pr + "Style")
            .First(e => e.Attributes(xa + "Key").Any(a => a.Value == "RibbonScrollButtonStyle"));
            var scrollInterval = radScrollStyle.Elements().First(e => e.Attribute("Property").Value == "Interval");
            scrollInterval.SetAttributeValue("Value", "10");
            scrollInterval.AddAfterSelf(new XElement(pr + "Setter", new XAttribute("Property", "Delay"), new XAttribute("Value", "200")));
            #endregion

            #region 给左侧增加滚动效果，保证Level 1数量多时，能正常显示
            var tabControlTemplate = xDoc.Elements().First().Elements(pr + "ControlTemplate").First(e => e.Attributes(xa + "Key").Any(a => a.Value == "TabControlTemplate"));
            var presenterGrid = tabControlTemplate.Element(pr + "ScrollViewer").Element(pr + "Grid").Elements(pr + "Grid").First();
            var nodes = presenterGrid.Elements().ToList();
            presenterGrid.RemoveNodes();
            foreach(var node in nodes) {
                if(node.Name == pr + "ItemsPresenter") {
                    var scrollViewer = new XElement(pr + "ScrollViewer", new XAttribute("Background", "#02FFFFFF"),
                        new XAttribute("HorizontalScrollBarVisibility", "Disabled"), new XAttribute("VerticalScrollBarVisibility", "Auto"), node);
                    presenterGrid.Add(scrollViewer);
                } else
                    presenterGrid.Add(node);
            }
            #endregion

            this.Resources.MergedDictionaries.Add((ResourceDictionary)XamlReader.Load(xDoc.ToString()));
        }

        protected override void LoadResources() {
            StyleManager.ApplicationTheme = new Office_BlackTheme();
            #region 这里的方法的位置不要移动，否则后面自定义的某些资源就会失效了。
            ModifyRadTabSelectdItemStyle();
            ModifyRibbonViewStyle();
            AddNotFocusableButtonStyle();
            this.Resources.MergedDictionaries.Add(new ResourceDictionary {
                Source = new Uri("/Sunlight.Silverlight;component/Styles/Telerik.ReportViewer.Silverlight.xaml", UriKind.Relative)
            });
            base.LoadResources();
            #endregion
            this.Resources.Add("ApplicationName", this.ApplicationName);
            this.Resources.MergedDictionaries.Add(new ResourceDictionary {
                Source = new Uri("/Sunlight.Silverlight;component/Styles/LabelStyle.xaml", UriKind.Relative)
            });
        }

        public App() {
            this.MainView = new MainView();
        }
    }
}