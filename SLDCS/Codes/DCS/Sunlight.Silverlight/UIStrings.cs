﻿using System.Linq;
using Sunlight.Silverlight.Core.Model;

namespace Sunlight.Silverlight {
    public class UIStrings : ModelBase {
        public void Refresh(params string[] propertyNames) {
            this.NotifyOfPropertiesChange(propertyNames.Length == 0 ? this.GetType().GetProperties().Select(p => p.Name).ToArray() : propertyNames);
        }

        public string ApplicationName {
            get {
                return Resources.UIStrings.ApplicationName;
            }
        }

        public string DockMenuName {
            get {
                return Resources.UIStrings.DockMenuName;
            }
        }

    }
}
