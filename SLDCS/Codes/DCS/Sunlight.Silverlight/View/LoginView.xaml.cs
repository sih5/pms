﻿using System;
using System.Windows;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.View {
    public partial class LoginView {
        public LoginView() {
            this.InitializeComponent();
            this.LoginPanel.ViewModel.PropertyChanged += (sender, args) => {
                if(args.PropertyName == "LoginSuccess" && ((LoginViewModel)sender).LoginSuccess)
                    ((MainView)Application.Current.RootVisual).ShowShellView();
            };
            //try {
            //    var assembly = Assembly.Load("Sunlight.Silverlight.Dcs, Version=1.0.0.0, Culture=neutral, PublicKeyToken=6fc94565b181c045");
            //    var name = new AssemblyName(assembly.FullName);
            //    var date = BaseApp.Current.InitParams.ContainsKey("UpdateDate") ? string.Format(" ({0})", BaseApp.Current.InitParams["UpdateDate"]) : null;
            //    //this.VersionText.Text = string.Format("v{0}{1}", name.Version.ToString(3), date);
            //} catch {
            //    //this.VersionText.Visibility = Visibility.Collapsed;
            //}
        }
    }
}
