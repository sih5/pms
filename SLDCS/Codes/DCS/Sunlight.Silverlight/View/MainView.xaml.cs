﻿using System;
using System.Threading;
using System.Windows;
using System.Windows.Markup;

namespace Sunlight.Silverlight.View {
    public partial class MainView {
        private LoginView loginView;
        private ShellView shellView;

        private LoginView LoginView {
            get {
                return this.loginView ?? (this.loginView = new LoginView());
            }
        }

        private ShellView ShellView {
            get {
                return this.shellView ?? (this.shellView = new ShellView {
                    Language = XmlLanguage.GetLanguage(Thread.CurrentThread.CurrentUICulture.Name)
                });
            }
        }

        //public string PageTitle {
        //    get {
        //        return this.MainTransition.Content == this.ShellView ? this.ShellView.ViewModel.PageTitle : null;
        //    }
        //    set {
        //        if(this.MainTransition.Content == this.ShellView)
        //            this.ShellView.ViewModel.PageTitle = value;
        //    }
        //}

        private void UserControl_Loaded(object sender, RoutedEventArgs e) {
            Action checkIsAuthenticated = () => {
                if(WebContext.Current.Authentication.User.Identity.IsAuthenticated)
                    this.ShowShellView();
                else
                    this.ShowLoginView();
            };
            if(Application.Current.IsRunningOutOfBrowser)
                ((BaseApp)Application.Current).CheckIfAuthorized(checkIsAuthenticated);
            else
                checkIsAuthenticated();
        }

        internal void ShowShellView() {
            this.MainTransition.Content = this.ShellView;
        }

        internal void ShowLoginView() {
            this.MainTransition.Content = this.LoginView;
        }

        public MainView() {
            this.InitializeComponent();
        }
    }
}
