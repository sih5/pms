﻿using System;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.ViewModel;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.View {
    public partial class ShellView {
        private static readonly Uri HomeUri = new Uri("/home", UriKind.Relative);
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        private bool isUriMappingsLoaded;
        private Uri pendingPageUri;
        private ShellViewModel viewModel;

        public ShellViewModel ViewModel {
            get {
                return this.viewModel;
            }
            private set {
                this.DataContext = null;
                this.DataContext = this.viewModel = value;
            }
        }
        private void RefreshQuickAccessToolBarItemsSource() {
            this.MainRibbonView.QuickAccessToolBar.ItemsSource = null;
            this.MainRibbonView.QuickAccessToolBar.ItemsSource = this.ViewModel.RecentPageManager.MenuItems;
        }

        private void ViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            switch(e.PropertyName) {
                case "UriMappings":
                    var mapper = (UriMapper)this.MainFrame.UriMapper;
                    mapper.UriMappings.Clear();
                    foreach(var uriMapping in this.ViewModel.UriMappings)
                        mapper.UriMappings.Add(uriMapping);
                    this.isUriMappingsLoaded = true;
                    if(this.pendingPageUri == null) {
                        this.ViewModel.PageUri = HomeUri;
                        return;
                    }
                    this.ViewModel.PageUri = this.pendingPageUri;
                    this.pendingPageUri = null;
                    break;
                case "RecentPageManager":
                    if(this.ViewModel.RecentPageManager != null) {
                        this.ViewModel.RecentPageManager.PropertyChanged -= this.RecentPageManager_PropertyChanged;
                        this.ViewModel.RecentPageManager.PropertyChanged += this.RecentPageManager_PropertyChanged;
                        this.RefreshQuickAccessToolBarItemsSource();
                    }
                    break;
                case "PageUri":
                    this.ViewModel.IsRibbonMinimized = true;
                    this.MainRibbonView.Items.Clear();
                    break;
                case "RibbonTabs":
                    this.MainRibbonView.Items.Clear();
                    foreach(var ribbonTab in this.ViewModel.RibbonTabs)
                        this.MainRibbonView.Items.Add(ribbonTab);
                    this.ViewModel.IsRibbonMinimized = this.ViewModel.RibbonTabs.Count == 0;
                    this.MainRibbonView.SelectedIndex = 0;
                    break;
            }
        }

        private void ShellView_Loaded(object sender, RoutedEventArgs e) {
            if(this.ViewModel.LoadMenuCommand.CanExecute(null))
                this.ViewModel.LoadMenuCommand.Execute(null);
            this.dcsDomainContext.Load(this.dcsDomainContext.GetBranchesQuery().Where(r => r.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
            }, null);
        }

        private void RecentPageManager_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            if(e.PropertyName != "MenuItems")
                return;
            this.RefreshQuickAccessToolBarItemsSource();
        }

        private void MainFrame_Navigating(object sender, NavigatingCancelEventArgs e) {
            this.ViewModel.IsMenuOpen = false;
        }

        private void MainFrame_Navigated(object sender, NavigationEventArgs e) {
            this.ViewModel.Page = e.Content;
        }

        private void MainFrame_NavigationFailed(object sender, NavigationFailedEventArgs e) {
            if(e.Exception is ArgumentException && string.IsNullOrWhiteSpace(e.Uri.OriginalString)) {
                this.ViewModel.PageUri = HomeUri;
                e.Handled = true;
            } else if(!this.isUriMappingsLoaded) {
                e.Handled = true;
                this.pendingPageUri = e.Uri;
            } else if(e.Exception is ArgumentException && this.ViewModel.MenuItems.Any()) {
                this.ViewModel.PageUri = HomeUri;
                e.Handled = true;
                if(this.ViewModel.MenuItems.Any()) {
                    (new RadWindow {
                        Header = ((BaseApp)Application.Current).ApplicationName,
                        WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen,
                        IsTopmost = true,
                        ResizeMode = ResizeMode.NoResize,
                        CanClose = true,
                        CanMove = true,
                        Content = new ExceptionView {
                            Margin = new Thickness(10),
                            DataContext = e.Exception,
                        },
                    }).Show();
                }
            }
        }

        private void AppHostContent_FullScreenChanged(object sender, EventArgs e) {
            this.MainRibbonView.Visibility = Application.Current.Host.Content.IsFullScreen ? Visibility.Collapsed : Visibility.Visible;
        }

        private void BtnHome_Click(object sender, RoutedEventArgs e) {
            this.ViewModel.IsMenuOpen = false;
            this.ViewModel.PageUri = HomeUri;
        }

        public ShellView() {
            this.InitializeComponent();
            this.ViewModel = new ShellViewModel {
                IsRibbonMinimized = true,
                MenuButtonContent = new Image {
                    HorizontalAlignment = HorizontalAlignment.Center,
                    VerticalAlignment = VerticalAlignment.Center,
                    Stretch = Stretch.None,
                    Source = new BitmapImage(Utils.MakeServerUri("Client/Images/menu-logo.png"))
                },
                IsNotShowPwd = true
            };
            this.ViewModel.SystemMenuItemsChanged += () => {
                this.menuTreeView.ItemsSource = ViewModel.SystemMenuItems;
                this.RefreshQuickAccessTemplate();
            };
            this.Loaded += this.ShellView_Loaded;
            this.ViewModel.PropertyChanged += this.ViewModel_PropertyChanged;
            this.MainFrame.Navigating += this.MainFrame_Navigating;
            this.MainFrame.Navigated += this.MainFrame_Navigated;
            this.MainFrame.NavigationFailed += this.MainFrame_NavigationFailed;
            this.MainFrame.ContentLoader = new RootViewNavigationContentLoader();
            Application.Current.Host.Content.FullScreenChanged += this.AppHostContent_FullScreenChanged;
            RootViewCacheManager.Current.MaxCacheSize = 2;
        }

        private void BtnQuickAccessTemplate_Click(object sender, RoutedEventArgs e) {
            this.ViewModel.RecentPageManager.ShowQuickAccessButtonName = !this.ViewModel.RecentPageManager.ShowQuickAccessButtonName;
            this.RefreshQuickAccessTemplate();
        }

        private void RefreshQuickAccessTemplate() {
            //直接用过this.QuickAccessTemplate1，但是不知道为什么是null
            var template1 = this.Resources["QuickAccessTemplate1"] as DataTemplate;
            if(this.ViewModel.RecentPageManager.ShowQuickAccessButtonName) {
                this.QuickAccessToolBar.ItemTemplate = this.QuickAccessTemplate2;
                this.BtnQuickAccessTemplate.Content = new Image {
                    Source = new BitmapImage(new Uri("/Sunlight.Silverlight;component/Images/ico.png", UriKind.Relative))
                };
            } else {
                this.QuickAccessToolBar.ItemTemplate = template1;
                this.BtnQuickAccessTemplate.Content = new Image {
                    Source = new BitmapImage(new Uri("/Sunlight.Silverlight;component/Images/ImageAndFont.png", UriKind.Relative))
                };
            }
            this.RefreshQuickAccessToolBarItemsSource();
        }

        private void BtnFaq_Click(object sender, RoutedEventArgs e) {
            //http://wpa.b.qq.com/cgi/wpa.php?ln=1&key=XzgwMDAwMzAyMl8zNzIzNjdfODAwMDAzMDIyXzJf
            //http://webchat.b.qq.com/webchat.htm?sid=2188z8p8p8p8p8P8p8K8K
            HtmlPage.Window.Navigate(new Uri("http://wpa.b.qq.com/cgi/wpa.php?ln=1&key=XzgwMDAwMzAyMl8zNzIzNjdfODAwMDAzMDIyXzJf"), "blank",
                "fullscreen=yes,channelmode=no");
        }
    }
}
