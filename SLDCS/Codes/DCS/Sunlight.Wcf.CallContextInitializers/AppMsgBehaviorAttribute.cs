﻿using System;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;

namespace Sunlight.Wcf.CallContextInitializers {
    public class AppMsgBehaviorAttribute : Attribute, IOperationBehavior {
        #region IOperationBehavior 成员
        public void AddBindingParameters(OperationDescription operationDescription, BindingParameterCollection bindingParameters) {
        }

        public void ApplyClientBehavior(OperationDescription operationDescription, ClientOperation clientOperation) {
        }

        public void ApplyDispatchBehavior(OperationDescription operationDescription, DispatchOperation dispatchOperation) {
            dispatchOperation.CallContextInitializers.Add(new AppMsgSettingCallContextInitializer());
        }

        public void Validate(OperationDescription operationDescription) {
        }
        #endregion
    }
}
