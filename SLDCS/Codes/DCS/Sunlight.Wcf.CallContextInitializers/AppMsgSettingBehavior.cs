﻿using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;

namespace Sunlight.Wcf.CallContextInitializers {
    public class AppMsgSettingBehavior : IEndpointBehavior {

        #region IEndpointBehavior 成员
        public void AddBindingParameters(ServiceEndpoint endpoint, BindingParameterCollection bindingParameters) {
        }

        public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime) {
        }

        public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher) {
            foreach(var operation in endpointDispatcher.DispatchRuntime.Operations) {
                operation.CallContextInitializers.Add(new AppMsgSettingCallContextInitializer());
            }
        }

        public void Validate(ServiceEndpoint endpoint) {
        }
        #endregion
    }
}
