﻿using System;
using System.ServiceModel.Configuration;

namespace Sunlight.Wcf.CallContextInitializers {
    public class AppMsgSettingBehaviorElement : BehaviorExtensionElement {

        protected override object CreateBehavior() {
            return new AppMsgSettingBehavior();
        }

        public override Type BehaviorType {
            get {
                return typeof(AppMsgSettingBehavior);
            }
        }
    }
}