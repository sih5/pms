﻿using System.Globalization;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;
using System.Threading;

namespace Sunlight.Wcf.CallContextInitializers {
    public class AppMsgSettingCallContextInitializer : ICallContextInitializer {
        private const string CULTURE_INFO_HEAD_LOCAL_NAME = "__CultureInfo";
        private const string CULTURE_INFO_HEADER_NAMESPACE = "http://sunlight.bz/excelservice/";

        #region ICallContextInitializer 成员
        public void AfterInvoke(object correlationState) {
            if(correlationState == null)
                return;
            var currentCulture = correlationState as CultureInfo[];
            Thread.CurrentThread.CurrentCulture = currentCulture[0];
            Thread.CurrentThread.CurrentUICulture = currentCulture[1];
        }

        public object BeforeInvoke(InstanceContext instanceContext, IClientChannel channel, Message message) {
            var currentCulture = Thread.CurrentThread.CurrentCulture;
            var currentUICulture = Thread.CurrentThread.CurrentUICulture;
            if(message.Headers.FindHeader(CULTURE_INFO_HEAD_LOCAL_NAME, CULTURE_INFO_HEADER_NAMESPACE) > -1) {
                var cultureName = message.Headers.GetHeader<string>(CULTURE_INFO_HEAD_LOCAL_NAME, CULTURE_INFO_HEADER_NAMESPACE);
                Thread.CurrentThread.CurrentCulture = new CultureInfo(cultureName);
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(cultureName);
            }
            return new[] { currentCulture, currentUICulture };
        }
        #endregion
    }
}
