select a.code as BranchCode,
       a.PartsSalesCategoryId,
       b.CityLevel,
       b.ChannelCapabilityId,
       b.星级,
       pscg.name as 品牌,
       a.TotalService as 网络总数,
       (case b.CityLevel
         when 1 then
          '省级'
         when 2 then
          '地级'
         when 3 then
          '县级'
         else
          '无城市级别'
       end) as 城市级别,
       (case b.ChannelCapabilityId
         when 1 then
          '4s店'
         when 2 then
          '服务站'
         else
          '无业务能力类别'
       end) as 业务能力,
       b.NetworkNumber as 网络数量,
       to_char(round((b.NetworkNumber / d.TotalQtyOfSets * 100), 2),
               'fm99990.90') || '%' as 所占比率,
       d.TotalQtyOfSets as 合计数量,
       to_char(round((d.TotalQtyOfSets / a.TotalService * 100), 2),
               'fm99990.90') || '%' as 合计比率
  from yxdcs.PartsSalesCategory pscg
 inner join (select bff.code,
                    dsi.PartsSalesCategoryId,
                    count(d.Code) as TotalService
               from yxdcs.company c
              inner join yxdcs.Dealer d
                 on c.Code = d.Code
                and c.type in (2, 7, 10)
              inner join yxdcs.DealerServiceInfo dsi
                 on dsi.DealerId = d.id
                and dsi.ChannelCapabilityId in (1, 2)
                and dsi.status = 1
                and dsi.PartsSalesCategoryId not in (5, 6)
              inner join yxdcs.Branch bff
                 on dsi.BranchId = bff.id
              group by dsi.PartsSalesCategoryId, bff.code) a
    on a.PartsSalesCategoryid = pscg.id
 inner join (select bff.code,
                    dsi.PartsSalesCategoryId,
                    (case
                      when c.CityLevel is null then
                       66
                      else
                       c.CityLevel
                    end) as CityLevel,
                    dsi.ChannelCapabilityId,
                    dsi.Grade 星级,
                    count(d.Code) as NetworkNumber
               from yxdcs.company c
              inner join yxdcs.Dealer d
                 on c.Code = d.Code
                and c.type in (2, 7, 10)
              inner join yxdcs.DealerServiceInfo dsi
                 on dsi.DealerId = d.id
                and dsi.ChannelCapabilityId in (1, 2)
                and dsi.status = 1
                and dsi.PartsSalesCategoryId not in (5, 6)
              inner join yxdcs.Branch bff
                 on dsi.BranchId = bff.id
              inner join yxdcs.GradeCoefficient gc
                 on gc.id = dsi.GradeCoefficientId
                and gc.PartsSalesCategoryId = dsi.PartsSalesCategoryId
              group by dsi.PartsSalesCategoryId,
                       c.CityLevel,
                       dsi.ChannelCapabilityId,
                       dsi.Grade,
                       bff.code) b
    on b.PartsSalesCategoryId = pscg.id
  left join (select dsi.PartsSalesCategoryId,
                    (case
                      when c.CityLevel is null then
                       66
                      else
                       c.CityLevel
                    end) as CityLevel,
                    dsi.ChannelCapabilityId,
                    count(d.Code) as TotalQtyOfSets
               from yxdcs.company c
              inner join yxdcs.Dealer d
                 on c.Code = d.Code
                and c.type in (2, 7, 10)
              inner join yxdcs.DealerServiceInfo dsi
                 on dsi.DealerId = d.id
                and dsi.ChannelCapabilityId in (1, 2)
                and dsi.status = 1
                and dsi.PartsSalesCategoryId not in (5, 6)
              group by dsi.PartsSalesCategoryId,
                       c.CityLevel,
                       dsi.ChannelCapabilityId) d
    on d.PartsSalesCategoryId = pscg.id
   and d.CityLevel = b.CityLevel
   and d.ChannelCapabilityId = b.ChannelCapabilityId
 group by a.code,
          a.PartsSalesCategoryId,
          pscg.name,
          a.TotalService,
          b.CityLevel,
          b.星级,
          b.ChannelCapabilityId,
          b.NetworkNumber,
          d.TotalQtyOfSets
union all --gcdcs
select a.code as BranchCode,
       a.PartsSalesCategoryId,
       b.CityLevel,
       b.ChannelCapabilityId,
       b.星级,
       pscg.name as 品牌,
       a.TotalService as 网络总数,
       (case b.CityLevel
         when 1 then
          '省级'
         when 2 then
          '地级'
         when 3 then
          '县级'
         else
          '无城市级别'
       end) as 城市级别,
       (case b.ChannelCapabilityId
         when 1 then
          '4s店'
         when 2 then
          '服务站'
         else
          '无业务能力类别'
       end) as 业务能力,
       b.NetworkNumber as 网络数量,
       to_char(round((b.NetworkNumber / d.TotalQtyOfSets * 100), 2),
               'fm99990.90') || '%' as 所占比率,
       d.TotalQtyOfSets as 合计数量,
       to_char(round((d.TotalQtyOfSets / a.TotalService * 100), 2),
               'fm99990.90') || '%' as 合计比率
  from gcdcs.PartsSalesCategory pscg
 inner join (select bff.code,
                    dsi.PartsSalesCategoryId,
                    count(d.Code) as TotalService
               from gcdcs.company c
              inner join gcdcs.Dealer d
                 on c.Code = d.Code
                and c.type in (2, 7, 10)
              inner join gcdcs.DealerServiceInfo dsi
                 on dsi.DealerId = d.id
                and dsi.ChannelCapabilityId in (1, 2)
                and dsi.status = 1
                and dsi.PartsSalesCategoryId not in (5, 6)
              inner join gcdcs.Branch bff
                 on dsi.BranchId = bff.id
              group by dsi.PartsSalesCategoryId, bff.code) a
    on a.PartsSalesCategoryid = pscg.id
 inner join (select bff.code,
                    dsi.PartsSalesCategoryId,
                    (case
                      when c.CityLevel is null then
                       66
                      else
                       c.CityLevel
                    end) as CityLevel,
                    dsi.ChannelCapabilityId,
                    dsi.Grade 星级,
                    count(d.Code) as NetworkNumber
               from gcdcs.company c
              inner join gcdcs.Dealer d
                 on c.Code = d.Code
                and c.type in (2, 7, 10)
              inner join gcdcs.DealerServiceInfo dsi
                 on dsi.DealerId = d.id
                and dsi.ChannelCapabilityId in (1, 2)
                and dsi.status = 1
                and dsi.PartsSalesCategoryId not in (5, 6)
              inner join gcdcs.Branch bff
                 on dsi.BranchId = bff.id
              inner join gcdcs.GradeCoefficient gc
                 on gc.id = dsi.GradeCoefficientId
                and gc.PartsSalesCategoryId = dsi.PartsSalesCategoryId
              group by dsi.PartsSalesCategoryId,
                       c.CityLevel,
                       dsi.ChannelCapabilityId,
                       dsi.Grade,
                       bff.code) b
    on b.PartsSalesCategoryId = pscg.id
  left join (select dsi.PartsSalesCategoryId,
                    (case
                      when c.CityLevel is null then
                       66
                      else
                       c.CityLevel
                    end) as CityLevel,
                    dsi.ChannelCapabilityId,
                    count(d.Code) as TotalQtyOfSets
               from gcdcs.company c
              inner join gcdcs.Dealer d
                 on c.Code = d.Code
                and c.type in (2, 7, 10)
              inner join gcdcs.DealerServiceInfo dsi
                 on dsi.DealerId = d.id
                and dsi.ChannelCapabilityId in (1, 2)
                and dsi.status = 1
                and dsi.PartsSalesCategoryId not in (5, 6)
              group by dsi.PartsSalesCategoryId,
                       c.CityLevel,
                       dsi.ChannelCapabilityId) d
    on d.PartsSalesCategoryId = pscg.id
   and d.CityLevel = b.CityLevel
   and d.ChannelCapabilityId = b.ChannelCapabilityId
 group by a.code,
          a.PartsSalesCategoryId,
          pscg.name,
          a.TotalService,
          b.CityLevel,
          b.星级,
          b.ChannelCapabilityId,
          b.NetworkNumber,
          d.TotalQtyOfSets
union all --sddcs 
select a.code as BranchCode,
       a.PartsSalesCategoryId,
       b.CityLevel,
       b.ChannelCapabilityId,
       b.星级,
       pscg.name as 品牌,
       a.TotalService as 网络总数,
       (case b.CityLevel
         when 1 then
          '省级'
         when 2 then
          '地级'
         when 3 then
          '县级'
         else
          '无城市级别'
       end) as 城市级别,
       (case b.ChannelCapabilityId
         when 1 then
          '4s店'
         when 2 then
          '服务站'
         else
          '无业务能力类别'
       end) as 业务能力,
       b.NetworkNumber as 网络数量,
       to_char(round((b.NetworkNumber / d.TotalQtyOfSets * 100), 2),
               'fm99990.90') || '%' as 所占比率,
       d.TotalQtyOfSets as 合计数量,
       to_char(round((d.TotalQtyOfSets / a.TotalService * 100), 2),
               'fm99990.90') || '%' as 合计比率
  from sddcs.PartsSalesCategory pscg
 inner join (select bff.code,
                    dsi.PartsSalesCategoryId,
                    count(d.Code) as TotalService
               from sddcs.company c
              inner join sddcs.Dealer d
                 on c.Code = d.Code
                and c.type in (2, 7, 10)
              inner join sddcs.DealerServiceInfo dsi
                 on dsi.DealerId = d.id
                and dsi.ChannelCapabilityId in (1, 2)
                and dsi.status = 1
                and dsi.PartsSalesCategoryId not in (5, 6)
              inner join sddcs.Branch bff
                 on dsi.BranchId = bff.id
              group by dsi.PartsSalesCategoryId, bff.code) a
    on a.PartsSalesCategoryid = pscg.id
 inner join (select bff.code,
                    dsi.PartsSalesCategoryId,
                    (case
                      when c.CityLevel is null then
                       66
                      else
                       c.CityLevel
                    end) as CityLevel,
                    dsi.ChannelCapabilityId,
                    dsi.Grade 星级,
                    count(d.Code) as NetworkNumber
               from sddcs.company c
              inner join sddcs.Dealer d
                 on c.Code = d.Code
                and c.type in (2, 7, 10)
              inner join sddcs.DealerServiceInfo dsi
                 on dsi.DealerId = d.id
                and dsi.ChannelCapabilityId in (1, 2)
                and dsi.status = 1
                and dsi.PartsSalesCategoryId not in (5, 6)
              inner join sddcs.Branch bff
                 on dsi.BranchId = bff.id
              inner join sddcs.GradeCoefficient gc
                 on gc.id = dsi.GradeCoefficientId
                and gc.PartsSalesCategoryId = dsi.PartsSalesCategoryId
              group by dsi.PartsSalesCategoryId,
                       c.CityLevel,
                       dsi.ChannelCapabilityId,
                       dsi.Grade,
                       bff.code) b
    on b.PartsSalesCategoryId = pscg.id
  left join (select dsi.PartsSalesCategoryId,
                    (case
                      when c.CityLevel is null then
                       66
                      else
                       c.CityLevel
                    end) as CityLevel,
                    dsi.ChannelCapabilityId,
                    count(d.Code) as TotalQtyOfSets
               from sddcs.company c
              inner join sddcs.Dealer d
                 on c.Code = d.Code
                and c.type in (2, 7, 10)
              inner join sddcs.DealerServiceInfo dsi
                 on dsi.DealerId = d.id
                and dsi.ChannelCapabilityId in (1, 2)
                and dsi.status = 1
                and dsi.PartsSalesCategoryId not in (5, 6)
              group by dsi.PartsSalesCategoryId,
                       c.CityLevel,
                       dsi.ChannelCapabilityId) d
    on d.PartsSalesCategoryId = pscg.id
   and d.CityLevel = b.CityLevel
   and d.ChannelCapabilityId = b.ChannelCapabilityId
 group by a.code,
          a.PartsSalesCategoryId,
          pscg.name,
          a.TotalService,
          b.CityLevel,
          b.星级,
          b.ChannelCapabilityId,
          b.NetworkNumber,
          d.TotalQtyOfSets
union all --dcs
select a.code as BranchCode,
       a.PartsSalesCategoryId,
       b.CityLevel,
       b.ChannelCapabilityId,
       b.星级,
       pscg.name as 品牌,
       a.TotalService as 网络总数,
       (case b.CityLevel
         when 1 then
          '省级'
         when 2 then
          '地级'
         when 3 then
          '县级'
         else
          '无城市级别'
       end) as 城市级别,
       (case b.ChannelCapabilityId
         when 1 then
          '4s店'
         when 2 then
          '服务站'
         else
          '无业务能力类别'
       end) as 业务能力,
       b.NetworkNumber as 网络数量,
       to_char(round((b.NetworkNumber / d.TotalQtyOfSets * 100), 2),
               'fm99990.90') || '%' as 所占比率,
       d.TotalQtyOfSets as 合计数量,
       to_char(round((d.TotalQtyOfSets / a.TotalService * 100), 2),
               'fm99990.90') || '%' as 合计比率
  from dcs.PartsSalesCategory pscg
 inner join (select bff.code,
                    dsi.PartsSalesCategoryId,
                    count(d.Code) as TotalService
               from dcs.company c
              inner join dcs.Dealer d
                 on c.Code = d.Code
                and c.type in (2, 7, 10)
              inner join dcs.DealerServiceInfo dsi
                 on dsi.DealerId = d.id
                and dsi.ChannelCapabilityId in (1, 2)
                and dsi.status = 1
                and dsi.PartsSalesCategoryId not in (5, 6)
              inner join dcs.Branch bff
                 on dsi.BranchId = bff.id
              group by dsi.PartsSalesCategoryId, bff.code) a
    on a.PartsSalesCategoryid = pscg.id
 inner join (select bff.code,
                    dsi.PartsSalesCategoryId,
                    (case
                      when c.CityLevel is null then
                       66
                      else
                       c.CityLevel
                    end) as CityLevel,
                    dsi.ChannelCapabilityId,
                    dsi.Grade 星级,
                    count(d.Code) as NetworkNumber
               from dcs.company c
              inner join dcs.Dealer d
                 on c.Code = d.Code
                and c.type in (2, 7, 10)
              inner join dcs.DealerServiceInfo dsi
                 on dsi.DealerId = d.id
                and dsi.ChannelCapabilityId in (1, 2)
                and dsi.status = 1
                and dsi.PartsSalesCategoryId not in (5, 6)
              inner join dcs.Branch bff
                 on dsi.BranchId = bff.id
              inner join dcs.GradeCoefficient gc
                 on gc.id = dsi.GradeCoefficientId
                and gc.PartsSalesCategoryId = dsi.PartsSalesCategoryId
              group by dsi.PartsSalesCategoryId,
                       c.CityLevel,
                       dsi.ChannelCapabilityId,
                       dsi.Grade,
                       bff.code) b
    on b.PartsSalesCategoryId = pscg.id
  left join (select dsi.PartsSalesCategoryId,
                    (case
                      when c.CityLevel is null then
                       66
                      else
                       c.CityLevel
                    end) as CityLevel,
                    dsi.ChannelCapabilityId,
                    count(d.Code) as TotalQtyOfSets
               from dcs.company c
              inner join dcs.Dealer d
                 on c.Code = d.Code
                and c.type in (2, 7, 10)
              inner join dcs.DealerServiceInfo dsi
                 on dsi.DealerId = d.id
                and dsi.ChannelCapabilityId in (1, 2)
                and dsi.status = 1
                and dsi.PartsSalesCategoryId not in (5, 6)
              group by dsi.PartsSalesCategoryId,
                       c.CityLevel,
                       dsi.ChannelCapabilityId) d
    on d.PartsSalesCategoryId = pscg.id
   and d.CityLevel = b.CityLevel
   and d.ChannelCapabilityId = b.ChannelCapabilityId
 group by a.code,
          a.PartsSalesCategoryId,
          pscg.name,
          a.TotalService,
          b.CityLevel,
          b.星级,
          b.ChannelCapabilityId,
          b.NetworkNumber,
          d.TotalQtyOfSets
