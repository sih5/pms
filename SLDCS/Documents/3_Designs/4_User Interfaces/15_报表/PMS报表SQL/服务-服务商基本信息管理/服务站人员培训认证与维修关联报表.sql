select (select name
          from dcs.partssalescategory c
         where c.id = a.partssalescategoryid) Ʒ��,
       a.PartsSalesCategoryId,
       (select d.ProductLineName
          from dcs.serviceproductlineview d
         where a.serviceproductlineid = d.ProductLineId) �����Ʒ��,
       a.ServiceProductLineId,
       (select e.name
          from dcs.marketingdepartment e
         where e.id = a.marketingdepartmentid) �г���,
       a.MarketingDepartmentId,
       a.dealercode ����վ���,
       a.dealername ����վ����,
       b.repairworkername ά�޹�,
       f.TrainingName ��ѵ�γ�,
       f.TrainingTime ��ѵʱ��,
       a.claimbillcode ά�����ⵥ��,
       h.value ά�����ⵥ״̬,
       a.Status,
       a.vin VIN��,
       a.enginemodel �������ͺ�,
       a.RepairRequestTime ά��ʱ��,
       a.MalfunctionCode ���ϴ���,
       a.malfunctiondescription ������������,
       b.repairitemcode ά����Ŀ���,
       b.repairitemname ά����Ŀ����,
       a.faultypartsassemblyname ���׼�����
  from dcs.repairclaimbill a
 inner join dcs.repairclaimitemdetail b
    on a.id = b.repairclaimbillid
  left join dcs.DealerKeyEmployee g
    on g.id = b.repairworkerid
  left join dcs.DealerPerTrainAut f
    on f.partssalescategoryid = a.partssalescategoryid
   and f.dealerid = a.dealerid
   and f.positionid = g.keypositionid
   and f.name = b.repairworkername
  left join dcs.keyvalueitem h
    on h.caption = 'ά�����ⵥ����״̬'
   and h.key = a.status
union
select (select name
          from yxdcs.partssalescategory c
         where c.id = a.partssalescategoryid) Ʒ��,
       a.PartsSalesCategoryId,
       (select d.ProductLineName
          from yxdcs.serviceproductlineview d
         where a.serviceproductlineid = d.ProductLineId) �����Ʒ��,
       a.ServiceProductLineId,
       (select e.name
          from yxdcs.marketingdepartment e
         where e.id = a.marketingdepartmentid) �г���,
       a.MarketingDepartmentId,
       a.dealercode ����վ���,
       a.dealername ����վ����,
       b.repairworkername ά�޹�,
       f.TrainingName ��ѵ�γ�,
       f.TrainingTime ��ѵʱ��,
       a.claimbillcode ά�����ⵥ��,
       h.value ά�����ⵥ״̬,
       a.Status,
       a.vin VIN��,
       a.enginemodel �������ͺ�,
       a.RepairRequestTime ά��ʱ��,
       a.MalfunctionCode ���ϴ���,
       a.malfunctiondescription ������������,
       b.repairitemcode ά����Ŀ���,
       b.repairitemname ά����Ŀ����,
       a.faultypartsassemblyname ���׼�����
  from yxdcs.repairclaimbill a
 inner join yxdcs.repairclaimitemdetail b
    on a.id = b.repairclaimbillid
  left join yxdcs.DealerKeyEmployee g
    on g.id = b.repairworkerid
  left join yxdcs.DealerPerTrainAut f
    on f.partssalescategoryid = a.partssalescategoryid
   and f.dealerid = a.dealerid
   and f.positionid = g.keypositionid
   and f.name = b.repairworkername
  left join yxdcs.keyvalueitem h
    on h.caption = 'ά�����ⵥ����״̬'
   and h.key = a.status
union
select (select name
          from gcdcs.partssalescategory c
         where c.id = a.partssalescategoryid) Ʒ��,
       a.PartsSalesCategoryId,
       (select d.ProductLineName
          from gcdcs.serviceproductlineview d
         where a.serviceproductlineid = d.ProductLineId) �����Ʒ��,
       a.ServiceProductLineId,
       (select e.name
          from gcdcs.marketingdepartment e
         where e.id = a.marketingdepartmentid) �г���,
       a.MarketingDepartmentId,
       a.dealercode ����վ���,
       a.dealername ����վ����,
       b.repairworkername ά�޹�,
       f.TrainingName ��ѵ�γ�,
       f.TrainingTime ��ѵʱ��,
       a.claimbillcode ά�����ⵥ��,
       h.value ά�����ⵥ״̬,
       a.Status,
       a.vin VIN��,
       a.enginemodel �������ͺ�,
       a.RepairRequestTime ά��ʱ��,
       a.MalfunctionCode ���ϴ���,
       a.malfunctiondescription ������������,
       b.repairitemcode ά����Ŀ���,
       b.repairitemname ά����Ŀ����,
       a.faultypartsassemblyname ���׼�����
  from gcdcs.repairclaimbill a
 inner join gcdcs.repairclaimitemdetail b
    on a.id = b.repairclaimbillid
  left join gcdcs.DealerKeyEmployee g
    on g.id = b.repairworkerid
  left join gcdcs.DealerPerTrainAut f
    on f.partssalescategoryid = a.partssalescategoryid
   and f.dealerid = a.dealerid
   and f.positionid = g.keypositionid
   and f.name = b.repairworkername
  left join gcdcs.keyvalueitem h
    on h.caption = 'ά�����ⵥ����״̬'
   and h.key = a.status
union
select (select name
          from sddcs.partssalescategory c
         where c.id = a.partssalescategoryid) Ʒ��,
       a.PartsSalesCategoryId,
       (select d.ProductLineName
          from sddcs.serviceproductlineview d
         where a.serviceproductlineid = d.ProductLineId) �����Ʒ��,
       a.ServiceProductLineId,
       (select e.name
          from sddcs.marketingdepartment e
         where e.id = a.marketingdepartmentid) �г���,
       a.MarketingDepartmentId,
       a.dealercode ����վ���,
       a.dealername ����վ����,
       b.repairworkername ά�޹�,
       f.TrainingName ��ѵ�γ�,
       f.TrainingTime ��ѵʱ��,
       a.claimbillcode ά�����ⵥ��,
       h.value ά�����ⵥ״̬,
       a.Status,
       a.vin VIN��,
       a.enginemodel �������ͺ�,
       a.RepairRequestTime ά��ʱ��,
       a.MalfunctionCode ���ϴ���,
       a.malfunctiondescription ������������,
       b.repairitemcode ά����Ŀ���,
       b.repairitemname ά����Ŀ����,
       a.faultypartsassemblyname ���׼�����
  from sddcs.repairclaimbill a
 inner join sddcs.repairclaimitemdetail b
    on a.id = b.repairclaimbillid
  left join sddcs.DealerKeyEmployee g
    on g.id = b.repairworkerid
  left join sddcs.DealerPerTrainAut f
    on f.partssalescategoryid = a.partssalescategoryid
   and f.dealerid = a.dealerid
   and f.positionid = g.keypositionid
   and f.name = b.repairworkername
  left join sddcs.keyvalueitem h
    on h.caption = 'ά�����ⵥ����״̬'
   and h.key = a.status
