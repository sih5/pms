select type,
       partssalescategoryid,
       branchid,
       (select code from dcs.branch where branch.id=branchid)branchcode,
       工单类型,
       (select name
          from dcs.partssalescategory
         where partssalescategory.id = partssalescategoryid) 品牌,
      sum(是否已响应) as 已响应数量,
      sum(是否已派工) as  PMS系统派工单接收数量,
      sum(是否接收 ) as 接受数量,
      sum(响应时长) as 响应时长总,--
      sum(接收响应时长) as 接收响应时长总,--
      sum(拒绝响应时长) as 拒绝响应时长总,--
      sum(待出发时长) as 待出发时长总,--
      sum(路程时长) as 路程时长总, --
      sum(救援准备时长) as 救援准备时长总,--
      sum(报修准备时长) as 报修准备时长总,--
      sum(维修时长) as 维修时长总,--
      sum(已接受到处理完毕时长) as 已接受到处理完毕时长总,--
      sum(已派工到处理完毕时长) as 已派工到处理完毕时长总, --
      (select count(*)from dcs.repairworkordermd v where v.partssalescategoryid=t.partssalescategoryid
        and v.receivetime is not null  and  sendtime between @Prompt('CreateTimeBegin', 'D',, Mono, Free, Persistent) and  @Prompt('CreateTimeEnd', 'D',, Mono, Free, Persistent)) as 反馈数量总,--
     case when sum(是否已派工)>0 then round (sum(是否接收 )/ sum(是否已派工) ,4) else 0 end 接收比率,
      sum(是否拒绝) 拒绝数量,
     case when sum(是否已派工)>0 then round(sum(是否拒绝)/sum(是否已派工),4) else 0 end  拒绝比率,
      sum(是否20分钟内响应) 二十分钟内响应,
     case when sum(是否已响应)>0 then round(sum(是否20分钟内响应)/ sum(是否已响应),4) else 0 end  二十分钟内响应比率  ,
      sum(是否超20分钟内响应) 超20分钟响应,
     case when sum(是否已响应)>0 then round(sum(是否超20分钟内响应)/sum(是否已响应),4 )else 0 end 超二十分钟响应比率,
     case when sum(是否已响应)>0 then round( sum(响应时长)/sum(是否已响应),4) else 0 end  平均响应时长分,
      sum(接收是否20分钟内响应)  已接受二十分钟内响应,
     case when sum(是否接收)>0   then round(sum(接收是否20分钟内响应)/ sum(是否接收),4) else 0 end 已接受二十分钟内响应比率  ,
      sum(接收是否超20分钟内响应) 接受超20分钟响应,
     case when sum(是否接收)>0   then round(sum(接收是否超20分钟内响应)/sum(是否接收),4) else 0 end 已接受超二十分钟响应比率,
     case when   sum(是否接收)>0 then round( sum(接收响应时长)/sum(是否接收),4) else 0 end 已接受平均响应时长分,
      sum(拒绝是否20分钟内响应)  已拒绝二十分钟内响应,
     case when  sum(是否拒绝)>0  then round(sum(拒绝是否20分钟内响应)/ sum(是否拒绝),4)else 0 end   已拒绝二十分钟内响应比率  ,
      sum(拒绝是否超20分钟内响应) 拒绝超20分钟响应,
     case when sum(是否拒绝)>0   then round(sum(拒绝是否超20分钟内响应)/sum(是否拒绝),4) else 0 end  已拒绝超二十分钟响应比率,
     case when sum(是否拒绝)>0   then round( sum(拒绝响应时长)/sum(是否拒绝),4)  else 0 end  已拒绝平均响应时长分,
      sum(是否出发)出发数量 ,
     case when sum(是否出发)>0   then round(sum(待出发时长)/sum(是否出发),4)  else 0 end  待出发时长分,
      sum(是否到达)已到达数量 ,
     case when sum(是否到达)>0   then round(sum(路程时长)/ sum(是否到达),4) else 0 end 路程时长分,
      sum(是否到站)已到站 ,
     case when sum(是否到达)>0   then round(sum(路程时长)/ sum(是否到达),4) else 0 end 到站时长分,
      sum(是否开始维修) 开始维修数量,
     case when sum(是否到达)>0   then round(sum(救援准备时长)/sum(是否到达),4) else 0 end  救援准备时长分,
     case when sum(是否开始维修)>0 then round(sum(报修准备时长)/sum(是否开始维修),4) else 0 end  救援准备时长,
      sum(是否处理完毕) 处理完毕数量,
     case when sum(是否处理完毕)>0 then round( sum(维修时长)/sum(是否处理完毕),4) else 0 end  维修时长小时,  
     case when sum(是否处理完毕)>0 then round( sum(已接受到处理完毕时长)/sum(是否处理完毕),4) else 0 end   已接受到处理完毕时长小时,
     case when sum(是否处理完毕)>0 then round( sum(已派工到处理完毕时长)/sum(是否处理完毕),4) else 0 end   已派工到处理完毕时长小时 ,
      sum(是否已使用) 关联维修单数量,
     case when sum(是否处理完毕)>0 then round( sum(是否已使用)/sum(是否处理完毕),4) else 0 end  关联维修单数量比率,
      sum(二十四小时完成) 是的数量,
     case when sum(是否接收 )>0    then round(sum(二十四小时完成)/sum(是否接收 ) ,4)  else 0 end 是的比率,
      sum(二十四小时未完成) 否的数量,
     case when sum(是否接收 )>0    then round(sum(二十四小时未完成)/sum(是否接收 ) ,4) else 0 end 否的比率,
      sum(二十四小时未反馈) 未反馈的数量,
     case when sum(是否接收 )>0    then round(sum(二十四小时未反馈)/sum(是否接收 ) ,4) else 0 end 未反馈的比率,
        sum(是否无配件) 无配件数量, 
     case when sum(是否接收)>0     then round (sum(是否无配件)/sum(是否接收),4) else 0 end 无配件比率,
        sum(是否无维修工) 无维修工数量,
     case when sum(是否接收)>0     then round (sum(是否无维修工)/sum(是否接收),4)else 0 end 无维修工比率,
        sum(是否无救援车) 无救援车数量,
     case when sum(是否接收)>0     then round (sum(是否无救援车)/sum(是否接收),4) else 0 end 无救援车比率,
        sum(是否无维修权限) 无维修权限数量,
     case when sum(是否接收)>0     then round (sum(是否无维修权限)/sum(是否接收),4) else 0 end 无维修权限比率,
        sum(是否距离客户较远) 距离客户较远数量,
     case when sum(是否接收)>0     then round (sum(是否距离客户较远)/sum(是否接收),4) else 0 end 距离客户较远比率,
        sum(是否已撤站) 已撤站数量,
     case when sum(是否接收)>0     then round (sum(是否已撤站)/sum(是否接收),4) else 0 end 已撤站比率,
        sum(是否无维修能力) 无维修能力数量,
     case when sum(是否接收)>0     then round (sum(是否无维修能力)/sum(是否接收),4) else 0 end 无维修能力比率,
        sum(是否非事业本部负责品牌) 非事业本部负责品牌数量,
     case when sum(是否接收)>0     then round (sum(是否非事业本部负责品牌)/sum(是否接收),4) else 0 end 非事业本部负责品牌比率,
        sum(是否非福田客户) 非福田客户数量, 
     case when sum(是否接收)>0     then round (sum(是否非福田客户)/sum(是否接收),4) else 0 end 非福田客户比率,
        sum(是否客户取消预约) 客户取消预约数量,
     case when sum(是否接收)>0     then round (sum(是否客户取消预约)/sum(是否接收),4) else 0 end 客户取消预约比率,
          sum(是否其它)其它数量, 
     case when sum(是否接收)>0     then round (sum(是否其它)/sum(是否接收),4) else 0 end 其它比率,
     case when 工单类型 ='抱怨工单'then  sum(是否接收 ) else 0 end as 抱怨反馈数量,    
     case when 工单类型 ='抱怨工单'then  round(sum(是否接收 )/(select count(*)from dcs.repairworkordermd v where v.partssalescategoryid=t.partssalescategoryid
        and v.receivetime is not null  and  sendtime between @Prompt('CreateTimeBegin', 'D',, Mono, Free, Persistent) and  @Prompt('CreateTimeEnd', 'D',, Mono, Free, Persistent)),4) else 0 end as 抱怨反馈比率,  
                   case when 工单类型 ='抱怨工单'then   round( sum(已接受到处理完毕时长)/ sum(是否接收 ) ,4) else 0 end as 抱怨反馈时长小时           
  from (select case when status > 2 then 1 else  0  end as 是否已响应,
               case  when status >= 2 then 1  else 0 end as 是否已派工,
               decode(ReceiveTime, null, 0, 1) 是否接收,
               decode(RejectTime, null, 0, 1) 是否拒绝,
               case   when ReceiveTime is not null then
                   case  when (ReceiveTime - SendTime) * 60 * 24 > 20 then 0  else 1  end
                      when RejectTime is not null then 
                        case when (RejectTime - SendTime) * 60 * 24 > 20 then 0 else 1 end
               end 是否20分钟内响应,
               case  when ReceiveTime is not null then
                  case when (ReceiveTime - SendTime) * 60 * 24 > 20 then 1 else 0 end
                     when RejectTime is not null then
                  case  when (RejectTime - SendTime) * 60 * 24 > 20 then 1 else 0  end
               end 是否超20分钟内响应,
               case when ReceiveTime is not null then
                  (ReceiveTime - SendTime) * 60 * 24
                    when RejectTime is not null then
                  (RejectTime - SendTime) * 60 * 24
               end 响应时长,
               case when ReceiveTime is not null then
                  case  when (ReceiveTime - SendTime) * 60 * 24 > 20 then 0 else 1 end
               end 接收是否20分钟内响应,
               case when ReceiveTime is not null then
                  case when (ReceiveTime - SendTime) * 60 * 24 > 20 then 1 else 0 end
               end 接收是否超20分钟内响应,
               case when ReceiveTime is not null then (ReceiveTime - SendTime) * 60 * 24
               end 接收响应时长,
               case  when RejectTime is not null then
                  case when (RejectTime - SendTime) * 60 * 24 > 20 then 0 else 1 end
               end 拒绝是否20分钟内响应,
               case when RejectTime is not null then
                  case when (RejectTime - SendTime) * 60 * 24 > 20 then 1  else  0 end
               end 拒绝是否超20分钟内响应,
               case when RejectTime is not null then (RejectTime - SendTime) * 60 * 24
               end 拒绝响应时长,
               decode(LeavingTime, null, 0, 1) 是否出发,
               case  when (ReceiveTime is not null) and (LeavingTime is not null) then (LeavingTime - ReceiveTime) * 60 * 24
               end 待出发时长,
               decode(ArriveTime, null, 0, 1) 是否到达,
               case when (ReceiveTime is not null) and (ArriveTime is not null) then (ArriveTime - ReceiveTime) * 60 * 24
               end 路程时长,
               decode(ArriveStationTime, null, 0, 1) 是否到站,
               case when (ReceiveTime is not null) and (ArriveStationTime is not null) then (ArriveStationTime - ReceiveTime) * 60 * 24
               end 到站时长,
               decode(StartRepairTime, null, 0, 1) 是否开始维修,
               case when (ArriveTime is not null) and (StartRepairTime is not null) then (StartRepairTime - ArriveTime) * 60 * 24
               end 救援准备时长,
               case  when (ArriveStationTime is not null) and (StartRepairTime is not null) then (StartRepairTime - ArriveStationTime) * 60 * 24
               end 报修准备时长,
               decode(FimishTime, null, 0, 1) 是否处理完毕,
               case  when (FimishTime is not null) and (StartRepairTime is not null) then(FimishTime - StartRepairTime)  * 24
               end 维修时长,
               case when (FimishTime is not null) and (ReceiveTime is not null) then (FimishTime - ReceiveTime)  * 24
               end 已接受到处理完毕时长,
               case when (FimishTime is not null) then (FimishTime - SendTime)  * 24
               end 已派工到处理完毕时长,
               IsUsed 是否已使用,
               case when (ReceiveTime is not null) then  decode(IsCanFinish, 1, 1, 0)
               end as 二十四小时完成,
               case  when (ReceiveTime is not null) then decode(IsCanFinish, 2, 1, 0)
               end as 二十四小时未完成,
               case when (ReceiveTime is not null) then  decode(IsCanFinish, 0, 1, 0)
               end as 二十四小时未反馈,
               case when (RejectTime is not null) then decode(RefuseReason, 1, 1, 0)
               end as 是否无配件,
               case when (RejectTime is not null) then decode(RefuseReason, 2, 1, 0)
               end as 是否无维修工,
               case when (RejectTime is not null) then  decode(RefuseReason, 4, 1, 0)
               end as 是否无维修权限,
               case when (RejectTime is not null) then  decode(RefuseReason, 3, 1, 0)
               end as 是否无救援车,
               case when (RejectTime is not null) then  decode(RefuseReason, 5, 1, 0)
               end as 是否距离客户较远,
               case when (RejectTime is not null) then  decode(RefuseReason, 6, 1, 0)
               end as 是否已撤站,
               case when (RejectTime is not null) then decode(RefuseReason, 7, 1, 0)
               end as 是否无维修能力,
               case when (RejectTime is not null) then decode(RefuseReason, 8, 1, 0)
               end as 是否非事业本部负责品牌,
               case  when (RejectTime is not null) then  decode(RefuseReason, 9, 1, 0)
               end as 是否非福田客户,
               case when (RejectTime is not null) then  decode(RefuseReason, 10, 1, 0)
               end as 是否客户取消预约,
               case when (RejectTime is not null) then  decode(RefuseReason, 11, 1, 0)
               end as 是否其它,
               RejectTime,
               ReceiveTime,
               SendTime,
               code,
               partssalescategoryid　,
               status,
               Type,
               repairworkordermd.branchid,
               decode(Type, 1, '抱怨工单', 2, '救援工单', 3, '报修工单') 工单类型
          from dcs.repairworkordermd where sendtime between @Prompt('CreateTimeBegin', 'D',, Mono, Free, Persistent) and  @Prompt('CreateTimeEnd', 'D',, Mono, Free, Persistent))t
 group by type, partssalescategoryid, 工单类型,branchid 
 
 union
 select  type,
       partssalescategoryid,
        branchid,
       (select code from yxdcs.branch where branch.id=branchid)branchcode,
       工单类型,
       (select name
          from yxdcs.partssalescategory
         where partssalescategory.id = partssalescategoryid) 品牌,
      sum(是否已响应) 已响应数量,
      sum(是否已派工) PMS系统派工单接收数量,
      sum(是否接收 ) 接受数量,
            sum(响应时长) as 响应时长总,--
      sum(接收响应时长) as 接收响应时长总,--
      sum(拒绝响应时长) as 拒绝响应时长总,--
      sum(待出发时长) as 待出发时长总,--
      sum(路程时长) as 路程时长总, --
      sum(救援准备时长) as 救援准备时长总,--
      sum(报修准备时长) as 报修准备时长总,--
      sum(维修时长) as 维修时长总,--
      sum(已接受到处理完毕时长) as 已接受到处理完毕时长总,--
      sum(已派工到处理完毕时长) as 已派工到处理完毕时长总, --
      (select count(*)from dcs.repairworkordermd v where v.partssalescategoryid=t.partssalescategoryid
        and v.receivetime is not null  and  sendtime between @Prompt('CreateTimeBegin', 'D',, Mono, Free, Persistent) and  @Prompt('CreateTimeEnd', 'D',, Mono, Free, Persistent)) as 反馈数量总,--
     case when sum(是否已派工)>0 then round (sum(是否接收 )/ sum(是否已派工) ,4) else 0 end 接收比率,
      sum(是否拒绝) 拒绝数量,
     case when sum(是否已派工)>0 then round(sum(是否拒绝)/sum(是否已派工),4) else 0 end  拒绝比率,
      sum(是否20分钟内响应) 二十分钟内响应,
     case when sum(是否已响应)>0 then round(sum(是否20分钟内响应)/ sum(是否已响应),4) else 0 end  二十分钟内响应比率  ,
      sum(是否超20分钟内响应) 超20分钟响应,
     case when sum(是否已响应)>0 then round(sum(是否超20分钟内响应)/sum(是否已响应),4 )else 0 end 超二十分钟响应比率,
     case when sum(是否已响应)>0 then round( sum(响应时长)/sum(是否已响应),4) else 0 end  平均响应时长分,
      sum(接收是否20分钟内响应)  已接受二十分钟内响应,
     case when sum(是否接收)>0   then round(sum(接收是否20分钟内响应)/ sum(是否接收),4) else 0 end 已接受二十分钟内响应比率  ,
      sum(接收是否超20分钟内响应) 接受超20分钟响应,
     case when sum(是否接收)>0   then round(sum(接收是否超20分钟内响应)/sum(是否接收),4) else 0 end 已接受超二十分钟响应比率,
     case when   sum(是否接收)>0 then round( sum(接收响应时长)/sum(是否接收),4) else 0 end 已接受平均响应时长分,
      sum(拒绝是否20分钟内响应)  已拒绝二十分钟内响应,
     case when  sum(是否拒绝)>0  then round(sum(拒绝是否20分钟内响应)/ sum(是否拒绝),4)else 0 end   已拒绝二十分钟内响应比率  ,
      sum(拒绝是否超20分钟内响应) 拒绝超20分钟响应,
     case when sum(是否拒绝)>0   then round(sum(拒绝是否超20分钟内响应)/sum(是否拒绝),4) else 0 end  已拒绝超二十分钟响应比率,
     case when sum(是否拒绝)>0   then round( sum(拒绝响应时长)/sum(是否拒绝),4)  else 0 end  已拒绝平均响应时长分,
      sum(是否出发)出发数量 ,
     case when sum(是否出发)>0   then round(sum(待出发时长)/sum(是否出发),4)  else 0 end  待出发时长分,
      sum(是否到达)已到达数量 ,
     case when sum(是否到达)>0   then round(sum(路程时长)/ sum(是否到达),4) else 0 end 路程时长分,
      sum(是否到站)已到站 ,
     case when sum(是否到达)>0   then round(sum(路程时长)/ sum(是否到达),4) else 0 end 到站时长分,
      sum(是否开始维修) 开始维修数量,
     case when sum(是否到达)>0   then round(sum(救援准备时长)/sum(是否到达),4) else 0 end  救援准备时长分,
     case when sum(是否开始维修)>0 then round(sum(报修准备时长)/sum(是否开始维修),4) else 0 end  救援准备时长,
      sum(是否处理完毕) 处理完毕数量,
     case when sum(是否处理完毕)>0 then round( sum(维修时长)/sum(是否处理完毕),4) else 0 end  维修时长小时,  
     case when sum(是否处理完毕)>0 then round( sum(已接受到处理完毕时长)/sum(是否处理完毕),4) else 0 end   已接受到处理完毕时长小时,
     case when sum(是否处理完毕)>0 then round( sum(已派工到处理完毕时长)/sum(是否处理完毕),4) else 0 end   已派工到处理完毕时长小时 ,
      sum(是否已使用) 关联维修单数量,
     case when sum(是否处理完毕)>0 then round( sum(是否已使用)/sum(是否处理完毕),4) else 0 end  关联维修单数量比率,
      sum(二十四小时完成) 是的数量,
     case when sum(是否接收 )>0    then round(sum(二十四小时完成)/sum(是否接收 ) ,4)  else 0 end 是的比率,
      sum(二十四小时未完成) 否的数量,
     case when sum(是否接收 )>0    then round(sum(二十四小时未完成)/sum(是否接收 ) ,4) else 0 end 否的比率,
      sum(二十四小时未反馈) 未反馈的数量,
     case when sum(是否接收 )>0    then round(sum(二十四小时未反馈)/sum(是否接收 ) ,4) else 0 end 未反馈的比率,
        sum(是否无配件) 无配件数量, 
     case when sum(是否接收)>0     then round (sum(是否无配件)/sum(是否接收),4) else 0 end 无配件比率,
        sum(是否无维修工) 无维修工数量,
     case when sum(是否接收)>0     then round (sum(是否无维修工)/sum(是否接收),4) else 0 end 无维修工比率,
        sum(是否无救援车) 无救援车数量,
     case when sum(是否接收)>0     then round (sum(是否无救援车)/sum(是否接收),4) else 0 end 无救援车比率,
        sum(是否无维修权限) 无维修权限数量,
     case when sum(是否接收)>0     then round (sum(是否无维修权限)/sum(是否接收),4) else 0 end 无维修权限比率,
        sum(是否距离客户较远) 距离客户较远数量,
     case when sum(是否接收)>0     then round (sum(是否距离客户较远)/sum(是否接收),4) else 0 end 距离客户较远比率,
        sum(是否已撤站) 已撤站数量,
     case when sum(是否接收)>0     then round (sum(是否已撤站)/sum(是否接收),4) else 0 end 已撤站比率,
        sum(是否无维修能力) 无维修能力数量,
     case when sum(是否接收)>0     then round (sum(是否无维修能力)/sum(是否接收),4) else 0 end 无维修能力比率,
        sum(是否非事业本部负责品牌) 非事业本部负责品牌数量,
     case when sum(是否接收)>0     then round (sum(是否非事业本部负责品牌)/sum(是否接收),4) else 0 end 非事业本部负责品牌比率,
        sum(是否非福田客户) 非福田客户数量, 
     case when sum(是否接收)>0     then round (sum(是否非福田客户)/sum(是否接收),4) else 0 end 非福田客户比率,
        sum(是否客户取消预约) 客户取消预约数量,
     case when sum(是否接收)>0     then round (sum(是否客户取消预约)/sum(是否接收),4) else 0 end 客户取消预约比率,
          sum(是否其它)其它数量, 
     case when sum(是否接收)>0     then round (sum(是否其它)/sum(是否接收),4) else 0 end 其它比率,
     case when 工单类型 ='抱怨工单'then  sum(是否接收 ) else 0 end as 抱怨反馈数量,    
     case when 工单类型 ='抱怨工单'then  round(sum(是否接收 )/(select count(*)from yxdcs.repairworkordermd v where v.partssalescategoryid=t.partssalescategoryid
        and v.receivetime is not null  and  sendtime between @Prompt('CreateTimeBegin', 'D',, Mono, Free, Persistent) and  @Prompt('CreateTimeEnd', 'D',, Mono, Free, Persistent)),4) else 0 end as 抱怨反馈比率,  
                   case when 工单类型 ='抱怨工单'then   round( sum(已接受到处理完毕时长)/ sum(是否接收 ) ,4) else 0 end as 抱怨反馈时长小时 
  from (select case when status > 2 then 1 else  0  end as 是否已响应,
               case  when status >= 2 then 1  else 0 end as 是否已派工,
               decode(ReceiveTime, null, 0, 1) 是否接收,
               decode(RejectTime, null, 0, 1) 是否拒绝,
               case   when ReceiveTime is not null then
                   case  when (ReceiveTime - SendTime) * 60 * 24 > 20 then 0  else 1  end
                      when RejectTime is not null then 
                        case when (RejectTime - SendTime) * 60 * 24 > 20 then 0 else 1 end
               end 是否20分钟内响应,
               case  when ReceiveTime is not null then
                  case when (ReceiveTime - SendTime) * 60 * 24 > 20 then 1 else 0 end
                     when RejectTime is not null then
                  case  when (RejectTime - SendTime) * 60 * 24 > 20 then 1 else 0  end
               end 是否超20分钟内响应,
               case when ReceiveTime is not null then
                  (ReceiveTime - SendTime) * 60 * 24
                    when RejectTime is not null then
                  (RejectTime - SendTime) * 60 * 24
               end 响应时长,
               case when ReceiveTime is not null then
                  case  when (ReceiveTime - SendTime) * 60 * 24 > 20 then 0 else 1 end
               end 接收是否20分钟内响应,
               case when ReceiveTime is not null then
                  case when (ReceiveTime - SendTime) * 60 * 24 > 20 then 1 else 0 end
               end 接收是否超20分钟内响应,
               case when ReceiveTime is not null then (ReceiveTime - SendTime) * 60 * 24
               end 接收响应时长,
               case  when RejectTime is not null then
                  case when (RejectTime - SendTime) * 60 * 24 > 20 then 0 else 1 end
               end 拒绝是否20分钟内响应,
               case when RejectTime is not null then
                  case when (RejectTime - SendTime) * 60 * 24 > 20 then 1  else  0 end
               end 拒绝是否超20分钟内响应,
               case when RejectTime is not null then (RejectTime - SendTime) * 60 * 24
               end 拒绝响应时长,
               decode(LeavingTime, null, 0, 1) 是否出发,
               case  when (ReceiveTime is not null) and (LeavingTime is not null) then (LeavingTime - ReceiveTime) * 60 * 24
               end 待出发时长,
               decode(ArriveTime, null, 0, 1) 是否到达,
               case when (ReceiveTime is not null) and (ArriveTime is not null) then (ArriveTime - ReceiveTime) * 60 * 24
               end 路程时长,
               decode(ArriveStationTime, null, 0, 1) 是否到站,
               case when (ReceiveTime is not null) and (ArriveStationTime is not null) then (ArriveStationTime - ReceiveTime) * 60 * 24
               end 到站时长,
               decode(StartRepairTime, null, 0, 1) 是否开始维修,
               case when (ArriveTime is not null) and (StartRepairTime is not null) then (StartRepairTime - ArriveTime) * 60 * 24
               end 救援准备时长,
               case  when (ArriveStationTime is not null) and (StartRepairTime is not null) then (StartRepairTime - ArriveStationTime) * 60 * 24
               end 报修准备时长,
               decode(FimishTime, null, 0, 1) 是否处理完毕,
               case  when (FimishTime is not null) and (StartRepairTime is not null) then(FimishTime - StartRepairTime)  * 24
               end 维修时长,
               case when (FimishTime is not null) and (ReceiveTime is not null) then (FimishTime - ReceiveTime)  * 24
               end 已接受到处理完毕时长,
               case when (FimishTime is not null) then (FimishTime - SendTime)  * 24
               end 已派工到处理完毕时长,
               IsUsed 是否已使用,
               case when (ReceiveTime is not null) then  decode(IsCanFinish, 1, 1, 0)
               end as 二十四小时完成,
               case  when (ReceiveTime is not null) then decode(IsCanFinish, 2, 1, 0)
               end as 二十四小时未完成,
               case when (ReceiveTime is not null) then  decode(IsCanFinish, 0, 1, 0)
               end as 二十四小时未反馈,
               case when (RejectTime is not null) then decode(RefuseReason, 1, 1, 0)
               end as 是否无配件,
               case when (RejectTime is not null) then decode(RefuseReason, 2, 1, 0)
               end as 是否无维修工,
               case when (RejectTime is not null) then  decode(RefuseReason, 4, 1, 0)
               end as 是否无维修权限,
               case when (RejectTime is not null) then  decode(RefuseReason, 3, 1, 0)
               end as 是否无救援车,
               case when (RejectTime is not null) then  decode(RefuseReason, 5, 1, 0)
               end as 是否距离客户较远,
               case when (RejectTime is not null) then  decode(RefuseReason, 6, 1, 0)
               end as 是否已撤站,
               case when (RejectTime is not null) then decode(RefuseReason, 7, 1, 0)
               end as 是否无维修能力,
               case when (RejectTime is not null) then decode(RefuseReason, 8, 1, 0)
               end as 是否非事业本部负责品牌,
               case  when (RejectTime is not null) then  decode(RefuseReason, 9, 1, 0)
               end as 是否非福田客户,
               case when (RejectTime is not null) then  decode(RefuseReason, 10, 1, 0)
               end as 是否客户取消预约,
               case when (RejectTime is not null) then  decode(RefuseReason, 11, 1, 0)
               end as 是否其它,
               RejectTime,
               ReceiveTime,
               SendTime,
               code,
               partssalescategoryid　,
               status,
               Type,
                repairworkordermd.branchid,
               decode(Type, 1, '抱怨工单', 2, '救援工单', 3, '报修工单') 工单类型
          from yxdcs.repairworkordermd where sendtime between @Prompt('CreateTimeBegin', 'D',, Mono, Free, Persistent) and  @Prompt('CreateTimeEnd', 'D',, Mono, Free, Persistent))t
 group by type, partssalescategoryid, 工单类型,branchid 
 union
 select  type,
       partssalescategoryid,
       
        branchid,
       (select code from gcdcs.branch where branch.id=branchid)branchcode,
       工单类型,
       (select name
          from gcdcs.partssalescategory
         where partssalescategory.id = partssalescategoryid) 品牌,
      sum(是否已响应) 已响应数量,
      sum(是否已派工) PMS系统派工单接收数量,
      sum(是否接收 ) 接受数量,
            sum(响应时长) as 响应时长总,--
      sum(接收响应时长) as 接收响应时长总,--
      sum(拒绝响应时长) as 拒绝响应时长总,--
      sum(待出发时长) as 待出发时长总,--
      sum(路程时长) as 路程时长总, --
      sum(救援准备时长) as 救援准备时长总,--
      sum(报修准备时长) as 报修准备时长总,--
      sum(维修时长) as 维修时长总,--
      sum(已接受到处理完毕时长) as 已接受到处理完毕时长总,--
      sum(已派工到处理完毕时长) as 已派工到处理完毕时长总, --
     (select count(*)from dcs.repairworkordermd v where v.partssalescategoryid=t.partssalescategoryid
        and v.receivetime is not null  and  sendtime between @Prompt('CreateTimeBegin', 'D',, Mono, Free, Persistent) and  @Prompt('CreateTimeEnd', 'D',, Mono, Free, Persistent)) as 反馈数量总,--
     case when sum(是否已派工)>0 then round (sum(是否接收 )/ sum(是否已派工) ,4) else 0 end 接收比率,
      sum(是否拒绝) 拒绝数量,
     case when sum(是否已派工)>0 then round(sum(是否拒绝)/sum(是否已派工),4) else 0 end  拒绝比率,
      sum(是否20分钟内响应) 二十分钟内响应,
     case when sum(是否已响应)>0 then round(sum(是否20分钟内响应)/ sum(是否已响应),4) else 0 end  二十分钟内响应比率  ,
      sum(是否超20分钟内响应) 超20分钟响应,
     case when sum(是否已响应)>0 then round(sum(是否超20分钟内响应)/sum(是否已响应),4 )else 0 end 超二十分钟响应比率,
     case when sum(是否已响应)>0 then round( sum(响应时长)/sum(是否已响应),4) else 0 end  平均响应时长分,
      sum(接收是否20分钟内响应)  已接受二十分钟内响应,
     case when sum(是否接收)>0   then round(sum(接收是否20分钟内响应)/ sum(是否接收),4) else 0 end 已接受二十分钟内响应比率  ,
      sum(接收是否超20分钟内响应) 接受超20分钟响应,
     case when sum(是否接收)>0   then round(sum(接收是否超20分钟内响应)/sum(是否接收),4) else 0 end 已接受超二十分钟响应比率,
     case when   sum(是否接收)>0 then round( sum(接收响应时长)/sum(是否接收),4) else 0 end 已接受平均响应时长分,
      sum(拒绝是否20分钟内响应)  已拒绝二十分钟内响应,
     case when  sum(是否拒绝)>0  then round(sum(拒绝是否20分钟内响应)/ sum(是否拒绝),4)else 0 end   已拒绝二十分钟内响应比率  ,
      sum(拒绝是否超20分钟内响应) 拒绝超20分钟响应,
     case when sum(是否拒绝)>0   then round(sum(拒绝是否超20分钟内响应)/sum(是否拒绝),4) else 0 end  已拒绝超二十分钟响应比率,
     case when sum(是否拒绝)>0   then round( sum(拒绝响应时长)/sum(是否拒绝),4)  else 0 end  已拒绝平均响应时长分,
      sum(是否出发)出发数量 ,
     case when sum(是否出发)>0   then round(sum(待出发时长)/sum(是否出发),4)  else 0 end  待出发时长分,
      sum(是否到达)已到达数量 ,
     case when sum(是否到达)>0   then round(sum(路程时长)/ sum(是否到达),4) else 0 end 路程时长分,
      sum(是否到站)已到站 ,
     case when sum(是否到达)>0   then round(sum(路程时长)/ sum(是否到达),4) else 0 end 到站时长分,
      sum(是否开始维修) 开始维修数量,
     case when sum(是否到达)>0   then round(sum(救援准备时长)/sum(是否到达),4) else 0 end  救援准备时长分,
     case when sum(是否开始维修)>0 then round(sum(报修准备时长)/sum(是否开始维修),4) else 0 end  救援准备时长,
      sum(是否处理完毕) 处理完毕数量,
     case when sum(是否处理完毕)>0 then round( sum(维修时长)/sum(是否处理完毕),4) else 0 end  维修时长小时,  
     case when sum(是否处理完毕)>0 then round( sum(已接受到处理完毕时长)/sum(是否处理完毕),4) else 0 end   已接受到处理完毕时长小时,
     case when sum(是否处理完毕)>0 then round( sum(已派工到处理完毕时长)/sum(是否处理完毕),4) else 0 end   已派工到处理完毕时长小时 ,
      sum(是否已使用) 关联维修单数量,
     case when sum(是否处理完毕)>0 then round( sum(是否已使用)/sum(是否处理完毕),4) else 0 end  关联维修单数量比率,
      sum(二十四小时完成) 是的数量,
     case when sum(是否接收 )>0    then round(sum(二十四小时完成)/sum(是否接收 ) ,4)  else 0 end 是的比率,
      sum(二十四小时未完成) 否的数量,
     case when sum(是否接收 )>0    then round(sum(二十四小时未完成)/sum(是否接收 ) ,4) else 0 end 否的比率,
      sum(二十四小时未反馈) 未反馈的数量,
     case when sum(是否接收 )>0    then round(sum(二十四小时未反馈)/sum(是否接收 ) ,4) else 0 end 未反馈的比率,
        sum(是否无配件) 无配件数量, 
     case when sum(是否接收)>0     then round (sum(是否无配件)/sum(是否接收),4) else 0 end 无配件比率,
        sum(是否无维修工) 无维修工数量,
     case when sum(是否接收)>0     then round (sum(是否无维修工)/sum(是否接收),4) else 0 end 无维修工比率,
        sum(是否无救援车) 无救援车数量,
     case when sum(是否接收)>0     then round (sum(是否无救援车)/sum(是否接收),4) else 0 end 无救援车比率,
        sum(是否无维修权限) 无维修权限数量,
     case when sum(是否接收)>0     then round (sum(是否无维修权限)/sum(是否接收),4) else 0 end 无维修权限比率,
        sum(是否距离客户较远) 距离客户较远数量,
     case when sum(是否接收)>0     then round (sum(是否距离客户较远)/sum(是否接收),4) else 0 end 距离客户较远比率,
        sum(是否已撤站) 已撤站数量,
     case when sum(是否接收)>0     then round (sum(是否已撤站)/sum(是否接收),4) else 0 end 已撤站比率,
        sum(是否无维修能力) 无维修能力数量,
     case when sum(是否接收)>0     then round (sum(是否无维修能力)/sum(是否接收),4) else 0 end 无维修能力比率,
        sum(是否非事业本部负责品牌) 非事业本部负责品牌数量,
     case when sum(是否接收)>0     then round (sum(是否非事业本部负责品牌)/sum(是否接收),4) else 0 end 非事业本部负责品牌比率,
        sum(是否非福田客户) 非福田客户数量, 
     case when sum(是否接收)>0     then round (sum(是否非福田客户)/sum(是否接收),4) else 0 end 非福田客户比率,
        sum(是否客户取消预约) 客户取消预约数量,
     case when sum(是否接收)>0     then round (sum(是否客户取消预约)/sum(是否接收),4) else 0 end 客户取消预约比率,
          sum(是否其它)其它数量, 
     case when sum(是否接收)>0     then round (sum(是否其它)/sum(是否接收),4) else 0 end 其它比率,
     case when 工单类型 ='抱怨工单'then  sum(是否接收 ) else 0 end as 抱怨反馈数量,    
     case when 工单类型 ='抱怨工单'then  round(sum(是否接收 )/(select count(*)from gcdcs.repairworkordermd v where v.partssalescategoryid=t.partssalescategoryid
        and v.receivetime is not null  and  sendtime between @Prompt('CreateTimeBegin', 'D',, Mono, Free, Persistent) and  @Prompt('CreateTimeEnd', 'D',, Mono, Free, Persistent)),4) else 0 end as 抱怨反馈比率,  
                   case when 工单类型 ='抱怨工单'then   round( sum(已接受到处理完毕时长)/ sum(是否接收 ) ,4) else 0 end as 抱怨反馈时长小时 
  from (select case when status > 2 then 1 else  0  end as 是否已响应,
               case  when status >= 2 then 1  else 0 end as 是否已派工,
               decode(ReceiveTime, null, 0, 1) 是否接收,
               decode(RejectTime, null, 0, 1) 是否拒绝,
               case   when ReceiveTime is not null then
                   case  when (ReceiveTime - SendTime) * 60 * 24 > 20 then 0  else 1  end
                      when RejectTime is not null then 
                        case when (RejectTime - SendTime) * 60 * 24 > 20 then 0 else 1 end
               end 是否20分钟内响应,
               case  when ReceiveTime is not null then
                  case when (ReceiveTime - SendTime) * 60 * 24 > 20 then 1 else 0 end
                     when RejectTime is not null then
                  case  when (RejectTime - SendTime) * 60 * 24 > 20 then 1 else 0  end
               end 是否超20分钟内响应,
               case when ReceiveTime is not null then
                  (ReceiveTime - SendTime) * 60 * 24
                    when RejectTime is not null then
                  (RejectTime - SendTime) * 60 * 24
               end 响应时长,
               case when ReceiveTime is not null then
                  case  when (ReceiveTime - SendTime) * 60 * 24 > 20 then 0 else 1 end
               end 接收是否20分钟内响应,
               case when ReceiveTime is not null then
                  case when (ReceiveTime - SendTime) * 60 * 24 > 20 then 1 else 0 end
               end 接收是否超20分钟内响应,
               case when ReceiveTime is not null then (ReceiveTime - SendTime) * 60 * 24
               end 接收响应时长,
               case  when RejectTime is not null then
                  case when (RejectTime - SendTime) * 60 * 24 > 20 then 0 else 1 end
               end 拒绝是否20分钟内响应,
               case when RejectTime is not null then
                  case when (RejectTime - SendTime) * 60 * 24 > 20 then 1  else  0 end
               end 拒绝是否超20分钟内响应,
               case when RejectTime is not null then (RejectTime - SendTime) * 60 * 24
               end 拒绝响应时长,
               decode(LeavingTime, null, 0, 1) 是否出发,
               case  when (ReceiveTime is not null) and (LeavingTime is not null) then (LeavingTime - ReceiveTime) * 60 * 24
               end 待出发时长,
               decode(ArriveTime, null, 0, 1) 是否到达,
               case when (ReceiveTime is not null) and (ArriveTime is not null) then (ArriveTime - ReceiveTime) * 60 * 24
               end 路程时长,
               decode(ArriveStationTime, null, 0, 1) 是否到站,
               case when (ReceiveTime is not null) and (ArriveStationTime is not null) then (ArriveStationTime - ReceiveTime) * 60 * 24
               end 到站时长,
               decode(StartRepairTime, null, 0, 1) 是否开始维修,
               case when (ArriveTime is not null) and (StartRepairTime is not null) then (StartRepairTime - ArriveTime) * 60 * 24
               end 救援准备时长,
               case  when (ArriveStationTime is not null) and (StartRepairTime is not null) then (StartRepairTime - ArriveStationTime) * 60 * 24
               end 报修准备时长,
               decode(FimishTime, null, 0, 1) 是否处理完毕,
               case  when (FimishTime is not null) and (StartRepairTime is not null) then(FimishTime - StartRepairTime)  * 24
               end 维修时长,
               case when (FimishTime is not null) and (ReceiveTime is not null) then (FimishTime - ReceiveTime)  * 24
               end 已接受到处理完毕时长,
               case when (FimishTime is not null) then (FimishTime - SendTime)  * 24
               end 已派工到处理完毕时长,
               IsUsed 是否已使用,
               case when (ReceiveTime is not null) then  decode(IsCanFinish, 1, 1, 0)
               end as 二十四小时完成,
               case  when (ReceiveTime is not null) then decode(IsCanFinish, 2, 1, 0)
               end as 二十四小时未完成,
               case when (ReceiveTime is not null) then  decode(IsCanFinish, 0, 1, 0)
               end as 二十四小时未反馈,
               case when (RejectTime is not null) then decode(RefuseReason, 1, 1, 0)
               end as 是否无配件,
               case when (RejectTime is not null) then decode(RefuseReason, 2, 1, 0)
               end as 是否无维修工,
               case when (RejectTime is not null) then  decode(RefuseReason, 4, 1, 0)
               end as 是否无维修权限,
               case when (RejectTime is not null) then  decode(RefuseReason, 3, 1, 0)
               end as 是否无救援车,
               case when (RejectTime is not null) then  decode(RefuseReason, 5, 1, 0)
               end as 是否距离客户较远,
               case when (RejectTime is not null) then  decode(RefuseReason, 6, 1, 0)
               end as 是否已撤站,
               case when (RejectTime is not null) then decode(RefuseReason, 7, 1, 0)
               end as 是否无维修能力,
               case when (RejectTime is not null) then decode(RefuseReason, 8, 1, 0)
               end as 是否非事业本部负责品牌,
               case  when (RejectTime is not null) then  decode(RefuseReason, 9, 1, 0)
               end as 是否非福田客户,
               case when (RejectTime is not null) then  decode(RefuseReason, 10, 1, 0)
               end as 是否客户取消预约,
               case when (RejectTime is not null) then  decode(RefuseReason, 11, 1, 0)
               end as 是否其它,
               RejectTime,
               ReceiveTime,
               SendTime,
               code,
               partssalescategoryid　,
               status,
               Type,
             repairworkordermd.branchid,
               decode(Type, 1, '抱怨工单', 2, '救援工单', 3, '报修工单') 工单类型
          from gcdcs.repairworkordermd where sendtime between @Prompt('CreateTimeBegin', 'D',, Mono, Free, Persistent) and  @Prompt('CreateTimeEnd', 'D',, Mono, Free, Persistent))t
 group by type, partssalescategoryid, 工单类型,branchid 
 union 
 select  type,
       partssalescategoryid,
        branchid,
       (select code from sddcs.branch where branch.id=branchid)branchcode,
       工单类型,
       (select name
          from sddcs.partssalescategory
         where partssalescategory.id = partssalescategoryid) 品牌,
      sum(是否已响应) 已响应数量,
      sum(是否已派工) PMS系统派工单接收数量,
      sum(是否接收 ) 接受数量,
            sum(响应时长) as 响应时长总,--
      sum(接收响应时长) as 接收响应时长总,--
      sum(拒绝响应时长) as 拒绝响应时长总,--
      sum(待出发时长) as 待出发时长总,--
      sum(路程时长) as 路程时长总, --
      sum(救援准备时长) as 救援准备时长总,--
      sum(报修准备时长) as 报修准备时长总,--
      sum(维修时长) as 维修时长总,--
      sum(已接受到处理完毕时长) as 已接受到处理完毕时长总,--
      sum(已派工到处理完毕时长) as 已派工到处理完毕时长总, --
      (select count(*)from dcs.repairworkordermd v where v.partssalescategoryid=t.partssalescategoryid
        and v.receivetime is not null  and  sendtime between @Prompt('CreateTimeBegin', 'D',, Mono, Free, Persistent) and  @Prompt('CreateTimeEnd', 'D',, Mono, Free, Persistent)) as 反馈数量总,--
     case when sum(是否已派工)>0 then round (sum(是否接收 )/ sum(是否已派工) ,4) else 0 end 接收比率,
      sum(是否拒绝) 拒绝数量,
     case when sum(是否已派工)>0 then round(sum(是否拒绝)/sum(是否已派工),4) else 0 end  拒绝比率,
      sum(是否20分钟内响应) 二十分钟内响应,
     case when sum(是否已响应)>0 then round(sum(是否20分钟内响应)/ sum(是否已响应),4) else 0 end  二十分钟内响应比率  ,
      sum(是否超20分钟内响应) 超20分钟响应,
     case when sum(是否已响应)>0 then round(sum(是否超20分钟内响应)/sum(是否已响应),4 )else 0 end 超二十分钟响应比率,
     case when sum(是否已响应)>0 then round( sum(响应时长)/sum(是否已响应),4) else 0 end  平均响应时长分,
      sum(接收是否20分钟内响应)  已接受二十分钟内响应,
     case when sum(是否接收)>0   then round(sum(接收是否20分钟内响应)/ sum(是否接收),4) else 0 end 已接受二十分钟内响应比率  ,
      sum(接收是否超20分钟内响应) 接受超20分钟响应,
     case when sum(是否接收)>0   then round(sum(接收是否超20分钟内响应)/sum(是否接收),4) else 0 end 已接受超二十分钟响应比率,
     case when   sum(是否接收)>0 then round( sum(接收响应时长)/sum(是否接收),4) else 0 end 已接受平均响应时长分,
      sum(拒绝是否20分钟内响应)  已拒绝二十分钟内响应,
     case when  sum(是否拒绝)>0  then round(sum(拒绝是否20分钟内响应)/ sum(是否拒绝),4)else 0 end   已拒绝二十分钟内响应比率  ,
      sum(拒绝是否超20分钟内响应) 拒绝超20分钟响应,
     case when sum(是否拒绝)>0   then round(sum(拒绝是否超20分钟内响应)/sum(是否拒绝),4) else 0 end  已拒绝超二十分钟响应比率,
     case when sum(是否拒绝)>0   then round( sum(拒绝响应时长)/sum(是否拒绝),4)  else 0 end  已拒绝平均响应时长分,
      sum(是否出发)出发数量 ,
     case when sum(是否出发)>0   then round(sum(待出发时长)/sum(是否出发),4)  else 0 end  待出发时长分,
      sum(是否到达)已到达数量 ,
     case when sum(是否到达)>0   then round(sum(路程时长)/ sum(是否到达),4) else 0 end 路程时长分,
      sum(是否到站)已到站 ,
     case when sum(是否到达)>0   then round(sum(路程时长)/ sum(是否到达),4) else 0 end 到站时长分,
      sum(是否开始维修) 开始维修数量,
     case when sum(是否到达)>0   then round(sum(救援准备时长)/sum(是否到达),4) else 0 end  救援准备时长分,
     case when sum(是否开始维修)>0 then round(sum(报修准备时长)/sum(是否开始维修),4) else 0 end  救援准备时长,
      sum(是否处理完毕) 处理完毕数量,
     case when sum(是否处理完毕)>0 then round( sum(维修时长)/sum(是否处理完毕),4) else 0 end  维修时长小时,  
     case when sum(是否处理完毕)>0 then round( sum(已接受到处理完毕时长)/sum(是否处理完毕),4) else 0 end   已接受到处理完毕时长小时,
     case when sum(是否处理完毕)>0 then round( sum(已派工到处理完毕时长)/sum(是否处理完毕),4) else 0 end   已派工到处理完毕时长小时 ,
      sum(是否已使用) 关联维修单数量,
     case when sum(是否处理完毕)>0 then round( sum(是否已使用)/sum(是否处理完毕),4) else 0 end  关联维修单数量比率,
      sum(二十四小时完成) 是的数量,
     case when sum(是否接收 )>0    then round(sum(二十四小时完成)/sum(是否接收 ) ,4)  else 0 end 是的比率,
      sum(二十四小时未完成) 否的数量,
     case when sum(是否接收 )>0    then round(sum(二十四小时未完成)/sum(是否接收 ) ,4) else 0 end 否的比率,
      sum(二十四小时未反馈) 未反馈的数量,
     case when sum(是否接收 )>0    then round(sum(二十四小时未反馈)/sum(是否接收 ) ,4) else 0 end 未反馈的比率,
        sum(是否无配件) 无配件数量, 
     case when sum(是否接收)>0     then round (sum(是否无配件)/sum(是否接收),4) else 0 end 无配件比率,
        sum(是否无维修工) 无维修工数量,
     case when sum(是否接收)>0     then round (sum(是否无维修工)/sum(是否接收),4) else 0 end 无维修工比率,
        sum(是否无救援车) 无救援车数量,
     case when sum(是否接收)>0     then round (sum(是否无救援车)/sum(是否接收),4) else 0 end 无救援车比率,
        sum(是否无维修权限) 无维修权限数量,
     case when sum(是否接收)>0     then round (sum(是否无维修权限)/sum(是否接收),4) else 0 end 无维修权限比率,
        sum(是否距离客户较远) 距离客户较远数量,
     case when sum(是否接收)>0     then round (sum(是否距离客户较远)/sum(是否接收),4) else 0 end 距离客户较远比率,
        sum(是否已撤站) 已撤站数量,
     case when sum(是否接收)>0     then round (sum(是否已撤站)/sum(是否接收),4) else 0 end 已撤站比率,
        sum(是否无维修能力) 无维修能力数量,
     case when sum(是否接收)>0     then round (sum(是否无维修能力)/sum(是否接收),4) else 0 end 无维修能力比率,
        sum(是否非事业本部负责品牌) 非事业本部负责品牌数量,
     case when sum(是否接收)>0     then round (sum(是否非事业本部负责品牌)/sum(是否接收),4) else 0 end 非事业本部负责品牌比率,
        sum(是否非福田客户) 非福田客户数量, 
     case when sum(是否接收)>0     then round (sum(是否非福田客户)/sum(是否接收),4) else 0 end 非福田客户比率,
        sum(是否客户取消预约) 客户取消预约数量,
     case when sum(是否接收)>0     then round (sum(是否客户取消预约)/sum(是否接收),4) else 0 end 客户取消预约比率,
          sum(是否其它)其它数量, 
     case when sum(是否接收)>0     then round (sum(是否其它)/sum(是否接收),4) else 0 end 其它比率,
     case when 工单类型 ='抱怨工单'then  sum(是否接收 ) else 0 end as 抱怨反馈数量,    
     case when 工单类型 ='抱怨工单'then  round(sum(是否接收 )/(select count(*)from sddcs.repairworkordermd v where v.partssalescategoryid=t.partssalescategoryid
        and v.receivetime is not null  and  sendtime between @Prompt('CreateTimeBegin', 'D',, Mono, Free, Persistent) and  @Prompt('CreateTimeEnd', 'D',, Mono, Free, Persistent)),4) else 0 end as 抱怨反馈比率,  
                   case when 工单类型 ='抱怨工单'then   round( sum(已接受到处理完毕时长)/ sum(是否接收 ) ,4) else 0 end as 抱怨反馈时长小时 
  from (select case when status > 2 then 1 else  0  end as 是否已响应,
               case  when status >= 2 then 1  else 0 end as 是否已派工,
               decode(ReceiveTime, null, 0, 1) 是否接收,
               decode(RejectTime, null, 0, 1) 是否拒绝,
               case   when ReceiveTime is not null then
                   case  when (ReceiveTime - SendTime) * 60 * 24 > 20 then 0  else 1  end
                      when RejectTime is not null then 
                        case when (RejectTime - SendTime) * 60 * 24 > 20 then 0 else 1 end
               end 是否20分钟内响应,
               case  when ReceiveTime is not null then
                  case when (ReceiveTime - SendTime) * 60 * 24 > 20 then 1 else 0 end
                     when RejectTime is not null then
                  case  when (RejectTime - SendTime) * 60 * 24 > 20 then 1 else 0  end
               end 是否超20分钟内响应,
               case when ReceiveTime is not null then
                  (ReceiveTime - SendTime) * 60 * 24
                    when RejectTime is not null then
                  (RejectTime - SendTime) * 60 * 24
               end 响应时长,
               case when ReceiveTime is not null then
                  case  when (ReceiveTime - SendTime) * 60 * 24 > 20 then 0 else 1 end
               end 接收是否20分钟内响应,
               case when ReceiveTime is not null then
                  case when (ReceiveTime - SendTime) * 60 * 24 > 20 then 1 else 0 end
               end 接收是否超20分钟内响应,
               case when ReceiveTime is not null then (ReceiveTime - SendTime) * 60 * 24
               end 接收响应时长,
               case  when RejectTime is not null then
                  case when (RejectTime - SendTime) * 60 * 24 > 20 then 0 else 1 end
               end 拒绝是否20分钟内响应,
               case when RejectTime is not null then
                  case when (RejectTime - SendTime) * 60 * 24 > 20 then 1  else  0 end
               end 拒绝是否超20分钟内响应,
               case when RejectTime is not null then (RejectTime - SendTime) * 60 * 24
               end 拒绝响应时长,
               decode(LeavingTime, null, 0, 1) 是否出发,
               case  when (ReceiveTime is not null) and (LeavingTime is not null) then (LeavingTime - ReceiveTime) * 60 * 24
               end 待出发时长,
               decode(ArriveTime, null, 0, 1) 是否到达,
               case when (ReceiveTime is not null) and (ArriveTime is not null) then (ArriveTime - ReceiveTime) * 60 * 24
               end 路程时长,
               decode(ArriveStationTime, null, 0, 1) 是否到站,
               case when (ReceiveTime is not null) and (ArriveStationTime is not null) then (ArriveStationTime - ReceiveTime) * 60 * 24
               end 到站时长,
               decode(StartRepairTime, null, 0, 1) 是否开始维修,
               case when (ArriveTime is not null) and (StartRepairTime is not null) then (StartRepairTime - ArriveTime) * 60 * 24
               end 救援准备时长,
               case  when (ArriveStationTime is not null) and (StartRepairTime is not null) then (StartRepairTime - ArriveStationTime) * 60 * 24
               end 报修准备时长,
               decode(FimishTime, null, 0, 1) 是否处理完毕,
               case  when (FimishTime is not null) and (StartRepairTime is not null) then(FimishTime - StartRepairTime)  * 24
               end 维修时长,
               case when (FimishTime is not null) and (ReceiveTime is not null) then (FimishTime - ReceiveTime)  * 24
               end 已接受到处理完毕时长,
               case when (FimishTime is not null) then (FimishTime - SendTime)  * 24
               end 已派工到处理完毕时长,
               IsUsed 是否已使用,
               case when (ReceiveTime is not null) then  decode(IsCanFinish, 1, 1, 0)
               end as 二十四小时完成,
               case  when (ReceiveTime is not null) then decode(IsCanFinish, 2, 1, 0)
               end as 二十四小时未完成,
               case when (ReceiveTime is not null) then  decode(IsCanFinish, 0, 1, 0)
               end as 二十四小时未反馈,
               case when (RejectTime is not null) then decode(RefuseReason, 1, 1, 0)
               end as 是否无配件,
               case when (RejectTime is not null) then decode(RefuseReason, 2, 1, 0)
               end as 是否无维修工,
               case when (RejectTime is not null) then  decode(RefuseReason, 4, 1, 0)
               end as 是否无维修权限,
               case when (RejectTime is not null) then  decode(RefuseReason, 3, 1, 0)
               end as 是否无救援车,
               case when (RejectTime is not null) then  decode(RefuseReason, 5, 1, 0)
               end as 是否距离客户较远,
               case when (RejectTime is not null) then  decode(RefuseReason, 6, 1, 0)
               end as 是否已撤站,
               case when (RejectTime is not null) then decode(RefuseReason, 7, 1, 0)
               end as 是否无维修能力,
               case when (RejectTime is not null) then decode(RefuseReason, 8, 1, 0)
               end as 是否非事业本部负责品牌,
               case  when (RejectTime is not null) then  decode(RefuseReason, 9, 1, 0)
               end as 是否非福田客户,
               case when (RejectTime is not null) then  decode(RefuseReason, 10, 1, 0)
               end as 是否客户取消预约,
               case when (RejectTime is not null) then  decode(RefuseReason, 11, 1, 0)
               end as 是否其它,
               RejectTime,
               ReceiveTime,
               SendTime,
               code,
               partssalescategoryid　,
               status,
               Type,
               repairworkordermd.branchid,
               decode(Type, 1, '抱怨工单', 2, '救援工单', 3, '报修工单') 工单类型
          from sddcs.repairworkordermd where sendtime between @Prompt('CreateTimeBegin', 'D',, Mono, Free, Persistent) and  @Prompt('CreateTimeEnd', 'D',, Mono, Free, Persistent))t
 group by type, partssalescategoryid, 工单类型,branchid 
 
 