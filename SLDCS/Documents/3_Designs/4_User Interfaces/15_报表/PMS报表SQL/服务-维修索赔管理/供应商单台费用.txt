SELECT a.branchid,
       a.serviceproductlineid,
       c.UsedPartsCode,
       c.UsedPartsName,
       c.UsedPartsSupplierCode,
       c.UsedPartsSupplierName,
       line.code as linecode,
       (c.materialcost + c.partsmanagementcost) as Fee,
       a.createtime,
       (select count(*)
          from dcs.vehicleinformation a
         inner join dcs.product b on a.productid = b.id
         inner join dcs.serviceprodlineproduct c on b.brandid = c.brandid
         where SalesDate >= add_months(sysdate, -12)
           and c.serviceproductlinecode = 'JBC') as JBCRetained,
       (select count(*)
          from dcs.vehicleinformation a
         inner join dcs.product b on a.productid = b.id
         inner join dcs.serviceprodlineproduct c on b.brandid = c.brandid
         where SalesDate >= add_months(sysdate, -12)
           and c.serviceproductlinecode = 'BC') as BCRetained
  FROM dcs.repairclaimbill a
 inner join dcs.serviceproductline line on line.id = a.serviceproductlineid
 inner join dcs.RepairClaimItemDetail b on a.id = b.RepairClaimBillId
 inner join dcs.RepairClaimMaterialDetail c on c.RepairClaimItemDetailId = b.id
 where a.status not in (1, 99)
 union 
 SELECT a.branchid,
       a.serviceproductlineid,
       c.UsedPartsCode,
       c.UsedPartsName,
       c.UsedPartsSupplierCode,
       c.UsedPartsSupplierName,
       line.code as linecode,
       (c.materialcost + c.partsmanagementcost) as Fee,
       a.createtime,
       (select count(*)
          from yxdcs.vehicleinformation a
         inner join yxdcs.product b on a.productid = b.id
         inner join yxdcs.serviceprodlineproduct c on b.brandid = c.brandid
         where SalesDate >= add_months(sysdate, -12)
           and c.serviceproductlinecode = 'JBC') as JBCRetained,
       (select count(*)
          from yxdcs.vehicleinformation a
         inner join yxdcs.product b on a.productid = b.id
         inner join yxdcs.serviceprodlineproduct c on b.brandid = c.brandid
         where SalesDate >= add_months(sysdate, -12)
           and c.serviceproductlinecode = 'BC') as BCRetained
  FROM yxdcs.repairclaimbill a
 inner join yxdcs.serviceproductline line on line.id = a.serviceproductlineid
 inner join yxdcs.RepairClaimItemDetail b on a.id = b.RepairClaimBillId
 inner join yxdcs.RepairClaimMaterialDetail c on c.RepairClaimItemDetailId = b.id
 where a.status not in (1, 99)
 union all
 SELECT a.branchid,
       a.serviceproductlineid,
       c.UsedPartsCode,
       c.UsedPartsName,
       c.UsedPartsSupplierCode,
       c.UsedPartsSupplierName,
       line.code as linecode,
       (c.materialcost + c.partsmanagementcost) as Fee,
       a.createtime,
       (select count(*)
          from gcdcs.vehicleinformation a
         inner join gcdcs.product b on a.productid = b.id
         inner join gcdcs.serviceprodlineproduct c on b.brandid = c.brandid
         where SalesDate >= add_months(sysdate, -12)
           and c.serviceproductlinecode = 'JBC') as JBCRetained,
       (select count(*)
          from gcdcs.vehicleinformation a
         inner join gcdcs.product b on a.productid = b.id
         inner join gcdcs.serviceprodlineproduct c on b.brandid = c.brandid
         where SalesDate >= add_months(sysdate, -12)
           and c.serviceproductlinecode = 'BC') as BCRetained
  FROM gcdcs.repairclaimbill a
 inner join gcdcs.serviceproductline line on line.id = a.serviceproductlineid
 inner join gcdcs.RepairClaimItemDetail b on a.id = b.RepairClaimBillId
 inner join gcdcs.RepairClaimMaterialDetail c on c.RepairClaimItemDetailId = b.id
 where a.status not in (1, 99)
 union all 
 SELECT a.branchid,
       a.serviceproductlineid,
       c.UsedPartsCode,
       c.UsedPartsName,
       c.UsedPartsSupplierCode,
       c.UsedPartsSupplierName,
       line.code as linecode,
       (c.materialcost + c.partsmanagementcost) as Fee,
       a.createtime,
       (select count(*)
          from sddcs.vehicleinformation a
         inner join sddcs.product b on a.productid = b.id
         inner join sddcs.serviceprodlineproduct c on b.brandid = c.brandid
         where SalesDate >= add_months(sysdate, -12)
           and c.serviceproductlinecode = 'JBC') as JBCRetained,
       (select count(*)
          from sddcs.vehicleinformation a
         inner join sddcs.product b on a.productid = b.id
         inner join sddcs.serviceprodlineproduct c on b.brandid = c.brandid
         where SalesDate >= add_months(sysdate, -12)
           and c.serviceproductlinecode = 'BC') as BCRetained
  FROM sddcs.repairclaimbill a
 inner join sddcs.serviceproductline line on line.id = a.serviceproductlineid
 inner join sddcs.RepairClaimItemDetail b on a.id = b.RepairClaimBillId
 inner join sddcs.RepairClaimMaterialDetail c on c.RepairClaimItemDetailId = b.id
 where a.status not in (1, 99)