
select e.DealerCode                   as 服务站编号,
       e.dealername                   as 服务站名称,
       e.partssalescategoryid,
       e.PartsSalesCategoryName       as 品牌,
       d.Marketingdepartmentid        as 市场部,
       e.serviceproductlineid,
       e.Serviceproductline           as 服务产品线,
       e.ExtendedWarrantyOrderCode    as 延保单号,
       e.customername                 as 客户姓名,
       e.cellnumber                   as 手机号码,
       e.vin                          as VIN码,
       e.Carsalesdate                 as 车辆销售日期,
       e.EngineModel                  as 发动机型号,
       e.VehicleLicensePlate          as 车牌号,
       ep.Extendedwarrantyproductname as 延保产品,
       el.Customerretailprice         as 客户零售价,
       el.Salesprice                  as 批发价,
       atype.Extendedwarrantyagiotype as 折扣类型,
       el.Discountedprice             as 折扣价,
       el.Totalamount                 as 延保总价,
       el.Salesprice                  as 服务商回款金额,
       e.creatortime                  as 创建时间
  from yxdcs.ExtendedWarrantyOrder e
 inner join yxdcs.DealerServiceInfo d
    on e.dealerid = d.dealerid
   and e.partssalescategoryid = d.PartsSalesCategoryId
   and d.status = 1
 inner join yxdcs.ExtendedWarrantyOrderList el
    on el.extendedwarrantyorderid = e.id
 inner join yxdcs.ExtendedWarrantyProduct ep
    on ep.id = el.Extendedwarrantyproductid
    and ep.status=1
 inner join yxdcs.DeferredDiscountType atype
    on el.agiotype = atype.id
    and atype.status=1
 where e.creatortime >= @Prompt('TimeBegin', 'D',, Mono, Free, Persistent)
   and e.creatortime <= @Prompt('TimeEnd', 'D',, Mono, Free, Persistent)
union all

select e.DealerCode                   as 服务站编号,
       e.dealername                   as 服务站名称,
       e.partssalescategoryid,
       e.PartsSalesCategoryName       as 品牌,
       d.Marketingdepartmentid        as 市场部,
       e.serviceproductlineid,
       e.Serviceproductline           as 服务产品线,
       e.ExtendedWarrantyOrderCode    as 延保单号,
       e.customername                 as 客户姓名,
       e.cellnumber                   as 手机号码,
       e.vin                          as VIN码,
       e.Carsalesdate                 as 车辆销售日期,
       e.EngineModel                  as 发动机型号,
       e.VehicleLicensePlate          as 车牌号,
       ep.Extendedwarrantyproductname as 延保产品,
       el.Customerretailprice         as 客户零售价,
       el.Salesprice                  as 批发价,
       atype.Extendedwarrantyagiotype as 折扣类型,
       el.Discountedprice             as 折扣价,
       el.Totalamount                 as 延保总价,
       el.Salesprice                  as 服务商回款金额,
       e.creatortime                  as 创建时间
  from dcs.ExtendedWarrantyOrder e
 inner join dcs.DealerServiceInfo d
    on e.dealerid = d.dealerid
   and e.partssalescategoryid = d.PartsSalesCategoryId
   and d.status = 1
 inner join dcs.ExtendedWarrantyOrderList el
    on el.extendedwarrantyorderid = e.id
 inner join dcs.ExtendedWarrantyProduct ep
    on ep.id = el.Extendedwarrantyproductid
    and ep.status=1
 inner join dcs.DeferredDiscountType atype
    on el.agiotype = atype.id
      and atype.status=1
 where e.creatortime >= @Prompt('TimeBegin', 'D',, Mono, Free, Persistent)
   and e.creatortime <= @Prompt('TimeEnd', 'D',, Mono, Free, Persistent)
union all

select e.DealerCode                   as 服务站编号,
       e.dealername                   as 服务站名称,
       e.partssalescategoryid,
       e.PartsSalesCategoryName       as 品牌,
       d.Marketingdepartmentid        as 市场部,
       e.serviceproductlineid,
       e.Serviceproductline           as 服务产品线,
       e.ExtendedWarrantyOrderCode    as 延保单号,
       e.customername                 as 客户姓名,
       e.cellnumber                   as 手机号码,
       e.vin                          as VIN码,
       e.Carsalesdate                 as 车辆销售日期,
       e.EngineModel                  as 发动机型号,
       e.VehicleLicensePlate          as 车牌号,
       ep.Extendedwarrantyproductname as 延保产品,
       el.Customerretailprice         as 客户零售价,
       el.Salesprice                  as 批发价,
       atype.Extendedwarrantyagiotype as 折扣类型,
       el.Discountedprice             as 折扣价,
       el.Totalamount                 as 延保总价,
       el.Salesprice                  as 服务商回款金额,
       e.creatortime                  as 创建时间
  from gcdcs.ExtendedWarrantyOrder e
 inner join gcdcs.DealerServiceInfo d
    on e.dealerid = d.dealerid
   and e.partssalescategoryid = d.PartsSalesCategoryId
   and d.status = 1
 inner join gcdcs.ExtendedWarrantyOrderList el
    on el.extendedwarrantyorderid = e.id
 inner join gcdcs.ExtendedWarrantyProduct ep
    on ep.id = el.Extendedwarrantyproductid
    and ep.status=1
 inner join gcdcs.DeferredDiscountType atype
    on el.agiotype = atype.id
      and atype.status=1
 where e.creatortime >= @Prompt('TimeBegin', 'D',, Mono, Free, Persistent)
   and e.creatortime <= @Prompt('TimeEnd', 'D',, Mono, Free, Persistent)
union all

select e.DealerCode                   as 服务站编号,
       e.dealername                   as 服务站名称,
       e.partssalescategoryid,
       e.PartsSalesCategoryName       as 品牌,
       d.Marketingdepartmentid        as 市场部,
       e.serviceproductlineid,
       e.Serviceproductline           as 服务产品线,
       e.ExtendedWarrantyOrderCode    as 延保单号,
       e.customername                 as 客户姓名,
       e.cellnumber                   as 手机号码,
       e.vin                          as VIN码,
       e.Carsalesdate                 as 车辆销售日期,
       e.EngineModel                  as 发动机型号,
       e.VehicleLicensePlate          as 车牌号,
       ep.Extendedwarrantyproductname as 延保产品,
       el.Customerretailprice         as 客户零售价,
       el.Salesprice                  as 批发价,
       atype.Extendedwarrantyagiotype as 折扣类型,
       el.Discountedprice             as 折扣价,
       el.Totalamount                 as 延保总价,
       el.Salesprice                  as 服务商回款金额,
       e.creatortime                  as 创建时间
  from sddcs.ExtendedWarrantyOrder e
 inner join sddcs.DealerServiceInfo d
    on e.dealerid = d.dealerid
   and e.partssalescategoryid = d.PartsSalesCategoryId 
   and d.status = 1
 inner join sddcs.ExtendedWarrantyOrderList el
    on el.extendedwarrantyorderid = e.id
 inner join sddcs.ExtendedWarrantyProduct ep
    on ep.id = el.Extendedwarrantyproductid
    and ep.status=1
 inner join sddcs.DeferredDiscountType atype
    on el.agiotype = atype.id
      and atype.status=1
 where e.creatortime >= @Prompt('TimeBegin', 'D',, Mono, Free, Persistent)
   and e.creatortime <= @Prompt('TimeEnd', 'D',, Mono, Free, Persistent)
