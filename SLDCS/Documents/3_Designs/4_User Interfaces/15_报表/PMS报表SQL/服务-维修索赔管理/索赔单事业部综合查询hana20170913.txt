
 /********* Begin Procedure Script ************/ 
 BEGIN 
   var_out = select b.code                 ,--as 分公司编号,
       b.name                 ,--as 分公司名称,
       rcb.ClaimBillCode      ,--as 索赔单编号,
       psc.id as PartsSalesCategoryId ,
     case  rcb.status 
        when 1  then '新增' 
        when 2  then '提交' 
        when 3  then '初审通过' 
          when 4  then '终审通过' 
          when 6  then '审核不通过' 
          when 99  then '作废' 
       end as bill_status,--单据状态,
       case rcb.RepairType 
          when 1  then '普通维修' 
          when 2  then   '外出服务' 
          when 3  then '强制保养' 
          when 4  then '普通保养' 
          when 5  then'商品车维修' 
          when 6  then '事故维修' 
          when 7  then '服务活动' 
          when 8  then '商品车保养' 
          when 15 then '让度服务'
       end as RepairType,--维修类型,
       rcb.serviceactivitycode ,--as 服务活动编号,
       rcb.serviceactivitycontent ,--as 服务活动内容,
       case rcb.serviceactivitytype 
          when 1 then '车辆召回' 
          when 2 then '特殊服务活动' 
          when 3 then '一般服务活动' 
          when  4 then '整改' 
       end as serviceactivitytype,--服务活动类型,
       (select sa.name from pms.serviceactivity sa where sa.id=rcb.serviceactivityid and sa.source_code=rcb.source_code) as serviceactivity_name,--服务活动名称,   
       rcb.vin,
       v.serialnumber      ,--as 出厂编号,
       psc.name as brand_name ,--as 品牌,
       v.productlinename   ,--as 整车产品线,
       spl.name    as serv_prdl_name ,--as 服务产品线,     
       spl."产品线类型"       as product_line_type,-- 产品线类型,
       case substr(spl.name,0,2) 
          when '金刚' then 1 
          when '瑞沃' then 2 
       end as ProductLineType,
       v.OutOfFactoryDate     ,--as 出厂日期,
       v.SalesDate            ,--as 购买日期,
       RCB.Mileage            ,--as 行驶里程,
       v.FirstStoppageMileage ,--as 首次故障里程, 
       v.vehiclecategoryname  ,--as 产品类别,
       /*case v.IsRoadVehicl when 1 then '公路车' when 0 then '非公路车'end*/ 
       v.ProductCategoryName ,--as 车型,
       v.annoucementnumber ,--as 公告号,
       v.EngineModel ,--as 发动机型号,
       v.EngineSerialNumber ,--as 发动机编号,
       v.Device_Type ,--as 终端型号
       md.name as market_name,--as 市场部,
       rcb.DealerCode ,--as 服务站编号,
       rcb.DealerName ,--as 服务站名称,
       rcb.VehicleContactPerson ,--as 车辆联系人,
       rcb.ContactPhone  ,--as 联系电话,
       v.ProductCode,
       dsi.Fix ,--as 服务站联系电话,
       rcb.RepairRequestTime ,--as 报修时间,
       rcb.createtime ,--as 创建日期,
       rcb.MalfunctionCode ,--as 故障代码,
       rcb.MalfunctionReason ,--as 故障现象描述,
       rcb.MalfunctionDescription ,--as 故障模式,
       rcb.FaultyPartsCode ,--as 祸首件图号,
       rcb.FaultyPartsName ,--as 祸首件名称,
       rcb.FaultyPartsSupplierName ,--as 祸首件生产厂家,
       rcb.ClaimSupplierCode ,--as 责任单位编号,
       rcb.ClaimSupplierName ,--as 责任单位名称,
       rcb.faultypartsassemblycode ,--as 祸首件所属总成编号,
       rcb.faultypartsassemblyname ,--as 祸首件所属总成名称,
       case   rcb.SupplierConfirmStatus 
          when  1  then '未确认' 
          when 2  then '已确认' 
          when 3  then '确认未通过' 
       end as SupplierConfirmStatus,--as 供应商确认状态,
       rcb.ApproveTime as part_ApproveTime,--as 分公司审核时间,
       rcb.ServiceDepartmentComment ,--as 终审意见,
       rcb.ApproveTime as final_ApproveTime,--as 终审时间,
       rcb.SupplierConfirmComment ,--as 供应商确认意见,
       case  rcb.IfClaimToSupplier 
          when 1 then '是' 
          when 0 then '否'
       end as IfClaimToSupplier,--as 是否向责任供应商索赔,
       rcb.RepairClaimApplicationCode ,--as 维修索赔申请单号,
       rcb.ServiceTripClaimAppCode ,--as 外出索赔申请单号,
       case rca.ApplicationType  
          when 1  then '重大索赔' 
          when 2  then '商品车维修' 
          when 3 then  '更换总成及典型故障索赔' 
          when 4  then '管控件维修'
          when 5 then  '外采或直拨维修' 
          when 6  then '强制保养' 
          when 7  then '商品车保养' 
          when 9 then  '批量整改' 
          when 10  then '让度服务' 
          when 11 then '油品补加' 
       end as ApplicationType,--as 维修索赔申请类型,
       ifnull(stcb.ServiceTripPerson,stcb.DealerTripPerson) as TripPerson,--as 外出人数,
       ifnull(stcb.ServiceTripDuration,stcb.DealerTripDuration) as TripDuration,--as 外出天数,
       ifnull(stcb.SettleDistance,stcb.ServiceTripDistance) as TripDistance,--as 外出里程  ,
       stcb.outservicecarunitprice ,--as 车辆补助单价,
       stcb.outsubsidyprice ,--as 人员补助单价,
       case stcb.ServiceTripType 
          when 1 then'白天' 
          when 2  then '夜间' 
       end as ServiceTripType,--as 外出类型  ,
       stcb.ServiceTripReason ,--as 外出原因  ,
       stcb.ApproveCommentHistory ,--as 外出审核意见,
       rcb.remark ,--故障备注,
       case  rcb.SettlementStatus  
          when 1  then '不结算' 
          when 2  then '待结算' 
          when 3 then '已结算'
       end as SettlementStatus,--as 结算状态,
       scsb.SettlementEndTime ,--as 结算时间,
       - ifnull(eabkk.transactionamount,0) as l_transactionamount,--as 扣款金额,
       null as l_transaction_reason,--as 扣款原因,
       null as transaction_type,--as 扣补款类型,
       ifnull(eabbk.transactionamount,0) as u_transactionamount,--as 补款金额,
       null as u_transaction_reason,--as 补款原因,
       ifnull(rcb.MaterialCost,0) as MaterialCost_all,--as 材料费,
       ifnull(rcb.MaterialCost,0)-ifnull(total.clkk,0) as MaterialCost,--as ⑴材料费,
       ifnull(case rcb.repairtype 
          when 3 then 0 
          when 4 then 0 
          when 8 then 0 
          else (ifnull(rcb.MaterialCost,0)-ifnull(total.clkk,0))*ifnull(pmg.rate,0) 
       end,0)  as repair_parts_manage_cost,--配件管理费,---强保、普通保养、商品车维修向供应商索赔未零
       rcb.LaborCost as LaborCost_all,--AS 工时费,
       ifnull(rcb.LaborCost,0)-ifnull(total.gskk,0) as LaborCost,--as ⑴工时费,
      ifnull(  case  
          when (rcb.repairtype =3 or rcb.repairtype= 4 or rcb.repairtype = 8) then (ifnull(rcb.LaborCost,0)-ifnull(total.gskk,0)) 
       else
         case bsr.ifspbylabor when 0 then round(((ifnull(rcb.LaborCost,0) - ifnull(total.gskk,0))/map(wxxmqd.xjxs,0,1,wxxmqd.xjxs)),2) * ifnull(bsr.laborcoefficient,0) 
       else 
          round(((ifnull(rcb.LaborCost,0) - ifnull(total.gskk,0))/(wxxmqd.LaborUnitPrice*map(wxxmqd.xjxs,0,1,wxxmqd.xjxs))),2) * bsr.laborunitprice 
       end end,0)  as repair_money,--as ⑵工时费,  ----强保、普通保养、商品车维修
       ifnull(stcb.TotalAmount,0) as TotalAmount,--as 外出服务费,
       case stcb.status 
          when 6 then (ifnull(stcb.TotalAmount,0) - ifnull(total.wckk,0)) 
          else 0 
       end  as out_service_money_1,--as ⑴外出服务费,--生效的外出服务索赔单
     ifnull(case  when (stcb.status=1 or stcb.status=2 or stcb.status=4 or stcb.status=6)
           then case ifnull(bsr.ifhaveoutfee,0) 
               when 0 then ifnull(stcb.TotalAmount,0) - ifnull(total.wckk,0) 
               else
            ((ifnull(stcb.SettleDistance*bsr.outservicecarunitprice,0) 
              + ifnull(stcb.ServiceTripPerson*stcb.Servicetripduration*bsr.outsubsidyprice,0)) 
              - ifnull(total.wckk,0)+ifnull(stcb.TowCharge,0)
              +ifnull(stcb.othercost,0)) 
          end 
       else 0 
    end,0)  as out_service_money_2,--as ⑵外出服务费,
        ifnull(case rcb.repairtype 
        when 3 then 0 
        when 4 then 0 
        when 8 then 0 
        else (ifnull(rcb.MaterialCost,0)-ifnull(total.clkk,0)) * ifnull(bsr.oldparttranscoefficient,0) 
    end,0) as repair_back_money,--as 故障件清退运费,

/*修改*/     ifnull(rcb.OtherCost,0) as OtherCost_all,
/*修改*/      ifnull(rcb.OtherCost,0) - ifnull(total.Otherkk,0)  as  OtherCost_all_pure,
       -- ifnull(rcb.OtherCost,0)+ifnull(stcb.othercost,0) as OtherCost_all,--as 其他费用,
      --  ifnull(rcb.OtherCost,0)+ifnull(stcb.othercost,0) - ifnull(total.Otherkk,0) as OtherCost_all_pure ,--as ⑴其他费用,
        /*(ifnull(rcb.MaterialCost,0)-ifnull(total.clkk,0))
          +(case when( rcb.repairtype = 3 or rcb.repairtype = 4 or rcb.repairtype=8) 
              then (ifnull(rcb.LaborCost,0)-ifnull(total.gskk,0)) 
               else
              case bsr.ifspbylabor 
                when 0 then round(((ifnull(rcb.LaborCost,0) - ifnull(total.gskk,0))/map(wxxmqd.xjxs,0,1,wxxmqd.xjxs)),2) * ifnull(bsr.laborcoefficient,0) 
              else round(((ifnull(rcb.LaborCost,0) - ifnull(total.gskk,0))/(wxxmqd.LaborUnitPrice*map(wxxmqd.xjxs,0,1,wxxmqd.xjxs))),2) * bsr.laborunitprice 
            end 
          end)
        +(case when (stcb.status=1 or stcb.status=2 or stcb.status=4 or stcb.status=6)
              then 
                case ifnull(bsr.ifhaveoutfee,0) 
                  when 0 then ifnull(stcb.TotalAmount,0) - ifnull(total.wckk,0) 
                else
              ((ifnull(stcb.SettleDistance*bsr.outservicecarunitprice,0) 
                + ifnull(stcb.ServiceTripPerson*stcb.Servicetripduration*bsr.outsubsidyprice,0)) 
                - ifnull(total.wckk,0)) 
            end 
          else 0 
        end)
      +(case rcb.repairtype 
          when 3 then 0 
          when 4 then 0 
          when 8 then 0 
          else (ifnull(rcb.MaterialCost,0)-ifnull(total.clkk,0))*ifnull(pmg.rate,0) 
        end )
      +(case rcb.repairtype 
          when 3 then 0 
          when 4 then 0 
          when 8 then 0 
          else (ifnull(rcb.MaterialCost,0)-ifnull(total.clkk,0)) 
              * ifnull(bsr.oldparttranscoefficient,0) 
        end)
      +( ifnull(rcb.OtherCost,0)+ifnull(stcb.othercost,0) 
          - ifnull(total.Otherkk,0)) as tatol_cost,--"费用合计",---⑴材料费+⑵工时费+⑵外出服务费+ 配件管理费+故障件清退运费+⑴其他费用
*/
 /*(ifnull(rcb.MaterialCost,
  0)-ifnull(total.clkk,
  0))+(case when( rcb.repairtype = 3 
    or rcb.repairtype = 4 
    or rcb.repairtype=8) 
  then (ifnull(rcb.LaborCost,
  0)-ifnull(total.gskk,
  0)) 
  else case bsr.ifspbylabor when 0 
  then round(((ifnull(rcb.LaborCost,
  0) - ifnull(total.gskk,
  0))/map(wxxmqd.xjxs,
  0,
  1,
  wxxmqd.xjxs)),
  2) * ifnull(bsr.laborcoefficient,
  0) 
  else round(((ifnull(rcb.LaborCost,
  0) - ifnull(total.gskk,
  0))/(wxxmqd.LaborUnitPrice*map(wxxmqd.xjxs,
  0,
  1,
  wxxmqd.xjxs))),
  2) * bsr.laborunitprice 
  end 
  end)+( case when (stcb.status=1 
    or stcb.status=2 
    or stcb.status=4 
    or stcb.status=6) 
  then case ifnull(bsr.ifhaveoutfee,
  0) when 0 
  then ifnull(stcb.TotalAmount,
  0) - ifnull(total.wckk,
  0) 
  else ((ifnull(stcb.SettleDistance*bsr.outservicecarunitprice,
  0) + ifnull(stcb.ServiceTripPerson*stcb.Servicetripduration*bsr.outsubsidyprice,
  0)) - ifnull(total.wckk,
  0)+ifnull(stcb.TowCharge,
  0)+ifnull(stcb.othercost,
  0)) 
  end 
  else 0 
  end)+(case rcb.repairtype when 3 
  then 0 when 4 
  then 0 when 8 
  then 0 
  else (ifnull(rcb.MaterialCost,
  0)-ifnull(total.clkk,
  0))*ifnull(pmg.rate,
  0) 
  end ) +(case rcb.repairtype when 3 
  then 0 when 4 
  then 0 when 8 
  then 0 
  else (ifnull(rcb.MaterialCost,
  0)-ifnull(total.clkk,
  0)) * ifnull(bsr.oldparttranscoefficient,
  0) 
  end)+( ifnull(rcb.OtherCost,0)
  --+ifnull(stcb.othercost,0) 
  - ifnull(total.Otherkk, 0)) as  tatol_cost,*/---update by 2016/8/27
(ifnull(rcb.MaterialCost,0)-ifnull(total.clkk,0))+ifnull((case when( rcb.repairtype = 3 or rcb.repairtype = 4 or rcb.repairtype=8) then (ifnull(rcb.LaborCost,0)-ifnull(total.gskk,0)) else
       case bsr.ifspbylabor when 0 then round(((ifnull(rcb.LaborCost,0) - ifnull(total.gskk,0))/map(wxxmqd.xjxs,0,1,wxxmqd.xjxs)),2) * ifnull(bsr.laborcoefficient,0) 
     else round(((ifnull(rcb.LaborCost,0) - ifnull(total.gskk,0))/(wxxmqd.LaborUnitPrice*map(wxxmqd.xjxs,0,1,wxxmqd.xjxs))),2) * bsr.laborunitprice end end),0)+ifnull((  case  when (stcb.status=1 or stcb.status=2 or stcb.status=4 or stcb.status=6)
        then case ifnull(bsr.ifhaveoutfee,0) when 0 then ifnull(stcb.TotalAmount,0) - ifnull(total.wckk,0) else
  ((ifnull(stcb.SettleDistance*bsr.outservicecarunitprice,0) + ifnull(stcb.ServiceTripPerson*stcb.Servicetripduration*bsr.outsubsidyprice,0)) - ifnull(total.wckk,0)+ifnull(stcb.TowCharge,0)+ifnull(stcb.othercost,0)) end else 0 end),0)+ifnull((case rcb.repairtype when 3 then 0 when 4 then 0 when 8 then 0 else (ifnull(rcb.MaterialCost,0)-ifnull(total.clkk,0))*ifnull(pmg.rate,0) end ),0)
 +ifnull((case rcb.repairtype when 3 then 0 when 4 then 0 when 8 then 0 else (ifnull(rcb.MaterialCost,0)-ifnull(total.clkk,0)) * ifnull(bsr.oldparttranscoefficient,0) end),0)+( ifnull(rcb.OtherCost,0) - ifnull(total.Otherkk,0)) as tatol_cost,---⑴材料费+⑵工时费+⑵外出服务费+ 配件管理费+故障件清退运费+⑴其他费用
    ifnull(rcb.MaterialCost,0)+rcb.LaborCost +ifnull(stcb.TotalAmount,0)
      +ifnull(rcb.OtherCost,0) --+ifnull(stcb.othercost,0) 
      as carry_out_tatol_cost,--"提报费用合计",--材料费+工时费+外出服务费+其他费用
     ifnull(rcb.MaterialCost,0)-ifnull(total.clkk,0)+ ifnull(rcb.LaborCost,0)-ifnull(total.gskk,0)
      +(case stcb.status 
          when 6 then (ifnull(stcb.TotalAmount,0) - ifnull(total.wckk,0)) 
          else 0 
        end )
      + ifnull(rcb.OtherCost,0)-- + ifnull(stcb.othercost,0)
       - ifnull(total.Otherkk,0) + ifnull(rcb.partsmanagementcost,0)
      - ifnull(total.pjglfkk,0) - ifnull(total.jjyfkk,0)  as cost_all,--⑴费用合计,---⑴材料费+⑴工时费+⑴外出服务费+ ⑴其他费用+⑴配件管理费+⑴故障件清退运费

    ifnull(rcb.partsmanagementcost,0)-ifnull(total.pjglfkk,0)  as parts_manage_cost,--as ⑴配件管理费,
      -ifnull(total.jjyfkk,0) as Fault_component_cost,--as ⑴故障件清退运费,
      case rcb.repairtype 
          when 3 then 0 
          else  wxxmqd.DefaultLaborHour 
      end  as LaborHour,--"工时定额",
      case rcb.repairtype 
          when 3 then 0 
          else wxxmqd.LaborUnitPrice 
      end as LaborUnitPrice,--"工时单价",
      case rcb.repairtype 
          when 3 then 0 
          else wxxmqd.xjxs 
      end  as xjxs,--星级,
      v.DriveModeCode ,--as 驱动形式,
      stcb.code as trip_code,--as 外出索赔单编号,
      map(stcb.status,1,'新增',2,'提交',4,'初审通过',6,'生效',10,'审核不通过',99,'作废') as status_name,--as "外出索赔单状态",
      dsi.businesscode ,--as "业务编号",
      rcb.approvername ,--as 终审人,
      rcb.OtherCostReason ,--as 维修索赔单其他费用说明,
      stcb.OtherCostReason as trip_OtherCostReason,--as 外出其他费用说明,
      rcb.status,
/*修改*/     (select map(count(*),0,'否','是')from pms.repairclaimbill rcba 
        where rcba.source_code = rcb.source_code and rcba.id<>rcb.id and rcba.vin= rcb.vin 
            and days_between(rcba.createtime, rcb.createtime)<=30 and rcba.status<>99) as REPEAT_REPAIR_BY_BRAND_VEHICLE, --按品牌车辆重复维修,
/*修改*/     (select map(count(*),0,'否','是')from pms.repairclaimbill rcba 
      where rcba.source_code = rcb.source_code and rcba.id<>rcb.id and rcba.vin= rcb.vin 
        and  days_between(rcba.createtime, rcb.createtime)<=30 and rcba.status<>99 
        and substr(rcba.MalfunctionCode,1,5)=substr(rcb.MalfunctionCode,1,5)) as REPEAT_REPAIR_BY_BRAND_DEFECT, --按品牌故障重复维修,
/*修改*/     (select map(count(*),0,'否','是')from pms.repairclaimbill rcba 
      where rcba.source_code = rcb.source_code and rcba.id<>rcb.id and rcba.vin= rcb.vin 
        and  days_between(rcba.createtime, rcb.createtime)<=30 and rcba.status<>99 and rcba.dealerid=rcb.dealerid) AS REPEAT_REPAIR_BY_SERVICER_VEHICLE, --按服务站车辆重复维修,
/*修改*/     (select map(count(*),0,'否','是')from pms.repairclaimbill rcba 
      where rcba.source_code = rcb.source_code and rcba.id<>rcb.id and rcba.vin= rcb.vin 
        and  days_between(rcba.createtime, rcb.createtime)<=30 and rcba.status<>99 and rcba.dealerid=rcb.dealerid 
        and substr(rcba.MalfunctionCode,1,5)=substr(rcb.MalfunctionCode,1,5)) REPEAT_REPAIR_BY_SERVICER_DEFECT --按服务站故障重复维修
  from pms.repairclaimbill rcb 
  inner join pms.branch b on b.id = rcb.branchid and b.source_code = rcb.source_code
  inner join pms.partssalescategory psc on psc.id = rcb.partssalescategoryid and psc.source_code = rcb.source_code
  left join ( select id,name,'服务产品线' as "产品线类型" ,source_code
        from pms.serviceproductline  
        union all 
        select id,EngineName as name,'服务产品线'as "产品线类型" ,source_code
        from pms.EngineProductLine)spl
      on spl.id = rcb.serviceproductlineid and spl.source_code = rcb.source_code
  inner join pms.vehicleinformation v  on v.id = rcb.vehicleid and v.source_code = rcb.source_code
  left join ( select RepairClaimBillId,source_code,sum(DefaultLaborHour) as DefaultLaborHour,
          max(LaborCost/(LaborUnitPrice*DefaultLaborHour)) as xjxs,max(LaborUnitPrice) LaborUnitPrice
        from pms.RepairClaimItemDetail 
        where LaborUnitPrice<>0 and DefaultLaborHour <>0 
        group by RepairClaimBillId,source_code) wxxmqd 
    on rcb.id = wxxmqd.RepairClaimBillId and rcb.source_code = wxxmqd.source_code
  left join pms.branchsupplierrelation bsr 
    on bsr.partssalescategoryid = rcb.partssalescategoryid 
    and bsr.supplierid = rcb.Claimsupplierid
    and bsr.source_code = rcb.source_code and bsr.status=1  --20170309
 -----供应商的配件管理费
  LEFT JOIN (select a.id,b.rate,a.source_code 
        from pms.partsmanagementcostgrade  a 
        inner join pms.partsmanagementcostrate b 
        on a.id = b.partsmanagementcostgradeid
        and a.source_code = b.source_code)pmg 
    on pmg.id = bsr.partsmanagementcostgradeid 
    and pmg.source_code = bsr.source_code 
  left join pms.ServiceTripClaimBill stcb on stcb.RepairClaimBillId=rcb.id and stcb.source_code=rcb.source_code and stcb.status<>99
  left join pms.RepairClaimApplication rca on rca.id=rcb.Repairclaimapplicationid and rca.source_code=rcb.source_code
 -----汇总索赔单所有扣款
  left join (select sourcecode,source_code,sum(transactionamount) as transactionamount from pms.ExpenseAdjustmentBill
        where status<>99 and DebitOrReplenish=1 group by sourcecode,source_code) eabkk 
    on eabkk.sourcecode=rcb.claimbillcode and eabkk.source_code=rcb.source_code
 -----汇总索赔单所有补款
  left join (select sourcecode,source_code,sum(transactionamount) as transactionamount from pms.ExpenseAdjustmentBill
        where status<>99 and DebitOrReplenish=2 group by sourcecode,source_code) eabbk 
    on eabbk.sourcecode=rcb.claimbillcode and eabbk.source_code=rcb.source_code
 ------汇总材料费、工时费、其他费用扣补款
  left join (select a.id,(ifnull(clkk.fee,0)-ifnull(clbk.fee,0)) as clkk,(ifnull(gskk.fee,0)-ifnull(gsbk.fee,0)) as gskk,
          (ifnull(Otherkk.fee,0) - ifnull(Otherbk.fee,0))as Otherkk,(ifnull(wckk.fee,0) - ifnull(wcbk.fee,0)) as wckk,
          (ifnull(pjglfkk.fee,0) - ifnull(pjglfbk.fee,0))as pjglfkk,(ifnull(jjyfkk.fee,0) - ifnull(jjyfbk.fee,0)) as jjyfkk,
          a.source_code
        from pms.RepairClaimBill a 
      left join
        (select sourcecode,sum(TransactionAmount) as fee,source_code from pms.ExpenseAdjustmentBill
          where status<>99 and TransactionCategory in(2,16)  and DebitOrReplenish=1 
          group by sourcecode,source_code)clkk 
        on clkk.sourcecode = a.claimbillcode and clkk.source_code = a.source_code
      left join
        (select sourcecode,sum(TransactionAmount) as fee,source_code from pms.ExpenseAdjustmentBill
          where status<>99 and TransactionCategory in(2,16) and DebitOrReplenish=2  
          group by sourcecode,source_code)clbk 
      on clbk.sourcecode = a.claimbillcode and clbk.source_code = a.source_code
----工时扣补款
      left join(select sourcecode,sum(TransactionAmount) as fee,source_code from pms.ExpenseAdjustmentBill
            where status<>99 and TransactionCategory in(1,15) and DebitOrReplenish=1  group by sourcecode,source_code)gskk 
        on gskk.sourcecode = a.claimbillcode and gskk.source_code = a.source_code
      left join(select sourcecode,sum(TransactionAmount) as fee,source_code from pms.ExpenseAdjustmentBill
            where status<>99 and TransactionCategory in(1,15) and DebitOrReplenish=2  group by sourcecode,source_code)gsbk 
        on gsbk.sourcecode = a.claimbillcode and gsbk.source_code = a.source_code
---外出扣补款
----      left join(select sourcecode,sum(TransactionAmount) as fee,source_code from pms.ExpenseAdjustmentBill
--            where status<>99 and TransactionCategory in(12) and DebitOrReplenish=1 group by sourcecode,source_code)wckk 
---       on wckk.sourcecode = a.claimbillcode and wckk.source_code = a.source_code
--      left join(select sourcecode,sum(TransactionAmount) as fee,source_code from pms.ExpenseAdjustmentBill
--            where status<>99 and TransactionCategory in(12) and DebitOrReplenish=2  group by sourcecode,source_code)wcbk 
--        on wcbk.sourcecode = a.claimbillcode  and wcbk.source_code = a.source_code
        
---外出扣补款
/*修改*/left join(select STC.RepairClaimBillId,sum(TransactionAmount) as fee , stc.source_code
        from pms.ExpenseAdjustmentBill EAB
          inner join pms.servicetripclaimbill STC on EAB.Sourcecode=STC.code AND EAB.source_code = stc.source_code
/*修改*/        where eab.status<>99 and TransactionCategory in(12) 
            and DebitOrReplenish=1 
        group by stc.RepairClaimBillId, stc.source_code)wckk on wckk.RepairClaimBillId = a.id and wckk.source_code = a.source_code
/*修改*/left join(select  stc.RepairClaimBillId,sum(TransactionAmount) as fee, stc.source_code
         from pms.ExpenseAdjustmentBill  eab
          inner join pms.servicetripclaimbill stc on eab.Sourcecode=stc.code AND EAB.source_code = stc.source_code
/*修改*/      where eab.status<>99 and TransactionCategory in(12) and DebitOrReplenish=2 
         group by RepairClaimBillId, stc.source_code)wcbk on wcbk.RepairClaimBillId = a.id and wckk.source_code = a.source_code

---------配件管理费
      left join(select sourcecode,sum(TransactionAmount) as fee,source_code from pms.ExpenseAdjustmentBill
            where status<>99 and TransactionCategory in(3,17) and DebitOrReplenish=1 group by sourcecode,source_code)pjglfkk 
        on pjglfkk.sourcecode = a.claimbillcode and pjglfkk.source_code = a.source_code
      left join(select sourcecode,sum(TransactionAmount) as fee,source_code from pms.ExpenseAdjustmentBill
            where status<>99 and TransactionCategory in(3,17) and DebitOrReplenish=2  group by sourcecode,source_code)pjglfbk 
        on pjglfbk.sourcecode = a.claimbillcode and pjglfbk.source_code = a.source_code
---------旧件运费
      left join(select sourcecode,sum(TransactionAmount) as fee,source_code from pms.ExpenseAdjustmentBill
            where status<>99 and TransactionCategory in(14) and DebitOrReplenish=1 group by sourcecode,source_code)jjyfkk 
        on jjyfkk.sourcecode = a.claimbillcode and jjyfkk.source_code = a.source_code
      left join(select sourcecode,sum(TransactionAmount) as fee,source_code from pms.ExpenseAdjustmentBill
            where status<>99 and TransactionCategory in(14) and DebitOrReplenish=2  group by sourcecode,source_code)jjyfbk 
        on jjyfbk.sourcecode = a.claimbillcode and jjyfbk.source_code = a.source_code
----其他扣补款合计
      left join(select sourcecode,sum(TransactionAmount) as fee,source_code from pms.ExpenseAdjustmentBill
            where status<>99 and TransactionCategory not in(1,2,3,14,15,16,17,12) and DebitOrReplenish=1   group by sourcecode,source_code)Otherkk 
        on Otherkk.sourcecode = a.claimbillcode and Otherkk.source_code = a.source_code
      left join(select sourcecode,sum(TransactionAmount) as fee,source_code from pms.ExpenseAdjustmentBill
            where status<>99 and TransactionCategory not in(1,2,3,14,15,16,17,12) and DebitOrReplenish=2   group by sourcecode,source_code)Otherbk 
        on Otherbk.sourcecode = a.claimbillcode and Otherbk.source_code = a.source_code
    ) total 
    on total.id = rcb.id and total.source_code = rcb.source_code
  left join (select scsb.SettlementEndTime,scsd.sourcecode,scsd.source_code from pms.SsClaimSettlementDetail scsd
      left join pms.SsClaimSettlementBill scsb
        on scsb.id=scsd.SsClaimSettlementBillId and scsb.source_code=scsd.source_code
        where scsb.status<>99)scsb
    on scsb.SourceCode=rcb.claimbillcode and scsb.source_code=rcb.source_code
  left join pms.DealerServiceInfo dsi
    on dsi.BranchId =rcb.branchid and rcb.dealerid=dsi.Dealerid
     and dsi.partssalescategoryid=rcb.partssalescategoryid and dsi.source_code=rcb.source_code
  left join pms.Marketingdepartment md
    on md.id=dsi.Marketingdepartmentid and md.source_code=dsi.source_code where dsi.status=1;

END /********* End Procedure Script ************/


