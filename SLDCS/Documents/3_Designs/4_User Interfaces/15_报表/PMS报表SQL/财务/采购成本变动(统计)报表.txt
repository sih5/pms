select *
  from ( select pcb.branchcode,
                pcb.code as 入库单号,
                pcb.partssalescategoryid,
                ps.name as 品牌,
                pcbd.sparepartcode as 配件图号,
                pcbd.sparepartname as 配件名称,
                pcbd.inspectedquantity as 入库数量,
                pcbd.settlementprice as 采购价格,
                ppod.UnitPrice as 采购订单价格,
                ppod.PURCHASEPRICEBEFORE as 变动前价格,
                (ppod.PURCHASEPRICEBEFORE - ppod.UnitPrice) *
                pcbd.inspectedquantity as 变动金额,
                pcb.counterpartcompanycode as 供应商编号,
                pcb.counterpartcompanyname as 供应商名称,
                pcb.warehouseid,
                pcb.warehousecode as 仓库编号,
                pcb.warehousename as 仓库名称,
                pcb.createtime as 入库时间,
                dense_rank() over(partition by pppp.PartsSalesCategoryId, pppp.partid,pcb.code order by pppp.ApproveTime desc) as rank,
                dense_rank() over(partition by ppppa.PartsSalesCategoryId, ppppa.partid,pcb.code order by ppppa.ApproveTime desc) as ranka,  
                 dense_rank() over(partition by ppppb.PartsSalesCategoryId, ppppb.partid,pcb.code order by ppppb.ApproveTime asc) as rankb,                     
                nvl(ppppa.purchaseprice,ppppb.purchaseprice) as 今年最早采购价格,   
                 ( pcbd.settlementprice-nvl(ppppa.purchaseprice,ppppb.purchaseprice)) *pcbd.inspectedquantity as 今年最早变动金额,         
                pppp.approvetime as 订单采购价格生效时间,decode(pcb.settlementstatus,
                      1,
                      '不结算',
                      2,
                      '待结算',
                      3,
                      '已结算') 结算状态,pcb.SettlementStatus,a.code 结算单号,decode(a.status,1,'新建',2,'已审批',3,'发票登记',4,'已反冲',5,'发票驳回',6,'发票已审核',99,'作废') 结算单状态,a.invoiceapprovetime 发票审核时间,ppo.code 采购订单号	
   from yxdcs.partsinboundcheckbill pcb
         inner join yxdcs.partssalescategory ps
            on ps.id = pcb.partssalescategoryid
         inner join yxdcs.partsinboundcheckbilldetail pcbd
            on pcb.id = pcbd.partsinboundcheckbillid
           and pcb.inboundtype = 1
           and pcb.storagecompanytype = 1
         inner join yxdcs.partsinboundplan pbp
            on pbp.id = pcb.partsinboundplanid
         inner join yxdcs.Suppliershippingorder sso
            on sso.id = pbp.sourceid
         inner join yxdcs.PartsPurchaseOrder ppo
            on ppo.id = sso.PartsPurchaseOrderId
         inner join yxdcs.Partspurchaseorderdetail ppod
            on ppod.partspurchaseorderid = ppo.id
           and ppod.sparepartid = pcbd.sparepartid
           left join   yxdcs.partspurchasesettleref b on  b.sourcecode=pcb.code
           inner join yxdcs.partspurchasesettlebill a  on a.id=b.partspurchasesettlebillid 
          left join (select pppc.id,
                            pppc.PartsSalesCategoryId,
                            pppd.partid,
                            pppd.Price                as purchaseprice,
                            pppd.Supplierid           as partssupplierid,
                            pppc.ApproveTime,
                            pppd.validfrom,
                            pppd.validto
                     --,dense_rank() over (order by pppc.id desc) rank
                       from yxdcs.PartsPurchasePricingChange pppc
                      inner join yxdcs.PartsPurchasePricingDetail pppd
                         on pppc.id = pppd.parentid
                        and pppc.status = 2
                        and pppc.ApproveTime is not null) pppp
            on pcbd.sparepartid = pppp.Partid
           and pcb.counterpartcompanyid = pppp.partssupplierid
           and pcb.partssalescategoryid = pppp.partssalescategoryid
           and ppo.createtime >= pppp.approvetime
           and ppo.createtime >= pppp.validfrom
           and ppo.createtime <= pppp.validto
          left join (select pppc.id,
                            pppc.PartsSalesCategoryId,
                            pppd.partid,
                            pppd.Price                as purchaseprice,
                            pppd.Supplierid           as partssupplierid,
                            pppc.ApproveTime,
                            pppd.validfrom,
                            pppd.validto
                     --,dense_rank() over (order by pppc.id desc) rank
                       from yxdcs.PartsPurchasePricingChange pppc
                      inner join yxdcs.PartsPurchasePricingDetail pppd
                         on pppc.id = pppd.parentid
                        and pppc.status = 2
                        and pppc.ApproveTime is not null) ppppa
            on pcbd.sparepartid = ppppa.Partid
           and pcb.counterpartcompanyid = ppppa.partssupplierid
           and pcb.partssalescategoryid = ppppa.partssalescategoryid
          
           and to_date(to_char(ppo.createtime, 'yyyy') || '-1-1 23:59:59',
                       'yyyy-mm-dd hh24:mi:ss')-1 = ppppa.validto
            left join (select pppc.id,
                            pppc.PartsSalesCategoryId,
                            pppd.partid,
                            pppd.Price                as purchaseprice,
                            pppd.Supplierid           as partssupplierid,
                            pppc.ApproveTime,
                            pppd.validfrom,
                            pppd.validto
                     --,dense_rank() over (order by pppc.id desc) rank
                       from yxdcs.PartsPurchasePricingChange pppc
                      inner join yxdcs.PartsPurchasePricingDetail pppd
                         on pppc.id = pppd.parentid
                        and pppc.status = 2
                        and pppc.ApproveTime is not null) ppppb
            on pcbd.sparepartid = ppppb.Partid
           and pcb.counterpartcompanyid = ppppb.partssupplierid
           and pcb.partssalescategoryid = ppppb.partssalescategoryid
          
           and to_date(to_char(ppo.createtime, 'yyyy') || '-1-1',
                       'yyyy-mm-dd') <= ppppb.validfrom                  
        /* where ppod.UnitPrice <> ppod.PURCHASEPRICEBEFORE
           and ppod.PURCHASEPRICEBEFORE is not null
           and ppod.PURCHASEPRICEBEFORE <> 0*/)                    
 where (rank = 1)  and ( ranka = 1) and (rankb=1) 
and  采购订单价格 <> nvl(今年最早采购价格,0)
           and nvl(今年最早采购价格,0) <> 0
 union
 select *
  from (select pcb.branchcode,
                pcb.code as 入库单号,
                pcb.partssalescategoryid,
                ps.name as 品牌,
                pcbd.sparepartcode as 配件图号,
                pcbd.sparepartname as 配件名称,
                pcbd.inspectedquantity as 入库数量,
                pcbd.settlementprice as 采购价格,
                ppod.UnitPrice as 采购订单价格,
                ppod.PURCHASEPRICEBEFORE as 变动前价格,
                (ppod.PURCHASEPRICEBEFORE - ppod.UnitPrice) *
                pcbd.inspectedquantity as 变动金额,
                pcb.counterpartcompanycode as 供应商编号,
                pcb.counterpartcompanyname as 供应商名称,
                pcb.warehouseid,
                pcb.warehousecode as 仓库编号,
                pcb.warehousename as 仓库名称,
                pcb.createtime as 入库时间,
                dense_rank() over(partition by pppp.PartsSalesCategoryId, pppp.partid,pcb.code order by pppp.ApproveTime desc) as rank,
                dense_rank() over(partition by ppppa.PartsSalesCategoryId, ppppa.partid,pcb.code order by ppppa.ApproveTime desc) as ranka,  
                 dense_rank() over(partition by ppppb.PartsSalesCategoryId, ppppb.partid,pcb.code order by ppppb.ApproveTime asc) as rankb,                     
                nvl(ppppa.purchaseprice,ppppb.purchaseprice) as 今年最早采购价格,     
                 ( pcbd.settlementprice-nvl(ppppa.purchaseprice,ppppb.purchaseprice)) *pcbd.inspectedquantity as 今年最早变动金额,                         
                pppp.approvetime as 订单采购价格生效时间,decode(pcb.settlementstatus,
                      1,
                      '不结算',
                      2,
                      '待结算',
                      3,
                      '已结算') 结算状态,pcb.SettlementStatus,a.code 结算单号,decode(a.status,1,'新建',2,'已审批',3,'发票登记',4,'已反冲',5,'发票驳回',6,'发票已审核',99,'作废') 结算单状态,a.invoiceapprovetime 发票审核时间,ppo.code 采购订单号
          from dcs.partsinboundcheckbill pcb
         inner join dcs.partssalescategory ps
            on ps.id = pcb.partssalescategoryid
         inner join dcs.partsinboundcheckbilldetail pcbd
            on pcb.id = pcbd.partsinboundcheckbillid
           and pcb.inboundtype = 1
           and pcb.storagecompanytype = 1
         inner join dcs.partsinboundplan pbp
            on pbp.id = pcb.partsinboundplanid
         inner join dcs.Suppliershippingorder sso
            on sso.id = pbp.sourceid
         inner join dcs.PartsPurchaseOrder ppo
            on ppo.id = sso.PartsPurchaseOrderId
         inner join dcs.Partspurchaseorderdetail ppod
            on ppod.partspurchaseorderid = ppo.id
           and ppod.sparepartid = pcbd.sparepartid
              left join   dcs.partspurchasesettleref b on  b.sourcecode=pcb.code
           inner  join dcs.partspurchasesettlebill a  on a.id=b.partspurchasesettlebillid 
          left join (select pppc.id,
                            pppc.PartsSalesCategoryId,
                            pppd.partid,
                            pppd.Price                as purchaseprice,
                            pppd.Supplierid           as partssupplierid,
                            pppc.ApproveTime,
                            pppd.validfrom,
                            pppd.validto
                     --,dense_rank() over (order by pppc.id desc) rank
                       from dcs.PartsPurchasePricingChange pppc
                      inner join dcs.PartsPurchasePricingDetail pppd
                         on pppc.id = pppd.parentid
                        and pppc.status = 2
                        and pppc.ApproveTime is not null) pppp
            on pcbd.sparepartid = pppp.Partid
           and pcb.counterpartcompanyid = pppp.partssupplierid
           and pcb.partssalescategoryid = pppp.partssalescategoryid
           and ppo.createtime >= pppp.approvetime
           and ppo.createtime >= pppp.validfrom
           and ppo.createtime <= pppp.validto
          left join (select pppc.id,
                            pppc.PartsSalesCategoryId,
                            pppd.partid,
                            pppd.Price                as purchaseprice,
                            pppd.Supplierid           as partssupplierid,
                            pppc.ApproveTime,
                            pppd.validfrom,
                            pppd.validto
                     --,dense_rank() over (order by pppc.id desc) rank
                       from dcs.PartsPurchasePricingChange pppc
                      inner join dcs.PartsPurchasePricingDetail pppd
                         on pppc.id = pppd.parentid
                        and pppc.status = 2
                        and pppc.ApproveTime is not null) ppppa
            on pcbd.sparepartid = ppppa.Partid
           and pcb.counterpartcompanyid = ppppa.partssupplierid
           and pcb.partssalescategoryid = ppppa.partssalescategoryid
          
           and to_date(to_char(ppo.createtime, 'yyyy') || '-1-1 23:59:59',
                       'yyyy-mm-dd hh24:mi:ss')-1 = ppppa.validto
            left join (select pppc.id,
                            pppc.PartsSalesCategoryId,
                            pppd.partid,
                            pppd.Price                as purchaseprice,
                            pppd.Supplierid           as partssupplierid,
                            pppc.ApproveTime,
                            pppd.validfrom,
                            pppd.validto
                     --,dense_rank() over (order by pppc.id desc) rank
                       from dcs.PartsPurchasePricingChange pppc
                      inner join dcs.PartsPurchasePricingDetail pppd
                         on pppc.id = pppd.parentid
                        and pppc.status = 2
                        and pppc.ApproveTime is not null) ppppb
            on pcbd.sparepartid = ppppb.Partid
           and pcb.counterpartcompanyid = ppppb.partssupplierid
           and pcb.partssalescategoryid = ppppb.partssalescategoryid
          
           and to_date(to_char(ppo.createtime, 'yyyy') || '-1-1',
                       'yyyy-mm-dd') <= ppppb.validfrom
        /* where ppod.UnitPrice <> ppod.PURCHASEPRICEBEFORE
           and ppod.PURCHASEPRICEBEFORE is not null
           and ppod.PURCHASEPRICEBEFORE <> 0*/)                    
 where (rank = 1)  and ( ranka = 1) and (rankb=1) 
and  采购订单价格 <> nvl(今年最早采购价格,0)
           and nvl(今年最早采购价格,0) <> 0
 union 
 
 select *
  from (select pcb.branchcode,
                pcb.code as 入库单号,
                pcb.partssalescategoryid,
                ps.name as 品牌,
                pcbd.sparepartcode as 配件图号,
                pcbd.sparepartname as 配件名称,
                pcbd.inspectedquantity as 入库数量,
                pcbd.settlementprice as 采购价格,
                ppod.UnitPrice as 采购订单价格,
                ppod.PURCHASEPRICEBEFORE as 变动前价格,
                (ppod.PURCHASEPRICEBEFORE - ppod.UnitPrice) *
                pcbd.inspectedquantity as 变动金额,
                pcb.counterpartcompanycode as 供应商编号,
                pcb.counterpartcompanyname as 供应商名称,
                pcb.warehouseid,
                pcb.warehousecode as 仓库编号,
                pcb.warehousename as 仓库名称,
                pcb.createtime as 入库时间,
                dense_rank() over(partition by pppp.PartsSalesCategoryId, pppp.partid,pcb.code order by pppp.ApproveTime desc) as rank,
                dense_rank() over(partition by ppppa.PartsSalesCategoryId, ppppa.partid,pcb.code order by ppppa.ApproveTime desc) as ranka,  
                 dense_rank() over(partition by ppppb.PartsSalesCategoryId, ppppb.partid,pcb.code order by ppppb.ApproveTime asc) as rankb,                     
                nvl(ppppa.purchaseprice,ppppb.purchaseprice) as 今年最早采购价格,          
                 ( pcbd.settlementprice-nvl(ppppa.purchaseprice,ppppb.purchaseprice)) *pcbd.inspectedquantity as 今年最早变动金额,                    
                pppp.approvetime as 订单采购价格生效时间,decode(pcb.settlementstatus,
                      1,
                      '不结算',
                      2,
                      '待结算',
                      3,
                      '已结算') 结算状态,pcb.SettlementStatus,a.code 结算单号,decode(a.status,1,'新建',2,'已审批',3,'发票登记',4,'已反冲',5,'发票驳回',6,'发票已审核',99,'作废') 结算单状态,a.invoiceapprovetime 发票审核时间,ppo.code 采购订单号
          from gcdcs.partsinboundcheckbill pcb
         inner join gcdcs.partssalescategory ps
            on ps.id = pcb.partssalescategoryid
         inner join gcdcs.partsinboundcheckbilldetail pcbd
            on pcb.id = pcbd.partsinboundcheckbillid
           and pcb.inboundtype = 1
           and pcb.storagecompanytype = 1
         inner join gcdcs.partsinboundplan pbp
            on pbp.id = pcb.partsinboundplanid
         inner join gcdcs.Suppliershippingorder sso
            on sso.id = pbp.sourceid
         inner join gcdcs.PartsPurchaseOrder ppo
            on ppo.id = sso.PartsPurchaseOrderId
         inner join gcdcs.Partspurchaseorderdetail ppod
            on ppod.partspurchaseorderid = ppo.id
           and ppod.sparepartid = pcbd.sparepartid
              left join   gcdcs.partspurchasesettleref b on  b.sourcecode=pcb.code
           left join gcdcs.partspurchasesettlebill a  on a.id=b.partspurchasesettlebillid 
          left join (select pppc.id,
                            pppc.PartsSalesCategoryId,
                            pppd.partid,
                            pppd.Price                as purchaseprice,
                            pppd.Supplierid           as partssupplierid,
                            pppc.ApproveTime,
                            pppd.validfrom,
                            pppd.validto
                     --,dense_rank() over (order by pppc.id desc) rank
                       from gcdcs.PartsPurchasePricingChange pppc
                      inner join gcdcs.PartsPurchasePricingDetail pppd
                         on pppc.id = pppd.parentid
                        and pppc.status = 2
                        and pppc.ApproveTime is not null) pppp
            on pcbd.sparepartid = pppp.Partid
           and pcb.counterpartcompanyid = pppp.partssupplierid
           and pcb.partssalescategoryid = pppp.partssalescategoryid
           and ppo.createtime >= pppp.approvetime
           and ppo.createtime >= pppp.validfrom
           and ppo.createtime <= pppp.validto
          left join (select pppc.id,
                            pppc.PartsSalesCategoryId,
                            pppd.partid,
                            pppd.Price                as purchaseprice,
                            pppd.Supplierid           as partssupplierid,
                            pppc.ApproveTime,
                            pppd.validfrom,
                            pppd.validto
                     --,dense_rank() over (order by pppc.id desc) rank
                       from gcdcs.PartsPurchasePricingChange pppc
                      inner join gcdcs.PartsPurchasePricingDetail pppd
                         on pppc.id = pppd.parentid
                        and pppc.status = 2
                        and pppc.ApproveTime is not null) ppppa
            on pcbd.sparepartid = ppppa.Partid
           and pcb.counterpartcompanyid = ppppa.partssupplierid
           and pcb.partssalescategoryid = ppppa.partssalescategoryid
          
           and to_date(to_char(ppo.createtime, 'yyyy') || '-1-1 23:59:59',
                       'yyyy-mm-dd hh24:mi:ss')-1 = ppppa.validto
            left join (select pppc.id,
                            pppc.PartsSalesCategoryId,
                            pppd.partid,
                            pppd.Price                as purchaseprice,
                            pppd.Supplierid           as partssupplierid,
                            pppc.ApproveTime,
                            pppd.validfrom,
                            pppd.validto
                     --,dense_rank() over (order by pppc.id desc) rank
                       from gcdcs.PartsPurchasePricingChange pppc
                      inner join gcdcs.PartsPurchasePricingDetail pppd
                         on pppc.id = pppd.parentid
                        and pppc.status = 2
                        and pppc.ApproveTime is not null) ppppb
            on pcbd.sparepartid = ppppb.Partid
           and pcb.counterpartcompanyid = ppppb.partssupplierid
           and pcb.partssalescategoryid = ppppb.partssalescategoryid
          
           and to_date(to_char(ppo.createtime, 'yyyy') || '-1-1',
                       'yyyy-mm-dd') <= ppppb.validfrom
        /* where ppod.UnitPrice <> ppod.PURCHASEPRICEBEFORE
           and ppod.PURCHASEPRICEBEFORE is not null
           and ppod.PURCHASEPRICEBEFORE <> 0*/)                    
 where (rank = 1)  and ( ranka = 1) and (rankb=1) 
and  采购订单价格 <> nvl(今年最早采购价格,0)
           and nvl(今年最早采购价格,0) <> 0
 union 
 select *
  from (select pcb.branchcode,
                pcb.code as 入库单号,
                pcb.partssalescategoryid,
                ps.name as 品牌,
                pcbd.sparepartcode as 配件图号,
                pcbd.sparepartname as 配件名称,
                pcbd.inspectedquantity as 入库数量,
                pcbd.settlementprice as 采购价格,
                ppod.UnitPrice as 采购订单价格,
                ppod.PURCHASEPRICEBEFORE as 变动前价格,
                (ppod.PURCHASEPRICEBEFORE - ppod.UnitPrice) *
                pcbd.inspectedquantity as 变动金额,
                pcb.counterpartcompanycode as 供应商编号,
                pcb.counterpartcompanyname as 供应商名称,
                pcb.warehouseid,
                pcb.warehousecode as 仓库编号,
                pcb.warehousename as 仓库名称,
                pcb.createtime as 入库时间,
                dense_rank() over(partition by pppp.PartsSalesCategoryId, pppp.partid,pcb.code order by pppp.ApproveTime desc) as rank,
                dense_rank() over(partition by ppppa.PartsSalesCategoryId, ppppa.partid,pcb.code order by ppppa.ApproveTime desc) as ranka,  
                 dense_rank() over(partition by ppppb.PartsSalesCategoryId, ppppb.partid,pcb.code order by ppppb.ApproveTime asc) as rankb,                     
                nvl(ppppa.purchaseprice,ppppb.purchaseprice) as 今年最早采购价格,             
                 ( pcbd.settlementprice-nvl(ppppa.purchaseprice,ppppb.purchaseprice)) *pcbd.inspectedquantity as 今年最早变动金额,                 
                pppp.approvetime as 订单采购价格生效时间,decode(pcb.settlementstatus,
                      1,
                      '不结算',
                      2,
                      '待结算',
                      3,
                      '已结算') 结算状态,pcb.SettlementStatus,a.code 结算单号,decode(a.status,1,'新建',2,'已审批',3,'发票登记',4,'已反冲',5,'发票驳回',6,'发票已审核',99,'作废') 结算单状态,a.invoiceapprovetime 发票审核时间,ppo.code 采购订单号
          from sddcs.partsinboundcheckbill pcb
         inner join sddcs.partssalescategory ps
            on ps.id = pcb.partssalescategoryid
         inner join sddcs.partsinboundcheckbilldetail pcbd
            on pcb.id = pcbd.partsinboundcheckbillid
           and pcb.inboundtype = 1
           and pcb.storagecompanytype = 1
         inner join sddcs.partsinboundplan pbp
            on pbp.id = pcb.partsinboundplanid
         inner join sddcs.Suppliershippingorder sso
            on sso.id = pbp.sourceid
         inner join sddcs.PartsPurchaseOrder ppo
            on ppo.id = sso.PartsPurchaseOrderId
         inner join sddcs.Partspurchaseorderdetail ppod
            on ppod.partspurchaseorderid = ppo.id
           and ppod.sparepartid = pcbd.sparepartid
              left join   sddcs.partspurchasesettleref b on  b.sourcecode=pcb.code
           left join sddcs.partspurchasesettlebill a  on a.id=b.partspurchasesettlebillid
          left join (select pppc.id,
                            pppc.PartsSalesCategoryId,
                            pppd.partid,
                            pppd.Price                as purchaseprice,
                            pppd.Supplierid           as partssupplierid,
                            pppc.ApproveTime,
                            pppd.validfrom,
                            pppd.validto
                     --,dense_rank() over (order by pppc.id desc) rank
                       from sddcs.PartsPurchasePricingChange pppc
                      inner join sddcs.PartsPurchasePricingDetail pppd
                         on pppc.id = pppd.parentid
                        and pppc.status = 2
                        and pppc.ApproveTime is not null) pppp
            on pcbd.sparepartid = pppp.Partid
           and pcb.counterpartcompanyid = pppp.partssupplierid
           and pcb.partssalescategoryid = pppp.partssalescategoryid
           and ppo.createtime >= pppp.approvetime
           and ppo.createtime >= pppp.validfrom
           and ppo.createtime <= pppp.validto
          left join (select pppc.id,
                            pppc.PartsSalesCategoryId,
                            pppd.partid,
                            pppd.Price                as purchaseprice,
                            pppd.Supplierid           as partssupplierid,
                            pppc.ApproveTime,
                            pppd.validfrom,
                            pppd.validto
                     --,dense_rank() over (order by pppc.id desc) rank
                       from sddcs.PartsPurchasePricingChange pppc
                      inner join sddcs.PartsPurchasePricingDetail pppd
                         on pppc.id = pppd.parentid
                        and pppc.status = 2
                        and pppc.ApproveTime is not null) ppppa
            on pcbd.sparepartid = ppppa.Partid
           and pcb.counterpartcompanyid = ppppa.partssupplierid
           and pcb.partssalescategoryid = ppppa.partssalescategoryid
          
           and to_date(to_char(ppo.createtime, 'yyyy') || '-1-1 23:59:59',
                       'yyyy-mm-dd hh24:mi:ss')-1 = ppppa.validto
            left join (select pppc.id,
                            pppc.PartsSalesCategoryId,
                            pppd.partid,
                            pppd.Price                as purchaseprice,
                            pppd.Supplierid           as partssupplierid,
                            pppc.ApproveTime,
                            pppd.validfrom,
                            pppd.validto
                     --,dense_rank() over (order by pppc.id desc) rank
                       from sddcs.PartsPurchasePricingChange pppc
                      inner join sddcs.PartsPurchasePricingDetail pppd
                         on pppc.id = pppd.parentid
                        and pppc.status = 2
                        and pppc.ApproveTime is not null) ppppb
            on pcbd.sparepartid = ppppb.Partid
           and pcb.counterpartcompanyid = ppppb.partssupplierid
           and pcb.partssalescategoryid = ppppb.partssalescategoryid
          
           and to_date(to_char(ppo.createtime, 'yyyy') || '-1-1',
                       'yyyy-mm-dd') <= ppppb.validfrom
         /* where ppod.UnitPrice <> ppod.PURCHASEPRICEBEFORE
           and ppod.PURCHASEPRICEBEFORE is not null
           and ppod.PURCHASEPRICEBEFORE <> 0*/)                    
 where (rank = 1)  and ( ranka = 1) and (rankb=1) 
and  采购订单价格 <> nvl(今年最早采购价格,0)
           and nvl(今年最早采购价格,0) <> 0