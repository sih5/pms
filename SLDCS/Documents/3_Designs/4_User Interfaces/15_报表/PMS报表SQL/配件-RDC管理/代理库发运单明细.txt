select psc.branchcode as 分公司编号,
       pso.code as 配件发运单号,
       pso.Originalrequirementbillcode as 原始需求单据编号,
       pso.partssalescategoryid,
       psc.name as 品牌,
       pso.ShippingCompanyCode as 代理库编号,
       pso.ShippingCompanyName as 代理库名称,
       pso.ReceivingCompanyCode as 收货单位编号,
       pso.ReceivingCompanyName as 收货单位名称,
       psod.sparepartcode as 配件图号,
       psod.sparepartname as 配件名称,
       psod.Settlementprice as 销售价,
       psod.Shippingamount as 数量,
       psod.Settlementprice* psod.Shippingamount as 金额,
       pso.RequestedArrivalDate as 要求到货日期,
       pso.ShippingDate as 发运日期,
       pso.ConfirmedReceptionTime as 确认时间,
       case pso.status  when 1  then '新建' when 2  then '收货确认' when 3  then '回执确认' when 99  then '作废' end
 as 状态,
       pso.status,
       pso.warehousecode as 仓库编号,
       pso.warehousename as 仓库名称,kvi.value as 发运方式,pso.remark as 发运单备注
  from yxdcs.PartsShippingOrder pso
  inner join yxdcs.partssalescategory psc
  on psc.id=pso.partssalescategoryid
  inner join yxdcs.partsshippingorderdetail psod
  on psod.partsshippingorderid=pso.id
   left join yxdcs.keyvalueitem kvi on kvi.name = 'PartsShipping_Method' and kvi.status = 1 and kvi.key = pso.ShippingMethod
 where pso.shippingcompanyid <> 2
 
 union all 
 
 select psc.branchcode as 分公司编号,
       pso.code as 配件发运单号,
       pso.Originalrequirementbillcode as 原始需求单据编号,
       pso.partssalescategoryid,
       psc.name as 品牌,
       pso.ShippingCompanyCode as 代理库编号,
       pso.ShippingCompanyName as 代理库名称,
       pso.ReceivingCompanyCode as 收货单位编号,
       pso.ReceivingCompanyName as 收货单位名称,
       psod.sparepartcode as 配件图号,
       psod.sparepartname as 配件名称,
       psod.Settlementprice as 销售价,
       psod.Shippingamount as 数量,
       psod.Settlementprice* psod.Shippingamount as 金额,
       pso.RequestedArrivalDate as 要求到货日期,
       pso.ShippingDate as 发运日期,
       pso.ConfirmedReceptionTime as 确认时间,
       case pso.status  when 1  then '新建' when 2  then '收货确认' when 3  then '回执确认' when 99  then '作废' end
 as 状态,
       pso.status,
       pso.warehousecode as 仓库编号,
       pso.warehousename as 仓库名称,kvi.value as 发运方式,pso.remark as 发运单备注
  from gcdcs.PartsShippingOrder pso
  inner join gcdcs.partssalescategory psc
  on psc.id=pso.partssalescategoryid
  inner join gcdcs.partsshippingorderdetail psod
  on psod.partsshippingorderid=pso.id
   left join gcdcs.keyvalueitem kvi on kvi.name = 'PartsShipping_Method' and kvi.status = 1 and kvi.key = pso.ShippingMethod
 where pso.shippingcompanyid <> 2
 
  union all 
  
  select psc.branchcode as 分公司编号,
       pso.code as 配件发运单号,
       pso.Originalrequirementbillcode as 原始需求单据编号,
       pso.partssalescategoryid,
       psc.name as 品牌,
       pso.ShippingCompanyCode as 代理库编号,
       pso.ShippingCompanyName as 代理库名称,
       pso.ReceivingCompanyCode as 收货单位编号,
       pso.ReceivingCompanyName as 收货单位名称,
       psod.sparepartcode as 配件图号,
       psod.sparepartname as 配件名称,
       psod.Settlementprice as 销售价,
       psod.Shippingamount as 数量,
       psod.Settlementprice* psod.Shippingamount as 金额,
       pso.RequestedArrivalDate as 要求到货日期,
       pso.ShippingDate as 发运日期,
       pso.ConfirmedReceptionTime as 确认时间,
       case pso.status  when 1  then '新建' when 2  then '收货确认' when 3  then '回执确认' when 99  then '作废' end
 as 状态,
       pso.status,
       pso.warehousecode as 仓库编号,
       pso.warehousename as 仓库名称,kvi.value as 发运方式,pso.remark as 发运单备注
  from dcs.PartsShippingOrder pso
  inner join dcs.partssalescategory psc
  on psc.id=pso.partssalescategoryid
  inner join dcs.partsshippingorderdetail psod
  on psod.partsshippingorderid=pso.id
   left join dcs.keyvalueitem kvi on kvi.name = 'PartsShipping_Method' and kvi.status = 1 and kvi.key = pso.ShippingMethod
 where pso.shippingcompanyid <> 2
  union all 
  
  select psc.branchcode as 分公司编号,
       pso.code as 配件发运单号,
       pso.Originalrequirementbillcode as 原始需求单据编号,
       pso.partssalescategoryid,
       psc.name as 品牌,
       pso.ShippingCompanyCode as 代理库编号,
       pso.ShippingCompanyName as 代理库名称,
       pso.ReceivingCompanyCode as 收货单位编号,
       pso.ReceivingCompanyName as 收货单位名称,
       psod.sparepartcode as 配件图号,
       psod.sparepartname as 配件名称,
       psod.Settlementprice as 销售价,
       psod.Shippingamount as 数量,
       psod.Settlementprice* psod.Shippingamount as 金额,
       pso.RequestedArrivalDate as 要求到货日期,
       pso.ShippingDate as 发运日期,
       pso.ConfirmedReceptionTime as 确认时间,
       case pso.status  when 1  then '新建' when 2  then '收货确认' when 3  then '回执确认' when 99  then '作废' end
 as 状态,
       pso.status,
       pso.warehousecode as 仓库编号,
       pso.warehousename as 仓库名称,kvi.value as 发运方式,pso.remark as 发运单备注
  from sddcs.PartsShippingOrder pso
  inner join sddcs.partssalescategory psc
  on psc.id=pso.partssalescategoryid
  inner join sddcs.partsshippingorderdetail psod
  on psod.partsshippingorderid=pso.id
   left join sddcs.keyvalueitem kvi on kvi.name = 'PartsShipping_Method' and kvi.status = 1 and kvi.key = pso.ShippingMethod
 where pso.shippingcompanyid <> 2