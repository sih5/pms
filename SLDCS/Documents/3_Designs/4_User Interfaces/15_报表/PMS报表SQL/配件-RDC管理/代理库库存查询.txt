select a.BranchId,
       bch.code as BranchCode,
       a.Code,
       a.Name,
       a.Id,
       a.WarehouseName,
       a.SpareId,
       a.SpareCode,
       a.SpareName,
       a.Qty,
       a.qty - nvl(c.lockedqty, 0) as UsableQty,
       nvl(b.salesprice, 0) as Saleprice,
       a.qty * nvl(b.salesprice, 0) as Totalprice,
       a.partssalescategoryid,
       a.partssalescategoryname,a.value as 配件所属分类
  from (select f.branchid,
               d.code,
               d.name,
               b.id,
               b.name warehousename,
               c.id spareid,
               c.code sparecode,
               c.name sparename,
               sum(a.quantity) qty,
               f.partssalescategoryid,
               psc.name as partssalescategoryname,kvi.value
          from dcs.partsstock a
         inner join dcs.warehouse b
            on a.warehouseid = b.id
         inner join dcs.sparepart c
            on c.id = a.partid
         inner join dcs.company d
            on d.id = a.storagecompanyid
         inner join dcs.salesunitaffiwarehouse e
            on e.warehouseid = b.id
         inner join dcs.salesunit f
            on f.id = e.salesunitid
         left join dcs.partssalescategory psc on f.partssalescategoryid = psc.id
         left join dcs.partsbranch psb on psb.partssalescategoryid = f.partssalescategoryid and psb.partid = c.id and psb.status=1
          left join dcs.keyvalueitem kvi on kvi.status = 1 and kvi.name = 'PartsAttribution' and kvi.key = psb.PartsAttribution
         where a.storagecompanytype in (3, 7)
         group by f.branchid,
                  d.code,
                  d.name,
                  b.id,
                  b.name,
                  c.id,
                  c.code,
                  c.name,
                  f.partssalescategoryid,
                  psc.name,kvi.value) a
  inner join dcs.branch bch
    on bch.id=a.branchid
  left join dcs.partssalesprice b
    on a.spareid = b.sparepartid
   and a.partssalescategoryid = b.partssalescategoryid
   and b.status=1
  left join (select WarehouseId, PartId, sum(LockedQuantity) as lockedqty
               from dcs.PartsLockedStock
              group by WarehouseId, PartId) c
    on c.warehouseid = a.id
   and c.PartId = a.spareid
 where a.qty <> 0

union all 

select a.BranchId,
       bch.code as BranchCode,
       a.Code,
       a.Name,
       a.Id,
       a.WarehouseName,
       a.SpareId,
       a.SpareCode,
       a.SpareName,
       a.Qty,
       a.qty - nvl(c.lockedqty, 0) as UsableQty,
       nvl(b.salesprice, 0) as Saleprice,
       a.qty * nvl(b.salesprice, 0) as Totalprice,
       a.partssalescategoryid,
       a.partssalescategoryname,a.value as 配件所属分类
  from (select f.branchid,
               d.code,
               d.name,
               b.id,
               b.name warehousename,
               c.id spareid,
               c.code sparecode,
               c.name sparename,
               sum(a.quantity) qty,
               f.partssalescategoryid,
               psc.name as partssalescategoryname,kvi.value
          from yxdcs.partsstock a
         inner join yxdcs.warehouse b
            on a.warehouseid = b.id
         inner join yxdcs.sparepart c
            on c.id = a.partid
         inner join yxdcs.company d
            on d.id = a.storagecompanyid
         inner join yxdcs.salesunitaffiwarehouse e
            on e.warehouseid = b.id
         inner join yxdcs.salesunit f
            on f.id = e.salesunitid
         left join yxdcs.partssalescategory psc on f.partssalescategoryid = psc.id
         left join yxdcs.partsbranch psb on psb.partssalescategoryid = f.partssalescategoryid and psb.partid = c.id and psb.status=1
          left join yxdcs.keyvalueitem kvi on kvi.status = 1 and kvi.name = 'PartsAttribution' and kvi.key = psb.PartsAttribution
         where a.storagecompanytype in (3, 7)
         group by f.branchid,
                  d.code,
                  d.name,
                  b.id,
                  b.name,
                  c.id,
                  c.code,
                  c.name,
                  f.partssalescategoryid,
                  psc.name,kvi.value) a
  inner join yxdcs.branch bch
    on bch.id=a.branchid
  left join yxdcs.partssalesprice b
    on a.spareid = b.sparepartid
   and a.partssalescategoryid = b.partssalescategoryid
      and b.status=1
  left join (select WarehouseId, PartId, sum(LockedQuantity) as lockedqty
               from yxdcs.PartsLockedStock
              group by WarehouseId, PartId) c
    on c.warehouseid = a.id
   and c.PartId = a.spareid
 where a.qty <> 0 
 union all 
select a.BranchId,
       bch.code as BranchCode,
       a.Code,
       a.Name,
       a.Id,
       a.WarehouseName,
       a.SpareId,
       a.SpareCode,
       a.SpareName,
       a.Qty,
       a.qty - nvl(c.lockedqty, 0) as UsableQty,
       nvl(b.salesprice, 0) as Saleprice,
       a.qty * nvl(b.salesprice, 0) as Totalprice,
       a.partssalescategoryid,
       a.partssalescategoryname,a.value as 配件所属分类
  from (select f.branchid,
               d.code,
               d.name,
               b.id,
               b.name warehousename,
               c.id spareid,
               c.code sparecode,
               c.name sparename,
               sum(a.quantity) qty,
               f.partssalescategoryid,
               psc.name as partssalescategoryname,kvi.value
          from gcdcs.partsstock a
         inner join gcdcs.warehouse b
            on a.warehouseid = b.id
         inner join gcdcs.sparepart c
            on c.id = a.partid
         inner join gcdcs.company d
            on d.id = a.storagecompanyid
         inner join gcdcs.salesunitaffiwarehouse e
            on e.warehouseid = b.id
         inner join gcdcs.salesunit f
            on f.id = e.salesunitid
         left join gcdcs.partssalescategory psc on f.partssalescategoryid = psc.id
         left join gcdcs.partsbranch psb on psb.partssalescategoryid = f.partssalescategoryid and psb.partid = c.id and psb.status=1
          left join gcdcs.keyvalueitem kvi on kvi.status = 1 and kvi.name = 'PartsAttribution' and kvi.key = psb.PartsAttribution
         where a.storagecompanytype in (3, 7)
         group by f.branchid,
                  d.code,
                  d.name,
                  b.id,
                  b.name,
                  c.id,
                  c.code,
                  c.name,
                  f.partssalescategoryid,
                  psc.name,kvi.value) a
  inner join gcdcs.branch bch
    on bch.id=a.branchid
  left join gcdcs.partssalesprice b
    on a.spareid = b.sparepartid
   and a.partssalescategoryid = b.partssalescategoryid
      and b.status=1
  left join (select WarehouseId, PartId, sum(LockedQuantity) as lockedqty
               from gcdcs.PartsLockedStock
              group by WarehouseId, PartId) c
    on c.warehouseid = a.id
   and c.PartId = a.spareid
 where a.qty <> 0 
 union all 
select a.BranchId,
       bch.code as BranchCode,
       a.Code,
       a.Name,
       a.Id,
       a.WarehouseName,
       a.SpareId,
       a.SpareCode,
       a.SpareName,
       a.Qty,
       a.qty - nvl(c.lockedqty, 0) as UsableQty,
       nvl(b.salesprice, 0) as Saleprice,
       a.qty * nvl(b.salesprice, 0) as Totalprice,
       a.partssalescategoryid,
       a.partssalescategoryname,a.value as 配件所属分类
  from (select f.branchid,
               d.code,
               d.name,
               b.id,
               b.name warehousename,
               c.id spareid,
               c.code sparecode,
               c.name sparename,
               sum(a.quantity) qty,
               f.partssalescategoryid,
               psc.name as partssalescategoryname,kvi.value
          from sddcs.partsstock a
         inner join sddcs.warehouse b
            on a.warehouseid = b.id
         inner join sddcs.sparepart c
            on c.id = a.partid
         inner join sddcs.company d
            on d.id = a.storagecompanyid
         inner join sddcs.salesunitaffiwarehouse e
            on e.warehouseid = b.id
         inner join sddcs.salesunit f
            on f.id = e.salesunitid
         left join sddcs.partssalescategory psc on f.partssalescategoryid = psc.id
          left join sddcs.partsbranch psb on psb.partssalescategoryid = f.partssalescategoryid and psb.partid = c.id and psb.status=1
          left join sddcs.keyvalueitem kvi on kvi.status = 1 and kvi.name = 'PartsAttribution' and kvi.key = psb.PartsAttribution
         where a.storagecompanytype in (3, 7)
         group by f.branchid,
                  d.code,
                  d.name,
                  b.id,
                  b.name,
                  c.id,
                  c.code,
                  c.name,
                  f.partssalescategoryid,
                  psc.name,kvi.value) a
  inner join sddcs.branch bch
    on bch.id=a.branchid
  left join sddcs.partssalesprice b
    on a.spareid = b.sparepartid
   and a.partssalescategoryid = b.partssalescategoryid
      and b.status=1
  left join (select WarehouseId, PartId, sum(LockedQuantity) as lockedqty
               from sddcs.PartsLockedStock
              group by WarehouseId, PartId) c
    on c.warehouseid = a.id
   and c.PartId = a.spareid
 where a.qty <> 0 