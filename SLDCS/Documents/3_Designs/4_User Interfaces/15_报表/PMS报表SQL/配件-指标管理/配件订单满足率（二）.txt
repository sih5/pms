select a.branchcode,
       a.CategoryID,
       a.Createtime,
       a.CategoryName,
       a.SubmitCompanyCode,
       a.SubmitCompanyName,
       a.PartsSalesOrdertypeID,
       a.PartsSalesOrdertypeName,
       a.OrderCode,
       a.CompanyName,
       a.SubmitTime,
       a.ApproveTime,
       a.PZS,
       a.SQS,
       nvl(a.ConfirmPz, 0) as ConfirmPz,
       nvl(a.ConfirmQty, 0) as ConfirmQty,
       a.ConfirmPrice,
       case  a.pzs when 0 then '/' else  round(nvl(a.confirmpz, 0) / a.pzs, 4) * 100 || '%' end as PZRate,
       case a.SQS when 0 then '/' else round(nvl(a.confirmQty, 0) / a.SQS, 4) * 100 || '%' end as QtyRate,
       case a.TotalPrice when 0 then '/' else round(nvl(a.ConfirmPrice, 0) / a.TotalPrice, 4) * 100 || '%' end as PriceRate
  from (select a.branchcode,
               b.id as CategoryID,
               b.name as CategoryName,
               a.SubmitCompanyCode,
               a.SubmitCompanyName,
               a.PartsSalesOrdertypeid,
               a.partssalesordertypename,
               a.code as OrderCode,
               company.name as CompanyName,
               a.SubmitTime,
               a.ApproveTime,
               a.Createtime,
               (select count(*) as pzs
                  from dcs.partssalesorder
                 inner join dcs.partssalesorderdetail b on partssalesorder.id =
                                                       b.partssalesorderid
                 where partssalesorder.id = a.id
                 group by partssalesorder.id) PZS,
               (select sum(b.orderedquantity) as SQS
                  from dcs.partssalesorder
                 inner join dcs.partssalesorderdetail b on partssalesorder.id =
                                                       b.partssalesorderid
                 where partssalesorder.id = a.id
                 group by partssalesorder.id) SQS,
               a.totalamount,
               (select count(*) as ConfirmPZ
                  from dcs.partssalesorder
                 inner join dcs.partssalesorderdetail b on partssalesorder.id =
                                                       b.partssalesorderid
                 where b.approvequantity > 0
                   and partssalesorder.id = a.id
                 group by partssalesorder.id) ConfirmPZ,
               (select sum(nvl(b.approvequantity, 0)) as ConfirmQty
                  from dcs.partssalesorder
                 inner join dcs.partssalesorderdetail b on partssalesorder.id =
                                                       b.partssalesorderid
                 where partssalesorder.id = a.id
                 group by partssalesorder.id) ConfirmQty,
               (select sum(nvl(b.approvequantity, 0) * b.orderprice) as ConfirmPrice
                  from dcs.partssalesorder
                 inner join dcs.partssalesorderdetail b on partssalesorder.id =
                                                       b.partssalesorderid
                 where partssalesorder.id = a.id
                 group by partssalesorder.id) ConfirmPrice,
               (select sum(nvl(b.orderedquantity, 0) * b.orderprice) as ConfirmPrice
                  from dcs.partssalesorder
                 inner join dcs.partssalesorderdetail b on partssalesorder.id =
                                                       b.partssalesorderid
                 where partssalesorder.id = a.id
                 group by partssalesorder.id) TotalPrice
          from dcs.partssalesorder a
         inner join dcs.partssalescategory b on b.id = a.salescategoryid
         inner join dcs.salesunit d on d.id = a.salesunitid
         inner join dcs.company on company.id = d.ownercompanyid
         inner join dcs.partssalesorderdetail c on a.id = c.partssalesorderid
         where a.status not in (1, 3, 99)) a
 group by a.branchcode,
          a.CategoryID,
          a.Createtime,
          a.CategoryName,
          a.SubmitCompanyCode,
          a.SubmitCompanyName,
          a.PartsSalesOrdertypeID,
          a.PartsSalesOrdertypeName,
          a.OrderCode,
          a.CompanyName,
          a.SubmitTime,
          a.ApproveTime,
          a.PZS,
          a.SQS,
          a.confirmpz,
          a.confirmqty,
          a.confirmprice,
          case  a.pzs when 0 then '/' else  round(nvl(a.confirmpz, 0) / a.pzs, 4) * 100 || '%' end,
          case a.SQS when 0 then '/' else round(nvl(a.confirmQty, 0) / a.SQS, 4) * 100 || '%' end,
          case a.TotalPrice when 0 then '/' else round(nvl(a.ConfirmPrice, 0) / a.TotalPrice, 4) * 100 || '%' end
          union all
          select a.branchcode,
       a.CategoryID,
       a.Createtime,
       a.CategoryName,
       a.SubmitCompanyCode,
       a.SubmitCompanyName,
       a.PartsSalesOrdertypeID,
       a.PartsSalesOrdertypeName,
       a.OrderCode,
       a.CompanyName,
       a.SubmitTime,
       a.ApproveTime,
       a.PZS,
       a.SQS,
       nvl(a.ConfirmPz, 0) as ConfirmPz,
       nvl(a.ConfirmQty, 0) as ConfirmQty,
       a.ConfirmPrice,
       case a.pzs when 0 then '/' else  round(nvl(a.confirmpz, 0) / a.pzs, 4) * 100 || '%' end as PZRate,
       case a.SQS when 0 then '/' else round(nvl(a.confirmQty, 0) / a.SQS, 4) * 100 || '%' end as QtyRate,
      case a.TotalPrice when 0 then '/' else round(nvl(a.ConfirmPrice, 0) / a.TotalPrice, 4) * 100 || '%' end as PriceRate
  from (select a.branchcode,
               b.id as CategoryID,
               b.name as CategoryName,
               a.SubmitCompanyCode,
               a.SubmitCompanyName,
               a.PartsSalesOrdertypeid,
               a.partssalesordertypename,
               a.code as OrderCode,
               company.name as CompanyName,
               a.SubmitTime,
               a.ApproveTime,
               a.Createtime,
               (select count(*) as pzs
                  from sddcs.partssalesorder
                 inner join sddcs.partssalesorderdetail b on partssalesorder.id =
                                                       b.partssalesorderid
                 where partssalesorder.id = a.id
                 group by partssalesorder.id) PZS,
               (select sum(b.orderedquantity) as SQS
                  from sddcs.partssalesorder
                 inner join sddcs.partssalesorderdetail b on partssalesorder.id =
                                                       b.partssalesorderid
                 where partssalesorder.id = a.id
                 group by partssalesorder.id) SQS,
               a.totalamount,
               (select count(*) as ConfirmPZ
                  from sddcs.partssalesorder
                 inner join sddcs.partssalesorderdetail b on partssalesorder.id =
                                                       b.partssalesorderid
                 where b.approvequantity > 0
                   and partssalesorder.id = a.id
                 group by partssalesorder.id) ConfirmPZ,
               (select sum(nvl(b.approvequantity, 0)) as ConfirmQty
                  from sddcs.partssalesorder
                 inner join sddcs.partssalesorderdetail b on partssalesorder.id =
                                                       b.partssalesorderid
                 where partssalesorder.id = a.id
                 group by partssalesorder.id) ConfirmQty,
               (select sum(nvl(b.approvequantity, 0) * b.orderprice) as ConfirmPrice
                  from sddcs.partssalesorder
                 inner join sddcs.partssalesorderdetail b on partssalesorder.id =
                                                       b.partssalesorderid
                 where partssalesorder.id = a.id
                 group by partssalesorder.id) ConfirmPrice,
               (select sum(nvl(b.orderedquantity, 0) * b.orderprice) as ConfirmPrice
                  from sddcs.partssalesorder
                 inner join sddcs.partssalesorderdetail b on partssalesorder.id =
                                                       b.partssalesorderid
                 where partssalesorder.id = a.id
                 group by partssalesorder.id) TotalPrice
          from sddcs.partssalesorder a
         inner join sddcs.partssalescategory b on b.id = a.salescategoryid
         inner join sddcs.salesunit d on d.id = a.salesunitid
         inner join sddcs.company on company.id = d.ownercompanyid
         inner join sddcs.partssalesorderdetail c on a.id = c.partssalesorderid
         where a.status not in (1, 3, 99)) a
 group by a.branchcode,
          a.CategoryID,
          a.Createtime,
          a.CategoryName,
          a.SubmitCompanyCode,
          a.SubmitCompanyName,
          a.PartsSalesOrdertypeID,
          a.PartsSalesOrdertypeName,
          a.OrderCode,
          a.CompanyName,
          a.SubmitTime,
          a.ApproveTime,
          a.PZS,
          a.SQS,
          a.confirmpz,
          a.confirmqty,
          a.confirmprice,
          case  a.pzs when 0 then '/' else  round(nvl(a.confirmpz, 0) / a.pzs, 4) * 100 || '%' end,
          case a.SQS when 0 then '/' else round(nvl(a.confirmQty, 0) / a.SQS, 4) * 100 || '%' end,
          case a.TotalPrice when 0 then '/' else round(nvl(a.ConfirmPrice, 0) / a.TotalPrice, 4) * 100 || '%' end
