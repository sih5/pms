select partssalescategory.name 品牌,
       sparepart.code 配件图号,
       sparepart.name 配件名称,
       '采购价' 价格类型,
       1 as pricetype,
       partssupplier.code 供应商编号,
       partssupplier.name 供应商名称,
       partspurchasepricing.purchaseprice 价格,
       partspurchasepricing.validfrom 生效日期,
       partspurchasepricing.validto 失效日期,
       partspurchasepricing.createtime 创建时间,
       partspurchasepricing.creatorname 创建人,
       partspurchasepricing.modifytime 修改时间,
       partspurchasepricing.modifiername 修改人
  from yxdcs.partspurchasepricing
 inner join yxdcs.sparepart
    on sparepart.id = partspurchasepricing.partid
 inner join yxdcs.partssalescategory
    on partssalescategory.id = partspurchasepricing.partssalescategoryid
 inner join yxdcs.partssupplier
    on partssupplier.id = partspurchasepricing.partssupplierid
where partspurchasepricing.createtime between @Prompt('CreateTimeBegin', 'D',, Mono, Free, Persistent) and @Prompt('CreateTimeEnd', 'D',, Mono, Free, Persistent)
union
select partssalescategory.name 品牌,
       sparepart.code 配件图号,
       sparepart.name 配件名称,
       '计划价',
       2                              as pricetype,
       '',
       '',
       partsplannedprice.plannedprice,
       null,
       null,
       partsplannedprice.createtime,
       partsplannedprice.creatorname,
       partsplannedprice.modifytime,
       partsplannedprice.modifiername
  from yxdcs.partsplannedprice
 inner join yxdcs.sparepart
    on sparepart.id = partsplannedprice.sparepartid
 inner join yxdcs.partssalescategory
    on partssalescategory.id = partsplannedprice.partssalescategoryid
where partsplannedprice.createtime between @Prompt('CreateTimeBegin', 'D',, Mono, Free, Persistent) and @Prompt('CreateTimeEnd', 'D',, Mono, Free, Persistent)
union
select partssalescategory.name 品牌,
       sparepart.code 配件图号,
       sparepart.name 配件名称,
       '销售价',
       3                            as pricetype,
       '',
       '',
       partssalesprice.salesprice,
       null,
       null,
       partssalesprice.createtime,
       partssalesprice.creatorname,
       partssalesprice.modifytime,
       partssalesprice.modifiername
  from yxdcs.partssalesprice
 inner join yxdcs.sparepart
    on sparepart.id = partssalesprice.sparepartid
 inner join yxdcs.partssalescategory
    on partssalescategory.id = partssalesprice.partssalescategoryid
where partssalesprice.createtime between @Prompt('CreateTimeBegin', 'D',, Mono, Free, Persistent) and @Prompt('CreateTimeEnd', 'D',, Mono, Free, Persistent)
union
select partssalescategory.name 品牌,
       sparepart.code 配件图号,
       sparepart.name 配件名称,
       '零售指导价',
       4,
       '',
       '',
       PartsRetailGuidePrice.Retailguideprice,
       null,
       null,
       PartsRetailGuidePrice.createtime,
       PartsRetailGuidePrice.creatorname,
       PartsRetailGuidePrice.modifytime,
       PartsRetailGuidePrice.modifiername
  from yxdcs.PartsRetailGuidePrice
 inner join yxdcs.sparepart
    on sparepart.id = PartsRetailGuidePrice.sparepartid
 inner join yxdcs.partssalescategory
    on partssalescategory.id = PartsRetailGuidePrice.partssalescategoryid
where PartsRetailGuidePrice.createtime between @Prompt('CreateTimeBegin', 'D',, Mono, Free, Persistent) and @Prompt('CreateTimeEnd', 'D',, Mono, Free, Persistent)
    union
--sddcs
select partssalescategory.name 品牌,
       sparepart.code 配件图号,
       sparepart.name 配件名称,
       '采购价' 价格类型,
       1 as pricetype,
       partssupplier.code 供应商编号,
       partssupplier.name 供应商名称,
       partspurchasepricing.purchaseprice 价格,
       partspurchasepricing.validfrom 生效日期,
       partspurchasepricing.validto 失效日期,
       partspurchasepricing.createtime 创建时间,
       partspurchasepricing.creatorname 创建人,
       partspurchasepricing.modifytime 修改时间,
       partspurchasepricing.modifiername 修改人
  from sddcs.partspurchasepricing
 inner join sddcs.sparepart
    on sparepart.id = partspurchasepricing.partid
 inner join sddcs.partssalescategory
    on partssalescategory.id = partspurchasepricing.partssalescategoryid
 inner join sddcs.partssupplier
    on partssupplier.id = partspurchasepricing.partssupplierid
where partspurchasepricing.createtime between @Prompt('CreateTimeBegin', 'D',, Mono, Free, Persistent) and @Prompt('CreateTimeEnd', 'D',, Mono, Free, Persistent)
union
select partssalescategory.name 品牌,
       sparepart.code 配件图号,
       sparepart.name 配件名称,
       '计划价',
       2                              as pricetype,
       '',
       '',
       partsplannedprice.plannedprice,
       null,
       null,
       partsplannedprice.createtime,
       partsplannedprice.creatorname,
       partsplannedprice.modifytime,
       partsplannedprice.modifiername
  from sddcs.partsplannedprice
 inner join sddcs.sparepart
    on sparepart.id = partsplannedprice.sparepartid
 inner join sddcs.partssalescategory
    on partssalescategory.id = partsplannedprice.partssalescategoryid
where partsplannedprice.createtime between @Prompt('CreateTimeBegin', 'D',, Mono, Free, Persistent) and @Prompt('CreateTimeEnd', 'D',, Mono, Free, Persistent)
union
select partssalescategory.name 品牌,
       sparepart.code 配件图号,
       sparepart.name 配件名称,
       '销售价',
       3                            as pricetype,
       '',
       '',
       partssalesprice.salesprice,
       null,
       null,
       partssalesprice.createtime,
       partssalesprice.creatorname,
       partssalesprice.modifytime,
       partssalesprice.modifiername
  from sddcs.partssalesprice
 inner join sddcs.sparepart
    on sparepart.id = partssalesprice.sparepartid
 inner join sddcs.partssalescategory
    on partssalescategory.id = partssalesprice.partssalescategoryid
where partssalesprice.createtime between @Prompt('CreateTimeBegin', 'D',, Mono, Free, Persistent) and @Prompt('CreateTimeEnd', 'D',, Mono, Free, Persistent)
union
select partssalescategory.name 品牌,
       sparepart.code 配件图号,
       sparepart.name 配件名称,
       '零售指导价',
       4,
       '',
       '',
       PartsRetailGuidePrice.Retailguideprice,
       null,
       null,
       PartsRetailGuidePrice.createtime,
       PartsRetailGuidePrice.creatorname,
       PartsRetailGuidePrice.modifytime,
       PartsRetailGuidePrice.modifiername
  from sddcs.PartsRetailGuidePrice
 inner join sddcs.sparepart
    on sparepart.id = PartsRetailGuidePrice.sparepartid
 inner join sddcs.partssalescategory
    on partssalescategory.id = PartsRetailGuidePrice.partssalescategoryid
where PartsRetailGuidePrice.createtime between @Prompt('CreateTimeBegin', 'D',, Mono, Free, Persistent) and @Prompt('CreateTimeEnd', 'D',, Mono, Free, Persistent)