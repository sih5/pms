select abc.partcode,abc.partcode as 配件图号,abc.partname,abc.partname as 配件名称,abc.ReferenceCode as 零部件图号,
       abc.CADCode as 研究院图号,abc.partssalescategoryid,abc.partssalescategoryname as 品牌,abc.Specification as 规格型号,
       abc.MeasureUnit as 计量单位,abc.MInPackingAmount as 最小包装数量,abc.Weight as 重量,abc.Volume as 体积,
       abc.Feature as 特征说明,abc.creatorname as 创建人,abc.createtime as 创建时间,
       abc.ModifierName as 修改人,abc.ModifyTime as 修改时间,abc.AbandonerName as 作废人,abc.AbandonTime as 作废时间
         from (
               select pb.partcode,pb.partname,sp.ReferenceCode,
                 sp.CADCode,pb.partssalescategoryid,pb.partssalescategoryname,sp.Specification,
                 sp.MeasureUnit,sp.MInPackingAmount,sp.Weight,sp.Volume,
                 sp.Feature,sp.creatorname,sp.createtime,
                 sp.ModifierName,sp.ModifyTime,sp.AbandonerName,sp.AbandonTime
                     from gcdcs.SparePart sp
                     left join gcdcs.PartsBranch pb on sp.code = pb.partcode
              union all 
                select pb.partcode,pb.partname,sp.ReferenceCode,
                 sp.CADCode,pb.partssalescategoryid,pb.partssalescategoryname,sp.Specification,
                 sp.MeasureUnit,sp.MInPackingAmount,sp.Weight,sp.Volume,
                 sp.Feature,sp.creatorname,sp.createtime,
                 sp.ModifierName,sp.ModifyTime,sp.AbandonerName,sp.AbandonTime
                     from dcs.SparePart sp
                     left join dcs.PartsBranch pb on sp.code = pb.partcode
              union all 
                select pb.partcode,pb.partname,sp.ReferenceCode,
                 sp.CADCode,pb.partssalescategoryid,pb.partssalescategoryname,sp.Specification,
                 sp.MeasureUnit,sp.MInPackingAmount,sp.Weight,sp.Volume,
                 sp.Feature,sp.creatorname,sp.createtime,
                 sp.ModifierName,sp.ModifyTime,sp.AbandonerName,sp.AbandonTime
                     from yxdcs.SparePart sp
                     left join yxdcs.PartsBranch pb on sp.code = pb.partcode
              union all 
                select pb.partcode,pb.partname,sp.ReferenceCode,
                 sp.CADCode,pb.partssalescategoryid,pb.partssalescategoryname,sp.Specification,
                 sp.MeasureUnit,sp.MInPackingAmount,sp.Weight,sp.Volume,
                 sp.Feature,sp.creatorname,sp.createtime,
                 sp.ModifierName,sp.ModifyTime,sp.AbandonerName,sp.AbandonTime
                     from sddcs.SparePart sp
                     left join sddcs.PartsBranch pb on sp.code = pb.partcode
              ) abc
         where abc.partcode in (
               select t.partcode from
    (
      select partcode,MInPackingAmount,row_number() over(partition by  partcode,MInPackingAmount order by partcode,MInPackingAmount desc) as rowno from 
      (
       select distinct abc.partcode,abc.MInPackingAmount
            from      
              (
               select pb.partcode,sp.MInPackingAmount,pb.partssalescategoryid
                     from gcdcs.SparePart sp
                     left join gcdcs.PartsBranch pb on sp.code = pb.partcode
              union all 
                select pb.partcode,sp.MInPackingAmount,pb.partssalescategoryid
                     from dcs.SparePart sp
                     left join dcs.PartsBranch pb on sp.code = pb.partcode
              union all 
                select pb.partcode,sp.MInPackingAmount,pb.partssalescategoryid
                     from yxdcs.SparePart sp
                     left join yxdcs.PartsBranch pb on sp.code = pb.partcode
              union all 
                select pb.partcode,sp.MInPackingAmount,pb.partssalescategoryid
                     from sddcs.SparePart sp
                     left join sddcs.PartsBranch pb on sp.code = pb.partcode
              ) abc
               where abc.partcode in 
               (select abc1.partcode from 
                 (
                 select pb.partcode
                   from gcdcs.SparePart sp
                   left join gcdcs.PartsBranch pb on sp.code = pb.partcode
                   group by pb.partcode
                   having count(pb.partcode) > 1
                union all 
                 select pb.partcode 
                   from dcs.SparePart sp
                   left join dcs.PartsBranch pb on sp.code = pb.partcode
                   group by pb.partcode
                   having count(pb.partcode) > 1
                union all
                 select pb.partcode 
                   from yxdcs.SparePart sp
                   left join yxdcs.PartsBranch pb on sp.code = pb.partcode
                   group by pb.partcode
                   having count(pb.partcode) > 1
                union all 
                   select pb.partcode 
                   from sddcs.SparePart sp
                   left join sddcs.PartsBranch pb on sp.code = pb.partcode
                   group by pb.partcode
                   having count(pb.partcode) > 1
                 ) abc1 group by abc1.partcode)
               --and abc.partcode = 'FF1305315X0003A0664'  
       ) )  t where t.rowno>1
         )
              