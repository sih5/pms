--dcs
select abc.OrderType,
       abc.branchcode,
       abc.branchname,
       abc.PartOrderCode,
       abc.submitcompanycode,
       abc.submitcompanyname,
       abc.createtime,
       abc.OutPlanCode,
       abc.OutCode,
       abc.OutboundType,
       abc.OutboundTypeValue,
       abc.Outcreatorname,
       abc.PartsShippingOrderCode,
       abc.pspocreatorname,
       abc.pspocreatetime,
       abc.popCreateTime,
       abc.StorageCompanyCode,
       abc.storagecompanyname,
       abc.pobCreatetime,
       abc.requestedarrivaldate,
       abc.ConfirmedReceptionTime,
       abc.PartsSalesCategoryId,
       abc.PartsSalesCategoryName,
       abc.SubmitTime,
       abc.approvetime
  from (select PartOrder.OrderType,
               PartOrder.branchcode,
               PartOrder.branchname,
               PartOrder.code as PartOrderCode,
               PartOrder.submitcompanycode,
               PartOrder.submitcompanyname,
               PartOrder.createtime,
               pop.code as OutPlanCode,
               pop.CreateTime as popCreateTime,
               pop.StorageCompanyCode,
               pop.storagecompanyname,
               pob.createtime as pobCreatetime,
               pob.code as OutCode,
               pob.OutboundType,
               decode(pob.OutboundType,
                      1,
                      '配件销售',
                      2,
                      '采购退货',
                      3,
                      '内部领出',
                      4,
                      '配件调拨',
                      5,
                      '积压件调剂',
                      6,
                      '配件零售') OutboundTypeValue,
               
               pob.creatorname             Outcreatorname,
               pspo.code                   as PartsShippingOrderCode,
               pspo.creatorname            as pspocreatorname,
               pspo.ShippingDate           as pspocreatetime,pspo.requestedarrivaldate,
               pspo.ConfirmedReceptionTime,
               PartOrder.partssalescategoryid,
               PartOrder.PartsSalesCategoryName,
               PartOrder.SubmitTime,
               PartOrder.approvetime
          from (select pso.PartsSalesOrderTypeName as OrderType,
                       pso.branchcode,
                       pso.branchname,
                       pso.code,
                       pso.submitcompanycode,
                       pso.submitcompanyname,
                       pso.createtime,
                       pso.SalesCategoryId as PartsSalesCategoryId,    --品牌ID
                       pso.SalesCategoryName as PartsSalesCategoryName, --品牌
                       pso.SubmitTime,    --订单提交时间
                       pso.ApproveTime    --订单审批时间
                  from dcs.PartsSalesOrder pso --销售订单
                union all
                select null as OrderType,
                       pro.branchcode,
                       pro.branchname,
                       pro.code,
                       pro.branchcode as submitcompanycode,
                       pro.branchname as submitcompanyname,
                       pro.createtime,
                       pro.PartsSalesCategoryId,
                       pro.PartsSalesCategoryName,
                       null as SubmitTime,
                       pro.ApproveTime
                  from dcs.Partspurreturnorder pro --采购退货单
                union all
                select null as OrderType,
                       branch.code as branchcode,
                       iab.branchname,
                       iab.code,
                       iab.DepartmentName as submitcompanycode,
                       iab.Departmentname as submitcompanyname,
                       iab.createtime,
                       su.partssalescategoryid,
                       psc.name as PartsSalesCategoryName,
                       null as SubmitTime,
                       iab.ApproveTime
                  from dcs.Internalallocationbill iab --内部领出单
                  left join dcs.SalesUnitAffiWarehouse suaw on iab.warehouseid = suaw.warehouseid
                  left join dcs.salesunit su on suaw.salesunitid = su.id
                  left join dcs.partssalescategory psc on su.partssalescategoryid = psc.id
                 inner join dcs.branch
                    on branch.id = iab.branchid
                union all
                select null as OrderType,
                       branch.code,
                       branch.name,
                       pto.code,
                       pto.DestWarehouseCode as submitcompanycode,
                       pto.destwarehousename as submitcompanyname,
                       pto.createtime,
                       su.partssalescategoryid,
                       psc.name as PartsSalesCategoryName,
                       null as SubmitTime,
                       pto.ApproveTime
                  from dcs.PartsTransferOrder pto --配件调拨单
                  left join dcs.SalesUnitAffiWarehouse suaw on pto.DestWarehouseId = suaw.warehouseid
                  left join dcs.salesunit su on suaw.salesunitid = su.id
                  left join dcs.partssalescategory psc on su.partssalescategoryid = psc.id
                 inner join dcs.branch
                    on pto.storagecompanyid = branch.id
                 where pto.StorageCompanyType = 1
                union all
                select null as OrderType,
                       opab.branchcode,
                       opab.branchname,
                       opab.code,
                       opab.DestStorageCompanyCode as submitcompanycode,
                       opab.deststoragecompanyname as submitcompanyname,
                       opab.createtime,
                       su.partssalescategoryid,
                       psc.name as PartsSalesCategoryName,
                       null as SubmitTime,
                       opab.ApproveTime
                  from dcs.OverstockPartsAdjustBill opab --积压件调剂单
                  left join dcs.SalesUnitAffiWarehouse suaw on opab.DestStorageCompanyId = suaw.warehouseid
                  left join dcs.salesunit su on suaw.salesunitid = su.id
                  left join dcs.partssalescategory psc on su.partssalescategoryid = psc.id
                union all
                select null as OrderType,
                       psro.branchcode,
                       psro.branchname,
                       psro.code,
                       psro.RetailCustomerCompanyCode as submitcompanycode,
                       psro.RetailCustomerCompanyName as submitcompanyname,
                       psro.createtime,
                       su.partssalescategoryid,
                       psc.name as PartsSalesCategoryName,
                       null as SubmitTime,
                       psro.approvetime
                  from dcs.PartsRetailOrder psro
                  left join dcs.SalesUnitAffiWarehouse suaw on psro.warehouseid = suaw.warehouseid
                  left join dcs.salesunit su on suaw.salesunitid = su.id
                  left join dcs.partssalescategory psc on su.partssalescategoryid = psc.id
                  ) PartOrder --配件零售订单
         inner join dcs.PartsOutboundPlan pop
            on PartOrder.code = pop.sourcecode
         inner join dcs.partsoutboundbill pob
            on pob.partsoutboundplanid = pop.id
         inner join dcs.Partsshippingorderref pr
            on pr.partsoutboundbillid = pob.id
         inner join dcs.PartsShippingOrder pspo
            on pspo.Originalrequirementbillcode = PartOrder.code
           and pr.partsshippingorderid = pspo.id
         group by PartOrder.OrderType,
                  PartOrder.branchcode,
                  PartOrder.branchname,
                  PartOrder.code,
                  PartOrder.submitcompanycode,
                  PartOrder.submitcompanyname,
                  PartOrder.createtime,
                  PartOrder.partssalescategoryid,
                  PartOrder.PartsSalesCategoryName,
                  PartOrder.SubmitTime,
                  PartOrder.approvetime,
                  pop.code,
                  pop.CreateTime,
                  pop.StorageCompanyCode,
                  pop.storagecompanyname,
                  pob.createtime,
                  pob.code,
                  pob.OutboundType,
                  pob.creatorname,
                  pspo.code,
                  pspo.creatorname,
                  pspo.ShippingDate,pspo.requestedarrivaldate,
                  pspo.ConfirmedReceptionTime) abc
union all --yxdcs
select abc.OrderType,
       abc.branchcode,
       abc.branchname,
       abc.PartOrderCode,
       abc.submitcompanycode,
       abc.submitcompanyname,
       abc.createtime,
       abc.OutPlanCode,
       abc.OutCode,
       abc.OutboundType,
       abc.OutboundTypeValue,
       abc.Outcreatorname,
       abc.PartsShippingOrderCode,
       abc.pspocreatorname,
       abc.pspocreatetime,
       abc.popCreateTime,
       abc.StorageCompanyCode,
       abc.storagecompanyname,
       abc.pobCreatetime,
       abc.requestedarrivaldate,
       abc.ConfirmedReceptionTime,
       abc.PartsSalesCategoryId,
       abc.PartsSalesCategoryName,
       abc.SubmitTime,
       abc.approvetime
  from (select PartOrder.OrderType,
               PartOrder.branchcode,
               PartOrder.branchname,
               PartOrder.code as PartOrderCode,
               PartOrder.submitcompanycode,
               PartOrder.submitcompanyname,
               PartOrder.createtime,
               pop.code as OutPlanCode,
               pop.CreateTime as popCreateTime,
               pop.StorageCompanyCode,
               pop.storagecompanyname,
               pob.createtime as pobCreatetime,
               pob.code as OutCode,
               pob.OutboundType,
               decode(pob.OutboundType,
                      1,
                      '配件销售',
                      2,
                      '采购退货',
                      3,
                      '内部领出',
                      4,
                      '配件调拨',
                      5,
                      '积压件调剂',
                      6,
                      '配件零售') OutboundTypeValue,
               
               pob.creatorname             Outcreatorname,
               pspo.code                   as PartsShippingOrderCode,
               pspo.creatorname            as pspocreatorname,
               pspo.ShippingDate           as pspocreatetime,pspo.requestedarrivaldate,
               pspo.ConfirmedReceptionTime,
               PartOrder.partssalescategoryid,
               PartOrder.PartsSalesCategoryName,
               PartOrder.SubmitTime,
               PartOrder.approvetime
          from (select pso.PartsSalesOrderTypeName as OrderType,
                       pso.branchcode,
                       pso.branchname,
                       pso.code,
                       pso.submitcompanycode,
                       pso.submitcompanyname,
                       pso.createtime,
                       pso.SalesCategoryId as PartsSalesCategoryId,    --品牌ID
                       pso.SalesCategoryName as PartsSalesCategoryName, --品牌
                       pso.SubmitTime,    --订单提交时间
                       pso.ApproveTime    --订单审批时间
                  from yxdcs.PartsSalesOrder pso --销售订单
                union all
                select null as OrderType,
                       pro.branchcode,
                       pro.branchname,
                       pro.code,
                       pro.branchcode as submitcompanycode,
                       pro.branchname as submitcompanyname,
                       pro.createtime,
                       pro.PartsSalesCategoryId,
                       pro.PartsSalesCategoryName,
                       null as SubmitTime,
                       pro.ApproveTime
                  from yxdcs.Partspurreturnorder pro --采购退货单
                union all
                select null as OrderType,
                       branch.code as branchcode,
                       iab.branchname,
                       iab.code,
                       iab.DepartmentName as submitcompanycode,
                       iab.Departmentname as submitcompanyname,
                       iab.createtime,
                       su.partssalescategoryid,
                       psc.name as PartsSalesCategoryName,
                       null as SubmitTime,
                       iab.ApproveTime
                  from yxdcs.Internalallocationbill iab --内部领出单
                  left join yxdcs.SalesUnitAffiWarehouse suaw on iab.warehouseid = suaw.warehouseid
                  left join yxdcs.salesunit su on suaw.salesunitid = su.id
                  left join yxdcs.partssalescategory psc on su.partssalescategoryid = psc.id
                 inner join yxdcs.branch
                    on branch.id = iab.branchid
                union all
                select null as OrderType,
                       branch.code,
                       branch.name,
                       pto.code,
                       pto.DestWarehouseCode as submitcompanycode,
                       pto.destwarehousename as submitcompanyname,
                       pto.createtime,
                       su.partssalescategoryid,
                       psc.name as PartsSalesCategoryName,
                       null as SubmitTime,
                       pto.ApproveTime
                  from yxdcs.PartsTransferOrder pto --配件调拨单
                  left join yxdcs.SalesUnitAffiWarehouse suaw on pto.DestWarehouseId = suaw.warehouseid
                  left join yxdcs.salesunit su on suaw.salesunitid = su.id
                  left join yxdcs.partssalescategory psc on su.partssalescategoryid = psc.id
                 inner join yxdcs.branch
                    on pto.storagecompanyid = branch.id
                 where pto.StorageCompanyType = 1
                union all
                select null as OrderType,
                       opab.branchcode,
                       opab.branchname,
                       opab.code,
                       opab.DestStorageCompanyCode as submitcompanycode,
                       opab.deststoragecompanyname as submitcompanyname,
                       opab.createtime,
                       su.partssalescategoryid,
                       psc.name as PartsSalesCategoryName,
                       null as SubmitTime,
                       opab.ApproveTime
                  from yxdcs.OverstockPartsAdjustBill opab --积压件调剂单
                  left join yxdcs.SalesUnitAffiWarehouse suaw on opab.DestStorageCompanyId = suaw.warehouseid
                  left join yxdcs.salesunit su on suaw.salesunitid = su.id
                  left join yxdcs.partssalescategory psc on su.partssalescategoryid = psc.id
                union all
                select null as OrderType,
                       psro.branchcode,
                       psro.branchname,
                       psro.code,
                       psro.RetailCustomerCompanyCode as submitcompanycode,
                       psro.RetailCustomerCompanyName as submitcompanyname,
                       psro.createtime,
                       su.partssalescategoryid,
                       psc.name as PartsSalesCategoryName,
                       null as SubmitTime,
                       psro.approvetime
                  from yxdcs.PartsRetailOrder psro
                  left join yxdcs.SalesUnitAffiWarehouse suaw on psro.warehouseid = suaw.warehouseid
                  left join yxdcs.salesunit su on suaw.salesunitid = su.id
                  left join yxdcs.partssalescategory psc on su.partssalescategoryid = psc.id
                  ) PartOrder --配件零售订单
         inner join yxdcs.PartsOutboundPlan pop
            on PartOrder.code = pop.sourcecode
         inner join yxdcs.partsoutboundbill pob
            on pob.partsoutboundplanid = pop.id
         inner join yxdcs.Partsshippingorderref pr
            on pr.partsoutboundbillid = pob.id
         inner join yxdcs.PartsShippingOrder pspo
            on pspo.Originalrequirementbillcode = PartOrder.code
           and pr.partsshippingorderid = pspo.id
         group by PartOrder.OrderType,
                  PartOrder.branchcode,
                  PartOrder.branchname,
                  PartOrder.code,
                  PartOrder.submitcompanycode,
                  PartOrder.submitcompanyname,
                  PartOrder.createtime,
                  PartOrder.partssalescategoryid,
                  PartOrder.PartsSalesCategoryName,
                  PartOrder.SubmitTime,
                  PartOrder.approvetime,
                  pop.code,
                  pop.CreateTime,
                  pop.StorageCompanyCode,
                  pop.storagecompanyname,
                  pob.createtime,
                  pob.code,
                  pob.OutboundType,
                  pob.creatorname,
                  pspo.code,
                  pspo.creatorname,
                  pspo.ShippingDate,pspo.requestedarrivaldate,
                  pspo.ConfirmedReceptionTime) abc

union all --gcdcs
select abc.OrderType,
       abc.branchcode,
       abc.branchname,
       abc.PartOrderCode,
       abc.submitcompanycode,
       abc.submitcompanyname,
       abc.createtime,
       abc.OutPlanCode,
       abc.OutCode,
       abc.OutboundType,
       abc.OutboundTypeValue,
       abc.Outcreatorname,
       abc.PartsShippingOrderCode,
       abc.pspocreatorname,
       abc.pspocreatetime,
       abc.popCreateTime,
       abc.StorageCompanyCode,
       abc.storagecompanyname,
       abc.pobCreatetime,
       abc.requestedarrivaldate,
       abc.ConfirmedReceptionTime,
       abc.PartsSalesCategoryId,
       abc.PartsSalesCategoryName,
       abc.SubmitTime,
       abc.approvetime
  from (select PartOrder.OrderType,
               PartOrder.branchcode,
               PartOrder.branchname,
               PartOrder.code as PartOrderCode,
               PartOrder.submitcompanycode,
               PartOrder.submitcompanyname,
               PartOrder.createtime,
               pop.code as OutPlanCode,
               pop.CreateTime as popCreateTime,
               pop.StorageCompanyCode,
               pop.storagecompanyname,
               pob.createtime as pobCreatetime,
               pob.code as OutCode,
               pob.OutboundType,
               decode(pob.OutboundType,
                      1,
                      '配件销售',
                      2,
                      '采购退货',
                      3,
                      '内部领出',
                      4,
                      '配件调拨',
                      5,
                      '积压件调剂',
                      6,
                      '配件零售') OutboundTypeValue,
               
               pob.creatorname             Outcreatorname,
               pspo.code                   as PartsShippingOrderCode,
               pspo.creatorname            as pspocreatorname,
               pspo.ShippingDate           as pspocreatetime,pspo.requestedarrivaldate,
               pspo.ConfirmedReceptionTime,
               PartOrder.partssalescategoryid,
               PartOrder.PartsSalesCategoryName,
               PartOrder.SubmitTime,
               PartOrder.approvetime
          from (select pso.PartsSalesOrderTypeName as OrderType,
                       pso.branchcode,
                       pso.branchname,
                       pso.code,
                       pso.submitcompanycode,
                       pso.submitcompanyname,
                       pso.createtime,
                       pso.SalesCategoryId as PartsSalesCategoryId,    --品牌ID
                       pso.SalesCategoryName as PartsSalesCategoryName, --品牌
                       pso.SubmitTime,    --订单提交时间
                       pso.ApproveTime    --订单审批时间
                  from gcdcs.PartsSalesOrder pso --销售订单
                union all
                select null as OrderType,
                       pro.branchcode,
                       pro.branchname,
                       pro.code,
                       pro.branchcode as submitcompanycode,
                       pro.branchname as submitcompanyname,
                       pro.createtime,
                       pro.PartsSalesCategoryId,
                       pro.PartsSalesCategoryName,
                       null as SubmitTime,
                       pro.ApproveTime
                  from gcdcs.Partspurreturnorder pro --采购退货单
                union all
                select null as OrderType,
                       branch.code as branchcode,
                       iab.branchname,
                       iab.code,
                       iab.DepartmentName as submitcompanycode,
                       iab.Departmentname as submitcompanyname,
                       iab.createtime,
                       su.partssalescategoryid,
                       psc.name as PartsSalesCategoryName,
                       null as SubmitTime,
                       iab.ApproveTime
                  from gcdcs.Internalallocationbill iab --内部领出单
                  left join gcdcs.SalesUnitAffiWarehouse suaw on iab.warehouseid = suaw.warehouseid
                  left join gcdcs.salesunit su on suaw.salesunitid = su.id
                  left join gcdcs.partssalescategory psc on su.partssalescategoryid = psc.id
                 inner join gcdcs.branch
                    on branch.id = iab.branchid
                union all
                select null as OrderType,
                       branch.code,
                       branch.name,
                       pto.code,
                       pto.DestWarehouseCode as submitcompanycode,
                       pto.destwarehousename as submitcompanyname,
                       pto.createtime,
                       su.partssalescategoryid,
                       psc.name as PartsSalesCategoryName,
                       null as SubmitTime,
                       pto.ApproveTime
                  from gcdcs.PartsTransferOrder pto --配件调拨单
                  left join gcdcs.SalesUnitAffiWarehouse suaw on pto.DestWarehouseId = suaw.warehouseid
                  left join gcdcs.salesunit su on suaw.salesunitid = su.id
                  left join gcdcs.partssalescategory psc on su.partssalescategoryid = psc.id
                 inner join gcdcs.branch
                    on pto.storagecompanyid = branch.id
                 where pto.StorageCompanyType = 1
                union all
                select null as OrderType,
                       opab.branchcode,
                       opab.branchname,
                       opab.code,
                       opab.DestStorageCompanyCode as submitcompanycode,
                       opab.deststoragecompanyname as submitcompanyname,
                       opab.createtime,
                       su.partssalescategoryid,
                       psc.name as PartsSalesCategoryName,
                       null as SubmitTime,
                       opab.ApproveTime
                  from gcdcs.OverstockPartsAdjustBill opab --积压件调剂单
                  left join gcdcs.SalesUnitAffiWarehouse suaw on opab.DestStorageCompanyId = suaw.warehouseid
                  left join gcdcs.salesunit su on suaw.salesunitid = su.id
                  left join gcdcs.partssalescategory psc on su.partssalescategoryid = psc.id
                union all
                select null as OrderType,
                       psro.branchcode,
                       psro.branchname,
                       psro.code,
                       psro.RetailCustomerCompanyCode as submitcompanycode,
                       psro.RetailCustomerCompanyName as submitcompanyname,
                       psro.createtime,
                       su.partssalescategoryid,
                       psc.name as PartsSalesCategoryName,
                       null as SubmitTime,
                       psro.approvetime
                  from gcdcs.PartsRetailOrder psro
                  left join gcdcs.SalesUnitAffiWarehouse suaw on psro.warehouseid = suaw.warehouseid
                  left join gcdcs.salesunit su on suaw.salesunitid = su.id
                  left join gcdcs.partssalescategory psc on su.partssalescategoryid = psc.id
                  ) PartOrder --配件零售订单
         inner join gcdcs.PartsOutboundPlan pop
            on PartOrder.code = pop.sourcecode
         inner join gcdcs.partsoutboundbill pob
            on pob.partsoutboundplanid = pop.id
         inner join gcdcs.Partsshippingorderref pr
            on pr.partsoutboundbillid = pob.id
         inner join gcdcs.PartsShippingOrder pspo
            on pspo.Originalrequirementbillcode = PartOrder.code
           and pr.partsshippingorderid = pspo.id
         group by PartOrder.OrderType,
                  PartOrder.branchcode,
                  PartOrder.branchname,
                  PartOrder.code,
                  PartOrder.submitcompanycode,
                  PartOrder.submitcompanyname,
                  PartOrder.createtime,
                  PartOrder.partssalescategoryid,
                  PartOrder.PartsSalesCategoryName,
                  PartOrder.SubmitTime,
                  PartOrder.approvetime,
                  pop.code,
                  pop.CreateTime,
                  pop.StorageCompanyCode,
                  pop.storagecompanyname,
                  pob.createtime,
                  pob.code,
                  pob.OutboundType,
                  pob.creatorname,
                  pspo.code,
                  pspo.creatorname,
                  pspo.ShippingDate,pspo.requestedarrivaldate,
                  pspo.ConfirmedReceptionTime) abc
union all --gcdcs
select abc.OrderType,
       abc.branchcode,
       abc.branchname,
       abc.PartOrderCode,
       abc.submitcompanycode,
       abc.submitcompanyname,
       abc.createtime,
       abc.OutPlanCode,
       abc.OutCode,
       abc.OutboundType,
       abc.OutboundTypeValue,
       abc.Outcreatorname,
       abc.PartsShippingOrderCode,
       abc.pspocreatorname,
       abc.pspocreatetime,
       abc.popCreateTime,
       abc.StorageCompanyCode,
       abc.storagecompanyname,
       abc.pobCreatetime,
       abc.requestedarrivaldate,
       abc.ConfirmedReceptionTime,
       abc.PartsSalesCategoryId,
       abc.PartsSalesCategoryName,
       abc.SubmitTime,
       abc.approvetime
  from (select PartOrder.OrderType,
               PartOrder.branchcode,
               PartOrder.branchname,
               PartOrder.code as PartOrderCode,
               PartOrder.submitcompanycode,
               PartOrder.submitcompanyname,
               PartOrder.createtime,
               pop.code as OutPlanCode,
               pop.CreateTime as popCreateTime,
               pop.StorageCompanyCode,
               pop.storagecompanyname,
               pob.createtime as pobCreatetime,
               pob.code as OutCode,
               pob.OutboundType,
               decode(pob.OutboundType,
                      1,
                      '配件销售',
                      2,
                      '采购退货',
                      3,
                      '内部领出',
                      4,
                      '配件调拨',
                      5,
                      '积压件调剂',
                      6,
                      '配件零售') OutboundTypeValue,
               
               pob.creatorname             Outcreatorname,
               pspo.code                   as PartsShippingOrderCode,
               pspo.creatorname            as pspocreatorname,
               pspo.ShippingDate           as pspocreatetime,pspo.requestedarrivaldate,
               pspo.ConfirmedReceptionTime,
               PartOrder.partssalescategoryid,
               PartOrder.PartsSalesCategoryName,
               PartOrder.SubmitTime,
               PartOrder.approvetime
          from (select pso.PartsSalesOrderTypeName as OrderType,
                       pso.branchcode,
                       pso.branchname,
                       pso.code,
                       pso.submitcompanycode,
                       pso.submitcompanyname,
                       pso.createtime,
                       pso.SalesCategoryId as PartsSalesCategoryId,    --品牌ID
                       pso.SalesCategoryName as PartsSalesCategoryName, --品牌
                       pso.SubmitTime,    --订单提交时间
                       pso.ApproveTime    --订单审批时间
                  from sddcs.PartsSalesOrder pso --销售订单
                union all
                select null as OrderType,
                       pro.branchcode,
                       pro.branchname,
                       pro.code,
                       pro.branchcode as submitcompanycode,
                       pro.branchname as submitcompanyname,
                       pro.createtime,
                       pro.PartsSalesCategoryId,
                       pro.PartsSalesCategoryName,
                       null as SubmitTime,
                       pro.ApproveTime
                  from sddcs.Partspurreturnorder pro --采购退货单
                union all
                select null as OrderType,
                       branch.code as branchcode,
                       iab.branchname,
                       iab.code,
                       iab.DepartmentName as submitcompanycode,
                       iab.Departmentname as submitcompanyname,
                       iab.createtime,
                       su.partssalescategoryid,
                       psc.name as PartsSalesCategoryName,
                       null as SubmitTime,
                       iab.ApproveTime
                  from sddcs.Internalallocationbill iab --内部领出单
                  left join sddcs.SalesUnitAffiWarehouse suaw on iab.warehouseid = suaw.warehouseid
                  left join sddcs.salesunit su on suaw.salesunitid = su.id
                  left join sddcs.partssalescategory psc on su.partssalescategoryid = psc.id
                 inner join sddcs.branch
                    on branch.id = iab.branchid
                union all
                select null as OrderType,
                       branch.code,
                       branch.name,
                       pto.code,
                       pto.DestWarehouseCode as submitcompanycode,
                       pto.destwarehousename as submitcompanyname,
                       pto.createtime,
                       su.partssalescategoryid,
                       psc.name as PartsSalesCategoryName,
                       null as SubmitTime,
                       pto.ApproveTime
                  from sddcs.PartsTransferOrder pto --配件调拨单
                  left join sddcs.SalesUnitAffiWarehouse suaw on pto.DestWarehouseId = suaw.warehouseid
                  left join sddcs.salesunit su on suaw.salesunitid = su.id
                  left join sddcs.partssalescategory psc on su.partssalescategoryid = psc.id
                 inner join sddcs.branch
                    on pto.storagecompanyid = branch.id
                 where pto.StorageCompanyType = 1
                union all
                select null as OrderType,
                       opab.branchcode,
                       opab.branchname,
                       opab.code,
                       opab.DestStorageCompanyCode as submitcompanycode,
                       opab.deststoragecompanyname as submitcompanyname,
                       opab.createtime,
                       su.partssalescategoryid,
                       psc.name as PartsSalesCategoryName,
                       null as SubmitTime,
                       opab.ApproveTime
                  from sddcs.OverstockPartsAdjustBill opab --积压件调剂单
                  left join sddcs.SalesUnitAffiWarehouse suaw on opab.DestStorageCompanyId = suaw.warehouseid
                  left join sddcs.salesunit su on suaw.salesunitid = su.id
                  left join sddcs.partssalescategory psc on su.partssalescategoryid = psc.id
                union all
                select null as OrderType,
                       psro.branchcode,
                       psro.branchname,
                       psro.code,
                       psro.RetailCustomerCompanyCode as submitcompanycode,
                       psro.RetailCustomerCompanyName as submitcompanyname,
                       psro.createtime,
                       su.partssalescategoryid,
                       psc.name as PartsSalesCategoryName,
                       null as SubmitTime,
                       psro.approvetime
                  from sddcs.PartsRetailOrder psro
                  left join sddcs.SalesUnitAffiWarehouse suaw on psro.warehouseid = suaw.warehouseid
                  left join sddcs.salesunit su on suaw.salesunitid = su.id
                  left join sddcs.partssalescategory psc on su.partssalescategoryid = psc.id
                  ) PartOrder --配件零售订单
         inner join sddcs.PartsOutboundPlan pop
            on PartOrder.code = pop.sourcecode
         inner join sddcs.partsoutboundbill pob
            on pob.partsoutboundplanid = pop.id
         inner join sddcs.Partsshippingorderref pr
            on pr.partsoutboundbillid = pob.id
         inner join sddcs.PartsShippingOrder pspo
            on pspo.Originalrequirementbillcode = PartOrder.code
           and pr.partsshippingorderid = pspo.id
         group by PartOrder.OrderType,
                  PartOrder.branchcode,
                  PartOrder.branchname,
                  PartOrder.code,
                  PartOrder.submitcompanycode,
                  PartOrder.submitcompanyname,
                  PartOrder.createtime,
                  PartOrder.partssalescategoryid,
                  PartOrder.PartsSalesCategoryName,
                  PartOrder.SubmitTime,
                  PartOrder.approvetime,
                  pop.code,
                  pop.CreateTime,
                  pop.StorageCompanyCode,
                  pop.storagecompanyname,
                  pob.createtime,
                  pob.code,
                  pob.OutboundType,
                  pob.creatorname,
                  pspo.code,
                  pspo.creatorname,
                  pspo.ShippingDate,pspo.requestedarrivaldate,
                  pspo.ConfirmedReceptionTime) abc
