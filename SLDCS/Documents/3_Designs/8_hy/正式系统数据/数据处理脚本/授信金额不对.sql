--中心库
select *
  from (select cat.sihcredit, pp.asihcredit, cm.code
          from CustomerAccount cat
          join AccountGroup ag
            on cat.accountgroupid = ag.id
          left join (select sum(nvl(sd.asihcredit, 0)) as asihcredit,
                           sd.companyid
                      from SIHCreditInfoDetail sd
                      join SIHCreditInfo st
                        on sd.sihcreditinfoid = st.id
                      join CustomerAccount y
                        on sd.dealerid = y.customercompanyid
                      join AccountGroup agt
                        on y.accountgroupid = agt.id
                       and sd.companyid = agt.salescompanyid
                     where sd.status = 1
                       and trunc(sd.ValidationTime) <= trunc(sysdate)
                       and ((trunc(sd.ValidationTime) >
                           trunc(y.ValidationTime) and
                           trunc(sd.ExpireTime) <= trunc(y.ExpireTime) and
                           st.type = 2) or st.type = 1)
                       and st.status = 6
                       group by sd.companyid) pp
            on ag.salescompanyid = pp.companyid
          join company cm
            on ag.salescompanyid = cm.id
           and cm.type = 1
          join company p
            on cat.customercompanyid = p.id
          )
 where asihcredit <> sihcredit;
--服务站
select *
  from (select nvl(sd.asihcredit, 0) ty, p.code, cat.sihcredit,cm.name 中心库 ,p.name as 服务站
        
          from CustomerAccount cat
          left join SIHCreditInfoDetail sd
            on cat.customercompanyid = sd.dealerid
           and sd.status = 1
           and trunc(sd.ValidationTime) <= trunc(sysdate)
          left join SIHCreditInfo st
            on sd.sihcreditinfoid = st.id
           and st.status = 6
          join AccountGroup ag
            on cat.accountgroupid = ag.id
          join company cm
            on ag.salescompanyid = cm.id
           and cm.type = 3
          join company p
            on cat.customercompanyid = p.id
          join agencydealerrelation ad
            on cat.customercompanyid = ad.dealerid
           and ad.agencyid = ag.salescompanyid
           and ad.status = 1)
 where ty <> sihcredit;
