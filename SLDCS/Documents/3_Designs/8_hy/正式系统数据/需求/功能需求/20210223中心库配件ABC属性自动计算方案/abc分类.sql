select Extract(year from trunc(sysdate)) as year,
       ceil(Extract(month from trunc(sysdate)) / 3) as Quarter,
       bb.SalesUnitOwnerCompanyId,
       bb.SalesUnitOwnerCompanyCode,
       bb.SalesUnitOwnerCompanyName,
       bb.sparepartid,
       bb.code,
       bb.name,
       bb.OrderedQuantity as QuarterlyDemand,
       times as DemandFrequency,
       (case
         when (round((fir + sec + thid) / 30, 3) * days) > 1 then
          round(round((fir + sec + thid) / 30, 2) * days)
         else
          ceil(round((fir + sec + thid) / 30, 2) * days)
       end) as RecommendedRe,
       round((fir + sec + thid) / 30, 2) as DailyDemand,
       
       (case
         when (bb.Mpre > 0 and bb.Mpre <= 50) or
              (bb.Ttimespre > 0 and bb.Ttimespre <= 50) then
          1
         when (bb.Mpre > 50 and bb.Mpre <= 80) or
              (bb.Ttimespre > 50 and bb.Ttimespre <= 80) then
          2
         when (bb.Mpre > 80 and bb.Mpre <= 95) or
              (bb.Ttimespre > 80 and bb.Ttimespre <= 95) then
          3
         when (bb.Mpre > 95 and bb.Mpre <= 100) or
              (bb.Ttimespre > 95 and bb.Ttimespre <= 100) then
          4
         else
          null
       end) as type,
       trunc(sysdate)
  from (select tt.sparepartid,
               null,
               tt.SalesUnitOwnerCompanyCode,
               tt.SalesUnitOwnerCompanyName,
               tt.SalesUnitOwnerCompanyId,
               OrderedQuantity,
               tt.money,
               tt.pre,
               tt.times,
               tt.timespre,
               tt.Mpre,
               tt.Ttimespre,
               tt.code,
               tt.name,
               
               (select cb.Days
                  from CenterBasicClassification cb
                 where cb.type = 2
                   and status = 1
                   and rownum = 1) as days,
               fir * (select cb.weight
                        from CenterBasicClassification cb
                       where cb.type = 1
                         and Month = 1
                         and status = 1
                         and rownum = 1) as fir,
               sec * (select cb.weight
                        from CenterBasicClassification cb
                       where cb.type = 1
                         and Month = 2
                         and status = 1
                         and rownum = 1) as sec,
               thid * (select cb.weight
                         from CenterBasicClassification cb
                        where cb.type = 1
                          and Month = 3
                          and status = 1
                          and rownum = 1) as thid
          from (select rr.SalesUnitOwnerCompanyCode,
                       rr.sparepartid,
                       rr.SalesUnitOwnerCompanyName,
                       sum(rr.pre) over(partition by rr.SalesUnitOwnerCompanyId order by rr.pre desc) as Mpre,
                       rr.pre as pre,
                       sum(rr.pre) over(partition by rr.SalesUnitOwnerCompanyId order by rr.timespre desc) as Ttimespre,
                       rr.times as times,
                       rr.timespre as timespre,
                       money,
                       rr.OrderedQuantity,
                       SalesUnitOwnerCompanyId,
                       rr.code,
                       rr.name,
                       fir,
                       sec,
                       thid
                  from (select pp.SalesUnitOwnerCompanyCode,
                               pp.sparepartid,
                               pp.code,
                               pp.name,
                               pp.SalesUnitOwnerCompanyName,
                               (ratio_to_report(money)
                                over(partition by pp.SalesUnitOwnerCompanyId)) * 100 as pre,
                               money,
                               (ratio_to_report(times)
                                over(partition by pp.SalesUnitOwnerCompanyId)) * 100 as timespre,
                               pp.OrderedQuantity,
                               pp.SalesUnitOwnerCompanyId,
                               times,
                               fir,
                               sec,
                               thid
                          from (select sum(dd.money) as money,
                                       sum(times) as times,
                                       partid as sparepartid,
                                       code,
                                       name,
                                       SalesUnitOwnerCompanyId,
                                       sum(OrderedQuantity) as OrderedQuantity,
                                       SalesUnitOwnerCompanyCode,
                                       SalesUnitOwnerCompanyName,
                                       sum(first) as fir,
                                       sum(sec) as sec,
                                       sum(thid) as thid
                                  from (select pw.money                     as money,
                                               pw.times                     as times,
                                               ps.partid,
                                               sp.code,
                                               sp.name,
                                               pw.SalesUnitOwnerCompanyId   as SalesUnitOwnerCompanyId,
                                               pw.OrderedQuantity           as OrderedQuantity,
                                               pw.SalesUnitOwnerCompanyCode as SalesUnitOwnerCompanyCode,
                                               pw.SalesUnitOwnerCompanyName as SalesUnitOwnerCompanyName,
                                               first,
                                               sec,
                                               thid
                                          from partsbranch ps
                                          join sparepart sp
                                            on ps.partid = sp.id
                                          join (select (case
                                                        when ps.status = 6 and
                                                             nvl(pd.approvequantity,
                                                                 0) = 0 then
                                                         0
                                                        else
                                                         (nvl(pd.OrderedQuantity,
                                                              0) * pd.orderprice)
                                                      end) as money,
                                                      pd.OrderedQuantity as OrderedQuantity,
                                                      (case
                                                        when ps.status = 6 and
                                                             nvl(pd.approvequantity,
                                                                 0) = 0 then
                                                         0
                                                        else
                                                         1
                                                      end) as times,
                                                      ps.SalesUnitOwnerCompanyId,
                                                      pd.sparepartid,
                                                      cp.code SalesUnitOwnerCompanyCode,
                                                      cp.name SalesUnitOwnerCompanyName,
                                                      (case
                                                        when to_char(ps.SubmitTime,
                                                                     'yyyyMM') =
                                                             to_char(trunc(sysdate - 65,
                                                                           'Q'),
                                                                     'yyyyMM') then
                                                         pd.OrderedQuantity
                                                        else
                                                         0
                                                      end) as first,
                                                      (case
                                                        when to_char(ps.SubmitTime,
                                                                     'yyyyMM') =
                                                             to_char(add_months(trunc(sysdate - 65,
                                                                                      'Q'),
                                                                                1),
                                                                     'yyyyMM') then
                                                         pd.OrderedQuantity
                                                        else
                                                         0
                                                      end) as sec,
                                                      (case
                                                        when to_char(ps.SubmitTime,
                                                                     'yyyyMM') =
                                                             to_char(add_months(trunc(sysdate - 65,
                                                                                      'Q'),
                                                                                2),
                                                                     'yyyyMM') then
                                                         pd.OrderedQuantity
                                                        else
                                                         0
                                                      end) as thid
                                               
                                                 from partssalesorderdetail pd
                                                 join partssalesorder ps
                                                   on ps.id =
                                                      pd.partssalesorderid
                                                 left join company cp
                                                   on ps.SalesUnitOwnerCompanyId = cp.id
                                                 join partsbranch pb
                                                   on pd.sparepartid = pb.partid
                                                  and pb.status = 1
                                                where cp.type = 3
                                                  and pd.orderedquantity > 0
                                                  and pb.isorderable = 1
                                                  and ps.status in (2, 4, 5, 6)
                                                  and ((Extract(year from
                                                                trunc(nvl(ps.SubmitTime,
                                                                          ps.createtime))) ||
                                                      ceil(Extract(month from
                                                                     trunc(nvl(ps.SubmitTime,
                                                                               ps.createtime))) / 3))) =
                                                      ((Extract(year from
                                                                trunc(sysdate)) ||
                                                      ceil(Extract(month from
                                                                     trunc(sysdate)) / 3))) - 1
                                               
                                               union all
                                               select pd.DiscountedAmount *
                                                      pd.Quantity as money,
                                                      pd.Quantity as OrderedQuantity,
                                                      1 as times,
                                                      pr.SalesUnitOwnerCompanyId,
                                                      pd.sparepartid,
                                                      pr.salesunitownercompanycode,
                                                      pr.salesunitownercompanyname,
                                                      (case
                                                        when to_char(pr.approvetime,
                                                                     'yyyyMM') =
                                                             to_char(trunc(sysdate - 65,
                                                                           'Q'),
                                                                     'yyyyMM') then
                                                         pd.Quantity
                                                        else
                                                         0
                                                      end) as first,
                                                      (case
                                                        when to_char(pr.approvetime,
                                                                     'yyyyMM') =
                                                             to_char(add_months(trunc(sysdate - 65,
                                                                                      'Q'),
                                                                                1),
                                                                     'yyyyMM') then
                                                         pd.Quantity
                                                        else
                                                         0
                                                      end) as sec,
                                                      (case
                                                        when to_char(pr.approvetime,
                                                                     'yyyyMM') =
                                                             to_char(add_months(trunc(sysdate - 65,
                                                                                      'Q'),
                                                                                2),
                                                                     'yyyyMM') then
                                                         pd.Quantity
                                                        else
                                                         0
                                                      end) as thid
                                                 from PartsRetailOrder pr
                                                 join PartsRetailOrderDetail pd
                                                   on pr.id =
                                                      pd.PartsRetailOrderId
                                                where (pr.status = 2 or
                                                      pr.status = 3)
                                                  and ((Extract(year from
                                                                trunc(pr.ApproveTime)) ||
                                                      ceil(Extract(month from
                                                                     trunc(pr.ApproveTime)) / 3))) =
                                                      ((Extract(year from
                                                                trunc(sysdate)) ||
                                                      ceil(Extract(month from
                                                                     trunc(sysdate)) / 3))) - 1) pw
                                            on ps.partid = pw.sparepartid
                                         where ps.status = 1
                                           and nvl(ps.isorderable, 0) = 1
                                           and ps.partid not in
                                               (select id
                                                  from (select spp.id,
                                                               spp.code,
                                                               spp.name,
                                                               pbp.partabc
                                                          from sparepart spp
                                                          join partsbranch pbp
                                                            on spp.id = pbp.partid
                                                          left join PartsSalePriceIncreaseRate psp
                                                            on pbp.IncreaseRateGroupId =
                                                               psp.id
                                                         where (pbp.partabc in
                                                               (6, 7, 11) or
                                                               psp.GroupName = '3��')))) dd
                                 group by dd.partid,
                                          dd.code,
                                          dd.name,
                                          dd.SalesUnitOwnerCompanyId,
                                          dd.SalesUnitOwnerCompanyCode,
                                          dd.SalesUnitOwnerCompanyName) pp) rr) tt) bb
