USE [Service]
GO
/****** Object:  StoredProcedure [dbo].[AddDealerRepairContract]    Script Date: 01/15/2015 10:12:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:    <Author,,Name>
-- Create date: <Create Date,,>
-- Description:  <Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[AddDealerRepairContract] 
AS
BEGIN  
  Declare
    --定义维修合同主单变量
    @1Xid int ,
    @1ID int ,
    @1Code varchar(50) ,
    @1ServiceAdvisorCode varchar(50) ,
    @1CreatorCode varchar(50) ,
    @1ServiceAdvisorName varchar(100) ,
    @1Customercode varchar(50) ,
    @1CustomerName varchar(40) ,
    @1CellNumber varchar(50) ,
    @1CompletionTime datetime ,
    @1Address varchar(200) ,
    @1ZipCode varchar(6) ,
    @1Vin varchar(50) ,
    @1EngineCode varchar(50) ,
    @1PlateNumber varchar(20) ,
    @1VehicleModelCode varchar(50) ,
    @1VehicleCategoryCode varchar(50) ,
    @1VehicleModelName varchar(100) ,
    @1Mileage int ,
    @1RepairType int ,
    @1RepairTime datetime ,
    @1StartRepairTime datetime ,
    @1ExpectedFinishTime datetime ,
    @1ActualDeliveryTime datetime ,
    @1VehicleObjects varchar(200) ,
    @1FuelAmount float ,
    @1Description varchar(1000) ,
    @1CustomerComplaint varchar(1000) ,
    @1WorkingFee money ,
    @1MaterialFee money ,
    @1AuxiliaryPartsFee money ,
    @1OutServiceFee money ,
    @1OtherFee money ,
    @1OtherFeeDescription varchar(200) ,
    @1FaultDescription varchar(600) ,
    @1CheckResults varchar(600) ,
    @1FaultAnalyes varchar(600) ,
    @1SettleProperty int ,
    @1MainOldPartID int ,
    @1MainOldPartCode varchar(100) ,
    @1MainOldPartName varchar(100) ,
    @1MainOldPartRepTime int ,
    @1IsRepWarn int ,
    @1IsReturnWarn int ,
    @1SumRepTime int ,
    @1HaulageFee money ,
    @1TrafficFee money ,
    @1RepTime int ,
    @1IsBookingService bit ,
    @1IsOutService bit ,
    @1RequisitionBillCode varchar(50) ,
    @1TechnicalServiceActivityCode varchar(50) ,
    @1Promise int ,
    @1ClaimStatus int ,
    @1Status int ,
    @1Remark varchar(200) ,
    @1CorporationCode varchar(50) ,
    @1CorporationName varchar(100) ,
    @1CreatorId int ,
    @1CreatorName varchar(100) ,
    @1CreateTime datetime ,
    @1ModifierName varchar(100) ,
    @1ModifyTime datetime ,
    @1SyncNo int ,
    @1EnterCode varchar(15) ,
    @1CustProp varchar(1) ,
    @1DateOfRetail datetime ,
    @1ISWin int ,
    @1OBJID uniqueidentifier,
  
    --定义维修项目清单变量
    @2OBJID uniqueidentifier,
    @2Id int ,
    @2code varchar(50) ,
    @2JobType int ,
    @2RepairItemCode varchar(50) ,
    @2RepairItemName varchar(100) ,
    @2AccountingProperty int ,
    @2Hours float ,
    @2Price money ,
    @2WorkingFee money ,
    @2WorkingFeeDiscount money ,
    @2Repairman varchar(100) ,
    @2RepairTeam varchar(100) ,
    @2RepairReason varchar(200) ,
    @2ClaimStatus int ,
    @2Remark varchar(200) ,
    @2RepairmanCode varchar(50) ,
    @2IsOEM int ,
    @2ItemName varchar(100) ,
      
    --定义维修材料清单变量
    @3OBJID uniqueidentifier,
    @3Id int ,
    @3RepairContactCode varchar(50) ,
    @3RepairContractItemscode varchar(50) ,
    @3WarehouseCode varchar(50) ,
    @3WarehouseName varchar(100) ,
    @3dealercode varchar(15) ,
    @3SparePartCode varchar(50) ,
    @3SparePartName varchar(100) ,
    @3AccountingProperty int ,
    @3Amount int ,
    @3Price money ,
    @3PrimarySupplierCode varchar(50) ,
    @3MaterialFee money ,
    @3MaterialFeeDiscount money ,
    @3SparePartManagementFee money ,
    @3ClaimStatus int ,
    @3Remark varchar(200) ,
    @3MeasureUnitname varchar(100) ,
    @3RepairContractItemId int ,
      
    --定义旧件清单变量
    @4OBJID uniqueidentifier,
    @4OldSparePartCode varchar(50) ,
    @4ClaimType int ,
    @4PreviousRepairContractCode varchar(50) ,
    @4code varchar(50) ,
    @4RepairItemCode varchar(50) ,
    @4SparePartCode varchar(50) ,
    @4RepairContractMaterialId int ,
    @4RepairContractItemId int ,
    
    --定义结算单变量
    @5OBJID uniqueidentifier,
    @5CreatorCode varchar(50)  ,
    @5Id int  ,
    @5Code varchar(50)  ,
    @5CustomerId int  ,
    @5CustomerName varchar(100)  ,
    @5WorkingFeeSum money  ,
    @5MaterialFeeSum money  ,
    @5AuxiliaryPartsFeeSum money  ,
    @5OutServiceFeeSum money  ,
    @5OtherFeeSum money  ,
    @5AccountingAmount money  ,
    @5PaymentType int  ,
    @5CustomerOpinion varchar(200) ,
    @5Status int  ,
    @5Remark varchar(1000) ,
    @5CorporationId int ,
    @5CorporationName varchar(100) ,
    @5CreatorId int ,
    @5CreatorName varchar(100) ,
    @5CreateTime datetime ,
    @5ModifierId int ,
    @5ModifierName varchar(100) ,
    @5ModifyTime datetime ,
    @5ApproverId int ,
    @5ApproverName varchar(100) ,
    @5ApproveTime datetime ,
    @5Comment varchar(200) ,
    @5RepairAccountingOrderId int  ,
    @5RepairContractId int  ,
    @5RepairContractCode varchar(50)  ,
    @5WorkingFee money  ,
    @5WorkingFeeDiscount money  ,
    @5MaterialFee money  ,
    @5MaterialFeeDiscount money  ,
    @5AuxiliaryPartsFee money  ,
    @5OutServiceFee money  ,
    @5OtherFee money  ,
    @5Total money  ,
    @5TotalDiscounted money  ,
    @5NextMaintanceMileage int ,
    @5NextMaintanceDate datetime ,
    
    --定义变量
    @EnterCode uniqueidentifier,
        @CarRecallSetID uniqueidentifier,
        @WarehouseID uniqueidentifier,
        @tRepGuid uniqueidentifier,
        @ItemGuid uniqueidentifier,
        @WorkItemName varchar(100),
        @IsBYSettleStatus int,
        @IsSPSettleStatus int,
        @Reptotalfee money,
        @WorkItemID varchar(50), 
        @WorkItemCode varchar(50),
        @WorkTotalFee money,
        @WorkProp int,
        @SettleProp int,
        @IsValid int,
        @ManageRate money,
        @SpareID uniqueidentifier,
        @UnitID uniqueidentifier,
        @MaterialCode varchar(50),
        @MaterialName varchar(50),
        @UnitName varchar(50),
        @MainOldPartID uniqueidentifier,
        @RepairProp int,
        @SPSettleStatus int,
        @IsDelete int,
        @RepMaterialsID uniqueidentifier,
        @OldRepcontactID uniqueidentifier,
        @OldSparePartID uniqueidentifier,
        @OldSparePartCode varchar(50),
        @SPType int,
        @Range int,
        @BeginSaleTime datetime,
        @DateOfRetail datetime,
        @TSMaxNum int,
        @PTMaxNum int,
        @SupMaxRepTime int,
        @tCarCustomerID uniqueidentifier,
        @NorMaxRepTime int,
        @SettlesGuid uniqueidentifier,
        @PayMode int,
        @i uniqueidentifier,
        @j int
        
    --定义游标，取维修合同主单数据
    Declare RepairContractCursor Cursor For select top 2000 ID,Code,ServiceAdvisorCode,CreatorCode,ServiceAdvisorName,Customercode,CustomerName
                      ,CellNumber,CompletionTime,Address,ZipCode,Vin,EngineCode,PlateNumber,VehicleModelCode
                      ,VehicleCategoryCode,VehicleModelName,Mileage,RepairType,RepairTime,StartRepairTime
                      ,ExpectedFinishTime,ActualDeliveryTime,VehicleObjects,FuelAmount,Description,CustomerComplaint
                      ,WorkingFee,MaterialFee,AuxiliaryPartsFee,OutServiceFee,OtherFee,OtherFeeDescription,FaultDescription
                      ,CheckResults,FaultAnalyes,SettleProperty,MainOldPartID,MainOldPartCode,MainOldPartName
                      ,MainOldPartRepTime,IsRepWarn,IsReturnWarn,SumRepTime,HaulageFee,TrafficFee,RepTime
                      ,IsBookingService,IsOutService,RequisitionBillCode,TechnicalServiceActivityCode,Promise
                      ,ClaimStatus,Status,Remark,CorporationCode,CorporationName,CreatorId,CreatorName
                      ,CreateTime,ModifierName,ModifyTime,SyncNo,EnterCode,CustProp,DateOfRetail
                      ,ISWin,OBJID From TempRepairContract with(nolock) Where IsWin = 0 Order By xid
  Open RepairContractCursor  --打开游标
  
  --循环游标每条数据
  fetch next from RepairContractCursor into @1ID,@1Code,@1ServiceAdvisorCode,@1CreatorCode,@1ServiceAdvisorName,@1Customercode,@1CustomerName
                      ,@1CellNumber,@1CompletionTime,@1Address,@1ZipCode,@1Vin,@1EngineCode,@1PlateNumber,@1VehicleModelCode
                      ,@1VehicleCategoryCode,@1VehicleModelName,@1Mileage,@1RepairType,@1RepairTime,@1StartRepairTime
                      ,@1ExpectedFinishTime,@1ActualDeliveryTime,@1VehicleObjects,@1FuelAmount,@1Description,@1CustomerComplaint
                      ,@1WorkingFee,@1MaterialFee,@1AuxiliaryPartsFee,@1OutServiceFee,@1OtherFee,@1OtherFeeDescription,@1FaultDescription
                      ,@1CheckResults,@1FaultAnalyes,@1SettleProperty,@1MainOldPartID,@1MainOldPartCode,@1MainOldPartName
                      ,@1MainOldPartRepTime,@1IsRepWarn,@1IsReturnWarn,@1SumRepTime,@1HaulageFee,@1TrafficFee,@1RepTime
                      ,@1IsBookingService,@1IsOutService,@1RequisitionBillCode,@1TechnicalServiceActivityCode,@1Promise
                      ,@1ClaimStatus,@1Status,@1Remark,@1CorporationCode,@1CorporationName,@1CreatorId,@1CreatorName
                      ,@1CreateTime,@1ModifierName,@1ModifyTime,@1SyncNo,@1EnterCode,@1CustProp,@1DateOfRetail
                      ,@1ISWin,@1OBJID   
  while(@@fetch_status=0) --数据集是否有数据
  Begin
    set @EnterCode = null
    set @IsBYSettleStatus = null
    set @Reptotalfee = null
    set @WarehouseID = null
    set @i = null
    
    select @EnterCode = objid from tcompany  with(nolock)  where code= @1EnterCode --取VEI中经销商ID
    select @IsBYSettleStatus=case when count(*)>0 then 2 else 1 end from TempRepItems  with(nolock)  where AccountingProperty = 1 and Objid = @1OBJID --取首保结算状态
    set @Reptotalfee = round(isnull(@1WorkingFee,0) + isnull(@1MaterialFee,0) + isnull(@1AuxiliaryPartsFee,0) + isnull(@1OtherFee,0),2) --取合同总金额
    select top 1 @i = objid from TempRepMaterials  with(nolock)  where Objid = @1OBJID
    --取第一条材料的仓库ID,如果没有材料信息，取一个仓库信息
    --if @i is not null 
      --select @WarehouseID = objid from tWarehouses  with(nolock) where code= (select top 1 WarehouseCode from TempRepMaterials  with(nolock)  where Objid = @1OBJID) and IsValid=2 and entercode= @EnterCode
    --else
      --select top 1 @WarehouseID = objid from tWarehouses  with(nolock) where  IsValid=2 and  entercode= @EnterCode
    if @WarehouseID is null set @WarehouseID = newid()
    
    --开始事务
    BEGIN TRANSACTION
    Begin Try 
      if @1RepairType >= 5
      begin
         Set @i=null
            end
            else
      begin
        set @IsSPSettleStatus = null
        set @IsValid = null
        set @RepairProp = null
        set @SPSettleStatus = null
        set @tRepGuid = null
        set @MainOldPartID = null
        set @isDelete = null
        
        --正常维修合同&商品车维修
        --取主因旧件
                if (@1MainOldPartCode is not null and @1MainOldPartCode <> '')
                begin
          set @i = null
          select top 1 @i = objid from tmaterials with(nolock)  where Code= @1MainOldPartCode and entercode='{400482AE-3C25-4ED1-B2E5-659C88F0CEFC}'                
          if @i is null 
            RAISERROR('无效主因旧件[%s]',16,1,@1MainOldPartCode) 
          else
            set @MainOldPartID = @i
                end            

                --合同主单
                set @i = null
                select top 1 @i = objid from  tRepContracts with(nolock)  where upper(code)= upper(@1Code)
                if @1Status not in (0,1,2,3) 
          RAISERROR('无效维修合同状态[%s]',16,1,@1Status)
        else
          set @IsValid = case @1Status when 0 then 0 when 1 then 1 when 2 then case when @1RepairType = 4 then 4 else 3 end when 3 then 4 end
        
        if @1RepairType not in (0,1,2,3,4,5,6)
          RAISERROR('无效维修合同类型[%s]',16,1,@1RepairType)
        else
          set @RepairProp = case @1RepairType when 0 then 2 when 1 then 1 when 2 then 4 when 3 then 5 when 4 then 10 when 5 then 1 when 6 then 1 end
        
        if @1ClaimStatus not in (0,1,2,3)
          RAISERROR('无效保用服务状态[%s]',16,1,@1ClaimStatus)
        else
          set @SPSettleStatus = case @1ClaimStatus when 0 then 1 when 1 then 2 when 2 then 3 when 3 then 4 end
                
                if @i is null
        begin 
          --新增维修合同
          set @tRepGuid = newid()  
          set @isDelete = 0        
        
          INSERT INTO tRepContracts (
          ObjID,Code,EnterCode,CreatorCode,CreateTime,IsValid,CustProp,
          SerialCode,Range,RepBeginTime,FaithFinTime,
          RepairProp,SerCode,WorkFeeMDis,MatFeeMDis,
          WorkFee,MatFee,MatManageFee,FMatFee,OtherFee,
          CarThing,FinishDate,
          SPSettleStatus,BYSettleStatus,StationProm,Memo,
          ProductType,ComeStationCust,ComeStationCustmPhone,
          Reptotalfee,warehouseID,HaulageFee ,TrafficFee ,settleprop ,MainOldPartID,MainOldPartRepTime
          ,IsRepWarn,IsReturnWarn ,RepTime ,SumRepTime,MainOldPartCode
          ,MainOldPartName,FaultDes,FaultAnalyse,TestResult) 
          VALUES (@tRepGuid,@1Code,'{' + cast(@EnterCode as varchar(50)) + '}',@1CreatorCode,@1CreateTime,@IsValid,@1CustProp,
          @1Vin,@1Mileage,@1RepairTime,@1ExpectedFinishTime,
          @RepairProp,@1ServiceAdvisorCode,1,1,
          round(isnull(@1WorkingFee,0),2),round(isnull(@1MaterialFee,0),2),0,round(isnull(@1AuxiliaryPartsFee,0),2),round(isnull(@1OtherFee,0) + isnull(@1OutServiceFee,0),2),
          @1VehicleObjects,@1ActualDeliveryTime,
          @SPSettleStatus,@IsBYSettleStatus,case when @1Promise = 0 then '客户是上帝，服务要满意！' else '' end,@1Remark,
          @1VehicleCategoryCode,@1CustomerName,@1CellNumber,
          @Reptotalfee,@WarehouseID,isnull(@1HaulageFee,0),isnull(@1TrafficFee,0),isnull(@1SettleProperty,0),@MainOldPartID,isnull(@1MainOldPartRepTime,0)
          ,isnull(@1IsRepWarn,0),isnull(@1IsReturnWarn,0),isnull(@1RepTime,0),isnull(@1SumRepTime,0),@1MainOldPartCode
          ,@1MainOldPartName,@1FaultDescription,@1FaultAnalyes,@1CheckResults)
        end
        else
        begin
          --修改维修合同
          set @tRepGuid = @i
          set @isDelete = 1    
          
          select @IsSPSettleStatus = case when count(*) > 0 then 1 else 0 end from tRepContracts  with(nolock) where upper(code)=upper(@1Code) and (SPSettleStatus in (3,4) or BYSettleStatus = 3)

          UPDATE tRepContracts 
          SET EnterCode = '{' + cast(@EnterCode as varchar(50)) + '}',CreatorCode = @1CreatorCode
          ,CreateTime = @1CreateTime,IsValid = @IsValid,CustProp=@1CustProp
          ,SerialCode = @1Vin,Range = @1Mileage
          ,RepBeginTime = @1RepairTime,FaithFinTime = @1ExpectedFinishTime
          ,RepairProp = @RepairProp,SerCode = @1ServiceAdvisorCode
          ,WorkFeeMDis = 1 ,MatFeeMDis = 1
          ,WorkFee = round(isnull(@1WorkingFee,0),2),MatFee = round(isnull(@1MaterialFee,0),2)
          ,MatManageFee = 0,FMatFee = round(isnull(@1AuxiliaryPartsFee,0),2)
          ,OtherFee = round(isnull(@1OtherFee,0)+isnull(@1OutServiceFee,0),2),CarThing = @1VehicleObjects
          ,FinishDate = @1ActualDeliveryTime
          ,StationProm = case when @1Promise = 0 then '客户是上帝，服务要满意！' else '' end,Memo = @1Remark
          ,ProductType = @1VehicleCategoryCode,ComeStationCust = @1CustomerName
          ,ComeStationCustmPhone = @1CellNumber,Reptotalfee = @Reptotalfee,warehouseID=@WarehouseID
          ,HaulageFee=isnull(@1HaulageFee,0),TrafficFee=isnull(@1TrafficFee,0),settleprop=isnull(@1SettleProperty,0)
          ,MainOldPartID=@MainOldPartID,MainOldPartRepTime=isnull(MainOldPartRepTime,0)
          ,IsRepWarn=isnull(@1IsRepWarn,0),IsReturnWarn=isnull(@1IsReturnWarn,0),RepTime=isnull(@1RepTime,0)
          ,SumRepTime=isnull(SumRepTime,0),MainOldPartCode=@1MainOldPartCode
          ,MainOldPartName=@1MainOldPartName,FaultDes=@1FaultDescription
          ,FaultAnalyse=@1FaultAnalyes,TestResult=@1CheckResults
          WHERE Code = @1Code
        end        
        
                
                --处理清单，关联单
                if @IsSPSettleStatus is null or @IsSPSettleStatus = 0
                begin
          set @CarRecallSetID = null
                
                    if @IsDelete is not null and @IsDelete = 1
                    begin
                        --删除项目清单和材料清单以及旧件登记
                        delete from tRepItems  where upper(parentid)=upper(@tRepGuid)
            delete from tRepMaterials where upper(parentid) =upper(@tRepGuid)
            delete from TOldMaterials where upper(parentid) =upper(@tRepGuid)
                    end

                    
                    --清单
                    --定义游标，取维修合同项目清单
          Declare RepItemsCursor Cursor For select Id,code,JobType,RepairItemCode,RepairItemName,AccountingProperty,Hours,
                          Price,WorkingFee,WorkingFeeDiscount,Repairman,RepairTeam,RepairReason,
                          ClaimStatus,Remark,RepairmanCode,IsOEM,ItemName,OBJID From TempRepItems with(nolock)  Where Objid = @1Objid
          Open RepItemsCursor  --打开游标
          
          --循环游标每条数据
          fetch next from RepItemsCursor into @2Id,@2code,@2JobType,@2RepairItemCode,@2RepairItemName,@2AccountingProperty,@2Hours,
                            @2Price,@2WorkingFee,@2WorkingFeeDiscount,@2Repairman,@2RepairTeam,@2RepairReason,
                            @2ClaimStatus,@2Remark,@2RepairmanCode,@2IsOEM,@2ItemName,@2OBJID   
          while(@@fetch_status=0) --数据集是否有数据
          Begin  
                        --1.判断是否是999999（dms经销商和总部自定义工位)，若是,hms对应的编号分别是zdy,999999,填写WorkItemID为空，code,WorkItemName
            --经销商自定义工时工位999999，在dms工位表中不存在，手动填写。
            --2.判断是否是本部工时工位IsOEM,若是，填写WorkItemID，WorkItemName，若不是，WorkItemID为空，填写code,WorkItemName
            set @WorkItemID = null
            set @WorkItemCode = null
            set @WorkItemName = null
            set @WorkTotalFee = null
            set @ItemGuid = null
            set @WorkProp = null
            set @SettleProp = null
            
            if @2IsOEM = 0
            begin
              if (@2RepairItemCode = '999999')
              begin
                if (@2AccountingProperty = 0) --结转属性为索赔
                begin
                  set @WorkItemID = '{00000000-0000-0000-0000-000000000000}'
                  set @WorkItemCode = '999999'
                end
                else
                begin
                  set @WorkItemCode = 'zdy'
                  set @WorkItemID = '{00000000-0000-0000-0000-000000000000}'
                end
              end
              else
              begin
                set @WorkItemCode = @2RepairItemCode
                set @WorkItemID = '{00000000-0000-0000-0000-000000000000}'
              end
            end
            else
            begin
              select @WorkItemID = objid from tWorkItems with(nolock)  where code=@2RepairItemCode and entercode = '{400482AE-3C25-4ED1-B2E5-659C88F0CEFC}'
              set @WorkItemCode =@2RepairItemCode
              if (@WorkItemID is null) RAISERROR('无效维修项目[%s]',16,1,@2RepairItemCode)
            end
            
                        --防止维修项目名称过长,无法插入
                        if (len(@2RepairItemName) <= 40) --数据库字段长度为varchar(80)
              set @WorkItemName = @2RepairItemName
            else
              set @WorkItemName = Substring(@2RepairItemName,1,40)
              
            set @WorkTotalFee = round(isnull(@2Hours,0) * isnull(@2Price,0),2)                           
            set @ItemGuid = newid()
            if @2JobType not in (0,1,2,3) 
              RAISERROR('无效作业属性[%s]',16,1,@2JobType)
            else
              set @WorkProp = case @2JobType when 0 then 3 when 1 then 2 when 2 then 1 when 3 then 4 end
              
            if @2AccountingProperty not in (0,1,2,3,4,5,6) 
              RAISERROR('无效结算属性[%s]',16,1,@2AccountingProperty)
            else
              set @SettleProp = case @2AccountingProperty when 0 then 1 when 1 then 2 when 2 then 3 when 3 then 4 when 4 then 5 when 5 then 6 when 6 then 7 end
              
                        --插入维修项目
            insert into tRepItems(ObjID,ParentID,WorkProp,WorkItemID,WorkItemCode,
            WorkItemName,SettleProp,WorkHour,WorkFeeStand,WorkerID,Memo,worktotalfee,SerCardStatus,WorkFeeDis)
            values(@ItemGuid,@tRepGuid,@WorkProp,@WorkItemID,@WorkItemCode,
            @WorkItemName,@SettleProp,@2Hours,@2Price,@2Repairman,@2Remark,@WorkTotalFee,0,
            case when isnull(@2WorkingFeeDiscount,0) = 0 then 1 
                 when @WorkTotalFee=0 then 0  -- by py 20150115避免被0除 
                 else round((@WorkTotalFee - @2WorkingFeeDiscount)/@WorkTotalFee,4) end)
                                      
                        --维修材料
                        --定义游标，取维修合同材料清单
            Declare RepMaterialsCursor Cursor For select Id,RepairContactCode,RepairContractItemscode,WarehouseCode,WarehouseName
                            ,dealercode,SparePartCode,SparePartName,AccountingProperty,Amount
                            ,Price,PrimarySupplierCode,MaterialFee,MaterialFeeDiscount,SparePartManagementFee
                            ,ClaimStatus,Remark,MeasureUnitname,RepairContractItemId,OBJID From TempRepMaterials with(nolock)  Where Objid = @1Objid And RepairContractItemId = @2ID
            Open RepMaterialsCursor  --打开游标
                
            --循环游标每条数据
            fetch next from RepMaterialsCursor into @3Id,@3RepairContactCode,@3RepairContractItemscode,@3WarehouseCode,@3WarehouseName
                              ,@3dealercode,@3SparePartCode,@3SparePartName,@3AccountingProperty,@3Amount
                              ,@3Price,@3PrimarySupplierCode,@3MaterialFee,@3MaterialFeeDiscount,@3SparePartManagementFee
                              ,@3ClaimStatus,@3Remark,@3MeasureUnitname,@3RepairContractItemId,@3OBJID   
            while(@@fetch_status=0) --数据集是否有数据
            Begin  
              set @SpareID = null
              set @MaterialCode = null
              set @MaterialName = null
              set @UnitID = null
              set @UnitName = null
              set @SpareID = null
              set @SettleProp = null
              set @RepMaterialsID = null
              
                            --判断配件是否存在
                            if @3PrimarySupplierCode = 'CAM'
                            begin
                set @i = null
                                select top 1 @i = objid from tmaterials with(nolock)  where code=@3SparePartCode and entercode='{400482AE-3C25-4ED1-B2E5-659C88F0CEFC}'
                                if @i is not null
                                begin
                  set @SpareID = @i
                  set @MaterialCode = @3SparePartCode
                  set @MaterialName = @3SparePartName
                                    --计量单位    
                                    set @i = null                        
                                    select top 1 @i = objid from tunits with(nolock)  where materialID=@SpareID and entercode='{400482AE-3C25-4ED1-B2E5-659C88F0CEFC}' and name=@3MeasureUnitname
                  if @i is null
                  begin                                
                    RAISERROR('无效计量单位[%s]',16,1,@3MeasureUnitname)
                  end  
                  else
                  begin
                    set @UnitID = @i
                    set @UnitName = @3MeasureUnitname
                  end
                                end
                                else
                                begin
                                    RAISERROR('无效总部配件[%s]',16,1,@3SparePartCode)
                                end
                            end
                            else
                            begin
                                set @SpareID = '{00000000-0000-0000-0000-000000000000}' 
                set @UnitID = '{00000000-0000-0000-0000-000000000000}'
set @MaterialCode = @3SparePartCode
                set @MaterialName = @3SparePartName
set @UnitName = @3MeasureUnitname
                            end
                            
                            if @3AccountingProperty not in (0,1,2,3,4,5,6) 
                RAISERROR('无效结算属性[%s]',16,1,@3AccountingProperty)
              else
                set @SettleProp = case @3AccountingProperty when 0 then 1 when 1 then 2 when 2 then 3 when 3 then 4 when 4 then 5 when 5 then 6 when 6 then 7 end

                            set @RepMaterialsID = newid();
                            --新增维修材料
                            insert into tRepMaterials(ObjID,ParentID,SpareID,Amount,SparePrice,ManageRate,SettleProp,RepItemID,
              UnitID,maltotalfee,MaterialCode,MaterialName,UnitName,MatFeeDis,SerCardStatus)
              values(@RepMaterialsID,@tRepGuid,@SpareID,@3Amount,@3Price,0.15,@SettleProp,@ItemGuid,
              @UnitID,@3MaterialFee,@MaterialCode,@MaterialName,@UnitName,
              case when isnull(@3MaterialFeeDiscount,0) = 0 then 1 
                   when @3MaterialFee=0 then 0  --by py20150115  避免被0除
                else round((@3MaterialFee - @3MaterialFeeDiscount)/@3MaterialFee,4) end,0)
                        
              --定义游标，取维修合同旧件
              Declare OldSparePartCursor Cursor For select OldSparePartCode,ClaimType,PreviousRepairContractCode,code,RepairItemCode
                                    ,SparePartCode,RepairContractMaterialId,RepairContractItemId,OBJID From TempOldSparePart with(nolock)  Where Objid = @1Objid And RepairContractMaterialId = @3ID
              Open OldSparePartCursor  --打开游标
                  
              --循环游标每条数据
              fetch next from OldSparePartCursor into @4OldSparePartCode,@4ClaimType,@4PreviousRepairContractCode,@4code,@4RepairItemCode
                                  ,@4SparePartCode,@4RepairContractMaterialId,@4RepairContractItemId,@4OBJID   
              while(@@fetch_status=0) --数据集是否有数据
              Begin  
                set @OldRepContactID = null
                set @OldSparePartID = null
                set @OldSparePartCode = null
                set @SPType = null
              
                                --取旧维修合同
                                if (@4PreviousRepairContractCode is not null and @4PreviousRepairContractCode <> '')
                                begin
                                    select top 1 @OldRepContactID = objid from tRepContracts with(nolock)  where code=@4PreviousRepairContractCode
                                    if @OldRepContactID is null RAISERROR('无效先前维修合同[%s]',16,1,@4PreviousRepairContractCode)
                                end
                                --旧件材料
                                set @OldSparePartCode = @4OldSparePartCode
                                if @OldSparePartCode is null or @OldSparePartCode = ''
                                    select top 1 @OldSparePartID = objid from tmaterials with(nolock)  where code=@4SparePartCode and entercode='{400482AE-3C25-4ED1-B2E5-659C88F0CEFC}'
                                else
                                begin
                                    select top 1 @OldSparePartID = objid from tmaterials with(nolock)  where code=@4OldSparePartCode and entercode='{400482AE-3C25-4ED1-B2E5-659C88F0CEFC}'
                                end
                                if (@OldSparePartID is null) RAISERROR('无效旧件编号[%s]',16,1,@4OldSparePartCode)
                                
                                if @4ClaimType not in (0,1,2) 
                  RAISERROR('无效索赔工位状态[%s]',16,1,@4ClaimType)
                else
                  set @SPType = case @4ClaimType when 0 then 3 when 1 then 1 when 2 then 2 end

                                --新增旧件
                                INSERT INTO TOldMaterials (Objid ,Parentid,RepItemID,RepItemCode,RepMaterialID
                 ,RepMaterialCode,OldMaterialID,SPType,FRepContractCode ,FRepContractID)
                VALUES
                (newid(),@tRepGuid,@ItemGuid,@4RepairItemCode,@RepMaterialsID,
                @MaterialCode,@OldSparePartID,@SPType,@4PreviousRepairContractCode,@OldRepContactID)
                                
                                --循环游标每条数据
                fetch next from OldSparePartCursor into @4OldSparePartCode,@4ClaimType,@4PreviousRepairContractCode,@4code,@4RepairItemCode
                                    ,@4SparePartCode,@4RepairContractMaterialId,@4RepairContractItemId,@4OBJID 
                            end
                            --关闭游标
              CLOSE OldSparePartCursor
              DEALLOCATE OldSparePartCursor
                              
                            --循环游标每条数据
              fetch next from RepMaterialsCursor into @3Id,@3RepairContactCode,@3RepairContractItemscode,@3WarehouseCode,@3WarehouseName
                                  ,@3dealercode,@3SparePartCode,@3SparePartName,@3AccountingProperty,@3Amount
                                  ,@3Price,@3PrimarySupplierCode,@3MaterialFee,@3MaterialFeeDiscount,@3SparePartManagementFee
                                  ,@3ClaimStatus,@3Remark,@3MeasureUnitname,@3RepairContractItemId,@3OBJID
            End
            --关闭游标
            CLOSE RepMaterialsCursor
            DEALLOCATE RepMaterialsCursor
            --循环游标每条数据
            fetch next from RepItemsCursor into @2Id,@2code,@2JobType,@2RepairItemCode,@2RepairItemName,@2AccountingProperty,@2Hours,
                                @2Price,@2WorkingFee,@2WorkingFeeDiscount,@2Repairman,@2RepairTeam,@2RepairReason,
                                @2ClaimStatus,@2Remark,@2RepairmanCode,@2IsOEM,@2ItemName,@2OBJID 
          end
          --关闭游标
          CLOSE RepItemsCursor
          DEALLOCATE RepItemsCursor
                end  
                
                --结算单及三包
                --定义游标，取维修合同结算单
                set @BeginSaleTime = null
                set @DateOfRetail = null
                
        select top 1 @BeginSaleTime = beginsaletime from tvehiclewarningsets with(nolock)  where IsValid = 1
                set @DateOfRetail = case when @1DateOfRetail is null or @1DateOfRetail = '' then '1900-01-01' else @1DateOfRetail end
        Declare RepairAccountingOrderCursor Cursor For select CreatorCode,Id,Code,CustomerId,CustomerName,WorkingFeeSum,MaterialFeeSum
                          ,AuxiliaryPartsFeeSum,OutServiceFeeSum,OtherFeeSum,AccountingAmount,PaymentType
                          ,CustomerOpinion,Status,Remark,CorporationId,CorporationName,CreatorId
                          ,CreatorName,CreateTime,ModifierId,ModifierName,ModifyTime,ApproverId
                          ,ApproverName,ApproveTime,Comment,RepairAccountingOrderId,RepairContractId
                          ,RepairContractCode,WorkingFee,WorkingFeeDiscount,MaterialFee,MaterialFeeDiscount
                          ,AuxiliaryPartsFee,OutServiceFee,OtherFee,Total,TotalDiscounted,NextMaintanceMileage
                          ,NextMaintanceDate,OBJID From TempRepairAccountingOrder with(nolock)  Where Objid = @1Objid 
        Open RepairAccountingOrderCursor  --打开游标
            
        --循环游标每条数据
        fetch next from RepairAccountingOrderCursor into @5CreatorCode,@5Id,@5Code,@5CustomerId,@5CustomerName,@5WorkingFeeSum,@5MaterialFeeSum
                            ,@5AuxiliaryPartsFeeSum,@5OutServiceFeeSum,@5OtherFeeSum,@5AccountingAmount,@5PaymentType
                            ,@5CustomerOpinion,@5Status,@5Remark,@5CorporationId,@5CorporationName,@5CreatorId
                            ,@5CreatorName,@5CreateTime,@5ModifierId,@5ModifierName,@5ModifyTime,@5ApproverId
                            ,@5ApproverName,@5ApproveTime,@5Comment,@5RepairAccountingOrderId,@5RepairContractId
                            ,@5RepairContractCode,@5WorkingFee,@5WorkingFeeDiscount,@5MaterialFee,@5MaterialFeeDiscount
                            ,@5AuxiliaryPartsFee,@5OutServiceFee,@5OtherFee,@5Total,@5TotalDiscounted,@5NextMaintanceMileage
                            ,@5NextMaintanceDate,@5OBJID   
        while(@@fetch_status=0) --数据集是否有数据
        Begin        
                    --作废状态的结算单
                    if @5status = 0
                    begin
            set @i = null
                        select @i = c.objid from trepcontracts a  with(nolock) 
                                    inner join tRepSetRepConLinks b  with(nolock)  on a.objid=b.repcontractid 
                                    inner join trepsettles  c  with(nolock)  on c.objid=b.repsettleid 
                                    where upper(a.code)=upper(@1Code) and upper(c.code)=upper(@5Code) and c.IsValid=1
                        if @i is null
                        begin
              set @tCarCustomerID = null
              set @SupMaxRepTime = null
              set @NorMaxRepTime = null
              set @TSMaxNum = null
              set @PTMaxNum = null
                        
                            --删除作废的结算单关联单和作废结算单                           
                            delete tRepSetRepConLinks where RepSettleId=@i
              update tRepSettles set IsValid=0 where objid=@i
                            --索赔维修合同后续处理

                        end
                    end
                    --生效状态的结算单
                    else
                    begin
            set @tCarCustomerID = null
            set @SupMaxRepTime = null
            set @NorMaxRepTime = null
            set @TSMaxNum = null
            set @PTMaxNum = null
            set @SettlesGuid = null
            set @IsValid = null
            set @PayMode = null
            set @i = null
                    
                        select top 1 @i = a.objid from trepcontracts a  with(nolock) 
                     inner join tRepSetRepConLinks b with(nolock)  on a.objid=b.repcontractid 
                     inner join trepsettles  c with(nolock)  on c.objid=b.repsettleid 
                     where a.code=@1Code and c.code=@5Code and c.IsValid=1
                        if @i is null
                        begin                        
                            set @SettlesGuid = newid()
                            if @5Status not in (0,2)
                RAISERROR('无效结算单状态[%s]',16,1,@5Status) 
              else
                set @IsValid = case when @5Status = 0 then 0 else 1 end 
              if @5PaymentType not in (0,1,2,3,4,5,6) 
                RAISERROR('无效付款方式[%s]',16,1,@5PaymentType) 
              else
                set @PayMode = case @5PaymentType when 0 then 1 when 1 then 2 when 2 then 3 when 3 then 4 when 4 then 5 when 5 then 6 when 6 then 7  end 
                
                            --新增结算单
                            INSERT INTO tRepSettles (ObjID,Code,EnterCode
               ,CreatorCode,CreateTime,IsValid
               ,PayMode,SettleDate,SerCode
               ,WorkDis ,MatDis
               ,WorkFee,MatFee ,AllFee
               ,Memo,NextBYRange
              ,NextBYTime,PayStatus ,YSAllFee)
               VALUES
               (@SettlesGuid,@5Code,'{' + cast(@EnterCode as varchar(50)) + '}'
               ,@5CreatorCode,@5CreateTime,@IsValid
               ,@PayMode,@5CreateTime,@5CreatorCode
               ,@5WorkingFeeDiscount,@5MaterialFeeDiscount
               ,@5WorkingFee,@5MaterialFee,@5TotalDiscounted
               ,@5CustomerOpinion,@5NextMaintanceMileage
               ,@5NextMaintanceDate,1,@5Total)
               
                            --新增结算单关联单
                            insert into tRepSetRepConLinks (ObjID,RepSettleID,RepContractID) values (newid(),@SettlesGuid,@tRepGuid)
              
                                
                            
                        end
                    end
                    
          --循环游标每条数据
          fetch next from RepairAccountingOrderCursor into @5CreatorCode,@5Id,@5Code,@5CustomerId,@5CustomerName,@5WorkingFeeSum,@5MaterialFeeSum
                              ,@5AuxiliaryPartsFeeSum,@5OutServiceFeeSum,@5OtherFeeSum,@5AccountingAmount,@5PaymentType
                              ,@5CustomerOpinion,@5Status,@5Remark,@5CorporationId,@5CorporationName,@5CreatorId
                              ,@5CreatorName,@5CreateTime,@5ModifierId,@5ModifierName,@5ModifyTime,@5ApproverId
                              ,@5ApproverName,@5ApproveTime,@5Comment,@5RepairAccountingOrderId,@5RepairContractId
                              ,@5RepairContractCode,@5WorkingFee,@5WorkingFeeDiscount,@5MaterialFee,@5MaterialFeeDiscount
                              ,@5AuxiliaryPartsFee,@5OutServiceFee,@5OtherFee,@5Total,@5TotalDiscounted,@5NextMaintanceMileage
                              ,@5NextMaintanceDate,@5OBJID 
                end 
                --关闭游标
        CLOSE RepairAccountingOrderCursor
        DEALLOCATE RepairAccountingOrderCursor
                /*  by  py20150115  里程更新由WSS传入SERVIICE，此处屏蔽修改
                --更改车辆里程
                set @Range = null
                select top 1 @Range = Range from tCarCustomers with(nolock)  where serialcode=@1Vin
                if @Range is not null
                begin
                    if @1Mileage > @Range
                    begin                        
                        update tCarCustomers set Range=@1Mileage where serialcode=@1Vin
                    end
                end  
                */     
      end
      
      update TempRepairContract set IsWin = 1 where Objid = @1Objid
            
      COMMIT TRANSACTION  --提交事务
    End Try
    Begin Catch 
      select @j = COUNT(*) from MASTER.dbo.syscursors where cursor_name='RepairAccountingOrderCursor'  
      if @j > 0 
      begin
        CLOSE RepairAccountingOrderCursor
        DEALLOCATE RepairAccountingOrderCursor
      end
      
      select @j = COUNT(*) from MASTER.dbo.syscursors where cursor_name='RepMaterialsCursor'  
      if @j > 0 
      begin
        CLOSE RepMaterialsCursor
        DEALLOCATE RepMaterialsCursor
      end
      
      select @j = COUNT(*) from MASTER.dbo.syscursors where cursor_name='RepItemsCursor'  
      if @j > 0 
      begin
        CLOSE RepItemsCursor  
        DEALLOCATE RepItemsCursor
      end
      
      select @j = COUNT(*) from MASTER.dbo.syscursors where cursor_name='OldSparePartCursor'  
      if @j > 0 
      begin
        CLOSE OldSparePartCursor
        DEALLOCATE OldSparePartCursor
      end
        
      ROLLBACK TRANSACTION  --事务回滚语句
      INSERT INTO SyncLog (SyncType,SyncCode,SyncLog,SyncDate) VALUES('RepairContract',@1Code,ERROR_MESSAGE(),GETDATE())
    End Catch  
    
    --循环游标每条数据
    fetch next from RepairContractCursor into @1ID,@1Code,@1ServiceAdvisorCode,@1CreatorCode,@1ServiceAdvisorName,@1Customercode,@1CustomerName
                      ,@1CellNumber,@1CompletionTime,@1Address,@1ZipCode,@1Vin,@1EngineCode,@1PlateNumber,@1VehicleModelCode
                      ,@1VehicleCategoryCode,@1VehicleModelName,@1Mileage,@1RepairType,@1RepairTime,@1StartRepairTime
                      ,@1ExpectedFinishTime,@1ActualDeliveryTime,@1VehicleObjects,@1FuelAmount,@1Description,@1CustomerComplaint
                      ,@1WorkingFee,@1MaterialFee,@1AuxiliaryPartsFee,@1OutServiceFee,@1OtherFee,@1OtherFeeDescription,@1FaultDescription
                      ,@1CheckResults,@1FaultAnalyes,@1SettleProperty,@1MainOldPartID,@1MainOldPartCode,@1MainOldPartName
                      ,@1MainOldPartRepTime,@1IsRepWarn,@1IsReturnWarn,@1SumRepTime,@1HaulageFee,@1TrafficFee,@1RepTime
                      ,@1IsBookingService,@1IsOutService,@1RequisitionBillCode,@1TechnicalServiceActivityCode,@1Promise
                      ,@1ClaimStatus,@1Status,@1Remark,@1CorporationCode,@1CorporationName,@1CreatorId,@1CreatorName
                      ,@1CreateTime,@1ModifierName,@1ModifyTime,@1SyncNo,@1EnterCode,@1CustProp,@1DateOfRetail
                      ,@1ISWin,@1OBJID  
  End
  
  --关闭游标
  CLOSE RepairContractCursor
    DEALLOCATE RepairContractCursor
END
