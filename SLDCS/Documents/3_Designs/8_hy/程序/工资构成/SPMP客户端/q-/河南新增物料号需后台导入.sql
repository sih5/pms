---插入产品信息----1 行 
insert into Product (id,Code,Name,CanBeOrdered,IfPurchasable,IfOption,ProductLifeCycle,EngineCylinder,EmissionStandard,ColorCode,Color,
Status,CreatorName,CreateTime,UncommonVehicleModel,SpecialVehicleModel,ManualAutomatic,remark)
select  s_product.nextval, a.*  from (
select trim(a.物料号) as code,trim(a.物料号描述) as name, 0  CanBeOrdered,0 as IfPurchasable,0 as IfOption,2 as ProductLifeCycle,
a.排放 as EngineCylinder,a.排量 as EmissionStandard,trim(b.颜色码),trim(b.颜色),1 as Status,'Admin',sysdate,0 as UncommonVehicleModel,0 as SpecialVehicleModel,
case when instr(a.变速箱类型,'AMT',1,1)>0 then '手自动' when instr(a.变速箱类型,'AT',1,1)>0 then '自动' when instr(a.变速箱类型,'CVT',1,1)>0 
then '无极变速' else '手动' end ManualAutomatic,trim(a.物料组) as remark
from producttmp02 a
left  join  (select distinct *  from VehicleModelColorCodetmp) b on  SUBSTR(a.物料号,8,2) = trim(b.颜色码) and trim(a.车型名称)= trim(b.车型名称)
where trim(a.物料号) is not null and length(a.物料号)=15 ) a
where a.code not in (select code from product);



