@echo 正在安装虚拟串口控件及驱动......
regsvr32 VSPort.ocx
VSBSetup.exe
@echo+
@echo+
@echo 注：在有的机器上此时Driver installer会报一个读系统注册表错误，经测试未发现有影响正常运行的问题
DriverInstall.exe
@echo+
@echo 虚拟串口控件及驱动已安装完毕，按任意键退出
@pause