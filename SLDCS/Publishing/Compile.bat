@echo off
set ROOT=%CD%
set SOURCE=%ROOT%\..\Codes\DCS
set OUT=%ROOT%\Files
set OUT_TEMP=%OUT%\Temp
set MSB="C:\Program Files (x86)\MSBuild\12.0\bin\MSBuild.exe" /nologo /v:m /t:Rebuild /l:FileLogger,Microsoft.Build.Engine;logfile=Compile.log;verbosity=diagnostic /p:WarningLevel=0;Optimize=true;DebugType=pdbonly;DebugSymbols=false;BuildInParallel=true;DocumentationFile="" /t:rebuild

echo 正在删除旧产出物
REM 排除%OUT%目录下的.svn目录。
forfiles /P "%OUT%" /C "cmd /C if @file NEQ \".svn\" (if EXIST @PATH (if @ISDIR==TRUE (rmdir /S /Q @Path) else (del /S /Q @path)))"
echo 旧产出物删除完成
echo.

echo 正在编译 Oracle 版本
set OUT_ORACLE=%OUT%\Oracle
REM 由于其余项目均与Sunlight.Silverlight.Web项目相关，故在编译此项目时，MSBuild会自动编译相关项目。
%MSB% /p:DBMS=Oracle;Configuration=Release;OutputPath="%OUT_TEMP%";WebProjectOutputDir="%OUT_ORACLE%" "%SOURCE%\Sunlight.Silverlight.Web\Sunlight.Silverlight.Web.csproj"
if errorlevel 1 goto err
for /R "%OUT_ORACLE%\bin" %%f in (*.pdb *.xml) do (
    del "%%f"
)
xcopy /S /Y "%SOURCE%\Sunlight.Silverlight.Web\Client" "%OUT_ORACLE%\Client\"
del "%OUT_ORACLE%\Web.*.config"
rmdir /S /Q "%OUT_TEMP%"
set OUT_ORACLE=

echo * 所有项目编译完成 *
echo.
goto ok

:err
echo 
echo * 编译时出现错误，请检查错误信息，处理之后再继续 *
echo.
if [%1] == [/autoclose] exit /b 1
:ok
set MSB=
set OUT=
set OUT_BIN= 
set OUT_TEMP=
set ROOT=
if [%1] == [/autoclose] exit /b 0

pause
