--1.配件采购计划单 增加字段：品牌ID，品牌名称，初审审批意见，终审审批意见
alter table PartsPurchasePlan add PartsSalesCategoryId number(9);
alter table PartsPurchasePlan add PartsSalesCategoryName VARCHAR2(100);
alter table PartsPurchasePlan add ApproveMemo VARCHAR2(200);
alter table PartsPurchasePlan add CloseMemo VARCHAR2(200);