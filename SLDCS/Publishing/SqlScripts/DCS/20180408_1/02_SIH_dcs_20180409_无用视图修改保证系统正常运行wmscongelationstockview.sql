create or replace view wmscongelationstockview as
/*select d.Id                     as WmsStockId, --  WMS冻结库存Id
       d.BranchId               as BranchId, --  营销分公司Id
       d.BranchCode             as BranchCode, --  营销分公司编号
       d.BranchName             as BranchName, --  营销分公司名称
       d.StorageCenter          as StorageCenter, --  储运中心
       d.PartsSalesCategoryId   as PartsSalesCategoryId, --  配件销售类型Id
       d.PartsSalesCategoryCode as PartsSalesCategoryCode, --  配件销售类型编号
       d.PartsSalesCategoryName as PartsSalesCategoryName, --  配件销售类型名称
       d.SparePartId            as SparePartId, --  配件Id
       d.SparePartCode          as SparePartCode, --  配件编号
       d.SparePartName          as SparePartName, --  配件名称
       d.CongelationStockQty    as CongelationStockQty, --  冻结库存
       d.DisabledStock          as DisabledStock, --  不可用库存
       a.Id                     as WarehouseId, --  仓库Id
       a.StorageCompanyId       as StorageCompanyId --  仓储企业Id
  from Warehouse a
 inner join SalesUnitAffiWarehouse b
    on a.Id = b.WarehouseId
 inner join SalesUnit c
    on b.SalesUnitId = c.Id
 inner join WmsCongelationStock d
    on c.PartsSalesCategoryId = d.PartsSalesCategoryId
 where d.StorageCenter = a.storagecenter
   and d.Branchid = a.branchid
   and a.WmsInterface = 1
;*/

select null as WmsStockId, --  WMS冻结库存Id
       a.branchid as BranchId, --  营销分公司Id
       (select branch.code from dcs.branch where branch.id = a.branchid) as BranchCode, --  营销分公司编号
       (select branch.name from dcs.branch where branch.id = a.branchid) as BranchName, --  营销分公司名称
       a.storagecenter as StorageCenter, --  储运中心
       null as PartsSalesCategoryId, --  配件销售类型Id
       null as PartsSalesCategoryCode, --  配件销售类型编号
       null as PartsSalesCategoryName, --  配件销售类型名称
       null as SparePartId, --  配件Id
       null as SparePartCode, --  配件编号
       null as SparePartName, --  配件名称
       0 as CongelationStockQty, --  冻结库存
       0 as DisabledStock, --  不可用库存
       a.Id as WarehouseId, --  仓库Id
       a.StorageCompanyId as StorageCompanyId --  仓储企业Id
  from Warehouse a
 inner join SalesUnitAffiWarehouse b
    on a.Id = b.WarehouseId
 inner join SalesUnit c
    on b.SalesUnitId = c.Id
/*inner join WmsCongelationStock d
   on c.PartsSalesCategoryId = d.PartsSalesCategoryId
where d.StorageCenter = a.storagecenter
  and d.Branchid = a.branchid*/
 where a.WmsInterface = 1;
