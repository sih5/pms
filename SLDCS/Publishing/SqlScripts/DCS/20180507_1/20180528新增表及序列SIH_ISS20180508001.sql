create sequence S_PackingTask
/

create sequence S_PackingTaskDetail
/

/*==============================================================*/
/* Table: PackingTask                                           */
/*==============================================================*/
create table PackingTask  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   PartsInboundPlanCode VARCHAR2(50),
   PartsInboundCheckBillCode VARCHAR2(100),
   SparePartCode        VARCHAR2(50)                    not null,
   SparePartName        VARCHAR2(100)                   not null,
   PlanQty              NUMBER(9)                       not null,
   PackingQty           NUMBER(9),
   SourceCode           VARCHAR2(50),
   Status               NUMBER(9),
   CounterpartCompanyCode VARCHAR2(50),
   CounterpartCompanyName VARCHAR2(100),
   ExpectedPlaceDate    DATE,
   RecomPackingMaterial VARCHAR2(50),
   PhyPackingMaterial   VARCHAR2(50),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   WarehouseId          NUMBER(9),
   WarehouseCode        VARCHAR2(50),
   WarehouseName        VARCHAR2(100),
   PartsInboundPlanId   NUMBER(9),
   PartsInboundCheckBillId NUMBER(9),
   SparePartId          NUMBER(9),
   BatchNumber          VARCHAR2(50),
   RowVersion           TIMESTAMP,
   constraint PK_PACKINGTASK primary key (Id)
)
/

/*==============================================================*/
/* Table: PackingTaskDetail                                     */
/*==============================================================*/
create table PackingTaskDetail  (
   Id                   NUMBER(9)                       not null,
   PackingTaskId        NUMBER(9)                       not null,
   RecomPackingMaterial VARCHAR2(50),
   RecomQty             NUMBER(9),
   PhyPackingMaterial   VARCHAR2(50),
   PhyQty               NUMBER(9),
   constraint PK_PACKINGTASKDETAIL primary key (Id)
)
/