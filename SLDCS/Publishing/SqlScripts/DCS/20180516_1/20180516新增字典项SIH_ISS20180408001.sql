--配件采购计划状态
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PurchasePlanOrderStatus', '配件采购计划状态', 0, '作废', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PurchasePlanOrderStatus', '配件采购计划状态', 1, '新增', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PurchasePlanOrderStatus', '配件采购计划状态', 2, '提交', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PurchasePlanOrderStatus', '配件采购计划状态', 3, '初审通过', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PurchasePlanOrderStatus', '配件采购计划状态', 4, '终审通过', 1, 1);

--配件采购计划类型
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PurchasePlanOrderType', '配件采购计划类型', 1, '正常计划', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PurchasePlanOrderType', '配件采购计划类型', 2, '紧急计划', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PurchasePlanOrderType', '配件采购计划类型', 3, '特急计划', 1, 1);


Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PartsInboundPlan_Status', '配件入库计划状态', 5, '部分包装', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PartsInboundPlan_Status', '配件入库计划状态', 6, '包装完成', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PartsInboundPlan_Status', '配件入库计划状态', 7, '部分上架', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PartsInboundPlan_Status', '配件入库计划状态', 8, '上架完成', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PartsInboundPlan_Status', '配件入库计划状态', 9, '部分收货', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PartsInboundPlan_Status', '配件入库计划状态', 10, '收货完成', 1, 1);