-- 上架任务单 编码生成规则
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'PartsShelvesTask', 'PartsShelvesTask', 1, 'PST{CORPCODE}{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);


-- 入库实绩 编码生成规则
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'PartsInboundPerformance', 'PartsInboundPerformance', 1, 'PIF{CORPCODE}{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);

-- 配件采购计划 编码生成规则
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'PartsPurchasePlan', 'PartsPurchasePlan', 1, 'PL{CORPCODE}{DATE:yyyyMMdd}{SERIAL:000000}', 1, NULL);

--捡货任务单
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'PickingTask', 'PickingTask', 1, 'PKT{CORPCODE}{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);

--配件包装属性申请单 编码生成规则
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'PartsPackingPropertyApp', 'PartsPackingPropertyApp', 1, 'PPK{CORPCODE}{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);