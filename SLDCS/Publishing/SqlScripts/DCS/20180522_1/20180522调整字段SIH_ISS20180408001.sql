alter table PartsPurchasePlanDetail modify SuplierId null;
alter table PartsPurchasePlanDetail modify SuplierCode null;
alter table PartsPurchasePlanDetail modify SuplierName null;

alter table PartsPurchasePlan add WarehouseCode VARCHAR2(50);

alter table PartsOutboundPlan add OrderTypeId NUMBER(9);
alter table PartsOutboundPlan add OrderTypeName VARCHAR2(100);
alter table PartsOutboundPlan add ReceivingAddress VARCHAR2(200);

alter table PartsPurchasePlan drop column PlanType;
alter table PartsPurchasePlan add PartsPlanTypeId NUMBER(9);
alter table PartsPurchasePlan add AbandonComment VARCHAR2(200);