/*create sequence S_EnterprisePartsCost
/*/

/*==============================================================*/
/* Table: EnterprisePartsCost                                   */
/*==============================================================*/
create table EnterprisePartsCost  (
   Id                   NUMBER(9)                       not null,
   OwnerCompanyId       NUMBER(9)                       not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   SparePartId          NUMBER(9)                       not null,
   Quantity             NUMBER(9)                       not null,
   CostAmount           NUMBER(19,4)                    not null,
   CostPrice            NUMBER(19,4)                    not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   constraint PK_ENTERPRISEPARTSCOST primary key (Id)
)
/

create sequence S_BoxUpTask
/

create sequence S_BoxUpTaskDetail
/

/*==============================================================*/
/* Table: BoxUpTask                                             */
/*==============================================================*/
create table BoxUpTask  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   WarehouseId          NUMBER(9),
   WarehouseCode        VARCHAR2(50),
   WarehouseName        VARCHAR2(100),
   CounterpartCompanyId NUMBER(9),
   CounterpartCompanyCode VARCHAR2(50),
   CounterpartCompanyName VARCHAR2(100),
   Status               NUMBER(9),
   IsExistShippingOrder NUMBER(1),
   ContainerNumber      VARCHAR2(50),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   BoxUpFinishTime      DATE,
   constraint PK_BOXUPTASK primary key (Id)
)
/

/*==============================================================*/
/* Table: BoxUpTaskDetail                                       */
/*==============================================================*/
create table BoxUpTaskDetail  (
   Id                   NUMBER(9)                       not null,
   BoxUpTaskId          NUMBER(9)                       not null,
   BoxUpTaskCode        VARCHAR2(50),
   PartsOutboundPlanId  NUMBER(9),
   PartsOutboundPlanCode VARCHAR2(50),
   PickingTaskId        NUMBER(9),
   PickingTaskCode      VARCHAR2(50),
   SourceCode           VARCHAR2(50),
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50)                    not null,
   SparePartName        VARCHAR2(100),
   SihCode              VARCHAR2(50),
   PlanQty              NUMBER(9),
   BoxUpQty             NUMBER(9),
   constraint PK_BOXUPTASKDETAIL primary key (Id)
)
/