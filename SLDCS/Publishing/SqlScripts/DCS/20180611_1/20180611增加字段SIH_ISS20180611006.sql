alter table PartsInboundPlanDetail add BatchNumber VARCHAR2(50);

alter table PackingTask drop column RowVersion;
alter table PartsShelvesTask drop column RowVersion;
alter table PickingTask drop column RowVersion;

alter table PickingTask add ResponsiblePersonId  NUMBER(9);
alter table PickingTask add ResponsiblePersonName VARCHAR2(100);

alter table PartsShelvesTask add Remark VARCHAR2(200);