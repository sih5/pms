alter table PartsShippingOrderDetail add PartsOutboundPlanId  NUMBER(9);
alter table PartsShippingOrderDetail add PartsOutboundPlanCode VARCHAR2(50);
alter table PartsShippingOrderDetail add TaskId NUMBER(9);
alter table PartsShippingOrderDetail add TaskType NUMBER(9);

alter table PartsShelvesTask add ShelvesFinishTime DATE;

create sequence S_PartsOutboundPerformance
/

/*==============================================================*/
/* Table: PartsOutboundPerformance                              */
/*==============================================================*/
create table PartsOutboundPerformance  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50),
   OutType              NUMBER(9),
   BarCode              VARCHAR2(50),
   WarehouseAreaId      NUMBER(9),
   Qty                  NUMBER(9),
   OperaterId           NUMBER(9),
   OperaterName         VARCHAR2(100),
   OperaterBeginTime    DATE,
   OperaterEndTime      DATE,
   Equipment            NUMBER(9),
   ContainerNumber      VARCHAR2(50),
   constraint PK_PARTSOUTBOUNDPERFORMANCE primary key (Id)
)
/