--装箱单号模板
insert into CodeTemplate (Id, Name, Entityname, Isactived, Template, resettype) values(S_codetemplate.Nextval,'BoxUpTask','BoxUpTask',1,'BU{CORPCODE}{DATE:yyyyMMdd}{SERIAL:000000}',1);

create sequence S_AccountPeriod
/

/*==============================================================*/
/* Table: AccountPeriod                                         */
/*==============================================================*/
create table AccountPeriod  (
   Id                   NUMBER(9)                       not null,
   Year                 VARCHAR2(20),
   Month                VARCHAR2(20),
   Status               NUMBER(9),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   constraint PK_ACCOUNTPERIOD primary key (Id)
)
/