--1.配件采购退货管理 :退货原因取消积压件
delete from KeyValueItem where Name='PartsPurReturnOrder_ReturnReason' and Key=2 and Value='积压件退货';

--2.配件采购退货管理 :和多界面共用参数，重新新增

Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PartsPurReturnOrder_Status', '配件采购退货单状态', 1, '新建', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PartsPurReturnOrder_Status', '配件采购退货单状态', 3, '初审通过', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PartsPurReturnOrder_Status', '配件采购退货单状态', 2, '终审通过', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PartsPurReturnOrder_Status', '配件采购退货单状态', 99, '作废', 1, 1);