Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PartsSalesReturnBill_Status', '配件销售退货单状态', 5, '初审通过', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PartsSalesReturnBill_Status', '配件销售退货单状态', 6, '终审通过', 1, 1);

Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PartsSalesReturn_ReturnType', '配件销售退货单_退货类型', 1, '正常退货', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PartsSalesReturn_ReturnType', '配件销售退货单_退货类型', 2, '保底退货', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PartsSalesReturn_ReturnType', '配件销售退货单_退货类型', 3, '特殊退货', 1, 1);
update keyvalueitem set value = '审核通过' where name = 'PartsSalesReturnBill_Status' and key = 3;