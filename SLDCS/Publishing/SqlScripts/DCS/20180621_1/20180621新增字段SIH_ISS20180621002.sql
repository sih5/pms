alter table PartsSalesPriceChangeDetail add CenterPrice NUMBER(19,4);
alter table PartsSalesPriceChangeDetail add CenterPriceRatio NUMBER(15,6);