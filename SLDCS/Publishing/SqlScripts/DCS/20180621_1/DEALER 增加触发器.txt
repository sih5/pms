create or replace trigger dealer_Trg
   after insert  on dealer
   for each row
begin
  if :old.id is null then
    insert into customerinformation(id,salescompanyid,customercompanyid,status,creatorid,creatorname,createtime)
select s_customerinformation.nextval,a.id,:new.id,1,1,:new.creatorname,sysdate
from branch a 
left join  customerinformation tmp on tmp.customercompanyid=:new.id and tmp.salescompanyid= a.id
where tmp.id is null;
  end if;
end dealer_Trg;

create or replace trigger agency_Trg
   after insert  on agency
   for each row
begin
  if :old.id is null then
    insert into customerinformation(id,salescompanyid,customercompanyid,status,creatorid,creatorname,createtime)
select s_customerinformation.nextval,a.id,:new.id,1,1,:new.creatorname,sysdate
from branch a 
left join  customerinformation tmp on tmp.customercompanyid=:new.id and tmp.salescompanyid= a.id
where tmp.id is null;
  end if;
end agency_Trg;



create or replace trigger logisticcompany_Trg
   after insert  on logisticcompany
   for each row
begin
  if :old.id is null then
    insert into customerinformation(id,salescompanyid,customercompanyid,status,creatorid,creatorname,createtime)
select s_customerinformation.nextval,a.id,:new.id,1,1,:new.creatorname,sysdate
from branch a 
left join  customerinformation tmp on tmp.customercompanyid=:new.id and tmp.salescompanyid= a.id
where tmp.id is null;
  end if;
end logisticcompany_Trg;