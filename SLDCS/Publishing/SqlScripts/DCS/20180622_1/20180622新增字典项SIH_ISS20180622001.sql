Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'SalesOrderType_SettleType', '配件销售订单类型_结算类型', 1, '销售结算', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'SalesOrderType_SettleType', '配件销售订单类型_结算类型', 2, '出口结算', 1, 1);

Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'SalesOrderType_BusinessType', '配件销售订单类型_业务类型', 1, '国内销售', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'SalesOrderType_BusinessType', '配件销售订单类型_业务类型', 2, '集团客户销售', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'SalesOrderType_BusinessType', '配件销售订单类型_业务类型', 3, '三包垫件', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'SalesOrderType_BusinessType', '配件销售订单类型_业务类型', 4, '国内赠送', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'SalesOrderType_BusinessType', '配件销售订单类型_业务类型', 5, '出口IV', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'SalesOrderType_BusinessType', '配件销售订单类型_业务类型', 6, '出口其他', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'SalesOrderType_BusinessType', '配件销售订单类型_业务类型', 7, '6K1', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'SalesOrderType_BusinessType', '配件销售订单类型_业务类型', 8, '中国IV', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'SalesOrderType_BusinessType', '配件销售订单类型_业务类型', 9, 'CKD', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'SalesOrderType_BusinessType', '配件销售订单类型_业务类型', 10, '6k2', 1, 1);