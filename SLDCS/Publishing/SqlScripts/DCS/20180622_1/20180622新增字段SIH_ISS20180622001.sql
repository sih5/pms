alter table CustomerTransferBill add InvoiceDate DATE;
alter table PaymentBill add InvoiceDate DATE;
alter table PartsRequisitionSettleBill add InvoiceDate DATE;
alter table PartsPurchaseRtnSettleBill add InvoiceDate DATE;
alter table PartsPurchaseSettleBill add InvoiceDate DATE;

alter table PartsSalesOrderType add SettleType NUMBER(9);
alter table PartsSalesOrderType add BusinessType NUMBER(9);
alter table PartsSalesSettlement add SettleType NUMBER(9);
alter table PartsSalesSettlement add BusinessType NUMBER(9);

alter table PartsPurchasePricingChange add ApprovalComment VARCHAR2(200);
alter table PartsPurchasePricingDetail add IsPrimary NUMBER(1);
alter table PartsPurchasePricingDetail add MinPurchasePrice NUMBER(19,4);
alter table PartsPurchasePricingDetail add MinPriceSupplierId NUMBER(9);
alter table PartsPurchasePricingDetail add MinPriceSupplier VARCHAR2(100);
alter table PartsPurchasePricingDetail add IsPrimaryMinPrice NUMBER(1);

alter table PartsPurchaseOrderDetail add PriceType NUMBER(9);