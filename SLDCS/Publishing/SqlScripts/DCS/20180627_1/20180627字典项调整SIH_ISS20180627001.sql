delete from KeyValueItem where name = 'PartsClaimBill_StatusNew';

Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PartsClaimBill_StatusNew', '配件索赔单单据状态新', 5, '新增', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PartsClaimBill_StatusNew', '配件索赔单单据状态新', 10, '待配件公司审核', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PartsClaimBill_StatusNew', '配件索赔单单据状态新', 11, '待中心库审核', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PartsClaimBill_StatusNew', '配件索赔单单据状态新', 12, '中心库审核通过', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PartsClaimBill_StatusNew', '配件索赔单单据状态新', 13, '审核通过', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PartsClaimBill_StatusNew', '配件索赔单单据状态新', 14, '初审通过', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PartsClaimBill_StatusNew', '配件索赔单单据状态新', 15, '终审通过', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PartsClaimBill_StatusNew', '配件索赔单单据状态新', 20, '服务站已发运', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PartsClaimBill_StatusNew', '配件索赔单单据状态新', 25, '区域已收货', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PartsClaimBill_StatusNew', '配件索赔单单据状态新', 26, '区域已发运', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PartsClaimBill_StatusNew', '配件索赔单单据状态新', 30, '入库完成', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PartsClaimBill_StatusNew', '配件索赔单单据状态新', 98, '终止', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PartsClaimBill_StatusNew', '配件索赔单单据状态新', 99, '作废', 1, 1);