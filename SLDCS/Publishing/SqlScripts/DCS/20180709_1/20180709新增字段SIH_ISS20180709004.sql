alter table SalesUnit add SubmitCompanyId NUMBER(9);
alter table InternalAllocationBill add Type NUMBER(9);
alter table PartsRequisitionSettleBill add Type NUMBER(9);
alter table PickingTask add BranchId NUMBER(9);

/*
create sequence S_SAP_YX_InvoiceverAccountInfo
/
*/

/*==============================================================*/
/* Table: SAP_YX_InvoiceverAccountInfo                          */
/*==============================================================*/
create table SAP_YX_InvoiceverAccountInfo  (
   Id                   NUMBER(9)                       not null,
   BUKRS                VARCHAR2(4),
   GJAHR                NUMBER(9),
   MONAT                NUMBER(9),
   AccountingInvoiceNumber VARCHAR2(50),
   Code                 VARCHAR2(50),
   InvoiceAmount        NUMBER(19,4),
   InvoiceTax           NUMBER(19,4),
   InvoiceDate          date,
   InvoiceCode          VARCHAR2(50),
   InvoiceNumber        VARCHAR2(50),
   ReversalInvoiceNumber VARCHAR2(50),
   ZGZFS                VARCHAR2(1),
   CreateTime           DATE,
   Status               NUMBER(9),
   ModifyTime           DATE,
   Message              VARCHAR2(500),
   constraint PK_SAP_YX_INVOICEVERACCOUNTINF primary key (Id)
)
/

alter table PartsSalesOrderDetail add MInSaleingAmount NUMBER(9);

alter table BoxUpTaskDetail add ContainerNumber VARCHAR2(50);

alter table BoxUpTaskDetail add PickingTaskDetailId NUMBER(9);