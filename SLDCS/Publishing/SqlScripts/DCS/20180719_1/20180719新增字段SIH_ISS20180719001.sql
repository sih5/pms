alter table PartsPurReturnOrder add InitialApproverId NUMBER(9);
alter table PartsPurReturnOrder add InitialApproverName VARCHAR2(100);
alter table PartsPurReturnOrder add InitialApproveTime DATE;
alter table PartsPurReturnOrder add InitialApproverComment VARCHAR2(200);
alter table PartsPurReturnOrder add ApprovertComment VARCHAR2(200);