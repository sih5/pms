create sequence S_BottomStock
/

/*==============================================================*/
/* Table: BottomStock                                           */
/*==============================================================*/
create table BottomStock  (
   Id                   NUMBER(9)                       not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   PartsSalesCategoryName VARCHAR2(100),
   CompanyID            NUMBER(9)                       not null,
   CompanyCode          VARCHAR2(50),
   CompanyName          VARCHAR2(100),
   CompanyType          NUMBER(9),
   WarehouseID          NUMBER(9),
   WarehouseCode        VARCHAR2(50),
   WarehouseName        VARCHAR2(100),
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50),
   SparePartName        VARCHAR2(100),
   StockQty             NUMBER(9),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   Remark               VARCHAR2(200),
   Status               NUMBER(9),
   constraint PK_BOTTOMSTOCK primary key (Id)
)
/