alter table PartsPurchaseOrderDetail add PriceTypeName VARCHAR2(20);
alter table PartsPurchaseOrderDetail add ABCStrategy VARCHAR2(10);

alter table PartsPurchasePlanDetail add PriceTypeName VARCHAR2(20);
alter table PartsPurchasePlanDetail add ABCStrategy VARCHAR2(10);

alter table PartsSalesOrderDetail add PriceTypeName VARCHAR2(20);
alter table PartsSalesOrderDetail add ABCStrategy VARCHAR2(10);
alter table PartsSalesOrderDetail drop column IncreaseRateGroupId;
alter table PartsSalesOrderDetail drop column ABCStrategyId;

alter table CustomerAccountHisDetail add SerialType NUMBER(9);

alter table PartsShiftOrder add ModifierId NUMBER(9);
alter table PartsShiftOrder add ModifierName VARCHAR2(100);
alter table PartsShiftOrder add ModifyTime DATE;
alter table PartsShiftOrder add ApproverId NUMBER(9);
alter table PartsShiftOrder add ApproverName VARCHAR2(100);
alter table PartsShiftOrder add ApproveTime DATE;

alter table PaymentBill add Drawer VARCHAR2(100);
alter table PaymentBill add IssueDate DATE;
alter table PaymentBill add DueDate DATE;
alter table PaymentBill add PayBank VARCHAR2(100);
alter table PaymentBill add MoneyOrderNum VARCHAR2(50);

alter table PartsSalesPriceChangeDetail add PriceTypeName VARCHAR2(50);