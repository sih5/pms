Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'BorrowBill_Type', '借用单类型', 1, '生产借用', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'BorrowBill_Type', '借用单类型', 2, '销售借用', 1, 1);

Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'BorrowBill_Status', '借用单状态', 1, '新增', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'BorrowBill_Status', '借用单状态', 2, '提交', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'BorrowBill_Status', '借用单状态', 3, '初审通过', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'BorrowBill_Status', '借用单状态', 4, '终审通过', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'BorrowBill_Status', '借用单状态', 5, '已出库', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'BorrowBill_Status', '借用单状态', 6, '归还待确认', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'BorrowBill_Status', '借用单状态', 7, '部分确认', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'BorrowBill_Status', '借用单状态', 8, '确认完成', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'BorrowBill_Status', '借用单状态', 99, '作废', 1, 1);