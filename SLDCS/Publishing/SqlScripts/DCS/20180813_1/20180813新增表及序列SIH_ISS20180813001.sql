create sequence S_BorrowBill
/

create sequence S_BorrowBillDetail
/

/*==============================================================*/
/* Table: BorrowBill                                            */
/*==============================================================*/
create table BorrowBill  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   PartsSalesCategoryName VARCHAR2(100),
   WarehouseId          NUMBER(9),
   WarehouseCode        VARCHAR2(50),
   WarehouseName        VARCHAR2(100),
   BorrowDepartmentName VARCHAR2(100),
   BorrowName           VARCHAR2(100),
   ContactMethod        VARCHAR2(100),
   Purpose              VARCHAR2(100),
   Type                 NUMBER(9),
   ExpectReturnTime     DATE,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   SubmitterId          NUMBER(9),
   SubmitterName        VARCHAR2(100),
   SubmitTime           DATE,
   InitialApproverId    NUMBER(9),
   InitialApproverName  VARCHAR2(100),
   InitialApproveTime   DATE,
   ApproverId           NUMBER(9),
   ApproverName         VARCHAR2(100),
   ApproveTime          DATE,
   OutBoundTime         DATE,
   Status               NUMBER(9),
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   Remark               VARCHAR2(200),
   constraint PK_BORROWBILL primary key (Id)
)
/

/*==============================================================*/
/* Table: BorrowBillDetail                                      */
/*==============================================================*/
create table BorrowBillDetail  (
   Id                   NUMBER(9)                       not null,
   BorrowBillId         NUMBER(9)                       not null,
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50),
   SparePartName        VARCHAR2(100),
   SihCode              VARCHAR2(50),
   MeasureUnit          VARCHAR2(50),
   WarehouseAreaCode    VARCHAR2(50),
   BorrowQty            NUMBER(9),
   OutboundAmount       NUMBER(9),
   ConfirmingQty        NUMBER(9),
   ConfirmedQty         NUMBER(9),
   constraint PK_BORROWBILLDETAIL primary key (Id)
)
/