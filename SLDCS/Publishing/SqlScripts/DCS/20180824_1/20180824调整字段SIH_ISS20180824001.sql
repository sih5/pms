alter table PartsSalesReturnBill drop column PartsSalesOrderId;
alter table PartsSalesReturnBill drop column OriginalRequirementBillId;
alter table PartsSalesReturnBill drop column OriginalRequirementBillType;
alter table PartsSalesReturnBill drop column PartsSalesOrderCode;

alter table PartsSalesReturnBillDetail add PartsSalesOrderId NUMBER(9);
alter table PartsSalesReturnBillDetail add OriginalRequirementBillId NUMBER(9);
alter table PartsSalesReturnBillDetail add OriginalRequirementBillType NUMBER(9);
alter table PartsSalesReturnBillDetail add PartsSalesOrderCode VARCHAR2(50);