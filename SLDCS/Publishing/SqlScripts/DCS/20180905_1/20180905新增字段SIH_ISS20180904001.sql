alter table Company add BusinessLinkName VARCHAR2(100);
alter table Company add BusinessContactMethod VARCHAR2(100);

alter table PartsShiftOrder add InitialApproverId NUMBER(9);
alter table PartsShiftOrder add InitialApproverName VARCHAR2(100);
alter table PartsShiftOrder add InitialApproveTime DATE;
alter table PartsShiftOrder add QuestionType NUMBER(9);

alter table PartsPurchaseOrder add Path VARCHAR2(2000);

alter table PartsPurchasePlan add Path VARCHAR2(2000);

alter table AccountGroup add SalesCompanyName VARCHAR2(100);

update AccountGroup t
   set t.salescompanyname =
       (select c.name
          from security.enterprise c
         where c.id = t.salescompanyid)
 where exists
 (select 1 from security.enterprise c where c.id = t.salescompanyid);