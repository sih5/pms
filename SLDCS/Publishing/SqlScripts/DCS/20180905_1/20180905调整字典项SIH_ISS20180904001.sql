delete from keyvalueitem  where name = 'ShortPickingReason';

Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'ShortPickingReason', '拣货任务单短拣原因', 1, '不符最小包装', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'ShortPickingReason', '拣货任务单短拣原因', 2, '借条', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'ShortPickingReason', '拣货任务单短拣原因', 3, '损坏', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'ShortPickingReason', '拣货任务单短拣原因', 4, '包装问题', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'ShortPickingReason', '拣货任务单短拣原因', 5, '呆滞', 1, 1);


Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PartsShiftOrder_QuestionType', '配件移库单_移库问题类型', 1, '装箱差异', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PartsShiftOrder_QuestionType', '配件移库单_移库问题类型', 2, '质量问题', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PartsShiftOrder_QuestionType', '配件移库单_移库问题类型', 3, '技术问题', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PartsShiftOrder_QuestionType', '配件移库单_移库问题类型', 4, '仓储管理', 1, 1);