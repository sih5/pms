alter table PartsBranchPackingProp add IsPrintPartStandard NUMBER(9);
alter table PartsBranchPackingProp add IsBoxStandardPrint NUMBER(9);
alter table PartsBranchPackingProp add PackingMaterialName VARCHAR2(100);

alter table PartsPackingPropAppDetail add IsPrintPartStandard NUMBER(9);
alter table PartsPackingPropAppDetail add IsBoxStandardPrint NUMBER(9);
alter table PartsPackingPropAppDetail add PackingMaterialName VARCHAR2(100);