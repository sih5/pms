create sequence S_CenterAuthorizedProvince
/

/*==============================================================*/
/* Table: CenterAuthorizedProvince                              */
/*==============================================================*/
create table CenterAuthorizedProvince  (
   Id                   NUMBER(9)                       not null,
   AgencyId             NUMBER(9)                       not null,
   ProvinceId           NUMBER(9)                       not null,
   ProvinceName         VARCHAR2(100),
   constraint PK_CENTERAUTHORIZEDPROVINCE primary key (Id)
)
/