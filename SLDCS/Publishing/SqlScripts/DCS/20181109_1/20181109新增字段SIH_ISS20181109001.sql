alter table PartsPurchasePricingDetail add IfPurchasable NUMBER(1);

alter table DealerPartsSalesReturnBill add SourceId NUMBER(9);
alter table DealerPartsSalesReturnBill add SourceCode VARCHAR2(50);

alter table PartsSalesOrder add AutoApproveStatus NUMBER(9);