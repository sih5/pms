create sequence S_CenterApproveBill
/

create sequence S_CenterSaleReturnStrategy
/

/*==============================================================*/
/* Table: CenterApproveBill                                     */
/*==============================================================*/
create table CenterApproveBill  (
   Id                   NUMBER(9)                       not null,
   CompanyId            NUMBER(9)                       not null,
   CompanyCode          VARCHAR2(50),
   CompanyName          VARCHAR2(100),
   MarketingDepartmentId NUMBER(9),
   ApproveWeek          VARCHAR2(50),
   Status               NUMBER(9),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   constraint PK_CENTERAPPROVEBILL primary key (Id)
)
/

/*==============================================================*/
/* Table: CenterSaleReturnStrategy                              */
/*==============================================================*/
create table CenterSaleReturnStrategy  (
   Id                   NUMBER(9)                       not null,
   TimeFrom             NUMBER(9),
   TimeTo               NUMBER(9),
   ReturnType           NUMBER(9),
   DiscountRate         NUMBER(15,6),
   Status               NUMBER(9),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   constraint PK_CENTERSALERETURNSTRATEGY primary key (Id)
)
/