alter table PartsPurchasePricingDetail add OldPriSupplierId NUMBER(9);
alter table PartsPurchasePricingDetail add OldPriSupplierCode VARCHAR2(50);
alter table PartsPurchasePricingDetail add OldPriSupplierName VARCHAR2(100);
alter table PartsPurchasePricingDetail add OldPriPrice NUMBER(19,4);
alter table PartsPurchasePricingDetail add OldPriPriceType NUMBER(9);