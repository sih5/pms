create sequence S_CenterSaleSettle
/

/*==============================================================*/
/* Table: CenterSaleSettle                                      */
/*==============================================================*/
create table CenterSaleSettle  (
   Id                   NUMBER(9)                       not null,
   SaleOrderCode        VARCHAR2(50),
   CenterId             NUMBER(9),
   CenterCode           VARCHAR2(50),
   CenterName           VARCHAR2(100),
   DealerId             NUMBER(9),
   DealerCode           VARCHAR2(50),
   DealerName           VARCHAR2(100),
   SparePartId          NUMBER(9),
   SparePartCode        VARCHAR2(50),
   SparePartName        VARCHAR2(100),
   RetailPrice          NUMBER(19,4),
   OrderQty             NUMBER(9),
   FirstApproveQty      NUMBER(9),
   ApproveTime          DATE,
   constraint PK_CENTERSALESETTLE primary key (Id)
)
/
