create sequence S_AssemblyPartRequisitionLink
/

/*==============================================================*/
/* Table: AssemblyPartRequisitionLink                           */
/*==============================================================*/
create table AssemblyPartRequisitionLink  (
   Id                   NUMBER(9)                       not null,
   KeyCode              NUMBER(9),
   PartId               NUMBER(9)                       not null,
   PartCode             VARCHAR2(50)                    not null,
   PartName             VARCHAR2(100)                   not null,
   Qty                  NUMBER(9),
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   constraint PK_ASSEMBLYPARTREQUISITIONLINK primary key (Id)
)
/

alter table SparePart add AssemblyKeyCode NUMBER(9);
alter table SparePartHistory add AssemblyKeyCode NUMBER(9);