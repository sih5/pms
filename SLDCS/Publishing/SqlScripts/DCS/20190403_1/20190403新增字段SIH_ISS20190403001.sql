alter table PartsTransferOrder add InitialApproverId NUMBER(9);
alter table PartsTransferOrder add InitialApproverName VARCHAR2(100);
alter table PartsTransferOrder add InitialApproveTime DATE;