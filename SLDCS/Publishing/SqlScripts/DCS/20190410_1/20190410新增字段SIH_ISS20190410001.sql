alter table DealerPartsInventoryBill add RejecterId NUMBER(9);
alter table DealerPartsInventoryBill add RejecterName VARCHAR2(100);
alter table DealerPartsInventoryBill add RejectTime DATE;
alter table DealerPartsInventoryBill add ApproveComment VARCHAR2(200);

alter table PartsInventoryBill add RejecterId NUMBER(9);
alter table PartsInventoryBill add RejecterName VARCHAR2(100);
alter table PartsInventoryBill add RejectTime DATE;