create sequence S_OverstockPartsPlatFormBill
/

create sequence S_OverstockPartsRecommendBill
/

/*==============================================================*/
/* Table: OverstockPartsPlatFormBill                            */
/*==============================================================*/
create table OverstockPartsPlatFormBill  (
   Id                   NUMBER(9)                       not null,
   CenterId             NUMBER(9),
   CenterName           VARCHAR2(100),
   DealerId             NUMBER(9),
   DealerCode           VARCHAR2(50),
   DealerName           VARCHAR2(100),
   SparePartId          NUMBER(9),
   SparePartCode        VARCHAR2(50),
   SparePartName        VARCHAR2(100),
   DiscountRate         NUMBER(15,6),
   DiscountedPrice      NUMBER(19,4),
   EntityStatus         VARCHAR2(100),
   Attachment           VARCHAR2(2000),
   Status               NUMBER(9),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   constraint PK_OVERSTOCKPARTSPLATFORMBILL primary key (Id)
)
/

/*==============================================================*/
/* Table: OverstockPartsRecommendBill                           */
/*==============================================================*/
create table OverstockPartsRecommendBill  (
   Id                   NUMBER(9)                       not null,
   CenterId             NUMBER(9),
   CenterName           VARCHAR2(100),
   DealerId             NUMBER(9),
   DealerCode           VARCHAR2(50),
   DealerName           VARCHAR2(100),
   SparePartId          NUMBER(9),
   SparePartCode        VARCHAR2(50),
   SparePartName        VARCHAR2(100),
   CreateTime           DATE,
   constraint PK_OVERSTOCKPARTSRECOMMENDBILL primary key (Id)
)
/
