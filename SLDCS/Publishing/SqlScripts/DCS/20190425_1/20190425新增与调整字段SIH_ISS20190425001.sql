alter table OverstockPartsPlatFormBill add WarehouseId NUMBER(9);
alter table OverstockPartsPlatFormBill add WarehouseCode VARCHAR2(50);
alter table OverstockPartsPlatFormBill add WarehouseName VARCHAR2(100);

alter table OverstockPartsRecommendBill add WarehouseId NUMBER(9);
alter table OverstockPartsRecommendBill add WarehouseCode VARCHAR2(50);
alter table OverstockPartsRecommendBill add WarehouseName VARCHAR2(100);

alter table OverstockTransferOrder modify TransferOutWarehouseId null;
alter table OverstockTransferOrder modify TransferOutWarehouseCode null;
alter table OverstockTransferOrder modify TransferOutWarehouseName null;