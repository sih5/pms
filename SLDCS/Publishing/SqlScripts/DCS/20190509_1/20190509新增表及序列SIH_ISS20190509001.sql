create sequence S_CenterABCBasis
/

/*==============================================================*/
/* Table: CenterABCBasis                                        */
/*==============================================================*/
create table CenterABCBasis  (
   Id                   NUMBER(9)                       not null,
   CenterId             NUMBER(9),
   CenterCode           VARCHAR2(50),
   CenterName           VARCHAR2(100),
   SparePartId          NUMBER(9),
   SparePartCode        VARCHAR2(50),
   SparePartName        VARCHAR2(100),
   SihCode              VARCHAR2(50),
   OldType              NUMBER(9),
   SalesPrice           NUMBER(19,4),
   SaleSubNumber        NUMBER(9),
   SaleSubAmount        NUMBER(19,4),
   SubAmountPercentage  NUMBER(15,6),
   SaleSubFrequency     NUMBER(9),
   SubFrequencyPercentage NUMBER(15,6),
   NewType              NUMBER(9),
   CreateTime           DATE,
   constraint PK_CENTERABCBASIS primary key (Id)
)
/