alter table PartsSalesPriceHistory add OldCenterPrice NUMBER(19,4);
alter table PartsSalesPriceHistory add OldRetailGuidePrice NUMBER(19,4);
alter table PartsSalesPriceHistory add OldSalesPrice NUMBER(19,4);