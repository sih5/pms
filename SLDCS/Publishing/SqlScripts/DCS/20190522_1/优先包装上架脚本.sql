create sequence S_AddPriorityPackingAndShelves;
create table AddPriorityPackingAndShelves  (
   Id                   NUMBER(9)                       not null,
   Priority number(1),
   PartsPurchaseOrderCode  VARCHAR2(100),
   InBoundCode VARCHAR2(100),
   SparePartCode VARCHAR2(100),
   ReferenceCode VARCHAR2(100),
   SparePartName VARCHAR2(100),
   InspectedQuantity NUMBER(9),
   PackingQty NUMBER(9),
   ShallPackingEntries NUMBER(9),
   ActualPackingEntries NUMBER(9),
   ShelvesAmount NUMBER(9),
   ShallShelvesEntries NUMBER(9),
   ActualShelvesEntries NUMBER(9),
   InBoundFinishTime DATE,
   PackingFinishTime DATE,
   PackingFinishStatus number(1),
   PriorityPackingTime DATE,
   ShelvesFinishTime DATE,
   ShelvesFinishStatus number(1),
   PriorityShelvesTime DATE,
   CreateTime DATE,
   constraint PK_PriorityPackingAndShelves primary key (Id)
);

create or replace procedure AddPriorityPackingShelvesInit as
begin
  delete AddPriorityPackingAndShelves where createtime < sysdate-100;
  
   insert into AddPriorityPackingAndShelves 
   select S_AddPriorityPackingAndShelves.Nextval,priority,originalrequirementbillcode,code,sparepartcode,referencecode,sparepartname,inspectedquantity,PackingQty,ShallPackingEntries,ActualPackingEntries,ShelvesAmount,
   ShallShelvesEntries,ActualShelvesEntries,createtime,ModifyTime,PackingFinishStatus,PriorityPackingTime,ShelvesFinishTime,ShelvesFinishStatus,PriorityShelvesTime,sysdate
    from (
   select distinct ppb.priority,picb.originalrequirementbillcode,picb.code,picbd.sparepartcode,s.referencecode,picbd.sparepartname,picbd.inspectedquantity,pt.PackingQty,
   nvl(picbd.inspectedquantity,0)/decode(s.MInPackingAmount,0,1,null,1,s.MInPackingAmount) as ShallPackingEntries,
   nvl(pt.PackingQty,0)/decode(s.MInPackingAmount,0,1,null,1,s.MInPackingAmount) as ActualPackingEntries,sum(pst.ShelvesAmount) as ShelvesAmount,
   nvl(pt.PackingQty,0)/decode(s.MInPackingAmount,0,1,null,1,s.MInPackingAmount) as ShallShelvesEntries,
   nvl(sum(pst.ShelvesAmount),0)/decode(s.MInPackingAmount,0,1,null,1,s.MInPackingAmount) as ActualShelvesEntries,picb.createtime,pt.ModifyTime,
   (case when ppb.PriorityCreateTime<pt.ModifyTime then pt.ModifyTime
  when ppb.PriorityCreateTime>pt.ModifyTime then ppb.PriorityCreateTime 
    else pt.ModifyTime end) as  PriorityPackingTime,
   (case when nvl(pt.PackingQty,0) =0 then 3 
  when nvl(picbd.inspectedquantity,0)>nvl(pt.PackingQty,0) then 2 
    else 1 end) as  PackingFinishStatus,
  max(pst.ShelvesFinishTime) as ShelvesFinishTime,
  (case when nvl(sum(pst.ShelvesAmount),0) = 0 then 3 
  when nvl(pt.PackingQty,0)>nvl(sum(pst.ShelvesAmount),0) then 2 
    else 1 end) as  ShelvesFinishStatus,
  (case when ppb.PriorityCreateTime<max(pst.ShelvesFinishTime) then max(pst.ShelvesFinishTime)
  when ppb.PriorityCreateTime>max(pst.ShelvesFinishTime) then ppb.PriorityCreateTime 
    else max(pst.ShelvesFinishTime) end) as  PriorityShelvesTime 
   from partsinboundcheckbill picb
   inner join  partsinboundcheckbilldetail picbd on picb.id =picbd.partsinboundcheckbillid
   inner join PartsPriorityBill ppb on ppb.partid=picbd.sparepartid and ppb.Priority is not null
   inner join sparepart s on s.id=picbd.sparepartid and s.Status=1
   inner join packingtask pt on pt.partsinboundcheckbillid = picb.id and pt.sparepartid=picbd.sparepartid
   left join PartsShelvesTask pst on pst.partsinboundcheckbillid = picb.id and pst.sparepartid=picbd.sparepartid
   where pt.status in(1,2) or pst.status in(1,2)
   group by ppb.priority,picb.originalrequirementbillcode,picb.code,picbd.sparepartcode,s.referencecode,picbd.sparepartname,picbd.inspectedquantity,pt.PackingQty,picbd.inspectedquantity,s.MInPackingAmount,
   pt.PackingQty,picb.createtime,pt.ModifyTime, ppb.PriorityCreateTime,pt.ModifyTime
   );
end;