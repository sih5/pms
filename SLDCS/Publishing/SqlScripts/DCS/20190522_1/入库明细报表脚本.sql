create sequence S_AddInboundDetail;
create table AddInboundDetail  (
   Id                   NUMBER(9)                       not null,
   PriorityCreateTime  DATE,
   InBoundPlanCode VARCHAR2(100),
   InboundPriority number(1),
   PartsPurchaseOrderCode  VARCHAR2(100),
   SparePartCode VARCHAR2(100),
   SupplierPartCode VARCHAR2(100),
   SparePartName VARCHAR2(100),
   ShippingAmount NUMBER(9),
   InspectedQuantity NUMBER(9),
   ShallInBoundQty NUMBER(9),
   ShallInBoundEntries NUMBER(9),
   ActualInBoundEntries NUMBER(9),
   ShippingDate DATE,
   PlanDeliveryTime DATE,
   InBoundFinishTime DATE,
   InBoundFinishStatus number(1),
   SupplierName VARCHAR2(100),
   CreateTime DATE,
   constraint PK_AddInboundDetail primary key (Id)
);

create or replace procedure AddInboundDetailInit as
begin
  delete AddInboundDetail where createtime < sysdate-100;
  
   insert into AddInboundDetail  
   select S_AddInboundDetail.Nextval,PriorityCreateTime,InBoundPlanCode,Priority,code,sparepartCode,ReferenceCode,sparepartName,shippingamount,inspectedquantity,ShallInBoundQty,ShallInBoundEntry ,ShallInBoundEntries,shippingdate,
   PlanDeliveryTime,createtime,InBoundFinishStatus,SupplierName,sysdate from (
     select distinct ppb.PriorityCreateTime,pip.code as InBoundPlanCode,ppb.Priority,ppo.code,pipd.sparepartCode,s.ReferenceCode,pipd.sparepartName,ppod.shippingamount, nvl(pipd.inspectedquantity,0) as inspectedquantity,nvl(pipd.inspectedquantity,0),(nvl(ppod.ShippingAmount,0) - nvl(pipd.inspectedquantity,0)) as ShallInBoundQty,
  (nvl(ppod.ShippingAmount,0) - nvl(pipd.inspectedquantity,0))/decode(s.MInPackingAmount,0,1,null,1,s.MInPackingAmount) as ShallInBoundEntry ,
  nvl(pipd.inspectedquantity,0)/decode(s.MInPackingAmount,0,1,null,1,s.MInPackingAmount) as ShallInBoundEntries,sso.shippingdate,
  sso.shippingdate + bsr.ArrivalCycle as PlanDeliveryTime,(select max(picb.createtime) from partsinboundcheckbill picb where picb.partsinboundplanid = pip.id) as createtime,(case when nvl(pipd.inspectedquantity,0)=0 then 3 
  when nvl(pipd.inspectedquantity,0)>0 and (nvl(ppod.ShippingAmount,0) - nvl(pipd.inspectedquantity,0))=0 then 1 
    else 2 end) as  InBoundFinishStatus,ppo.partssuppliername as SupplierName
  from partsinboundplan pip
  inner join partsinboundplandetail pipd on pip.id =pipd.partsinboundplanid
  inner join sparepart s on s.id=pipd.sparepartid and s.Status=1
  inner join PartsPriorityBill ppb on ppb.partid=pipd.sparepartid
  inner join PartsPurchaseOrder ppo on pip.originalrequirementbillcode = ppo.code
  inner join PartsPurchaseOrderdetail ppod on ppo.id=ppod.Partspurchaseorderid and ppod.sparepartid=pipd.sparepartid
  inner join SupplierShippingOrder sso on sso.partspurchaseorderid=ppo.id and sso.status=2
  inner join BranchSupplierRelation bsr  on ppo.partssupplierid = bsr.SupplierId and bsr.status = 1
  where pip.warehousename='˫���ܿ�' and pip.PlanDeliveryTime < to_date(to_char(SYSDATE,'yyyy-mm-dd'),'yyyy-mm-dd') and
  (nvl(ppod.ShippingAmount,0) - nvl(pipd.inspectedquantity,0)) >0 and pip.status!=8 and not exists (select 1 from AddInboundDetail where PartsPurchaseOrderCode =ppo.code));
end;
