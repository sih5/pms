create or replace procedure OverstockPartsRecommend as
      cursor CenterStock  is
       select ps.*,c.name as CompanyName,c.code as CompanyCode,c.type as CompanyType,s.code as SparePartCode,s.name as SparePartName,w.code as WarehouseCode,w.name as WarehouseName  from partsstock ps
       inner join company c on c.id=ps.StorageCompanyId
       inner join Warehouse w on w.id=ps.WarehouseId
       inner join SparePart s on s.id=ps.partid
       where c.type=3 and s.status=1 and
       exists(select 1 FROM  PartsInboundCheckBill picb
                     inner join partsinboundcheckbilldetail picbd on picb.id=picbd.partsinboundcheckbillid
                     where picb.createtime < sysdate-180 and picb.InboundType =1 and picb.warehouseid =ps.warehouseid
                     and picbd.sparepartid=ps.partid) and
       not exists(select 1 from OverstockPartsRecommendBill where CenterId =ps.storagecompanyid and SparePartid=ps.partid and warehouseid =ps.warehouseid) and
       exists(select 1 from WarehouseAreaCategory wac
                     where wac.id=ps.WarehouseAreaCategoryId and wac.category=1);

       cursor DealerStock is
       select dps.*,adr.agencyid as CenterId,adr.agencyname as CenterName,s.name as SparePartName from DealerPartsStock dps
       inner join AgencyDealerRelation adr on dps.dealerid = adr.dealerid
       inner join SparePart s on s.id=dps.SparePartId
       where s.status=1 and
       exists(select 1 FROM  PartsShippingOrder pso
                     inner join PartsShippingOrderDetail psod on pso.id=psod.PartsShippingOrderid
                     where pso.ConfirmedReceptionTime < sysdate-180 and pso.Type =1 and pso.ReceivingCompanyId =dps.dealerid
                     and psod.sparepartid=dps.sparepartid and pso.status=2) and
       not exists(select 1 from OverstockPartsRecommendBill where dealerid =dps.dealerid and SparePartid=dps.SparePartId and CenterId =adr.agencyid);
begin
    FOR c_CenterStock IN CenterStock
    LOOP
        if(c_CenterStock.CompanyType = 7) then
           insert into OverstockPartsRecommendBill
        values(s_overstockpartsrecommendbill.nextval,null,null,c_CenterStock.StorageCompanyId,c_CenterStock.CompanyCode,c_CenterStock.CompanyName
        ,c_CenterStock.PartId,c_CenterStock.SparePartCode,c_CenterStock.SparePartName,sysdate,c_CenterStock.warehouseid,c_CenterStock.warehousecode,c_CenterStock.warehousename);
           continue;
        end if;
        insert into OverstockPartsRecommendBill
        values(s_overstockpartsrecommendbill.nextval,c_CenterStock.StorageCompanyId,c_CenterStock.CompanyName,null,null,null
        ,c_CenterStock.PartId,c_CenterStock.SparePartCode,c_CenterStock.SparePartName,sysdate,c_CenterStock.warehouseid,c_CenterStock.warehousecode,c_CenterStock.warehousename);
    end loop;

    FOR c_DealerStock IN DealerStock
    LOOP
        insert into OverstockPartsRecommendBill
        values(s_overstockpartsrecommendbill.nextval,c_DealerStock.CenterId,c_DealerStock.CenterName,c_DealerStock.DealerId,c_DealerStock.DealerCode,c_DealerStock.DealerName
        ,c_DealerStock.SparePartId,c_DealerStock.SparePartCode,c_DealerStock.SparePartName,sysdate,null,null,null);
    end loop;
end;