create sequence S_CenterAccumulateDailyExp
/

create sequence S_SalesOrderDailyAvg
/

create sequence S_SalesOrderRecommendForce
/

create sequence S_SalesOrderRecommendWeekly
/

/*==============================================================*/
/* Table: CenterAccumulateDailyExp                              */
/*==============================================================*/
create table CenterAccumulateDailyExp  (
   Id                   NUMBER(9)                       not null,
   Year                 NUMBER(9),
   CenterId             NUMBER(9),
   CenterCode           VARCHAR2(50),
   CenterName           VARCHAR2(100),
   Type                 NUMBER(9),
   OrderCycle           NUMBER(9),
   ArrivalCycle         NUMBER(9),
   LogisticsCycle       NUMBER(9),
   ReserveCoefficient   NUMBER(15,6),
   StockUpperCoefficient NUMBER(15,6),
   StockUpperQty        NUMBER(9),
   PackingQty           NUMBER(9),
   UsableQty            NUMBER(9),
   OnLineQty            NUMBER(9),
   OweQty               NUMBER(9),
   SparePartId          NUMBER(9),
   SparePartCode        VARCHAR2(50),
   SparePartName        VARCHAR2(100),
   ExpectPlanQty        NUMBER(15,6),
   CreateTime           DATE,
   constraint PK_CENTERACCUMULATEDAILYEXP primary key (Id)
)
/

/*==============================================================*/
/* Table: SalesOrderDailyAvg                                    */
/*==============================================================*/
create table SalesOrderDailyAvg  (
   Id                   NUMBER(9)                       not null,
   Year                 NUMBER(9),
   Week                 NUMBER(9),
   CenterId             NUMBER(9),
   CenterCode           VARCHAR2(50),
   CenterName           VARCHAR2(100),
   WarehouseId          NUMBER(9),
   WarehouseCode        VARCHAR2(50),
   WarehouseName        VARCHAR2(100),
   Type                 NUMBER(9),
   SafeStockDays        NUMBER(9),
   ReserveCoefficient   NUMBER(15,6),
   StockUpperCoefficient NUMBER(15,6),
   PurchasePrice        NUMBER(19,4),
   PackingQty           NUMBER(9),
   UsableQty            NUMBER(9),
   OnLineQty            NUMBER(9),
   OweQty               NUMBER(9),
   SparePartId          NUMBER(9),
   SparePartCode        VARCHAR2(50),
   SparePartName        VARCHAR2(100),
   SixMonthBefAvgQty    NUMBER(15,6),
   T3WeekAvgQty         NUMBER(15,6),
   T2WeekAvgQty         NUMBER(15,6),
   T1WeekAvgQty         NUMBER(15,6),
   TWeekAvgQty          NUMBER(15,6),
   TWeekActualAvgQty    NUMBER(15,6),
   StandardQty          NUMBER(15,6),
   CreateTime           DATE,
   OrderCycle           NUMBER(9),
   ArrivalCycle         NUMBER(9),
   LogisticsCycle       NUMBER(9),
   constraint PK_SALESORDERDAILYAVG primary key (Id)
)
/

/*==============================================================*/
/* Table: SalesOrderRecommendForce                              */
/*==============================================================*/
create table SalesOrderRecommendForce  (
   Id                   NUMBER(9)                       not null,
   Year                 NUMBER(9),
   CenterId             NUMBER(9),
   CenterCode           VARCHAR2(50),
   CenterName           VARCHAR2(100),
   Type                 NUMBER(9),
   ForceReserveQty      NUMBER(9),
   StockUpperQty        NUMBER(9),
   ActualStockUpperQty  NUMBER(9),
   PackingQty           NUMBER(9),
   UsableQty            NUMBER(9),
   OnLineQty            NUMBER(9),
   OweQty               NUMBER(9),
   SparePartId          NUMBER(9),
   SparePartCode        VARCHAR2(50),
   SparePartName        VARCHAR2(100),
   RecommendQty         NUMBER(15,6),
   CreateTime           DATE,
   constraint PK_SALESORDERRECOMMENDFORCE primary key (Id)
)
/

/*==============================================================*/
/* Table: SalesOrderRecommendWeekly                             */
/*==============================================================*/
create table SalesOrderRecommendWeekly  (
   Id                   NUMBER(9)                       not null,
   Year                 NUMBER(9),
   Week                 NUMBER(9),
   CenterId             NUMBER(9),
   CenterCode           VARCHAR2(50),
   CenterName           VARCHAR2(100),
   Type                 NUMBER(9),
   OrderCycle           NUMBER(9),
   ArrivalCycle         NUMBER(9),
   LogisticsCycle       NUMBER(9),
   ReserveCoefficient   NUMBER(15,6),
   StockUpperCoefficient NUMBER(15,6),
   PackingQty           NUMBER(9),
   UsableQty            NUMBER(9),
   OnLineQty            NUMBER(9),
   OweQty               NUMBER(9),
   SparePartId          NUMBER(9),
   SparePartCode        VARCHAR2(50),
   SparePartName        VARCHAR2(100),
   RecommendQty         NUMBER(15,6),
   CreateTime           DATE,
   constraint PK_SALESORDERRECOMMENDWEEKLY primary key (Id)
)
/