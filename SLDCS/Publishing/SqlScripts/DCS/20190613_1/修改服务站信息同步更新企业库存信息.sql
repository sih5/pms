CREATE OR REPLACE TRIGGER Company_Update_Trg
   before  update on Company
   for each row
begin
  if :new.Type == 2 and :new.Name<>:old.Name then
    update dealerpartsstock set dealername =:new.Name where DealerName=:old.Name;
  end if;
end Company_Update_Trg;