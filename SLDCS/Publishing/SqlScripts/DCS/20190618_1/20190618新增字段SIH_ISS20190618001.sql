alter table InOutWorkPerHour add OutPersonNum NUMBER(9);
alter table InOutWorkPerHour add OutOrderLineNum NUMBER(9);
alter table InOutWorkPerHour add OutOrderItemNum NUMBER(9);
alter table InOutWorkPerHour add OutQty NUMBER(9);
alter table InOutWorkPerHour add OutFee NUMBER(19,4);