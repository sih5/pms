alter table CenterAccumulateDailyExp add ExpectPlanQtyAct NUMBER(15,6);
alter table CenterAccumulateDailyExp add CumulativeFst NUMBER(15,6);
alter table CenterAccumulateDailyExp add CenterSubmit NUMBER(15,6);

alter table PartsTransferOrder add Path VARCHAR2(2000);