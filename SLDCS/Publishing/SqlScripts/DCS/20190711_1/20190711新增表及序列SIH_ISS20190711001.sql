create sequence S_MultiLevelApproveConfig
/

/*==============================================================*/
/* Table: MultiLevelApproveConfig                               */
/*==============================================================*/
create table MultiLevelApproveConfig  (
   Id                   NUMBER(9)                       not null,
   Type                 NUMBER(9)                       not null,
   ApproverId           NUMBER(9)                       not null,
   ApproverName         VARCHAR2(100)                   not null,
   MinApproveFee        NUMBER(19,4),
   MaxApproveFee        NUMBER(19,4),
   IsFinished           NUMBER(1)                       not null,
   Status               NUMBER(9)                       not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   constraint PK_MULTILEVELAPPROVECONFIG primary key (Id)
)
/