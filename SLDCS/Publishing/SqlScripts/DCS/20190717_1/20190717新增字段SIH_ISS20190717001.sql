alter table PartsPurchasePlan add CheckerId NUMBER(9);
alter table PartsPurchasePlan add CheckerName VARCHAR2(100);
alter table PartsPurchasePlan add CheckTime DATE;
alter table PartsPurchasePlan add CheckMemo VARCHAR2(200);

alter table PartsPurchasePricingChange add InitialApproveComment VARCHAR2(200);
alter table PartsPurchasePricingChange add CheckerId NUMBER(9);
alter table PartsPurchasePricingChange add CheckerName VARCHAR2(100);
alter table PartsPurchasePricingChange add CheckTime DATE;
alter table PartsPurchasePricingChange add CheckComment VARCHAR2(200);

alter table PartsSalesPriceChange add InitialApproveComment VARCHAR2(200);
alter table PartsSalesPriceChange add ApproverId NUMBER(9);
alter table PartsSalesPriceChange add ApproverName VARCHAR2(100);
alter table PartsSalesPriceChange add ApproveTime DATE;
alter table PartsSalesPriceChange add ApproveComment VARCHAR2(200);
alter table PartsSalesPriceChange add FinalApproveComment VARCHAR2(200);

alter table SpecialTreatyPriceChange add ApproverId NUMBER(9);
alter table SpecialTreatyPriceChange add ApproverName VARCHAR2(100);
alter table SpecialTreatyPriceChange add ApproveTime DATE;
alter table SpecialTreatyPriceChange add ApproveComment VARCHAR2(200);

alter table PartsInventoryBill add InitialApproverId NUMBER(9);
alter table PartsInventoryBill add InitialApproverName VARCHAR2(100);
alter table PartsInventoryBill add InitialApproveTime DATE;

alter table DealerPartsInventoryBill add InitialApproverId NUMBER(9);
alter table DealerPartsInventoryBill add InitialApproverName VARCHAR2(100);
alter table DealerPartsInventoryBill add InitialApproveTime DATE;
alter table DealerPartsInventoryBill add CheckerId NUMBER(9);
alter table DealerPartsInventoryBill add CheckerName VARCHAR2(100);
alter table DealerPartsInventoryBill add CheckTime DATE;
alter table DealerPartsInventoryBill add UpperCheckerId NUMBER(9);
alter table DealerPartsInventoryBill add UpperCheckerName VARCHAR2(100);
alter table DealerPartsInventoryBill add UpperCheckTime DATE;

alter table CredenceApplication add InitialApproverId NUMBER(9);
alter table CredenceApplication add InitialApproverName VARCHAR2(100);
alter table CredenceApplication add InitialApproveTime DATE;
alter table CredenceApplication add CheckerId NUMBER(9);
alter table CredenceApplication add CheckerName VARCHAR2(100);
alter table CredenceApplication add CheckTime DATE;
alter table CredenceApplication add UpperCheckerId NUMBER(9);
alter table CredenceApplication add UpperCheckerName VARCHAR2(100);
alter table CredenceApplication add UpperCheckTime DATE;
alter table CredenceApplication add UpperApproverId NUMBER(9);
alter table CredenceApplication add UpperApproverName VARCHAR2(100);
alter table CredenceApplication add UpperApproveTime DATE;

alter table PartsOuterPurchaseChange add InitialApproverId NUMBER(9);
alter table PartsOuterPurchaseChange add InitialApproverName VARCHAR2(100);
alter table PartsOuterPurchaseChange add InitialApproveTime DATE;
alter table PartsOuterPurchaseChange add CheckerId NUMBER(9);
alter table PartsOuterPurchaseChange add CheckerName VARCHAR2(100);
alter table PartsOuterPurchaseChange add CheckTime DATE;