create sequence S_AccurateTrace
/

create sequence S_TraceTempType
/

/*==============================================================*/
/* Table: AccurateTrace                                         */
/*==============================================================*/
create table AccurateTrace  (
   Id                   NUMBER(9)                       not null,
   Type                 NUMBER(9),
   CompanyType          NUMBER(9),
   CompanyId            NUMBER(9),
   SourceBillId         NUMBER(9),
   PartId               NUMBER(9),
   TraceCode            VARCHAR2(50),
   SIHLabelCode         VARCHAR2(50),
   Status               NUMBER(9),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_ACCURATETRACE primary key (Id)
)
/

/*==============================================================*/
/* Table: TraceTempType                                         */
/*==============================================================*/
create table TraceTempType  (
   Id                   NUMBER(9)                       not null,
   Type                 NUMBER(9),
   SourceBillId         NUMBER(9),
   SourceBillDetailId   NUMBER(9),
   PartId               NUMBER(9),
   TraceCode            VARCHAR2(50),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_TRACETEMPTYPE primary key (Id)
)
/

alter table SparePart add TraceProperty NUMBER(9);

alter table PartsPurchaseOrderDetail add TraceProperty NUMBER(9);