create sequence S_SIHCreditInfo
/

create sequence S_SIHCreditInfoDetail
/

/*==============================================================*/
/* Table: SIHCreditInfo                                         */
/*==============================================================*/
create table SIHCreditInfo  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   Status               NUMBER(9)                       not null,
   ATotalFee            NUMBER(19,4),
   BTotalFee            NUMBER(19,4),
   ChangeAmount         NUMBER(19,4),
   IsAttach             NUMBER(1),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   InitialApproverId    NUMBER(9),
   InitialApproverName  VARCHAR2(100),
   InitialApproveTime   DATE,
   CheckerId            NUMBER(9),
   CheckerName          VARCHAR2(100),
   CheckTime            DATE,
   ApproverId           NUMBER(9),
   ApproverName         VARCHAR2(100),
   ApproveTime          DATE,
   UpperApproverId      NUMBER(9),
   UpperApproverName    VARCHAR2(100),
   UpperCheckTime       DATE,
   RejecterId           NUMBER(9),
   RejecterName         VARCHAR2(100),
   RejectTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   RowVersion           TIMESTAMP,
   constraint PK_SIHCREDITINFO primary key (Id)
)
/

/*==============================================================*/
/* Table: SIHCreditInfoDetail                                   */
/*==============================================================*/
create table SIHCreditInfoDetail  (
   Id                   NUMBER(9)                       not null,
   SIHCreditInfoId      NUMBER(9)                       not null,
   MarketingDepartmentId NUMBER(9),
   MarketingDepartmentCode VARCHAR2(50),
   MarketingDepartmentName VARCHAR2(100),
   CompanyId            NUMBER(9),
   CompanyCode          VARCHAR2(50),
   CompanyName          VARCHAR2(100),
   DealerId             NUMBER(9),
   DealerCode           VARCHAR2(50),
   DealerName           VARCHAR2(100),
   AccountGroupId       NUMBER(9),
   AccountGroupCode     VARCHAR2(50),
   AccountGroupName     VARCHAR2(100),
   ASIHCredit           NUMBER(19,4),
   BSIHCredit           NUMBER(19,4),
   ChangeAmount         NUMBER(19,4),
   ValidationTime       DATE,
   ExpireTime           DATE,
   Status               NUMBER(9),
   constraint PK_SIHCREDITINFODETAIL primary key (Id)
)
/