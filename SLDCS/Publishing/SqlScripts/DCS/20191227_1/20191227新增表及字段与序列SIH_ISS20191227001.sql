alter table AccurateTrace add TraceProperty NUMBER(9);
alter table TraceTempType add TraceProperty NUMBER(9);

create sequence S_SupplierTraceCode
/

create sequence S_SupplierTraceCodeDetail
/

/*==============================================================*/
/* Table: SupplierTraceCode                                     */
/*==============================================================*/
create table SupplierTraceCode  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   Status               NUMBER(9)                       not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   SubmitterId          NUMBER(9),
   SubmitterName        VARCHAR2(100),
   SubmitTime           DATE,
   ApproverId           NUMBER(9),
   ApproverName         VARCHAR2(100),
   ApproverTime         DATE,
   CheckerId            NUMBER(9),
   CheckerName          VARCHAR2(100),
   CheckTime            DATE,
   constraint PK_SUPPLIERTRACECODE primary key (Id)
)
/

/*==============================================================*/
/* Table: SupplierTraceCodeDetail                               */
/*==============================================================*/
create table SupplierTraceCodeDetail  (
   Id                   NUMBER(9)                       not null,
   SupplierTraceCodeId  NUMBER(9)                       not null,
   SparePartId          NUMBER(9),
   SparePartCode        VARCHAR2(50),
   SparePartName        VARCHAR2(100),
   PartsSupplierId      NUMBER(9),
   PartsSupplierCode    VARCHAR2(50),
   PartsSupplierName    VARCHAR2(100),
   PartsPurchaseOrderId NUMBER(9),
   PartsPurchaseOrderCode VARCHAR2(50),
   ShippingId           NUMBER(9),
   ShippingCode         VARCHAR2(50),
   TraceProperty        NUMBER(9),
   OldTraceCode         VARCHAR2(100),
   NewTraceCode         VARCHAR2(100),
   constraint PK_SUPPLIERTRACECODEDETAIL primary key (Id)
)
/