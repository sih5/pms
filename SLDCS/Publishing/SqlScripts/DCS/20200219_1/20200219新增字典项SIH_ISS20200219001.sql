Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'SupplierShippingOrderArriveMethod', '供应商发运单到货方式', 1, '供应商确认', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'SupplierShippingOrderArriveMethod', '供应商发运单到货方式', 2, '供货计划组确认', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'SupplierShippingOrderArriveMethod', '供应商发运单到货方式', 3, 'PDA收货确认', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'SupplierShippingOrderArriveMethod', '供应商发运单到货方式', 4, '默认到货', 1, 1);

Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'SupplierShippingOrderModifyStatus', '供应商发运单修改状态', 1, '已修改', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'SupplierShippingOrderModifyStatus', '供应商发运单修改状态', 2, '审核通过', 1, 1);

Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'SupplierShippingOrderComfirmStatus', '供应商发运单确认状态', 1, '新建', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'SupplierShippingOrderComfirmStatus', '供应商发运单确认状态', 2, '入库确认', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'SupplierShippingOrderComfirmStatus', '供应商发运单确认状态', 3, '确认完成', 1, 1);