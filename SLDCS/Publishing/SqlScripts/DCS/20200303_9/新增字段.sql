alter table DealerRetailReturnBillDetail add SIHLabelCode  VARCHAR2(500);
alter table DealerRetailReturnBillDetail add TraceProperty  NUMBER(9);

alter table DealerRetailOrderDetail add SIHLabelCode  VARCHAR2(500);
alter table DealerRetailOrderDetail add TraceProperty  NUMBER(9);