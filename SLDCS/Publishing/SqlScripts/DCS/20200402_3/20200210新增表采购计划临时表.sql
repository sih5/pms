create sequence S_PartsPurchasePlanTemp
/
create table PartsPurchasePlanTemp  (
   Id                 NUMBER(9)                       not null,
   PartsPurchaseOrderId               NUMBER(9),
   PartsPurchaseOrderCode               VARCHAR2(50),
   WarehouseId  NUMBER(9)   not null,
   WarehouseName VARCHAR2(100) not null,
   PackSparePartId        NUMBER(9),
   PackSparePartCode      VARCHAR2(50),
   PackSparePartName      VARCHAR2(100),
   PackPlanQty        NUMBER(9), 
   PackNum            NUMBER(9), 
   SparePartId        NUMBER(9)                       not null,
   SparePartCode      VARCHAR2(50)                    not null,
   SparePartName      VARCHAR2(100)                   not null,
   OrderAmount             NUMBER(9) , 
   Status             NUMBER(9)                       not null,  
   CreatorId          NUMBER(9),
   CreatorName        VARCHAR2(100),
   CreateTime         DATE,
   ModifierId         NUMBER(9),
   ModifierName       VARCHAR2(100),
   ModifyTime         DATE,
   constraint PK_PartsPurchasePlanTemp primary key (Id)
)
/

