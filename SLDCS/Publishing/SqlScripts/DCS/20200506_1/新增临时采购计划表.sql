create sequence S_TemPurchaseOrder 
/ 
 
create sequence S_TemPurchaseOrderDetail 
/ 
 
create sequence S_TemPurchasePlanOrder 
/ 
 
create sequence S_TemPurchasePlanOrderDetail 
/ 
 /*==============================================================*/ 
/* Table: TemPurchaseOrder                                      */ 
/*==============================================================*/ 
create table TemPurchaseOrder  ( 
   Id                   NUMBER(9)                       not null, 
   Code                 VARCHAR2(50)                    not null, 
   TemPurchasePlanOrderId NUMBER(9)                       not null, 
   TemPurchasePlanOrderCode VARCHAR2(50)                    not null, 
   WarehouseId          NUMBER(9), 
   WarehouseName        VARCHAR2(100), 
   Status               NUMBER(9)                       not null, 
   ApproveStatus        NUMBER(9), 
   SuplierId            NUMBER(9), 
   SuplierCode          VARCHAR2(50), 
   SuplierName          VARCHAR2(100), 
   OrderType            NUMBER(9), 
   IsTurnSale           NUMBER(1), 
   ReceCompanyId        NUMBER(9), 
   ReceCompanyCode      VARCHAR2(50), 
   ReceCompanyName      VARCHAR2(100), 
   OrderCompanyId       NUMBER(9), 
   OrderCompanyCode     VARCHAR2(50), 
   OrderCompanyName     VARCHAR2(100), 
   ShippingMethod       NUMBER(9), 
   ReceiveStatus        NUMBER(9), 
   PartsSalesOrderCode  VARCHAR2(50), 
   PartsPurchaseOrderCode VARCHAR2(50), 
   BuyerId              NUMBER(9), 
   Buyer                VARCHAR2(50), 
   CreatorId            NUMBER(9), 
   CreatorName          VARCHAR2(100), 
   CreateTime           DATE, 
   ModifierId           NUMBER(9), 
   ModifierName         VARCHAR2(100), 
   ModifyTime           DATE, 
   Memo                 VARCHAR2(200), 
   SubmitterId          NUMBER(9), 
   SubmitterName        VARCHAR2(100), 
   SubmitTime           DATE, 
   ApproverId           NUMBER(9), 
   ApproverName         VARCHAR2(100), 
   ApproveTime          DATE, 
   CheckerId            NUMBER(9), 
   CheckerName          VARCHAR2(100), 
   CheckTime            DATE, 
   ConfirmerId          NUMBER(9), 
   Confirmer            VARCHAR2(100), 
   ConfirmeTime         DATE, 
   ShippingId           NUMBER(9), 
   Shipper              VARCHAR2(50), 
   ShippTime            DATE, 
   StopperId            NUMBER(9), 
   Stopper              VARCHAR2(50), 
   StopTime             DATE, 
   StopReason           VARCHAR2(500), 
   ForcederId           NUMBER(9), 
   Forcedder            VARCHAR2(50), 
   ForcedTime           DATE, 
   ForcedReason         VARCHAR2(500), 
   ReceiveAddress       VARCHAR2(200), 
   Linker               VARCHAR2(50), 
   LinkPhone            VARCHAR2(50),
   CustomerType         NUMBER(9), 
   FreightType          NUMBER(9),   
   RejectReason         VARCHAR2(200),    
   constraint PK_TEMPURCHASEORDER primary key (Id) 
) 
/
/*==============================================================*/ 
/* Table: TemPurchaseOrderDetail                                */ 
/*==============================================================*/ 
create table TemPurchaseOrderDetail  ( 
   Id                   NUMBER(9)                       not null, 
   TemPurchaseOrderId   NUMBER(9)                       not null, 
   SparePartId          NUMBER(9), 
   SparePartCode        VARCHAR2(50), 
   SparePartName        VARCHAR2(100), 
   SupplierPartCode     VARCHAR2(50), 
   PlanAmount           NUMBER(9)                       not null, 
   MeasureUnit          VARCHAR2(50), 
   ConfirmedAmount      NUMBER(9)                       not null, 
   ShippingAmount       NUMBER(9), 
   Remark               VARCHAR2(200), 
   ShortSupReason       NUMBER(9), 
   constraint PK_TEMPURCHASEORDERDETAIL primary key (Id) 
) 
/ 
 
/*==============================================================*/ 
/* Table: TemPurchasePlanOrder                                  */ 
/*==============================================================*/ 
create table TemPurchasePlanOrder  ( 
   Id                   NUMBER(9)                       not null, 
   Code                 VARCHAR2(50)                    not null, 
   WarehouseId          NUMBER(9), 
   WarehouseName        VARCHAR2(100), 
   Status               NUMBER(9)                       not null, 
   IsTurnSale           NUMBER(1), 
   PlanType             NUMBER(9)                       not null, 
   ReceCompanyId        NUMBER(9), 
   ReceCompanyCode      VARCHAR2(50), 
   ReceCompanyName      VARCHAR2(100), 
   CustomerType         NUMBER(9), 
   OrderCompanyId       NUMBER(9), 
   OrderCompanyCode     VARCHAR2(50), 
   OrderCompanyName     VARCHAR2(100), 
   ShippingMethod       NUMBER(9), 
   IsReplace            NUMBER(1), 
   Memo                 VARCHAR2(200), 
   FreightType          NUMBER(9), 
   CreatorId            NUMBER(9), 
   CreatorName          VARCHAR2(100), 
   CreateTime           DATE, 
   ModifierId           NUMBER(9), 
   ModifierName         VARCHAR2(100), 
   ModifyTime           DATE, 
   AbandonerId          NUMBER(9), 
   AbandonerName        VARCHAR2(100), 
   AbandonTime          DATE, 
   SubmitterId          NUMBER(9), 
   SubmitterName        VARCHAR2(100), 
   SubmitTime           DATE, 
   ApproverId           NUMBER(9), 
   ApproverName         VARCHAR2(100), 
   CheckerId            NUMBER(9), 
   ApproveTime          DATE, 
   CheckerName          VARCHAR2(100), 
   CheckTime            DATE, 
   ConfirmerId          NUMBER(9), 
   Confirmer            VARCHAR2(100), 
   ConfirmeTime         DATE, 
   RejectId             NUMBER(9), 
   Rejector             VARCHAR2(100), 
   RejectTime           DATE, 
   Path                 VARCHAR2(2000), 
   ReceiveAddress       VARCHAR2(200), 
   Linker               VARCHAR2(50), 
   LinkPhone            VARCHAR2(50), 
   RejectReason         VARCHAR2(200), 
   constraint PK_TEMPURCHASEPLANORDER primary key (Id) 
) 
/ 
 
/*==============================================================*/ 
/* Table: TemPurchasePlanOrderDetail                            */ 
/*==============================================================*/ 
create table TemPurchasePlanOrderDetail  ( 
   Id                   NUMBER(9)                       not null, 
   TemPurchasePlanOrderId NUMBER(9)                       not null, 
   SparePartId          NUMBER(9), 
   SparePartCode        VARCHAR2(50), 
   SparePartName        VARCHAR2(100), 
   SupplierPartCode     VARCHAR2(50), 
   ReferenceCode        VARCHAR2(50), 
   PlanAmount           NUMBER(9)                       not null, 
   MeasureUnit          VARCHAR2(50), 
   SuplierId            NUMBER(9), 
   SuplierCode          VARCHAR2(50), 
   SuplierName          VARCHAR2(100), 
   Memo                 VARCHAR2(200), 
   CodeSource           NUMBER(9), 
   constraint PK_TEMPURCHASEPLANORDERDETAIL primary key (Id) 
) 
/ 
 
