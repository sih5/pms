Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'TemPurchasePlanOrderStatus', '临时采购计划单状态', 1, '新建', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'TemPurchasePlanOrderStatus', '临时采购计划单状态', 2, '提交', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'TemPurchasePlanOrderStatus', '临时采购计划单状态', 3, '审核通过', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'TemPurchasePlanOrderStatus', '临时采购计划单状态', 4, '复核通过', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'TemPurchasePlanOrderStatus', '临时采购计划单状态', 5, '确认通过', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'TemPurchasePlanOrderStatus', '临时采购计划单状态',99 , '作废', 1, 1);

Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'TemPurchasePlanOrderPlanType', '临时采购计划单计划类型', 1, '正常', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'TemPurchasePlanOrderPlanType', '临时采购计划单计划类型', 2, '紧急', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'TemPurchasePlanOrderPlanType', '临时采购计划单计划类型', 3, '特急', 1, 1);

Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'TemPurchasePlanOrderCustomerType', '临时采购计划单客户属性', 1, '服务站', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'TemPurchasePlanOrderCustomerType', '临时采购计划单客户属性', 2, '经销商', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'TemPurchasePlanOrderCustomerType', '临时采购计划单客户属性', 3, '中心库', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'TemPurchasePlanOrderCustomerType', '临时采购计划单客户属性', 4, '集团企业', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'TemPurchasePlanOrderCustomerType', '临时采购计划单客户属性', 5, '出口客户', 1, 1);


Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'TemPurchasePlanOrderShippingMethod', '临时采购计划单发运方式', 1, '顺丰公路', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'TemPurchasePlanOrderShippingMethod', '临时采购计划单发运方式', 2, '顺丰航空', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'TemPurchasePlanOrderShippingMethod', '临时采购计划单发运方式', 3, '德邦物流', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'TemPurchasePlanOrderShippingMethod', '临时采购计划单发运方式', 4, '航空', 1, 1);

Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'TemPurchasePlanOrderFreightType', '临时采购计划单运费方式', 1, '到付', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'TemPurchasePlanOrderFreightType', '临时采购计划单运费方式', 2, '寄付', 1, 1);

Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'TemPurchasePlanOrderDetailCodeSource', '临时采购计划单清单编号来源', 1, 'SPM', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'TemPurchasePlanOrderDetailCodeSource', '临时采购计划单清单编号来源', 2, '手工录入', 1, 1);

Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'TemPurchaseOrderStatus', '临时采购定单状态', 1, '新增', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'TemPurchaseOrderStatus', '临时采购定单状态', 2, '提交', 1,1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'TemPurchaseOrderStatus', '临时采购定单状态', 3, '确认完毕', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'TemPurchaseOrderStatus', '临时采购定单状态', 4, '部分发运', 1,1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'TemPurchaseOrderStatus', '临时采购定单状态', 5, '发运完毕', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'TemPurchaseOrderStatus', '临时采购定单状态', 6, '终止', 1,1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'TemPurchaseOrderStatus', '临时采购定单状态', 7, '强制完成', 1, 1);

Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'TemPurchaseOrderApproveStatus', '临时采购计划单审核状态', 1, '新增', 1,1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'TemPurchaseOrderApproveStatus', '临时采购计划单审核状态', 2, '提交', 1,1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'TemPurchaseOrderApproveStatus', '临时采购计划单审核状态', 3 ,'审核通过', 1,1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'TemPurchaseOrderApproveStatus', '临时采购计划单审核状态', 4, '审批通过', 1,1);

Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'TemPurchaseOrderReceiveStatus', '临时采购订单收货状态', 1, '未收货', 1,1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'TemPurchaseOrderReceiveStatus', '临时采购订单收货状态', 2 ,'收货完成', 1,1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'TemPurchaseOrderReceiveStatus', '临时采购订单收货状态', 3, '部分收货', 1,1);