create sequence S_PartsShiftSIHDetail 
/ 

create table PartsShiftSIHDetail  ( 
   Id                   NUMBER(9)                       not null, 
   PartsShiftOrderId    NUMBER(9), 
   PartsShiftOrderDetailId NUMBER(9), 
   Type                 NUMBER(9), 
   SIHCode              VARCHAR2(100), 
   Quantity             NUMBER(9), 
   constraint PK_PARTSSHIFTSIHDETAIL primary key (Id) 
)