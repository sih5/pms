create sequence S_DealerClaimSpareParts 
/ 
create table DealerClaimSpareParts  ( 
   Id                   NUMBER(9)                       not null, 
   ClaimCode            VARCHAR2(50), 
   ClaimStatus          VARCHAR2(50), 
   DealerId             NUMBER(9), 
   DealerCode           VARCHAR2(50), 
   SparePartId          NUMBER(9), 
   SparePartCode        VARCHAR2(50), 
   SparePartName        VARCHAR2(100), 
   DealerName           VARCHAR2(100), 
   Price                NUMBER(19,4), 
   SettleDate           DATE, 
   Quantity             NUMBER(9), 
   CreateTime           DATE, 
   constraint PK_DEALERCLAIMSPAREPARTS primary key (Id) 
) 
/