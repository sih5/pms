Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'CrossSalesOrderStatus', '跨区域销售订单状态', 1, '新建', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'CrossSalesOrderStatus', '跨区域销售订单状态', 2, '提交', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'CrossSalesOrderStatus', '跨区域销售订单状态', 3, '终止', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'CrossSalesOrderStatus', '跨区域销售订单状态', 4, '初审通过', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'CrossSalesOrderStatus', '跨区域销售订单状态', 5, '审核通过', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'CrossSalesOrderStatus', '跨区域销售订单状态', 6, '审批通过', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'CrossSalesOrderStatus', '跨区域销售订单状态', 7, '高级审核通过', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'CrossSalesOrderStatus', '跨区域销售订单状态', 8, '生效', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'CrossSalesOrderStatus', '跨区域销售订单状态', 99, '作废', 1, 1);


Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'CrossSalesOrderType', '跨区域销售订单品种类型', 1, '驾驶室', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'CrossSalesOrderType', '跨区域销售订单品种类型', 2, '本体', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'CrossSalesOrderType', '跨区域销售订单品种类型', 3, '车架', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'CrossSalesOrderType', '跨区域销售订单品种类型', 4, '发动机', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'CrossSalesOrderType', '跨区域销售订单品种类型', 5, '其他', 1, 1);



Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values 
(S_CodeTemplate.Nextval, 'CrossSalesOrder', 'CrossSalesOrder', 1, 'KQ{CORPCODE}{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);

