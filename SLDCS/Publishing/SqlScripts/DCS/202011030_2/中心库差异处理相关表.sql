create sequence S_ResRelationship 
/ 
 
create sequence S_ResponsibleDetail 
/ 
 
create sequence S_ResponsibleMembers 
/ 
create sequence S_AgencyDifferenceBackBill 
/ 
/*==============================================================*/ 
/* Table: ResRelationship                                       */ 
/*==============================================================*/ 
create table ResRelationship  ( 
   Id                   NUMBER(9)                       not null, 
   ResType              VARCHAR2(200)                   not null, 
   ResTem               NUMBER(9)                       not null, 
   Status               NUMBER(9)                       not null, 
   CreatorId            NUMBER(9), 
   CreatorName          VARCHAR2(100), 
   CreateTime           DATE, 
   ModifierId           NUMBER(9), 
   ModifierName         VARCHAR2(100), 
   ModifyTime           DATE, 
   AbandonerId          NUMBER(9), 
   AbandonerName        VARCHAR2(100), 
   AbandonTime          DATE, 
   RowVersion           TIMESTAMP, 
   constraint PK_RESRELATIONSHIP primary key (Id) 
) 
/ 
 
/*==============================================================*/ 
/* Table: ResponsibleDetail                                     */ 
/*==============================================================*/ 
create table ResponsibleDetail  ( 
   Id                   NUMBER(9)                       not null, 
   ResponMemberId       NUMBER(9)                       not null, 
   PersonelId           NUMBER(9)                       not null, 
   PersonelCode         VARCHAR2(50), 
   PersonelName         VARCHAR2(100), 
   constraint PK_RESPONSIBLEDETAIL primary key (Id) 
) 
/ 
 
/*==============================================================*/ 
/* Table: ResponsibleMembers                                    */ 
/*==============================================================*/ 
create table ResponsibleMembers  ( 
   Id                   NUMBER(9)                       not null, 
   ResTem               NUMBER(9)                       not null, 
   Status               NUMBER(9), 
   CreatorId            NUMBER(9), 
   CreatorName          VARCHAR2(100), 
   CreateTime           DATE, 
   ModifierId           NUMBER(9), 
   ModifierName         VARCHAR2(100), 
   ModifyTime           DATE, 
   AbandonerId          NUMBER(9), 
   AbandonerName        VARCHAR2(100), 
   AbandonTime          DATE, 
   RowVersion           TIMESTAMP, 
   constraint PK_RESPONSIBLEMEMBERS primary key (Id) 
) 
/ 
/*==============================================================*/ 
/* Table: AgencyDifferenceBackBill                              */ 
/*==============================================================*/ 
create table AgencyDifferenceBackBill  ( 
   Id                   NUMBER(9)                       not null, 
   Code                 VARCHAR2(50)                    not null, 
   Status               NUMBER(9)                       not null, 
   StorageCompanyId     NUMBER(9)                       not null, 
   StorageCompanyCode   VARCHAR2(50)                    not null, 
   StorageCompanyName   VARCHAR2(100)                   not null, 
   SparePartId          NUMBER(9)                       not null, 
   SparePartCode        VARCHAR2(50)                    not null, 
   SparePartName        VARCHAR2(100)                   not null, 
   MeasureUnit          VARCHAR2(20), 
   PartsInboundPlanId   NUMBER(9), 
   PartsInboundPlanCode VARCHAR2(50), 
   InWarehouseId        NUMBER(9), 
   InWarehouseCode      VARCHAR2(50), 
   InWarehouseName      VARCHAR2(100), 
   PartsShippingId      NUMBER(9), 
   PartsShippingCode    VARCHAR2(50), 
   PartsSalesOrderId    NUMBER(9), 
   PartsSalesOrderCode  VARCHAR2(50), 
   InboundQty           NUMBER(9), 
   ActQty               NUMBER(9), 
   DifferQty            NUMBER(9), 
   OrderPrice           NUMBER(19,4), 
   DifferSum            NUMBER(19,4), 
   DIfferReason         VARCHAR2(1000), 
   ResRelationshipId    NUMBER(9), 
   ResType              VARCHAR2(200), 
   ResTem               NUMBER(9), 
   Path                 VARCHAR2(2000), 
   CreatorId            NUMBER(9), 
   CreatorName          VARCHAR2(100), 
   CreateTime           DATE, 
   ModifierId           NUMBER(9), 
   ModifierName         VARCHAR2(100), 
   ModifyTime           DATE, 
   AbandonerId          NUMBER(9), 
   AbandonerName        VARCHAR2(100), 
   AbandonTime          DATE, 
   SubmitterId          NUMBER(9), 
   SubmitterName        VARCHAR2(100), 
   SubmitTime           DATE, 
   DetermineId          NUMBER(9), 
   DetermineName        VARCHAR2(100), 
   DetermineTime        DATE, 
   ConfirmId            NUMBER(9), 
   ConfirmName          VARCHAR2(100), 
   ConfirmTime          DATE, 
   HandlerId            NUMBER(9), 
   Handler              VARCHAR2(100), 
   HandlTime            DATE, 
   HandMemo             VARCHAR2(500), 
   CheckerId            NUMBER(9), 
   CheckerName          VARCHAR2(100), 
   CheckTime            DATE, 
   ApproverId           NUMBER(9), 
   ApproverName         VARCHAR2(100), 
   ApproveTime          DATE, 
   RejectMemo           VARCHAR2(200), 
   CloserId             NUMBER(9), 
   CloserName           VARCHAR2(100), 
   CloseTime            DATE, 
   CloseMemo            VARCHAR2(200), 
   RowVersion           TIMESTAMP, 
   constraint PK_AGENCYDIFFERENCEBACKBILL primary key (Id) 
) 
/ 
