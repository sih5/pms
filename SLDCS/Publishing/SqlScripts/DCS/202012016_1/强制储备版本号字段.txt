alter table BottomStockColVersion add(ReserveTypeId      NUMBER(9), 
   ReserveType        VARCHAR2(100));

alter table BottomStockSubVersion  add(ReserveTypeId      NUMBER(9), 
   ReserveType        VARCHAR2(100), 
   ReserveTypeSubItemId NUMBER(9), 
   ReserveTypeSubItem VARCHAR2(100))