Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'DealerFormatStoreType', '服务商储备类型', 1, '4S', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'DealerFormatStoreType', '服务商储备类型', 2, '2S店/重点服务商', 1, 1);


Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'MaintenanceVarietyMaterialCost', '服务商储备材料费', 1, 'Y≥10万元', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'MaintenanceVarietyMaterialCost', '服务商储备材料费', 2, '10万元＞Y≥5万元', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'MaintenanceVarietyMaterialCost', '服务商储备材料费', 3, '5万元＞Y', 1, 1);

Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'MaintenanceReserveType', '服务商储备品种或天数', 1, '储备品种数', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'MaintenanceReserveType', '服务商储备品种或天数', 2, '储备天数', 1, 1);

Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'MaintenancePartPrice', '服务商配件是否储备维护单价', 1, '单价>4000元', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'MaintenancePartPrice', '服务商配件是否储备维护单价', 2, '2000元＜单价≤4000元', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'MaintenancePartPrice', '服务商配件是否储备维护单价', 3, '单价≤2000元', 1, 1);
