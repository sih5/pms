create sequence S_MaintenanceMandatory
/

create sequence S_MaintenancePart
/

create sequence S_MaintenanceReserve
/

create sequence S_MaintenanceVariety
/

create table MaintenanceMandatory  (
   Id                   NUMBER(9)                       not null,
   Lower                NUMBER(19,4)                    not null,
   Higher               NUMBER(19,4)                    not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   constraint PK_MAINTENANCEMANDATORY primary key (Id)
)
/

/*==============================================================*/
/* Table: MaintenancePart                                       */
/*==============================================================*/
create table MaintenancePart  (
   Id                   NUMBER(9)                       not null,
   Price                NUMBER(9),
   MaterialCost         NUMBER(9)                       not null,
   IsSave               NUMBER(1),
   Status               NUMBER(9)                       not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   constraint PK_MAINTENANCEPART primary key (Id)
)
/

/*==============================================================*/
/* Table: MaintenanceReserve                                    */
/*==============================================================*/
create table MaintenanceReserve  (
   Id                   NUMBER(9)                       not null,
   Type                 NUMBER(9)                       not null,
   Amount               NUMBER(9)                       not null,
   Status               NUMBER(9)                       not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   constraint PK_MAINTENANCERESERVE primary key (Id)
)
/

/*==============================================================*/
/* Table: MaintenanceVariety                                    */
/*==============================================================*/
create table MaintenanceVariety  (
   Id                   NUMBER(9)                       not null,
   MaterialCost         NUMBER(9)                       not null,
   StoreType            NUMBER(9)                       not null,
   ReserveRatio         NUMBER(19,4)                    not null,
   Status               NUMBER(9)                       not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   constraint PK_MAINTENANCEVARIETY primary key (Id)
)
/


create sequence S_DealerReservesRecommended
/
create table DealerReservesRecommended  (
   Id                 NUMBER(9)                       not null,
   Year               NUMBER(9)                       not null,
   Quarter            NUMBER(9)                       not null,
   DealerId           NUMBER(9)                       not null,
   DealerCode         VARCHAR2(50)                    not null,
   DealerName         VARCHAR2(100)                   not null,
   SparePartId        NUMBER(9)                       not null,
   SparePartCode      VARCHAR2(50)                    not null,
   SparePartName      VARCHAR2(100)                   not null,
   PartType           NUMBER(9),
   QuarterlyDemand    NUMBER(9),
   DemandFrequency    NUMBER(9),
   Quantity           NUMBER(9),
   AdviseQuantity     NUMBER(9),
   CreateTime         DATE,
   DailyDemand        NUMBER(9),
   constraint PK_DEALERRESERVESRECOMMENDED primary key (Id)
)
