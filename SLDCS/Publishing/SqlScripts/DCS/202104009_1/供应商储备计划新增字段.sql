create table SupplierStoreInboundDetail  ( 
   Id                 NUMBER(9)                       not null, 
   PlanId             NUMBER(9)                       not null, 
   InBoundQty         NUMBER(9), 
   InBoundDate        DATE, 
   InBoundName        VARCHAR2(100), 
   constraint PK_SUPPLIERSTOREINBOUNDDETAIL primary key (Id) 
) 
/ 

alter  table SupplierStorePlanOrder  add(BindQty   NUMBER(9))
/
create sequence S_SupplierStoreInboundDetail