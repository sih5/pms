 
create sequence S_SupplierLabelReport 
/ 
create table SupplierLabelReport  ( 
   Id                   NUMBER(9)                       not null, 
   DataMark             VARCHAR2(100), 
   Year                 VARCHAR2(100), 
   Month                VARCHAR2(100), 
   PartsSupplierId      NUMBER(9)                       not null, 
   PartsSupplierCode    VARCHAR2(100)                   not null, 
   PartsSupplierName    VARCHAR2(100), 
   Label                NUMBER(9), 
   OutQty               NUMBER(9), 
   NeedQty              NUMBER(9), 
   UpQty                NUMBER(9), 
   SuggQty              NUMBER(9), 
   PartId               NUMBER(9)                       not null, 
   SparePartCode        VARCHAR2(50), 
   SparePartName        VARCHAR2(100), 
   SupplierPartCode     VARCHAR2(50), 
   CreateTime           DATE, 
   constraint PK_SUPPLIERLABELREPORT primary key (Id) 
) 
/ 
