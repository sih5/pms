create sequence S_CenterBasicClassification 
/
create sequence S_PartsQuarterly 
/ 
create sequence S_RegionalPartsCategory 
/ 
 create sequence S_StationServiceReport 
/ 
create table CenterBasicClassification  ( 
   Id                   NUMBER(9)                       not null, 
   Type                 NUMBER(9)                       not null, 
   Month                NUMBER(9), 
   Weight               NUMBER(15,6), 
   Days                 NUMBER(9), 
   Status               NUMBER(9), 
   CreatorId            NUMBER(9), 
   CreatorName          VARCHAR2(100), 
   CreateTime           DATE, 
   AbandonerId          NUMBER(9), 
   AbandonerName        VARCHAR2(100), 
   AbandonTime          DATE, 
   ModifierId           NUMBER(9), 
   ModifierName         VARCHAR2(100), 
   ModifyTime           DATE, 
   constraint PK_CENTERBASICCLASSIFICATION primary key (Id) 
)
/ 
create table PartsQuarterly  ( 
   Id                   NUMBER(9)                       not null, 
   StockType            NUMBER(9)                       not null, 
   Islast               NUMBER(1)                       not null, 
   CreatorId            NUMBER(9), 
   CreatorName          VARCHAR2(100), 
   CreateTime           DATE, 
   ModifierId           NUMBER(9), 
   ModifierName         VARCHAR2(100), 
   ModifyTime           DATE, 
   constraint PK_PARTSQUARTERLY primary key (Id) 
) 
/ 
create table RegionalPartsCategory  ( 
   Id                   NUMBER(9)                       not null, 
   Year                NUMBER(9)                       not null, 
   Quarter              NUMBER(9)                       not null, 
   CenterId             NUMBER(9)                       not null, 
   CenterCode           VARCHAR2(50)                    not null, 
   CenterName           VARCHAR2(100)                   not null, 
   SparePartId          NUMBER(9)                       not null, 
   SparePartCode        VARCHAR2(50)                    not null, 
   SparePartName        VARCHAR2(100), 
   QuarterlyDemand      NUMBER(9), 
   DemandFrequency      NUMBER(9), 
   RecommendedRe        NUMBER(15,6), 
   DailyDemand          NUMBER(15,6), 
   PartType             NUMBER(9), 
   TurnDate             DATE, 
   constraint PK_REGIONALPARTSCATEGORY primary key (Id) 
) 
/ 
create table StationServiceReport  ( 
   Id                   NUMBER(9)                       not null, 
   Year                 NUMBER(9)                       not null, 
   Quarter              NUMBER(9)                       not null, 
   MarketDepartmentId   NUMBER(9)                       not null, 
   MarketDepartmentName VARCHAR2(100)                   not null, 
   DealerId              NUMBER(9)                  not null, 
   DealerCode           VARCHAR2(100)                       not null, 
   DealerName           VARCHAR2(50)                    not null, 
   SparePartId          NUMBER(9)                       not null, 
   SparePartCode        VARCHAR2(50)                    not null, 
   SparePartName        VARCHAR2(100)                   not null, 
   TQuantity            NUMBER(9), 
   FreGuar              NUMBER(9), 
   AreaTQuantity        NUMBER(9), 
   AreaFreGuar          NUMBER(9), 
   Rank                 NUMBER(9), 
   CreatorId            NUMBER(9), 
   CreatorName          VARCHAR2(100), 
   CreateTime           DATE, 
   ModifierId           NUMBER(9), 
   ModifierName         VARCHAR2(100), 
   ModifyTime           DATE, 
   constraint PK_STATIONSERVICEREPORT primary key (Id) 
) 
/ 
