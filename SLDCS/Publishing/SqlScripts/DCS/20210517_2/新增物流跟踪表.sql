drop table LogisticsTracking;
drop sequence S_LogisticsTracking;

create sequence S_LogisticsTracking;
/*==============================================================*/
/* Table: LogisticsTracking                                     */
/*==============================================================*/
create table LogisticsTracking  (
   Id                   NUMBER(9)                       not null,
   PartsShippingOrderCode VARCHAR2(50)                    not null,
   CarrierNumber        VARCHAR2(50)                    not null,
   LogisticsLabel       VARCHAR2(100)                   not null,
   MergerCaseNumber     VARCHAR2(100)                   not null,
   BoxUpTaskCode        VARCHAR2(50)                    not null,
   CounterpartCompanyCode VARCHAR2(50)                    not null,
   CounterpartCompanyName VARCHAR2(100),
   DispatchOrderCode    VARCHAR2(500),
   ArrivalTime          DATE,
   LatitudeAndLongitude VARCHAR2(100),
   Position             VARCHAR2(200),
   CarrierDate          DATE,
   TheoryArrivalTime    DATE,
   RouteDescription     VARCHAR2(200),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   OperationTime        DATE,
   ExpectArrivalTime    DATE,
   constraint PK_LOGISTICSTRACKING primary key (Id)
);

drop table LogisticsTrackingDetail;
drop sequence S_LogisticsTrackingDetail;
create sequence S_LogisticsTrackingDetail;
/*==============================================================*/
/* Table: LogisticsTrackingDetail                               */
/*==============================================================*/
create table LogisticsTrackingDetail  (
   Id                   NUMBER(9)                       not null,
   LogisticsTrackingId  NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50)                    not null,
   SparePartName        VARCHAR2(100),
   Amount               NUMBER(9)                       not null,
   constraint PK_LOGISTICSTRACKINGDETAIL primary key (Id)
);


drop table InterfaceDataSyncRecord;
create sequence S_InterfaceDataSyncRecord;
create sequence S_InterfaceDataSyncRecord;
create table InterfaceDataSyncRecord 
(
   Id                   NUMBER(6)            not null,
   BusinessName         VARCHAR2(100)        not null,
   IsActive             SMALLINT             not null,
   MaxSyncCount         INTEGER              not null,
   LastOperateTime      DATE,
   LastSyncRowVersion   TIMESTAMP,
   LastSyncCount        INTEGER,
   constraint PK_INTERFACEDATASYNCRECORD primary key (Id)
);

alter  table PartsShippingOrder add(IsSync   NUMBER(1));

insert into InterfaceDataSyncRecord
values(s_InterfaceDataSyncRecord.Nextval,'PartsShippingOrderToSF',1,200,sysdate,sysdate,null);

insert into InterfaceDataSyncRecord
values(s_InterfaceDataSyncRecord.Nextval,'PartsShippingOrderToAG',1,200,sysdate,sysdate,null);