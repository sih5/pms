create sequence S_FinancialSettlement 
/ create table FinancialSettlement  ( 
   Id                   NUMBER(9)                       not null, 
   Status               NUMBER(9)                       not null, 
   Type                 NUMBER(9)                       not null, 
   StartDate            DATE, 
   EndDate              DATE, 
   CreatorId            NUMBER(9), 
   CreatorName          VARCHAR2(100), 
   CreateTime           DATE, 
   AbandonerId          NUMBER(9), 
   AbandonerName        VARCHAR2(100), 
   AbandonTime          DATE, 
   RowVersion           TIMESTAMP, 
   constraint PK_FINANCIALSETTLEMENT primary key (Id) 
)