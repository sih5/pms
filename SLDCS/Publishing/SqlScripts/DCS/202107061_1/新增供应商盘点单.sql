create sequence S_SupplierInventoryBill;

create sequence S_SupplierInventoryBillDetail;

/*==============================================================*/
/* Table: SupplierInventoryBill                                 */
/*==============================================================*/
create table SupplierInventoryBill  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   PartsSupplierId      NUMBER(9)                       not null,
   PartsSupplierCode    VARCHAR2(50)                    not null,
   PartsSupplierName    VARCHAR2(100)                   not null,
   Status               NUMBER(9)                       not null,
   Path                 VARCHAR2(2000),
   IsUploadFile         NUMBER(1),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   RejecterId           NUMBER(9),
   RejecterName         VARCHAR2(100),
   RejectTime           DATE,
   RejectComment        VARCHAR2(200),
   CheckerId            NUMBER(9),
   CheckerName          VARCHAR2(100),
   CheckTime            DATE,
   RowVersion           TIMESTAMP,
   constraint PK_SUPPLIERINVENTORYBILL primary key (Id)
);

/*==============================================================*/
/* Table: SupplierInventoryBillDetial                           */
/*=========================================a'a=====================*/
create table SupplierInventoryBillDetail  (
   Id                   NUMBER(9)                       not null,
   SupplierInventoryBillId NUMBER(9)                       not null,
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50),
   SparePartName        VARCHAR2(100),
   SupplierPartCode     VARCHAR2(100),
   CurrentStorage       NUMBER(9)                       not null,
   StorageAfterInventory NUMBER(9)                       not null,
   StorageDifference    NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   constraint PK_SUPPLIERINVENTORYBILLDETAIL primary key (Id)
);