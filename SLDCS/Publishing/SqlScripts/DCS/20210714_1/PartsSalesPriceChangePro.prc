create or replace procedure PartsSalesPriceChangePro is
  cursor Changes is
    select ps.id,
           ps.code,
           ps.isturnprice,
           ps.branchid,
           ps.partssalescategoryid,
           ps.partssalescategorycode,
           ps.partssalescategoryname,
           ps.ifclaim,
           ps.modifierid,
           ps.modifiername,
           ps.modifytime,
           ps.finalapproverid,
           ps.finalapprovername,
           ps.finalapprovetime,
           ps.creatorid,
           ps.creatorname,
           ps.createtime
      from PartsSalesPriceChange ps
     where ps.status = 3
       and ps.isturnprice = 1
       and ps.validatdate <= trunc(sysdate);
  cursor ChangeDetails(fparentid int) is
    select psp.id,
           psp.sparepartid,
           psp.sparepartcode,
           psp.sparepartname,
           psp.salesprice,
           psp.retailguideprice,
           psp.PriceTypeName,
           psp.pricetype,
           psp.centerprice,
           pr.id                as PriceId,
           pg.id                as RetailId,
           psp.ExpireTime,
           psp.Remark,
           pr.salesprice        as oldsalesprice,
           pr.centerprice       as oldcenterprice,
           ps.id                as IncreaseRateGroupId
      from PartsSalesPriceChangeDetail psp
      left join partssalesprice pr
        on pr.sparepartid = psp.sparepartid
       and pr.status = 1
      left join PartsRetailGuidePrice pg
        on psp.sparepartid = pg.sparepartid
       and pg.status = 1
      left join PartsSalePriceIncreaseRate ps
        on psp.pricetypename = ps.groupname
       and ps.status = 1
     where psp.parentid = fparentid;
begin
  for c_change in Changes loop
    for detail in ChangeDetails(c_change.id) loop
      --没有销售价记录时，新增销售价格记录
      if detail.salesprice is null then
        insert into partssalesprice
          (Id,
           BranchId,
           PartsSalesCategoryId,
           PartsSalesCategoryCode,
           PartsSalesCategoryName,
           SparePartId,
           SparePartCode,
           SparePartName,
           IfClaim,
           SalesPrice,
           PriceType,
           Status,
           ValidationTime,
           ExpireTime,
           Remark,
           CenterPrice,
           CreatorId,
           CreatorName,
           CreateTime,
           ModifierId,
           ModifierName,
           ModifyTime,
           ApproverId,
           ApproverName,
           ApproveTime)
        values
          (S_PartsSalesPrice.Nextval,
           c_change.branchid,
           c_change.partssalescategoryid,
           c_change.partssalescategorycode,
           c_change.partssalescategoryname,
           detail.sparepartid,
           detail.sparepartcode,
           detail.sparepartname,
           c_change.ifclaim,
           detail.salesprice,
           detail.pricetype,
           1,
           sysdate,
           detail.ExpireTime,
           detail.Remark,
           detail.CenterPrice,
           c_change.CreatorId,
           c_change.CreatorName,
           c_change.CreateTime,
           c_change.ModifierId,
           c_change.ModifierName,
           c_change.ModifyTime,
           c_change.finalapproverid,
           c_change.finalapprovername,
           c_change.finalapprovetime);
      else
        --否则修改销售价
        update partssalesprice ps
           set ps.centerprice    = detail.centerprice,
               ps.salesprice     = detail.salesprice,
               ps.ValidationTime = sysdate,
               ps.ExpireTime     = detail.expiretime,
               ps.pricetype      = detail.pricetype,
               ps.approverid     = c_change.finalapproverid,
               ps.approvername   = c_change.finalapprovername,
               ps.approvetime    = c_change.finalapprovetime
         where id = detail.priceid;
      end if;
      --增加销售价格变更履历
      insert into PartsSalesPriceHistory
        (Id,
         BranchId,
         PartsSalesCategoryId,
         PartsSalesCategoryCode,
         PartsSalesCategoryName,
         SparePartId,
         SparePartCode,
         SparePartName,
         IfClaim,
         SalesPrice,
         PriceType,
         Status,
         ValidationTime,
         ExpireTime,
         Remark,
         CenterPrice,
         PriceTypeName,
         OldSalesPrice,
         OldCenterPrice,
         RetailGuidePrice,
         CreatorId,
         CreatorName,
         CreateTime)
      values
        (S_PartsSalesPriceHistory.Nextval,
         c_change.branchid,
         c_change.partssalescategoryid,
         c_change.partssalescategorycode,
         c_change.partssalescategoryname,
         detail.sparepartid,
         detail.sparepartcode,
         detail.sparepartname,
         c_change.ifclaim,
         detail.salesprice,
         detail.pricetype,
         1,
         sysdate,
         detail.ExpireTime,
         detail.Remark,
         detail.CenterPrice,
         detail.pricetypename,
         nvl(detail.oldsalesprice, detail.salesprice),
         nvl(detail.oldcenterprice, detail.centerprice),
         detail.RetailGuidePrice,
         c_change.finalapproverid,
         c_change.finalapprovetime,
         sysdate);
      --零售指导价为空时
      if detail.retailid is null then
        insert into PartsRetailGuidePrice
          (id,
           BranchId,
           PartsSalesCategoryId,
           PartsSalesCategoryCode,
           PartsSalesCategoryName,
           SparePartId,
           SparePartCode,
           SparePartName,
           RetailGuidePrice,
           Status,
           ValidationTime,
           ExpireTime,
           Remark,
           CreatorId,
           CreatorName,
           CreateTime)
        values
          (s_PartsRetailGuidePrice.Nextval,
           c_change.branchid,
           c_change.partssalescategoryid,
           c_change.partssalescategorycode,
           c_change.partssalescategoryname,
           detail.sparepartid,
           detail.sparepartcode,
           detail.sparepartname,
           detail.retailguideprice,
           1,
           sysdate,
           detail.ExpireTime,
           detail.Remark,
           c_change.finalapproverid,
           c_change.finalapprovetime,
           sysdate);
        --零售指导价不为空时，更新零售指导价
      else
        update PartsRetailGuidePrice ps
           set ps.retailguideprice = detail.retailguideprice,
               ps.validationtime   = sysdate,
               ps.expiretime       = detail.expiretime,
               ps.modifierid       = c_change.finalapproverid,
               ps.modifiername     = c_change.finalapprovername,
               ps.modifytime       = sysdate
         where id = detail.retailid;
      end if;
      --插入零售指导价更新履历
      insert into PartsRetailGuidePriceHistory
        (id,
         BranchId,
         PartsSalesCategoryId,
         PartsSalesCategoryCode,
         PartsSalesCategoryName,
         SparePartId,
         SparePartCode,
         SparePartName,
         RetailGuidePrice,
         Status,
         ValidationTime,
         ExpireTime,
         Remark,
         CreatorId,
         CreatorName,
         CreateTime)
      values
        (s_PartsRetailGuidePriceHistory.Nextval,
         c_change.branchid,
         c_change.partssalescategoryid,
         c_change.partssalescategorycode,
         c_change.partssalescategoryname,
         detail.sparepartid,
         detail.sparepartcode,
         detail.sparepartname,
         detail.retailguideprice,
         1,
         sysdate,
         detail.ExpireTime,
         detail.Remark,
         c_change.finalapproverid,
         c_change.finalapprovetime,
         sysdate);
    
      --销售价变更申请审核通过后，倒冲 采购价格变动情况记录 的销售价维护时间
      update PurchasePricingChangeHis ps
         set ps.maintenancetime   = sysdate,
             ps.centerprice       = detail.centerprice,
             ps.distributionprice = detail.salesprice,
             ps.retailprice       = detail.retailguideprice,
             ps.status            = 99,
             ps.modifierid        = c_change.finalapproverid,
             ps.modifiername      = c_change.finalapprovername,
             ps.modifytime        = sysdate
       where ps.sparepartid = detail.sparepartid
         and ps.maintenancetime is null
         and ps.status = 1;
      --更新价格类型字段的值需覆盖配件分品牌信息管理中的价格类型
      update partsbranch pb
         set pb.increaserategroupid = detail.increaserategroupid,
             pb.modifierid          = c_change.finalapproverid,
             pb.modifiername        = c_change.finalapprovername,
             pb.modifytime          = sysdate
       where pb.partid = detail.sparepartid
         and pb.status = 1;
    end loop;
    --更新单据是否已生效为是
    update PartsSalesPriceChange ps
       set ps.iseffective = 1
     where id = c_change.id;
  end loop;
end PartsSalesPriceChangePro;
