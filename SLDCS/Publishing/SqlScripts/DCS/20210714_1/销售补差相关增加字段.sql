alter table PurchasePricingChangeHis  add(SupplierId         NUMBER(9), 
   SupplierCode       VARCHAR2(50), 
   SupplierName       VARCHAR2(100), 
   PriceType          NUMBER(9));
   
   alter table PartsSalesPriceChange add(ValidatDate        DATE, 
   IsTurnPrice        NUMBER(1), 
   IsEffective        NUMBER(1))
