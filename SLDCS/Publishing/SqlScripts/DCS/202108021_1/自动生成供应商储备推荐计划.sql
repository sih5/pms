create or replace procedure AddSReserveRecommendPlan is
begin
  execute immediate 'truncate table SupplierReserveRecommendPlan';
  insert into SupplierReserveRecommendPlan
    (Id,
     PartsSupplierCode,
     PartsSupplierName,
     SupplierPartCode,
     PartCode,
     PartName,
     IsPrimary,
     IsDirectSupply,
     IsOrderable,
     PackingCoefficient,
     IsPurchasePricing,
     CustomerCount,
     DaysAvg,
     LowerLimit,
     StandStock,
     UpperLimit,
     ActQty,
     NoShippingQuantity,
     NoInReserveQuantity,
     Quantity,
     RecommendQuantity )
    select s_SupplierReserveRecommendPlan.Nextval,
           temp.*,
           round(temp.UpperLimit - temp.ActQty + temp.NoShippingQuantity -
                 nvl(temp.NoInReserveQuantity, 0) / PackingCoefficient,
                 0) * PackingCoefficient,
                 trunc(sysdate)
      from (select c.code as PartsSupplierCode,
                   c.name as PartsSupplierName,
                   psr.SupplierPartCode,
                   pb.partcode,
                   pb.partname,
                   psr.isprimary,
                   pb.isdirectsupply,
                   pb.isorderable,
                   (select pbpp.PackingCoefficient
                      from PartsBranchPackingProp pbpp
                     where pbpp.partsbranchid = pb.id
                       and pbpp.packingtype = pb.MainPackingType
                       and rownum = 1) as PackingCoefficient,
                   case
                     when exists (select 1
                             from PartsPurchasePricing ppp
                            where ppp.partid = pss.partid
                              and ppp.Validfrom <= sysdate
                              and ppp.Validto >= sysdate
                              and ppp.status = 2) then
                      1
                     else
                      0
                   end as IsPurchasePricing,
                   (select count(distinct cds.CustomerId)
                      from CustomerDirectSpareList cds
                     where cds.sparepartid = pss.partid) as CustomerCount,
                   tempSdsa.DaysAvg,
                   tempSrp.LowerLimit,
                   tempSrp.StandStock,
                   tempSrp.UpperLimit,
                   pss.quantity - nvl(pss.lockedqty, 0) as ActQty,
                   (select sum(case
                                 when ppo.status = 4 or ppo.status = 5 then
                                  ppod.ConfirmedAmount -
                                  nvl(ppod.ShippingAmount, 0)
                                 when ppo.status = 1 then
                                  ppod.OrderAmount - nvl(ppod.ShippingAmount, 0)
                                 else
                                  0
                               end)
                      from PartsPurchaseOrderDetail ppod
                     inner join PartsPurchaseOrder ppo
                        on ppo.id = ppod.PartsPurchaseOrderId
                     where ppo.status in (1, 4, 5, 7)
                       and ppod.sparepartid = pss.partid) as NoShippingQuantity,
                   (select sum(sspo.PlanQty - nvl(sspo.InboundQty, 0))
                      from SupplierStorePlanOrder sspo
                     where sspo.partssupplierid = pss.partssupplierid
                       and sspo.sparepartid = pss.partid
                       and sspo.status in (1, 2)) as NoInReserveQuantity,
                   pss.quantity
              from PartsSupplierStock pss
             inner join company c
                on pss.partssupplierid = c.id
             inner join PartsSupplierRelation psr
                on pss.partssupplierid = psr.supplierid
               and pss.partid = psr.partid
             inner join partsbranch pb
                on pss.partid = pb.partid
             inner join (select srp.sparepartid,
                               sum(srp.LowerLimit) as LowerLimit,
                               sum(srp.StandStock) as StandStock,
                               sum(srp.UpperLimit) as UpperLimit
                          from SIHRecommendPlan srp
                         where srp.CarryTime =
                               (select max(CarryTime) from SIHRecommendPlan)
                         group by srp.sparepartid) tempSrp
                on tempSrp.sparepartid = pss.partid
             inner join (select sdsa.sparepartid,
                               sum(sdsa.NDaysAvg) as DaysAvg
                          from SIHDailySalesAverage sdsa
                         where sdsa.CarryTime =
                               (select max(CarryTime)
                                  from SIHDailySalesAverage)
                         group by sdsa.sparepartid) tempSdsa
                on tempSdsa.sparepartid = pss.partid) temp;
end;
