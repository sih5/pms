CREATE OR REPLACE Procedure AutoAddSupplierStorePlanOrder As
  PCodeTemplate   varchar2(50);
  PComplateId NUMBER(9);
  PMyException  EXCEPTION;
  PCode varchar2(50);
  errormsg varchar2(8000);
  
  Cursor SupplierStorePlanOrders is
  select ps.id as PartsSupplierId,srrp.PartsSupplierCode,srrp.PartsSupplierName,srrp.SupplierPartCode,sp.id as PartId,srrp.PartCode,srrp.PartName,sp.TraceProperty,srrp.RecommendQuantity
  From SupplierReserveRecommendPlan srrp
  inner join PartsSupplier ps on srrp.partssuppliercode= ps.code
  inner join SparePart sp on srrp.partcode= sp.code
  where srrp.IsOrderable=1 and srrp.IsPrimary=1 and srrp.IsPurchasePricing=1 and ps.IsPlan=1 and srrp.RecommendQuantity>0;
Begin
    For item In SupplierStorePlanOrders Loop
    Begin
      PCodeTemplate := 'SSP{CORPCODE}' || TO_CHAR(sysdate, 'yyyymmdd') || '{SERIAL}';
      BEGIN
        SELECT Id
        INTO PComplateId
        FROM CodeTemplate
        WHERE name = 'SupplierStorePlanOrder'
        AND IsActived = 1;
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
        errormsg := '未找到名称为"SupplierStorePlanOrder"的有效编码规则。';
        raise PMyException;
      WHEN OTHERS THEN
        raise;
      END;
      PCode := regexp_replace(regexp_replace(PCodeTemplate, '{CORPCODE}', item.PartsSupplierCode), '{SERIAL}', lpad(TO_CHAR(GetSerial(PComplateId, TRUNC(sysdate, 'DD'), item.PartsSupplierCode)), 4, '0'));

      Insert into SupplierStorePlanOrder(Id,Code,Status,PartsSupplierId,PartsSupplierCode,PartsSupplierName,SparePartId,SparePartCode,SparePartName,SupplierPartCode,TraceProperty,PlanQty,CreatorName,CreateTime)
      values(s_SupplierStorePlanOrder.Nextval,PCode,1,item.PartsSupplierId,item.PartsSupplierCode,item.PartsSupplierName,item.PartId,item.PartCode,item.PartName,item.SupplierPartCode,item.TraceProperty,
      item.RecommendQuantity,'系统自动推荐生成',sysdate);
    end;
    end loop;
end;
