alter table PartsClaimOrderNew  add( InitialApproveId   NUMBER(9), 
   InitialApproveDt   DATE, 
   InitialAppName     VARCHAR2(100), 
   AuditId            NUMBER(9), 
   AuditName          VARCHAR2(100), 
   AuditTime          DATE, 
   ExamineId          NUMBER(9), 
   ExamineName        VARCHAR2(100), 
   ExamineTime        DATE)
