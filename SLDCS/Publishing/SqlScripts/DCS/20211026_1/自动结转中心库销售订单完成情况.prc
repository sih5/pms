create or replace procedure PartsSalesOrderFinishPro is
  --销售订单
  cursor partsSalesOrderProcesss is
    select distinct pd.code
      from partssalesorder pd
      left join PartsSalesOrderProcess ps
        on pd.id = ps.originalsalesorderid
      join company cm
        on pd.salesunitownercompanyid = cm.id
      join partssalesorderdetail pt
        on pd.id = pt.partssalesorderid
     where cm.type = 3
       and pd.status <> 99
       and pt.orderedquantity > 0
       and (not exists (select 1
                          from PartsSalesOrderFinish p
                         where p.partssalesorderprocesscode = ps.code) or
            pd.status in (1, 2));
  -- 零售订单
  cursor partsRetailOrders is
    select *
      from PartsRetailOrder po
     where po.status <> 99
       and not exists
     (select 1 from PartsSalesOrderFinish p where po.code = p.code);
begin
  -- 删除处理单号为空且未审核过的数据
  delete from PartsSalesOrderFinish s
   where s.partssalesorderprocesscode is null
     and s.type = 1
     and exists (select 1
            from PartsSalesOrderProcess p
           where s.id = p.originalsalesorderid);
  --删除已作废的零售订单
  delete from PartsSalesOrderFinish s
   where s.type = 2
     and exists (select 1
            from PartsRetailOrder p
           where s.code = p.code
             and p.status = 99);
  commit;
  --更新出库单号为空的
  update PartsSalesOrderFinish p
     set p.partsoutboundbillcode =
         (select p1.code
            from PartsOutboundBill p1
            join Partsoutboundbilldetail p2
              on p1.id = p2.partsoutboundbillid
           where p1.originalrequirementbillcode = p.code
             and p1.outboundtype = 1
             and p1.storagecompanytype = 3
             and p2.sparepartid = p.sparepartid
             and rownum = 1)
   where p.partsoutboundbillcode is null;
  --更新出库单发运时间为空的
  update PartsSalesOrderFinish p
     set p.shippingdate =
         (select ph.ShippingDate
            from PartsShippingOrder ph
            join Partsshippingorderdetail psd
              on psd.partsshippingorderid = ph.id
            join partsoutboundplan tmp
              on tmp.id = psd.partsoutboundplanid
             and tmp.outboundtype = 1
           where psd.sparepartid = p.sparepartid
             and rownum = 1
             and tmp.sourcecode = p.code
             and ph.status <> 99)
   where p.shippingdate is null;
  --更新发运单创建时间为空的 
  update PartsSalesOrderFinish p
     set p.shippingcreattime =
         (select ph.createtime
            from PartsShippingOrder ph
            join Partsshippingorderdetail psd
              on psd.partsshippingorderid = ph.id
            join partsoutboundplan tmp
              on tmp.id = psd.partsoutboundplanid
             and tmp.outboundtype = 1
           where psd.sparepartid = p.sparepartid
             and rownum = 1
             and tmp.sourcecode = p.code
             and ph.status <> 99)
   where p.shippingcreattime is null;
  --插入新的审核单
  for partsSalesOrderProcess in partsSalesOrderProcesss loop
    insert into PartsSalesOrderFinish
      select s_PartsSalesOrderFinish.Nextval,
             po.id,
             po.code,
             pspd.code,
             po.SalesUnitOwnerCompanyName,
             po.warehouseId,
             w.name as WarehouseName,
             po.province,
             po.submitcompanycode,
             po.submitcompanyname,
             po.ReceivingCompanyName,
             p.sparepartid,
             p.sparepartcode,
             p.sparepartname,
             sp.referencecode,
             sp.englishname,
             b.partabc,
             b.PurchaseRoute,
             po.IfDirectProvision,
             pspd.OrderProcessMethod,
             nvl(pspd.OrderedQuantity, p.orderedquantity) as OrderedQuantity,
             pspd.currentfulfilledquantity,
             pr.groupname,
             p.OrderPrice,
             p.OrderPrice * p.OrderedQuantity as OrderSum,
             p.originalprice,
             p.originalprice * p.OrderedQuantity as OriginalPriceSum,
             sp.weight * p.orderedquantity as weight,
             sp.volume * p.orderedquantity as volume,
             (select p1.code
                from PartsOutboundBill p1
                join Partsoutboundbilldetail p2
                  on p1.id = p2.partsoutboundbillid
               where p1.OriginalRequirementBillId = po.id
                 and p1.outboundtype = 1
                 and p1.storagecompanytype = 3
                 and p2.sparepartid = p.sparepartid
                 and rownum = 1) as PartsOutboundBillCode,
             nvl(pspd.createtime, po.approvetime) as approvetime,
             po.Remark,
             (select tmp.CREATETIME
                from partssalesorderprocess tmp
               where tmp.ORIGINALSALESORDERID = po.id
                 and tmp.time = 1) firstapprovetime,
             (select ph.ShippingDate
                from PartsShippingOrder ph
                join Partsshippingorderdetail psd
                  on psd.partsshippingorderid = ph.id
                join partsoutboundplan tmp
                  on tmp.id = psd.partsoutboundplanid
                 and tmp.outboundtype = 1
               where psd.sparepartid = p.sparepartid
                 and rownum = 1
                 and tmp.SOURCEID = po.id
                 and ph.status <> 99) as ShippingDate,
             (select ph.createtime
                from PartsShippingOrder ph
                join Partsshippingorderdetail psd
                  on psd.partsshippingorderid = ph.id
                join partsoutboundplan tmp
                  on tmp.id = psd.partsoutboundplanid
                 and tmp.outboundtype = 1
               where psd.sparepartid = p.sparepartid
                 and rownum = 1
                 and tmp.SOURCEID = po.id
                 and ph.status <> 99) as ShippingCreatTime,
             po.PartsSalesOrderTypeName,
             po.createtime,
             po.SubmitTime,
             (case
               when cp.type = 3 then
                (select mt.Name
                   from MarketingDepartment mt
                   join Agency ag
                     on mt.id = ag.marketingdepartmentid
                  where ag.id = cp.id)
               when cp.type = 2 or cp.type = 7 then
                (select mt.Name
                   from MarketingDepartment mt
                   join DealerServiceInfo ag
                     on mt.id = ag.marketingdepartmentid
                  where ag.DealerId = cp.id)
               else
                null
             end) as MarketingDepartmentName,
             po.salesunitownercompanyid,
             po.partsSalesOrderTypeId,
             1,
             pspd.time
        from partssalesorder po
       inner join partssalesorderdetail p
          on p.partssalesorderid = po.id
        left join (select psp.time,
                          psp.code,
                          psp.originalsalesorderid,
                          psp.createtime,
                          pspd.orderedquantity,
                          pspd.sparepartid,
                          pspd.OrderProcessMethod,
                          pspd.currentfulfilledquantity
                     from partssalesorderprocess psp
                    inner join partssalesorderprocessdetail pspd
                       on psp.id = pspd.partssalesorderprocessid) pspd
          on pspd.originalsalesorderid = po.id
         and p.sparepartid = pspd.sparepartid
       inner join warehouse w
          on po.warehouseid = w.id
         and w.storagecompanytype = 3
        join sparepart sp
          on p.sparepartid = sp.id
        join partsbranch b
          on p.sparepartid = b.partid
         and b.status = 1
        left join PartsSalePriceIncreaseRate pr
          on b.IncreaseRateGroupId = pr.id
        join company cp
          on po.SubmitCompanyId = cp.id
        join keyvalueitem kv
          on po.status = kv.key
         and kv.name = 'PartsSalesOrder_Status'
       where p.orderedquantity > 0
         and po.status in (1, 2, 4, 5, 6)
         and po.code = partsSalesOrderProcess.Code
         and (pspd.code is null or not exists
              (select 1
                 from PartsSalesOrderFinish pp
                where pp.code = po.code
                  and pp.partssalesorderprocesscode = pspd.code));
  
  end loop;
  for retail in partsRetailOrders loop
    insert into PartsSalesOrderFinish
      select s_PartsSalesOrderFinish.Nextval,
             po.id,
             po.code,
             null as PartsSalesOrderProcessCode,
             po.SalesUnitOwnerCompanyName,
             po.warehouseId,
             po.warehousename,
             null as Province,
             null as SubmitCompanyCode,
             po.CustomerName as submitcompanyname,
             null as ReceivingCompanyName,
             pd.sparepartid,
             pd.sparepartcode,
             pd.sparepartname,
             sp.referencecode,
             sp.englishname,
             pb.PartABC,
             pb.PurchaseRoute,
             null as IfDirectProvision,
             null as OrderProcessMethod,
             pd.quantity as OrderedQuantity,
             decode(po.status, 2, pd.quantity, 3, pd.quantity, 0) as ApproveQuantity,
             pr.groupname,
             pd.DiscountedPrice as OrderPrice,
             pd.DiscountedAmount as OrderSum,
             pd.SalesPrice as originalprice,
             pd.SalesPrice * pd.Quantity as OriginalPriceSum,
             sp.weight * pd.quantity as weight,
             sp.volume * pd.quantity as volume,
             (select p1.code
                from PartsOutboundBill p1
                join Partsoutboundbilldetail p2
                  on p1.id = p2.partsoutboundbillid
               where p1.OriginalRequirementBillId = po.id
                 and p1.OriginalRequirementBillCode = po.code
                 and p2.sparepartid = pd.sparepartid
                 and rownum = 1) as PartsOutboundBillCode,
             po.approvetime,
             po.remark,
             po.ApproveTime as firstapprovetime,
             (select ph.ShippingDate
                from PartsShippingOrder ph
                join Partsshippingorderdetail psd
                  on psd.partsshippingorderid = ph.id
                join partsoutboundplan tmp
                  on tmp.id = psd.partsoutboundplanid
                 and tmp.outboundtype = 6
               where psd.sparepartid = pd.sparepartid
                 and rownum = 1
                 and tmp.originalrequirementbillid = po.id
                 and ph.status <> 99) as ShippingDate,
             (select ph.createtime
                from PartsShippingOrder ph
                join Partsshippingorderdetail psd
                  on psd.partsshippingorderid = ph.id
                join partsoutboundplan tmp
                  on tmp.id = psd.partsoutboundplanid
                 and tmp.outboundtype = 6
               where psd.sparepartid = pd.sparepartid
                 and rownum = 1
                 and tmp.originalrequirementbillid = po.id
                 and ph.status <> 99) as ShippingCreatTime,
             cast('零售订单' as varchar2(100)) as PartsSalesOrderTypeName,
             po.createtime,
             null as SubmitTime,
             (select mt.Name
                from MarketingDepartment mt
                join Agency ag
                  on mt.id = ag.marketingdepartmentid
               where ag.id = po.SalesUnitOwnerCompanyId) as MarketingDepartmentName,
             po.SalesUnitOwnerCompanyId,
             null as PartsSalesOrderTypeId,
             2,
             null
        from PartsRetailOrderDetail pd
        join PartsRetailOrder po
          on pd.partsretailorderid = po.id
       inner join sparepart sp
          on pd.sparepartid = sp.id
        left join partsbranch pb
          on pd.sparepartid = pb.partid
         and pb.status = 1
         and pb.branchid = po.branchid
        left join PartsSalePriceIncreaseRate pr
          on pb.IncreaseRateGroupId = pr.id
       where po.status <> 99;
  end loop;
end PartsSalesOrderFinishPro;
/
