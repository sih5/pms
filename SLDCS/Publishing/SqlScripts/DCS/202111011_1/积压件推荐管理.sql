create sequence S_OverstockPartsRecommend;
create sequence S_OverstockPartsRecommendSon;

create table OverstockPartsRecommend  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   Status               NUMBER(9)                       not null,
   WarehouseId          NUMBER(9)                       not null,
   WarehouseCode        VARCHAR2(50)                    not null,
   WarehouseName        VARCHAR2(100)                   not null,
   RejecteId            NUMBER(9),
   RejecterName         VARCHAR2(50),
   RejecterTime         DATE,
   RejectComment        VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   SubmitterId          NUMBER(9),
   SubmitterName        VARCHAR2(100),
   SubmitTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   InitialApproverId    NUMBER(9),
   InitialApproverName  VARCHAR2(100),
   InitialApproveTime   DATE,
   CheckerId            NUMBER(9),
   CheckerName          VARCHAR2(100),
   CheckTime            DATE,
   ApproverId           NUMBER(9),
   ApproverName         VARCHAR2(100),
   ApproveTime          DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   RowVersion           TIMESTAMP,
   constraint PK_OVERSTOCKPARTSRECOMMEND primary key (Id)
);

create table OverstockPartsRecommendDetail  (
   Id                   NUMBER(9)                       not null,
   OverstockPartsRecommendId NUMBER(9)                       not null,
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50)                    not null,
   SparePartName        VARCHAR2(100)                   not null,
   DiscountRate         NUMBER(15,6),
   DiscountedPrice      NUMBER(19,4),
   RetailPrice          NUMBER(19,4),
   ActualUseableQty     NUMBER(9),
   Quantity             NUMBER(9),
   constraint PK_OVERSTOCKPARTSRECOMMENDDETA primary key (Id)
);

alter table OverstockPartsPlatFormBill add SihCompanyId NUMBER(9);
alter table OverstockPartsPlatFormBill add SihCompanyCode VARCHAR2(50);
alter table OverstockPartsPlatFormBill add SihCompanyName VARCHAR2(100);


Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'OverstockPartsRecommend_Status', '积压件推荐管理状态', 1, '新建', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'OverstockPartsRecommend_Status', '积压件推荐管理状态', 2, '提交', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'OverstockPartsRecommend_Status', '积压件推荐管理状态', 3, '初审通过', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'OverstockPartsRecommend_Status', '积压件推荐管理状态', 4, '审核通过', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'OverstockPartsRecommend_Status', '积压件推荐管理状态', 5, '生效', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'OverstockPartsRecommend_Status', '积压件推荐管理状态', 99, '作废', 1, 1);


Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values 
(S_CodeTemplate.Nextval, 'OverstockPartsRecommend', 'OverstockPartsRecommend', 1, 'OPR{CORPCODE}{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);