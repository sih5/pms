create sequence S_SparePartpurchApplication
/ 
create sequence S_SparePartpurchDetails
/
create table SparePartpurchApplication  (
   Id                   NUMBER(9)                       not null,
  Code                 VARCHAR2(100)                   not null,
   Status               NUMBER(9)                       not null,
   ReceivingWarehouseId NUMBER(9)                       not null,
   ReceivingWarehouseCode VARCHAR2(50)                    not null,
   ReceivingWarehouseName VARCHAR2(50)                    not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   SubmitterId          NUMBER(9),
   SubmitterName        VARCHAR2(100),
   SubmitTime           DATE,
   ApproverId           NUMBER(9),
   ApproverName         VARCHAR2(100),
   ApproveTime          DATE,
   RejecterId           NUMBER(9),
   RejecterName         VARCHAR2(100),
   RejectTime           DATE,
   CheckerId            NUMBER(9),
   CheckerName          VARCHAR2(100),
   CheckTime            DATE,
   CheckMemo            VARCHAR2(200),
   RowVersion           TIMESTAMP,
   constraint PK_SPAREPARTPURCHAPPLICATION primary key (Id)
)
/

create table SparePartpurchDetails  (
   Id                   NUMBER(9)                       not null,
   SpareApplicationId   NUMBER(9)                       not null,
   PartId               NUMBER(9)                       not null,
   PartCode             VARCHAR2(50)                    not null,
   PartName             VARCHAR2(100)                   not null,
   SupplierId           NUMBER(9)                       not null,
   SupplierCode         VARCHAR2(50)                    not null,
   SupplierName         VARCHAR2(100)                   not null,
   IsPrimary            NUMBER(1),
   DeliveryCycle        NUMBER(9),
   LogisticsCycle       NUMBER(9),
   OrderingCycle        NUMBER(9),
   constraint PK_SPAREPARTPURCHDETAILS primary key (Id)
)
/
