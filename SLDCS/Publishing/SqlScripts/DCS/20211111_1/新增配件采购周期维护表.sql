create sequence S_SparePartpurchCycle 
/ 

create table SparePartpurchCycle  ( 
   Id                   NUMBER(9)                       not null, 
   Status               NUMBER(9)                       not null, 
   PartId               NUMBER(9)                       not null, 
   PartCode             VARCHAR2(50)                    not null, 
   PartName             VARCHAR2(100)                   not null, 
   SupplierId           NUMBER(9)                       not null, 
   SupplierCode         VARCHAR2(50)                    not null, 
   SupplierName         VARCHAR2(100)                   not null, 
   IsPrimary            NUMBER(1), 
   ReceivingWarehouseId NUMBER(9)                       not null, 
   ReceivingWarehouseName VARCHAR2(50)                    not null, 
   ReceivingWarehouseCode VARCHAR2(50)                    not null, 
   DeliveryCycle        NUMBER(9), 
   LogisticsCycle       NUMBER(9), 
   OrderingCycle        NUMBER(9), 
   CreatorId            NUMBER(9), 
   CreatorName          VARCHAR2(100), 
   CreateTime           DATE, 
   ModifierId           NUMBER(9), 
   ModifierName         VARCHAR2(100), 
   ModifyTime           DATE, 
   AbandonerId          NUMBER(9), 
   AbandonerName        VARCHAR2(100), 
   AbandonTime          DATE, 
   RowVersion           TIMESTAMP, 
   constraint PK_SPAREPARTPURCHCYCLE primary key (Id) 
) 
/ 
