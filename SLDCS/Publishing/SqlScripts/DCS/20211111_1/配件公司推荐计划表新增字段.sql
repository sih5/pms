alter table SIHRecommendPlan add( SixAmount          NUMBER(19,4), 
   FiveAmount         NUMBER(19,4), 
   FourAmount         NUMBER(19,4), 
   ThreeAmount        NUMBER(19,4), 
   TwoAmount          NUMBER(19,4), 
   OneAmount          NUMBER(19,4))
