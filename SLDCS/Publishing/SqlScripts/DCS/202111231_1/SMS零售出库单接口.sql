insert into InterfaceDataSyncRecord
values(s_InterfaceDataSyncRecord.Nextval,'RetailPartsOutboundBillCenter',1,10,sysdate,to_date('2020-01-01','yyyy-mm-dd'),null);
insert into InterfaceDataSyncRecord
values(s_InterfaceDataSyncRecord.Nextval,'RetailPartsOutboundBillDealer',1,10,sysdate,to_date('2020-01-01','yyyy-mm-dd'),null);
insert into InterfaceDataSyncRecord
values(s_InterfaceDataSyncRecord.Nextval,'RetailReturnsPartsOutboundBillCenter',1,10,sysdate,to_date('2020-01-01','yyyy-mm-dd'),null);
insert into InterfaceDataSyncRecord
values(s_InterfaceDataSyncRecord.Nextval,'RetailReturnsPartsOutboundBillDealer',1,10,sysdate,to_date('2020-01-01','yyyy-mm-dd'),null);

alter table PartsSupplierRelation add IsSync NUMBER(1);

-- update InterfaceDataSyncRecord set LastSyncRowVersion = to_date('2020-01-01','yyyy-mm-dd') where BUSINESSNAME in('RetailPartsOutboundBillCenter','RetailPartsOutboundBillDealer')

