create sequence S_OutBoundDetail
/

create sequence S_OutoundAnalysis
/

create table OutBoundDetail  (
   Id                   NUMBER(9)                       not null,
   OutBoundPlanStatus   NUMBER(9)                       not null,
   OutBoundPlanCode     VARCHAR2(50)                    not null,
   WarehouseId          NUMBER(9)                       not null,
   WarehouseCode        VARCHAR2(50)                    not null,
   WarehouseName        VARCHAR2(100)                   not null,
   OutBoudType          NUMBER(9),
   PlanCreateTime       DATE,
   OriginalRequirementBillCode VARCHAR2(50),
   PartsSalesOrderTypeName VARCHAR2(100),
   ShippingCode         VARCHAR2(50),
   OutBoundCode         VARCHAR2(50),
   OutBoundCreateTime   DATE,
   CounterpartCompanyCode VARCHAR2(50),
   CounterpartCompanyName VARCHAR2(100),
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50)                    not null,
   SparePartName        VARCHAR2(100)                   not null,
   PlannedAmount        NUMBER(9)                       not null,
   OutboundFulfillment  NUMBER(9),
   UnOutboundAmount     NUMBER(9),
   SalesPrice           NUMBER(19,4),
   UnOutFee             NUMBER(19,4),
   FulfillmentFee       NUMBER(19,4),
   OutJudgment          NUMBER(9),
   CreateTime           DATE,
   constraint PK_OUTBOUNDDETAIL primary key (Id)
)
/

/*==============================================================*/
/* Table: OutoundAnalysis                                       */
/*==============================================================*/
create table OutoundAnalysis  (
   Id                   NUMBER(9)                       not null,
   WarehouseId          NUMBER(9)                       not null,
   WarehouseCode        VARCHAR2(50)                    not null,
   WarehouseName        VARCHAR2(100)                   not null,
   AppAmountFee         NUMBER(19,4),
   NormalTUnFee         NUMBER(19,4),
   NormalTOverFee       NUMBER(19,4),
   NormalTOutFee        NUMBER(19,4),
   NormalTOutOverFee    NUMBER(19,4),
   TOutRate             NUMBER(19,4),
   NormalTUnOutFee      NUMBER(19,4),
   NormalTUnOutRate     NUMBER(19,4),
   NormalFUnFee         NUMBER(19,4),
   NormalFOverFee       NUMBER(19,4),
   NormalFOutFee        NUMBER(19,4),
   FOutRate             NUMBER(19,4),
   NormalFOutOverFee    NUMBER(19,4),
   NormalFUnOutFee      NUMBER(19,4),
   NormalFUnOutRate     NUMBER(19,4),
   UrgentFee            NUMBER(19,4),
   UrgentTwUnFee        NUMBER(19,4),
   UrgentTwOverFee      NUMBER(19,4),
   UrgentTwOutFee       NUMBER(19,4),
   UrgentTwOutOverFee   NUMBER(19,4),
   UrgentTwRate         NUMBER(19,4),
   UrgentOneUnFee       NUMBER(19,4),
   UrgentOneOutFee      NUMBER(19,4),
   UrgentOneOutOverFee  NUMBER(19,4),
   UrgentOneRate        NUMBER(19,4),
   CreateTime           DATE,
   constraint PK_OUTOUNDANALYSIS primary key (Id)
)
