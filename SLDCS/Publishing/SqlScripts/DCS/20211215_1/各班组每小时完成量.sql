create sequence S_CompletedByMonth
/
create sequence S_HourlyCompletionTeam
/
create table CompletedByMonth  (
   Id                   NUMBER(9)                       not null,
   WarehouseId          NUMBER(9)                       not null,
   WarehouseCode        VARCHAR2(50)                    not null,
   WarehouseName        VARCHAR2(100)                   not null,
   Month                VARCHAR2(50),
   CreateTime           DATE,
   InBoundItem          NUMBER(9),
   InBoundFee           NUMBER(19,4),
   PackItem             NUMBER(9),
   PackFee              NUMBER(19,4),
   ShelevsItem          NUMBER(9),
   ShelevsFee           NUMBER(19,4),
   PickItem             NUMBER(9),
   PickFee              NUMBER(19,4),
   BoxItem              NUMBER(9),
   BoxFee               NUMBER(19,4),
   constraint PK_COMPLETEDBYMONTH primary key (Id)
)
/

create table HourlyCompletionTeam  (
   Id                   NUMBER(9)                       not null,
   CreateTime           DATE                            not null,
   WarehouseId          NUMBER(9)                       not null,
   WarehouseCode        VARCHAR2(50)                    not null,
   WarehouseName        VARCHAR2(100)                   not null,
   InBoundFinishFee     NUMBER(19,4),
   InBoundTakFee        NUMBER(19,4),
   InboundFinishRate    NUMBER(19,4),
   InBoundFinishItem    NUMBER(9),
   InBoundTakItem       NUMBER(9),
   InboundItemRate      NUMBER(19,4),
   PackFinishFee        NUMBER(19,4),
   PackTaskFee          NUMBER(19,4),
   PackFinishRate       NUMBER(19,4),
   PackFinishItem       NUMBER(9),
   PackTaskItem         NUMBER(9),
   PackItemRate         NUMBER(19,4),
   ShelvesFinishFee     NUMBER(19,4),
   ShelvesTaskFee       NUMBER(19,4),
   ShelvesFinishRate    NUMBER(19,4),
   ShelvesFinishItem    NUMBER(9),
   ShelvesTaskItem      NUMBER(9),
   ShelvesItemRate      NUMBER(19,4),
   PickFInishFee        NUMBER(19,4),
   PickTaskFee          NUMBER(19,4),
   PickFInishRate       NUMBER(19,4),
   PickFInishItem       NUMBER(9),
   PickTaskItem         NUMBER(9),
   PickItemRate         NUMBER(19,4),
   BoxFinishFee         NUMBER(19,4),
   BoxTaskFee           NUMBER(19,4),
   BoxFinishRate        NUMBER(19,4),
   BoxFinishItem        NUMBER(9),
   BoxTaskItem          NUMBER(9),
   BoxItemRate          NUMBER(19,4),
   constraint PK_HOURLYCOMPLETIONTEAM primary key (Id)
)
/
