create sequence S_WarehouseHoliday
 
/
 create table WarehouseHoliday  (
 
   Id                   NUMBER(9)                       not null,
 
   WarehouseId          NUMBER(9),
 
   WarehouseCode        VARCHAR2(100),
 
   WarehouseName        VARCHAR2(100),
 
   StartsDate           DATE,
 
   EndDates             DATE,
 
   Status               NUMBER(9),
 
   CreatorId            NUMBER(9),
 
   CreatorName          VARCHAR2(100),
 
   CreateTime           DATE,
 
   AbandonerId          NUMBER(9),
 
   AbandonerName        VARCHAR2(100),
 
   AbandonTime          DATE,
 
   constraint PK_WAREHOUSEHOLIDAY primary key (Id)
 
)
 
