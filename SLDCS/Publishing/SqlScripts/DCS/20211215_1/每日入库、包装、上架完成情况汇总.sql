create sequence S_CompletionSummary
/

create sequence S_DailyInboundCompletion
/

create sequence S_DailyPackingCompletion
/

create sequence S_DailyShelveCompletion
/
create table CompletionSummary  (
   Id                   NUMBER(9)                       not null,
   Type                 NUMBER(9)                       not null,
   WarehouseId          NUMBER(9)                       not null,
   WarehouseCode        VARCHAR2(50)                    not null,
   WarehouseName        VARCHAR2(100)                   not null,
   WithinCompleteItem   NUMBER(9),
   WithinCompleteFee    NUMBER(19,4),
   OverCompleteItem     NUMBER(9),
   OverCompleteFee      NUMBER(19,4),
   WithinUnCompleteItem NUMBER(9),
   WithinUnCompleteFee  NUMBER(19,4),
   OverUnCompleteItem   NUMBER(9),
   OverUnCompleteFee    NUMBER(19,4),
   WithinCompleteItemRate NUMBER(19,4),
   WithinCompleteFeeRate NUMBER(19,4),
   StandardItem         NUMBER(9),
   StandardFee          NUMBER(19,4),
   CompleteItems        NUMBER(9),
   CompleteFee          NUMBER(19,4),
   CompleteItemRate     NUMBER(19,4),
   CompleteFeeRate      NUMBER(19,4),
   CreateTime           DATE,
   constraint PK_COMPLETIONSUMMARY primary key (Id)
)
/

/*==============================================================*/
/* Table: DailyInboundCompletion                                */
/*==============================================================*/
create table DailyInboundCompletion  (
   Id                   NUMBER(9)                       not null,
   WarehouseId          NUMBER(9)                       not null,
   WarehouseCode        VARCHAR2(50)                    not null,
   WarehouseName        VARCHAR2(100)                   not null,
   PartsPurchaseOrderCode VARCHAR2(50),
   ShippingCode         VARCHAR2(50),
   ShippingDate         DATE,
   ShippingAmount       NUMBER(9),
   InboundPlanCode      VARCHAR2(50),
   InboundPlanStatus    NUMBER(9),
   SupplierComfirm      VARCHAR2(100),
   SupplierComfirmDate  DATE,
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50)                    not null,
   SparePartName        VARCHAR2(100)                   not null,
   SIHCode              VARCHAR2(50),
   SalesPrice           NUMBER(19,4),
   ConfirmedAmount      NUMBER(9),
   MinSaleQuantity      NUMBER(9),
   InspectedQuantity    NUMBER(9),
   InspectedItem        NUMBER(9),
   InspectedFee         NUMBER(19,4),
   InboundDate          DATE,
   Judgment             NUMBER(9),
   CreateTime           DATE,
   constraint PK_DAILYINBOUNDCOMPLETION primary key (Id)
)
/

/*==============================================================*/
/* Table: DailyPackingCompletion                                */
/*==============================================================*/
create table DailyPackingCompletion  (
   Id                   NUMBER(9)                       not null,
   WarehouseId          NUMBER(9)                       not null,
   WarehouseCode        VARCHAR2(50)                    not null,
   WarehouseName        VARCHAR2(100)                   not null,
   PartsPurchaseOrderCode VARCHAR2(50),
   PartsInboundCheckBillCode VARCHAR2(50),
   PackingTaskCode      VARCHAR2(50),
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50)                    not null,
   SparePartName        VARCHAR2(100)                   not null,
   SIHCode              VARCHAR2(50),
   SalesPrice           NUMBER(19,4),
   MinSaleQuantity      NUMBER(9),
   InspectedQuantity    NUMBER(9),
   InboundDate          DATE,
   PackingQty           NUMBER(9),
   PackingFee           NUMBER(19,4),
   PackingItem          NUMBER(9),
   PackingFinishiDate   DATE,
   Judgment             NUMBER(9),
   CreateTime           DATE,
   constraint PK_DAILYPACKINGCOMPLETION primary key (Id)
)
/

/*==============================================================*/
/* Table: DailyShelveCompletion                                 */
/*==============================================================*/
create table DailyShelveCompletion  (
   Id                   NUMBER(9)                       not null,
   WarehouseId          NUMBER(9)                       not null,
   WarehouseCode        VARCHAR2(50)                    not null,
   WarehouseName        VARCHAR2(100)                   not null,
   PartsPurchaseOrderCode VARCHAR2(50),
   PartsInboundCheckBillCode VARCHAR2(50),
   PackingTaskCode      VARCHAR2(50),
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50)                    not null,
   SparePartName        VARCHAR2(100)                   not null,
   SIHCode              VARCHAR2(50),
   SalesPrice           NUMBER(19,4),
   MinSaleQuantity      NUMBER(9),
   PlannedAmount        NUMBER(9),
   ShelvesDate          DATE,
   ShelvesAmount        NUMBER(9),
   ShelvesFee           NUMBER(19,4),
   ShelvesItem          NUMBER(9),
   ShelvesFinishTime    DATE,
   Judgment             NUMBER(9),
   CreateTime           DATE,
   constraint PK_DAILYSHELVECOMPLETION primary key (Id)
)
