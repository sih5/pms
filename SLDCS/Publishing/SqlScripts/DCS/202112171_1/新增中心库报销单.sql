create sequence S_CenterReimbursement;

create table CenterReimbursement  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   MarketingDepartmentId NUMBER(9),
   MarketingDepartmentName VARCHAR2(100),
   CenterId             NUMBER(9),
   CenterCode           VARCHAR2(50),
   CenterName           VARCHAR2(100),
   Type                 NUMBER(9),
   Reason               VARCHAR2(500),
   DealerName           VARCHAR2(100),
   OccurrenceTime       DATE,
   Amount               NUMBER(19,4),
   Remark               VARCHAR2(2000),
   Status               NUMBER(9)                       not null,
   CreatorId            NUMBER(9)                       not null,
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   SubmitterId          NUMBER(9),
   SubmitterName        VARCHAR2(100),
   SubmitTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   InitialApproverId    NUMBER(9),
   InitialApproverName  VARCHAR2(100),
   InitialApproveTime   DATE,
   CheckerId            NUMBER(9),
   CheckerName          VARCHAR2(100),
   CheckTime            DATE,
   ApproverId           NUMBER(9),
   ApproverName         VARCHAR2(100),
   ApproveTime          DATE,
   ApproverComment      VARCHAR2(500),
   StoperId             NUMBER(9),
   Stoper               VARCHAR2(100),
   StopTime             DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   RowVersion           TIMESTAMP,
   Path                 VARCHAR2(2000),
   constraint PK_CENTERREIMBURSEMENT primary key (Id)
);

Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'CenterReimbursement_Status', '中心库报销单状态', 1, '新建', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'CenterReimbursement_Status', '中心库报销单状态', 2, '提交', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'CenterReimbursement_Status', '中心库报销单状态', 3, '初审通过', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'CenterReimbursement_Status', '中心库报销单状态', 4, '审核通过', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'CenterReimbursement_Status', '中心库报销单状态', 5, '审批通过', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'CenterReimbursement_Status', '中心库报销单状态', 6, '终止', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'CenterReimbursement_Status', '中心库报销单状态', 99, '作废', 1, 1);

Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'CenterReimbursement_Type', '中心库报销类型', 1, '特急件运费报销', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'CenterReimbursement_Type', '中心库报销类型', 2, '工单调拨运费报销', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'CenterReimbursement_Type', '中心库报销类型', 3, '服务商盘点差旅补贴', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'CenterReimbursement_Type', '中心库报销类型', 4, '经销商形象验收补贴', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'CenterReimbursement_Type', '中心库报销类型', 5, '其他', 1, 1);

Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'MultiLevelApproveConfigType', '多级审核配置类型', 5, '中心库报销', 1, 1);


Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values 
(S_CodeTemplate.Nextval, 'CenterReimbursement', 'CenterReimbursement', 1, 'CR{CORPCODE}{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);