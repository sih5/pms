alter table AgencyDealerRelation add RowVersion TIMESTAMP;

insert into InterfaceDataSyncRecord
values(s_InterfaceDataSyncRecord.Nextval,'AgencyDealerRelationToSms',1,200,sysdate,to_date('2020-01-01','yyyy-mm-dd'),null);

update AgencyDealerRelation set RowVersion=sysdate where status=1