create or replace trigger PartsBranch_TRG
  BEFORE insert or update OF increaserategroupid ON PartsBranch
  FOR EACH ROW
declare
  -- local variables here
begin
  if :old.increaserategroupid != :NEW.Increaserategroupid then
    insert into PARTSSALESPRICE_TMP
      (id, OBJID, Iostatus, CREATETIME)
      select s_PARTSSALESPRICE_TMP.Nextval, tt.code, 0, sysdate
        from partssalesprice pb
        join  sparepart tt on pb.sparepartid =tt.id
       where pb.sparepartid = :new.partid
         and pb.status = 1
         and exists
       (select 1
                from PartsSupplierRelation ps
                join PartsSupplier ss
                  on ps.supplierid = ss.id
               where ps.partid = pb.sparepartid
                 and ps.status = 1
                 and ss.code = '1000000007')
         and (exists (select 1
                        from PartsSalePriceIncreaseRate ps
                       where ps.id = :old.increaserategroupid
                         and ps.GroupCode = '1��') or exists
              (select 1
                 from PartsSalePriceIncreaseRate ps
                where ps.id = :NEW.increaserategroupid
                  and ps.GroupCode = '1��'));
  else
    if (:OLD.increaserategroupid is null or :OLD.Id is null) then
      insert into PARTSSALESPRICE_TMP
        (id, OBJID, Iostatus, CREATETIME)
        select s_PARTSSALESPRICE_TMP.Nextval, tt.code, 0, sysdate
          from partssalesprice pb
          join  sparepart tt on pb.sparepartid =tt.id
         where pb.sparepartid = :new.partid
           and pb.status = 1
           and exists (select 1
                  from PartsSupplierRelation ps
                  join PartsSupplier ss
                    on ps.supplierid = ss.id
                 where ps.partid = pb.sparepartid
                   and ps.status = 1
                   and ss.code = '1000000007')
           and exists (select 1
                  from PartsSalePriceIncreaseRate ps
                 where ps.id = :NEW.increaserategroupid
                   and ps.GroupCode = '1��');
    end if;
  end if;
end PartsBranch_TRG;
/
