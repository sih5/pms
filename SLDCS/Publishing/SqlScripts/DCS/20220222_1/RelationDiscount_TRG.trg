create or replace trigger RelationDiscount_TRG
  after insert or update on PartsSupplierRelation
  for each row
declare
  -- local variables here
begin
  if :old.id is null or :new.status = 99 then
    insert into PARTSSALESPRICE_TMP
      (id, OBJID, Iostatus, CREATETIME)
      select s_PARTSSALESPRICE_TMP.Nextval, tt.code, 0, sysdate
        from sparepart tt
       where tt.id = :new.partid
         and exists (select 1
                from PartsSupplier ss
               where ss.id = :new.supplierid
                 and ss.code = '1000000007')
         and exists (select 1
                from PartsSalePriceIncreaseRate ps
                join partsbranch pt
                  on ps.id = pt.increaserategroupid
               where pt.partid = :NEW.Partid
                 and ps.GroupCode = '1��')
         and not exists (select 1
                from PARTSSALESPRICE_TMP tp
               where tp.objid = tt.code
                 and nvl(tp.iostatus, 0) = 0);
  end if;
  if :old.supplierid != :NEW.Supplierid or :old.partid != :NEW.Partid then
    insert into PARTSSALESPRICE_TMP
      (id, OBJID, Iostatus, CREATETIME)
      select s_PARTSSALESPRICE_TMP.Nextval, tt.code, 0, sysdate   
        from sparepart tt
       where tt.id = :old.partid
         and (exists (select 1
                        from PartsSupplier ss
                       where ss.id = :old.supplierid
                         and ss.status = 1
                         and ss.code = '1000000007') and exists
              (select 1
                 from PartsSalePriceIncreaseRate ps
                 join partsbranch pt
                   on ps.id = pt.increaserategroupid
                where pt.partid = :OLD.Partid
                  and ps.GroupCode = '1��'))
         and not exists (select 1
                from PARTSSALESPRICE_TMP tp
               where tp.objid = tt.code
                 and nvl(tp.iostatus, 0) = 0);
    insert into PARTSSALESPRICE_TMP
      (id, OBJID, Iostatus, CREATETIME)
      select s_PARTSSALESPRICE_TMP.Nextval, tt.code, 0, sysdate
        from sparepart tt
       where tt.id = :new.partid
         and (exists (select 1
                        from PartsSupplier ss
                       where ss.id = :new.supplierid
                         and ss.status = 1
                         and ss.code = '1000000007') and exists
              (select 1
                 from PartsSalePriceIncreaseRate ps
                 join partsbranch pt
                   on ps.id = pt.increaserategroupid
                where pt.partid = :NEW.Partid
                  and ps.GroupCode = '1��'))
         and not exists (select 1
                from PARTSSALESPRICE_TMP tp
               where tp.objid = tt.code
                 and nvl(tp.iostatus, 0) = 0);
  end if;
end RelationDiscount_TRG;
/
