create or replace trigger SpecialDiscount_TRG
  after insert or update on SpecialDiscount
  for each row
declare
  -- local variables here
begin
  if :old.id is null or :old.discount != :new.discount then
    insert into PARTSSALESPRICE_TMP
      (id, OBJID, Iostatus, CREATETIME)
      select s_PARTSSALESPRICE_TMP.Nextval, tt.code, 0, sysdate
        from sparepart tt
       where exists (select 1
                from PartsSupplier ss
                join PartsSupplierRelation psr
                  on ss.id = psr.supplierid
                 and psr.status = 1
               where psr.partid = tt.id
                 and ss.code = '1000000007')
         and exists (select 1
                from PartsSalePriceIncreaseRate ps
                join partsbranch pt
                  on ps.id = pt.increaserategroupid
               where pt.partid = tt.id
                 and ps.GroupCode = '1��')
         and not exists (select 1
                from PARTSSALESPRICE_TMP tp
               where tp.objid = tt.code
                 and nvl(tp.iostatus, 0) = 0);
  end if;
end SpecialDiscount_TRG;
/
