create sequence S_SpecialDiscount;

create table SpecialDiscount  (
 
   Id                 NUMBER(9)                       not null,
 
   Discount           NUMBER(19,4),
 
   CreatorId          NUMBER(9),
 
   CreatorName        VARCHAR2(100),
 
   CreateTime         DATE,
 
   ModifierId         NUMBER(9),
 
   ModifierName       VARCHAR2(100),
 
   ModifyTime         DATE,
 
   constraint PK_SPECIALDISCOUNT primary key (Id)
 
)
 
/
 
