create sequence S_PartsRtnInboundCheck;


create table PartsRtnInboundCheck  (
   Id                 NUMBER(9)                       not null,
   MarketingDepartmentName VARCHAR2(200),
   Province           VARCHAR2(200),
   WarehouseId        NUMBER(9),
   WarehouseName      VARCHAR2(200),
   CounterpartCompanyName VARCHAR2(200),
   CounterpartCompanyCode VARCHAR2(200),
   PartsSalesReturnBillId NUMBER(9),
   PartsSalesReturnBillCode VARCHAR2(50),
   ReturnWarehouseCode VARCHAR2(150),
   ReturnWarehouseName VARCHAR2(150),
   ReturnReason       VARCHAR2(500),
   InBoundId          NUMBER(9),
   Code               VARCHAR2(50),
   SparePartId        NUMBER(9),
   SparePartCode      VARCHAR2(50),
   SparePartName      VARCHAR2(150),
   PartABC            NUMBER(9),
   InspectedQuantity  NUMBER(9),
   OriginalPrice      NUMBER(19,4),
   OriginalPriceAll   NUMBER(19,4),
   SettlementPrice    NUMBER(19,4),
   SettlementPriceAll NUMBER(19,4),
   IsDiscount         NUMBER(1),
   PriceType          VARCHAR2(150),
   CostPrice          NUMBER(19,4),
   CostPriceAll       NUMBER(19,4),
   SettlementId       NUMBER(9),
   SettlementNo       VARCHAR2(50),
   CreateTime         DATE,
   CreatorName        VARCHAR2(150),
   TurnDate           DATE,
   constraint PK_PARTSRTNINBOUNDCHECK primary key (Id)
)

