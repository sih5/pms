alter table   PartsPurchaseRtnSettleBill add(IsEInvoice         NUMBER(1),
   InputStatus        NUMBER(9),
   RedCode            VARCHAR2(100));
   
   alter table PartsPurchaseSettleBill add(IsEInvoice         NUMBER(1),
   InputStatus        NUMBER(9));
   create sequence S_InputSystem_Sync;
   create table InputSystem_Sync  (
   Id                 NUMBER(9)                       not null,
   Type               NUMBER(9),
   Objid              VARCHAR2(40),
   IOStatus           NUMBER(9),
   CreateTime         DATE,
   FirstSendTime      DATE,
   ModifySendTime     DATE,
   Token              VARCHAR2(50),
   Sender             VARCHAR2(20),
   Message            VARCHAR2(500),
   constraint PK_INPUTSYSTEM_SYNC primary key (Id)
);

Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'InputSystem_SyncType', '进项接口类型', 1, '采购结算', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'InputSystem_SyncType', '进项接口类型', 2, '采购退货结算', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'InputSystem_SyncType', '进项接口类型', 3, '补差', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'InputSystem_SyncType', '进项接口类型', 4, '年度返利', 1, 1);


Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PartsPurchaseSettleBillInputStatus', '进项状态', 1, '未导入', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PartsPurchaseSettleBillInputStatus', '进项状态', 2, '已导入', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PartsPurchaseSettleBillInputStatus', '进项状态', 3, '已开票', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PartsPurchaseSettleBillInputStatus', '进项状态', 4, '已校验', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PartsPurchaseSettleBillInputStatus', '进项状态', 5, '已作废', 1, 1);

alter table PartsSupplier add(IsUnlimited        NUMBER(1));
create sequence S_SupplierMaintenance;
create table SupplierMaintenance  (
   Id                 NUMBER(9)                       not null,
   Status             NUMBER(9)                       not null,
   Year               NUMBER(9)                       not null,
   SupplierId         NUMBER(9)                       not null,
   SupplierCode       VARCHAR2(50)                    not null,
   SupplierName       VARCHAR2(100)                   not null,
   Type               NUMBER(9)                       not null,
   Rate               NUMBER(19,4)                    not null,
   CreatorId          NUMBER(9),
   CreatorName        VARCHAR2(100),
   CreateTime         DATE,
   AbandonerId        NUMBER(9),
   AbandonerName      VARCHAR2(100),
   AbandonTime        DATE,
   RowVersion         TIMESTAMP,
   constraint PK_SUPPLIERMAINTENANCE primary key (Id)
);
create sequence S_SupplierAnnualRebateBill;


create table SupplierAnnualRebateBill  (
   Id                 NUMBER(9)                       not null,
   InvoiceDate        DATE,
   Code               VARCHAR2(50)                    not null,
   Status             NUMBER(9)                       not null,
   PartsSupplierId    NUMBER(9)                       not null,
   PartsSupplierCode  VARCHAR2(50)                    not null,
   PartsSupplierName  VARCHAR2(100)                   not null,
   Type               NUMBER(9)                       not null,
   TotalSettlementAmount NUMBER(19,4)                    not null,
   TaxRate            NUMBER(15,6)                    not null,
   Tax                NUMBER(19,4)                    not null,
   TotalInvoicedAmount NUMBER(19,4),
   IsEInvoice         NUMBER(1),
   InputStatus        NUMBER(9),
   RedCode            VARCHAR2(100),
   IsRed              NUMBER(1),
   InvoiceCode        VARCHAR2(50),
   InvoiceNumber      VARCHAR2(50),
   Remark             VARCHAR2(200),
   IsInterFace        NUMBER(1),
   CreatorId          NUMBER(9),
   CreatorName        VARCHAR2(100),
   CreateTime         DATE,
   ModifierId         NUMBER(9),
   ModifierName       VARCHAR2(100),
   ModifyTime         DATE,
   AbandonerId        NUMBER(9),
   AbandonerName      VARCHAR2(100),
   AbandonTime        DATE,
   ApproverId         NUMBER(9),
   ApproverName       VARCHAR2(100),
   ApproveTime        DATE,
   ApproveMent        VARCHAR2(200),
   Registrant         VARCHAR2(100),
   RegistrantId       NUMBER(9),
   RegistrantDate     DATE,
   InvoiceApproverId  NUMBER(9),
   InvoiceApproverName VARCHAR2(100),
   InvoiceApproveTime DATE,
   Rejected           VARCHAR2(100),
   RejectedId         NUMBER(9),
   RejectedTime       DATE,
   Recoil             VARCHAR2(100),
   RecoilId           NUMBER(9),
   RecoilTime         DATE,
   RowVersion         TIMESTAMP,
   SubmitterId        NUMBER(9),
   SubmitterName      VARCHAR2(100),
   SubmitTime         DATE,
   ConfirmerId        NUMBER(9),
   Confirmer          VARCHAR2(100),
   ConfirmTime        DATE,
   InvoiceTime        DATE,
   constraint PK_SUPPLIERANNUALREBATEBILL primary key (Id)
);
create sequence S_SupplierAnnualRebateDetail;
create table SupplierAnnualRebateDetail  (
   Id                 NUMBER(9)                       not null,
   SupplierAnnualRebateBillId NUMBER(9)                       not null,
   Year               NUMBER(9)                       not null,
   InBoundId          NUMBER(9)                       not null,
   InBoundCode        VARCHAR2(50)                    not null,
   SparePartId        NUMBER(9)                       not null,
   SparePartCode      VARCHAR2(50)                    not null,
   SparePartName      VARCHAR2(100)                   not null,
   SupplidrSpareCode  VARCHAR2(50),
   InBoundQty         NUMBER(9),
   InvoicePrice       NUMBER(19,4),
   PartsPurchaseOrderId NUMBER(9),
   PartsPurchaseOrderCode VARCHAR2(50),
   OrderType          NUMBER(9),
   PurchasePrice      NUMBER(19,4),
   DifferPurchasePrice NUMBER(19,4),
   DifferPurchasePriceAll NUMBER(19,4),
   SettlementPrice    NUMBER(19,4),
   Rate               NUMBER(19,4),
   TotalRebate        NUMBER(19,4),
   PartsPurchaseOrderTypeName VARCHAR2(100),
   constraint PK_SUPPLIERANNUALREBATEDETAIL primary key (Id)
);

Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'SupplierMaintenanceType', '供应商返利类型', 1, '年度返利比例', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'SupplierMaintenanceType', '供应商返利类型', 2, '质保买断比例', 1, 1);

Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'SupplierAnnualRebateBillStatus', '供应商补差返利状态',1, '新建', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'SupplierAnnualRebateBillStatus', '供应商补差返利状态', 2, '提交', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'SupplierAnnualRebateBillStatus', '供应商补差返利状态', 3, '已确认', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'SupplierAnnualRebateBillStatus', '供应商补差返利状态',4, '已审批', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'SupplierAnnualRebateBillStatus', '供应商补差返利状态', 5, '发票登记', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'SupplierAnnualRebateBillStatus', '供应商补差返利状态', 6, '已反冲', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'SupplierAnnualRebateBillStatus', '供应商补差返利状态',7, '发票驳回', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'SupplierAnnualRebateBillStatus', '供应商补差返利状态',8, '发票已审核', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'SupplierAnnualRebateBillStatus', '供应商补差返利状态', 99, '已作废', 1, 1);


Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'SupplierAnnualRebateBillType', '供应商补差返利类型', 1, '补差', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'SupplierAnnualRebateBillType', '供应商补差返利类型', 2, '年度返利', 1, 1);

Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values 
(S_CodeTemplate.Nextval, 'SupplierAnnualRebateBill', 'SupplierAnnualRebateBill', 1, 'SAR{CORPCODE}{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);


create sequence S_RebateInvoiceInformation;

create sequence S_RedCodeDetail;

create table RebateInvoiceInformation  (
   Id                 NUMBER(9)                       not null,
   SupplierAnnualRebateBillId NUMBER(9)                       not null,
   TaxRate            NUMBER(15,6)                    not null,
   Tax                NUMBER(19,4)                    not null,
   TotalInvoicedAmount NUMBER(19,4),
   InvoiceCode        VARCHAR2(50)                    not null,
   InvoiceNumber      VARCHAR2(50)                    not null,
   InvoiceTime        DATE,
   CreatorId          NUMBER(9),
   CreatorName        VARCHAR2(100),
   CreateTime         DATE,
   constraint PK_REBATEINVOICEINFORMATION primary key (Id)
);
create table RedCodeDetail  (
   Id                 NUMBER(9)                       not null,
   Type               NUMBER(9)                       not null,
   RedCode            VARCHAR2(200)                   not null,
   OrdeId             NUMBER(9)                       not null,
   constraint PK_REDCODEDETAIL primary key (Id)
)
