insert into ZRUN_mODULE values(s_ZRUN_mODULe.Nextval,'A001','系统登录');
insert into ZRUN_mODULE values(s_ZRUN_mODULe.Nextval,'B001','采购管理');
insert into ZRUN_mODULE values(s_ZRUN_mODULe.Nextval,'B002','销售管理');
insert into ZRUN_mODULE values(s_ZRUN_mODULe.Nextval,'B003','仓储物流');
insert into ZRUN_mODULE values(s_ZRUN_mODULe.Nextval,'B004','财务管理');
insert into ZRUN_mODULE values(s_ZRUN_mODULe.Nextval,'C001','基础数据管理');


insert into ZRUN_fUNCTION values(s_ZRUN_fUNCTION.Nextval,'B001','配件销售订单查询');
insert into ZRUN_fUNCTION values(s_ZRUN_fUNCTION.Nextval,'B002','配件销售订单处理');
insert into ZRUN_fUNCTION values(s_ZRUN_fUNCTION.Nextval,'B003','出口订单查询');
insert into ZRUN_fUNCTION values(s_ZRUN_fUNCTION.Nextval,'B004','出口订单处理');
insert into ZRUN_fUNCTION values(s_ZRUN_fUNCTION.Nextval,'B005','配件销售退货查询');
insert into ZRUN_fUNCTION values(s_ZRUN_fUNCTION.Nextval,'B006','配件销售退货处理');
insert into ZRUN_fUNCTION values(s_ZRUN_fUNCTION.Nextval,'B007','跨区销售备案查询');
insert into ZRUN_fUNCTION values(s_ZRUN_fUNCTION.Nextval,'B008','跨区销售备案处理');
insert into ZRUN_fUNCTION values(s_ZRUN_fUNCTION.Nextval,'B009','中心库报销查询');
insert into ZRUN_fUNCTION values(s_ZRUN_fUNCTION.Nextval,'B010','中心库报销处理');
