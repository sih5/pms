create sequence S_ZRUN_fUNCTION
/

create sequence S_ZRUN_lOG_DETAIL
/

create sequence S_ZRUN_lOG_SUM
/

create sequence S_ZRUN_mODULE
/

create table ZRUN_fUNCTION  (
   Id                   NUMBER(9)                       not null,
   IdF                  VARCHAR2(10)                    not null,
   DEScF                VARCHAR2(50),
   constraint PK_ZRUN_FUNCTION primary key (Id)
)
/

/*==============================================================*/
/* Table: ZRUN_lOG_DETAIL                                       */
/*==============================================================*/
create table ZRUN_lOG_DETAIL  (
   Id                   NUMBER(9)                       not null,
   IdM                  VARCHAR2(10)                    not null,
   IdF                  VARCHAR2(10)                    not null,
   UnAME                VARCHAR2(50)                    not null,
   UNAMEId              NUMBER(9),
   STATISTICALdATE      DATE                            not null,
   constraint PK_ZRUN_LOG_DETAIL primary key (Id)
)
/

/*==============================================================*/
/* Table: ZRUN_lOG_SUM                                          */
/*==============================================================*/
create table ZRUN_lOG_SUM  (
   Id                   NUMBER(9)                       not null,
   IdM                  VARCHAR2(10)                    not null,
   IdF                  VARCHAR2(10)                    not null,
   UnAME                VARCHAR2(50)                    not null,
   UNAMEId              NUMBER(9),
   STATISTICALdATE      DATE                            not null,
   Num                  NUMBER(9),
   constraint PK_ZRUN_LOG_SUM primary key (Id)
)
/

/*==============================================================*/
/* Table: ZRUN_mODULE                                           */
/*==============================================================*/
create table ZRUN_mODULE  (
   Id                   NUMBER(9)                       not null,
   IdM                  VARCHAR2(10)                    not null,
   DEScM                VARCHAR2(50),
   constraint PK_ZRUN_MODULE primary key (Id)
)
/
