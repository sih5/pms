create or replace procedure SIHRecommendPlanPro is
begin
  insert into SIHRecommendPlan
    select s_SIHRecommendPlan.Nextval,
           tt.warehouseid,
           tt.warehousecode, --仓库编号,
           tt.warehousename, --仓库名称,
           tt.sparepartid,
           tt.sparepartcode, --配件编号,  
           tt.ReserveCoefficient, --储备系数               
           tt.sparepartName, --配件名称
           tt.salesprice, --经销价
           tt.UpperLimitCoefficient, --库存上限系数
           tt.referencecode, --红岩号,
           tt.DirecCount, --直供客户
           tt.OrderTimes, --销售总频次
           tt.PartsSupplierName, --首选供应商名称, 
           tt.maxorderedquantity, --最大订购数
           tt.issalable, --是否可销售
           tt.IsDirectSupply, --是否直供 
           tt.IsOrderable, --是否可采购       
           tt.OrderGoodsCycle, --计划周期 
           tt.lowerlimitcoefficient, --库存下限系数
           tt.TemDays, --临时天数
           tt.ndaysavg, --日均销量
           tt.partabc, --配件属性,
           tt.SafeDays, --安全天数
           tt.standStock, --标准库存  
           round((tt.standStock * tt.salesprice),2) as standStockFee, --标准库存金额                 
           trunc( 
                 tt.ndaysavg * (tt.LowerLimitDays+tt.OrderGoodsCycle+tt.ArriveCycle+ tt.WarehousDays + tt.SafeDays) * tt.ReserveCoefficient
           ) as UpperLimit, --库存上限
           round((tt.ndaysavg * (tt.LowerLimitDays+tt.OrderGoodsCycle+tt.ArriveCycle+ tt.WarehousDays + tt.SafeDays) * tt.ReserveCoefficient * tt.salesprice), 2) as UpperLimitFee, --库存上限金额
           trunc(tt.ndaysavg * tt.LowerLimitDays * tt.ReserveCoefficient) as lowerlimit, --库存下限
           round((tt.ndaysavg * tt.LowerLimitDays * tt.ReserveCoefficient * tt.salesprice), 2) as lowerlimitFee, --库存下限金额
           tt.ArriveCycle, --到货周期    
           tt.AvailableStock, --可用库存
           round((tt.AvailableStock * salesprice), 2) as AvailableStockFee, --可用库存金额
           tt.OnWayNumber, --采购在途
           round((tt.OnWayNumber * salesprice), 2) as OnWayNumberFee, --采购在途金额
           (
             --库存上限
             trunc(tt.ndaysavg * (tt.LowerLimitDays+tt.OrderGoodsCycle+tt.ArriveCycle+ tt.WarehousDays + tt.SafeDays) * tt.ReserveCoefficient)
             - tt.AvailableStock - tt.OnWayNumber - tt.CurrentShortager
           )  as PlanQty, --理论计划数       
           (
             case
               when tt.pack = 0 then
                0
               else
                   (trunc(tt.ndaysavg * (tt.LowerLimitDays+tt.OrderGoodsCycle+tt.ArriveCycle+ tt.WarehousDays + tt.SafeDays) * tt.ReserveCoefficient) - tt.AvailableStock - tt.OnWayNumber - tt.CurrentShortager) 
                   / tt.pack * tt.pack
             end
           ) as RecommendQty, --推荐计划数  
           (case
             when ndaysavg = 0 then
              0
             else
              round(maxorderedquantity / ndaysavg, 2)
           end) as SalesPer, --日销量占比
           (case
             when round(tt.ndaysavg *
                        (((tt.ArriveCycle + nvl(tt.OrderGoodsCycle, 0) +
                        nvl(tt.WarehousDays, 0)) *
                        tt.UpperLimitCoefficient + tt.SafeDays))) = 0 then
              0
             else
              round((tt.AvailableStock + tt.OnWayNumber - tt.CurrentShortager) /
                     trunc(tt.ndaysavg * (tt.LowerLimitDays+tt.OrderGoodsCycle+tt.ArriveCycle+ tt.WarehousDays + tt.SafeDays) * tt.ReserveCoefficient),2)
           end) as StockPer, --理论库存占比
           tt.CurrentShortager, --当期欠货
           tt.pack， --包装倍数
           null,
           null,
           trunc(sysdate),
           SixAmount,
           FiveAmount,
           FourAmount,
           ThreeAmount,
           TwoAmount,
           OneAmount,
		   LowerLimitDays 
    
      from (select yy.warehouseid,
                   yy.warehousecode, --仓库编号,
                   yy.warehousename, --仓库名称,
                   yy.sparepartid,
                   yy.sparepartcode, --配件编号,          
                   yy.sparepartName, --配件名称
                   yy.partabc, --配件属性,
                   yy.salesprice, --经销价
                   yy.referencecode, --红岩号,
                   yy.OrderTimes, --销售总频次
                   yy.PartsSupplierName, --首选供应商名称,          
                   yy.issalable, --是否可销售
                   yy.IsDirectSupply, --是否直供
                   yy.IsOrderable, --是否可采购
                   yy.MinBatch as MinBatch, --最小订购量
                   yy.minpackingamount, --最小销售数量,
                   yy.OrderGoodsCycle, --计划周期
                   yy.OrderCycle, --供应商发货周期
                   yy.ArrivalCycle, --供应商物流周期   
                   yy.ndaysavg,
                   yy.SafeDays as SafeDays, --安全天数
                   trunc(
                     (
                      -- 库存上限
                      (yy.LowerLimitDays+ yy.OrderGoodsCycle + yy.ArriveCycle+ yy.WarehousDays + yy.SafeDays) * ndaysavg * ReserveCoefficient  
                      -- 库存下限
                      + yy.LowerLimitDays * ndaysavg * ReserveCoefficient
                     )/2
                   ) as standStock, --标准库存
                   yy.ReserveCoefficient, --储备系数          
                   yy.UpperLimitCoefficient, --库存上限系数
                   yy.lowerlimitcoefficient, --库存下限系数
                   yy.ArriveCycle, --到货周期
                   yy.WarehousDays, --库房天数
                   yy.TemDays, --临时天数
                   yy.AvailableStock, --可用库存
                   yy.OnWayNumber, --采购在途
                   yy.owOrder as CurrentShortager, --当期欠货
                   GetGbs(nvl(yy.MinBatch, 0), nvl(yy.minpackingamount, 0)) as pack,
                   (select count(1)
                      from CustomerDirectSpareList cd
                     where cd.sparepartid = yy.sparepartid
                       and cd.status = 1
                       and cd.ifdirectprovision = 1) as DirecCount, --直供客户数量
                   yy.maxorderedquantity, --最大订货数量
                   SixAmount,
                   FiveAmount,
                   FourAmount,
                   ThreeAmount,
                   TwoAmount,
                   OneAmount,
                   yy.LowerLimitDays 
              from (select sih.warehouseid,
                           sih.warehousecode,
                           sih.warehousename,
                           sih.sparepartid,
                           sih.sparepartcode,
                           sih.sparepartName,
                           sih.referencecode,
                           ps.name as PartsSupplierName,
                           pb.partabc,
                           pb.issalable,
                           pb.IsDirectSupply,
                           pb.IsOrderable,
                           nvl(psr.MinBatch, 0) as MinBatch,
                           sp.minpackingamount,
                           nvl(sc.OrderingCycle, 0) as OrderGoodsCycle,
                           nvl(sc.DeliveryCycle, 0) as OrderCycle,
                           nvl(sc.LogisticsCycle, 0) as ArrivalCycle,
                           sih.OrderTimes,
                           nvl(sp.TemDays, 0) as SafeDays,
                           maxo.maxorderedquantity,
                           sih.ndaysavg,
                           nvl(rf.ReserveCoefficient, 0) as ReserveCoefficient,
                           psp.salesprice,
                           nvl(rfd.UpperLimitCoefficient, 0) as UpperLimitCoefficient,
                           nvl(rfd.lowerlimitcoefficient, 0) as lowerlimitcoefficient,
                           (nvl(sc.DeliveryCycle, 0) +
                           nvl(sc.LogisticsCycle, 0)) as ArriveCycle,
                           nvl(sp.WarehousDays, 0) as WarehousDays,
                           nvl(sp.TemDays, 0) as TemDays,
                           sih.availablestock,
                           nvl(pway.OnWayNumber, 0) as OnWayNumber,
                           nvl(sales.owOrder, 0) owOrder,
                           (nvl(sih.OneWeek, 0) + nvl(sih.TwoWeek, 0) +
                           nvl(sih.ThreeWeek, 0) + nvl(sih.FourWeek, 0)) as OneAmount,
                           (nvl(sih.FiveWeek, 0) + nvl(sih.SixWeek, 0) +
                           nvl(sih.SevenWeek, 0) + nvl(sih.EightWeek, 0)) as TwoAmount,
                           (nvl(sih.NineWeek, 0) + nvl(sih.TenWeek, 0) +
                           nvl(sih.ElevenWeek, 0) + nvl(sih.TwelveWeek, 0)) as ThreeAmount,
                           (nvl(sih.ThirtWeek, 0) + nvl(sih.FourteenWeek, 0) +
                           nvl(sih.FifteenWeek, 0) + nvl(sih.SixteenWeek, 0)) as FourAmount,
                           (nvl(sih.SeventeenWeek, 0) +
                           nvl(sih.EightteenWeek, 0) +
                           nvl(sih.NineteenWeek, 0) + nvl(sih.TwentyWeek, 0)) as FiveAmount,
                           (nvl(sih.TwentyOneWeek, 0) +
                           nvl(sih.TwentyTwoWeek, 0) +
                           nvl(sih.TwentyThreeWeek, 0) +
                           nvl(sih.TwentyFourWeek, 0)) as SixAmount,
                           rf.lowerlimitdays as LowerLimitDays 
                      from SIHDailySalesAverage sih
                      left join PartsSupplierRelation psr
                        on sih.sparepartid = psr.partid
                       and psr.status = 1
                       and psr.isprimary = 1
                      left join SparePartpurchCycle sc
                        on psr.supplierid = sc.id
                       and psr.partid = sc.partid
                       and sih.warehouseid = sc.receivingwarehouseid
                       and sc.status = 1
                      left join PartsSupplier ps
                        on psr.supplierid = ps.id
                      join partsbranch pb
                        on sih.sparepartid = pb.partid
                       and pb.status = 1
                      join SparePart sp
                        on sih.sparepartid = sp.id
                      left join ReserveFactorMasterOrder rf
                        on sih.warehouseid = rf.warehouseid
                       and pb.partabc = rf.abcstrategyid
                       and rf.status = 1
                      left join PartsSalesPrice psp
                        on sih.sparepartid = psp.sparepartid
                       and psp.status = 1
                      left join ReserveFactorOrderDetail rfd
                        on rf.id = rfd.reservefactormasterorderid
                       and psp.salesprice >= rfd.PriceFloor
                       and psp.salesprice <= rfd.PriceCap
                      left join (select max(tss.OrderedQuantityOrder) as maxorderedquantity,
                                       tss.sparepartid,
                                       tss.warehouseid
                                  from TemSmartSales tss
                                 where tss.createtime = trunc(sysdate)
                                 group by tss.sparepartid, tss.warehouseid) maxo
                        on maxo.sparepartid = sih.sparepartid
                       and maxo.warehouseid = sih.warehouseid
                      left join (select a.sparepartid,
                                       a.sparepartcode,
                                       a.WarehouseId,
                                       sum((case
                                             when a.partsSupplierCode in
                                                  ('1000002367', '1000003829') then
                                              (case
                                                when a.PartsPurchaseOrderStatus in
                                                     (1, 2, 3, 4) then
                                                 a.PlanAmount
                                                when a.PartsPurchaseOrderStatus = 99 then
                                                 0
                                                when a.PartsPurchaseOrderStatus = 7 then
                                                 (case
                                                   when days > 60 then
                                                    0
                                                   else
                                                    (nvl(a.PlanAmount, 0) -
                                                    nvl(a.InspectedQuantity, 0) -
                                                    nvl(a.CompletionQuantity, 0) -
                                                    finishiQty)
                                                 end)
                                                else
                                                 (a.PlanAmount -
                                                 nvl(a.InspectedQuantity, 0) -
                                                 nvl(a.CompletionQuantity, 0) -
                                                 finishiQty)
                                              end)
                                             else
                                              (case
                                                when a.PartsPurchaseOrderStatus = 99 then
                                                 0
                                                when a.PartsPurchaseOrderStatus = 1 then
                                                 a.PlanAmount
                                                when a.PartsPurchaseOrderStatus = 2 then
                                                 (case
                                                   when nvl(a.ConfirmedAmount, 0) = 0 and
                                                        a.Shortsupreason is not null then
                                                    0
                                                   else
                                                    (a.PlanAmount -
                                                    nvl(a.InspectedQuantity, 0) -
                                                    nvl(a.CompletionQuantity, 0) -
                                                    finishiQty)
                                                 end)
                                                when a.PartsPurchaseOrderStatus = 7 then
                                                 (case
                                                   when days > 60 then
                                                    0
                                                   else
                                                    (nvl(a.ConfirmedAmount, 0) -
                                                    nvl(a.InspectedQuantity, 0) -
                                                    nvl(a.CompletionQuantity, 0) -
                                                    finishiQty)
                                                 end)
                                                else
                                                 (case
                                                   when nvl(a.ConfirmedAmount, 0) = 0 and
                                                        a.Shortsupreason is not null then
                                                    0
                                                   else
                                                    (a.ConfirmedAmount -
                                                    nvl(a.InspectedQuantity, 0) -
                                                    nvl(a.CompletionQuantity, 0) -
                                                    finishiQty)
                                                 end)
                                              end)
                                           end)) as OnWayNumber --在途数量
                                  from (select detail.sparepartid,
                                               detail.PlanAmount,
                                               detail.sparepartcode,
                                               puDetail.Confirmedamount,
                                               puDetail.Shortsupreason,
                                               puDetail.Shippingamount,
                                               orders.WarehouseId,
                                               (select sum(InspectedQuantity)
                                                  from PartsInboundCheckBillDetail a
                                                 where exists
                                                 (select 1
                                                          from PartsInboundCheckBill tmp
                                                         where a.partsinboundcheckbillid =
                                                               tmp.id
                                                           and tmp.OriginalRequirementBillType = 4
                                                           and tmp.originalrequirementbillid =
                                                               orders.id
                                                           and tmp.Status <> 4)
                                                   and a.sparepartid =
                                                       detail.sparepartid) as InspectedQuantity, --入库数量   配件入库检验单清单.检验量
                                               (select sum(nvl(a.PlannedAmount,
                                                               0)) -
                                                       sum(nvl(a.InspectedQuantity,
                                                               0))
                                                  from PartsInboundPlanDetail a
                                                  join PartsInboundPlan b
                                                    on a.PartsInboundPlanId = b.id
                                                 where b.originalrequirementbillid =
                                                       orders.id
                                                   and b.originalrequirementbilltype = 4
                                                   and a.sparepartid =
                                                       puDetail.Sparepartid
                                                   and b.status = 3) as CompletionQuantity, --入库强制完成数量  入库计划清单。计划量-检验量  计划单状态为终止
                                               (case
                                                 when orders.status = 7 then
                                                  puDetail.Confirmedamount -
                                                  nvl(puDetail.Shippingamount,
                                                      0)
                                                 else
                                                  0
                                               end) as finishiQty,
                                               (trunc(sysdate) -
                                               nvl(orders.submittime,
                                                    orders.createtime)) as days,
                                               orders.status as PartsPurchaseOrderStatus,
                                               orders.partssuppliercode as partsSupplierCode
                                          from PartsPurchaseOrder orders
                                          join warehouse wh
                                            on orders.warehouseid = wh.id
                                           and wh.IsPurchase = 1
                                          left join PartsPurchaseOrderDetail puDetail
                                            on orders.id =
                                               puDetail.Partspurchaseorderid
                                          join partspurchaseplan plans
                                            on plans.id =
                                               orders.originalrequirementbillid
                                           and plans.code =
                                               orders.originalrequirementbillcode
                                          join partspurchaseplandetail detail
                                            on plans.id =
                                               detail.purchaseplanid
                                           and detail.sparepartid =
                                               puDetail.Sparepartid
                                         where (trunc(sysdate) -
                                               trunc(nvl(orders.submittime,
                                                          orders.createtime))) <= 60) a
                                 group by a.sparepartid,
                                          a.sparepartcode,
                                          a.WarehouseId) Pway
                        on sih.sparepartid = pway.sparepartid
                       and sih.warehouseid = pway.warehouseid
                      left join (select pd.sparepartid,
                                       pd.sparepartcode,
                                       pso.warehouseid,
                                       sum(pd.orderedquantity -
                                           nvl(pd.approvequantity, 0)) as owOrder
                                  from partssalesorder pso
                                  join warehouse wh
                                    on pso.warehouseid = wh.id
                                   and wh.issales = 1
                                  join partssalesorderdetail pd
                                    on pso.id = pd.partssalesorderid
                                 where ((not exists (select 1
                                                       from PartsSalesOrderProcess pso
                                                       join PartsSalesOrderProcessDetail psd
                                                         on pso.id =
                                                            psd.partssalesorderprocessid
                                                      where pso.originalsalesorderid =
                                                            pso.id
                                                        and psd.sparepartid =
                                                            pd.sparepartid
                                                        and psd.orderprocessmethod in
                                                            (select OrderProcessMethod
                                                               from SmartProcessMethod
                                                              where status = 1)) and
                                        pso.status = 4) or pso.status = 2)
                                   and wh.storagecompanyid = 12730
                                 group by pd.sparepartid,
                                          pd.sparepartcode,
                                          pso.warehouseid) sales
                        on sih.sparepartid = sales.sparepartid
                       and sih.warehouseid = sales.warehouseid
                     where sih.carrytime = trunc(sysdate)) yy) tt;
  commit;

end SIHRecommendPlanPro;
