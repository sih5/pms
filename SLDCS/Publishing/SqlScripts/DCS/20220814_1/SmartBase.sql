create or replace procedure SmartBase is
  days      number(8);
  actdays   number(8);
  isExist   number(1);
  recodeDay number(8);
  cursor warehouses is
    select id as warehouseid, code as warehouseCode, name as warehouseName
      from warehouse
     where status = 1
       and StorageCompanyId = 12730
       and nvl(IsSales, 0) = 1;
  cursor forDays is
    SELECT ROWNUM NUM FROM DUAL CONNECT BY ROWNUM <= 190;
begin
  for temwarehouse in warehouses loop
    days      := 168;
    actdays   := 1;
    recodeDay := 1;
    for day in forDays loop
      if recodeDay <= days then
        begin
          select count(1)
            into isExist
            from SmartOrderCalendar
           where status = 1
             and WarehouseId = temwarehouse.warehouseid
             and trunc(Times) = trunc(sysdate - actdays);
        end;
        if isExist > 0 then
          actdays := actdays + 1;
        else
          insert into TemSmartSales
           select s_TemSmartSales.Nextval,
                   case when swpr.warehouseid is not null then  swpr.warehouseid 
                        when a.WarehouseName like '%区域库%' then  a.warehouseid 
                        else temwarehouse.warehouseid  end,
                   case when swpr.warehousecode is not null then  swpr.warehousecode 
                        when a.WarehouseName like '%区域库%' then  w.code
                        else temwarehouse.warehousecode   end,
                   case when swpr.warehousename is not null then  swpr.warehousename   
                        when a.WarehouseName like '%区域库%' then  a.WarehouseName 
                        else temwarehouse.warehousename  end,
                   pd.sparepartid,
                   pd.sparepartcode,
                   sp.name as sparepartname,
                   1,
                   trunc(sysdate),
                   pb.partabc,
                   sp.referencecode,
                   psp.salesprice,
                   (case
                     when ps.status = 6 then
                      pd.approvequantity
                     else
                      pd.orderedquantity
                   end) * (nvl(dsw.weight, 0)),
                   ps.code,
                   ps.FirstApproveTime,
                   pd.id,
                   ps.status,
                   pd.orderedquantity,
                   ps.submitcompanyid,
                   ps.submitcompanycode,
                   ps.submitcompanyname,
                   ((select sum(quantity)
                       from partsstock tmp
                      inner join warehouseareacategory b
                         on tmp.warehouseareacategoryid = b.id
                      where tmp.partid = pd.SparePartId
                        and tmp.warehouseid = ps.warehouseid
                        and (b.category = 1 or b.category = 3)) -
                   nvl((select sum(lockedquantity)
                          from partslockedstock tmp
                         where tmp.partid = pd.SparePartId
                           and tmp.warehouseid = ps.warehouseid),
                        0) -
                   nvl((select sum(StockQty)
                          from bottomstock tmp
                         where tmp.SparePartId = pd.SparePartId
                           and tmp.CompanyID = ps.salesunitownercompanyid
                           and tmp.status = 1),
                        0))
              from partssalesorder ps
              join partssalesorderdetail pd
                on ps.id = pd.partssalesorderid
              join partsbranch pb
                on pd.sparepartid = pb.partid
              join sparepart sp
                on pd.sparepartid = sp.id
              join PartsSalesPrice psp
                on pd.sparepartid = psp.sparepartid
               and psp.status = 1
              join DailySalesWeight dsw
                on dsw.WarehouseId = ps.warehouseid
               and dsw.Times = recodeDay
              left join SmartWarehousePartRelation swpr on pd.sparepartid=swpr.partid  and swpr.status=1
              left join Agency a on a.id=ps.SubmitCompanyId
              left join warehouse w on w.id=a.warehouseid
             where ps.warehouseid = temwarehouse.warehouseid
               and trunc(ps.FirstApproveTime) = trunc(sysdate - actdays)
               and ps.status in (4, 5, 6)
               and ps.submitcompanyid not in
                   (select CompanyId from SmartCompany where status = 1)
              and not exists
             (select 1
                      from PartsSalesOrderProcess pso
                      join PartsSalesOrderProcessDetail psd
                        on pso.id = psd.partssalesorderprocessid
                     where pso.originalsalesorderid = ps.id
                       and psd.sparepartid = pd.sparepartid
                       and psd.orderprocessmethod in
                           (select OrderProcessMethod
                              from SmartProcessMethod
                             where status = 1));
          actdays   := actdays + 1;
          recodeDay := recodeDay + 1;
          commit;
        end if;
      end if;
    end loop;
  end loop;
  commit;
end SmartBase;
