 

alter table AgencyDifferenceBackBill add (   Responsibility       NUMBER(9),
   ApProvalResolution   VARCHAR2(500),
   RejectedId           NUMBER(9),
   RejectedName         VARCHAR2(100),
   RejectedTime         DATE,
   IsChange             NUMBER(1),
   IsMaterial NUMBER(1));
 
 
 
alter table PackingTask add(Remark VARCHAR2(500));

Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'Responsibility', '中心库收货差异处理责任类型', 1, '红岩责任', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'Responsibility', '中心库收货差异处理责任类型', 2, '供应商责任', 1, 1);