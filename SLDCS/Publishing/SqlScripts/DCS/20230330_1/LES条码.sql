alter table DealerRetailOrderDetail add(   LESCode              VARCHAR2(1500));

alter table PartsRetailOrderDetail add(     TraceProperty        NUMBER(9),
   LESCode              VARCHAR2(1500));

Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'TraceProperty', '追溯属性', 3, 'LES条码', 1, 1);