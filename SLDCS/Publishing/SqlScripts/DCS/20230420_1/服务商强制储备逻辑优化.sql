alter table DealerReservesRecommended  modify (   DailyDemand        NUMBER(19,4));

alter table SparePart  add(   MinCarAmount       NUMBER(9));

alter table  MaintenanceMandatory add (   MaxDays            NUMBER(9));


update keyvalueitem kv set kv.value='单价>5000元' where kv.name='MaintenancePartPrice' and kv.key=1;
update keyvalueitem kv set kv.value='2000元＜单价≤5000元' where kv.name='MaintenancePartPrice' and kv.key=2;


delete from StationServiceReport ;

update MaintenanceMandatory set MaxDays=600;
