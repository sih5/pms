alter  table PartsPurchasePlanTemp add (   PackPurcheseTime   DATE,
   PackPartsPurchaseOrderCode VARCHAR2(50));
   
   alter table PackingTask add(   IsUse              NUMBER(1));
   
   alter  table PartsPurchasePlanTemp modify (      PartsPurchaseOrderId NUMBER(9)                       not null,
   PartsPurchaseOrderCode VARCHAR2(50)                    not null);
   
   
   create sequence S_PackagingCollection;
   create table PackagingCollection  (
   Id                 NUMBER(9)                       not null,
   Code               VARCHAR2(50)                    not null,
   WarehouseId        NUMBER(9),
   WarehouseCode      VARCHAR2(50),
   WarehouseName      VARCHAR2(100),
   Status             NUMBER(9),
   CreatorId          NUMBER(9),
   CreatorName        VARCHAR2(100),
   CreateTime         DATE,
   ModifierId         NUMBER(9),
   ModifierName       VARCHAR2(100),
   ModifyTime         DATE,
   CheckerId          NUMBER(9),
   CheckerName        VARCHAR2(100),
   CheckTime          DATE,
   AbandonerId        NUMBER(9),
   AbandonerName      VARCHAR2(100),
   AbandonTime        DATE,
   constraint PK_PACKAGINGCOLLECTION primary key (Id)
);
create sequence S_PackagingCollectionDetail;
create table PackagingCollectionDetail  (
   Id                 NUMBER(9)                       not null,
   PackagingCollectionId NUMBER(9)                       not null,
   PackingId          NUMBER(9)                       not null,
   PackingCode        VARCHAR2(50)                    not null,
   SparePartId        NUMBER(9)                       not null,
   SparePartCode      VARCHAR2(50),
   SparePartName      VARCHAR2(100),
   PlanQty            NUMBER(9),
   PackingMaterialId  NUMBER(9),
   PackingMaterialName VARCHAR2(100),
   PackingMaterial    VARCHAR2(50),
   ReferenceCode      VARCHAR2(50),
   PackNum            NUMBER(9),
   PackNumConfirm     NUMBER(9),
   constraint PK_PACKAGINGCOLLECTIONDETAIL primary key (Id)
);

Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PackagingCollectionStatus','包材待领用单状态', 1, '新增', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PackagingCollectionStatus','包材待领用单状态', 2, '已审核', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PackagingCollectionStatus','包材待领用单状态', 99, '已作废', 1, 1);

Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values 
(S_CodeTemplate.Nextval, 'PackagingCollection', 'PackagingCollection', 1, 'PKC{CORPCODE}{DATE:yyyyMMdd}{SERIAL:000000}', 1, NULL);




