alter table PaymentBill add(   DraftVersion       NUMBER(9),
   DraftCdRange       VARCHAR2(50));
   Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PaymentBillDraftVersion', '来款单新老票据', 1, '新票', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PaymentBillDraftVersion', '来款单新老票据', 2, '老票', 1, 1);

alter table PartsSupplier add(   PaymentMethod      NUMBER(9),
   PaymentTerms       NUMBER(9));
   alter table PartsPurchaseSettleBill add(   PaymentMethod      NUMBER(9),
   PaymentTerms       NUMBER(9));
   alter table PartsPurchaseRtnSettleBill add(   PaymentMethod      NUMBER(9),
   PaymentTerms       NUMBER(9));
   alter table SupplierAnnualRebateBill add(   PaymentMethod      NUMBER(9),
   PaymentTerms       NUMBER(9));

 Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PartsSupplierPaymentMethod', '供应商付款方式', 1, '现金', 1, 1);
 Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PartsSupplierPaymentMethod', '来款单新老票据', 2, '票据', 1, 1);

 Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PartsSupplierPaymentTerms', '供应商支付条件', 1, '立即付款', 1, 1);
 Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PartsSupplierPaymentTerms', '供应商支付条件', 2, '15天付款', 1, 1);
 Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PartsSupplierPaymentTerms', '供应商支付条件', 3, '30天付款', 1, 1);
 Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PartsSupplierPaymentTerms', '供应商支付条件', 4, '45天付款', 1, 1);
 Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PartsSupplierPaymentTerms', '供应商支付条件', 5, '60天付款', 1, 1);
 Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PartsSupplierPaymentTerms', '供应商支付条件', 6, '90天付款', 1, 1);
 Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PartsSupplierPaymentTerms', '供应商支付条件', 7, '120天付款', 1, 1);
 Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PartsSupplierPaymentTerms', '供应商支付条件', 8, '210天付款', 1, 1);
 Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PartsSupplierPaymentTerms', '供应商支付条件', 9, '240天付款', 1, 1);
 Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PartsSupplierPaymentTerms', '供应商支付条件', 10, '270天付款', 1, 1);
 Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PartsSupplierPaymentTerms', '供应商支付条件', 11, '300天付款', 1, 1);
 Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PartsSupplierPaymentTerms', '供应商支付条件', 12, '330天付款', 1, 1);


