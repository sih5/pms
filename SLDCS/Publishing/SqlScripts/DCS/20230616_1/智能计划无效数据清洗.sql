create sequence S_SmartOrderAbnormal;

create sequence S_SmartOrderAndType;

create table SmartOrderAbnormal  (
   Id                 NUMBER(9)                       not null,
   PartsSalesOrderId  NUMBER(9)                       not null,
   PartsSalesOrderCode VARCHAR2(50)                    not null,
   SparePartId        NUMBER(9)                       not null,
   SparePartCode      VARCHAR2(50)                    not null,
   SparePartName      VARCHAR2(100)                   not null,
   Status             NUMBER(9)                       not null,
   CreatorId          NUMBER(9),
   CreatorName        VARCHAR2(100),
   CreateTime         DATE,
   ModifierId         NUMBER(9),
   ModifierName       VARCHAR2(100),
   ModifyTime         DATE,
   RowVersion         TIMESTAMP,
   constraint PK_SMARTORDERABNORMAL primary key (Id)
)
;
create table SmartOrderAndType  (
   Id                 NUMBER(9)                       not null,
   WarehouseId        NUMBER(9)                       not null,
   WarehouseName      VARCHAR2(100)                   not null,
   WarehouseCode      VARCHAR2(100)                   not null,
   PartsSalesOrderTypeId NUMBER(9)                       not null,
   PartsSalesOrderTypeName VARCHAR2(100)                   not null,
   Status             NUMBER(9)                       not null,
   CreatorId          NUMBER(9),
   CreatorName        VARCHAR2(100),
   CreateTime         DATE,
   ModifierId         NUMBER(9),
   ModifierName       VARCHAR2(100),
   ModifyTime         DATE,
   RowVersion         TIMESTAMP,
   constraint PK_SMARTORDERANDTYPE primary key (Id)
)
