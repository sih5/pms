create or replace procedure CalculatIntelligentPlanPro is
begin
  update CalculatIntelligentPlan set IsCalculate = 1, ModifyTime = sysdate;
  commit;
end CalculatIntelligentPlanPro;
/
