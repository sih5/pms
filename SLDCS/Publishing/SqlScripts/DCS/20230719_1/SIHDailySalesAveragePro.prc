create or replace procedure SIHDailySalesAveragePro is
begin
   --插入新数据到配件公司日均销量明细表
  insert into SIHDailySalesAverage
    select s_SIHDailySalesAverage.Nextval, yy.*
      from (select y.warehouseid,
                   y.warehousecode,
                   y.warehousename,
                   y.sparepartid,
                   y.sparepartcode,
                   y.sparepartname,
                   y.partabc,
                   y.salesprice,                   
                   y.referencecode,
                  decode(nvl((select sum(weight) from dailysalesweight op where times>=1 and times<=7 and op.WAREHOUSEID=y.warehouseid),0),0,0,round(sum(one) / nvl((select sum(weight) from dailysalesweight op where times>=1 and times<=7 and op.WAREHOUSEID=y.warehouseid),0), 3))    as oneweek,
               decode(nvl((select sum(weight) from dailysalesweight op  where times>=8 and times<=14 and op.WAREHOUSEID=y.warehouseid),0),0,0,round(sum(two) / nvl((select sum(weight) from dailysalesweight op  where times>=8 and times<=14 and op.WAREHOUSEID=y.warehouseid),0),3))     as two,
                decode(nvl((select sum(weight) from dailysalesweight op where times>=15 and times<=21 and op.WAREHOUSEID=y.warehouseid),0),0,0,round(sum(three) / nvl((select sum(weight) from dailysalesweight op where times>=15 and times<=21 and op.WAREHOUSEID=y.warehouseid),0), 3))      as three,
                decode(nvl((select sum(weight) from dailysalesweight  op where times>=22 and times<=27 and op.WAREHOUSEID=y.warehouseid),0),0,0,round(sum(four) / nvl((select sum(weight) from dailysalesweight  op where times>=22 and times<=27 and op.WAREHOUSEID=y.warehouseid),0), 3))      as four,
               decode(nvl((select sum(weight) from dailysalesweight op where times>=28 and times<=35 and op.WAREHOUSEID=y.warehouseid),0),0,0,round(sum(five) / nvl((select sum(weight) from dailysalesweight op where times>=28 and times<=35 and op.WAREHOUSEID=y.warehouseid),0), 3))       as five,
               decode(nvl((select sum(weight) from dailysalesweight op where times>=36 and times<=42 and op.WAREHOUSEID=y.warehouseid),0),0,0, round(sum(six) / nvl((select sum(weight) from dailysalesweight op where times>=36 and times<=42 and op.WAREHOUSEID=y.warehouseid),0), 3))      as six,
               decode(nvl((select sum(weight) from dailysalesweight op where times>=43 and times<=49 and op.WAREHOUSEID=y.warehouseid ),0),0,0,round(sum(seven) / nvl((select sum(weight) from dailysalesweight op where times>=43 and times<=49 and op.WAREHOUSEID=y.warehouseid ),0), 3) )      as seven,
               decode( nvl((select sum(weight) from dailysalesweight op where times>=50 and times<=56 and op.WAREHOUSEID=y.warehouseid),0),0,0,round(sum(eight) / nvl((select sum(weight) from dailysalesweight op where times>=50 and times<=56 and op.WAREHOUSEID=y.warehouseid),0), 3))       as eight,
                decode(nvl((select sum(weight) from dailysalesweight op where times>=57 and times<=63 and op.WAREHOUSEID=y.warehouseid),0),0,0,round(sum(nine) / nvl((select sum(weight) from dailysalesweight op where times>=57 and times<=63 and op.WAREHOUSEID=y.warehouseid),0), 3))      as nine,
                decode(nvl((select sum(weight) from dailysalesweight op where times>=64 and times<=70 and op.WAREHOUSEID=y.warehouseid),0),0,0, round(sum(ten) / nvl((select sum(weight) from dailysalesweight op where times>=64 and times<=70 and op.WAREHOUSEID=y.warehouseid),0), 3) )    as ten,
                 decode(nvl((select sum(weight) from dailysalesweight op where times>=71 and times<=77 and op.WAREHOUSEID=y.warehouseid),0),0,0,round(sum(eleven) / nvl((select sum(weight) from dailysalesweight op where times>=71 and times<=77 and op.WAREHOUSEID=y.warehouseid),0), 3))     as eleven,
                decode( nvl((select sum(weight) from dailysalesweight op where times>=78 and times<=84 and op.WAREHOUSEID=y.warehouseid),0),0,0, round(sum(twelve) / nvl((select sum(weight) from dailysalesweight op where times>=78 and times<=84 and op.WAREHOUSEID=y.warehouseid),0), 3))     as twelve,
               decode( nvl((select sum(weight) from dailysalesweight op where times>=85 and times<=91 and op.WAREHOUSEID=y.warehouseid),0),0,0,round(sum(thirten) / nvl((select sum(weight) from dailysalesweight op where times>=85 and times<=91 and op.WAREHOUSEID=y.warehouseid),0), 3))       as thirten,
                decode(nvl((select sum(weight) from dailysalesweight op where times>=92 and times<=98 and op.WAREHOUSEID=y.warehouseid),0),0,0, round(sum(fourteen) / nvl((select sum(weight) from dailysalesweight op where times>=92 and times<=98 and op.WAREHOUSEID=y.warehouseid),0), 3)) as fourteen,
                decode(nvl((select sum(weight) from dailysalesweight op where times>=99 and times<=105 and op.WAREHOUSEID=y.warehouseid),0),0,0,round(sum(fifteen) / nvl((select sum(weight) from dailysalesweight op where times>=99 and times<=105 and op.WAREHOUSEID=y.warehouseid),0), 3)) as fiften,
                decode(nvl((select sum(weight) from dailysalesweight op where times>=106 and times<=112 and op.WAREHOUSEID=y.warehouseid),0),0,0,round(sum(sixteen) / nvl((select sum(weight) from dailysalesweight op where times>=106 and times<=112 and op.WAREHOUSEID=y.warehouseid),0), 3)) as sixten,
               decode(nvl((select sum(weight) from dailysalesweight op where times>=113 and times<=119 and op.WAREHOUSEID=y.warehouseid),0),0,0, round(sum(seventeen) / nvl((select sum(weight) from dailysalesweight op where times>=113 and times<=119 and op.WAREHOUSEID=y.warehouseid),0), 3)) as seventeen,
                decode(nvl((select sum(weight) from dailysalesweight op where times>=120 and times<=126 and op.WAREHOUSEID=y.warehouseid),0),0,0,round(sum(eighteen) / nvl((select sum(weight) from dailysalesweight op where times>=120 and times<=126 and op.WAREHOUSEID=y.warehouseid),0), 3)) as eighteen,                
                decode(nvl((select sum(weight) from dailysalesweight op where times>=127 and times<=133 and op.WAREHOUSEID=y.warehouseid),0),0,0,round(sum(nineteen) / nvl((select sum(weight) from dailysalesweight op where times>=127 and times<=133 and op.WAREHOUSEID=y.warehouseid),0), 3)) as nineteen,                
                 decode(nvl((select sum(weight) from dailysalesweight op where times>=134 and times<=140 and op.WAREHOUSEID=y.warehouseid),0),0,0, round(sum(twenty) / nvl((select sum(weight) from dailysalesweight op where times>=134 and times<=140 and op.WAREHOUSEID=y.warehouseid),0), 3)) as twenty,
                decode(nvl((select sum(weight) from dailysalesweight op where times>=141 and times<=147 and op.WAREHOUSEID=y.warehouseid),0),0,0, round(sum(twentynoe) / nvl((select sum(weight) from dailysalesweight op where times>=141 and times<=147 and op.WAREHOUSEID=y.warehouseid),0), 3) )as twentyone,
                 decode(nvl((select sum(weight) from dailysalesweight op where times>=148 and times<=154 and op.WAREHOUSEID=y.warehouseid),0),0,0, round(sum(twentytwo) / nvl((select sum(weight) from dailysalesweight op where times>=148 and times<=154 and op.WAREHOUSEID=y.warehouseid),0), 3)) as twentytwo,
                 decode(nvl((select sum(weight) from dailysalesweight op where times>=155 and times<=161 and op.WAREHOUSEID=y.warehouseid),0),0,0, round(sum(twentythree) / nvl((select sum(weight) from dailysalesweight op where times>=155 and times<=161 and op.WAREHOUSEID=y.warehouseid),0), 3)) as twentythree,
                decode(nvl((select sum(weight) from dailysalesweight op where times>=162 and times<=168 and op.WAREHOUSEID=y.warehouseid),0),0,0, round(sum(twentyfour) / nvl((select sum(weight) from dailysalesweight op where times>=162 and times<=168 and op.WAREHOUSEID=y.warehouseid),0), 3)) as twentyfour,
                 decode(nvl((select sum(weight) from dailysalesweight op where op.warehouseid=y.warehouseid),0),0,0,    round(sum(orderedquantity) / nvl((select sum(weight) from dailysalesweight op where op.warehouseid=y.warehouseid),0), 3)) as avg,
                   sum( case when weight=0 then 0 else 1 end  )times,
                   trunc(sysdate),
                   availablestock,
                    (nvl(y.twentyfour, 0) +
                           nvl(y.twentythree, 0) +
                           nvl(y.twentytwo, 0) +
                           nvl(y.twentynoe, 0)) as SixAmount,
                            (nvl(y.seventeen, 0) +
                           nvl(y.eighteen, 0) +
                           nvl(y.nineteen, 0) + nvl(y.twenty, 0)) as FiveAmount,
                            (nvl(y.thirten, 0) + nvl(y.fourteen, 0) +
                           nvl(y.fifteen, 0) + nvl(y.sixteen, 0)) as FourAmount,
                           (nvl(y.nine, 0) + nvl(y.ten, 0) +
                           nvl(y.eleven, 0) + nvl(y.twelve, 0)) as ThreeAmount,
                            (nvl(y.five, 0) + nvl(y.six, 0) +
                           nvl(y.seven, 0) + nvl(y.eight, 0)) as TwoAmount,
                    (nvl(y.one, 0) + nvl(y.two, 0) +
                           nvl(y.three, 0) + nvl(y.four, 0)) as OneAmount                              
              from (select ts.warehouseid,
                           ts.warehousecode,
                           ts.warehousename,
                           ts.sparepartid,
                           ts.sparepartcode,
                           ts.sparepartname,
                           ts.times,
                           ts.createtime,
                           ts.partabc,
                           ts.referencecode,
                           ts.salesprice,
                           ts.orderedquantity,
                           ts.code,
                          (
                             nvl((select sum(weight)  from dailysalesweight op where op.times=ts.times and op.WAREHOUSEID=ts.warehouseid),0)
                             
                           ) weight,
                           (case
                             when ts.times >= 1 and ts.times <= 7 then
                              ts.orderedquantity
                             else
                              0
                           end) as one,
                           (case
                             when ts.times >= 8 and ts.times <= 14 then
                              ts.orderedquantity
                             else
                              0
                           end) as two,
                           (case
                             when ts.times >= 15 and ts.times <= 21 then
                              ts.orderedquantity
                             else
                              0
                           end) as three,
                           (case
                             when ts.times >= 22 and ts.times <= 28 then
                              ts.orderedquantity
                             else
                              0
                           end) as four,
                           (case
                             when ts.times >= 29 and ts.times <= 35 then
                              ts.orderedquantity
                             else
                              0
                           end) as five,
                           (case
                             when ts.times >= 36 and ts.times <= 42 then
                              ts.orderedquantity
                             else
                              0
                           end) as six,
                           (case
                             when ts.times >= 43 and ts.times <= 49 then
                              ts.orderedquantity
                             else
                              0
                           end) as seven,
                           (case
                             when ts.times >= 50 and ts.times <= 56 then
                              ts.orderedquantity
                             else
                              0
                           end) as eight,
                           (case
                             when ts.times >= 57 and ts.times <= 63 then
                              ts.orderedquantity
                             else
                              0
                           end) as nine,
                           (case
                             when ts.times >= 64 and ts.times <= 70 then
                              ts.orderedquantity
                             else
                              0
                           end) as ten,
                           (case
                             when ts.times >= 71 and ts.times <= 77 then
                              ts.orderedquantity
                             else
                              0
                           end) as eleven,
                           (case
                             when ts.times >= 78 and ts.times <= 84 then
                              ts.orderedquantity
                             else
                              0
                           end) as twelve,
                           (case
                             when ts.times >= 85 and ts.times <= 91 then
                              ts.orderedquantity
                             else
                              0
                           end) as thirten,
                           (case
                             when ts.times >= 92 and ts.times <= 98 then
                              ts.orderedquantity
                             else
                              0
                           end) as fourteen,
                           (case
                             when ts.times >= 99 and ts.times <= 105 then
                              ts.orderedquantity
                             else
                              0
                           end) as fifteen,
                           (case
                             when ts.times >= 106 and ts.times <= 112 then
                              ts.orderedquantity
                             else
                              0
                           end) as sixteen,
                           (case
                             when ts.times >= 113 and ts.times <= 119 then
                              ts.orderedquantity
                             else
                              0
                           end) as seventeen,
                           (case
                             when ts.times >= 120 and ts.times <= 126 then
                              ts.orderedquantity
                             else
                              0
                           end) as eighteen,
                           (case
                             when ts.times >= 127 and ts.times <= 133 then
                              ts.orderedquantity
                             else
                              0
                           end) as nineteen,
                           (case
                             when ts.times >= 134 and ts.times <= 140 then
                              ts.orderedquantity
                             else
                              0
                           end) as twenty,
                           (case
                             when ts.times >= 141 and ts.times <= 147 then
                              ts.orderedquantity
                             else
                              0
                           end) as twentynoe,
                           (case
                             when ts.times >= 148 and ts.times <= 154 then
                              ts.orderedquantity
                             else
                              0
                           end) as twentytwo,
                           (case
                             when ts.times >= 155 and ts.times <= 161 then
                              ts.orderedquantity
                             else
                              0
                           end) as twentythree,
                           (case
                             when ts.times >= 162 and ts.times <= 168 then
                              ts.orderedquantity
                             else
                              0
                           end) as twentyfour,
                           ts.availablestock
                      from TemSmartSales ts
                     where ts.createtime = trunc(sysdate) and status<>2 ) y
             group by y.warehouseid,
                      y.warehousecode,
                      y.warehousename,
                      y.sparepartid,
                      y.sparepartcode,
                      y.sparepartname,
                      y.partabc,
                      y.referencecode,
                      y.salesprice,
                      y.availablestock) yy;
  commit;
end SIHDailySalesAveragePro;
/
