create or replace procedure SIHRecommendPlanPro is
begin
  insert into SIHRecommendPlan

    select s_SIHRecommendPlan.Nextval, ---id
           tt.warehouseid,---warehouseid
           tt.warehousecode, --warehousecode  仓库编号,
           tt.warehousename, -- warehousename仓库名称,
           tt.sparepartid,--sparepartid
           tt.sparepartcode, --sparepartcode 配件编号,
           tt.ReserveCoefficient, --reservecoefficient 储备系数
           tt.sparepartName, -- sparepartname 配件名称
           tt.salesprice, --salesprice 经销价
           round((tt.standStock * tt.salesprice),2) as standStockFee, --standstockfee 标准库存金额
           tt.UpperLimitCoefficient, --upperlimitcoefficient库存上限系数
           tt.referencecode, --referencecode红岩号,
           tt.DirecCount, --direccount 直供客户
           tt.OrderTimes, -- ordertimes 销售总频次
           tt.PartsSupplierName, --partssuppliername 首选供应商名称,
           tt.maxorderedquantity, --maxorderedquantity 最大订购数
           tt.issalable, --issalable 是否可销售
           tt.IsDirectSupply, --isdirectsupply 是否直供
           tt.IsOrderable, --isorderable 是否可采购
           tt.OrderGoodsCycle, --ordergoodscycle计划周期
           tt.lowerlimitcoefficient, --lowerlimitcoefficient 库存下限系数
           tt.TemDays, --temdays 临时天数
           tt.ndaysavg, --ndaysavg 日均销量
           tt.partabc, --partabc 配件属性,
           tt.SafeDays, --safeday 安全天数
           tt.standStock, --standstock 标准库存
           trunc(
                 tt.ndaysavg * (tt.LowerLimitDays+tt.OrderGoodsCycle+tt.ArriveCycle+ tt.WarehousDays + tt.SafeDays) * tt.ReserveCoefficient
           ) as UpperLimit, --upperlimit 库存上限=日均销量*(库存下限天数+计划周期+到货周期+ 库房天数 +安全天数) * 储备系数
           round((tt.ndaysavg * (tt.LowerLimitDays+tt.OrderGoodsCycle+tt.ArriveCycle+ tt.WarehousDays + tt.SafeDays) * tt.ReserveCoefficient * tt.salesprice), 2) as UpperLimitFee, --upperlimitfee库存上限金额
           trunc(tt.ndaysavg * tt.LowerLimitDays * tt.ReserveCoefficient) as lowerlimit, --lowerlimit库存下限=日均销量 * 库存下限天数 * 储备系数
           round((tt.ndaysavg * tt.LowerLimitDays * tt.ReserveCoefficient * tt.salesprice), 2) as lowerlimitFee, --lowerlimitfee库存下限金额
           tt.ArriveCycle, --arrivecycle到货周期
           tt.AvailableStock, --availablestock 可用库存
           round((tt.AvailableStock * salesprice), 2) as AvailableStockFee, --availablestockfee 可用库存金额
           tt.OnWayNumber, --onwaynumber 采购在途
           round((tt.OnWayNumber * salesprice), 2) as OnWayNumberFee, --onwaynumberfee采购在途金额
           (
             --库存上限
             trunc(tt.ndaysavg * (tt.LowerLimitDays+tt.OrderGoodsCycle+tt.ArriveCycle+ tt.WarehousDays + tt.SafeDays) * tt.ReserveCoefficient)
             - tt.AvailableStock - tt.OnWayNumber - tt.CurrentShortager
           )  as PlanQty, --planqty理论计划数
           (
             case
               when tt.pack = 0 then
                0
               else
                   (trunc(tt.ndaysavg * (tt.LowerLimitDays+tt.OrderGoodsCycle+tt.ArriveCycle+ tt.WarehousDays + tt.SafeDays) * tt.ReserveCoefficient) - tt.AvailableStock - tt.OnWayNumber - tt.CurrentShortager)
                   / tt.pack * tt.pack
             end
           ) as RecommendQty, --recommendqty推荐计划数
           (case
             when ndaysavg = 0 then
              0
             else
              round(maxorderedquantity / ndaysavg, 2)
           end) as SalesPer, --salesper日销量占比
           (case
             when trunc(tt.ndaysavg * (tt.LowerLimitDays+tt.OrderGoodsCycle+tt.ArriveCycle+ tt.WarehousDays + tt.SafeDays) * tt.ReserveCoefficient) = 0 then
              0
             else
              round((tt.AvailableStock + tt.OnWayNumber - tt.CurrentShortager) /
                     trunc(tt.ndaysavg * (tt.LowerLimitDays+tt.OrderGoodsCycle+tt.ArriveCycle+ tt.WarehousDays + tt.SafeDays) * tt.ReserveCoefficient),2)
           end) as StockPer, --stockper理论库存占比
           tt.CurrentShortager, --currentshortager当期欠货
           tt.pack, --pack包装倍数
           null,--actqty 实际计划数量
           null,--actqtyfee 实际计划金额
           trunc(sysdate),--carrytime 结转时间
           SixAmount,--sixamount 前第6个4周订货数量
           FiveAmount,--fiveamount 前第5个4周订货数量
           FourAmount,--fouramount 前第4个4周订货数量
           ThreeAmount,--threeamount 前第3个4周订货数量
           TwoAmount,--twoamount 前第2个4周订货数量
           OneAmount,--oneamount 前第1个4周订货数量
           LowerLimitDays--lowerlimitdays 库存下限天数

      from (select yy.warehouseid,
                   yy.warehousecode, --仓库编号,
                   yy.warehousename, --仓库名称,
                   yy.sparepartid,
                   yy.sparepartcode, --配件编号,
                   yy.sparepartName, --配件名称
                   yy.partabc, --配件属性,
                   yy.salesprice, --经销价
                   yy.referencecode, --红岩号,
                   yy.OrderTimes, --销售总频次
                   yy.PartsSupplierName, --首选供应商名称,
                   yy.issalable, --是否可销售
                   yy.IsDirectSupply, --是否直供
                   yy.IsOrderable, --是否可采购
                   yy.MinBatch as MinBatch, --最小订购量
                   yy.minpackingamount, --最小销售数量,
                   yy.OrderGoodsCycle, --计划周期
                   yy.OrderCycle, --供应商发货周期
                   yy.ArrivalCycle, --供应商物流周期
                   yy.ndaysavg,--ndaysavg 日均销量
                   yy.SafeDays as SafeDays, --安全天数
                   trunc(
                     (
                      -- 库存上限
                      (yy.LowerLimitDays+ yy.OrderGoodsCycle + yy.ArriveCycle+ yy.WarehousDays + yy.SafeDays) * ndaysavg * ReserveCoefficient
                      -- 库存下限
                      + yy.LowerLimitDays * ndaysavg * ReserveCoefficient
                     )/2
                   ) as standStock, --标准库存
                   yy.ReserveCoefficient, --储备系数
                   yy.UpperLimitCoefficient, --库存上限系数
                   yy.lowerlimitcoefficient, --库存下限系数
                   yy.ArriveCycle, --到货周期
                   yy.WarehousDays, --库房天数
                   yy.TemDays, --临时天数
                   yy.AvailableStock, --可用库存
                   yy.OnWayNumber, --采购在途
                   yy.owOrder as CurrentShortager, --当期欠货
                   GetGbs(nvl(yy.MinBatch, 0), nvl(yy.minpackingamount, 0)) as pack,--pack包装倍数
                   (select count(1)
                      from CustomerDirectSpareList cd
                     where cd.sparepartid = yy.sparepartid
                       and cd.status = 1
                       and cd.ifdirectprovision = 1) as DirecCount, --直供客户数量
                   yy.maxorderedquantity, --最大订货数量
                   SixAmount,--sixamount 前第6个4周订货数量
                   FiveAmount,--fiveamount 前第5个4周订货数量
                   FourAmount,--fouramount 前第4个4周订货数量
                   ThreeAmount,--threeamount 前第3个4周订货数量
                   TwoAmount,--twoamount 前第2个4周订货数量
                   OneAmount,--oneamount 前第1个4周订货数量
                   yy.LowerLimitDays --lowerlimitdays 库存下限天数
              from (select sih.warehouseid,--SIHDailySalesAverage  仓库Id
                           sih.warehousecode,--SIHDailySalesAverage 仓库编号
                           sih.warehousename,--SIHDailySalesAverage 仓库名称
                           sih.sparepartid,--SIHDailySalesAverage 配件Id
                           sih.sparepartcode,--SIHDailySalesAverage 配件编号
                           sih.sparepartName,--SIHDailySalesAverage 配件名称
                           sih.referencecode,--SIHDailySalesAverage 红岩号
                           ps.name as PartsSupplierName,--PartsSupplier  供应商名称
                           pb.partabc,--partsbranch 配件ABC分类
                           pb.issalable,--partsbranch 是否可销售
                           pb.IsDirectSupply,--partsbranch 是否可直供
                           pb.IsOrderable,--partsbranch 是否可采购
                           nvl(psr.MinBatch, 0) as MinBatch,-- PartsSupplierRelation 最小采购批量
                           sp.minpackingamount,--SparePart 最小包装数量
                           nvl(sc.OrderingCycle, 0) as OrderGoodsCycle,-- SparePartpurchCycle  供应商订货周期
                           nvl(sc.DeliveryCycle, 0) as OrderCycle,-- SparePartpurchCycle 供应商发货周期
                           nvl(sc.LogisticsCycle, 0) as ArrivalCycle,-- SparePartpurchCycle 供应商物流周期
                           sih.OrderTimes,-- SIHDailySalesAverage 总频次
                           nvl(sp.TemDays, 0) as SafeDays,--SparePart 临时天数
                           maxo.maxorderedquantity,--复杂计算字段 max(TemSmartSales.OrderedQuantityOrder)
                           nvl(sih.ndaysavg, 0) as ndaysavg,--SIHDailySalesAverage N天
                           nvl(rf.ReserveCoefficient, 0) as ReserveCoefficient,--ReserveFactorMasterOrder 储备系数
                           psp.salesprice,-- PartsSalesPrice  服务站价
                           nvl(rfd.UpperLimitCoefficient, 0) as UpperLimitCoefficient,--ReserveFactorOrderDetail 上限系数
                           nvl(rfd.lowerlimitcoefficient, 0) as lowerlimitcoefficient,--ReserveFactorOrderDetail 下限系数
                           (nvl(sc.DeliveryCycle, 0) +
                           nvl(sc.LogisticsCycle, 0)) as ArriveCycle,-- SparePartpurchCycle  供应商发货周期 + 供应商物流周期
                           nvl(sp.WarehousDays, 0) as WarehousDays,-- SparePart 库房天数
                           nvl(sp.TemDays, 0) as TemDays,--SparePart  临时天数
                           nvl(sih.availablestock, 0) as AvailableStock,--SIHDailySalesAverage  可用库存
                           nvl(pway.OnWayNumber, 0) as OnWayNumber,--复杂计算字段
                           nvl(sales.owOrder, 0) owOrder,--复杂计算字段
                            sih.SixAmount,--sixamount 前第6个4周订货数量
                   sih.FiveAmount,--fiveamount 前第5个4周订货数量
                   sih.FourAmount,--fouramount 前第4个4周订货数量
                   sih.ThreeAmount,--threeamount 前第3个4周订货数量
                   sih.TwoAmount,--twoamount 前第2个4周订货数量
                   sih.OneAmount,--oneamount 前第1个4周订货数量
                           nvl(rf.lowerlimitdays, 0)  as LowerLimitDays -- ReserveFactorMasterOrder 库存下限天数
                      from SIHDailySalesAverage sih --配件公司日均销量明细表
                      left join PartsSupplierRelation psr  --配件与供应商关系表
                        on sih.sparepartid = psr.partid
                       and psr.status = 1 --配件与供应商关系表 状态 = 有效
                       and psr.isprimary = 1--配件与供应商关系表 是否首选供应商 = 是
                      left join SparePartpurchCycle sc-- 配件采购周期维护表
                        on psr.supplierid = sc.id
                       and psr.partid = sc.partid
                       and sih.warehouseid = sc.receivingwarehouseid
                       and sc.status = 1
                      left join PartsSupplier ps --配件供应商基本信息表
                        on psr.supplierid = ps.id
                      join partsbranch pb --配件营销信息表
                        on sih.sparepartid = pb.partid
                       and pb.status = 1
                      join SparePart sp --配件信息表
                        on sih.sparepartid = sp.id
                      left join ReserveFactorMasterOrder rf --储备系数主单表
                        on sih.warehouseid = rf.warehouseid
                       and pb.partabc = rf.abcstrategyid
                       and rf.status = 1
                      left join PartsSalesPrice psp --配件销售价表
                        on sih.sparepartid = psp.sparepartid
                       and psp.status = 1
                      left join ReserveFactorOrderDetail rfd --储备系数清单表
                        on rf.id = rfd.reservefactormasterorderid
                       and psp.salesprice >= rfd.PriceFloor --配件销售价表.服务站价 >= 储备系数清单表 价格下限(服务站价)
                       and psp.salesprice <= rfd.PriceCap --配件销售价表.服务站价 <= 储备系数清单表 价格上限

                      left join (select max(tss.OrderedQuantityOrder) as maxorderedquantity, --最大订购数
                                       tss.sparepartid,
                                       tss.warehouseid
                                  from TemSmartSales tss--
                                 where tss.createtime = trunc(sysdate)
                                 group by tss.sparepartid, tss.warehouseid) maxo
                        on maxo.sparepartid = sih.sparepartid
                       and maxo.warehouseid = sih.warehouseid

                      left join (select a.sparepartid,
                                       a.sparepartcode,
                                       a.WarehouseId,
                                       sum((case --配件采购订单表 partsSupplierCode  上汽红岩汽车有限公司 上汽红岩车桥(重庆)有限公司
                                             when a.partsSupplierCode in ('1000002367', '1000003829') then
                                              (case
                                                when a.PartsPurchaseOrderStatus in (1, 2, 3, 4) then a.PlanAmount  --新增 提交 部分确认 确认完毕
                                                when a.PartsPurchaseOrderStatus = 99 then 0 --作废
                                                when a.PartsPurchaseOrderStatus = 7 then --终止
                                                 (case
                                                   when days > 60 then 0
                                                   else (nvl(a.PlanAmount, 0) - nvl(a.InspectedQuantity, 0) - nvl(a.CompletionQuantity, 0) - finishiQty)
                                                 end)
                                                else
                                                 (a.PlanAmount - nvl(a.InspectedQuantity, 0) - nvl(a.CompletionQuantity, 0) - finishiQty)
                                              end)
                                             else
                                              (case
                                                when a.PartsPurchaseOrderStatus = 99 then 0 --作废
                                                when a.PartsPurchaseOrderStatus = 1 then a.PlanAmount --新增  计划量
                                                when a.PartsPurchaseOrderStatus = 2 then --提交
                                                 (case
                                                   when nvl(a.ConfirmedAmount, 0) = 0 and a.Shortsupreason is not null then 0--确认量 = 0 短供原因不为空
                                                   else (a.PlanAmount - nvl(a.InspectedQuantity, 0) - nvl(a.CompletionQuantity, 0) - finishiQty) -- 计划量 - 检验量 - 入库强制完成数量
                                                 end)
                                                when a.PartsPurchaseOrderStatus = 7 then --终止
                                                 (case
                                                   when days > 60 then 0
                                                   else (nvl(a.ConfirmedAmount, 0) - nvl(a.InspectedQuantity, 0) - nvl(a.CompletionQuantity, 0) - finishiQty)
                                                 end)
                                                else
                                                 (case
                                                   when nvl(a.ConfirmedAmount, 0) = 0 and a.Shortsupreason is not null then 0
                                                   else (a.ConfirmedAmount - nvl(a.InspectedQuantity, 0) - nvl(a.CompletionQuantity, 0) -finishiQty)
                                                 end)
                                              end)
                                           end)) as OnWayNumber --在途数量
                                  from (select detail.sparepartid,--partspurchaseplandetail 配件Id
                                               detail.PlanAmount, --partspurchaseplandetail 计划量
                                               detail.sparepartcode, --partspurchaseplandetail 配件编号
                                               puDetail.Confirmedamount,--PartsPurchaseOrderDetail 确认量
                                               puDetail.Shortsupreason,--PartsPurchaseOrderDetail 短供原因
                                               puDetail.Shippingamount,--PartsPurchaseOrderDetail 发运量
                                               orders.WarehouseId,--PartsPurchaseOrder 仓库Id
                                               (select sum(InspectedQuantity) -- PartsInboundCheckBillDetail 检验量
                                                  from PartsInboundCheckBillDetail a --配件入库检验单清单表
                                                 where exists
                                                 (select 1
                                                          from PartsInboundCheckBill tmp --配件入库检验单
                                                         where a.partsinboundcheckbillid = tmp.id
                                                           and tmp.OriginalRequirementBillType = 4
                                                           and tmp.originalrequirementbillid = orders.id
                                                           and tmp.Status <> 4)
                                                and a.sparepartid = detail.sparepartid) as InspectedQuantity, --入库数量   配件入库检验单清单.检验量
                                               (select sum(nvl(a.PlannedAmount,0)) - sum(nvl(a.InspectedQuantity,0))
                                                  from PartsInboundPlanDetail a --配件入库计划清单表
                                                  join PartsInboundPlan b --配件入库计划表
                                                    on a.PartsInboundPlanId = b.id
                                                 where b.originalrequirementbillid = orders.id
                                                   and b.originalrequirementbilltype = 4 --原始需求单据类型 为 配件采购订单
                                                   and a.sparepartid = puDetail.Sparepartid
                                                   and b.status = 3) as CompletionQuantity, --入库强制完成数量  入库计划清单。计划量-检验量  计划单状态为终止
                                               (case
                                                 when orders.status = 7 then puDetail.Confirmedamount - nvl(puDetail.Shippingamount, 0)
                                                 else 0
                                                end) as finishiQty,--终止状态 完成量 =  配件采购清单表 确认量 - 发运量；否则 0
                                               (trunc(sysdate) -
                                               nvl(orders.submittime,
                                                    orders.createtime)) as days,
                                               orders.status as PartsPurchaseOrderStatus,
                                               orders.partssuppliercode as partsSupplierCode
                                          from PartsPurchaseOrder orders --配件采购订单表
                                          join warehouse wh --仓库表
                                            on orders.warehouseid = wh.id
                                           and wh.IsPurchase = 1
                                          left join PartsPurchaseOrderDetail puDetail --配件采购清单表
                                            on orders.id = puDetail.Partspurchaseorderid
                                          join partspurchaseplan plans --配件采购计划单表
                                            on plans.id = orders.originalrequirementbillid
                                           and plans.code = orders.originalrequirementbillcode
                                          join partspurchaseplandetail detail --配件采购计划单清单表
                                            on plans.id = detail.purchaseplanid
                                           and detail.sparepartid = puDetail.Sparepartid
                                         where (trunc(sysdate) -
                                               trunc(nvl(orders.submittime,
                                                          orders.createtime))) <= 60) a
                                 group by a.sparepartid,
                                          a.sparepartcode,
                                          a.WarehouseId) Pway

                        on sih.sparepartid = pway.sparepartid
                       and sih.warehouseid = pway.warehouseid
                      left join (select pd.sparepartid,
                                       pd.sparepartcode,
                                       pso.warehouseid,
                                       sum(pd.orderedquantity -
                                           nvl(pd.approvequantity, 0)) as owOrder
                                  from partssalesorder pso --配件销售订单表
                                  join warehouse wh --仓库表
                                    on pso.warehouseid = wh.id
                                   and wh.issales = 1
                                  join partssalesorderdetail pd
                                    on pso.id = pd.partssalesorderid
                                 where ((not exists (select 1
                                                       from PartsSalesOrderProcess pso --配件销售订单处理表
                                                       join PartsSalesOrderProcessDetail psd --配件销售订单处理清单表
                                                         on pso.id = psd.partssalesorderprocessid
                                                      where pso.originalsalesorderid = pso.id
                                                        and psd.sparepartid = pd.sparepartid
                                                        and psd.orderprocessmethod in
                                                            (select OrderProcessMethod
                                                               from SmartProcessMethod --智能订货处理方式配置表
                                                              where status = 1)) and
                                        pso.status = 4) or pso.status = 2)
                                   and wh.storagecompanyid = 12730
                                 group by pd.sparepartid,
                                          pd.sparepartcode,
                                          pso.warehouseid) sales
                        on sih.sparepartid = sales.sparepartid
                       and sih.warehouseid = sales.warehouseid
                     where sih.carrytime = trunc(sysdate)) yy) tt;
  commit;

end SIHRecommendPlanPro;
/
