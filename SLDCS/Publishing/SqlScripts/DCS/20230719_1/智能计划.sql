create sequence S_CalculatIntelligentPlan;
create table CalculatIntelligentPlan  (
   Id                 NUMBER(9)                       not null,
   IsCalculate        NUMBER(1),
   CreatorId          NUMBER(9),
   CreatorName        VARCHAR2(100),
   CreateTime         DATE,
   ModifierId         NUMBER(9),
   ModifierName       VARCHAR2(100),
   ModifyTime         DATE,
   constraint PK_CALCULATINTELLIGENTPLAN primary key (Id)
);

insert into CalculatIntelligentPlan values(S_CalculatIntelligentPlan.Nextval,0,0,'系统自动生成',sysdate,null,null,null);

alter table SIHDailySalesAverage  add( sixamount             NUMBER(19,4),
  fiveamount            NUMBER(19,4),
  fouramount            NUMBER(19,4),
  threeamount           NUMBER(19,4),
  twoamount             NUMBER(19,4),
  oneamount             NUMBER(19,4))
