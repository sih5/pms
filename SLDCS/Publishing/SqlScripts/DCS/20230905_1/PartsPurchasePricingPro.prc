create or replace procedure PartsPurchasePricingPro is
  cursor PartsPurchasePricings is
    select s.id, s.partssupplierid, s.partid
      from PartsPurchasePricing s
     where trunc(s.validfrom) = trunc(sysdate + 1)
       and s.isprimary = 1;
begin

  for c_PartsPurchasePricings in PartsPurchasePricings loop
    update PartsSupplierRelation
       set isprimary = 1, ModifyTime = sysdate, ModifierName = '自动更新'
     where PartId = c_PartsPurchasePricings.Partid
       and SupplierId = c_PartsPurchasePricings.Partssupplierid;
    update PartsSupplierRelation
       set isprimary = 0, ModifyTime = sysdate, ModifierName = '自动更新'
     where PartId = c_PartsPurchasePricings.Partid
       and SupplierId <> c_PartsPurchasePricings.Partssupplierid
       and isprimary = 1;
  end loop;
end PartsPurchasePricingPro;
/
