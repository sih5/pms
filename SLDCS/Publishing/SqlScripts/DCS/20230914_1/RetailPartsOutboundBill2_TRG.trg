create or replace trigger RetailPartsOutboundBill2_TRG
  after update on DealerPartsRetailOrder
  for each row
begin
  case
    when (updating and :new.status = 2) then
    
      insert into RetailPartsOutboundBill_Sync
        (id, objid, iostatus, createtime)
      values
        (S_RetailPartsOutboundBill_Sync.Nextval, :old.code, 0, sysdate);
    else
      dbms_output.put_line('nothing');
  end case;
end RetailPartsOutboundBill2_TRG;
/
