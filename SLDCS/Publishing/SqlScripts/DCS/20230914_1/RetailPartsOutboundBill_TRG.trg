create or replace trigger RetailPartsOutboundBill_TRG
  after insert on PartsOutboundBill
  for each row
begin
  case
    when (inserting and :new.OutboundType = 6 and
         :new.OriginalRequirementBillType = 9) then
    
      insert into RetailPartsOutboundBill_Sync
        (id, objid, iostatus, createtime)
      values
        (S_RetailPartsOutboundBill_Sync.Nextval,
         :new.originalrequirementbillcode,
         0,
         sysdate);
    else
      dbms_output.put_line('nothing');
  end case;
end RetailPartsOutboundBill_TRG;
/
