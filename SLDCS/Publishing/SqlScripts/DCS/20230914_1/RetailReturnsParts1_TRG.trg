create or replace trigger RetailReturnsParts1_TRG
   after insert on PartsInboundCheckBill
  for each row
begin
  case
    when (inserting and :new.inboundtype = 5 and
         :new.OriginalRequirementBillType = 10) then
    
      insert into RetailPartsOutboundBill_Sync
        (id, objid, iostatus, createtime)
      values
        (S_RetailReturnsParts_Sync.Nextval,
         :new.originalrequirementbillcode,
         0,
         sysdate);
    else
      dbms_output.put_line('nothing');
  end case;
end RetailReturnsParts1_TRG;
/
