create or replace trigger RetailReturnsParts2_TRG
  after update on DealerPartsSalesReturnBill
  for each row
begin
  case
    when (updating and :new.status = 2) then
    
      insert into RetailReturnsParts_Sync
        (id, objid, iostatus, createtime)
      values
        (S_RetailReturnsParts_Sync.Nextval, :old.code, 0, sysdate);
    else
      dbms_output.put_line('nothing');
  end case;
end RetailReturnsParts2_TRG;
/
