create table RetailPartsOutboundBill_Sync
(
  id             NUMBER(9) not null,
  objid          VARCHAR2(40),
  iostatus       INTEGER,
  createtime     DATE,
  firstsendtime  DATE,
  modifysendtime DATE,
  token          VARCHAR2(50),
  sender         VARCHAR2(50),
  message        VARCHAR2(50)
)
;
create sequence S_RetailPartsOutboundBill_Sync;
--出库的数据
insert into RetailPartsOutboundBill_Sync
  (id, objid, iostatus, createtime)
  select S_RetailPartsOutboundBill_Sync.Nextval,
         pobp.originalrequirementbillcode,
         0,
         sysdate
    from PartsOutboundBill pobp
   where pobp.OutboundType = 6
     and pobp.OriginalRequirementBillType = 9
     and to_char(pobp.createtime, 'yyyy') >= '2022';
     
     
 insert into RetailPartsOutboundBill_Sync
  (id, objid, iostatus, createtime)
  select S_RetailPartsOutboundBill_Sync.Nextval, dprop.code, 0, sysdate
    from DealerPartsRetailOrder dprop
   where dprop.status = 2
     and to_char(dprop.createtime, 'yyyy') >= '2022';

--零售退货
create table RetailReturnsParts_Sync
(
  id             NUMBER(9) not null,
  objid          VARCHAR2(40),
  iostatus       INTEGER,
  createtime     DATE,
  firstsendtime  DATE,
  modifysendtime DATE,
  token          VARCHAR2(50),
  sender         VARCHAR2(50),
  message        VARCHAR2(50)
)
;
create sequence S_RetailReturnsParts_Sync;

insert into RetailReturnsParts_Sync
  (id, objid, iostatus, createtime)
  select S_RetailReturnsParts_Sync.Nextval,
         pobp.OriginalRequirementBillCode,
         0,
         sysdate
    from PartsInboundCheckBill pobp
    where pobp.InboundType = 5
     and pobp.OriginalRequirementBillType = 10
     and to_char(pobp.createtime, 'yyyy') >= '2022';
     
     
 insert into RetailReturnsParts_Sync
  (id, objid, iostatus, createtime)
  select S_RetailReturnsParts_Sync.Nextval, dprop.code, 0, sysdate
    from DealerPartsSalesReturnBill dprop
   where dprop.status = 2
     and to_char(dprop.createtime, 'yyyy') >= '2022';
     
    
 
