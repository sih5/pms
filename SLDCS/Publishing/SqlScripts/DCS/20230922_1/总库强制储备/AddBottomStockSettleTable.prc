create or replace procedure AddBottomStockSettleTable is
begin
  insert into BottomStockSettleTable
  select S_BottomStockSettleTable.Nextval, sysdate, pp.* from (select
     yy.MarketingDepartmentcode,
      yy.MarketingDepartmentName,     
       yy.centerCode,
       yy.centerName,
       yy.dealerCode,
       yy.dealerName,      
   sum(yy.Varieties) as Varieties, --应储备品种数
   sum(yy.StockQty * yy.price) as StockQtyPrice, --应储备金额
       sum(yy.actual) as actual, --实际保底品种数      
       sum( (case when nvl(yy.ActualAvailableStock,0)>yy.StockQty then yy.StockQty else nvl(yy.ActualAvailableStock,0) end) * yy.price) as actualPrice, --实际保底金额
       sum(yy.Varieties) - sum(yy.actual) as differentvarieties, --差异品种数             
       (sum(yy.Varieties * yy.price ) - sum(yy.actual * yy.price )) as DIFFERSPECIESNUMFEE,
        yy.CompanyCode,
       yy.CompanyName
  from (select 1 as Varieties,
               bs.companyid,
               bs.companytype,
               bs.ReserveQty as StockQty,
               (case
                 when bs.companytype = 3 then
                  psp.centerprice
                  when bs.companytype=1 then ep.costprice
                 else
                  psp.SalesPrice
               end) as price,
              ( case when bs.companytype=2 then (case when dps.Quantity>0 then 1 else 0 end ) else 
               (case
                 when tt.ActualAvailableStock > 0 then
                  1
                 else
                  0
               end) end ) as actual,
               
               (case when bs.companytype=2 then dps.Quantity else tt.ActualAvailableStock end ) as ActualAvailableStock, --实际
               (case
                 when bs.companytype = 3 then
                  (select mt.Name
                     from MarketingDepartment mt
                     join Agency ag
                       on mt.id = ag.marketingdepartmentid
                    where ag.id = bs.companyid)
                else 
                  (select mt.Name
                     from MarketingDepartment mt
                     join DealerServiceInfo ag
                       on mt.id = ag.marketingdepartmentid
                    where ag.DealerId = bs.companyid)               
               end) as MarketingDepartmentName,
                (case
                 when bs.companytype = 3 then
                  (select mt.code
                     from MarketingDepartment mt
                     join Agency ag
                       on mt.id = ag.marketingdepartmentid
                    where ag.id = bs.companyid)
                else 
                  (select mt.code
                     from MarketingDepartment mt
                     join DealerServiceInfo ag
                       on mt.id = ag.marketingdepartmentid
                    where ag.DealerId = bs.companyid)               
               end) as MarketingDepartmentcode,
               (case when bs.companytype =2 then (select AgencyName from AgencyDealerRelation ar where ar.dealerid=bs.companyid and  ar.status=1 and rownum=1) when bs.companytype =3 then  bs.companyname  else  cast(''as varchar2(20)) end )  as centerName,
               (case when bs.companytype =2 then (select AgencyCode from AgencyDealerRelation ar where ar.dealerid=bs.companyid and  ar.status=1 and rownum=1) when bs.companytype =3  then bs.companycode  else cast(''as varchar2(20)) end )  as centerCode,
                (case when bs.companytype =2 then bs.companyname else cast(''as varchar2(20)) end )  as dealerName,
               (case when bs.companytype =2 then bs.companycode  else cast('' as varchar2(20)) end )  as dealerCode,
               (case when bs.companytype =1 then bs.companyname else cast(''as varchar2(20)) end )  as CompanyName,
               (case when bs.companytype =1 then bs.companycode  else cast('' as varchar2(20)) end )  as CompanyCode
          from BottomStockColVersion bs
          left join PartsSalesPrice psp
            on bs.sparepartid = psp.sparepartid
           and psp.status = 1
          left join (select aa.partid,
                           aa.StorageCompanyId,
                           (aa.actualqty - nvl(bb.orderlockqty, 0)) as ActualAvailableStock
                      from (select sum(quantity) as actualqty,
                                   tmp.partid,
                                   tmp.StorageCompanyId
                              from partsstock tmp
                             inner join warehouseareacategory b
                                on tmp.warehouseareacategoryid = b.id
                             where b.category = 1
                             group by tmp.partid, tmp.StorageCompanyId) aa
                      left join (select sum(lockedquantity) as orderlockqty,
                                       tmp.StorageCompanyId,
                                       tmp.partid
                                  from partslockedstock tmp
                                 group by tmp.StorageCompanyId, tmp.partid) bb
                        on aa.partid = bb.partid
                       and aa.StorageCompanyId = bb.StorageCompanyId) tt
            on bs.sparepartid = tt.partid
           and bs.companyid = tt.StorageCompanyId
          left join DealerPartsStock dps
            on bs.sparepartid = dps.sparepartid
           and bs.companyid = dps.dealerid
           and bs.companytype = 2
           left join EnterprisePartsCost ep on bs.sparepartid =ep.sparepartid and ep.ownercompanyid =bs.companyid 
         where  bs.status = 1
           and( (bs.companytype=2 and bs.createtime> to_date('2019-06-01','yyyy-mm-dd')) or bs.companytype in (3,1))
          ) yy
 group by  yy.MarketingDepartmentName,yy.MarketingDepartmentcode,
       yy.centerName,
       yy.centerCode,
       yy.dealerName,
       yy.dealerCode,
       yy.CompanyCode,
       yy.CompanyName)pp;
       commit;
         
end AddBottomStockSettleTable;
/
