create or replace procedure AddCenterABCBasis is
begin
  --删除超过半年的数据
  delete from CenterABCBasis where CreateTime < sysdate - 180;
  commit;
  --插入 J类
  insert into CenterABCBasis value
    (select S_CenterABCBasis.Nextval,
            SalesUnitOwnerCompanyId,
            SalesUnitOwnerCompanyCode,
            SalesUnitOwnerCompanyName,
            id,
            code,
            name,
            ReferenceCode,
            PartABC,
            RetailGuidePrice,
            null,
            null,
            null,
            null,
            null,
            5,
            sysdate,
            to_char(sysdate, 'iw')
       from (select distinct ps.SalesUnitOwnerCompanyCode,
                             ps.SalesUnitOwnerCompanyId,
                             sp.id,
                             sp.code,
                             sp.name,
                             sp.ReferenceCode,
                             ps.SalesUnitOwnerCompanyName,
                             psp.RetailGuidePrice,
                             pb.PartABC
               from partssalesorder ps
               join partssalesorderdetail pd
                 on ps.id = pd.partssalesorderid
               join sparepart sp
                 on pd.sparepartid = sp.id
               join partsbranch pb
                 on sp.id = pb.partid
               join PartsRetailGuidePrice psp
                 on sp.id = psp.sparepartid
                and psp.status = 1
               join CentralABCClassification cf
                 on ps.SalesUnitOwnerCompanyId = cf.centerid
                and cf.sparepartid = pd.sparepartid
                and cf.newtype = 5) tt);
  --红岩号包含//RZ的产品直接纳入C类
  insert into CenterABCBasis value
    (select S_CenterABCBasis.Nextval,
            SalesUnitOwnerCompanyId,
            SalesUnitOwnerCompanyCode,
            SalesUnitOwnerCompanyName,
            id,
            code,
            name,
            ReferenceCode,
            PartABC,
            RetailGuidePrice,
            null,
            null,
            null,
            null,
            null,
            3,
            sysdate,
            to_char(sysdate, 'iw')
       from (select distinct ps.SalesUnitOwnerCompanyCode,
                             ps.SalesUnitOwnerCompanyId,
                             sp.id,
                             sp.code,
                             sp.name,
                             sp.ReferenceCode,
                             ps.SalesUnitOwnerCompanyName,
                             psp.RetailGuidePrice,
                             pb.PartABC
               from partssalesorder ps
               join partssalesorderdetail pd
                 on ps.id = pd.partssalesorderid
               join sparepart sp
                 on pd.sparepartid = sp.id
               join partsbranch pb
                 on sp.id = pb.partid
               join PartsRetailGuidePrice psp
                 on sp.id = psp.sparepartid
                and psp.status = 1
               left join CentralABCClassification cf
                 on sp.id = cf.sparepartid
                and cf.CenterId = ps.SalesUnitOwnerCompanyId
              where cf.id is null
                and sp.ReferenceCode like '%//RZ%') tt);
  commit;
  --插入其他类
  insert into CenterABCBasis value
    (select S_CenterABCBasis.Nextval,
            bb.CenterId,
            bb.CenterCode,
            bb.CenterName,
            bb.id,
            bb.code,
            bb.name,
            bb.ReferenceCode,
            bb.PartABC,
            bb.RetailGuidePrice,
            bb.SaleSubNumber,
            bb.SaleSubAmount,
            bb.SubAmountPercentage,
            bb.SaleSubFrequency,
            bb.SubFrequencyPercentage,
            bb.NewType,
            sysdate,
            to_char(sysdate, 'iw')
       from (select sp.code,
                    sp.id,
                    sp.name,
                    sp.ReferenceCode,
                    pb.PartABC,
                    cf.CenterCode,
                    cf.CenterName,
                    cf.CenterId,
                    psp.RetailGuidePrice,
                    cf.SaleSubNumber,
                    cf.SaleSubAmount,
                    cf.SubAmountPercentage,
                    cf.SaleSubFrequency,
                    cf.SubFrequencyPercentage,
                    cf.NewType
               from sparepart sp
               join PartsBranch pb
                 on sp.id = pb.partid
                and pb.status = 1
               join PartsRetailGuidePrice psp
                 on sp.id = psp.sparepartid
                and psp.status = 1
               join CentralABCClassification cf
                 on sp.id = cf.sparepartid
              where not exists (select 1
                       from CenterABCBasis abc
                      where abc.CenterId = cf.CenterId
                        and abc.SparePartId = sp.id
                        and abc.NewType = 5
                        and abc.weeknum = to_char(sysdate, 'iw'))
                and cf.NewType is not null) bb);
  commit;
end AddCenterABCBasis;
/
