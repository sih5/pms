create or replace procedure AddCentralABCClassification is
begin
  --删除所有
  delete from CentralABCClassification;
  commit;
  --插入 J类
  insert into CentralABCClassification value
    (select S_CentralABCClassification.Nextval,
            SalesUnitOwnerCompanyId,
            SalesUnitOwnerCompanyCode,
            SalesUnitOwnerCompanyName,
            id,
            code,
            name,
            null,
            null,
            null,
            null,
            null,
            5,
            trunc(sysdate-2)
       from (select sp.id,
                    sp.code,
                    sp.name,
                    cp.name SalesUnitOwnerCompanyName,
                    cp.code SalesUnitOwnerCompanyCode,
                    psk.StorageCompanyId as SalesUnitOwnerCompanyid,
                    yy.sparepartid,
                    sum(psk.Quantity)
               from partsstock psk
               join sparepart sp
                 on psk.PartId = sp.id
               join partsbranch pb
                 on sp.id = pb.partid
               join company cp
                 on psk.StorageCompanyId = cp.id
              inner join warehouseareacategory c
                 on c.id = psk.warehouseareacategoryid
               left join PartsReplacement pr
                 on pr.newpartid = psk.partid
               left join (select distinct pd.sparepartid,
                                         ps.SalesUnitOwnerCompanyid
                           from partssalesorder ps
                           join partssalesorderdetail pd
                             on ps.id = pd.partssalesorderid
                           join partssalesorderprocess pps
                             on ps.id = pps.originalsalesorderid
                            and pps.time = 1
                          where ps.status in (4, 5, 6)
                            and pd.orderedquantity > 0
                            and ps.CustomerType = 2
                            and trunc(pps.createTime) < trunc(sysdate)-0
                            and trunc(pps.createTime) >=
                                trunc(sysdate) - 0 -
                                nvl((select NoSaleMonth
                                      from ABCSetting
                                     where Type = 5),
                                    0) * 7) yy
                 on yy.sparepartid = psk.partid
                and psk.StorageCompanyId = yy.SalesUnitOwnerCompanyid
               left join (select distinct pr.newpartid as sparepartid,
                                         ps.SalesUnitOwnerCompanyid
                           from partssalesorder ps
                           join partssalesorderdetail pd
                             on ps.id = pd.partssalesorderid
                           join partssalesorderprocess pps
                             on ps.id = pps.originalsalesorderid
                            and pps.time = 1
                           join PartsReplacement pr
                             on pr.oldpartid = pd.sparepartid
                          where ps.status in (4, 5, 6)
                            and pd.orderedquantity > 0
                            and ps.CustomerType = 2
                            and trunc(pps.createTime) < trunc(sysdate) - 0
                            and trunc(pps.createTime) >=
                                trunc(sysdate) - 0 -
                                nvl((select NoSaleMonth
                                      from ABCSetting
                                     where Type = 5),
                                    0) * 7) tt
                 on tt.sparepartid = psk.partid
                and psk.StorageCompanyId = yy.SalesUnitOwnerCompanyid
               left join (select distinct pod.sparepartid,
                                         pro.salesunitownercompanyid
                           from PartsRetailOrder pro
                           join partsretailorderdetail pod
                             on pro.id = pod.partsretailorderid
                          where trunc(pro.approvetime) >=
                                trunc(sysdate - 0 - nvl((select NoSaleMonth
                                                          from ABCSetting
                                                         where Type = 5),
                                                        0) * 7)
                            and trunc(pro.approvetime) < trunc(sysdate) - 0) pp
                 on pp.sparepartid = psk.partid
                and psk.StorageCompanyId = pp.SalesUnitOwnerCompanyid
               left join (select distinct pr.newpartid as sparepartid,
                                         pro.salesunitownercompanyid
                           from PartsRetailOrder pro
                           join partsretailorderdetail pod
                             on pro.id = pod.partsretailorderid
                           join PartsReplacement pr
                             on pr.oldpartid = pod.sparepartid
                          where trunc(pro.approvetime) >=
                                trunc(sysdate -0- nvl((select NoSaleMonth
                                                      from ABCSetting
                                                     where Type = 5),
                                                    0) * 7)
                            and trunc(pro.approvetime) < trunc(sysdate) - 0) dd
                 on dd.sparepartid = psk.partid
                and psk.StorageCompanyId = dd.SalesUnitOwnerCompanyid
              where ((yy.sparepartid is null and tt.sparepartid is null and
                    pp.sparepartid is null and dd.sparepartid is null) or
                    pb.PartABC in (6, 7,10,11) or nvl(pb.IsOrderable, 0) = 0)
                and c.category <> 2
                and psk.StorageCompanyType = 3
                and sp.ReferenceCode not like '%//RZ%'
              group by sp.id,
                       sp.code,
                       sp.name,
                       cp.name,
                       cp.code,
                       psk.StorageCompanyId,
                       yy.sparepartid) tt);
  commit;
  insert into CentralABCClassification value
    (select S_CentralABCClassification.Nextval,
            SalesUnitOwnerCompanyId,
            SalesUnitOwnerCompanyCode,
            SalesUnitOwnerCompanyName,
            id,
            code,
            name,
            null,
            null,
            null,
            null,
            null,
            5,
            trunc(sysdate-2)
       from (select distinct pd.SparePartId               as id,
                    pd.SparePartcode             as code,
                    pd.SparePartName             as name,
                    ps.SalesUnitOwnerCompanyName,
                    ps.SalesUnitOwnerCompanyCode,
                    ps.SalesUnitOwnerCompanyid
               from partssalesorder ps
               join partssalesorderdetail pd
                 on ps.id = pd.partssalesorderid
                 join sparepart sp on pd.SparePartId=sp.id
               join partsbranch pb
                 on pd.SparePartId = pb.partid
               join company cp
                 on ps.SalesUnitOwnerCompanyid = cp.id
               join partssalesorderprocess pps
                 on ps.id = pps.originalsalesorderid
                and pps.time = 1
              where cp.type = 3
                and ps.status in (4, 5, 6)
                 and sp.ReferenceCode not like '%//RZ%'
                and (pb.partabc in (6, 7,10,11) or nvl(pb.IsOrderable, 0) = 0)
                and not exists
              (select 1
                       from CentralABCClassification cs
                      where cs.centerid = ps.salesunitownercompanyid
                        and cs.sparepartid = pd.sparepartid)
                and not exists
              (select 1
                       from PartsReplacement pr
                       join partssalesorderdetail pd
                         on pr.oldpartid = pd.sparepartid
                       join partssalesorder ps
                         on ps.id = pd.partssalesorderid
                       join company cp
                         on ps.SalesUnitOwnerCompanyid = cp.id
                       join partssalesorderprocess pps
                         on ps.id = pps.originalsalesorderid
                        and pps.time = 1
                      where cp.type = 3
                        and ps.status in (4, 5, 6)
                        and trunc(pps.createTime) < trunc(sysdate) - 0
                        and pr.newpartid = pd.sparepartid
                        and trunc(pps.createTime) <
                            trunc(sysdate) - 0 -
                            nvl((select NoSaleMonth
                                  from ABCSetting
                                 where Type = 5),
                                0) * 7)
                and trunc(pps.createTime) < trunc(sysdate) - 0
                and trunc(pps.createTime) >=
                    trunc(sysdate) - 0 -
                    nvl((select NoSaleMonth from ABCSetting where Type = 5),
                        0) * 7) tt);
  commit;
  --插入其他类
  insert into CentralABCClassification value
    (select S_CentralABCClassification.Nextval,  bb.SalesUnitOwnerCompanyId,
            bb.SalesUnitOwnerCompanyCode,
            bb.SalesUnitOwnerCompanyName,
            bb.sparepartid,
            null,
            null,
            bb.OrderedQuantity,
            money,
            pre,
            times,
            timespre,
            (case
              when (bb.Mpre > bb.ASaleFeeFrom and bb.Mpre <= bb.ASaleFeeTo) or
                   (bb.Ttimespre > bb.ASaleFrequencyFrom and
                   bb.Ttimespre <= bb.ASaleFrequencyTo) then
               1
              when (bb.Mpre > bb.BSaleFeeFrom and bb.Mpre <= bb.BSaleFeeTo) or
                   (bb.Ttimespre > bb.BSaleFrequencyFrom and
                   bb.Ttimespre <= bb.BSaleFrequencyTo) then
               2
              when (bb.Mpre > bb.CSaleFeeFrom and bb.Mpre <= bb.CSaleFeeTo) or
                   (bb.Ttimespre > bb.CSaleFrequencyFrom and
                   bb.Ttimespre <= bb.CSaleFrequencyTo) or referencecode like '%//RZ%'  then
               3
              when (bb.Mpre > bb.DSaleFeeFrom and bb.Mpre <= bb.DSaleFeeTo) or
                   (bb.Ttimespre > bb.DSaleFrequencyFrom and
                   bb.Ttimespre <= bb.DSaleFrequencyTo) then
               4
              else
               null
            end) as type,
            trunc(sysdate-2)
       from (select tt.sparepartid,
                    null,
                    pb.PartABC,
                    tt.SalesUnitOwnerCompanyCode,
                    tt.SalesUnitOwnerCompanyName,
                    tt.SalesUnitOwnerCompanyId,
                    OrderedQuantity,
                    tt.money,
                    tt.pre,
                    tt.times,
                    tt.timespre,
                    tt.Mpre,
                    tt.Ttimespre,
                    (select SaleFrequencyFrom
                       from ABCSetting abc
                      where abc.type = 1) as ASaleFrequencyFrom,
                    (select SaleFrequencyTo
                       from ABCSetting abc
                      where abc.type = 1) as ASaleFrequencyTo,
                    (select SaleFeeFrom
                       from ABCSetting abc
                      where abc.type = 1) as ASaleFeeFrom,
                    (select SaleFeeTo from ABCSetting abc where abc.type = 1) as ASaleFeeTo,
                    (select SaleFrequencyFrom
                       from ABCSetting abc
                      where abc.type = 2) as BSaleFrequencyFrom,
                    (select SaleFrequencyTo
                       from ABCSetting abc
                      where abc.type = 2) as BSaleFrequencyTo,
                    (select SaleFeeFrom
                       from ABCSetting abc
                      where abc.type = 2) as BSaleFeeFrom,
                    (select SaleFeeTo from ABCSetting abc where abc.type = 2) as BSaleFeeTo,
                    (select SaleFrequencyFrom
                       from ABCSetting abc
                      where abc.type = 3) as CSaleFrequencyFrom,
                    (select SaleFrequencyTo
                       from ABCSetting abc
                      where abc.type = 3) as CSaleFrequencyTo,
                    (select SaleFeeFrom
                       from ABCSetting abc
                      where abc.type = 3) as CSaleFeeFrom,
                    (select SaleFeeTo from ABCSetting abc where abc.type = 3) as CSaleFeeTo,
                    (select SaleFrequencyFrom
                       from ABCSetting abc
                      where abc.type = 4) as DSaleFrequencyFrom,
                    (select SaleFrequencyTo
                       from ABCSetting abc
                      where abc.type = 4) as DSaleFrequencyTo,
                    (select SaleFeeFrom
                       from ABCSetting abc
                      where abc.type = 4) as DSaleFeeFrom,
                    (select SaleFeeTo from ABCSetting abc where abc.type = 4) as DSaleFeeTo,tt.referencecode
               from (select rr.SalesUnitOwnerCompanyCode,
                            rr.sparepartid,
                            rr.SalesUnitOwnerCompanyName,
                            sum(rr.pre) over(partition by rr.SalesUnitOwnerCompanyId order by rr.pre desc) as Mpre,
                            rr.pre as pre,
                            sum(rr.pre) over(partition by rr.SalesUnitOwnerCompanyId order by rr.timespre desc) as Ttimespre,
                            rr.times as times,
                            rr.timespre as timespre,
                            money,
                            rr.OrderedQuantity,
                            SalesUnitOwnerCompanyId,
                            referencecode
                       from (select pp.SalesUnitOwnerCompanyCode,
                                    pp.sparepartid,
                                    pp.SalesUnitOwnerCompanyName,
                                    (ratio_to_report(money)
                                     over(partition by
                                          pp.SalesUnitOwnerCompanyId)) * 100 as pre,
                                    money,
                                    (ratio_to_report(times)
                                     over(partition by
                                          pp.SalesUnitOwnerCompanyId)) * 100 as timespre,
                                    pp.OrderedQuantity,
                                    pp.SalesUnitOwnerCompanyId,
                                    times,
                                    referencecode
                               from (select sum(dd.money) as money,
                                            sum(times) as times,
                                            partid as sparepartid,
                                            code,
                                            SalesUnitOwnerCompanyId,
                                            sum(OrderedQuantity) as OrderedQuantity,
                                            SalesUnitOwnerCompanyCode,
                                            SalesUnitOwnerCompanyName,
                                            referencecode
                                       from (select (case
         when pb.id is null then
          pp.money
         else
          (case
            when nvl(pb.IsOrderable, 0) = 0 and nvl(pb.IsSalable, 0) = 1 then
             nvl(yy.money, 0)
            when nvl(pb.IsOrderable, 0) = 0 and nvl(pb.IsSalable, 0) = 0 then
             nvl(yy.money, 0) + nvl(pp.money, 0)
            else
             (pp.money)
          end)
       end) as money,
       (case
         when pb.id is null then
          pp.times
         else
          (case
            when nvl(pb.IsOrderable, 0) = 0 and nvl(pb.IsSalable, 0) = 1 then
             (yy.times)
            when nvl(pb.IsOrderable, 0) = 0 and nvl(pb.IsSalable, 0) = 0 then
             nvl(yy.times, 0) + nvl(pp.times, 0)
            else
             pp.times
          end)
       end) as times,
       ps.partid,
       sp.code,
       (case
         when pb.id is null then
          pp.SalesUnitOwnerCompanyId
         else
          (case
            when nvl(pb.IsOrderable, 0) = 0 then
             yy.SalesUnitOwnerCompanyId
            else
             pp.SalesUnitOwnerCompanyId
          end)
       end) as SalesUnitOwnerCompanyId,
       (case
         when pb.id is null then
          pp.OrderedQuantity
         else
          (case
            when nvl(pb.IsOrderable, 0) = 0 and nvl(pb.IsSalable, 0) = 1 then
             nvl(yy.OrderedQuantity, 0)
            when nvl(pb.IsOrderable, 0) = 0 and nvl(pb.IsSalable, 0) = 0 then
             nvl(yy.OrderedQuantity, 0) + nvl(pp.OrderedQuantity, 0)
            else
             pp.OrderedQuantity
          end)
       end) as OrderedQuantity,
       (case
         when pb.id is null then
          pp.SalesUnitOwnerCompanyCode
         else
          (case
            when nvl(pb.IsOrderable, 0) = 0 then
             yy.SalesUnitOwnerCompanyCode
            else
             pp.SalesUnitOwnerCompanyCode
          end)
       end) as SalesUnitOwnerCompanyCode,
       (case
         when pb.id is null then
          pp.SalesUnitOwnerCompanyName
         else
          (case
            when nvl(pb.IsOrderable, 0) = 0 then
             yy.SalesUnitOwnerCompanyName
            else
             pp.SalesUnitOwnerCompanyName
          end)
       end) as SalesUnitOwnerCompanyName,
       sp.referencecode
  from partsbranch ps
  join (select distinct  pp.sparepartid,
                 pp.salesunitownercompanyid
            from (select 
                         pd.sparepartid,
                         ps.salesunitownercompanyid
                    from partssalesorder ps
                    join partssalesorderdetail pd
                      on ps.id = pd.partssalesorderid
                 
                    join partssalesorderprocess psp
                      on ps.id = psp.originalsalesorderid
                     and psp.time = 1
                   where ps.status in (4, 5, 6)
                     and pd.orderedquantity > 0
                     and (ps.CustomerType = 2 or ps.CustomerType = 7)
                     and trunc(psp.createtime) >= trunc(sysdate - 84)
                     and trunc(psp.createtime) < trunc(sysdate - 0)
                     and not exists(select 1 from CentralABCClassification ca where ca.sparepartid =pd.sparepartid and ca.centerid =ps.salesunitownercompanyid and ca.newtype=5)
                  union
                  select 
                         pr.newpartid               as sparepartid,
                         ps.salesunitownercompanyid
                    from partssalesorder ps
                    join partssalesorderdetail pd
                      on ps.id = pd.partssalesorderid
                   
                    join partssalesorderprocess psp
                      on ps.id = psp.originalsalesorderid
                     and psp.time = 1
                    join PartsReplacement pr
                      on pd.sparepartid = pr.oldpartid
                   where ps.status in (4, 5, 6)
                     and pd.orderedquantity > 0
                     and (ps.CustomerType = 2 or ps.CustomerType = 7)
                     and trunc(psp.createtime) >= trunc(sysdate - 84)
                     and trunc(psp.createtime) < trunc(sysdate - 0)
                     and not exists(select 1 from CentralABCClassification ca where ca.sparepartid =pd.sparepartid and ca.centerid =ps.salesunitownercompanyid and ca.newtype=5)
                  union
                  select
                         pd.sparepartid,
                         ps.salesunitownercompanyid
                    from partsretailorder ps
                    join partsretailorderdetail pd
                      on ps.id = pd.PartsRetailOrderId               
                   where pd.Quantity > 0
                    and ps.status<>99
                     and trunc(ps.approvetime) >= trunc(sysdate - 84)
                     and trunc(ps.approvetime) < trunc(sysdate - 0)
                     and not exists(select 1 from CentralABCClassification ca where ca.sparepartid =pd.sparepartid and ca.centerid =ps.salesunitownercompanyid and ca.newtype=5)
                  union
                  select pr.newpartid               as sparepartid,
                         ps.salesunitownercompanyid
                    from partsretailorder ps
                    join partsretailorderdetail pd
                      on ps.id = pd.PartsRetailOrderId                  
                    join PartsReplacement pr
                      on pd.sparepartid = pr.oldpartid
                   where pd.Quantity > 0
                   and ps.status<>99
                     and trunc(ps.approvetime) >= trunc(sysdate - 84)
                     and trunc(ps.approvetime) < trunc(sysdate - 0)
                     and not exists(select 1 from CentralABCClassification ca where ca.sparepartid =pd.sparepartid and ca.centerid =ps.salesunitownercompanyid and ca.newtype=5)) pp
           ) tt
        on tt.sparepartid = ps.partid
  join sparepart sp
    on ps.partid = sp.id
     left join PartsReplacement pr
    on ps.partid = pr.newpartid
   and pr.status = 1
  left join partsbranch pb
    on pr.oldpartid = pb.partid
   and pb.status = 1
  left join (select sum(money) as money,
                    sum(OrderedQuantity) as OrderedQuantity,
                    sum(times) as times,
                    SalesUnitOwnerCompanyId,
                    sparepartid,
                    SalesUnitOwnerCompanyName,
                    SalesUnitOwnerCompanyCode
               from (select (case
                              when pps.createtime is null then
                               0
                              else
                               (nvl(pd.OrderedQuantity, 0) *
                               pg.retailguideprice)
                            end) as money,
                            (case
                              when pps.createtime is null then
                               0
                              else
                               pd.OrderedQuantity
                            end) as OrderedQuantity,
                            (case
                              when pps.createtime is null then
                               0
                              else
                               1
                            end) as times,
                            pps.createtime as timess,
                            pss.StorageCompanyId as SalesUnitOwnerCompanyId,
                            pss.partid sparepartid,
                            cp.name SalesUnitOwnerCompanyName,
                            cp.code SalesUnitOwnerCompanyCode
                       from (select distinct sss.partid, sss.StorageCompanyId
                               from partsstock sss
                              where sss.StorageCompanyType = 3) pss
                       left join partssalesorderdetail pd
                         on pss.partid = pd.sparepartid
                        and pd.orderedquantity > 0
                       left join partssalesorder ps
                         on ps.id = pd.partssalesorderid
                        and ps.status in (4, 5, 6)
                        and ps.salesunitownercompanyid = pss.StorageCompanyId
                        and ps.CustomerType = 2
                       left join company cp
                         on pss.StorageCompanyId = cp.id
                       left join partssalesorderprocess pps
                         on ps.id = pps.originalsalesorderid
                        and pps.time = 1
                        and trunc(pps.createtime) >= trunc(sysdate - 84)
                        and trunc(pps.createtime) < trunc(sysdate - 0)
                       join partsbranch pb
                         on pd.sparepartid = pb.partid
                        and pb.status = 1
                       left join PartsRetailGuidePrice pg
                         on pd.sparepartid = pg.sparepartid
                        and pg.status = 1
                      where cp.type = 3
                        and pb.isorderable = 1
                        and not exists
                      (select 1
                               from CentralABCClassification abc
                              where abc.CenterId = ps.SalesUnitOwnerCompanyId
                                and abc.SparePartId = pd.sparepartid
                                and abc.newtype = 5)
                     union
                     select (case
                              when pps.createtime is null then
                               0
                              else
                               (nvl(pd.OrderedQuantity, 0) *
                               pg.retailguideprice)
                            end) as money,
                            (case
                              when pps.createtime is null then
                               0
                              else
                               pd.OrderedQuantity
                            end) as OrderedQuantity,
                            (case
                              when pps.createtime is null then
                               0
                              else
                               1
                            end) as times,
                            pps.createtime as timess,
                            ps.SalesUnitOwnerCompanyId,
                            pd.sparepartid,
                            cp.name SalesUnitOwnerCompanyName,
                            cp.code SalesUnitOwnerCompanyCode
                       from partssalesorderdetail pd
                       left join partssalesorder ps
                         on ps.id = pd.partssalesorderid
                        and ps.status in (4, 5, 6)
                        and ps.CustomerType = 2
                       left join company cp
                         on ps.salesunitownercompanyid = cp.id
                       left join partssalesorderprocess pps
                         on ps.id = pps.originalsalesorderid
                        and pps.time = 1
                        and trunc(pps.createtime) >= trunc(sysdate - 84)
                        and trunc(pps.createtime) < trunc(sysdate - 0)
                       join partsbranch pb
                         on pd.sparepartid = pb.partid
                        and pb.status = 1
                       left join PartsRetailGuidePrice pg
                         on pd.sparepartid = pg.sparepartid
                        and pg.status = 1
                      where cp.type = 3
                        and pd.orderedquantity > 0
                        and pb.isorderable = 1
                        and not exists
                      (select 1
                               from partsstock pk
                              where pk.partid = pd.sparepartid
                                and pk.storagecompanyid =
                                    ps.salesunitownercompanyid
                                and pk.StorageCompanyType = 3)
                        and not exists
                      (select 1
                               from CentralABCClassification abc
                              where abc.CenterId = ps.SalesUnitOwnerCompanyId
                                and abc.SparePartId = pd.sparepartid
                                and abc.newtype = 5)
                     
                     union
                     select (case
                              when ps.approvetime is null then
                               0
                              else
                               (nvl(pd.Quantity, 0) * pg.retailguideprice)
                            end) as money,
                            (case
                              when ps.approvetime is null then
                               0
                              else
                               pd.Quantity
                            end) as OrderedQuantity,
                            (case
                              when ps.approvetime is null then
                               0
                              else
                               1
                            end) as times,
                            ps.approvetime as timess,
                            pss.StorageCompanyId as SalesUnitOwnerCompanyId,
                            pss.partid sparepartid,
                            cp.name as SalesUnitOwnerCompanyName,
                            cp.code SalesUnitOwnerCompanyCode
                       from (select distinct sss.partid, sss.StorageCompanyId
                               from partsstock sss
                              where sss.StorageCompanyType = 3) pss
                       left join partsretailorderdetail pd
                         on pss.partid = pd.sparepartid
                        and pd.Quantity > 0
                       left join PartsRetailOrder ps
                         on ps.id = pd.partsretailorderid
                        and ps.status in (2, 3)
                        and ps.salesunitownercompanyid = pss.StorageCompanyId
                        and trunc(ps.approvetime) >= trunc(sysdate - 84)
                        and trunc(ps.approvetime) < trunc(sysdate - 0)
                       join partsbranch pb
                         on pd.sparepartid = pb.partid
                        and pb.status = 1
                       left join PartsRetailGuidePrice pg
                         on pd.sparepartid = pg.sparepartid
                        and pg.status = 1
                       left join company cp
                         on pss.storagecompanyid = cp.id
                      where pb.isorderable = 1
                        and not exists
                      (select 1
                               from CentralABCClassification abc
                              where abc.CenterId = ps.SalesUnitOwnerCompanyId
                                and abc.SparePartId = pd.sparepartid
                                and abc.newtype = 5)) ff
              group by ff.SalesUnitOwnerCompanyId,
                       ff.sparepartid,
                       ff.SalesUnitOwnerCompanyCode,
                       ff.SalesUnitOwnerCompanyName) pp
    on ps.partid = pp.sparepartid
    and pp.SalesUnitOwnerCompanyId=tt.SalesUnitOwnerCompanyId
  left join (select sum(money) as money,
                    sum(times) as times,
                    sum(OrderedQuantity) as OrderedQuantity,
                    SalesUnitOwnerCompanyId,
                    sparepartid,
                    SalesUnitOwnerCompanyName,
                    SalesUnitOwnerCompanyCode
               from (select (nvl(pd.OrderedQuantity, 0) * pg.retailguideprice) as money,
                            1 as times,
                            (pd.OrderedQuantity) as OrderedQuantity,
                            ps.SalesUnitOwnerCompanyId,
                            pd.sparepartid,
                            ps.SalesUnitOwnerCompanyName,
                            ps.SalesUnitOwnerCompanyCode
                       from partssalesorder ps
                       join partssalesorderdetail pd
                         on ps.id = pd.partssalesorderid
                       join company cp
                         on ps.SalesUnitOwnerCompanyId = cp.id
                       join partssalesorderprocess pps
                         on ps.id = pps.originalsalesorderid
                        and pps.time = 1
                       left join PartsRetailGuidePrice pg
                         on pd.sparepartid = pg.sparepartid
                        and pg.status = 1
                      where trunc(pps.createtime) >= trunc(sysdate) - 84
                        and trunc(pps.createtime) < trunc(sysdate - 0)
                        and ps.status in (4, 5, 6)
                        and pd.orderedquantity > 0
                        and ps.CustomerType = 2
                        and cp.type = 3
                     union
                     select (nvl(pd.quantity, 0) * pg.retailguideprice) as money,
                            1 as times,
                            (pd.quantity) as OrderedQuantity,
                            ps.SalesUnitOwnerCompanyId,
                            pd.sparepartid,
                            ps.SalesUnitOwnerCompanyName,
                            ps.SalesUnitOwnerCompanyCode
                       from PartsRetailOrder ps
                       join partsretailorderdetail pd
                         on ps.id = pd.partsretailorderid
                       left join PartsRetailGuidePrice pg
                         on pd.sparepartid = pg.sparepartid
                        and pg.status = 1
                      where trunc(ps.approvetime) >= trunc(sysdate) - 84
                        and trunc(ps.approvetime) < trunc(sysdate - 0)
                        and ps.status in (2, 3)
                        and pd.quantity > 0) hh
              group by hh.SalesUnitOwnerCompanyId,
                       hh.sparepartid,
                       hh.SalesUnitOwnerCompanyName,
                       hh.SalesUnitOwnerCompanyCode) yy
    on pr.oldpartid = yy.sparepartid
   and tt.SalesUnitOwnerCompanyId = yy.SalesUnitOwnerCompanyId
 where ps.status = 1 
) dd
                                      where dd.SalesUnitOwnerCompanyId is not null
                                      group by dd.partid,
                                               dd.code,
                                               dd.SalesUnitOwnerCompanyId,
                                               dd.SalesUnitOwnerCompanyCode,
                                               dd.SalesUnitOwnerCompanyName) pp
                              where pp.money > 0) rr) tt
             
               join PartsBranch pb
                 on tt.sparepartid = pb.partid
               join PartsRetailGuidePrice psp
                 on tt.sparepartid = psp.sparepartid
              where pb.status = 1
                and pb.PartABC not in (6, 7,10,11)
                and psp.status = 1) bb);                
  commit;
end AddCentralABCClassification;
/
