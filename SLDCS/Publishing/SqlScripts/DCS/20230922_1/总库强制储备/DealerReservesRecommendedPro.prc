create or replace procedure DealerReservesRecommendedPro is
  cursor dealers is
    select pp.dealerid,
           pp.agencyid,
           (case
             when (case
                    when sumAb <= Amount then
                     sumAb
                    else
                     (case
                       when ceil(sumAb * reserveratio) < Amount then
                        Amount
                       else
                        ceil(sumAb * reserveratio)
                     end)
                  end) > MaxDays then
              MaxDays
             else
              (case
                when sumAb <= Amount then
                 sumAb
                else
                 (case
                   when ceil(sumAb * reserveratio) < Amount then
                    Amount
                   else
                    ceil(sumAb * reserveratio)
                 end)
              end)
           end) as shouldAmount,
           abCount,
           sumAb
      from (select  sum(case
                     when rp.PartType in (1, 2) and ppp.submitcompanyid is not null  then
                      1
                     else
                      0
                   end) as abCount,
                   ag.dealerid,
                   ag.agencyid,
                   mr.Amount,
                   mv.reserveratio,
                   (select sum(1)
                      from regionalpartscategory rt
                     where Extract(year from trunc(sysdate - 92)) = rt.year
                       and rt.Quarter =
                           ceil(Extract(month from trunc(sysdate - 92)) / 3)
                       and rt.centerid = ag.agencyid
                       and rt.PartType in (1, 2)) as sumAb,
                       nvl((select MaxDays from MaintenanceMandatory),0) MaxDays
              from regionalpartscategory rp
              join agencydealerrelation ag
                on rp.centerid = ag.agencyid
               and ag.status = 1
            /*  join StationServiceReport ss
             on ag.dealerid = ss.dealerid
            and ss.Year = rp.year
            and ss.quarter = rp.quarter
            and rp.sparepartid = ss.sparepartid*/
              join DealerFormat sf
                on ag.dealerid = sf.dealerid
               and sf.status = 1
               and sf.isbottom = 1
              join PartsSalesPrice pss
                on rp.sparepartid = pss.sparepartid
               and pss.status = 1
              join MaintenancePart mp
                on mp.materialcost = (case
                     when sf.MonthlyCosts < 50000 then
                      3
                     when sf.MonthlyCosts >= 50000 and
                          sf.MonthlyCosts < 100000 then
                      2
                     else
                      1
                   end)
               and mp.issave = 1
               and mp.price = (case
                     when pss.salesprice <= 2000 then
                      3
                     when pss.salesprice > 2000 and pss.salesprice <= 5000 then
                      2
                     else
                      1
                   end)
              join MaintenanceReserve mr
                on mr.type = 1
              join MaintenanceVariety mv
                on mv.MaterialCost = (case
                     when sf.MonthlyCosts < 50000 then
                      3
                     when sf.MonthlyCosts >= 50000 and
                          sf.MonthlyCosts < 100000 then
                      2
                     else
                      1
                   end)
               and mv.Format = sf.Format
               and mv.Status = 1
                 left join (select distinct ps.submitcompanyid,
                                    ps.salesunitownercompanyid,
                                    psd.sparepartid
                      from partssalesorder ps
                      join partssalesorderdetail psd
                        on ps.id = psd.partssalesorderid
                    
                     where Extract(year from
                                   trunc(nvl(ps.submittime, ps.createtime))) =
                           Extract(year from trunc(sysdate - 92))
                       and ceil(Extract(month from trunc(nvl(ps.submittime,
                                                  ps.createtime))) / 3) =
                           ceil(Extract(month from trunc(sysdate - 92)) / 3)
                       and ((ps.status = 1 or
                            (ps.status <> 1 and ps.Time > 0)))) ppp
            on rp.centerid = ppp.salesunitownercompanyid
           and ppp.submitcompanyid = ag.dealerid
           and rp.sparepartid = ppp.sparepartid
             where Extract(year from trunc(sysdate - 92)) = rp.year
               and rp.Quarter =
                   ceil(Extract(month from trunc(sysdate - 92)) / 3)
              -- and rp.PartType in (1, 2)
              /* and exists
             (select 1
                      from partssalesorder ps
                      join partssalesorderdetail psd
                        on ps.id = psd.partssalesorderid
                    
                     where ag.dealerid = ps.submitcompanyid
                       and ag.agencyid = ps.salesunitownercompanyid
                       and rp.sparepartid = psd.sparepartid
                       and Extract(year from
                                   trunc(nvl(ps.submittime, ps.createtime))) =
                           Extract(year from trunc(sysdate - 92))
                       and ceil(Extract(month from trunc(nvl(ps.submittime,
                                                  ps.createtime))) / 3) =
                           ceil(Extract(month from trunc(sysdate - 92)) / 3)
                       and ((ps.status = 1 or
                           (ps.status <> 1 and ps.Time > 0))))*/
            
             group by ag.dealerid,
                      mr.Amount,
                      mv.reserveratio,
                      ag.agencyid,
                      mv.reserveratio) pp;
begin
  for dealer in dealers loop
    insert into DealerReservesRecommended
      select s_DealerReservesRecommended.Nextval,
             Extract(year from trunc(sysdate - 92)),
             ceil(Extract(month from trunc(sysdate - 92)) / 3),
             DealerId,
             DealerCode,
             DealerName,
             SparePartId,
             SparePartCode,
             SparePartName,
             PartType,
             QuarterlyDemand,
             DemandFrequency,
             Quantity,
             
             (case
                 when (case
                        when nvl(minpackingamount, 0) = 0 then
                         DailyDemand
                        else
                         (case
                           when DailyDemand <= minpackingamount then
                            minpackingamount
                           else
                            trunc(DailyDemand / minpackingamount) * minpackingamount
                         end)
                      end) < minpackingamount then
                  minpackingamount
               
                 when (case
                        when nvl(minpackingamount, 0) = 0 then
                         DailyDemand
                        else
                         (case
                           when DailyDemand <= minpackingamount then
                            minpackingamount
                           else
                            trunc(DailyDemand / minpackingamount) * minpackingamount
                         end)
                      end) > minpackingamount and minpackingamount <> 0 then
                  ((case
                    when nvl(minpackingamount, 0) = 0 then
                     DailyDemand
                    else
                     (case
                       when DailyDemand <= minpackingamount then
                        minpackingamount
                       else
                        trunc(DailyDemand / minpackingamount) * minpackingamount
                     end)
                  end) / minpackingamount) * minpackingamount
                 else
                  (case
                    when nvl(minpackingamount, 0) = 0 then
                     DailyDemand
                    else
                     (case
                       when DailyDemand <= minpackingamount then
                        minpackingamount
                       else
                        trunc(DailyDemand / minpackingamount) * minpackingamount
                     end)
                  end)
               end) AdviseQuantity,
             trunc(sysdate),
             round((DailyDemand / amount), 2) as DailyDemand
        from (select DealerId,
                     DealerCode,
                     DealerName,
                     SparePartId,
                     SparePartCode,
                     SparePartName,
                     PartType,
                     QuarterlyDemand,
                     DemandFrequency,
                     nvl(fir, 0) + nvl(sec, 0) + nvl(thid, 0) as Quantity,
                     minpackingamount,
                     mr.amount,
                     (round(nvl(fir * (select cb.weight
                                         from CenterBasicClassification cb
                                        where cb.type = 1
                                          and Month = 1
                                          and status = 1
                                          and rownum = 1),
                                0),
                            2) + round(nvl(sec * (select cb.weight
                                                     from CenterBasicClassification cb
                                                    where cb.type = 1
                                                      and Month = 2
                                                      and status = 1
                                                      and rownum = 1),
                                            0),
                                        2) +
                     round(nvl(thid * (select cb.weight
                                          from CenterBasicClassification cb
                                         where cb.type = 1
                                           and Month = 3
                                           and status = 1
                                           and rownum = 1),
                                0),
                            2)) as DailyDemand 
                from (select dt.id as DealerId,
                             dt.code DealerCode,
                             dt.name DealerName,
                             sp.id SparePartId,
                             sp.code SparePartCode,
                             sp.name SparePartName,
                             rp.parttype parttype,
                             rp.QuarterlyDemand,
                             rp.DemandFrequency,
                             sum((case
                                   when to_char(ps.SubmitTime, 'yyyyMM') =
                                        to_char(trunc(sysdate - 90, 'Q'), 'yyyyMM') then
                                    psd.OrderedQuantity
                                   else
                                    0
                                 end)) as fir,
                             sum((case
                                   when to_char(ps.SubmitTime, 'yyyyMM') =
                                        to_char(add_months(trunc(sysdate - 90, 'Q'), 1), 'yyyyMM') then
                                    psd.OrderedQuantity
                                   else
                                    0
                                 end)) as sec,
                             sum((case
                                   when to_char(ps.SubmitTime, 'yyyyMM') =
                                        to_char(add_months(trunc(sysdate - 90, 'Q'), 2), 'yyyyMM') then
                                    psd.OrderedQuantity
                                   else
                                    0
                                 end)) as thid,
                              nvl(sp.mincaramount,0) minpackingamount
                        from regionalpartscategory rp
                        join agencydealerrelation ag
                          on rp.centerid = ag.agencyid
                         and ag.status = 1
                        join dealer dt
                          on ag.dealerid = dt.id
                        join sparepart sp
                          on rp.sparepartid = sp.id
                        join partsbranch pb
                          on sp.id = pb.partid
                         and pb.status = 1
                        left join PartsSalePriceIncreaseRate psp
                          on pb.increaserategroupid = psp.id
                      /* join StationServiceReport ss
                       on ag.dealerid = ss.dealerid
                      and ss.Year = rp.year
                      and ss.quarter = rp.quarter
                      and rp.sparepartid = ss.sparepartid*/
                        join DealerFormat sf
                          on ag.dealerid = sf.dealerid
                         and sf.status = 1
                         and sf.isbottom = 1
                        join partssalesorder ps
                          on ag.dealerid = ps.submitcompanyid
                         and ag.agencyid = ps.salesunitownercompanyid
                        join partssalesorderdetail psd
                          on ps.id = psd.partssalesorderid
                         and rp.sparepartid = psd.sparepartid
                        join PartsSalesPrice pss
                          on rp.sparepartid = pss.sparepartid
                         and pss.status = 1
                        join MaintenancePart mp
                          on mp.materialcost = (case
                               when sf.MonthlyCosts < 50000 then
                                3
                               when sf.MonthlyCosts >= 50000 and sf.MonthlyCosts < 100000 then
                                2
                               else
                                1
                             end)
                         and mp.issave = 1
                         and mp.price = (case
                               when pss.salesprice <= 2000 then
                                3
                               when pss.salesprice > 2000 and pss.salesprice <= 5000 then
                                2
                               else
                                1
                             end)
                        join MaintenanceReserve mr
                          on mr.type = 1
                        join MaintenanceVariety mv
                          on mv.MaterialCost = (case
                               when sf.MonthlyCosts < 50000 then
                                3
                               when sf.MonthlyCosts >= 50000 and sf.MonthlyCosts < 100000 then
                                2
                               else
                                1
                             end)
                         and mv.Format = sf.Format
                         and mv.Status = 1
                       where Extract(year from trunc(sysdate - 92)) = rp.year
                         and ag.dealerid = dealer.dealerid
                         and ag.agencyid = dealer.agencyid
                         and rp.Quarter =
                             ceil(Extract(month from trunc(sysdate - 92)) / 3)
                         and rp.PartType in (1, 2)
                         and Extract(year from
                                     trunc(nvl(ps.submittime, ps.createtime))) =
                             Extract(year from trunc(sysdate - 92))
                         and ceil(Extract(month from
                                          trunc(nvl(ps.submittime,
                                                    ps.createtime))) / 3) =
                             ceil(Extract(month from trunc(sysdate - 92)) / 3)
                         and ((ps.status = 1 or
                              (ps.status <> 1 and ps.Time > 0)))
                       group by dt.id,
                                dt.code,
                                dt.name,
                                sp.id,
                                sp.code,
                                sp.name,
                                rp.parttype,
                                rp.QuarterlyDemand,
                                rp.DemandFrequency,
                                 nvl(sp.mincaramount,0) 
                       order by rp.QuarterlyDemand desc) pp
                join MaintenanceReserve mr
                  on mr.type = 2
                 and mr.status = 1
               where rownum <= dealer.shouldAmount);
    commit;
    if dealer.shouldAmount > dealer.abCount then
      insert into DealerReservesRecommended
        select s_DealerReservesRecommended.Nextval,
               Extract(year from trunc(sysdate - 92)),
               ceil(Extract(month from trunc(sysdate - 92)) / 3),
               DealerId,
               DealerCode,
               DealerName,
               SparePartId,
               SparePartCode,
               SparePartName,
               PartType,
               QuarterlyDemand,
               DemandFrequency,
               Quantity,
               
               (case
                 when (case
                        when nvl(minpackingamount, 0) = 0 then
                         DailyDemand
                        else
                         (case
                           when DailyDemand <= minpackingamount then
                            minpackingamount
                           else
                            trunc(DailyDemand / minpackingamount) * minpackingamount
                         end)
                      end) < minpackingamount then
                  minpackingamount
               
                 when (case
                        when nvl(minpackingamount, 0) = 0 then
                         DailyDemand
                        else
                         (case
                           when DailyDemand <= minpackingamount then
                            minpackingamount
                           else
                            trunc(DailyDemand / minpackingamount) * minpackingamount
                         end)
                      end) > minpackingamount and minpackingamount <> 0 then
                  ((case
                    when nvl(minpackingamount, 0) = 0 then
                     DailyDemand
                    else
                     (case
                       when DailyDemand <= minpackingamount then
                        minpackingamount
                       else
                        trunc(DailyDemand / minpackingamount) * minpackingamount
                     end)
                  end) / minpackingamount) * minpackingamount
                 else
                  (case
                    when nvl(minpackingamount, 0) = 0 then
                     DailyDemand
                    else
                     (case
                       when DailyDemand <= minpackingamount then
                        minpackingamount
                       else
                        trunc(DailyDemand / minpackingamount) * minpackingamount
                     end)
                  end)
               end) AdviseQuantity,
               trunc(sysdate),
               round((DailyDemand / amount), 2) as DailyDemand
          from (select DealerId,
                       DealerCode,
                       DealerName,
                       SparePartId,
                       SparePartCode,
                       SparePartName,
                       PartType,
                       QuarterlyDemand,
                       DemandFrequency,
                       nvl(fir, 0) + nvl(sec, 0) + nvl(thid, 0) as Quantity,
                       minpackingamount,
                       mr.amount,
                       (round(nvl(fir * (select cb.weight
                                           from CenterBasicClassification cb
                                          where cb.type = 1
                                            and Month = 1
                                            and status = 1
                                            and rownum = 1),
                                  0),
                              2) +
                       round(nvl(sec * (select cb.weight
                                           from CenterBasicClassification cb
                                          where cb.type = 1
                                            and Month = 2
                                            and status = 1
                                            and rownum = 1),
                                  0),
                              2) +
                       round(nvl(thid * (select cb.weight
                                            from CenterBasicClassification cb
                                           where cb.type = 1
                                             and Month = 3
                                             and status = 1
                                             and rownum = 1),
                                  0),
                              2)) as DailyDemand
                  from (select dt.id   as DealerId,
                               dt.code DealerCode,
                               dt.name DealerName,
                               sp.id   SparePartId,
                               sp.code SparePartCode,
                               sp.name SparePartName,
                               /*  (case
                                 when pb.partabc in (6, 7, 11) or psp.groupcode = '3��' then
                                  pb.partabc
                                 else
                                  rp.parttype
                               end) parttype,*/
                               rp.parttype,
                               rp.QuarterlyDemand,
                               rp.DemandFrequency,
                               sum((case
                                     when to_char(ps.SubmitTime, 'yyyyMM') =
                                          to_char(trunc(sysdate - 90, 'Q'), 'yyyyMM') then
                                      psd.OrderedQuantity
                                     else
                                      0
                                   end)) as fir,
                               sum((case
                                     when to_char(ps.SubmitTime, 'yyyyMM') =
                                          to_char(add_months(trunc(sysdate - 90, 'Q'), 1), 'yyyyMM') then
                                      psd.OrderedQuantity
                                     else
                                      0
                                   end)) as sec,
                               sum((case
                                     when to_char(ps.SubmitTime, 'yyyyMM') =
                                          to_char(add_months(trunc(sysdate - 90, 'Q'), 2), 'yyyyMM') then
                                      psd.OrderedQuantity
                                     else
                                      0
                                   end)) as thid,
                                nvl(sp.mincaramount,0)  minpackingamount
                          from regionalpartscategory rp
                          join agencydealerrelation ag
                            on rp.centerid = ag.agencyid
                           and ag.status = 1
                          join dealer dt
                            on ag.dealerid = dt.id
                          join sparepart sp
                            on rp.sparepartid = sp.id
                          join partsbranch pb
                            on sp.id = pb.partid
                           and pb.status = 1
                          join PartsSalesPrice pss
                            on rp.sparepartid = pss.sparepartid
                           and pss.status = 1
                          join DealerFormat sf
                            on ag.dealerid = sf.dealerid
                           and sf.status = 1
                           and sf.isbottom = 1
                          join MaintenancePart mp
                            on mp.materialcost = (case
                                 when sf.MonthlyCosts < 50000 then
                                  3
                                 when sf.MonthlyCosts >= 50000 and sf.MonthlyCosts < 100000 then
                                  2
                                 else
                                  1
                               end)
                           and mp.issave = 1
                           and mp.price = (case
                                 when pss.salesprice <= 2000 then
                                  3
                                 when pss.salesprice > 2000 and pss.salesprice <= 5000 then
                                  2
                                 else
                                  1
                               end)
                          left join PartsSalePriceIncreaseRate psp
                            on pb.increaserategroupid = psp.id
                          left join partssalesorder ps
                            on ag.dealerid = ps.submitcompanyid
                           and ag.agencyid = ps.salesunitownercompanyid
                           and Extract(year from
                                       trunc(nvl(ps.submittime, ps.createtime))) =
                               Extract(year from trunc(sysdate - 92))
                           and ceil(Extract(month from
                                            trunc(nvl(ps.submittime,
                                                      ps.createtime))) / 3) =
                               ceil(Extract(month from trunc(sysdate - 92)) / 3)
                           and ((ps.status = 1 or
                               (ps.status <> 1 and ps.Time > 0)))
                          left join partssalesorderdetail psd
                            on ps.id = psd.partssalesorderid
                           and rp.sparepartid = psd.sparepartid
                        
                         where Extract(year from trunc(sysdate - 92)) =
                               rp.year
                           and rp.Quarter =
                               ceil(Extract(month from trunc(sysdate - 92)) / 3)
                           and rp.sparepartid not in
                               (select dr.sparepartid
                                  from DealerReservesRecommended dr
                                 where trunc(dr.createtime) = trunc(sysdate)
                                   and dr.dealerid = ag.dealerid)
                           and ag.dealerid = dealer.dealerid
                           and rp.PartType in (1, 2)
                           and ag.agencyid = dealer.agencyid
                         group by dt.id,
                                  dt.code,
                                  dt.name,
                                  sp.id,
                                  sp.code,
                                  sp.name,
                                  rp.parttype,
                                  rp.QuarterlyDemand,
                                  rp.DemandFrequency,
                                
                                   nvl(sp.mincaramount,0) 
                         order by rp.QuarterlyDemand desc) pp
                  join MaintenanceReserve mr
                    on mr.type = 2
                   and mr.status = 1
                 where rownum <= dealer.shouldamount - dealer.abCount);
      commit;
    end if;
  end loop;
  commit;
end DealerReservesRecommendedPro;
/
