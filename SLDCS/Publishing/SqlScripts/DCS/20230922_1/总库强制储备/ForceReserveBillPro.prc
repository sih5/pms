create or replace procedure ForceReserveBillPro is
  FComplateId     NUMBER(9);
  FSubVersionCode varchar2(50);
  --查询需要增加服务站强制储备单的服务站
  cursor dealers is
    select distinct d.dealerid,
                    d.dealercode,
                    d.dealername,
                    bs.id as ReserveTypeId,
                    bs.reservetype as ReserveType,
                    ss.id as ReserveTypeSubItemId,
                    ss.subcode as ReserveTypeSubItem,
                    nvl(vs.id, 0) as VersionInfoId,
                    vs.serialcode
      from DealerReservesRecommended d
      join BottomStockForceReserveType bs
        on bs.reservetype = 'L'
       and bs.status = 1
      join BottomStockForceReserveSub ss
        on ss.reservetype = 'L'
       and ss.subcode = 'L001'
       and ss.status = 1
      left join VersionInfo vs
        on vs.companyid = d.dealerid
       and vs.type = 1
       and vs.bottomstockforcereservesubid = ss.id
     where ceil(Extract(month from trunc(sysdate)) / 3) =
           ceil(Extract(month from trunc(d.createtime)) / 3)
       and to_char(d.createtime, 'yyyy') = to_char(sysdate, 'yyyy');

begin
  for c_dealers in dealers loop
    --储备单Id
    FComplateId := s_ForceReserveBill.Nextval;
    --获取子集版本号
    if c_dealers.versioninfoid = 0 then
      insert into VersionInfo
      values
        (s_VersionInfo.Nextval,
         c_dealers.dealerid,
         c_dealers.dealercode,
         1,
         1,
         c_dealers.reservetypesubitemid);
      FSubVersionCode := c_dealers.dealercode ||
                         c_dealers.reservetypesubitem || '0001';
    else
      FSubVersionCode := c_dealers.dealercode ||
                         c_dealers.reservetypesubitem || '000' ||
                         to_char(c_dealers.serialcode + 1);
      update VersionInfo
         set SerialCode = SerialCode + 1
       where id = c_dealers.versioninfoid;
    end if;
    --插入主单
    insert into ForceReserveBill
      (id,
       Companyid,
       CompanyCode,
       Companyname,
       Companytype,
       Subversioncode,
       Status,
       ReserveTypeId,
       ReserveType,
       ReserveTypeSubItemId,
       ReserveTypeSubItem,
       CreatorName,
       CreateTime,
       ValidateFrom)
    values
      (FComplateId,
       c_dealers.dealerid,
       c_dealers.dealercode,
       c_dealers.dealername,
       2,
       FSubVersionCode,
       1,
       c_dealers.reservetypeid,
       c_dealers.reservetype,
       c_dealers.reservetypesubitemid,
       c_dealers.reservetypesubitem,
       '系统自动生成',
       sysdate,
       trunc(last_day(SYSDATE) + 1));
    --清单
    insert into ForceReserveBillDetail
      select s_ForceReserveBillDetail.Nextval,
             FComplateId,
             d.sparepartid,
             d.sparepartcode,
             d.sparepartname,
             d.AdviseQuantity,
             d.AdviseQuantity,
             d.AdviseQuantity * pr.centerprice,
             d.PartType,
             pr.centerprice
        from DealerReservesRecommended d
        join BottomStockForceReserveType bs
          on bs.reservetype = 'L'
         and bs.status = 1
        join BottomStockForceReserveSub ss
          on ss.reservetype = 'L'
         and ss.subcode = 'L001'
         and ss.status = 1
        join partssalesprice pr
          on d.sparepartid = pr.sparepartid
         and pr.status = 1
        join agencydealerrelation ag
          on ag.dealerid = d.dealerid
         and ag.status = 1
        left join VersionInfo vs
          on vs.companyid = d.dealerid
         and vs.type = 1
         and vs.bottomstockforcereservesubid = ss.id
       where ceil(Extract(month from trunc(sysdate)) / 3) =
             ceil(Extract(month from trunc(d.createtime)) / 3)
         and to_char(d.createtime, 'yyyy') = to_char(sysdate, 'yyyy')
         and d.dealerid = c_dealers.dealerid;
    commit;
    --2、 定时任务，根据【服务商推荐储备明细】生成“强制储备单”时，若配件在【中心库区域AB类】里，是属于AB类的，
    --但是不在【服务商推荐储备明细】中，也要生成“强制储备单”，且推荐数量和储备数量都设置为0
    insert into ForceReserveBillDetail
      select s_ForceReserveBillDetail.Nextval,
             FComplateId,
             rt.sparepartid,
             sp.code,
             sp.name,
             0,
             0,
             0,
             rt.PartType,
             pr.centerprice
        from regionalpartscategory rt
        join sparepart sp
          on rt.sparepartid = sp.id
        join agencydealerrelation ag
          on rt.centerid = ag.agencyid
         and ag.status = 1
        join BottomStockForceReserveType bs
          on bs.reservetype = 'L'
         and bs.status = 1
        join BottomStockForceReserveSub ss
          on ss.reservetype = 'L'
         and ss.subcode = 'L001'
         and ss.status = 1
        join partssalesprice pr
          on rt.sparepartid = pr.sparepartid
         and pr.status = 1
        left join VersionInfo vs
          on vs.companyid = ag.dealerid
         and vs.type = 1
         and vs.bottomstockforcereservesubid = ss.id
       where Extract(year from trunc(sysdate - 92)) = rt.year
         and rt.Quarter = ceil(Extract(month from trunc(sysdate - 92)) / 3)
         and rt.PartType in (1, 2)
         and not exists
       (select 1
                from DealerReservesRecommended d
               where ceil(Extract(month from trunc(sysdate)) / 3) =
                     ceil(Extract(month from trunc(d.createtime)) / 3)
                 and to_char(d.createtime, 'yyyy') =
                     to_char(sysdate, 'yyyy')
                 and d.sparepartid = rt.sparepartid
                 and d.DealerId = ag.DealerId)
         and ag.dealerid = c_dealers.dealerid;
    commit;
  end loop;
end ForceReserveBillPro;
/
