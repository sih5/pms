create or replace procedure NewForceReserveBillPro is
  FComplateId     NUMBER(9);
  FSubVersionCode varchar2(50);
  FColVersionCode varchar2(50);
  cursor bills is
    select fr.id,
           nvl(vs.id, 0) as VersionInfoId,
           vs.serialcode,
           fr.companyid,
           fr.companycode,
           fr.ReserveTypeSubItem,
           fr.ReserveTypeSubItemId,
           nvl(vs2.id, 0) as ColVersionCodeId,
           vs2.serialcode as ColVersionCodeSeri,
           fr.companytype
      from ForceReserveBill fr
      left join VersionInfo vs
        on vs.companyid = fr.companyid
       and vs.type = 1
       and vs.bottomstockforcereservesubid = fr.ReserveTypeSubItemId
      left join VersionInfo vs2
        on vs.companyid = fr.companyid
       and vs.type = 2
     where fr.status = 4
       and sysdate - fr.approvetime > 365
       and fr.companytype = 3
       and exists
     (select 1
              from ForceReserveBillDetail frb
              join regionalpartscategory rt
                on frb.sparepartid = rt.sparepartid
               and rt.Quarter =
                   ceil(Extract(month from trunc(sysdate - 20)) / 3)
               and rt.PartType in (1, 2)
               and Extract(year from trunc(sysdate - 20)) = rt.year
             where frb.forcereservebillid = fr.id
               and fr.companyid = rt.centerid)
       and exists
     (select 1
              from ForceReserveBillDetail frb
             where frb.forcereservebillid = fr.id
               and frb.sparepartid not in
                   (select rt.sparepartid
                      from regionalpartscategory rt
                     where rt.centerid = fr.companyid
                       and rt.Quarter =
                           ceil(Extract(month from trunc(sysdate - 20)) / 3)
                       and rt.PartType not in (1, 2)
                       and Extract(year from trunc(sysdate - 20)) = rt.year))
    
    union all
    select fr.id,
           nvl(vs.id, 0) as VersionInfoId,
           vs.serialcode,
           fr.companyid,
           fr.companycode,
           fr.ReserveTypeSubItem,
           fr.ReserveTypeSubItemId,
           nvl(vs2.id, 0) as ColVersionCodeId,
           vs2.serialcode as ColVersionCodeSeri,
           fr.companytype
      from ForceReserveBill fr
      left join VersionInfo vs
        on vs.companyid = fr.companyid
       and vs.type = 1
       and vs.bottomstockforcereservesubid = fr.ReserveTypeSubItemId
      left join VersionInfo vs2
        on vs.companyid = fr.companyid
       and vs.type = 2
     where fr.status = 4
       and sysdate - fr.approvetime > 365
       and fr.companytype = 1
       and exists (select 1
              from ForceReserveBillDetail frb
              join partsbranch rt
                on frb.sparepartid = rt.PartId
               and rt.PartABC in (1, 2)
             where frb.forcereservebillid = fr.id)
       and exists (select 1
              from ForceReserveBillDetail frb
              join partsbranch rt
                on frb.sparepartid = rt.PartId
               and rt.PartABC not in (1, 2)
             where frb.forcereservebillid = fr.id);

begin
  for bill in bills loop
    --储备单Id
    FComplateId := s_ForceReserveBill.Nextval;
    --获取子集版本号
    if bill.versioninfoid = 0 then
      insert into VersionInfo
      values
        (s_VersionInfo.Nextval,
         bill.companyid,
         bill.companycode,
         1,
         1,
         bill.reservetypesubitemid);
      FSubVersionCode := bill.companycode || bill.reservetypesubitem ||
                         '0001';
    else
      FSubVersionCode := bill.companycode || bill.reservetypesubitem ||
                         '000' || to_char(bill.serialcode + 1);
      update VersionInfo
         set SerialCode = SerialCode + 1
       where id = bill.versioninfoid;
    end if;
    --获取合集版本号
    if bill.ColVersionCodeId = 0 then
      insert into VersionInfo
      values
        (s_VersionInfo.Nextval,
         bill.companyid,
         bill.companycode,
         2,
         1,
         null);
      FColVersionCode := 'BZ' || bill.companycode || '0001';
    else
      FColVersionCode := 'BZ' || bill.companycode || '000' ||
                         to_char(bill.ColVersionCodeSeri + 1);
      update VersionInfo
         set SerialCode = SerialCode + 1
       where id = bill.ColVersionCodeId;
    end if;
    --插入主单
    insert into ForceReserveBill
      (id,
       companyid,
       CompanyCode,
       CompanyName,
       CompanyType,
       SubVersionCode,
       ColVersionCode,
       Status,
       ReserveTypeId,
       ReserveType,
       ReserveTypeSubItemId,
       ReserveTypeSubItem,
       CreatorName,
       CreateTime,
       ValidateFrom)
      select FComplateId,
             b.companyid,
             b.companycode,
             b.companyname,
             b.companytype,
             FSubVersionCode,
             FColVersionCode,
             4,
             b.reservetypeid,
             b.reservetype,
             b.reservetypesubitemid,
             b.reservetypesubitem,
             'AB类更新',
             sysdate,
             sysdate
        from ForceReserveBill b
       where id = bill.id;
    --插入清单
    if bill.companytype = 3 then
      insert into ForceReserveBillDetail
        select s_ForceReserveBillDetail.Nextval,
               FComplateId,
               p.sparepartid,
               p.sparepartcode,
               p.sparepartname,
               p.suggestforcereserveqty,
               p.forcereserveqty,
               p.reservefee,
               p.centerpartproperty,
               p.centerprice
          from ForceReserveBillDetail p
         where ForceReserveBillId = bill.id
           and p.sparepartid not in
               (select rt.sparepartid
                  from regionalpartscategory rt
                 where rt.centerid = bill.companyid
                   and rt.Quarter =
                       ceil(Extract(month from trunc(sysdate - 20)) / 3)
                   and rt.PartType in (1, 2)
                   and Extract(year from trunc(sysdate - 20)) = rt.year);
    else
      insert into ForceReserveBillDetail
        select s_ForceReserveBillDetail.Nextval,
               FComplateId,
               p.sparepartid,
               p.sparepartcode,
               p.sparepartname,
               p.suggestforcereserveqty,
               p.forcereserveqty,
               p.reservefee,
               p.centerpartproperty,
               p.centerprice
          from ForceReserveBillDetail p
         where ForceReserveBillId = bill.id
           and p.sparepartid not in
               (select pb.partid
                  from partsbranch pb
                 where pb.partabc in (1, 2));
    end if;
    --更新原单据为作废
    update ForceReserveBill
       set status = 99, AbandonerName = 'AB类更新', AbandonTime = sysdate
     where id = bill.id;
    commit;
  end loop;
end NewForceReserveBillPro;
/
