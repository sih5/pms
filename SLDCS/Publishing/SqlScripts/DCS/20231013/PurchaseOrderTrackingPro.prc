create or replace procedure PurchaseOrderTrackingPro is
begin
  insert into PurchaseOrderTracking
    select s_PurchaseOrderTracking.Nextval,
           p.code,
           pd.sparepartid,
           pd.sparepartcode,
           pd.sparepartname,
           pd.SupplierPartCode,
           pd.orderamount,
           p.partssupplierid,
           p.partssuppliercode,
           p.partssuppliername,
           p.warehouseid,
           wh.name,
           wh.code,
           p.createtime,
           (select to_char(wm_concat(PersonName))
              from PersonnelSupplierRelation p
             where status = 1
               and p.supplierid = p.partssupplierid),
           0,
           p.PartsPurchaseOrderTypeId
      from partspurchaseorder p
      join warehouse wh
        on p.warehouseid = wh.id
      join partspurchaseorderdetail pd
        on p.id = pd.partspurchaseorderid
     where p.warehousename not like '%����%'
       and p.createtime >= to_date('2023/9/1', 'YYYY/MM/DD')
       and nvl(p.ifdirectprovision, 0) = 0
       and not exists (select 1
              from PurchaseOrderTracking pp
             where pp.purchasecode = p.code
               and pd.sparepartid = pp.sparepartid);
  commit;
end PurchaseOrderTrackingPro;

