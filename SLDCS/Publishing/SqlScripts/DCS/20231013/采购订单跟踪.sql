create sequence S_PurchaseOrderTrackDetail
;

create sequence S_PurchaseOrderTracking
;
create sequence S_PartsPurchaseHoliday;
create table PurchaseOrderTrackDetail  (
   Id                 NUMBER(9)                       not null,
   TrackingId         NUMBER(9)                       not null,
   TrackStatus        VARCHAR2(500),
   TrackDate          DATE,
   constraint PK_PURCHASEORDERTRACKDETAIL primary key (Id)
)
 ;
 
create table PurchaseOrderTracking  (
   Id                 NUMBER(9)                       not null,
   PurchaseCode       VARCHAR2(50)                    not null,
   SparePartId        NUMBER(9)                       not null,
   SparePartCode      VARCHAR2(50)                    not null,
   SparePartName      VARCHAR2(100)                   not null,
   SupplierPartCode   VARCHAR2(50),
   OrderAmount        NUMBER(9),
   PartsSupplierId    NUMBER(9),
   PartsSupplierCode  VARCHAR2(50),
   PartsSupplierName  VARCHAR2(100),
   WarehouseId        NUMBER(9),
   WarehouseName      VARCHAR2(100),
   WarehouseCode      VARCHAR2(50),
   OrderCreateTime    DATE,
   PersonName         VARCHAR2(100),
   Levels              NUMBER(9),
   PartsPurchaseOrderTypeId NUMBER(9)
   constraint PK_PURCHASEORDERTRACKING primary key (Id)
);

 create table PartsPurchaseHoliday  (
   Id                 NUMBER(9)                       not null,
   Status             NUMBER(9)                       not null,
   ValidFrom          DATE                            not null,
   ValidTo            DATE                            not null,
   Days               NUMBER(9)                       not null,
   CreatorId          NUMBER(9),
   CreatorName        VARCHAR2(100),
   CreateTime         DATE,
   AbandonerId        NUMBER(9),
   AbandonerName      VARCHAR2(50),
   AbandonTime        DATE,
   constraint PK_PARTSPURCHASEHOLIDAY primary key (Id)
);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PurchaseOrderTrackingLevel', '优先层级', 1, '特急订单', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PurchaseOrderTrackingLevel', '优先层级', 2, '整改订单', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PurchaseOrderTrackingLevel', '优先层级', 3, '紧急订单', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PurchaseOrderTrackingLevel', '优先层级', 4, '出口订单', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PurchaseOrderTrackingLevel', '优先层级', 5, '销售缺件', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PurchaseOrderTrackingLevel', '优先层级', 6, '计划需求', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PurchaseOrderTrackingLevel', '优先层级', 7, '项目件订单', 1, 1);
