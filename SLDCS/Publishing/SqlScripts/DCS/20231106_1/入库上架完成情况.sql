create table InboundShelvesCompletion  (
   Id                   NUMBER(9)                       not null,
   WorkDays             NUMBER(9)                       not null,
   InboundDate          DATE                            not null,
   FinishFeeTwentyF     NUMBER(19,4),
   FinishFeeFortyE      NUMBER(19,4),
   FinishFeeSeventT     NUMBER(19,4),
   FinishFeeOverTwentyF NUMBER(19,4),
   InBoundFee           NUMBER(19,4),
   DayTwentyFRate       NUMBER(19,4),
   DayFortyERate        NUMBER(19,4),
   DaySeventyTRate      NUMBER(19,4),
   OverTwentyFRate      NUMBER(19,4),
   OverFortyERate       NUMBER(19,4),
   OverSeventyTRate     NUMBER(19,4),
   CreateTime           DATE,
   constraint PK_INBOUNDSHELVESCOMPLETION primary key (Id)
);
create sequence S_InboundShelvesCompletion;

Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'InboundShelvesCompletionWorkDays', '工作日', 1, '星期一', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'InboundShelvesCompletionWorkDays', '工作日', 2, '星期二', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'InboundShelvesCompletionWorkDays', '工作日', 3, '星期三', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'InboundShelvesCompletionWorkDays', '工作日', 4, '星期四', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'InboundShelvesCompletionWorkDays', '工作日', 5, '星期五', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'InboundShelvesCompletionWorkDays', '工作日', 6, '星期六', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'InboundShelvesCompletionWorkDays', '工作日', 7, '星期日', 1, 1);
