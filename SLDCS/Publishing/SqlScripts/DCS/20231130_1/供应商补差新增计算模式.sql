alter table SupplierAnnualRebateBill add(   CalculationMode    NUMBER(9));
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'SupplierAnnualRebateBillCalculationMode', '计算模式', 1, '按照比例', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'SupplierAnnualRebateBillCalculationMode', '计算模式', 2, '固定金额', 1, 1);
