create or replace procedure AUTOBOTTOMSTOCKCOLVERSION is
begin
update BottomStockColVersion s
   set LapseReason =
       (select distinct LapseReason
          from ForceReserveBill d
         where s.ColVersionCode = d.ColVersionCode
           and (d.abandonername = 'AB类更新' or d.abandonername = '审核通过后作废'))
 where s.LapseReason is null and  s.ColVersionCode in
       (select distinct d.colversioncode
          from ForceReserveBill d
         where (d.abandonername = 'AB类更新' or d.abandonername = '审核通过后作废'));
commit;
  --当前配件在上以版本号不存在时，插入新的合集信息
  insert into BottomStockColVersion
    select s_BottomStockColVersion.Nextval,
           d.companyid,
           d.companycode,
           d.companyname,
           d.companytype,
           d.sparepartid,
           d.sparepartcode,
           d.sparepartname,
           d.ColVersionCode,
           d.saleprice,
           d.reservefee,
           d.reserveqty,
           d.starttime,
           null,
           1,
           sysdate,
           d.reservetypeid,
           d.reservetype,
           null
      from (select bv.companyid,
                   bv.companycode,
                   bv.companyname,
                   cp.type as companytype,
                   bv.sparepartid,
                   bv.sparepartcode,
                   bv.sparepartname,
                   bs.ColVersionCode,
                   bv.saleprice,
                   bv.reservefee,
                   bv.reserveqty,
                   bv.starttime,
                   bv.reservetypeid,
                   bv.reservetype,
                   row_number() over(partition by bv.companyid, bv.sparepartid order by bv.reserveqty desc) as ydy
              from BottomStockSubVersion bv
              join company cp
                on bv.companyid = cp.id
              left join (select distinct ColVersionCode, CompanyId
                          from BottomStockSubVersion bs
                         where status = 1
                           and to_number(SUBSTR(ColVersionCode, 16)) =
                               (select max(to_number(SUBSTR(ColVersionCode,
                                                            16)))
                                  from BottomStockSubVersion yu
                                 where yu.companyid = bs.companyid
                                   and yu.status = 1)) bs
                on bv.companyid = bs.companyId
             where bv.status = 1
               and not exists
             (select 1
                      from BottomStockColVersion bk
                     where bk.sparepartid = bv.sparepartid
                       and bk.companyid = bv.companyid)) d
     where d.ydy = 1;
  commit;
  --当前配件在上一版本号中存在有效时，更新储备金额，储备数量，经销价，合集版本号
  update BottomStockColVersion bs
     set (reservefee,
          reserveqty,
          saleprice,
          ColVersionCode,
          reservetypeid,
          reservetype) =
         (select d.reservefee,
                 d.reserveqty,
                 d.saleprice,
                 d.ColVersionCode,
                 d.reservetypeid,
                 d.reservetype
            from (select bv.sparepartid,
                         bv.companyid,
                         bv.reservefee,
                         bv.reserveqty,
                         bv.saleprice,
                         bs.ColVersionCode,
                         bv.reservetypeid,
                         bv.reservetype,
                         row_number() over(partition by bv.companyid, bv.sparepartid order by bv.reserveqty desc) as ydy
                    from BottomStockSubVersion bv
                    left join (select distinct ColVersionCode, CompanyId
                                from BottomStockSubVersion bs
                               where status = 1
                                 and to_number(SUBSTR(ColVersionCode,
                                                      greatest(-4,
                                                               -length(ColVersionCode)),
                                                      4)) =
                                     (select max(to_number(SUBSTR(ColVersionCode,
                                                                  greatest(-4,
                                                                           -length(ColVersionCode)),
                                                                  4)))
                                        from BottomStockSubVersion yu
                                       where yu.companyid = bs.companyid
                                         and yu.status = 1)) bs
                      on bv.companyid = bs.companyId
                   where bv.status = 1) d
           where d.ydy = 1
             and d.companyid = bs.companyid
             and d.sparepartid = bs.sparepartid)
   where bs.status = 1
     and exists (select 1
            from BottomStockSubVersion s
           where bs.sparepartid = s.sparepartid
             and bs.companyid = s.companyid
             and s.status = 1);
  commit;
  --当前配件在上一版本号中存在作废时，更新储备金额，储备数量，经销价，合集版本号，生效时间（审核通过时间），失效时间（清空），状态（有效）
  update BottomStockColVersion bs
     set (reservefee,
          reserveqty,
          saleprice,
          ColVersionCode,
          status,
          EndTime,
          StartTime,
          reservetypeid,
          reservetype) =
         (select d.reservefee,
                 d.reserveqty,
                 d.saleprice,
                 d.ColVersionCode,
                 1,
                 null,
                 d.StartTime,
                 d.reservetypeid,
                 d.reservetype
            from (select bv.sparepartid,
                         bv.companyid,
                         bv.reservefee,
                         bv.reserveqty,
                         bv.saleprice,
                         bs.ColVersionCode,
                         bv.StartTime,
                         bv.reservetype,
                         bv.reservetypeid,
                         row_number() over(partition by bv.companyid, bv.sparepartid order by bv.reserveqty desc) as ydy
                    from BottomStockSubVersion bv
                    left join (select distinct ColVersionCode, CompanyId
                                from BottomStockSubVersion bs
                               where status = 1
                                 and to_number(SUBSTR(ColVersionCode, 16)) =
                                     (select max(to_number(SUBSTR(ColVersionCode,
                                                                  16)))
                                        from BottomStockSubVersion yu
                                       where yu.companyid = bs.companyid
                                         and yu.status = 1)) bs
                      on bv.companyid = bs.companyId
                   where bv.status = 1) d
           where d.ydy = 1
             and d.companyid = bs.companyid
             and d.sparepartid = bs.sparepartid)
   where bs.status = 99
     and exists (select 1
            from BottomStockSubVersion s
           where bs.sparepartid = s.sparepartid
             and bs.companyid = s.companyid
             and s.status = 1);
  commit;
  --  同时跟上一个合集比较，上一个有但是最新的没有的，标记上一个状态为取消
  update BottomStockColVersion bv
     set status = 99, EndTime = sysdate
   where not exists (select 1
            from BottomStockSubVersion s
           where bv.sparepartid = s.sparepartid
             and bv.companyid = s.companyid
             and s.status = 1)
     and status = 1;
  commit;
  -- 若 合集版本号中的生效时间，对应的子集版本号是失效状态，则更新为子集版本号有效的最早的生效时间
  --若合集版本号的生效时间不是 子集版本号有效的数据的最小生效时间，则更新
  update BottomStockColVersion y
     set starttime =
         (select pp.StartTime
            from BottomStockColVersion bb
            join (select companyid, sparepartid, StartTime
                   from (select bv.sparepartid,
                                bv.companyid,
                                bv.StartTime,
                                row_number() over(partition by bv.companyid, bv.sparepartid order by bv.StartTime asc) as ydy
                           from BottomStockSubVersion bv
                          where bv.status = 1) d
                  where d.ydy = 1) pp
              on bb.companyid = pp.companyid
             and bb.sparepartid = pp.sparepartid
           where bb.status = 1
             and y.id = bb.id)
   where id in (select bb.id
                  from BottomStockColVersion bb
                  join (select companyid, sparepartid, StartTime
                         from (select bv.sparepartid,
                                      bv.companyid,
                                      bv.StartTime,
                                      row_number() over(partition by bv.companyid, bv.sparepartid order by bv.StartTime asc) as ydy
                                 from BottomStockSubVersion bv
                                where bv.status = 1) d
                        where d.ydy = 1) pp
                    on bb.companyid = pp.companyid
                   and bb.sparepartid = pp.sparepartid
                 where bb.status = 1
                   and bb.starttime <> pp.starttime);
  commit;
end AUTOBOTTOMSTOCKCOLVERSION;
/
