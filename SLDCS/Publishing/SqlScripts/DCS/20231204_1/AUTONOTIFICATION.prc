create or replace procedure AUTONOTIFICATION is
  NotificationId NUMBER(9);
  FComplateId    NUMBER(9);
  FNewCode       varchar2(50);
  FNewCodePOPCB  varchar2(50);
  EnterpriseCode varchar2(50);
  EnterpriseName varchar2(350);
  cursor CancelSparepart is
    select distinct bs.companyid,
                    bs.companycode,
                    bs.companyname,
                    cp.type as companytype,
                    bs.ColVersionCode
      from BottomStockColVersion bs
      join company cp
        on bs.companyid = cp.id
      left join BottomStockSubVersion bsn
        on bs.companyid = bsn.companyid
       and bs.sparepartid = bsn.sparepartid
       and bsn.status = 1
     where bs.status = 99
       and bsn.id is null
       and (case
             when bs.companytype = 2 then
              (select distinct 1
                 from DealerPartsStock dps
                where dps.sparepartid = bs.sparepartid
                  and dps.dealerid = bs.companyid
                  and dps.Quantity > 0)
             else
              (select distinct 1
                 from partsstock psk
                 join WarehouseAreaCategory wa
                   on psk.warehouseareacategoryid = wa.id
                where psk.partid = bs.sparepartid
                  and psk.storagecompanyid = bs.companyid
                  and wa.category = 1
                  and psk.quantity - nvl(psk.lockedqty, 0) > 0)
           end) = 1;
  cursor updateBottomStockColVersions is
    select distinct s.companyid,
                    c.code as companycode,
                    c.name as companyname,
                    c.type as companytype,
                    s.type,
                    (case
                      when s.type = 1 then
                       '储备数据有增加'
                      else
                       '储备数据有减少'
                    end) as Title,
                    (case
                      when s.type = 1 then
                       '保底储备合集版本号配件明细增加或储备数量有增加，请控制计划量并优先消耗保底储备库存'
                      else
                       '合集版本号保底明细有失效或配件储备明细数量有减少，请控制计划量并优先消耗保底储备库存'
                    end) as Content
      from AddOrDeleteColVersion s
      join company c
        on s.companyid = c.id
     where nvl(s.iswrite, 0) = 0;
begin
  delete from AddOrDeleteColVersion s
   where trunc(s.createtime) <= sysdate - 60;
  commit;
  for c_CancelSparepart in updateBottomStockColVersions loop
    begin
      NotificationId := s_Notification.Nextval;
      FNewCode       := 'GG{CORPCODE}' || TO_CHAR(sysdate, 'yyyymmdd') ||
                        '{SERIAL}';
      BEGIN
        SELECT Id
          INTO FComplateId
          FROM CodeTemplate
         WHERE name = 'Notification'
           AND IsActived = 1;
      END;
    
      BEGIN
        select code, name
          into EnterpriseCode, EnterpriseName
          from Company
         where type = 1
           and status = 1;
      END;
    
      FNewCodePOPCB := regexp_replace(regexp_replace(FNewCode,
                                                     '{CORPCODE}',
                                                     EnterpriseCode),
                                      '{SERIAL}',
                                      lpad(TO_CHAR(GetSerial(FComplateId,
                                                             TRUNC(sysdate,
                                                                   'DD'),
                                                             EnterpriseCode)),
                                           6,
                                           '0'));
      insert into NotificationLimit
      values
        (s_NotificationLimit.Nextval,
         NotificationId,
         c_CancelSparepart.Companytype);
      insert into CompanyDetail
      values
        (s_CompanyDetail.Nextval,
         NotificationId,
         c_CancelSparepart.Companyid,
         c_CancelSparepart.Companytype,
         c_CancelSparepart.Companycode,
         c_CancelSparepart.Companyname);
      insert into Notification
        (Id,
         Code,
         CompanyId,
         CompanyCode,
         CompanyName,
         CompanyType,
         Type,
         Title,
         Content,
         Status,
         PublishDep,
         UrgentLevel,
         CreateTime)
      values
        (NotificationId,
         FNewCodePOPCB,
         12730,
         EnterpriseCode,
         EnterpriseName,
         1,
         6,
         c_CancelSparepart.Title,
         c_CancelSparepart.Content,
         1,
         '销售服务组',
         2,
         sysdate);
      commit;
      update AddOrDeleteColVersion
         set iswrite = 1
       where companyid = c_CancelSparepart.Companyid
         and type = c_CancelSparepart.type;
      commit;
    end;
  end loop;
  for c_CancelSparepart in CancelSparepart loop
    begin
      NotificationId := s_Notification.Nextval;
      FNewCode       := 'GG{CORPCODE}' || TO_CHAR(sysdate, 'yyyymmdd') ||
                        '{SERIAL}';
      BEGIN
        SELECT Id
          INTO FComplateId
          FROM CodeTemplate
         WHERE name = 'Notification'
           AND IsActived = 1;
      END;
    
      BEGIN
        select code, name
          into EnterpriseCode, EnterpriseName
          from Company
         where type = 1
           and status = 1;
      END;
    
      FNewCodePOPCB := regexp_replace(regexp_replace(FNewCode,
                                                     '{CORPCODE}',
                                                     EnterpriseCode),
                                      '{SERIAL}',
                                      lpad(TO_CHAR(GetSerial(FComplateId,
                                                             TRUNC(sysdate,
                                                                   'DD'),
                                                             EnterpriseCode)),
                                           6,
                                           '0'));
      insert into NotificationLimit
      values
        (s_NotificationLimit.Nextval,
         NotificationId,
         c_CancelSparepart.Companytype);
      insert into CompanyDetail
      values
        (s_CompanyDetail.Nextval,
         NotificationId,
         c_CancelSparepart.Companyid,
         c_CancelSparepart.Companytype,
         c_CancelSparepart.Companycode,
         c_CancelSparepart.Companyname);
      insert into Notification
        (Id,
         Code,
         CompanyId,
         CompanyCode,
         CompanyName,
         CompanyType,
         Type,
         Title,
         Content,
         Status,
         PublishDep,
         UrgentLevel,
         CreateTime)
      values
        (NotificationId,
         FNewCodePOPCB,
         12730,
         EnterpriseCode,
         EnterpriseName,
         1,
         6,
         c_CancelSparepart.Colversioncode || '合集版本号取消强制储备',
         c_CancelSparepart.Colversioncode ||
         '有配件合集版本号取消了强制储备,请到【保底库存明合集版本号查询】查看取消储备配件明细',
         1,
         '销售服务组',
         2,
         sysdate);
      commit;
    end;
  end loop;
end AUTONOTIFICATION;
/
