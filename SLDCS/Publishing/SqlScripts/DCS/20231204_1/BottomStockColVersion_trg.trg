create or replace trigger BottomStockColVersion_trg
  after insert or update on BottomStockColVersion
  for each row
--declare
  -- local variables here
begin
  if :old.id is null or :old.reserveqty < :new.reserveqty then
    insert into AddOrDeleteColVersion
      select S_AddOrDeleteColVersion.Nextval, c.id, 1, 0, sysdate
        from company c
       where c.id = :new.companyid
         and not exists (select 1
                from AddOrDeleteColVersion p
               where p.companyid = c.id
                 and trunc(p.createtime) = trunc(sysdate)
                 and p.type != 1);
  end if;
  if :old.id is not null and
     (:old.reserveqty > :new.reserveqty or :new.status = 99) then
    insert into AddOrDeleteColVersion
      select S_AddOrDeleteColVersion.Nextval, c.id, 2, 0, sysdate
        from company c
       where c.id = :new.companyid
         and not exists (select 1
                from AddOrDeleteColVersion p
               where p.companyid = c.id
                 and trunc(p.createtime) = trunc(sysdate)
                 and p.type != 2);
  end if;
end BottomStockColVersion_trg;
/
