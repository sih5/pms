alter table BottomStockColVersion add(LapseReason        VARCHAR2(500));
alter table BottomStockSubVersion add(LapseReason        VARCHAR2(500));
alter table SparePart add(IsBottom           NUMBER(1));


create table AddOrDeleteColVersion  (
   Id                   NUMBER(9)                       not null,
   CompanyId            NUMBER(9)                       not null,
   Type                 NUMBER(9)                       not null,
   IsWrite              NUMBER(1) ,
   CreateTime           DATE,
constraint PK_ADDORDELETECOLVERSION primary key (Id)
);
create sequence S_AddOrDeleteColVersion;
update keyvalueitem set value='更新强制储备' where name='Notification_Type' and key=6;

