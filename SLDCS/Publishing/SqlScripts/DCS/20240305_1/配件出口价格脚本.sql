alter table ExportCustomerInfo add Coefficient NUMBER(19,4);

alter table PartsSalesOrder add IsMissing NUMBER(1);
alter table PartsSalesOrder add WorkOrderCode VARCHAR2(100);