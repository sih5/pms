/*==============================================================*/
/* DBMS name:      ORACLE Version 10gR2                         */
/* Created on:     2023/9/25 14:05:14                           */
/*==============================================================*/


declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('CenterApproveBill');
  if num>0 then
    execute immediate 'drop table CenterApproveBill cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('CenterAuthorizedProvince');
  if num>0 then
    execute immediate 'drop table CenterAuthorizedProvince cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('CenterSaleReturnStrategy');
  if num>0 then
    execute immediate 'drop table CenterSaleReturnStrategy cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('CodeTemplate');
  if num>0 then
    execute immediate 'drop table CodeTemplate cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('CodeTemplateSerial');
  if num>0 then
    execute immediate 'drop table CodeTemplateSerial cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('CompanyDetail');
  if num>0 then
    execute immediate 'drop table CompanyDetail cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('Countersignature');
  if num>0 then
    execute immediate 'drop table Countersignature cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('CountersignatureDetail');
  if num>0 then
    execute immediate 'drop table CountersignatureDetail cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('DailySalesWeight');
  if num>0 then
    execute immediate 'drop table DailySalesWeight cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('DataBaseLink');
  if num>0 then
    execute immediate 'drop table DataBaseLink cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('DataBaseLinkBranch');
  if num>0 then
    execute immediate 'drop table DataBaseLinkBranch cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('DealerReservesRecommended');
  if num>0 then
    execute immediate 'drop table DealerReservesRecommended cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('KeyValueItem');
  if num>0 then
    execute immediate 'drop table KeyValueItem cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('LayerNodeType');
  if num>0 then
    execute immediate 'drop table LayerNodeType cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('LayerNodeTypeAffiliation');
  if num>0 then
    execute immediate 'drop table LayerNodeTypeAffiliation cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('LayerStructure');
  if num>0 then
    execute immediate 'drop table LayerStructure cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('LayerStructurePurpose');
  if num>0 then
    execute immediate 'drop table LayerStructurePurpose cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('LinkAllocation');
  if num>0 then
    execute immediate 'drop table LinkAllocation cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('MDMPartsSalesCategoryMap');
  if num>0 then
    execute immediate 'drop table MDMPartsSalesCategoryMap cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('MaintenanceMandatory');
  if num>0 then
    execute immediate 'drop table MaintenanceMandatory cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('MaintenancePart');
  if num>0 then
    execute immediate 'drop table MaintenancePart cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('MaintenanceReserve');
  if num>0 then
    execute immediate 'drop table MaintenanceReserve cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('MaintenanceVariety');
  if num>0 then
    execute immediate 'drop table MaintenanceVariety cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('Notification');
  if num>0 then
    execute immediate 'drop table Notification cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('NotificationLimit');
  if num>0 then
    execute immediate 'drop table NotificationLimit cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('NotificationOpLog');
  if num>0 then
    execute immediate 'drop table NotificationOpLog cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('NotificationReply');
  if num>0 then
    execute immediate 'drop table NotificationReply cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('Personnel');
  if num>0 then
    execute immediate 'drop table Personnel cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('Region');
  if num>0 then
    execute immediate 'drop table Region cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('ReportDownloadMsg');
  if num>0 then
    execute immediate 'drop table ReportDownloadMsg cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('ReserveFactorMasterOrder');
  if num>0 then
    execute immediate 'drop table ReserveFactorMasterOrder cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('ReserveFactorOrderDetail');
  if num>0 then
    execute immediate 'drop table ReserveFactorOrderDetail cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('ScheduleExportState');
  if num>0 then
    execute immediate 'drop table ScheduleExportState cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('SmartCompany');
  if num>0 then
    execute immediate 'drop table SmartCompany cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('SmartOrderCalendar');
  if num>0 then
    execute immediate 'drop table SmartOrderCalendar cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('SmartProcessMethod');
  if num>0 then
    execute immediate 'drop table SmartProcessMethod cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('WarehouseHoliday');
  if num>0 then
    execute immediate 'drop table WarehouseHoliday cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_CenterApproveBill');
  if num>0 then
    execute immediate 'drop sequence S_CenterApproveBill';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_CenterAuthorizedProvince');
  if num>0 then
    execute immediate 'drop sequence S_CenterAuthorizedProvince';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_CenterSaleReturnStrategy');
  if num>0 then
    execute immediate 'drop sequence S_CenterSaleReturnStrategy';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_CodeTemplate');
  if num>0 then
    execute immediate 'drop sequence S_CodeTemplate';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_CodeTemplateSerial');
  if num>0 then
    execute immediate 'drop sequence S_CodeTemplateSerial';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_Company');
  if num>0 then
    execute immediate 'drop sequence S_Company';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_CompanyDetail');
  if num>0 then
    execute immediate 'drop sequence S_CompanyDetail';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_Countersignature');
  if num>0 then
    execute immediate 'drop sequence S_Countersignature';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_CountersignatureDetail');
  if num>0 then
    execute immediate 'drop sequence S_CountersignatureDetail';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_DailySalesWeight');
  if num>0 then
    execute immediate 'drop sequence S_DailySalesWeight';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_DataBaseLink');
  if num>0 then
    execute immediate 'drop sequence S_DataBaseLink';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_DataBaseLinkBranch');
  if num>0 then
    execute immediate 'drop sequence S_DataBaseLinkBranch';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_DealerReservesRecommended');
  if num>0 then
    execute immediate 'drop sequence S_DealerReservesRecommended';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_KeyValueItem');
  if num>0 then
    execute immediate 'drop sequence S_KeyValueItem';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_LayerNodeType');
  if num>0 then
    execute immediate 'drop sequence S_LayerNodeType';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_LayerNodeTypeAffiliation');
  if num>0 then
    execute immediate 'drop sequence S_LayerNodeTypeAffiliation';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_LayerStructure');
  if num>0 then
    execute immediate 'drop sequence S_LayerStructure';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_LayerStructurePurpose');
  if num>0 then
    execute immediate 'drop sequence S_LayerStructurePurpose';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_LinkAllocation');
  if num>0 then
    execute immediate 'drop sequence S_LinkAllocation';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_MDMPartsSalesCategoryMap');
  if num>0 then
    execute immediate 'drop sequence S_MDMPartsSalesCategoryMap';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_MaintenanceMandatory');
  if num>0 then
    execute immediate 'drop sequence S_MaintenanceMandatory';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_MaintenancePart');
  if num>0 then
    execute immediate 'drop sequence S_MaintenancePart';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_MaintenanceReserve');
  if num>0 then
    execute immediate 'drop sequence S_MaintenanceReserve';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_MaintenanceVariety');
  if num>0 then
    execute immediate 'drop sequence S_MaintenanceVariety';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_Notification');
  if num>0 then
    execute immediate 'drop sequence S_Notification';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_NotificationLimit');
  if num>0 then
    execute immediate 'drop sequence S_NotificationLimit';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_NotificationOpLog');
  if num>0 then
    execute immediate 'drop sequence S_NotificationOpLog';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_NotificationReply');
  if num>0 then
    execute immediate 'drop sequence S_NotificationReply';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_Region');
  if num>0 then
    execute immediate 'drop sequence S_Region';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_ReportDownloadMsg');
  if num>0 then
    execute immediate 'drop sequence S_ReportDownloadMsg';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_ReserveFactorMasterOrder');
  if num>0 then
    execute immediate 'drop sequence S_ReserveFactorMasterOrder';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_ReserveFactorOrderDetail');
  if num>0 then
    execute immediate 'drop sequence S_ReserveFactorOrderDetail';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_ScheduleExportState');
  if num>0 then
    execute immediate 'drop sequence S_ScheduleExportState';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_SmartCompany');
  if num>0 then
    execute immediate 'drop sequence S_SmartCompany';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_SmartOrderCalendar');
  if num>0 then
    execute immediate 'drop sequence S_SmartOrderCalendar';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_SmartProcessMethod');
  if num>0 then
    execute immediate 'drop sequence S_SmartProcessMethod';
  end if;
end;
/

create sequence S_CenterApproveBill
/

create sequence S_CenterAuthorizedProvince
/

create sequence S_CenterSaleReturnStrategy
/

create sequence S_CodeTemplate
/

create sequence S_CodeTemplateSerial
/

create sequence S_Company
/

create sequence S_CompanyDetail
/

create sequence S_Countersignature
/

create sequence S_CountersignatureDetail
/

create sequence S_DailySalesWeight
/

create sequence S_DataBaseLink
/

create sequence S_DataBaseLinkBranch
/

create sequence S_DealerReservesRecommended
/

create sequence S_KeyValueItem
/

create sequence S_LayerNodeType
/

create sequence S_LayerNodeTypeAffiliation
/

create sequence S_LayerStructure
/

create sequence S_LayerStructurePurpose
/

create sequence S_LinkAllocation
/

create sequence S_MDMPartsSalesCategoryMap
/

create sequence S_MaintenanceMandatory
/

create sequence S_MaintenancePart
/

create sequence S_MaintenanceReserve
/

create sequence S_MaintenanceVariety
/

create sequence S_Notification
/

create sequence S_NotificationLimit
/

create sequence S_NotificationOpLog
/

create sequence S_NotificationReply
/

create sequence S_Region
/

create sequence S_ReportDownloadMsg
/

create sequence S_ReserveFactorMasterOrder
/

create sequence S_ReserveFactorOrderDetail
/

create sequence S_ScheduleExportState
/

create sequence S_SmartCompany
/

create sequence S_SmartOrderCalendar
/

create sequence S_SmartProcessMethod
/

/*==============================================================*/
/* Table: CenterApproveBill                                     */
/*==============================================================*/
create table CenterApproveBill  (
   Id                   NUMBER(9)                       not null,
   CompanyId            NUMBER(9)                       not null,
   CompanyCode          VARCHAR2(50),
   CompanyName          VARCHAR2(100),
   MarketingDepartmentId NUMBER(9),
   ApproveWeek          VARCHAR2(50),
   Status               NUMBER(9),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   WarehouseId          NUMBER(9),
   WarehouseName        VARCHAR2(100),
   constraint PK_CENTERAPPROVEBILL primary key (Id)
)
/

/*==============================================================*/
/* Table: CenterAuthorizedProvince                              */
/*==============================================================*/
create table CenterAuthorizedProvince  (
   Id                   NUMBER(9)                       not null,
   AgencyId             NUMBER(9)                       not null,
   ProvinceId           NUMBER(9)                       not null,
   ProvinceName         VARCHAR2(100),
   constraint PK_CENTERAUTHORIZEDPROVINCE primary key (Id)
)
/

/*==============================================================*/
/* Table: CenterSaleReturnStrategy                              */
/*==============================================================*/
create table CenterSaleReturnStrategy  (
   Id                   NUMBER(9)                       not null,
   TimeFrom             NUMBER(9),
   TimeTo               NUMBER(9),
   ReturnType           NUMBER(9),
   DiscountRate         NUMBER(15,6),
   Status               NUMBER(9),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   constraint PK_CENTERSALERETURNSTRATEGY primary key (Id)
)
/

/*==============================================================*/
/* Table: CodeTemplate                                          */
/*==============================================================*/
create table CodeTemplate  (
   Id                   NUMBER(9)                       not null,
   Name                 VARCHAR2(100)                   not null,
   EntityName           VARCHAR2(100)                   not null,
   IsActived            NUMBER(1)                       not null,
   Template             VARCHAR2(200)                   not null,
   ResetType            NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   CorporationId        NUMBER(9),
   CorporationName      VARCHAR2(100),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_CODETEMPLATE primary key (Id)
)
/

/*==============================================================*/
/* Table: CodeTemplateSerial                                    */
/*==============================================================*/
create table CodeTemplateSerial  (
   Id                   NUMBER(9)                       not null,
   TemplateId           NUMBER(9)                       not null,
   CreateDate           DATE                            not null,
   CorpCode             VARCHAR2(50),
   Serial               NUMBER(9)                       not null,
   RowVersion           TIMESTAMP,
   constraint PK_CODETEMPLATESERIAL primary key (Id)
)
/

/*==============================================================*/
/* Table: CompanyDetail                                         */
/*==============================================================*/
create table CompanyDetail  (
   Id                   NUMBER(9)                       not null,
   NoticeId             NUMBER(9)                       not null,
   CompanyId            NUMBER(9)                       not null,
   Type                 NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   Name                 VARCHAR2(100)                   not null,
   constraint PK_COMPANYDETAIL primary key (Id)
)
/

/*==============================================================*/
/* Table: Countersignature                                      */
/*==============================================================*/
create table Countersignature  (
   Id                   NUMBER(9)                       not null,
   SourceId             NUMBER(9)                       not null,
   SourceCode           VARCHAR2(50)                    not null,
   SourceType           NUMBER(9)                       not null,
   Tpye                 NUMBER(9)                       not null,
   NextNumber           NUMBER(9)                       not null,
   Status               NUMBER(9)                       not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   constraint PK_COUNTERSIGNATURE primary key (Id)
)
/

/*==============================================================*/
/* Table: CountersignatureDetail                                */
/*==============================================================*/
create table CountersignatureDetail  (
   Id                   NUMBER(9)                       not null,
   SerialNumber         NUMBER(9)                       not null,
   CountersignatureId   NUMBER(9)                       not null,
   ApproverId           NUMBER(9)                       not null,
   Approver             VARCHAR2(100)                   not null,
   Status               NUMBER(9)                       not null,
   ApproverComment      VARCHAR2(200),
   constraint PK_COUNTERSIGNATUREDETAIL primary key (Id)
)
/

/*==============================================================*/
/* Table: DailySalesWeight                                      */
/*==============================================================*/
create table DailySalesWeight  (
   Id                   NUMBER(9)                       not null,
   WarehouseId          NUMBER(9),
   WarehouseCode        VARCHAR2(100),
   WarehouseName        VARCHAR2(100),
   Times                NUMBER(9),
   Weight               NUMBER(15,6),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   constraint PK_DAILYSALESWEIGHT primary key (Id)
)
/

/*==============================================================*/
/* Table: DataBaseLink                                          */
/*==============================================================*/
create table DataBaseLink  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(100)                   not null,
   Link                 VARCHAR2(500)                   not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_DATABASELINK primary key (Id)
)
/

/*==============================================================*/
/* Table: DataBaseLinkBranch                                    */
/*==============================================================*/
create table DataBaseLinkBranch  (
   Id                   NUMBER(9)                       not null,
   BranchCode           VARCHAR2(50)                    not null,
   DataBaseLinkCode     VARCHAR2(100)                   not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_DATABASELINKBRANCH primary key (Id)
)
/

/*==============================================================*/
/* Table: DealerReservesRecommended                             */
/*==============================================================*/
create table DealerReservesRecommended  (
   Id                   NUMBER(9)                       not null,
   Year                 NUMBER(9)                       not null,
   Quarter              NUMBER(9)                       not null,
   DealerId             NUMBER(9)                       not null,
   DealerCode           VARCHAR2(50)                    not null,
   DealerName           VARCHAR2(100)                   not null,
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50)                    not null,
   SparePartName        VARCHAR2(100)                   not null,
   PartType             NUMBER(9),
   QuarterlyDemand      NUMBER(9),
   DemandFrequency      NUMBER(9),
   Quantity             NUMBER(9),
   AdviseQuantity       NUMBER(9),
   CreateTime           DATE,
   DailyDemand          NUMBER(19,4),
   constraint PK_DEALERRESERVESRECOMMENDED primary key (Id)
)
/

/*==============================================================*/
/* Table: KeyValueItem                                          */
/*==============================================================*/
create table KeyValueItem  (
   Id                   NUMBER(9)                       not null,
   Category             VARCHAR2(50)                    not null,
   Name                 VARCHAR2(50)                    not null,
   Caption              VARCHAR2(100)                   not null,
   Key                  NUMBER(9)                       not null,
   Value                VARCHAR2(200)                   not null,
   IsBuiltIn            NUMBER(1)                       not null,
   Status               NUMBER(9)                       not null,
   RowVersion           TIMESTAMP,
   constraint PK_KEYVALUEITEM primary key (Id)
)
/

/*==============================================================*/
/* Table: LayerNodeType                                         */
/*==============================================================*/
create table LayerNodeType  (
   Id                   NUMBER(9)                       not null,
   LayerStructureId     NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   Name                 VARCHAR2(100)                   not null,
   EndNode              NUMBER(1)                       not null,
   TopLevelNode         NUMBER(1)                       not null,
   constraint PK_LAYERNODETYPE primary key (Id)
)
/

/*==============================================================*/
/* Table: LayerNodeTypeAffiliation                              */
/*==============================================================*/
create table LayerNodeTypeAffiliation  (
   Id                   NUMBER(9)                       not null,
   LayerStructureId     NUMBER(9)                       not null,
   ParentLayerNodeTypeId NUMBER(9)                       not null,
   SubLayerNodeTypeId   NUMBER(9)                       not null,
   constraint PK_LAYERNODETYPEAFFILIATION primary key (Id)
)
/

/*==============================================================*/
/* Table: LayerStructure                                        */
/*==============================================================*/
create table LayerStructure  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   Purpose              VARCHAR2(100)                   not null,
   MaxLayerNumber       NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   constraint PK_LAYERSTRUCTURE primary key (Id)
)
/

/*==============================================================*/
/* Table: LayerStructurePurpose                                 */
/*==============================================================*/
create table LayerStructurePurpose  (
   Id                   NUMBER(9)                       not null,
   PurposeDescription   VARCHAR2(100)                   not null,
   LayerStructureId     NUMBER(9)                       not null,
   RootNodeId           NUMBER(9)                       not null,
   BoName               VARCHAR2(50)                    not null,
   PurposeType          NUMBER(9)                       not null,
   constraint PK_LAYERSTRUCTUREPURPOSE primary key (Id)
)
/

/*==============================================================*/
/* Table: LinkAllocation                                        */
/*==============================================================*/
create table LinkAllocation  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(100)                   not null,
   Link                 VARCHAR2(2000)                  not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_LINKALLOCATION primary key (Id)
)
/

/*==============================================================*/
/* Table: MDMPartsSalesCategoryMap                              */
/*==============================================================*/
create table MDMPartsSalesCategoryMap  (
   Id                   NUMBER(9)                       not null,
   MDMPartsSalesCategory VARCHAR2(100)                   not null,
   PMSPartsSalesCategoryCode VARCHAR2(100)                   not null,
   PMSPartsSalesCategoryName VARCHAR2(100)                   not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_MDMPARTSSALESCATEGORYMAP primary key (Id)
)
/

/*==============================================================*/
/* Table: MaintenanceMandatory                                  */
/*==============================================================*/
create table MaintenanceMandatory  (
   Id                   NUMBER(9)                       not null,
   Lower                NUMBER(19,4)                    not null,
   Higher               NUMBER(19,4)                    not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   MaxDays              NUMBER(9),
   constraint PK_MAINTENANCEMANDATORY primary key (Id)
)
/

/*==============================================================*/
/* Table: MaintenancePart                                       */
/*==============================================================*/
create table MaintenancePart  (
   Id                   NUMBER(9)                       not null,
   Price                NUMBER(9),
   MaterialCost         NUMBER(9)                       not null,
   IsSave               NUMBER(1),
   Status               NUMBER(9)                       not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   constraint PK_MAINTENANCEPART primary key (Id)
)
/

/*==============================================================*/
/* Table: MaintenanceReserve                                    */
/*==============================================================*/
create table MaintenanceReserve  (
   Id                   NUMBER(9)                       not null,
   Type                 NUMBER(9)                       not null,
   Amount               NUMBER(9)                       not null,
   Status               NUMBER(9)                       not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   constraint PK_MAINTENANCERESERVE primary key (Id)
)
/

/*==============================================================*/
/* Table: MaintenanceVariety                                    */
/*==============================================================*/
create table MaintenanceVariety  (
   Id                   NUMBER(9)                       not null,
   MaterialCost         NUMBER(9)                       not null,
   StoreType            NUMBER(9)                       not null,
   ReserveRatio         NUMBER(19,4)                    not null,
   Status               NUMBER(9)                       not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   Format               VARCHAR2(200),
   constraint PK_MAINTENANCEVARIETY primary key (Id)
)
/

/*==============================================================*/
/* Table: Notification                                          */
/*==============================================================*/
create table Notification  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   CompanyId            NUMBER(9)                       not null,
   CompanyCode          VARCHAR2(50)                    not null,
   CompanyName          VARCHAR2(100)                   not null,
   CompanyType          NUMBER(9)                       not null,
   Type                 NUMBER(9)                       not null,
   Title                VARCHAR2(100)                   not null,
   Content              VARCHAR2(1000),
   Status               NUMBER(9)                       not null,
   Top                  NUMBER(1),
   TopModifierId        NUMBER(9),
   TopModifierName      VARCHAR2(100),
   TopTime              DATE,
   PublishDep           VARCHAR2(100)                   not null,
   UrgentLevel          NUMBER(9)                       not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   Path                 VARCHAR2(2000),
   RowVersion           TIMESTAMP,
   constraint PK_NOTIFICATION primary key (Id)
)
/

/*==============================================================*/
/* Table: NotificationLimit                                     */
/*==============================================================*/
create table NotificationLimit  (
   Id                   NUMBER(9)                       not null,
   NoticeId             NUMBER(9)                       not null,
   DesCompanyTpye       NUMBER(9)                       not null,
   constraint PK_NOTIFICATIONLIMIT primary key (Id)
)
/

/*==============================================================*/
/* Table: NotificationOpLog                                     */
/*==============================================================*/
create table NotificationOpLog  (
   Id                   NUMBER(9)                       not null,
   NotificationID       NUMBER(9)                       not null,
   OperationType        NUMBER(9)                       not null,
   CustomerCompanyId    NUMBER(9)                       not null,
   CustomerCompanyCode  VARCHAR2(50)                    not null,
   CustomerCompanyName  VARCHAR2(100)                   not null,
   Status               NUMBER(9)                       not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_NOTIFICATIONOPLOG primary key (Id)
)
/

/*==============================================================*/
/* Table: NotificationReply                                     */
/*==============================================================*/
create table NotificationReply  (
   id                   NUMBER(9)                       not null,
   NotificationId       NUMBER(9),
   NotificationCode     VARCHAR2(50),
   CompanyId            NUMBER(9),
   CompanyCode          VARCHAR2(100),
   CompanyName          VARCHAR2(200),
   ReplyChannel         NUMBER(9),
   ReplyContent         VARCHAR2(2000),
   ReplyFilePath        VARCHAR2(4000),
   ReplyDate            DATE,
   constraint PK_NOTIFICATIONREPLY primary key (id)
)
/

/*==============================================================*/
/* Table: Personnel                                             */
/*==============================================================*/
create table Personnel  (
   Id                   NUMBER(9)                       not null,
   LoginId              VARCHAR2(20)                    not null,
   Name                 VARCHAR2(50)                    not null,
   Status               NUMBER(9)                       not null,
   CellNumber           VARCHAR2(20),
   Remark               VARCHAR2(200),
   CorporationId        NUMBER(9),
   CorporationName      VARCHAR2(100),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_PERSONNEL primary key (Id)
)
/

/*==============================================================*/
/* Table: Region                                                */
/*==============================================================*/
create table Region  (
   Id                   NUMBER(9)                       not null,
   ParentId             NUMBER(9)                       not null,
   Type                 NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   Name                 VARCHAR2(100)                   not null,
   SARNumber            VARCHAR2(20),
   DDD                  VARCHAR2(50),
   PostCode             VARCHAR2(10),
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   EnterpriseId         NUMBER(9),
   CorporationName      VARCHAR2(100),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_REGION primary key (Id)
)
/

/*==============================================================*/
/* Table: ReportDownloadMsg                                     */
/*==============================================================*/
create table ReportDownloadMsg  (
   Id                   NUMBER(9)                       not null,
   CompanyId            NUMBER(9)                       not null,
   CompanyCode          VARCHAR2(50)                    not null,
   CompanyName          VARCHAR2(100)                   not null,
   NodeName             VARCHAR2(100)                   not null,
   NodeId               NUMBER(9)                       not null,
   QueryCriteria        VARCHAR2(1000),
   FilePath             VARCHAR2(2000),
   Status               NUMBER(9)                       not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_REPORTDOWNLOADMSG primary key (Id)
)
/

/*==============================================================*/
/* Table: ReserveFactorMasterOrder                              */
/*==============================================================*/
create table ReserveFactorMasterOrder  (
   Id                   NUMBER(9)                       not null,
   WarehouseId          NUMBER(9),
   WarehouseCode        VARCHAR2(100),
   WarehouseName        VARCHAR2(100),
   ABCStrategyId        NUMBER(9),
   ABCStrategy          VARCHAR2(50),
   ReserveCoefficient   NUMBER(9),
   Status               NUMBER(9),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   LowerLimitDays       NUMBER(9),
   constraint PK_RESERVEFACTORMASTERORDER primary key (Id)
)
/

/*==============================================================*/
/* Table: ReserveFactorOrderDetail                              */
/*==============================================================*/
create table ReserveFactorOrderDetail  (
   Id                   NUMBER(9)                       not null,
   ReserveFactorMasterOrderId NUMBER(9)                       not null,
   PriceCap             NUMBER(19,4),
   PriceFloor           NUMBER(19,4),
   UpperLimitCoefficient NUMBER(19,4),
   LowerLimitCoefficient NUMBER(19,4),
   constraint PK_RESERVEFACTORORDERDETAIL primary key (Id)
)
/

/*==============================================================*/
/* Table: ScheduleExportState                                   */
/*==============================================================*/
create table ScheduleExportState  (
   Id                   NUMBER(9)                       not null,
   JobName              VARCHAR2(200)                   not null,
   Category             VARCHAR2(50)                    not null,
   ScheduleTime         DATE                            not null,
   FilePath             VARCHAR2(200),
   Remark               VARCHAR2(200),
   EnterpriseId         NUMBER(9)                       not null,
   CreatorId            NUMBER(9)                       not null,
   CreateTime           DATE                            not null,
   ModifierId           NUMBER(9),
   ModifyTime           DATE,
   constraint PK_SCHEDULEEXPORTSTATE primary key (Id)
)
/

/*==============================================================*/
/* Table: SmartCompany                                          */
/*==============================================================*/
create table SmartCompany  (
   Id                   NUMBER(9)                       not null,
   CompanyId            NUMBER(9),
   CompanyCode          VARCHAR2(50),
   CompanyName          VARCHAR2(100),
   Status               NUMBER(9),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   constraint PK_SMARTCOMPANY primary key (Id)
)
/

/*==============================================================*/
/* Table: SmartOrderCalendar                                    */
/*==============================================================*/
create table SmartOrderCalendar  (
   Id                   NUMBER(9)                       not null,
   WarehouseId          NUMBER(9),
   WarehouseCode        VARCHAR2(100),
   WarehouseName        VARCHAR2(100),
   Times                DATE,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   Status               NUMBER(9),
   constraint PK_SMARTORDERCALENDAR primary key (Id)
)
/

/*==============================================================*/
/* Table: SmartProcessMethod                                    */
/*==============================================================*/
create table SmartProcessMethod  (
   Id                   NUMBER(9)                       not null,
   WarehouseId          NUMBER(9),
   WarehouseCode        VARCHAR2(100),
   WarehouseName        VARCHAR2(100),
   OrderProcessMethod   NUMBER(9),
   Status               NUMBER(9),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   constraint PK_SMARTPROCESSMETHOD primary key (Id)
)
/

/*==============================================================*/
/* Table: WarehouseHoliday                                      */
/*==============================================================*/
create table WarehouseHoliday  (
   Id                   NUMBER(9)                       not null,
   WarehouseId          NUMBER(9),
   WarehouseCode        VARCHAR2(100),
   WarehouseName        VARCHAR2(100),
   StartsDate           DATE,
   EndDates             DATE,
   Status               NUMBER(9),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   constraint PK_WAREHOUSEHOLIDAY primary key (Id)
)
/

