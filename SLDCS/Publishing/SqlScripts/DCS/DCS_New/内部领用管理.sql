/*==============================================================*/
/* DBMS name:      ORACLE Version 10gR2                         */
/* Created on:     2020-11-02 09:43:12                          */
/*==============================================================*/


declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('DepartmentBrandRelation');
  if num>0 then
    execute immediate 'drop table DepartmentBrandRelation cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('DepartmentInformation');
  if num>0 then
    execute immediate 'drop table DepartmentInformation cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('InternalAcquisitionBill');
  if num>0 then
    execute immediate 'drop table InternalAcquisitionBill cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('InternalAcquisitionDetail');
  if num>0 then
    execute immediate 'drop table InternalAcquisitionDetail cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('InternalAllocationBill');
  if num>0 then
    execute immediate 'drop table InternalAllocationBill cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('InternalAllocationDetail');
  if num>0 then
    execute immediate 'drop table InternalAllocationDetail cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsSupplierCollection');
  if num>0 then
    execute immediate 'drop table PartsSupplierCollection cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('SupplierCollectionDetail');
  if num>0 then
    execute immediate 'drop table SupplierCollectionDetail cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_DepartmentBrandRelation');
  if num>0 then
    execute immediate 'drop sequence S_DepartmentBrandRelation';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_DepartmentInformation');
  if num>0 then
    execute immediate 'drop sequence S_DepartmentInformation';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_InternalAcquisitionBill');
  if num>0 then
    execute immediate 'drop sequence S_InternalAcquisitionBill';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_InternalAcquisitionDetail');
  if num>0 then
    execute immediate 'drop sequence S_InternalAcquisitionDetail';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_InternalAllocationBill');
  if num>0 then
    execute immediate 'drop sequence S_InternalAllocationBill';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_InternalAllocationDetail');
  if num>0 then
    execute immediate 'drop sequence S_InternalAllocationDetail';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsSupplierCollection');
  if num>0 then
    execute immediate 'drop sequence S_PartsSupplierCollection';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_SupplierCollectionDetail');
  if num>0 then
    execute immediate 'drop sequence S_SupplierCollectionDetail';
  end if;
end;
/

create sequence S_DepartmentBrandRelation
/

create sequence S_DepartmentInformation
/

create sequence S_InternalAcquisitionBill
/

create sequence S_InternalAcquisitionDetail
/

create sequence S_InternalAllocationBill
/

create sequence S_InternalAllocationDetail
/

create sequence S_PartsSupplierCollection
/

create sequence S_SupplierCollectionDetail
/

/*==============================================================*/
/* Table: DepartmentBrandRelation                               */
/*==============================================================*/
create table DepartmentBrandRelation  (
   Id                   NUMBER(9)                       not null,
   DepartmentId         NUMBER(9)                       not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   Branchid             NUMBER(9)                       not null,
   constraint PK_DEPARTMENTBRANDRELATION primary key (Id)
)
/

/*==============================================================*/
/* Table: DepartmentInformation                                 */
/*==============================================================*/
create table DepartmentInformation  (
   Id                   NUMBER(9)                       not null,
   BranchId             NUMBER(9)                       not null,
   BranchName           VARCHAR2(100)                   not null,
   Code                 VARCHAR2(50)                    not null,
   Name                 VARCHAR2(100)                   not null,
   Responsible          VARCHAR2(100),
   ContactPhone         VARCHAR2(50),
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   constraint PK_DEPARTMENTINFORMATION primary key (Id)
)
/

/*==============================================================*/
/* Table: InternalAcquisitionBill                               */
/*==============================================================*/
create table InternalAcquisitionBill  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   WarehouseId          NUMBER(9)                       not null,
   WarehouseName        VARCHAR2(100)                   not null,
   BranchId             NUMBER(9)                       not null,
   BranchName           VARCHAR2(100),
   DepartmentId         NUMBER(9)                       not null,
   DepartmentName       VARCHAR2(100)                   not null,
   Reason               VARCHAR2(200),
   Operator             VARCHAR2(100),
   TotalAmount          NUMBER(19,4)                    not null,
   InStatus             NUMBER(9)                       not null,
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   ApproverId           NUMBER(9),
   ApproverName         VARCHAR2(100),
   ApproveTime          DATE,
   RowVersion           TIMESTAMP,
   PartsPurchaseOrderTypeId NUMBER(9),
   RequestedDeliveryTime DATE,
   Objid                VARCHAR2(40)                    not null,
   constraint PK_INTERNALACQUISITIONBILL primary key (Id)
)
/

/*==============================================================*/
/* Table: InternalAcquisitionDetail                             */
/*==============================================================*/
create table InternalAcquisitionDetail  (
   Id                   NUMBER(9)                       not null,
   SerialNumber         NUMBER(9)                       not null,
   InternalAcquisitionBillId NUMBER(9)                       not null,
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50)                    not null,
   SparePartName        VARCHAR2(100)                   not null,
   MeasureUnit          VARCHAR2(50),
   SalesPrice           NUMBER(19,4),
   UnitPrice            NUMBER(19,4)                    not null,
   Quantity             NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   constraint PK_INTERNALACQUISITIONDETAIL primary key (Id)
)
/

/*==============================================================*/
/* Table: InternalAllocationBill                                */
/*==============================================================*/
create table InternalAllocationBill  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   BranchId             NUMBER(9)                       not null,
   BranchName           VARCHAR2(100)                   not null,
   WarehouseId          NUMBER(9)                       not null,
   WarehouseName        VARCHAR2(100)                   not null,
   DepartmentId         NUMBER(9)                       not null,
   DepartmentName       VARCHAR2(100)                   not null,
   Reason               VARCHAR2(200),
   Operator             VARCHAR2(100),
   TotalAmount          NUMBER(19,4)                    not null,
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   ApproverId           NUMBER(9),
   ApproverName         VARCHAR2(100),
   ApproveTime          DATE,
   RowVersion           TIMESTAMP,
   Objid                VARCHAR2(40)                    not null,
   Type                 NUMBER(9),
   SourceCode           VARCHAR2(50),
   CheckerId            NUMBER(9),
   CheckerName          VARCHAR2(100),
   CheckTime            DATE,
   ReceiverName         VARCHAR2(100),
   ReceiveAddress       VARCHAR2(300),
   LinkPhone            VARCHAR2(100),
   PartsSupplierId      NUMBER(9),
   PartsSupplierCode    VARCHAR2(50),
   PartsSupplierName    VARCHAR2(100),
   Path                 VARCHAR2(2000),
   constraint PK_INTERNALALLOCATIONBILL primary key (Id)
)
/

/*==============================================================*/
/* Table: InternalAllocationDetail                              */
/*==============================================================*/
create table InternalAllocationDetail  (
   Id                   NUMBER(9)                       not null,
   SerialNumber         NUMBER(9)                       not null,
   InternalAllocationBillId NUMBER(9)                       not null,
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50)                    not null,
   SparePartName        VARCHAR2(100)                   not null,
   MeasureUnit          VARCHAR2(50),
   SalesPrice           NUMBER(19,4),
   UnitPrice            NUMBER(19,4)                    not null,
   Quantity             NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   constraint PK_INTERNALALLOCATIONDETAIL primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsSupplierCollection                               */
/*==============================================================*/
create table PartsSupplierCollection  (
   Id                   NUMBER(9)                       not null,
   Status               NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   BranchId             NUMBER(9)                       not null,
   BranchName           VARCHAR2(100)                   not null,
   WarehouseId          NUMBER(9)                       not null,
   WarehouseName        VARCHAR2(100)                   not null,
   PartsSupplierId      NUMBER(9),
   PartsSupplierCode    VARCHAR2(50),
   PartsSupplierName    VARCHAR2(100),
   Reason               VARCHAR2(200),
   Operator             VARCHAR2(200),
   ReceiverName         VARCHAR2(100),
   ReceiveAddress       VARCHAR2(300),
   LinkPhone            VARCHAR2(100),
   IsAuto               NUMBER(1),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   SubmitterId          NUMBER(9),
   SubmitterName        VARCHAR2(100),
   SubmitTime           DATE,
   ApproverId           NUMBER(9),
   ApproverName         VARCHAR2(100),
   ApproveTime          DATE,
   CheckerId            NUMBER(9),
   CheckerName          VARCHAR2(100),
   CheckTime            DATE,
   CloserId             NUMBER(9),
   CloserName           VARCHAR2(100),
   CloseTime            DATE,
   CheckMemo            VARCHAR2(200),
   RowVersion           TIMESTAMP,
   constraint PK_PARTSSUPPLIERCOLLECTION primary key (Id)
)
/

/*==============================================================*/
/* Table: SupplierCollectionDetail                              */
/*==============================================================*/
create table SupplierCollectionDetail  (
   Id                   NUMBER(9)                       not null,
   SupplierCollectionId NUMBER(9)                       not null,
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50)                    not null,
   SparePartName        VARCHAR2(100)                   not null,
   MeasureUnit          VARCHAR2(50),
   Quantity             NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   constraint PK_SUPPLIERCOLLECTIONDETAIL primary key (Id)
)
/

