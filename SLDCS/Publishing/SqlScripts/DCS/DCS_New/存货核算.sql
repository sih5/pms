/*==============================================================*/
/* DBMS name:      ORACLE Version 10gR2                         */
/* Created on:     2017/2/15 15:31:55                           */
/*==============================================================*/


declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('EnterprisePartsCost');
  if num>0 then
    execute immediate 'drop table EnterprisePartsCost cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('FinancialSnapshotSet');
  if num>0 then
    execute immediate 'drop table FinancialSnapshotSet cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('LossOverflowClaimApp');
  if num>0 then
    execute immediate 'drop table LossOverflowClaimApp cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('LossOverflowClaimAppDetail');
  if num>0 then
    execute immediate 'drop table LossOverflowClaimAppDetail cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsCostTransferInTransit');
  if num>0 then
    execute immediate 'drop table PartsCostTransferInTransit cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsHistoryStock');
  if num>0 then
    execute immediate 'drop table PartsHistoryStock cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsPlannedPrice');
  if num>0 then
    execute immediate 'drop table PartsPlannedPrice cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsWarehousePendingCost');
  if num>0 then
    execute immediate 'drop table PartsWarehousePendingCost cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PlannedPriceApp');
  if num>0 then
    execute immediate 'drop table PlannedPriceApp cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PlannedPriceAppDetail');
  if num>0 then
    execute immediate 'drop table PlannedPriceAppDetail cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('WarehouseCostChangeBill');
  if num>0 then
    execute immediate 'drop table WarehouseCostChangeBill cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('WarehouseCostChangeDetail');
  if num>0 then
    execute immediate 'drop table WarehouseCostChangeDetail cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_EnterprisePartsCost');
  if num>0 then
    execute immediate 'drop sequence S_EnterprisePartsCost';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_FinancialSnapshotSet');
  if num>0 then
    execute immediate 'drop sequence S_FinancialSnapshotSet';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_LossOverflowClaimApp');
  if num>0 then
    execute immediate 'drop sequence S_LossOverflowClaimApp';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_LossOverflowClaimAppDetail');
  if num>0 then
    execute immediate 'drop sequence S_LossOverflowClaimAppDetail';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsCostTransferInTransit');
  if num>0 then
    execute immediate 'drop sequence S_PartsCostTransferInTransit';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsHistoryStock');
  if num>0 then
    execute immediate 'drop sequence S_PartsHistoryStock';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsPlannedPrice');
  if num>0 then
    execute immediate 'drop sequence S_PartsPlannedPrice';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsWarehousePendingCost');
  if num>0 then
    execute immediate 'drop sequence S_PartsWarehousePendingCost';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PlannedPriceApp');
  if num>0 then
    execute immediate 'drop sequence S_PlannedPriceApp';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PlannedPriceAppDetail');
  if num>0 then
    execute immediate 'drop sequence S_PlannedPriceAppDetail';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_WarehouseCostChangeBill');
  if num>0 then
    execute immediate 'drop sequence S_WarehouseCostChangeBill';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_WarehouseCostChangeDetail');
  if num>0 then
    execute immediate 'drop sequence S_WarehouseCostChangeDetail';
  end if;
end;
/

create sequence S_EnterprisePartsCost
/

create sequence S_FinancialSnapshotSet
/

create sequence S_LossOverflowClaimApp
/

create sequence S_LossOverflowClaimAppDetail
/

create sequence S_PartsCostTransferInTransit
/

create sequence S_PartsHistoryStock
/

create sequence S_PartsPlannedPrice
/

create sequence S_PartsWarehousePendingCost
/

create sequence S_PlannedPriceApp
/

create sequence S_PlannedPriceAppDetail
/

create sequence S_WarehouseCostChangeBill
/

create sequence S_WarehouseCostChangeDetail
/

/*==============================================================*/
/* Table: EnterprisePartsCost                                   */
/*==============================================================*/
create table EnterprisePartsCost  (
   Id                   NUMBER(9)                       not null,
   OwnerCompanyId       NUMBER(9)                       not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   SparePartId          NUMBER(9)                       not null,
   Quantity             NUMBER(9)                       not null,
   CostAmount           NUMBER(19,4)                    not null,
   CostPrice            NUMBER(19,4)                    not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   constraint PK_ENTERPRISEPARTSCOST primary key (Id)
)
/

/*==============================================================*/
/* Table: FinancialSnapshotSet                                  */
/*==============================================================*/
create table FinancialSnapshotSet  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50),
   SnapshotTime         DATE,
   BranchId             NUMBER(9),
   PartsSalesCategoryId NUMBER(9),
   PartsSalesCategoryName VARCHAR2(50),
   Status               NUMBER(9),
   Memo                 VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonerTime        DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_FINANCIALSNAPSHOTSET primary key (Id)
)
/

/*==============================================================*/
/* Table: LossOverflowClaimApp                                  */
/*==============================================================*/
create table LossOverflowClaimApp  (
   Id                   NUMBER(9)                       not null,
   OwnerCompanyId       NUMBER(9)                       not null,
   OwnerCompanyCode     VARCHAR2(50)                    not null,
   OwnerCompanyName     VARCHAR2(100)                   not null,
   Code                 VARCHAR2(50)                    not null,
   LossOverFlag         NUMBER(9)                       not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   PartsSalesCategoryName VARCHAR2(50)                    not null,
   WarehouseId          NUMBER(9)                       not null,
   WarehouseCode        VARCHAR2(50)                    not null,
   Amount               NUMBER(19,4)                    not null,
   Memo                 VARCHAR2(200),
   Operator             VARCHAR2(100),
   Status               NUMBER(9)                       not null,
   AccountingStatus     NUMBER(9)                       not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_LOSSOVERFLOWCLAIMAPP primary key (Id)
)
/

/*==============================================================*/
/* Table: LossOverflowClaimAppDetail                            */
/*==============================================================*/
create table LossOverflowClaimAppDetail  (
   Id                   NUMBER(9)                       not null,
   LossOverflowClaimAppId NUMBER(9)                       not null,
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50)                    not null,
   SparePartName        VARCHAR2(100)                   not null,
   Quantity             NUMBER(9)                       not null,
   CostPrice            NUMBER(19,4)                    not null,
   Amount               NUMBER(19,4)                    not null,
   constraint PK_LOSSOVERFLOWCLAIMAPPDETAIL primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsCostTransferInTransit                            */
/*==============================================================*/
create table PartsCostTransferInTransit  (
   Id                   NUMBER(9)                       not null,
   OwnerCompanyId       NUMBER(9)                       not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   SparePartId          NUMBER(9)                       not null,
   Quantity             NUMBER(9)                       not null,
   CostAmount           NUMBER(19,4)                    not null,
   CostPrice            NUMBER(19,4)                    not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   constraint PK_PARTSCOSTTRANSFERINTRANSIT primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsHistoryStock                                     */
/*==============================================================*/
create table PartsHistoryStock  (
   Id                   NUMBER(9)                       not null,
   WarehouseId          NUMBER(9)                       not null,
   StorageCompanyId     NUMBER(9)                       not null,
   PartsSalesCategoryId NUMBER(9),
   StorageCompanyType   NUMBER(9)                       not null,
   BranchId             NUMBER(9)                       not null,
   WarehouseAreaId      NUMBER(9)                       not null,
   WarehouseAreaCategoryId NUMBER(9),
   PartId               NUMBER(9)                       not null,
   Quantity             NUMBER(9)                       not null,
   PlannedPrice         NUMBER(19,4),
   PlannedPriceAmount   NUMBER(19,4),
   SalePrice            NUMBER(19,4),
   SaleAmount           NUMBER(19,4),
   Remark               VARCHAR2(200),
   SnapshoTime          DATE,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   constraint PK_PARTSHISTORYSTOCK primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsPlannedPrice                                     */
/*==============================================================*/
create table PartsPlannedPrice  (
   Id                   NUMBER(9)                       not null,
   OwnerCompanyId       NUMBER(9)                       not null,
   OwnerCompanyType     NUMBER(9)                       not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   PartsSalesCategoryName VARCHAR2(50)                    not null,
   SparePartId          NUMBER(9)                       not null,
   PlannedPrice         NUMBER(19,4)                    not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_PARTSPLANNEDPRICE primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsWarehousePendingCost                             */
/*==============================================================*/
create table PartsWarehousePendingCost  (
   Id                   NUMBER(9)                       not null,
   OwnerCompanyId       NUMBER(9)                       not null,
   WarehouseId          NUMBER(9)                       not null,
   SparePartId          NUMBER(9)                       not null,
   Quantity             NUMBER(9)                       not null,
   CostPrice            NUMBER(19,4)                    not null,
   constraint PK_PARTSWAREHOUSEPENDINGCOST primary key (Id)
)
/

/*==============================================================*/
/* Table: PlannedPriceApp                                       */
/*==============================================================*/
create table PlannedPriceApp  (
   Id                   NUMBER(9)                       not null,
   OwnerCompanyId       NUMBER(9)                       not null,
   OwnerCompanyCode     VARCHAR2(50)                    not null,
   OwnerCompanyName     VARCHAR2(100)                   not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   PartsSalesCategoryName VARCHAR2(50),
   Code                 VARCHAR2(50)                    not null,
   AmountBeforeChange   NUMBER(19,4),
   AmountAfterChange    NUMBER(19,4),
   AmountDifference     NUMBER(19,4),
   Status               NUMBER(9)                       not null,
   RecordStatus         NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   PlannedExecutionTime DATE                            not null,
   ActualExecutionTime  DATE,
   RowVersion           TIMESTAMP,
   constraint PK_PLANNEDPRICEAPP primary key (Id)
)
/

/*==============================================================*/
/* Table: PlannedPriceAppDetail                                 */
/*==============================================================*/
create table PlannedPriceAppDetail  (
   Id                   NUMBER(9)                       not null,
   PlannedPriceAppId    NUMBER(9)                       not null,
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50)                    not null,
   SparePartName        VARCHAR2(100)                   not null,
   RequestedPrice       NUMBER(19,4)                    not null,
   Quantity             NUMBER(9)                       not null,
   PriceBeforeChange    NUMBER(19,4),
   AmountDifference     NUMBER(19,4),
   PriceFluctuationRatio NUMBER(15,6),
   constraint PK_PLANNEDPRICEAPPDETAIL primary key (Id)
)
/

/*==============================================================*/
/* Table: WarehouseCostChangeBill                               */
/*==============================================================*/
create table WarehouseCostChangeBill  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   PlannedPriceAppId    NUMBER(9)                       not null,
   OwnerCompanyId       NUMBER(9)                       not null,
   OwnerCompanyCode     VARCHAR2(50)                    not null,
   OwnerCompanyName     VARCHAR2(100)                   not null,
   WarehouseId          NUMBER(9)                       not null,
   WarehouseCode        VARCHAR2(50)                    not null,
   WarehouseName        VARCHAR2(100)                   not null,
   AmountBeforeChange   NUMBER(19,4)                    not null,
   AmountAfterChange    NUMBER(19,4)                    not null,
   AmountDifference     NUMBER(19,4)                    not null,
   RecordStatus         NUMBER(9)                       not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_WAREHOUSECOSTCHANGEBILL primary key (Id)
)
/

/*==============================================================*/
/* Table: WarehouseCostChangeDetail                             */
/*==============================================================*/
create table WarehouseCostChangeDetail  (
   Id                   NUMBER(9)                       not null,
   WarehouseCostChangeBillId NUMBER(9)                       not null,
   WarehouseId          NUMBER(9)                       not null,
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50)                    not null,
   SparePartName        VARCHAR2(100)                   not null,
   PriceBeforeChange    NUMBER(19,4)                    not null,
   Quantity             NUMBER(9)                       not null,
   PriceAfterChange     NUMBER(19,4)                    not null,
   AmountDifference     NUMBER(19,4)                    not null,
   constraint PK_WAREHOUSECOSTCHANGEDETAIL primary key (Id)
)
/

