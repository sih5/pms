/*==============================================================*/
/* DBMS name:      ORACLE Version 10gR2                         */
/* Created on:     2014/12/17 20:36:06                          */
/*==============================================================*/


declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('SsUsedPartsDisposalBill');
  if num>0 then
    execute immediate 'drop table SsUsedPartsDisposalBill cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('SsUsedPartsDisposalDetail');
  if num>0 then
    execute immediate 'drop table SsUsedPartsDisposalDetail cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('SsUsedPartsStorage');
  if num>0 then
    execute immediate 'drop table SsUsedPartsStorage cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('UsedPartsDistanceInfor');
  if num>0 then
    execute immediate 'drop table UsedPartsDistanceInfor cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('UsedPartsLogisticLossBill');
  if num>0 then
    execute immediate 'drop table UsedPartsLogisticLossBill cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('UsedPartsLogisticLossDetail');
  if num>0 then
    execute immediate 'drop table UsedPartsLogisticLossDetail cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('UsedPartsReturnPolicyHistory');
  if num>0 then
    execute immediate 'drop table UsedPartsReturnPolicyHistory cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('UsedPartsShippingDetail');
  if num>0 then
    execute immediate 'drop table UsedPartsShippingDetail cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('UsedPartsShippingOrder');
  if num>0 then
    execute immediate 'drop table UsedPartsShippingOrder cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_SsUsedPartsDisposalBill');
  if num>0 then
    execute immediate 'drop sequence S_SsUsedPartsDisposalBill';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_SsUsedPartsDisposalDetail');
  if num>0 then
    execute immediate 'drop sequence S_SsUsedPartsDisposalDetail';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_SsUsedPartsStorage');
  if num>0 then
    execute immediate 'drop sequence S_SsUsedPartsStorage';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_UsedPartsDistanceInfor');
  if num>0 then
    execute immediate 'drop sequence S_UsedPartsDistanceInfor';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_UsedPartsLogisticLossBill');
  if num>0 then
    execute immediate 'drop sequence S_UsedPartsLogisticLossBill';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_UsedPartsLogisticLossDetail');
  if num>0 then
    execute immediate 'drop sequence S_UsedPartsLogisticLossDetail';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_UsedPartsReturnPolicyHistory');
  if num>0 then
    execute immediate 'drop sequence S_UsedPartsReturnPolicyHistory';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_UsedPartsShippingDetail');
  if num>0 then
    execute immediate 'drop sequence S_UsedPartsShippingDetail';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_UsedPartsShippingOrder');
  if num>0 then
    execute immediate 'drop sequence S_UsedPartsShippingOrder';
  end if;
end;
/

create sequence S_SsUsedPartsDisposalBill
/

create sequence S_SsUsedPartsDisposalDetail
/

create sequence S_SsUsedPartsStorage
/

create sequence S_UsedPartsDistanceInfor
/

create sequence S_UsedPartsLogisticLossBill
/

create sequence S_UsedPartsLogisticLossDetail
/

create sequence S_UsedPartsReturnPolicyHistory
/

create sequence S_UsedPartsShippingDetail
/

create sequence S_UsedPartsShippingOrder
/

/*==============================================================*/
/* Table: SsUsedPartsDisposalBill                               */
/*==============================================================*/
create table SsUsedPartsDisposalBill  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(100)                   not null,
   DealerId             NUMBER(9)                       not null,
   DealerCode           VARCHAR2(50)                    not null,
   DealerName           VARCHAR2(100)                   not null,
   BranchId             NUMBER(9)                       not null,
   BranchName           VARCHAR2(100)                   not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   UsedPartsDisposalMethod NUMBER(9)                       not null,
   ProcessTime          DATE,
   TotalAmount          NUMBER(19,4)                    not null,
   ProcessAmount        NUMBER(19,4),
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   ApprovalComment      VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   ApproverId           NUMBER(9),
   ApproverName         VARCHAR2(100),
   ApproveTime          DATE,
   RowVersion           TIMESTAMP,
   constraint PK_SSUSEDPARTSDISPOSALBILL primary key (Id)
)
/

/*==============================================================*/
/* Table: SsUsedPartsDisposalDetail                             */
/*==============================================================*/
create table SsUsedPartsDisposalDetail  (
   Id                   NUMBER(9)                       not null,
   SerialNumber         NUMBER(9)                       not null,
   BranchId             NUMBER(9)                       not null,
   BranchName           VARCHAR2(100)                   not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   SsUsedPartsDisposalBillId NUMBER(9)                       not null,
   ClaimBillId          NUMBER(9)                       not null,
   ClaimBillCode        VARCHAR2(50)                    not null,
   ClaimBillCreateTime  DATE,
   ClaimBillType        NUMBER(9)                       not null,
   UsedPartsBarCode     VARCHAR2(50)                    not null,
   UsedPartsId          NUMBER(9)                       not null,
   UsedPartsCode        VARCHAR2(50)                    not null,
   UsedPartsName        VARCHAR2(100)                   not null,
   UsedPartsSerialNumber VARCHAR2(50),
   UsedPartsBatchNumber VARCHAR2(50),
   UsedPartsSupplierId  NUMBER(9)                       not null,
   UsedPartsSupplierCode VARCHAR2(50)                    not null,
   UsedPartsSupplierName VARCHAR2(100)                   not null,
   UsedPartsReturnPolicy NUMBER(9)                       not null,
   Quantity             NUMBER(9)                       not null,
   UnitPrice            NUMBER(19,4)                    not null,
   PartsManagementCost  NUMBER(19,4)                    not null,
   FaultyPartsId        NUMBER(9),
   FaultyPartsCode      VARCHAR2(50),
   FaultyPartsName      VARCHAR2(100),
   FaultyPartsSupplierId NUMBER(9),
   FaultyPartsSupplierCode VARCHAR2(50),
   FaultyPartsSupplierName VARCHAR2(100),
   IfFaultyParts        NUMBER(1)                       not null,
   ResponsibleUnitId    NUMBER(9)                       not null,
   ResponsibleUnitCode  VARCHAR2(50)                    not null,
   ResponsibleUnitName  VARCHAR2(100)                   not null,
   Shipped              NUMBER(1)                       not null,
   NewPartsId           NUMBER(9)                       not null,
   NewPartsCode         VARCHAR2(50)                    not null,
   NewPartsName         VARCHAR2(100)                   not null,
   NewPartsSupplierId   NUMBER(9)                       not null,
   NewPartsSupplierCode VARCHAR2(50)                    not null,
   NewPartsSupplierName VARCHAR2(100)                   not null,
   NewPartsSerialNumber VARCHAR2(50),
   NewPartsBatchNumber  VARCHAR2(50),
   NewPartsSecurityNumber VARCHAR2(50),
   Remark               VARCHAR2(200),
   constraint PK_SSUSEDPARTSDISPOSALDETAIL primary key (Id)
)
/

/*==============================================================*/
/* Table: SsUsedPartsStorage                                    */
/*==============================================================*/
create table SsUsedPartsStorage  (
   Id                   NUMBER(9)                       not null,
   DealerId             NUMBER(9)                       not null,
   SubDealerId          NUMBER(9)                       not null,
   ClaimBillId          NUMBER(9)                       not null,
   ClaimBillCode        VARCHAR2(50)                    not null,
   ClaimBillType        NUMBER(9)                       not null,
   BranchId             NUMBER(9)                       not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   BranchName           VARCHAR2(100)                   not null,
   UsedPartsId          NUMBER(9)                       not null,
   UsedPartsCode        VARCHAR2(50)                    not null,
   UsedPartsName        VARCHAR2(100)                   not null,
   UsedPartsSerialNumber VARCHAR2(50),
   UsedPartsBatchNumber VARCHAR2(50),
   UsedPartsSupplierId  NUMBER(9)                       not null,
   UsedPartsSupplierCode VARCHAR2(50)                    not null,
   UsedPartsSupplierName VARCHAR2(100)                   not null,
   UsedPartsReturnPolicy NUMBER(9)                       not null,
   Quantity             NUMBER(9)                       not null,
   UnitPrice            NUMBER(19,4)                    not null,
   PartsManagementCost  NUMBER(19,4)                    not null,
   UsedPartsBarCode     VARCHAR2(50)                    not null,
   FaultyPartsId        NUMBER(9),
   FaultyPartsCode      VARCHAR2(50),
   FaultyPartsName      VARCHAR2(100),
   FaultyPartsSupplierId NUMBER(9),
   FaultyPartsSupplierCode VARCHAR2(50),
   FaultyPartsSupplierName VARCHAR2(100),
   IfFaultyParts        NUMBER(1)                       not null,
   ResponsibleUnitId    NUMBER(9)                       not null,
   ResponsibleUnitCode  VARCHAR2(50)                    not null,
   ResponsibleUnitName  VARCHAR2(100)                   not null,
   Shipped              NUMBER(1)                       not null,
   NewPartsId           NUMBER(9)                       not null,
   NewPartsCode         VARCHAR2(50)                    not null,
   NewPartsName         VARCHAR2(100)                   not null,
   NewPartsSupplierId   NUMBER(9)                       not null,
   NewPartsSupplierCode VARCHAR2(50)                    not null,
   NewPartsSupplierName VARCHAR2(100)                   not null,
   NewPartsSerialNumber VARCHAR2(50),
   NewPartsBatchNumber  VARCHAR2(50),
   NewPartsSecurityNumber VARCHAR2(50),
   ClaimBillCreateTime  DATE,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_SSUSEDPARTSSTORAGE primary key (Id)
)
/

/*==============================================================*/
/* Table: UsedPartsDistanceInfor                                */
/*==============================================================*/
create table UsedPartsDistanceInfor  (
   Id                   NUMBER(9)                       not null,
   UsedPartsCompanyId   NUMBER(9)                       not null,
   UsedPartsWarehouseId NUMBER(9)                       not null,
   UsedPartsDistanceType NUMBER(9)                       not null,
   OriginCompanyId      NUMBER(9),
   OriginUsedPartsWarehouseId NUMBER(9),
   ShippingDistance     NUMBER(9)                       not null,
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(500),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   RowVersion           TIMESTAMP,
   constraint PK_USEDPARTSDISTANCEINFOR primary key (Id)
)
/

/*==============================================================*/
/* Table: UsedPartsLogisticLossBill                             */
/*==============================================================*/
create table UsedPartsLogisticLossBill  (
   Id                   NUMBER(9)                       not null,
   DealerId             NUMBER(9)                       not null,
   DealerCode           VARCHAR2(50)                    not null,
   DealerName           VARCHAR2(100)                   not null,
   UsedPartsShippingOrderId NUMBER(9)                       not null,
   LogisticCompanyId    NUMBER(9),
   LogisticCompanyCode  VARCHAR2(50),
   LogisticCompanyName  VARCHAR2(100),
   TotalQuantity        NUMBER(9)                       not null,
   TotalAmount          NUMBER(19,4)                    not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   Remark               VARCHAR2(200),
   RowVersion           TIMESTAMP,
   constraint PK_USEDPARTSLOGISTICLOSSBILL primary key (Id)
)
/

/*==============================================================*/
/* Table: UsedPartsLogisticLossDetail                           */
/*==============================================================*/
create table UsedPartsLogisticLossDetail  (
   Id                   NUMBER(9)                       not null,
   UsedPartsLogisticLossBillId NUMBER(9)                       not null,
   SerialNumber         NUMBER(9)                       not null,
   BranchId             NUMBER(9)                       not null,
   BranchName           VARCHAR2(100)                   not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   ClaimBillId          NUMBER(9)                       not null,
   ClaimBillCode        VARCHAR2(50)                    not null,
   ClaimBillType        NUMBER(9)                       not null,
   ClaimBillCreateTime  DATE,
   UsedPartsId          NUMBER(9)                       not null,
   UsedPartsCode        VARCHAR2(50)                    not null,
   UsedPartsName        VARCHAR2(100)                   not null,
   UsedPartsSerialNumber VARCHAR2(50),
   UsedPartsSupplierId  NUMBER(9)                       not null,
   UsedPartsSupplierCode VARCHAR2(50)                    not null,
   UsedPartsSupplierName VARCHAR2(100)                   not null,
   UsedPartsBarCode     VARCHAR2(50)                    not null,
   ProcessStatus        NUMBER(9)                       not null,
   Quantity             NUMBER(9)                       not null,
   Price                NUMBER(19,4)                    not null,
   PartsManagementCost  NUMBER(19,4)                    not null,
   UsedPartsReturnPolicy NUMBER(9)                       not null,
   FaultyPartsId        NUMBER(9),
   FaultyPartsCode      VARCHAR2(50),
   FaultyPartsName      VARCHAR2(100),
   FaultyPartsSupplierId NUMBER(9),
   FaultyPartsSupplierCode VARCHAR2(50),
   FaultyPartsSupplierName VARCHAR2(100),
   IfFaultyParts        NUMBER(1)                       not null,
   ResponsibleUnitId    NUMBER(9)                       not null,
   ResponsibleUnitCode  VARCHAR2(50)                    not null,
   ResponsibleUnitName  VARCHAR2(100)                   not null,
   Shipped              NUMBER(1)                       not null,
   NewPartsId           NUMBER(9)                       not null,
   NewPartsCode         VARCHAR2(50)                    not null,
   NewPartsName         VARCHAR2(100)                   not null,
   NewPartsSupplierId   NUMBER(9)                       not null,
   NewPartsSupplierCode VARCHAR2(50)                    not null,
   NewPartsSupplierName VARCHAR2(100)                   not null,
   NewPartsSerialNumber VARCHAR2(50),
   NewPartsSecurityNumber VARCHAR2(50),
   Remark               VARCHAR2(200),
   constraint PK_USEDPARTSLOGISTICLOSSDETAIL primary key (Id)
)
/

/*==============================================================*/
/* Table: UsedPartsReturnPolicyHistory                          */
/*==============================================================*/
create table UsedPartsReturnPolicyHistory  (
   Id                   NUMBER(9)                       not null,
   DealerId             NUMBER(9)                       not null,
   ClaimBillId          NUMBER(9)                       not null,
   ClaimBillType        NUMBER(9)                       not null,
   BranchId             NUMBER(9)                       not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   BranchName           VARCHAR2(100)                   not null,
   ClaimBillCode        VARCHAR2(50)                    not null,
   UsedPartsReturnPolicy NUMBER(9)                       not null,
   UsedPartsId          NUMBER(9)                       not null,
   UsedPartsCode        VARCHAR2(50)                    not null,
   UsedPartsName        VARCHAR2(100)                   not null,
   UsedPartsBatchNumber VARCHAR2(50),
   UsedPartsSerialNumber VARCHAR2(50),
   UsedPartsSupplierId  NUMBER(9)                       not null,
   UsedPartsSupplierCode VARCHAR2(50)                    not null,
   UsedPartsSupplierName VARCHAR2(100)                   not null,
   Quantity             NUMBER(9)                       not null,
   UnitPrice            NUMBER(19,4)                    not null,
   PartsManagementCost  NUMBER(19,4)                    not null,
   UsedPartsBarCode     VARCHAR2(50)                    not null,
   FaultyPartsId        NUMBER(9),
   FaultyPartsCode      VARCHAR2(50),
   FaultyPartsName      VARCHAR2(100),
   FaultyPartsSupplierId NUMBER(9),
   FaultyPartsSupplierCode VARCHAR2(50),
   FaultyPartsSupplierName VARCHAR2(100),
   IfFaultyParts        NUMBER(1)                       not null,
   ResponsibleUnitId    NUMBER(9)                       not null,
   ResponsibleUnitCode  VARCHAR2(50)                    not null,
   ResponsibleUnitName  VARCHAR2(100)                   not null,
   Shipped              NUMBER(1)                       not null,
   NewPartsId           NUMBER(9)                       not null,
   NewPartsCode         VARCHAR2(50)                    not null,
   NewPartsName         VARCHAR2(100)                   not null,
   NewPartsSupplierId   NUMBER(9)                       not null,
   NewPartsSupplierCode VARCHAR2(50)                    not null,
   NewPartsSupplierName VARCHAR2(100)                   not null,
   NewPartsSerialNumber VARCHAR2(50),
   NewPartsBatchNumber  VARCHAR2(50),
   ClaimBillCreateTime  DATE,
   NewPartsSecurityNumber VARCHAR2(50),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_USEDPARTSRTNPOLICYHISTORY primary key (Id)
)
/

/*==============================================================*/
/* Table: UsedPartsShippingDetail                               */
/*==============================================================*/
create table UsedPartsShippingDetail  (
   Id                   NUMBER(9)                       not null,
   BranchId             NUMBER(9)                       not null,
   BranchName           VARCHAR2(100)                   not null,
   UsedPartsShippingOrderId NUMBER(9)                       not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   SerialNumber         NUMBER(9)                       not null,
   UsedPartsId          NUMBER(9)                       not null,
   UsedPartsCode        VARCHAR2(50)                    not null,
   UsedPartsName        VARCHAR2(100)                   not null,
   UsedPartsSerialNumber VARCHAR2(50),
   UsedPartsBatchNumber VARCHAR2(50),
   UsedPartsSupplierId  NUMBER(9)                       not null,
   UsedPartsSupplierCode VARCHAR2(50)                    not null,
   UsedPartsSupplierName VARCHAR2(100)                   not null,
   UsedPartsReturnPolicy NUMBER(9)                       not null,
   Quantity             NUMBER(9)                       not null,
   UnitPrice            NUMBER(19,4)                    not null,
   UsedPartsEncourageAmount NUMBER(19,4),
   PartsManagementCost  NUMBER(19,4)                    not null,
   UsedPartsBarCode     VARCHAR2(50)                    not null,
   UsedPartsWarehouseAreaId NUMBER(9),
   UsedPartsWarehouseAreaCode VARCHAR2(50),
   FaultyPartsId        NUMBER(9),
   FaultyPartsCode      VARCHAR2(50),
   FaultyPartsName      VARCHAR2(100),
   FaultyPartsSupplierId NUMBER(9),
   FaultyPartsSupplierCode VARCHAR2(50),
   FaultyPartsSupplierName VARCHAR2(100),
   IfFaultyParts        NUMBER(1)                       not null,
   ResponsibleUnitId    NUMBER(9)                       not null,
   ResponsibleUnitCode  VARCHAR2(50)                    not null,
   ResponsibleUnitName  VARCHAR2(100)                   not null,
   Shipped              NUMBER(1)                       not null,
   NewPartsId           NUMBER(9)                       not null,
   NewPartsCode         VARCHAR2(50)                    not null,
   NewPartsName         VARCHAR2(100)                   not null,
   NewPartsSupplierId   NUMBER(9)                       not null,
   NewPartsSupplierCode VARCHAR2(50)                    not null,
   NewPartsSupplierName VARCHAR2(100)                   not null,
   NewPartsSerialNumber VARCHAR2(50),
   NewPartsBatchNumber  VARCHAR2(50),
   NewPartsSecurityNumber VARCHAR2(50),
   ClaimBillId          NUMBER(9)                       not null,
   ClaimBillCode        VARCHAR2(50)                    not null,
   ClaimBillCreateTime  DATE,
   ClaimBillType        NUMBER(9)                       not null,
   ShippingRemark       VARCHAR2(200),
   ReceptionStatus      NUMBER(9)                       not null,
   ReceptionRemark      VARCHAR2(200),
   SubDealerId          NUMBER(9),
   SubDealerCode        VARCHAR2(50),
   SubDealerName        VARCHAR2(100),
   InboundAmount        NUMBER(9),
   constraint PK_USEDPARTSSHIPPINGDETAIL primary key (Id)
)
/

/*==============================================================*/
/* Table: UsedPartsShippingOrder                                */
/*==============================================================*/
create table UsedPartsShippingOrder  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   BranchId             NUMBER(9)                       not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   BranchCode           VARCHAR2(50)                    not null,
   BranchName           VARCHAR2(100)                   not null,
   DealerId             NUMBER(9)                       not null,
   DealerCode           VARCHAR2(50)                    not null,
   DealerName           VARCHAR2(100)                   not null,
   DestinationWarehouseId NUMBER(9)                       not null,
   ShippingMethod       NUMBER(9),
   Dispatcher           NUMBER(9)                       not null,
   Operator             VARCHAR2(50),
   OperatorPhone        VARCHAR2(50),
   Supervisor           VARCHAR2(50),
   SupervisorPhone      VARCHAR2(50),
   WarehouseContact     VARCHAR2(50),
   WarehousePhone       VARCHAR2(50),
   DestinationAddress   VARCHAR2(100),
   RequestedArrivalDate DATE,
   ActualArrivalDate    DATE,
   ShippingDate         DATE,
   LogisticCompanyId    NUMBER(9),
   LogisticCompanyCode  VARCHAR2(50),
   LogisticCompanyName  VARCHAR2(100),
   TransportDriver      VARCHAR2(50),
   DriverPhone          VARCHAR2(50),
   TransportationVehicle VARCHAR2(50),
   TotalAmount          NUMBER(19,4)                    not null,
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   ReceptionRemark      VARCHAR2(200),
   InboundStatus        NUMBER(9),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   ConfirmorId          NUMBER(9),
   ConfirmorName        VARCHAR2(100),
   ConfirmationTime     DATE,
   RowVersion           TIMESTAMP,
   constraint PK_USEDPARTSSHIPPINGORDER primary key (Id)
)
/

