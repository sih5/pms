/*==============================================================*/
/* DBMS name:      ORACLE Version 10gR2                         */
/* Created on:     2017/11/13 13:42:13                          */
/*==============================================================*/


declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('DeferredDiscountType');
  if num>0 then
    execute immediate 'drop table DeferredDiscountType cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('ExtendedSparePartList');
  if num>0 then
    execute immediate 'drop table ExtendedSparePartList cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('ExtendedWarrantyOrder');
  if num>0 then
    execute immediate 'drop table ExtendedWarrantyOrder cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('ExtendedWarrantyOrderList');
  if num>0 then
    execute immediate 'drop table ExtendedWarrantyOrderList cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('ExtendedWarrantyProduct');
  if num>0 then
    execute immediate 'drop table ExtendedWarrantyProduct cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_DeferredDiscountType');
  if num>0 then
    execute immediate 'drop sequence S_DeferredDiscountType';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_ExtendedSparePartList');
  if num>0 then
    execute immediate 'drop sequence S_ExtendedSparePartList';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_ExtendedWarrantyOrder');
  if num>0 then
    execute immediate 'drop sequence S_ExtendedWarrantyOrder';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_ExtendedWarrantyOrderList');
  if num>0 then
    execute immediate 'drop sequence S_ExtendedWarrantyOrderList';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_ExtendedWarrantyProduct');
  if num>0 then
    execute immediate 'drop sequence S_ExtendedWarrantyProduct';
  end if;
end;
/

create sequence S_DeferredDiscountType
/

create sequence S_ExtendedSparePartList
/

create sequence S_ExtendedWarrantyOrder
/

create sequence S_ExtendedWarrantyOrderList
/

create sequence S_ExtendedWarrantyProduct
/

/*==============================================================*/
/* Table: DeferredDiscountType                                  */
/*==============================================================*/
create table DeferredDiscountType  (
   Id                   NUMBER(9)                       not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   PartsSalesCategory   VARCHAR2(100)                   not null,
   ExtendedWarrantyProductId NUMBER(9)                       not null,
   ExtendedWarrantyProductName VARCHAR2(100)                   not null,
   ExtendedWarrantyAgioType VARCHAR2(100)                   not null,
   ExtendedWarrantyAgio NUMBER(15,6)                    not null,
   EffectiveTime        DATE                            not null,
   EndTime              DATE                            not null,
   Status               NUMBER(9)                       not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreatorTime          DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifierTime         DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonerTime        DATE,
   constraint PK_DEFERREDDISCOUNTTYPE primary key (Id)
)
/

/*==============================================================*/
/* Table: ExtendedSparePartList                                 */
/*==============================================================*/
create table ExtendedSparePartList  (
   Id                   NUMBER(9)                       not null,
   ExtendedWarrantyProductId NUMBER(9)                       not null,
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50)                    not null,
   SparePartName        VARCHAR2(100)                   not null,
   ReqPart              VARCHAR2(160),
   constraint PK_EXTENDEDSPAREPARTLIST primary key (Id)
)
/

/*==============================================================*/
/* Table: ExtendedWarrantyOrder                                 */
/*==============================================================*/
create table ExtendedWarrantyOrder  (
   Id                   NUMBER(9)                       not null,
   ExtendedWarrantyOrderCode VARCHAR2(50)                    not null,
   CustomerName         VARCHAR2(100)                   not null,
   CellNumber           VARCHAR2(50)                    not null,
   MemberCode           VARCHAR2(50),
   MemberRank           VARCHAR2(50),
   Email                VARCHAR2(50),
   IDType               NUMBER(9),
   IDNumer              VARCHAR2(50),
   RegionId             NUMBER(9),
   Province             VARCHAR2(50),
   City                 VARCHAR2(50),
   Region               VARCHAR2(50),
   DetailAddress        VARCHAR2(50),
   PartsSalesCategoryId NUMBER(9)                       not null,
   PartsSalesCategoryName VARCHAR2(100)                   not null,
   ServiceProductLineId NUMBER(9)                       not null,
   ServiceProductLine   VARCHAR2(100)                   not null,
   VehicleId            NUMBER(9)                       not null,
   VIN                  VARCHAR2(50)                    not null,
   CarSalesDate         DATE                            not null,
   Mileage              NUMBER(9)                       not null,
   EngineCylinder       NUMBER(15,6),
   PurchaseAmount       NUMBER(19,4)                    not null,
   EngineModel          VARCHAR2(100)                   not null,
   VehicleLicensePlate  VARCHAR2(50),
   SalesDate            DATE                            not null,
   TotalAmount          NUMBER(19,4)                    not null,
   DealerTotalAmount    NUMBER(19,4)                    not null,
   SellPersonnel        VARCHAR2(100),
   Remark               VARCHAR2(100),
   Status               NUMBER(9)                       not null,
   CheckerResult        NUMBER(9)                       not null,
   DetailedReasons      VARCHAR2(100),
   ProtocolPhotos       VARCHAR2(2000)                  not null,
   MileagePhotos        VARCHAR2(2000)                  not null,
   TravelPermitPhoto    VARCHAR2(2000)                  not null,
   IDPhoto              VARCHAR2(2000),
   CarInvoicePhotos     VARCHAR2(2000),
   DealerId             NUMBER(9)                       not null,
   DealerCode           VARCHAR2(100)                   not null,
   DealerName           VARCHAR2(100)                   not null,
   BranchId             NUMBER(9)                       not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreatorTime          DATE,
   SubmitterId          NUMBER(9),
   SubmitterName        VARCHAR2(100),
   SubmitterTime        DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifierTime         DATE,
   ConfirmorId          NUMBER(9),
   ConfirmorName        VARCHAR2(100),
   ConfirmorTime        DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonerTime        DATE,
   CheckerId            NUMBER(9),
   CheckerName          VARCHAR2(100),
   CheckerTime          DATE,
   RepairObjectId       NUMBER(9)                       not null,
   SerialNumber         VARCHAR2(50)                    not null,
   OtherAccessories     VARCHAR2(2000),
   constraint PK_EXTENDEDWARRANTYORDER primary key (Id)
)
/

/*==============================================================*/
/* Table: ExtendedWarrantyOrderList                             */
/*==============================================================*/
create table ExtendedWarrantyOrderList  (
   Id                   NUMBER(9)                       not null,
   ExtendedWarrantyOrderId NUMBER(9)                       not null,
   ExtendedWarrantyProductId NUMBER(9)                       not null,
   CustomerRetailPrice  NUMBER(19,4)                    not null,
   SalesPrice           NUMBER(19,4)                    not null,
   AgioType             NUMBER(9),
   DiscountRate         NUMBER(15,6)                    not null,
   DiscountedPrice      NUMBER(19,4)                    not null,
   TotalAmount          NUMBER(19,4)                    not null,
   ValidFrom            DATE,
   EndDate              DATE,
   StartMileage         NUMBER(9),
   EndMileage           NUMBER(9),
   ExtensionCoverage    VARCHAR2(200),
   constraint PK_EXTENDEDWARRANTYORDERLIST primary key (Id)
)
/

/*==============================================================*/
/* Table: ExtendedWarrantyProduct                               */
/*==============================================================*/
create table ExtendedWarrantyProduct  (
   Id                   NUMBER(9)                       not null,
   ExtendedWarrantyProductCode VARCHAR2(50)                    not null,
   ExtendedWarrantyProductName VARCHAR2(100)                   not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   PartsSalesCategory   VARCHAR2(100)                   not null,
   ServiceProductLineId NUMBER(9)                       not null,
   ServiceProductLine   VARCHAR2(100)                   not null,
   ExtendedWarrantyTerm NUMBER(15,6),
   ExtendedWarrantyMileage NUMBER(9),
   ExtendedWarrantyTermRat NUMBER(9),
   ExtendedWarrantyMileageRat NUMBER(9),
   TradePrice           NUMBER(19,4)                    not null,
   CustomerRetailPrice  NUMBER(19,4)                    not null,
   PayStartTimeCondition NUMBER(15,6)                    not null,
   PayEndTimeCondition  NUMBER(15,6)                    not null,
   PayStartTimeMileage  NUMBER(9),
   PayEndTimeMileage    NUMBER(9),
   AutomaticAuditPeriod NUMBER(9),
   Status               NUMBER(9)                       not null,
   ExtensionCoverage    VARCHAR2(500),
   OutExtensionCoverage VARCHAR2(500),
   AttachmentUpload     VARCHAR2(2000),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreatorTime          DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifierTime         DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonerTime        DATE,
   constraint PK_EXTENDEDWARRANTYPRODUCT primary key (Id)
)
/

