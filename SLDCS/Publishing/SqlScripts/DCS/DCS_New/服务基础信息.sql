/*==============================================================*/
/* DBMS name:      ORACLE Version 10gR2                         */
/* Created on:     2017/12/25 14:00:56                          */
/*==============================================================*/


declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('ApplicationType');
  if num>0 then
    execute immediate 'drop table ApplicationType cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('FaultModeStandard');
  if num>0 then
    execute immediate 'drop table FaultModeStandard cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('FundRedPostVehicleMsg');
  if num>0 then
    execute immediate 'drop table FundRedPostVehicleMsg cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('GradeCoefficient');
  if num>0 then
    execute immediate 'drop table GradeCoefficient cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('LaborHourUnitPriceGrade');
  if num>0 then
    execute immediate 'drop table LaborHourUnitPriceGrade cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('LaborHourUnitPriceRate');
  if num>0 then
    execute immediate 'drop table LaborHourUnitPriceRate cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('MalfCatAffiVehiCat');
  if num>0 then
    execute immediate 'drop table MalfCatAffiVehiCat cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('Malfunction');
  if num>0 then
    execute immediate 'drop table Malfunction cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('MalfunctionBrandRelation');
  if num>0 then
    execute immediate 'drop table MalfunctionBrandRelation cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('MalfunctionCategory');
  if num>0 then
    execute immediate 'drop table MalfunctionCategory cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsManagementCostGrade');
  if num>0 then
    execute immediate 'drop table PartsManagementCostGrade cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsManagementCostRate');
  if num>0 then
    execute immediate 'drop table PartsManagementCostRate cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsWarrantyCategory');
  if num>0 then
    execute immediate 'drop table PartsWarrantyCategory cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsWarrantyStandard');
  if num>0 then
    execute immediate 'drop table PartsWarrantyStandard cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsWarrantyTerm');
  if num>0 then
    execute immediate 'drop table PartsWarrantyTerm cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsWarrantyTermHistory');
  if num>0 then
    execute immediate 'drop table PartsWarrantyTermHistory cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PreSalesCheckFeeStandard');
  if num>0 then
    execute immediate 'drop table PreSalesCheckFeeStandard cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('RepairClassification');
  if num>0 then
    execute immediate 'drop table RepairClassification cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('RepairItem');
  if num>0 then
    execute immediate 'drop table RepairItem cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('RepairItemAffiServProdLine');
  if num>0 then
    execute immediate 'drop table RepairItemAffiServProdLine cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('RepairItemBrandRelation');
  if num>0 then
    execute immediate 'drop table RepairItemBrandRelation cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('RepairItemCatAffiVehiCat');
  if num>0 then
    execute immediate 'drop table RepairItemCatAffiVehiCat cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('RepairItemCategory');
  if num>0 then
    execute immediate 'drop table RepairItemCategory cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('RepairItemProdLineHistory');
  if num>0 then
    execute immediate 'drop table RepairItemProdLineHistory cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('RepairMethodStandard');
  if num>0 then
    execute immediate 'drop table RepairMethodStandard cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('RepairObject');
  if num>0 then
    execute immediate 'drop table RepairObject cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('ServiceActivity');
  if num>0 then
    execute immediate 'drop table ServiceActivity cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('ServiceActivityCondition');
  if num>0 then
    execute immediate 'drop table ServiceActivityCondition cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('ServiceActivityDealerVehicle');
  if num>0 then
    execute immediate 'drop table ServiceActivityDealerVehicle cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('ServiceActivityMaterialDetail');
  if num>0 then
    execute immediate 'drop table ServiceActivityMaterialDetail cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('ServiceActivityQuota');
  if num>0 then
    execute immediate 'drop table ServiceActivityQuota cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('ServiceActivityRepairItem');
  if num>0 then
    execute immediate 'drop table ServiceActivityRepairItem cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('ServiceActivityVehicleList');
  if num>0 then
    execute immediate 'drop table ServiceActivityVehicleList cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('ServiceTripArea');
  if num>0 then
    execute immediate 'drop table ServiceTripArea cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('ServiceTripPriceGrade');
  if num>0 then
    execute immediate 'drop table ServiceTripPriceGrade cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('ServiceTripPriceRate');
  if num>0 then
    execute immediate 'drop table ServiceTripPriceRate cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('VMainteProductCategoryHist');
  if num>0 then
    execute immediate 'drop table VMainteProductCategoryHist cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('VehicleAttribute');
  if num>0 then
    execute immediate 'drop table VehicleAttribute cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('VehicleMainteProductCategory');
  if num>0 then
    execute immediate 'drop table VehicleMainteProductCategory cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('VehicleMainteRepairItem');
  if num>0 then
    execute immediate 'drop table VehicleMainteRepairItem cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('VehicleMainteRepairItemHist');
  if num>0 then
    execute immediate 'drop table VehicleMainteRepairItemHist cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('VehicleMainteTerm');
  if num>0 then
    execute immediate 'drop table VehicleMainteTerm cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('VehicleMainteTermHistory');
  if num>0 then
    execute immediate 'drop table VehicleMainteTermHistory cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('VehicleWarrantyTerm');
  if num>0 then
    execute immediate 'drop table VehicleWarrantyTerm cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('VehicleWarrantyTermHistory');
  if num>0 then
    execute immediate 'drop table VehicleWarrantyTermHistory cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('VeriCodeEffectTime');
  if num>0 then
    execute immediate 'drop table VeriCodeEffectTime cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('WarrPolNServProdLineHistory');
  if num>0 then
    execute immediate 'drop table WarrPolNServProdLineHistory cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('WarrantyPolicy');
  if num>0 then
    execute immediate 'drop table WarrantyPolicy cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('WarrantyPolicyHistory');
  if num>0 then
    execute immediate 'drop table WarrantyPolicyHistory cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('WarrantyPolicyNServProdLine');
  if num>0 then
    execute immediate 'drop table WarrantyPolicyNServProdLine cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('qtsinfo');
  if num>0 then
    execute immediate 'drop table qtsinfo cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_ApplicationType');
  if num>0 then
    execute immediate 'drop sequence S_ApplicationType';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_FaultModeStandard');
  if num>0 then
    execute immediate 'drop sequence S_FaultModeStandard';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_FundRedPostVehicleMsg');
  if num>0 then
    execute immediate 'drop sequence S_FundRedPostVehicleMsg';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_GradeCoefficient');
  if num>0 then
    execute immediate 'drop sequence S_GradeCoefficient';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_LaborHourUnitPriceGrade');
  if num>0 then
    execute immediate 'drop sequence S_LaborHourUnitPriceGrade';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_LaborHourUnitPriceRate');
  if num>0 then
    execute immediate 'drop sequence S_LaborHourUnitPriceRate';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_MalfCatAffiVehiCat');
  if num>0 then
    execute immediate 'drop sequence S_MalfCatAffiVehiCat';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_Malfunction');
  if num>0 then
    execute immediate 'drop sequence S_Malfunction';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_MalfunctionBrandRelation');
  if num>0 then
    execute immediate 'drop sequence S_MalfunctionBrandRelation';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_MalfunctionCategory');
  if num>0 then
    execute immediate 'drop sequence S_MalfunctionCategory';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsManagementCostGrade');
  if num>0 then
    execute immediate 'drop sequence S_PartsManagementCostGrade';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsManagementCostRate');
  if num>0 then
    execute immediate 'drop sequence S_PartsManagementCostRate';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsWarrantyCategory');
  if num>0 then
    execute immediate 'drop sequence S_PartsWarrantyCategory';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsWarrantyStandard');
  if num>0 then
    execute immediate 'drop sequence S_PartsWarrantyStandard';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsWarrantyTerm');
  if num>0 then
    execute immediate 'drop sequence S_PartsWarrantyTerm';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsWarrantyTermHistory');
  if num>0 then
    execute immediate 'drop sequence S_PartsWarrantyTermHistory';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PreSalesCheckFeeStandard');
  if num>0 then
    execute immediate 'drop sequence S_PreSalesCheckFeeStandard';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_RepairClassification');
  if num>0 then
    execute immediate 'drop sequence S_RepairClassification';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_RepairItem');
  if num>0 then
    execute immediate 'drop sequence S_RepairItem';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_RepairItemAffiServProdLine');
  if num>0 then
    execute immediate 'drop sequence S_RepairItemAffiServProdLine';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_RepairItemBrandRelation');
  if num>0 then
    execute immediate 'drop sequence S_RepairItemBrandRelation';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_RepairItemCatAffiVehiCat');
  if num>0 then
    execute immediate 'drop sequence S_RepairItemCatAffiVehiCat';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_RepairItemCategory');
  if num>0 then
    execute immediate 'drop sequence S_RepairItemCategory';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_RepairItemProdLineHistory');
  if num>0 then
    execute immediate 'drop sequence S_RepairItemProdLineHistory';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_RepairMethodStandard');
  if num>0 then
    execute immediate 'drop sequence S_RepairMethodStandard';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_RepairObject');
  if num>0 then
    execute immediate 'drop sequence S_RepairObject';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_ServiceActivity');
  if num>0 then
    execute immediate 'drop sequence S_ServiceActivity';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_ServiceActivityCondition');
  if num>0 then
    execute immediate 'drop sequence S_ServiceActivityCondition';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_ServiceActivityDealerVehicle');
  if num>0 then
    execute immediate 'drop sequence S_ServiceActivityDealerVehicle';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_ServiceActivityMatlDetail');
  if num>0 then
    execute immediate 'drop sequence S_ServiceActivityMatlDetail';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_ServiceActivityQuota');
  if num>0 then
    execute immediate 'drop sequence S_ServiceActivityQuota';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_ServiceActivityRepairItem');
  if num>0 then
    execute immediate 'drop sequence S_ServiceActivityRepairItem';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_ServiceActivityVehicleList');
  if num>0 then
    execute immediate 'drop sequence S_ServiceActivityVehicleList';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_ServiceTripArea');
  if num>0 then
    execute immediate 'drop sequence S_ServiceTripArea';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_ServiceTripPriceGrade');
  if num>0 then
    execute immediate 'drop sequence S_ServiceTripPriceGrade';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_ServiceTripPriceRate');
  if num>0 then
    execute immediate 'drop sequence S_ServiceTripPriceRate';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_VMainteProductCategoryHist');
  if num>0 then
    execute immediate 'drop sequence S_VMainteProductCategoryHist';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_VehicleMainteProductCategory');
  if num>0 then
    execute immediate 'drop sequence S_VehicleMainteProductCategory';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_VehicleMainteRepairItem');
  if num>0 then
    execute immediate 'drop sequence S_VehicleMainteRepairItem';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_VehicleMainteRepairItemHist');
  if num>0 then
    execute immediate 'drop sequence S_VehicleMainteRepairItemHist';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_VehicleMainteTerm');
  if num>0 then
    execute immediate 'drop sequence S_VehicleMainteTerm';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_VehicleMainteTermHistory');
  if num>0 then
    execute immediate 'drop sequence S_VehicleMainteTermHistory';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_VehicleWarrantyTerm');
  if num>0 then
    execute immediate 'drop sequence S_VehicleWarrantyTerm';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_VehicleWarrantyTermHistory');
  if num>0 then
    execute immediate 'drop sequence S_VehicleWarrantyTermHistory';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_VeriCodeEffectTime');
  if num>0 then
    execute immediate 'drop sequence S_VeriCodeEffectTime';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_WarrPolNServProdLineHistory');
  if num>0 then
    execute immediate 'drop sequence S_WarrPolNServProdLineHistory';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_WarrantyPolicy');
  if num>0 then
    execute immediate 'drop sequence S_WarrantyPolicy';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_WarrantyPolicyHistory');
  if num>0 then
    execute immediate 'drop sequence S_WarrantyPolicyHistory';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_WarrantyPolicyNServProdLine');
  if num>0 then
    execute immediate 'drop sequence S_WarrantyPolicyNServProdLine';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_qtsinfo');
  if num>0 then
    execute immediate 'drop sequence S_qtsinfo';
  end if;
end;
/

create sequence S_ApplicationType
/

create sequence S_FaultModeStandard
/

create sequence S_FundRedPostVehicleMsg
/

create sequence S_GradeCoefficient
/

create sequence S_LaborHourUnitPriceGrade
/

create sequence S_LaborHourUnitPriceRate
/

create sequence S_MalfCatAffiVehiCat
/

create sequence S_Malfunction
/

create sequence S_MalfunctionBrandRelation
/

create sequence S_MalfunctionCategory
/

create sequence S_PartsManagementCostGrade
/

create sequence S_PartsManagementCostRate
/

create sequence S_PartsWarrantyCategory
/

create sequence S_PartsWarrantyStandard
/

create sequence S_PartsWarrantyTerm
/

create sequence S_PartsWarrantyTermHistory
/

create sequence S_PreSalesCheckFeeStandard
/

create sequence S_RepairClassification
/

create sequence S_RepairItem
/

create sequence S_RepairItemAffiServProdLine
/

create sequence S_RepairItemBrandRelation
/

create sequence S_RepairItemCatAffiVehiCat
/

create sequence S_RepairItemCategory
/

create sequence S_RepairItemProdLineHistory
/

create sequence S_RepairMethodStandard
/

create sequence S_RepairObject
/

create sequence S_ServiceActivity
/

create sequence S_ServiceActivityCondition
/

create sequence S_ServiceActivityDealerVehicle
/

create sequence S_ServiceActivityMatlDetail
/

create sequence S_ServiceActivityQuota
/

create sequence S_ServiceActivityRepairItem
/

create sequence S_ServiceActivityVehicleList
/

create sequence S_ServiceTripArea
/

create sequence S_ServiceTripPriceGrade
/

create sequence S_ServiceTripPriceRate
/

create sequence S_VMainteProductCategoryHist
/

create sequence S_VehicleMainteProductCategory
/

create sequence S_VehicleMainteRepairItem
/

create sequence S_VehicleMainteRepairItemHist
/

create sequence S_VehicleMainteTerm
/

create sequence S_VehicleMainteTermHistory
/

create sequence S_VehicleWarrantyTerm
/

create sequence S_VehicleWarrantyTermHistory
/

create sequence S_VeriCodeEffectTime
/

create sequence S_WarrPolNServProdLineHistory
/

create sequence S_WarrantyPolicy
/

create sequence S_WarrantyPolicyHistory
/

create sequence S_WarrantyPolicyNServProdLine
/

create sequence S_qtsinfo
/

/*==============================================================*/
/* Table: ApplicationType                                       */
/*==============================================================*/
create table ApplicationType  (
   Id                   NUMBER(9)                       not null,
   ApplicationTypeName  VARCHAR2(100)                   not null,
   BranchId             NUMBER(9)                       not null,
   ApplicationTypeClass NUMBER(9),
   constraint PK_APPLICATIONTYPE primary key (Id)
)
/

/*==============================================================*/
/* Table: FaultModeStandard                                     */
/*==============================================================*/
create table FaultModeStandard  (
   Id                   NUMBER(9)                       not null,
   FaultModeCode        VARCHAR2(50)                    not null,
   FaultModeName        VARCHAR2(100)                   not null,
   FaultModeDescription VARCHAR2(200),
   Status               NUMBER(9)                       not null,
   CreatorId            NUMBER(9)                       not null,
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   constraint PK_FAULTMODESTANDARD primary key (Id)
)
/

/*==============================================================*/
/* Table: FundRedPostVehicleMsg                                 */
/*==============================================================*/
create table FundRedPostVehicleMsg  (
   Id                   NUMBER(9)                       not null,
   RedPostCode          VARCHAR2(50)                    not null,
   VIN                  VARCHAR2(50)                    not null,
   BrandName            VARCHAR2(100),
   RedPostName          VARCHAR2(100)                   not null,
   RedPostDescribe      VARCHAR2(200)                   not null,
   CustomerName         VARCHAR2(100)                   not null,
   CellPhoneNumber      VARCHAR2(100)                   not null,
   RedPostAmount        NUMBER(19,4)                    not null,
   LimitPrice           NUMBER(19,4)                    not null,
   ValidationDate       DATE                            not null,
   LssuingUnit          NUMBER(9),
   ExpireDate           DATE                            not null,
   Status               NUMBER(9)                       not null,
   RepairContractCode   VARCHAR2(150),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_FUNDREDPOSTVEHICLEMSG primary key (Id)
)
/

/*==============================================================*/
/* Table: GradeCoefficient                                      */
/*==============================================================*/
create table GradeCoefficient  (
   Id                   NUMBER(9)                       not null,
   BranchId             NUMBER(9)                       not null,
   BranchName           VARCHAR2(50)                    not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   PartsSalesCategoryName VARCHAR2(50)                    not null,
   Grade                VARCHAR2(50)                    not null,
   Coefficient          NUMBER(15,6)                    not null,
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   RowVersion           TIMESTAMP,
   constraint PK_GRADECOEFFICIENT primary key (Id)
)
/

/*==============================================================*/
/* Table: LaborHourUnitPriceGrade                               */
/*==============================================================*/
create table LaborHourUnitPriceGrade  (
   Id                   NUMBER(9)                       not null,
   BranchId             NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   Name                 VARCHAR2(100)                   not null,
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_LABORHOURUNITPRICEGRADE primary key (Id)
)
/

/*==============================================================*/
/* Table: LaborHourUnitPriceRate                                */
/*==============================================================*/
create table LaborHourUnitPriceRate  (
   Id                   NUMBER(9)                       not null,
   LaborHourUnitPriceGradeId NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   ServiceProductLineId NUMBER(9)                       not null,
   ProductLineType      NUMBER(9)                       not null,
   Price                NUMBER(19,4)                    not null,
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_LABORHOURUNITPRICERATE primary key (Id)
)
/

/*==============================================================*/
/* Table: MalfCatAffiVehiCat                                    */
/*==============================================================*/
create table MalfCatAffiVehiCat  (
   Id                   NUMBER(9)                       not null,
   VehicleCategoryId    NUMBER(9)                       not null,
   MalfunctionCategoryId NUMBER(9)                       not null,
   constraint PK_MALFCATAFFIVEHICAT primary key (Id)
)
/

/*==============================================================*/
/* Table: Malfunction                                           */
/*==============================================================*/
create table Malfunction  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   Description          VARCHAR2(600)                   not null,
   MalfunctionCategoryId NUMBER(9)                       not null,
   QuickCode            VARCHAR2(50),
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   ISISG                NUMBER(9),
   constraint PK_MALFUNCTION primary key (Id)
)
/

/*==============================================================*/
/* Table: MalfunctionBrandRelation                              */
/*==============================================================*/
create table MalfunctionBrandRelation  (
   Id                   NUMBER(9)                       not null,
   BranchId             NUMBER(9)                       not null,
   MalfunctionId        NUMBER(9)                       not null,
   MalfunctionCode      VARCHAR2(50)                    not null,
   MalfunctionName      VARCHAR2(600)                   not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   IsApplication        NUMBER(1)                       not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   RowVersion           TIMESTAMP,
   constraint PK_MALFUNCTIONBRANDRELATION primary key (Id)
)
/

/*==============================================================*/
/* Table: MalfunctionCategory                                   */
/*==============================================================*/
create table MalfunctionCategory  (
   Id                   NUMBER(9)                       not null,
   ParentId             NUMBER(9),
   RootGroupId          NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   Name                 VARCHAR2(200)                   not null,
   LayerNodeTypeId      NUMBER(9)                       not null,
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   STMSCODE             VARCHAR2(64),
   constraint PK_MALFUNCTIONCATEGORY primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsManagementCostGrade                              */
/*==============================================================*/
create table PartsManagementCostGrade  (
   Id                   NUMBER(9)                       not null,
   BranchId             NUMBER(9)                       not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   SalesCategoryName    VARCHAR2(50)                    not null,
   Code                 VARCHAR2(50)                    not null,
   Name                 VARCHAR2(100)                   not null,
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_PARTSMANAGEMENTCOSTGRADE primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsManagementCostRate                               */
/*==============================================================*/
create table PartsManagementCostRate  (
   Id                   NUMBER(9)                       not null,
   PartsManagementCostGradeId NUMBER(9)                       not null,
   PartsPriceUpperLimit NUMBER(19,4),
   PartsPriceLowerLimit NUMBER(19,4),
   Rate                 NUMBER(15,6)                    not null,
   UpperLimitAmount     NUMBER(19,4),
   LowerLimitAmount     NUMBER(19,4),
   InvoiceType          NUMBER(9),
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_PARTSMANAGEMENTCOSTRATE primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsWarrantyCategory                                 */
/*==============================================================*/
create table PartsWarrantyCategory  (
   Id                   NUMBER(9)                       not null,
   BranchId             NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   Name                 VARCHAR2(100)                   not null,
   Remark               VARCHAR2(200),
   Status               NUMBER(9)                       not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   constraint PK_PARTSWARRANTYCATEGORY primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsWarrantyStandard                                 */
/*==============================================================*/
create table PartsWarrantyStandard  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   Name                 VARCHAR2(100)                   not null,
   PartsWarrantyType    NUMBER(9)                       not null,
   WarrantyPeriod       NUMBER(9)                       not null,
   WarrantyMileage      NUMBER(9)                       not null,
   Capacity             NUMBER(9)                       not null,
   WorkingHours         NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   Status               NUMBER(9)                       not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   constraint PK_PARTSWARRANTYSTANDARD primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsWarrantyTerm                                     */
/*==============================================================*/
create table PartsWarrantyTerm  (
   Id                   NUMBER(9)                       not null,
   WarrantyPolicyId     NUMBER(9)                       not null,
   PartsWarrantyType    NUMBER(9)                       not null,
   PartsWarrantyCategoryId NUMBER(9)                       not null,
   WarrantyPeriod       NUMBER(9)                       not null,
   WarrantyMileage      NUMBER(9),
   Capacity             NUMBER(9)                       not null,
   WorkingHours         NUMBER(9)                       not null,
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_PARTSWARRANTYTERM primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsWarrantyTermHistory                              */
/*==============================================================*/
create table PartsWarrantyTermHistory  (
   Id                   NUMBER(9)                       not null,
   PartsWarrantyTermId  NUMBER(9)                       not null,
   WarrantyPolicyId     NUMBER(9),
   PartsWarrantyType    NUMBER(9),
   PartsWarrantyCategoryId NUMBER(9),
   WarrantyPeriod       NUMBER(9),
   Capacity             NUMBER(9)                       not null,
   WorkingHours         NUMBER(9)                       not null,
   WarrantyMileage      NUMBER(9),
   Status               NUMBER(9),
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   FilerId              NUMBER(9),
   FilerName            VARCHAR2(100),
   FileTime             DATE,
   constraint PK_PARTSWARRANTYTERMHISTORY primary key (Id)
)
/

/*==============================================================*/
/* Table: PreSalesCheckFeeStandard                              */
/*==============================================================*/
create table PreSalesCheckFeeStandard  (
   Id                   NUMBER(9)                       not null,
   Fee                  NUMBER(19,4),
   PartsSalesCategoryId NUMBER(9),
   BranchId             NUMBER(9),
   ServiceProductLineId NUMBER(9),
   constraint PK_PRESALESCHECKFEESTANDARD primary key (Id)
)
/

/*==============================================================*/
/* Table: RepairClassification                                  */
/*==============================================================*/
create table RepairClassification  (
   Id                   NUMBER(9)                       not null,
   BranchId             NUMBER(9)                       not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   SalesCategoryName    NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   Name                 VARCHAR2(100)                   not null,
   RepairHours          NUMBER(9)                       not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   Status               NUMBER(9)                       not null,
   RowVersion           TIMESTAMP,
   constraint PK_REPAIRCLASSIFICATION primary key (Id)
)
/

/*==============================================================*/
/* Table: RepairItem                                            */
/*==============================================================*/
create table RepairItem  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   Name                 VARCHAR2(600)                   not null,
   RepairItemCategoryId NUMBER(9)                       not null,
   QuickCode            VARCHAR2(50),
   JobType              NUMBER(9)                       not null,
   RepairQualificationGrade NUMBER(9)                       not null,
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_REPAIRITEM primary key (Id)
)
/

/*==============================================================*/
/* Table: RepairItemAffiServProdLine                            */
/*==============================================================*/
create table RepairItemAffiServProdLine  (
   Id                   NUMBER(9)                       not null,
   BranchId             NUMBER(9)                       not null,
   RepairItemId         NUMBER(9)                       not null,
   ServiceProductLineId NUMBER(9)                       not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   ProductLineType      NUMBER(9)                       not null,
   DefaultLaborHour     NUMBER(15,6)                    not null,
   Remark               VARCHAR2(200),
   Status               NUMBER(9)                       not null,
   EngineRepairPower    NUMBER(9),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   RowVersion           TIMESTAMP,
   constraint PK_REPAIRITEMAFFISERVPRODLINE primary key (Id)
)
/

/*==============================================================*/
/* Table: RepairItemBrandRelation                               */
/*==============================================================*/
create table RepairItemBrandRelation  (
   Id                   NUMBER(9)                       not null,
   BranchId             NUMBER(9)                       not null,
   RepairItemId         NUMBER(9)                       not null,
   RepairItemCode       VARCHAR2(50)                    not null,
   RepairItemName       VARCHAR2(600)                   not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   RepairQualificationGrade NUMBER(9)                       not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   RowVersion           TIMESTAMP,
   constraint PK_REPAIRITEMBRANDRELATION primary key (Id)
)
/

/*==============================================================*/
/* Table: RepairItemCatAffiVehiCat                              */
/*==============================================================*/
create table RepairItemCatAffiVehiCat  (
   Id                   NUMBER(9)                       not null,
   VehicleCategoryId    NUMBER(9)                       not null,
   RepairItemCategoryId NUMBER(9)                       not null,
   constraint PK_REPAIRITEMCATAFFIVEHICAT primary key (Id)
)
/

/*==============================================================*/
/* Table: RepairItemCategory                                    */
/*==============================================================*/
create table RepairItemCategory  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   Name                 VARCHAR2(200)                   not null,
   LayerNodeTypeId      NUMBER(9)                       not null,
   ParentId             NUMBER(9),
   RootGroupId          NUMBER(9)                       not null,
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   STMSCODE             VARCHAR2(64),
   constraint PK_REPAIRITEMCATEGORY primary key (Id)
)
/

/*==============================================================*/
/* Table: RepairItemProdLineHistory                             */
/*==============================================================*/
create table RepairItemProdLineHistory  (
   Id                   NUMBER(9)                       not null,
   RepairItemAffiServProdLineId NUMBER(9)                       not null,
   ProductLineType      NUMBER(9)                       not null,
   BranchId             NUMBER(9)                       not null,
   RepairItemId         NUMBER(9)                       not null,
   ServiceProductLineId NUMBER(9)                       not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   DefaultLaborHour     NUMBER(15,6)                    not null,
   Remark               VARCHAR2(200),
   Status               NUMBER(9)                       not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   constraint PK_REPAIRITEMPRODLINEHISTORY primary key (Id)
)
/

/*==============================================================*/
/* Table: RepairMethodStandard                                  */
/*==============================================================*/
create table RepairMethodStandard  (
   Id                   NUMBER(9)                       not null,
   RepairMethodCode     VARCHAR2(50)                    not null,
   RepairMethodName     VARCHAR2(100)                   not null,
   RepairMethodDescription VARCHAR2(200),
   Status               NUMBER(9)                       not null,
   CreatorId            NUMBER(9)                       not null,
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   constraint PK_REPAIRMETHODSTANDARD primary key (Id)
)
/

/*==============================================================*/
/* Table: RepairObject                                          */
/*==============================================================*/
create table RepairObject  (
   Id                   NUMBER(9)                       not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   PartsSalesCategoryName VARCHAR2(50)                    not null,
   RepairTarget         VARCHAR2(50)                    not null,
   Status               NUMBER(9)                       not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_REPAIROBJECT primary key (Id)
)
/

/*==============================================================*/
/* Table: ServiceActivity                                       */
/*==============================================================*/
create table ServiceActivity  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   Name                 VARCHAR2(100)                   not null,
   BranchId             NUMBER(9)                       not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   Type                 NUMBER(9)                       not null,
   ProductLineType      NUMBER(9)                       not null,
   ServiceProductLineId NUMBER(9)                       not null,
   CanUsedTime          NUMBER(9)                       not null,
   Content              VARCHAR2(200),
   Remark               VARCHAR2(200),
   BillingMethod        NUMBER(9)                       not null,
   RectificationType    NUMBER(9),
   LaborCost            NUMBER(19,4),
   MaterialCost         NUMBER(19,4),
   OtherCost            NUMBER(19,4),
   StartDate            DATE                            not null,
   EndDate              DATE                            not null,
   ApplicationScope     NUMBER(9)                       not null,
   AutomaticApproval    NUMBER(1)                       not null,
   ResponsibleUnitId    NUMBER(9),
   FaultyPartsId        NUMBER(9),
   FaultyPartsSupplierId NUMBER(9),
   FaultyPartsWCId      NUMBER(9),
   DealerQuotaLimit     NUMBER(1)                       not null,
   ActivityCount        NUMBER(9),
   UsedActivityCount    NUMBER(9),
   IfAccountingPartsMngCost NUMBER(1)                       not null,
   Status               NUMBER(9)                       not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_SERVICEACTIVITY primary key (Id)
)
/

/*==============================================================*/
/* Table: ServiceActivityCondition                              */
/*==============================================================*/
create table ServiceActivityCondition  (
   Id                   NUMBER(9)                       not null,
   ServiceActivityId    NUMBER(9)                       not null,
   AttributeName        VARCHAR2(50)                    not null,
   DataType             NUMBER(9)                       not null,
   MathOperator         NUMBER(9)                       not null,
   Value                VARCHAR2(50)                    not null,
   constraint PK_SERVICEACTIVITYCONDITION primary key (Id)
)
/

/*==============================================================*/
/* Table: ServiceActivityDealerVehicle                          */
/*==============================================================*/
create table ServiceActivityDealerVehicle  (
   Id                   NUMBER(9)                       not null,
   ServiceActivityId    NUMBER(9)                       not null,
   VehicleId            NUMBER(9)                       not null,
   DealerId             NUMBER(9)                       not null,
   VIN                  VARCHAR2(50)                    not null,
   AlreadyUsed          NUMBER(1)                       not null,
   constraint PK_SERVICEACTIVITYDEALERVEHICL primary key (Id, ServiceActivityId)
)
/

/*==============================================================*/
/* Table: ServiceActivityMaterialDetail                         */
/*==============================================================*/
create table ServiceActivityMaterialDetail  (
   Id                   NUMBER(9)                       not null,
   SparePartId          NUMBER(9)                       not null,
   UsedPartsCode        VARCHAR2(50)                    not null,
   UsedPartsName        VARCHAR2(100)                   not null,
   PartsWarrantyCategoryId NUMBER(9),
   PartsSalesOrderId    NUMBER(9),
   Quantity             NUMBER(9)                       not null,
   PartsSalesOrderCode  VARCHAR2(50),
   ServiceActivityRepairItemId NUMBER(9)                       not null,
   UsedPartsBatchNumber VARCHAR2(50),
   UsedPartsSupplierId  NUMBER(9)                       not null,
   UsedPartsSupplierCode VARCHAR2(50)                    not null,
   UsedPartsSupplierName VARCHAR2(100)                   not null,
   UnitPrice            NUMBER(19,4)                    not null,
   MaterialCost         NUMBER(19,4)                    not null,
   PartsManagementCost  NUMBER(19,4)                    not null,
   UsedPartsDisposalStatus NUMBER(9)                       not null,
   UsedPartsReturnPolicy NUMBER(9)                       not null,
   NewPartsId           NUMBER(9),
   NewPartsCode         VARCHAR2(50),
   NewPartsName         VARCHAR2(100),
   NewPartsBatchNumber  VARCHAR2(50),
   NewPartsSupplierId   NUMBER(9),
   NewPartsSupplierCode VARCHAR2(50),
   NewPartsSupplierName VARCHAR2(100),
   constraint PK_SERVICEACTIVITYMATLDETA primary key (Id)
)
/

/*==============================================================*/
/* Table: ServiceActivityQuota                                  */
/*==============================================================*/
create table ServiceActivityQuota  (
   Id                   NUMBER(9)                       not null,
   ServiceActivityId    NUMBER(9)                       not null,
   DealerId             NUMBER(9)                       not null,
   Quota                NUMBER(9)                       not null,
   QuotaUsed            NUMBER(9)                       not null,
   constraint PK_SERVICEACTIVITYQUOTA primary key (Id)
)
/

/*==============================================================*/
/* Table: ServiceActivityRepairItem                             */
/*==============================================================*/
create table ServiceActivityRepairItem  (
   Id                   NUMBER(9)                       not null,
   ServiceActivityId    NUMBER(9)                       not null,
   RepairItemId         NUMBER(9)                       not null,
   DefaultLaborHour     NUMBER(15,6)                    not null,
   IfObliged            NUMBER(1)                       not null,
   constraint PK_SERVICEACTIVITYREPAIRITEM primary key (Id)
)
/

/*==============================================================*/
/* Table: ServiceActivityVehicleList                            */
/*==============================================================*/
create table ServiceActivityVehicleList  (
   Id                   NUMBER(9)                       not null,
   ServiceActivityId    NUMBER(9)                       not null,
   VehicleId            NUMBER(9)                       not null,
   VIN                  VARCHAR2(50)                    not null,
   AlreadyUsed          NUMBER(1)                       not null,
   constraint PK_SERVICEACTIVITYVEHICLELIST primary key (Id)
)
/

/*==============================================================*/
/* Table: ServiceTripArea                                       */
/*==============================================================*/
create table ServiceTripArea  (
   Id                   NUMBER(9)                       not null,
   Name                 VARCHAR2(100)                   not null,
   Code                 VARCHAR2(50)                    not null,
   SubsidyPrice         NUMBER(19,4)                    not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   BranchId             NUMBER(9)                       not null,
   PartsSalesCategoryName VARCHAR2(100),
   Status               NUMBER(9)                       not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_SERVICETRIPAREA primary key (Id)
)
/

/*==============================================================*/
/* Table: ServiceTripPriceGrade                                 */
/*==============================================================*/
create table ServiceTripPriceGrade  (
   Id                   NUMBER(9)                       not null,
   BranchId             NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   Name                 VARCHAR2(100)                   not null,
   RegionDescription    VARCHAR2(200)                   not null,
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   PartsSalesCategoryId NUMBER(9)                       not null,
   constraint PK_SERVICETRIPPRICEGRADE primary key (Id)
)
/

/*==============================================================*/
/* Table: ServiceTripPriceRate                                  */
/*==============================================================*/
create table ServiceTripPriceRate  (
   Id                   NUMBER(9)                       not null,
   ServiceTripPriceGradeId NUMBER(9)                       not null,
   ServiceProductLineId NUMBER(9)                       not null,
   ProductLineType      NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   ServiceTripType      NUMBER(9)                       not null,
   MaxMileage           NUMBER(9)                       not null,
   MinMileage           NUMBER(9)                       not null,
   FixedAmount          NUMBER(19,4)                    not null,
   IsSelfCar            NUMBER(1)                       not null,
   SubsidyPrice         NUMBER(19,4)                    not null,
   Price                NUMBER(19,4)                    not null,
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_SERVICETRIPPRICERATE primary key (Id)
)
/

/*==============================================================*/
/* Table: VMainteProductCategoryHist                            */
/*==============================================================*/
create table VMainteProductCategoryHist  (
   id                   NUMBER(9)                       not null,
   VehicleMainteProductCategoryId NUMBER(9)                       not null,
   VehicleMainteTermId  NUMBER(9)                       not null,
   ProductCategoryName  VARCHAR2(200),
   ProductCategoryCode  VARCHAR2(200),
   MaterialCost         NUMBER(19,4),
   Type1                NUMBER(9),
   Quantity             NUMBER(15,6),
   Price                NUMBER(19,4),
   Type1Amount          NUMBER(19,4),
   Type2                NUMBER(9),
   Quantity2            NUMBER(15,6),
   Price2               NUMBER(19,4),
   Type2Amount          NUMBER(19,4),
   constraint PK_VMAINTEPRODUCTCATEGORYHIST primary key (id)
)
/

/*==============================================================*/
/* Table: VehicleAttribute                                      */
/*==============================================================*/
create table VehicleAttribute  (
   Id                   NUMBER(9)                       not null,
   AttributeName        VARCHAR2(50)                    not null,
   DataType             NUMBER(9)                       not null,
   constraint PK_VEHICLEATTRIBUTE primary key (Id)
)
/

/*==============================================================*/
/* Table: VehicleMainteProductCategory                          */
/*==============================================================*/
create table VehicleMainteProductCategory  (
   id                   NUMBER(9)                       not null,
   VehicleMainteTermId  NUMBER(9)                       not null,
   ProductCategoryName  VARCHAR2(200),
   ProductCategoryCode  VARCHAR2(200),
   MaterialCost         NUMBER(19,4),
   Type1                NUMBER(9),
   Quantity             NUMBER(15,6),
   Price                NUMBER(19,4),
   Type1Amount          NUMBER(19,4),
   Type2                NUMBER(9),
   Quantity2            NUMBER(15,6),
   Price2               NUMBER(19,4),
   Type2Amount          NUMBER(19,4),
   constraint PK_VEHICLEMAINTEPRODUCTCATEGOR primary key (id)
)
/

/*==============================================================*/
/* Table: VehicleMainteRepairItem                               */
/*==============================================================*/
create table VehicleMainteRepairItem  (
   Id                   NUMBER(9)                       not null,
   VehicleMainteTermId  NUMBER(9)                       not null,
   RepairItemId         NUMBER(9)                       not null,
   DefaultLaborHour     NUMBER(15,6)                    not null,
   IfObliged            NUMBER(1)                       not null,
   constraint PK_VEHICLEMAINTEREPAIRITEM primary key (Id)
)
/

/*==============================================================*/
/* Table: VehicleMainteRepairItemHist                           */
/*==============================================================*/
create table VehicleMainteRepairItemHist  (
   Id                   NUMBER(9)                       not null,
   VehicleMainteRepairItemId NUMBER(9)                       not null,
   VehicleMainteTermId  NUMBER(9)                       not null,
   RepairItemId         NUMBER(9)                       not null,
   DefaultLaborHour     NUMBER(15,6)                    not null,
   IfObliged            NUMBER(1)                       not null,
   constraint PK_VEHICLEMAINTEREPAIRITEMHIST primary key (Id)
)
/

/*==============================================================*/
/* Table: VehicleMainteTerm                                     */
/*==============================================================*/
create table VehicleMainteTerm  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   Name                 VARCHAR2(100)                   not null,
   WarrantyPolicyId     NUMBER(9)                       not null,
   MainteType           NUMBER(9)                       not null,
   PartsPriceCap        NUMBER(19,4),
   WorkingHours         NUMBER(9)                       not null,
   Capacity             NUMBER(9)                       not null,
   MaxMileage           NUMBER(9)                       not null,
   MinMileage           NUMBER(9)                       not null,
   DaysUpperLimit       NUMBER(9)                       not null,
   DaysLowerLimit       NUMBER(9)                       not null,
   BillingMethod        NUMBER(9)                       not null,
   LaborCost            NUMBER(19,4)                    not null,
   MaterialCost         NUMBER(19,4)                    not null,
   OtherCost            NUMBER(19,4)                    not null,
   DisplayOrder         NUMBER(9),
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_VEHICLEMAINTETERM primary key (Id)
)
/

/*==============================================================*/
/* Table: VehicleMainteTermHistory                              */
/*==============================================================*/
create table VehicleMainteTermHistory  (
   Id                   NUMBER(9)                       not null,
   VehicleMainteTermId  NUMBER(9)                       not null,
   Code                 VARCHAR2(50),
   Name                 VARCHAR2(100),
   MainteType           NUMBER(9)                       not null,
   WarrantyPolicyId     NUMBER(9),
   WorkingHours         NUMBER(9),
   PartsPriceCap        NUMBER(19,4),
   MaxMileage           NUMBER(9),
   MinMileage           NUMBER(9),
   DaysUpperLimit       NUMBER(9),
   DaysLowerLimit       NUMBER(9),
   BillingMethod        NUMBER(9),
   LaborCost            NUMBER(19,4),
   MaterialCost         NUMBER(19,4),
   OtherCost            NUMBER(19,4),
   DisplayOrder         NUMBER(9),
   Status               NUMBER(9),
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   FilerId              NUMBER(9),
   FilerName            VARCHAR2(100),
   FileTime             DATE,
   constraint PK_VEHICLEMAINTETERMHISTORY primary key (Id)
)
/

/*==============================================================*/
/* Table: VehicleWarrantyTerm                                   */
/*==============================================================*/
create table VehicleWarrantyTerm  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   Name                 VARCHAR2(100)                   not null,
   WarrantyPolicyId     NUMBER(9)                       not null,
   MaxMileage           NUMBER(9)                       not null,
   MinMileage           NUMBER(9)                       not null,
   DaysUpperLimit       NUMBER(9)                       not null,
   DaysLowerLimit       NUMBER(9)                       not null,
   Capacity             NUMBER(9)                       not null,
   WorkingHours         NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   Status               NUMBER(9)                       not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_VEHICLEWARRANTYTERM primary key (Id)
)
/

/*==============================================================*/
/* Table: VehicleWarrantyTermHistory                            */
/*==============================================================*/
create table VehicleWarrantyTermHistory  (
   Id                   NUMBER(9)                       not null,
   VehicleWarrantyTermId NUMBER(9)                       not null,
   Code                 VARCHAR2(50),
   Name                 VARCHAR2(100),
   WarrantyPolicyId     NUMBER(9),
   MaxMileage           NUMBER(9),
   MinMileage           NUMBER(9),
   DaysUpperLimit       NUMBER(9),
   DaysLowerLimit       NUMBER(9),
   Capacity             NUMBER(9),
   WorkingHours         NUMBER(9),
   Status               NUMBER(9),
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   FilerId              NUMBER(9),
   FilerName            VARCHAR2(100),
   FileTime             DATE,
   constraint PK_VEHICLEWARRANTYTERMHISTORY primary key (Id)
)
/

/*==============================================================*/
/* Table: VeriCodeEffectTime                                    */
/*==============================================================*/
create table VeriCodeEffectTime  (
   Id                   NUMBER(9)                       not null,
   effectTime           NUMBER(9)                       not null,
   retransTime          NUMBER(9),
   creatorid            NUMBER(9),
   creatorname          VARCHAR2(100),
   createtime           DATE,
   constraint PK_VERICODEEFFECTTIME primary key (Id)
)
/

/*==============================================================*/
/* Table: WarrPolNServProdLineHistory                           */
/*==============================================================*/
create table WarrPolNServProdLineHistory  (
   Id                   NUMBER(9)                       not null,
   WarrantyPolicyNServProdLineId NUMBER(9)                       not null,
   WarrantyPolicyId     NUMBER(9),
   ServiceProductLineId NUMBER(9),
   ValidationDate       DATE,
   ExpireDate           DATE,
   ProductLineType      NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   Status               NUMBER(9),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   constraint PK_WARRPOLNSERVPRODLINEHISTORY primary key (Id)
)
/

/*==============================================================*/
/* Table: WarrantyPolicy                                        */
/*==============================================================*/
create table WarrantyPolicy  (
   Id                   NUMBER(9)                       not null,
   BranchId             NUMBER(9)                       not null,
   BranchCode           VARCHAR2(50)                    not null,
   BranchName           VARCHAR2(100)                   not null,
   Code                 VARCHAR2(50)                    not null,
   Name                 VARCHAR2(100)                   not null,
   Category             NUMBER(9)                       not null,
   ApplicationScope     NUMBER(9)                       not null,
   RepairTermInvolved   NUMBER(1)                       not null,
   MainteTermInvolved   NUMBER(1)                       not null,
   PartsWarrantyTermInvolved NUMBER(1)                       not null,
   ReleaseDate          DATE,
   ExecutionDate        DATE,
   Remark               VARCHAR2(200),
   Status               NUMBER(9)                       not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_WARRANTYPOLICY primary key (Id)
)
/

/*==============================================================*/
/* Table: WarrantyPolicyHistory                                 */
/*==============================================================*/
create table WarrantyPolicyHistory  (
   Id                   NUMBER(9)                       not null,
   MasterRecordId       NUMBER(9)                       not null,
   Code                 VARCHAR2(50),
   Name                 VARCHAR2(100),
   Category             NUMBER(9),
   ApplicationScope     NUMBER(9),
   RepairTermInvolved   NUMBER(1),
   MainteTermInvolved   NUMBER(1),
   PartsWarrantyTermInvolved NUMBER(1),
   ReleaseDate          DATE,
   ExecutionDate        DATE,
   Remark               VARCHAR2(200),
   Status               NUMBER(9),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   FilerId              NUMBER(9),
   FilerName            VARCHAR2(100),
   FileTime             DATE,
   constraint PK_WARRANTYPOLICYHISTORY primary key (Id)
)
/

/*==============================================================*/
/* Table: WarrantyPolicyNServProdLine                           */
/*==============================================================*/
create table WarrantyPolicyNServProdLine  (
   Id                   NUMBER(9)                       not null,
   WarrantyPolicyId     NUMBER(9)                       not null,
   ServiceProductLineId NUMBER(9)                       not null,
   ValidationDate       DATE                            not null,
   ExpireDate           DATE                            not null,
   ProductLineType      NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   Status               NUMBER(9)                       not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_WARRANTYPOLICYNSERVPRODLINE primary key (Id)
)
/

/*==============================================================*/
/* Table: qtsinfo                                               */
/*==============================================================*/
create table qtsinfo  (
   ID                   NUMBER(9),
   DETAILID             VARCHAR2(50)                    not null,
   CSCODE               VARCHAR2(25)                    not null,
   ZCBMCODE             VARCHAR2(100)                   not null,
   VINCODE              VARCHAR2(40)                    not null,
   ASSEMBLY_DATE        DATE,
   BIND_DATE            DATE,
   FACTORY              VARCHAR2(20),
   MAPID                VARCHAR2(50),
   MAPNAME              VARCHAR2(100),
   PATCH                VARCHAR2(20),
   PRODUCELINE          VARCHAR2(20),
   SUBMITTIME           DATE,
   CHUCHANGID           VARCHAR2(20),
   SYSCODE              VARCHAR2(20)                    not null
)
/

