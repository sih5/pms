/*==============================================================*/
/* DBMS name:      ORACLE Version 10gR2                         */
/* Created on:     2015/6/16 15:49:49                           */
/*==============================================================*/


declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('DealerInvoiceInformation');
  if num>0 then
    execute immediate 'drop table DealerInvoiceInformation cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('SettleBillInvoiceLink');
  if num>0 then
    execute immediate 'drop table SettleBillInvoiceLink cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('SsClaimSettleInstruction');
  if num>0 then
    execute immediate 'drop table SsClaimSettleInstruction cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('SsClaimSettlementBill');
  if num>0 then
    execute immediate 'drop table SsClaimSettlementBill cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('SsClaimSettlementDetail');
  if num>0 then
    execute immediate 'drop table SsClaimSettlementDetail cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('SupClaimSettleInstruction');
  if num>0 then
    execute immediate 'drop table SupClaimSettleInstruction cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('SupplierClaimSettleBill');
  if num>0 then
    execute immediate 'drop table SupplierClaimSettleBill cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('SupplierClaimSettleDetail');
  if num>0 then
    execute immediate 'drop table SupplierClaimSettleDetail cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_DealerInvoiceInformation');
  if num>0 then
    execute immediate 'drop sequence S_DealerInvoiceInformation';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_SettleBillInvoiceLink');
  if num>0 then
    execute immediate 'drop sequence S_SettleBillInvoiceLink';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_SsClaimSettleInstruction');
  if num>0 then
    execute immediate 'drop sequence S_SsClaimSettleInstruction';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_SsClaimSettlementBill');
  if num>0 then
    execute immediate 'drop sequence S_SsClaimSettlementBill';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_SsClaimSettlementDetail');
  if num>0 then
    execute immediate 'drop sequence S_SsClaimSettlementDetail';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_SupClaimSettleInstruction');
  if num>0 then
    execute immediate 'drop sequence S_SupClaimSettleInstruction';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_SupplierClaimSettleBill');
  if num>0 then
    execute immediate 'drop sequence S_SupplierClaimSettleBill';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_SupplierClaimSettleDetail');
  if num>0 then
    execute immediate 'drop sequence S_SupplierClaimSettleDetail';
  end if;
end;
/

create sequence S_DealerInvoiceInformation
/

create sequence S_SettleBillInvoiceLink
/

create sequence S_SsClaimSettleInstruction
/

create sequence S_SsClaimSettlementBill
/

create sequence S_SsClaimSettlementDetail
/

create sequence S_SupClaimSettleInstruction
/

create sequence S_SupplierClaimSettleBill
/

create sequence S_SupplierClaimSettleDetail
/

/*==============================================================*/
/* Table: DealerInvoiceInformation                              */
/*==============================================================*/
create table DealerInvoiceInformation  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   InvoiceCode          VARCHAR2(50)                    not null,
   InvoiceNumber        VARCHAR2(50)                    not null,
   ClaimSettlementBillCode VARCHAR2(500)                   not null,
   BranchId             NUMBER(9)                       not null,
   BranchCode           VARCHAR2(50)                    not null,
   BranchName           VARCHAR2(100)                   not null,
   DealerId             NUMBER(9)                       not null,
   DealerCode           VARCHAR2(50)                    not null,
   DealerName           VARCHAR2(100)                   not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   InvoiceFee           NUMBER(19,4)                    not null,
   TaxRate              NUMBER(15,6)                    not null,
   InvoiceTaxFee        NUMBER(19,4)                    not null,
   Status               NUMBER(9)                       not null,
   InvoiceDate          DATE                            not null,
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   InitialApproverId    NUMBER(9),
   InitialApproverName  VARCHAR2(100),
   InitialApproverTime  DATE,
   InitialApproverComment VARCHAR2(200),
   ApproverId           NUMBER(9),
   ApproverName         VARCHAR2(100),
   ApproveTime          DATE,
   ApproveComment       VARCHAR2(200),
   RejectId             NUMBER(9),
   RejectName           VARCHAR2(100),
   RejectTime           DATE,
   RejectComment        VARCHAR2(200),
   ApproveCommentHistory VARCHAR2(2000),
   RowVersion           TIMESTAMP,
   SettleType           NUMBER(9),
   constraint PK_DEALERINVOICEINFORMATION primary key (Id)
)
/

/*==============================================================*/
/* Table: SettleBillInvoiceLink                                 */
/*==============================================================*/
create table SettleBillInvoiceLink  (
   Id                   NUMBER(9)                       not null,
   DealerInvoiceId      NUMBER(9)                       not null,
   ClaimSettlementBillId NUMBER(9)                       not null,
   constraint PK_SETTLEBILLINVOICELINK primary key (Id)
)
/

/*==============================================================*/
/* Table: SsClaimSettleInstruction                              */
/*==============================================================*/
create table SsClaimSettleInstruction  (
   Id                   NUMBER(9)                       not null,
   BranchId             NUMBER(9)                       not null,
   BranchCode           VARCHAR2(50)                    not null,
   BranchName           VARCHAR2(200)                   not null,
   PartsSalesCategoryId NUMBER(9),
   ServiceProductLineId NUMBER(9),
   SettleMethods        NUMBER(9),
   ProductLineType      NUMBER(9),
   Status               NUMBER(9)                       not null,
   SettlementStartTime  DATE,
   SettlementEndTime    DATE                            not null,
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ExecutionTime        DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   ExecutionResult      VARCHAR2(200),
   constraint PK_SSCLAIMSETTLEINSTRUCTION primary key (Id)
)
/

/*==============================================================*/
/* Table: SsClaimSettlementBill                                 */
/*==============================================================*/
create table SsClaimSettlementBill  (
   RelatedId            NUMBER(9),
   SettleType           NUMBER(9),
   SettleMethods        NUMBER(9),
   IfInvoiced           NUMBER(1)                       not null,
   Status               NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   DealerId             NUMBER(9)                       not null,
   DealerCode           VARCHAR2(50)                    not null,
   DealerName           VARCHAR2(100)                   not null,
   BranchId             NUMBER(9)                       not null,
   BranchCode           VARCHAR2(50)                    not null,
   BranchName           VARCHAR2(100)                   not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   ServiceProductLineId NUMBER(9),
   ProductLineType      NUMBER(9),
   SettlementStartTime  DATE,
   SettlementEndTime    DATE                            not null,
   LaborCost            NUMBER(19,4)                    not null,
   MaterialCost         NUMBER(19,4)                    not null,
   PartsManagementCost  NUMBER(19,4)                    not null,
   FieldServiceExpense  NUMBER(19,4)                    not null,
   DebitAmount          NUMBER(19,4)                    not null,
   ComplementAmount     NUMBER(19,4)                    not null,
   PreSaleAmount        NUMBER(19,4),
   OtherCost            NUMBER(19,4)                    not null,
   UsedPartsEncourageAmount NUMBER(19,4),
   TotalAmount          NUMBER(19,4)                    not null,
   TaxRate              NUMBER(15,6),
   InvoiceAmountDifference NUMBER(19,4),
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   ApproverId           NUMBER(9),
   ApproverName         VARCHAR2(100),
   ApproveTime          DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   Id                   NUMBER(9)                       not null,
   constraint PK_SSCLAIMSETTLEMENTBILL primary key (Id)
)
/

/*==============================================================*/
/* Table: SsClaimSettlementDetail                               */
/*==============================================================*/
create table SsClaimSettlementDetail  (
   Id                   NUMBER(9)                       not null,
   SsClaimSettlementBillId NUMBER(9)                       not null,
   SourceId             NUMBER(9)                       not null,
   SourceCode           VARCHAR2(50)                    not null,
   SourceType           NUMBER(9)                       not null,
   LaborCost            NUMBER(19,4)                    not null,
   MaterialCost         NUMBER(19,4)                    not null,
   PartsManagementCost  NUMBER(19,4)                    not null,
   FieldServiceExpense  NUMBER(19,4)                    not null,
   DebitAmount          NUMBER(19,4)                    not null,
   ComplementAmount     NUMBER(19,4)                    not null,
   PreSaleAmount        NUMBER(19,4),
   OtherCost            NUMBER(19,4)                    not null,
   TotalAmount          NUMBER(19,4)                    not null,
   UsedPartsEncourageAmount NUMBER(19,4),
   constraint PK_SSCLAIMSETTLEMENTDETAIL primary key (Id)
)
/

/*==============================================================*/
/* Table: SupClaimSettleInstruction                             */
/*==============================================================*/
create table SupClaimSettleInstruction  (
   Id                   NUMBER(9)                       not null,
   BranchId             NUMBER(9)                       not null,
   BranchCode           VARCHAR2(50)                    not null,
   BranchName           VARCHAR2(200)                   not null,
   PartsSalesCategoryId NUMBER(9),
   ServiceProductLineId NUMBER(9),
   ProductLineType      NUMBER(9),
   Status               NUMBER(9)                       not null,
   SettlementStartTime  DATE,
   SettlementEndTime    DATE                            not null,
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ExecutionTime        DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   ExecutionResult      VARCHAR2(200),
   constraint PK_SUPCLAIMSETTLEINSTRUCTION primary key (Id)
)
/

/*==============================================================*/
/* Table: SupplierClaimSettleBill                               */
/*==============================================================*/
create table SupplierClaimSettleBill  (
   Id                   NUMBER(9)                       not null,
   IfInvoiced           NUMBER(1)                       not null,
   Status               NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   SupplierId           NUMBER(9)                       not null,
   SupplierCode         VARCHAR2(50)                    not null,
   SupplierName         VARCHAR2(100)                   not null,
   BranchId             NUMBER(9)                       not null,
   BranchCode           VARCHAR2(50)                    not null,
   BranchName           VARCHAR2(100)                   not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   ServiceProductLineId NUMBER(9),
   ProductLineType      NUMBER(9),
   SettlementStartTime  DATE,
   SettlementEndTime    DATE                            not null,
   LaborCost            NUMBER(19,4)                    not null,
   MaterialCost         NUMBER(19,4)                    not null,
   PartsManagementCost  NUMBER(19,4)                    not null,
   FieldServiceExpense  NUMBER(19,4)                    not null,
   DebitAmount          NUMBER(19,4)                    not null,
   ComplementAmount     NUMBER(19,4)                    not null,
   OtherCost            NUMBER(19,4)                    not null,
   OldPartTranseCost    NUMBER(19,4),
   UsedPartsEncourageAmount NUMBER(19,4),
   TotalAmount          NUMBER(19,4)                    not null,
   TaxRate              NUMBER(15,6),
   InvoiceAmountDifference NUMBER(19,4),
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   ApproverId           NUMBER(9),
   ApproverName         VARCHAR2(100),
   ApproveTime          DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   constraint PK_SUPPLIERCLAIMSETTLEBILL primary key (Id)
)
/

/*==============================================================*/
/* Table: SupplierClaimSettleDetail                             */
/*==============================================================*/
create table SupplierClaimSettleDetail  (
   Id                   NUMBER(9)                       not null,
   SupplierClaimSettlementBillId NUMBER(9)                       not null,
   SourceId             NUMBER(9)                       not null,
   SourceCode           VARCHAR2(50)                    not null,
   SourceType           NUMBER(9)                       not null,
   LaborCost            NUMBER(19,4)                    not null,
   MaterialCost         NUMBER(19,4)                    not null,
   PartsManagementCost  NUMBER(19,4)                    not null,
   FieldServiceExpense  NUMBER(19,4)                    not null,
   DebitAmount          NUMBER(19,4)                    not null,
   ComplementAmount     NUMBER(19,4)                    not null,
   OtherCost            NUMBER(19,4)                    not null,
   OldPartTranseCost    NUMBER(19,4),
   TotalAmount          NUMBER(19,4)                    not null,
   constraint PK_SUPPLIERCLAIMSETTLEDETAIL primary key (Id)
)
/

