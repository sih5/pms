/*==============================================================*/
/* DBMS name:      ORACLE Version 10gR2                         */
/* Created on:     2013/4/15 10:42:44                           */
/*==============================================================*/


declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('ChannelPlan');
  if num>0 then
    execute immediate 'drop table ChannelPlan cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('ChannelPlanDetail');
  if num>0 then
    execute immediate 'drop table ChannelPlanDetail cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('ChannelPlanHistory');
  if num>0 then
    execute immediate 'drop table ChannelPlanHistory cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('ChannelPlanHistoryDetail');
  if num>0 then
    execute immediate 'drop table ChannelPlanHistoryDetail cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_ChannelPlan');
  if num>0 then
    execute immediate 'drop sequence S_ChannelPlan';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_ChannelPlanDetail');
  if num>0 then
    execute immediate 'drop sequence S_ChannelPlanDetail';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_ChannelPlanHistory');
  if num>0 then
    execute immediate 'drop sequence S_ChannelPlanHistory';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_ChannelPlanHistoryDetail');
  if num>0 then
    execute immediate 'drop sequence S_ChannelPlanHistoryDetail';
  end if;
end;
/

create sequence S_ChannelPlan
/

create sequence S_ChannelPlanDetail
/

create sequence S_ChannelPlanHistory
/

create sequence S_ChannelPlanHistoryDetail
/

/*==============================================================*/
/* Table: ChannelPlan                                           */
/*==============================================================*/
create table ChannelPlan  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   BrandId              NUMBER(9)                       not null,
   Description          VARCHAR2(500),
   ValidFrom            DATE                            not null,
   ValidTo              DATE                            not null,
   ExecuteObjectDesc    VARCHAR2(500),
   Status               NUMBER(9)                       not null,
   CreatorId            NUMBER(9)                       not null,
   CreatorName          VARCHAR2(100)                   not null,
   CreateTime           DATE                            not null,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   Remark               VARCHAR2(200),
   RowVersion           TIMESTAMP,
   constraint PK_CHANNELPLAN primary key (Id)
)
/

/*==============================================================*/
/* Table: ChannelPlanDetail                                     */
/*==============================================================*/
create table ChannelPlanDetail  (
   Id                   NUMBER(9)                       not null,
   ParentId             NUMBER(9)                       not null,
   ChannelCapabilityId  NUMBER(9)                       not null,
   Amount               NUMBER(9)                       not null,
   RegionId             NUMBER(9)                       not null,
   RegionScopeDesc      VARCHAR2(500)                   not null,
   constraint PK_CHANNELPLANDETAIL primary key (Id)
)
/

/*==============================================================*/
/* Table: ChannelPlanHistory                                    */
/*==============================================================*/
create table ChannelPlanHistory  (
   Id                   NUMBER(9)                       not null,
   ChannelPlanId        NUMBER(9)                       not null,
   Code                 VARCHAR2(50),
   BrandId              NUMBER(9),
   Description          VARCHAR2(500),
   ValidFrom            DATE,
   ValidTo              DATE,
   ExecuteObjectDesc    VARCHAR2(500),
   Status               NUMBER(9)                       not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   FilerId              NUMBER(9),
   FilerName            VARCHAR2(100),
   FileTime             DATE,
   Remark               VARCHAR2(200),
   constraint PK_CHANNELPLANHISTORY primary key (Id)
)
/

/*==============================================================*/
/* Table: ChannelPlanHistoryDetail                              */
/*==============================================================*/
create table ChannelPlanHistoryDetail  (
   Id                   NUMBER(9)                       not null,
   ParentId             NUMBER(9)                       not null,
   ChannelCapabilityId  NUMBER(9)                       not null,
   Amount               NUMBER(9)                       not null,
   RegionId             NUMBER(9)                       not null,
   RegionScopeDesc      VARCHAR2(500),
   constraint PK_CHANNELPLANHISTORYDETAIL primary key (Id)
)
/

