/*==============================================================*/
/* DBMS name:      ORACLE Version 10gR2                         */
/* Created on:     2017/12/18 9:57:05                           */
/*==============================================================*/


declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('ChassisInformation');
  if num>0 then
    execute immediate 'drop table ChassisInformation cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('Dtp_VehicleInformation');
  if num>0 then
    execute immediate 'drop table Dtp_VehicleInformation cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('EngineInformation');
  if num>0 then
    execute immediate 'drop table EngineInformation cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('Sep_Dtp_VehicleInformation');
  if num>0 then
    execute immediate 'drop table Sep_Dtp_VehicleInformation cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('VehicleChangeMsg');
  if num>0 then
    execute immediate 'drop table VehicleChangeMsg cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('VehicleExtensionInformation');
  if num>0 then
    execute immediate 'drop table VehicleExtensionInformation cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('VehicleInformation');
  if num>0 then
    execute immediate 'drop table VehicleInformation cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_ChassisInformation');
  if num>0 then
    execute immediate 'drop sequence S_ChassisInformation';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_Dtp_VehicleInformation');
  if num>0 then
    execute immediate 'drop sequence S_Dtp_VehicleInformation';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_EngineInformation');
  if num>0 then
    execute immediate 'drop sequence S_EngineInformation';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_Sep_Dtp_VehicleInformation');
  if num>0 then
    execute immediate 'drop sequence S_Sep_Dtp_VehicleInformation';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_VehicleChangeMsg');
  if num>0 then
    execute immediate 'drop sequence S_VehicleChangeMsg';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_VehicleExtensionInformation');
  if num>0 then
    execute immediate 'drop sequence S_VehicleExtensionInformation';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_VehicleInformation');
  if num>0 then
    execute immediate 'drop sequence S_VehicleInformation';
  end if;
end;
/

create sequence S_ChassisInformation
/

create sequence S_Dtp_VehicleInformation
/

create sequence S_EngineInformation
/

create sequence S_Sep_Dtp_VehicleInformation
/

create sequence S_VehicleChangeMsg
/

create sequence S_VehicleExtensionInformation
/

create sequence S_VehicleInformation
/

/*==============================================================*/
/* Table: ChassisInformation                                    */
/*==============================================================*/
create table ChassisInformation  (
   Id                   NUMBER(9)                       not null,
   SerialNumber         VARCHAR2(50)                    not null,
   ResponsibleUnitId    NUMBER(9),
   ResponsibleUnitName  VARCHAR2(100),
   VehicleCategoryId    NUMBER(9)                       not null,
   VehicleCategoryName  VARCHAR2(100)                   not null,
   TrademarkBrand       VARCHAR2(50),
   ProductId            NUMBER(9)                       not null,
   ProductCode          DATE,
   SalesDate            DATE,
   OutOfFactoryDate     DATE,
   Status               NUMBER(9),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_CHASSISINFORMATION primary key (Id)
)
/

/*==============================================================*/
/* Table: Dtp_VehicleInformation                                */
/*==============================================================*/
create table Dtp_VehicleInformation  (
   Id                   NUMBER(9)                       not null,
   DMSID                VARCHAR2(38)                    not null,
   VIN                  VARCHAR2(50)                    not null,
   OldVIN               VARCHAR2(50)                    not null,
   ProductCode          VARCHAR2(50)                    not null,
   ChassisCode          VARCHAR2(50),
   ChassisBrandName     VARCHAR2(100),
   CUSTOMCODE           VARCHAR2(50)                    not null,
   VehicleLicensePlate  VARCHAR2(50),
   VehicleCategoryName  VARCHAR2(100),
   IsWorkOff            NUMBER(1)                       not null,
   BrandName            VARCHAR2(100)                   not null,
   ProductLineName      VARCHAR2(50)                    not null,
   ProductCategoryName  VARCHAR2(200),
   ProductCategoryCode  VARCHAR2(50),
   AnnoucementNumber    VARCHAR2(50),
   EngineModel          VARCHAR2(50)                    not null,
   EngineSerialNumber   VARCHAR2(50)                    not null,
   BridgeType           NUMBER(9),
   IsRoadVehicl         NUMBER(1)                       not null,
   CageCategory         VARCHAR2(50),
   DealerName           VARCHAR2(100),
   FirstStoppageMileage NUMBER(9),
   OutOfFactoryDate     DATE,
   SalesDate            DATE,
   FrontAxleCode        VARCHAR2(50),
   SecondFrontAxleCode  VARCHAR2(50),
   CentreAxleCode       VARCHAR2(50),
   BehindAxleCode       VARCHAR2(50),
   BalanceShaftCode     VARCHAR2(50),
   GearSerialNumber     VARCHAR2(50),
   VehicleSeries        VARCHAR2(50),
   DriveModeCode        VARCHAR2(50),
   BehindAxleTypTonneLevel VARCHAR2(50),
   AdaptCompanyName     VARCHAR2(100),
   TopsCode             VARCHAR2(50),
   VehicleFunction      VARCHAR2(50)                    not null,
   ProductCategoryTerrace VARCHAR2(50)                    not null,
   GearModel            VARCHAR2(50),
   AdaptType            NUMBER(9),
   SteeringEngine       VARCHAR2(50),
   ARMLENGTH            NUMBER(9),
   GEARPUMPMODEL        VARCHAR2(50),
   GEARPUMPNUMBER       VARCHAR2(50),
   MASTERCYLINDERMODEL  VARCHAR2(50),
   MASTERCYLINDERNUMBER VARCHAR2(50),
   CONVEYINGCYLINDERMODEL VARCHAR2(50),
   CONVEYINGCYLINDERNUMBER VARCHAR2(50),
   ARMSUPPORTPUMPMODEL  VARCHAR2(50),
   ARMSUPPORTPUMPNUMBER VARCHAR2(50),
   ROLLINGREDUCERMODEL  VARCHAR2(50),
   ROLLINGREDUCERNUMBER VARCHAR2(50),
   MOTORMODEL           VARCHAR2(50),
   MOTORNUMBER          VARCHAR2(50),
   OILPUMPMODEL         VARCHAR2(50),
   OILPUMPNUMBER        VARCHAR2(50),
   RADIATORMODEL        VARCHAR2(50),
   RADIATORNUMBER       VARCHAR2(50),
   REDUCERMODEL         VARCHAR2(50),
   REDUCERNUMBER        VARCHAR2(50),
   IPCMODEL             VARCHAR2(50),
   IPCNUMBER            VARCHAR2(50),
   ELECTRICCONTROLCABINETMODEL VARCHAR2(50),
   ELECTRICCONTROLCABINETNUMBER VARCHAR2(50),
   MIXERMODEL           VARCHAR2(50),
   MIXERNUMBER          VARCHAR2(50),
   AIRCOMPRESSORMODEL   VARCHAR2(50),
   AIRCOMPRESSORNUMBER  VARCHAR2(50),
   GASTANKMODEL         VARCHAR2(50),
   GASTANKNUMBER        VARCHAR2(50),
   SCREWCONVEYORMODEL   VARCHAR2(50),
   SCREWCONVEYORNUMBER  VARCHAR2(50),
   DUSTINGMACHINEMODEL  VARCHAR2(50),
   DUSTINGMACHINENUMBER VARCHAR2(50),
   CONTROLSYSTEMMODEL   VARCHAR2(50),
   CONTROLSYSTENUMBER   VARCHAR2(50),
   INCLINEDBELTMODEL    VARCHAR2(50),
   INCLINEDBELTNUMBER   VARCHAR2(50),
   Remark               VARCHAR2(200),
   Status               NUMBER(9),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   SYSCODE              VARCHAR2(30)                    not null,
   VIPVehicle           NUMBER(1)                       not null,
   WorkingHours         NUMBER(9),
   LastMaintenanceWorkingHours NUMBER(9),
   LastMaintenanceCapacity NUMBER(9),
   LastMaintenanceMileage NUMBER(9),
   LastMaintenanceTime  DATE,
   SalesInvoiceNumber   VARCHAR2(50),
   InvoiceDate          DATE,
   VehicleType          NUMBER(9),
   SalesMarketingDepName VARCHAR2(50),
   PreSaleMaxTime       NUMBER(9),
   PreSaleTime          NUMBER(9),
   TRANSFERCASEMODEL    VARCHAR2(50),
   TRANSFERCASENUMBER   VARCHAR2(50),
   MSINOILPUMPMODEL     VARCHAR2(50),
   MSINOILPUMPNUMBER    VARCHAR2(50),
   CONSTANTPRESSUREPUMPMODEL VARCHAR2(50),
   CONSTANTPRESSUREPUMPNUMBER VARCHAR2(50),
   Mileage              NUMBER(9),
   ResponsibleUnitName  VARCHAR2(100),
   VehicleColor         VARCHAR2(50),
   EngineManufacture    VARCHAR2(100),
   Capacity             NUMBER(9),
   THEDATE              DATE                            not null,
   RECEIVETIME          DATE,
   HANDLESTATUS         NUMBER(9),
   HANDLETIME           DATE,
   HANDLEMESSAGE        VARCHAR2(500),
   constraint PK_DTP_VEHICLEINFORMATION primary key (Id)
)
/

/*==============================================================*/
/* Table: EngineInformation                                     */
/*==============================================================*/
create table EngineInformation  (
   Id                   NUMBER(9)                       not null,
   SerialNumber         VARCHAR2(50)                    not null,
   constraint PK_ENGINEINFORMATION primary key (Id)
)
/

/*==============================================================*/
/* Table: Sep_Dtp_VehicleInformation                            */
/*==============================================================*/
create table Sep_Dtp_VehicleInformation  (
   Id                   NUMBER(9)                       not null,
   DMSID                VARCHAR2(38)                    not null,
   VIN                  VARCHAR2(50)                    not null,
   OldVIN               VARCHAR2(50)                    not null,
   ProductCode          VARCHAR2(50)                    not null,
   ChassisCode          VARCHAR2(50),
   ChassisBrandName     VARCHAR2(100),
   CUSTOMCODE           VARCHAR2(50)                    not null,
   VehicleLicensePlate  VARCHAR2(50),
   VehicleCategoryName  VARCHAR2(100),
   IsWorkOff            NUMBER(1)                       not null,
   BrandName            VARCHAR2(100)                   not null,
   ProductLineName      VARCHAR2(50)                    not null,
   ProductCategoryName  VARCHAR2(200),
   ProductCategoryCode  VARCHAR2(50),
   AnnoucementNumber    VARCHAR2(50),
   EngineModel          VARCHAR2(50)                    not null,
   EngineSerialNumber   VARCHAR2(50)                    not null,
   BridgeType           NUMBER(9),
   IsRoadVehicl         NUMBER(1)                       not null,
   CageCategory         VARCHAR2(50),
   DealerName           VARCHAR2(100),
   FirstStoppageMileage NUMBER(9),
   OutOfFactoryDate     DATE,
   SalesDate            DATE,
   FrontAxleCode        VARCHAR2(50),
   SecondFrontAxleCode  VARCHAR2(50),
   CentreAxleCode       VARCHAR2(50),
   BehindAxleCode       VARCHAR2(50),
   BalanceShaftCode     VARCHAR2(50),
   GearSerialNumber     VARCHAR2(50),
   VehicleSeries        VARCHAR2(50),
   DriveModeCode        VARCHAR2(50),
   BehindAxleTypTonneLevel VARCHAR2(50),
   AdaptCompanyName     VARCHAR2(100),
   TopsCode             VARCHAR2(50),
   VehicleFunction      VARCHAR2(50)                    not null,
   ProductCategoryTerrace VARCHAR2(50)                    not null,
   GearModel            VARCHAR2(50),
   AdaptType            NUMBER(9),
   SteeringEngine       VARCHAR2(50),
   ARMLENGTH            NUMBER(9),
   GEARPUMPMODEL        VARCHAR2(50),
   GEARPUMPNUMBER       VARCHAR2(50),
   MASTERCYLINDERMODEL  VARCHAR2(50),
   MASTERCYLINDERNUMBER VARCHAR2(50),
   CONVEYINGCYLINDERMODEL VARCHAR2(50),
   CONVEYINGCYLINDERNUMBER VARCHAR2(50),
   ARMSUPPORTPUMPMODEL  VARCHAR2(50),
   ARMSUPPORTPUMPNUMBER VARCHAR2(50),
   ROLLINGREDUCERMODEL  VARCHAR2(50),
   ROLLINGREDUCERNUMBER VARCHAR2(50),
   MOTORMODEL           VARCHAR2(50),
   MOTORNUMBER          VARCHAR2(50),
   OILPUMPMODEL         VARCHAR2(50),
   OILPUMPNUMBER        VARCHAR2(50),
   RADIATORMODEL        VARCHAR2(50),
   RADIATORNUMBER       VARCHAR2(50),
   REDUCERMODEL         VARCHAR2(50),
   REDUCERNUMBER        VARCHAR2(50),
   IPCMODEL             VARCHAR2(50),
   IPCNUMBER            VARCHAR2(50),
   ELECTRICCONTROLCABINETMODEL VARCHAR2(50),
   ELECTRICCONTROLCABINETNUMBER VARCHAR2(50),
   MIXERMODEL           VARCHAR2(50),
   MIXERNUMBER          VARCHAR2(50),
   AIRCOMPRESSORMODEL   VARCHAR2(50),
   AIRCOMPRESSORNUMBER  VARCHAR2(50),
   GASTANKMODEL         VARCHAR2(50),
   GASTANKNUMBER        VARCHAR2(50),
   SCREWCONVEYORMODEL   VARCHAR2(50),
   SCREWCONVEYORNUMBER  VARCHAR2(50),
   DUSTINGMACHINEMODEL  VARCHAR2(50),
   DUSTINGMACHINENUMBER VARCHAR2(50),
   CONTROLSYSTEMMODEL   VARCHAR2(50),
   CONTROLSYSTENUMBER   VARCHAR2(50),
   INCLINEDBELTMODEL    VARCHAR2(50),
   INCLINEDBELTNUMBER   VARCHAR2(50),
   Remark               VARCHAR2(200),
   Status               NUMBER(9),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   SYSCODE              VARCHAR2(30)                    not null,
   VIPVehicle           NUMBER(1)                       not null,
   WorkingHours         NUMBER(9),
   LastMaintenanceWorkingHours NUMBER(9),
   LastMaintenanceCapacity NUMBER(9),
   LastMaintenanceMileage NUMBER(9),
   LastMaintenanceTime  DATE,
   SalesInvoiceNumber   VARCHAR2(50),
   InvoiceDate          DATE,
   VehicleType          NUMBER(9),
   SalesMarketingDepName VARCHAR2(50),
   PreSaleMaxTime       NUMBER(9),
   PreSaleTime          NUMBER(9),
   TRANSFERCASEMODEL    VARCHAR2(50),
   TRANSFERCASENUMBER   VARCHAR2(50),
   MSINOILPUMPMODEL     VARCHAR2(50),
   MSINOILPUMPNUMBER    VARCHAR2(50),
   CONSTANTPRESSUREPUMPMODEL VARCHAR2(50),
   CONSTANTPRESSUREPUMPNUMBER VARCHAR2(50),
   Mileage              NUMBER(9),
   ResponsibleUnitName  VARCHAR2(100),
   VehicleColor         VARCHAR2(50),
   EngineManufacture    VARCHAR2(100),
   Capacity             NUMBER(9),
   THEDATE              DATE                            not null,
   RECEIVETIME          DATE,
   HANDLESTATUS         NUMBER(9),
   HANDLETIME           DATE,
   HANDLEMESSAGE        VARCHAR2(500),
   constraint PK_SEP_DTP_VEHICLEINFORMATION primary key (Id)
)
/

/*==============================================================*/
/* Table: VehicleChangeMsg                                      */
/*==============================================================*/
create table VehicleChangeMsg  (
   Id                   NUMBER(9)                       not null,
   VIN                  VARCHAR2(50)                    not null,
   OldVIN               VARCHAR2(50)                    not null,
   AVIN                 VARCHAR2(50)                    not null,
   VehicleLicensePlate  VARCHAR2(50),
   BVehicleLicensePlate VARCHAR2(50),
   CustomerName         VARCHAR2(100),
   BCustomerName        VARCHAR2(100),
   CellPhoneNumber      VARCHAR2(50),
   BCellPhoneNumber     VARCHAR2(50),
   SalesStatus          NUMBER(9),
   BSalesStatus         NUMBER(9),
   SalesDate            DATE,
   BSalesDate           DATE,
   OutOfFactoryDate     DATE,
   BOutOfFactoryDate    DATE,
   ResponsibleUnit      VARCHAR2(100),
   BResponsibleUnit     VARCHAR2(100),
   VehicleCode          VARCHAR2(50),
   BVehicleCode         VARCHAR2(50),
   VehicleSeries        VARCHAR2(50),
   BVehicleSeries       VARCHAR2(50),
   BrandName            VARCHAR2(100),
   BBrandName           VARCHAR2(100),
   ProductLineName      VARCHAR2(50),
   BProductLineName     VARCHAR2(50),
   CategoryName         VARCHAR2(200),
   BCategoryName        VARCHAR2(200),
   IsRoadVehicl         NUMBER(1),
   VehicleType          NUMBER(9),
   BVehicleType         NUMBER(9),
   EngineModel          NUMBER(9),
   BEngineModel         NUMBER(9),
   EngineModelCode      VARCHAR2(50),
   BEngineModelCode     VARCHAR2(50),
   GearModel            VARCHAR2(50),
   BGearModel           VARCHAR2(50),
   GPSNUMBER            VARCHAR2(50),
   BGPSNUMBER           VARCHAR2(50),
   SIMCARD              VARCHAR2(50),
   BSIMCARD             VARCHAR2(50),
   ChassisBrandName     VARCHAR2(100),
   BChassisBrandName    VARCHAR2(100),
   ChassisCode          VARCHAR2(50),
   BChassisCode         VARCHAR2(50),
   AdaptType            NUMBER(9),
   BAdaptType           NUMBER(9),
   CustomerCategory     NUMBER(9),
   BCustomerCategory    NUMBER(9),
   CustomerType         NUMBER(9),
   BCustomerType        NUMBER(9),
   BehindAxleTypTonneLevel VARCHAR2(50),
   BBehindAxleTypTonneLevel VARCHAR2(50),
   VIPVehicle           NUMBER(1),
   BVIPVehicle          NUMBER(1),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   DealerCode           VARCHAR2(50),
   FactoryPrise         NUMBER(19,4),
   SalesPrise           NUMBER(19,4),
   BDealerCode          VARCHAR2(50),
   BFactoryPrise        NUMBER(19,4),
   BSalesPrise          NUMBER(19,4),
   IsLicensePlate       NUMBER(1),
   BIsLicensePlate      NUMBER(1),
   IsGuarantee          NUMBER(1),
   BIsGuarantee         NUMBER(1),
   PreSaleTime          NUMBER(9),
   BPreSaleTime         NUMBER(9),
   constraint PK_VEHICLECHANGEMSG primary key (Id)
)
/

/*==============================================================*/
/* Table: VehicleExtensionInformation                           */
/*==============================================================*/
create table VehicleExtensionInformation  (
   Id                   NUMBER(9)                       not null,
   VehicleId            NUMBER(9)                       not null,
   ProblemId            NUMBER(9)                       not null,
   Content              VARCHAR2(200),
   constraint PK_VEHICLEEXTENSIONINFORMATION primary key (Id)
)
/

/*==============================================================*/
/* Table: VehicleInformation                                    */
/*==============================================================*/
create table VehicleInformation  (
   Id                   NUMBER(9)                       not null,
   VIN                  VARCHAR2(50)                    not null,
   SerialNumber         VARCHAR2(50)                    not null,
   OldVIN               VARCHAR2(50)                    not null,
   ResponsibleUnitId    NUMBER(9),
   ResponsibleUnitName  VARCHAR2(100),
   VehicleCategoryId    NUMBER(9)                       not null,
   VehicleCategoryName  VARCHAR2(100)                   not null,
   VehicleLicensePlate  VARCHAR2(50),
   VIPVehicle           NUMBER(1)                       not null,
   VehicleSeries        VARCHAR2(50),
   DealerName           VARCHAR2(100),
   ProductLineName      VARCHAR2(50),
   ProductCategoryTerrace VARCHAR2(50),
   VehicleFunction      VARCHAR2(50),
   VehicleColor         VARCHAR2(50),
   TopsCode             VARCHAR2(50),
   AdaptType            NUMBER(9),
   AdaptCompanyName     VARCHAR2(100),
   CageCategory         VARCHAR2(50),
   DriveModeCode        VARCHAR2(50),
   BridgeType           NUMBER(9),
   IsRoadVehicl         NUMBER(1)                       not null,
   FrontAxleCode        VARCHAR2(50),
   SecondFrontAxleCode  VARCHAR2(50),
   CentreAxleCode       VARCHAR2(50),
   BehindAxleCode       VARCHAR2(50),
   BehindAxleTypTonneLevel VARCHAR2(50),
   BalanceShaftCode     VARCHAR2(50),
   SteeringEngine       VARCHAR2(50),
   ChassisBrandName     VARCHAR2(100),
   ChassisCode          VARCHAR2(50),
   ProductId            NUMBER(9),
   ProductCode          VARCHAR2(50),
   BrandName            VARCHAR2(100),
   ProductCategoryCode  VARCHAR2(50),
   ProductCategoryName  VARCHAR2(200),
   EngineManufacture    VARCHAR2(100),
   AnnoucementNumber    VARCHAR2(50),
   EngineModel          VARCHAR2(50),
   EngineModelId        NUMBER(9),
   GearModel            VARCHAR2(50),
   EngineSerialNumber   VARCHAR2(50),
   GearSerialNumber     VARCHAR2(50),
   FirstStoppageMileage NUMBER(9),
   OutOfFactoryDate     DATE,
   IsWorkOff            NUMBER(1)                       not null,
   SalesDate            DATE,
   Capacity             NUMBER(9),
   WorkingHours         NUMBER(9),
   Mileage              NUMBER(9),
   LastMaintenanceWorkingHours NUMBER(9),
   LastMaintenanceCapacity NUMBER(9),
   LastMaintenanceMileage NUMBER(9),
   LastMaintenanceTime  DATE,
   SalesInvoiceNumber   VARCHAR2(50),
   InvoiceDate          DATE,
   VehicleType          NUMBER(9),
   Remark               VARCHAR2(200),
   PreSaleMaxTime       NUMBER(9),
   PreSaleTime          NUMBER(9),
   SalesMarketingDepName VARCHAR2(50),
   TRANSFERCASEMODEL    VARCHAR2(50),
   TRANSFERCASENUMBER   VARCHAR2(50),
   MSINOILPUMPMODEL     VARCHAR2(50),
   MSINOILPUMPNUMBER    VARCHAR2(50),
   CPPMODEL             VARCHAR2(50),
   CPPNUMBER            VARCHAR2(50),
   GEARPUMPMODEL        VARCHAR2(50),
   GEARPUMPNUMBER       VARCHAR2(50),
   MCMODEL              VARCHAR2(50),
   MCNUMBER             VARCHAR2(50),
   CCMODEL              VARCHAR2(50),
   CCNUMBER             VARCHAR2(50),
   ARMSUPPORTPUMPMODEL  VARCHAR2(50),
   ARMSUPPORTPUMPNUMBER VARCHAR2(50),
   ROLLINGREDUCERMODEL  VARCHAR2(50),
   ROLLINGREDUCERNUMBER VARCHAR2(50),
   MOTORMODEL           VARCHAR2(50),
   MOTORNUMBER          VARCHAR2(50),
   OILPUMPMODEL         VARCHAR2(50),
   OILPUMPNUMBER        VARCHAR2(50),
   RADIATORMODEL        VARCHAR2(50),
   RADIATORNUMBER       VARCHAR2(50),
   REDUCERMODEL         VARCHAR2(50),
   REDUCERNUMBER        VARCHAR2(50),
   IPCMODEL             VARCHAR2(50),
   IPCNUMBER            VARCHAR2(50),
   ECCMODEL             VARCHAR2(50),
   ECCNUMBER            VARCHAR2(50),
   MIXERMODEL           VARCHAR2(50),
   MIXERNUMBER          VARCHAR2(50),
   ACMODEL              VARCHAR2(50),
   ACNUMBER             VARCHAR2(50),
   GASTANKMODEL         VARCHAR2(50),
   GASTANKNUMBER        VARCHAR2(50),
   SCMODEL              VARCHAR2(50),
   SCNUMBER             VARCHAR2(50),
   DMMODEL              VARCHAR2(50),
   DMNUMBER             VARCHAR2(50),
   CSMODEL              VARCHAR2(50),
   CSNUMBER             VARCHAR2(50),
   INCLINEDBELTMODEL    VARCHAR2(50),
   INCLINEDBELTNUMBER   VARCHAR2(50),
   GPSNUMBER            VARCHAR2(50),
   SIMCARD              VARCHAR2(50),
   Status               NUMBER(9),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   IsTeamCustomers      NUMBER(9),
   DealerCode1          VARCHAR2(50),
   FactoryPrise         NUMBER(19,4),
   SalesPrise           NUMBER(19,4),
   IsLicensePlate       NUMBER(1),
   IsGuarantee          NUMBER(1)                       not null,
   CustomerCategory     NUMBER(9),
   Route                VARCHAR2(2000),
   Optyid               VARCHAR2(30),
   VehicleGrade         VARCHAR2(50),
   ChassisGrade         VARCHAR2(50),
   DEVICE_TYPE          VARCHAR2(32),
   IS_FINANCE           VARCHAR2(6),
   DID                  VARCHAR2(32),
   COMM_CODE            VARCHAR2(16),
   STD_SIMCODE          VARCHAR2(32),
   EXT_SIMCODE          VARCHAR2(32),
   BOUND_TIME           DATE,
   SN                   VARCHAR2(32),
   RealSalesType        NUMBER(9),
   constraint PK_VEHICLEINFORMATION primary key (Id)
)
/

