/*==============================================================*/
/* DBMS name:      ORACLE Version 10gR2                         */
/* Created on:     2023/12/4 9:55:43                            */
/*==============================================================*/


declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('APartsOutboundBillDetail');
  if num>0 then
    execute immediate 'drop table APartsOutboundBillDetail cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('APartsOutboundPlanDetail');
  if num>0 then
    execute immediate 'drop table APartsOutboundPlanDetail cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('AgencyDifferenceBackBill');
  if num>0 then
    execute immediate 'drop table AgencyDifferenceBackBill cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('AgencyPartsOutboundBill');
  if num>0 then
    execute immediate 'drop table AgencyPartsOutboundBill cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('AgencyPartsOutboundPlan');
  if num>0 then
    execute immediate 'drop table AgencyPartsOutboundPlan cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('BorrowBill');
  if num>0 then
    execute immediate 'drop table BorrowBill cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('BorrowBillDetail');
  if num>0 then
    execute immediate 'drop table BorrowBillDetail cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('BottomStockColVersion');
  if num>0 then
    execute immediate 'drop table BottomStockColVersion cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('BottomStockSubVersion');
  if num>0 then
    execute immediate 'drop table BottomStockSubVersion cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('BoxUpTask');
  if num>0 then
    execute immediate 'drop table BoxUpTask cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('BoxUpTaskDetail');
  if num>0 then
    execute immediate 'drop table BoxUpTaskDetail cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('BoxUpTaskMergeDetail');
  if num>0 then
    execute immediate 'drop table BoxUpTaskMergeDetail cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('CompanyTransferOrder');
  if num>0 then
    execute immediate 'drop table CompanyTransferOrder cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('CompanyTransferOrderDetail');
  if num>0 then
    execute immediate 'drop table CompanyTransferOrderDetail cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('CompletedByMonth');
  if num>0 then
    execute immediate 'drop table CompletedByMonth cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('CompletionSummary');
  if num>0 then
    execute immediate 'drop table CompletionSummary cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('DailyInboundCompletion');
  if num>0 then
    execute immediate 'drop table DailyInboundCompletion cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('DailyPackingCompletion');
  if num>0 then
    execute immediate 'drop table DailyPackingCompletion cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('DailyShelveCompletion');
  if num>0 then
    execute immediate 'drop table DailyShelveCompletion cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('DealerClaimSpareParts');
  if num>0 then
    execute immediate 'drop table DealerClaimSpareParts cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('DealerFormat');
  if num>0 then
    execute immediate 'drop table DealerFormat cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('DealerPartsInventoryBill');
  if num>0 then
    execute immediate 'drop table DealerPartsInventoryBill cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('DealerPartsInventoryDetail');
  if num>0 then
    execute immediate 'drop table DealerPartsInventoryDetail cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('DealerPartsTransOrderDetail');
  if num>0 then
    execute immediate 'drop table DealerPartsTransOrderDetail cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('DealerPartsTransferOrder');
  if num>0 then
    execute immediate 'drop table DealerPartsTransferOrder cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('ForceReserveBill');
  if num>0 then
    execute immediate 'drop table ForceReserveBill cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('ForceReserveBillDetail');
  if num>0 then
    execute immediate 'drop table ForceReserveBillDetail cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('HourlyCompletionTeam');
  if num>0 then
    execute immediate 'drop table HourlyCompletionTeam cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('InboundShelvesCompletion');
  if num>0 then
    execute immediate 'drop table InboundShelvesCompletion cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('InputSystem_Sync');
  if num>0 then
    execute immediate 'drop table InputSystem_Sync cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('MultiLevelApproveConfig');
  if num>0 then
    execute immediate 'drop table MultiLevelApproveConfig cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('OutBoundDetail');
  if num>0 then
    execute immediate 'drop table OutBoundDetail cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('OutoundAnalysis');
  if num>0 then
    execute immediate 'drop table OutoundAnalysis cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PackagingCollection');
  if num>0 then
    execute immediate 'drop table PackagingCollection cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PackagingCollectionDetail');
  if num>0 then
    execute immediate 'drop table PackagingCollectionDetail cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PackingTask');
  if num>0 then
    execute immediate 'drop table PackingTask cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PackingTaskDetail');
  if num>0 then
    execute immediate 'drop table PackingTaskDetail cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsDifferenceBackBill');
  if num>0 then
    execute immediate 'drop table PartsDifferenceBackBill cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsDifferenceBackBillDtl');
  if num>0 then
    execute immediate 'drop table PartsDifferenceBackBillDtl cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsInboundCheckBill');
  if num>0 then
    execute immediate 'drop table PartsInboundCheckBill cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsInboundCheckBillDetail');
  if num>0 then
    execute immediate 'drop table PartsInboundCheckBillDetail cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsInboundCheckBill_Sync');
  if num>0 then
    execute immediate 'drop table PartsInboundCheckBill_Sync cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsInboundPackingDetail');
  if num>0 then
    execute immediate 'drop table PartsInboundPackingDetail cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsInboundPerformance');
  if num>0 then
    execute immediate 'drop table PartsInboundPerformance cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsInboundPlan');
  if num>0 then
    execute immediate 'drop table PartsInboundPlan cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsInboundPlanDetail');
  if num>0 then
    execute immediate 'drop table PartsInboundPlanDetail cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsInventoryBill');
  if num>0 then
    execute immediate 'drop table PartsInventoryBill cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsInventoryDetail');
  if num>0 then
    execute immediate 'drop table PartsInventoryDetail cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsLogisticBatch');
  if num>0 then
    execute immediate 'drop table PartsLogisticBatch cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsLogisticBatchBillDetail');
  if num>0 then
    execute immediate 'drop table PartsLogisticBatchBillDetail cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsLogisticBatchItemDetail');
  if num>0 then
    execute immediate 'drop table PartsLogisticBatchItemDetail cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsOutboundBill');
  if num>0 then
    execute immediate 'drop table PartsOutboundBill cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsOutboundBillDetail');
  if num>0 then
    execute immediate 'drop table PartsOutboundBillDetail cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsOutboundPerformance');
  if num>0 then
    execute immediate 'drop table PartsOutboundPerformance cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsOutboundPlan');
  if num>0 then
    execute immediate 'drop table PartsOutboundPlan cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsOutboundPlanDetail');
  if num>0 then
    execute immediate 'drop table PartsOutboundPlanDetail cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsPacking');
  if num>0 then
    execute immediate 'drop table PartsPacking cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsRtnInboundCheck');
  if num>0 then
    execute immediate 'drop table PartsRtnInboundCheck cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsShelvesTask');
  if num>0 then
    execute immediate 'drop table PartsShelvesTask cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsShiftOrder');
  if num>0 then
    execute immediate 'drop table PartsShiftOrder cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsShiftOrderDetail');
  if num>0 then
    execute immediate 'drop table PartsShiftOrderDetail cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsShiftSIHDetail');
  if num>0 then
    execute immediate 'drop table PartsShiftSIHDetail cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsTransferOrder');
  if num>0 then
    execute immediate 'drop table PartsTransferOrder cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsTransferOrderDetail');
  if num>0 then
    execute immediate 'drop table PartsTransferOrderDetail cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PickingTask');
  if num>0 then
    execute immediate 'drop table PickingTask cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PickingTaskBatchDetail');
  if num>0 then
    execute immediate 'drop table PickingTaskBatchDetail cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PickingTaskDetail');
  if num>0 then
    execute immediate 'drop table PickingTaskDetail cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('SupplierInventoryBill');
  if num>0 then
    execute immediate 'drop table SupplierInventoryBill cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('SupplierInventoryBillDetail');
  if num>0 then
    execute immediate 'drop table SupplierInventoryBillDetail cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('SupplierReserveRecommendPlan');
  if num>0 then
    execute immediate 'drop table SupplierReserveRecommendPlan cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('SupplierStoreInboundDetail');
  if num>0 then
    execute immediate 'drop table SupplierStoreInboundDetail cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('SupplierStorePlanDetail');
  if num>0 then
    execute immediate 'drop table SupplierStorePlanDetail cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('SupplierStorePlanOrder');
  if num>0 then
    execute immediate 'drop table SupplierStorePlanOrder cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('VersionInfo');
  if num>0 then
    execute immediate 'drop table VersionInfo cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_APartsOutboundBillDetail');
  if num>0 then
    execute immediate 'drop sequence S_APartsOutboundBillDetail';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_APartsOutboundPlanDetail');
  if num>0 then
    execute immediate 'drop sequence S_APartsOutboundPlanDetail';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_AgencyDifferenceBackBill');
  if num>0 then
    execute immediate 'drop sequence S_AgencyDifferenceBackBill';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_AgencyPartsOutboundBill');
  if num>0 then
    execute immediate 'drop sequence S_AgencyPartsOutboundBill';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_AgencyPartsOutboundPlan');
  if num>0 then
    execute immediate 'drop sequence S_AgencyPartsOutboundPlan';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_BorrowBill');
  if num>0 then
    execute immediate 'drop sequence S_BorrowBill';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_BorrowBillDetail');
  if num>0 then
    execute immediate 'drop sequence S_BorrowBillDetail';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_BottomStockColVersion');
  if num>0 then
    execute immediate 'drop sequence S_BottomStockColVersion';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_BottomStockSubVersion');
  if num>0 then
    execute immediate 'drop sequence S_BottomStockSubVersion';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_BoxUpTask');
  if num>0 then
    execute immediate 'drop sequence S_BoxUpTask';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_BoxUpTaskDetail');
  if num>0 then
    execute immediate 'drop sequence S_BoxUpTaskDetail';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_BoxUpTaskMergeDetail');
  if num>0 then
    execute immediate 'drop sequence S_BoxUpTaskMergeDetail';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_CompanyTransferOrder');
  if num>0 then
    execute immediate 'drop sequence S_CompanyTransferOrder';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_CompanyTransferOrderDetail');
  if num>0 then
    execute immediate 'drop sequence S_CompanyTransferOrderDetail';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_CompletedByMonth');
  if num>0 then
    execute immediate 'drop sequence S_CompletedByMonth';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_CompletionSummary');
  if num>0 then
    execute immediate 'drop sequence S_CompletionSummary';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_DailyInboundCompletion');
  if num>0 then
    execute immediate 'drop sequence S_DailyInboundCompletion';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_DailyPackingCompletion');
  if num>0 then
    execute immediate 'drop sequence S_DailyPackingCompletion';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_DailyShelveCompletion');
  if num>0 then
    execute immediate 'drop sequence S_DailyShelveCompletion';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_DealerClaimSpareParts');
  if num>0 then
    execute immediate 'drop sequence S_DealerClaimSpareParts';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_DealerFormat');
  if num>0 then
    execute immediate 'drop sequence S_DealerFormat';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_DealerPartsInventoryBill');
  if num>0 then
    execute immediate 'drop sequence S_DealerPartsInventoryBill';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_DealerPartsInventoryDetail');
  if num>0 then
    execute immediate 'drop sequence S_DealerPartsInventoryDetail';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_DealerPartsTransOrderDetail');
  if num>0 then
    execute immediate 'drop sequence S_DealerPartsTransOrderDetail';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_DealerPartsTransferOrder');
  if num>0 then
    execute immediate 'drop sequence S_DealerPartsTransferOrder';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_ForceReserveBill');
  if num>0 then
    execute immediate 'drop sequence S_ForceReserveBill';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_ForceReserveBillDetail');
  if num>0 then
    execute immediate 'drop sequence S_ForceReserveBillDetail';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_HourlyCompletionTeam');
  if num>0 then
    execute immediate 'drop sequence S_HourlyCompletionTeam';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_InboundShelvesCompletion');
  if num>0 then
    execute immediate 'drop sequence S_InboundShelvesCompletion';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_InputSystem_Sync');
  if num>0 then
    execute immediate 'drop sequence S_InputSystem_Sync';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_MultiLevelApproveConfig');
  if num>0 then
    execute immediate 'drop sequence S_MultiLevelApproveConfig';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_OutBoundDetail');
  if num>0 then
    execute immediate 'drop sequence S_OutBoundDetail';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_OutoundAnalysis');
  if num>0 then
    execute immediate 'drop sequence S_OutoundAnalysis';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PackagingCollection');
  if num>0 then
    execute immediate 'drop sequence S_PackagingCollection';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PackagingCollectionDetail');
  if num>0 then
    execute immediate 'drop sequence S_PackagingCollectionDetail';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PackingTask');
  if num>0 then
    execute immediate 'drop sequence S_PackingTask';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PackingTaskDetail');
  if num>0 then
    execute immediate 'drop sequence S_PackingTaskDetail';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsDifferenceBackBill');
  if num>0 then
    execute immediate 'drop sequence S_PartsDifferenceBackBill';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsDifferenceBackBillDtl');
  if num>0 then
    execute immediate 'drop sequence S_PartsDifferenceBackBillDtl';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsInboundCheckBill');
  if num>0 then
    execute immediate 'drop sequence S_PartsInboundCheckBill';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsInboundCheckBillDetail');
  if num>0 then
    execute immediate 'drop sequence S_PartsInboundCheckBillDetail';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsInboundCheckBill_Sync');
  if num>0 then
    execute immediate 'drop sequence S_PartsInboundCheckBill_Sync';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsInboundPackingDetail');
  if num>0 then
    execute immediate 'drop sequence S_PartsInboundPackingDetail';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsInboundPerformance');
  if num>0 then
    execute immediate 'drop sequence S_PartsInboundPerformance';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsInboundPlan');
  if num>0 then
    execute immediate 'drop sequence S_PartsInboundPlan';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsInboundPlanDetail');
  if num>0 then
    execute immediate 'drop sequence S_PartsInboundPlanDetail';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsInventoryBill');
  if num>0 then
    execute immediate 'drop sequence S_PartsInventoryBill';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsInventoryDetail');
  if num>0 then
    execute immediate 'drop sequence S_PartsInventoryDetail';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsLogisticBatch');
  if num>0 then
    execute immediate 'drop sequence S_PartsLogisticBatch';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsLogisticBatchBillDetail');
  if num>0 then
    execute immediate 'drop sequence S_PartsLogisticBatchBillDetail';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsLogisticBatchItemDetail');
  if num>0 then
    execute immediate 'drop sequence S_PartsLogisticBatchItemDetail';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsOutboundBill');
  if num>0 then
    execute immediate 'drop sequence S_PartsOutboundBill';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsOutboundBillDetail');
  if num>0 then
    execute immediate 'drop sequence S_PartsOutboundBillDetail';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsOutboundPerformance');
  if num>0 then
    execute immediate 'drop sequence S_PartsOutboundPerformance';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsOutboundPlan');
  if num>0 then
    execute immediate 'drop sequence S_PartsOutboundPlan';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsOutboundPlanDetail');
  if num>0 then
    execute immediate 'drop sequence S_PartsOutboundPlanDetail';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsPacking');
  if num>0 then
    execute immediate 'drop sequence S_PartsPacking';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsRtnInboundCheck');
  if num>0 then
    execute immediate 'drop sequence S_PartsRtnInboundCheck';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsShelvesTask');
  if num>0 then
    execute immediate 'drop sequence S_PartsShelvesTask';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsShiftOrder');
  if num>0 then
    execute immediate 'drop sequence S_PartsShiftOrder';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsShiftOrderDetail');
  if num>0 then
    execute immediate 'drop sequence S_PartsShiftOrderDetail';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsShiftSIHDetail');
  if num>0 then
    execute immediate 'drop sequence S_PartsShiftSIHDetail';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsTransferOrder');
  if num>0 then
    execute immediate 'drop sequence S_PartsTransferOrder';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsTransferOrderDetail');
  if num>0 then
    execute immediate 'drop sequence S_PartsTransferOrderDetail';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PickingTask');
  if num>0 then
    execute immediate 'drop sequence S_PickingTask';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PickingTaskBatchDetail');
  if num>0 then
    execute immediate 'drop sequence S_PickingTaskBatchDetail';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PickingTaskDetail');
  if num>0 then
    execute immediate 'drop sequence S_PickingTaskDetail';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_SupplierInventoryBill');
  if num>0 then
    execute immediate 'drop sequence S_SupplierInventoryBill';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_SupplierInventoryBillDetail');
  if num>0 then
    execute immediate 'drop sequence S_SupplierInventoryBillDetail';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_SupplierReserveRecommendPlan');
  if num>0 then
    execute immediate 'drop sequence S_SupplierReserveRecommendPlan';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_SupplierStoreInboundDetail');
  if num>0 then
    execute immediate 'drop sequence S_SupplierStoreInboundDetail';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_SupplierStorePlanDetail');
  if num>0 then
    execute immediate 'drop sequence S_SupplierStorePlanDetail';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_SupplierStorePlanOrder');
  if num>0 then
    execute immediate 'drop sequence S_SupplierStorePlanOrder';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_VersionInfo');
  if num>0 then
    execute immediate 'drop sequence S_VersionInfo';
  end if;
end;
/

create sequence S_APartsOutboundBillDetail
/

create sequence S_APartsOutboundPlanDetail
/

create sequence S_AgencyDifferenceBackBill
/

create sequence S_AgencyPartsOutboundBill
/

create sequence S_AgencyPartsOutboundPlan
/

create sequence S_BorrowBill
/

create sequence S_BorrowBillDetail
/

create sequence S_BottomStockColVersion
/

create sequence S_BottomStockSubVersion
/

create sequence S_BoxUpTask
/

create sequence S_BoxUpTaskDetail
/

create sequence S_BoxUpTaskMergeDetail
/

create sequence S_CompanyTransferOrder
/

create sequence S_CompanyTransferOrderDetail
/

create sequence S_CompletedByMonth
/

create sequence S_CompletionSummary
/

create sequence S_DailyInboundCompletion
/

create sequence S_DailyPackingCompletion
/

create sequence S_DailyShelveCompletion
/

create sequence S_DealerClaimSpareParts
/

create sequence S_DealerFormat
/

create sequence S_DealerPartsInventoryBill
/

create sequence S_DealerPartsInventoryDetail
/

create sequence S_DealerPartsTransOrderDetail
/

create sequence S_DealerPartsTransferOrder
/

create sequence S_ForceReserveBill
/

create sequence S_ForceReserveBillDetail
/

create sequence S_HourlyCompletionTeam
/

create sequence S_InboundShelvesCompletion
/

create sequence S_InputSystem_Sync
/

create sequence S_MultiLevelApproveConfig
/

create sequence S_OutBoundDetail
/

create sequence S_OutoundAnalysis
/

create sequence S_PackagingCollection
/

create sequence S_PackagingCollectionDetail
/

create sequence S_PackingTask
/

create sequence S_PackingTaskDetail
/

create sequence S_PartsDifferenceBackBill
/

create sequence S_PartsDifferenceBackBillDtl
/

create sequence S_PartsInboundCheckBill
/

create sequence S_PartsInboundCheckBillDetail
/

create sequence S_PartsInboundCheckBill_Sync
/

create sequence S_PartsInboundPackingDetail
/

create sequence S_PartsInboundPerformance
/

create sequence S_PartsInboundPlan
/

create sequence S_PartsInboundPlanDetail
/

create sequence S_PartsInventoryBill
/

create sequence S_PartsInventoryDetail
/

create sequence S_PartsLogisticBatch
/

create sequence S_PartsLogisticBatchBillDetail
/

create sequence S_PartsLogisticBatchItemDetail
/

create sequence S_PartsOutboundBill
/

create sequence S_PartsOutboundBillDetail
/

create sequence S_PartsOutboundPerformance
/

create sequence S_PartsOutboundPlan
/

create sequence S_PartsOutboundPlanDetail
/

create sequence S_PartsPacking
/

create sequence S_PartsRtnInboundCheck
/

create sequence S_PartsShelvesTask
/

create sequence S_PartsShiftOrder
/

create sequence S_PartsShiftOrderDetail
/

create sequence S_PartsShiftSIHDetail
/

create sequence S_PartsTransferOrder
/

create sequence S_PartsTransferOrderDetail
/

create sequence S_PickingTask
/

create sequence S_PickingTaskBatchDetail
/

create sequence S_PickingTaskDetail
/

create sequence S_SupplierInventoryBill
/

create sequence S_SupplierInventoryBillDetail
/

create sequence S_SupplierReserveRecommendPlan
/

create sequence S_SupplierStoreInboundDetail
/

create sequence S_SupplierStorePlanDetail
/

create sequence S_SupplierStorePlanOrder
/

create sequence S_VersionInfo
/

/*==============================================================*/
/* Table: APartsOutboundBillDetail                              */
/*==============================================================*/
create table APartsOutboundBillDetail  (
   Id                   NUMBER(9)                       not null,
   PartsOutboundBillId  NUMBER(9)                       not null,
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50)                    not null,
   SparePartName        VARCHAR2(100)                   not null,
   OutboundAmount       NUMBER(9)                       not null,
   WarehouseAreaId      NUMBER(9),
   WarehouseAreaCode    VARCHAR2(50),
   BatchNumber          VARCHAR2(100),
   SettlementPrice      NUMBER(19,4)                    not null,
   CostPrice            NUMBER(19,4)                    not null,
   Remark               VARCHAR2(200),
   constraint PK_APARTSOUTBOUNDBILLDETAIL primary key (Id)
)
/

/*==============================================================*/
/* Table: APartsOutboundPlanDetail                              */
/*==============================================================*/
create table APartsOutboundPlanDetail  (
   Id                   NUMBER(9)                       not null,
   PartsOutboundPlanId  NUMBER(9)                       not null,
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50)                    not null,
   SparePartName        VARCHAR2(100)                   not null,
   PlannedAmount        NUMBER(9)                       not null,
   OutboundFulfillment  NUMBER(9),
   Price                NUMBER(19,4)                    not null,
   Remark               VARCHAR2(200),
   constraint PK_APARTSOUTBOUNDPLANDETAIL primary key (Id)
)
/

/*==============================================================*/
/* Table: AgencyDifferenceBackBill                              */
/*==============================================================*/
create table AgencyDifferenceBackBill  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   Status               NUMBER(9)                       not null,
   StorageCompanyId     NUMBER(9)                       not null,
   StorageCompanyCode   VARCHAR2(50)                    not null,
   StorageCompanyName   VARCHAR2(100)                   not null,
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50)                    not null,
   SparePartName        VARCHAR2(100)                   not null,
   MeasureUnit          VARCHAR2(20),
   PartsInboundPlanId   NUMBER(9),
   PartsInboundPlanCode VARCHAR2(50),
   InWarehouseId        NUMBER(9),
   InWarehouseCode      VARCHAR2(50),
   InWarehouseName      VARCHAR2(100),
   PartsShippingId      NUMBER(9),
   PartsShippingCode    VARCHAR2(50),
   PartsSalesOrderId    NUMBER(9),
   PartsSalesOrderCode  VARCHAR2(50),
   InboundQty           NUMBER(9),
   ActQty               NUMBER(9),
   DifferQty            NUMBER(9),
   OrderPrice           NUMBER(19,4),
   DifferSum            NUMBER(19,4),
   DIfferReason         VARCHAR2(1000),
   ResRelationshipId    NUMBER(9),
   ResType              VARCHAR2(200),
   ResTem               NUMBER(9),
   Path                 VARCHAR2(2000),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   SubmitterId          NUMBER(9),
   SubmitterName        VARCHAR2(100),
   SubmitTime           DATE,
   DetermineId          NUMBER(9),
   DetermineName        VARCHAR2(100),
   DetermineTime        DATE,
   ConfirmId            NUMBER(9),
   ConfirmName          VARCHAR2(100),
   ConfirmTime          DATE,
   HandlerId            NUMBER(9),
   Handler              VARCHAR2(100),
   HandlTime            DATE,
   HandMemo             VARCHAR2(500),
   CheckerId            NUMBER(9),
   CheckerName          VARCHAR2(100),
   CheckTime            DATE,
   ApproverId           NUMBER(9),
   ApproverName         VARCHAR2(100),
   ApproveTime          DATE,
   RejectMemo           VARCHAR2(200),
   CloserId             NUMBER(9),
   CloserName           VARCHAR2(100),
   CloseTime            DATE,
   CloseMemo            VARCHAR2(200),
   RowVersion           TIMESTAMP,
   Responsibility       NUMBER(9),
   ApProvalResolution   VARCHAR2(500),
   RejectedId           NUMBER(9),
   RejectedName         VARCHAR2(100),
   RejectedTime         DATE,
   IsChange             NUMBER(1),
   IsMaterial           NUMBER(1),
   constraint PK_AGENCYDIFFERENCEBACKBILL primary key (Id)
)
/

/*==============================================================*/
/* Table: AgencyPartsOutboundBill                               */
/*==============================================================*/
create table AgencyPartsOutboundBill  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   PartsOutboundPlanId  NUMBER(9)                       not null,
   WarehouseId          NUMBER(9)                       not null,
   WarehouseCode        VARCHAR2(50)                    not null,
   WarehouseName        VARCHAR2(100)                   not null,
   StorageCompanyId     NUMBER(9)                       not null,
   StorageCompanyCode   VARCHAR2(50)                    not null,
   StorageCompanyName   VARCHAR2(100)                   not null,
   StorageCompanyType   NUMBER(9)                       not null,
   BranchId             NUMBER(9)                       not null,
   BranchCode           VARCHAR2(50)                    not null,
   BranchName           VARCHAR2(100)                   not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   PartsSalesOrderTypeId NUMBER(9),
   PartsSalesOrderTypeName VARCHAR2(100),
   CounterpartCompanyId NUMBER(9)                       not null,
   CounterpartCompanyCode VARCHAR2(50)                    not null,
   CounterpartCompanyName VARCHAR2(100)                   not null,
   ReceivingCompanyId   NUMBER(9)                       not null,
   ReceivingCompanyCode VARCHAR2(50)                    not null,
   ReceivingCompanyName VARCHAR2(100)                   not null,
   ReceivingWarehouseId NUMBER(9),
   ReceivingWarehouseCode VARCHAR2(50),
   ReceivingWarehouseName VARCHAR2(100),
   OutboundType         NUMBER(9)                       not null,
   ShippingMethod       NUMBER(9),
   CustomerAccountId    NUMBER(9),
   OriginalRequirementBillId NUMBER(9)                       not null,
   OriginalRequirementBillType NUMBER(9)                       not null,
   OriginalRequirementBillCode VARCHAR2(50)                    not null,
   SettlementStatus     NUMBER(9)                       not null,
   OrderApproveComment  VARCHAR2(2000),
   InterfaceRecordId    VARCHAR2(25),
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   ERPSourceOrderCode   VARCHAR2(500),
   constraint PK_AGENCYPARTSOUTBOUNDBILL primary key (Id)
)
/

/*==============================================================*/
/* Table: AgencyPartsOutboundPlan                               */
/*==============================================================*/
create table AgencyPartsOutboundPlan  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   WarehouseId          NUMBER(9)                       not null,
   WarehouseCode        VARCHAR2(50)                    not null,
   WarehouseName        VARCHAR2(100)                   not null,
   StorageCompanyId     NUMBER(9)                       not null,
   StorageCompanyCode   VARCHAR2(50)                    not null,
   StorageCompanyName   VARCHAR2(100)                   not null,
   StorageCompanyType   NUMBER(9)                       not null,
   CompanyAddressId     NUMBER(9),
   BranchId             NUMBER(9)                       not null,
   BranchCode           VARCHAR2(50)                    not null,
   BranchName           VARCHAR2(100)                   not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   PartsSalesOrderTypeId NUMBER(9),
   PartsSalesOrderTypeName VARCHAR2(100),
   CounterpartCompanyId NUMBER(9)                       not null,
   CounterpartCompanyCode VARCHAR2(50)                    not null,
   CounterpartCompanyName VARCHAR2(100)                   not null,
   ReceivingCompanyId   NUMBER(9)                       not null,
   ReceivingCompanyCode VARCHAR2(50)                    not null,
   ReceivingCompanyName VARCHAR2(100)                   not null,
   ReceivingWarehouseId NUMBER(9),
   ReceivingWarehouseCode VARCHAR2(50),
   ReceivingWarehouseName VARCHAR2(100),
   SourceId             NUMBER(9)                       not null,
   SourceCode           VARCHAR2(50)                    not null,
   OutboundType         NUMBER(9)                       not null,
   ShippingMethod       NUMBER(9),
   CustomerAccountId    NUMBER(9),
   OriginalRequirementBillId NUMBER(9)                       not null,
   OriginalRequirementBillType NUMBER(9)                       not null,
   OriginalRequirementBillCode VARCHAR2(50)                    not null,
   Status               NUMBER(9)                       not null,
   StopComment          VARCHAR2(200),
   Remark               VARCHAR2(500),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   StoperId             NUMBER(9),
   Stoper               VARCHAR2(100),
   StopTime             DATE,
   RowVersion           TIMESTAMP,
   ERPSourceOrderCode   VARCHAR2(500),
   constraint PK_AGENCYPARTSOUTBOUNDPLAN primary key (Id)
)
/

/*==============================================================*/
/* Table: BorrowBill                                            */
/*==============================================================*/
create table BorrowBill  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   PartsSalesCategoryName VARCHAR2(100),
   WarehouseId          NUMBER(9),
   WarehouseCode        VARCHAR2(50),
   WarehouseName        VARCHAR2(100),
   BorrowDepartmentName VARCHAR2(100),
   BorrowName           VARCHAR2(100),
   ContactMethod        VARCHAR2(100),
   Purpose              VARCHAR2(100),
   Type                 NUMBER(9),
   ExpectReturnTime     DATE,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   SubmitterId          NUMBER(9),
   SubmitterName        VARCHAR2(100),
   SubmitTime           DATE,
   InitialApproverId    NUMBER(9),
   InitialApproverName  VARCHAR2(100),
   InitialApproveTime   DATE,
   ApproverId           NUMBER(9),
   ApproverName         VARCHAR2(100),
   ApproveTime          DATE,
   OutBoundTime         DATE,
   Status               NUMBER(9),
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   Remark               VARCHAR2(200),
   ReturnerName         VARCHAR2(100),
   ReturnTime           DATE,
   Path                 VARCHAR2(2000),
   constraint PK_BORROWBILL primary key (Id)
)
/

/*==============================================================*/
/* Table: BorrowBillDetail                                      */
/*==============================================================*/
create table BorrowBillDetail  (
   Id                   NUMBER(9)                       not null,
   BorrowBillId         NUMBER(9)                       not null,
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50),
   SparePartName        VARCHAR2(100),
   SihCode              VARCHAR2(50),
   MeasureUnit          VARCHAR2(50),
   WarehouseAreaCode    VARCHAR2(50),
   BorrowQty            NUMBER(9),
   OutboundAmount       NUMBER(9),
   ConfirmingQty        NUMBER(9),
   ConfirmedQty         NUMBER(9),
   WarehouseAreaId      NUMBER(9),
   constraint PK_BORROWBILLDETAIL primary key (Id)
)
/

/*==============================================================*/
/* Table: BottomStockColVersion                                 */
/*==============================================================*/
create table BottomStockColVersion  (
   Id                   NUMBER(9)                       not null,
   CompanyId            NUMBER(9)                       not null,
   CompanyCode          VARCHAR2(50)                    not null,
   CompanyName          VARCHAR2(100)                   not null,
   CompanyType          NUMBER(9)                       not null,
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50)                    not null,
   SparePartName        VARCHAR2(100)                   not null,
   ColVersionCode       VARCHAR2(50),
   SalePrice            NUMBER(19,4),
   ReserveFee           NUMBER(19,4),
   ReserveQty           NUMBER(9),
   StartTime            DATE,
   EndTime              DATE,
   Status               NUMBER(9)                       not null,
   CreateTime           DATE,
   ReserveTypeId        NUMBER(9),
   ReserveType          VARCHAR2(100),
   LapseReason          VARCHAR2(500),
   constraint PK_BOTTOMSTOCKCOLVERSION primary key (Id)
)
/

/*==============================================================*/
/* Table: BottomStockSubVersion                                 */
/*==============================================================*/
create table BottomStockSubVersion  (
   Id                   NUMBER(9)                       not null,
   CompanyId            NUMBER(9)                       not null,
   CompanyCode          VARCHAR2(50)                    not null,
   CompanyName          VARCHAR2(100)                   not null,
   CompanyType          NUMBER(9)                       not null,
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50)                    not null,
   SparePartName        VARCHAR2(100)                   not null,
   SubVersionCode       VARCHAR2(50),
   ColVersionCode       VARCHAR2(50),
   SalePrice            NUMBER(19,4),
   ReserveFee           NUMBER(19,4),
   ReserveQty           NUMBER(9),
   StartTime            DATE,
   EndTime              DATE,
   Status               NUMBER(9)                       not null,
   CreateTime           DATE,
   ReserveTypeId        NUMBER(9),
   ReserveType          VARCHAR2(100),
   ReserveTypeSubItemId NUMBER(9),
   ReserveTypeSubItem   VARCHAR2(100),
   LapseReason          VARCHAR2(500),
   constraint PK_BOTTOMSTOCKSUBVERSION primary key (Id)
)
/

/*==============================================================*/
/* Table: BoxUpTask                                             */
/*==============================================================*/
create table BoxUpTask  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   WarehouseId          NUMBER(9),
   WarehouseCode        VARCHAR2(50),
   WarehouseName        VARCHAR2(100),
   CounterpartCompanyId NUMBER(9),
   CounterpartCompanyCode VARCHAR2(50),
   CounterpartCompanyName VARCHAR2(100),
   Status               NUMBER(9),
   IsExistShippingOrder NUMBER(1),
   ContainerNumber      VARCHAR2(50),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   BoxUpFinishTime      DATE,
   Remark               VARCHAR2(200),
   RowVersion           TIMESTAMP,
   OrderTypeId          NUMBER(9),
   OrderTypeName        VARCHAR2(100),
   CarrierId            NUMBER(9),
   CarrierCode          VARCHAR2(50),
   CarrierName          VARCHAR2(100),
   constraint PK_BOXUPTASK primary key (Id)
)
/

/*==============================================================*/
/* Table: BoxUpTaskDetail                                       */
/*==============================================================*/
create table BoxUpTaskDetail  (
   Id                   NUMBER(9)                       not null,
   BoxUpTaskId          NUMBER(9)                       not null,
   BoxUpTaskCode        VARCHAR2(50),
   PartsOutboundPlanId  NUMBER(9),
   PartsOutboundPlanCode VARCHAR2(50),
   PickingTaskId        NUMBER(9),
   PickingTaskCode      VARCHAR2(50),
   SourceCode           VARCHAR2(50),
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50)                    not null,
   SparePartName        VARCHAR2(100),
   SihCode              VARCHAR2(50),
   PlanQty              NUMBER(9),
   BoxUpQty             NUMBER(9),
   ContainerNumber      VARCHAR2(50),
   PickingTaskDetailId  NUMBER(9),
   constraint PK_BOXUPTASKDETAIL primary key (Id)
)
/

/*==============================================================*/
/* Table: BoxUpTaskMergeDetail                                  */
/*==============================================================*/
create table BoxUpTaskMergeDetail  (
   Id                   NUMBER(9)                       not null,
   BoxUpTaskDetailId    NUMBER(9)                       not null,
   MergeCaseNumber      VARCHAR2(100)                   not null,
   BoxUpQty             NUMBER(9)                       not null,
   constraint PK_BOXUPTASKMERGEDETAIL primary key (Id)
)
/

/*==============================================================*/
/* Table: CompanyTransferOrder                                  */
/*==============================================================*/
create table CompanyTransferOrder  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   OriginalCompanyId    NUMBER(9)                       not null,
   OriginalCompanyCode  VARCHAR2(50)                    not null,
   OriginalCompanyName  VARCHAR2(100)                   not null,
   DestCompanyId        NUMBER(9)                       not null,
   DestCompanyCode      VARCHAR2(50)                    not null,
   DestCompanyName      VARCHAR2(100)                   not null,
   OriginalWarehouseId  NUMBER(9),
   DestWarehouseId      NUMBER(9),
   TransferDirection    NUMBER(9)                       not null,
   TotalAmount          NUMBER(19,4)                    not null,
   ReceivingAddress     VARCHAR2(200),
   Type                 NUMBER(9)                       not null,
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   ApproverId           NUMBER(9),
   ApproverName         VARCHAR2(100),
   ApproveTime          DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   RowVersion           TIMESTAMP,
   constraint PK_COMPANYTRANSFERORDER primary key (Id)
)
/

/*==============================================================*/
/* Table: CompanyTransferOrderDetail                            */
/*==============================================================*/
create table CompanyTransferOrderDetail  (
   Id                   NUMBER(9)                       not null,
   PartsTransferOrderId NUMBER(9)                       not null,
   PartsId              NUMBER(9)                       not null,
   PartsCode            VARCHAR2(50)                    not null,
   PartsName            VARCHAR2(100)                   not null,
   PlannedAmount        NUMBER(9)                       not null,
   ConfirmedAmount      NUMBER(9),
   Price                NUMBER(19,4)                    not null,
   Remark               VARCHAR2(200),
   constraint PK_COMPANYTRANSFERORDERDETAIL primary key (Id)
)
/

/*==============================================================*/
/* Table: CompletedByMonth                                      */
/*==============================================================*/
create table CompletedByMonth  (
   Id                   NUMBER(9)                       not null,
   WarehouseId          NUMBER(9)                       not null,
   WarehouseCode        VARCHAR2(50)                    not null,
   WarehouseName        VARCHAR2(100)                   not null,
   Month                VARCHAR2(50),
   CreateTime           DATE,
   InBoundItem          NUMBER(9),
   InBoundFee           NUMBER(19,4),
   PackItem             NUMBER(9),
   PackFee              NUMBER(19,4),
   ShelevsItem          NUMBER(9),
   ShelevsFee           NUMBER(19,4),
   PickItem             NUMBER(9),
   PickFee              NUMBER(19,4),
   BoxItem              NUMBER(9),
   BoxFee               NUMBER(19,4),
   constraint PK_COMPLETEDBYMONTH primary key (Id)
)
/

/*==============================================================*/
/* Table: CompletionSummary                                     */
/*==============================================================*/
create table CompletionSummary  (
   Id                   NUMBER(9)                       not null,
   Type                 NUMBER(9)                       not null,
   WarehouseId          NUMBER(9)                       not null,
   WarehouseCode        VARCHAR2(50)                    not null,
   WarehouseName        VARCHAR2(100)                   not null,
   WithinCompleteItem   NUMBER(9),
   WithinCompleteFee    NUMBER(19,4),
   OverCompleteItem     NUMBER(9),
   OverCompleteFee      NUMBER(19,4),
   WithinUnCompleteItem NUMBER(9),
   WithinUnCompleteFee  NUMBER(19,4),
   OverUnCompleteItem   NUMBER(9),
   OverUnCompleteFee    NUMBER(19,4),
   WithinCompleteItemRate NUMBER(19,4),
   WithinCompleteFeeRate NUMBER(19,4),
   StandardItem         NUMBER(9),
   StandardFee          NUMBER(19,4),
   CompleteItems        NUMBER(9),
   CompleteFee          NUMBER(19,4),
   CompleteItemRate     NUMBER(19,4),
   CompleteFeeRate      NUMBER(19,4),
   CreateTime           DATE,
   constraint PK_COMPLETIONSUMMARY primary key (Id)
)
/

/*==============================================================*/
/* Table: DailyInboundCompletion                                */
/*==============================================================*/
create table DailyInboundCompletion  (
   Id                   NUMBER(9)                       not null,
   WarehouseId          NUMBER(9)                       not null,
   WarehouseCode        VARCHAR2(50)                    not null,
   WarehouseName        VARCHAR2(100)                   not null,
   PartsPurchaseOrderCode VARCHAR2(50),
   ShippingCode         VARCHAR2(50),
   ShippingDate         DATE,
   ShippingAmount       NUMBER(9),
   InboundPlanCode      VARCHAR2(50),
   InboundPlanStatus    NUMBER(9),
   SupplierComfirm      VARCHAR2(100),
   SupplierComfirmDate  DATE,
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50)                    not null,
   SparePartName        VARCHAR2(100)                   not null,
   SIHCode              VARCHAR2(50),
   SalesPrice           NUMBER(19,4),
   ConfirmedAmount      NUMBER(9),
   MinSaleQuantity      NUMBER(9),
   InspectedQuantity    NUMBER(9),
   InspectedItem        NUMBER(9),
   InspectedFee         NUMBER(19,4),
   InboundDate          DATE,
   Judgment             NUMBER(9),
   CreateTime           DATE,
   constraint PK_DAILYINBOUNDCOMPLETION primary key (Id)
)
/

/*==============================================================*/
/* Table: DailyPackingCompletion                                */
/*==============================================================*/
create table DailyPackingCompletion  (
   Id                   NUMBER(9)                       not null,
   WarehouseId          NUMBER(9)                       not null,
   WarehouseCode        VARCHAR2(50)                    not null,
   WarehouseName        VARCHAR2(100)                   not null,
   PartsPurchaseOrderCode VARCHAR2(50),
   PartsInboundCheckBillCode VARCHAR2(50),
   PackingTaskCode      VARCHAR2(50),
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50)                    not null,
   SparePartName        VARCHAR2(100)                   not null,
   SIHCode              VARCHAR2(50),
   SalesPrice           NUMBER(19,4),
   MinSaleQuantity      NUMBER(9),
   InspectedQuantity    NUMBER(9),
   InboundDate          DATE,
   PackingQty           NUMBER(9),
   PackingFee           NUMBER(19,4),
   PackingItem          NUMBER(9),
   PackingFinishiDate   DATE,
   Judgment             NUMBER(9),
   CreateTime           DATE,
   constraint PK_DAILYPACKINGCOMPLETION primary key (Id)
)
/

/*==============================================================*/
/* Table: DailyShelveCompletion                                 */
/*==============================================================*/
create table DailyShelveCompletion  (
   Id                   NUMBER(9)                       not null,
   WarehouseId          NUMBER(9)                       not null,
   WarehouseCode        VARCHAR2(50)                    not null,
   WarehouseName        VARCHAR2(100)                   not null,
   PartsPurchaseOrderCode VARCHAR2(50),
   PartsInboundCheckBillCode VARCHAR2(50),
   PackingTaskCode      VARCHAR2(50),
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50)                    not null,
   SparePartName        VARCHAR2(100)                   not null,
   SIHCode              VARCHAR2(50),
   SalesPrice           NUMBER(19,4),
   MinSaleQuantity      NUMBER(9),
   PlannedAmount        NUMBER(9),
   ShelvesDate          DATE,
   ShelvesAmount        NUMBER(9),
   ShelvesFee           NUMBER(19,4),
   ShelvesItem          NUMBER(9),
   ShelvesFinishTime    DATE,
   Judgment             NUMBER(9),
   CreateTime           DATE,
   constraint PK_DAILYSHELVECOMPLETION primary key (Id)
)
/

/*==============================================================*/
/* Table: DealerClaimSpareParts                                 */
/*==============================================================*/
create table DealerClaimSpareParts  (
   Id                   NUMBER(9)                       not null,
   ClaimCode            VARCHAR2(50),
   ClaimStatus          VARCHAR2(50),
   DealerId             NUMBER(9),
   DealerCode           VARCHAR2(50),
   SparePartId          NUMBER(9),
   SparePartCode        VARCHAR2(50),
   SparePartName        VARCHAR2(100),
   DealerName           VARCHAR2(100),
   Price                NUMBER(19,4),
   SettleDate           DATE,
   Quantity             NUMBER(9),
   CreateTime           DATE,
   constraint PK_DEALERCLAIMSPAREPARTS primary key (Id)
)
/

/*==============================================================*/
/* Table: DealerFormat                                          */
/*==============================================================*/
create table DealerFormat  (
   Id                   NUMBER(9)                       not null,
   DealerId             NUMBER(9)                       not null,
   DealerCode           VARCHAR2(50)                    not null,
   DealerName           VARCHAR2(100)                   not null,
   Format               VARCHAR2(200),
   Quarter              VARCHAR2(50),
   Status               NUMBER(9)                       not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   MonthlyCosts         NUMBER(19,4),
   StoreType            NUMBER(9),
   constraint PK_DEALERFORMAT primary key (Id)
)
/

/*==============================================================*/
/* Table: DealerPartsInventoryBill                              */
/*==============================================================*/
create table DealerPartsInventoryBill  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   BranchId             NUMBER(9)                       not null,
   StorageCompanyId     NUMBER(9)                       not null,
   StorageCompanyCode   VARCHAR2(50)                    not null,
   StorageCompanyName   VARCHAR2(100)                   not null,
   WarehouseId          NUMBER(9),
   SalesCategoryId      NUMBER(9)                       not null,
   CancelReason         VARCHAR2(200),
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   Path                 VARCHAR2(2000),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   InitialApproverId    NUMBER(9),
   InitialApproverName  VARCHAR2(100),
   InitialApproveTime   DATE,
   CheckerId            NUMBER(9),
   CheckerName          VARCHAR2(100),
   CheckTime            DATE,
   UpperCheckerId       NUMBER(9),
   UpperCheckerName     VARCHAR2(100),
   UpperCheckTime       DATE,
   ApproverId           NUMBER(9),
   ApproveTime          DATE,
   ApproverName         VARCHAR2(100),
   ApproveComment       VARCHAR2(200),
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   RejecterId           NUMBER(9),
   RejecterName         VARCHAR2(100),
   RejectTime           DATE,
   SeniorId             NUMBER(9),
   SeniorName           VARCHAR2(100),
   SeniorTime           DATE,
   AgencyId             NUMBER(9),
   AgencyName           VARCHAR2(100),
   AgencyCode           VARCHAR2(500),
   constraint PK_DEALERPARTSINVENTORYBILL primary key (Id)
)
/

/*==============================================================*/
/* Table: DealerPartsInventoryDetail                            */
/*==============================================================*/
create table DealerPartsInventoryDetail  (
   Id                   NUMBER(9)                       not null,
   DealerPartsInventoryId NUMBER(9)                       not null,
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50)                    not null,
   SparePartName        VARCHAR2(100)                   not null,
   CurrentStorage       NUMBER(9)                       not null,
   StorageAfterInventory NUMBER(9)                       not null,
   StorageDifference    NUMBER(9)                       not null,
   Memo                 VARCHAR2(200),
   DealerPrice          NUMBER(19,4),
   constraint PK_DEALERPARTSINVENTORYDETAIL primary key (Id)
)
/

/*==============================================================*/
/* Table: DealerPartsTransOrderDetail                           */
/*==============================================================*/
create table DealerPartsTransOrderDetail  (
   Id                   NUMBER(9)                       not null,
   DealerPartsTransferOrderId NUMBER(9)                       not null,
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50)                    not null,
   SparePartName        VARCHAR2(100)                   not null,
   Amount               NUMBER(9)                       not null,
   WarrantyPrice        NUMBER(19,4),
   NoWarrantyPrice      NUMBER(19,4),
   DiffPrice            NUMBER(19,4),
   Remark               VARCHAR2(200),
   constraint PK_DEALERPARTSTRANSORDERDETAIL primary key (Id)
)
/

/*==============================================================*/
/* Table: DealerPartsTransferOrder                              */
/*==============================================================*/
create table DealerPartsTransferOrder  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   BranchId             NUMBER(9)                       not null,
   BranchIdCode         VARCHAR2(50)                    not null,
   BranchIdName         VARCHAR2(100)                   not null,
   WarrantyBrandId      NUMBER(9)                       not null,
   WarrantyBrandCode    VARCHAR2(50)                    not null,
   WarrantyBrandName    VARCHAR2(100)                   not null,
   NoWarrantyBrandId    NUMBER(9)                       not null,
   NoWarrantyBrandCode  VARCHAR2(50)                    not null,
   NoWarrantyBrandName  VARCHAR2(100)                   not null,
   DealerId             NUMBER(9)                       not null,
   DealerCode           VARCHAR2(50)                    not null,
   DealerName           VARCHAR2(100)                   not null,
   SourceId             NUMBER(9),
   SourceCode           VARCHAR2(50),
   SumWarrantyPrice     NUMBER(19,4),
   SumNoWarrantyPrice   NUMBER(19,4),
   DiffPrice            NUMBER(19,4),
   Remark               VARCHAR2(500),
   Status               NUMBER(9)                       not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   SubmitterId          NUMBER(9),
   SubmitterName        VARCHAR2(100),
   SubmitTime           DATE,
   CheckerId            NUMBER(9),
   CheckerName          VARCHAR2(100),
   CheckTime            DATE,
   CheckOpinion         VARCHAR2(200),
   RejecterId           NUMBER(9),
   RejecterName         VARCHAR2(100),
   RejectTime           DATE,
   RejectOpinion        VARCHAR2(200),
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   Rowversion           TIMESTAMP,
   constraint PK_DEALERPARTSTRANSFERORDER primary key (Id)
)
/

/*==============================================================*/
/* Table: ForceReserveBill                                      */
/*==============================================================*/
create table ForceReserveBill  (
   Id                   NUMBER(9)                       not null,
   CompanyId            NUMBER(9)                       not null,
   CompanyCode          VARCHAR2(50)                    not null,
   CompanyName          VARCHAR2(100)                   not null,
   CompanyType          NUMBER(9)                       not null,
   SubVersionCode       VARCHAR2(50),
   ColVersionCode       VARCHAR2(50),
   Status               NUMBER(9)                       not null,
   IsAttach             NUMBER(1),
   ReserveTypeId        NUMBER(9),
   ReserveType          VARCHAR2(100),
   ReserveTypeSubItemId NUMBER(9),
   ReserveTypeSubItem   VARCHAR2(100),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   CheckerId            NUMBER(9),
   CheckerName          VARCHAR2(100),
   CheckTime            DATE,
   ApproverId           NUMBER(9),
   ApproverName         VARCHAR2(100),
   ApproveTime          DATE,
   RejecterId           NUMBER(9),
   RejecterName         VARCHAR2(100),
   RejectTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   ValidateFrom         DATE,
   SubmitterId          NUMBER(9),
   SubmitterName        VARCHAR2(100),
   SubmitTime           DATE,
   constraint PK_FORCERESERVEBILL primary key (Id)
)
/

/*==============================================================*/
/* Table: ForceReserveBillDetail                                */
/*==============================================================*/
create table ForceReserveBillDetail  (
   Id                   NUMBER(9)                       not null,
   ForceReserveBillId   NUMBER(9)                       not null,
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50)                    not null,
   SparePartName        VARCHAR2(100)                   not null,
   SuggestForceReserveQty NUMBER(9),
   ForceReserveQty      NUMBER(9),
   ReserveFee           NUMBER(19,4),
   CenterPartProperty   NUMBER(9),
   CenterPrice          NUMBER(9),
   constraint PK_FORCERESERVEBILLDETAIL primary key (Id)
)
/

/*==============================================================*/
/* Table: HourlyCompletionTeam                                  */
/*==============================================================*/
create table HourlyCompletionTeam  (
   Id                   NUMBER(9)                       not null,
   CreateTime           DATE                            not null,
   WarehouseId          NUMBER(9)                       not null,
   WarehouseCode        VARCHAR2(50)                    not null,
   WarehouseName        VARCHAR2(100)                   not null,
   InBoundFinishFee     NUMBER(19,4),
   InBoundTakFee        NUMBER(19,4),
   InboundFinishRate    NUMBER(19,4),
   InBoundFinishItem    NUMBER(9),
   InBoundTakItem       NUMBER(9),
   InboundItemRate      NUMBER(19,4),
   PackFinishFee        NUMBER(19,4),
   PackTaskFee          NUMBER(19,4),
   PackFinishRate       NUMBER(19,4),
   PackFinishItem       NUMBER(9),
   PackTaskItem         NUMBER(9),
   PackItemRate         NUMBER(19,4),
   ShelvesFinishFee     NUMBER(19,4),
   ShelvesTaskFee       NUMBER(19,4),
   ShelvesFinishRate    NUMBER(19,4),
   ShelvesFinishItem    NUMBER(9),
   ShelvesTaskItem      NUMBER(9),
   ShelvesItemRate      NUMBER(19,4),
   PickFInishFee        NUMBER(19,4),
   PickTaskFee          NUMBER(19,4),
   PickFInishRate       NUMBER(19,4),
   PickFInishItem       NUMBER(9),
   PickTaskItem         NUMBER(9),
   PickItemRate         NUMBER(19,4),
   BoxFinishFee         NUMBER(19,4),
   BoxTaskFee           NUMBER(19,4),
   BoxFinishRate        NUMBER(19,4),
   BoxFinishItem        NUMBER(9),
   BoxTaskItem          NUMBER(9),
   BoxItemRate          NUMBER(19,4),
   constraint PK_HOURLYCOMPLETIONTEAM primary key (Id)
)
/

/*==============================================================*/
/* Table: InboundShelvesCompletion                              */
/*==============================================================*/
create table InboundShelvesCompletion  (
   Id                   NUMBER(9)                       not null,
   WorkDays             NUMBER(9)                       not null,
   InboundDate          DATE                            not null,
   FinishFeeTwentyF     NUMBER(19,4),
   FinishFeeFortyE      NUMBER(19,4),
   FinishFeeSeventT     NUMBER(19,4),
   FinishFeeOverTwentyF NUMBER(19,4),
   InBoundFee           NUMBER(19,4),
   DayTwentyFRate       NUMBER(19,4),
   DayFortyERate        NUMBER(19,4),
   DaySeventyTRate      NUMBER(19,4),
   OverTwentyFRate      NUMBER(19,4),
   OverFortyERate       NUMBER(19,4),
   OverSeventyTRate     NUMBER(19,4),
   CreateTime           DATE,
   constraint PK_INBOUNDSHELVESCOMPLETION primary key (Id)
)
/

/*==============================================================*/
/* Table: InputSystem_Sync                                      */
/*==============================================================*/
create table InputSystem_Sync  (
   Id                   NUMBER(9)                       not null,
   Type                 NUMBER(9),
   Objid                VARCHAR2(40),
   IOStatus             NUMBER(9),
   CreateTime           DATE,
   FirstSendTime        DATE,
   ModifySendTime       DATE,
   Token                VARCHAR2(50),
   Sender               VARCHAR2(20),
   Message              VARCHAR2(500),
   constraint PK_INPUTSYSTEM_SYNC primary key (Id)
)
/

/*==============================================================*/
/* Table: MultiLevelApproveConfig                               */
/*==============================================================*/
create table MultiLevelApproveConfig  (
   Id                   NUMBER(9)                       not null,
   Type                 NUMBER(9)                       not null,
   ApproverId           NUMBER(9),
   ApproverName         VARCHAR2(100),
   MinApproveFee        NUMBER(19,4),
   MaxApproveFee        NUMBER(19,4),
   IsFinished           NUMBER(1)                       not null,
   Status               NUMBER(9)                       not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   constraint PK_MULTILEVELAPPROVECONFIG primary key (Id)
)
/

/*==============================================================*/
/* Table: OutBoundDetail                                        */
/*==============================================================*/
create table OutBoundDetail  (
   Id                   NUMBER(9)                       not null,
   OutBoundPlanStatus   NUMBER(9)                       not null,
   OutBoundPlanCode     VARCHAR2(50)                    not null,
   WarehouseId          NUMBER(9)                       not null,
   WarehouseCode        VARCHAR2(50)                    not null,
   WarehouseName        VARCHAR2(100)                   not null,
   OutBoudType          NUMBER(9),
   PlanCreateTime       DATE,
   OriginalRequirementBillCode VARCHAR2(50),
   PartsSalesOrderTypeName VARCHAR2(100),
   ShippingCode         VARCHAR2(50),
   OutBoundCode         VARCHAR2(50),
   OutBoundCreateTime   DATE,
   CounterpartCompanyCode VARCHAR2(50),
   CounterpartCompanyName VARCHAR2(100),
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50)                    not null,
   SparePartName        VARCHAR2(100)                   not null,
   PlannedAmount        NUMBER(9)                       not null,
   OutboundFulfillment  NUMBER(9),
   UnOutboundAmount     NUMBER(9),
   SalesPrice           NUMBER(19,4),
   UnOutFee             NUMBER(19,4),
   FulfillmentFee       NUMBER(19,4),
   OutJudgment          NUMBER(9),
   CreateTime           DATE,
   constraint PK_OUTBOUNDDETAIL primary key (Id)
)
/

/*==============================================================*/
/* Table: OutoundAnalysis                                       */
/*==============================================================*/
create table OutoundAnalysis  (
   Id                   NUMBER(9)                       not null,
   WarehouseId          NUMBER(9)                       not null,
   WarehouseCode        VARCHAR2(50)                    not null,
   WarehouseName        VARCHAR2(100)                   not null,
   AppAmountFee         NUMBER(19,4),
   NormalTUnFee         NUMBER(19,4),
   NormalTOverFee       NUMBER(19,4),
   NormalTOutFee        NUMBER(19,4),
   NormalTOutOverFee    NUMBER(19,4),
   TOutRate             NUMBER(19,4),
   NormalTUnOutFee      NUMBER(19,4),
   NormalTUnOutRate     NUMBER(19,4),
   NormalFUnFee         NUMBER(19,4),
   NormalFOverFee       NUMBER(19,4),
   NormalFOutFee        NUMBER(19,4),
   FOutRate             NUMBER(19,4),
   NormalFOutOverFee    NUMBER(19,4),
   NormalFUnOutFee      NUMBER(19,4),
   NormalFUnOutRate     NUMBER(19,4),
   UrgentFee            NUMBER(19,4),
   UrgentTwUnFee        NUMBER(19,4),
   UrgentTwOverFee      NUMBER(19,4),
   UrgentTwOutFee       NUMBER(19,4),
   UrgentTwOutOverFee   NUMBER(19,4),
   UrgentTwRate         NUMBER(19,4),
   UrgentOneUnFee       NUMBER(19,4),
   UrgentOneOutFee      NUMBER(19,4),
   UrgentOneOutOverFee  NUMBER(19,4),
   UrgentOneRate        NUMBER(19,4),
   CreateTime           DATE,
   constraint PK_OUTOUNDANALYSIS primary key (Id)
)
/

/*==============================================================*/
/* Table: PackagingCollection                                   */
/*==============================================================*/
create table PackagingCollection  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   WarehouseId          NUMBER(9),
   WarehouseCode        VARCHAR2(50),
   WarehouseName        VARCHAR2(100),
   Status               NUMBER(9),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   CheckerId            NUMBER(9),
   CheckerName          VARCHAR2(100),
   CheckTime            DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   constraint PK_PACKAGINGCOLLECTION primary key (Id)
)
/

/*==============================================================*/
/* Table: PackagingCollectionDetail                             */
/*==============================================================*/
create table PackagingCollectionDetail  (
   Id                   NUMBER(9)                       not null,
   PackagingCollectionId NUMBER(9)                       not null,
   PackingId            NUMBER(9)                       not null,
   PackingCode          VARCHAR2(50)                    not null,
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50),
   SparePartName        VARCHAR2(100),
   PlanQty              NUMBER(9),
   PackingMaterialId    NUMBER(9),
   PackingMaterialName  VARCHAR2(100),
   PackingMaterial      VARCHAR2(50),
   ReferenceCode        VARCHAR2(50),
   PackNum              NUMBER(9),
   PackNumConfirm       NUMBER(9),
   constraint PK_PACKAGINGCOLLECTIONDETAIL primary key (Id)
)
/

/*==============================================================*/
/* Table: PackingTask                                           */
/*==============================================================*/
create table PackingTask  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   PartsInboundPlanCode VARCHAR2(50),
   PartsInboundCheckBillCode VARCHAR2(100),
   SparePartCode        VARCHAR2(50)                    not null,
   SparePartName        VARCHAR2(100)                   not null,
   PlanQty              NUMBER(9)                       not null,
   PackingQty           NUMBER(9),
   SourceCode           VARCHAR2(50),
   Status               NUMBER(9),
   CounterpartCompanyCode VARCHAR2(50),
   CounterpartCompanyName VARCHAR2(100),
   ExpectedPlaceDate    DATE,
   RecomPackingMaterial VARCHAR2(50),
   PhyPackingMaterial   VARCHAR2(50),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   WarehouseId          NUMBER(9),
   WarehouseCode        VARCHAR2(50),
   WarehouseName        VARCHAR2(100),
   PartsInboundPlanId   NUMBER(9),
   PartsInboundCheckBillId NUMBER(9),
   SparePartId          NUMBER(9),
   BatchNumber          VARCHAR2(50),
   Remark               VARCHAR2(500),
   IsUse                NUMBER(1),
   constraint PK_PACKINGTASK primary key (Id)
)
/

/*==============================================================*/
/* Table: PackingTaskDetail                                     */
/*==============================================================*/
create table PackingTaskDetail  (
   Id                   NUMBER(9)                       not null,
   PackingTaskId        NUMBER(9)                       not null,
   RecomPackingMaterial VARCHAR2(50),
   RecomQty             NUMBER(9),
   PhyPackingMaterial   VARCHAR2(50),
   PhyQty               NUMBER(9),
   constraint PK_PACKINGTASKDETAIL primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsDifferenceBackBill                               */
/*==============================================================*/
create table PartsDifferenceBackBill  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   Status               NUMBER(9)                       not null,
   Type                 NUMBER(9)                       not null,
   SourceCode           VARCHAR2(50)                    not null,
   PartsInboundCheckBillCode VARCHAR2(50),
   SubmitterId          NUMBER(9),
   SubmitterName        VARCHAR2(100),
   SubmitTime           DATE,
   InitialApproverId    NUMBER(9),
   InitialApproverName  VARCHAR2(100),
   InitialApproveTime   DATE,
   CheckerId            NUMBER(9),
   CheckerName          VARCHAR2(100),
   CheckTime            DATE,
   ApproverId           NUMBER(9),
   ApproverName         VARCHAR2(100),
   ApproveTime          DATE,
   RejecterId           NUMBER(9),
   RejecterName         VARCHAR2(100),
   RejectTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   Path                 VARCHAR2(2000),
   IsAttach             NUMBER(1),
   OriginalRequirementBillId NUMBER(9),
   OriginalRequirementBillCode VARCHAR2(100),
   PartsSupplierId      NUMBER(9),
   PartsSupplierCode    VARCHAR2(50),
   PartsSupplierName    VARCHAR2(100),
   constraint PK_PARTSDIFFERENCEBACKBILL primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsDifferenceBackBillDtl                            */
/*==============================================================*/
create table PartsDifferenceBackBillDtl  (
   Id                   NUMBER(9)                       not null,
   PartsDifferenceBackBillId NUMBER(9)                       not null,
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50)                    not null,
   SparePartName        VARCHAR2(100)                   not null,
   DiffQuantity         NUMBER(9)                       not null,
   ErrorType            NUMBER(9)                       not null,
   TaskId               NUMBER(9),
   TaskCode             VARCHAR2(50),
   TraceProperty        NUMBER(9),
   TraceCode            VARCHAR2(3000),
   constraint PK_PARTSDIFFERENCEBACKBILLDTL primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsInboundCheckBill                                 */
/*==============================================================*/
create table PartsInboundCheckBill  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   PartsInboundPlanId   NUMBER(9)                       not null,
   WarehouseId          NUMBER(9)                       not null,
   WarehouseCode        VARCHAR2(50)                    not null,
   WarehouseName        VARCHAR2(100)                   not null,
   StorageCompanyId     NUMBER(9)                       not null,
   StorageCompanyCode   VARCHAR2(50)                    not null,
   StorageCompanyName   VARCHAR2(100)                   not null,
   StorageCompanyType   NUMBER(9)                       not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   BranchId             NUMBER(9)                       not null,
   BranchCode           VARCHAR2(50)                    not null,
   BranchName           VARCHAR2(100)                   not null,
   CounterpartCompanyId NUMBER(9)                       not null,
   CounterpartCompanyCode VARCHAR2(50)                    not null,
   CounterpartCompanyName VARCHAR2(100)                   not null,
   InboundType          NUMBER(9)                       not null,
   CustomerAccountId    NUMBER(9),
   OriginalRequirementBillId NUMBER(9)                       not null,
   OriginalRequirementBillType NUMBER(9)                       not null,
   OriginalRequirementBillCode VARCHAR2(50)                    not null,
   Objid                VARCHAR2(25),
   Status               NUMBER(9)                       not null,
   SettlementStatus     NUMBER(9)                       not null,
   Remark               VARCHAR2(400),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   GPMSPurOrderCode     VARCHAR2(50),
   PurOrderCode         VARCHAR2(50),
   ReturnContainerNumber VARCHAR2(25),
   SAPPurchasePlanCode  VARCHAR2(50),
   ERPSourceOrderCode   VARCHAR2(500),
   EcommerceMoney       NUMBER(19,4),
   PWMSStorageCode      VARCHAR2(50),
   CPPartsPurchaseOrderCode VARCHAR2(50),
   CPPartsInboundCheckCode VARCHAR2(50),
   IsDifference         NUMBER(1),
   IsSync               NUMBER(1),
   SyncTime             DATE,
   constraint PK_PARTSINBOUNDCHECKBILL primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsInboundCheckBillDetail                           */
/*==============================================================*/
create table PartsInboundCheckBillDetail  (
   Id                   NUMBER(9)                       not null,
   PartsInboundCheckBillId NUMBER(9)                       not null,
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50)                    not null,
   SparePartName        VARCHAR2(100)                   not null,
   WarehouseAreaId      NUMBER(9)                       not null,
   WarehouseAreaCode    VARCHAR2(50)                    not null,
   InspectedQuantity    NUMBER(9)                       not null,
   SettlementPrice      NUMBER(19,4)                    not null,
   CostPrice            NUMBER(19,4)                    not null,
   SpareOrderRemark     VARCHAR2(200),
   Remark               VARCHAR2(200),
   OverseasPartsFigure  VARCHAR2(25),
   OverseasProjectNumber NUMBER(9),
   ZXIANGI              VARCHAR2(25),
   POCode               VARCHAR2(50),
   IsPacking            NUMBER(1),
   OriginalPrice        NUMBER(19,4),
   BatchNumber          VARCHAR2(50),
   constraint PK_PARTSINBOUNDCHECKBILLDETAIL primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsInboundCheckBill_Sync                            */
/*==============================================================*/
create table PartsInboundCheckBill_Sync  (
   Id                   NUMBER(9)                       not null,
   Objid                VARCHAR2(40),
   IOStatus             NUMBER(9),
   CreateTime           DATE,
   FirstSendTime        DATE,
   ModifySendTime       DATE,
   Token                VARCHAR2(50),
   Sender               VARCHAR2(20),
   Message              VARCHAR2(500)
)
/

/*==============================================================*/
/* Table: PartsInboundPackingDetail                             */
/*==============================================================*/
create table PartsInboundPackingDetail  (
   Id                   NUMBER(9)                       not null,
   PartsInboundCheckBillId NUMBER(9)                       not null,
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50)                    not null,
   SparePartName        VARCHAR2(100)                   not null,
   Quantity             NUMBER(9)                       not null,
   WarehouseAreaId      NUMBER(9)                       not null,
   WarehouseAreaCode    VARCHAR2(50)                    not null,
   BatchNumber          VARCHAR2(100)                   not null,
   PackingSpecification VARCHAR2(200),
   PackingMaterialQuantity NUMBER(9),
   Remark               VARCHAR2(200),
   constraint PK_PARTSINBOUNDPACKINGDETAIL primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsInboundPerformance                               */
/*==============================================================*/
create table PartsInboundPerformance  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50),
   InType               NUMBER(9),
   BarCode              VARCHAR2(50),
   WarehouseAreaId      NUMBER(9),
   Qty                  NUMBER(9),
   OperaterId           NUMBER(9),
   OperaterName         VARCHAR2(100),
   OperaterBeginTime    DATE,
   OperaterEndTime      DATE,
   Equipment            NUMBER(9),
   ContainerNum         NUMBER(9),
   SparePartId          NUMBER(9),
   SpareCode            VARCHAR2(50),
   SpareName            VARCHAR2(100),
   constraint PK_PARTSINBOUNDPERFORMANCE primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsInboundPlan                                      */
/*==============================================================*/
create table PartsInboundPlan  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   WarehouseId          NUMBER(9)                       not null,
   WarehouseCode        VARCHAR2(50)                    not null,
   WarehouseName        VARCHAR2(100)                   not null,
   StorageCompanyId     NUMBER(9)                       not null,
   StorageCompanyCode   VARCHAR2(50)                    not null,
   StorageCompanyName   VARCHAR2(100)                   not null,
   StorageCompanyType   NUMBER(9)                       not null,
   BranchId             NUMBER(9)                       not null,
   BranchCode           VARCHAR2(50)                    not null,
   BranchName           VARCHAR2(100)                   not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   CounterpartCompanyId NUMBER(9)                       not null,
   CounterpartCompanyCode VARCHAR2(50)                    not null,
   CounterpartCompanyName VARCHAR2(100)                   not null,
   SourceId             NUMBER(9)                       not null,
   SourceCode           VARCHAR2(50)                    not null,
   InboundType          NUMBER(9)                       not null,
   CustomerAccountId    NUMBER(9),
   OriginalRequirementBillId NUMBER(9)                       not null,
   OriginalRequirementBillType NUMBER(9)                       not null,
   OriginalRequirementBillCode VARCHAR2(50)                    not null,
   Status               NUMBER(9)                       not null,
   IfWmsInterface       NUMBER(1)                       not null,
   ArrivalDate          DATE,
   Remark               VARCHAR2(400),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   GPMSPurOrderCode     VARCHAR2(50),
   PartsPurchaseOrderTypeId NUMBER(9),
   RequestedDeliveryTime DATE,
   ReturnContainerNumber VARCHAR2(25),
   SAPPurchasePlanCode  VARCHAR2(50),
   BPMAudited           NUMBER(9),
   ERPSourceOrderCode   VARCHAR2(500),
   EcommerceMoney       NUMBER(19,4),
   PlanDeliveryTime     DATE,
   PackingFinishTime    DATE,
   ShelvesFinishTime    DATE,
   ReceivingFinishTime  DATE,
   constraint PK_PARTSINBOUNDPLAN primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsInboundPlanDetail                                */
/*==============================================================*/
create table PartsInboundPlanDetail  (
   Id                   NUMBER(9)                       not null,
   PartsInboundPlanId   NUMBER(9)                       not null,
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50)                    not null,
   SparePartName        VARCHAR2(100)                   not null,
   PlannedAmount        NUMBER(9)                       not null,
   InspectedQuantity    NUMBER(9),
   Price                NUMBER(19,4)                    not null,
   SpareOrderRemark     VARCHAR2(200),
   Remark               VARCHAR2(200),
   OverseasPartsFigure  VARCHAR2(25),
   OverseasProjectNumber NUMBER(9),
   OriginalPlanNumber   NUMBER(9),
   POCode               VARCHAR2(50),
   BatchNumber          VARCHAR2(50),
   OriginalPrice        NUMBER(19,4),
   constraint PK_PARTSINBOUNDPLANDETAIL primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsInventoryBill                                    */
/*==============================================================*/
create table PartsInventoryBill  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   BranchId             NUMBER(9)                       not null,
   WarehouseAreaCategory NUMBER(9)                       not null,
   WarehouseId          NUMBER(9)                       not null,
   WarehouseCode        VARCHAR2(50)                    not null,
   WarehouseName        VARCHAR2(100)                   not null,
   StorageCompanyId     NUMBER(9)                       not null,
   StorageCompanyCode   VARCHAR2(50)                    not null,
   StorageCompanyName   VARCHAR2(100)                   not null,
   StorageCompanyType   NUMBER(9)                       not null,
   InitiatorName        VARCHAR2(200),
   InventoryReason      VARCHAR2(200),
   AmountDifference     NUMBER(19,4),
   AccountingStatus     NUMBER(9),
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   Path                 VARCHAR2(2000),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   ResultInputOperatorId NUMBER(9),
   ResultInputOperatorName VARCHAR2(100),
   ResultInputTime      DATE,
   InitialApproverId    NUMBER(9),
   InitialApproverName  VARCHAR2(100),
   InitialApproveTime   DATE,
   CheckerId            NUMBER(9),
   CheckerName          VARCHAR2(100),
   CheckTime            DATE,
   ApproverId           NUMBER(9),
   ApproverName         VARCHAR2(100),
   ApproveTime          DATE,
   InventoryRecordOperatorId NUMBER(9),
   InventoryRecordOperatorName VARCHAR2(100),
   InventoryRecordTime  DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   RejecterId           NUMBER(9),
   RejecterName         VARCHAR2(100),
   RejectTime           DATE,
   RejectComment        VARCHAR2(200),
   RowVersion           TIMESTAMP,
   AdvancedAuditID      NUMBER(9),
   AdvancedAuditName    VARCHAR2(100),
   AdvancedAuditTime    NUMBER(9),
   constraint PK_PARTSINVENTORYBILL primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsInventoryDetail                                  */
/*==============================================================*/
create table PartsInventoryDetail  (
   Id                   NUMBER(9)                       not null,
   PartsInventoryBillId NUMBER(9)                       not null,
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50)                    not null,
   SparePartName        VARCHAR2(100)                   not null,
   WarehouseAreaId      NUMBER(9)                       not null,
   WarehouseAreaCode    VARCHAR2(50)                    not null,
   CurrentBatchNumber   VARCHAR2(100),
   CurrentStorage       NUMBER(9)                       not null,
   StorageAfterInventory NUMBER(9)                       not null,
   Ifcover              NUMBER(1),
   StorageDifference    NUMBER(9)                       not null,
   CostPrice            NUMBER(19,4),
   AmountDifference     NUMBER(19,4),
   NewBatchNumber       VARCHAR2(100),
   Remark               VARCHAR2(200),
   OverseasPartsFigure  VARCHAR2(25),
   constraint PK_PARTSINVENTORYDETAIL primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsLogisticBatch                                    */
/*==============================================================*/
create table PartsLogisticBatch  (
   Id                   NUMBER(9)                       not null,
   CompanyId            NUMBER(9)                       not null,
   Status               NUMBER(9)                       not null,
   ShippingStatus       NUMBER(9)                       not null,
   SourceId             NUMBER(9)                       not null,
   SourceType           NUMBER(9)                       not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_PARTSLOGISTICBATCH primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsLogisticBatchBillDetail                          */
/*==============================================================*/
create table PartsLogisticBatchBillDetail  (
   Id                   NUMBER(9)                       not null,
   PartsLogisticBatchId NUMBER(9)                       not null,
   BillId               NUMBER(9)                       not null,
   BillType             NUMBER(9)                       not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   constraint PK_PARTSLOGISTICBATCHBILLD primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsLogisticBatchItemDetail                          */
/*==============================================================*/
create table PartsLogisticBatchItemDetail  (
   Id                   NUMBER(9)                       not null,
   PartsLogisticBatchId NUMBER(9)                       not null,
   CounterpartCompanyId NUMBER(9)                       not null,
   ReceivingCompanyId   NUMBER(9)                       not null,
   ShippingCompanyId    NUMBER(9)                       not null,
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50)                    not null,
   SparePartName        VARCHAR2(100)                   not null,
   BatchNumber          VARCHAR2(100),
   OutboundAmount       NUMBER(9)                       not null,
   InboundAmount        NUMBER(9),
   OutReturnAmount      NUMBER(9)                       not null,
   constraint PK_PARTSLOGISTICBATCHITEMD primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsOutboundBill                                     */
/*==============================================================*/
create table PartsOutboundBill  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   PartsOutboundPlanId  NUMBER(9)                       not null,
   WarehouseId          NUMBER(9)                       not null,
   WarehouseCode        VARCHAR2(50)                    not null,
   WarehouseName        VARCHAR2(100)                   not null,
   StorageCompanyId     NUMBER(9)                       not null,
   StorageCompanyCode   VARCHAR2(50)                    not null,
   StorageCompanyName   VARCHAR2(100)                   not null,
   StorageCompanyType   NUMBER(9)                       not null,
   BranchId             NUMBER(9)                       not null,
   BranchCode           VARCHAR2(50)                    not null,
   BranchName           VARCHAR2(100)                   not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   PartsSalesOrderTypeId NUMBER(9),
   PartsSalesOrderTypeName VARCHAR2(100),
   CounterpartCompanyId NUMBER(9)                       not null,
   CounterpartCompanyCode VARCHAR2(50)                    not null,
   CounterpartCompanyName VARCHAR2(100)                   not null,
   ReceivingCompanyId   NUMBER(9)                       not null,
   ReceivingCompanyCode VARCHAR2(50)                    not null,
   ReceivingCompanyName VARCHAR2(100)                   not null,
   ReceivingWarehouseId NUMBER(9),
   ReceivingWarehouseCode VARCHAR2(50),
   ReceivingWarehouseName VARCHAR2(100),
   OutboundType         NUMBER(9)                       not null,
   ShippingMethod       NUMBER(9),
   CustomerAccountId    NUMBER(9),
   OriginalRequirementBillId NUMBER(9)                       not null,
   OriginalRequirementBillType NUMBER(9)                       not null,
   OriginalRequirementBillCode VARCHAR2(50)                    not null,
   SettlementStatus     NUMBER(9)                       not null,
   OrderApproveComment  VARCHAR2(2000),
   InterfaceRecordId    VARCHAR2(25),
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   OutboundPackPlanCode VARCHAR2(50),
   ContractCode         VARCHAR2(50),
   GPMSPurOrderCode     VARCHAR2(50),
   PrintTimes           NUMBER(9)                      default 0,
   PurDistributionStatus NUMBER(9),
   SAPPurchasePlanCode  VARCHAR2(50),
   ERPSourceOrderCode   VARCHAR2(500),
   EcommerceMoney       NUMBER(19,4),
   PWMSOutboundCode     VARCHAR2(50),
   CPPartsPurchaseOrderCode VARCHAR2(50),
   CPPartsInboundCheckCode VARCHAR2(50),
   PartsPurchaseSettleCode VARCHAR2(50),
   PartsShippingOrderId NUMBER(9),
   constraint PK_PARTSOUTBOUNDBILL primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsOutboundBillDetail                               */
/*==============================================================*/
create table PartsOutboundBillDetail  (
   Id                   NUMBER(9)                       not null,
   PartsOutboundBillId  NUMBER(9)                       not null,
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50)                    not null,
   SparePartName        VARCHAR2(100)                   not null,
   OutboundAmount       NUMBER(9)                       not null,
   WarehouseAreaId      NUMBER(9),
   WarehouseAreaCode    VARCHAR2(50),
   BatchNumber          VARCHAR2(100),
   SettlementPrice      NUMBER(19,4)                    not null,
   CostPrice            NUMBER(19,4)                    not null,
   Remark               VARCHAR2(200),
   ContainerNumber      VARCHAR2(25),
   ContainerType        VARCHAR2(50),
   SonContainer         VARCHAR2(255),
   PackageOrderNumber   VARCHAR2(25),
   PackageOrderNumberLine VARCHAR2(25),
   ContainerTotalVolume VARCHAR2(25),
   ContainerTotalWeight VARCHAR2(25),
   ContainerLong        VARCHAR2(25),
   ContainerWide        VARCHAR2(25),
   ContainerHigh        VARCHAR2(25),
   ContainerHeavy       VARCHAR2(25),
   OverseasPartsFigure  VARCHAR2(25),
   OverseasProjectNumber NUMBER(9),
   OriginalPrice        NUMBER(19,4),
   constraint PK_PARTSOUTBOUNDBILLDETAIL primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsOutboundPerformance                              */
/*==============================================================*/
create table PartsOutboundPerformance  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50),
   OutType              NUMBER(9),
   BarCode              VARCHAR2(50),
   WarehouseAreaId      NUMBER(9),
   Qty                  NUMBER(9),
   OperaterId           NUMBER(9),
   OperaterName         VARCHAR2(100),
   OperaterBeginTime    DATE,
   OperaterEndTime      DATE,
   Equipment            NUMBER(9),
   ContainerNumber      VARCHAR2(50),
   SparePartId          NUMBER(9),
   SpareCode            VARCHAR2(50),
   SpareName            VARCHAR2(100),
   PickingTaskDetailId  NUMBER(9),
   constraint PK_PARTSOUTBOUNDPERFORMANCE primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsOutboundPlan                                     */
/*==============================================================*/
create table PartsOutboundPlan  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   WarehouseId          NUMBER(9)                       not null,
   WarehouseCode        VARCHAR2(50)                    not null,
   WarehouseName        VARCHAR2(100)                   not null,
   StorageCompanyId     NUMBER(9)                       not null,
   StorageCompanyCode   VARCHAR2(50)                    not null,
   StorageCompanyName   VARCHAR2(100)                   not null,
   StorageCompanyType   NUMBER(9)                       not null,
   CompanyAddressId     NUMBER(9),
   BranchId             NUMBER(9)                       not null,
   BranchCode           VARCHAR2(50)                    not null,
   BranchName           VARCHAR2(100)                   not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   PartsSalesOrderTypeId NUMBER(9),
   PartsSalesOrderTypeName VARCHAR2(100),
   CounterpartCompanyId NUMBER(9)                       not null,
   CounterpartCompanyCode VARCHAR2(50)                    not null,
   CounterpartCompanyName VARCHAR2(100)                   not null,
   ReceivingCompanyId   NUMBER(9)                       not null,
   ReceivingCompanyCode VARCHAR2(50)                    not null,
   ReceivingCompanyName VARCHAR2(100)                   not null,
   ReceivingWarehouseId NUMBER(9),
   ReceivingWarehouseCode VARCHAR2(50),
   ReceivingWarehouseName VARCHAR2(100),
   SourceId             NUMBER(9)                       not null,
   SourceCode           VARCHAR2(50)                    not null,
   OutboundType         NUMBER(9)                       not null,
   ShippingMethod       NUMBER(9),
   CustomerAccountId    NUMBER(9),
   OriginalRequirementBillId NUMBER(9)                       not null,
   OriginalRequirementBillType NUMBER(9)                       not null,
   OriginalRequirementBillCode VARCHAR2(50)                    not null,
   Status               NUMBER(9)                       not null,
   StopComment          VARCHAR2(200),
   IfWmsInterface       NUMBER(1)                       not null,
   OrderApproveComment  VARCHAR2(2000),
   Remark               VARCHAR2(500),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   StoperId             NUMBER(9),
   Stoper               VARCHAR2(100),
   StopTime             DATE,
   RowVersion           TIMESTAMP,
   GPMSPurOrderCode     VARCHAR2(50),
   IsPurDistribution    NUMBER(1),
   SPRAS                VARCHAR2(25),
   IsWoodenFumigation   VARCHAR2(1),
   IsAdvanceSignature   VARCHAR2(1),
   PackingDescription   VARCHAR2(25),
   Mark                 VARCHAR2(25),
   IsInspectionCamera   VARCHAR2(1),
   OtherPackingRequirements VARCHAR2(25),
   IsSpecialInspection  VARCHAR2(25),
   ZPNUMBER             VARCHAR2(20),
   SAPPurchasePlanCode  VARCHAR2(50),
   ERPSourceOrderCode   VARCHAR2(500),
   EcommerceMoney       NUMBER(19,4),
   OrderTypeId          NUMBER(9),
   OrderTypeName        VARCHAR2(100),
   ReceivingAddress     VARCHAR2(200),
   IsTurn               NUMBER(1),
   constraint PK_PARTSOUTBOUNDPLAN primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsOutboundPlanDetail                               */
/*==============================================================*/
create table PartsOutboundPlanDetail  (
   Id                   NUMBER(9)                       not null,
   PartsOutboundPlanId  NUMBER(9)                       not null,
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50)                    not null,
   SparePartName        VARCHAR2(100)                   not null,
   PlannedAmount        NUMBER(9)                       not null,
   OutboundFulfillment  NUMBER(9),
   Price                NUMBER(19,4)                    not null,
   Remark               VARCHAR2(200),
   OverseasPartsFigure  VARCHAR2(25),
   OverseasProjectNumber NUMBER(9),
   Vbeln                VARCHAR2(10),
   Posnr                NUMBER(9),
   Ebeln                VARCHAR2(10),
   Ebelp                NUMBER(9),
   Zpitem               NUMBER(9),
   Pnumber              VARCHAR2(20),
   OriginalPrice        NUMBER(19,4),
   DownloadQty          NUMBER(9),
   constraint PK_PARTSOUTBOUNDPLANDETAIL primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsPacking                                          */
/*==============================================================*/
create table PartsPacking  (
   Id                   NUMBER(9)                       not null,
   PartsStockId         NUMBER(9)                       not null,
   WarehouseId          NUMBER(9)                       not null,
   WarehouseCode        VARCHAR2(50)                    not null,
   WarehouseName        VARCHAR2(100)                   not null,
   StorageCompanyId     NUMBER(9)                       not null,
   StorageCompanyType   NUMBER(9)                       not null,
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50)                    not null,
   SparePartName        VARCHAR2(100)                   not null,
   Quantity             NUMBER(9)                       not null,
   WarehouseAreaId      NUMBER(9)                       not null,
   WarehouseAreaCode    VARCHAR2(50)                    not null,
   BatchNumber          VARCHAR2(100)                   not null,
   PackingSpecification VARCHAR2(200),
   PackingMaterialQuantity NUMBER(9),
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_PARTSPACKING primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsRtnInboundCheck                                  */
/*==============================================================*/
create table PartsRtnInboundCheck  (
   Id                   NUMBER(9)                       not null,
   MarketingDepartmentName VARCHAR2(200),
   Province             VARCHAR2(200),
   WarehouseId          NUMBER(9),
   WarehouseName        VARCHAR2(200),
   CounterpartCompanyName VARCHAR2(200),
   CounterpartCompanyCode VARCHAR2(200),
   PartsSalesReturnBillId NUMBER(9),
   PartsSalesReturnBillCode VARCHAR2(50),
   ReturnWarehouseCode  VARCHAR2(150),
   ReturnWarehouseName  VARCHAR2(150),
   ReturnReason         VARCHAR2(500),
   InBoundId            NUMBER(9),
   Code                 VARCHAR2(50),
   SparePartId          NUMBER(9),
   SparePartCode        VARCHAR2(50),
   SparePartName        VARCHAR2(150),
   PartABC              NUMBER(9),
   InspectedQuantity    NUMBER(9),
   OriginalPrice        NUMBER(19,4),
   OriginalPriceAll     NUMBER(19,4),
   SettlementPrice      NUMBER(19,4),
   SettlementPriceAll   NUMBER(19,4),
   IsDiscount           NUMBER(1),
   PriceType            VARCHAR2(150),
   CostPrice            NUMBER(19,4),
   CostPriceAll         NUMBER(19,4),
   SettlementId         NUMBER(9),
   SettlementNo         VARCHAR2(50),
   CreateTime           DATE,
   CreatorName          VARCHAR2(150),
   TurnDate             DATE,
   constraint PK_PARTSRTNINBOUNDCHECK primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsShelvesTask                                      */
/*==============================================================*/
create table PartsShelvesTask  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50),
   PackingTaskId        NUMBER(9),
   PackingTaskCode      VARCHAR2(50),
   PartsInboundPlanId   NUMBER(9),
   PartsInboundPlanCode VARCHAR2(50),
   PartsInboundCheckBillId NUMBER(9),
   PartsInboundCheckBillCode VARCHAR2(100),
   WarehouseId          NUMBER(9),
   WarehouseCode        VARCHAR2(50),
   WarehouseName        VARCHAR2(100),
   SparePartId          NUMBER(9),
   PlannedAmount        NUMBER(9),
   ShelvesAmount        NUMBER(9),
   SourceBillCode       VARCHAR2(100),
   Status               NUMBER(9),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   BatchNumber          VARCHAR2(50),
   Remark               VARCHAR2(200),
   ShelvesFinishTime    DATE,
   SIHCodes             VARCHAR2(3000),
   NewRemark            VARCHAR2(200),
   constraint PK_PARTSSHELVESTASK primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsShiftOrder                                       */
/*==============================================================*/
create table PartsShiftOrder  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   BranchId             NUMBER(9)                       not null,
   WarehouseId          NUMBER(9)                       not null,
   WarehouseCode        VARCHAR2(50)                    not null,
   WarehouseName        VARCHAR2(100)                   not null,
   StorageCompanyId     NUMBER(9)                       not null,
   StorageCompanyCode   VARCHAR2(50)                    not null,
   StorageCompanyName   VARCHAR2(100)                   not null,
   StorageCompanyType   NUMBER(9)                       not null,
   Status               NUMBER(9)                       not null,
   Type                 NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   RowVersion           TIMESTAMP,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   ApproverId           NUMBER(9),
   ApproverName         VARCHAR2(100),
   ApproveTime          DATE,
   InitialApproverId    NUMBER(9),
   InitialApproverName  VARCHAR2(100),
   InitialApproveTime   DATE,
   QuestionType         NUMBER(9),
   Path                 VARCHAR2(2000),
   ShiftStatus          NUMBER(9),
   SubmitterId          NUMBER(9),
   SubmitterName        VARCHAR2(100),
   SubmitTime           DATE,
   CloserId             NUMBER(9),
   CloserName           VARCHAR2(100),
   CloseTime            DATE,
   constraint PK_PARTSSHIFTORDER primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsShiftOrderDetail                                 */
/*==============================================================*/
create table PartsShiftOrderDetail  (
   Id                   NUMBER(9)                       not null,
   PartsShiftOrderId    NUMBER(9)                       not null,
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50)                    not null,
   SparePartName        VARCHAR2(100)                   not null,
   OriginalWarehouseAreaId NUMBER(9)                       not null,
   OriginalWarehouseAreaCode VARCHAR2(50)                    not null,
   OriginalWarehouseAreaCategory NUMBER(9)                       not null,
   DestWarehouseAreaId  NUMBER(9)                       not null,
   DestWarehouseAreaCode VARCHAR2(50)                    not null,
   DestWarehouseAreaCategory NUMBER(9)                       not null,
   BatchNumber          VARCHAR2(100),
   Quantity             NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   UpShelfQty           NUMBER(9),
   DownShelfQty         NUMBER(9),
   SIHCode              VARCHAR2(4000),
   DownSIHCode          VARCHAR2(4000),
   constraint PK_PARTSSHIFTORDERDETAIL primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsShiftSIHDetail                                   */
/*==============================================================*/
create table PartsShiftSIHDetail  (
   Id                   NUMBER(9)                       not null,
   PartsShiftOrderId    NUMBER(9),
   PartsShiftOrderDetailId NUMBER(9),
   Type                 NUMBER(9),
   SIHCode              VARCHAR2(100),
   Quantity             NUMBER(9),
   constraint PK_PARTSSHIFTSIHDETAIL primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsTransferOrder                                    */
/*==============================================================*/
create table PartsTransferOrder  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   OriginalWarehouseId  NUMBER(9)                       not null,
   OriginalWarehouseCode VARCHAR2(50)                    not null,
   OriginalWarehouseName VARCHAR2(100)                   not null,
   StorageCompanyId     NUMBER(9)                       not null,
   StorageCompanyType   NUMBER(9)                       not null,
   OriginalBillId       NUMBER(9),
   OriginalBillCode     VARCHAR2(50),
   DestWarehouseId      NUMBER(9)                       not null,
   DestWarehouseCode    VARCHAR2(50)                    not null,
   DestWarehouseName    VARCHAR2(100)                   not null,
   TotalAmount          NUMBER(19,4)                    not null,
   TotalPlanAmount      NUMBER(19,4),
   Status               NUMBER(9)                       not null,
   SAPPurchasePlanCode  VARCHAR2(50),
   Type                 NUMBER(9)                       not null,
   AbandonComment       VARCHAR2(200),
   ShippingMethod       NUMBER(9),
   ReceivingAddress     VARCHAR2(200),
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   ApproverId           NUMBER(9),
   ApproverName         VARCHAR2(100),
   ApproveTime          DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   RowVersion           TIMESTAMP,
   GPMSPurOrderCode     VARCHAR2(50),
   PurOrderCode         VARCHAR2(50),
   BPMAudited           NUMBER(9),
   ERPSourceOrderCode   VARCHAR2(500),
   EcommerceMoney       NUMBER(19,4),
   InitialApproverId    NUMBER(9),
   InitialApproverName  VARCHAR2(100),
   InitialApproveTime   DATE,
   Path                 VARCHAR2(2000),
   constraint PK_PARTSTRANSFERORDER primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsTransferOrderDetail                              */
/*==============================================================*/
create table PartsTransferOrderDetail  (
   Id                   NUMBER(9)                       not null,
   PartsTransferOrderId NUMBER(9)                       not null,
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50)                    not null,
   SparePartName        VARCHAR2(100)                   not null,
   PlannedAmount        NUMBER(9)                       not null,
   ConfirmedAmount      NUMBER(9),
   Price                NUMBER(19,4)                    not null,
   PlannPrice           NUMBER(19,4),
   Remark               VARCHAR2(200),
   OverseasPartsFigure  VARCHAR2(25),
   OverseasProjectNumber NUMBER(9),
   OriginalPlanNumber   NUMBER(9),
   POCode               VARCHAR2(50),
   constraint PK_PARTSTRANSFERORDERDETAIL primary key (Id)
)
/

/*==============================================================*/
/* Table: PickingTask                                           */
/*==============================================================*/
create table PickingTask  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   WarehouseId          NUMBER(9),
   WarehouseCode        VARCHAR2(50),
   WarehouseName        VARCHAR2(100),
   CounterpartCompanyId NUMBER(9),
   CounterpartCompanyCode VARCHAR2(50),
   CounterpartCompanyName VARCHAR2(100),
   Status               NUMBER(9),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   PickingFinishTime    DATE,
   OrderTypeId          NUMBER(9),
   OrderTypeName        VARCHAR2(100),
   ShippingMethod       NUMBER(9),
   IsExistShippingOrder NUMBER(1),
   Remark               VARCHAR2(200),
   ResponsiblePersonId  NUMBER(9),
   ResponsiblePersonName VARCHAR2(100),
   BranchId             NUMBER(9),
   constraint PK_PICKINGTASK primary key (Id)
)
/

/*==============================================================*/
/* Table: PickingTaskBatchDetail                                */
/*==============================================================*/
create table PickingTaskBatchDetail  (
   Id                   NUMBER(9)                       not null,
   PickingTaskDetailId  NUMBER(9)                       not null,
   BatchNumber          VARCHAR2(50),
   PickingQty           NUMBER(9),
   PickingTime          DATE,
   constraint PK_PICKINGTASKBATCHDETAIL primary key (Id)
)
/

/*==============================================================*/
/* Table: PickingTaskDetail                                     */
/*==============================================================*/
create table PickingTaskDetail  (
   Id                   NUMBER(9)                       not null,
   PickingTaskId        NUMBER(9)                       not null,
   PartsOutboundPlanId  NUMBER(9),
   PartsOutboundPlanCode VARCHAR2(50),
   SourceCode           VARCHAR2(50),
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50)                    not null,
   SparePartName        VARCHAR2(100)                   not null,
   MeasureUnit          VARCHAR2(50),
   WarehouseAreaId      NUMBER(9),
   WarehouseAreaCode    VARCHAR2(50),
   PlanQty              NUMBER(9),
   PickingQty           NUMBER(9),
   ShortPickingReason   NUMBER(9),
   Price                NUMBER(19,4),
   constraint PK_PICKINGTASKDETAIL primary key (Id)
)
/

/*==============================================================*/
/* Table: SupplierInventoryBill                                 */
/*==============================================================*/
create table SupplierInventoryBill  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   PartsSupplierId      NUMBER(9)                       not null,
   PartsSupplierCode    VARCHAR2(50)                    not null,
   PartsSupplierName    VARCHAR2(100)                   not null,
   Status               NUMBER(9)                       not null,
   Path                 VARCHAR2(2000),
   IsUploadFile         NUMBER(1),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   RejecterId           NUMBER(9),
   RejecterName         VARCHAR2(100),
   RejectTime           DATE,
   RejectComment        VARCHAR2(200),
   CheckerId            NUMBER(9),
   CheckerName          VARCHAR2(100),
   CheckTime            DATE,
   RowVersion           TIMESTAMP,
   constraint PK_SUPPLIERINVENTORYBILL primary key (Id)
)
/

/*==============================================================*/
/* Table: SupplierInventoryBillDetail                           */
/*==============================================================*/
create table SupplierInventoryBillDetail  (
   Id                   NUMBER(9)                       not null,
   SupplierInventoryBillId NUMBER(9)                       not null,
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50),
   SparePartName        VARCHAR2(100),
   SupplierPartCode     VARCHAR2(100),
   CurrentStorage       NUMBER(9)                       not null,
   StorageAfterInventory NUMBER(9)                       not null,
   StorageDifference    NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   constraint PK_SUPPLIERINVENTORYBILLDETAIL primary key (Id)
)
/

/*==============================================================*/
/* Table: SupplierReserveRecommendPlan                          */
/*==============================================================*/
create table SupplierReserveRecommendPlan  (
   Id                   NUMBER(9)                       not null,
   PartsSupplierCode    VARCHAR2(50)                    not null,
   PartsSupplierName    VARCHAR2(100)                   not null,
   SupplierPartCode     VARCHAR2(50)                    not null,
   PartCode             VARCHAR2(50)                    not null,
   PartName             VARCHAR2(100)                   not null,
   IsPrimary            NUMBER(1),
   IsDirectSupply       NUMBER(1),
   IsOrderable          NUMBER(1),
   PackingCoefficient   NUMBER(9),
   IsPurchasePricing    NUMBER(1),
   CustomerCount        NUMBER(9),
   DaysAvg              NUMBER(9),
   LowerLimit           NUMBER(9),
   StandStock           NUMBER(9),
   UpperLimit           NUMBER(9),
   ActQty               NUMBER(9),
   NoShippingQuantity   NUMBER(9),
   NoInReserveQuantity  NUMBER(9),
   Quantity             NUMBER(9),
   RecommendQuantity    NUMBER(9),
   constraint PK_SUPPLIERRESERVERECOMMENDPLA primary key (Id)
)
/

/*==============================================================*/
/* Table: SupplierStoreInboundDetail                            */
/*==============================================================*/
create table SupplierStoreInboundDetail  (
   Id                   NUMBER(9)                       not null,
   PlanId               NUMBER(9)                       not null,
   InBoundQty           NUMBER(9),
   InBoundDate          DATE,
   InBoundName          VARCHAR2(100),
   constraint PK_SUPPLIERSTOREINBOUNDDETAIL primary key (Id)
)
/

/*==============================================================*/
/* Table: SupplierStorePlanDetail                               */
/*==============================================================*/
create table SupplierStorePlanDetail  (
   Id                   NUMBER(9)                       not null,
   PlanId               NUMBER(9)                       not null,
   SIHBox               VARCHAR2(200),
   SIHPiece             VARCHAR2(200),
   TraceCode            VARCHAR2(100),
   InboundQty           NUMBER(9),
   constraint PK_SUPPLIERSTOREPLANDETAIL primary key (Id)
)
/

/*==============================================================*/
/* Table: SupplierStorePlanOrder                                */
/*==============================================================*/
create table SupplierStorePlanOrder  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   Status               NUMBER(9)                       not null,
   PartsSupplierId      NUMBER(9)                       not null,
   PartsSupplierCode    VARCHAR2(50)                    not null,
   PartsSupplierName    VARCHAR2(100)                   not null,
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50)                    not null,
   SparePartName        VARCHAR2(100)                   not null,
   SupplierPartCode     VARCHAR2(50)                    not null,
   TraceProperty        NUMBER(9),
   PlanQty              NUMBER(9),
   InboundQty           NUMBER(9),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   InboudId             NUMBER(9),
   InboundName          VARCHAR2(100),
   InboundTime          DATE,
   StoperId             NUMBER(9),
   Stoper               VARCHAR2(100),
   StopTime             DATE,
   RowVersion           TIMESTAMP,
   BindQty              NUMBER(9),
   constraint PK_SUPPLIERSTOREPLANORDER primary key (Id)
)
/

/*==============================================================*/
/* Table: VersionInfo                                           */
/*==============================================================*/
create table VersionInfo  (
   Id                   NUMBER(9)                       not null,
   CompanyId            NUMBER(9)                       not null,
   CompanyCode          VARCHAR2(50)                    not null,
   Type                 NUMBER(9),
   SerialCode           NUMBER(9),
   BottomStockForceReserveSubId NUMBER(9),
   constraint PK_VERSIONINFO primary key (Id)
)
/

