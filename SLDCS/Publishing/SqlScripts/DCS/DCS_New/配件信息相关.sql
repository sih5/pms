/*==============================================================*/
/* DBMS name:      ORACLE Version 10gR2                         */
/* Created on:     2023/12/4 9:57:55                            */
/*==============================================================*/


declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('ABCSetting');
  if num>0 then
    execute immediate 'drop table ABCSetting cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('ABCStrategy');
  if num>0 then
    execute immediate 'drop table ABCStrategy cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('ABCTypeSafeDays');
  if num>0 then
    execute immediate 'drop table ABCTypeSafeDays cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('AssemblyPartRequisitionLink');
  if num>0 then
    execute immediate 'drop table AssemblyPartRequisitionLink cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('CalculatIntelligentPlan');
  if num>0 then
    execute immediate 'drop table CalculatIntelligentPlan cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('CombinedPart');
  if num>0 then
    execute immediate 'drop table CombinedPart cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('GoldenTaxClassifyMsg');
  if num>0 then
    execute immediate 'drop table GoldenTaxClassifyMsg cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('IntelligentOrderMonthlyBase');
  if num>0 then
    execute immediate 'drop table IntelligentOrderMonthlyBase cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('IntelligentOrderWeeklyBase');
  if num>0 then
    execute immediate 'drop table IntelligentOrderWeeklyBase cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartDeleaveHistory');
  if num>0 then
    execute immediate 'drop table PartDeleaveHistory cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartDeleaveInformation');
  if num>0 then
    execute immediate 'drop table PartDeleaveInformation cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsBranch');
  if num>0 then
    execute immediate 'drop table PartsBranch cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsBranchHistory');
  if num>0 then
    execute immediate 'drop table PartsBranchHistory cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsBranchPackingProp');
  if num>0 then
    execute immediate 'drop table PartsBranchPackingProp cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsExchange');
  if num>0 then
    execute immediate 'drop table PartsExchange cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsExchangeGroup');
  if num>0 then
    execute immediate 'drop table PartsExchangeGroup cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsExchangeHistory');
  if num>0 then
    execute immediate 'drop table PartsExchangeHistory cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsPackingPropAppDetail');
  if num>0 then
    execute immediate 'drop table PartsPackingPropAppDetail cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsPackingPropertyApp');
  if num>0 then
    execute immediate 'drop table PartsPackingPropertyApp cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsPriceIncreaseRate');
  if num>0 then
    execute immediate 'drop table PartsPriceIncreaseRate cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsPriorityBill');
  if num>0 then
    execute immediate 'drop table PartsPriorityBill cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsReplacement');
  if num>0 then
    execute immediate 'drop table PartsReplacement cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsReplacementHistory');
  if num>0 then
    execute immediate 'drop table PartsReplacementHistory cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('ProductStandard');
  if num>0 then
    execute immediate 'drop table ProductStandard cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('SEP_SparePartInfo');
  if num>0 then
    execute immediate 'drop table SEP_SparePartInfo cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('SmartWarehousePartRelation');
  if num>0 then
    execute immediate 'drop table SmartWarehousePartRelation cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('SparePart');
  if num>0 then
    execute immediate 'drop table SparePart cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('SparePartHistory');
  if num>0 then
    execute immediate 'drop table SparePartHistory cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('WarehousePartRelation');
  if num>0 then
    execute immediate 'drop table WarehousePartRelation cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_ABCSetting');
  if num>0 then
    execute immediate 'drop sequence S_ABCSetting';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_ABCStrategy');
  if num>0 then
    execute immediate 'drop sequence S_ABCStrategy';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_ABCTypeSafeDays');
  if num>0 then
    execute immediate 'drop sequence S_ABCTypeSafeDays';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_AssemblyPartRequisitionLink');
  if num>0 then
    execute immediate 'drop sequence S_AssemblyPartRequisitionLink';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_CalculatIntelligentPlan');
  if num>0 then
    execute immediate 'drop sequence S_CalculatIntelligentPlan';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_CombinedPart');
  if num>0 then
    execute immediate 'drop sequence S_CombinedPart';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_GoldenTaxClassifyMsg');
  if num>0 then
    execute immediate 'drop sequence S_GoldenTaxClassifyMsg';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_IntelligentOrderMonthlyBase');
  if num>0 then
    execute immediate 'drop sequence S_IntelligentOrderMonthlyBase';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_IntelligentOrderWeeklyBase');
  if num>0 then
    execute immediate 'drop sequence S_IntelligentOrderWeeklyBase';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartDeleaveHistory');
  if num>0 then
    execute immediate 'drop sequence S_PartDeleaveHistory';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartDeleaveInformation');
  if num>0 then
    execute immediate 'drop sequence S_PartDeleaveInformation';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsBranch');
  if num>0 then
    execute immediate 'drop sequence S_PartsBranch';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsBranchHistory');
  if num>0 then
    execute immediate 'drop sequence S_PartsBranchHistory';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsBranchPackingProp');
  if num>0 then
    execute immediate 'drop sequence S_PartsBranchPackingProp';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsExchange');
  if num>0 then
    execute immediate 'drop sequence S_PartsExchange';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsExchangeGroup');
  if num>0 then
    execute immediate 'drop sequence S_PartsExchangeGroup';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsExchangeHistory');
  if num>0 then
    execute immediate 'drop sequence S_PartsExchangeHistory';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsPackingPropAppDetail');
  if num>0 then
    execute immediate 'drop sequence S_PartsPackingPropAppDetail';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsPackingPropertyApp');
  if num>0 then
    execute immediate 'drop sequence S_PartsPackingPropertyApp';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsPriceIncreaseRate');
  if num>0 then
    execute immediate 'drop sequence S_PartsPriceIncreaseRate';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsPriorityBill');
  if num>0 then
    execute immediate 'drop sequence S_PartsPriorityBill';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsReplacement');
  if num>0 then
    execute immediate 'drop sequence S_PartsReplacement';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsReplacementHistory');
  if num>0 then
    execute immediate 'drop sequence S_PartsReplacementHistory';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_ProductStandard');
  if num>0 then
    execute immediate 'drop sequence S_ProductStandard';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_SEP_SparePartInfo');
  if num>0 then
    execute immediate 'drop sequence S_SEP_SparePartInfo';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_SmartWarehousePartRelation');
  if num>0 then
    execute immediate 'drop sequence S_SmartWarehousePartRelation';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_SparePart');
  if num>0 then
    execute immediate 'drop sequence S_SparePart';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_SparePartHistory');
  if num>0 then
    execute immediate 'drop sequence S_SparePartHistory';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_WarehousePartRelation');
  if num>0 then
    execute immediate 'drop sequence S_WarehousePartRelation';
  end if;
end;
/

create sequence S_ABCSetting
/

create sequence S_ABCStrategy
/

create sequence S_ABCTypeSafeDays
/

create sequence S_AssemblyPartRequisitionLink
/

create sequence S_CalculatIntelligentPlan
/

create sequence S_CombinedPart
/

create sequence S_GoldenTaxClassifyMsg
/

create sequence S_IntelligentOrderMonthlyBase
/

create sequence S_IntelligentOrderWeeklyBase
/

create sequence S_PartDeleaveHistory
/

create sequence S_PartDeleaveInformation
/

create sequence S_PartsBranch
/

create sequence S_PartsBranchHistory
/

create sequence S_PartsBranchPackingProp
/

create sequence S_PartsExchange
/

create sequence S_PartsExchangeGroup
/

create sequence S_PartsExchangeHistory
/

create sequence S_PartsPackingPropAppDetail
/

create sequence S_PartsPackingPropertyApp
/

create sequence S_PartsPriceIncreaseRate
/

create sequence S_PartsPriorityBill
/

create sequence S_PartsReplacement
/

create sequence S_PartsReplacementHistory
/

create sequence S_ProductStandard
/

create sequence S_SEP_SparePartInfo
/

create sequence S_SmartWarehousePartRelation
/

create sequence S_SparePart
/

create sequence S_SparePartHistory
/

create sequence S_WarehousePartRelation
/

/*==============================================================*/
/* Table: ABCSetting                                            */
/*==============================================================*/
create table ABCSetting  (
   Id                   NUMBER(9)                       not null,
   Type                 NUMBER(9),
   IsAutoAdjust         NUMBER(1),
   SaleFrequencyFrom    NUMBER(15,6),
   SaleFrequencyTo      NUMBER(15,6),
   SaleFeeFrom          NUMBER(15,6),
   SaleFeeTo            NUMBER(15,6),
   NoSaleMonth          NUMBER(9),
   SafeStockCoefficient NUMBER(15,6),
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   IsReserve            NUMBER(1),
   constraint PK_ABCSETTING primary key (Id)
)
/

/*==============================================================*/
/* Table: ABCStrategy                                           */
/*==============================================================*/
create table ABCStrategy  (
   Id                   NUMBER(9)                       not null,
   BranchId             NUMBER(9)                       not null,
   BranchName           VARCHAR2(100)                   not null,
   Category             NUMBER(9)                       not null,
   ReferenceBaseType    NUMBER(9)                       not null,
   Percentage           NUMBER(15,6)                    not null,
   SamplingDuration     NUMBER(9)                       not null,
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   RowVersion           TIMESTAMP,
   YeardOutBoundStartTime NUMBER(9),
   YeardOutBoundEndTime NUMBER(9),
   constraint PK_ABCSTRATEGY primary key (Id)
)
/

/*==============================================================*/
/* Table: ABCTypeSafeDays                                       */
/*==============================================================*/
create table ABCTypeSafeDays  (
   Id                   NUMBER(9)                       not null,
   ProductType          NUMBER(9)                       not null,
   Days                 NUMBER(9),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   constraint PK_ABCTYPESAFEDAYS primary key (Id)
)
/

/*==============================================================*/
/* Table: AssemblyPartRequisitionLink                           */
/*==============================================================*/
create table AssemblyPartRequisitionLink  (
   Id                   NUMBER(9)                       not null,
   KeyCode              NUMBER(9),
   PartId               NUMBER(9)                       not null,
   PartCode             VARCHAR2(50)                    not null,
   PartName             VARCHAR2(100)                   not null,
   Qty                  NUMBER(9),
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   constraint PK_ASSEMBLYPARTREQUISITIONLINK primary key (Id)
)
/

/*==============================================================*/
/* Table: CalculatIntelligentPlan                               */
/*==============================================================*/
create table CalculatIntelligentPlan  (
   Id                   NUMBER(9)                       not null,
   IsCalculate          NUMBER(1),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   constraint PK_CALCULATINTELLIGENTPLAN primary key (Id)
)
/

/*==============================================================*/
/* Table: CombinedPart                                          */
/*==============================================================*/
create table CombinedPart  (
   Id                   NUMBER(9)                       not null,
   ParentId             NUMBER(9)                       not null,
   PartId               NUMBER(9)                       not null,
   Quantity             NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   constraint PK_COMBINEDPART primary key (Id)
)
/

/*==============================================================*/
/* Table: GoldenTaxClassifyMsg                                  */
/*==============================================================*/
create table GoldenTaxClassifyMsg  (
   Id                   NUMBER(9)                       not null,
   GoldenTaxClassifyCode VARCHAR2(50),
   GoldenTaxClassifyName VARCHAR2(50),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   constraint PK_GOLDENTAXCLASSIFYMSG primary key (Id)
)
/

/*==============================================================*/
/* Table: IntelligentOrderMonthlyBase                           */
/*==============================================================*/
create table IntelligentOrderMonthlyBase  (
   Id                   NUMBER(9)                       not null,
   BRANCHID             NUMBER(9)                       not null,
   BRANCHCODE           VARCHAR2(50)                    not null,
   BRANCHNAME           VARCHAR2(100)                   not null,
   ORDERTYPEID          NUMBER(9),
   ORDERTYPENAME        VARCHAR2(100),
   IFDIRECTPROVISION    NUMBER(1),
   SALESCATEGORYID      NUMBER(9)                       not null,
   SALESCATEGORYCODE    VARCHAR2(50)                    not null,
   SALESCATEGORYNAME    VARCHAR2(100)                   not null,
   PARTID               NUMBER(9)                       not null,
   PARTCODE             VARCHAR2(50)                    not null,
   PARTNAME             VARCHAR2(100)                   not null,
   SUPPLIERID           NUMBER(9),
   SUPPLIERCODE         VARCHAR2(50),
   SUPPLIERNAME         VARCHAR2(100),
   PARTABC              NUMBER(9),
   PRODUCTLIFECYCLE     NUMBER(9),
   PARTSBRANCHCREATETIME DATE                            not null,
   PLANNEDPRICE         NUMBER(19,4),
   PLANNEDPRICETYPE     NUMBER(9),
   STOCKQUANTITY        NUMBER(9)                       not null,
   STOCKAMOUNT          NUMBER(19,4)                    not null,
   BUSINESSTYPE         NUMBER(9),
   TOTALQUANTITY        NUMBER(9),
   TOTALMONTHLYFREQUENCY NUMBER(9)                       not null,
   OCCURMONTH           VARCHAR2(50)                    not null,
   THEDATE              DATE,
   constraint PK_INTELLIGENTORDERMONTHLYBASE primary key (Id)
)
/

/*==============================================================*/
/* Table: IntelligentOrderWeeklyBase                            */
/*==============================================================*/
create table IntelligentOrderWeeklyBase  (
   Id                   NUMBER(9)                       not null,
   BRANCHID             NUMBER(9)                       not null,
   BRANCHCODE           VARCHAR2(50)                    not null,
   BRANCHNAME           VARCHAR2(100)                   not null,
   ORDERTYPEID          NUMBER(9),
   ORDERTYPENAME        VARCHAR2(100),
   IFDIRECTPROVISION    NUMBER(1),
   SALESCATEGORYID      NUMBER(9)                       not null,
   SALESCATEGORYCODE    VARCHAR2(50)                    not null,
   SALESCATEGORYNAME    VARCHAR2(100)                   not null,
   PARTID               NUMBER(9)                       not null,
   PARTCODE             VARCHAR2(50)                    not null,
   PARTNAME             VARCHAR2(100),
   SUPPLIERID           NUMBER(9),
   SUPPLIERCODE         VARCHAR2(50),
   SUPPLIERNAME         VARCHAR2(100),
   PARTABC              NUMBER(9),
   PRODUCTLIFECYCLE     NUMBER(9),
   PARTSBRANCHCREATETIME DATE                            not null,
   PLANNEDPRICE         NUMBER(19,4),
   PLANNEDPRICETYPE     NUMBER(9),
   STOCKQUANTITY        NUMBER(9)                       not null,
   STOCKAMOUNT          NUMBER(19,4)                    not null,
   BUSINESSTYPE         NUMBER(9),
   TOTALQUANTITY        NUMBER(9),
   YEARD                NUMBER(9)                       not null,
   TOTALWEEKLYFREQUENCY NUMBER(9),
   WEEKLYD              NUMBER(9)                       not null,
   THEDATE              DATE,
   constraint PK_INTELLIGENTORDERWEEKLYBASE primary key (Id)
)
/

/*==============================================================*/
/* Table: PartDeleaveHistory                                    */
/*==============================================================*/
create table PartDeleaveHistory  (
   Id                   NUMBER(9)                       not null,
   PartDeleaveInformationId NUMBER(9)                       not null,
   OldPartId            NUMBER(9)                       not null,
   OldPartCode          VARCHAR2(50)                    not null,
   DeleavePartCode      VARCHAR2(50)                    not null,
   DeleavePartName      VARCHAR2(100)                   not null,
   BranchId             NUMBER(9)                       not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   DeleaveAmount        NUMBER(9)                       not null,
   Status               NUMBER(9),
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9)                       not null,
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   constraint PK_PARTDELEAVEHISTORY primary key (Id)
)
/

/*==============================================================*/
/* Table: PartDeleaveInformation                                */
/*==============================================================*/
create table PartDeleaveInformation  (
   Id                   NUMBER(9)                       not null,
   OldPartId            NUMBER(9)                       not null,
   OldPartCode          VARCHAR2(50)                    not null,
   DeleavePartCode      VARCHAR2(50)                    not null,
   DeleavePartName      VARCHAR2(100)                   not null,
   BranchId             NUMBER(9)                       not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   DeleaveAmount        NUMBER(9)                       not null,
   Status               NUMBER(9),
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9)                       not null,
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   constraint PK_PARTDELEAVEINFORMATION primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsBranch                                           */
/*==============================================================*/
create table PartsBranch  (
   Id                   NUMBER(9)                       not null,
   PartId               NUMBER(9)                       not null,
   PartCode             VARCHAR2(50)                    not null,
   PartName             VARCHAR2(100)                   not null,
   ReferenceCode        VARCHAR2(50),
   StockMaximum         NUMBER(9),
   StockMinimum         NUMBER(9),
   IncreaseRateGroupId  NUMBER(9),
   BranchId             NUMBER(9)                       not null,
   BranchName           VARCHAR2(100)                   not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   PartsSalesCategoryName VARCHAR2(50)                    not null,
   ABCStrategyId        NUMBER(9),
   ProductLifeCycle     NUMBER(9),
   LossType             NUMBER(9),
   PartsAttribution     NUMBER(9),
   IsOrderable          NUMBER(1)                       not null,
   PurchaseCycle        NUMBER(9),
   IsService            NUMBER(1),
   IsSalable            NUMBER(1),
   PartsReturnPolicy    NUMBER(9)                       not null,
   IsDirectSupply       NUMBER(1)                       not null,
   WarrantySupplyStatus NUMBER(9),
   MinSaleQuantity      NUMBER(9),
   PartABC              NUMBER(9),
   PurchaseRoute        NUMBER(9),
   RepairMatMinUnit     NUMBER(1),
   PartsWarrantyCategoryId NUMBER(9),
   PartsWarrantyCategoryCode VARCHAR2(50),
   PartsWarrantyCategoryName VARCHAR2(100),
   PartsWarhouseManageGranularity NUMBER(9),
   PartsMaterialManageCost NUMBER(19,4),
   PartsWarrantyLong    NUMBER(9),
   AutoApproveUpLimit   NUMBER(9),
   Status               NUMBER(9)                       not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   Remark               VARCHAR2(200),
   PlannedPriceCategory NUMBER(9),
   RowVersion           TIMESTAMP,
   ExchangeIdentification VARCHAR2(50),
   OMSparePartMark      NUMBER(9),
   MainPackingType      NUMBER(9),
   BreakTime            DATE,
   IsAccreditPack       NUMBER(1),
   IsAutoCaclSaleProperty NUMBER(1),
   constraint PK_PARTSBRANCH primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsBranchHistory                                    */
/*==============================================================*/
create table PartsBranchHistory  (
   Id                   NUMBER(9)                       not null,
   PartsBranchId        NUMBER(9)                       not null,
   PartId               NUMBER(9)                       not null,
   PartCode             VARCHAR2(50)                    not null,
   PartName             VARCHAR2(100)                   not null,
   ReferenceCode        VARCHAR2(50),
   StockMaximum         NUMBER(9),
   StockMinimum         NUMBER(9),
   IncreaseRateGroupId  NUMBER(9),
   BranchId             NUMBER(9)                       not null,
   BranchName           VARCHAR2(100)                   not null,
   LossType             NUMBER(9),
   PartsAttribution     NUMBER(9),
   PartsSalesCategoryId NUMBER(9),
   PartsSalesCategoryName VARCHAR2(50),
   ABCStrategyId        NUMBER(9),
   ProductLifeCycle     NUMBER(9),
   IsOrderable          NUMBER(1),
   PurchaseCycle        NUMBER(9),
   IsService            NUMBER(1),
   IsSalable            NUMBER(1),
   PartsReturnPolicy    NUMBER(9),
   IsDirectSupply       NUMBER(1),
   WarrantySupplyStatus NUMBER(9),
   MinSaleQuantity      NUMBER(9),
   PartABC              NUMBER(9),
   PurchaseRoute        NUMBER(9),
   PartsWarrantyCategoryId NUMBER(9),
   PartsWarrantyCategoryCode VARCHAR2(50),
   PartsWarrantyCategoryName VARCHAR2(100),
   PartsWarhouseManageGranularity NUMBER(9),
   PartsMaterialManageCost NUMBER(19,4),
   PartsWarrantyLong    NUMBER(9),
   AutoApproveUpLimit   NUMBER(9),
   Status               NUMBER(9)                       not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   Remark               VARCHAR2(200),
   RowVersion           TIMESTAMP,
   RepairMatMinUnit     NUMBER(1),
   constraint PK_PARTSBRANCHHISTORY primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsBranchPackingProp                                */
/*==============================================================*/
create table PartsBranchPackingProp  (
   Id                   NUMBER(9)                       not null,
   PartsBranchId        NUMBER(9),
   PackingCode          VARCHAR2(50),
   PackingType          NUMBER(9),
   MeasureUnit          VARCHAR2(20)                    not null,
   PackingCoefficient   NUMBER(9),
   PackingMaterial      VARCHAR2(50),
   Volume               NUMBER(15,6),
   Weight               NUMBER(15,6),
   Length               NUMBER(15,6),
   Width                NUMBER(15,6),
   Height               NUMBER(15,6),
   SparePartId          NUMBER(9),
   IsPrintPartStandard  NUMBER(9),
   IsBoxStandardPrint   NUMBER(9),
   PackingMaterialName  VARCHAR2(100),
   constraint PK_PARTSBRANCHPACKINGPROP primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsExchange                                         */
/*==============================================================*/
create table PartsExchange  (
   Id                   NUMBER(9)                       not null,
   ExchangeCode         VARCHAR2(50)                    not null,
   ExchangeName         VARCHAR2(100),
   PartId               NUMBER(9)                       not null,
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   RowVersion           TIMESTAMP,
   IsVisible            NUMBER(9),
   constraint PK_PARTSEXCHANGE primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsExchangeGroup                                    */
/*==============================================================*/
create table PartsExchangeGroup  (
   Id                   NUMBER(9)                       not null,
   ExGroupCode          VARCHAR2(100)                   not null,
   ExchangeCode         VARCHAR2(100),
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   constraint PK_PARTSEXCHANGEGROUP primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsExchangeHistory                                  */
/*==============================================================*/
create table PartsExchangeHistory  (
   Id                   NUMBER(9)                       not null,
   PartsExchangeId      NUMBER(9)                       not null,
   ExchangeCode         VARCHAR2(50)                    not null,
   ExchangeName         VARCHAR2(100),
   PartId               NUMBER(9)                       not null,
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_PARTSEXCHANGEHISTORY primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsPackingPropAppDetail                             */
/*==============================================================*/
create table PartsPackingPropAppDetail  (
   Id                   NUMBER(9)                       not null,
   PartsPackingPropAppId NUMBER(9),
   PackingCode          VARCHAR2(50),
   PackingType          NUMBER(9),
   MeasureUnit          VARCHAR2(20)                    not null,
   PackingCoefficient   NUMBER(9),
   PackingMaterial      VARCHAR2(50),
   Volume               NUMBER(15,6),
   Weight               NUMBER(15,6),
   Length               NUMBER(15,6),
   Width                NUMBER(15,6),
   Height               NUMBER(15,6),
   SparePartId          NUMBER(9),
   IsPrintPartStandard  NUMBER(9),
   IsBoxStandardPrint   NUMBER(9),
   PackingMaterialName  VARCHAR2(100),
   constraint PK_PARTSPACKINGPROPAPPDETAIL primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsPackingPropertyApp                               */
/*==============================================================*/
create table PartsPackingPropertyApp  (
   Id                   NUMBER(9)                       not null,
   PartsSalesCategoryId NUMBER(9),
   SparePartId          NUMBER(9)                       not null,
   SpareCode            VARCHAR2(50)                    not null,
   SihCode              VARCHAR2(50),
   SpareName            VARCHAR2(100),
   Status               NUMBER(9)                       not null,
   ApprovalComment      VARCHAR2(200),
   MainPackingType      NUMBER(9),
   MInSalesAmount       NUMBER(9),
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   ApproverId           NUMBER(9),
   ApproverName         VARCHAR2(100),
   ApproveTime          DATE,
   Code                 VARCHAR2(50)                    not null,
   OldMInPackingAmount  NUMBER(9),
   constraint PK_PARTSPACKINGPROPERTYAPP primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsPriceIncreaseRate                                */
/*==============================================================*/
create table PartsPriceIncreaseRate  (
   Id                   NUMBER(9)                       not null,
   BrandId              NUMBER(9),
   Brand                VARCHAR2(100),
   CategoryCode         VARCHAR2(100),
   CategoryName         VARCHAR2(100),
   SaleIncreaseRate     NUMBER(15,6),
   retailIncreaseRate   NUMBER(15,6),
   MaxSalesPriceFloating NUMBER(15,6),
   MinSalesPriceFloating NUMBER(15,6),
   MaxRetailOrderPriceFloating NUMBER(15,6),
   MinRetailOrderPriceFloating NUMBER(15,6),
   Status               NUMBER(9),
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   constraint PK_PARTSPRICEINCREASERATE primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsPriorityBill                                     */
/*==============================================================*/
create table PartsPriorityBill  (
   Id                   NUMBER(9)                       not null,
   PartId               NUMBER(9),
   PartCode             VARCHAR2(50),
   PartName             VARCHAR2(100),
   Priority             NUMBER(9),
   PriorityCreateTime   DATE,
   ActualUseableQty     NUMBER(9),
   StockQty             NUMBER(9),
   TotalDurativeDayNum  NUMBER(9),
   CreateTime           DATE,
   ModifyTime           DATE,
   constraint PK_PARTSPRIORITYBILL primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsReplacement                                      */
/*==============================================================*/
create table PartsReplacement  (
   Id                   NUMBER(9)                       not null,
   OldPartId            NUMBER(9)                       not null,
   OldPartCode          VARCHAR2(50)                    not null,
   OldPartName          VARCHAR2(100)                   not null,
   NewPartId            NUMBER(9)                       not null,
   NewPartCode          VARCHAR2(50)                    not null,
   NewPartName          VARCHAR2(100)                   not null,
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   RowVersion           TIMESTAMP,
   IsSPM                NUMBER(1),
   OldMInAmount         NUMBER(9),
   RepMInAmount         NUMBER(9),
   IsInterFace          NUMBER(1),
   IsThisTime           NUMBER(1),
   constraint PK_PARTSREPLACEMENT primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsReplacementHistory                               */
/*==============================================================*/
create table PartsReplacementHistory  (
   Id                   NUMBER(9)                       not null,
   PartsReplacementId   NUMBER(9)                       not null,
   OldPartId            NUMBER(9)                       not null,
   OldPartCode          VARCHAR2(50)                    not null,
   OldPartName          VARCHAR2(100)                   not null,
   NewPartId            NUMBER(9)                       not null,
   NewPartCode          VARCHAR2(50)                    not null,
   NewPartName          VARCHAR2(100)                   not null,
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   RowVersion           TIMESTAMP,
   IsSPM                NUMBER(1),
   OldMInAmount         NUMBER(9),
   RepMInAmount         NUMBER(9),
   constraint PK_PARTSREPLACEMENTHISTORY primary key (Id)
)
/

/*==============================================================*/
/* Table: ProductStandard                                       */
/*==============================================================*/
create table ProductStandard  (
   Id                   NUMBER(9)                       not null,
   StandardCode         VARCHAR2(100)                   not null,
   StandardName         VARCHAR2(100)                   not null,
   Remark               VARCHAR2(200),
   Status               NUMBER(9)                       not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   constraint PK_PRODUCTSTANDARD primary key (Id)
)
/

/*==============================================================*/
/* Table: SEP_SparePartInfo                                     */
/*==============================================================*/
create table SEP_SparePartInfo  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50),
   Name                 VARCHAR2(100),
   ReferenceCode        VARCHAR2(50),
   PartsSalesCategoryName VARCHAR2(100),
   Specification        VARCHAR2(100),
   MeasureUnit          VARCHAR2(20),
   EnglishName          VARCHAR2(100),
   Feature              VARCHAR2(200),
   LastSubstitute       VARCHAR2(50),
   NextSubstitute       VARCHAR2(50),
   ProcessStatus        NUMBER(9),
   LastProcessTime      DATE,
   ErrorMessage         VARCHAR2(200),
   SepId                NUMBER(9),
   WarrantyRange        VARCHAR2(200),
   CategoryCode         VARCHAR2(100),
   OverseasPartsFigure  VARCHAR2(18),
   CategoryName         VARCHAR2(100),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   MInPackingAmount     NUMBER(9),
   ExpandLanguageNAME1  VARCHAR2(100),
   ExpandLanguageCode1  VARCHAR2(50),
   ExpandLanguageNAME2  VARCHAR2(100),
   ExpandLanguageCode2  VARCHAR2(50),
   ExpandLanguageNAME3  VARCHAR2(100),
   ExpandLanguageCode3  VARCHAR2(50),
   ExpandLanguageNAME4  VARCHAR2(100),
   ExpandLanguageCode4  VARCHAR2(50),
   ExpandLanguageNAME5  VARCHAR2(100),
   ExpandLanguageCode5  VARCHAR2(50),
   ExpandLanguageNAME6  VARCHAR2(100),
   ExpandLanguageCode6  VARCHAR2(50),
   ExpandLanguageNAME7  VARCHAR2(100),
   ExpandLanguageCode7  VARCHAR2(50),
   ExpandLanguageNAME8  VARCHAR2(100),
   ExpandLanguageCode8  VARCHAR2(50),
   ExpandLanguageNAME9  VARCHAR2(100),
   ExpandLanguageCode9  VARCHAR2(50),
   ExpandLanguageNAME10 VARCHAR2(100),
   ExpandLanguageCode10 VARCHAR2(50),
   ExpandLanguageNAME11 VARCHAR2(100),
   ExpandLanguageCode11 VARCHAR2(50),
   ExpandLanguageNAME12 VARCHAR2(100),
   ExpandLanguageCode13 VARCHAR2(50),
   ExpandLanguageNAME13 VARCHAR2(100),
   ExpandLanguageCode12 VARCHAR2(50),
   RowVersion           TIMESTAMP,
   constraint PK_SEP_SPAREPARTINFO primary key (Id)
)
/

/*==============================================================*/
/* Table: SmartWarehousePartRelation                            */
/*==============================================================*/
create table SmartWarehousePartRelation  (
   Id                   NUMBER(9)                       not null,
   Status               NUMBER(9)                       not null,
   WarehouseId          NUMBER(9)                       not null,
   WarehouseCode        VARCHAR2(50),
   WarehouseName        VARCHAR2(100),
   PartId               NUMBER(9)                       not null,
   PartCode             VARCHAR2(50),
   PartName             VARCHAR2(100),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   RowVersion           TIMESTAMP,
   constraint PK_SMARTWAREHOUSEPARTRELATION primary key (Id)
)
/

/*==============================================================*/
/* Table: SparePart                                             */
/*==============================================================*/
create table SparePart  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   Name                 VARCHAR2(100)                   not null,
   LastSubstitute       VARCHAR2(50),
   NextSubstitute       VARCHAR2(50),
   ShelfLife            NUMBER(9),
   EnglishName          VARCHAR2(100),
   PinyinCode           VARCHAR2(50),
   ReferenceCode        VARCHAR2(50),
   ReferenceName        VARCHAR2(100),
   CADCode              VARCHAR2(50),
   CADName              VARCHAR2(100),
   PartType             NUMBER(9)                       not null,
   Specification        VARCHAR2(100),
   Feature              VARCHAR2(200),
   Status               NUMBER(9)                       not null,
   Length               NUMBER(15,6),
   Width                NUMBER(15,6),
   Height               NUMBER(15,6),
   Volume               NUMBER(15,6),
   Weight               NUMBER(15,6),
   Material             VARCHAR2(50),
   PackingAmount        NUMBER(9),
   MInPackingAmount     NUMBER(9),
   PackingSpecification VARCHAR2(200),
   PartsOutPackingCode  VARCHAR2(50),
   PartsInPackingCode   VARCHAR2(50),
   MeasureUnit          VARCHAR2(20)                    not null,
   IsNotWarrantyTransfer NUMBER(1)                      default 0,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   GroupABCCategory     NUMBER(9),
   IMSCompressionNumber VARCHAR2(50),
   IMSManufacturerNumber VARCHAR2(50),
   ProductBrand         VARCHAR2(50),
   RowVersion           TIMESTAMP,
   SubstandardName      VARCHAR2(500),
   TotalNumber          VARCHAR2(100),
   Factury              VARCHAR2(100),
   IsOriginal           NUMBER(1),
   CategoryCode         VARCHAR2(100),
   OverseasPartsFigure  VARCHAR2(18),
   CategoryName         VARCHAR2(100),
   ExpandLanguageNAME1  VARCHAR2(100),
   ExpandLanguageCode1  VARCHAR2(50),
   ExpandLanguageCode13 VARCHAR2(50),
   ExpandLanguageNAME13 VARCHAR2(100),
   ExpandLanguageCode12 VARCHAR2(50),
   ExpandLanguageNAME12 VARCHAR2(100),
   ExpandLanguageCode11 VARCHAR2(50),
   ExpandLanguageNAME11 VARCHAR2(100),
   ExpandLanguageCode10 VARCHAR2(50),
   ExpandLanguageNAME10 VARCHAR2(100),
   ExpandLanguageCode9  VARCHAR2(50),
   ExpandLanguageNAME9  VARCHAR2(100),
   ExpandLanguageCode8  VARCHAR2(50),
   ExpandLanguageNAME8  VARCHAR2(100),
   ExpandLanguageCode7  VARCHAR2(50),
   ExpandLanguageNAME7  VARCHAR2(100),
   ExpandLanguageCode6  VARCHAR2(50),
   ExpandLanguageNAME6  VARCHAR2(100),
   ExpandLanguageCode5  VARCHAR2(50),
   ExpandLanguageNAME5  VARCHAR2(100),
   ExpandLanguageCode4  VARCHAR2(50),
   ExpandLanguageNAME4  VARCHAR2(100),
   ExpandLanguageCode3  VARCHAR2(50),
   ExpandLanguageNAME3  VARCHAR2(100),
   ExpandLanguageCode2  VARCHAR2(50),
   ExpandLanguageNAME2  VARCHAR2(100),
   ExchangeIdentification VARCHAR2(50),
   GoldenTaxClassifyId  NUMBER(9),
   GoldenTaxClassifyCode VARCHAR2(50),
   GoldenTaxClassifyName VARCHAR2(50),
   StandardCode         VARCHAR2(100),
   StandardName         VARCHAR2(100),
   OMSparePartMark      NUMBER(9),
   DeclareElement       VARCHAR2(500),
   Path                 VARCHAR2(2000),
   AssemblyKeyCode      NUMBER(9),
   IsSupplierPutIn      NUMBER(1),
   TraceProperty        NUMBER(9),
   SafeDays             NUMBER(9),
   WarehousDays         NUMBER(9),
   TemDays              NUMBER(9),
   CollectionAttribute  NUMBER(9),
   Label                NUMBER(9),
   MinCarAmount         NUMBER(9),
   IsBottom             NUMBER(1),
   constraint PK_SPAREPART primary key (Id)
)
/

/*==============================================================*/
/* Table: SparePartHistory                                      */
/*==============================================================*/
create table SparePartHistory  (
   Id                   NUMBER(9)                       not null,
   SparePartId          NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   Name                 VARCHAR2(100)                   not null,
   LastSubstitute       VARCHAR2(50),
   NextSubstitute       VARCHAR2(50),
   ShelfLife            NUMBER(9),
   EnglishName          VARCHAR2(100),
   PinyinCode           VARCHAR2(50),
   ReferenceCode        VARCHAR2(50),
   ReferenceName        VARCHAR2(100),
   CADCode              VARCHAR2(50),
   CADName              VARCHAR2(100),
   PartType             NUMBER(9)                       not null,
   Specification        VARCHAR2(100),
   Feature              VARCHAR2(200),
   Status               NUMBER(9)                       not null,
   Length               NUMBER(15,6),
   Width                NUMBER(15,6),
   Height               NUMBER(15,6),
   Volume               NUMBER(15,6),
   Weight               NUMBER(15,6),
   Material             VARCHAR2(50),
   MInPackingAmount     NUMBER(9),
   PackingAmount        NUMBER(9),
   PackingSpecification VARCHAR2(200),
   PartsOutPackingCode  VARCHAR2(50),
   PartsInPackingCode   VARCHAR2(50),
   MeasureUnit          VARCHAR2(20)                    not null,
   IsNotWarrantyTransfer NUMBER(1)                      default 0,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   GroupABCCategory     NUMBER(9),
   RowVersion           TIMESTAMP,
   IMSCompressionNumber VARCHAR2(50),
   IMSManufacturerNumber VARCHAR2(50),
   SubstandardName      VARCHAR2(500),
   TotalNumber          VARCHAR2(100),
   Factury              VARCHAR2(100),
   IsOriginal           NUMBER(1),
   CategoryCode         VARCHAR2(100),
   OverseasPartsFigure  VARCHAR2(18),
   PreOverseasPartsFigure VARCHAR2(18),
   CategoryName         VARCHAR2(100),
   PreCategoryName      VARCHAR2(100),
   ExchangeIdentification VARCHAR2(50),
   GoldenTaxClassifyId  NUMBER(9),
   GoldenTaxClassifyCode VARCHAR2(50),
   GoldenTaxClassifyName VARCHAR2(50),
   ProductBrand         VARCHAR2(50),
   StandardCode         VARCHAR2(100),
   StandardName         VARCHAR2(100),
   DeclareElement       VARCHAR2(100),
   Path                 VARCHAR2(2000),
   AssemblyKeyCode      NUMBER(9),
   IsSupplierPutIn      NUMBER(1),
   constraint PK_SPAREPARTHISTORY primary key (Id)
)
/

/*==============================================================*/
/* Table: WarehousePartRelation                                 */
/*==============================================================*/
create table WarehousePartRelation  (
   Id                   NUMBER(9)                       not null,
   Status               NUMBER(9)                       not null,
   WarehouseId          NUMBER(9)                       not null,
   WarehouseCode        VARCHAR2(50),
   WarehouseName        VARCHAR2(100),
   PartId               NUMBER(9)                       not null,
   PartCode             VARCHAR2(50),
   PartName             VARCHAR2(100),
   IsMarketable         NUMBER(1),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   FrozenTime           DATE,
   FrozenName           VARCHAR2(100),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   RecoveryTime         DATE,
   RecoveryName         VARCHAR2(100),
   constraint PK_WAREHOUSEPARTRELATION primary key (Id)
)
/

