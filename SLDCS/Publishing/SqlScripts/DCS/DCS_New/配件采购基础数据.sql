/*==============================================================*/
/* DBMS name:      ORACLE Version 10gR2                         */
/* Created on:     2023/10/12 9:55:08                           */
/*==============================================================*/


declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('BranchSupplierRelation');
  if num>0 then
    execute immediate 'drop table BranchSupplierRelation cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('FactoryPurchacePrice_Tmp');
  if num>0 then
    execute immediate 'drop table FactoryPurchacePrice_Tmp cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('IntelligentOrderMonthly');
  if num>0 then
    execute immediate 'drop table IntelligentOrderMonthly cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('IntelligentOrderMonthlyBase');
  if num>0 then
    execute immediate 'drop table IntelligentOrderMonthlyBase cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('IntelligentOrderWeekly');
  if num>0 then
    execute immediate 'drop table IntelligentOrderWeekly cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('IntelligentOrderWeeklyBase');
  if num>0 then
    execute immediate 'drop table IntelligentOrderWeeklyBase cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartABCDetail');
  if num>0 then
    execute immediate 'drop table PartABCDetail cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartSafeStock');
  if num>0 then
    execute immediate 'drop table PartSafeStock cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsPriceTimeLimit');
  if num>0 then
    execute immediate 'drop table PartsPriceTimeLimit cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsPurchaseHoliday');
  if num>0 then
    execute immediate 'drop table PartsPurchaseHoliday cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsPurchaseOrderType');
  if num>0 then
    execute immediate 'drop table PartsPurchaseOrderType cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsPurchasePricing');
  if num>0 then
    execute immediate 'drop table PartsPurchasePricing cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsPurchasePricingChange');
  if num>0 then
    execute immediate 'drop table PartsPurchasePricingChange cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsPurchasePricingDetail');
  if num>0 then
    execute immediate 'drop table PartsPurchasePricingDetail cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsServiceLevel');
  if num>0 then
    execute immediate 'drop table PartsServiceLevel cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsSupplier');
  if num>0 then
    execute immediate 'drop table PartsSupplier cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsSupplierRelation');
  if num>0 then
    execute immediate 'drop table PartsSupplierRelation cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsSupplierRelationHistory');
  if num>0 then
    execute immediate 'drop table PartsSupplierRelationHistory cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PlanPriceCategory');
  if num>0 then
    execute immediate 'drop table PlanPriceCategory cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('ProductLifeCycleCategory');
  if num>0 then
    execute immediate 'drop table ProductLifeCycleCategory cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PurchasePricingChangeHis');
  if num>0 then
    execute immediate 'drop table PurchasePricingChangeHis cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('ServiceLevelBySafeCf');
  if num>0 then
    execute immediate 'drop table ServiceLevelBySafeCf cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('SparePartpurchApplication');
  if num>0 then
    execute immediate 'drop table SparePartpurchApplication cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('SparePartpurchCycle');
  if num>0 then
    execute immediate 'drop table SparePartpurchCycle cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('SparePartpurchDetails');
  if num>0 then
    execute immediate 'drop table SparePartpurchDetails cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('SupplierMaintenance');
  if num>0 then
    execute immediate 'drop table SupplierMaintenance cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('TemporarySupplier');
  if num>0 then
    execute immediate 'drop table TemporarySupplier cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_BranchSupplierRelation');
  if num>0 then
    execute immediate 'drop sequence S_BranchSupplierRelation';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_FactoryPurchacePrice_Tmp');
  if num>0 then
    execute immediate 'drop sequence S_FactoryPurchacePrice_Tmp';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_IntelligentOrderMonthly');
  if num>0 then
    execute immediate 'drop sequence S_IntelligentOrderMonthly';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_IntelligentOrderMonthlyBase');
  if num>0 then
    execute immediate 'drop sequence S_IntelligentOrderMonthlyBase';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_IntelligentOrderWeekly');
  if num>0 then
    execute immediate 'drop sequence S_IntelligentOrderWeekly';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_IntelligentOrderWeeklyBase');
  if num>0 then
    execute immediate 'drop sequence S_IntelligentOrderWeeklyBase';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartABCDetail');
  if num>0 then
    execute immediate 'drop sequence S_PartABCDetail';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartSafeStock');
  if num>0 then
    execute immediate 'drop sequence S_PartSafeStock';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsPriceTimeLimit');
  if num>0 then
    execute immediate 'drop sequence S_PartsPriceTimeLimit';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsPurchaseHoliday');
  if num>0 then
    execute immediate 'drop sequence S_PartsPurchaseHoliday';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsPurchaseOrderType');
  if num>0 then
    execute immediate 'drop sequence S_PartsPurchaseOrderType';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsPurchasePricing');
  if num>0 then
    execute immediate 'drop sequence S_PartsPurchasePricing';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsPurchasePricingChange');
  if num>0 then
    execute immediate 'drop sequence S_PartsPurchasePricingChange';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsPurchasePricingDetail');
  if num>0 then
    execute immediate 'drop sequence S_PartsPurchasePricingDetail';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsServiceLevel');
  if num>0 then
    execute immediate 'drop sequence S_PartsServiceLevel';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsSupplierRelation');
  if num>0 then
    execute immediate 'drop sequence S_PartsSupplierRelation';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsSupplierRelationHistory');
  if num>0 then
    execute immediate 'drop sequence S_PartsSupplierRelationHistory';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PlanPriceCategory');
  if num>0 then
    execute immediate 'drop sequence S_PlanPriceCategory';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_ProductLifeCycleCategory');
  if num>0 then
    execute immediate 'drop sequence S_ProductLifeCycleCategory';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PurchasePricingChangeHis');
  if num>0 then
    execute immediate 'drop sequence S_PurchasePricingChangeHis';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_ServiceLevelBySafeCf');
  if num>0 then
    execute immediate 'drop sequence S_ServiceLevelBySafeCf';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_SparePartpurchApplication');
  if num>0 then
    execute immediate 'drop sequence S_SparePartpurchApplication';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_SparePartpurchCycle');
  if num>0 then
    execute immediate 'drop sequence S_SparePartpurchCycle';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_SparePartpurchDetails');
  if num>0 then
    execute immediate 'drop sequence S_SparePartpurchDetails';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_SupplierMaintenance');
  if num>0 then
    execute immediate 'drop sequence S_SupplierMaintenance';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_TemporarySupplier');
  if num>0 then
    execute immediate 'drop sequence S_TemporarySupplier';
  end if;
end;
/

create sequence S_BranchSupplierRelation
/

create sequence S_FactoryPurchacePrice_Tmp
/

create sequence S_IntelligentOrderMonthly
/

create sequence S_IntelligentOrderMonthlyBase
/

create sequence S_IntelligentOrderWeekly
/

create sequence S_IntelligentOrderWeeklyBase
/

create sequence S_PartABCDetail
/

create sequence S_PartSafeStock
/

create sequence S_PartsPriceTimeLimit
/

create sequence S_PartsPurchaseHoliday
/

create sequence S_PartsPurchaseOrderType
/

create sequence S_PartsPurchasePricing
/

create sequence S_PartsPurchasePricingChange
/

create sequence S_PartsPurchasePricingDetail
/

create sequence S_PartsServiceLevel
/

create sequence S_PartsSupplierRelation
/

create sequence S_PartsSupplierRelationHistory
/

create sequence S_PlanPriceCategory
/

create sequence S_ProductLifeCycleCategory
/

create sequence S_PurchasePricingChangeHis
/

create sequence S_ServiceLevelBySafeCf
/

create sequence S_SparePartpurchApplication
/

create sequence S_SparePartpurchCycle
/

create sequence S_SparePartpurchDetails
/

create sequence S_SupplierMaintenance
/

create sequence S_TemporarySupplier
/

/*==============================================================*/
/* Table: BranchSupplierRelation                                */
/*==============================================================*/
create table BranchSupplierRelation  (
   Id                   NUMBER(9)                       not null,
   BranchId             NUMBER(9)                       not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   SupplierId           NUMBER(9)                       not null,
   BusinessCode         VARCHAR2(50)                    not null,
   BusinessName         VARCHAR2(100)                   not null,
   PurchasingCycle      NUMBER(9),
   PartsManagementCostGradeId NUMBER(9),
   PartsClaimCoefficient NUMBER(15,6),
   ClaimPriceCategory   NUMBER(9),
   LaborUnitPrice       NUMBER(19,4),
   IfSPByLabor          NUMBER(1)                       not null,
   LaborCoefficient     NUMBER(15,6),
   OutServiceCarUnitPrice NUMBER(19,4),
   OutSubsidyPrice      NUMBER(19,4),
   IfHaveOutFee         NUMBER(1)                       not null,
   OldPartTransCoefficient NUMBER(15,6),
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   RowVersion           TIMESTAMP,
   ShippingCycle        NUMBER(9),
   ArrivalCycle         NUMBER(9),
   constraint PK_BRANCHSUPPLIERRELATION primary key (Id)
)
/

/*==============================================================*/
/* Table: FactoryPurchacePrice_Tmp                              */
/*==============================================================*/
create table FactoryPurchacePrice_Tmp  (
   Id                   NUMBER(9)                       not null,
   SupplierCode         VARCHAR2(50),
   SapSpareCode         VARCHAR2(50),
   ContractPrice        NUMBER(19,4),
   ValidFrom            DATE,
   ValidTo              DATE,
   UserCode             VARCHAR2(50),
   CreatetTime          DATE                           default Sysdate not null,
   IsGenerate           NUMBER(1),
   constraint PK_FACTORYPURCHACEPRICE_TMP primary key (Id)
)
/

/*==============================================================*/
/* Table: IntelligentOrderMonthly                               */
/*==============================================================*/
create table IntelligentOrderMonthly  (
   Id                   NUMBER(9)                       not null,
   BRANCHID             NUMBER(9)                       not null,
   BRANCHCODE           VARCHAR2(50)                    not null,
   BRANCHNAME           VARCHAR2(100)                   not null,
   ORDERTYPENAME        VARCHAR2(100),
   SALESCATEGORYID      NUMBER(9)                       not null,
   SALESCATEGORYCODE    VARCHAR2(50)                    not null,
   SALESCATEGORYNAME    VARCHAR2(100)                   not null,
   PARTID               NUMBER(9)                       not null,
   PARTCODE             VARCHAR2(50)                    not null,
   PARTNAME             VARCHAR2(100)                   not null,
   SUPPLIERID           NUMBER(9),
   SUPPLIERCODE         VARCHAR2(50),
   SUPPLIERNAME         VARCHAR2(100),
   PARTABC              NUMBER(9),
   PRODUCTLIFECYCLE     NUMBER(9),
   PLANNEDPRICETYPE     NUMBER(9),
   ORDERQUANTITY1       NUMBER(9),
   ORDERQUANTITY2       NUMBER(9),
   ORDERQUANTITY3       NUMBER(9),
   ORDERQUANTITY4       NUMBER(9),
   ORDERQUANTITY5       NUMBER(9),
   ORDERQUANTITY6       NUMBER(9),
   ORDERQUANTITY7       NUMBER(9),
   ORDERQUANTITY8       NUMBER(9),
   ORDERQUANTITY9       NUMBER(9),
   ORDERQUANTITY10      NUMBER(9),
   ORDERQUANTITY11      NUMBER(9),
   ORDERQUANTITY12      NUMBER(9),
   FREQUENCY1           NUMBER(9),
   FREQUENCY2           NUMBER(9),
   FREQUENCY3           NUMBER(9),
   FREQUENCY4           NUMBER(9),
   FREQUENCY5           NUMBER(9),
   FREQUENCY6           NUMBER(9),
   FREQUENCY7           NUMBER(9),
   FREQUENCY8           NUMBER(9),
   FREQUENCY9           NUMBER(9),
   FREQUENCY10          NUMBER(9),
   FREQUENCY11          NUMBER(9),
   FREQUENCY12          NUMBER(9),
   FREQUENCYFOR12M      NUMBER(9),
   ORDERQUANTITYFOR3M   NUMBER(9),
   ORDERQUANTITYFOR6M   NUMBER(9),
   ORDERQUANTITYFOR12M  NUMBER(9),
   MONTHAVGFOR3M        NUMBER(15,6),
   MONTHAVGFOR6M        NUMBER(15,6),
   MONTHAVGFOR12M       NUMBER(15,6),
   DAYAVGFOR1M          NUMBER(15,6),
   DAYAVGFOR3M          NUMBER(15,6),
   DAYAVGFOR6M          NUMBER(15,6),
   DAYAVGFOR12M         NUMBER(15,6),
   constraint PK_INTELLIGENTORDERMONTHLY primary key (Id)
)
/

/*==============================================================*/
/* Table: IntelligentOrderMonthlyBase                           */
/*==============================================================*/
create table IntelligentOrderMonthlyBase  (
   Id                   NUMBER(9)                       not null,
   BRANCHID             NUMBER(9)                       not null,
   BRANCHCODE           VARCHAR2(50)                    not null,
   BRANCHNAME           VARCHAR2(100)                   not null,
   ORDERTYPEID          NUMBER(9),
   ORDERTYPENAME        VARCHAR2(100),
   IFDIRECTPROVISION    NUMBER(1),
   SALESCATEGORYID      NUMBER(9)                       not null,
   SALESCATEGORYCODE    VARCHAR2(50)                    not null,
   SALESCATEGORYNAME    VARCHAR2(100)                   not null,
   PARTID               NUMBER(9)                       not null,
   PARTCODE             VARCHAR2(50)                    not null,
   PARTNAME             VARCHAR2(100)                   not null,
   SUPPLIERID           NUMBER(9),
   SUPPLIERCODE         VARCHAR2(50),
   SUPPLIERNAME         VARCHAR2(100),
   PARTABC              NUMBER(9),
   PRODUCTLIFECYCLE     NUMBER(9),
   PARTSBRANCHCREATETIME DATE                            not null,
   PLANNEDPRICE         NUMBER(19,4),
   PLANNEDPRICETYPE     NUMBER(9),
   STOCKQUANTITY        NUMBER(9)                       not null,
   STOCKAMOUNT          NUMBER(19,4)                    not null,
   BUSINESSTYPE         NUMBER(9),
   TOTALQUANTITY        NUMBER(9),
   TOTALMONTHLYFREQUENCY NUMBER(9)                       not null,
   OCCURMONTH           VARCHAR2(50)                    not null,
   THEDATE              DATE                            not null,
   constraint PK_INTELLIGENTORDERMONTHLYBASE primary key (Id)
)
/

/*==============================================================*/
/* Table: IntelligentOrderWeekly                                */
/*==============================================================*/
create table IntelligentOrderWeekly  (
   Id                   NUMBER(9)                       not null,
   BRANCHID             NUMBER(9)                       not null,
   BRANCHCODE           VARCHAR2(50)                    not null,
   BRANCHNAME           VARCHAR2(100)                   not null,
   ORDERTYPENAME        VARCHAR2(100),
   SALESCATEGORYID      NUMBER(9)                       not null,
   SALESCATEGORYCODE    VARCHAR2(50)                    not null,
   SALESCATEGORYNAME    VARCHAR2(100)                   not null,
   PARTID               NUMBER(9)                       not null,
   PARTCODE             VARCHAR2(50)                    not null,
   PARTNAME             VARCHAR2(100)                   not null,
   SUPPLIERID           NUMBER(9),
   SUPPLIERCODE         VARCHAR2(50),
   SUPPLIERNAME         VARCHAR2(100),
   PARTABC              NUMBER(9),
   PRODUCTLIFECYCLE     NUMBER(9),
   PLANNEDPRICETYPE     NUMBER(9),
   ORDERQUANTITY1       NUMBER(9),
   ORDERQUANTITY2       NUMBER(9),
   ORDERQUANTITY3       NUMBER(9),
   ORDERQUANTITY4       NUMBER(9),
   ORDERQUANTITY5       NUMBER(9),
   ORDERQUANTITY6       NUMBER(9),
   ORDERQUANTITY7       NUMBER(9),
   ORDERQUANTITY8       NUMBER(9),
   ORDERQUANTITY9       NUMBER(9),
   ORDERQUANTITY10      NUMBER(9),
   ORDERQUANTITY11      NUMBER(9),
   ORDERQUANTITY12      NUMBER(9),
   ORDERQUANTITY13      NUMBER(9),
   ORDERQUANTITY14      NUMBER(9),
   ORDERQUANTITY15      NUMBER(9),
   ORDERQUANTITY16      NUMBER(9),
   ORDERQUANTITY17      NUMBER(9),
   ORDERQUANTITY18      NUMBER(9),
   ORDERQUANTITY19      NUMBER(9),
   ORDERQUANTITY20      NUMBER(9),
   ORDERQUANTITY21      NUMBER(9),
   ORDERQUANTITY22      NUMBER(9),
   ORDERQUANTITY23      NUMBER(9),
   ORDERQUANTITY24      NUMBER(9),
   ORDERQUANTITY25      NUMBER(9),
   ORDERQUANTITY26      NUMBER(9),
   ORDERQUANTITY27      NUMBER(9),
   ORDERQUANTITY28      NUMBER(9),
   ORDERQUANTITY29      NUMBER(9),
   ORDERQUANTITY30      NUMBER(9),
   ORDERQUANTITY31      NUMBER(9),
   ORDERQUANTITY32      NUMBER(9),
   ORDERQUANTITY33      NUMBER(9),
   ORDERQUANTITY34      NUMBER(9),
   ORDERQUANTITY35      NUMBER(9),
   ORDERQUANTITY36      NUMBER(9),
   ORDERQUANTITY37      NUMBER(9),
   ORDERQUANTITY38      NUMBER(9),
   ORDERQUANTITY39      NUMBER(9),
   ORDERQUANTITY40      NUMBER(9),
   ORDERQUANTITY41      NUMBER(9),
   ORDERQUANTITY42      NUMBER(9),
   ORDERQUANTITY43      NUMBER(9),
   ORDERQUANTITY44      NUMBER(9),
   ORDERQUANTITY45      NUMBER(9),
   ORDERQUANTITY46      NUMBER(9),
   ORDERQUANTITY47      NUMBER(9),
   ORDERQUANTITY48      NUMBER(9),
   ORDERQUANTITY49      NUMBER(9),
   ORDERQUANTITY50      NUMBER(9),
   ORDERQUANTITY51      NUMBER(9),
   ORDERQUANTITY52      NUMBER(9),
   AVG                  NUMBER(15,6),
   DEVIATION            NUMBER(15,6),
   constraint PK_INTELLIGENTORDERWEEKLY primary key (Id)
)
/

/*==============================================================*/
/* Table: IntelligentOrderWeeklyBase                            */
/*==============================================================*/
create table IntelligentOrderWeeklyBase  (
   Id                   NUMBER(9)                       not null,
   BRANCHID             NUMBER(9)                       not null,
   BRANCHCODE           VARCHAR2(50)                    not null,
   BRANCHNAME           VARCHAR2(100)                   not null,
   ORDERTYPEID          NUMBER(9),
   ORDERTYPENAME        VARCHAR2(100),
   IFDIRECTPROVISION    NUMBER(1),
   SALESCATEGORYID      NUMBER(9)                       not null,
   SALESCATEGORYCODE    VARCHAR2(50)                    not null,
   SALESCATEGORYNAME    VARCHAR2(100)                   not null,
   PARTID               NUMBER(9)                       not null,
   PARTCODE             VARCHAR2(50)                    not null,
   PARTNAME             VARCHAR2(100),
   SUPPLIERID           NUMBER(9),
   SUPPLIERCODE         VARCHAR2(50),
   SUPPLIERNAME         VARCHAR2(100),
   PARTABC              NUMBER(9),
   PRODUCTLIFECYCLE     NUMBER(9),
   PARTSBRANCHCREATETIME DATE                            not null,
   PLANNEDPRICE         NUMBER(19,4),
   PLANNEDPRICETYPE     NUMBER(9),
   STOCKQUANTITY        NUMBER(9)                       not null,
   STOCKAMOUNT          NUMBER(19,4)                    not null,
   BUSINESSTYPE         NUMBER(9),
   TOTALQUANTITY        NUMBER(9),
   YEARD                NUMBER(9)                       not null,
   TOTALWEEKLYFREQUENCY NUMBER(9),
   WEEKLYD              NUMBER(9)                       not null,
   THEDATE              DATE                            not null,
   constraint PK_INTELLIGENTORDERWEEKLYBASE primary key (Id)
)
/

/*==============================================================*/
/* Table: PartABCDetail                                         */
/*==============================================================*/
create table PartABCDetail  (
   Id                   NUMBER(9)                       not null,
   ProductLifeCycleCategoryId NUMBER(9)                       not null,
   PartABC              NUMBER(9)                       not null,
   constraint PK_PARTABCDETAIL primary key (Id)
)
/

/*==============================================================*/
/* Table: PartSafeStock                                         */
/*==============================================================*/
create table PartSafeStock  (
   Id                   NUMBER(9)                       not null,
   BranchId             NUMBER(9)                       not null,
   BranchName           VARCHAR2(100)                   not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   PartsSalesCategoryName VARCHAR2(100)                   not null,
   PartId               NUMBER(9)                       not null,
   PartCode             VARCHAR2(50)                    not null,
   PartName             VARCHAR2(100)                   not null,
   PrimarySupplierId    NUMBER(9),
   PrimarySupplierCode  VARCHAR2(50),
   PrimarySupplierName  VARCHAR2(100),
   OrderQuantityFor12M  NUMBER(9),
   CreatDuration        NUMBER(9),
   PartABC              NUMBER(9),
   PruductLifeCycle     NUMBER(9),
   PlannedPriceType     NUMBER(9),
   ServiceLevel         NUMBER(15,6),
   SafeCoefficient      NUMBER(15,6),
   NearFTWStandardDiff  NUMBER(15,6),
   NearFTWAverage       NUMBER(15,6),
   SafeStock            NUMBER(9),
   constraint PK_PARTSAFESTOCK primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsPriceTimeLimit                                   */
/*==============================================================*/
create table PartsPriceTimeLimit  (
   Id                   NUMBER(9)                       not null,
   Type                 NUMBER(9)                       not null,
   TimeLimit            NUMBER(9),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   constraint PK_PARTSPRICETIMELIMIT primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsPurchaseHoliday                                  */
/*==============================================================*/
create table PartsPurchaseHoliday  (
   Id                   NUMBER(9)                       not null,
   Status               NUMBER(9)                       not null,
   ValidFrom            DATE                            not null,
   ValidTo              DATE                            not null,
   Days                 NUMBER(9)                       not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(50),
   AbandonTime          DATE,
   constraint PK_PARTSPURCHASEHOLIDAY primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsPurchaseOrderType                                */
/*==============================================================*/
create table PartsPurchaseOrderType  (
   Id                   NUMBER(9)                       not null,
   BranchId             NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   Name                 VARCHAR2(100)                   not null,
   Status               NUMBER(9)                       not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   BusinessCode         VARCHAR2(10),
   constraint PK_PARTSPURCHASEORDERTYPE primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsPurchasePricing                                  */
/*==============================================================*/
create table PartsPurchasePricing  (
   Id                   NUMBER(9)                       not null,
   BranchId             NUMBER(9)                       not null,
   PartId               NUMBER(9)                       not null,
   PartsSupplierId      NUMBER(9)                       not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   PartsSalesCategoryName VARCHAR2(50)                    not null,
   ValidFrom            DATE                            not null,
   ValidTo              DATE,
   PriceType            NUMBER(9),
   PurchasePrice        NUMBER(19,4)                    not null,
   Status               NUMBER(9)                       not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(50),
   AbandonTime          DATE,
   Remark               VARCHAR2(200),
   OverseasPartsFigure  VARCHAR2(25),
   LimitQty             NUMBER(9),
   UsedQty              NUMBER(9),
   DataSource           NUMBER(9),
   IsPrimary            NUMBER(1),
   constraint PK_PARTSPURCHASEPRICING primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsPurchasePricingChange                            */
/*==============================================================*/
create table PartsPurchasePricingChange  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   BranchId             NUMBER(9)                       not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   PartsSalesCategoryName VARCHAR2(50)                    not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   InitialApproverId    NUMBER(9),
   InitialApproverName  VARCHAR2(100),
   InitialApproveTime   DATE,
   InitialApproveComment VARCHAR2(200),
   CheckerId            NUMBER(9),
   CheckerName          VARCHAR2(100),
   CheckTime            DATE,
   CheckComment         VARCHAR2(200),
   ApproverId           NUMBER(9),
   ApproverName         VARCHAR2(100),
   ApproveTime          DATE,
   ApprovalComment      VARCHAR2(200),
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   Status               NUMBER(9)                       not null,
   Tax                  NUMBER(8,2),
   Remark               VARCHAR2(200),
   RowVersion           TIMESTAMP,
   Path                 VARCHAR2(2000),
   constraint PK_PARTSPURCHASEPRICINGCHANGE primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsPurchasePricingDetail                            */
/*==============================================================*/
create table PartsPurchasePricingDetail  (
   Id                   NUMBER(9)                       not null,
   ParentId             NUMBER(9)                       not null,
   PartId               NUMBER(9)                       not null,
   PartCode             VARCHAR2(50)                    not null,
   PartName             VARCHAR2(100)                   not null,
   SupplierId           NUMBER(9)                       not null,
   SupplierCode         VARCHAR2(50)                    not null,
   SupplierName         VARCHAR2(100),
   PriceType            NUMBER(9),
   Price                NUMBER(19,4)                    not null,
   ReferencePrice       NUMBER(19,4),
   ValidFrom            DATE                            not null,
   ValidTo              DATE                            not null,
   Remark               VARCHAR2(200),
   PriceFluctuationRatio NUMBER(15,6),
   Brand                VARCHAR2(100),
   IsPrimary            NUMBER(1),
   MinPurchasePrice     NUMBER(19,4),
   MinPriceSupplierId   NUMBER(9),
   MinPriceSupplier     VARCHAR2(100),
   IsPrimaryMinPrice    NUMBER(1),
   LimitQty             NUMBER(9),
   SupplierPartCode     VARCHAR2(50),
   IfPurchasable        NUMBER(1),
   DataSource           NUMBER(9),
   OldPriSupplierId     NUMBER(9),
   OldPriSupplierCode   VARCHAR2(50),
   OldPriSupplierName   VARCHAR2(100),
   OldPriPrice          NUMBER(19,4),
   OldPriPriceType      NUMBER(9),
   constraint PK_PARTSPURCHASEPRICINGDETAIL primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsServiceLevel                                     */
/*==============================================================*/
create table PartsServiceLevel  (
   Id                   NUMBER(9)                       not null,
   BranchId             NUMBER(9)                       not null,
   BranchName           VARCHAR2(100)                   not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   PartsSalesCategoryName VARCHAR2(50)                    not null,
   PartABC              NUMBER(9),
   ServiceLevel         NUMBER(15,6),
   PlannedPriceType     NUMBER(9),
   StartDuration        NUMBER(9),
   EndDuration          NUMBER(9),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_PARTSSERVICELEVEL primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsSupplier                                         */
/*==============================================================*/
create table PartsSupplier  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   Name                 VARCHAR2(100)                   not null,
   ShortName            VARCHAR2(20),
   SupplierType         NUMBER(9),
   IsCanClaim           NUMBER(1),
   Status               NUMBER(9),
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   RowVersion           TIMESTAMP,
   OverseasPartsFigure  VARCHAR2(25),
   MDMSupplierType      NUMBER(9),
   IsLabel              NUMBER(1),
   IsWarehouse          NUMBER(1),
   IsPlan               NUMBER(1),
   IsUnlimited          NUMBER(1),
   PaymentMethod        NUMBER(9),
   PaymentTerms         NUMBER(9),
   constraint PK_PARTSSUPPLIER primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsSupplierRelation                                 */
/*==============================================================*/
create table PartsSupplierRelation  (
   Id                   NUMBER(9)                       not null,
   BranchId             NUMBER(9)                       not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   PartsSalesCategoryName VARCHAR2(50)                    not null,
   DefaultOrderWarehouseId NUMBER(9)                       not null,
   DefaultOrderWarehouseName VARCHAR2(100),
   PartId               NUMBER(9)                       not null,
   SupplierId           NUMBER(9)                       not null,
   SupplierPartCode     VARCHAR2(50),
   SupplierPartName     VARCHAR2(100),
   IsPrimary            NUMBER(1),
   IsCanClaim           NUMBER(1),
   PurchasePercentage   NUMBER(15,6),
   EconomicalBatch      NUMBER(9),
   MinBatch             NUMBER(9),
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   RowVersion           TIMESTAMP,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   MonthlyArrivalPeriod NUMBER(9),
   ArrivalReplenishmentCycle NUMBER(9),
   EmergencyArrivalPeriod NUMBER(9),
   OrderCycle           NUMBER(9),
   OrderGoodsCycle      NUMBER(9),
   IsSync               NUMBER(1),
   constraint PK_PARTSSUPPLIERRELATION primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsSupplierRelationHistory                          */
/*==============================================================*/
create table PartsSupplierRelationHistory  (
   Id                   NUMBER(9)                       not null,
   PartsSupplierRelationId NUMBER(9)                       not null,
   BranchId             NUMBER(9)                       not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   PartsSalesCategoryName VARCHAR2(50)                    not null,
   PartId               NUMBER(9)                       not null,
   SupplierId           NUMBER(9)                       not null,
   SupplierPartCode     VARCHAR2(50),
   SupplierPartName     VARCHAR2(100),
   IsPrimary            NUMBER(1),
   PurchasePercentage   NUMBER(15,6),
   EconomicalBatch      NUMBER(9),
   MinBatch             NUMBER(9),
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   RowVersion           TIMESTAMP,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   MonthlyArrivalPeriod NUMBER(9),
   ArrivalReplenishmentCycle NUMBER(9),
   EmergencyArrivalPeriod NUMBER(9),
   constraint PK_PARTSSUPPLIERRELATIONHISTOR primary key (Id)
)
/

/*==============================================================*/
/* Table: PlanPriceCategory                                     */
/*==============================================================*/
create table PlanPriceCategory  (
   Id                   NUMBER(9)                       not null,
   BranchId             NUMBER(9)                       not null,
   BranchName           VARCHAR2(100)                   not null,
   PriceCategory        NUMBER(9)                       not null,
   PriceMax             NUMBER(15,6)                    not null,
   PriceMin             NUMBER(15,6)                    not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_PLANPRICECATEGORY primary key (Id)
)
/

/*==============================================================*/
/* Table: ProductLifeCycleCategory                              */
/*==============================================================*/
create table ProductLifeCycleCategory  (
   Id                   NUMBER(9)                       not null,
   BranchId             NUMBER(9)                       not null,
   BranchName           VARCHAR2(100)                   not null,
   CycleCategory        NUMBER(9),
   CreatorStartTime     NUMBER(9),
   CreatorEndTime       NUMBER(9),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_PRODUCTLIFECYCLECATEGORY primary key (Id)
)
/

/*==============================================================*/
/* Table: PurchasePricingChangeHis                              */
/*==============================================================*/
create table PurchasePricingChangeHis  (
   Id                   NUMBER(9)                       not null,
   PartsSalesCategoryId NUMBER(9),
   SparePartId          NUMBER(9),
   SparePartCode        VARCHAR2(50),
   SparePartName        VARCHAR2(100),
   OldPurchasePrice     NUMBER(19,4),
   PurchasePrice        NUMBER(19,4),
   ChangeRatio          NUMBER(15,6),
   CenterPrice          NUMBER(19,4),
   DistributionPrice    NUMBER(19,4),
   RetailPrice          NUMBER(19,4),
   MaintenanceTime      DATE,
   Status               NUMBER(9),
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   IsPrimary            NUMBER(1),
   ValidFrom            DATE,
   ValidTo              DATE,
   SupplierId           NUMBER(9),
   SupplierCode         VARCHAR2(50),
   SupplierName         VARCHAR2(100),
   PriceType            NUMBER(9),
   constraint PK_PURCHASEPRICINGCHANGEHIS primary key (Id)
)
/

/*==============================================================*/
/* Table: ServiceLevelBySafeCf                                  */
/*==============================================================*/
create table ServiceLevelBySafeCf  (
   Id                   NUMBER(9)                       not null,
   SafeCoefficient      NUMBER(15,6)                    not null,
   ServiceLevel         NUMBER(15,6)                    not null,
   constraint PK_SERVICELEVELBYSAFECF primary key (Id)
)
/

/*==============================================================*/
/* Table: SparePartpurchApplication                             */
/*==============================================================*/
create table SparePartpurchApplication  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(100)                   not null,
   Status               NUMBER(9)                       not null,
   ReceivingWarehouseId NUMBER(9)                       not null,
   ReceivingWarehouseCode VARCHAR2(50)                    not null,
   ReceivingWarehouseName VARCHAR2(50)                    not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   SubmitterId          NUMBER(9),
   SubmitterName        VARCHAR2(100),
   SubmitTime           DATE,
   ApproverId           NUMBER(9),
   ApproverName         VARCHAR2(100),
   ApproveTime          DATE,
   RejecterId           NUMBER(9),
   RejecterName         VARCHAR2(100),
   RejectTime           DATE,
   CheckerId            NUMBER(9),
   CheckerName          VARCHAR2(100),
   CheckTime            DATE,
   CheckMemo            VARCHAR2(200),
   RowVersion           TIMESTAMP,
   constraint PK_SPAREPARTPURCHAPPLICATION primary key (Id)
)
/

/*==============================================================*/
/* Table: SparePartpurchCycle                                   */
/*==============================================================*/
create table SparePartpurchCycle  (
   Id                   NUMBER(9)                       not null,
   Status               NUMBER(9)                       not null,
   PartId               NUMBER(9)                       not null,
   PartCode             VARCHAR2(50)                    not null,
   PartName             VARCHAR2(100)                   not null,
   SupplierId           NUMBER(9)                       not null,
   SupplierCode         VARCHAR2(50)                    not null,
   SupplierName         VARCHAR2(100)                   not null,
   IsPrimary            NUMBER(1),
   ReceivingWarehouseId NUMBER(9)                       not null,
   ReceivingWarehouseCode VARCHAR2(50)                    not null,
   ReceivingWarehouseName VARCHAR2(50)                    not null,
   DeliveryCycle        NUMBER(9),
   LogisticsCycle       NUMBER(9),
   OrderingCycle        NUMBER(9),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   RowVersion           TIMESTAMP,
   constraint PK_SPAREPARTPURCHCYCLE primary key (Id)
)
/

/*==============================================================*/
/* Table: SparePartpurchDetails                                 */
/*==============================================================*/
create table SparePartpurchDetails  (
   Id                   NUMBER(9)                       not null,
   SpareApplicationId   NUMBER(9)                       not null,
   PartId               NUMBER(9)                       not null,
   PartCode             VARCHAR2(50)                    not null,
   PartName             VARCHAR2(100)                   not null,
   SupplierId           NUMBER(9)                       not null,
   SupplierCode         VARCHAR2(50)                    not null,
   SupplierName         VARCHAR2(100)                   not null,
   IsPrimary            NUMBER(1),
   DeliveryCycle        NUMBER(9),
   LogisticsCycle       NUMBER(9),
   OrderingCycle        NUMBER(9),
   constraint PK_SPAREPARTPURCHDETAILS primary key (Id)
)
/

/*==============================================================*/
/* Table: SupplierMaintenance                                   */
/*==============================================================*/
create table SupplierMaintenance  (
   Id                   NUMBER(9)                       not null,
   Status               NUMBER(9)                       not null,
   Year                 NUMBER(9)                       not null,
   SupplierId           NUMBER(9)                       not null,
   SupplierCode         VARCHAR2(50)                    not null,
   SupplierName         VARCHAR2(100)                   not null,
   Type                 NUMBER(9)                       not null,
   Rate                 NUMBER(19,4)                    not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   RowVersion           TIMESTAMP,
   constraint PK_SUPPLIERMAINTENANCE primary key (Id)
)
/

/*==============================================================*/
/* Table: TemporarySupplier                                     */
/*==============================================================*/
create table TemporarySupplier  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   BranchId             NUMBER(9)                       not null,
   BranchName           VARCHAR2(50)                    not null,
   Name                 VARCHAR2(100)                   not null,
   ContactPerson        VARCHAR2(50),
   ContactPhone         VARCHAR2(50),
   ContactAddress       VARCHAR2(200),
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   RowVersion           TIMESTAMP,
   constraint PK_TEMPORARYSUPPLIER primary key (Id)
)
/

