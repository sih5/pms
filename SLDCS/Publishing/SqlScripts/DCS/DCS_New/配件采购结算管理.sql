/*==============================================================*/
/* DBMS name:      ORACLE Version 10gR2                         */
/* Created on:     2023/11/30 9:41:48                           */
/*==============================================================*/


declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsPurchaseRtnSettleBill');
  if num>0 then
    execute immediate 'drop table PartsPurchaseRtnSettleBill cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsPurchaseRtnSettleDetail');
  if num>0 then
    execute immediate 'drop table PartsPurchaseRtnSettleDetail cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsPurchaseRtnSettleRef');
  if num>0 then
    execute immediate 'drop table PartsPurchaseRtnSettleRef cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsPurchaseSettleBill');
  if num>0 then
    execute immediate 'drop table PartsPurchaseSettleBill cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsPurchaseSettleDetail');
  if num>0 then
    execute immediate 'drop table PartsPurchaseSettleDetail cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsPurchaseSettleRef');
  if num>0 then
    execute immediate 'drop table PartsPurchaseSettleRef cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PurchaseNoSettleTable');
  if num>0 then
    execute immediate 'drop table PurchaseNoSettleTable cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PurchaseRtnSettleInvoiceRel');
  if num>0 then
    execute immediate 'drop table PurchaseRtnSettleInvoiceRel cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PurchaseSettleInvoiceRel');
  if num>0 then
    execute immediate 'drop table PurchaseSettleInvoiceRel cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('RebateInvoiceInformation');
  if num>0 then
    execute immediate 'drop table RebateInvoiceInformation cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('RedCodeDetail');
  if num>0 then
    execute immediate 'drop table RedCodeDetail cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('SupplierAnnualRebateBill');
  if num>0 then
    execute immediate 'drop table SupplierAnnualRebateBill cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('SupplierAnnualRebateDetail');
  if num>0 then
    execute immediate 'drop table SupplierAnnualRebateDetail cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsPurchaseRtnSettleBill');
  if num>0 then
    execute immediate 'drop sequence S_PartsPurchaseRtnSettleBill';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsPurchaseRtnSettleDetail');
  if num>0 then
    execute immediate 'drop sequence S_PartsPurchaseRtnSettleDetail';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsPurchaseRtnSettleRef');
  if num>0 then
    execute immediate 'drop sequence S_PartsPurchaseRtnSettleRef';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsPurchaseSettleBill');
  if num>0 then
    execute immediate 'drop sequence S_PartsPurchaseSettleBill';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsPurchaseSettleDetail');
  if num>0 then
    execute immediate 'drop sequence S_PartsPurchaseSettleDetail';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsPurchaseSettleRef');
  if num>0 then
    execute immediate 'drop sequence S_PartsPurchaseSettleRef';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PurchaseNoSettleTable');
  if num>0 then
    execute immediate 'drop sequence S_PurchaseNoSettleTable';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PurchaseRtnSettleInvoiceRel');
  if num>0 then
    execute immediate 'drop sequence S_PurchaseRtnSettleInvoiceRel';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PurchaseSettleInvoiceRel');
  if num>0 then
    execute immediate 'drop sequence S_PurchaseSettleInvoiceRel';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_RebateInvoiceInformation');
  if num>0 then
    execute immediate 'drop sequence S_RebateInvoiceInformation';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_RedCodeDetail');
  if num>0 then
    execute immediate 'drop sequence S_RedCodeDetail';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_SupplierAnnualRebateBill');
  if num>0 then
    execute immediate 'drop sequence S_SupplierAnnualRebateBill';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_SupplierAnnualRebateDetail');
  if num>0 then
    execute immediate 'drop sequence S_SupplierAnnualRebateDetail';
  end if;
end;
/

create sequence S_PartsPurchaseRtnSettleBill
/

create sequence S_PartsPurchaseRtnSettleDetail
/

create sequence S_PartsPurchaseRtnSettleRef
/

create sequence S_PartsPurchaseSettleBill
/

create sequence S_PartsPurchaseSettleDetail
/

create sequence S_PartsPurchaseSettleRef
/

create sequence S_PurchaseNoSettleTable
/

create sequence S_PurchaseRtnSettleInvoiceRel
/

create sequence S_PurchaseSettleInvoiceRel
/

create sequence S_RebateInvoiceInformation
/

create sequence S_RedCodeDetail
/

create sequence S_SupplierAnnualRebateBill
/

create sequence S_SupplierAnnualRebateDetail
/

/*==============================================================*/
/* Table: PartsPurchaseRtnSettleBill                            */
/*==============================================================*/
create table PartsPurchaseRtnSettleBill  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   WarehouseId          NUMBER(9),
   WarehouseCode        VARCHAR2(50),
   WarehouseName        VARCHAR2(100),
   BranchId             NUMBER(9)                       not null,
   BranchCode           VARCHAR2(50)                    not null,
   BranchName           VARCHAR2(100)                   not null,
   PartsSalesCategoryId NUMBER(9),
   PartsSalesCategoryName VARCHAR2(100),
   PartsSupplierId      NUMBER(9)                       not null,
   PartsSupplierCode    VARCHAR2(50)                    not null,
   PartsSupplierName    VARCHAR2(100)                   not null,
   TotalSettlementAmount NUMBER(19,4)                    not null,
   OffsettedSettlementBillId NUMBER(9),
   OffsettedSettlementBillCode VARCHAR2(50),
   InvoiceAmountDifference NUMBER(19,4)                    not null,
   TaxRate              NUMBER(15,6)                    not null,
   Tax                  NUMBER(19,4)                    not null,
   InvoicePath          NUMBER(9)                       not null,
   Status               NUMBER(9)                       not null,
   SettlementPath       NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   ApproverId           NUMBER(9),
   ApproverName         VARCHAR2(100),
   ApproveTime          DATE,
   RowVersion           TIMESTAMP,
   InvoiceDate          DATE,
   IsEInvoice           NUMBER(1),
   InputStatus          NUMBER(9),
   RedCode              VARCHAR2(100),
   PaymentMethod        NUMBER(9),
   PaymentTerms         NUMBER(9),
   constraint PK_PARTSPURCHASERTNSETTLEBILL primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsPurchaseRtnSettleDetail                          */
/*==============================================================*/
create table PartsPurchaseRtnSettleDetail  (
   Id                   NUMBER(9)                       not null,
   SerialNumber         NUMBER(9)                       not null,
   PartsPurchaseRtnSettleBillId NUMBER(9)                       not null,
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50)                    not null,
   SparePartName        VARCHAR2(100)                   not null,
   SettlementPrice      NUMBER(19,4)                    not null,
   QuantityToSettle     NUMBER(9)                       not null,
   SettlementAmount     NUMBER(19,4)                    not null,
   Remark               VARCHAR2(200),
   constraint PK_PARTSPURRTNSETTLEDETAIL primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsPurchaseRtnSettleRef                             */
/*==============================================================*/
create table PartsPurchaseRtnSettleRef  (
   Id                   NUMBER(9)                       not null,
   SerialNumber         NUMBER(9)                       not null,
   PartsPurchaseRtnSettleBillId NUMBER(9)                       not null,
   SourceId             NUMBER(9)                       not null,
   SourceCode           VARCHAR2(50)                    not null,
   SettlementAmount     NUMBER(19,4)                    not null,
   SourceBillCreateTime DATE                            not null,
   WarehouseId          NUMBER(9)                       not null,
   WarehouseName        VARCHAR2(200)                   not null,
   constraint PK_PARTSPURCHASERTNSETTLEREF primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsPurchaseSettleBill                               */
/*==============================================================*/
create table PartsPurchaseSettleBill  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   WarehouseId          NUMBER(9),
   WarehouseCode        VARCHAR2(50),
   WarehouseName        VARCHAR2(100),
   BranchId             NUMBER(9)                       not null,
   BranchCode           VARCHAR2(50)                    not null,
   BranchName           VARCHAR2(100)                   not null,
   PartsSalesCategoryId NUMBER(9),
   PartsSalesCategoryName VARCHAR2(100),
   PartsSupplierId      NUMBER(9)                       not null,
   PartsSupplierCode    VARCHAR2(50)                    not null,
   PartsSupplierName    VARCHAR2(100)                   not null,
   TotalSettlementAmount NUMBER(19,4)                    not null,
   OffsettedSettlementBillId NUMBER(9),
   OffsettedSettlementBillCode VARCHAR2(50),
   TaxRate              NUMBER(15,6)                    not null,
   Tax                  NUMBER(19,4)                    not null,
   InvoiceAmountDifference NUMBER(19,4)                    not null,
   TotalCostAmount      NUMBER(19,4)                    not null,
   CostAmountDifference NUMBER(19,4)                    not null,
   Status               NUMBER(9)                       not null,
   SettlementPath       NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   InvoiceApproverId    NUMBER(9),
   InvoiceApproverName  VARCHAR2(100),
   InvoiceApproveTime   DATE,
   ApproverId           NUMBER(9),
   ApproverName         VARCHAR2(100),
   ApproveTime          DATE,
   InvoiceFlag          NUMBER(9),
   RowVersion           TIMESTAMP,
   IsUnifiedSettle      NUMBER(1),
   InvoiceDate          DATE,
   IsEInvoice           NUMBER(1),
   InputStatus          NUMBER(9),
   PaymentMethod        NUMBER(9),
   PaymentTerms         NUMBER(9),
   constraint PK_PARTSPURCHASESETTLEBILL primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsPurchaseSettleDetail                             */
/*==============================================================*/
create table PartsPurchaseSettleDetail  (
   Id                   NUMBER(9)                       not null,
   SerialNumber         NUMBER(9)                       not null,
   PartsPurchaseSettleBillId NUMBER(9)                       not null,
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50)                    not null,
   SparePartName        VARCHAR2(100)                   not null,
   SettlementPrice      NUMBER(19,4)                    not null,
   QuantityToSettle     NUMBER(9)                       not null,
   SettlementAmount     NUMBER(19,4)                    not null,
   Remark               VARCHAR2(200),
   constraint PK_PARTSPURCHASESETTLEDETAIL primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsPurchaseSettleRef                                */
/*==============================================================*/
create table PartsPurchaseSettleRef  (
   Id                   NUMBER(9)                       not null,
   SerialNumber         NUMBER(9)                       not null,
   PartsPurchaseSettleBillId NUMBER(9)                       not null,
   SourceId             NUMBER(9)                       not null,
   SourceCode           VARCHAR2(50)                    not null,
   SourceType           NUMBER(9)                       not null,
   SettlementAmount     NUMBER(19,4)                    not null,
   SourceBillCreateTime DATE                            not null,
   WarehouseId          NUMBER(9)                       not null,
   WarehouseName        VARCHAR2(200)                   not null,
   constraint PK_PARTSPURCHASESETTLEREF primary key (Id)
)
/

/*==============================================================*/
/* Table: PurchaseNoSettleTable                                 */
/*==============================================================*/
create table PurchaseNoSettleTable  (
   Id                   NUMBER(9)                       not null,
   BranchCode           VARCHAR2(50),
   PartsSalesCategoryId NUMBER(9),
   CategoryName         VARCHAR2(100),
   CompanyId            NUMBER(9),
   Code                 VARCHAR2(50),
   Name                 VARCHAR2(100),
   OrderCode            VARCHAR2(50),
   PartCode             VARCHAR2(50),
   PartName             VARCHAR2(100),
   JH                   NUMBER(19,4),
   HT                   NUMBER(19,4),
   QTY                  NUMBER(19,4),
   JHCB                 NUMBER(19,4),
   HTJE                 NUMBER(19,4),
   CreateTime           DATE,
   InBoundType          NUMBER(9),
   Value                VARCHAR2(200),
   WarehouseCode        VARCHAR2(50),
   WarehouseName        VARCHAR2(100),
   SYNCDate             VARCHAR2(100),
   constraint PK_PURCHASENOSETTLETABLE primary key (Id)
)
/

/*==============================================================*/
/* Table: PurchaseRtnSettleInvoiceRel                           */
/*==============================================================*/
create table PurchaseRtnSettleInvoiceRel  (
   Id                   NUMBER(9)                       not null,
   PartsPurchaseRtnSettleBillId NUMBER(9)                       not null,
   InvoiceId            NUMBER(9)                       not null,
   constraint PK_PURCHASERTNSETTLEINVOICEREL primary key (Id)
)
/

/*==============================================================*/
/* Table: PurchaseSettleInvoiceRel                              */
/*==============================================================*/
create table PurchaseSettleInvoiceRel  (
   Id                   NUMBER(9)                       not null,
   PartsPurchaseSettleBillId NUMBER(9)                       not null,
   InvoiceId            NUMBER(9)                       not null,
   constraint PK_PURCHASESETTLEINVOICEREL primary key (Id)
)
/

/*==============================================================*/
/* Table: RebateInvoiceInformation                              */
/*==============================================================*/
create table RebateInvoiceInformation  (
   Id                   NUMBER(9)                       not null,
   SupplierAnnualRebateBillId NUMBER(9)                       not null,
   TaxRate              NUMBER(15,6)                    not null,
   Tax                  NUMBER(19,4)                    not null,
   TotalInvoicedAmount  NUMBER(19,4),
   InvoiceCode          VARCHAR2(50)                    not null,
   InvoiceNumber        VARCHAR2(50)                    not null,
   InvoiceTime          DATE,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   constraint PK_REBATEINVOICEINFORMATION primary key (Id)
)
/

/*==============================================================*/
/* Table: RedCodeDetail                                         */
/*==============================================================*/
create table RedCodeDetail  (
   Id                   NUMBER(9)                       not null,
   Type                 NUMBER(9)                       not null,
   RedCode              VARCHAR2(200)                   not null,
   OrdeId               NUMBER(9)                       not null,
   constraint PK_REDCODEDETAIL primary key (Id)
)
/

/*==============================================================*/
/* Table: SupplierAnnualRebateBill                              */
/*==============================================================*/
create table SupplierAnnualRebateBill  (
   Id                   NUMBER(9)                       not null,
   InvoiceDate          DATE,
   Code                 VARCHAR2(50)                    not null,
   Status               NUMBER(9)                       not null,
   PartsSupplierId      NUMBER(9)                       not null,
   PartsSupplierCode    VARCHAR2(50)                    not null,
   PartsSupplierName    VARCHAR2(100)                   not null,
   Type                 NUMBER(9)                       not null,
   TotalSettlementAmount NUMBER(19,4)                    not null,
   TaxRate              NUMBER(15,6)                    not null,
   Tax                  NUMBER(19,4)                    not null,
   TotalInvoicedAmount  NUMBER(19,4),
   IsEInvoice           NUMBER(1),
   InputStatus          NUMBER(9),
   RedCode              VARCHAR2(100),
   IsRed                NUMBER(1),
   InvoiceCode          VARCHAR2(50),
   InvoiceNumber        VARCHAR2(50),
   Remark               VARCHAR2(200),
   IsInterFace          NUMBER(1),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   ApproverId           NUMBER(9),
   ApproverName         VARCHAR2(100),
   ApproveTime          DATE,
   ApproveMent          VARCHAR2(200),
   Registrant           VARCHAR2(100),
   RegistrantId         NUMBER(9),
   RegistrantDate       DATE,
   InvoiceApproverId    NUMBER(9),
   InvoiceApproverName  VARCHAR2(100),
   InvoiceApproveTime   DATE,
   Rejected             VARCHAR2(100),
   RejectedId           NUMBER(9),
   RejectedTime         DATE,
   Recoil               VARCHAR2(100),
   RecoilId             NUMBER(9),
   RecoilTime           DATE,
   RowVersion           TIMESTAMP,
   SubmitterId          NUMBER(9),
   SubmitterName        VARCHAR2(100),
   SubmitTime           DATE,
   ConfirmerId          NUMBER(9),
   Confirmer            VARCHAR2(100),
   ConfirmTime          DATE,
   InvoiceTime          DATE,
   PaymentMethod        NUMBER(9),
   PaymentTerms         NUMBER(9),
   CalculationMode      NUMBER(9),
   constraint PK_SUPPLIERANNUALREBATEBILL primary key (Id)
)
/

/*==============================================================*/
/* Table: SupplierAnnualRebateDetail                            */
/*==============================================================*/
create table SupplierAnnualRebateDetail  (
   Id                   NUMBER(9)                       not null,
   SupplierAnnualRebateBillId NUMBER(9)                       not null,
   Year                 NUMBER(9)                       not null,
   InBoundId            NUMBER(9)                       not null,
   InBoundCode          VARCHAR2(50)                    not null,
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50)                    not null,
   SparePartName        VARCHAR2(100)                   not null,
   SupplidrSpareCode    VARCHAR2(50),
   InBoundQty           NUMBER(9),
   InvoicePrice         NUMBER(19,4),
   PartsPurchaseOrderId NUMBER(9),
   PartsPurchaseOrderCode VARCHAR2(50),
   OrderType            NUMBER(9),
   PurchasePrice        NUMBER(19,4),
   DifferPurchasePrice  NUMBER(19,4),
   DifferPurchasePriceAll NUMBER(19,4),
   SettlementPrice      NUMBER(19,4),
   Rate                 NUMBER(19,4),
   TotalRebate          NUMBER(19,4),
   PartsPurchaseOrderTypeName VARCHAR2(100),
   constraint PK_SUPPLIERANNUALREBATEDETAIL primary key (Id)
)
/

