/*==============================================================*/
/* DBMS name:      ORACLE Version 10gR2                         */
/* Created on:     2024/3/5 11:06:46                            */
/*==============================================================*/


alter table BonusPointsOrderList
   drop constraint FK_BONUSPOI_RELATIONS_BONUSPOI
/

alter table ERPDeliveryDetail
   drop constraint FK_ERPDELIV_RELATIONS_ERPDELIV
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('AgencyDealerRelation');
  if num>0 then
    execute immediate 'drop table AgencyDealerRelation cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('AgencyDealerRelationHistory');
  if num>0 then
    execute immediate 'drop table AgencyDealerRelationHistory cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('AgencyRetailerList');
  if num>0 then
    execute immediate 'drop table AgencyRetailerList cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('AgencyRetailerOrder');
  if num>0 then
    execute immediate 'drop table AgencyRetailerOrder cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('BonusPointsOrder');
  if num>0 then
    execute immediate 'drop table BonusPointsOrder cascade constraints';
  end if;
end;
/

drop index Relationship_17_FK
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('BonusPointsOrderList');
  if num>0 then
    execute immediate 'drop table BonusPointsOrderList cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('CenterABCBasis');
  if num>0 then
    execute immediate 'drop table CenterABCBasis cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('CenterAccumulateDailyExp');
  if num>0 then
    execute immediate 'drop table CenterAccumulateDailyExp cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('CenterBasicClassification');
  if num>0 then
    execute immediate 'drop table CenterBasicClassification cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('CenterReimbursement');
  if num>0 then
    execute immediate 'drop table CenterReimbursement cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('CenterSaleSettle');
  if num>0 then
    execute immediate 'drop table CenterSaleSettle cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('CrossSalesOrder');
  if num>0 then
    execute immediate 'drop table CrossSalesOrder cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('CrossSalesOrderDetail');
  if num>0 then
    execute immediate 'drop table CrossSalesOrderDetail cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('CustomerDirectSpareList');
  if num>0 then
    execute immediate 'drop table CustomerDirectSpareList cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('CustomerInformation');
  if num>0 then
    execute immediate 'drop table CustomerInformation cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('CustomerSupplyInitialFeeSet');
  if num>0 then
    execute immediate 'drop table CustomerSupplyInitialFeeSet cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('DealerPartsRetailOrder');
  if num>0 then
    execute immediate 'drop table DealerPartsRetailOrder cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('DealerPartsSalesReturnBill');
  if num>0 then
    execute immediate 'drop table DealerPartsSalesReturnBill cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('DealerRetailOrderDetail');
  if num>0 then
    execute immediate 'drop table DealerRetailOrderDetail cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('DealerRetailReturnBillDetail');
  if num>0 then
    execute immediate 'drop table DealerRetailReturnBillDetail cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('ERPDelivery');
  if num>0 then
    execute immediate 'drop table ERPDelivery cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('ERPDeliveryDetail');
  if num>0 then
    execute immediate 'drop table ERPDeliveryDetail cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('ERPStock');
  if num>0 then
    execute immediate 'drop table ERPStock cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('ESBLogDianShang');
  if num>0 then
    execute immediate 'drop table ESBLogDianShang cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('ExportCustomerInfo');
  if num>0 then
    execute immediate 'drop table ExportCustomerInfo cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsQuarterly');
  if num>0 then
    execute immediate 'drop table PartsQuarterly cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsRetailOrder');
  if num>0 then
    execute immediate 'drop table PartsRetailOrder cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsRetailOrderDetail');
  if num>0 then
    execute immediate 'drop table PartsRetailOrderDetail cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsRetailReturnBill');
  if num>0 then
    execute immediate 'drop table PartsRetailReturnBill cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsRetailReturnBillDetail');
  if num>0 then
    execute immediate 'drop table PartsRetailReturnBillDetail cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsSalesOrder');
  if num>0 then
    execute immediate 'drop table PartsSalesOrder cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsSalesOrderDetail');
  if num>0 then
    execute immediate 'drop table PartsSalesOrderDetail cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsSalesOrderFinish');
  if num>0 then
    execute immediate 'drop table PartsSalesOrderFinish cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsSalesOrderProcess');
  if num>0 then
    execute immediate 'drop table PartsSalesOrderProcess cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsSalesOrderProcessDetail');
  if num>0 then
    execute immediate 'drop table PartsSalesOrderProcessDetail cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsSalesReturnBill');
  if num>0 then
    execute immediate 'drop table PartsSalesReturnBill cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsSalesReturnBillDetail');
  if num>0 then
    execute immediate 'drop table PartsSalesReturnBillDetail cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsSalesWeeklyBase');
  if num>0 then
    execute immediate 'drop table PartsSalesWeeklyBase cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PersonSalesCenterLink');
  if num>0 then
    execute immediate 'drop table PersonSalesCenterLink cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PreOrder');
  if num>0 then
    execute immediate 'drop table PreOrder cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PurDistributionDealerLog');
  if num>0 then
    execute immediate 'drop table PurDistributionDealerLog cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('RegionalPartsCategory');
  if num>0 then
    execute immediate 'drop table RegionalPartsCategory cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('RetailOrderCustomer');
  if num>0 then
    execute immediate 'drop table RetailOrderCustomer cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('Retailer_Delivery');
  if num>0 then
    execute immediate 'drop table Retailer_Delivery cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('Retailer_DeliveryDetail');
  if num>0 then
    execute immediate 'drop table Retailer_DeliveryDetail cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('Retailer_ServiceApply');
  if num>0 then
    execute immediate 'drop table Retailer_ServiceApply cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('Retailer_ServiceApplyDetail');
  if num>0 then
    execute immediate 'drop table Retailer_ServiceApplyDetail cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('SIHCenterPer');
  if num>0 then
    execute immediate 'drop table SIHCenterPer cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('SIHDailySalesAverage');
  if num>0 then
    execute immediate 'drop table SIHDailySalesAverage cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('SIHRecommendPlan');
  if num>0 then
    execute immediate 'drop table SIHRecommendPlan cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('SIHSmartOrderBase');
  if num>0 then
    execute immediate 'drop table SIHSmartOrderBase cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('SalesOrderDailyAvg');
  if num>0 then
    execute immediate 'drop table SalesOrderDailyAvg cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('SalesOrderRecommendForce');
  if num>0 then
    execute immediate 'drop table SalesOrderRecommendForce cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('SalesOrderRecommendWeekly');
  if num>0 then
    execute immediate 'drop table SalesOrderRecommendWeekly cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('SalesUnit');
  if num>0 then
    execute immediate 'drop table SalesUnit cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('SalesUnitAffiPersonnel');
  if num>0 then
    execute immediate 'drop table SalesUnitAffiPersonnel cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('SalesUnitAffiWarehouse');
  if num>0 then
    execute immediate 'drop table SalesUnitAffiWarehouse cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('SalesUnitAffiWhouseHistory');
  if num>0 then
    execute immediate 'drop table SalesUnitAffiWhouseHistory cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('SecondClassStationPlan');
  if num>0 then
    execute immediate 'drop table SecondClassStationPlan cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('SecondClassStationPlanDetail');
  if num>0 then
    execute immediate 'drop table SecondClassStationPlanDetail cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('SmartOrderAbnormal');
  if num>0 then
    execute immediate 'drop table SmartOrderAbnormal cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('SmartOrderAndType');
  if num>0 then
    execute immediate 'drop table SmartOrderAndType cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('StationServiceReport');
  if num>0 then
    execute immediate 'drop table StationServiceReport cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('VehiclePartsHandleList');
  if num>0 then
    execute immediate 'drop table VehiclePartsHandleList cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('VehiclePartsHandleOrder');
  if num>0 then
    execute immediate 'drop table VehiclePartsHandleOrder cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_AgencyDealerRelation');
  if num>0 then
    execute immediate 'drop sequence S_AgencyDealerRelation';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_AgencyDealerRelationHistory');
  if num>0 then
    execute immediate 'drop sequence S_AgencyDealerRelationHistory';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_AgencyRetailerList');
  if num>0 then
    execute immediate 'drop sequence S_AgencyRetailerList';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_AgencyRetailerOrder');
  if num>0 then
    execute immediate 'drop sequence S_AgencyRetailerOrder';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_BonusPointsOrder');
  if num>0 then
    execute immediate 'drop sequence S_BonusPointsOrder';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_BonusPointsOrderList');
  if num>0 then
    execute immediate 'drop sequence S_BonusPointsOrderList';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_CenterABCBasis');
  if num>0 then
    execute immediate 'drop sequence S_CenterABCBasis';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_CenterAccumulateDailyExp');
  if num>0 then
    execute immediate 'drop sequence S_CenterAccumulateDailyExp';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_CenterBasicClassification');
  if num>0 then
    execute immediate 'drop sequence S_CenterBasicClassification';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_CenterReimbursement');
  if num>0 then
    execute immediate 'drop sequence S_CenterReimbursement';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_CenterSaleSettle');
  if num>0 then
    execute immediate 'drop sequence S_CenterSaleSettle';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_CrossSalesOrder');
  if num>0 then
    execute immediate 'drop sequence S_CrossSalesOrder';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_CrossSalesOrderDetail');
  if num>0 then
    execute immediate 'drop sequence S_CrossSalesOrderDetail';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_CustomerDirectSpareList');
  if num>0 then
    execute immediate 'drop sequence S_CustomerDirectSpareList';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_CustomerInformation');
  if num>0 then
    execute immediate 'drop sequence S_CustomerInformation';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_CustomerSupplyInitialFeeSet');
  if num>0 then
    execute immediate 'drop sequence S_CustomerSupplyInitialFeeSet';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_DealerPartsRetailOrder');
  if num>0 then
    execute immediate 'drop sequence S_DealerPartsRetailOrder';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_DealerPartsSalesReturnBill');
  if num>0 then
    execute immediate 'drop sequence S_DealerPartsSalesReturnBill';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_DealerRetailOrderDetail');
  if num>0 then
    execute immediate 'drop sequence S_DealerRetailOrderDetail';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_DealerRetailReturnBillDetail');
  if num>0 then
    execute immediate 'drop sequence S_DealerRetailReturnBillDetail';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_ERPDelivery');
  if num>0 then
    execute immediate 'drop sequence S_ERPDelivery';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_ERPDeliveryDetail');
  if num>0 then
    execute immediate 'drop sequence S_ERPDeliveryDetail';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_ERPStock');
  if num>0 then
    execute immediate 'drop sequence S_ERPStock';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_ESBLogDianShang');
  if num>0 then
    execute immediate 'drop sequence S_ESBLogDianShang';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_ExportCustomerInfo');
  if num>0 then
    execute immediate 'drop sequence S_ExportCustomerInfo';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsQuarterly');
  if num>0 then
    execute immediate 'drop sequence S_PartsQuarterly';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsRetailOrder');
  if num>0 then
    execute immediate 'drop sequence S_PartsRetailOrder';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsRetailOrderDetail');
  if num>0 then
    execute immediate 'drop sequence S_PartsRetailOrderDetail';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsRetailReturnBill');
  if num>0 then
    execute immediate 'drop sequence S_PartsRetailReturnBill';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsRetailReturnBillDetail');
  if num>0 then
    execute immediate 'drop sequence S_PartsRetailReturnBillDetail';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsSalesOrder');
  if num>0 then
    execute immediate 'drop sequence S_PartsSalesOrder';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsSalesOrderDetail');
  if num>0 then
    execute immediate 'drop sequence S_PartsSalesOrderDetail';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsSalesOrderFinish');
  if num>0 then
    execute immediate 'drop sequence S_PartsSalesOrderFinish';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsSalesOrderProcess');
  if num>0 then
    execute immediate 'drop sequence S_PartsSalesOrderProcess';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsSalesOrderProcessDetail');
  if num>0 then
    execute immediate 'drop sequence S_PartsSalesOrderProcessDetail';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsSalesReturnBill');
  if num>0 then
    execute immediate 'drop sequence S_PartsSalesReturnBill';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsSalesReturnBillDetail');
  if num>0 then
    execute immediate 'drop sequence S_PartsSalesReturnBillDetail';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsSalesWeeklyBase');
  if num>0 then
    execute immediate 'drop sequence S_PartsSalesWeeklyBase';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PersonSalesCenterLink');
  if num>0 then
    execute immediate 'drop sequence S_PersonSalesCenterLink';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PreOrder');
  if num>0 then
    execute immediate 'drop sequence S_PreOrder';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PurDistributionDealerLog');
  if num>0 then
    execute immediate 'drop sequence S_PurDistributionDealerLog';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_RegionalPartsCategory');
  if num>0 then
    execute immediate 'drop sequence S_RegionalPartsCategory';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_RetailOrderCustomer');
  if num>0 then
    execute immediate 'drop sequence S_RetailOrderCustomer';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_Retailer_Delivery');
  if num>0 then
    execute immediate 'drop sequence S_Retailer_Delivery';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_Retailer_DeliveryDetail');
  if num>0 then
    execute immediate 'drop sequence S_Retailer_DeliveryDetail';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_Retailer_ServiceApply');
  if num>0 then
    execute immediate 'drop sequence S_Retailer_ServiceApply';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_Retailer_ServiceApplyDetail');
  if num>0 then
    execute immediate 'drop sequence S_Retailer_ServiceApplyDetail';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_SIHCenterPer');
  if num>0 then
    execute immediate 'drop sequence S_SIHCenterPer';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_SIHDailySalesAverage');
  if num>0 then
    execute immediate 'drop sequence S_SIHDailySalesAverage';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_SIHRecommendPlan');
  if num>0 then
    execute immediate 'drop sequence S_SIHRecommendPlan';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_SIHSmartOrderBase');
  if num>0 then
    execute immediate 'drop sequence S_SIHSmartOrderBase';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_SalesOrderDailyAvg');
  if num>0 then
    execute immediate 'drop sequence S_SalesOrderDailyAvg';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_SalesOrderRecommendForce');
  if num>0 then
    execute immediate 'drop sequence S_SalesOrderRecommendForce';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_SalesOrderRecommendWeekly');
  if num>0 then
    execute immediate 'drop sequence S_SalesOrderRecommendWeekly';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_SalesUnit');
  if num>0 then
    execute immediate 'drop sequence S_SalesUnit';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_SalesUnitAffiPersonnel');
  if num>0 then
    execute immediate 'drop sequence S_SalesUnitAffiPersonnel';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_SalesUnitAffiWarehouse');
  if num>0 then
    execute immediate 'drop sequence S_SalesUnitAffiWarehouse';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_SalesUnitAffiWhouseHistory');
  if num>0 then
    execute immediate 'drop sequence S_SalesUnitAffiWhouseHistory';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_SecondClassStationPlan');
  if num>0 then
    execute immediate 'drop sequence S_SecondClassStationPlan';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_SecondClassStationPlanDetail');
  if num>0 then
    execute immediate 'drop sequence S_SecondClassStationPlanDetail';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_SmartOrderAbnormal');
  if num>0 then
    execute immediate 'drop sequence S_SmartOrderAbnormal';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_SmartOrderAndType');
  if num>0 then
    execute immediate 'drop sequence S_SmartOrderAndType';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_StationServiceReport');
  if num>0 then
    execute immediate 'drop sequence S_StationServiceReport';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_VehiclePartsHandleList');
  if num>0 then
    execute immediate 'drop sequence S_VehiclePartsHandleList';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_VehiclePartsHandleOrder');
  if num>0 then
    execute immediate 'drop sequence S_VehiclePartsHandleOrder';
  end if;
end;
/

create sequence S_AgencyDealerRelation
/

create sequence S_AgencyDealerRelationHistory
/

create sequence S_AgencyRetailerList
/

create sequence S_AgencyRetailerOrder
/

create sequence S_BonusPointsOrder
/

create sequence S_BonusPointsOrderList
/

create sequence S_CenterABCBasis
/

create sequence S_CenterAccumulateDailyExp
/

create sequence S_CenterBasicClassification
/

create sequence S_CenterReimbursement
/

create sequence S_CenterSaleSettle
/

create sequence S_CrossSalesOrder
/

create sequence S_CrossSalesOrderDetail
/

create sequence S_CustomerDirectSpareList
/

create sequence S_CustomerInformation
/

create sequence S_CustomerSupplyInitialFeeSet
/

create sequence S_DealerPartsRetailOrder
/

create sequence S_DealerPartsSalesReturnBill
/

create sequence S_DealerRetailOrderDetail
/

create sequence S_DealerRetailReturnBillDetail
/

create sequence S_ERPDelivery
/

create sequence S_ERPDeliveryDetail
/

create sequence S_ERPStock
/

create sequence S_ESBLogDianShang
/

create sequence S_ExportCustomerInfo
/

create sequence S_PartsQuarterly
/

create sequence S_PartsRetailOrder
/

create sequence S_PartsRetailOrderDetail
/

create sequence S_PartsRetailReturnBill
/

create sequence S_PartsRetailReturnBillDetail
/

create sequence S_PartsSalesOrder
/

create sequence S_PartsSalesOrderDetail
/

create sequence S_PartsSalesOrderFinish
/

create sequence S_PartsSalesOrderProcess
/

create sequence S_PartsSalesOrderProcessDetail
/

create sequence S_PartsSalesReturnBill
/

create sequence S_PartsSalesReturnBillDetail
/

create sequence S_PartsSalesWeeklyBase
/

create sequence S_PersonSalesCenterLink
/

create sequence S_PreOrder
/

create sequence S_PurDistributionDealerLog
/

create sequence S_RegionalPartsCategory
/

create sequence S_RetailOrderCustomer
/

create sequence S_Retailer_Delivery
/

create sequence S_Retailer_DeliveryDetail
/

create sequence S_Retailer_ServiceApply
/

create sequence S_Retailer_ServiceApplyDetail
/

create sequence S_SIHCenterPer
/

create sequence S_SIHDailySalesAverage
/

create sequence S_SIHRecommendPlan
/

create sequence S_SIHSmartOrderBase
/

create sequence S_SalesOrderDailyAvg
/

create sequence S_SalesOrderRecommendForce
/

create sequence S_SalesOrderRecommendWeekly
/

create sequence S_SalesUnit
/

create sequence S_SalesUnitAffiPersonnel
/

create sequence S_SalesUnitAffiWarehouse
/

create sequence S_SalesUnitAffiWhouseHistory
/

create sequence S_SecondClassStationPlan
/

create sequence S_SecondClassStationPlanDetail
/

create sequence S_SmartOrderAbnormal
/

create sequence S_SmartOrderAndType
/

create sequence S_StationServiceReport
/

create sequence S_VehiclePartsHandleList
/

create sequence S_VehiclePartsHandleOrder
/

/*==============================================================*/
/* Table: AgencyDealerRelation                                  */
/*==============================================================*/
create table AgencyDealerRelation  (
   Id                   NUMBER(9)                       not null,
   BranchId             NUMBER(9)                       not null,
   PartsSalesOrderTypeId NUMBER(9)                       not null,
   AgencyId             NUMBER(9)                       not null,
   AgencyCode           VARCHAR2(50)                    not null,
   AgencyName           VARCHAR2(100)                   not null,
   DealerId             NUMBER(9)                       not null,
   DealerCode           VARCHAR2(50)                    not null,
   DealerName           VARCHAR2(100)                   not null,
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   RowVersion           TIMESTAMP,
   constraint PK_AGENCYDEALERRELATION primary key (Id)
)
/

/*==============================================================*/
/* Table: AgencyDealerRelationHistory                           */
/*==============================================================*/
create table AgencyDealerRelationHistory  (
   Id                   NUMBER(9)                       not null,
   AgencyDealerRelationId NUMBER(9),
   BranchId             NUMBER(9)                       not null,
   PartsSalesOrderTypeId NUMBER(9)                       not null,
   AgencyId             NUMBER(9)                       not null,
   AgencyCode           VARCHAR2(50)                    not null,
   AgencyName           VARCHAR2(100)                   not null,
   DealerId             NUMBER(9)                       not null,
   DealerCode           VARCHAR2(50)                    not null,
   DealerName           VARCHAR2(100)                   not null,
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   constraint PK_AGENCYDEALERRELATIONHISTORY primary key (Id)
)
/

/*==============================================================*/
/* Table: AgencyRetailerList                                    */
/*==============================================================*/
create table AgencyRetailerList  (
   Id                   NUMBER(9)                       not null,
   AgencyRetailerOrderId NUMBER(9)                       not null,
   SparePartId          NUMBER(9),
   SparePartCode        VARCHAR2(50),
   SparePartName        VARCHAR2(50),
   OrderedQuantity      NUMBER(9),
   ConfirmedAmount      NUMBER(9),
   UnitPrice            NUMBER(19,4),
   OrderSum             NUMBER(19,4),
   Remark               VARCHAR2(200),
   constraint PK_AGENCYRETAILERLIST primary key (Id)
)
/

/*==============================================================*/
/* Table: AgencyRetailerOrder                                   */
/*==============================================================*/
create table AgencyRetailerOrder  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   BranchId             NUMBER(9)                       not null,
   BranchName           VARCHAR2(50)                    not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   PartsSalesCategoryName VARCHAR2(50),
   ShippingCompanyId    NUMBER(9),
   ShippingCompanyCode  VARCHAR2(50),
   ShippingCompanyName  VARCHAR2(50),
   WarehouseId          NUMBER(9),
   WarehouseCode        VARCHAR2(50),
   WarehouseName        VARCHAR2(50),
   CounterpartCompanyId NUMBER(9),
   CounterpartCompanyCode VARCHAR2(50),
   CounterpartCompanyName VARCHAR2(100),
   VehiclePartsHandleOrderId NUMBER(9),
   VehiclePartsHandleOrderCode VARCHAR2(50),
   Status               NUMBER(9),
   ShippingStatus       NUMBER(9),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(50),
   CreateTime           DATE,
   ConfirmorId          NUMBER(9),
   ConfirmorName        VARCHAR2(50),
   ConfirmorTime        DATE,
   CloserId             NUMBER(9),
   CloserName           VARCHAR2(50),
   CloserTime           DATE,
   RowVersion           TIMESTAMP,
   PartsSalesOrderId    NUMBER(9),
   PartsSalesOrderCode  VARCHAR2(50),
   ERPSourceOrderCode   VARCHAR2(500),
   Remark               VARCHAR2(200),
   constraint PK_AGENCYRETAILERORDER primary key (Id)
)
/

/*==============================================================*/
/* Table: BonusPointsOrder                                      */
/*==============================================================*/
create table BonusPointsOrder  (
   Id                   NUMBER(9)                       not null,
   BonusPointsOrderCode VARCHAR2(50)                    not null,
   BrandId              NUMBER(9),
   BrandName            VARCHAR2(50),
   CorporationId        NUMBER(9),
   CorporationCode      VARCHAR2(50),
   CorporationName      VARCHAR2(50),
   BonusPointsAmount    NUMBER(19,4),
   BonusPoints          NUMBER(9),
   PlatForm_Code        VARCHAR2(50),
   ServiceApplyCode     VARCHAR2(50),
   Type                 NUMBER(9),
   SettlementStatus     NUMBER(9),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(50),
   CreateTime           DATE,
   Status               NUMBER(9),
   PartsOutboundBillId  NUMBER(9),
   PartsOutboundBillCode VARCHAR2(50),
   PartsInboundCheckBillId NUMBER(9),
   PartsInboundCheckBillCode VARCHAR2(50),
   PartsPurchaseSettleBillId NUMBER(9),
   PartsPurchaseSettleBillCode VARCHAR2(50),
   PurchaseSettlementStatus NUMBER(9),
   PartsPurchaseRtnSettleId NUMBER(9),
   PartsPurchaseRtnSettleCode VARCHAR2(50),
   PartsPurchaseRtnSettleStatus NUMBER(9),
   OMVehiclePartsHandleCode VARCHAR2(50),
   constraint PK_BONUSPOINTSORDER primary key (Id)
)
/

/*==============================================================*/
/* Table: BonusPointsOrderList                                  */
/*==============================================================*/
create table BonusPointsOrderList  (
   Id                   NUMBER(9)                       not null,
   BonusPointsOrderId   NUMBER(9)                       not null,
   SparePartId          NUMBER(9),
   SparePartCode        VARCHAR2(50),
   SparePartName        VARCHAR2(50),
   Quantity             NUMBER(9),
   MeasureUnit          VARCHAR2(50),
   Amount               NUMBER(19,4),
   constraint PK_BONUSPOINTSORDERLIST primary key (Id)
)
/

/*==============================================================*/
/* Index: Relationship_17_FK                                    */
/*==============================================================*/
create index Relationship_17_FK on BonusPointsOrderList (
   BonusPointsOrderId ASC
)
/

/*==============================================================*/
/* Table: CenterABCBasis                                        */
/*==============================================================*/
create table CenterABCBasis  (
   Id                   NUMBER(9)                       not null,
   CenterId             NUMBER(9),
   CenterCode           VARCHAR2(50),
   CenterName           VARCHAR2(100),
   SparePartId          NUMBER(9),
   SparePartCode        VARCHAR2(50),
   SparePartName        VARCHAR2(100),
   SihCode              VARCHAR2(50),
   OldType              NUMBER(9),
   SalesPrice           NUMBER(19,4),
   SaleSubNumber        NUMBER(9),
   SaleSubAmount        NUMBER(19,4),
   SubAmountPercentage  NUMBER(15,6),
   SaleSubFrequency     NUMBER(9),
   SubFrequencyPercentage NUMBER(15,6),
   NewType              NUMBER(9),
   CreateTime           DATE,
   WeekNum              NUMBER(9),
   constraint PK_CENTERABCBASIS primary key (Id)
)
/

/*==============================================================*/
/* Table: CenterAccumulateDailyExp                              */
/*==============================================================*/
create table CenterAccumulateDailyExp  (
   Id                   NUMBER(9)                       not null,
   Year                 NUMBER(9),
   CenterId             NUMBER(9),
   CenterCode           VARCHAR2(50),
   CenterName           VARCHAR2(100),
   Type                 NUMBER(9),
   OrderCycle           NUMBER(9),
   ArrivalCycle         NUMBER(9),
   LogisticsCycle       NUMBER(9),
   ReserveCoefficient   NUMBER(15,6),
   StockUpperCoefficient NUMBER(15,6),
   StockUpperQty        NUMBER(9),
   PackingQty           NUMBER(9),
   UsableQty            NUMBER(9),
   OnLineQty            NUMBER(9),
   OweQty               NUMBER(9),
   SparePartId          NUMBER(9),
   SparePartCode        VARCHAR2(50),
   SparePartName        VARCHAR2(100),
   ExpectPlanQty        NUMBER(15,6),
   CreateTime           DATE,
   ExpectPlanQtyAct     NUMBER(15,6),
   CumulativeFst        NUMBER(15,6),
   CenterSubmit         NUMBER(15,6),
   constraint PK_CENTERACCUMULATEDAILYEXP primary key (Id)
)
/

/*==============================================================*/
/* Table: CenterBasicClassification                             */
/*==============================================================*/
create table CenterBasicClassification  (
   Id                   NUMBER(9)                       not null,
   Type                 NUMBER(9)                       not null,
   Month                NUMBER(9),
   Weight               NUMBER(15,6),
   Days                 NUMBER(9),
   Status               NUMBER(9),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   constraint PK_CENTERBASICCLASSIFICATION primary key (Id)
)
/

/*==============================================================*/
/* Table: CenterReimbursement                                   */
/*==============================================================*/
create table CenterReimbursement  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   MarketingDepartmentId NUMBER(9),
   MarketingDepartmentName VARCHAR2(100),
   CenterId             NUMBER(9),
   CenterCode           VARCHAR2(50),
   CenterName           VARCHAR2(100),
   Type                 NUMBER(9),
   Reason               VARCHAR2(500),
   DealerName           VARCHAR2(100),
   OccurrenceTime       DATE,
   Amount               NUMBER(19,4),
   Remark               VARCHAR2(2000),
   Status               NUMBER(9)                       not null,
   CreatorId            NUMBER(9)                       not null,
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   SubmitterId          NUMBER(9),
   SubmitterName        VARCHAR2(100),
   SubmitTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   InitialApproverId    NUMBER(9),
   InitialApproverName  VARCHAR2(100),
   InitialApproveTime   DATE,
   CheckerId            NUMBER(9),
   CheckerName          VARCHAR2(100),
   CheckTime            DATE,
   ApproverId           NUMBER(9),
   ApproverName         VARCHAR2(100),
   ApproveTime          DATE,
   ApproverComment      VARCHAR2(500),
   StoperId             NUMBER(9),
   Stoper               VARCHAR2(100),
   StopTime             DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   RowVersion           TIMESTAMP,
   Path                 VARCHAR2(2000),
   constraint PK_CENTERREIMBURSEMENT primary key (Id)
)
/

/*==============================================================*/
/* Table: CenterSaleSettle                                      */
/*==============================================================*/
create table CenterSaleSettle  (
   Id                   NUMBER(9)                       not null,
   SaleOrderCode        VARCHAR2(50),
   CenterId             NUMBER(9),
   CenterCode           VARCHAR2(50),
   CenterName           VARCHAR2(100),
   DealerId             NUMBER(9),
   DealerCode           VARCHAR2(50),
   DealerName           VARCHAR2(100),
   SparePartId          NUMBER(9),
   SparePartCode        VARCHAR2(50),
   SparePartName        VARCHAR2(100),
   RetailPrice          NUMBER(19,4),
   OrderQty             NUMBER(9),
   FirstApproveQty      NUMBER(9),
   ApproveTime          DATE,
   constraint PK_CENTERSALESETTLE primary key (Id)
)
/

/*==============================================================*/
/* Table: CrossSalesOrder                                       */
/*==============================================================*/
create table CrossSalesOrder  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   Status               NUMBER(9)                       not null,
   IsAutoSales          NUMBER(1),
   ApplyCompnayId       NUMBER(9)                       not null,
   ApplyCompnayCode     VARCHAR2(50)                    not null,
   ApplyCompnayName     VARCHAR2(100)                   not null,
   SubCompanyId         NUMBER(9)                       not null,
   SubCompanyCode       VARCHAR2(50)                    not null,
   Type                 NUMBER(9),
   SubCompanyName       VARCHAR2(100)                   not null,
   ReceivingName        VARCHAR2(50),
   ReceivingPhone       VARCHAR2(100),
   ReceivingAddress     VARCHAR2(200),
   ShippingMethod       NUMBER(9)                       not null,
   SalesCode            VARCHAR2(50),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   ModifyTime           DATE,
   SubmitterId          NUMBER(9),
   SubmitterName        VARCHAR2(100),
   SubmitTime           DATE,
   RejecterId           NUMBER(9),
   RejecterName         VARCHAR2(100),
   RejectTime           DATE,
   ApproverId           NUMBER(9),
   ApproverName         VARCHAR2(100),
   ApproveTime          DATE,
   CheckerId            NUMBER(9),
   CheckerName          VARCHAR2(100),
   CheckTime            DATE,
   CloserId             NUMBER(9),
   CloserName           VARCHAR2(100),
   CloseTime            DATE,
   SeniorAuditId        NUMBER(9),
   SeniorAuditName      VARCHAR2(100),
   SeniorAuditTime      DATE,
   StoperId             NUMBER(9),
   Stoper               VARCHAR2(100),
   StopTime             DATE,
   FinisherId           NUMBER(9),
   Finisher             VARCHAR2(100),
   FinisherTime         DATE,
   ApproveMent          VARCHAR2(500),
   Path                 VARCHAR2(2000),
   constraint PK_CROSSSALESORDER primary key (Id)
)
/

/*==============================================================*/
/* Table: CrossSalesOrderDetail                                 */
/*==============================================================*/
create table CrossSalesOrderDetail  (
   Id                   NUMBER(9)                       not null,
   CrossSalesOrderId    NUMBER(9)                       not null,
   SparePartId          NUMBER(9),
   SparePartCode        VARCHAR2(50),
   SparePartName        VARCHAR2(100)                   not null,
   OrderedQuantity      NUMBER(9)                       not null,
   ABCStrategy          VARCHAR2(10),
   CenterPrice          NUMBER(19,4),
   SalesPrice           NUMBER(19,4),
   RetailGuidePrice     NUMBER(19,4),
   PriceTypeName        VARCHAR2(20),
   Vin                  VARCHAR2(100),
   Explain              VARCHAR2(200),
   Status               NUMBER(9),
   IsHand               NUMBER(1),
   constraint PK_CROSSSALESORDERDETAIL primary key (Id)
)
/

/*==============================================================*/
/* Table: CustomerDirectSpareList                               */
/*==============================================================*/
create table CustomerDirectSpareList  (
   Id                   NUMBER(9)                       not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   PartsSalesCategoryName VARCHAR2(100),
   CustomerId           NUMBER(9)                       not null,
   CustomerCode         VARCHAR2(50),
   CustomerName         VARCHAR2(100),
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50),
   SparePartName        VARCHAR2(100),
   MeasureUnit          VARCHAR2(20),
   IfDirectProvision    NUMBER(9),
   Status               NUMBER(9),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   constraint PK_CUSTOMERDIRECTSPARELIST primary key (Id)
)
/

/*==============================================================*/
/* Table: CustomerInformation                                   */
/*==============================================================*/
create table CustomerInformation  (
   Id                   NUMBER(9)                       not null,
   SalesCompanyId       NUMBER(9)                       not null,
   CustomerCompanyId    NUMBER(9)                       not null,
   ContactPerson        VARCHAR2(100),
   ContactPhone         VARCHAR2(50),
   Status               NUMBER(9)                       not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_CUSTOMERINFORMATION primary key (Id)
)
/

/*==============================================================*/
/* Table: CustomerSupplyInitialFeeSet                           */
/*==============================================================*/
create table CustomerSupplyInitialFeeSet  (
   Id                   NUMBER(9)                       not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   PartsSalesCategoryName VARCHAR2(100),
   CustomerId           NUMBER(9)                       not null,
   CustomerCode         VARCHAR2(50),
   CustomerName         VARCHAR2(100),
   SupplierId           NUMBER(9)                       not null,
   SupplierCode         VARCHAR2(50),
   SupplierName         VARCHAR2(100),
   InitialFee           NUMBER(19,4),
   Status               NUMBER(9),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   AppointShippingDays  NUMBER(9),
   LogisticsCycleDays   NUMBER(9),
   constraint PK_CUSTOMERSUPPLYINITIALFEESET primary key (Id)
)
/

/*==============================================================*/
/* Table: DealerPartsRetailOrder                                */
/*==============================================================*/
create table DealerPartsRetailOrder  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   RetailOrderType      NUMBER(9),
   BranchId             NUMBER(9)                       not null,
   BranchName           VARCHAR2(50)                    not null,
   DealerId             NUMBER(9)                       not null,
   DealerCode           VARCHAR2(50)                    not null,
   CustomerUnit         VARCHAR2(200),
   DealerName           VARCHAR2(100)                   not null,
   SubDealerId          NUMBER(9),
   SubDealerCode        VARCHAR2(50),
   SubDealerName        VARCHAR2(100),
   SubDealerlOrderType  NUMBER(9),
   ApprovalComment      VARCHAR2(200),
   Customer             VARCHAR2(100),
   CustomerPhone        VARCHAR2(50),
   CustomerCellPhone    VARCHAR2(50),
   Address              VARCHAR2(200),
   PartsSalesCategoryId NUMBER(9)                       not null,
   SalesCategoryName    VARCHAR2(100)                   not null,
   TotalAmount          NUMBER(19,4)                    not null,
   ParentChanelId       NUMBER(9),
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   ApproverId           NUMBER(9),
   ApproverName         VARCHAR2(100),
   ApproveTime          DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   Status               NUMBER(9)                       not null,
   IsSubDealer          NUMBER(1),
   RowVersion           TIMESTAMP,
   constraint PK_DEALERPARTSRETAILORDER primary key (Id)
)
/

/*==============================================================*/
/* Table: DealerPartsSalesReturnBill                            */
/*==============================================================*/
create table DealerPartsSalesReturnBill  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   BranchId             NUMBER(9)                       not null,
   BranchName           VARCHAR2(50)                    not null,
   DealerId             NUMBER(9)                       not null,
   DealerCode           VARCHAR2(50)                    not null,
   DealerName           VARCHAR2(100)                   not null,
   TotalAmount          NUMBER(19,4)                    not null,
   Customer             VARCHAR2(100),
   SalesCategoryId      NUMBER(9)                       not null,
   SalesCategoryName    VARCHAR2(100)                   not null,
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   ApproverId           NUMBER(9),
   ApproverName         VARCHAR2(100),
   ApproveTime          DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   Status               NUMBER(9)                       not null,
   RowVersion           TIMESTAMP,
   SourceId             NUMBER(9),
   SourceCode           VARCHAR2(50),
   constraint PK_DEALERPARTSSALESRETURNBILL primary key (Id)
)
/

/*==============================================================*/
/* Table: DealerRetailOrderDetail                               */
/*==============================================================*/
create table DealerRetailOrderDetail  (
   id                   NUMBER(9)                       not null,
   SerialNumber         NUMBER(9)                       not null,
   DealerPartsRetailOrderId NUMBER(9)                       not null,
   PartsId              NUMBER(9)                       not null,
   PartsCode            VARCHAR2(50)                    not null,
   PartsName            VARCHAR2(100)                   not null,
   Quantity             NUMBER(9)                       not null,
   Price                NUMBER(19,4)                    not null,
   Remark               VARCHAR2(200),
   TraceProperty        NUMBER(9),
   SIHLabelCode         VARCHAR2(500),
   SalesPrice           NUMBER(19,4),
   RetailGuidePrice     NUMBER(19,4),
   GroupCode            VARCHAR2(50),
   LESCode              VARCHAR2(1500),
   constraint PK_DEALERRETAILORDERDETAIL primary key (id)
)
/

/*==============================================================*/
/* Table: DealerRetailReturnBillDetail                          */
/*==============================================================*/
create table DealerRetailReturnBillDetail  (
   id                   NUMBER(9)                       not null,
   DealerPartsSalesReturnBillId NUMBER(9)                       not null,
   PartsId              NUMBER(9)                       not null,
   PartsCode            VARCHAR2(50)                    not null,
   PartsName            VARCHAR2(100)                   not null,
   Quantity             NUMBER(9)                       not null,
   Price                NUMBER(19,4)                    not null,
   Remark               VARCHAR2(200),
   TraceProperty        NUMBER(9),
   SIHLabelCode         VARCHAR2(500),
   constraint PK_DEALERRETAILRETURNBILLDEL primary key (id)
)
/

/*==============================================================*/
/* Table: ERPDelivery                                           */
/*==============================================================*/
create table ERPDelivery  (
   Id                   NUMBER(9)                       not null,
   Qty                  NUMBER(9),
   Amount               NUMBER(19,4),
   Payment              NUMBER(19,4),
   Unpaid_Amount        NUMBER(19,4),
   Refund               NUMBER(9),
   Cod                  NUMBER(1),
   Tag_Name             VARCHAR2(500),
   Code                 VARCHAR2(500),
   PlatForm_Code        VARCHAR2(500),
   Express_Code         VARCHAR2(500),
   Express_Name         VARCHAR2(500),
   Express_No           VARCHAR2(500),
   Warehouse_Code       VARCHAR2(500),
   Warehouse_Name       VARCHAR2(500),
   VIP_Code             VARCHAR2(500),
   VIP_Name             VARCHAR2(500),
   Receiver_Address     VARCHAR2(500),
   Area_Name            VARCHAR2(500),
   Receiver_Zip         VARCHAR2(500),
   Receiver_Mobile      VARCHAR2(500),
   Receiver_Phone       VARCHAR2(500),
   Shelf_No             VARCHAR2(500),
   Buyer_Memo           VARCHAR2(500),
   Seller_Memo          VARCHAR2(500),
   Seller_Memo_Late     VARCHAR2(500),
   Post_Fee             NUMBER(19,4),
   Receiver_Name        VARCHAR2(500),
   Shop_Name            VARCHAR2(500),
   Create_Name          VARCHAR2(500),
   Create_Date          DATE,
   Cod_Fee              NUMBER(19,4),
   Plan_Delivery_Date   DATE,
   Pay_Time             DATE,
   SyncStatus           NUMBER(9),
   SyncType             NUMBER(9),
   SyncTime             DATE,
   Shop_Code            VARCHAR2(500),
   constraint PK_ERPDELIVERY primary key (Id)
)
/

/*==============================================================*/
/* Table: ERPDeliveryDetail                                     */
/*==============================================================*/
create table ERPDeliveryDetail  (
   Id                   NUMBER(9)                       not null,
   DeliveryId           NUMBER(9)                       not null,
   Item_Code            VARCHAR2(500),
   Item_Name            VARCHAR2(500),
   Sku_Code             VARCHAR2(500),
   Sku_Name             VARCHAR2(500),
   Sku_Note             VARCHAR2(500),
   Price                VARCHAR2(500),
   Qty                  NUMBER(9),
   Amount               NUMBER(19,4),
   Amount_After         NUMBER(19,4),
   Post_Fee             NUMBER(19,4),
   Refund               NUMBER(9),
   SyncStatus           NUMBER(9),
   SyncTime             DATE,
   ErrorMessage         VARCHAR2(500),
   Discount_Fee         NUMBER(19,4),
   constraint PK_ERPDELIVERYDETAIL primary key (Id)
)
/

/*==============================================================*/
/* Table: ERPStock                                              */
/*==============================================================*/
create table ERPStock  (
   Id                   NUMBER(9)                       not null,
   WAREHOUSECODE        VARCHAR2(500),
   SPAREPARTCODE        VARCHAR2(500),
   QUANTITY             NUMBER(9),
   SYNCSTATUS           NUMBER(9),
   SYNCTIME             DATE,
   ERRORMESSAGE         VARCHAR2(500),
   constraint PK_ERPSTOCK primary key (Id)
)
/

/*==============================================================*/
/* Table: ESBLogDianShang                                       */
/*==============================================================*/
create table ESBLogDianShang  (
   Id                   NUMBER(9)                       not null,
   InterfaceName        VARCHAR2(50),
   SyncNumberBegin      NUMBER(9),
   SyncNumberEnd        NUMBER(9),
   Message              CLOB,
   TheDate              DATE,
   constraint PK_ESBLOGDIANSHANG primary key (Id)
)
/

/*==============================================================*/
/* Table: ExportCustomerInfo                                    */
/*==============================================================*/
create table ExportCustomerInfo  (
   Id                   NUMBER(9)                       not null,
   CustomerCode         VARCHAR2(50),
   CustomerName         VARCHAR2(100),
   InvoiceCompanyId     NUMBER(9),
   InvoiceCompanyName   VARCHAR2(100),
   PriceType            NUMBER(9),
   Status               NUMBER(9),
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   Coefficient          NUMBER(19,4),
   constraint PK_EXPORTCUSTOMERINFO primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsQuarterly                                        */
/*==============================================================*/
create table PartsQuarterly  (
   Id                   NUMBER(9)                       not null,
   StockType            NUMBER(9)                       not null,
   Islast               NUMBER(1)                       not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   constraint PK_PARTSQUARTERLY primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsRetailOrder                                      */
/*==============================================================*/
create table PartsRetailOrder  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   CustomerAccountId    NUMBER(9),
   RetailCustomerCompanyId NUMBER(9),
   RetailCustomerCompanyCode VARCHAR2(50),
   RetailCustomerCompanyName VARCHAR2(100),
   BranchId             NUMBER(9)                       not null,
   BranchCode           VARCHAR2(50)                    not null,
   BranchName           VARCHAR2(100)                   not null,
   SalesUnitOwnerCompanyId NUMBER(9)                       not null,
   SalesUnitOwnerCompanyCode VARCHAR2(50)                    not null,
   SalesUnitOwnerCompanyName VARCHAR2(100)                   not null,
   SalesUnitId          NUMBER(9)                       not null,
   SalesUnitCode        VARCHAR2(50)                    not null,
   SalesUnitName        VARCHAR2(100)                   not null,
   WarehouseId          NUMBER(9)                       not null,
   WarehouseCode        VARCHAR2(50)                    not null,
   WarehouseName        VARCHAR2(100)                   not null,
   CustomerName         VARCHAR2(100)                   not null,
   CustomerPhone        VARCHAR2(50),
   CustomerCellPhone    VARCHAR2(50),
   CustomerAddress      VARCHAR2(200),
   OriginalAmount       NUMBER(19,4)                    not null,
   DiscountRate         NUMBER(15,6)                    not null,
   DiscountAmount       NUMBER(19,4)                    not null,
   DiscountedAmount     NUMBER(19,4)                    not null,
   PaymentMethod        NUMBER(9)                       not null,
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   ApproverId           NUMBER(9),
   ApproverName         VARCHAR2(100),
   ApproveTime          DATE,
   InvoiceOperatorId    NUMBER(9),
   InvoiceOperatorName  VARCHAR2(100),
   InvoiceTime          DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   RowVersion           TIMESTAMP,
   IsOil                NUMBER(1),
   constraint PK_PARTSRETAILORDER primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsRetailOrderDetail                                */
/*==============================================================*/
create table PartsRetailOrderDetail  (
   Id                   NUMBER(9)                       not null,
   PartsRetailOrderId   NUMBER(9)                       not null,
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50)                    not null,
   SparePartName        VARCHAR2(100)                   not null,
   MeasureUnit          VARCHAR2(20),
   Quantity             NUMBER(9)                       not null,
   SalesPrice           NUMBER(19,4)                    not null,
   DiscountAmount       NUMBER(19,4)                    not null,
   DiscountedPrice      NUMBER(19,4)                    not null,
   DiscountedAmount     NUMBER(19,4)                    not null,
   Remark               VARCHAR2(200),
   DelaerPrice          NUMBER(19,4),
   PriceTypeName        VARCHAR2(20),
   TraceProperty        NUMBER(9),
   LESCode              VARCHAR2(1500),
   constraint PK_PARTSRETAILORDERDETAIL primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsRetailReturnBill                                 */
/*==============================================================*/
create table PartsRetailReturnBill  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   SourceId             NUMBER(9)                       not null,
   SourceCode           VARCHAR2(50)                    not null,
   CustomerAccountId    NUMBER(9),
   RetailCustomerCompanyId NUMBER(9),
   RetailCustomerCompanyCode VARCHAR2(50),
   RetailCustomerCompanyName VARCHAR2(100),
   SalesUnitOwnerCompanyId NUMBER(9)                       not null,
   SalesUnitOwnerCompanyCode VARCHAR2(50)                    not null,
   SalesUnitOwnerCompanyName VARCHAR2(100)                   not null,
   BranchId             NUMBER(9)                       not null,
   BranchCode           VARCHAR2(50)                    not null,
   BranchName           VARCHAR2(100)                   not null,
   SalesUnitId          NUMBER(9)                       not null,
   SalesUnitCode        VARCHAR2(50)                    not null,
   SalesUnitName        VARCHAR2(100)                   not null,
   CustomerName         VARCHAR2(100)                   not null,
   CustomerPhone        VARCHAR2(50),
   CustomerCellPhone    VARCHAR2(50),
   CustomerAddress      VARCHAR2(200),
   WarehouseId          NUMBER(9)                       not null,
   WarehouseCode        VARCHAR2(50)                    not null,
   WarehouseName        VARCHAR2(100)                   not null,
   TotalAmount          NUMBER(19,4)                    not null,
   ReturnReason         VARCHAR2(200),
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   ApproverId           NUMBER(9),
   ApproverName         VARCHAR2(100),
   ApproveTime          DATE,
   RefundOperatorId     NUMBER(9),
   RefundOperatorName   VARCHAR2(100),
   RefundTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   RowVersion           TIMESTAMP,
   constraint PK_PARTSRETAILRETURNBILL primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsRetailReturnBillDetail                           */
/*==============================================================*/
create table PartsRetailReturnBillDetail  (
   Id                   NUMBER(9)                       not null,
   PartsRetailReturnBillId NUMBER(9)                       not null,
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50)                    not null,
   SparePartName        VARCHAR2(100)                   not null,
   MeasureUnit          VARCHAR2(20),
   Quantity             NUMBER(9)                       not null,
   OriginalOrderPrice   NUMBER(19,4)                    not null,
   ReturnPrice          NUMBER(19,4)                    not null,
   constraint PK_PARTSRETAILRETURNBILLDETAIL primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsSalesOrder                                       */
/*==============================================================*/
create table PartsSalesOrder  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   BranchId             NUMBER(9)                       not null,
   BranchCode           VARCHAR2(50)                    not null,
   BranchName           VARCHAR2(100)                   not null,
   SalesCategoryId      NUMBER(9)                       not null,
   SalesCategoryName    VARCHAR2(100),
   SalesUnitId          NUMBER(9)                       not null,
   SalesUnitCode        VARCHAR2(50)                    not null,
   SalesUnitName        VARCHAR2(100)                   not null,
   SalesUnitOwnerCompanyId NUMBER(9)                       not null,
   SalesUnitOwnerCompanyCode VARCHAR2(50)                    not null,
   SalesUnitOwnerCompanyName VARCHAR2(100)                   not null,
   SourceBillCode       VARCHAR2(50),
   SourceBillId         NUMBER(9),
   CustomerAccountId    NUMBER(9)                       not null,
   IsDebt               NUMBER(1)                       not null,
   InvoiceReceiveCompanyId NUMBER(9)                       not null,
   InvoiceReceiveCompanyCode VARCHAR2(50)                    not null,
   InvoiceReceiveCompanyName VARCHAR2(100)                   not null,
   InvoiceReceiveCompanyType NUMBER(9)                       not null,
   InvoiceReceiveSaleCateId NUMBER(9),
   InvoiceReceiveSaleCateName VARCHAR2(50),
   SubmitCompanyId      NUMBER(9)                       not null,
   SubmitCompanyCode    VARCHAR2(50)                    not null,
   SubmitCompanyName    VARCHAR2(100)                   not null,
   ProvinceID           NUMBER(9),
   Province             VARCHAR2(100),
   City                 VARCHAR2(100),
   FirstClassStationId  NUMBER(9),
   FirstClassStationCode VARCHAR2(50),
   FirstClassStationName VARCHAR2(100),
   PartsSalesOrderTypeId NUMBER(9)                       not null,
   PartsSalesOrderTypeName VARCHAR2(100)                   not null,
   TotalAmount          NUMBER(19,4)                    not null,
   IfAgencyService      NUMBER(1)                       not null,
   SalesActivityDiscountRate NUMBER(15,6)                    not null,
   SalesActivityDiscountAmount NUMBER(19,4)                    not null,
   RequestedShippingTime DATE,
   RequestedDeliveryTime DATE,
   IfDirectProvision    NUMBER(1)                       not null,
   CustomerType         NUMBER(9)                       not null,
   SecondLevelOrderType NUMBER(9),
   Remark               VARCHAR2(200),
   ApprovalComment      VARCHAR2(2000),
   ContactPerson        VARCHAR2(50),
   ContactPhone         VARCHAR2(100),
   ReceivingCompanyId   NUMBER(9)                       not null,
   ReceivingCompanyCode VARCHAR2(50)                    not null,
   ReceivingCompanyName VARCHAR2(100)                   not null,
   ShippingMethod       NUMBER(9)                       not null,
   ReceivingWarehouseId NUMBER(9),
   ReceivingWarehouseName VARCHAR2(50),
   CompanyAddressId     NUMBER(9),
   ReceivingAddress     VARCHAR2(200),
   OriginalRequirementBillId NUMBER(9),
   OriginalRequirementBillType NUMBER(9),
   WarehouseId          NUMBER(9),
   StopComment          VARCHAR2(200),
   RejectComment        VARCHAR2(200),
   IsTransSuccess       NUMBER(1),
   IsPurDistribution    NUMBER(1),
   FirstApproveTime     DATE,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   SubmitterId          NUMBER(9),
   SubmitterName        VARCHAR2(100),
   SubmitTime           DATE,
   ApproverId           NUMBER(9),
   ApproverName         VARCHAR2(100),
   ApproveTime          DATE,
   RejecterId           NUMBER(9),
   RejecterName         VARCHAR2(100),
   RejectTime           DATE,
   CancelOperatorId     NUMBER(9),
   CancelOperatorName   VARCHAR2(100),
   CancelTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   IsAutoApprove        NUMBER(1),
   AutoApproveComment   VARCHAR2(2000),
   AutoApproveTime      DATE,
   Status               NUMBER(9)                       not null,
   InvoiceCustomerName  VARCHAR2(100),
   BankAccount          VARCHAR2(200),
   Taxpayeridentification VARCHAR2(50),
   InvoiceAdress        VARCHAR2(100),
   InvoicePhone         VARCHAR2(50),
   InvoiceType          NUMBER(9),
   ERPOrderCode         VARCHAR2(500),
   ERPSourceOrderCode   VARCHAR2(500),
   RowVersion           TIMESTAMP,
   OverseasDemandSheetNo VARCHAR2(50),
   IsBarter             NUMBER(1)                       not null,
   EnterpriseType       NUMBER(9),
   ReceivingMailbox     VARCHAR2(100),
   IfInnerDirectProvision NUMBER(1),
   InnerSourceId        NUMBER(9),
   InnerSourceCode      VARCHAR2(50),
   VehiclePartsHandleOrderCode VARCHAR2(50),
   VehiclePartsHandleOrderId NUMBER(9),
   IsAgencyShip         NUMBER(1),
   EcommerceMoney       NUMBER(19,4),
   OMVehiclePartsHandleCode VARCHAR2(50),
   IsVehicleOutage      NUMBER(1),
   CPPartsPurchaseOrderCode VARCHAR2(50),
   CPPartsInboundCheckCode VARCHAR2(50),
   IsExport             NUMBER(9),
   InboundCompanyId     NUMBER(9),
   InboundCompanyCode   VARCHAR2(50),
   InboundCompanyName   VARCHAR2(100),
   ContractCode         VARCHAR2(50),
   PackingMethod        VARCHAR2(200),
   PriceType            NUMBER(9),
   Time                 NUMBER(9),
   AutoApproveStatus    NUMBER(9),
   IsDirectSales        NUMBER(1),
   IsMissing            NUMBER(1),
   WorkOrderCode        VARCHAR2(100),
   constraint PK_PARTSSALESORDER primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsSalesOrderDetail                                 */
/*==============================================================*/
create table PartsSalesOrderDetail  (
   Id                   NUMBER(9)                       not null,
   PartsSalesOrderId    NUMBER(9)                       not null,
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50)                    not null,
   SparePartName        VARCHAR2(100)                   not null,
   MeasureUnit          VARCHAR2(20),
   OrderedQuantity      NUMBER(9)                       not null,
   ApproveQuantity      NUMBER(9),
   OriginalPrice        NUMBER(19,4)                    not null,
   CustOrderPriceGradeCoefficient NUMBER(15,6)                    not null,
   IfCanNewPart         NUMBER(1)                       not null,
   DiscountedPrice      NUMBER(19,4)                    not null,
   OrderPrice           NUMBER(19,4)                    not null,
   OrderSum             NUMBER(19,4)                    not null,
   EstimatedFulfillTime DATE,
   Remark               VARCHAR2(200),
   OverseasPartsFigure  VARCHAR2(25),
   OverseasProjectNumber NUMBER(9),
   MInSaleingAmount     NUMBER(9),
   PriceTypeName        VARCHAR2(20),
   ABCStrategy          VARCHAR2(10),
   OrderNo              VARCHAR2(50),
   SalesPrice           NUMBER(19,4),
   constraint PK_PARTSSALESORDERDETAIL primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsSalesOrderFinish                                 */
/*==============================================================*/
create table PartsSalesOrderFinish  (
   Id                   NUMBER(9)                       not null,
   PartsSalesOrderId    NUMBER(9),
   Code                 VARCHAR2(100),
   PartsSalesOrderProcessCode VARCHAR2(100),
   SalesUnitOwnerCompanyId NUMBER(9),
   SalesUnitOwnerCompanyName VARCHAR2(100),
   WarehouseId          NUMBER(9),
   WarehouseName        VARCHAR2(100),
   Province             VARCHAR2(100),
   SubmitCompanyCode    VARCHAR2(100),
   SubmitCompanyName    VARCHAR2(100),
   ReceivingCompanyName VARCHAR2(100),
   SparePartId          NUMBER(9),
   SparePartCode        VARCHAR2(100),
   SparePartName        VARCHAR2(100),
   EnglishName          VARCHAR2(200),
   PartABC              NUMBER(9),
   IfDirectProvision    NUMBER(1),
   PurchaseRoute        NUMBER(9),
   OrderProcessMethod   NUMBER(9),
   OrderedQuantity      NUMBER(9),
   ApproveQuantity      NUMBER(9),
   GroupName            VARCHAR2(100),
   OrderPrice           NUMBER(19,4),
   OrderSum             NUMBER(19,4),
   OriginalPrice        NUMBER(19,4),
   OriginalPriceSum     NUMBER(19,4),
   Weight               NUMBER(19,4),
   Volume               NUMBER(19,4),
   PartsOutboundBillCode VARCHAR2(100),
   ApproveTime          DATE,
   Remark               VARCHAR2(500),
   FirstApproveTime     DATE,
   ShippingDate         DATE,
   ShippingCreatTime    DATE,
   PartsSalesOrderTypeName VARCHAR2(100),
   CreateTime           DATE,
   SubmitTime           DATE,
   MarketingDepartmentName VARCHAR2(200),
   PartsSalesOrderTypeId NUMBER(9),
   Type                 NUMBER(9),
   Time                 NUMBER(9),
   OutboundAmount       NUMBER(9),
   StatusStr            VARCHAR2(200),
   PartsOutboundPlanStatus NUMBER(9),
   constraint PK_PARTSSALESORDERFINISH primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsSalesOrderProcess                                */
/*==============================================================*/
create table PartsSalesOrderProcess  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   OriginalSalesOrderId NUMBER(9)                       not null,
   BillStatusBeforeProcess NUMBER(9)                       not null,
   BillStatusAfterProcess NUMBER(9)                       not null,
   RequestedShippingTime DATE,
   RequestedDeliveryTime DATE,
   PartsSalesActivityName VARCHAR2(100),
   CurrentFulfilledAmount NUMBER(19,4)                    not null,
   ApprovalComment      VARCHAR2(200),
   ShippingMethod       NUMBER(9)                       not null,
   ShippingInformationRemark VARCHAR2(200),
   CreateTime           DATE,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   Remark               VARCHAR2(200),
   Time                 NUMBER(9),
   constraint PK_PARTSSALESORDERPROCESS primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsSalesOrderProcessDetail                          */
/*==============================================================*/
create table PartsSalesOrderProcessDetail  (
   Id                   NUMBER(9)                       not null,
   PartsSalesOrderProcessId NUMBER(9)                       not null,
   WarehouseId          NUMBER(9),
   WarehouseCode        VARCHAR2(50),
   WarehouseName        VARCHAR2(100),
   PurchaseWarehouseId  NUMBER(9),
   PurchaseWarehouseCode VARCHAR2(50),
   PurchaseWarehouseName VARCHAR2(100),
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50)                    not null,
   SparePartName        VARCHAR2(100)                   not null,
   NewPartId            NUMBER(9),
   NewPartCode          VARCHAR2(50),
   NewPartName          VARCHAR2(100),
   IfDirectSupply       NUMBER(1),
   MeasureUnit          VARCHAR2(20),
   OrderedQuantity      NUMBER(9)                       not null,
   CurrentFulfilledQuantity NUMBER(9)                       not null,
   OrderPrice           NUMBER(19,4)                    not null,
   AdjustPrice          NUMBER(19,4),
   OrderProcessStatus   NUMBER(9)                       not null,
   OrderProcessMethod   NUMBER(9)                       not null,
   SupplierCompanyId    NUMBER(9),
   SupplierCompanyCode  VARCHAR2(50),
   SupplierCompanyName  VARCHAR2(100),
   SupplierWarehouseId  NUMBER(9),
   SupplierWarehouseCode VARCHAR2(50),
   SupplierWarehouseName VARCHAR2(100),
   CreateTime           DATE,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   constraint PK_PARTSSALESORDERPROCDETAIL primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsSalesReturnBill                                  */
/*==============================================================*/
create table PartsSalesReturnBill  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   ReturnCompanyId      NUMBER(9)                       not null,
   ReturnCompanyCode    VARCHAR2(50)                    not null,
   ReturnCompanyName    VARCHAR2(100)                   not null,
   InvoiceReceiveCompanyId NUMBER(9)                       not null,
   InvoiceReceiveCompanyCode VARCHAR2(50)                    not null,
   InvoiceReceiveCompanyName VARCHAR2(100)                   not null,
   SubmitCompanyId      NUMBER(9)                       not null,
   SubmitCompanyCode    VARCHAR2(50)                    not null,
   SubmitCompanyName    VARCHAR2(100)                   not null,
   FirstClassStationId  NUMBER(9),
   FirstClassStationName VARCHAR2(100),
   FirstClassStationCode VARCHAR2(50),
   TotalAmount          NUMBER(19,4)                    not null,
   ShippingMethod       NUMBER(9),
   SalesUnitId          NUMBER(9)                       not null,
   SalesUnitName        VARCHAR2(100)                   not null,
   SalesUnitOwnerCompanyId NUMBER(9)                       not null,
   SalesUnitOwnerCompanyCode VARCHAR2(50)                    not null,
   SalesUnitOwnerCompanyName VARCHAR2(100)                   not null,
   CustomerAccountId    NUMBER(9)                       not null,
   BranchId             NUMBER(9)                       not null,
   BranchCode           VARCHAR2(50)                    not null,
   BranchName           VARCHAR2(100)                   not null,
   InvoiceRequirement   NUMBER(9),
   BlueInvoiceNumber    VARCHAR2(50),
   ContactPerson        VARCHAR2(100),
   ContactPhone         VARCHAR2(50),
   RejectComment        VARCHAR2(200),
   StopComment          VARCHAR2(200),
   ReturnWarehouseId    NUMBER(9),
   ReturnWarehouseCode  VARCHAR2(50),
   ReturnWarehouseName  VARCHAR2(100),
   WarehouseId          NUMBER(9),
   WarehouseCode        VARCHAR2(50),
   WarehouseName        VARCHAR2(100),
   StopId               NUMBER(9),
   StopName             VARCHAR2(100),
   StopTime             DATE,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RejecterId           NUMBER(9),
   RejecterName         VARCHAR2(100),
   RejectTime           DATE,
   ApproverId           NUMBER(9),
   ApproverName         VARCHAR2(100),
   ApproveTime          DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   Status               NUMBER(9)                       not null,
   ReturnType           NUMBER(9)                       not null,
   ReturnReason         VARCHAR2(100),
   Remark               VARCHAR2(200),
   serviceApplyCode     VARCHAR2(50),
   serviceApplyId       NUMBER(9),
   RowVersion           TIMESTAMP,
   OldOverseasDemandSheetNo VARCHAR2(50),
   OverseasDemandSheetNo VARCHAR2(25),
   IsBarter             NUMBER(1)                       not null,
   ERPSourceOrderCode   VARCHAR2(500),
   DiscountRate         NUMBER(19,2),
   InitialApproverId    NUMBER(9),
   InitialApproverName  VARCHAR2(100),
   InitialApproveTime   DATE,
   FinalApproverId      NUMBER(9),
   FinalApproverName    VARCHAR2(100),
   FinalApproveTime     DATE,
   Path                 VARCHAR2(2000),
   IsWarrantyReturn     NUMBER(1),
   IsTurn               NUMBER(1),
   IsSpecial            NUMBER(1),
   IsOil                NUMBER(1),
   constraint PK_PARTSSALESRETURNBILL primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsSalesReturnBillDetail                            */
/*==============================================================*/
create table PartsSalesReturnBillDetail  (
   Id                   NUMBER(9)                       not null,
   PartsSalesReturnBillId NUMBER(9)                       not null,
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50)                    not null,
   SparePartName        VARCHAR2(100)                   not null,
   MeasureUnit          VARCHAR2(20),
   ReturnedQuantity     NUMBER(9)                       not null,
   ApproveQuantity      NUMBER(9),
   OriginalOrderPrice   NUMBER(19,4),
   ReturnPrice          NUMBER(19,4)                    not null,
   BatchNumber          VARCHAR2(100),
   Remark               VARCHAR2(200),
   OverseasPartsFigure  VARCHAR2(25),
   OverseasProjectNumber NUMBER(9),
   ReturnContainerNumberLine VARCHAR2(25),
   SettlementPurchaseOrder VARCHAR2(25),
   SettlementPurchaseOrderLine VARCHAR2(25),
   OriginalPrice        NUMBER(19,4),
   PartsSalesOrderId    NUMBER(9),
   OriginalRequirementBillId NUMBER(9),
   OriginalRequirementBillType NUMBER(9),
   PartsSalesOrderCode  VARCHAR2(50),
   DiscountRate         NUMBER(15,2),
   TraceProperty        NUMBER(9),
   SIHLabelCode         VARCHAR2(500),
   IsOil                NUMBER(1),
   constraint PK_PARTSSALESRETURNBILLDETAIL primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsSalesWeeklyBase                                  */
/*==============================================================*/
create table PartsSalesWeeklyBase  (
   Id                   NUMBER(9)                       not null,
   SparePartId          NUMBER(9),
   SparePartCode        VARCHAR2(50),
   SparePartName        VARCHAR2(100),
   Year                 NUMBER(9),
   Week                 NUMBER(9),
   WarehouseId          NUMBER(9),
   WarehouseCode        VARCHAR2(50),
   WarehouseName        VARCHAR2(50),
   PartsSalesOrderTypeId NUMBER(9),
   PartsSalesOrderTypeName VARCHAR2(100),
   Quantity             NUMBER(9),
   CreateTime           DATE,
   constraint PK_PARTSSALESWEEKLYBASE primary key (Id)
)
/

/*==============================================================*/
/* Table: PersonSalesCenterLink                                 */
/*==============================================================*/
create table PersonSalesCenterLink  (
   Id                   NUMBER(9)                       not null,
   CompanyId            NUMBER(9),
   PersonId             NUMBER(9)                       not null,
   PersonType           NUMBER(9),
   PartsSalesCategoryId NUMBER(9)                       not null,
   Status               NUMBER(9),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   constraint PK_PERSONSALESCENTERLINK primary key (Id)
)
/

/*==============================================================*/
/* Table: PreOrder                                              */
/*==============================================================*/
create table PreOrder  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50),
   DealerCode           VARCHAR2(50),
   PartsSalesCategoryCode VARCHAR2(50),
   PartCode             VARCHAR2(50),
   PartName             VARCHAR2(100),
   Qty                  NUMBER(9),
   Status               NUMBER(9),
   constraint PK_PREORDER primary key (Id)
)
/

/*==============================================================*/
/* Table: PurDistributionDealerLog                              */
/*==============================================================*/
create table PurDistributionDealerLog  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(1000),
   SyncDate             DATE,
   OutBoundCode         VARCHAR2(100),
   Status               NUMBER(9),
   DealDate             DATE,
   constraint PK_PURDISTRIBUTIONDEALERLOG primary key (Id)
)
/

/*==============================================================*/
/* Table: RegionalPartsCategory                                 */
/*==============================================================*/
create table RegionalPartsCategory  (
   Id                   NUMBER(9)                       not null,
   Year                 NUMBER(9)                       not null,
   Quarter              NUMBER(9)                       not null,
   CenterId             NUMBER(9)                       not null,
   CenterCode           VARCHAR2(50)                    not null,
   CenterName           VARCHAR2(100)                   not null,
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50)                    not null,
   SparePartName        VARCHAR2(100),
   QuarterlyDemand      NUMBER(9),
   DemandFrequency      NUMBER(9),
   RecommendedRe        NUMBER(15,6),
   DailyDemand          NUMBER(15,6),
   PartType             NUMBER(9),
   TurnDate             DATE,
   constraint PK_REGIONALPARTSCATEGORY primary key (Id)
)
/

/*==============================================================*/
/* Table: RetailOrderCustomer                                   */
/*==============================================================*/
create table RetailOrderCustomer  (
   Id                   NUMBER(9)                       not null,
   ��ҵId                 NUMBER(9)                       not null,
   CustomerName         VARCHAR2(100)                   not null,
   CustomerPhone        VARCHAR2(50),
   CustomerCellPhone    VARCHAR2(50),
   Address              VARCHAR2(200),
   CustomerUnit         VARCHAR2(200),
   Status               NUMBER(9)                       not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_RETAILORDERCUSTOMER primary key (Id)
)
/

/*==============================================================*/
/* Table: Retailer_Delivery                                     */
/*==============================================================*/
create table Retailer_Delivery  (
   Id                   NUMBER(9)                       not null,
   OrderNumber          VARCHAR2(50)                    not null,
   ReceiverName         VARCHAR2(50),
   Mobile               VARCHAR2(15),
   Province             VARCHAR2(20),
   City                 VARCHAR2(20),
   District             VARCHAR2(20),
   Address              VARCHAR2(500),
   Warehouse_Code       VARCHAR2(500),
   Warehouse_Name       VARCHAR2(500),
   CustomerCode         VARCHAR2(20),
   CustomerName         VARCHAR2(50),
   InvoiceStatus        NUMBER(9),
   InvoiceTitle         VARCHAR2(1024),
   BankAccount          VARCHAR2(50),
   Taxpayeridentification VARCHAR2(20),
   InvoiceAdress        VARCHAR2(100),
   InvoiceTel           VARCHAR2(20),
   BuyerMessage         VARCHAR2(1024),
   Remark               VARCHAR2(1024),
   OrderCreateTime      VARCHAR2(50),
   lastModifyTime       VARCHAR2(50),
   PaymentType          NUMBER(9),
   PaymentTypeName      VARCHAR2(20),
   ActualAmount         NUMBER(14,2),
   ActualFreight        NUMBER(14,2),
   PaymentStatus        NUMBER(1),
   PaymentTime          VARCHAR2(50),
   EnterpriseCode       VARCHAR2(50),
   InterfaceType        NUMBER(9),
   Shop_Code            VARCHAR2(500),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_RETAILER_DELIVERY primary key (Id)
)
/

/*==============================================================*/
/* Table: Retailer_DeliveryDetail                               */
/*==============================================================*/
create table Retailer_DeliveryDetail  (
   Id                   NUMBER(9)                       not null,
   OrderDeliveryId      NUMBER(9)                       not null,
   OrderDetailId        NUMBER(9)                       not null,
   GoodsCode            VARCHAR2(50),
   SyncStatus           NUMBER(9),
   SyncTime             DATE,
   Quantity             NUMBER(9),
   CouponPrice          NUMBER(14,2),
   Weight               NUMBER(14,2),
   TotalAmount          NUMBER(14,2),
   Message              VARCHAR2(500),
   constraint PK_RETAILER_DELIVERYDETAIL primary key (Id)
)
/

/*==============================================================*/
/* Table: Retailer_ServiceApply                                 */
/*==============================================================*/
create table Retailer_ServiceApply  (
   Id                   NUMBER(9)                       not null,
   OrderNumber          VARCHAR2(20),
   ServiceApplyId       NUMBER(9)                       not null,
   Reason               VARCHAR2(20),
   RefundAmount         NUMBER(14,2),
   RefundFreight        NUMBER(14,2),
   InterfaceType        NUMBER(9),
   SyncStatus           NUMBER(9),
   SyncTime             DATE,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_RETAILER_SERVICEAPPLY primary key (Id)
)
/

/*==============================================================*/
/* Table: Retailer_ServiceApplyDetail                           */
/*==============================================================*/
create table Retailer_ServiceApplyDetail  (
   Id                   NUMBER(9)                       not null,
   GoodsCode            VARCHAR2(50),
   GoodsName            VARCHAR2(40),
   ReturnQuantity       NUMBER(9),
   ReturnPrice          NUMBER(9),
   ServiceApplyId       NUMBER(9)                       not null,
   SyncTime             DATE,
   SyncStatus           NUMBER(9),
   Message              VARCHAR2(100),
   constraint PK_RETAILER_SERVICEAPPLYDETAIL primary key (Id)
)
/

/*==============================================================*/
/* Table: SIHCenterPer                                          */
/*==============================================================*/
create table SIHCenterPer  (
   Id                   NUMBER(9)                       not null,
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50)                    not null,
   SparePartName        VARCHAR2(100)                   not null,
   SubmitCompanyId      NUMBER(9)                       not null,
   SubmitCompanyCode    VARCHAR2(50)                    not null,
   SubmitCompanyName    VARCHAR2(100)                   not null,
   ReferenceCode        VARCHAR2(50),
   WarehouseId          NUMBER(9)                       not null,
   WarehouseName        VARCHAR2(100)                   not null,
   WarehouseCode        VARCHAR2(100)                   not null,
   CarryTime            DATE,
   PerQty               NUMBER(19,4),
   SumQty               NUMBER(9),
   MaxOrderedquantity   NUMBER(9),
   constraint PK_SIHCENTERPER primary key (Id)
)
/

/*==============================================================*/
/* Table: SIHDailySalesAverage                                  */
/*==============================================================*/
create table SIHDailySalesAverage  (
   Id                   NUMBER(9)                       not null,
   WarehouseId          NUMBER(9)                       not null,
   WarehouseName        VARCHAR2(100)                   not null,
   WarehouseCode        VARCHAR2(100)                   not null,
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50)                    not null,
   SparePartName        VARCHAR2(100)                   not null,
   PartABC              NUMBER(9),
   SalesPrice           NUMBER(19,4),
   ReferenceCode        VARCHAR2(50),
   OneWeek              NUMBER(19,4),
   TwoWeek              NUMBER(19,4),
   ThreeWeek            NUMBER(19,4),
   FourWeek             NUMBER(19,4),
   FiveWeek             NUMBER(19,4),
   SixWeek              NUMBER(19,4),
   SevenWeek            NUMBER(19,4),
   EightWeek            NUMBER(19,4),
   NineWeek             NUMBER(19,4),
   TenWeek              NUMBER(19,4),
   ElevenWeek           NUMBER(19,4),
   TwelveWeek           NUMBER(19,4),
   ThirtWeek            NUMBER(19,4),
   FourteenWeek         NUMBER(19,4),
   FifteenWeek          NUMBER(19,4),
   SixteenWeek          NUMBER(19,4),
   SeventeenWeek        NUMBER(19,4),
   EightteenWeek        NUMBER(19,4),
   NineteenWeek         NUMBER(19,4),
   TwentyWeek           NUMBER(19,4),
   TwentyOneWeek        NUMBER(19,4),
   TwentyTwoWeek        NUMBER(19,4),
   TwentyThreeWeek      NUMBER(19,4),
   TwentyFourWeek       NUMBER(19,4),
   NDaysAvg             NUMBER(19,4),
   OrderTimes           NUMBER(9),
   CarryTime            DATE,
   constraint PK_SIHDAILYSALESAVERAGE primary key (Id)
)
/

/*==============================================================*/
/* Table: SIHRecommendPlan                                      */
/*==============================================================*/
create table SIHRecommendPlan  (
   Id                   NUMBER(9)                       not null,
   WarehouseId          NUMBER(9)                       not null,
   WarehouseName        VARCHAR2(100)                   not null,
   WarehouseCode        VARCHAR2(100)                   not null,
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50)                    not null,
   ReserveCoefficient   NUMBER(9),
   SparePartName        VARCHAR2(100)                   not null,
   SalesPrice           NUMBER(19,4),
   StandStockFee        NUMBER(19,4),
   UpperLimitCoefficient NUMBER(19,4),
   ReferenceCode        VARCHAR2(50),
   DirecCount           NUMBER(9),
   OrderTimes           NUMBER(9),
   PartsSupplierName    VARCHAR2(100),
   MaxOrderedQuantity   NUMBER(19,4),
   IsSalable            NUMBER(1),
   IsDirectSupply       NUMBER(1),
   IsOrderable          NUMBER(1),
   OrderGoodsCycle      NUMBER(9),
   LowerLimitcoefficient NUMBER(19,4),
   TemDays              NUMBER(9),
   NDaysAvg             NUMBER(19,4),
   PartABC              NUMBER(9),
   SafeDay              NUMBER(19,4),
   StandStock           NUMBER(19,4),
   UpperLimit           NUMBER(9),
   UpperLimitFee        NUMBER(19,4),
   LowerLimit           NUMBER(9),
   LowerLimitFee        NUMBER(19,4),
   ArriveCycle          NUMBER(9),
   AvailableStock       NUMBER(9),
   AvailableStockFee    NUMBER(19,4),
   OnWayNumber          NUMBER(9),
   OnWayNumberFee       NUMBER(19,4),
   PlanQty              NUMBER(9),
   RecommendQty         NUMBER(9),
   SalesPer             NUMBER(19,4),
   StockPer             NUMBER(19,4),
   CurrentShortager     NUMBER(9),
   Pack                 NUMBER(9),
   ActQty               NUMBER(9),
   ActQtyFee            NUMBER(19,4),
   CarryTime            DATE,
   SixAmount            NUMBER(19,4),
   FiveAmount           NUMBER(19,4),
   FourAmount           NUMBER(19,4),
   ThreeAmount          NUMBER(19,4),
   TwoAmount            NUMBER(19,4),
   OneAmount            NUMBER(19,4),
   constraint PK_SIHRECOMMENDPLAN primary key (Id)
)
/

/*==============================================================*/
/* Table: SIHSmartOrderBase                                     */
/*==============================================================*/
create table SIHSmartOrderBase  (
   Id                   NUMBER(9)                       not null,
   WarehouseId          NUMBER(9)                       not null,
   WarehouseName        VARCHAR2(100)                   not null,
   WarehouseCode        VARCHAR2(100)                   not null,
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50)                    not null,
   SparePartName        VARCHAR2(100)                   not null,
   PartABC              NUMBER(9),
   SalesPrice           NUMBER(19,4),
   ReferenceCode        VARCHAR2(50),
   OrderTimes           NUMBER(9),
   PartsSupplierName    VARCHAR2(100),
   IsSalable            NUMBER(1),
   IsDirectSupply       NUMBER(1),
   IsOrderable          NUMBER(1),
   MinBatch             NUMBER(9),
   MinPackingAmount     NUMBER(9),
   OrderGoodsCycle      NUMBER(9),
   OrderCycle           NUMBER(9),
   ArrivalCycle         NUMBER(9),
   SafeDay              NUMBER(19,4),
   StandStock           NUMBER(19,4),
   ReserveCoefficient   NUMBER(9),
   UpperLimitCoefficient NUMBER(19,4),
   lowerlimitcoefficient NUMBER(19,4),
   ArriveCycle          NUMBER(9),
   WarehousDays         NUMBER(9),
   TemDays              NUMBER(9),
   AvailableStock       NUMBER(9),
   OnWayNumber          NUMBER(9),
   CurrentShortager     NUMBER(9),
   pack                 NUMBER(9),
   LowerLimitDays       NUMBER(9),
   CarryTime            DATE,
   constraint PK_SIHSMARTORDERBASE primary key (Id)
)
/

/*==============================================================*/
/* Table: SalesOrderDailyAvg                                    */
/*==============================================================*/
create table SalesOrderDailyAvg  (
   Id                   NUMBER(9)                       not null,
   Year                 NUMBER(9),
   Week                 NUMBER(9),
   CenterId             NUMBER(9),
   CenterCode           VARCHAR2(50),
   CenterName           VARCHAR2(100),
   WarehouseId          NUMBER(9),
   WarehouseCode        VARCHAR2(50),
   WarehouseName        VARCHAR2(100),
   Type                 NUMBER(9),
   SafeStockDays        NUMBER(9),
   ReserveCoefficient   NUMBER(15,6),
   StockUpperCoefficient NUMBER(15,6),
   PurchasePrice        NUMBER(19,4),
   PackingQty           NUMBER(9),
   UsableQty            NUMBER(9),
   OnLineQty            NUMBER(9),
   OweQty               NUMBER(9),
   SparePartId          NUMBER(9),
   SparePartCode        VARCHAR2(50),
   SparePartName        VARCHAR2(100),
   SixMonthBefAvgQty    NUMBER(15,6),
   T3WeekAvgQty         NUMBER(15,6),
   T2WeekAvgQty         NUMBER(15,6),
   T1WeekAvgQty         NUMBER(15,6),
   TWeekAvgQty          NUMBER(15,6),
   TWeekActualAvgQty    NUMBER(15,6),
   StandardQty          NUMBER(15,6),
   CreateTime           DATE,
   OrderCycle           NUMBER(9),
   ArrivalCycle         NUMBER(9),
   LogisticsCycle       NUMBER(9),
   constraint PK_SALESORDERDAILYAVG primary key (Id)
)
/

/*==============================================================*/
/* Table: SalesOrderRecommendForce                              */
/*==============================================================*/
create table SalesOrderRecommendForce  (
   Id                   NUMBER(9)                       not null,
   Year                 NUMBER(9),
   CenterId             NUMBER(9),
   CenterCode           VARCHAR2(50),
   CenterName           VARCHAR2(100),
   Type                 NUMBER(9),
   ForceReserveQty      NUMBER(9),
   StockUpperQty        NUMBER(9),
   ActualStockUpperQty  NUMBER(9),
   PackingQty           NUMBER(9),
   UsableQty            NUMBER(9),
   OnLineQty            NUMBER(9),
   OweQty               NUMBER(9),
   SparePartId          NUMBER(9),
   SparePartCode        VARCHAR2(50),
   SparePartName        VARCHAR2(100),
   RecommendQty         NUMBER(15,6),
   CreateTime           DATE,
   constraint PK_SALESORDERRECOMMENDFORCE primary key (Id)
)
/

/*==============================================================*/
/* Table: SalesOrderRecommendWeekly                             */
/*==============================================================*/
create table SalesOrderRecommendWeekly  (
   Id                   NUMBER(9)                       not null,
   Year                 NUMBER(9),
   Week                 NUMBER(9),
   CenterId             NUMBER(9),
   CenterCode           VARCHAR2(50),
   CenterName           VARCHAR2(100),
   Type                 NUMBER(9),
   OrderCycle           NUMBER(9),
   ArrivalCycle         NUMBER(9),
   LogisticsCycle       NUMBER(9),
   ReserveCoefficient   NUMBER(15,6),
   StockUpperCoefficient NUMBER(15,6),
   PackingQty           NUMBER(9),
   UsableQty            NUMBER(9),
   OnLineQty            NUMBER(9),
   OweQty               NUMBER(9),
   SparePartId          NUMBER(9),
   SparePartCode        VARCHAR2(50),
   SparePartName        VARCHAR2(100),
   RecommendQty         NUMBER(15,6),
   CreateTime           DATE,
   constraint PK_SALESORDERRECOMMENDWEEKLY primary key (Id)
)
/

/*==============================================================*/
/* Table: SalesUnit                                             */
/*==============================================================*/
create table SalesUnit  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   Name                 VARCHAR2(100)                   not null,
   BusinessCode         VARCHAR2(50)                    not null,
   BusinessName         VARCHAR2(100)                   not null,
   BranchId             NUMBER(9)                       not null,
   OwnerCompanyId       NUMBER(9)                       not null,
   AccountGroupId       NUMBER(9)                       not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   SubmitCompanyId      NUMBER(9),
   constraint PK_SALESUNIT primary key (Id)
)
/

/*==============================================================*/
/* Table: SalesUnitAffiPersonnel                                */
/*==============================================================*/
create table SalesUnitAffiPersonnel  (
   Id                   NUMBER(9)                       not null,
   PersonnelId          NUMBER(9)                       not null,
   SalesUnitId          NUMBER(9)                       not null,
   constraint PK_SALESUNITAFFIPERSONNEL primary key (Id)
)
/

/*==============================================================*/
/* Table: SalesUnitAffiWarehouse                                */
/*==============================================================*/
create table SalesUnitAffiWarehouse  (
   Id                   NUMBER(9)                       not null,
   SalesUnitId          NUMBER(9)                       not null,
   WarehouseId          NUMBER(9)                       not null,
   constraint PK_SALESUNITAFFIWAREHOUSE primary key (Id)
)
/

/*==============================================================*/
/* Table: SalesUnitAffiWhouseHistory                            */
/*==============================================================*/
create table SalesUnitAffiWhouseHistory  (
   Id                   NUMBER(9)                       not null,
   SalesUnitId          NUMBER(9),
   WarehouseId          NUMBER(9),
   Status               NUMBER(9)                       not null,
   FileTime             DATE,
   FilerId              NUMBER(9),
   FilerName            VARCHAR2(100),
   constraint PK_SALESUNITAFFIWHOUSEHISTORY primary key (Id)
)
/

/*==============================================================*/
/* Table: SecondClassStationPlan                                */
/*==============================================================*/
create table SecondClassStationPlan  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   SecondClassStationId NUMBER(9)                       not null,
   SecondClassStationCode VARCHAR2(50)                    not null,
   SecondClassStationName VARCHAR2(100)                   not null,
   FirstClassStationId  NUMBER(9)                       not null,
   FirstClassStationCode VARCHAR2(50)                    not null,
   FirstClassStationName VARCHAR2(100)                   not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   TotalAmount          NUMBER(19,4),
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9)                       not null,
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   SubmitterId          NUMBER(9),
   SubmitterName        VARCHAR2(100),
   SubmitTime           DATE,
   ApproverId           NUMBER(9),
   ApproverName         VARCHAR2(100),
   ApproveTime          DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   constraint PK_SECONDCLASSSTATIONPLAN primary key (Id)
)
/

/*==============================================================*/
/* Table: SecondClassStationPlanDetail                          */
/*==============================================================*/
create table SecondClassStationPlanDetail  (
   Id                   NUMBER(9)                       not null,
   SecondClassStationPlanId NUMBER(9)                       not null,
   SparePartId          NUMBER(9)                       not null,
   SparePartName        VARCHAR2(100),
   SparePartCode        VARCHAR2(50),
   Price                NUMBER(19,4),
   Quantity             NUMBER(9),
   ProcessMode          NUMBER(9),
   Remark               VARCHAR2(200),
   constraint PK_SECONDCLASSSTATIONPLANDETAI primary key (Id)
)
/

/*==============================================================*/
/* Table: SmartOrderAbnormal                                    */
/*==============================================================*/
create table SmartOrderAbnormal  (
   Id                   NUMBER(9)                       not null,
   PartsSalesOrderId    NUMBER(9)                       not null,
   PartsSalesOrderCode  VARCHAR2(50)                    not null,
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50)                    not null,
   SparePartName        VARCHAR2(100)                   not null,
   Status               NUMBER(9)                       not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   Amount               NUMBER(9),
   constraint PK_SMARTORDERABNORMAL primary key (Id)
)
/

/*==============================================================*/
/* Table: SmartOrderAndType                                     */
/*==============================================================*/
create table SmartOrderAndType  (
   Id                   NUMBER(9)                       not null,
   WarehouseId          NUMBER(9)                       not null,
   WarehouseName        VARCHAR2(100)                   not null,
   WarehouseCode        VARCHAR2(100)                   not null,
   PartsSalesOrderTypeId NUMBER(9)                       not null,
   PartsSalesOrderTypeName VARCHAR2(100)                   not null,
   Status               NUMBER(9)                       not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_SMARTORDERANDTYPE primary key (Id)
)
/

/*==============================================================*/
/* Table: StationServiceReport                                  */
/*==============================================================*/
create table StationServiceReport  (
   Id                   NUMBER(9)                       not null,
   Year                 NUMBER(9)                       not null,
   Quarter              NUMBER(9)                       not null,
   MarketDepartmentId   NUMBER(9)                       not null,
   MarketDepartmentName VARCHAR2(100)                   not null,
   DealerId             NUMBER(9)                       not null,
   DealerCode           VARCHAR2(100)                   not null,
   DealerName           VARCHAR2(50)                    not null,
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50)                    not null,
   SparePartName        VARCHAR2(100)                   not null,
   TQuantity            NUMBER(9),
   FreGuar              NUMBER(9),
   AreaTQuantity        NUMBER(9),
   AreaFreGuar          NUMBER(9),
   Rank                 NUMBER(9),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   constraint PK_STATIONSERVICEREPORT primary key (Id)
)
/

/*==============================================================*/
/* Table: VehiclePartsHandleList                                */
/*==============================================================*/
create table VehiclePartsHandleList  (
   Id                   NUMBER(9)                       not null,
   VehiclePartsHandleOrderId NUMBER(9)                       not null,
   SparePartId          NUMBER(9),
   SparePartCode        VARCHAR2(50),
   SparePartName        VARCHAR2(100),
   OrderedQuantity      NUMBER(9),
   UnitPrice            NUMBER(19,4),
   OrderSum             NUMBER(19,4),
   Remark               VARCHAR2(200),
   constraint PK_VEHICLEPARTSHANDLELIST primary key (Id)
)
/

/*==============================================================*/
/* Table: VehiclePartsHandleOrder                               */
/*==============================================================*/
create table VehiclePartsHandleOrder  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   BranchId             NUMBER(9)                       not null,
   BranchName           VARCHAR2(50)                    not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   PartsSalesCategoryName VARCHAR2(50),
   ShippingCompanyId    NUMBER(9),
   ShippingCompanyCode  VARCHAR2(50),
   ShippingCompanyName  VARCHAR2(50),
   WarehouseId          NUMBER(9),
   WarehouseCode        VARCHAR2(50),
   WarehouseName        VARCHAR2(50),
   CounterpartCompanyId NUMBER(9),
   CounterpartCompanyCode VARCHAR2(50),
   CounterpartCompanyName VARCHAR2(100),
   OrderNumber          VARCHAR2(50),
   IfDirectProvision    NUMBER(9),
   IsBarter             NUMBER(9),
   Status               NUMBER(9),
   Remark               VARCHAR2(200),
   TotalAmount          NUMBER(19,4),
   PaymentBonusPoints   NUMBER(9),
   RequestedDeliveryTime DATE,
   SubmitTime           DATE,
   ReceiverName         VARCHAR2(50),
   ContactPhone         VARCHAR2(15),
   ReceivingAddress     VARCHAR2(200),
   PaymentType          NUMBER(9),
   InvoiceType          NUMBER(9),
   InvoiceTitle         VARCHAR2(500),
   CreateTime           DATE,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   HandleId             NUMBER(9),
   HandleName           VARCHAR2(50),
   HandleTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   AbandonComment       VARCHAR2(500),
   RowVersion           TIMESTAMP,
   ActualFreight        NUMBER(19,4),
   Province             VARCHAR2(100),
   City                 VARCHAR2(100),
   BankAccount          VARCHAR2(200),
   Taxpayeridentification VARCHAR2(50),
   InvoiceAdress        VARCHAR2(100),
   InvoicePhone         VARCHAR2(50),
   EnterpriseType       NUMBER(9),
   ReceivingMailbox     VARCHAR2(100),
   constraint PK_VEHICLEPARTSHANDLEORDER primary key (Id)
)
/

alter table BonusPointsOrderList
   add constraint FK_BONUSPOI_RELATIONS_BONUSPOI foreign key (BonusPointsOrderId)
      references BonusPointsOrder (Id)
/

alter table ERPDeliveryDetail
   add constraint FK_ERPDELIV_RELATIONS_ERPDELIV foreign key (Id)
      references ERPDelivery (Id)
/

