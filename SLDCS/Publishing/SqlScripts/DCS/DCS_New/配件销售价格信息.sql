/*==============================================================*/
/* DBMS name:      ORACLE Version 10gR2                         */
/* Created on:     2022/4/18 10:59:03                           */
/*==============================================================*/


declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('AutoSalesOrderProcess');
  if num>0 then
    execute immediate 'drop table AutoSalesOrderProcess cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('CustomerOrderPriceGrade');
  if num>0 then
    execute immediate 'drop table CustomerOrderPriceGrade cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('CustomerOrderPriceGradeHistory');
  if num>0 then
    execute immediate 'drop table CustomerOrderPriceGradeHistory cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('DealerPartsSalesPrice');
  if num>0 then
    execute immediate 'drop table DealerPartsSalesPrice cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('DealerPartsSalesPriceHistory');
  if num>0 then
    execute immediate 'drop table DealerPartsSalesPriceHistory cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('IvecoPriceChangeApp');
  if num>0 then
    execute immediate 'drop table IvecoPriceChangeApp cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('IvecoPriceChangeAppDetail');
  if num>0 then
    execute immediate 'drop table IvecoPriceChangeAppDetail cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('IvecoSalesPrice');
  if num>0 then
    execute immediate 'drop table IvecoSalesPrice cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('OrderApproveWeekday');
  if num>0 then
    execute immediate 'drop table OrderApproveWeekday cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PMSDMSBRANDMAPPING');
  if num>0 then
    execute immediate 'drop table PMSDMSBRANDMAPPING cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PMSEPCBRANDMAPPING');
  if num>0 then
    execute immediate 'drop table PMSEPCBRANDMAPPING cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsRetailGuidePrice');
  if num>0 then
    execute immediate 'drop table PartsRetailGuidePrice cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsRetailGuidePriceHistory');
  if num>0 then
    execute immediate 'drop table PartsRetailGuidePriceHistory cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsSalePriceIncreaseRate');
  if num>0 then
    execute immediate 'drop table PartsSalePriceIncreaseRate cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsSalesCategory');
  if num>0 then
    execute immediate 'drop table PartsSalesCategory cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsSalesOrderType');
  if num>0 then
    execute immediate 'drop table PartsSalesOrderType cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsSalesPrice');
  if num>0 then
    execute immediate 'drop table PartsSalesPrice cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsSalesPriceChange');
  if num>0 then
    execute immediate 'drop table PartsSalesPriceChange cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsSalesPriceChangeDetail');
  if num>0 then
    execute immediate 'drop table PartsSalesPriceChangeDetail cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsSalesPriceHistory');
  if num>0 then
    execute immediate 'drop table PartsSalesPriceHistory cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsSpecialPriceHistory');
  if num>0 then
    execute immediate 'drop table PartsSpecialPriceHistory cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PartsSpecialTreatyPrice');
  if num>0 then
    execute immediate 'drop table PartsSpecialTreatyPrice cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('SpecialDiscount');
  if num>0 then
    execute immediate 'drop table SpecialDiscount cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('SpecialPriceChangeList');
  if num>0 then
    execute immediate 'drop table SpecialPriceChangeList cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('SpecialTreatyPriceChange');
  if num>0 then
    execute immediate 'drop table SpecialTreatyPriceChange cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('TCOMPANYMAP');
  if num>0 then
    execute immediate 'drop table TCOMPANYMAP cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('WarehouseSequence');
  if num>0 then
    execute immediate 'drop table WarehouseSequence cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('ZRUN_fUNCTION');
  if num>0 then
    execute immediate 'drop table ZRUN_fUNCTION cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('ZRUN_lOG_DETAIL');
  if num>0 then
    execute immediate 'drop table ZRUN_lOG_DETAIL cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('ZRUN_lOG_SUM');
  if num>0 then
    execute immediate 'drop table ZRUN_lOG_SUM cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('ZRUN_mODULE');
  if num>0 then
    execute immediate 'drop table ZRUN_mODULE cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_AutoSalesOrderProcess');
  if num>0 then
    execute immediate 'drop sequence S_AutoSalesOrderProcess';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_CustomerOrderPriceGrade');
  if num>0 then
    execute immediate 'drop sequence S_CustomerOrderPriceGrade';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_CustomerOrderPriceGradeHis');
  if num>0 then
    execute immediate 'drop sequence S_CustomerOrderPriceGradeHis';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_DealerPartsSalesPrice');
  if num>0 then
    execute immediate 'drop sequence S_DealerPartsSalesPrice';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_DealerPartsSalesPriceHistory');
  if num>0 then
    execute immediate 'drop sequence S_DealerPartsSalesPriceHistory';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_IvecoPriceChangeApp');
  if num>0 then
    execute immediate 'drop sequence S_IvecoPriceChangeApp';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_IvecoPriceChangeAppDetail');
  if num>0 then
    execute immediate 'drop sequence S_IvecoPriceChangeAppDetail';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_IvecoSalesPrice');
  if num>0 then
    execute immediate 'drop sequence S_IvecoSalesPrice';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_OrderApproveWeekday');
  if num>0 then
    execute immediate 'drop sequence S_OrderApproveWeekday';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PMSDMSBRANDMAPPING');
  if num>0 then
    execute immediate 'drop sequence S_PMSDMSBRANDMAPPING';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PMSEPCBRANDMAPPING');
  if num>0 then
    execute immediate 'drop sequence S_PMSEPCBRANDMAPPING';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsRetailGuidePrice');
  if num>0 then
    execute immediate 'drop sequence S_PartsRetailGuidePrice';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsRetailGuidePriceHistory');
  if num>0 then
    execute immediate 'drop sequence S_PartsRetailGuidePriceHistory';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsSalePriceIncreaseRate');
  if num>0 then
    execute immediate 'drop sequence S_PartsSalePriceIncreaseRate';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsSalesCategory');
  if num>0 then
    execute immediate 'drop sequence S_PartsSalesCategory';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsSalesOrderType');
  if num>0 then
    execute immediate 'drop sequence S_PartsSalesOrderType';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsSalesPrice');
  if num>0 then
    execute immediate 'drop sequence S_PartsSalesPrice';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsSalesPriceChange');
  if num>0 then
    execute immediate 'drop sequence S_PartsSalesPriceChange';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsSalesPriceChangeDetail');
  if num>0 then
    execute immediate 'drop sequence S_PartsSalesPriceChangeDetail';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsSalesPriceHistory');
  if num>0 then
    execute immediate 'drop sequence S_PartsSalesPriceHistory';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsSpecialPriceHistory');
  if num>0 then
    execute immediate 'drop sequence S_PartsSpecialPriceHistory';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PartsSpecialTreatyPrice');
  if num>0 then
    execute immediate 'drop sequence S_PartsSpecialTreatyPrice';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_SpecialDiscount');
  if num>0 then
    execute immediate 'drop sequence S_SpecialDiscount';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_SpecialPriceChangeList');
  if num>0 then
    execute immediate 'drop sequence S_SpecialPriceChangeList';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_SpecialTreatyPriceChange');
  if num>0 then
    execute immediate 'drop sequence S_SpecialTreatyPriceChange';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_WarehouseSequence');
  if num>0 then
    execute immediate 'drop sequence S_WarehouseSequence';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_ZRUN_fUNCTION');
  if num>0 then
    execute immediate 'drop sequence S_ZRUN_fUNCTION';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_ZRUN_lOG_DETAIL');
  if num>0 then
    execute immediate 'drop sequence S_ZRUN_lOG_DETAIL';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_ZRUN_lOG_SUM');
  if num>0 then
    execute immediate 'drop sequence S_ZRUN_lOG_SUM';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_ZRUN_mODULE');
  if num>0 then
    execute immediate 'drop sequence S_ZRUN_mODULE';
  end if;
end;
/

create sequence S_AutoSalesOrderProcess
/

create sequence S_CustomerOrderPriceGrade
/

create sequence S_CustomerOrderPriceGradeHis
/

create sequence S_DealerPartsSalesPrice
/

create sequence S_DealerPartsSalesPriceHistory
/

create sequence S_IvecoPriceChangeApp
/

create sequence S_IvecoPriceChangeAppDetail
/

create sequence S_IvecoSalesPrice
/

create sequence S_OrderApproveWeekday
/

create sequence S_PMSDMSBRANDMAPPING
/

create sequence S_PMSEPCBRANDMAPPING
/

create sequence S_PartsRetailGuidePrice
/

create sequence S_PartsRetailGuidePriceHistory
/

create sequence S_PartsSalePriceIncreaseRate
/

create sequence S_PartsSalesCategory
/

create sequence S_PartsSalesOrderType
/

create sequence S_PartsSalesPrice
/

create sequence S_PartsSalesPriceChange
/

create sequence S_PartsSalesPriceChangeDetail
/

create sequence S_PartsSalesPriceHistory
/

create sequence S_PartsSpecialPriceHistory
/

create sequence S_PartsSpecialTreatyPrice
/

create sequence S_SpecialDiscount
/

create sequence S_SpecialPriceChangeList
/

create sequence S_SpecialTreatyPriceChange
/

create sequence S_WarehouseSequence
/

create sequence S_ZRUN_fUNCTION
/

create sequence S_ZRUN_lOG_DETAIL
/

create sequence S_ZRUN_lOG_SUM
/

create sequence S_ZRUN_mODULE
/

/*==============================================================*/
/* Table: AutoSalesOrderProcess                                 */
/*==============================================================*/
create table AutoSalesOrderProcess  (
   Id                   NUMBER(9)                       not null,
   OrderId              NUMBER(9),
   Code                 VARCHAR2(50),
   Status               NUMBER(9),
   constraint PK_AUTOSALESORDERPROCESS primary key (Id)
)
/

/*==============================================================*/
/* Table: CustomerOrderPriceGrade                               */
/*==============================================================*/
create table CustomerOrderPriceGrade  (
   Id                   NUMBER(9)                       not null,
   BranchId             NUMBER(9)                       not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   CustomerCompanyId    NUMBER(9)                       not null,
   CustomerCompanyCode  VARCHAR2(50)                    not null,
   CustomerCompanyName  VARCHAR2(100)                   not null,
   PartsSalesOrderTypeId NUMBER(9)                       not null,
   PartsSalesOrderTypeCode VARCHAR2(50)                    not null,
   PartsSalesOrderTypeName VARCHAR2(100)                   not null,
   Coefficient          NUMBER(15,6)                    not null,
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   RowVersion           TIMESTAMP,
   constraint PK_CUSTOMERORDERPRICEGRADE primary key (Id)
)
/

/*==============================================================*/
/* Table: CustomerOrderPriceGradeHistory                        */
/*==============================================================*/
create table CustomerOrderPriceGradeHistory  (
   Id                   NUMBER(9)                       not null,
   BranchId             NUMBER(9)                       not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   CustomerCompanyId    NUMBER(9)                       not null,
   CustomerCompanyCode  VARCHAR2(50)                    not null,
   CustomerCompanyName  VARCHAR2(100)                   not null,
   PartsSalesOrderTypeId NUMBER(9)                       not null,
   PartsSalesOrderTypeCode VARCHAR2(50)                    not null,
   PartsSalesOrderTypeName VARCHAR2(100)                   not null,
   Coefficient          NUMBER(15,6)                    not null,
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_CUSTOMERORDERPRICEGRADEHIS primary key (Id)
)
/

/*==============================================================*/
/* Table: DealerPartsSalesPrice                                 */
/*==============================================================*/
create table DealerPartsSalesPrice  (
   Id                   NUMBER(9)                       not null,
   BranchId             NUMBER(9)                       not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   PartsSalesCategoryCode VARCHAR2(50)                    not null,
   PartsSalesCategoryName VARCHAR2(100)                   not null,
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50)                    not null,
   SparePartName        VARCHAR2(100)                   not null,
   DealerSalesPrice     NUMBER(19,4)                    not null,
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   ApproverId           NUMBER(9),
   ApproverName         VARCHAR2(100),
   ApproveTime          DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   RowVersion           TIMESTAMP,
   constraint PK_DEALERPARTSSALESPRICE primary key (Id)
)
/

/*==============================================================*/
/* Table: DealerPartsSalesPriceHistory                          */
/*==============================================================*/
create table DealerPartsSalesPriceHistory  (
   Id                   NUMBER(9)                       not null,
   DealerPartsSalesPriceId NUMBER(9),
   BranchId             NUMBER(9)                       not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   PartsSalesCategoryCode VARCHAR2(50)                    not null,
   PartsSalesCategoryName VARCHAR2(100)                   not null,
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50)                    not null,
   SparePartName        VARCHAR2(100)                   not null,
   DealerSalesPrice     NUMBER(19,4)                    not null,
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   constraint PK_DEALERPARTSSALESPRICEHISTOR primary key (Id)
)
/

/*==============================================================*/
/* Table: IvecoPriceChangeApp                                   */
/*==============================================================*/
create table IvecoPriceChangeApp  (
   Id                   NUMBER(9)                       not null,
   PartsSalesCategoryId NUMBER(9),
   Code                 VARCHAR2(50),
   PriceType            NUMBER(9),
   Status               NUMBER(9)                       not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   InitialApproverId    NUMBER(9),
   InitialApproverName  VARCHAR2(100),
   InitialApproveTime   DATE,
   FinalApproverId      NUMBER(9),
   FinalApproverName    VARCHAR2(100),
   FinalApproveTime     DATE,
   constraint PK_IVECOPRICECHANGEAPP primary key (Id)
)
/

/*==============================================================*/
/* Table: IvecoPriceChangeAppDetail                             */
/*==============================================================*/
create table IvecoPriceChangeAppDetail  (
   Id                   NUMBER(9)                       not null,
   IvecoPriceChangeAppId NUMBER(9)                       not null,
   SparePartId          NUMBER(9),
   SparePartCode        VARCHAR2(50),
   SparePartName        VARCHAR2(100),
   IvecoPrice           NUMBER(19,4),
   constraint PK_IVECOPRICECHANGEAPPDETAIL primary key (Id)
)
/

/*==============================================================*/
/* Table: IvecoSalesPrice                                       */
/*==============================================================*/
create table IvecoSalesPrice  (
   Id                   NUMBER(9)                       not null,
   PartsSalesCategoryId NUMBER(9),
   PartsSalesCategoryName VARCHAR2(50),
   SparePartId          NUMBER(9),
   SparePartCode        VARCHAR2(50),
   SparePartName        VARCHAR2(100),
   SalesPrice           NUMBER(19,4),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   constraint PK_IVECOSALESPRICE primary key (Id)
)
/

/*==============================================================*/
/* Table: OrderApproveWeekday                                   */
/*==============================================================*/
create table OrderApproveWeekday  (
   Id                   NUMBER(9)                       not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   PartsSalesCategoryName VARCHAR2(100)                   not null,
   ProvinceId           NUMBER(9)                       not null,
   ProvinceName         VARCHAR2(100)                   not null,
   WeeksName            NUMBER(9),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   Status               NUMBER(9),
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   RowVersion           TIMESTAMP,
   constraint PK_ORDERAPPROVEWEEKDAY primary key (Id)
)
/

/*==============================================================*/
/* Table: PMSDMSBRANDMAPPING                                    */
/*==============================================================*/
create table PMSDMSBRANDMAPPING  (
   Id                   NUMBER(9)                       not null,
   PMSBrandId           NUMBER(9),
   PMSBrandCode         VARCHAR2(50),
   PMSBrandName         VARCHAR2(50),
   DMSBrandId           VARCHAR2(50)                    not null,
   DMSBrandCode         VARCHAR2(50)                    not null,
   DMSBrandName         VARCHAR2(50)                    not null,
   Status               NUMBER(9)                       not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(50),
   Createtime           DATE,
   ModifyId             NUMBER(9),
   ModifyName           VARCHAR2(50),
   ModifyTime           DATE,
   Syscode              NUMBER(9)                       not null,
   constraint PK_PMSDMSBRANDMAPPING primary key (Id)
)
/

/*==============================================================*/
/* Table: PMSEPCBRANDMAPPING                                    */
/*==============================================================*/
create table PMSEPCBRANDMAPPING  (
   Id                   NUMBER(9)                       not null,
   PMSBrandId           NUMBER(9)                       not null,
   PMSBrandCode         VARCHAR2(50)                    not null,
   PMSBrandName         VARCHAR2(50)                    not null,
   EPCBrandId           VARCHAR2(50)                    not null,
   EPCBrandCode         VARCHAR2(50)                    not null,
   EPCBrandName         VARCHAR2(50)                    not null,
   Status               NUMBER(9)                       not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(50),
   Createtime           DATE,
   ModifyId             NUMBER(9),
   ModifyName           VARCHAR2(50),
   ModifyTime           DATE,
   Syscode              NUMBER(9)                       not null,
   constraint PK_PMSEPCBRANDMAPPING primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsRetailGuidePrice                                 */
/*==============================================================*/
create table PartsRetailGuidePrice  (
   Id                   NUMBER(9)                       not null,
   BranchId             NUMBER(9)                       not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   PartsSalesCategoryCode VARCHAR2(50)                    not null,
   PartsSalesCategoryName VARCHAR2(100)                   not null,
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50)                    not null,
   SparePartName        VARCHAR2(100)                   not null,
   RetailGuidePrice     NUMBER(19,4)                    not null,
   ValidationTime       DATE,
   ExpireTime           DATE,
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   RowVersion           TIMESTAMP,
   constraint PK_PARTSRETAILGUIDEPRICE primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsRetailGuidePriceHistory                          */
/*==============================================================*/
create table PartsRetailGuidePriceHistory  (
   Id                   NUMBER(9)                       not null,
   BranchId             NUMBER(9)                       not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   PartsSalesCategoryCode VARCHAR2(50)                    not null,
   PartsSalesCategoryName VARCHAR2(100)                   not null,
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50)                    not null,
   SparePartName        VARCHAR2(100)                   not null,
   RetailGuidePrice     NUMBER(19,4)                    not null,
   ValidationTime       DATE,
   ExpireTime           DATE,
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_PARTSRETAILGUIDEPRICEHIS primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsSalePriceIncreaseRate                            */
/*==============================================================*/
create table PartsSalePriceIncreaseRate  (
   Id                   NUMBER(9)                       not null,
   GroupCode            VARCHAR2(50),
   GroupName            VARCHAR2(100),
   IncreaseRate         NUMBER(15,6),
   IsDefault            NUMBER(1),
   status               NUMBER(9),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   CreateTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   constraint PK_PARTSSALEPRICEINCREASERATE primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsSalesCategory                                    */
/*==============================================================*/
create table PartsSalesCategory  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   Name                 VARCHAR2(100)                   not null,
   BranchId             NUMBER(9)                       not null,
   BranchCode           VARCHAR2(50)                    not null,
   BranchName           VARCHAR2(100)                   not null,
   IsForClaim           NUMBER(1)                       not null,
   IsNotWarranty        NUMBER(1)                       not null,
   IsOverseas           NUMBER(1),
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   RowVersion           TIMESTAMP,
   constraint PK_PARTSSALESCATEGORY primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsSalesOrderType                                   */
/*==============================================================*/
create table PartsSalesOrderType  (
   Id                   NUMBER(9)                       not null,
   BranchId             NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   Name                 VARCHAR2(100)                   not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   RowVersion           TIMESTAMP,
   SettleType           NUMBER(9),
   BusinessType         NUMBER(9),
   BusinessCode         VARCHAR2(10),
   IsSmart              NUMBER(1),
   constraint PK_PARTSSALESORDERTYPE primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsSalesPrice                                       */
/*==============================================================*/
create table PartsSalesPrice  (
   Id                   NUMBER(9)                       not null,
   BranchId             NUMBER(9)                       not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   PartsSalesCategoryCode VARCHAR2(50)                    not null,
   PartsSalesCategoryName VARCHAR2(100)                   not null,
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50)                    not null,
   SparePartName        VARCHAR2(100)                   not null,
   IfClaim              NUMBER(1),
   SalesPrice           NUMBER(19,4)                    not null,
   PriceType            NUMBER(9)                       not null,
   Status               NUMBER(9)                       not null,
   ValidationTime       DATE,
   ExpireTime           DATE,
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   ApproverId           NUMBER(9),
   ApproverName         VARCHAR2(100),
   ApproveTime          DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   RowVersion           TIMESTAMP,
   CenterPrice          NUMBER(19,4),
   constraint PK_PARTSSALESPRICE primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsSalesPriceChange                                 */
/*==============================================================*/
create table PartsSalesPriceChange  (
   Id                   NUMBER(9)                       not null,
   BranchId             NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   PartsSalesCategoryCode VARCHAR2(50)                    not null,
   PartsSalesCategoryName VARCHAR2(100)                   not null,
   AbandonComment       VARCHAR2(200),
   IfClaim              NUMBER(1),
   CreatorId            NUMBER(9)                       not null,
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   InitialApproverId    NUMBER(9),
   InitialApproverName  VARCHAR2(100),
   InitialApproveTime   DATE,
   InitialApproveComment VARCHAR2(200),
   ApproverId           NUMBER(9),
   ApproverName         VARCHAR2(100),
   ApproveTime          DATE,
   ApproveComment       VARCHAR2(200),
   FinalApproverId      NUMBER(9),
   FinalApproverName    VARCHAR2(100),
   FinalApproveTime     DATE,
   FinalApproveComment  VARCHAR2(200),
   RejectId             NUMBER(9),
   Rejecter             VARCHAR2(100),
   RejectTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   RowVersion           TIMESTAMP,
   RejectOpinion        VARCHAR2(200),
   Path                 VARCHAR2(2000),
   ValidatDate          DATE,
   IsTurnPrice          NUMBER(1),
   IsEffective          NUMBER(1),
   constraint PK_PARTSSALESPRICECHANGE primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsSalesPriceChangeDetail                           */
/*==============================================================*/
create table PartsSalesPriceChangeDetail  (
   Id                   NUMBER(9)                       not null,
   ParentId             NUMBER(9)                       not null,
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50)                    not null,
   SparePartName        VARCHAR2(100)                   not null,
   SalesPrice           NUMBER(19,4)                    not null,
   RetailGuidePrice     NUMBER(19,4),
   DealerSalesPrice     NUMBER(19,4),
   PriceType            NUMBER(9)                       not null,
   ValidationTime       DATE,
   ExpireTime           DATE,
   Remark               VARCHAR2(200),
   RowVersion           TIMESTAMP,
   CategoryCode         VARCHAR2(100),
   CategoryName         VARCHAR2(100),
   SalesPriceFluctuationRatio NUMBER(15,6),
   RetailPriceFluctuationRatio NUMBER(15,6),
   MaxSalesPriceFloating NUMBER(15,6),
   MinSalesPriceFloating NUMBER(15,6),
   MaxRetailOrderPriceFloating NUMBER(15,6),
   MinRetailOrderPriceFloating NUMBER(15,6),
   MaxPurchasePricing   NUMBER(19,4),
   IsUpsideDown         NUMBER(9),
   MaxExchangeSalePrice NUMBER(19,4),
   CenterPrice          NUMBER(19,4),
   CenterPriceRatio     NUMBER(15,6),
   PriceTypeName        VARCHAR2(50),
   constraint PK_PARTSSALESPRICECHANGEDETAIL primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsSalesPriceHistory                                */
/*==============================================================*/
create table PartsSalesPriceHistory  (
   Id                   NUMBER(9)                       not null,
   BranchId             NUMBER(9)                       not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   PartsSalesCategoryCode VARCHAR2(50)                    not null,
   PartsSalesCategoryName VARCHAR2(100)                   not null,
   IfClaim              NUMBER(1),
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50)                    not null,
   SparePartName        VARCHAR2(100)                   not null,
   SalesPrice           NUMBER(19,4)                    not null,
   PriceType            NUMBER(9)                       not null,
   ValidationTime       DATE,
   ExpireTime           DATE,
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   RowVersion           TIMESTAMP,
   CenterPrice          NUMBER(19,4),
   RetailGuidePrice     NUMBER(19,4),
   PriceTypeName        VARCHAR2(100),
   OldCenterPrice       NUMBER(19,4),
   OldRetailGuidePrice  NUMBER(19,4),
   OldSalesPrice        NUMBER(19,4),
   constraint PK_PARTSSALESPRICEHISTORY primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsSpecialPriceHistory                              */
/*==============================================================*/
create table PartsSpecialPriceHistory  (
   Id                   NUMBER(9)                       not null,
   PartsSpecialTreatyPriceId NUMBER(9),
   BranchId             NUMBER(9)                       not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   PartsSalesCategoryCode VARCHAR2(50)                    not null,
   PartsSalesCategoryName VARCHAR2(100)                   not null,
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50)                    not null,
   SparePartName        VARCHAR2(100)                   not null,
   CompanyId            NUMBER(9)                       not null,
   TreatyPrice          NUMBER(19,4)                    not null,
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   constraint PK_PARTSSPECIALPRICEHISTORY primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsSpecialTreatyPrice                               */
/*==============================================================*/
create table PartsSpecialTreatyPrice  (
   Id                   NUMBER(9)                       not null,
   BranchId             NUMBER(9)                       not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   PartsSalesCategoryCode VARCHAR2(50)                    not null,
   PartsSalesCategoryName VARCHAR2(100)                   not null,
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50)                    not null,
   SparePartName        VARCHAR2(100)                   not null,
   CompanyId            NUMBER(9)                       not null,
   TreatyPrice          NUMBER(19,4)                    not null,
   ValidationTime       DATE,
   ExpireTime           DATE,
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   RowVersion           TIMESTAMP,
   Path                 VARCHAR2(2000),
   constraint PK_PARTSSPECIALTREATYPRICE primary key (Id)
)
/

/*==============================================================*/
/* Table: SpecialDiscount                                       */
/*==============================================================*/
create table SpecialDiscount  (
   Id                   NUMBER(9)                       not null,
   Discount             NUMBER(19,4),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   constraint PK_SPECIALDISCOUNT primary key (Id)
)
/

/*==============================================================*/
/* Table: SpecialPriceChangeList                                */
/*==============================================================*/
create table SpecialPriceChangeList  (
   Id                   NUMBER(9)                       not null,
   SpecialTreatyPriceChangeId NUMBER(9),
   SparePartId          NUMBER(9),
   SparePartCode        VARCHAR2(50),
   SparePartName        VARCHAR2(100),
   SpecialTreatyPrice   NUMBER(19,4),
   Remark               VARCHAR2(200),
   RetailPrice          NUMBER(19,4),
   Ratio                NUMBER(15,6),
   constraint PK_SPECIALPRICECHANGELIST primary key (Id)
)
/

/*==============================================================*/
/* Table: SpecialTreatyPriceChange                              */
/*==============================================================*/
create table SpecialTreatyPriceChange  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(100),
   BrandId              NUMBER(9),
   BrandName            VARCHAR2(100),
   CorporationId        NUMBER(9),
   CorporationCode      VARCHAR2(100),
   CorporationName      VARCHAR2(100),
   Status               NUMBER(9),
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   InitialApproverId    NUMBER(9),
   InitialApproverName  VARCHAR2(100),
   InitialApproveTime   DATE,
   InitialApproverComment VARCHAR2(200),
   ApproverId           NUMBER(9),
   ApproverName         VARCHAR2(100),
   ApproveTime          DATE,
   ApproveComment       VARCHAR2(200),
   CheckerId            NUMBER(9),
   CheckerName          VARCHAR2(100),
   CheckerTime          DATE,
   CheckerComment       VARCHAR2(200),
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonerTime        DATE,
   ValidationTime       DATE,
   ExpireTime           DATE,
   Path                 VARCHAR2(2000),
   constraint PK_SPECIALTREATYPRICECHANGE primary key (Id)
)
/

/*==============================================================*/
/* Table: TCOMPANYMAP                                           */
/*==============================================================*/
create table TCOMPANYMAP  (
   SYSCODE              VARCHAR2(30)                    not null,
   COMPANYCODE          VARCHAR2(30)                    not null,
   DMSBRANDCODE         VARCHAR2(30)                    not null,
   DMSBRANDNAME         VARCHAR2(100)                   not null,
   FWBRANDCODE          VARCHAR2(30)                    not null,
   FWRANDNAME           VARCHAR2(100)                   not null,
   CREATETIME           DATE                            not null,
   MODIFYTIME           DATE,
   STATUS               NUMBER(9)                       not null
)
/

/*==============================================================*/
/* Table: WarehouseSequence                                     */
/*==============================================================*/
create table WarehouseSequence  (
   Id                   NUMBER(9)                       not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   PartsSalesCategoryName VARCHAR2(100)                   not null,
   CustomerCompanyId    NUMBER(9)                       not null,
   CustomerCompanyCode  VARCHAR2(50)                    not null,
   CustomerCompanyName  VARCHAR2(100)                   not null,
   IsAutoApprove        NUMBER(1)                       not null,
   DefaultWarehouseId   NUMBER(9),
   DefaultWarehouseName VARCHAR2(100),
   DefaultOutWarehouseId NUMBER(9),
   DefaultOutWarehouseName VARCHAR2(100),
   FirstWarehouseId     NUMBER(9),
   FirstWarehouseName   VARCHAR2(100),
   FirstOutWarehouseId  NUMBER(9),
   FirstOutWarehouseName VARCHAR2(100),
   SecoundWarehouseId   NUMBER(9),
   SecoundWarehouseName VARCHAR2(100),
   SecoundOutWarehouseId NUMBER(9),
   SecoundOutWarehouseName VARCHAR2(100),
   ThirdWarehouseId     NUMBER(9),
   ThirdWarehouseName   VARCHAR2(100),
   ThirdOutWarehouseId  NUMBER(9),
   ThirdOutWarehouseName VARCHAR2(100),
   Status               NUMBER(9)                       not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   RowVersion           TIMESTAMP
)
/

/*==============================================================*/
/* Table: ZRUN_fUNCTION                                         */
/*==============================================================*/
create table ZRUN_fUNCTION  (
   Id                   NUMBER(9)                       not null,
   IdF                  VARCHAR2(10)                    not null,
   DEScF                VARCHAR2(50),
   constraint PK_ZRUN_FUNCTION primary key (Id)
)
/

/*==============================================================*/
/* Table: ZRUN_lOG_DETAIL                                       */
/*==============================================================*/
create table ZRUN_lOG_DETAIL  (
   Id                   NUMBER(9)                       not null,
   IdM                  VARCHAR2(10)                    not null,
   IdF                  VARCHAR2(10)                    not null,
   UnAME                VARCHAR2(50)                    not null,
   UNAMEId              NUMBER(9),
   STATISTICALdATE      DATE                            not null,
   constraint PK_ZRUN_LOG_DETAIL primary key (Id)
)
/

/*==============================================================*/
/* Table: ZRUN_lOG_SUM                                          */
/*==============================================================*/
create table ZRUN_lOG_SUM  (
   Id                   NUMBER(9)                       not null,
   IdM                  VARCHAR2(10)                    not null,
   IdF                  VARCHAR2(10)                    not null,
   UnAME                VARCHAR2(50)                    not null,
   UNAMEId              NUMBER(9),
   STATISTICALdATE      DATE                            not null,
   Num                  NUMBER(9),
   constraint PK_ZRUN_LOG_SUM primary key (Id)
)
/

/*==============================================================*/
/* Table: ZRUN_mODULE                                           */
/*==============================================================*/
create table ZRUN_mODULE  (
   Id                   NUMBER(9)                       not null,
   IdM                  VARCHAR2(10)                    not null,
   DEScM                VARCHAR2(50),
   constraint PK_ZRUN_MODULE primary key (Id)
)
/

