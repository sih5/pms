﻿INSERT INTO Page(Id,ParentId,Type,PageId,PageType,Parameter,Name,Description,Icon,Sequence,Status) VALUES (3206,3200,2,'PartsPurchaseSettleBillCIF','Released',NULL,'配件采购结算管理(福田总部)','查询、维护配件采购结算单','Client/DCS/Images/Menu/PartsPurchasing/PartsPurchaseSettleBill.png', 5,2);   

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3206, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3206, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3206, 'Common|Approve', '审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3206, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3206, 'PartsPurchaseSettleBill|InvoiceRegister', '登记发票', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3206, 'PartsPurchaseSettleBill|InvoiceEdit', '发票修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3206, 'PartsPurchaseSettleBill|InvoiceApprove', '发票审核', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3206, 'PartsPurchaseSettleBill|AntiSettlement', '反结算', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3206, 'PartsPurchaseSettleBill|PrintForGC', '工程车打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3206, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3206, 'Common|MergeExport', '合并导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3206, 'Common|Print', '打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3206, 'PartsPurchaseSettleBill|InvoiceSettlement', '发票已审核反结算', 2);

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (4509,4500, 2, 'PartsSalesSettlementCIF', 'Released', NULL, '配件销售结算管理(福田总部)', '查询、维护配件销售结算单', 'Client/DCS/Images/Menu/PartsSales/PartsSalesSettlement.png', 9, 2);


insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4509, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4509, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4509, 'Common|Approve', '审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4509, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4509, 'Common|InvoiceRegister', '登记发票', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4509, 'Common|AntiSettlement', '反结算', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4509, 'Common|AppoveRebate', '审批（返利）', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4509, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4509, 'Common|MergeExport', '合并导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4509, 'Common|Print', '打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4509, 'Common|SettlementCost', '查看结算单成本', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4509, 'Common|PrintInvoice', '发票清单打印', 2);



-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);
   
-- 确保所有按钮，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 1, Id, 2 
  FROM Action 
 WHERE Status = 2
   and not exists (select * from node where node.categoryid = action.Id and CategoryType=1)
   and exists (select * from page
                WHERE Type = 2 AND Status = 2 
                  and page.id= action.Pageid);