﻿INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5314,5300, 2, 'PackingTask', 'Released', NULL, '包装管理', '根据包装任务单包装登记', 'Client/DCS/Images/Menu/PartsStocking/PartsPacking.png', 11, 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval,5314,'Common|MergeExport', '合并导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval,5314,'PackingTask|Packing', '包装登记', 2);

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5316, 5300, 2, 'PartsShelvesTask', 'Released', NULL, '上架管理', '配件公司根据上架任务办理上架', 'Client/DCS/Images/Menu/PartsStocking/PartsShelves.png', 12, 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS) values (S_Action.Nextval, 5316,'PartsShelvesTask|Shelves', '上架', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS) values (S_Action.Nextval, 5316,'Common|Print', '打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS) values (S_Action.Nextval, 5316,'Common|MergeExport', '合并导出', 2);

-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);
   
-- 确保所有按钮，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 1, Id, 2 
  FROM Action 
 WHERE Status = 2
   and not exists (select * from node where node.categoryid = action.Id and CategoryType=1)
   and exists (select * from page
                WHERE Type = 2 AND Status = 2 
                  and page.id= action.Pageid);