-- 收货管理节点权限
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) 
VALUES (5315, 5300, 2, 'ReceiptManagement', 'Released', NULL, '收货管理', '配件公司检索入库计划办理配件收货', 'Client/Dcs/Images/Menu/PartsStocking/PartsInboundCheckBill.png', 13, 2);

-- 按钮权限
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (s_action.nextval,5315,'ReceiptManagement|Receipt', '收货', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (s_action.nextval,5315,'ReceiptManagement|Mandatory', '强制完成', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (s_action.nextval,5315,'ReceiptManagement|Print', '打印收货指导单', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (s_action.nextval,5315,'Common|MergeExport', '合并导出', 2);

//发运节点新增按钮权限
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (s_action.nextval,5501,'PartsShippingOrder|ShippingConfirm', '发运确认', 2);

-- 配件采购退货管理-供应商：合并导出
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (s_action.nextval,3104,'Common|MergeExport', '合并导出', 2);

-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);
   
-- 确保所有按钮，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 1, Id, 2 
  FROM Action 
 WHERE Status = 2
   and not exists (select * from node where node.categoryid = action.Id and CategoryType=1)
   and exists (select * from page
                WHERE Type = 2 AND Status = 2 
                  and page.id= action.Pageid);

-- insert into node (id,categorytype,categoryid,status) values(s_node.nextval,0,5315,2);
-- insert into node (id,categorytype,categoryid,status) values(s_node.nextval,1,3213,2);
-- insert into node (id,categorytype,categoryid,status) values(s_node.nextval,1,3214,2);
-- insert into node (id,categorytype,categoryid,status) values(s_node.nextval,1,3215,2);
-- insert into node (id,categorytype,categoryid,status) values(s_node.nextval,1,3216,2);

