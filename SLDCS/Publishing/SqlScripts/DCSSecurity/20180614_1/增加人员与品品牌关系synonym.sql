-- Create the synonym 
create or replace synonym DCSPersonsalescenterlink
  for DCS.Personsalescenterlink;






create or replace trigger Personnel_Trg
   after insert or update on Personnel
   for each row
declare
begin
  if :new.Id is not null and :old.Id is null then
    insert into DcsPersonnel(Id, LoginId, Name, Status, CellNumber, Remark, CorporationId, CorporationName, CreatorId, CreatorName, CreateTime)
    values(:new.Id, :new.LoginId, :new.Name,
           case :new.Status when 1 then 1 when 2 then 2 when 0 then 99 end,
           :new.CellNumber, :new.Remark, :new.EnterpriseId,
           (select Name from Enterprise where Enterprise.Id = :new.EnterpriseId),
           :new.CreatorId, :new.CreatorName, :new.CreateTime);

insert into dcspersonsalescenterlink(id,companyid,personid,partssalescategoryid,status,creatorid,creatorname,createtime)
select dcs.s_personsalescenterlink.nextval,:new.EnterpriseId,:new.Id,a.id,1,1,:new.CreatorName,:new.CreateTime
from dcs.partssalescategory a 
inner join enterprise b on b.id = :new.EnterpriseId
where a.branchid =  :new.EnterpriseId and b.enterprisecategoryid = 1;

    insert into UserLoginInfo (id,LoginId,Enterprisecode,IsFisrt,ErrorPassNum,BeginLockTime,LockLenght)
    select
    s_UserLoginInfo .Nextval,
    :new.LoginId,
    Enterprise.Code,
    1,---是否首次登录=是
    0,--- 输错密码次数
    null,----锁定开始时间
    15--- 锁定时长(分钟)
    from  Enterprise where Enterprise.id=:new.EnterpriseId;

  else
    update DcsPersonnel set
      LoginId = :new.LoginId,
      Name = :new.Name,
      Status = case :new.Status when 1 then 1 when 2 then 2 when 0 then 99 end,
      CellNumber = :new.CellNumber,
      Remark = :new.Remark,
      CorporationId = :new.EnterpriseId,
      CorporationName = (select Name from Enterprise where Enterprise.Id = :new.EnterpriseId),
      ModifierId = :new.ModifierId,
      ModifierName = :new.ModifierName,
      ModifyTime = :new.ModifyTime
    where DcsPersonnel.Id = :new.Id;
  end if;
end Personnel_Trg;
