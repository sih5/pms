INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (6106,6100, 2, 'AccountPeriod', 'Released', NULL, '账期设置', '查询、设置账期', 'Client/DCS/Images/Menu/Financial/CAReconciliation.png', 5, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5411,5400, 2, 'BoxUpTask',  'Released',  NULL,  '配件装箱管理',  '装箱任务管理',  'Client/DCS/Images/Menu/PartsStocking/PartsPacking.png',  14,  2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5209,5200, 2, 'EnterprisePartsCost', 'Released', NULL, '企业库存管理', '企业库存管理', 'Client/DCS/Images/Menu/PartsStocking/PartsStockQuery.png',  9,  2);


insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6106, 'AccountPeriod|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6106, 'AccountPeriod|Close', '关闭', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6106, 'AccountPeriod|Open', '开启', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6106, 'AccountPeriod|MergeExport', '合并导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5411,'BoxUpTask|Create',  '生成装箱任务',  2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5411,'BoxUpTask|BoxUp',  '装箱',  2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5411,'BoxUpTask|Stop',  '终止',  2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5411,'Common|Abandon','作废',  2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5411,'Common|MergeExport',  '合并导出',  2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5209, 'Common|MergeExport', '合并导出', 2);

-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);
   
-- 确保所有按钮，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 1, Id, 2 
  FROM Action 
 WHERE Status = 2
   and not exists (select * from node where node.categoryid = action.Id and CategoryType=1)
   and exists (select * from page
                WHERE Type = 2 AND Status = 2 
                  and page.id= action.Pageid);