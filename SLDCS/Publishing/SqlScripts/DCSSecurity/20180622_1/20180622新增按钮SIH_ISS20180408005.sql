﻿update action set OPERATIONID='PartsPurReturnOrderForBranch|Add' where PAGEID=3103 and NAME='新增';
update action set OPERATIONID='PartsPurReturnOrderForBranch|Edit' where PAGEID=3103 and NAME='修改';
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3103, 'PartsPurReturnOrderForBranch|FinalApprove', '初审', 2);
update action set OPERATIONID='PartsPurReturnOrderForBranch|InsteadApprove',NAME='终审' where PAGEID=3103 and NAME='代审批';
update action set OPERATIONID='PartsPurReturnOrderForBranch|Abandon' where PAGEID=3103 and NAME='作废';
update action set OPERATIONID='PartsPurReturnOrderForBranch|Export' where PAGEID=3103 and NAME='导出';
update action set OPERATIONID='PartsPurReturnOrderForBranch|Print' where PAGEID=3103 and NAME='打印';

   
-- 确保所有按钮，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 1, Id, 2 
  FROM Action 
 WHERE Status = 2
   and not exists (select * from node where node.categoryid = action.Id and CategoryType=1)
   and exists (select * from page
                WHERE Type = 2 AND Status = 2 
                  and page.id= action.Pageid);