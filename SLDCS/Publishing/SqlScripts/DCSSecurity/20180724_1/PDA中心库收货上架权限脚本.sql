insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS,PAGESYSTEM)
values (16104, 16100, 2, 'SIH104', null, null, '收货', null, null, '中心库收货', null, null, null, 4, 2, 1);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS,PAGESYSTEM)
values (16105, 16100, 2, 'SIH105', null, null, '上架', null, null, '中心库上架', null, null, null, 5, 2, 1);

INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);