-- 三包索赔单审批策略节点
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status)
 VALUES (1211, 1200, 2, 'ClaimApproverStrategy', 'Released', NULL, '三包索赔单审批策略', '维护三包索赔单审批策略', 'Client/DCS/Images/Menu/Service/ApplyRules.png', 10, 2);

-- 配件索赔-中心库节点添加按钮
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS) values (S_Action.Nextval, 8212,'Common|Audit', '审核', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS) values (S_Action.Nextval, 8212,'PartsClaimOrderNewForAgency|Receive', '收货', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS) values (S_Action.Nextval, 8212,'PartsClaimOrderNewForAgency|Shipping', '发运', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS) values (S_Action.Nextval, 8212,'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS) values (S_Action.Nextval, 8212,'PartsClaimOrderNewForAgency|Submit', '提交', 2);

-- 总部配件索赔按钮
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS) values (S_Action.Nextval, 8211,'PartsClaimOrderNew|InitialApprove', '初审', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS) values (S_Action.Nextval, 8211,'PartsClaimOrderNew|FinalApprove', '终审', 2);

-- 三包索赔单审批策略按钮
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS) values (S_Action.Nextval, 1211,'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS) values (S_Action.Nextval, 1211,'Common|Edit', '修改', 2);

-- 中心库配件盘点管理
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS) values (S_Action.Nextval, 5604,'AgentsPartsInventoryBill|InitialApprove', '初审', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS) values (S_Action.Nextval, 5604,'AgentsPartsInventoryBill|FinalApprove', '终审', 2);

-- 配件盘点管理
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS) values (S_Action.Nextval, 5602,'PartsInventoryBill|FinalApprove', '终审', 2);
update action set status = 99 where operationid = 'Common|Approve' and pageid = 5602 

-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);

-- 确保所有按钮，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 1, Id, 2 
  FROM Action 
 WHERE Status = 2
   and not exists (select * from node where node.categoryid = action.Id and CategoryType=1)
   and exists (select * from page
                WHERE Type = 2 AND Status = 2 
                  and page.id= action.Pageid);