-- 拣货单打印
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval,5412,'Common|Print', '打印', 2);
--装箱单打印
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval,5411,'Common|Print', '打印', 2);

--入库检验 标签打印
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval,5305,'PartsInboundCheckBillQuery|ChPrint', '箱标中文打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval,5305,'PartsInboundCheckBillQuery|EnPrint', '箱标英文打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval,5305,'PartsInboundCheckBillQuery|PcPrint', '件标打印', 2);

-- 内部领出管理 新增 初审按钮
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval,3302,'Common|InitialApprove', '初审', 2);

-- 信用管理 新增 初审按钮
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval,6201,'Common|InitialApprove', '初审', 2);

--销售退货  新增 批量审核按钮
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval,4103,'PartsSalesReturnBill|BatchApprove', '批量审核', 2);

-- 确保所有按钮，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 1, Id, 2 
  FROM Action 
 WHERE Status = 2
   and not exists (select * from node where node.categoryid = action.Id and CategoryType=1)
   and exists (select * from page
                WHERE Type = 2 AND Status = 2 
                  and page.id= action.Pageid);