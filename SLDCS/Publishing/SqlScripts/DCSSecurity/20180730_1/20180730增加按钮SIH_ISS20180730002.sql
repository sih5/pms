-- 服务站配件盘点管理增加初审，终审按钮
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS) values (S_Action.Nextval, 5605,'Common|InitialApprove', '初审', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS) values (S_Action.Nextval, 5605,'Common|FinalApprove', '终审', 2);

-- 确保所有按钮，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 1, Id, 2 
  FROM Action 
 WHERE Status = 2
   and not exists (select * from node where node.categoryid = action.Id and CategoryType=1)
   and exists (select * from page
                WHERE Type = 2 AND Status = 2 
                  and page.id= action.Pageid);