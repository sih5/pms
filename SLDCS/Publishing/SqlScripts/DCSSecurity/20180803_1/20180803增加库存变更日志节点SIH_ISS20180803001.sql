--新增 库存变更日志节点
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status)
 VALUES (5210, 5200, 2, 'PartsStockHistory', 'Released', NULL, '库存变更日志', '记录仓库库位库存变更明细', 'Client/DCS/Images/Menu/PartsStocking/PartsStockQuery.png', 10, 2);
 
-- 库存变更日志节点增加合并导出按钮
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS) values (S_Action.Nextval, 5210,'Common|MergeExport', '合并导出', 2);

-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);

-- 确保所有按钮，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 1, Id, 2 
  FROM Action 
 WHERE Status = 2
   and not exists (select * from node where node.categoryid = action.Id and CategoryType=1)
   and exists (select * from page
                WHERE Type = 2 AND Status = 2 
                  and page.id= action.Pageid);