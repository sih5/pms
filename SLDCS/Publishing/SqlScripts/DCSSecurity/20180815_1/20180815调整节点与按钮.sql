--更改 配调拨管理 节点位置
update page set parentid=3000 where  id =5700;

-- 供应商发运管理-供应商  标签打印 -- 取消授权
delete from node where CategoryId = (select id from action where pageid = 3105 and name = '标签打印');
update action set  status=99 where name ='标签打印' and pageid=3105;


-- 采购价格变动情况查询 增加作废按钮
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval,2328,'Common|Abandon', '作废', 2);


--增加批量导入配件ABC类型

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval,2102,'PartsBranch|ReplacePartABC', '批量导入配件ABC类型', 2);


-- 确保所有按钮，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 1, Id, 2 
  FROM Action 
 WHERE Status = 2
   and not exists (select * from node where node.categoryid = action.Id and CategoryType=1)
   and exists (select * from page
                WHERE Type = 2 AND Status = 2 
                  and page.id= action.Pageid);
