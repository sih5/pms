--销售订单一次满足率（SIH 配件对中心库)报表查询
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (11104,11100, 2, 'TEST', 'Chart', '/Sunlight.Silverlight.Dcs.Common;component/Dashboard/Charts/Views/Sales/SalesSatisfactionCentralRate.xaml', '销售订单一次满足率(SIH 配件对中心库)', '销售订单一次满足率（SIH 配件对中心库)', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',2, 2);

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (14201,14200, 2, 'SalesSatisfactionCentralQery', 'Released', null, '销售订单一次满足率对中心库', '查询销售订单一次满足率对中心库)', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',2, 2);

--导出
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval,14201,'Common|Export', '导出', 2);

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (14202,14200, 2, 'AllSparePartReport', 'Released', null, '所有备件信息统计', '所有备件信息统计', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',2, 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval,14202,'Common|Export', '导出', 2);


                
      
-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);
   
-- 确保所有按钮，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 1, Id, 2 
  FROM Action 
 WHERE Status = 2
   and not exists (select * from node where node.categoryid = action.Id and CategoryType=1)
   and exists (select * from page
                WHERE Type = 2 AND Status = 2 
                  and page.id= action.Pageid);