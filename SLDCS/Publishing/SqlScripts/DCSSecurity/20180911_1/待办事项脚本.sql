-- 待办事项
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status,Pagesystem) VALUES 
(11105,11100, 2, 'PartsPurchasePricingChangeTrialAudit', 'Warning', '/partsbaseinfo/sparepartpurchase/partspurchasepricingchange?Status=1&&CreateTime=,&&ExecuteQuery', '采购价申请待初审', '采购价申请待初审， 查询新建状态的采购价变更申请单', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',3, 2,2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status,Pagesystem) VALUES 
(11106,11100, 2, 'PartsPurchasePricingChangeFinalAudit', 'Warning', '/partsbaseinfo/sparepartpurchase/partspurchasepricingchange?Status=3&&CreateTime=,&&ExecuteQuery', '采购价申请待终审', '采购价申请待终审， 查询初审通过的 采购价变更申请单', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',4, 2,2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status,Pagesystem) VALUES 
(11107,11100, 2, 'PartsSalesPriceChangeTrialAudit', 'Warning', '/partsbaseinfo/partssalesprice/partssalespricechange?Status=1&&CreateTime=,&&ExecuteQuery', '销售价申请待初审', '销售价申请待初审， 查询新建状态的销售价变更申请单', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',5, 2,2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status,Pagesystem) VALUES 
(11108,11100, 2, 'PartsSalesPriceChangeFinalAudit', 'Warning', '/partsbaseinfo/partssalesprice/partssalespricechange?Status=2&&CreateTime=,&&ExecuteQuery', '销售价申请待终审', '销售价申请待终审，查询初审通过的销售价变更申请单', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',6, 2,2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status,Pagesystem) VALUES 
(11109,11100, 2, 'PartsPackingPropertyAppAudit', 'Warning', '/partsbaseinfo/partssalesprice/partspackingpropertyapp?Status=4&&CreateTime=,&&ExecuteQuery', '包装设置待审核', '包装设置待审核， 查询提交状态的包装设置申请单', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',7, 2,2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status,Pagesystem) VALUES 
(11110,11100, 2, 'SpecialTreatyPriceChangeTrialAudit', 'Warning', '/partsbaseinfo/partssalesprice/specialtreatypricechange?Status=1&&CreateTime=,&&ExecuteQuery', '特殊协议价待初审', '特殊协议价待初审，查询新增状态的特殊协议价变更申请单', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',8, 2,2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status,Pagesystem) VALUES 
(11111,11100, 2, 'SpecialTreatyPriceChangeFinalAudit', 'Warning', '/partsbaseinfo/partssalesprice/specialtreatypricechange?Status=3&&CreateTime=,&&ExecuteQuery', '特殊协议价待终审', '特殊协议价待终审，查询初审通过的特殊协议价变更申请单', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',9, 2,2);
-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);