-- 待办事项
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status,Pagesystem) VALUES 
(11112,11100, 2, 'PartsPurchasePlanTrialAudit', 'Warning', '/partspurchasing/partspurchaseplanorder?Status=2&&CreateTime=,&&ExecuteQuery', '采购计划待初审', '查询提交状态的采购计划', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',10, 2,2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status,Pagesystem) VALUES 
(11113,11100, 2, 'PartsPurchasePlanFinalAudit', 'Warning', '/partspurchasing/partspurchaseplanorder?Status=3&&CreateTime=,&&ExecuteQuery', '采购计划待终审', '查询初审通过的采购计划', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',11, 2,2);

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status,Pagesystem) VALUES 
(11114,11100, 2, 'CredenceApplicationTrialAudit', 'Warning', '/partsfinancialandcontrols/partfunctions/credenceapplication?Status=1&&CreateTime=,&&ExecuteQuery', '信用申请待初审', '查询新增状态，并且销售企业ID等于当前登录人所属企业ID的信用申请单', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',12, 2,2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status,Pagesystem) VALUES 
(11115,11100, 2, 'CredenceApplicationFinalAudit', 'Warning', '/partsfinancialandcontrols/partfunctions/credenceapplication?Status=5&&CreateTime=,&&ExecuteQuery', '信用申请待终审', '查询初审通过状态，并且销售企业ID等于当前登录人所属企业ID的信用申请单', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',13, 2,2);

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status,Pagesystem) VALUES 
(11116,11100, 2, 'PartsPurReturnOrderTrialAudit', 'Warning', '/partspurchasing/partspurreturnorderforbranch?Status=1&&CreateTime=,&&ExecuteQuery', '采购退货待初审', '采购退货待初审', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',14, 2,2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status,Pagesystem) VALUES 
(11117,11100, 2, 'PartsPurReturnOrderFinalAudit', 'Warning', '/partspurchasing/partspurreturnorderforbranch?Status=3&&CreateTime=,&&ExecuteQuery', '采购退货待终审', '采购退货待终审', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',15, 2,2);

-- 销售退货待初审        查询审核通过状态，并且销售组织隶属企业ID等于当前登陆人所属企业ID 的销售退货单
-- 销售退货待终审        查询初审通过状态，并且销售组织隶属企业ID等于当前登陆人所属企业ID 的销售退货单
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status,Pagesystem) VALUES 
(11118,11100, 2, 'PartsSalesReturnBillTrialAudit', 'Warning', '/partssales/partssalesreturnbill?Status=3&&CreateTime=,&&ExecuteQuery', '销售退货待初审', '查询审核通过状态，并且销售组织隶属企业ID等于当前登陆人所属企业ID 的销售退货单', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',16, 2,2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status,Pagesystem) VALUES 
(11119,11100, 2, 'PartsSalesReturnBillFinalAudit', 'Warning', '/partssales/partssalesreturnbill?Status=5&&CreateTime=,&&ExecuteQuery', '销售退货待终审', '查询初审通过状态，并且销售组织隶属企业ID等于当前登陆人所属企业ID 的销售退货单', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',17, 2,2);

-- 配件外采待初审        查询提交状态的配件外采申请单
-- 配件外采待终审        查询初审通过状态的配件外采申请单
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status,Pagesystem) VALUES 
(11120,11100, 2, 'PartsOuterPurchaseChangeTrialAudit', 'Warning', '/partssales/partsouterpurchase/partsouterpurchasebill?Status=2&&CreateTime=,&&ExecuteQuery', '配件外采待初审', '查询提交状态的配件外采申请单', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',18, 2,2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status,Pagesystem) VALUES 
(11121,11100, 2, 'PartsOuterPurchaseChangeFinalAudit', 'Warning', '/partssales/partsouterpurchase/partsouterpurchasebill?Status=4&&CreateTime=,&&ExecuteQuery', '配件外采待终审', '查询初审通过状态的配件外采申请单', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',19, 2,2);

-- 中心库盘点待初审      查询已审批状态，并且仓储企业类型= 中心库 或者 服务站兼中心库的 配件盘点单
-- 中心库盘点待终审      查询已初审通过状态，并且仓储企业类型= 中心库 或者 服务站兼中心库的 盘点单
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status,Pagesystem) VALUES 
(11122,11100, 2, 'PartsInventoryBillCenterTrialAudit', 'Warning', '/partssales/partsouterpurchase/agentspartsinventorybill?Status=4&&StorageCompanyType=3,7&&CreateTime=,&&ExecuteQuery', '中心库盘点待初审', '查询已审批状态，并且仓储企业类型= 中心库 或者 服务站兼中心库的 配件盘点单', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',20, 2,2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status,Pagesystem) VALUES 
(11123,11100, 2, 'PartsInventoryBillCenterFinalAudit', 'Warning', '/partssales/partsouterpurchase/agentspartsinventorybill?Status=5&&StorageCompanyType=3,7&&CreateTime=,&&ExecuteQuery', '中心库盘点待终审', '查询已初审通过状态，并且仓储企业类型= 中心库 或者 服务站兼中心库的 盘点单', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',21, 2,2);

-- 服务站盘点待初审      查询已审核状态的经销商配件盘点单
-- 服务站盘点待终审      查询初审通过状态的经销商配件盘点单
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status,Pagesystem) VALUES 
(11124,11100, 2, 'DealerPartsInventoryBillTrialAudit', 'Warning', '/partssales/partsouterpurchase/dealerpartsinventorybill?Status=2&&CreateTime=,&&ExecuteQuery', '服务站盘点待初审', '查询已审核状态的经销商配件盘点单', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',22, 2,2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status,Pagesystem) VALUES 
(11125,11100, 2, 'DealerPartsInventoryBillFinalAudit', 'Warning', '/partssales/partsouterpurchase/dealerpartsinventorybill?Status=3&&CreateTime=,&&ExecuteQuery', '服务站盘点待终审', '查询初审通过状态的经销商配件盘点单', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',23, 2,2);

-- SIH盘点待初审         查询已审批状态的 并且仓储企业类型=分公司 的 配件盘点单
-- SIH盘点待终审         查询初审通过状态的 并且仓储企业类型= 分公司 的 配件盘点单
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status,Pagesystem) VALUES 
(11126,11100, 2, 'PartsInventoryBillTrialAudit', 'Warning', '/partsstockingandlogistics/warehouseexecution/partsinventorybill?Status=2&&StorageCompanyType=1&&CreateTime=,&&ExecuteQuery', 'SIH盘点待初审', '查询已审批状态的 并且仓储企业类型=分公司 的 配件盘点单', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',24, 2,2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status,Pagesystem) VALUES 
(11127,11100, 2, 'PartsInventoryBillFinalAudit', 'Warning', '/partsstockingandlogistics/warehouseexecution/partsinventorybill?Status=5&&StorageCompanyType=1&&CreateTime=,&&ExecuteQuery', 'SIH盘点待终审', '查询初审通过状态的 并且仓储企业类型= 分公司 的 配件盘点单', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',25, 2,2);

-- 三包索赔待初审        查询审核通过状态的配件索赔单（新）
-- 三包索赔待终审        查询初审通过状态的配件索赔单（新）
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status,Pagesystem) VALUES 
(11128,11100, 2, 'PartsClaimOrderNewTrialAudit', 'Warning', '/service/claimsbusinessforheadquarters/partsclaimordernew?Status=13&&CreateTime=,&&ExecuteQuery', '三包索赔待初审', '查询审核通过状态的配件索赔单（新）', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',26, 2,2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status,Pagesystem) VALUES 
(11129,11100, 2, 'PartsClaimOrderNewFinalAudit', 'Warning', '/service/claimsbusinessforheadquarters/partsclaimordernew?Status=14&&CreateTime=,&&ExecuteQuery', '三包索赔待终审', '查询初审通过状态的配件索赔单（新）', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',27, 2,2);

-- 调拨待初审            查询新建状态 并且仓储企业ID = 当前登陆人所属企业ID 的配件调拨单
-- 调拨待终审            查询初审通过状态 并且仓储企业ID = 当前登陆人所属企业ID 的配件调拨单
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status,Pagesystem) VALUES 
(11130,11100, 2, 'PartsTransferOrderTrialAudit', 'Warning', '/partspurchasing/accessories/partstransferorder?Status=1&&CreateTime=,&&ExecuteQuery','调拨待初审', '查询新建状态 并且仓储企业ID = 当前登陆人所属企业ID 的配件调拨单', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',28, 2,2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status,Pagesystem) VALUES 
(11131,11100, 2, 'PartsTransferOrderFinalAudit', 'Warning', '/partspurchasing/accessories/partstransferorder?Status=3&&CreateTime=,&&ExecuteQuery','调拨待终审', '查询初审通过状态 并且仓储企业ID = 当前登陆人所属企业ID 的配件调拨单', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',29, 2,2);

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status,Pagesystem) VALUES 
(11132,11100, 2, 'SupplierShippingOrderAudit', 'Warning', '/partspurchasing/suppliershippingorderforbranch?Status=2&&IfDirectProvision=1&&DirectProvisionFinished=0&&CreateTime=,&&ExecuteQuery','直供发运单待完成', '查询供应商发运单，状态=收货确认，是否直供  = 是，直供完成 = 0 或者空', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',30, 2,2);

-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);