--销售订单完成情况统计节点
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval,14203,'Common|Export', '导出', 2);

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (14203,14200, 2, 'PartsSalesOrderFinishReport', 'Released', null, '销售订单完成情况统计', '销售订单完成情况统计', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',3, 2);

--包装完成情况统计节点

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval,13101,'Common|Export', '导出', 2);

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13101,13100, 2, 'PartsPackingFinishReport', 'Released', null, '包装任务查询报表', '包装任务查询报表', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',1, 2);


--上架完成情况统计节点

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval,13102,'Common|Export', '导出', 2);





-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);
   
-- 确保所有按钮，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 1, Id, 2 
  FROM Action 
 WHERE Status = 2
   and not exists (select * from node where node.categoryid = action.Id and CategoryType=1)
   and exists (select * from page
                WHERE Type = 2 AND Status = 2 
                  and page.id= action.Pageid);
