--中心库销售订单完成情况统计节点
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval,14204,'Common|Export', '导出', 2);

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (14204,14200, 2, 'CentralPartsSalesOrderFinishReport', 'Released', null, '中心库销售订单完成情况统计', '中心库销售订单完成情况统计', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',4, 2);


--中心库欠货台账统计节点
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval,14205,'Common|Export', '导出', 2);

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (14205,14200, 2, 'PartsSalesOrderOwningReport', 'Released', null, '中心库欠货台账统计', '中心库欠货台账统计', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',5, 2);


--中心库销售出库单统计节点
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval,14206,'Common|Export', '导出', 2);

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (14206,14200, 2, 'PartsOutboundBillCentralReport', 'Released', null, '中心库销售出库单统计', '中心库销售出库单统计', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',6, 2);

--服务站出库单统计节点
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval,14207,'Common|Export', '导出', 2);

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (14207,14200, 2, 'DealerPartsOutboundBillReport', 'Released', null, '服务站出库单统计', '服务站出库单统计', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',7, 2);


--结算单与出入库单关联发票查询节点
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval,13301,'Common|Export', '导出', 2);

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13301,13300, 2, 'SettlementInvoiceReport', 'Released', null, '结算单与出入库单关联发票查询', '结算单与出入库单关联发票查询', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',1, 2);

--采购入库报表节点
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval,14102,'Common|Export', '导出', 2);

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (14102,14100, 2, 'PartsPurchasePlanReport', 'Released', null, '采购计划统计', '采购计划统计', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',2, 2);
--库龄报表
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval,14103,'Common|Export', '导出', 2);

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (14103,14100, 2, 'StorehouseCensusReport', 'Released', null, '库龄报表', '库龄报表', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',3, 2);


INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);
   
-- 确保所有按钮，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 1, Id, 2 
  FROM Action 
 WHERE Status = 2
   and not exists (select * from node where node.categoryid = action.Id and CategoryType=1)
   and exists (select * from page
                WHERE Type = 2 AND Status = 2 
                  and page.id= action.Pageid);

