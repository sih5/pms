-- 待办事项
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status,Pagesystem) VALUES 
(11133,11100, 2, 'BorrowBillTrialAudit', 'Warning', '/partsstockingandlogistics/warehouseexecution/borrowbill?Status=2&&CreateTime=,&&ExecuteQuery', '借用待审核', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',31, 2,2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status,Pagesystem) VALUES 
(11134,11100, 2, 'BorrowBillFinalAudit', 'Warning', '/partsstockingandlogistics/warehouseexecution/borrowbill?Status=3&&CreateTime=,&&ExecuteQuery', '借用待审批', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',32, 2,2);

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status,Pagesystem) VALUES 
(11135,11100, 2, 'InternalAllocationBillTrialAudit', 'Warning', '/partspurchasing/internalrecipient/internalallocationbill?Status=1&&CreateTime=,&&ExecuteQuery', '内部领出待审核', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',33, 2,2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status,Pagesystem) VALUES 
(11136,11100, 2, 'InternalAllocationBillFinalAudit', 'Warning', '/partspurchasing/internalrecipient/internalallocationbill?Status=3&&CreateTime=,&&ExecuteQuery', '内部领出待审批', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',34, 2,2);

-- 待办事项
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status,Pagesystem) VALUES 
(11137,11100, 2, 'BorrowBillReturn', 'Warning', '/partsstockingandlogistics/warehouseexecution/borrowbill?Status=5&&CreateTime=,&&ExecuteQuery', '借用待归还', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',33, 2,2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status,Pagesystem) VALUES 
(11138,11100, 2, 'BorrowBillReturnConfirm', 'Warning', '/partsstockingandlogistics/warehouseexecution/borrowbill?Status=6&&CreateTime=,&&ExecuteQuery', '借用归还待确认', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',34, 2,2);

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status,Pagesystem) VALUES 
(11139,11100, 2, 'DealerPartsInventoryBill', 'Warning', '/partssales/partsouterpurchase/dealerpartsinventorybill?Status=1&&CreateTime=,&&ExecuteQuery', '服务站盘点待初审', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',35, 2,2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status,Pagesystem) VALUES 
(11140,11100, 2, 'PartsInventoryBillCenter', 'Warning', '/partssales/partsouterpurchase/agentspartsinventorybill?Status=2&&CreateTime=,&&ExecuteQuery', '中心库盘点待初审', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',36, 2,2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status,Pagesystem) VALUES 
(11141,11100, 2, 'PartsOuterPurchaseChange', 'Warning', '/partssales/partsouterpurchase/partsouterpurchasebill?Status=2&&CreateTime=,&&ExecuteQuery', '配件外采待初审', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',37, 2,2);

update page set parameter='/partsfinancialandcontrols/partfunctions/credenceapplication?Status=6&&CreateTime=,&&ExecuteQuery'   where id=11114


-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);
   
   
