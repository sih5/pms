INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) 
VALUES(4407,4400, 2, 'OverstockPartsRecommendBill', 'Released', null, '积压件推荐明细', '积压件推荐明细', 'Client/DCS/Images/Menu/PartsStocking/OverstockPartsInformation.png',6, 2);

insert into action (ID, PAGEID, OPERATIONID, NAME,NAME_ENUS, STATUS)values (S_Action.Nextval,4407,'Common|Submit', '提交','Submit', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME,NAME_ENUS, STATUS)values (S_Action.Nextval,4407,'Common|Export', '导出','Export', 2);

-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);

-- 确保所有按钮，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 1, Id, 2 
  FROM Action 
 WHERE Status = 2
   and not exists (select * from node where node.categoryid = action.Id and CategoryType=1)
   and exists (select * from page
                WHERE Type = 2 AND Status = 2 
                  and page.id= action.Pageid);