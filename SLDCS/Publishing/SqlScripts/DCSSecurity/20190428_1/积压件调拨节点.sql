update page set name = '积压件调拨管理',description='查询、提交、确认积压件调拨单' where id = 4405;
delete action where PAGEID = 4405 and OPERATIONID = 'OverstockPartsAdjustBill|Complete';
delete action where PAGEID = 4405 and OPERATIONID = 'Common|Print';
delete action where PAGEID = 4405 and OPERATIONID = 'OverstockPartsAdjustBill|AdjustKanban';

insert into action (ID, PAGEID, OPERATIONID, NAME,NAME_ENUS, STATUS)values (S_Action.Nextval,4405,'Common|Edit', '修改','Edit', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME,NAME_ENUS, STATUS)values (S_Action.Nextval,4405,'OverstockPartsAdjustBill|Submit', '提交','Submit', 2);

-- 确保所有按钮，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 1, Id, 2 
  FROM Action 
 WHERE Status = 2
   and not exists (select * from node where node.categoryid = action.Id and CategoryType=1)
   and exists (select * from page
                WHERE Type = 2 AND Status = 2 
                  and page.id= action.Pageid);
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2
  FROM Page
 WHERE Type = 2 AND Status = 2