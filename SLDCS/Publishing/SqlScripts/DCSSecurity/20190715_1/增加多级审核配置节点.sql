
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES(1214,1400, 2, 'MultiAuditConfig', 'Released', null, '多级审核配置', '多级审核配置', 'Client/DCS/Images/Menu/Service/ApplyRules.png',7, 2);
insert into action (ID, PAGEID, OPERATIONID, NAME,NAME_ENUS, STATUS)values (S_Action.Nextval,1214,'Common|Add', '新增','Add', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME,NAME_ENUS, STATUS)values (S_Action.Nextval,1214,'Common|Edit', '修改','Edit', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME,NAME_ENUS, STATUS)values (S_Action.Nextval,1214,'Common|Abandon', '作废','Abandon', 2);

-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2
  FROM Page
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);
-- 确保所有按钮，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 1, Id, 2 
  FROM Action 
 WHERE Status = 2
   and not exists (select * from node where node.categoryid = action.Id and CategoryType=1)
   and exists (select * from page
                WHERE Type = 2 AND Status = 2 
                  and page.id= action.Pageid);
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2
  FROM Page
 WHERE Type = 2 AND Status = 2
 