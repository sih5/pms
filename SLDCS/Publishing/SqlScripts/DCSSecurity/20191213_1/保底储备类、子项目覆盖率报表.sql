--保底储备类、子项目覆盖率报表
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval,13114,'Common|Export', '导出', 2);

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13114,13100, 2, 'BottomStockSettleForReserve', 'Released', null, '保底储备类别、子项目覆盖率报表', '保底储备类别、子项目覆盖率报表', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',14, 2);

  
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);
   
   
-- 确保所有按钮，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 1, Id, 2 
  FROM Action 
 WHERE Status = 2
   and not exists (select * from node where node.categoryid = action.Id and CategoryType=1)
   and exists (select * from page
                WHERE Type = 2 AND Status = 2 
                  and page.id= action.Pageid);
                  
