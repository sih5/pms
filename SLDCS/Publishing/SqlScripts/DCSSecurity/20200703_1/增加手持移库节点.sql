
INSERT INTO Page
  (Id,
   ParentId,
   Type,
   PageId,
   PageType,
   Parameter,
   Name,
   Description,
   Icon,
   Sequence,
   Status,
   pagesystem)
VALUES
  (16302,
   16300,
   2,
   'SIH302',
   'Released',
   null,
   '�ƿ�',
   '�ƿ�',
   '',
   2,
   2,
   1);

INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);
