INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status,Pagesystem) VALUES (11190,11100, 2, 'CrossSalesOrderFir', 'Warning', '/partssales/crosssalesorder?Status=2&&CreateTime=,&&ExecuteQuery', '跨区域销售备案待初审', '跨区域销售备案待初审', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',56, 2,2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status,Pagesystem) VALUES (11191,11100, 2, 'CrossSalesOrderSec', 'Warning', '/partssales/crosssalesorder?Status=4&&CreateTime=,&&ExecuteQuery', '跨区域销售备案待审核', '跨区域销售备案待审核', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',57, 2,2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status,Pagesystem) VALUES (11192,11100, 2, 'CrossSalesOrderThid', 'Warning', '/partssales/crosssalesorder?Status=5&&CreateTime=,&&ExecuteQuery', '跨区域销售备案待审批', '跨区域销售备案待审批', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',58, 2,2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status,Pagesystem) VALUES (11193,11100, 2, 'CrossSalesOrderFour', 'Warning', '/partssales/crosssalesorder?Status=6&&CreateTime=,&&ExecuteQuery', '跨区域销售备案待高级审核', '跨区域销售备案待高级审核', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',59, 2,2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status,Pagesystem) VALUES (11194,11100, 2, 'CrossSalesOrderFive', 'Warning', '/partssales/crosssalesorder?Status=7&&CreateTime=,&&ExecuteQuery', '跨区域销售备案待完善资料', '跨区域销售备案待完善资料', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',60, 2,2);



INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);
   
   
-- 确保所有按钮，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 1, Id, 2 
  FROM Action 
 WHERE Status = 2
   and not exists (select * from node where node.categoryid = action.Id and CategoryType=1)
   and exists (select * from page
                WHERE Type = 2 AND Status = 2 
                  and page.id= action.Pageid);
                  




