INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status,Pagesystem) VALUES (11195,11100, 2, 'AgencyDifferenceBackBillFir', 'Warning', '/partsstockingandlogistics/warehouseexecution/agencydifferencebackbill?Status=2&&CreateTime=,&&ExecuteQuery', '中心库收货差异待判定', '中心库收货差异待判定', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',61, 2,2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status,Pagesystem) VALUES (11196,11100, 2, 'AgencyDifferenceBackBillSec', 'Warning', '/partsstockingandlogistics/warehouseexecution/agencydifferencebackbill?Status=3&&CreateTime=,&&ExecuteQuery', '中心库收货差异待确认', '中心库收货差异待确认', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',61, 2,2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status,Pagesystem) VALUES (11197,11100, 2, 'AgencyDifferenceBackBillThid', 'Warning', '/partsstockingandlogistics/warehouseexecution/agencydifferencebackbill?Status=4&&CreateTime=,&&ExecuteQuery', '中心库收货差异待处理', '中心库收货差异待处理', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',62, 2,2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status,Pagesystem) VALUES (11198,11100, 2, 'AgencyDifferenceBackBillFour', 'Warning', '/partsstockingandlogistics/warehouseexecution/agencydifferencebackbill?Status=5&&CreateTime=,&&ExecuteQuery', '中心库收货差异待审核', '中心库收货差异待审核', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',63, 2,2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status,Pagesystem) VALUES (11199,11100, 2, 'AgencyDifferenceBackBillFive', 'Warning', '/partsstockingandlogistics/warehouseexecution/agencydifferencebackbill?Status=6&&CreateTime=,&&ExecuteQuery', '中心库收货差异待审批', '中心库收货差异待审批', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',64, 2,2);


INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);
   
   
-- 确保所有按钮，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 1, Id, 2 
  FROM Action 
 WHERE Status = 2
   and not exists (select * from node where node.categoryid = action.Id and CategoryType=1)
   and exists (select * from page
                WHERE Type = 2 AND Status = 2 
                  and page.id= action.Pageid);
                  
