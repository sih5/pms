﻿INSERT INTO Page(Id,ParentId,Type,PageId,PageType,Parameter,Name,Description,Icon,Sequence,Status, pagesystem) 
VALUES (13125,13100,2,'TraceWarehouseInSupplierReport','Released',NULL,'供应商配件入库精确追溯物流跟踪','查询配件入库精确追溯物流跟踪','Client/DCS/Images/Menu/Channels/NotificationQuery.png', 24,2,2);   

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 13125, 'Common|Export', '导出', 2);

INSERT INTO Page(Id,ParentId,Type,PageId,PageType,Parameter,Name,Description,Icon,Sequence,Status, pagesystem) 
VALUES (13126,13100,2,'TraceWarehouseOutSupplierReport','Released',NULL,'供应商配件出库精确追溯物流跟踪','查询配件出库精确追溯物流跟踪','Client/DCS/Images/Menu/Channels/NotificationQuery.png', 25,2,2);   

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 13126, 'Common|Export', '导出', 2);

-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2

   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);
   
-- 确保所有按钮，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 1, Id, 2 
  FROM Action 
 WHERE Status = 2
   and not exists (select * from node where node.categoryid = action.Id and CategoryType=1)
   and exists (select * from page
                WHERE Type = 2 AND Status = 2 
                  and page.id= action.Pageid);
                  
