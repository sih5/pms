INSERT INTO Page(Id,ParentId,Type,PageId,PageType,Parameter,Name,Description,Icon,Sequence,Status, pagesystem) 
VALUES (4409,4400,2,'OverstockPartsRecommend','Released',NULL,'积压件推荐管理','查询、维护积压件推荐信息','Client/DCS/Images/Menu/PartsStocking/OverstockPartsInformation.png', 9,2,2);   

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4409, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4409, 'Common|Submit', '提交', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4409, 'Common|InitialApprove', '初审', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4409, 'Common|Audit', '审核', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4409, 'Common|Approve', '审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4409, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4409, 'Common|MergeExport', '合并导出', 2);

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status,Pagesystem) VALUES (11202,11100, 2, 'OverstockPartsRecommendPendingTrial', 'Warning', '/PartsSales/PartsSales/OverstockPartsRecommend?Status=2&&CreateTime=,&&ExecuteQuery', '积压件推荐管理待初审', '积压件推荐管理待初审', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',67, 2,2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status,Pagesystem) VALUES (11203,11100, 2, 'OverstockPartsRecommendPendingTrial', 'Warning', '/PartsSales/PartsSales/OverstockPartsRecommend?Status=3&&CreateTime=,&&ExecuteQuery', '积压件推荐管理待审核', '积压件推荐管理待审核', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',68, 2,2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status,Pagesystem) VALUES (11204,11100, 2, 'OverstockPartsRecommendPendingTrial', 'Warning', '/PartsSales/PartsSales/OverstockPartsRecommend?Status=4&&CreateTime=,&&ExecuteQuery', '积压件推荐管理待审批', '积压件推荐管理待审批', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',69, 2,2);

-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2

   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);
   
-- 确保所有按钮，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 1, Id, 2 
  FROM Action 
 WHERE Status = 2
   and not exists (select * from node where node.categoryid = action.Id and CategoryType=1)
   and exists (select * from page
                WHERE Type = 2 AND Status = 2 
                  and page.id= action.Pageid);
                  
