INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status,Pagesystem) VALUES (11205,11100, 2, 'SparePartpurchApplicationAudit', 'Warning', '/PartsBaseInfo/SparePartPurchase/SparePartpurchApplication?Status=2&&CreateTime=,&&ExecuteQuery', '配件采购周期申请单待审核', '配件采购周期申请单待审核', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',70, 2,2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status,Pagesystem) VALUES (11206,11100, 2, 'SparePartpurchApplicationApprove', 'Warning', '/PartsBaseInfo/SparePartPurchase/SparePartpurchApplication?Status=3&&CreateTime=,&&ExecuteQuery', '配件采购周期申请单待审批', '配件采购周期申请单待审批', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',71, 2,2);

-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2

   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);
   
-- 确保所有按钮，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 1, Id, 2 
  FROM Action 
 WHERE Status = 2
   and not exists (select * from node where node.categoryid = action.Id and CategoryType=1)
   and exists (select * from page
                WHERE Type = 2 AND Status = 2 
                  and page.id= action.Pageid);
                  
                  
