INSERT INTO Page(Id,ParentId,Type,PageId,PageType,Parameter,Name,Description,Icon,Sequence,Status, pagesystem) 
VALUES (4122,4100,2,'CenterReimbursement','Released',NULL,'中心库报销管理','查询、维护中心库报销管理','Client/DCS/Images/Menu/PartsSales/PartsSalesReturnBillForReport.png', 15,2,2);   

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4122, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4122, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4122, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4122, 'Common|Submit', '提交', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4122, 'Common|InitialApprove', '初审', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4122, 'Common|Audit', '审核', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4122, 'Common|Approve', '审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4122, 'Common|Terminate', '终止', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4122, 'Common|Export', '导出', 2);

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status,Pagesystem) VALUES (11207,11100, 2, 'CenterReimbursementPendingTrial', 'Warning', '/partssales/centerreimbursement?Status=2&&CreateTime=,&&ExecuteQuery', '中心库报销单待初审', '中心库报销单待初审', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',70, 2,2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status,Pagesystem) VALUES (11208,11100, 2, 'CenterReimbursementAutd', 'Warning', '/partssales/centerreimbursement?Status=3&&CreateTime=,&&ExecuteQuery', '中心库报销单待审核', '中心库报销单待审核', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',71, 2,2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status,Pagesystem) VALUES (11209,11100, 2, 'CenterReimbursementApp', 'Warning', '/partssales/centerreimbursement?Status=4&&CreateTime=,&&ExecuteQuery', '中心库报销单待审批', '中心库报销单待审批', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',72, 2,2);

-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2

   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);
   
-- 确保所有按钮，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 1, Id, 2 
  FROM Action 
 WHERE Status = 2
   and not exists (select * from node where node.categoryid = action.Id and CategoryType=1)
   and exists (select * from page
                WHERE Type = 2 AND Status = 2 
                  and page.id= action.Pageid);

                  
