INSERT INTO Page(Id,ParentId,Type,PageId,PageType,Parameter,Name,Description,Icon,Sequence,Status, pagesystem) 
VALUES (13400,13000,1,'Dashboards','Released',NULL,'配件-仪表盘','Dashboards',null, 30,2,2);   



INSERT INTO Page(Id,ParentId,Type,PageId,PageType,Parameter,Name,Description,Icon,Sequence,Status, pagesystem) 
VALUES (13401,13400,2,'DailyInboundCompletion','Released',NULL,'每日入库完成情况报表','每日入库完成情况报表','Client/DCS/Images/Menu/PartsStocking/PartsInventoryBill.png', 1,2,2);   

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 13401, 'Common|Export', '导出', 2);

INSERT INTO Page(Id,ParentId,Type,PageId,PageType,Parameter,Name,Description,Icon,Sequence,Status, pagesystem) 
VALUES (13402,13400,2,'DailyPackingCompletion','Released',NULL,'每日包装完成情况报表','每日入库完成情况报表','Client/DCS/Images/Menu/PartsStocking/PartsInventoryBill.png', 2,2,2);   

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 13402, 'Common|Export', '导出', 2);


INSERT INTO Page(Id,ParentId,Type,PageId,PageType,Parameter,Name,Description,Icon,Sequence,Status, pagesystem) 
VALUES (13403,13400,2,'DailyShelveCompletion','Released',NULL,'每日上架完成情况报表','每日上架完成情况报表','Client/DCS/Images/Menu/PartsStocking/PartsInventoryBill.png', 3,2,2);   

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 13403, 'Common|Export', '导出', 2);


INSERT INTO Page(Id,ParentId,Type,PageId,PageType,Parameter,Name,Description,Icon,Sequence,Status, pagesystem) 
VALUES (13404,13400,2,'CompletionSummary','Released',NULL,'每日入库、包装、上架完成情况汇总','每日入库、包装、上架完成情况汇总','Client/DCS/Images/Menu/PartsStocking/PartsInventoryBill.png', 4,2,2);   

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 13404, 'Common|Export', '导出', 2);

INSERT INTO Page(Id,ParentId,Type,PageId,PageType,Parameter,Name,Description,Icon,Sequence,Status, pagesystem) 
VALUES (13405,13400,2,'OutBoundDetail','Released',NULL,'48小时出库明细','48小时出库明细','Client/DCS/Images/Menu/PartsStocking/PartsInventoryBill.png', 5,2,2);  

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 13405, 'Common|Export', '导出', 2);

INSERT INTO Page(Id,ParentId,Type,PageId,PageType,Parameter,Name,Description,Icon,Sequence,Status, pagesystem) 
VALUES (13406,13400,2,'OutoundAnalysis','Released',NULL,'48小时出库情况分析','48小时出库情况分析','Client/DCS/Images/Menu/PartsStocking/PartsInventoryBill.png', 6,2,2);   

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 13406, 'Common|Export', '导出', 2);

INSERT INTO Page(Id,ParentId,Type,PageId,PageType,Parameter,Name,Description,Icon,Sequence,Status, pagesystem) 
VALUES (13407,13400,2,'CompletedByMonth','Released',NULL,'每月完成条目数及金额','每月完成条目数及金额','Client/DCS/Images/Menu/PartsStocking/PartsInventoryBill.png', 6,2,2);   

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 13407, 'Common|Export', '导出', 2);

INSERT INTO Page(Id,ParentId,Type,PageId,PageType,Parameter,Name,Description,Icon,Sequence,Status, pagesystem) 
VALUES (13408,13400,2,'HourlyCompletionTeam','Released',NULL,'各班组每小时完成量','各班组每小时完成量','Client/DCS/Images/Menu/PartsStocking/PartsInventoryBill.png', 6,2,2);   

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 13408, 'Common|Export', '导出', 2);

-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2

   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);
   
-- 确保所有按钮，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 1, Id, 2 
  FROM Action 
 WHERE Status = 2
   and not exists (select * from node where node.categoryid = action.Id and CategoryType=1)
   and exists (select * from page
                WHERE Type = 2 AND Status = 2 
                  and page.id= action.Pageid);
                  
