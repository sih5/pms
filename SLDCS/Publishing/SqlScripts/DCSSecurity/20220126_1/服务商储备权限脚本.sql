INSERT INTO Page(Id,ParentId,Type,PageId,PageType,Parameter,Name,Description,Icon,Sequence,Status, pagesystem) 
VALUES (1700,1000,1,'Maintenance','Released',NULL,'区域服务商储备明细自动计算需求','Maintenance',null, 6,2,2);   



INSERT INTO Page(Id,ParentId,Type,PageId,PageType,Parameter,Name,Description,Icon,Sequence,Status, pagesystem) 
VALUES (1701,1700,2,'MaintenanceVariety','Released',NULL,'服务商储备品种比例维护','服务商储备品种比例维护','Client/DCS/Images/Menu/PartsStocking/PartsInventoryBill.png', 1,2,2);   

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1701, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1701, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1701, 'Common|Abandon', '作废', 2);

INSERT INTO Page(Id,ParentId,Type,PageId,PageType,Parameter,Name,Description,Icon,Sequence,Status, pagesystem) 
VALUES (1702,1700,2,'MaintenanceReserve','Released',NULL,'服务商储备品种及储备天数维护','服务商储备品种及储备天数维护','Client/DCS/Images/Menu/PartsStocking/PartsInventoryBill.png', 2,2,2);   

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1702, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1702, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1702, 'Common|Abandon', '作废', 2);

INSERT INTO Page(Id,ParentId,Type,PageId,PageType,Parameter,Name,Description,Icon,Sequence,Status, pagesystem) 
VALUES (1703,1700,2,'MaintenancePart','Released',NULL,'服务商配件是否储备维护','服务商配件是否储备维护','Client/DCS/Images/Menu/PartsStocking/PartsInventoryBill.png', 3,2,2);   

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1703, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1703, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1703, 'Common|Abandon', '作废', 2);

INSERT INTO Page(Id,ParentId,Type,PageId,PageType,Parameter,Name,Description,Icon,Sequence,Status, pagesystem) 
VALUES (1704,1700,2,'MaintenanceMandatory','Released',NULL,'服务商强制储备品种修改比例维护','服务商强制储备品种修改比例维护','Client/DCS/Images/Menu/PartsStocking/PartsInventoryBill.png', 4,2,2);   

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1704, 'Common|Edit', '修改', 2);

INSERT INTO Page(Id,ParentId,Type,PageId,PageType,Parameter,Name,Description,Icon,Sequence,Status, pagesystem) 
VALUES (1705,1700,2,'DealerReservesRecommended','Released',NULL,'服务商推荐储备明细','服务商推荐储备明细','Client/DCS/Images/Menu/PartsStocking/PartsInventoryBill.png', 5,2,2);   

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1705, 'Common|Export', '导出', 2);

-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2

   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);
   
-- 确保所有按钮，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 1, Id, 2 
  FROM Action 
 WHERE Status = 2
   and not exists (select * from node where node.categoryid = action.Id and CategoryType=1)
   and exists (select * from page
                WHERE Type = 2 AND Status = 2 
                  and page.id= action.Pageid);
                  
 
