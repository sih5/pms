INSERT INTO Page(Id,ParentId,Type,PageId,PageType,Parameter,Name,Description,Icon,Sequence,Status, pagesystem) 
VALUES (2335,2300,2,'PartSpecialPrice','Released',NULL,'服务单特殊报价—上柴','服务单特殊报价—上柴','Client/DCS/Images/Menu/PartsSales/PartsRetailGuidePrice.png', 29,2,2);   

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2335, 'Common|Export', '导出', 2);

-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2

   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);
   
-- 确保所有按钮，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 1, Id, 2 
  FROM Action 
 WHERE Status = 2
   and not exists (select * from node where node.categoryid = action.Id and CategoryType=1)
   and exists (select * from page
                WHERE Type = 2 AND Status = 2 
                  and page.id= action.Pageid);
                  
                  
