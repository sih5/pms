
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3201, 'PartsPurchaseSettleBill|AddInvoice', '开票', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3204, 'PartsPurchaseSettleBill|AddInvoice', '开票', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3202, 'PartsPurchaseRtnSettleBill|AddInvoice', '开票', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3202, 'PartsPurchaseRtnSettleBill|EditRed', '修改红字通知单', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3202, 'PartsPurchaseRtnSettleBill|InvoiceApprove', '发票审核', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3202, 'PartsPurchaseRtnSettleBill|ImportRed', '导入红字通知单', 2);

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) 
VALUES (2216,2200, 2, 'SupplierMaintenance', 'Released', NULL, '供应商年度返利比例、质保买断比例维护', '供应商年度返利比例、质保买断比例维护', 'Client/DCS/Images/Menu/PartsPurchasing/PartConPurchasePlan.png', 10, 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2216, 'Common|Import', '导入', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2216, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2216, 'Common|Abandon', '作废', 2);


INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) 
VALUES (3206,3200, 2, 'SupplierAnnualRebateBill', 'Released', NULL, '供应商补差/年度返利', '供应商补差/年度返利', 'Client/DCS/Images/Menu/PartsPurchasing/PartConPurchasePlan.png', 5, 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3206, 'SupplierAnnualRebateBill|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3206, 'SupplierAnnualRebateBill|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3206, 'SupplierAnnualRebateBill|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3206, 'SupplierAnnualRebateBill|Submit', '提交', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3206, 'SupplierAnnualRebateBill|Confirm', '供应商确认', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3206, 'SupplierAnnualRebateBill|Approve', '审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3206, 'SupplierAnnualRebateBill|AddInvoice', '开票', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3206, 'SupplierAnnualRebateBill|InvoiceRegister', '发票登记', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3206, 'SupplierAnnualRebateBill|InvoiceEdit', '发票修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3206, 'SupplierAnnualRebateBill|InvoiceApprove', '发票审核', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3206, 'SupplierAnnualRebateBill|AntiSettlement', '反结算', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3206, 'SupplierAnnualRebateBill|InvoiceSettlement', '发票已审核反结算', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3206, 'SupplierAnnualRebateBill|EditRed', '修改红字通知单', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3206, 'SupplierAnnualRebateBill|Print', '打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3206, 'SupplierAnnualRebateBill|MerageExport', '合并导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3206, 'SupplierAnnualRebateBill|ImportRed', '导入红字通知单', 2);
--待办事项
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status,Pagesystem) VALUES (11210,11100, 2, 'SupplierAnnualRebateBillConfirm', 'Warning', '/partspurchasing/partspurchasesettle/supplierannualrebatebill?Status=2&&CreateTime=,&&ExecuteQuery', '供应商补差/年度返利管理待确认', '供应商补差/年度返利管理待确认', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',65, 2,2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status,Pagesystem) VALUES (11211,11100, 2, 'SupplierAnnualRebateBillApprove', 'Warning', '/partspurchasing/partspurchasesettle/supplierannualrebatebill?Status=3&&CreateTime=,&&ExecuteQuery', '供应商补差/年度返利管理待审批', '供应商补差/年度返利管理待审批', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',66, 2,2);

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) 
VALUES (14114,14100, 2, 'SupplierRebateDetail', 'Released', NULL, '供应商年度返利/质保买断明细总表', '供应商年度返利/质保买断明细总表', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 15, 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 14114, 'Common|Export', '导出', 2);
 
-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2

   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);
   
-- 确保所有按钮，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 1, Id, 2 
  FROM Action 
 WHERE Status = 2
   and not exists (select * from node where node.categoryid = action.Id and CategoryType=1)
   and exists (select * from page
                WHERE Type = 2 AND Status = 2 
                  and page.id= action.Pageid);
                  

