INSERT INTO Page(Id,ParentId,Type,PageId,PageType,Parameter,Name,Icon,Sequence,Status, pagesystem) 
VALUES (16204,16200,2,'SIH204','Released',NULL,'发运',null, 4,2,1);   

INSERT INTO Page(Id,ParentId,Type,PageId,PageType,Parameter,Name,Icon,Sequence,Status, pagesystem) 
VALUES (16107,16100,2,'SIH107','Released',NULL,'计划单入库',null, 7,2,1);   
INSERT INTO Page(Id,ParentId,Type,PageId,PageType,Parameter,Name,Icon,Sequence,Status, pagesystem) 
VALUES (16108,16100,2,'SIH108','Released',NULL,'计划单绑定',null, 8,2,1);  

INSERT INTO Page(Id,ParentId,Type,PageId,PageType,Parameter,Name,Icon,Sequence,Status, pagesystem) 
VALUES (16109,16100,2,'SIH109','Released',NULL,'直供收货',null, 9,2,1);  

-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2

   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);
   
-- 确保所有按钮，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 1, Id, 2 
  FROM Action 
 WHERE Status = 2
   and not exists (select * from node where node.categoryid = action.Id and CategoryType=1)
   and exists (select * from page
                WHERE Type = 2 AND Status = 2 
                  and page.id= action.Pageid);
