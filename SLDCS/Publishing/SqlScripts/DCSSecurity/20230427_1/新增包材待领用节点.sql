INSERT INTO Page(Id,ParentId,Type,PageId,PageType,Parameter,Name,Description,Icon,Sequence,Status, pagesystem) VALUES (3304,3300,2,'PackagingCollection','Released',NULL,'包材待领用单','包材待领用单','Client/DCS/Images/Menu/Common/SparePart.png', 2,2,2);   

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3304, 'Common|Print', '打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3304, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3304, 'Common|Approve', '审核', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3304, 'Common|MergeExport', '合并导出', 2);

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status,Pagesystem) VALUES (11213,11100, 2, 'PackagingCollectionApp', 'Warning', '/partspurchasing/internalrecipient/packagingcollection?Status=1&&ExecuteQuery', '包材待领用单待审核', '包材待领用单待审核', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',75, 2,2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5314, 'PackingTask|AddPack', '领包材', 2);

update page s set s.name='采购及销售退货对应包材明细查询',s.description='查询采购及销售退货对应包材明细查询' where s.pageid='PartsPurchasePlanTemp';


-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);
   
-- 确保所有按钮，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 1, Id, 2 
  FROM Action 
 WHERE Status = 2
   and not exists (select * from node where node.categoryid = action.Id and CategoryType=1)
   and exists (select * from page
                WHERE Type = 2 AND Status = 2 
                  and page.id= action.Pageid);
                  
           
