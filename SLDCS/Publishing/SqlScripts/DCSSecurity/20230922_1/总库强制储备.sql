
INSERT INTO Page(Id,ParentId,Type,PageId,PageType,Parameter,Name,Description,Icon,Sequence,Status, pagesystem) VALUES (1507,1500,2,'ForceReserveBillSIH','Released',NULL,'总库强制储备管理','总库强制储备管理','Client/DCS/Images/Menu/Common/PartsPurchasePricingChange.png', 7,2,2);   

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1507, 'ForceReserveBillCenter|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1507, 'ForceReserveBillCenter|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1507, 'ForceReserveBillCenter|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1507, 'ForceReserveBillCenter|Submit', '提交', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1507, 'ForceReserveBillCenter|Audit', '审核', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1507, 'ForceReserveBillCenter|Approve', '审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1507, 'ForceReserveBillCenter|MergeExport', '合并导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1507, 'ForceReserveBillCenter|Export', '导出', 2);

INSERT INTO Page(Id,ParentId,Type,PageId,PageType,Parameter,Name,Description,Icon,Sequence,Status, pagesystem) VALUES (13127,13100,2,'BottomStockSIHForReserve','Released',NULL,'保底储备类别、子项目覆盖率报表（总库）','保底储备类别、子项目覆盖率报表（总库）','Client/DCS/Images/Menu/Channels/NotificationQuery.png', 26,2,2);   
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 13127, 'Common|Export', '导出', 2);

 
-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);
   
-- 确保所有按钮，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 1, Id, 2 
  FROM Action 
 WHERE Status = 2
   and not exists (select * from node where node.categoryid = action.Id and CategoryType=1)
   and exists (select * from page
                WHERE Type = 2 AND Status = 2 
                  and page.id= action.Pageid);
                  
               
