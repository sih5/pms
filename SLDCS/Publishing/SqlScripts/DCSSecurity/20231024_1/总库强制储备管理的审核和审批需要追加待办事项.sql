INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status,Pagesystem) VALUES (11214,11100, 2, 'ForceReserveBillSIHAudi', 'Warning', '/common/bottomstock/forcereservebillsih?Status=2&&companytype=1&&ExecuteQuery', '总库强制储备待审核', '总库强制储备待审核', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',76, 2,2);

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status,Pagesystem) VALUES (11215,11100, 2, 'ForceReserveBillSIHApp', 'Warning', '/common/bottomstock/forcereservebillsih?Status=3&&companytype=1&&ExecuteQuery', '总库强制储备待审批', '总库强制储备待审批', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',77, 2,2);


-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);
   
-- 确保所有按钮，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 1, Id, 2 
  FROM Action 
 WHERE Status = 2
   and not exists (select * from node where node.categoryid = action.Id and CategoryType=1)
   and exists (select * from page
                WHERE Type = 2 AND Status = 2 
                  and page.id= action.Pageid);
                  
           
