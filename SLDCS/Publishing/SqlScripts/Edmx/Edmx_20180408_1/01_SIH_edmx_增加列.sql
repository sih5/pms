alter table "PartsPurchasePlan"  add   "PartsSalesCategoryId" NUMBER(9);
alter table "PartsPurchasePlan"  add   "PartsSalesCategoryName" VARCHAR2(100);
alter table "PartsPurchasePlan"  add   "ApproveMemo"        VARCHAR2(200);
alter table "PartsPurchasePlan"  add   "CloseMemo"          VARCHAR2(200);
