/*==============================================================*/
/* Table: "PurchasePersonSupplierLink"                          */
/*==============================================================*/
create table "PurchasePersonSupplierLink"  (
   "Id"                 NUMBER(9)                       not null,
   "PersonId"           NUMBER(9)                       not null,
   "PersonCode"         VARCHAR2(50),
   "PersonName"         VARCHAR2(50),
   "SupplierId"         NUMBER(9)                       not null,
   "SupplierCode"       VARCHAR2(50),
   "SupplierName"       VARCHAR2(100),
   "Status"             NUMBER(9)                       not null,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   constraint PK_PURCHASEPERSONSUPPLIERLINK primary key ("Id")
)
/

/*==============================================================*/
/* Table: "PurchasePricingChangeHis"                            */
/*==============================================================*/
create table "PurchasePricingChangeHis"  (
   "Id"                 NUMBER(9)                       not null,
   "PartsSalesCategoryId" NUMBER(9),
   "SparePartId"        NUMBER(9),
   "SparePartCode"      VARCHAR2(50),
   "SparePartName"      VARCHAR2(100),
   "OldPurchasePrice"   NUMBER(19,4),
   "PurchasePrice"      NUMBER(19,4),
   "ChangeRatio"        NUMBER(15,6),
   "CenterPrice"        NUMBER(19,4),
   "DistributionPrice"  NUMBER(19,4),
   "RetailPrice"        NUMBER(19,4),
   "MaintenanceTime"    DATE,
   "Status"             NUMBER(9),
   "Remark"             VARCHAR2(200),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   constraint PK_PURCHASEPRICINGCHANGEHIS primary key ("Id")
)
/

alter table "PartsSalesPrice" add "CenterPrice" NUMBER(19,4);

/*==============================================================*/
/* Table: "ExportCustomerInfo"                                  */
/*==============================================================*/
create table "ExportCustomerInfo"  (
   "Id"                 NUMBER(9)                       not null,
   "CustomerCode"       VARCHAR2(50),
   "CustomerName"       VARCHAR2(100),
   "InvoiceCompanyId"   NUMBER(9),
   "InvoiceCompanyName" VARCHAR2(100),
   "PriceType"          NUMBER(9),
   "Status"             NUMBER(9),
   "Remark"             VARCHAR2(200),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   constraint PK_EXPORTCUSTOMERINFO primary key ("Id")
)
/

/*==============================================================*/
/* Table: "IvecoPriceChangeApp"                                 */
/*==============================================================*/
create table "IvecoPriceChangeApp"  (
   "Id"                 NUMBER(9)                       not null,
   "PartsSalesCategoryId" NUMBER(9),
   "Code"               VARCHAR2(50),
   "PriceType"          NUMBER(9)  default 1,
   "Status"             NUMBER(9)                       not null,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "InitialApproverId"  NUMBER(9),
   "InitialApproverName" VARCHAR2(100),
   "InitialApproveTime" DATE,
   "FinalApproverId"    NUMBER(9),
   "FinalApproverName"  VARCHAR2(100),
   "FinalApproveTime"   DATE,   
   constraint PK_IVECOPRICECHANGEAPP primary key ("Id")
)
/

/*==============================================================*/
/* Table: "IvecoPriceChangeAppDetail"                           */
/*==============================================================*/
create table "IvecoPriceChangeAppDetail"  (
   "Id"                 NUMBER(9)                       not null,
   "IvecoPriceChangeAppId" NUMBER(9)                       not null,
   "SparePartId"        NUMBER(9),
   "SparePartCode"      VARCHAR2(50),
   "SparePartName"      VARCHAR2(100),
   "IvecoPrice"         NUMBER(19,4),
   constraint PK_IVECOPRICECHANGEAPPDETAIL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "IvecoSalesPrice"                                     */
/*==============================================================*/
create table "IvecoSalesPrice"  (
   "Id"                 NUMBER(9)                       not null,
   "PartsSalesCategoryId" NUMBER(9),
   "PartsSalesCategoryName" VARCHAR2(50),
   "SparePartId"        NUMBER(9),
   "SparePartCode"      VARCHAR2(50),
   "SparePartName"      VARCHAR2(100),
   "SalesPrice"         NUMBER(19,4),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   constraint PK_IVECOSALESPRICE primary key ("Id")
)
/

alter table "PartsPurchaseOrderDetail" add "ShortSupReason" NUMBER(9);
alter table "PartsPurchaseOrderDetail" add "ShippingDate" DATE;

alter table "BranchSupplierRelation" add "ShippingCycle" NUMBER(9);
alter table "BranchSupplierRelation" add "ArrivalCycle" NUMBER(9);

alter table "PartsSalesOrder" add "IsExport" NUMBER(9);
alter table "PartsSalesOrder" add "InboundCompanyId" NUMBER(9);
alter table "PartsSalesOrder" add "InboundCompanyCode" VARCHAR2(50);
alter table "PartsSalesOrder" add "InboundCompanyName" VARCHAR2(100);
alter table "PartsSalesOrder" add "ContractCode" VARCHAR2(50);
alter table "PartsSalesOrder" add "PackingMethod" VARCHAR2(200);
alter table "PartsSalesOrder" add "PriceType" NUMBER(9);