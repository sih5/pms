alter table "PartsBranch" add "MainPackingType" NUMBER(9);

/*==============================================================*/
/* Table: "PartsPackingPropertyApp"                             */
/*==============================================================*/
create table "PartsPackingPropertyApp"  (
   "Id"                 NUMBER(9)                       not null,
   "PartsSalesCategoryId" NUMBER(9),
   "SparePartId"        NUMBER(9)                       not null,
   "SpareCode"          VARCHAR2(50)                    not null,
   "SihCode"            VARCHAR2(50),
   "SpareName"          VARCHAR2(100),
   "Status"             NUMBER(9)                       not null,
   "ApprovalComment"    VARCHAR2(200),
   "MainPackingType"    NUMBER(9),
   "MInSalesAmount"     NUMBER(9),
   "Remark"             VARCHAR2(200),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "ApproverId"         NUMBER(9),
   "ApproverName"       VARCHAR2(100),
   "ApproveTime"        DATE,
   "Code"               VARCHAR2(50)                    not null,
   constraint PK_PARTSPACKINGPROPERTYAPP primary key ("Id")
)
/
/*==============================================================*/
/* Table: "PartsPackingPropAppDetail"                           */
/*==============================================================*/
create table "PartsPackingPropAppDetail"  (
   "Id"                 NUMBER(9)                       not null,
   "PartsPackingPropAppId" NUMBER(9),
   "PackingCode"        VARCHAR2(50),
   "PackingType"        NUMBER(9),
   "MeasureUnit"        VARCHAR2(20)                    not null,
   "PackingCoefficient" NUMBER(9),
   "PackingMaterial"    VARCHAR2(50),
   "Volume"             NUMBER(15,6),
   "Weight"             NUMBER(15,6),
   "Length"             NUMBER(15,6),
   "Width"              NUMBER(15,6),
   "Height"             NUMBER(15,6),
   "SparePartId"        NUMBER(9),
   constraint PK_PARTSPACKINGPROPAPPDETAIL primary key ("Id")
)
/
/*==============================================================*/
/* Table: "PartsBranchPackingProp"                              */
/*==============================================================*/
create table "PartsBranchPackingProp"  (
   "Id"                 NUMBER(9)                       not null,
   "PartsBranchId"      NUMBER(9),
   "PackingCode"        VARCHAR2(50),
   "PackingType"        NUMBER(9),
   "MeasureUnit"        VARCHAR2(20)                    not null,
   "PackingCoefficient" NUMBER(9),
   "PackingMaterial"    VARCHAR2(50),
   "Volume"             NUMBER(15,6),
   "Weight"             NUMBER(15,6),
   "Length"             NUMBER(15,6),
   "Width"              NUMBER(15,6),
   "Height"             NUMBER(15,6),
   "SparePartId"        NUMBER(9),
   constraint PK_PARTSBRANCHPACKINGPROP primary key ("Id")
)
/
/*==============================================================*/
/* Table: "PartsShelvesTask"                                    */
/*==============================================================*/
create table "PartsShelvesTask"  (
   "Id"                 NUMBER(9)                       not null,
   "Code"               VARCHAR2(50),
   "PackingTaskId"      NUMBER(9),
   "PackingTaskCode"    VARCHAR2(50),
   "PartsInboundPlanId" NUMBER(9),
   "PartsInboundPlanCode" VARCHAR2(50),
   "PartsInboundCheckBillId" NUMBER(9),
   "PartsInboundCheckBillCode" VARCHAR2(100),
   "WarehouseId"        NUMBER(9),
   "WarehouseCode"      VARCHAR2(50),
   "WarehouseName"      VARCHAR2(100),
   "SparePartId"        NUMBER(9),
   "PlannedAmount"      NUMBER(9),
   "ShelvesAmount"      NUMBER(9),
   "SourceBillCode"     VARCHAR2(100),
   "Status"             NUMBER(9),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "BatchNumber"        VARCHAR2(50),
   "RowVersion"         TIMESTAMP,
   constraint PK_PARTSSHELVESTASK primary key ("Id")
)
/
/*==============================================================*/
/* Table: "PartsInboundPerformance"                             */
/*==============================================================*/
create table "PartsInboundPerformance"  (
   "Id"                 NUMBER(9)                       not null,
   "Code"               VARCHAR2(50),
   "InType"             NUMBER(9),
   "BarCode"            VARCHAR2(50),
   "WarehouseAreaId"    NUMBER(9),
   "Qty"                NUMBER(9),
   "OperaterId"         NUMBER(9),
   "OperaterName"       VARCHAR2(100),
   "OperaterBeginTime"  DATE,
   "OperaterEndTime"    DATE,
   "Equipment"          NUMBER(9),
   "ContainerNum"       NUMBER(9),
   constraint PK_PARTSINBOUNDPERFORMANCE primary key ("Id")
)
/