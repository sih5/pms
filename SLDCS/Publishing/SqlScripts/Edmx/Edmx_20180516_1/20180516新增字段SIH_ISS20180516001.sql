alter table "PartsStock" add "LockedQty" NUMBER(9);
alter table "PartsStock" add "OutingQty" NUMBER(9);

alter table "PartsInboundPlan" add "PackingFinishTime" DATE;
alter table "PartsInboundPlan" add "ShelvesFinishTime" DATE;
alter table "PartsInboundPlan" add "ReceivingFinishTime" DATE;