alter table "PartsOutboundPlanDetail" add "DownloadQty" NUMBER(9);

/*==============================================================*/
/* Table: "PickingTask"                                         */
/*==============================================================*/
create table "PickingTask"  (
   "Id"                 NUMBER(9)                       not null,
   "Code"               VARCHAR2(50)                    not null,
   "PartsSalesCategoryId" NUMBER(9)                       not null,
   "WarehouseId"        NUMBER(9),
   "WarehouseCode"      VARCHAR2(50),
   "WarehouseName"      VARCHAR2(100),
   "CounterpartCompanyId" NUMBER(9),
   "CounterpartCompanyCode" VARCHAR2(50),
   "CounterpartCompanyName" VARCHAR2(100),
   "Status"             NUMBER(9),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "PickingFinishTime"  DATE,
   "OrderTypeId"        NUMBER(9),
   "OrderTypeName"      VARCHAR2(100),
   "ShippingMethod"     NUMBER(9),
   "RowVersion"         TIMESTAMP,
   constraint PK_PICKINGTASK primary key ("Id")
)
/

/*==============================================================*/
/* Table: "PickingTaskDetail"                                   */
/*==============================================================*/
create table "PickingTaskDetail"  (
   "Id"                 NUMBER(9)                       not null,
   "PickingTaskId"      NUMBER(9)                       not null,
   "PartsOutboundPlanId" NUMBER(9),
   "PartsOutboundPlanCode" VARCHAR2(50),
   "SourceCode"         VARCHAR2(50),
   "SparePartId"        NUMBER(9)                       not null,
   "SparePartCode"      VARCHAR2(50)                    not null,
   "SparePartName"      VARCHAR2(100)                   not null,
   "MeasureUnit"        VARCHAR2(50),
   "WarehouseAreaId"    NUMBER(9),
   "WarehouseAreaCode"  VARCHAR2(50),
   "PlanQty"            NUMBER(9),
   "PickingQty"         NUMBER(9),
   constraint PK_PICKINGTASKDETAIL primary key ("Id")
)
/
