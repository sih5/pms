alter table "BoxUpTask" add "Remark" VARCHAR2(200);
alter table "BoxUpTask" add "RowVersion" TIMESTAMP;

alter table "PickingTask" add "IsExistShippingOrder" NUMBER(1);