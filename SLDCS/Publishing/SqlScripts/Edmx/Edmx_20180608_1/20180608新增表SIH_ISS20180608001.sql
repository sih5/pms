/*==============================================================*/
/* Table: "PickingTaskBatchDetail"                              */
/*==============================================================*/
create table "PickingTaskBatchDetail"  (
   "Id"                 NUMBER(9)                       not null,
   "PickingTaskDetailId" NUMBER(9)                       not null,
   "BatchNumber"        VARCHAR2(50),
   "PickingQty"         NUMBER(9),
   "PickingTime"        DATE,
   constraint PK_PICKINGTASKBATCHDETAIL primary key ("Id")
)
/