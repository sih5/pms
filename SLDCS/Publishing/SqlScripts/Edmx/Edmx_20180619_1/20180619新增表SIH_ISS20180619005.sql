/*==============================================================*/
/* Table: "AccountPeriod"                                       */
/*==============================================================*/
create table "AccountPeriod"  (
   "Id"                 NUMBER(9)                       not null,
   "Year"               VARCHAR2(20),
   "Month"              VARCHAR2(20),
   "Status"             NUMBER(9),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   constraint PK_ACCOUNTPERIOD primary key ("Id")
)
/