alter table "PartsSalesReturnBillDetail" add "OriginalPrice" NUMBER(19,4);
alter table "PartsInboundCheckBillDetail" add "OriginalPrice" NUMBER(19,4);
alter table "PartsInboundPlanDetail" add "OriginalPrice" NUMBER(19,4);