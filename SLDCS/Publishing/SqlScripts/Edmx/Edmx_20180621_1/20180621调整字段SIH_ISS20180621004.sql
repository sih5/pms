alter table "CustomerAccountHisDetail" add "InvoiceDate" DATE;
alter table "CustomerAccountHistory" add "InvoiceDate" DATE;
alter table "PartsSalesRtnSettlement" add "InvoiceDate" DATE;
alter table "PartsSalesSettlement" add "InvoiceDate" DATE;

alter table "PartsSalesReturnBill" add "DiscountRate" NUMBER(19,2);
alter table "PartsSalesReturnBill" add "InitialApproverId" NUMBER(9);
alter table "PartsSalesReturnBill" add "InitialApproverName" VARCHAR2(100);
alter table "PartsSalesReturnBill" add "InitialApproveTime" DATE;
alter table "PartsSalesReturnBill" add "FinalApproverId" NUMBER(9);
alter table "PartsSalesReturnBill" add "FinalApproverName" VARCHAR2(100);
alter table "PartsSalesReturnBill" add "FinalApproveTime" DATE;
alter table "PartsSalesReturnBill" add "Path" VARCHAR2(2000);