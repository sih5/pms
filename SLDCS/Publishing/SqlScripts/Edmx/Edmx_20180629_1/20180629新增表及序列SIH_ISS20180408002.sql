/*==============================================================*/
/* Table: "PersonnelSupplierRelation"                           */
/*==============================================================*/
create table "PersonnelSupplierRelation"  (
   "Id"                 NUMBER(9)                       not null,
   "PartsSalesCategoryId" NUMBER(9)                       not null,
   "PartsSalesCategoryName" VARCHAR2(100)                   not null,
   "PersonId"           NUMBER(9)                       not null,
   "PersonCode"         VARCHAR2(50),
   "PersonName"         VARCHAR2(100),
   "SupplierId"         NUMBER(9)                       not null,
   "SupplierCode"       VARCHAR2(50),
   "SupplierName"       VARCHAR2(100),
   "Status"             NUMBER(9)                       not null,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "AbandonerId"        NUMBER(9),
   "AbandonerName"      VARCHAR2(100),
   "AbandonTime"        DATE,
   "Remark"             VARCHAR2(500),
   constraint PK_PERSONNELSUPPLIERRELATION primary key ("Id")
)
/