/*==============================================================*/
/* Table: "ClaimApproverStrategy"                               */
/*==============================================================*/
create table "ClaimApproverStrategy"  (
   "Id"                 NUMBER(9)                       not null,
   "Branchid"           NUMBER(9)                       not null,
   "BranchCode"         VARCHAR2(50),
   "BranchName"         VARCHAR2(100),
   "PartsSalesCategoryId" NUMBER(9)                       not null,
   "PartsSalesCategoryName" VARCHAR2(100),
   "FeeFrom"            NUMBER(19,4),
   "FeeTo"              NUMBER(19,4),
   "AfterApproverStatus" NUMBER(9),
   "InitialApproverStatus" NUMBER(9),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   constraint PK_CLAIMAPPROVERSTRATEGY primary key ("Id")
)
/