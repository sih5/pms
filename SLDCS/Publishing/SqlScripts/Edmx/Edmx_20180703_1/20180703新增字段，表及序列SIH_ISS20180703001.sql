alter table "PartsSalesRtnSettlement" add "SettleType" NUMBER(9);
alter table "PartsSalesRtnSettlement" add "BusinessType" NUMBER(9);

/*==============================================================*/
/* Table: "CustomerDirectSpareList"                             */
/*==============================================================*/
create table "CustomerDirectSpareList"  (
   "Id"                 NUMBER(9)                       not null,
   "PartsSalesCategoryId" NUMBER(9)                       not null,
   "PartsSalesCategoryName" VARCHAR2(100),
   "CustomerId"         NUMBER(9)                       not null,
   "CustomerCode"       VARCHAR2(50),
   "CustomerName"       VARCHAR2(100),
   "SparePartId"        NUMBER(9)                       not null,
   "SparePartCode"      VARCHAR2(50),
   "SparePartName"      VARCHAR2(100),
   "MeasureUnit"        VARCHAR2(20),
   "IfDirectProvision"  NUMBER(9),
   "Status"             NUMBER(9),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   constraint PK_CUSTOMERDIRECTSPARELIST primary key ("Id")
)
/

/*==============================================================*/
/* Table: "CustomerSupplyInitialFeeSet"                         */
/*==============================================================*/
create table "CustomerSupplyInitialFeeSet"  (
   "Id"                 NUMBER(9)                       not null,
   "PartsSalesCategoryId" NUMBER(9)                       not null,
   "PartsSalesCategoryName" VARCHAR2(100),
   "CustomerId"         NUMBER(9)                       not null,
   "CustomerCode"       VARCHAR2(50),
   "CustomerName"       VARCHAR2(100),
   "SupplierId"         NUMBER(9)                       not null,
   "SupplierCode"       VARCHAR2(50),
   "SupplierName"       VARCHAR2(100),
   "InitialFee"         NUMBER(19,4),
   "Status"             NUMBER(9),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   constraint PK_CUSTOMERSUPPLYINITIALFEESET primary key ("Id")
)
/