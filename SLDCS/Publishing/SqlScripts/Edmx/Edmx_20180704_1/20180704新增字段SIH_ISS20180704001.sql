alter table "PartsPurchaseOrderType" add "BusinessCode" VARCHAR2(10);
alter table "PartsSalesOrderType" add "BusinessCode" VARCHAR2(10);

alter table "CredenceApplication" add "CreditType" NUMBER(9);
alter table "CustomerAccount" add "TempCreditTotalFee" NUMBER(19,4);
alter table "CustomerTransferBill" add "BusinessType" NUMBER(9);