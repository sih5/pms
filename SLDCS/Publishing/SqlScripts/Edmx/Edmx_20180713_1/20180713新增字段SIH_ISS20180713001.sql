alter table "PartsPurchasePricing" add "LimitQty" NUMBER(9);
alter table "PartsPurchasePricing" add "UsedQty" NUMBER(9);
alter table "PartsPurchasePricingDetail" add "LimitQty" NUMBER(9);