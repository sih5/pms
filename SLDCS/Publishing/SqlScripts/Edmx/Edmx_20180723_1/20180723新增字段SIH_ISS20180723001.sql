alter table "SpecialTreatyPriceChange" add "ValidationTime" DATE;
alter table "SpecialTreatyPriceChange" add "ExpireTime" DATE;
alter table "SpecialTreatyPriceChange" add "InitialApproverId" NUMBER(9);
alter table "SpecialTreatyPriceChange" add "InitialApproverName" VARCHAR2(100);
alter table "SpecialTreatyPriceChange" add "InitialApproveTime" DATE;
alter table "SpecialTreatyPriceChange" add "InitialApproverComment" VARCHAR2(200);
alter table "SpecialTreatyPriceChange" add "CheckerComment" VARCHAR2(200);

alter table "PartsInboundCheckBillDetail" add "BatchNumber" VARCHAR2(50);