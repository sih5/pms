alter table "SpecialPriceChangeList" add "RetailPrice" NUMBER(19,4);
alter table "SpecialPriceChangeList" add "Ratio" NUMBER(15,6);

alter table "PartsPurchasePricingChange" add "Path" VARCHAR2(2000);

alter table "PartsStockHistory" add "LockedQty" NUMBER(9);
alter table "PartsStockHistory" add "OutingQty" NUMBER(9);
alter table "PartsStockHistory" add "OldQuantity" NUMBER(9);

alter table "PartsOuterPurchaseChange" add "Path" VARCHAR2(2000);

alter table "PartsSalesPriceHistory" add "CenterPrice" NUMBER(19,4);
alter table "PartsSalesPriceHistory" add "RetailGuidePrice" NUMBER(19,4);

alter table "DealerPartsInventoryDetail" add "DealerPrice" NUMBER(19,4);