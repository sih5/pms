alter table "PartsBranch" add "BreakTime" DATE;
alter table "PartsBranch" add "IsAccreditPack" NUMBER(1);
alter table "PartsBranch" add "IsAutoCaclSaleProperty" NUMBER(1);

alter table "SparePart" add "Path" VARCHAR2(2000);

alter table "SparePartHistory" add "Path" VARCHAR2(2000);

alter table "BorrowBill" add "ReturnerName" VARCHAR2(100);
alter table "BorrowBill" add "ReturnTime" DATE;