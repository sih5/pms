create table ACCESSINFO_TMP  (
   "Id"                 NUMBER(9)    not null,                   
   "Code"          VARCHAR2(18),
   "Name"          VARCHAR2(100),
   "ReferenceCode" VARCHAR2(125),
   "EnglishName"   VARCHAR2(125),
   "CreateTime"    DATE default sysdate not null,
   constraint PK_ACCESSINFO_TMP primary key ("Id")
)
/