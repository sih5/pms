/*==============================================================*/
/* Table: "FactoryPurchacePrice_Tmp"                            */
/*==============================================================*/
create table "FactoryPurchacePrice_Tmp"  (
   "Id"                 NUMBER(9)                       not null,
   "SupplierCode"       VARCHAR2(50),
   "SapSpareCode"       VARCHAR2(50),
   "ContractPrice"      NUMBER(19,4),
   "ValidFrom"          DATE,
   "ValidTo"            DATE,
   "UserCode"           VARCHAR2(50),
   "CreatetTime"        DATE                           default Sysdate not null,
   "IsGenerate"         NUMBER(1),
   constraint PK_FACTORYPURCHACEPRICE_TMP primary key ("Id")
)
/