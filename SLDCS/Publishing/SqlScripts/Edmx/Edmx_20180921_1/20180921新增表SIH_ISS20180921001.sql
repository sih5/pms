/*==============================================================*/
/* Table: "CustomerTransferBill_Sync"                           */
/*==============================================================*/
create table "CustomerTransferBill_Sync"  (
   "Id"                 NUMBER(9)                       not null,
   "Objid"              VARCHAR2(40),
   "IOStatus"           NUMBER(9),
   "CreateTime"         DATE,
   "FirstSendTime"      DATE,
   "ModifySendTime"     DATE,
   "Token"              VARCHAR2(50),
   "Sender"             VARCHAR2(20),
   "Message"            VARCHAR2(500),
   constraint PK_CUSTOMERTRANSFERBILL_SYNC primary key ("Id")
)
/

/*==============================================================*/
/* Table: "PaymentBill_Sync"                                    */
/*==============================================================*/
create table "PaymentBill_Sync"  (
   "Id"                 NUMBER(9)                       not null,
   "Objid"              VARCHAR2(40),
   "IOStatus"           NUMBER(9),
   "CreateTime"         DATE,
   "FirstSendTime"      DATE,
   "ModifySendTime"     DATE,
   "Token"              VARCHAR2(50),
   "Sender"             VARCHAR2(20),
   "Message"            VARCHAR2(500),
   constraint PK_PAYMENTBILL_SYNC primary key ("Id")
)
/