alter table "PartsShippingOrder" add "PartsSalesOrderTypeId" NUMBER(9);
alter table "PartsShippingOrder" add "PartsSalesOrderTypeName" VARCHAR2(100);
alter table "PartsShippingOrder" add "LinkName" VARCHAR2(100);
alter table "PartsShippingOrder" add "LinkPhone" VARCHAR2(100);