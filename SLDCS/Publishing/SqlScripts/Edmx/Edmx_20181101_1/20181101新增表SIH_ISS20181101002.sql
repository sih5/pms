/*==============================================================*/
/* Table: "PartsPurchaseOrder_Sync"                             */
/*==============================================================*/
create table "PartsPurchaseOrder_Sync"  (
   "Id"                 NUMBER(9)                       not null,
   "Objid"              VARCHAR2(40),
   "IOStatus"           NUMBER(9),
   "CreateTime"         DATE,
   "FirstSendTime"      DATE,
   "ModifySendTime"     DATE,
   "Token"              VARCHAR2(50),
   "Sender"             VARCHAR2(20),
   "Message"            VARCHAR2(500),
   constraint PK_PARTSPURCHASEORDER_SYNC primary key ("Id")
)
/