alter table "PartsInboundPerformance" add "SparePartId" NUMBER(9);
alter table "PartsInboundPerformance" add "SpareCode" VARCHAR2(50);
alter table "PartsInboundPerformance" add "SpareName" VARCHAR2(100);

alter table "PartsOutboundPerformance" add "SparePartId" NUMBER(9);
alter table "PartsOutboundPerformance" add "SpareCode" VARCHAR2(50);
alter table "PartsOutboundPerformance" add "SpareName" VARCHAR2(100);