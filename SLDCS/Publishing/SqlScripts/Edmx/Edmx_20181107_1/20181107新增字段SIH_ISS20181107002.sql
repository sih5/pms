alter table "PartsSalesOrderDetail" add "OrderNo" VARCHAR2(50);

alter table "SupplierShippingDetail" add "Weight" NUMBER(15,6);
alter table "SupplierShippingDetail" add "Volume" NUMBER(15,6);

alter table "SupplierShippingOrder" add "TotalWeight" NUMBER(15,6);
alter table "SupplierShippingOrder" add "TotalVolume" NUMBER(15,6);