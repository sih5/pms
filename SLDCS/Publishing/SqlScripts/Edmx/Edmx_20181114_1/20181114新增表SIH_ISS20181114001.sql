/*==============================================================*/
/* Table: "FundMonthlySettleBill"                               */
/*==============================================================*/
create table "FundMonthlySettleBill"  (
   "Id"                 NUMBER(9)                       not null,
   "MonthlySettleYear"  VARCHAR2(20),
   "MonthlySettleMonth" VARCHAR2(20),
   "Status"             NUMBER(9)                       not null,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   constraint PK_FUNDMONTHLYSETTLEBILL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "FundMonthlySettleBillDetail"                         */
/*==============================================================*/
create table "FundMonthlySettleBillDetail"  (
   "Id"                 NUMBER(9)                       not null,
   "FundMonthlySettleBillId" NUMBER(9)                       not null,
   "AccountGroupId"     NUMBER(9),
   "AccountGroupName"   VARCHAR2(100),
   "CustomerAccountId"  NUMBER(9),
   "CustomerCompanyId"  NUMBER(9),
   "CustomerCompanyCode" VARCHAR2(50),
   "CustomerCompanyName" VARCHAR2(100),
   "InitialAmount"      NUMBER(19,4),
   "ChangeAmount"       NUMBER(19,4),
   "FinalAmount"        NUMBER(19,4),
   "MonthlySettleYear"  VARCHAR2(20),
   "MonthlySettleMonth" VARCHAR2(20),
   constraint PK_FUNDMONTHLYSETTLEBILLDETAIL primary key ("Id")
)
/
