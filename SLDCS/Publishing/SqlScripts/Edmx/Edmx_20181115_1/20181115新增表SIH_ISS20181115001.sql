/*==============================================================*/
/* Table: "PurchaseOrderFinishedDetail"                         */
/*==============================================================*/
create table "PurchaseOrderFinishedDetail"  (
   "Id"                 NUMBER(9)                       not null,
   "PurchaseCode"       VARCHAR2(50),
   "SpareCode"          VARCHAR2(50),
   "PurchasePlanCode"   VARCHAR2(50),
   "SupplierName"       VARCHAR2(100),
   "SupplierPartCode"   VARCHAR2(50),
   "MapName"            VARCHAR2(100),
   "SihCode"            VARCHAR2(50),
   "PartABC"            NUMBER(9),
   "PurchasePlanQty"    NUMBER(9),
   "SupplyConfirmQty"   NUMBER(9),
   "SupplyConfirmReason" VARCHAR2(200),
   "SupplyShippingQty"  NUMBER(9),
   "InboundQty"         NUMBER(9),
   "InboundForceQty"    NUMBER(9),
   "OnLineQty"          NUMBER(9),
   "PurchaseCreateTime" DATE,
   "SupplyConfirmTime"  DATE,
   "SupplyShippingTime" DATE,
   "ExpectDeliveryTime" DATE,
   "InboundTime"        DATE,
   "PlanType"           VARCHAR2(100),
   "WarehouseName"      VARCHAR2(100),
   "SupplyShippingCycle" NUMBER(9),
   "SupplyArrivalCycle" NUMBER(9),
   "TheoryDeliveryTime" DATE,
   "PurchasePlanRemark" VARCHAR2(200),
   "PurchasePlanTime"   DATE,
   constraint PK_PURCHASEORDERFINISHEDDETAIL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "PurchaseOrderUnFinishDetail"                         */
/*==============================================================*/
create table "PurchaseOrderUnFinishDetail"  (
   "Id"                 NUMBER(9)                       not null,
   "PurchaseCode"       VARCHAR2(50),
   "SpareCode"          VARCHAR2(50),
   "PurchasePlanCode"   VARCHAR2(50),
   "SupplierName"       VARCHAR2(100),
   "SupplierPartCode"   VARCHAR2(50),
   "MapName"            VARCHAR2(100),
   "SihCode"            VARCHAR2(50),
   "PartABC"            NUMBER(9),
   "PurchasePlanQty"    NUMBER(9),
   "SupplyConfirmQty"   NUMBER(9),
   "SupplyConfirmReason" VARCHAR2(200),
   "SupplyShippingQty"  NUMBER(9),
   "InboundQty"         NUMBER(9),
   "InboundForceQty"    NUMBER(9),
   "OnLineQty"          NUMBER(9),
   "PurchaseCreateTime" DATE,
   "SupplyConfirmTime"  DATE,
   "SupplyShippingTime" DATE,
   "ExpectDeliveryTime" DATE,
   "InboundTime"        DATE,
   "PlanType"           VARCHAR2(100),
   "WarehouseName"      VARCHAR2(100),
   "SupplyShippingCycle" NUMBER(9),
   "SupplyArrivalCycle" NUMBER(9),
   "TheoryDeliveryTime" DATE,
   "PurchasePlanRemark" VARCHAR2(200),
   "PurchasePlanTime"   DATE,
   constraint PK_PURCHASEORDERUNFINISHDETAIL primary key ("Id")
)
/