alter table "PartsInventoryBill" add "CheckerId" NUMBER(9);
alter table "PartsInventoryBill" add "CheckerName" VARCHAR2(100);
alter table "PartsInventoryBill" add "CheckTime" DATE;