/*==============================================================*/
/* Table: "PostSaleClaims_Tmp"                                  */
/*==============================================================*/
create table "PostSaleClaims_Tmp"  (
   "Id"                 NUMBER(9)                       not null,
   "ClaimBill_Code"     VARCHAR2(30),
   "Dealer_Code"        VARCHAR2(30),
   "CreateTime"         DATE,
   "IsClaim"            NUMBER(9),
   "SparePart_Code"     VARCHAR2(18),
   "SparePart_Name"     VARCHAR2(100),
   "Amount"             NUMBER,
   "Status"             NUMBER(9),
   "Type"               NUMBER(9),
   "TransferTime"       DATE,
   constraint PK_POSTSALECLAIMS_TMP primary key ("Id")
)
/