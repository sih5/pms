alter table "PartsSupplierRelation" add "OrderGoodsCycle" NUMBER(9);

alter table "InternalAllocationBill" add "SourceCode" VARCHAR2(50);

/*==============================================================*/
/* Table: "PartsStandardStockBill"                              */
/*==============================================================*/
create table "PartsStandardStockBill"  (
   "Id"                 NUMBER(9)                       not null,
   "PartId"             NUMBER(9),
   "PartCode"           VARCHAR2(50),
   "PartName"           VARCHAR2(100),
   "StandardQty"        NUMBER(9),
   "DailySaleNum"       NUMBER(9,2),
   "QtyLowerLimit"      NUMBER(9),
   "CreateTime"         DATE,
   "ModifyTime"         DATE,
   constraint PK_PARTSSTANDARDSTOCKBILL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "PartsPurchaseInfoSummary"                            */
/*==============================================================*/
create table "PartsPurchaseInfoSummary"  (
   "Id"                 NUMBER(9)                       not null,
   "PartId"             NUMBER(9),
   "PartCode"           VARCHAR2(50),
   "PartName"           VARCHAR2(100),
   "SupplyOnLineQty"    NUMBER(9),
   "OverdueOnLineQty"   NUMBER(9),
   "WaitInQty"          NUMBER(9),
   "WaitPackQty"        NUMBER(9),
   "WaitShelvesQty"     NUMBER(9),
   "OweOrderNum"        NUMBER(9),
   "OwePartNum"         NUMBER(9),
   "CreateTime"         DATE,
   "ModifyTime"         DATE,
   constraint PK_PARTSPURCHASEINFOSUMMARY primary key ("Id")
)
/

/*==============================================================*/
/* Table: "PartsPriorityBill"                                   */
/*==============================================================*/
create table "PartsPriorityBill"  (
   "Id"                 NUMBER(9)                       not null,
   "PartId"             NUMBER(9),
   "PartCode"           VARCHAR2(50),
   "PartName"           VARCHAR2(100),
   "Priority"           NUMBER(9),
   "PriorityCreateTime" DATE,
   "ActualUseableQty"   NUMBER(9),
   "StockQty"           NUMBER(9),
   "TotalDurativeDayNum" NUMBER(9),
   "CreateTime"         DATE,
   "ModifyTime"         DATE,
   constraint PK_PARTSPRIORITYBILL primary key ("Id")
)
/