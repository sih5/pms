/*==============================================================*/
/* Table: "BottomStockSettleTable"                              */
/*==============================================================*/
create table "BottomStockSettleTable"  (
   "Id"                 NUMBER(9)                       not null,
   "CreateTime"         DATE,
   "DistributionCenterCode" VARCHAR2(50),
   "DistributionCenterName" VARCHAR2(100),
   "CenterCode"         VARCHAR2(50),
   "CenterName"         VARCHAR2(100),
   "DealerCode"         VARCHAR2(50),
   "DealerName"         VARCHAR2(100),
   "AccountSpeciesNum"  NUMBER(9),
   "AccountFee"         NUMBER(19,4),
   "ActualSpeciesNum"   NUMBER(9),
   "ActualFee"          NUMBER(19,4),
   "DifferSpeciesNum"   NUMBER(9),
   constraint PK_BOTTOMSTOCKSETTLETABLE primary key ("Id")
)
/