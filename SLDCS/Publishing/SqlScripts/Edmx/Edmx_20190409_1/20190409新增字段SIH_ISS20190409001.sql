alter table "SparePart" add "IsSupplierPutIn" NUMBER(1);
alter table "SparePartHistory" add "IsSupplierPutIn" NUMBER(1);
alter table "BorrowBill" add "Path" VARCHAR2(2000);