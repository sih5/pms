/*==============================================================*/
/* Table: "ABCSetting"                                          */
/*==============================================================*/
create table "ABCSetting"  (
   "Id"                 NUMBER(9)                       not null,
   "Type"               NUMBER(9),
   "IsAutoAdjust"       NUMBER(1),
   "SaleFrequencyFrom"  NUMBER(15,6),
   "SaleFrequencyTo"    NUMBER(15,6),
   "SaleFeeFrom"        NUMBER(15,6),
   "SaleFeeTo"          NUMBER(15,6),
   "NoSaleMonth"        NUMBER(9),
   "SafeStockCoefficient" NUMBER(15,6),
   "Remark"             VARCHAR2(200),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   constraint PK_ABCSETTING primary key ("Id")
)
/


/*==============================================================*/
/* Table: "OverstockTransferOrder"                              */
/*==============================================================*/
create table "OverstockTransferOrder"  (
   "Id"                 NUMBER(9)                       not null,
   "Code"               VARCHAR2(50)                    not null,
   "TransferInWarehouseId" NUMBER(9),
   "TransferInWarehouseCode" VARCHAR2(50),
   "TransferInWarehouseName" VARCHAR2(50),
   "TransferInCorpId"   NUMBER(9)                       not null,
   "TransferInCorpCode" VARCHAR2(50)                    not null,
   "TransferInCorpName" VARCHAR2(100)                   not null,
   "TransferOutWarehouseId" NUMBER(9)                       not null,
   "TransferOutWarehouseCode" VARCHAR2(50)                    not null,
   "TransferOutWarehouseName" VARCHAR2(50)                    not null,
   "TransferOutCorpId"  NUMBER(9)                       not null,
   "TransferOutCorpCode" VARCHAR2(50)                    not null,
   "TransferOutCorpName" VARCHAR2(100)                   not null,
   "CustomerType"       NUMBER(9),
   "TotalAmount"        NUMBER(19,4),
   "ReceivingAddress"   VARCHAR2(200),
   "ShippingMethod"     NUMBER(9),
   "PromisedDeliveryTime" DATE,
   "StopComment"        VARCHAR2(200),
   "Status"             NUMBER(9)                       not null,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "ApproverId"         NUMBER(9),
   "ApproverName"       VARCHAR2(100),
   "ApproveTime"        DATE,
   "AbandonerId"        NUMBER(9),
   "AbandonerName"      VARCHAR2(100),
   "AbandonTime"        DATE,
   constraint PK_OVERSTOCKTRANSFERORDER primary key ("Id")
)
/

/*==============================================================*/
/* Table: "OverstockTransferOrderDetail"                        */
/*==============================================================*/
create table "OverstockTransferOrderDetail"  (
   "Id"                 NUMBER(9)                       not null,
   "OverstockPartsTransferOrderId" NUMBER(9)                       not null,
   "SparePartId"        NUMBER(9)                       not null,
   "SparePartCode"      VARCHAR2(50)                    not null,
   "SparePartName"      VARCHAR2(100)                   not null,
   "TransferQty"        NUMBER(9),
   "EnoughQty"          NUMBER(9),
   "RetailPrice"        NUMBER(19,4),
   "DiscountRate"       NUMBER(15,6),
   "DiscountedPrice"    NUMBER(19,4),
   constraint PK_OVERSTOCKTRANSFERORDERDETAI primary key ("Id")
)
/
