/*==============================================================*/
/* Table: "ABCTypeSafeDays"                                     */
/*==============================================================*/
create table "ABCTypeSafeDays"  (
   "Id"                 NUMBER(9)                       not null,
   "ProductType"        NUMBER(9)                       not null,
   "Days"               NUMBER(9),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   constraint PK_ABCTYPESAFEDAYS primary key ("Id")
)
/

/*==============================================================*/
/* Table: "PartsStockCoefficient"                               */
/*==============================================================*/
create table "PartsStockCoefficient"  (
   "Id"                 NUMBER(9)                       not null,
   "Type"               NUMBER(9)                       not null,
   "ProductType"        NUMBER(9)                       not null,
   "Status"             NUMBER(9)                       not null,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   constraint PK_PARTSSTOCKCOEFFICIENT primary key ("Id")
)
/

/*==============================================================*/
/* Table: "PartsStockCoefficientPrice"                          */
/*==============================================================*/
create table "PartsStockCoefficientPrice"  (
   "Id"                 NUMBER(9)                       not null,
   "PartsStockCoefficientId" NUMBER(9)                       not null,
   "MinPrice"           NUMBER(19,4),
   "MaxPrice"           NUMBER(19,4),
   "Coefficient"        NUMBER(15,6),
   constraint PK_PARTSSTOCKCOEFFICIENTPRICE primary key ("Id")
)
/