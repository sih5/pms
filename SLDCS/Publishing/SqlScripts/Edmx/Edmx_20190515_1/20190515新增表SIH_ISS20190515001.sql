/*==============================================================*/
/* Table: "PartsSalesWeeklyBase"                                */
/*==============================================================*/
create table "PartsSalesWeeklyBase"  (
   "Id"                 NUMBER(9)                       not null,
   "SparePartId"        NUMBER(9),
   "SparePartCode"      VARCHAR2(50),
   "SparePartName"      VARCHAR2(100),
   "Year"               NUMBER(9),
   "Week"               NUMBER(9),
   "WarehouseId"        NUMBER(9),
   "WarehouseCode"      VARCHAR2(50),
   "WarehouseName"      VARCHAR2(50),
   "PartsSalesOrderTypeId" NUMBER(9),
   "PartsSalesOrderTypeName" VARCHAR2(100),
   "Quantity"           NUMBER(9),
   "CreateTime"         DATE,
   constraint PK_PARTSSALESWEEKLYBASE primary key ("Id")
)
/