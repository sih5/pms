alter table "Company" add "OrderCycle" NUMBER(9);
alter table "Company" add "ShippingCycle" NUMBER(9);
alter table "Company" add "ArrivalCycle" NUMBER(9);

alter table "ABCSetting" add "IsReserve" NUMBER(1);