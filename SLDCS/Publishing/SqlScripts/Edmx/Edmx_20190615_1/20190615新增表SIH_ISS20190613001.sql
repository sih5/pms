/*==============================================================*/
/* Table: "InOutWorkPerHour"                                    */
/*==============================================================*/
create table "InOutWorkPerHour"  (
   "Id"                 NUMBER(9)                       not null,
   "Date"               DATE,
   "TimeFrame"          VARCHAR2(50),
   "InPersonNum"        NUMBER(9),
   "InOrderLineNum"     NUMBER(9),
   "InOrderItemNum"     NUMBER(9),
   "InQty"              NUMBER(9),
   "InFee"              NUMBER(19,4),
   "PackingPersonNum"   NUMBER(9),
   "PackingLineNum"     NUMBER(9),
   "PackingItemNum"     NUMBER(9),
   "PackingQty"         NUMBER(9),
   "PackingFee"         NUMBER(19,4),
   "ShelvePersonNum"    NUMBER(9),
   "ShelveLineNum"      NUMBER(9),
   "ShelveItemNum"      NUMBER(9),
   "ShelveQty"          NUMBER(9),
   "ShelveFee"          NUMBER(19,4),
   "UnShelvePersonNum"  NUMBER(9),
   "UnShelveLineNum"    NUMBER(9),
   "UnShelveItemNum"    NUMBER(9),
   "UnShelveQty"        NUMBER(9),
   "UnShelveFee"        NUMBER(19,4),
   "CreateTime"         DATE,
   constraint PK_INOUTWORKPERHOUR primary key ("Id")
)
/

/*==============================================================*/
/* Table: "WorkGruopPersonNumber"                               */
/*==============================================================*/
create table "WorkGruopPersonNumber"  (
   "Id"                 NUMBER(9)                       not null,
   "WorkGruop"          NUMBER(9),
   "EmployeeNumber"     NUMBER(9),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   constraint PK_WORKGRUOPPERSONNUMBER primary key ("Id")
)
/