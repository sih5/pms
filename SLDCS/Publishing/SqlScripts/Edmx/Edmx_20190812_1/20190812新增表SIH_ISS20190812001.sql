/*==============================================================*/
/* Table: "PartsPriceTimeLimit"                                 */
/*==============================================================*/
create table "PartsPriceTimeLimit"  (
   "Id"                 NUMBER(9)                       not null,
   "Type"               NUMBER(9)                       not null,
   "TimeLimit"          NUMBER(9),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   constraint PK_PARTSPRICETIMELIMIT primary key ("Id")
)
/