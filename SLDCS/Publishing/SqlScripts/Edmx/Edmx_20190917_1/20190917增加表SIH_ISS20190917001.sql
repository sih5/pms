/*==============================================================*/
/* Table: "BottomStockForceReserveSub"                          */
/*==============================================================*/
create table "BottomStockForceReserveSub"  (
   "Id"                 NUMBER(9)                       not null,
   "ReserveType"        VARCHAR2(100),
   "SubCode"            VARCHAR2(50),
   "SubName"            VARCHAR2(100),
   "Serial"             NUMBER(9),
   "Status"             NUMBER(9)                       not null,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   constraint PK_BOTTOMSTOCKFORCERESERVESUB primary key ("Id")
)
/

/*==============================================================*/
/* Table: "BottomStockForceReserveType"                         */
/*==============================================================*/
create table "BottomStockForceReserveType"  (
   "Id"                 NUMBER(9)                       not null,
   "ReserveType"        VARCHAR2(100),
   "Status"             NUMBER(9)                       not null,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   constraint PK_BOTTOMSTOCKFORCERESERVETYPE primary key ("Id")
)
/

/*==============================================================*/
/* Table: "BottomStockColVersion"                               */
/*==============================================================*/
create table "BottomStockColVersion"  (
   "Id"                 NUMBER(9)                       not null,
   "CompanyId"          NUMBER(9)                       not null,
   "CompanyCode"        VARCHAR2(50)                    not null,
   "CompanyName"        VARCHAR2(100)                   not null,
   "CompanyType"        NUMBER(9)                       not null,
   "SparePartId"        NUMBER(9)                       not null,
   "SparePartCode"      VARCHAR2(50)                    not null,
   "SparePartName"      VARCHAR2(100)                   not null,
   "ColVersionCode"     VARCHAR2(50),
   "SalePrice"          NUMBER(19,4),
   "ReserveFee"         NUMBER(19,4),
   "ReserveQty"         NUMBER(9),
   "StartTime"          DATE,
   "EndTime"            DATE,
   "Status"             NUMBER(9)                       not null,
   "CreateTime"         DATE,
   constraint PK_BOTTOMSTOCKCOLVERSION primary key ("Id")
)
/

/*==============================================================*/
/* Table: "BottomStockSubVersion"                               */
/*==============================================================*/
create table "BottomStockSubVersion"  (
   "Id"                 NUMBER(9)                       not null,
   "CompanyId"          NUMBER(9)                       not null,
   "CompanyCode"        VARCHAR2(50)                    not null,
   "CompanyName"        VARCHAR2(100)                   not null,
   "CompanyType"        NUMBER(9)                       not null,
   "SparePartId"        NUMBER(9)                       not null,
   "SparePartCode"      VARCHAR2(50)                    not null,
   "SparePartName"      VARCHAR2(100)                   not null,
   "SubVersionCode"     VARCHAR2(50),
   "ColVersionCode"     VARCHAR2(50),
   "SalePrice"          NUMBER(19,4),
   "ReserveFee"         NUMBER(19,4),
   "ReserveQty"         NUMBER(9),
   "StartTime"          DATE,
   "EndTime"            DATE,
   "Status"             NUMBER(9)                       not null,
   "CreateTime"         DATE,
   constraint PK_BOTTOMSTOCKSUBVERSION primary key ("Id")
)
/

/*==============================================================*/
/* Table: "DealerFormat"                                        */
/*==============================================================*/
create table "DealerFormat"  (
   "Id"                 NUMBER(9)                       not null,
   "DealerId"           NUMBER(9)                       not null,
   "DealerCode"         VARCHAR2(50)                    not null,
   "DealerName"         VARCHAR2(100)                   not null,
   "Format"             VARCHAR2(200),
   "Quarter"            VARCHAR2(50),
   "Status"             NUMBER(9)                       not null,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "AbandonerId"        NUMBER(9),
   "AbandonerName"      VARCHAR2(100),
   "AbandonTime"        DATE,
   constraint PK_DEALERFORMAT primary key ("Id")
)
/

/*==============================================================*/
/* Table: "ForceReserveBill"                                    */
/*==============================================================*/
create table "ForceReserveBill"  (
   "Id"                 NUMBER(9)                       not null,
   "CompanyId"          NUMBER(9)                       not null,
   "CompanyCode"        VARCHAR2(50)                    not null,
   "CompanyName"        VARCHAR2(100)                   not null,
   "CompanyType"        NUMBER(9)                       not null,
   "SubVersionCode"     VARCHAR2(50),
   "ColVersionCode"     VARCHAR2(50),
   "Status"             NUMBER(9)                       not null,
   "IsAttach"           NUMBER(1),
   "ReserveType"        VARCHAR2(100),
   "ReserveTypeSubItem" VARCHAR2(100),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "CheckerId"          NUMBER(9),
   "CheckerName"        VARCHAR2(100),
   "CheckTime"          DATE,
   "ApproverId"         NUMBER(9),
   "ApproverName"       VARCHAR2(100),
   "ApproveTime"        DATE,
   "RejecterId"         NUMBER(9),
   "RejecterName"       VARCHAR2(100),
   "RejectTime"         DATE,
   "AbandonerId"        NUMBER(9),
   "AbandonerName"      VARCHAR2(100),
   "AbandonTime"        DATE,
   constraint PK_FORCERESERVEBILL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "ForceReserveBillDetail"                              */
/*==============================================================*/
create table "ForceReserveBillDetail"  (
   "Id"                 NUMBER(9)                       not null,
   "ForceReserveBillId" NUMBER(9)                       not null,
   "SparePartId"        NUMBER(9)                       not null,
   "SparePartCode"      VARCHAR2(50)                    not null,
   "SparePartName"      VARCHAR2(100)                   not null,
   "SuggestForceReserveQty" NUMBER(9),
   "ForceReserveQty"    NUMBER(9),
   "ReserveFee"         NUMBER(19,4),
   "CenterPartProperty" NUMBER(9),
   constraint PK_FORCERESERVEBILLDETAIL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "VersionInfo"                                         */
/*==============================================================*/
create table "VersionInfo"  (
   "Id"                 NUMBER(9)                       not null,
   "CompanyId"          NUMBER(9)                       not null,
   "CompanyCode"        VARCHAR2(50)                    not null,
   "Type"               NUMBER(9),
   "SerialCode"         NUMBER(9),
   "BottomStockForceReserveSubId" NUMBER(9),
   constraint PK_VERSIONINFO primary key ("Id")
)
/
