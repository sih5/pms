/*==============================================================*/
/* Table: "SalesInvoice_tmp"                                    */
/*==============================================================*/
create table "SalesInvoice_tmp"  (
   "Id"                 NUMBER(9)                       not null,
   "VoucherCode"        VARCHAR2(50),
   "InvoiceCode"        VARCHAR2(100),
   "CreateTime"         DATE,
   constraint PK_SALESINVOICE_TMP primary key ("Id")
)
/