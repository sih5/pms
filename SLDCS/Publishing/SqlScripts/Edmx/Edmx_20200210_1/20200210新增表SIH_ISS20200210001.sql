/*==============================================================*/
/* Table: "PartsDifferenceBackBill"                             */
/*==============================================================*/
create table "PartsDifferenceBackBill"  (
   "Id"                 NUMBER(9)                       not null,
   "Code"               VARCHAR2(50)                    not null,
   "Status"             NUMBER(9)                       not null,
   "Type"               NUMBER(9)                       not null,
   "SourceCode"         VARCHAR2(50)                    not null,
   "PartsInboundCheckBillCode" VARCHAR2(50)                    not null,
   "SubmitterId"        NUMBER(9),
   "SubmitterName"      VARCHAR2(100),
   "SubmitTime"         DATE,
   "InitialApproverId"  NUMBER(9),
   "InitialApproverName" VARCHAR2(100),
   "InitialApproveTime" DATE,
   "CheckerId"          NUMBER(9),
   "CheckerName"        VARCHAR2(100),
   "CheckTime"          DATE,
   "ApproverId"         NUMBER(9),
   "ApproverName"       VARCHAR2(100),
   "ApproveTime"        DATE,
   "RejecterId"         NUMBER(9),
   "RejecterName"       VARCHAR2(100),
   "RejectTime"         DATE,
   "AbandonerId"        NUMBER(9),
   "AbandonerName"      VARCHAR2(100),
   "AbandonTime"        DATE,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "RowVersion"         TIMESTAMP,
   constraint PK_PARTSDIFFERENCEBACKBILL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "PartsDifferenceBackBillDtl"                          */
/*==============================================================*/
create table "PartsDifferenceBackBillDtl"  (
   "Id"                 NUMBER(9)                       not null,
   "PartsDifferenceBackBillId" NUMBER(9)                       not null,
   "SparePartId"        NUMBER(9)                       not null,
   "SparePartCode"      VARCHAR2(50)                    not null,
   "SparePartName"      VARCHAR2(100)                   not null,
   "DiffQuantity"       NUMBER(9)                       not null,
   "ErrorType"          NUMBER(9)                       not null,
   constraint PK_PARTSDIFFERENCEBACKBILLDTL primary key ("Id")
)
/