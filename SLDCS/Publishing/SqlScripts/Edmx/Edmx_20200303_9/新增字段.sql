alter table "DealerRetailReturnBillDetail" add "SIHLabelCode"  VARCHAR2(50);
alter table "DealerRetailReturnBillDetail" add "TraceProperty"  NUMBER(9);

alter table "DealerRetailOrderDetail" add "SIHLabelCode"  VARCHAR2(50);
alter table "DealerRetailOrderDetail" add "TraceProperty"  NUMBER(9);