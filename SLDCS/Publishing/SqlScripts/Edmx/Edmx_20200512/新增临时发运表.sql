/*==============================================================*/ 
/* Table: "TemShippingOrderDetail"                              */ 
/*==============================================================*/ 
create table "TemShippingOrderDetail"  ( 
   "Id"                 NUMBER(9)                       not null, 
   "ShippingOrderId"    NUMBER(9)                       not null, 
   "SparePartId"        NUMBER(9)                       not null, 
   "SparePartCode"      VARCHAR2(50)                    not null, 
   "SparePartName"      VARCHAR2(100)                   not null, 
   "SupplierPartCode"   VARCHAR2(50), 
   "MeasureUnit"        VARCHAR2(50), 
   "Quantity"           NUMBER(9)                       not null, 
   "ConfirmedAmount"    NUMBER(9), 
   "Remark"             VARCHAR2(200), 
   "Weight"             NUMBER(15,6), 
   "Volume"             NUMBER(15,6), 
   constraint PK_TEMSHIPPINGORDERDETAIL primary key ("Id") 
) 
/ 
 
/*==============================================================*/ 
/* Table: "TemSupplierShippingOrder"                            */ 
/*==============================================================*/ 
create table "TemSupplierShippingOrder"  ( 
   "Id"                 NUMBER(9)                       not null, 
   "Code"               VARCHAR2(50)                    not null, 
   "PartsSupplierId"    NUMBER(9)                       not null, 
   "PartsSupplierCode"  VARCHAR2(50)                    not null, 
   "PartsSupplierName"  VARCHAR2(100)                   not null, 
   "ReceivingWarehouseId" NUMBER(9), 
   "ReceivingWarehouseName" VARCHAR2(100), 
   "ReceivingCompanyId" NUMBER(9), 
   "ReceivingCompanyName" VARCHAR2(100), 
   "ReceivingCompanyCode" VARCHAR2(100), 
   "ReceivingAddress"   VARCHAR2(200), 
   "OriginalRequirementBillId" NUMBER(9)                       not null, 
   "OriginalRequirementBillCode" VARCHAR2(50)                    not null, 
   "LogisticCompany"    VARCHAR2(100), 
   "Phone"              VARCHAR2(50), 
   "VehicleLicensePlate" VARCHAR2(50), 
   "Driver"             VARCHAR2(50), 
   "ShippingMethod"     NUMBER(9)                       not null, 
   "DeliveryBillNumber" VARCHAR2(50), 
   "TotalWeight"        NUMBER(15,6), 
   "TotalVolume"        NUMBER(15,6), 
   "Status"             NUMBER(9)                       not null, 
   "ShippingDate"       DATE, 
   "PlanDeliveryTime"   DATE                            not null, 
   "ArrivalDate"        DATE, 
   "LogisticArrivalDate" DATE, 
   "ConfirmorId"        NUMBER(9), 
   "ConfirmorName"      VARCHAR2(100), 
   "ConfirmationTime"   DATE, 
   "FirstConfirmationTime" DATE, 
   "Remark"             VARCHAR2(200), 
   "CreatorId"          NUMBER(9), 
   "CreatorName"        VARCHAR2(100), 
   "CreateTime"         DATE, 
   "ModifierId"         NUMBER(9), 
   "ModifierName"       VARCHAR2(100), 
   "ModifyTime"         DATE, 
   "CloserId"           NUMBER(9), 
   "CloserName"         VARCHAR2(100), 
   "CloseTime"          DATE, 
   "TemPurchaseOrderCode" VARCHAR2(50), 
   "TemPurchaseOrderId" NUMBER(9), 
   "ForceId"            NUMBER(9), 
   "ForceName"          VARCHAR2(50), 
   "ForceTime"          DATE, 
   "Path"               VARCHAR2(2000),
   "OrderCompanyId"     NUMBER(9), 
   "OrderCompanyCode"   VARCHAR2(50), 
   "OrderCompanyName"   VARCHAR2(100), 
   constraint PK_TEMSUPPLIERSHIPPINGORDER primary key ("Id") 
) 
/ 