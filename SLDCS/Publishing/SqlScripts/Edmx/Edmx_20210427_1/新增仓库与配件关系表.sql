declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"WarehousePartRelation"');
  if num>0 then
    execute immediate 'drop table "WarehousePartRelation" cascade constraints';
  end if;
end;

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_WarehousePartRelation"');
  if num>0 then
    execute immediate 'drop sequence "S_WarehousePartRelation"';
  end if;
end;

create sequence "S_WarehousePartRelation";

/*==============================================================*/
/* Table: "WarehousePartRelation"                               */
/*==============================================================*/
create table "WarehousePartRelation"  (
   "Id"                 NUMBER(9)                       not null,
   "Status"             NUMBER(9)                       not null,
   "WarehouseId"        NUMBER(9)                       not null,
   "WarehouseCode"      VARCHAR2(50),
   "WarehouseName"      VARCHAR2(100),
   "PartId"             NUMBER(9)                       not null,
   "PartCode"           VARCHAR2(50),
   "PartName"           VARCHAR2(100),
   "IsMarketable"       NUMBER(1),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "FrozenTime"         DATE,
   "FrozenName"         VARCHAR2(100),
   "AbandonerName"      VARCHAR2(100),
   "AbandonTime"        DATE,
   "RecoveryTime"       DATE,
   "RecoveryName"       VARCHAR2(100),
   constraint PK_WAREHOUSEPARTRELATION primary key ("Id")
)