declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"LogisticsTime"');
  if num>0 then
    execute immediate 'drop table "LogisticsTime" cascade constraints';
  end if;
end;

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_LogisticsTime"');
  if num>0 then
    execute immediate 'drop sequence "S_LogisticsTime"';
  end if;
end;


create sequence "S_LogisticsTime";

/*==============================================================*/
/* Table: "LogisticsTime"                                       */
/*==============================================================*/
create table "LogisticsTime"  (
   "Id"                 NUMBER(9)                       not null,
   "Status"             NUMBER(9)                       not null,
   "CompanyId"          NUMBER(9)                       not null,
   "CompanyCode"        VARCHAR2(50),
   "CompanyName"        VARCHAR2(100),
   "CompanyType"        NUMBER(9),
   "LogisticCompanyId"  NUMBER(9)                       not null,
   "LogisticCompanyCode" VARCHAR2(50),
   "LogisticCompanyName" VARCHAR2(100),
   "ShippingWarehouseId" NUMBER(9)                       not null,
   "ShippingWarehouseName" VARCHAR2(100),
   "ShippingWarehouseCode" VARCHAR2(50),
   "ReceivingWarehouseId" NUMBER(9),
   "ReceivingWarehouseCode" VARCHAR2(50),
   "ReceivingWarehouseName" VARCHAR2(100),
   "ReceivingAddress"   VARCHAR2(200),
   "Day"                NUMBER(9)                       not null,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "AbandonerId"        NUMBER(9),
   "AbandonerName"      VARCHAR2(100),
   "AbandonTime"        DATE,
   "RowVersion"         TIMESTAMP,
   constraint PK_LOGISTICSTIME primary key ("Id")
);