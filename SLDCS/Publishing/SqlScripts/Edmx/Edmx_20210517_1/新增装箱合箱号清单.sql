alter  table "BoxUpTask"  add("CarrierId"   NUMBER(9));
alter  table "BoxUpTask"  add("CarrierCode"   VARCHAR2(50));
alter  table "BoxUpTask"  add("CarrierName"   VARCHAR2(100));

drop table "BoxUpTaskMergeDetail";
drop sequence "S_BoxUpTaskMergeDetail";
create sequence "S_BoxUpTaskMergeDetail";

create table "BoxUpTaskMergeDetail"  (
   "Id"                 NUMBER(9)                       not null,
   "BoxUpTaskDetailId"  NUMBER(9)                       not null,
   "MergeCaseNumber"    VARCHAR2(100)                   not null,
   "BoxUpQty"           NUMBER(9)                       not null,
   constraint PK_BOXUPTASKMERGEDETAIL primary key ("Id")
);