create sequence "S_SupplierReserveRecommendPlan";

/*==============================================================*/
/* Table: "SupplierReserveRecommendPlan"                        */
/*==============================================================*/
create table "SupplierReserveRecommendPlan"  (
   "Id"                 NUMBER(9)                       not null,
   "PartsSupplierCode"  VARCHAR2(50)                    not null,
   "PartsSupplierName"  VARCHAR2(100)                   not null,
   "SupplierPartCode"   VARCHAR2(50)                    not null,
   "PartCode"           VARCHAR2(50)                    not null,
   "PartName"           VARCHAR2(100)                   not null,
   "IsPrimary"          NUMBER(1),
   "IsDirectSupply"     NUMBER(1),
   "IsOrderable"        NUMBER(1),
   "PackingCoefficient" NUMBER(9),
   "IsPurchasePricing"  NUMBER(1),
   "CustomerCount"      NUMBER(9),
   "DaysAvg"            NUMBER(9),
   "LowerLimit"         NUMBER(9),
   "StandStock"         NUMBER(9),
   "UpperLimit"         NUMBER(9),
   "ActQty"             NUMBER(9),
   "NoShippingQuantity" NUMBER(9),
   "NoInReserveQuantity" NUMBER(9),
   "Quantity"           NUMBER(9),
   "RecommendQuantity"  NUMBER(9),
   constraint PK_SUPPLIERRESERVERECOMMENDPLA primary key ("Id")
);