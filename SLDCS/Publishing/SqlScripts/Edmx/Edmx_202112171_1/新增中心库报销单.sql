create sequence "S_CenterReimbursement";

create table "CenterReimbursement"  (
   "Id"                 NUMBER(9)                       not null,
   "Code"               VARCHAR2(50)                    not null,
   "MarketingDepartmentId" NUMBER(9),
   "MarketingDepartmentName" VARCHAR2(100),
   "CenterId"           NUMBER(9),
   "CenterCode"         VARCHAR2(50),
   "CenterName"         VARCHAR2(100),
   "Type"               NUMBER(9),
   "Reason"             VARCHAR2(500),
   "DealerName"         VARCHAR2(100),
   "OccurrenceTime"     DATE,
   "Amount"             NUMBER(19,4),
   "Remark"             VARCHAR2(2000),
   "Status"             NUMBER(9)                       not null,
   "CreatorId"          NUMBER(9)                       not null,
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "SubmitterId"        NUMBER(9),
   "SubmitterName"      VARCHAR2(100),
   "SubmitTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "InitialApproverId"  NUMBER(9),
   "InitialApproverName" VARCHAR2(100),
   "InitialApproveTime" DATE,
   "CheckerId"          NUMBER(9),
   "CheckerName"        VARCHAR2(100),
   "CheckTime"          DATE,
   "ApproverId"         NUMBER(9),
   "ApproverName"       VARCHAR2(100),
   "ApproveTime"        DATE,
   "ApproverComment"    VARCHAR2(500),
   "StoperId"           NUMBER(9),
   "Stoper"             VARCHAR2(100),
   "StopTime"           DATE,
   "AbandonerId"        NUMBER(9),
   "AbandonerName"      VARCHAR2(100),
   "AbandonTime"        DATE,
   "RowVersion"         TIMESTAMP,
   "Path"               VARCHAR2(2000),
   constraint PK_CENTERREIMBURSEMENT primary key ("Id")
);