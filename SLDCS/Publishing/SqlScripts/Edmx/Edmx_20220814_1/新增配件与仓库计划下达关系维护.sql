create sequence "S_SmartWarehousePartRelation";

create table "SmartWarehousePartRelation"  (
   "Id"                 NUMBER(9)                       not null,
   "Status"             NUMBER(9)                       not null,
   "WarehouseId"        NUMBER(9)                       not null,
   "WarehouseCode"      VARCHAR2(50),
   "WarehouseName"      VARCHAR2(100),
   "PartId"             NUMBER(9)                       not null,
   "PartCode"           VARCHAR2(50),
   "PartName"           VARCHAR2(100),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "AbandonerId"        NUMBER(9),
   "AbandonerName"      VARCHAR2(100),
   "AbandonTime"        DATE,
   "RowVersion"         TIMESTAMP,
   constraint PK_SMARTWAREHOUSEPARTRELATION primary key ("Id")
);

alter table "SIHSmartOrderBase" add "LowerLimitDays" number(9);
alter table "ReserveFactorMasterOrder" add "LowerLimitDays" number(9);
alter table "SIHRecommendPlan" add "LowerLimitDays" number(9);