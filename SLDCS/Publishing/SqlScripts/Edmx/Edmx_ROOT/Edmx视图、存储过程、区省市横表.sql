--区省市横表
CREATE TABLE "TiledRegion"
( "Id" NUMBER(9) NOT NULL, 
  "RegionName" VARCHAR2(100), 
  "ProvinceName" VARCHAR2(100), 
  "CityName" VARCHAR2(100), 
  "CountyName" VARCHAR2(100),
  constraint PK_TILEDREGION primary key ("Id")
);
/
-- 配件索赔价
/
create or replace view "PartsClaimPrice"  as
select "PartsBranch"."BranchId" as "BranchId",
       "SparePart"."Id" as "UsedPartsId",
       "SparePart"."Code" as "UsedPartsCode",
       "SparePart"."Name" as "UsedPartsName",
       "PartsBranch"."PartsWarrantyCategoryId",
       "PartsBranch"."PartsWarrantyCategoryName",
       "PartsBranch"."PartsSalesCategoryId",
       "PartsSupplierRelation"."SupplierId" as "PartsSupplierId",
       "PartsSupplier"."Code" as "PartsSupplierCode",
       "PartsSupplier"."Name" as "PartsSupplierName",
       "PartsSalesPrice"."SalesPrice",
       "SparePart"."MeasureUnit",
       "PartsBranch"."PartsReturnPolicy" as "UsedPartsReturnPolicy",
       "SparePart"."Specification",
       "SparePart"."Feature",
       --"PartsWarrantyTerm"."PartsWarrantyType",
       "SparePart"."PartType",
       --"WarrantyPolicy"."Category" as "WarrantyPolicyCategory",
        "PartDeleaveInformation"."DeleaveAmount" as "DeleaveAmount",
        "PartDeleaveInformation"."DeleavePartCode" as "DeleavePartCode",
         "DealerPartsStock"."DealerId" as "DealerId",
        "DealerPartsStock"."SubDealerId" as "SubDealerId",
       nvl("DealerPartsStock"."Quantity",0) as "Quantity"
  from "SparePart"
 inner join "PartsSalesPrice" on "PartsSalesPrice"."SparePartId" = "SparePart"."Id"
 inner join "PartsBranch" on "SparePart"."Id" = "PartsBranch"."PartId"
 inner join "PartsSupplierRelation" on "PartsSupplierRelation"."PartId" = "SparePart"."Id"
 inner join "PartsSupplier" on "PartsSupplier"."Id" = "PartsSupplierRelation"."SupplierId"
 --left join "PartsWarrantyTerm" on "PartsWarrantyTerm"."PartsWarrantyCategoryId" = "PartsBranch"."PartsWarrantyCategoryId"
 --left join "WarrantyPolicy" on "PartsWarrantyTerm"."WarrantyPolicyId"= "WarrantyPolicy"."Id"
 left join "PartDeleaveInformation" on "PartDeleaveInformation"."OldPartId"="SparePart"."Id" and "PartDeleaveInformation"."BranchId"="PartsBranch"."BranchId" and "PartDeleaveInformation"."PartsSalesCategoryId"="PartsBranch"."PartsSalesCategoryId"
 left join "DealerPartsStock" on "DealerPartsStock"."SparePartId"="SparePart"."Id";

/

--保外配件索赔价
/
CREATE or replace view "PartsClaimPriceForbw" AS
SELECT "PartsBranch"."BranchId" as "BranchId",
       "SparePart"."Id" as "UsedPartsId",
       "SparePart"."Code" as "UsedPartsCode",
       "SparePart"."Name" as "UsedPartsName",
       "PartsBranch"."PartsWarrantyCategoryId",
       "PartsBranch"."PartsWarrantyCategoryName",
       "PartsBranch"."PartsSalesCategoryId",
       "PartsSupplierRelation"."SupplierId" as "PartsSupplierId",
       "PartsSupplier"."Code" as "PartsSupplierCode",
       "PartsSupplier"."Name" as "PartsSupplierName",
       "PartsSalesPrice"."SalesPrice",
       "SparePart"."MeasureUnit",
       "PartsBranch"."PartsReturnPolicy" as "UsedPartsReturnPolicy",
       "SparePart"."Specification",
       "SparePart"."Feature",
       --"PartsWarrantyTerm"."PartsWarrantyType",
       "SparePart"."PartType",
       --"WarrantyPolicy"."Category" as "WarrantyPolicyCategory",
        "PartDeleaveInformation"."DeleaveAmount" as "DeleaveAmount",
        "PartDeleaveInformation"."DeleavePartCode" as "DeleavePartCode"
  from "SparePart"
 LEFT join "PartsSalesPrice" on "PartsSalesPrice"."SparePartId" = "SparePart"."Id"
 inner join "PartsBranch" on "SparePart"."Id" = "PartsBranch"."PartId"   and "PartsBranch".
 "PartsSalesCategoryId" = "PartsSalesPrice"."PartsSalesCategoryId"
 LEFT join "PartsSupplierRelation" on "PartsSupplierRelation"."PartId" = "SparePart"."Id"  and "PartsBranch".
 "PartsSalesCategoryId" = "PartsSalesPrice"."PartsSalesCategoryId"
LEFT join "PartsSupplier" on "PartsSupplier"."Id" = "PartsSupplierRelation"."SupplierId"
 --left join "PartsWarrantyTerm" on "PartsWarrantyTerm"."PartsWarrantyCategoryId" = "PartsBranch"."PartsWarrantyCategoryId"
 --left join "WarrantyPolicy" on "PartsWarrantyTerm"."WarrantyPolicyId"= "WarrantyPolicy"."Id"
 left join "PartDeleaveInformation" on "PartDeleaveInformation"."OldPartId"="SparePart"."Id" and "PartDeleaveInformation"."BranchId"="PartsBranch"."BranchId" and "PartDeleaveInformation"."PartsSalesCategoryId"="PartsBranch"."PartsSalesCategoryId"
 where "SparePart"."Status" = 1
   and "PartsBranch"."Status" = 1
   and "PartsSupplierRelation"."Status" = 1
   and "PartsSalesPrice"."Status" = 1
   and "PartsSupplier"."Status" = 1
      --and PartsSalesPrice.Ifclaim = 1
   and exists
 (select *
          from "PartsSalesCategory"
         where "PartsSalesCategory"."Id" = "PartsSalesPrice"."PartsSalesCategoryId"
           and "Status" = 1);
/
-- 责任单位产品关系
/
create or replace view "ResponsibleUnitAffiProduct" as
select "ResponsibleUnitId",
       "ProductId"
  from "ResponsibleUnitProductDetail"
/

-- 服务产品线产品关系
/
create or replace view "ServProdLineAffiProduct" as
select "Product"."Id"                                 as "ProductId",
       "EngineModelProductLine"."EngineProductLineId" as "ServiceProductLineId",
       2                                          as "ProductLineType"
  from "EngineModelProductLine"
 inner join "EngineModel"
    on "EngineModelProductLine"."EngineModeId" = "EngineModel"."Id"
 inner join "Product"
    on "Product"."EngineTypeCode" = "EngineModel"."EngineModel";
/


-- 车辆种类产品关系
/
create or replace view "VehicleCategoryAffiProduct" as
select "VehicleCategoryId",
       "ProductId"
  from "VehicleCategoryProductDetail"
/

-- 销售车型与产品关系(已弃用)
/
create or replace view "VehicleModelAffiProduct" as
select "ProductCategory"."Id" as "ProductCategoryId",
       "ProductCategory"."Code" as "ProductCategoryCode",
       "ProductCategory"."Name" as "ProductCategoryName",
       "Product"."Id" as "ProductId",
       "Product"."Code" as "ProductCode",
       --"Product"."Name" as "ProductName",
       --"Product"."CanBeOrdered",
       --"Product"."IfPurchasable",
       "Product"."OrderCycle",
       "Product"."PurchaseCycle",
       --"Product"."IfOption",
       "Product"."ParameterDescription",
       "Product"."Version",
       "Product"."ColorCode",
       "Product"."Picture",
      -- "Product"."Color",
       "Product"."Configuration",
       "Product"."EngineCylinder",
       "Product"."EmissionStandard",
       "Product"."ManualAutomatic"
  from "Product"
 inner join "ProductAffiProductCategory" on "ProductAffiProductCategory"."ProductId" = "Product"."Id"
 inner join "ProductCategory" on "ProductCategory"."Id" = "ProductAffiProductCategory"."ProductCategoryId"
/

-- 销售车型以及零售指导价
/
create or replace view "VehModelRetailSuggestedPrice" as
select distinct 
       "ProductCategory"."Id" as "ProductCategoryId",
       "ProductCategory"."Code" as "ProductCategoryCode",
       "ProductCategory"."Name" as "ProductCategoryName",  
       "VehicleRetailSuggestedPrice"."Price"
  from "VehicleRetailSuggestedPrice"
 inner join "Product" on "Product"."Id" = "VehicleRetailSuggestedPrice"."ProductId"
 inner join "ProductAffiProductCategory" on "ProductAffiProductCategory"."ProductId" = "Product"."Id"
 inner join "ProductCategory" on "ProductCategory"."Id" = "ProductAffiProductCategory"."ProductCategoryId"
/
-- 库区库位及配件库存
/
create or replace view "WarehouseAreaWithPartsStock" as
select "WarehouseArea"."Id" as  "WarehouseAreaId",
       "WarehouseArea"."WarehouseId",
       "WarehouseArea"."ParentId" as "WarehouseAreaParentId",
       "WarehouseArea"."Code" as "WarehouseAreaCode",
       "WarehouseArea"."TopLevelWarehouseAreaId",
       "WarehouseArea"."AreaCategoryId",
       "WarehouseArea"."Status",
       "WarehouseArea"."Remark",
       "WarehouseArea"."AreaKind",
       "WarehouseAreaCategory"."Category" as "AreaCategory",
       "Warehouse"."Code" as "WarehouseCode",
       "Warehouse"."Name" as "WarehouseName",
       "Warehouse"."Type" as "WarehouseType",
       "Warehouse"."BranchId",
       "Warehouse"."StorageCompanyId",
       "Warehouse"."StorageCompanyType",
       nvl("PartsStock"."PartId",0) as "SparePartId",
       "PartsStock"."Quantity",
       "SparePart"."Code" as "SparePartCode",
       "SparePart"."Name" as "SparePartName"
  from "WarehouseArea"
 inner join "Warehouse" on "WarehouseArea"."WarehouseId" = "Warehouse"."Id"
  left join "WarehouseAreaCategory" on "WarehouseArea"."AreaCategoryId" = "WarehouseAreaCategory"."Id"
  left join "PartsStock" on "WarehouseArea"."Id" = "PartsStock"."WarehouseAreaId"
  left join "SparePart" on "PartsStock"."PartId" = "SparePart"."Id"
/

-- 根据Id查询故障现象分类及其全部下级分类
create or replace function "GetChildMalfunctionCategories"("Id" integer) return SYS_REFCURSOR
as
  Result SYS_REFCURSOR;
  my_sql varchar2(4000);
begin
  my_sql := 'with tmp(id, parentid) as( '||
            '  select id, parentid from MalfunctionCategory '||
            '  where id = '||to_char("Id")||
            '  union all '||
            '  select MalfunctionCategory.id, MalfunctionCategory.parentid '||
            '  from MalfunctionCategory '||
            '  inner join tmp on tmp.id = MalfunctionCategory.parentid '||
            ')  cycle id set dup_id TO ''Y'' DEFAULT ''N'' '||
            'select * from MalfunctionCategory '||
            'where exists (select * from tmp where MalfunctionCategory.id = tmp.id) ';
  open Result for my_sql;
  return(Result);
end;
/

-- 根据Id查询旧件库区库位分类及其全部下级库区库位
create or replace function "GetChildUsedPartsWhseAreas"("Id" integer) return SYS_REFCURSOR
as
  Result SYS_REFCURSOR;
  my_sql varchar2(4000);
begin
  my_sql := 'with tmp(id, parentid) as( '||
            '  select id, parentid from usedpartswarehousearea '||
            '  where id = '||to_char("Id")||
            '  union all '||
            '  select usedpartswarehousearea.id, usedpartswarehousearea.parentid '||
            '  from usedpartswarehousearea '||
            '  inner join tmp on tmp.id = usedpartswarehousearea.parentid '||
            ')  cycle id set dup_id TO ''Y'' DEFAULT ''N'' '||
            'select * from usedpartswarehousearea '||
            'where exists (select * from tmp where usedpartswarehousearea.id = tmp.id) ';
  open Result for my_sql;
  return(Result);
end;
/


-- 根据Id查询库区库位分类及其全部下级库区库位
create or replace function "GetChildWarehouseAreas"("Id" integer) return SYS_REFCURSOR
as
  Result SYS_REFCURSOR;
  my_sql varchar2(4000);
begin
  my_sql := 'with tmp(id, parentid) as( '||
            '  select id, parentid from warehousearea '||
            '  where id = '||to_char("Id")||
            '  union all '||
            '  select warehousearea.id, warehousearea.parentid '||
            '  from warehousearea '||
            '  inner join tmp on tmp.id = warehousearea.parentid '||
            ')  cycle id set dup_id TO ''Y'' DEFAULT ''N'' '||
            'select * from warehousearea '||
            'where exists (select * from tmp where warehousearea.id = tmp.id) ';
  open Result for my_sql;
  return(Result);
end;
/

-- 生成编号
create or replace function "GetSerial"("FTemplateId" integer, "FCreateDate" date, "FCorpCode" varchar2) return integer 
as
  FId integer;
  FSerial integer;
begin
/*  begin
    if FCorpCode is null then
      select id,serial into FId,FSerial from CodeTemplateSerial 
       where TemplateId = FTemplateId and CreateDate = FCreateDate and CorpCode is null and rownum<2
         for update;
    else
      select id,serial into FId,FSerial from CodeTemplateSerial 
       where TemplateId = FTemplateId and CreateDate = FCreateDate and CorpCode = FCorpCode and rownum<2
         for update;
    end if;
    FSerial:=FSerial+1;
    UPDATE CodeTemplateSerial SET Serial = FSerial WHERE Id = FId;
  exception
    when NO_DATA_FOUND then
      FSerial:=1;
      INSERT INTO CodeTemplateSerial(Id ,TemplateId, CreateDate, CorpCode, Serial) 
      VALUES(S_CodeTemplateSerial.Nextval, FTemplateId, FCreateDate, FCorpCode, FSerial);
    when others then
      raise;
  end;*/
  return FSerial;
end;
/
--服务产品线（视图）
create or replace view "ServiceProductLineView" as 
select "ServiceProductLine"."Id"                   as "ProductLineId",
       1                                           as "ProductLineType",
       "ServiceProductLine"."Code" as "ProductLineCode",
       "ServiceProductLine"."Name" as "ProductLineName",
       "ServiceProductLine"."PartsSalesCategoryId",
       "ServiceProductLine"."BranchId"
  from "ServiceProductLine"
/
--维修项目（视图）
create or replace view "RepairItemView" as
select "RepairItem"."Id"               as "RepairItemId",
       "RepairItem"."Code"             as "Code",
       "RepairItem"."Name"             as "Name",
       "RepairItemCategory"."Id"       as "RepairItemCategoryId",
       "RepairItemCategory"."Code"     as "RepairItemCategoryCode",
       "RepairItemCategory"."Name"     as "RepairItemCategoryName",
       "RepairItemCategory"."ParentId" as "ParentId",
       "Parent_RepairItemCategory"."Code" as "ParentCode",
       "GParent_RepairItemCategory"."Code" as "GrandParentCode",
       "RepairItemBrandRelation"."Id" as "MalfunctionBrandRelationId",
       "Parent_RepairItemCategory"."Name" as "ParentName",
       "GParent_RepairItemCategory"."Name" as "GrandParentName",  
       "RepairItemBrandRelation"."PartsSalesCategoryId",
       "PartsSalesCategory"."Name"     as "PartsSalesCategoryName"
  from "RepairItemCategory" 
 inner join "RepairItemCategory" "Parent_RepairItemCategory"
  on "RepairItemCategory"."ParentId" ="Parent_RepairItemCategory"."Id"
 inner join "RepairItemCategory" "GParent_RepairItemCategory"
  on "Parent_RepairItemCategory"."ParentId"="GParent_RepairItemCategory"."Id"
 inner join "RepairItem"
    on "RepairItem"."RepairItemCategoryId" = "RepairItemCategory"."Id"
   and "RepairItem"."Status" = 1
 inner join "RepairItemBrandRelation"
    on "RepairItemBrandRelation"."RepairItemId" = "RepairItem"."Id"
   and "RepairItemBrandRelation"."Status" = 1
 inner join "PartsSalesCategory"
 on "PartsSalesCategory"."Id"="RepairItemBrandRelation"."PartsSalesCategoryId"
 where "RepairItemCategory"."Status" = 1
   and not exists
 (select *
          from "LayerNodeType"  a
         where "RepairItemCategory"."LayerNodeTypeId" = a."Id"
           and a."TopLevelNode" = 1);
/
--故障原因（视图）
create or replace view "MalfunctionView" as
select "Malfunction"."Id"               as "MalfunctionId",
       "Malfunction"."Code"             as "Code",
       "Malfunction"."Description"      as "Description",
       "MalfunctionCategory"."Id"       as "MalfunctionCategoryId",
       "MalfunctionCategory"."Code"     as "MalfunctionCategoryCode",
       "MalfunctionCategory"."Name"     as "MalfunctionCategoryName",
       "MalfunctionCategory"."ParentId" as "ParentId",
       "Parent_MalfunctionCategory"."Code" as "ParentCode",
       "Parent_MalfunctionCategory"."Name" as "ParentName",
       "GParent_MalfunctionCategory"."Code" as "GrandParentCode",
       "GParent_MalfunctionCategory"."Name" as "GrandParentName",
       "MalfunctionBrandRelation"."Id" as "MalfunctionBrandRelationId",
       "MalfunctionBrandRelation"."PartsSalesCategoryId",
       "PartsSalesCategory"."Name"     as "PartsSalesCategoryName"
  from "MalfunctionCategory" 
 inner join "MalfunctionCategory" "Parent_MalfunctionCategory"
  on "MalfunctionCategory"."ParentId" ="Parent_MalfunctionCategory"."Id"
 inner join "MalfunctionCategory" "GParent_MalfunctionCategory"
  on "Parent_MalfunctionCategory"."ParentId"="GParent_MalfunctionCategory"."Id"
 inner join "Malfunction"
    on "Malfunction"."MalfunctionCategoryId" = "MalfunctionCategory"."Id"
   and "Malfunction"."Status" = 1
 left join "MalfunctionBrandRelation"
    on "MalfunctionBrandRelation"."MalfunctionId" = "Malfunction"."Id"
   and "MalfunctionBrandRelation"."Status" = 1
 inner join "PartsSalesCategory"
 on "PartsSalesCategory"."Id"="MalfunctionBrandRelation"."PartsSalesCategoryId"
 where "MalfunctionCategory"."Status" = 1
   and not exists
 (select *
          from "LayerNodeType"
         where "MalfunctionCategory"."LayerNodeTypeId" = "LayerNodeType"."Id"
           and "LayerNodeType"."TopLevelNode" = 1)
/
--维修项目与服务产品线关系（视图）
create or replace view "RepairItemLaborHourView" as
select "RepairItem"."Id"               as "RepairItemId",
       "RepairItem"."Code"             as "Code",
       "RepairItem"."Name"             as "Name",
       "RepairItemCategory"."Id"       as "RepairItemCategoryId",
       "RepairItemCategory"."Code"     as "RepairItemCategoryCode",
       "RepairItemCategory"."Name"     as "RepairItemCategoryName",
       "RepairItemCategory"."ParentId" as "ParentId",
       "Parent_RepairItemCategory"."Code" as "ParentCode",
       "RepairItemBrandRelation"."Id" as "MalfunctionBrandRelationId",
       "Parent_RepairItemCategory"."Name" as "ParentName",
       "RepairItemBrandRelation"."PartsSalesCategoryId",
       "PartsSalesCategory"."Name"     as "PartsSalesCategoryName",
       "RepairItemAffiServProdLine"."ServiceProductLineId",
       "RepairItemAffiServProdLine"."DefaultLaborHour",	
       "RepairItemAffiServProdLine"."ProductLineType" as "ProductLineType"
  from "RepairItemCategory" 
 inner join "RepairItemCategory" "Parent_RepairItemCategory"
  on "RepairItemCategory"."ParentId" ="Parent_RepairItemCategory"."Id"
 inner join "RepairItem"
    on "RepairItem"."RepairItemCategoryId" = "RepairItemCategory"."Id"
   and "RepairItem"."Status" = 1
 inner join "RepairItemBrandRelation"
    on "RepairItemBrandRelation"."RepairItemId" = "RepairItem"."Id"
   and "RepairItemBrandRelation"."Status" = 1
 inner join "PartsSalesCategory"
 on "PartsSalesCategory"."Id"="RepairItemBrandRelation"."PartsSalesCategoryId"
 inner join "RepairItemAffiServProdLine" on "RepairItemAffiServProdLine"."RepairItemId" ="RepairItem"."Id"
 where "RepairItemCategory"."Status" = 1
   and not exists
 (select *
          from "LayerNodeType"  a
         where "RepairItemCategory"."LayerNodeTypeId" = a."Id"
           and a."TopLevelNode" = 1);
/
--WMS仓库配件冻结库存（视图）
create or replace view "WmsCongelationStockView" as
select 
d."Id" as "WmsStockId",--  WMS冻结库存Id
d."BranchId" as "BranchId",--  营销分公司Id
d."BranchCode" as "BranchCode",--  营销分公司编号
d."BranchName" as "BranchName",--  营销分公司名称
d."StorageCenter" as "StorageCenter",--  储运中心
d."PartsSalesCategoryId" as "PartsSalesCategoryId",--  配件销售类型Id
d."PartsSalesCategoryCode" as "PartsSalesCategoryCode",--  配件销售类型编号
d."PartsSalesCategoryName" as "PartsSalesCategoryName",--  配件销售类型名称
d."SparePartId" as "SparePartId",--  配件Id
d."SparePartCode" as "SparePartCode",--  配件编号
d."SparePartName" as "SparePartName",--  配件名称
d."CongelationStockQty" as "CongelationStockQty",--  冻结库存
d."DisabledStock" as "DisabledStock",--  不可用库存
a."Id" as "WarehouseId",--  仓库Id
a."StorageCompanyId" as "StorageCompanyId"--  仓储企业Id
from "Warehouse" a inner join "SalesUnitAffiWarehouse" b 
on a."Id"=b."WarehouseId" 
inner join "SalesUnit" c 
on b."SalesUnitId"=c."Id"
inner join "WmsCongelationStock" d on
c."PartsSalesCategoryId"=d."PartsSalesCategoryId";
/

/
-- 装车明细临时表(仅供装车明细视图使用)
create table "V_QTSINFO"
(
  "Cscode"        VARCHAR2(50),
  "Zcbmcode"      VARCHAR2(100),
  "Vincode"       VARCHAR2(50),
  "Assembly_date" DATE not null,
  "Bind_date"     DATE not null,
  "Factory"       VARCHAR2(20),
  "Mapid"         VARCHAR2(50),
  "Patch"         VARCHAR2(20),
  "Produceline"   VARCHAR2(20),
  "Mapname"       VARCHAR2(150)
)
--装车明细视图
create or replace view "QTSDataQuery" as
  select "Cscode","Zcbmcode","Vincode","Assembly_date","Bind_date","Factory","Mapid","Patch","Produceline","Mapname"
     from "V_QTSINFO"
/

--产品视图
create or replace view "ProductView" as
select t."Id"                    ,
  t."Code"                       ,
  t."ProductCategoryCode"        ,
  t."ProductCategoryName"        ,
  t."BrandId"                    ,
  t."BrandCode"                  ,
  t."BrandName"                  ,
  t."SubBrandCode"               ,
  t."SubBrandName"               ,
  t."TerraceCode"                ,
  t."TerraceName"                ,
  t."ProductLine"                ,
  t."ProductName"                ,
  t."OldInternalCode"            ,
  t."AnnoucementNumber"          ,
  t."TonnageCode"                ,
  t."EngineTypeCode"             ,
  t."EngineManufacturerCode"     ,
  t."GearSerialTypeCode"         ,
  t."GearSerialManufacturerCode" ,
  t."RearAxleTypeCode"           ,
  t."RearAxleManufacturerCode"   ,
  t."TireTypeCode"               ,
  t."TireFormCode"               ,
  t."BrakeModeCode"              ,
  t."DriveModeCode"              ,
  t."FrameConnetCode"            ,
  t."VehicleSize"                ,
  t."AxleDistanceCode"           ,
  t."EmissionStandardCode"       ,
  t."ProductFunctionCode"        ,
  t."ProductLevelCode"           ,
  t."VehicleTypeCode"            ,
  t."ColorDescribe"              ,
  t."SeatingCapacity"            ,
  t."Weight"                     ,
  t."Remark"                     ,
  t."Status"                     ,
  t."OrderCycle"                 ,
  t."PurchaseCycle"              ,
  t."ParameterDescription"       ,
  t."Version"                    ,
  t."Configuration"              ,
  t."EngineCylinder"             ,
  t."EmissionStandard"           ,
  t."ManualAutomatic"            ,
  t."ColorCode"                  ,
  t."Picture"                    ,
  t."CreatorId"                  ,
  t."CreatorName"                ,
  t."CreateTime"                 ,
  t."ModifierId"                 ,
  t."ModifierName"               ,
  t."ModifyTime"                 ,
  t."AbandonerId"                ,
  t."AbandonerName"              ,
  t."AbandonTime"                ,
CAST(decode (v."ServiceProductLineId",null,0, 1) AS NUMBER(1))    as "IsSetRelation",
v."ServiceProductLineId" as "ServiceProductLineId"
from "Product" t
left join "ServProdLineProductDetail" v on t."Id"=v."ProductId";
/
--老PMS系统维修保养索赔单临时表（供视图查询使用）
create table "RepairClaimBillQuery_Tmp"
(
  索赔单编号       VARCHAR2(20) not null,
  单据状态        NUMBER(3) not null,
  市场部         VARCHAR2(40) not null,
  申请单编号       VARCHAR2(20),
  索赔单类型       VARCHAR2(6),
  故障原因编码      VARCHAR2(20),
  客户意见        VARCHAR2(200),
  是否已回访       VARCHAR2(2),
  回访意见        VARCHAR2(200),
  服务科审核意见     VARCHAR2(200),
  材料费         NUMBER(19,4) not null,
  配件管理费       NUMBER,
  工时费         NUMBER(19,4),
  索赔类型        VARCHAR2(10),
  维修索赔备注信息    VARCHAR2(200),
  创建日期        DATE not null,
  创建人编号       VARCHAR2(20) not null,
  分公司审核时间     DATE,
  故障原因名称      VARCHAR2(200),
  星级系数        NUMBER(9,2),
  是否需要向事业部索赔  VARCHAR2(2),
  客户姓名        VARCHAR2(40),
  客户电话        VARCHAR2(64),
  客户手机        VARCHAR2(64),
  祸首件图号       VARCHAR2(40),
  祸首件名称       VARCHAR2(50),
  供应商确认状态     VARCHAR2(10),
  供应商审核意见     VARCHAR2(400),
  外出车费单价      NUMBER(19,4),
  外出路途补助单价    NUMBER(19,4),
  外出公里数       NUMBER(9,2),
  高速公路拖车费     NUMBER(19,4),
  外出人数        NUMBER(9),
  外出天数        NUMBER(9),
  外出差旅费用合计    NUMBER,
  外出救援地       VARCHAR2(40),
  外出索赔备注信息    VARCHAR2(200),
  维修属性        NUMBER(3) not null,
  出厂编号        VARCHAR2(20) not null,
  品牌名称        VARCHAR2(40) not null,
  产品线名称       VARCHAR2(40) not null,
  报修时间        DATE not null,
  运行里程        NUMBER(9),
  故障现象及原因描述   VARCHAR2(200),
  维修附加属性      VARCHAR2(16),
  福康工时单价      NUMBER,
  祸首件生产厂家     VARCHAR2(40),
  祸首件标记       VARCHAR2(40),
  祸首件总成生产厂家   VARCHAR2(40),
  祸首件总成标记     VARCHAR2(40),
  车型          VARCHAR2(100),
  发动机型号       VARCHAR2(100),
  发动机编号       VARCHAR2(32),
  vin码        VARCHAR2(40),
  祸首件总成名称     VARCHAR2(40),
  祸首件总成图号     VARCHAR2(40),
  祸首件流水号      VARCHAR2(40),
  购买日期        DATE,
  首次故障里程      NUMBER(16),
  生产日期        DATE,
  服务站编号       VARCHAR2(20) not null,
  服务站名称       VARCHAR2(100) not null,
  索赔单位事业部编号 VARCHAR2(20) not null,
  工时单价        NUMBER(19,4) not null,
  索赔单位事业部名称 VARCHAR2(50) not null,
  客户地址        VARCHAR2(100),
  责任单位供应商编号 VARCHAR2(20),
  责任单位供应商名称 VARCHAR2(50),
  维修索赔费用合计    NUMBER
);
--老PMS系统维修保养索赔单视图
create or replace view "RepairClaimBillQuery" as
  select *  from "RepairClaimBillQuery_Tmp"
/

--老PMS系统维修单临时表（供视图查询使用）
create table "RepairOrderQuery_Tmp"
(
  配件图号        VARCHAR2(40),
  配件名称        VARCHAR2(50),
  数量          NUMBER(9),
  是否发运        VARCHAR2(2),
  配件维修价       NUMBER(19,4),
  调整后配件维修价    NUMBER(19,4),
  旧件图号        VARCHAR2(40),
  防伪编码        VARCHAR2(42),
  旧件条码        VARCHAR2(40),
  是否装车件       NUMBER(3),
  新件批次号       VARCHAR2(20),
  规格型号    VARCHAR2(50),
  旧件供应商编号     VARCHAR2(20),
  旧件供应商名称     VARCHAR2(50),
  维修项目编号      VARCHAR2(40),
  维修项目名称      VARCHAR2(500),
  结算属性        VARCHAR2(4),
  调整后工时       NUMBER(9,2),
  完成时间        DATE,
  工时          NUMBER(9,2),
  故障原因编码      VARCHAR2(20),
  故障原因名称      VARCHAR2(200),
  故障索赔类型      VARCHAR2(10),
  祸首件图号       VARCHAR2(40),
  祸首件名称       VARCHAR2(50),
  故障原因备注信息    VARCHAR2(200),
  维修单编号       VARCHAR2(20) not null,
  单据状态        NUMBER(3) not null,
  出厂编号        VARCHAR2(20) not null,
  国家规定标准里程    NUMBER(9),
  维修属性        NUMBER(3) not null,
  旧件处理方式      VARCHAR2(10),
  随车物件        VARCHAR2(200),
  车况描述        VARCHAR2(200),
  故障现象及原因描述   VARCHAR2(200),
  外出原因        VARCHAR2(200),
  处理方法        VARCHAR2(200),
  处理结果        VARCHAR2(200),
  连带责任说明      VARCHAR2(200),
  保用服务状态      VARCHAR2(14),
  客户意见        VARCHAR2(200),
  报修时间        DATE not null,
  预定交车时间      DATE,
  vin码       VARCHAR2(40),
  外出救援地       VARCHAR2(40),
  结算里程        NUMBER(9),
  出发时间        DATE,
  返回时间        DATE,
  外出人员编号      VARCHAR2(40),
  是否使用自备车     VARCHAR2(2),
  备注信息        VARCHAR2(200),
  单据创建时间      DATE not null,
  单据创建人编号     VARCHAR2(20) not null,
  车牌号         VARCHAR2(20),
  工时费单价       NUMBER(19,4) not null,
  其他费用产生原因    VARCHAR2(200),
  是否包括重大或优惠索赔 VARCHAR2(2),
  客户姓名        VARCHAR2(40) not null,
  固定电话        VARCHAR2(64),
  移动电话        VARCHAR2(64),
  运行里程        NUMBER(9),
  强保次数        NUMBER(3),
  维修附加属性      VARCHAR2(16),
  故障发生时间      DATE not null,
  车型          VARCHAR2(100),
  发动机型号       VARCHAR2(100),
  发动机编号       VARCHAR2(32),
  生产日期        DATE,
  购买日期        DATE,
  首次故障里程      NUMBER(16),
  联系人姓名       VARCHAR2(40) not null,
  联系人地址       VARCHAR2(100),
  事业部         VARCHAR2(50) not null,
  外出责任单位编号    VARCHAR2(20),
  外出责任单位名称    VARCHAR2(50),
  祸首件供应商编号    VARCHAR2(20),
  索赔单编号       VARCHAR2(20),
  来客登记单编号     VARCHAR2(20) not null,
  服务活动编号      VARCHAR2(20),
  服务站编号       VARCHAR2(20) not null,
  服务站名称       VARCHAR2(100) not null
);

--服务站历史出入库查询(视图)
create or replace view "DealerPartsStockOutInRecord" as
select "PartRequisitionReturnCode" as "code",'维修领料单' as "OutInBandtype",
"NewPartsId" as "PartsId","NewPartsCode" as "PartsCode","NewPartsName" as "PartsName",
"Quantity","CreateTime","DealerId"
from "PartRequisitionReturnBill"
where "RequisitionReturnStatus" =1
union all
select "pr"."Code",'销售退货单' as "OutInBandtype",
"prlist"."SparePartId" as "PartsId","prlist"."SparePartCode" as "PartsCode","prlist"."SparePartName" as "PartsName",
"prlist"."ApproveQuantity" as "Quantity","pr"."CreateTime","pr"."SubmitCompanyId" as "DealerId"
from "PartsSalesReturnBill" "pr"
inner join "PartsSalesReturnBillDetail" "prlist" on "pr"."Id" ="prlist"."PartsSalesReturnBillId"
where "pr"."Status" = 3   --审批完成
union all
select "po"."Code",'零售订单' as "OutInBandtype",
"polist"."PartsId","polist"."PartsCode","polist"."PartsName",
"polist"."Quantity","po"."CreateTime","po"."DealerId"
from "DealerPartsRetailOrder" "po"
inner join "DealerRetailOrderDetail" "polist" on "po"."Id" ="polist"."DealerPartsRetailOrderId"
where "po"."Status" =2   --已审核
union all
select "pc"."Code",'外采单' as "OutInBandtype",
"pclist"."PartsId","pclist"."PartsCode","pclist"."PartsName",
"pclist"."Quantity","pc"."CreateTime","pc"."CustomerCompanyId" as "DealerId"
from "PartsOuterPurchaseChange" "pc"
inner join "PartsOuterPurchaselist" "pclist" on "pc"."Id" ="pclist"."PartsOuterPurchaseChangeId"
where "pc"."Status" =3   --生效
union all
select DI."Code",'盘亏' as "OutInBandtype",
"DIlist"."SparePartId" as "partsid","DIlist"."SparePartCode" as "PartsCode","DIlist"."SparePartName" as "partsname",
("DIlist"."CurrentStorage" -"DIlist"."StorageAfterInventory" ) as "Quantity","DI"."CreateTime","DI"."StorageCompanyId" as "DealerId"
from "DealerPartsInventoryBill" DI
inner join "DealerPartsInventoryDetail" "DIlist" on DI."Id" ="DIlist"."DealerPartsInventoryId"
where DI."Status" =2   --已审核
and "DIlist"."StorageAfterInventory" < "DIlist"."CurrentStorage"
union all
--入库
select "PartRequisitionReturnCode" as "code",'退料单' as "OutInBandtype",
"NewPartsId" as "PartId","NewPartsCode" as "PartsCode","NewPartsName" as "PartsName","Quantity","CreateTime","DealerId"
from "PartRequisitionReturnBill"
where "RequisitionReturnStatus" =2
union all
select "ps"."Code",'发运单' as "OutInBandtype",
"pslist"."SparePartId" as "partsid","pslist"."SparePartCode" as "PartsCode","pslist"."SparePartName" as "partsname",
"pslist"."ConfirmedAmount" as "Quantity","ps"."CreateTime","ps"."ReceivingCompanyId" as "DealerId"
from "PartsShippingOrder" "ps"
inner join "PartsShippingOrderDetail" "pslist" on "ps"."Id" ="pslist"."PartsShippingOrderId"
where "ps"."Status" in (2,3)   --2收货确认,3回执确认
union all
select "dpr"."Code",'零售退货' as "OutInBandtype",
"dprlist"."PartsId","dprlist"."PartsCode","dprlist"."PartsName",
"dprlist"."Quantity","dpr"."CreateTime","dpr"."DealerId"
from "DealerPartsSalesReturnBill" "dpr"
inner join "DealerRetailReturnBillDetail" "dprlist" on "dpr"."Id" ="dprlist"."DealerPartsSalesReturnBillId"
where "dpr"."Status" =2   --已审核
union all
select DI."Code",'盘盈' as "OutInBandtype",
"DIlist"."SparePartId" as "partsid","DIlist"."SparePartCode" as "PartsCode","DIlist"."SparePartName" as "partsname",
("DIlist"."StorageAfterInventory" -"DIlist"."CurrentStorage") as "Quantity","DI"."CreateTime","DI"."StorageCompanyId" as "DealerId"
from "DealerPartsInventoryBill" DI
inner join "DealerPartsInventoryDetail" "DIlist" on DI."Id" ="DIlist"."DealerPartsInventoryId"
where DI."Status" =2   --已审核
and "DIlist"."StorageAfterInventory" >"DIlist"."CurrentStorage";



--代理库库存查询(视图)
create or replace view "AgencyStockQryFrmOrder" as
select "PartsSalesOrderId","PartsSalesOrderCode","SparePartCode","SparePartName","AgencyCode","AgencyName", "OrderedQuantity","Usablequantity",
       case when "Usablequantity">"OrderedQuantity" then "OrderedQuantity" else "Usablequantity" end as "Fulfilquantity",
       case when "Usablequantity">"OrderedQuantity" then 0 else "OrderedQuantity"-"Usablequantity" end as "NoFulfilquantity"
  from (
  select "ps"."Id" as "PartsSalesOrderId","ps"."Code" as "PartsSalesOrderCode","psd"."SparePartId","psd"."SparePartCode","psd"."SparePartName","ar"."AgencyCode","ar"."AgencyName",
  sum(nvl("psd"."OrderedQuantity",0)) as "OrderedQuantity",sum(nvl("s"."Quantity",0)-nvl("pls"."LockedQuantity",0)-nvl("wv"."CongelationStockQty",0)-nvl("wv"."DisabledStock",0)) as "Usablequantity"
  from "PartsSalesOrder" "ps"
  inner join "PartsSalesOrderDetail" "psd" on "ps"."Id" = "psd"."PartsSalesOrderId"
  inner join "AgencyDealerRelation" "ar" on "ps"."SubmitCompanyId" = "ar"."DealerId" and "ps"."SalesCategoryId" ="ar"."PartsSalesOrderTypeId"
  left join "PartsStock" "s" on "psd"."SparePartId" = "s"."PartId" and "ps"."WarehouseId" = "s"."WarehouseId" and "s"."BranchId" = "ps"."BranchId"
  left join "WarehouseAreaCategory" "kw" on "kw"."Id" ="s"."WarehouseAreaCategoryId" and  "kw"."Category" in (1,3)
  left join "PartsLockedStock" "pls" on "pls"."WarehouseId" = "s"."WarehouseId" and "s"."PartId" ="pls"."PartId"
  left join "WmsCongelationStockView" "wv" on "wv"."WarehouseId" = "s"."WarehouseId" and "wv"."SparePartId" = "s"."PartId"
  where "ps"."Status" =2
  group by "ps"."Id","ps"."Code","psd"."SparePartId","psd"."SparePartCode","psd"."SparePartName","ar"."AgencyCode","ar"."AgencyName"
  ) "tmp"
  where 1=1;


----Sap错误信息（视图）
create or replace View "View_ErrorTable_SAP_YX" as
select --"ErrorTable_SAP_YX".*,
  "ErrorTable_SAP_YX"."Id","ErrorTable_SAP_YX"."BussinessId","ErrorTable_SAP_YX"."BussinessCode","ErrorTable_SAP_YX"."BussinessTableName","ErrorTable_SAP_YX"."SyncTypeName",
  "ErrorTable_SAP_YX"."Message","ErrorTable_SAP_YX"."Status","ErrorTable_SAP_YX"."CreatorId","ErrorTable_SAP_YX"."CreatorName",
  "ErrorTable_SAP_YX"."CreateTime","ErrorTable_SAP_YX"."ModifierId","ErrorTable_SAP_YX"."ModifierName","ErrorTable_SAP_YX"."ModifyTime","ErrorTable_SAP_YX"."SysCode",
  "PartsSalesCategory"."Id" as  "Partssalescategoryid",---品牌id
  "PartsSalesCategory"."Name" as "BrandName",--品牌名称
  "PartsInboundCheckBill"."CounterpartCompanyName" as "CustomerName",--对方单位名称
  "PartsInboundCheckBill"."CreatorName" as "BillCreatename"--创建人
  from "ErrorTable_SAP_YX"
 inner join "PartsInboundCheckBill"
    on "ErrorTable_SAP_YX"."BussinessCode" = "PartsInboundCheckBill"."Code"
   and "ErrorTable_SAP_YX"."SyncTypeName" in (6,9)--(销售退货入库，采购入库)
 inner join "PartsSalesCategory"
    on "PartsInboundCheckBill"."PartsSalesCategoryId" = "PartsSalesCategory"."Id"   
  union all
   select --"ErrorTable_SAP_YX".*,
     "ErrorTable_SAP_YX"."Id","ErrorTable_SAP_YX"."BussinessId","ErrorTable_SAP_YX"."BussinessCode","ErrorTable_SAP_YX"."BussinessTableName","ErrorTable_SAP_YX"."SyncTypeName",
  "ErrorTable_SAP_YX"."Message","ErrorTable_SAP_YX"."Status","ErrorTable_SAP_YX"."CreatorId","ErrorTable_SAP_YX"."CreatorName",
  "ErrorTable_SAP_YX"."CreateTime","ErrorTable_SAP_YX"."ModifierId","ErrorTable_SAP_YX"."ModifierName","ErrorTable_SAP_YX"."ModifyTime","ErrorTable_SAP_YX"."SysCode",
  "PartsSalesCategory"."Id" as  "Partssalescategoryid",---品牌id
      "PartsSalesCategory"."Name" as "brandname",--品牌名称
      "PartsPurchaseSettleBill"."PartsSupplierName" as "customername",--对方单位名称
      "PartsPurchaseSettleBill"."CreatorName" as "billCreatename"--创建人  
  from "ErrorTable_SAP_YX"
 inner join "PartsPurchaseSettleBill"
    on "ErrorTable_SAP_YX"."BussinessCode" = "PartsPurchaseSettleBill"."Code"
   and "ErrorTable_SAP_YX"."SyncTypeName" in (8,17,18)--（采购结算，采购结算供应商发票，一采购结算单多发票）
 inner join "PartsSalesCategory"
    on "PartsPurchaseSettleBill"."PartsSalesCategoryId" = "PartsSalesCategory"."Id"   
   union all
   select --"ErrorTable_SAP_YX".*,
     "ErrorTable_SAP_YX"."Id","ErrorTable_SAP_YX"."BussinessId","ErrorTable_SAP_YX"."BussinessCode","ErrorTable_SAP_YX"."BussinessTableName","ErrorTable_SAP_YX"."SyncTypeName",
  "ErrorTable_SAP_YX"."Message","ErrorTable_SAP_YX"."Status","ErrorTable_SAP_YX"."CreatorId","ErrorTable_SAP_YX"."CreatorName",
  "ErrorTable_SAP_YX"."CreateTime","ErrorTable_SAP_YX"."ModifierId","ErrorTable_SAP_YX"."ModifierName","ErrorTable_SAP_YX"."ModifyTime","ErrorTable_SAP_YX"."SysCode",
  "PartsSalesCategory"."Id" as  "Partssalescategoryid",---品牌id
   "PartsSalesCategory"."Name" as "brandname" ,--品牌名称
   "PartsSalesSettlement"."CustomerCompanyName" as "customername",---客户名称
   "PartsSalesSettlement"."CreatorName" as "billCreatename"--创建人名称
   from "ErrorTable_SAP_YX" 
   inner join "PartsSalesSettlement" on "ErrorTable_SAP_YX"."BussinessCode"="PartsSalesSettlement"."Code"
   and "ErrorTable_SAP_YX"."SyncTypeName" in (2,14)--（销售结算单成本结转，销售结算）
   inner join "PartsSalesCategory" on "PartsSalesSettlement"."PartsSalesCategoryId" = "PartsSalesCategory"."Id" 
   union all 
   select --"ErrorTable_SAP_YX".*,
     "ErrorTable_SAP_YX"."Id","ErrorTable_SAP_YX"."BussinessId","ErrorTable_SAP_YX"."BussinessCode","ErrorTable_SAP_YX"."BussinessTableName","ErrorTable_SAP_YX"."SyncTypeName",
  "ErrorTable_SAP_YX"."Message","ErrorTable_SAP_YX"."Status","ErrorTable_SAP_YX"."CreatorId","ErrorTable_SAP_YX"."CreatorName",
  "ErrorTable_SAP_YX"."CreateTime","ErrorTable_SAP_YX"."ModifierId","ErrorTable_SAP_YX"."ModifierName","ErrorTable_SAP_YX"."ModifyTime","ErrorTable_SAP_YX"."SysCode",
  "PartsSalesCategory"."Id" as  "Partssalescategoryid",---品牌id
   "PartsSalesCategory"."Name" as "brandname",--品牌名称
   "PartsOutboundBill"."CounterpartCompanyName" as "customername",--客户名称
   "PartsOutboundBill"."CreatorName" as "billCreatename"--创建人名称
   from "ErrorTable_SAP_YX"
   inner join "PartsOutboundBill" on "ErrorTable_SAP_YX"."BussinessCode"="PartsOutboundBill"."Code"
   and "ErrorTable_SAP_YX"."SyncTypeName" in(5,10)---（采购退货出库，销售出库）
   inner join "PartsSalesCategory" on "PartsOutboundBill"."PartsSalesCategoryId"="PartsSalesCategory"."Id"
   union all 
   select --"ErrorTable_SAP_YX".*,
     "ErrorTable_SAP_YX"."Id","ErrorTable_SAP_YX"."BussinessId","ErrorTable_SAP_YX"."BussinessCode","ErrorTable_SAP_YX"."BussinessTableName","ErrorTable_SAP_YX"."SyncTypeName",
  "ErrorTable_SAP_YX"."Message","ErrorTable_SAP_YX"."Status","ErrorTable_SAP_YX"."CreatorId","ErrorTable_SAP_YX"."CreatorName",
  "ErrorTable_SAP_YX"."CreateTime","ErrorTable_SAP_YX"."ModifierId","ErrorTable_SAP_YX"."ModifierName","ErrorTable_SAP_YX"."ModifyTime","ErrorTable_SAP_YX"."SysCode",
  "PartsSalesCategory"."Id" as  "Partssalescategoryid",---品牌id
   "PartsSalesCategory"."Name" as "brandname",--品牌名称
   "PartsSalesRtnSettlement"."CustomerCompanyName" as "customername",--客户名称
   "PartsSalesRtnSettlement"."CreatorName" as "billCreatename"--创建人名称
   from "ErrorTable_SAP_YX"
   inner join "PartsSalesRtnSettlement" on "ErrorTable_SAP_YX"."BussinessCode"="PartsSalesRtnSettlement"."Code"
   and "ErrorTable_SAP_YX"."SyncTypeName" in(1,4,15)-----（销售退货结算单成本结转，销售退货结算，销售退货结算开红票）
   inner join "PartsSalesCategory" on "PartsSalesRtnSettlement"."PartsSalesCategoryId"="PartsSalesCategory"."Id"
   union all
   select --"ErrorTable_SAP_YX".*,
     "ErrorTable_SAP_YX"."Id","ErrorTable_SAP_YX"."BussinessId","ErrorTable_SAP_YX"."BussinessCode","ErrorTable_SAP_YX"."BussinessTableName","ErrorTable_SAP_YX"."SyncTypeName",
  "ErrorTable_SAP_YX"."Message","ErrorTable_SAP_YX"."Status","ErrorTable_SAP_YX"."CreatorId","ErrorTable_SAP_YX"."CreatorName",
  "ErrorTable_SAP_YX"."CreateTime","ErrorTable_SAP_YX"."ModifierId","ErrorTable_SAP_YX"."ModifierName","ErrorTable_SAP_YX"."ModifyTime","ErrorTable_SAP_YX"."SysCode",
  "PartsSalesCategory"."Id" as  "Partssalescategoryid",---品牌id
   "PartsSalesCategory"."Name" as "brandname",--品牌名称
   "PartsPurchaseRtnSettleBill"."PartsSupplierName" as "customername",--客户名称
   "PartsPurchaseRtnSettleBill"."CreatorName" as "billCreatename"---创建人名称
   from "ErrorTable_SAP_YX"
   inner join "PartsPurchaseRtnSettleBill" on "ErrorTable_SAP_YX"."BussinessCode"="PartsPurchaseRtnSettleBill"."Code"
   and "ErrorTable_SAP_YX"."SyncTypeName" in (3,16)-----（采购退货结算，采购退货结算开红票）
   inner join "PartsSalesCategory" on "PartsPurchaseRtnSettleBill"."PartsSalesCategoryId"="PartsSalesCategory"."Id"   
   union all
   select --"ErrorTable_SAP_YX".*,
     "ErrorTable_SAP_YX"."Id","ErrorTable_SAP_YX"."BussinessId","ErrorTable_SAP_YX"."BussinessCode","ErrorTable_SAP_YX"."BussinessTableName","ErrorTable_SAP_YX"."SyncTypeName",
  "ErrorTable_SAP_YX"."Message","ErrorTable_SAP_YX"."Status","ErrorTable_SAP_YX"."CreatorId","ErrorTable_SAP_YX"."CreatorName",
  "ErrorTable_SAP_YX"."CreateTime","ErrorTable_SAP_YX"."ModifierId","ErrorTable_SAP_YX"."ModifierName","ErrorTable_SAP_YX"."ModifyTime","ErrorTable_SAP_YX"."SysCode",
  "PartsSalesCategory"."Id" as  "Partssalescategoryid",---品牌id
   "PartsSalesCategory"."Name" as "brandname",--品牌名称
   "PartsRequisitionSettleBill"."DepartmentName" as "customername",--客户名称
   "PartsRequisitionSettleBill"."CreatorName" as "billCreatename"---创建人名称
   from "ErrorTable_SAP_YX"
   inner join "PartsRequisitionSettleBill" on "ErrorTable_SAP_YX"."BussinessCode"="PartsRequisitionSettleBill"."Code"
   and "ErrorTable_SAP_YX"."SyncTypeName"=7---（领用结算）
   inner join "PartsSalesCategory" on "PartsRequisitionSettleBill"."PartsSalesCategoryId"="PartsSalesCategory"."Id" 
   union all
   select --"ErrorTable_SAP_YX".*,
     "ErrorTable_SAP_YX"."Id","ErrorTable_SAP_YX"."BussinessId","ErrorTable_SAP_YX"."BussinessCode","ErrorTable_SAP_YX"."BussinessTableName","ErrorTable_SAP_YX"."SyncTypeName",
  "ErrorTable_SAP_YX"."Message","ErrorTable_SAP_YX"."Status","ErrorTable_SAP_YX"."CreatorId","ErrorTable_SAP_YX"."CreatorName",
  "ErrorTable_SAP_YX"."CreateTime","ErrorTable_SAP_YX"."ModifierId","ErrorTable_SAP_YX"."ModifierName","ErrorTable_SAP_YX"."ModifyTime","ErrorTable_SAP_YX"."SysCode",
  "PartsSalesCategory"."Id" as  "Partssalescategoryid",---品牌id
   "PartsSalesCategory"."Name" as "brandname",--品牌名称
   "PartsInventoryBill"."WarehouseName" as "customername",--客户名称
   "PartsInventoryBill"."CreatorName" as "billCreatename"---创建人名称
   from "ErrorTable_SAP_YX"
   inner join "PartsInventoryBill" on "ErrorTable_SAP_YX"."BussinessCode"="PartsInventoryBill"."Code"
   and "ErrorTable_SAP_YX"."SyncTypeName"=12----盘点
   inner join "SalesUnitAffiWarehouse" on "PartsInventoryBill"."WarehouseId"="SalesUnitAffiWarehouse"."WarehouseId"
   inner join "SalesUnit" on "SalesUnitAffiWarehouse"."SalesUnitId"="SalesUnit"."Id"
   inner join "PartsSalesCategory" on "SalesUnit"."PartsSalesCategoryId"="PartsSalesCategory"."Id" 
   union all
   select --"ErrorTable_SAP_YX".*,
     "ErrorTable_SAP_YX"."Id","ErrorTable_SAP_YX"."BussinessId","ErrorTable_SAP_YX"."BussinessCode","ErrorTable_SAP_YX"."BussinessTableName","ErrorTable_SAP_YX"."SyncTypeName",
  "ErrorTable_SAP_YX"."Message","ErrorTable_SAP_YX"."Status","ErrorTable_SAP_YX"."CreatorId","ErrorTable_SAP_YX"."CreatorName",
  "ErrorTable_SAP_YX"."CreateTime","ErrorTable_SAP_YX"."ModifierId","ErrorTable_SAP_YX"."ModifierName","ErrorTable_SAP_YX"."ModifyTime","ErrorTable_SAP_YX"."SysCode",
  "PartsSalesCategory"."Id" as  "Partssalescategoryid",---品牌id
   "PartsSalesCategory"."Name" as "brandname",--品牌名称
   "PlannedPriceApp"."OwnerCompanyName" as "customername",--客户名称
   "PlannedPriceApp"."CreatorName" as "billCreatename"---创建人名称
   from "ErrorTable_SAP_YX"
   inner join "PlannedPriceApp" on "ErrorTable_SAP_YX"."BussinessCode"="PlannedPriceApp"."Code"
   and "ErrorTable_SAP_YX"."SyncTypeName"=13---成本变更
   inner join "PartsSalesCategory" on "PlannedPriceApp"."PartsSalesCategoryId"="PartsSalesCategory"."Id" 
   union all
   select --"ErrorTable_SAP_YX".*,
     "ErrorTable_SAP_YX"."Id","ErrorTable_SAP_YX"."BussinessId","ErrorTable_SAP_YX"."BussinessCode","ErrorTable_SAP_YX"."BussinessTableName","ErrorTable_SAP_YX"."SyncTypeName",
  "ErrorTable_SAP_YX"."Message","ErrorTable_SAP_YX"."Status","ErrorTable_SAP_YX"."CreatorId","ErrorTable_SAP_YX"."CreatorName",
  "ErrorTable_SAP_YX"."CreateTime","ErrorTable_SAP_YX"."ModifierId","ErrorTable_SAP_YX"."ModifierName","ErrorTable_SAP_YX"."ModifyTime","ErrorTable_SAP_YX"."SysCode",
  "PartsSalesCategory"."Id" as  "Partssalescategoryid",---品牌id
   "PartsSalesCategory"."Name" as "brandname",--品牌名称
   "PartsTransferOrder"."OriginalWarehouseName" as "customername",--客户名称
   "PartsTransferOrder"."CreatorName" as "billCreatename"---创建人名称
   from "ErrorTable_SAP_YX"
   inner join "PartsTransferOrder" on "ErrorTable_SAP_YX"."BussinessCode"="PartsTransferOrder"."Code"
   and "ErrorTable_SAP_YX"."SyncTypeName"=11----调拨
   inner join "SalesUnitAffiWarehouse" on "PartsTransferOrder"."OriginalWarehouseId"="SalesUnitAffiWarehouse"."WarehouseId"
   inner join  "SalesUnit" on "SalesUnitAffiWarehouse"."SalesUnitId"="SalesUnit"."Id"
   inner join "PartsSalesCategory" on  "SalesUnit"."PartsSalesCategoryId"="PartsSalesCategory"."Id" 
   union all
   select --"ErrorTable_SAP_YX".*,
     "ErrorTable_SAP_YX"."Id","ErrorTable_SAP_YX"."BussinessId","ErrorTable_SAP_YX"."BussinessCode","ErrorTable_SAP_YX"."BussinessTableName","ErrorTable_SAP_YX"."SyncTypeName",
  "ErrorTable_SAP_YX"."Message","ErrorTable_SAP_YX"."Status","ErrorTable_SAP_YX"."CreatorId","ErrorTable_SAP_YX"."CreatorName",
  "ErrorTable_SAP_YX"."CreateTime","ErrorTable_SAP_YX"."ModifierId","ErrorTable_SAP_YX"."ModifierName","ErrorTable_SAP_YX"."ModifyTime","ErrorTable_SAP_YX"."SysCode",
  "PartsSalesCategory"."Id" as  "Partssalescategoryid",---品牌id
   "PartsSalesCategory"."Name" as "brandname",---品牌名称
   "InvoiceInformation"."InvoiceCompanyName" as "customername",--开票企业名称
   "InvoiceInformation"."CreatorName" as "billCreatename"--创建人名称
   from "ErrorTable_SAP_YX"
   inner join "InvoiceInformation" on "ErrorTable_SAP_YX"."BussinessCode"="InvoiceInformation"."Code"
   and "ErrorTable_SAP_YX"."SyncTypeName"=19----一发票多采购结算单
   inner join "PartsSalesCategory" on "InvoiceInformation"."PartsSalesCategoryId"="PartsSalesCategory"."Id";



--旧件库存视图
create or replace view "UsedPartsStockView" as
select
a."Id",u."Id" as "WarehouseId",u."Code",u."Name","UsedPartsBarCode","UsedPartsCode","UsedPartsName",s."Code" as "AreaCode","StorageQuantity","LockedQuantity","SettlementPrice","ClaimBillCode",
"IfFaultyParts","FaultyPartsCode","UsedPartsSupplierCode","FaultyPartsSupplierCode","ResponsibleUnitCode",a."CreatorName",a."CreateTime","UsedPartsId",
"UsedPartsBatchNumber",
"UsedPartsSerialNumber",
"ClaimBillType",
"ClaimBillId",
"FaultyPartsId",
"FaultyPartsName",
"UsedPartsSupplierId",
"UsedPartsSupplierName",
"FaultyPartsSupplierId",
"FaultyPartsSupplierName",
"ResponsibleUnitId",
"ResponsibleUnitName",
a."BranchId",
CAST(case when (select upt."Status" from "UsedPartsTransferDetail" uptd
inner join "UsedPartsTransferOrder" upt on uptd."UsedPartsTransferOrderId" = upt."Id"
where upt."Status" <> 99 and uptd."UsedPartsBarCode" = a."UsedPartsBarCode" and rownum = 1) is not null then 1 else 0 end AS NUMBER(1))  as "IsTransfers"
from "UsedPartsStock" a
inner join "UsedPartsWarehouse"  u on u."Id"=a."UsedPartsWarehouseId"
inner join "UsedPartsWarehouseArea" s on s."Id"=a."UsedPartsWarehouseAreaId";


--配件非常规采购计划（视图）
--"TpartNotPurchasePlan"  表结构临时表
create or replace View "PartNotConPurchasePlan"
as
select "BranchId",
       "BranchName",
       "PartsSalesCategoryId",
       "PartsSalesCategoryName",
       "PartId",
       "PartCode",
       "PartName",
       "DefaultOrderWarehouseId",
       "DefaultOrderWarehouseName",
       "SupplierId",
       "SupplierPartCode",
       "SupplierPartName",
       "OrderCycle",
       "EmergencyArrivalPeriod",
       "MonthlyArrivalPeriod",
       "ArrivalReplenishmentCycle",
       "PartABC",
       "ProductLifeCycle",
       "PlannedPriceCategory",
       "OrderQuantityFor12M",
       "PlannedPrice",
       "FREQUENCY1",
       "DAYAVGFOR1M",
       "OrderQuantity1",
       "ORDERQUANTITY2",
       "ORDERQUANTITY3",
       "SafeStock",
       "AllValidStocknums",
       "PurchaseOLnums",
       "PCnums",
       "TransferOLnums",
       "Replacementnums",
       "ReplacePurchaseOLnums",
       "PartsExchangenums",
       "PartsExchangeOLnums",
       "SLNCnums",
       "DeliveryTime",
       "RsAdvicePurchase",
       "EgAdvicePurchase",
       "IsMeetSupplementPlan",
       "IsMeetEmergencyPlan"
  from "TpartNotPurchasePlan";


--经销商配件库存（视图）
Create Or Replace View "DealerPartsStockQueryView" As 
Select t."BranchId",t."BranchCode",t."PartsSalesCategoryId",t."PartsSalesCategoryName",t."DealerCode",t."DealerName",t."PartCode",t."PartName",
 t."Quantity",t."CustomerType",t."ProvinceName",t."SalesPrice",t."SalesPriceAmount",t."StockMaximum",t."PartsAttribution",Cast (Decode(t."Quantity", 0, Null, Null, Null, 1)AS NUMBER(1)) As "IsStoreMoreThanZero"
  From (Select a."BranchId",
               Br."Code" As "BranchCode",
               a."SalesCategoryId" As "PartsSalesCategoryId",
               a."SalesCategoryName" As "PartsSalesCategoryName",
               a."DealerCode",-- 服务站编码,
               a."DealerName",-- 服务站名称,
               b."Code" As "PartCode",--配件图号
               b."Name" As "PartName",--配件名称
               Sum(a."Quantity") As "Quantity",--库存
               Nvl(c."SalesPrice", 0) As "SalesPrice",--批发价
               d."ProvinceName" As "ProvinceName",--省份
               Case
                 When Dsi."ChannelCapabilityId" = 3 Then
                  '专卖店'
                 Else
                  '服务站'
               End As "CustomerType",--客户类型
               Sum(a."Quantity" * Nvl(c."SalesPrice", 0)) As "SalesPriceAmount",--批发价金额
               Pb."StockMaximum",--库存高限
               Pb."PartsAttribution"--配件所属分类
                 From
                 "DealerPartsStock" a
                 Inner Join "Branch" Br
                    On a."BranchId" = Br."Id"
                 Inner Join "SparePart" b
                    On a."SparePartId" = b."Id"
                  Left Join "PartsSalesPrice" c
                    On c."PartsSalesCategoryId" = a."SalesCategoryId"
                   And c."SparePartId" = a."SparePartId"
                   And c."Status" = 1
                  Left Join "Company" d
                    On a."DealerCode" = d."Code"
                  Left Join "Dealer" e
                    On e."Code" = d."Code"
                  Left Join "DealerServiceInfo" Dsi
                    On e."Id" = Dsi."DealerId"
                   And a."SalesCategoryId" = Dsi."PartsSalesCategoryId"
                  Left Join "PartsBranch" Pb
                    On a."SalesCategoryId" = Pb."PartsSalesCategoryId"
                   And b."Code" = Pb."PartCode"
                 Where a."SubDealerId" = -1
                   And d."Type" In (2, 7, 10)
                 Group By a."BranchId",
                          d."ProvinceName",
                          a."SalesCategoryId",
                          a."SalesCategoryName",
                          a."DealerCode",
                          a."DealerName",
                          b."Code",
                          b."Name",
                          c."SalesPrice",
                          Dsi."ChannelCapabilityId",
                          Br."Code",
                          Pb."StockMaximum",
                          Pb."PartsAttribution",
                          Dsi."ChannelCapabilityId")t;


--品牌视图
create or replace view "PartsSalesCategoryView" as
select "Id" ,"BranchCode","Name" from yxdcs."PartsSalesCategory"
union
select "Id" ,"BranchCode","Name" from dcs."PartsSalesCategory"
union
select "Id" ,"BranchCode","Name" from gcdcs."PartsSalesCategory"
union
select "Id" ,"BranchCode","Name" from sddcs."PartsSalesCategory";



--发动机型号视图
create or replace view "EngineModelview" as
select 
a."BranchId",
a."EngineModel",
a."Status",
b."PartsSalesCategoryId" as "PartsSalesCategoryId",
b."EngineModeId",
b."EngineProductLineId" as "EngineProductLineId",
Cast(decode (b."EngineProductLineId",null,0, 1) as Number(1)) as "IsSetRelation"
from "EngineModel" a 
left join "EngineModelProductLine" b on b."EngineModeId"=a."Id"




--老PMS系统维修单视图
create or replace view "RepairOrderQuery" as
  select *  from "RepairOrderQuery_Tmp"
/
--创建QTS查询视图
--（福戴）
create table "QTS_FD"
(
  "CsCode"        VARCHAR2(50),
  "ZcbmCode"      VARCHAR2(100),
  "VinCode"       VARCHAR2(50),
  "Assembly_Date" DATE,
  "Bind_Date"     DATE not null,
  "Factory"       VARCHAR2(20),
  "Mapid"         VARCHAR2(50),
  "Patch"         VARCHAR2(20),
  "ProduceLine"   VARCHAR2(20),
  "MapName"       VARCHAR2(100)
)

--(萨普)
create table "QTS_SP"
(
  "CsCode"        VARCHAR2(50),
  "ZcbmCode"      VARCHAR2(100),
  "VinCode"       VARCHAR2(50),
  "Assembly_Date" DATE,
  "Bind_Date"     DATE not null,
  "Factory"       VARCHAR2(20),
  "Mapid"        VARCHAR2(50),
  "Patch"         VARCHAR2(20),
  "ProduceLine"   VARCHAR2(20),
  "MapName"       VARCHAR2(100),
  "SubmitTime"    DATE
)

--(轻型车)
create table "QTS_QXC"
(
  "CsCode"        VARCHAR2(50),
  "ZcbmCode"      VARCHAR2(100),
  "VinCode"       VARCHAR2(50),
  "Assembly_Date" DATE,
  "Bind_Date"     DATE not null,
  "Factory"       VARCHAR2(20),
  "Mapid"        VARCHAR2(50),
  "Patch"         VARCHAR2(20),
  "ProduceLine"   VARCHAR2(20),
  "MapName"       VARCHAR2(100),
  "SubmitTime"    DATE
)


--(多功能)
create table "QTS_DGN"
(
  "CsCode"        VARCHAR2(50),
  "ZcbmCode"      VARCHAR2(100),
  "VinCode"       VARCHAR2(50),
  "Assembly_Date" DATE,
  "Bind_Date"     DATE not null,
  "Factory"      VARCHAR2(20),
  "Mapid"         VARCHAR2(50),
  "Patch"         VARCHAR2(20),
  "ProduceLine"   VARCHAR2(20),
  "MapName"       VARCHAR2(100)
)

--(南方工程)
create table "QTS_NFGC"
(
  "CsCode"        VARCHAR2(50),
  "ZcbmCode"      VARCHAR2(100),
  "VinCode"       VARCHAR2(50),
  "Assembly_Date" DATE,
  "Bind_Date"     DATE not null,
  "Factory"      VARCHAR2(20),
  "Mapid"         VARCHAR2(50),
  "Patch"         VARCHAR2(20),
  "ProduceLine"   VARCHAR2(20),
  "MapName"       VARCHAR2(100)
)

--(时代)
create table "QTS_SD"
(
  "DetailId"      VARCHAR2(50) NOT NULL,
  "ZcbmCode"      VARCHAR2(100) NOT NULL,
  "VinCode"       VARCHAR2(50),
  "Assembly_Date" DATE,
  "Factory"      VARCHAR2(20),
  "Mapid"         VARCHAR2(50),
  "Patch"         VARCHAR2(20),
  "ProduceLine"   VARCHAR2(20),
  "MapName"       VARCHAR2(100),
  "ChuChangId"    VARCHAR2(20)
)

create or replace view "QTSDataQueryForDGN" as
  select * 
     from "QTS_DGN"
/

create or replace view "QTSDataQueryForSP" as
  select * 
     from "QTS_SP"
/

create or replace view "QTSDataQueryForQXC" as
  select * 
     from "QTS_QXC"
/

create or replace view "QTSDataQueryForFD" as
  select * 
     from "QTS_FD"
/

create or replace view "QTSDataQueryForNFGC" as
  select * 
     from "QTS_NFGC"
/

create or replace view "QTSDataQueryForSD" as
  select * 
     from "QTS_SD"
/
--SAP福戴发票信息视图
CREATE OR REPLACE VIEW "SAPInvoiceInfo_FD" AS
SELECT  "Id",
        "BUKRS"         as "CompanyCode",
         Cast ((2)AS NUMBER(9))             as "BusinessType",
        "Code",
        "InvoiceNumber",
        "InvoiceAmount",
        a."InvoiceTax",
        "CreateTime",
        "Status",
        "Message"
   from "Sap_FD_SalesBillInfo" a ;
--SAP发票信息视图
create or replace view "SAPInvoiceInfo" AS
 SELECT a."Id",
        "BUKRS"         as "CompanyCode",
        Cast ((2)AS NUMBER(9))   as "BusinessType",--定义保存到edmx为int32的属性
        "Code",
        "InvoiceNumber",
        "InvoiceAmount",
        a."InvoiceTax",
        "CreateTime",
        "Status",
        "Message"
   from "SAP_YX_SALESBILLINFO" a
 /*  union
 SELECT "Id",
        "BUKRS"         as "CompanyCode",
        1             as "BusinessType",
        "Code",
        "InvocieNumber",
        "InvoiceAmount",
        b."InvocieTax",
        "CreateTime",
        "Status",
        "Message"
   from "SAP_YX_InvoiceverAccountInfo" b*/

--车型视图
create or replace view "ProductCategoryView" as
Select distinct "ProductCategoryCode","ProductCategoryName" from "Product"

--建WMS接口日志视图表
create table "SYNCWMSINOUTLOGINFOView"
(
  "Objid"     NUMBER(9) not null,
  "SYNCDate"  DATE not null,
  "Code"      VARCHAR2(1000) not null,
  "SYNCType"  NUMBER(3) not null,
  "SYNCCode"  VARCHAR2(25) not null,
  "InOutCode" VARCHAR2(25),
  "Status"    NUMBER(3) default 0 not null,
  "WarehouseName" VARCHAR2(100)
)
/

--建WMS接口日志视图
CREATE OR REPLACE VIEW "ViewSYNCWMSINOUTLOGINFO" AS
Select  * From "SYNCWMSINOUTLOGINFOView";

 
--查询锁定单位（视图）
create or replace view "LocketUnitQuery" as
select a."Code",
       a."Status",a."OutboundType",
       (select "Code" from "SparePart" where "SparePart"."Id" = b."SparePartId") "SparePartCode",
       (select "Name" from "SparePart" where "SparePart"."Id" = b."SparePartId") "SparePartName",
       b."PlannedAmount" - nvl(b."OutboundFulfillment", 0) "LocketQty",
       a."CreatorName",
       a."CreateTime",
       (select "Code" from "Warehouse" where "Warehouse"."Id" = a."WarehouseId") "WarehouseCode",
       (select "Name" from "Warehouse" where "Warehouse"."Id" = a."WarehouseId") "WarehouseName",
       (select "Code" from "Company" where "Company"."Id"=a."CounterpartCompanyId") "CounterpartCompanyCode",
       (select "Name" from "Company" where "Company"."Id"=a."CounterpartCompanyId") "CounterpartCompanyName"
  from "PartsOutboundPlan" a
 inner join "PartsOutboundPlanDetail" b
    on a."Id" = b."PartsOutboundPlanId" where a."Status" in(1,2) 
and b."PlannedAmount" - nvl(b."OutboundFulfillment", 0)>0;

--销售结算源单据发票视图

create or replace view "SalesSettleInvoiceResource" as
select a."Id" "SalesSettleId",
       a."Code" "SalesSettleCode",
       '配件销售结算单' "Type",
       b."Id" "InvoiceId"
  from "PartsSalesSettlement" a
 inner join "InvoiceInformation" b
    on a."Id" = b."SourceId" and b."SourceType"=1
--采购结算源单据发票视图

create or replace view "PurchaseSettleInvoiceResource" as
select a."Id"  "PartsPurchaseSettleId",
       a."Code" "PartsPurchaseSettleCode",
       '配件采购结算单' "Type",
       b."InvoiceId" "InvoiceId"
  from "PartsPurchaseSettleBill" a
 inner join "PurchaseSettleInvoiceRel" b
    on a."Id" = b."PartsPurchaseSettleBillId"

--星级系数视图
create or replace view "GradeCoefficientView" as
select "Id", "PartsSalesCategoryName",a."Grade" from dcs."GradeCoefficient" a;

--产品线跨库视图

create or replace view "ServiceProductLineViewAllDB" as
select a."ProductLineId"  "Id",
       a."ProductLineName",
       b."Name"                                 as "PartsSalesCategoryName"
  from "ServiceProductLineView" a
 inner join "PartsSalesCategory" b
    on a."PartsSalesCategoryId" = b."Id";
--维修索赔管理-供应商节点查询视图
create or replace view "RepairClaimBillWithSupplier" as
select
     "RepairClaimBill"."Id", 
     "RepairClaimBill"."BranchId",
     "RepairClaimBill"."ProductLineType",
     "RepairClaimBill"."IfClaimToSupplier",
     "RepairClaimBill"."ClaimBillCode" ,
     "RepairClaimBill"."RepairContractCode",
     dsi."BusinessCode" as "CustomerCode",
     "RepairClaimBill"."RepairWorkOrder",
     "RepairClaimBill"."QualityInformationCode",
     "RepairClaimBill"."InitialApprovertComment",
     "RepairClaimBill"."OtherCost",
     "RepairClaimBill"."SupplierSettleStatus",
     "RepairClaimBill"."InitialApproverName" ,
     "RepairClaimBill"."RepairType",
     "RepairClaimBill"."Status",
     "RepairClaimBill"."AutoApproveStatus",
     "RepairClaimBill"."RepairClaimApplicationCode",
     "RepairClaimBill"."SupplierCheckStatus",
     "RepairClaimBill"."SupplierConfirmStatus" ,
     "RepairClaimBill"."SettlementStatus" ,
     "RepairClaimBill"."UsedPartsDisposalStatus",
     "RepairClaimBill"."RejectQty" ,
     "RepairClaimBill"."DealerId",
     "RepairClaimBill"."DealerCode",
     "RepairClaimBill"."DealerName",
     ve."SerialNumber" as "SerialNumber",
     ve."SalesDate" as "SalesDate",
     "RepairClaimBill"."Mileage" as "Mileage",
     "RepairClaimBill"."WorkingHours" as "WorkingHours",
     "RepairClaimBill"."Capacity" as "Capacity",
     sp."ProductLineId" as "ServiceProductLineId",
     sp."ProductLineCode" as "ServiceProductLineCode",
     sp."ProductLineName" as "ServiceProductLineName",
     ve."ProductCategoryName" as "VehicleType",
     "RepairClaimBill"."ApplicationType" as "ApplicationType",
     at."ApplicationTypeName" as "ApplicationTypeName",
     "RepairClaimBill"."MalfunctionId" as "MalfunctionId",
     "RepairClaimBill"."MalfunctionCode" as "MalfunctionCode",
     "RepairClaimBill"."MalfunctionDescription" as "MalfunctionDescription",
     "RepairClaimBill"."MalfunctionReason" as "MalfunctionReason",
     "RepairClaimBill"."FaultyPartsId" as "FaultyPartsId",
     "RepairClaimBill"."FaultyPartsCode" as "FaultyPartsCode",
     "RepairClaimBill"."FaultyPartsName" as "FaultyPartsName",
     "RepairClaimBill"."ClaimSupplierId" as "ClaimSupplierId",
     "RepairClaimBill"."ClaimSupplierCode" as "ClaimSupplierCode",
     "RepairClaimBill"."ClaimSupplierName" as "ClaimSupplierName",
     "RepairClaimBill"."ResponsibleUnitId" as "ResponsibleUnitId",
     ru."Code" as "ResponsibleUnitCode",
     ru."Name" as "ResponsibleUnitName",
     decode(k."IfSPByLabor",1,(select sum("DefaultLaborHour") from "RepairClaimItemDetail" where "RepairClaimBill"."Id"="RepairClaimItemDetail"."RepairClaimBillId") * nvl(k."LaborUnitPrice",0),
     (select sum("DefaultLaborHour"*"LaborUnitPrice") from "RepairClaimItemDetail" where "RepairClaimBill"."Id"="RepairClaimItemDetail"."RepairClaimBillId") * nvl(k."LaborCoefficient",0)) as "LaborCost",
     
     (select sum(t2."MaterialCost") from "RepairClaimItemDetail" t1
     inner join "RepairClaimMaterialDetail" t2 on t1."Id"=t2."RepairClaimItemDetailId"
     where "RepairClaimBill"."Id"=t1."RepairClaimBillId")* nvl(k."PartsClaimCoefficient",0) as "MaterialCost",
     
     ((select sum(t2."MaterialCost") from "RepairClaimItemDetail" t1
     inner join "RepairClaimMaterialDetail" t2 on t1."Id"=t2."RepairClaimItemDetailId"
     where "RepairClaimBill"."Id"=t1."RepairClaimBillId") * l."Rate"+ (select sum(t2."MaterialCost") from "RepairClaimItemDetail" t1
     inner join "RepairClaimMaterialDetail" t2 on t1."Id"=t2."RepairClaimItemDetailId"
     where "RepairClaimBill"."Id"=t1."RepairClaimBillId") * nvl(k."OldPartTransCoefficient",0)) as "PartsManagementCost",
     
     decode(k."IfSPByLabor",1,(((select sum("DefaultLaborHour") from "RepairClaimItemDetail" where "RepairClaimBill"."Id"="RepairClaimItemDetail"."RepairClaimBillId") * nvl(k."LaborUnitPrice",0))
     +((select sum(t2."MaterialCost") from "RepairClaimItemDetail" t1 inner join "RepairClaimMaterialDetail" t2 on t1."Id"=t2."RepairClaimItemDetailId" where "RepairClaimBill"."Id"=t1."RepairClaimBillId") * nvl(k."PartsClaimCoefficient",0))
     +((select sum(t2."MaterialCost") from "RepairClaimItemDetail" t1
     inner join "RepairClaimMaterialDetail" t2 on t1."Id"=t2."RepairClaimItemDetailId"
     where "RepairClaimBill"."Id"=t1."RepairClaimBillId") * l."Rate"+ (select sum(t2."MaterialCost") from "RepairClaimItemDetail" t1
     inner join "RepairClaimMaterialDetail" t2 on t1."Id"=t2."RepairClaimItemDetailId"
     where "RepairClaimBill"."Id"=t1."RepairClaimBillId") * nvl(k."OldPartTransCoefficient",0))+nvl("RepairClaimBill"."OtherCost",0)),
     ((select sum("DefaultLaborHour"*"LaborUnitPrice") from "RepairClaimItemDetail" where "RepairClaimBill"."Id"="RepairClaimItemDetail"."RepairClaimBillId") * nvl(k."LaborCoefficient",1))
     +nvl("RepairClaimBill"."MaterialCost",0)+nvl("RepairClaimBill"."PartsManagementCost",0)+nvl("RepairClaimBill"."OtherCost",0)
     ) as "TotalAmount",
     
     "RepairClaimBill"."Remark" as "Remark",
     "RepairClaimBill"."MarketingDepartmentId" as "MarketingDepartmentId",
     md."Code" as "MarketingDepartmentCode",
     md."Name" as "MarketingDepartmentName",
     ve."BridgeType" as "BridgeType",
     "RepairClaimBill"."PartsSalesCategoryId" as "PartsSalesCategoryId",
     pc."Name" as "PartsSalesCategoryName",
     "RepairClaimBill"."EngineModel" as "EngineModel",
     "RepairClaimBill"."EngineSerialNumber" as "EngineCode",
     "RepairClaimBill"."CreatorId" as "CreatorId",
     "RepairClaimBill"."CreatorName" as "CreatorName",
     "RepairClaimBill"."CreateTime" as "CreateTime",
     "RepairClaimBill"."ApproverId" as "ApproverId",
     "RepairClaimBill"."ApproverName" as "ApproverName",
     "RepairClaimBill"."ApproveTime" as "ApproveTime",
     "RepairClaimBill"."FinalApproverId" as "FinalApproverId",
     "RepairClaimBill"."FinalApproverName" as "FinalApproverName",
     "RepairClaimBill"."FinalApproverTime" as "FinalApproveTime",
     "RepairClaimBill"."SupplierConfirmComment" as "SupplierConfirmComment",
     "RepairClaimBill"."EngineSerialNumber" as "EngineSerialNumber",
     "RepairClaimBill"."ServiceDepartmentComment" as "ServiceDepartmentComment",
     "RepairClaimBill"."RejectReason" as "RejectReason",
     "RepairClaimBill"."VideoMobileNumber" as "VideoMobileNumber",
     "RepairClaimBill"."MVSOutRange" as "MVSOutRange",
     "RepairClaimBill"."MVSOutTime" as "MVSOutTime",
     "RepairClaimBill"."MVSOutDistance" as "MVSOutDistance",
     "RepairClaimBill"."MVSOutCoordinate" as "MVSOutCoordinate",
     rca."CheckComment" as "CheckComment",
     "RepairClaimBill"."OutOfFactoryDate" as "OutOfFactoryDate",
     "RepairClaimBill"."VehicleContactPerson" as "VehicleContactPerson",
     "RepairClaimBill"."ContactPhone" as "ContactPhone",
     "RepairClaimBill"."RepairRequestTime" as "RepairRequestTime"
 from "RepairClaimBill"
left join  "BranchSupplierRelation" k  on  "RepairClaimBill"."ClaimSupplierId"= k."SupplierId"
and "RepairClaimBill"."BranchId"= k."BranchId" and  "RepairClaimBill"."PartsSalesCategoryId"= k."PartsSalesCategoryId"
left join "PartsManagementCostRate" l on k."PartsManagementCostGradeId" = l."PartsManagementCostGradeId"
left join "ServiceProductLineView" sp on "RepairClaimBill"."ServiceProductLineId" = sp."ProductLineId"
left join "VehicleInformation" ve on "RepairClaimBill"."VehicleId" = ve."Id"
left join "SalesRegion" sr on "RepairClaimBill"."SalesRegionId" = sr."Id"
left join "MarketingDepartment" md  on "RepairClaimBill"."MarketingDepartmentId" = md."Id"
left join "PartsSalesCategory" pc on "RepairClaimBill"."PartsSalesCategoryId" = pc."Id"
left join "ResponsibleUnit" ru on "RepairClaimBill"."ResponsibleUnitId" = ru."Id"
left join "ApplicationType" at on "RepairClaimBill"."BranchId" = at."BranchId"
left join "RepairClaimApplication" rca on "RepairClaimBill"."RepairClaimApplicationId" = rca."Id"
left join "DealerServiceInfo" dsi on "RepairClaimBill"."DealerId"= dsi."DealerId"
and "RepairClaimBill"."PartsSalesCategoryId"=dsi."PartsSalesCategoryId";

/