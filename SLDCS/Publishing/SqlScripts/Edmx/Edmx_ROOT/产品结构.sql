﻿/*==============================================================*/
/* DBMS name:      ORACLE Version 10gR2                         */
/* Created on:     2016/11/16 15:25:10                          */
/*==============================================================*/



-- Type package declaration
create or replace package PDTypes  
as
    TYPE ref_cursor IS REF CURSOR;
end;

-- Integrity package declaration
create or replace package IntegrityPackage AS
 procedure InitNestLevel;
 function GetNestLevel return number;
 procedure NextNestLevel;
 procedure PreviousNestLevel;
 end IntegrityPackage;
/

-- Integrity package definition
create or replace package body IntegrityPackage AS
 NestLevel number;

-- Procedure to initialize the trigger nest level
 procedure InitNestLevel is
 begin
 NestLevel := 0;
 end;


-- Function to return the trigger nest level
 function GetNestLevel return number is
 begin
 if NestLevel is null then
     NestLevel := 0;
 end if;
 return(NestLevel);
 end;

-- Procedure to increase the trigger nest level
 procedure NextNestLevel is
 begin
 if NestLevel is null then
     NestLevel := 0;
 end if;
 NestLevel := NestLevel + 1;
 end;

-- Procedure to decrease the trigger nest level
 procedure PreviousNestLevel is
 begin
 NestLevel := NestLevel - 1;
 end;

 end IntegrityPackage;
/


drop trigger "tib_engineproductline"
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"BranchVehicleBrand"');
  if num>0 then
    execute immediate 'drop table "BranchVehicleBrand" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"EngineModel"');
  if num>0 then
    execute immediate 'drop table "EngineModel" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"EngineModelProductLine"');
  if num>0 then
    execute immediate 'drop table "EngineModelProductLine" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"EngineProductLine"');
  if num>0 then
    execute immediate 'drop table "EngineProductLine" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"ExtendedInfoTemplate"');
  if num>0 then
    execute immediate 'drop table "ExtendedInfoTemplate" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"Product"');
  if num>0 then
    execute immediate 'drop table "Product" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"ProductAffiProductCategory"');
  if num>0 then
    execute immediate 'drop table "ProductAffiProductCategory" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"ProductCategory"');
  if num>0 then
    execute immediate 'drop table "ProductCategory" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"ProductCategoryExtendInfo"');
  if num>0 then
    execute immediate 'drop table "ProductCategoryExtendInfo" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"ProductExtendInfo"');
  if num>0 then
    execute immediate 'drop table "ProductExtendInfo" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"ServProdLineProductDetail"');
  if num>0 then
    execute immediate 'drop table "ServProdLineProductDetail" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"ServiceProdLineProduct"');
  if num>0 then
    execute immediate 'drop table "ServiceProdLineProduct" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"ServiceProductLine"');
  if num>0 then
    execute immediate 'drop table "ServiceProductLine" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"VehicleCategory"');
  if num>0 then
    execute immediate 'drop table "VehicleCategory" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"VehicleCategoryProductDetail"');
  if num>0 then
    execute immediate 'drop table "VehicleCategoryProductDetail" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_BranchVehicleBrand"');
  if num>0 then
    execute immediate 'drop sequence "S_BranchVehicleBrand"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_EngineModel"');
  if num>0 then
    execute immediate 'drop sequence "S_EngineModel"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_EngineModelProductLine"');
  if num>0 then
    execute immediate 'drop sequence "S_EngineModelProductLine"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_ExtendedInfoTemplate"');
  if num>0 then
    execute immediate 'drop sequence "S_ExtendedInfoTemplate"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_OptionalCategory"');
  if num>0 then
    execute immediate 'drop sequence "S_OptionalCategory"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_OptionalList"');
  if num>0 then
    execute immediate 'drop sequence "S_OptionalList"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_Product"');
  if num>0 then
    execute immediate 'drop sequence "S_Product"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_ProductAffiProductCategory"');
  if num>0 then
    execute immediate 'drop sequence "S_ProductAffiProductCategory"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_ProductCategory"');
  if num>0 then
    execute immediate 'drop sequence "S_ProductCategory"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_ProductCategoryExtendInfo"');
  if num>0 then
    execute immediate 'drop sequence "S_ProductCategoryExtendInfo"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_ProductExtendInfo"');
  if num>0 then
    execute immediate 'drop sequence "S_ProductExtendInfo"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_ServProdLineProductDetail"');
  if num>0 then
    execute immediate 'drop sequence "S_ServProdLineProductDetail"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_ServiceProdLineProduct"');
  if num>0 then
    execute immediate 'drop sequence "S_ServiceProdLineProduct"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_ServiceProductLine"');
  if num>0 then
    execute immediate 'drop sequence "S_ServiceProductLine"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_VehicleCategory"');
  if num>0 then
    execute immediate 'drop sequence "S_VehicleCategory"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_VehicleCategoryProductDetail"');
  if num>0 then
    execute immediate 'drop sequence "S_VehicleCategoryProductDetail"';
  end if;
end;
/

create sequence "S_BranchVehicleBrand"
/

create sequence "S_EngineModel"
/

create sequence "S_EngineModelProductLine"
/

create sequence "S_ExtendedInfoTemplate"
/

create sequence "S_OptionalCategory"
/

create sequence "S_OptionalList"
/

create sequence "S_Product"
/

create sequence "S_ProductAffiProductCategory"
/

create sequence "S_ProductCategory"
/

create sequence "S_ProductCategoryExtendInfo"
/

create sequence "S_ProductExtendInfo"
/

create sequence "S_ServProdLineProductDetail"
/

create sequence "S_ServiceProdLineProduct"
/

create sequence "S_ServiceProductLine"
/

create sequence "S_VehicleCategory"
/

create sequence "S_VehicleCategoryProductDetail"
/

/*==============================================================*/
/* Table: "BranchVehicleBrand"                                  */
/*==============================================================*/
create table "BranchVehicleBrand"  (
   "Id"                 NUMBER(9)                       not null,
   "BranchId"           NUMBER(9)                       not null,
   "BrandCode"          VARCHAR2(50)                    not null,
   "BrandName"          VARCHAR2(100)                   not null,
   constraint PK_BRANCHVEHICLEBRAND primary key ("Id")
)
/

/*==============================================================*/
/* Table: "EngineModel"                                         */
/*==============================================================*/
create table "EngineModel"  (
   "Id"                 NUMBER(9)                       not null,
   "BranchId"           NUMBER(9)                       not null,
   "EngineModel"        VARCHAR2(50)                    not null,
   "Status"             NUMBER(9),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "RowVersion"         TIMESTAMP,
   constraint PK_ENGINEMODEL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "EngineModelProductLine"                              */
/*==============================================================*/
create table "EngineModelProductLine"  (
   "Id"                 NUMBER(9)                       not null,
   "BranchId"           NUMBER(9)                       not null,
   "PartsSalesCategoryId" NUMBER(9)                       not null,
   "EngineProductLineId" NUMBER(9)                       not null,
   "EngineModeId"       NUMBER(9)                       not null,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "RowVersion"         TIMESTAMP,
   constraint PK_ENGINEMODELPRODUCTLINE primary key ("Id")
)
/

/*==============================================================*/
/* Table: "EngineProductLine"                                   */
/*==============================================================*/
create table "EngineProductLine"  (
   "Id"                 NUMBER(9)                       not null,
   "BranchId"           NUMBER(9)                       not null,
   "PartsSalesCategoryId" NUMBER(9)                       not null,
   "EngineCode"         VARCHAR2(50)                    not null,
   "EngineName"         VARCHAR2(100)                   not null,
   "Remark"             VARCHAR2(200),
   "Status"             NUMBER(9)                       not null,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "RowVersion"         TIMESTAMP,
   constraint PK_ENGINEPRODUCTLINE primary key ("Id")
)
/

/*==============================================================*/
/* Table: "ExtendedInfoTemplate"                                */
/*==============================================================*/
create table "ExtendedInfoTemplate"  (
   "Id"                 NUMBER(9)                       not null,
   "Category"           NUMBER(9)                       not null,
   "ParentId"           NUMBER(9)                       not null,
   "QuestionPosition"   NUMBER(9),
   "DataType"           NUMBER(9)                       not null,
   "QuestionType"       NUMBER(9)                       not null,
   "Position"           NUMBER(9),
   "Content"            VARCHAR2(200)                   not null,
   "IsInputAllowed"     NUMBER(1),
   "Status"             NUMBER(9)                       not null,
   "IsSearchAllowed"    NUMBER(1),
   "CorporationId"      NUMBER(9),
   "CorporationName"    VARCHAR2(100),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "Remark"             VARCHAR2(200),
   "RowVersion"         TIMESTAMP,
   constraint PK_EXTENDEDINFOTEMPLATE primary key ("Id")
)
/

/*==============================================================*/
/* Table: "Product"                                             */
/*==============================================================*/
create table "Product"  (
   "Id"                 NUMBER(9)                       not null,
   "Code"               VARCHAR2(50)                    not null,
   "ProductCategoryCode" VARCHAR2(50)                    not null,
   "ProductCategoryName" VARCHAR2(200),
   "BrandId"            VARCHAR2(100)                   not null,
   "BrandCode"          VARCHAR2(50),
   "BrandName"          VARCHAR2(100),
   "SubBrandCode"       VARCHAR2(50),
   "SubBrandName"       VARCHAR2(100),
   "TerraceCode"        VARCHAR2(50),
   "TerraceName"        VARCHAR2(100),
   "ProductLine"        VARCHAR2(50),
   "ProductName"        VARCHAR2(100),
   "OldInternalCode"    VARCHAR2(50),
   "AnnoucementNumber"  VARCHAR2(100),
   "TonnageCode"        VARCHAR2(50),
   "EngineTypeCode"     VARCHAR2(50),
   "EngineManufacturerCode" VARCHAR2(50),
   "GearSerialTypeCode" VARCHAR2(50),
   "GearSerialManufacturerCode" VARCHAR2(50),
   "RearAxleTypeCode"   VARCHAR2(50),
   "RearAxleManufacturerCode" VARCHAR2(50),
   "TireTypeCode"       VARCHAR2(50),
   "TireFormCode"       VARCHAR2(50),
   "BrakeModeCode"      VARCHAR2(50),
   "DriveModeCode"      VARCHAR2(50),
   "FrameConnetCode"    VARCHAR2(50),
   "VehicleSize"        VARCHAR2(50),
   "AxleDistanceCode"   VARCHAR2(50),
   "EmissionStandardCode" VARCHAR2(50),
   "ProductFunctionCode" VARCHAR2(50),
   "ProductLevelCode"   VARCHAR2(50),
   "VehicleTypeCode"    VARCHAR2(50),
   "ColorDescribe"      VARCHAR2(100),
   "SeatingCapacity"    NUMBER(9),
   "Weight"             NUMBER(9),
   "Remark"             VARCHAR2(200),
   "Status"             NUMBER(9)                       not null,
   "OrderCycle"         NUMBER(9),
   "PurchaseCycle"      NUMBER(9),
   "ParameterDescription" VARCHAR2(200),
   "Version"            VARCHAR2(20),
   "Configuration"      VARCHAR2(50),
   "EngineCylinder"     VARCHAR2(50),
   "EmissionStandard"   VARCHAR2(50),
   "ManualAutomatic"    VARCHAR2(50),
   "ColorCode"          VARCHAR2(20),
   "Picture"            VARCHAR2(200),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "AbandonerId"        NUMBER(9),
   "AbandonerName"      VARCHAR2(100),
   "AbandonTime"        DATE,
   "RowVersion"         TIMESTAMP,
   constraint PK_PRODUCT primary key ("Id")
)
/

/*==============================================================*/
/* Table: "ProductAffiProductCategory"                          */
/*==============================================================*/
create table "ProductAffiProductCategory"  (
   "Id"                 NUMBER(9)                       not null,
   "ProductCategoryId"  NUMBER(9)                       not null,
   "ProductId"          NUMBER(9)                       not null,
   "RootCategoryId"     NUMBER(9)                       not null,
   constraint PK_PRODUCTAFFIPRODUCTCATEGORY primary key ("Id")
)
/

/*==============================================================*/
/* Table: "ProductCategory"                                     */
/*==============================================================*/
create table "ProductCategory"  (
   "Id"                 NUMBER(9)                       not null,
   "Code"               VARCHAR2(50)                    not null,
   "Name"               VARCHAR2(100)                   not null,
   "LayerNodeTypeId"    NUMBER(9),
   "RootCategoryId"     NUMBER(9)                       not null,
   "ParentId"           NUMBER(9),
   "Picture"            VARCHAR2(200),
   "Status"             NUMBER(9)                       not null,
   "ProductCategoryLevel" NUMBER(9)                       not null,
   "Remark"             VARCHAR2(200),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "AbandonerId"        NUMBER(9),
   "AbandonerName"      VARCHAR2(100),
   "AbandonTime"        DATE,
   "RowVersion"         TIMESTAMP,
   constraint PK_PRODUCTCATEGORY primary key ("Id")
)
/

/*==============================================================*/
/* Table: "ProductCategoryExtendInfo"                           */
/*==============================================================*/
create table "ProductCategoryExtendInfo"  (
   "Id"                 NUMBER(9)                       not null,
   "ProductCategoryId"  NUMBER(9)                       not null,
   "ExtendedInfoTemplateId" NUMBER(9),
   "Content"            VARCHAR2(200),
   constraint PK_PRODUCTCATEGORYEXTENDINFO primary key ("Id")
)
/

/*==============================================================*/
/* Table: "ProductExtendInfo"                                   */
/*==============================================================*/
create table "ProductExtendInfo"  (
   "Id"                 NUMBER(9)                       not null,
   "ProductId"          NUMBER(9)                       not null,
   "ExtendedInfoTemplateId" NUMBER(9),
   "Content"            VARCHAR2(200),
   constraint PK_PRODUCTEXTENDINFO primary key ("Id")
)
/

/*==============================================================*/
/* Table: "ServProdLineProductDetail"                           */
/*==============================================================*/
create table "ServProdLineProductDetail"  (
   "Id"                 NUMBER(9)                       not null,
   "ServiceProductLineId" NUMBER(9)                       not null,
   "ProductId"          NUMBER(9)                       not null,
   constraint PK_SERVPRODLINEPRODUCTDETAIL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "ServiceProdLineProduct"                              */
/*==============================================================*/
create table "ServiceProdLineProduct"  (
   "Id"                 NUMBER(9)                       not null,
   "BrandId"            VARCHAR2(100)                   not null,
   "VehicleBrandCode"   VARCHAR2(50),
   "VehicleBrandName"   VARCHAR2(100),
   "SubBrandCode"       VARCHAR2(50),
   "SubBrandName"       VARCHAR2(100),
   "TerraceCode"        VARCHAR2(50),
   "TerraceName"        VARCHAR2(100),
   "ProductFunctionCode" VARCHAR2(50),
   "VehicleProductLine" VARCHAR2(50),
   "VehicleProductName" VARCHAR2(100),
   "ServiceProductLineId" NUMBER(9),
   "ServiceProductLineCode" VARCHAR2(50),
   "ServiceProductLineName" VARCHAR2(100),
   "PartsSalesCategoryId" NUMBER(9),
   "SalesCategoryCode"  VARCHAR2(50),
   "SalesCategoryName"  VARCHAR2(100),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   constraint PK_SERVICEPRODLINEPRODUCT primary key ("Id")
)
/

/*==============================================================*/
/* Table: "ServiceProductLine"                                  */
/*==============================================================*/
create table "ServiceProductLine"  (
   "Id"                 NUMBER(9)                       not null,
   "BranchId"           NUMBER(9)                       not null,
   "PartsSalesCategoryId" NUMBER(9)                       not null,
   "Code"               VARCHAR2(50)                    not null,
   "Name"               VARCHAR2(100)                   not null,
   "Remark"             VARCHAR2(200),
   "Status"             NUMBER(9)                       not null,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "RowVersion"         TIMESTAMP,
   constraint PK_SERVICEPRODUCTLINE primary key ("Id")
)
/

/*==============================================================*/
/* Table: "VehicleCategory"                                     */
/*==============================================================*/
create table "VehicleCategory"  (
   "BranchId"           NUMBER(9)                       not null,
   "Id"                 NUMBER(9)                       not null,
   "Code"               VARCHAR2(50)                    not null,
   "Name"               VARCHAR2(100)                   not null,
   "ExtendedInfoTemplateCategory" NUMBER(9),
   "Status"             NUMBER(9)                       not null,
   "Remark"             VARCHAR2(200),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "RowVersion"         TIMESTAMP,
   constraint PK_VEHICLECATEGORY primary key ("Id")
)
/

/*==============================================================*/
/* Table: "VehicleCategoryProductDetail"                        */
/*==============================================================*/
create table "VehicleCategoryProductDetail"  (
   "Id"                 NUMBER(9)                       not null,
   "VehicleCategoryId"  NUMBER(9)                       not null,
   "ProductId"          NUMBER(9)                       not null,
   constraint PK_VEHICLECATEGORYPRODDETAIL primary key ("Id")
)
/


create trigger "tib_engineproductline" before insert
on "EngineProductLine" for each row
declare
    integrity_error  exception;
    errno            integer;
    errmsg           char(200);
    dummy            integer;
    found            boolean;

begin
    --  Column ""Id"" uses sequence S_ServiceProductLine
    select S_ServiceProductLine.NEXTVAL INTO :new."Id" from dual;

--  Errors handling
exception
    when integrity_error then
       raise_application_error(errno, errmsg);
end;
/

