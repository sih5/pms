/*==============================================================*/
/* DBMS name:      ORACLE Version 10gR2                         */
/* Created on:     2018/7/9 17:21:20                            */
/*==============================================================*/


declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"PartsRequisitionSettleBill"');
  if num>0 then
    execute immediate 'drop table "PartsRequisitionSettleBill" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"PartsRequisitionSettleDetail"');
  if num>0 then
    execute immediate 'drop table "PartsRequisitionSettleDetail" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"PartsRequisitionSettleRef"');
  if num>0 then
    execute immediate 'drop table "PartsRequisitionSettleRef" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_PartsRequisitionSettleBill"');
  if num>0 then
    execute immediate 'drop sequence "S_PartsRequisitionSettleBill"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_PartsRequisitionSettleDetail"');
  if num>0 then
    execute immediate 'drop sequence "S_PartsRequisitionSettleDetail"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_PartsRequisitionSettleRef"');
  if num>0 then
    execute immediate 'drop sequence "S_PartsRequisitionSettleRef"';
  end if;
end;
/

create sequence "S_PartsRequisitionSettleBill"
/

create sequence "S_PartsRequisitionSettleDetail"
/

create sequence "S_PartsRequisitionSettleRef"
/

/*==============================================================*/
/* Table: "PartsRequisitionSettleBill"                          */
/*==============================================================*/
create table "PartsRequisitionSettleBill"  (
   "Id"                 NUMBER(9)                       not null,
   "Code"               VARCHAR2(50)                    not null,
   "BranchId"           NUMBER(9)                       not null,
   "BranchCode"         VARCHAR2(50)                    not null,
   "BranchName"         VARCHAR2(100)                   not null,
   "PartsSalesCategoryId" NUMBER(9)                       not null,
   "PartsSalesCategoryName" VARCHAR2(100)                   not null,
   "SettleType"         NUMBER(9),
   "DepartmentId"       NUMBER(9)                       not null,
   "DepartmentCode"     VARCHAR2(50)                    not null,
   "DepartmentName"     VARCHAR2(100)                   not null,
   "TotalSettlementAmount" NUMBER(19,4)                    not null,
   "Remark"             VARCHAR2(200),
   "Status"             NUMBER(9)                       not null,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "ApproverId"         NUMBER(9),
   "ApproverName"       VARCHAR2(100),
   "ApproveTime"        DATE,
   "AbandonerId"        NUMBER(9),
   "AbandonerName"      VARCHAR2(100),
   "AbandonTime"        DATE,
   "RowVersion"         TIMESTAMP,
   "InvoiceDate"        DATE,
   "Type"               NUMBER(9),
   constraint PK_PARTSREQUISITIONSETTLEBILL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "PartsRequisitionSettleDetail"                        */
/*==============================================================*/
create table "PartsRequisitionSettleDetail"  (
   "Id"                 NUMBER(9)                       not null,
   "SerialNumber"       NUMBER(9)                       not null,
   "PartsRequisitionSettleBillId" NUMBER(9)                       not null,
   "SparePartId"        NUMBER(9)                       not null,
   "SparePartCode"      VARCHAR2(50)                    not null,
   "SparePartName"      VARCHAR2(100)                   not null,
   "SettlementPrice"    NUMBER(19,4)                    not null,
   "Quantity"           NUMBER(9)                       not null,
   constraint PK_PARTSREQUISITIONSETTLEDETAI primary key ("Id")
)
/

/*==============================================================*/
/* Table: "PartsRequisitionSettleRef"                           */
/*==============================================================*/
create table "PartsRequisitionSettleRef"  (
   "Id"                 NUMBER(9)                       not null,
   "PartsRequisitionSettleBillId" NUMBER(9)                       not null,
   "SourceId"           NUMBER(9)                       not null,
   "SourceCode"         VARCHAR2(50)                    not null,
   "SourceType"         NUMBER(9)                       not null,
   "SettlementAmount"   NUMBER(19,4)                    not null,
   constraint PK_PARTSREQUISITIONSETTLEREF primary key ("Id")
)
/

