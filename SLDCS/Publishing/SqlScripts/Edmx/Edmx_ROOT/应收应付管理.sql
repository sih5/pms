/*==============================================================*/
/* DBMS name:      ORACLE Version 10gR2                         */
/* Created on:     2023/6/9 14:31:57                            */
/*==============================================================*/


declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"AccountGroup"');
  if num>0 then
    execute immediate 'drop table "AccountGroup" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"AccountPayableHistoryDetail"');
  if num>0 then
    execute immediate 'drop table "AccountPayableHistoryDetail" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"AccountPeriod"');
  if num>0 then
    execute immediate 'drop table "AccountPeriod" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"AccountReceivableBill"');
  if num>0 then
    execute immediate 'drop table "AccountReceivableBill" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"BankAccount"');
  if num>0 then
    execute immediate 'drop table "BankAccount" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"CAReconciliation"');
  if num>0 then
    execute immediate 'drop table "CAReconciliation" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"CAReconciliationList"');
  if num>0 then
    execute immediate 'drop table "CAReconciliationList" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"CredenceApplication"');
  if num>0 then
    execute immediate 'drop table "CredenceApplication" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"CustomerAccount"');
  if num>0 then
    execute immediate 'drop table "CustomerAccount" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"CustomerAccountHisDetail"');
  if num>0 then
    execute immediate 'drop table "CustomerAccountHisDetail" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"CustomerAccountHistory"');
  if num>0 then
    execute immediate 'drop table "CustomerAccountHistory" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"CustomerOpenAccountApp"');
  if num>0 then
    execute immediate 'drop table "CustomerOpenAccountApp" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"CustomerTransferBill"');
  if num>0 then
    execute immediate 'drop table "CustomerTransferBill" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"CustomerTransferBill_Sync"');
  if num>0 then
    execute immediate 'drop table "CustomerTransferBill_Sync" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('ERRORTABLE_SAP_FD');
  if num>0 then
    execute immediate 'drop table ERRORTABLE_SAP_FD cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"ESBLogSAP"');
  if num>0 then
    execute immediate 'drop table "ESBLogSAP" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"ErrorTable_SAP_YX"');
  if num>0 then
    execute immediate 'drop table "ErrorTable_SAP_YX" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"FinancialSettlement"');
  if num>0 then
    execute immediate 'drop table "FinancialSettlement" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"FundMonthlySettleBill"');
  if num>0 then
    execute immediate 'drop table "FundMonthlySettleBill" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"FundMonthlySettleBillDetail"');
  if num>0 then
    execute immediate 'drop table "FundMonthlySettleBillDetail" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"InvoiceInformation"');
  if num>0 then
    execute immediate 'drop table "InvoiceInformation" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"PayOutBill"');
  if num>0 then
    execute immediate 'drop table "PayOutBill" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"PayOutPlan"');
  if num>0 then
    execute immediate 'drop table "PayOutPlan" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"PayOutPlanDetail"');
  if num>0 then
    execute immediate 'drop table "PayOutPlanDetail" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"PaymentBeneficiaryList"');
  if num>0 then
    execute immediate 'drop table "PaymentBeneficiaryList" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"PaymentBill"');
  if num>0 then
    execute immediate 'drop table "PaymentBill" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"PaymentBill_Sync"');
  if num>0 then
    execute immediate 'drop table "PaymentBill_Sync" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('SAP_18_SYNC');
  if num>0 then
    execute immediate 'drop table SAP_18_SYNC cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('SAP_18_SYNC_FD');
  if num>0 then
    execute immediate 'drop table SAP_18_SYNC_FD cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('SAP_19_SYNC');
  if num>0 then
    execute immediate 'drop table SAP_19_SYNC cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('SAP_19_SYNC_FD');
  if num>0 then
    execute immediate 'drop table SAP_19_SYNC_FD cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"SAP_YX_InvoiceverAccountInfo"');
  if num>0 then
    execute immediate 'drop table "SAP_YX_InvoiceverAccountInfo" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"SAP_YX_PaymentInfo"');
  if num>0 then
    execute immediate 'drop table "SAP_YX_PaymentInfo" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('SAP_YX_SALESBILLINFO');
  if num>0 then
    execute immediate 'drop table SAP_YX_SALESBILLINFO cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"SIHCreditInfo"');
  if num>0 then
    execute immediate 'drop table "SIHCreditInfo" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"SIHCreditInfoDetail"');
  if num>0 then
    execute immediate 'drop table "SIHCreditInfoDetail" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"SalesInvoice_tmp"');
  if num>0 then
    execute immediate 'drop table "SalesInvoice_tmp" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"Sap_FD_InvoiceVerAccountInfo"');
  if num>0 then
    execute immediate 'drop table "Sap_FD_InvoiceVerAccountInfo" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"Sap_FD_PurchaseRtnInvoice"');
  if num>0 then
    execute immediate 'drop table "Sap_FD_PurchaseRtnInvoice" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"Sap_FD_SalesBillInfo"');
  if num>0 then
    execute immediate 'drop table "Sap_FD_SalesBillInfo" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"Summary"');
  if num>0 then
    execute immediate 'drop table "Summary" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"SupplierAccount"');
  if num>0 then
    execute immediate 'drop table "SupplierAccount" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"SupplierInformation"');
  if num>0 then
    execute immediate 'drop table "SupplierInformation" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"SupplierOpenAccountApp"');
  if num>0 then
    execute immediate 'drop table "SupplierOpenAccountApp" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"SupplierTransferBill"');
  if num>0 then
    execute immediate 'drop table "SupplierTransferBill" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_AccountGroup"');
  if num>0 then
    execute immediate 'drop sequence "S_AccountGroup"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_AccountPayableHistoryDetail"');
  if num>0 then
    execute immediate 'drop sequence "S_AccountPayableHistoryDetail"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_AccountPeriod"');
  if num>0 then
    execute immediate 'drop sequence "S_AccountPeriod"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_AccountReceivableBill"');
  if num>0 then
    execute immediate 'drop sequence "S_AccountReceivableBill"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_BankAccount"');
  if num>0 then
    execute immediate 'drop sequence "S_BankAccount"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_CAReconciliation"');
  if num>0 then
    execute immediate 'drop sequence "S_CAReconciliation"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_CAReconciliationList"');
  if num>0 then
    execute immediate 'drop sequence "S_CAReconciliationList"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_CredenceApplication"');
  if num>0 then
    execute immediate 'drop sequence "S_CredenceApplication"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_CustomerAccount"');
  if num>0 then
    execute immediate 'drop sequence "S_CustomerAccount"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_CustomerAccountHisDetail"');
  if num>0 then
    execute immediate 'drop sequence "S_CustomerAccountHisDetail"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_CustomerAccountHistory"');
  if num>0 then
    execute immediate 'drop sequence "S_CustomerAccountHistory"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_CustomerOpenAccountApp"');
  if num>0 then
    execute immediate 'drop sequence "S_CustomerOpenAccountApp"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_CustomerTransferBill"');
  if num>0 then
    execute immediate 'drop sequence "S_CustomerTransferBill"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_CustomerTransferBill_Sync"');
  if num>0 then
    execute immediate 'drop sequence "S_CustomerTransferBill_Sync"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_ERRORTABLE_SAP_FD');
  if num>0 then
    execute immediate 'drop sequence S_ERRORTABLE_SAP_FD';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_ESBLogSAP"');
  if num>0 then
    execute immediate 'drop sequence "S_ESBLogSAP"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_ErrorTable_SAP_YX"');
  if num>0 then
    execute immediate 'drop sequence "S_ErrorTable_SAP_YX"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_FinancialSettlement"');
  if num>0 then
    execute immediate 'drop sequence "S_FinancialSettlement"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_FundMonthlySettleBill"');
  if num>0 then
    execute immediate 'drop sequence "S_FundMonthlySettleBill"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_FundMonthlySettleBillDetail"');
  if num>0 then
    execute immediate 'drop sequence "S_FundMonthlySettleBillDetail"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_InvoiceInformation"');
  if num>0 then
    execute immediate 'drop sequence "S_InvoiceInformation"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_PayOutBill"');
  if num>0 then
    execute immediate 'drop sequence "S_PayOutBill"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_PayOutPlan"');
  if num>0 then
    execute immediate 'drop sequence "S_PayOutPlan"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_PaymentBeneficiaryList"');
  if num>0 then
    execute immediate 'drop sequence "S_PaymentBeneficiaryList"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_PaymentBill"');
  if num>0 then
    execute immediate 'drop sequence "S_PaymentBill"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_PaymentBill_Sync"');
  if num>0 then
    execute immediate 'drop sequence "S_PaymentBill_Sync"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_SAP_18_SYNC');
  if num>0 then
    execute immediate 'drop sequence S_SAP_18_SYNC';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_SAP_18_SYNC_FD');
  if num>0 then
    execute immediate 'drop sequence S_SAP_18_SYNC_FD';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_SAP_19_SYNC');
  if num>0 then
    execute immediate 'drop sequence S_SAP_19_SYNC';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_SAP_19_SYNC_FD');
  if num>0 then
    execute immediate 'drop sequence S_SAP_19_SYNC_FD';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_SAP_YX_InvoiceverAccountInfo"');
  if num>0 then
    execute immediate 'drop sequence "S_SAP_YX_InvoiceverAccountInfo"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_SAP_YX_PaymentInfo"');
  if num>0 then
    execute immediate 'drop sequence "S_SAP_YX_PaymentInfo"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_SAP_YX_SALESBILLINFO');
  if num>0 then
    execute immediate 'drop sequence S_SAP_YX_SALESBILLINFO';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_SIHCreditInfo"');
  if num>0 then
    execute immediate 'drop sequence "S_SIHCreditInfo"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_SIHCreditInfoDetail"');
  if num>0 then
    execute immediate 'drop sequence "S_SIHCreditInfoDetail"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_SalesInvoice_tmp"');
  if num>0 then
    execute immediate 'drop sequence "S_SalesInvoice_tmp"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_Sap_FD_InvoiceVerAccountInfo"');
  if num>0 then
    execute immediate 'drop sequence "S_Sap_FD_InvoiceVerAccountInfo"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_Sap_FD_PurchaseRtnInvoice"');
  if num>0 then
    execute immediate 'drop sequence "S_Sap_FD_PurchaseRtnInvoice"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_Sap_FD_SalesBillInfo"');
  if num>0 then
    execute immediate 'drop sequence "S_Sap_FD_SalesBillInfo"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_Summary"');
  if num>0 then
    execute immediate 'drop sequence "S_Summary"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_SupplierAccount"');
  if num>0 then
    execute immediate 'drop sequence "S_SupplierAccount"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_SupplierInformation"');
  if num>0 then
    execute immediate 'drop sequence "S_SupplierInformation"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_SupplierOpenAccountApp"');
  if num>0 then
    execute immediate 'drop sequence "S_SupplierOpenAccountApp"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_SupplierTransferBill"');
  if num>0 then
    execute immediate 'drop sequence "S_SupplierTransferBill"';
  end if;
end;
/

create sequence "S_AccountGroup"
/

create sequence "S_AccountPayableHistoryDetail"
/

create sequence "S_AccountPeriod"
/

create sequence "S_AccountReceivableBill"
/

create sequence "S_BankAccount"
/

create sequence "S_CAReconciliation"
/

create sequence "S_CAReconciliationList"
/

create sequence "S_CredenceApplication"
/

create sequence "S_CustomerAccount"
/

create sequence "S_CustomerAccountHisDetail"
/

create sequence "S_CustomerAccountHistory"
/

create sequence "S_CustomerOpenAccountApp"
/

create sequence "S_CustomerTransferBill"
/

create sequence "S_CustomerTransferBill_Sync"
/

create sequence S_ERRORTABLE_SAP_FD
/

create sequence "S_ESBLogSAP"
/

create sequence "S_ErrorTable_SAP_YX"
/

create sequence "S_FinancialSettlement"
/

create sequence "S_FundMonthlySettleBill"
/

create sequence "S_FundMonthlySettleBillDetail"
/

create sequence "S_InvoiceInformation"
/

create sequence "S_PayOutBill"
/

create sequence "S_PayOutPlan"
/

create sequence "S_PaymentBeneficiaryList"
/

create sequence "S_PaymentBill"
/

create sequence "S_PaymentBill_Sync"
/

create sequence S_SAP_18_SYNC
/

create sequence S_SAP_18_SYNC_FD
/

create sequence S_SAP_19_SYNC
/

create sequence S_SAP_19_SYNC_FD
/

create sequence "S_SAP_YX_InvoiceverAccountInfo"
/

create sequence "S_SAP_YX_PaymentInfo"
/

create sequence S_SAP_YX_SALESBILLINFO
/

create sequence "S_SIHCreditInfo"
/

create sequence "S_SIHCreditInfoDetail"
/

create sequence "S_SalesInvoice_tmp"
/

create sequence "S_Sap_FD_InvoiceVerAccountInfo"
/

create sequence "S_Sap_FD_PurchaseRtnInvoice"
/

create sequence "S_Sap_FD_SalesBillInfo"
/

create sequence "S_Summary"
/

create sequence "S_SupplierAccount"
/

create sequence "S_SupplierInformation"
/

create sequence "S_SupplierOpenAccountApp"
/

create sequence "S_SupplierTransferBill"
/

/*==============================================================*/
/* Table: "AccountGroup"                                        */
/*==============================================================*/
create table "AccountGroup"  (
   "Id"                 NUMBER(9)                       not null,
   "SalesCompanyId"     NUMBER(9)                       not null,
   "Code"               VARCHAR2(50)                    not null,
   "Name"               VARCHAR2(100)                   not null,
   "AccountType"        NUMBER(9)                       not null,
   "Status"             NUMBER(9)                       not null,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "AbandonerId"        NUMBER(9),
   "AbandonerName"      VARCHAR2(100),
   "AbandonTime"        DATE,
   "RowVersion"         TIMESTAMP,
   "SalesCompanyName"   VARCHAR2(100),
   constraint PK_ACCOUNTGROUP primary key ("Id")
)
/

/*==============================================================*/
/* Table: "AccountPayableHistoryDetail"                         */
/*==============================================================*/
create table "AccountPayableHistoryDetail"  (
   "Id"                 NUMBER(9)                       not null,
   "SupplierAccountId"  NUMBER(9)                       not null,
   "ChangeAmount"       NUMBER(19,4)                    not null,
   "debit"              NUMBER(19,4),
   "credit"             NUMBER(19,4),
   "BeforeChangeAmount" NUMBER(19,4),
   "AfterChangeAmount"  NUMBER(19,4),
   "ProcessDate"        DATE,
   "BusinessType"       NUMBER(9),
   "SourceId"           NUMBER(9),
   "SourceCode"         VARCHAR2(50),
   "Summary"            VARCHAR2(200),
   constraint PK_ACCOUNTPAYABLEHISTORYDETAIL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "AccountPeriod"                                       */
/*==============================================================*/
create table "AccountPeriod"  (
   "Id"                 NUMBER(9)                       not null,
   "Year"               VARCHAR2(20),
   "Month"              VARCHAR2(20),
   "Status"             NUMBER(9),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   constraint PK_ACCOUNTPERIOD primary key ("Id")
)
/

/*==============================================================*/
/* Table: "AccountReceivableBill"                               */
/*==============================================================*/
create table "AccountReceivableBill"  (
   "Id"                 NUMBER(9)                       not null,
   "Code"               VARCHAR2(50)                    not null,
   "Type"               NUMBER(9)                       not null,
   "SalesCompanyId"     NUMBER(9)                       not null,
   "CustomerCompanyId"  NUMBER(9)                       not null,
   "CustomerCompanyCode" VARCHAR2(50)                    not null,
   "CustomerCompanyName" VARCHAR2(100)                   not null,
   "Drawer"             VARCHAR2(100)                   not null,
   "DrawerAccount"      VARCHAR2(100)                   not null,
   "DrawerBank"         VARCHAR2(100)                   not null,
   "IssueDate"          DATE                            not null,
   "Amount"             NUMBER(19,4)                    not null,
   "Operator"           VARCHAR2(100)                   not null,
   "Drawee"             VARCHAR2(100),
   "AcceptanceDate"     DATE,
   "MaturityInterestRate" NUMBER(15,6),
   "MaturityInterest"   NUMBER(19,4),
   "ArBankAccountId"    NUMBER(9),
   "DiscountDate"       DATE,
   "DiscountAmount"     NUMBER(19,4),
   "DiscountOperator"   VARCHAR2(100),
   "Summary"            VARCHAR2(200),
   "Status"             NUMBER(9)                       not null,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "RowVersion"         TIMESTAMP,
   constraint PK_ACCOUNTRECEIVABLEBILL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "BankAccount"                                         */
/*==============================================================*/
create table "BankAccount"  (
   "Id"                 NUMBER(9)                       not null,
   "OwnerCompanyId"     NUMBER(9)                       not null,
   "BankName"           VARCHAR2(100)                   not null,
   "BankAccountNumber"  VARCHAR2(100)                   not null,
   "Purpose"            VARCHAR2(200),
   "Remark"             VARCHAR2(200),
   "Status"             NUMBER(9)                       not null,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "RowVersion"         TIMESTAMP,
   constraint PK_BANKACCOUNT primary key ("Id")
)
/

/*==============================================================*/
/* Table: "CAReconciliation"                                    */
/*==============================================================*/
create table "CAReconciliation"  (
   "Id"                 NUMBER(9)                       not null,
   "StatementCode"      VARCHAR2(50)                    not null,
   "AccountGroupId"     NUMBER(9),
   "AccountGroupName"   VARCHAR2(100),
   "CorporationId"      NUMBER(9)                       not null,
   "CorporationCode"    VARCHAR2(100)                   not null,
   "CorporationName"    VARCHAR2(100)                   not null,
   "YeBalance"          NUMBER(19,4)                    not null,
   "ReconciliationTime" DATE                            not null,
   "ReconciliationId"   NUMBER(9)                       not null,
   "ReconciliationName" VARCHAR2(100)                   not null,
   "Status"             NUMBER(9)                       not null,
   "Remark"             VARCHAR2(500),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "SubmitterId"        NUMBER(9),
   "SubmitterName"      VARCHAR2(100),
   "SubmitTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifierTime"       DATE,
   "ConfirmorId"        NUMBER(9),
   "ConfirmorName"      VARCHAR2(100),
   "ConfirmorTime"      DATE,
   "AbandonerId"        NUMBER(9),
   "AbandonerName"      VARCHAR2(100),
   "AbandonerTime"      DATE,
   "PartsSalesCategoryId" NUMBER(9),
   "PartsSalesCategoryName" VARCHAR2(50),
   "NoArrivalAdd"       NUMBER(19,4),
   "NoArrivalReduce"    NUMBER(19,4),
   "AfterAdjustmentAmount" NUMBER(19,4),
   "CustomerAccountAmount" NUMBER(19,4),
   constraint PK_CARECONCILIATION primary key ("Id")
)
/

/*==============================================================*/
/* Table: "CAReconciliationList"                                */
/*==============================================================*/
create table "CAReconciliationList"  (
   "Id"                 NUMBER(9)                       not null,
   "CAReconciliationId" NUMBER(9)                       not null,
   "ReconciliationDirection" NUMBER(9)                       not null,
   "Time"               DATE                            not null,
   "Abstract"           VARCHAR2(500)                   not null,
   "Money"              NUMBER(19,4)                    not null,
   "Remark"             VARCHAR2(500),
   constraint PK_CARECONCILIATIONLIST primary key ("Id")
)
/

/*==============================================================*/
/* Table: "CredenceApplication"                                 */
/*==============================================================*/
create table "CredenceApplication"  (
   "Id"                 NUMBER(9)                       not null,
   "Code"               VARCHAR2(50)                    not null,
   "SalesCompanyId"     NUMBER(9)                       not null,
   "GuarantorCompanyId" NUMBER(9)                       not null,
   "GuarantorCompanyName" VARCHAR2(100)                   not null,
   "MortgageAssets"     VARCHAR2(100),
   "FinancingContractId" NUMBER(9),
   "FinancingContractCode" VARCHAR2(50),
   "AccountGroupId"     NUMBER(9)                       not null,
   "CustomerCompanyId"  NUMBER(9)                       not null,
   "ValidationDate"     DATE                            not null,
   "ExpireDate"         DATE                            not null,
   "CredencePurpose"    VARCHAR2(200),
   "ApplyCondition"     NUMBER(9)                       not null,
   "CredenceLimit"      NUMBER(19,4)                    not null,
   "Status"             NUMBER(9)                       not null,
   "ConsumedAmount"     NUMBER(19,4)                    not null,
   "Remark"             VARCHAR2(200),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "AbandonerId"        NUMBER(9),
   "AbandonerName"      VARCHAR2(100),
   "AbandonTime"        DATE,
   "InitialApproverId"  NUMBER(9),
   "InitialApproverName" VARCHAR2(100),
   "InitialApproveTime" DATE,
   "CheckerId"          NUMBER(9),
   "CheckerName"        VARCHAR2(100),
   "CheckTime"          DATE,
   "UpperCheckerId"     NUMBER(9),
   "UpperCheckerName"   VARCHAR2(100),
   "UpperCheckTime"     DATE,
   "UpperApproverId"    NUMBER(9),
   "UpperApproverName"  VARCHAR2(100),
   "UpperApproveTime"   DATE,
   "ApproverId"         NUMBER(9),
   "ApproverName"       VARCHAR2(100),
   "ApproveTime"        DATE,
   "RowVersion"         TIMESTAMP,
   "CreditType"         NUMBER(9),
   "Path"               VARCHAR2(2000),
   constraint PK_CREDENCEAPPLICATION primary key ("Id")
)
/

/*==============================================================*/
/* Table: "CustomerAccount"                                     */
/*==============================================================*/
create table "CustomerAccount"  (
   "Id"                 NUMBER(9)                       not null,
   "AccountGroupId"     NUMBER(9)                       not null,
   "CustomerCompanyId"  NUMBER(9)                       not null,
   "AccountBalance"     NUMBER(19,4)                    not null,
   "ShippedProductValue" NUMBER(19,4)                    not null,
   "PendingAmount"      NUMBER(19,4)                    not null,
   "CustomerCredenceAmount" NUMBER(19,4)                    not null,
   "Status"             NUMBER(9)                       not null,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "TempCreditTotalFee" NUMBER(19,4),
   "SIHCredit"          NUMBER(19,4),
   "SIHCreditZB"        NUMBER(19,4),
   "ValidationTime"     DATE,
   "ExpireTime"         DATE,
   constraint PK_CUSTOMERACCOUNT primary key ("Id")
)
/

/*==============================================================*/
/* Table: "CustomerAccountHisDetail"                            */
/*==============================================================*/
create table "CustomerAccountHisDetail"  (
   "Id"                 NUMBER(9)                       not null,
   "CustomerAccountId"  NUMBER(9)                       not null,
   "ChangeAmount"       NUMBER(19,4),
   "debit"              NUMBER(19,4),
   "credit"             NUMBER(19,4),
   "BeforeChangeAmount" NUMBER(19,4),
   "AfterChangeAmount"  NUMBER(19,4),
   "ProcessDate"        DATE,
   "BusinessType"       NUMBER(9),
   "SourceId"           NUMBER(9),
   "SourceCode"         VARCHAR2(50),
   "Summary"            VARCHAR2(200),
   "CreatorName"        VARCHAR2(100),
   "InvoiceDate"        DATE,
   "SerialType"         NUMBER(9),
   constraint PK_CUSTOMERACCOUNTHISDETAIL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "CustomerAccountHistory"                              */
/*==============================================================*/
create table "CustomerAccountHistory"  (
   "Id"                 NUMBER(9)                       not null,
   "AccountId"          NUMBER(9)                       not null,
   "AccountBalance"     NUMBER(19,4)                    not null,
   "ShippedProductValue" NUMBER(19,4)                    not null,
   "PendingAmount"      NUMBER(19,4)                    not null,
   "CustomerCredenceAmount" NUMBER(19,4)                    not null,
   "Status"             NUMBER(9)                       not null,
   "CustomerCompanyId"  NUMBER(9)                       not null,
   "Type"               NUMBER(9)                       not null,
   "Code"               VARCHAR2(50)                    not null,
   "Name"               VARCHAR2(100)                   not null,
   "AccountGroupCode"   VARCHAR2(50)                    not null,
   "AccountGroupName"   VARCHAR2(100)                   not null,
   "WshBalance"         NUMBER(19,4),
   "KyBalance"          NUMBER(19,4),
   "YeBalance"          NUMBER(19,4),
   "SdBalance"          NUMBER(19,4),
   "FlBalance"          NUMBER(19,4),
   "TheDate"            DATE,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "RowVersion"         TIMESTAMP,
   "AccountGroupId"     NUMBER(9)                       not null,
   "InvoiceDate"        DATE,
   "SIHCredit"          NUMBER(19,4),
   "TempCreditTotalFee" NUMBER(19,4),
   constraint PK_CUSTOMERACCOUNTHISTORY primary key ("Id")
)
/

/*==============================================================*/
/* Table: "CustomerOpenAccountApp"                              */
/*==============================================================*/
create table "CustomerOpenAccountApp"  (
   "Id"                 NUMBER(9)                       not null,
   "Code"               VARCHAR2(50)                    not null,
   "AccountGroupId"     NUMBER(9)                       not null,
   "CustomerCompanyId"  NUMBER(9)                       not null,
   "AccountInitialDeposit" NUMBER(19,4)                    not null,
   "Operator"           VARCHAR2(100),
   "Status"             NUMBER(9)                       not null,
   "Remark"             VARCHAR2(200),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "AbandonerId"        NUMBER(9),
   "AbandonerName"      VARCHAR2(100),
   "AbandonTime"        DATE,
   "RowVersion"         TIMESTAMP,
   constraint PK_CUSTOMEROPENACCOUNTAPP primary key ("Id")
)
/

/*==============================================================*/
/* Table: "CustomerTransferBill"                                */
/*==============================================================*/
create table "CustomerTransferBill"  (
   "Id"                 NUMBER(9)                       not null,
   "Code"               VARCHAR2(50)                    not null,
   "SalesCompanyId"     NUMBER(9)                       not null,
   "OutboundAccountGroupId" NUMBER(9)                       not null,
   "OutboundCustomerCompanyId" NUMBER(9)                       not null,
   "InboundAccountGroupId" NUMBER(9)                       not null,
   "InboundCustomerCompanyId" NUMBER(9)                       not null,
   "Summary"            VARCHAR2(200),
   "Amount"             NUMBER(19,4)                    not null,
   "CredentialDocument" VARCHAR2(100),
   "Operator"           VARCHAR2(100),
   "Status"             NUMBER(9)                       not null,
   "Remark"             VARCHAR2(200),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "RowVersion"         TIMESTAMP,
   "InvoiceDate"        DATE,
   "BusinessType"       NUMBER(9),
   "IsNegative"         NUMBER(1),
   constraint PK_CUSTOMERTRANSFERBILL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "CustomerTransferBill_Sync"                           */
/*==============================================================*/
create table "CustomerTransferBill_Sync"  (
   "Id"                 NUMBER(9)                       not null,
   "Objid"              VARCHAR2(40),
   "IOStatus"           NUMBER(9),
   "CreateTime"         DATE,
   "FirstSendTime"      DATE,
   "ModifySendTime"     DATE,
   "Token"              VARCHAR2(50),
   "Sender"             VARCHAR2(20),
   "Message"            VARCHAR2(500),
   constraint PK_CUSTOMERTRANSFERBILL_SYNC primary key ("Id")
)
/

/*==============================================================*/
/* Table: ERRORTABLE_SAP_FD                                     */
/*==============================================================*/
create table ERRORTABLE_SAP_FD  (
   "Id"                 NUMBER(9)                       not null,
   "BussinessId"        NUMBER(9),
   "BussinessCode"      VARCHAR2(100),
   "BussinessTableName" VARCHAR2(100),
   "SYNCTypeName"       NUMBER(9),
   "Message"            VARCHAR2(100),
   "Status"             NUMBER(9),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "SysCode"            VARCHAR2(100),
   constraint PK_ERRORTABLE_SAP_FD primary key ("Id")
)
/

/*==============================================================*/
/* Table: "ESBLogSAP"                                           */
/*==============================================================*/
create table "ESBLogSAP"  (
   "Id"                 NUMBER(9)                       not null,
   "InterfaceName"      VARCHAR2(50),
   "SyncNumberBegin"    NUMBER(9),
   "SyncNumberEnd"      NUMBER(9),
   "Message"            CLOB,
   "TheDate"            DATE,
   constraint PK_ESBLOGSAP primary key ("Id")
)
/

/*==============================================================*/
/* Table: "ErrorTable_SAP_YX"                                   */
/*==============================================================*/
create table "ErrorTable_SAP_YX"  (
   "Id"                 NUMBER(9)                       not null,
   "BussinessId"        NUMBER(9),
   "BussinessCode"      VARCHAR2(100),
   "BussinessTableName" VARCHAR2(100),
   "SyncTypeName"       VARCHAR2(100),
   "Message"            VARCHAR2(100),
   "Status"             NUMBER(9),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "SysCode"            VARCHAR2(100),
   constraint PK_ERRORTABLE_SAP_YX primary key ("Id")
)
/

/*==============================================================*/
/* Table: "FinancialSettlement"                                 */
/*==============================================================*/
create table "FinancialSettlement"  (
   "Id"                 NUMBER(9)                       not null,
   "Status"             NUMBER(9)                       not null,
   "Type"               NUMBER(9)                       not null,
   "StartDate"          DATE,
   "EndDate"            DATE,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "AbandonerId"        NUMBER(9),
   "AbandonerName"      VARCHAR2(100),
   "AbandonTime"        DATE,
   "RowVersion"         TIMESTAMP,
   constraint PK_FINANCIALSETTLEMENT primary key ("Id")
)
/

/*==============================================================*/
/* Table: "FundMonthlySettleBill"                               */
/*==============================================================*/
create table "FundMonthlySettleBill"  (
   "Id"                 NUMBER(9)                       not null,
   "MonthlySettleYear"  VARCHAR2(20),
   "MonthlySettleMonth" VARCHAR2(20),
   "Status"             NUMBER(9)                       not null,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   constraint PK_FUNDMONTHLYSETTLEBILL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "FundMonthlySettleBillDetail"                         */
/*==============================================================*/
create table "FundMonthlySettleBillDetail"  (
   "Id"                 NUMBER(9)                       not null,
   "FundMonthlySettleBillId" NUMBER(9)                       not null,
   "AccountGroupId"     NUMBER(9),
   "AccountGroupName"   VARCHAR2(100),
   "CustomerAccountId"  NUMBER(9),
   "CustomerCompanyId"  NUMBER(9),
   "CustomerCompanyCode" VARCHAR2(50),
   "CustomerCompanyName" VARCHAR2(100),
   "InitialAmount"      NUMBER(19,4),
   "ChangeAmount"       NUMBER(19,4),
   "FinalAmount"        NUMBER(19,4),
   "MonthlySettleYear"  VARCHAR2(20),
   "MonthlySettleMonth" VARCHAR2(20),
   constraint PK_FUNDMONTHLYSETTLEBILLDETAIL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "InvoiceInformation"                                  */
/*==============================================================*/
create table "InvoiceInformation"  (
   "Id"                 NUMBER(9)                       not null,
   "Code"               VARCHAR2(50)                    not null,
   "InvoicePurpose"     NUMBER(9)                       not null,
   "OwnerCompanyId"     NUMBER(9)                       not null,
   "InvoiceCompanyId"   NUMBER(9)                       not null,
   "InvoiceCompanyCode" VARCHAR2(50)                    not null,
   "InvoiceCompanyName" VARCHAR2(100)                   not null,
   "InvoiceReceiveCompanyId" NUMBER(9)                       not null,
   "InvoiceReceiveCompanyCode" VARCHAR2(50)                    not null,
   "InvoiceReceiveCompanyName" VARCHAR2(100)                   not null,
   "Type"               NUMBER(9)                       not null,
   "InvoiceCode"        VARCHAR2(50),
   "InvoiceNumber"      VARCHAR2(50),
   "InvoiceAmount"      NUMBER(19,4),
   "InvoiceTax"         NUMBER(19,4),
   "TaxRate"            NUMBER(15,6),
   "InvoiceDate"        DATE,
   "SourceId"           NUMBER(9)                       not null,
   "SourceType"         NUMBER(9)                       not null,
   "SourceCode"         VARCHAR2(50)                    not null,
   "Status"             NUMBER(9)                       not null,
   "VoucherNumber"      VARCHAR2(30),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "RowVersion"         TIMESTAMP,
   "PartsSalesCategoryId" NUMBER(9),
   "ApproverId"         NUMBER(9),
   "ApproveTime"        DATE,
   "ApproverName"       VARCHAR2(100),
   "InvoiceDownloadLink" VARCHAR2(200),
   constraint PK_INVOICEINFORMATION primary key ("Id")
)
/

/*==============================================================*/
/* Table: "PayOutBill"                                          */
/*==============================================================*/
create table "PayOutBill"  (
   "Id"                 NUMBER(9)                       not null,
   "Code"               VARCHAR2(50)                    not null,
   "PayOutPlanId"       NUMBER(9),
   "BuyerCompanyId"     NUMBER(9)                       not null,
   "SupplierCompanyId"  NUMBER(9)                       not null,
   "PartsSalesCategoryId" NUMBER(9),
   "PartsSalesCategoryName" VARCHAR2(100),
   "Remark"             VARCHAR2(200),
   "Summary"            VARCHAR2(200),
   "Amount"             NUMBER(19,4)                    not null,
   "PaymentMethod"      NUMBER(9)                       not null,
   "BankAccountId"      NUMBER(9),
   "Status"             NUMBER(9)                       not null,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "RowVersion"         TIMESTAMP,
   constraint PK_PAYOUTBILL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "PayOutPlan"                                          */
/*==============================================================*/
create table "PayOutPlan"  (
   "Id"                 NUMBER(9)                       not null,
   "购货方企业Id"            NUMBER(9)                       not null,
   申请日期                 DATE                            not null,
   计划付款日期               DATE                            not null,
   备注                   VARCHAR2(200),
   constraint PK_PAYOUTPLAN primary key ("Id")
)
/

/*==============================================================*/
/* Table: "PayOutPlanDetail"                                    */
/*==============================================================*/
create table "PayOutPlanDetail"  (
   "付款计划Id"             NUMBER(9)                       not null,
   "供应商企业Id"            NUMBER(9)                       not null,
   付款金额                 NUMBER(19,4)                    not null,
   付款账户                 NUMBER(9),
   付款方式                 NUMBER(9)                       not null,
   constraint PK_PAYOUTPLANDETAIL primary key ("付款计划Id")
)
/

/*==============================================================*/
/* Table: "PaymentBeneficiaryList"                              */
/*==============================================================*/
create table "PaymentBeneficiaryList"  (
   "Id"                 NUMBER(9)                       not null,
   "Code"               VARCHAR2(50)                    not null,
   "SalesCompanyId"     NUMBER(9)                       not null,
   "AccountGroupId"     NUMBER(9)                       not null,
   "CustomerCompanyId"  NUMBER(9)                       not null,
   "PaymentMethod"      NUMBER(9)                       not null,
   "Amount"             NUMBER(19,4)                    not null,
   "PaymentBillId"      NUMBER(9),
   "PaymentBillCode"    VARCHAR2(50),
   "BillNumber"         VARCHAR2(50),
   "Operator"           VARCHAR2(100),
   "Summary"            VARCHAR2(200),
   "Status"             NUMBER(9)                       not null,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "RowVersion"         TIMESTAMP,
   constraint PK_PAYMENTBENEFICIARYLIST primary key ("Id")
)
/

/*==============================================================*/
/* Table: "PaymentBill"                                         */
/*==============================================================*/
create table "PaymentBill"  (
   "Id"                 NUMBER(9)                       not null,
   "Code"               VARCHAR2(50)                    not null,
   "SalesCompanyId"     NUMBER(9)                       not null,
   "AccountGroupId"     NUMBER(9),
   "CustomerCompanyId"  NUMBER(9)                       not null,
   "Amount"             NUMBER(19,4)                    not null,
   "PaymentMethod"      NUMBER(9)                       not null,
   "Summary"            VARCHAR2(200),
   "BillNumber"         VARCHAR2(50),
   "Operator"           VARCHAR2(100),
   "BankAccountId"      NUMBER(9),
   "Status"             NUMBER(9)                       not null,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "RowVersion"         TIMESTAMP,
   "InvoiceDate"        DATE,
   "Drawer"             VARCHAR2(100),
   "IssueDate"          DATE,
   "DueDate"            DATE,
   "PayBank"            VARCHAR2(100),
   "MoneyOrderNum"      VARCHAR2(50),
   "DraftVersion"       NUMBER(9),
   "DraftCdRange"       VARCHAR2(50),
   constraint PK_PAYMENTBILL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "PaymentBill_Sync"                                    */
/*==============================================================*/
create table "PaymentBill_Sync"  (
   "Id"                 NUMBER(9)                       not null,
   "Objid"              VARCHAR2(40),
   "IOStatus"           NUMBER(9),
   "CreateTime"         DATE,
   "FirstSendTime"      DATE,
   "ModifySendTime"     DATE,
   "Token"              VARCHAR2(50),
   "Sender"             VARCHAR2(20),
   "Message"            VARCHAR2(500),
   constraint PK_PAYMENTBILL_SYNC primary key ("Id")
)
/

/*==============================================================*/
/* Table: SAP_18_SYNC                                           */
/*==============================================================*/
create table SAP_18_SYNC  (
   "SYNCNumber"         NUMBER(9)                       not null,
   "BussinessId"        NUMBER(9),
   "BussinessCode"      VARCHAR2(100),
   "BussinessTableName" VARCHAR2(100),
   constraint PK_SAP_18_SYNC primary key ("SYNCNumber")
)
/

/*==============================================================*/
/* Table: SAP_18_SYNC_FD                                        */
/*==============================================================*/
create table SAP_18_SYNC_FD  (
   "SYNCNumber"         NUMBER(9)                       not null,
   "BussinessId"        NUMBER(9),
   "BussinessCode"      VARCHAR2(100),
   "BussinessTableName" VARCHAR2(100),
   constraint PK_SAP_18_SYNC_FD primary key ("SYNCNumber")
)
/

/*==============================================================*/
/* Table: SAP_19_SYNC                                           */
/*==============================================================*/
create table SAP_19_SYNC  (
   "SYNCNumber"         NUMBER(9)                       not null,
   "BussinessId"        NUMBER(9),
   "BussinessCode"      VARCHAR2(100),
   "BussinessTableName" VARCHAR2(100),
   constraint PK_SAP_19_SYNC primary key ("SYNCNumber")
)
/

/*==============================================================*/
/* Table: SAP_19_SYNC_FD                                        */
/*==============================================================*/
create table SAP_19_SYNC_FD  (
   "SYNCNumber"         NUMBER(9)                       not null,
   "BussinessId"        NUMBER(9),
   "BussinessCode"      VARCHAR2(100),
   "BussinessTableName" VARCHAR2(100),
   constraint PK_SAP_19_SYNC_FD primary key ("SYNCNumber")
)
/

/*==============================================================*/
/* Table: "SAP_YX_InvoiceverAccountInfo"                        */
/*==============================================================*/
create table "SAP_YX_InvoiceverAccountInfo"  (
   "Id"                 NUMBER(9)                       not null,
   BUKRS                VARCHAR2(4),
   GJAHR                NUMBER(9),
   MONAT                NUMBER(9),
   "AccountingInvoiceNumber" VARCHAR2(50),
   "Code"               VARCHAR2(50),
   "InvoiceAmount"      NUMBER(19,4),
   "InvoiceTax"         NUMBER(19,4),
   "InvoiceDate"        date,
   "InvoiceCode"        VARCHAR2(50),
   "InvoiceNumber"      VARCHAR2(50),
   "ReversalInvoiceNumber" VARCHAR2(50),
   ZGZFS                VARCHAR2(1),
   "CreateTime"         DATE,
   "Status"             NUMBER(9),
   "ModifyTime"         DATE,
   "Message"            VARCHAR2(500),
   constraint PK_SAP_YX_INVOICEVERACCOUNTINF primary key ("Id")
)
/

/*==============================================================*/
/* Table: "SAP_YX_PaymentInfo"                                  */
/*==============================================================*/
create table "SAP_YX_PaymentInfo"  (
   "Id"                 NUMBER(9)                       not null,
   "CompanyCode"        VARCHAR2(50),
   "FiscalYear"         NUMBER(9),
   "FiscalPeriod"       NUMBER(9),
   "VoucherCreateTime"  DATE,
   "TallyTime"          DATE,
   "SupplierCompanyCode" VARCHAR2(50),
   "CustomerCompanyCode" VARCHAR2(50),
   "PartsSalesCategoryCode" VARCHAR2(50),
   "GeneralLedger"      VARCHAR2(50),
   "CurrencyCode"       VARCHAR2(5),
   "Amount"             NUMBER(19,4),
   "Item1"              VARCHAR2(50),
   "Item2"              VARCHAR2(50),
   "CreatorName"        VARCHAR2(100),
   "Summary"            VARCHAR2(200),
   "PaymentMethod"      VARCHAR2(1),
   "BillNumber"         VARCHAR2(50),
   "BillRowNumber"      NUMBER(9),
   "IntRunDate"         DATE,
   "IntRunTime"         DATE,
   "IsPassBack"         NUMBER(9),
   "TallyRecordNumber"  VARCHAR2(50),
   "ErrorMessage"       VARCHAR2(500),
   "TallyMark"          VARCHAR2(50),
   "ReceiveTime"        DATE,
   "HandleStatus"       NUMBER(9),
   "HandleTime"         DATE,
   "HandleMessage"      VARCHAR2(500),
   constraint PK_SAP_YX_PAYMENTINFO primary key ("Id")
)
/

/*==============================================================*/
/* Table: SAP_YX_SALESBILLINFO                                  */
/*==============================================================*/
create table SAP_YX_SALESBILLINFO  (
   "Id"                 NUMBER(9)                       not null,
   BUKRS                VARCHAR2(4),
   GJAHR                NUMBER(9),
   MONAT                NUMBER(9),
   "SapSysInvoiceNumber" VARCHAR2(50),
   "Code"               VARCHAR2(50),
   "InvoiceAmount"      NUMBER(19,4),
   "InvoiceTax"         NUMBER(19,4),
   "InvoiceDate"        DATE,
   "InvoiceCode"        VARCHAR2(50),
   "InvoiceNumber"      VARCHAR2(50),
   "ReverSalSysInvoiceNumber" VARCHAR2(50),
   "CreateTime"         DATE,
   "Status"             NUMBER(9),
   "ModifyTime"         DATE,
   "Message"            VARCHAR2(500),
   constraint PK_SAP_YX_SALESBILLINFO primary key ("Id")
)
/

/*==============================================================*/
/* Table: "SIHCreditInfo"                                       */
/*==============================================================*/
create table "SIHCreditInfo"  (
   "Id"                 NUMBER(9)                       not null,
   "Code"               VARCHAR2(50)                    not null,
   "Status"             NUMBER(9)                       not null,
   "ATotalFee"          NUMBER(19,4),
   "BTotalFee"          NUMBER(19,4),
   "ChangeAmount"       NUMBER(19,4),
   "IsAttach"           NUMBER(1),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "InitialApproverId"  NUMBER(9),
   "InitialApproverName" VARCHAR2(100),
   "InitialApproveTime" DATE,
   "CheckerId"          NUMBER(9),
   "CheckerName"        VARCHAR2(100),
   "CheckTime"          DATE,
   "ApproverId"         NUMBER(9),
   "ApproverName"       VARCHAR2(100),
   "ApproveTime"        DATE,
   "UpperApproverId"    NUMBER(9),
   "UpperApproverName"  VARCHAR2(100),
   "UpperCheckTime"     DATE,
   "RejecterId"         NUMBER(9),
   "RejecterName"       VARCHAR2(100),
   "RejectTime"         DATE,
   "AbandonerId"        NUMBER(9),
   "AbandonerName"      VARCHAR2(100),
   "AbandonTime"        DATE,
   "Path"               VARCHAR2(2000),
   "RowVersion"         TIMESTAMP,
   "Type"               NUMBER(9),
   constraint PK_SIHCREDITINFO primary key ("Id")
)
/

/*==============================================================*/
/* Table: "SIHCreditInfoDetail"                                 */
/*==============================================================*/
create table "SIHCreditInfoDetail"  (
   "Id"                 NUMBER(9)                       not null,
   "SIHCreditInfoId"    NUMBER(9)                       not null,
   "MarketingDepartmentId" NUMBER(9),
   "MarketingDepartmentCode" VARCHAR2(50),
   "MarketingDepartmentName" VARCHAR2(100),
   "CompanyId"          NUMBER(9),
   "CompanyCode"        VARCHAR2(50),
   "CompanyName"        VARCHAR2(100),
   "DealerId"           NUMBER(9),
   "DealerCode"         VARCHAR2(50),
   "DealerName"         VARCHAR2(100),
   "AccountGroupId"     NUMBER(9),
   "AccountGroupCode"   VARCHAR2(50),
   "AccountGroupName"   VARCHAR2(100),
   "ASIHCredit"         NUMBER(19,4),
   "BSIHCredit"         NUMBER(19,4),
   "ChangeAmount"       NUMBER(19,4),
   "ValidationTime"     DATE,
   "ExpireTime"         DATE,
   "Status"             NUMBER(9),
   constraint PK_SIHCREDITINFODETAIL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "SalesInvoice_tmp"                                    */
/*==============================================================*/
create table "SalesInvoice_tmp"  (
   "Id"                 NUMBER(9)                       not null,
   "VoucherCode"        VARCHAR2(50),
   "InvoiceCode"        VARCHAR2(100),
   "CreateTime"         DATE,
   constraint PK_SALESINVOICE_TMP primary key ("Id")
)
/

/*==============================================================*/
/* Table: "Sap_FD_InvoiceVerAccountInfo"                        */
/*==============================================================*/
create table "Sap_FD_InvoiceVerAccountInfo"  (
   "Id"                 NUMBER(9)                       not null,
   BUKRS                VARCHAR2(4),
   GJAHR                NUMBER(9),
   MONAT                NUMBER(9),
   BELNR1               VARCHAR2(10),
   JSDNUM               VARCHAR2(20),
   BKPF                 DATE,
   BELNR2               VARCHAR2(10),
   "CreateTime"         DATE,
   "Status"             NUMBER(9),
   "ModifyTime"         DATE,
   "Message"            VARCHAR2(500),
   constraint PK_SAP_FD_INVOICEVERACCOUNTINF primary key ("Id")
)
/

/*==============================================================*/
/* Table: "Sap_FD_PurchaseRtnInvoice"                           */
/*==============================================================*/
create table "Sap_FD_PurchaseRtnInvoice"  (
   "Id"                 NUMBER(9)                       not null,
   BUKRS                VARCHAR2(4),
   GJAHR                NUMBER(9),
   MONAT                NUMBER(9),
   BELNR1               VARCHAR2(50),
   JSDNUM               VARCHAR2(50),
   "InvoiceAmount"      NUMBER(19,4),
   "InvoiceTax"         NUMBER(19,4),
   "InvoiceDate"        date,
   "InvoiceCode"        VARCHAR2(50),
   "InvoiceNumber"      VARCHAR2(50),
   "ReversalInvoiceNumber" VARCHAR2(50),
   ZGZFS                VARCHAR2(1),
   "CreateTime"         DATE,
   "Status"             NUMBER(9),
   "ModifyTime"         DATE,
   "Message"            VARCHAR2(500),
   constraint PK_SAP_FD_PURCHASERTNINVOICE primary key ("Id")
)
/

/*==============================================================*/
/* Table: "Sap_FD_SalesBillInfo"                                */
/*==============================================================*/
create table "Sap_FD_SalesBillInfo"  (
   "Id"                 NUMBER(9)                       not null,
   BUKRS                VARCHAR2(4),
   GJAHR                NUMBER(9),
   MONAT                NUMBER(9),
   "SapSysInvoiceNumber" VARCHAR2(50),
   "Code"               VARCHAR2(50),
   "InvoiceAmount"      NUMBER(19,4),
   "InvoiceTax"         NUMBER(19,4),
   "InvoiceDate"        DATE,
   "InvoiceCode"        VARCHAR2(50),
   "InvoiceNumber"      VARCHAR2(50),
   "ReverSalSysInvoiceNumber" VARCHAR2(50),
   "CreateTime"         DATE,
   "Status"             NUMBER(9),
   "ModifyTime"         DATE,
   "Message"            VARCHAR2(500),
   constraint PK_SAP_FD_SALESBILLINFO primary key ("Id")
)
/

/*==============================================================*/
/* Table: "Summary"                                             */
/*==============================================================*/
create table "Summary"  (
   "Id"                 NUMBER(9)                       not null,
   "OwnerCompanyId"     NUMBER(9)                       not null,
   "Content"            VARCHAR2(200)                   not null,
   "Purpose"            NUMBER(9)                       not null,
   constraint PK_SUMMARY primary key ("Id")
)
/

/*==============================================================*/
/* Table: "SupplierAccount"                                     */
/*==============================================================*/
create table "SupplierAccount"  (
   "Id"                 NUMBER(9)                       not null,
   "BuyerCompanyId"     NUMBER(9)                       not null,
   "PartsSalesCategoryId" NUMBER(9),
   "PartsSalesCategoryName" VARCHAR2(100),
   "SupplierCompanyId"  NUMBER(9)                       not null,
   "DueAmount"          NUMBER(19,4)                    not null,
   "EstimatedDueAmount" NUMBER(19,4)                    not null,
   "PaymentLimit"       NUMBER(19,4)                    not null,
   "Status"             NUMBER(9)                       not null,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "RowVersion"         TIMESTAMP,
   constraint PK_SUPPLIERACCOUNT primary key ("Id")
)
/

/*==============================================================*/
/* Table: "SupplierInformation"                                 */
/*==============================================================*/
create table "SupplierInformation"  (
   "Id"                 NUMBER(9)                       not null,
   "BuyerCompanyId"     NUMBER(9)                       not null,
   "SupplierCompanyId"  NUMBER(9)                       not null,
   "Status"             NUMBER(9)                       not null,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "RowVersion"         TIMESTAMP,
   constraint PK_SUPPLIERINFORMATION primary key ("Id")
)
/

/*==============================================================*/
/* Table: "SupplierOpenAccountApp"                              */
/*==============================================================*/
create table "SupplierOpenAccountApp"  (
   "Id"                 NUMBER(9)                       not null,
   "Code"               VARCHAR2(50),
   "BuyerCompanyId"     NUMBER(9)                       not null,
   "PartsSalesCategoryId" NUMBER(9),
   "PartsSalesCategoryName" VARCHAR2(100),
   "SupplierCompanyId"  NUMBER(9)                       not null,
   "AccountInitialDeposit" NUMBER(19,4)                    not null,
   "Operator"           VARCHAR2(100),
   "Remark"             VARCHAR2(200),
   "Status"             NUMBER(9)                       not null,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "AbandonerId"        NUMBER(9),
   "AbandonerName"      VARCHAR2(100),
   "AbandonTime"        DATE,
   "RowVersion"         TIMESTAMP,
   constraint PK_SUPPLIEROPENACCOUNTAPP primary key ("Id")
)
/

/*==============================================================*/
/* Table: "SupplierTransferBill"                                */
/*==============================================================*/
create table "SupplierTransferBill"  (
   "Id"                 NUMBER(9)                       not null,
   "BuyerCompanyId"     NUMBER(9)                       not null,
   "OutboundCompanyId"  NUMBER(9)                       not null,
   "InboundCompanyId"   NUMBER(9)                       not null,
   "Amount"             NUMBER(19,4)                    not null,
   "Summary"            VARCHAR2(200),
   "CredentialDocument" VARCHAR2(100),
   "Operator"           VARCHAR2(100),
   "Supervisor"         VARCHAR2(100),
   "Remark"             VARCHAR2(200),
   "Status"             NUMBER(9)                       not null,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "RowVersion"         TIMESTAMP,
   constraint PK_SUPPLIERTRANSFERBILL primary key ("Id")
)
/

