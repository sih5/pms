/*==============================================================*/
/* DBMS name:      ORACLE Version 11g                           */
/* Created on:     2021/5/25 11:10:08                           */
/*==============================================================*/


drop table "InterfaceDataSyncRecord" cascade constraints;

drop table "InvoiceVerAccountInfo" cascade constraints;

drop table "SalesBillInfo" cascade constraints;

drop sequence "S_InterfaceDataSyncRecord";

drop sequence "S_InvoiceVerAccountInfo";

drop sequence "S_SalesBillInfo";

create sequence "S_InterfaceDataSyncRecord";

create sequence "S_InvoiceVerAccountInfo";

create sequence "S_SalesBillInfo";

/*==============================================================*/
/* Table: "InterfaceDataSyncRecord"                             */
/*==============================================================*/
create table "InterfaceDataSyncRecord" 
(
   "Id"                 NUMBER(6)            not null,
   "BusinessName"       VARCHAR2(100)        not null,
   "IsActive"           SMALLINT             not null,
   "MaxSyncCount"       INTEGER              not null,
   "LastOperateTime"    DATE,
   "LastSyncRowVersion" rowversion,
   "LastSyncCount"      INTEGER,
   constraint PK_INTERFACEDATASYNCRECORD primary key ("Id")
);

/*==============================================================*/
/* Table: "InvoiceVerAccountInfo"                               */
/*==============================================================*/
create table "InvoiceVerAccountInfo" 
(
   "Id"                 NUMBER(6)            not null,
   BUKRS                CHAR(4),
   GJAHR                NUMBER(4),
   MONAT                NUMBER(2),
   "AccountingInvoiceNumber" VARCHAR2(50),
   "Code"               VARCHAR2(50),
   "InvoiceAmount"      NUMBER(8,2),
   "InvoiceTax"         NUMBER(8,2),
   "InvoiceDate"        DATE,
   "InvoiceCode"        VARCHAR2(50),
   "InvoiceNumber"      VARCHAR2(50),
   "ReversalInvoiceNumber" VARCHAR2(50),
   "CreateTime"         DATE,
   constraint PK_INVOICEVERACCOUNTINFO primary key ("Id")
);

/*==============================================================*/
/* Table: "SalesBillInfo"                                       */
/*==============================================================*/
create table "SalesBillInfo" 
(
   "Id"                 NUMBER(6)            not null,
   BUKRS                CHAR(4),
   GJAHR                NUMBER(4),
   MONAT                NUMBER(2),
   "SAPSysInvoiceNumber" VARCHAR2(50),
   "Code"               VARCHAR2(50),
   "InvoiceAmount"      NUMBER(8,2),
   "InvoiceTax"         NUMBER(8,2),
   "InvoiceDate"        DATE,
   "InvoiceCode"        VARCHAR2(50),
   "InvoiceNumber"      VARCHAR2(50),
   "ReversalSysInvoiceNumber" VARCHAR2(50),
   "CreateTime"         DATE,
   constraint PK_SALESBILLINFO primary key ("Id")
);

