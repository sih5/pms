/*==============================================================*/
/* DBMS name:      ORACLE Version 10gR2                         */
/* Created on:     2014/2/27 15:25:08                           */
/*==============================================================*/


declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"UsedPartsDisposalBill"');
  if num>0 then
    execute immediate 'drop table "UsedPartsDisposalBill" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"UsedPartsDisposalDetail"');
  if num>0 then
    execute immediate 'drop table "UsedPartsDisposalDetail" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"UsedPartsInboundDetail"');
  if num>0 then
    execute immediate 'drop table "UsedPartsInboundDetail" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"UsedPartsInboundOrder"');
  if num>0 then
    execute immediate 'drop table "UsedPartsInboundOrder" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"UsedPartsLoanBill"');
  if num>0 then
    execute immediate 'drop table "UsedPartsLoanBill" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"UsedPartsLoanDetail"');
  if num>0 then
    execute immediate 'drop table "UsedPartsLoanDetail" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"UsedPartsOutboundDetail"');
  if num>0 then
    execute immediate 'drop table "UsedPartsOutboundDetail" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"UsedPartsOutboundOrder"');
  if num>0 then
    execute immediate 'drop table "UsedPartsOutboundOrder" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"UsedPartsRefitBill"');
  if num>0 then
    execute immediate 'drop table "UsedPartsRefitBill" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"UsedPartsRefitReqDetail"');
  if num>0 then
    execute immediate 'drop table "UsedPartsRefitReqDetail" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"UsedPartsRefitReturnDetail"');
  if num>0 then
    execute immediate 'drop table "UsedPartsRefitReturnDetail" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"UsedPartsReturnDetail"');
  if num>0 then
    execute immediate 'drop table "UsedPartsReturnDetail" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"UsedPartsReturnOrder"');
  if num>0 then
    execute immediate 'drop table "UsedPartsReturnOrder" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"UsedPartsShiftDetail"');
  if num>0 then
    execute immediate 'drop table "UsedPartsShiftDetail" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"UsedPartsShiftOrder"');
  if num>0 then
    execute immediate 'drop table "UsedPartsShiftOrder" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"UsedPartsTransferDetail"');
  if num>0 then
    execute immediate 'drop table "UsedPartsTransferDetail" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"UsedPartsTransferOrder"');
  if num>0 then
    execute immediate 'drop table "UsedPartsTransferOrder" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_UsedPartsDisposalBill"');
  if num>0 then
    execute immediate 'drop sequence "S_UsedPartsDisposalBill"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_UsedPartsDisposalDetail"');
  if num>0 then
    execute immediate 'drop sequence "S_UsedPartsDisposalDetail"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_UsedPartsInboundDetail"');
  if num>0 then
    execute immediate 'drop sequence "S_UsedPartsInboundDetail"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_UsedPartsInboundOrder"');
  if num>0 then
    execute immediate 'drop sequence "S_UsedPartsInboundOrder"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_UsedPartsLoanBill"');
  if num>0 then
    execute immediate 'drop sequence "S_UsedPartsLoanBill"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_UsedPartsLoanDetail"');
  if num>0 then
    execute immediate 'drop sequence "S_UsedPartsLoanDetail"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_UsedPartsOutboundDetail"');
  if num>0 then
    execute immediate 'drop sequence "S_UsedPartsOutboundDetail"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_UsedPartsOutboundOrder"');
  if num>0 then
    execute immediate 'drop sequence "S_UsedPartsOutboundOrder"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_UsedPartsRefitBill"');
  if num>0 then
    execute immediate 'drop sequence "S_UsedPartsRefitBill"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_UsedPartsRefitReqDetail"');
  if num>0 then
    execute immediate 'drop sequence "S_UsedPartsRefitReqDetail"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_UsedPartsRefitReturnDetail"');
  if num>0 then
    execute immediate 'drop sequence "S_UsedPartsRefitReturnDetail"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_UsedPartsReturnDetail"');
  if num>0 then
    execute immediate 'drop sequence "S_UsedPartsReturnDetail"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_UsedPartsReturnOrder"');
  if num>0 then
    execute immediate 'drop sequence "S_UsedPartsReturnOrder"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_UsedPartsShiftDetail"');
  if num>0 then
    execute immediate 'drop sequence "S_UsedPartsShiftDetail"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_UsedPartsShiftOrder"');
  if num>0 then
    execute immediate 'drop sequence "S_UsedPartsShiftOrder"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_UsedPartsTransferDetail"');
  if num>0 then
    execute immediate 'drop sequence "S_UsedPartsTransferDetail"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_UsedPartsTransferOrder"');
  if num>0 then
    execute immediate 'drop sequence "S_UsedPartsTransferOrder"';
  end if;
end;
/

create sequence "S_UsedPartsDisposalBill"
/

create sequence "S_UsedPartsDisposalDetail"
/

create sequence "S_UsedPartsInboundDetail"
/

create sequence "S_UsedPartsInboundOrder"
/

create sequence "S_UsedPartsLoanBill"
/

create sequence "S_UsedPartsLoanDetail"
/

create sequence "S_UsedPartsOutboundDetail"
/

create sequence "S_UsedPartsOutboundOrder"
/

create sequence "S_UsedPartsRefitBill"
/

create sequence "S_UsedPartsRefitReqDetail"
/

create sequence "S_UsedPartsRefitReturnDetail"
/

create sequence "S_UsedPartsReturnDetail"
/

create sequence "S_UsedPartsReturnOrder"
/

create sequence "S_UsedPartsShiftDetail"
/

create sequence "S_UsedPartsShiftOrder"
/

create sequence "S_UsedPartsTransferDetail"
/

create sequence "S_UsedPartsTransferOrder"
/

/*==============================================================*/
/* Table: "UsedPartsDisposalBill"                               */
/*==============================================================*/
create table "UsedPartsDisposalBill"  (
   "Id"                 NUMBER(9)                       not null,
   "Code"               VARCHAR2(50)                    not null,
   "UsedPartsWarehouseId" NUMBER(9)                       not null,
   "UsedPartsWarehouseCode" VARCHAR2(50)                    not null,
   "UsedPartsWarehouseName" VARCHAR2(100)                   not null,
   "RelatedCompanyName" VARCHAR2(100),
   "UsedPartsDisposalMethod" NUMBER(9)                       not null,
   "TotalAmount"        NUMBER(19,4),
   "ResidualValue"      NUMBER(19,4),
   "Status"             NUMBER(9)                       not null,
   "OutboundStatus"     NUMBER(9),
   "Operator"           VARCHAR2(100),
   "Supervisor"         VARCHAR2(100),
   "Remark"             VARCHAR2(200),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "ApproverId"         NUMBER(9),
   "ApproverName"       VARCHAR2(100),
   "ApproveTime"        DATE,
   "AbandonerId"        NUMBER(9),
   "AbandonerName"      VARCHAR2(100),
   "AbandonTime"        DATE,
   "RowVersion"         TIMESTAMP,
   constraint PK_USEDPARTSDISPOSALBILL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "UsedPartsDisposalDetail"                             */
/*==============================================================*/
create table "UsedPartsDisposalDetail"  (
   "Id"                 NUMBER(9)                       not null,
   "UsedPartsDisposalBillId" NUMBER(9)                       not null,
   "UsedPartsId"        NUMBER(9)                       not null,
   "UsedPartsCode"      VARCHAR2(50)                    not null,
   "UsedPartsName"      VARCHAR2(100)                   not null,
   "UsedPartsBarCode"   VARCHAR2(100)                   not null,
   "UsedPartsBatchNumber" VARCHAR2(50),
   "UsedPartsSerialNumber" VARCHAR2(50),
   "Price"              NUMBER(19,4),
   "BranchId"           NUMBER(9),
   "PlannedAmount"      NUMBER(9)                       not null,
   "ConfirmedAmount"    NUMBER(9),
   "OutboundAmount"     NUMBER(9),
   "UsedPartsSupplierId" NUMBER(9)                       not null,
   "UsedPartsSupplierCode" VARCHAR2(50)                    not null,
   "UsedPartsSupplierName" VARCHAR2(100)                   not null,
   "ClaimBillId"        NUMBER(9)                       not null,
   "ClaimBillType"      NUMBER(9)                       not null,
   "ClaimBillCode"      VARCHAR2(50)                    not null,
   "IfFaultyParts"      NUMBER(1),
   "FaultyPartsSupplierId" NUMBER(9),
   "FaultyPartsSupplierCode" VARCHAR2(50),
   "FaultyPartsSupplierName" VARCHAR2(100),
   "ResponsibleUnitId"  NUMBER(9),
   "ResponsibleUnitCode" VARCHAR2(50),
   "ResponsibleUnitName" VARCHAR2(100),
   "FaultyPartsId"      NUMBER(9),
   "FaultyPartsCode"    VARCHAR2(50),
   "FaultyPartsName"    VARCHAR2(100),
   constraint PK_USEDPARTSDISPOSALDETAIL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "UsedPartsInboundDetail"                              */
/*==============================================================*/
create table "UsedPartsInboundDetail"  (
   "Id"                 NUMBER(9)                       not null,
   "UsedPartsInboundOrderId" NUMBER(9)                       not null,
   "UsedPartsId"        NUMBER(9)                       not null,
   "UsedPartsCode"      VARCHAR2(50)                    not null,
   "UsedPartsName"      VARCHAR2(100)                   not null,
   "UsedPartsBarCode"   VARCHAR2(100)                   not null,
   "UsedPartsBatchNumber" VARCHAR2(50),
   "UsedPartsSerialNumber" VARCHAR2(50),
   "UsedPartsWarehouseAreaId" NUMBER(9)                       not null,
   "UsedPartsWarehouseAreaCode" VARCHAR2(50)                    not null,
   "Quantity"           NUMBER(9)                       not null,
   "SettlementPrice"    NUMBER(19,4)                    not null,
   "CostPrice"          NUMBER(19,4),
   "BranchId"           NUMBER(9),
   "Remark"             VARCHAR2(200),
   "UsedPartsSupplierId" NUMBER(9)                       not null,
   "UsedPartsSupplierCode" VARCHAR2(50)                    not null,
   "UsedPartsSupplierName" VARCHAR2(100)                   not null,
   "ClaimBillId"        NUMBER(9)                       not null,
   "ClaimBillType"      NUMBER(9)                       not null,
   "ClaimBillCode"      VARCHAR2(50)                    not null,
   "IfFaultyParts"      NUMBER(1),
   "FaultyPartsSupplierId" NUMBER(9),
   "FaultyPartsSupplierCode" VARCHAR2(50),
   "FaultyPartsSupplierName" VARCHAR2(100),
   "ResponsibleUnitId"  NUMBER(9),
   "ResponsibleUnitCode" VARCHAR2(50),
   "ResponsibleUnitName" VARCHAR2(100),
   "FaultyPartsId"      NUMBER(9),
   "FaultyPartsCode"    VARCHAR2(50),
   "FaultyPartsName"    VARCHAR2(100),
   constraint PK_USEDPARTSINBOUNDDETAIL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "UsedPartsInboundOrder"                               */
/*==============================================================*/
create table "UsedPartsInboundOrder"  (
   "Id"                 NUMBER(9)                       not null,
   "Code"               VARCHAR2(50)                    not null,
   "UsedPartsWarehouseId" NUMBER(9)                       not null,
   "UsedPartsWarehouseCode" VARCHAR2(50)                    not null,
   "UsedPartsWarehouseName" VARCHAR2(100)                   not null,
   "InboundType"        NUMBER(9)                       not null,
   "TotalAmount"        NUMBER(19,4)                    not null,
   "TotalQuantity"      NUMBER(9)                       not null,
   "SourceId"           NUMBER(9)                       not null,
   "SourceCode"         VARCHAR2(50)                    not null,
   "RelatedCompanyId"   NUMBER(9),
   "RelatedCompanyCode" VARCHAR2(50),
   "RelatedCompanyName" VARCHAR2(100),
   "Operator"           VARCHAR2(100),
   "IfBillable"         NUMBER(1),
   "IfAlreadySettled"   NUMBER(1),
   "Remark"             VARCHAR2(200),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "RowVersion"         TIMESTAMP,
   constraint PK_USEDPARTSINBOUNDORDER primary key ("Id")
)
/

/*==============================================================*/
/* Table: "UsedPartsLoanBill"                                   */
/*==============================================================*/
create table "UsedPartsLoanBill"  (
   "Id"                 NUMBER(9)                       not null,
   "Code"               VARCHAR2(50)                    not null,
   "UsedPartsWarehouseId" NUMBER(9)                       not null,
   "UsedPartsWarehouseCode" VARCHAR2(50)                    not null,
   "UsedPartsWarehouseName" VARCHAR2(100)                   not null,
   "IfReturned"         NUMBER(1)                       not null,
   "RelatedCompanyName" VARCHAR2(100),
   "LoanRequestor"      VARCHAR2(100),
   "ReturnExecutor"     VARCHAR2(100),
   "PlannedReturnTime"  DATE,
   "Status"             NUMBER(9)                       not null,
   "OutboundStatus"     NUMBER(9),
   "InboundStatus"      NUMBER(9),
   "Remark"             VARCHAR2(200),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "ApproverId"         NUMBER(9),
   "ApproverName"       VARCHAR2(100),
   "ApproveTime"        DATE,
   "AbandonerId"        NUMBER(9),
   "AbandonerName"      VARCHAR2(100),
   "AbandonTime"        DATE,
   "RowVersion"         TIMESTAMP,
   constraint PK_USEDPARTSLOANBILL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "UsedPartsLoanDetail"                                 */
/*==============================================================*/
create table "UsedPartsLoanDetail"  (
   "Id"                 NUMBER(9)                       not null,
   "UsedPartsLoanBillId" NUMBER(9)                       not null,
   "UsedPartsId"        NUMBER(9)                       not null,
   "UsedPartsCode"      VARCHAR2(50)                    not null,
   "UsedPartsName"      VARCHAR2(100)                   not null,
   "UsedPartsBarCode"   VARCHAR2(100)                   not null,
   "UsedPartsBatchNumber" VARCHAR2(50),
   "UsedPartsSerialNumber" VARCHAR2(50),
   "Price"              NUMBER(19,4),
   "BranchId"           NUMBER(9),
   "PlannedAmount"      NUMBER(9)                       not null,
   "ConfirmedAmount"    NUMBER(9),
   "OutboundAmount"     NUMBER(9),
   "InboundAmount"      NUMBER(9),
   "UsedPartsSupplierId" NUMBER(9)                       not null,
   "UsedPartsSupplierCode" VARCHAR2(50)                    not null,
   "UsedPartsSupplierName" VARCHAR2(100)                   not null,
   "ClaimBillId"        NUMBER(9)                       not null,
   "ClaimBillType"      NUMBER(9)                       not null,
   "ClaimBillCode"      VARCHAR2(50)                    not null,
   "IfFaultyParts"      NUMBER(1),
   "FaultyPartsSupplierId" NUMBER(9),
   "FaultyPartsSupplierCode" VARCHAR2(50),
   "FaultyPartsSupplierName" VARCHAR2(100),
   "ResponsibleUnitId"  NUMBER(9),
   "ResponsibleUnitCode" VARCHAR2(50),
   "ResponsibleUnitName" VARCHAR2(100),
   "FaultyPartsId"      NUMBER(9),
   "FaultyPartsCode"    VARCHAR2(50),
   "FaultyPartsName"    VARCHAR2(100),
   constraint PK_USEDPARTSLOANDETAIL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "UsedPartsOutboundDetail"                             */
/*==============================================================*/
create table "UsedPartsOutboundDetail"  (
   "Id"                 NUMBER(9)                       not null,
   "UsedPartsOutboundOrderId" NUMBER(9)                       not null,
   "UsedPartsId"        NUMBER(9)                       not null,
   "UsedPartsCode"      VARCHAR2(50)                    not null,
   "UsedPartsName"      VARCHAR2(100)                   not null,
   "UsedPartsBarCode"   VARCHAR2(100)                   not null,
   "UsedPartsBatchNumber" VARCHAR2(50),
   "UsedPartsSerialNumber" VARCHAR2(50),
   "UsedPartsWarehouseAreaId" NUMBER(9)                       not null,
   "UsedPartsWarehouseAreaCode" VARCHAR2(50)                    not null,
   "Quantity"           NUMBER(9)                       not null,
   "SettlementPrice"    NUMBER(19,4)                    not null,
   "CostPrice"          NUMBER(19,4),
   "BranchId"           NUMBER(9),
   "Remark"             VARCHAR2(200),
   "UsedPartsSupplierId" NUMBER(9)                       not null,
   "UsedPartsSupplierCode" VARCHAR2(50)                    not null,
   "UsedPartsSupplierName" VARCHAR2(100)                   not null,
   "ClaimBillId"        NUMBER(9)                       not null,
   "ClaimBillType"      NUMBER(9)                       not null,
   "ClaimBillCode"      VARCHAR2(50)                    not null,
   "IfFaultyParts"      NUMBER(1),
   "FaultyPartsSupplierId" NUMBER(9),
   "FaultyPartsSupplierCode" VARCHAR2(50),
   "FaultyPartsSupplierName" VARCHAR2(100),
   "ResponsibleUnitId"  NUMBER(9),
   "ResponsibleUnitCode" VARCHAR2(50),
   "ResponsibleUnitName" VARCHAR2(100),
   "FaultyPartsId"      NUMBER(9),
   "FaultyPartsCode"    VARCHAR2(50),
   "FaultyPartsName"    VARCHAR2(100),
   constraint PK_USEDPARTSOUTBOUNDDETAIL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "UsedPartsOutboundOrder"                              */
/*==============================================================*/
create table "UsedPartsOutboundOrder"  (
   "Id"                 NUMBER(9)                       not null,
   "Code"               VARCHAR2(50)                    not null,
   "UsedPartsWarehouseId" NUMBER(9)                       not null,
   "UsedPartsWarehouseCode" VARCHAR2(50)                    not null,
   "UsedPartsWarehouseName" VARCHAR2(100)                   not null,
   "OutboundType"       NUMBER(9)                       not null,
   "TotalAmount"        NUMBER(19,4)                    not null,
   "TotalQuantity"      NUMBER(9)                       not null,
   "SourceId"           NUMBER(9)                       not null,
   "SourceCode"         VARCHAR2(50)                    not null,
   "RelatedCompanyId"   NUMBER(9),
   "RelatedCompanyCode" VARCHAR2(50),
   "RelatedCompanyName" VARCHAR2(100),
   "Operator"           VARCHAR2(100),
   "IfBillable"         NUMBER(1),
   "IfAlreadySettled"   NUMBER(1),
   "Remark"             VARCHAR2(200),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "RowVersion"         TIMESTAMP,
   constraint PK_USEDPARTSOUTBOUNDORDER primary key ("Id")
)
/

/*==============================================================*/
/* Table: "UsedPartsRefitBill"                                  */
/*==============================================================*/
create table "UsedPartsRefitBill"  (
   "Id"                 NUMBER(9)                       not null,
   "Code"               VARCHAR2(50)                    not null,
   "UsedPartsWarehouseId" NUMBER(9)                       not null,
   "UsedPartsWarehouseCode" VARCHAR2(50)                    not null,
   "UsedPartsWarehouseName" VARCHAR2(100)                   not null,
   "RelatedCompanyName" VARCHAR2(100),
   "Contact"            VARCHAR2(100),
   "ContactMethod"      VARCHAR2(100),
   "RequisitionRequestor" VARCHAR2(100),
   "ReturnExecutor"     VARCHAR2(100),
   "RequisitionTime"    DATE,
   "PlannedFinishingTime" DATE,
   "ReturnTime"         DATE,
   "MaterialCost"       NUMBER(19,4),
   "AccessoryCost"      NUMBER(19,4),
   "RefitCost"          NUMBER(19,4),
   "OtherCost"          NUMBER(19,4),
   "Status"             NUMBER(9)                       not null,
   "OutboundStatus"     NUMBER(9),
   "InboundStatus"      NUMBER(9),
   "Remark"             VARCHAR2(200),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "ApproverId"         NUMBER(9),
   "ApproverName"       VARCHAR2(100),
   "ApproveTime"        DATE,
   "AbandonerId"        NUMBER(9),
   "AbandonerName"      VARCHAR2(100),
   "AbandonTime"        DATE,
   "RowVersion"         TIMESTAMP,
   constraint PK_USEDPARTSREFITBILL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "UsedPartsRefitReqDetail"                             */
/*==============================================================*/
create table "UsedPartsRefitReqDetail"  (
   "Id"                 NUMBER(9)                       not null,
   "UsedPartsRefitBillId" NUMBER(9)                       not null,
   "UsedPartsId"        NUMBER(9)                       not null,
   "UsedPartsCode"      VARCHAR2(50)                    not null,
   "UsedPartsName"      VARCHAR2(100)                   not null,
   "UsedPartsBarCode"   VARCHAR2(100)                   not null,
   "UsedPartsBatchNumber" VARCHAR2(50),
   "UsedPartsSerialNumber" VARCHAR2(50),
   "Price"              NUMBER(19,4),
   "BranchId"           NUMBER(9),
   "PlannedAmount"      NUMBER(9)                       not null,
   "ConfirmedAmount"    NUMBER(9),
   "OutboundAmount"     NUMBER(9),
   constraint PK_USEDPARTSREFITREQDETAIL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "UsedPartsRefitReturnDetail"                          */
/*==============================================================*/
create table "UsedPartsRefitReturnDetail"  (
   "Id"                 NUMBER(9)                       not null,
   "UsedPartsRefitBillId" NUMBER(9)                       not null,
   "UsedPartsId"        NUMBER(9)                       not null,
   "UsedPartsCode"      VARCHAR2(50)                    not null,
   "UsedPartsName"      VARCHAR2(100)                   not null,
   "UsedPartsBarCode"   VARCHAR2(100)                   not null,
   "UsedPartsBatchNumber" VARCHAR2(50),
   "UsedPartsSerialNumber" VARCHAR2(50),
   "Price"              NUMBER(19,4),
   "BranchId"           NUMBER(9),
   "PlannedAmount"      NUMBER(9)                       not null,
   "ConfirmedAmount"    NUMBER(9),
   "InboundAmount"      NUMBER(9),
   constraint PK_USEDPARTSREFITRETURNDETAIL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "UsedPartsReturnDetail"                               */
/*==============================================================*/
create table "UsedPartsReturnDetail"  (
   "Id"                 NUMBER(9)                       not null,
   "SerialNumber"       NUMBER(9)                       not null,
   "UsedPartsReturnOrderId" NUMBER(9)                       not null,
   "BranchId"           NUMBER(9)                       not null,
   "UsedPartsId"        NUMBER(9)                       not null,
   "UsedPartsCode"      VARCHAR2(50)                    not null,
   "UsedPartsName"      VARCHAR2(100)                   not null,
   "UsedPartsBarCode"   VARCHAR2(50)                    not null,
   "UsedPartsSerialNumber" VARCHAR2(50),
   "UsedPartsBatchNumber" VARCHAR2(50),
   "UsedPartsSupplierId" NUMBER(9),
   "UsedPartsSupplierCode" VARCHAR2(50),
   "UsedPartsSupplierName" VARCHAR2(100),
   "ClaimBillId"        NUMBER(9)                       not null,
   "ClaimBillType"      NUMBER(9)                       not null,
   "ClaimBillCode"      VARCHAR2(50)                    not null,
   "UnitPrice"          NUMBER(19,4)                    not null,
   "PlannedAmount"      NUMBER(9)                       not null,
   "ConfirmedAmount"    NUMBER(9)                       not null,
   "OutboundAmount"     NUMBER(9)                       not null,
   "IfFaultyParts"      NUMBER(1),
   "FaultyPartsSupplierId" NUMBER(9),
   "FaultyPartsSupplierCode" VARCHAR2(50),
   "FaultyPartsSupplierName" VARCHAR2(100),
   "ResponsibleUnitId"  NUMBER(9),
   "ResponsibleUnitCode" VARCHAR2(50),
   "ResponsibleUnitName" VARCHAR2(100),
   "FaultyPartsId"      NUMBER(9),
   "FaultyPartsCode"    VARCHAR2(50),
   "FaultyPartsName"    VARCHAR2(100),
   constraint PK_USEDPARTSRETURNDETAIL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "UsedPartsReturnOrder"                                */
/*==============================================================*/
create table "UsedPartsReturnOrder"  (
   "Id"                 NUMBER(9)                       not null,
   "Code"               VARCHAR2(50)                    not null,
   "ReturnType"         NUMBER(9)                       not null,
   "ReturnOfficeId"     NUMBER(9)                       not null,
   "ReturnOfficeCode"   VARCHAR2(50)                    not null,
   "ReturnOfficeName"   VARCHAR2(100)                   not null,
   "Operator"           VARCHAR2(100),
   "OutboundWarehouseId" NUMBER(9)                       not null,
   "OutboundWarehouseCode" VARCHAR2(50)                    not null,
   "OutboundWarehouseName" VARCHAR2(100)                   not null,
   "TotalAmount"        NUMBER(19,4)                    not null,
   "TotalQuantity"      NUMBER(9)                       not null,
   "Remark"             VARCHAR2(200),
   "Status"             NUMBER(9)                       not null,
   "OutboundStatus"     NUMBER(9),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "ApproverId"         NUMBER(9),
   "ApproverName"       VARCHAR2(100),
   "ApproveTime"        DATE,
   "AbandonerName"      VARCHAR2(100),
   "AbandonerId"        NUMBER(9),
   "AbandonTime"        DATE,
   "RowVersion"         TIMESTAMP,
   constraint PK_USEDPARTSRETURNORDER primary key ("Id")
)
/

/*==============================================================*/
/* Table: "UsedPartsShiftDetail"                                */
/*==============================================================*/
create table "UsedPartsShiftDetail"  (
   "Id"                 NUMBER(9)                       not null,
   "UsedPartsShiftOrderId" NUMBER(9)                       not null,
   "UsedPartsId"        NUMBER(9)                       not null,
   "UsedPartsCode"      VARCHAR2(50)                    not null,
   "UsedPartsName"      VARCHAR2(100)                   not null,
   "UsedPartsBarCode"   VARCHAR2(100)                   not null,
   "UsedPartsBatchNumber" VARCHAR2(50),
   "UsedPartsSerialNumber" VARCHAR2(50),
   "BranchId"           NUMBER(9),
   "Quantity"           NUMBER(9)                       not null,
   "OriginStoragePositionId" NUMBER(9)                       not null,
   "OriginStoragePositionCode" VARCHAR2(50)                    not null,
   "DestiStoragePositionId" NUMBER(9)                       not null,
   "DestiStoragePositionCode" VARCHAR2(50)                    not null,
   constraint PK_USEDPARTSSHIFTDETAIL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "UsedPartsShiftOrder"                                 */
/*==============================================================*/
create table "UsedPartsShiftOrder"  (
   "Id"                 NUMBER(9)                       not null,
   "Code"               VARCHAR2(50)                    not null,
   "UsedPartsWarehouseId" NUMBER(9)                       not null,
   "UsedPartsWarehouseCode" VARCHAR2(50)                    not null,
   "UsedPartsWarehouseName" VARCHAR2(100)                   not null,
   "Status"             NUMBER(9)                       not null,
   "Remark"             VARCHAR2(200),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "RowVersion"         TIMESTAMP,
   constraint PK_USEDPARTSSHIFTORDER primary key ("Id")
)
/

/*==============================================================*/
/* Table: "UsedPartsTransferDetail"                             */
/*==============================================================*/
create table "UsedPartsTransferDetail"  (
   "Id"                 NUMBER(9)                       not null,
   "UsedPartsTransferOrderId" NUMBER(9)                       not null,
   "UsedPartsId"        NUMBER(9)                       not null,
   "UsedPartsCode"      VARCHAR2(50)                    not null,
   "UsedPartsName"      VARCHAR2(100)                   not null,
   "UsedPartsBarCode"   VARCHAR2(100)                   not null,
   "UsedPartsBatchNumber" VARCHAR2(50),
   "UsedPartsSerialNumber" VARCHAR2(50),
   "Price"              NUMBER(19,4)                    not null,
   "PlannedAmount"      NUMBER(9)                       not null,
   "ConfirmedAmount"    NUMBER(9),
   "OutboundAmount"     NUMBER(9),
   "InboundAmount"      NUMBER(9),
   "ClaimBillId"        NUMBER(9),
   "ClaimBillType"      NUMBER(9),
   "ClaimBillCode"      VARCHAR2(50),
   "IfFaultyParts"      NUMBER(1),
   "UsedPartsSupplierId" NUMBER(9),
   "UsedPartsSupplierCode" VARCHAR2(50),
   "UsedPartsSupplierName" VARCHAR2(100),
   "FaultyPartsSupplierId" NUMBER(9),
   "FaultyPartsSupplierCode" VARCHAR2(50),
   "FaultyPartsSupplierName" VARCHAR2(100),
   "ResponsibleUnitId"  NUMBER(9),
   "ResponsibleUnitCode" VARCHAR2(50),
   "ResponsibleUnitName" VARCHAR2(100),
   "BranchId"           NUMBER(9),
   "FaultyPartsId"      NUMBER(9),
   "FaultyPartsCode"    VARCHAR2(50),
   "FaultyPartsName"    VARCHAR2(100),
   constraint PK_USEDPARTSTRANSFERDETAIL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "UsedPartsTransferOrder"                              */
/*==============================================================*/
create table "UsedPartsTransferOrder"  (
   "Id"                 NUMBER(9)                       not null,
   "Code"               VARCHAR2(50)                    not null,
   "OriginWarehouseId"  NUMBER(9)                       not null,
   "OriginWarehouseCode" VARCHAR2(50)                    not null,
   "OriginWarehouseName" VARCHAR2(100)                   not null,
   "DestinationWarehouseId" NUMBER(9)                       not null,
   "DestinationWarehouseCode" VARCHAR2(50)                    not null,
   "TotalAmount"        NUMBER(19,4)                    not null,
   "DestinationWarehouseName" VARCHAR2(100)                   not null,
   "TotalQuantity"      NUMBER(9)                       not null,
   "Status"             NUMBER(9)                       not null,
   "OutboundStatus"     NUMBER(9),
   "InboundStatus"      NUMBER(9),
   "Remark"             VARCHAR2(200),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "ApproverId"         NUMBER(9),
   "ApproverName"       VARCHAR2(100),
   "ApproveTime"        DATE,
   "AbandonerId"        NUMBER(9),
   "AbandonerName"      VARCHAR2(100),
   "AbandonTime"        DATE,
   "RowVersion"         TIMESTAMP,
   constraint PK_USEDPARTSTRANSFERORDER primary key ("Id")
)
/

