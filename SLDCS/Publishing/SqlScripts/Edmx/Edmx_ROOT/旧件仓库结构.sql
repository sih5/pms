/*==============================================================*/
/* DBMS name:      ORACLE Version 10gR2                         */
/* Created on:     2014/2/27 15:24:44                           */
/*==============================================================*/


declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"UsedPartsStock"');
  if num>0 then
    execute immediate 'drop table "UsedPartsStock" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"UsedPartsWarehouse"');
  if num>0 then
    execute immediate 'drop table "UsedPartsWarehouse" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"UsedPartsWarehouseArea"');
  if num>0 then
    execute immediate 'drop table "UsedPartsWarehouseArea" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"UsedPartsWarehouseManager"');
  if num>0 then
    execute immediate 'drop table "UsedPartsWarehouseManager" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"UsedPartsWarehouseStaff"');
  if num>0 then
    execute immediate 'drop table "UsedPartsWarehouseStaff" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_UsedPartsStock"');
  if num>0 then
    execute immediate 'drop sequence "S_UsedPartsStock"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_UsedPartsWarehouse"');
  if num>0 then
    execute immediate 'drop sequence "S_UsedPartsWarehouse"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_UsedPartsWarehouseArea"');
  if num>0 then
    execute immediate 'drop sequence "S_UsedPartsWarehouseArea"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_UsedPartsWarehouseManager"');
  if num>0 then
    execute immediate 'drop sequence "S_UsedPartsWarehouseManager"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_UsedPartsWarehouseStaff"');
  if num>0 then
    execute immediate 'drop sequence "S_UsedPartsWarehouseStaff"';
  end if;
end;
/

create sequence "S_UsedPartsStock"
/

create sequence "S_UsedPartsWarehouse"
/

create sequence "S_UsedPartsWarehouseArea"
/

create sequence "S_UsedPartsWarehouseManager"
/

create sequence "S_UsedPartsWarehouseStaff"
/

/*==============================================================*/
/* Table: "UsedPartsStock"                                      */
/*==============================================================*/
create table "UsedPartsStock"  (
   "Id"                 NUMBER(9)                       not null,
   "UsedPartsWarehouseId" NUMBER(9)                       not null,
   "UsedPartsWarehouseAreaId" NUMBER(9)                       not null,
   "UsedPartsId"        NUMBER(9)                       not null,
   "UsedPartsCode"      VARCHAR2(50),
   "UsedPartsName"      VARCHAR2(100),
   "UsedPartsBarCode"   VARCHAR2(50),
   "UsedPartsBatchNumber" VARCHAR2(50),
   "UsedPartsSerialNumber" VARCHAR2(50),
   "StorageQuantity"    NUMBER(9)                       not null,
   "LockedQuantity"     NUMBER(9)                       not null,
   "ClaimBillId"        NUMBER(9),
   "ClaimBillType"      NUMBER(9),
   "ClaimBillCode"      VARCHAR2(50),
   "FaultyPartsId"      NUMBER(9),
   "FaultyPartsCode"    VARCHAR2(50),
   "FaultyPartsName"    VARCHAR2(100),
   "IfFaultyParts"      NUMBER(1),
   "UsedPartsSupplierId" NUMBER(9),
   "UsedPartsSupplierCode" VARCHAR2(50),
   "UsedPartsSupplierName" VARCHAR2(100),
   "FaultyPartsSupplierId" NUMBER(9),
   "FaultyPartsSupplierCode" VARCHAR2(50),
   "FaultyPartsSupplierName" VARCHAR2(100),
   "ResponsibleUnitId"  NUMBER(9),
   "ResponsibleUnitCode" VARCHAR2(50),
   "ResponsibleUnitName" VARCHAR2(100),
   "SettlementPrice"    NUMBER(19,4),
   "CostPrice"          NUMBER(19,4),
   "BranchId"           NUMBER(9),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "RowVersion"         TIMESTAMP,
   constraint PK_USEDPARTSSTOCK primary key ("Id")
)
/

/*==============================================================*/
/* Table: "UsedPartsWarehouse"                                  */
/*==============================================================*/
create table "UsedPartsWarehouse"  (
   "Id"                 NUMBER(9)                       not null,
   "Code"               VARCHAR2(50)                    not null,
   "Name"               VARCHAR2(100)                   not null,
   "BranchId"           NUMBER(9)                       not null,
   "PartsSalesCategoryId" NUMBER(9)                       not null,
   "Status"             NUMBER(9)                       not null,
   "Address"            VARCHAR2(200),
   "RegionId"           NUMBER(9),
   "ContactPhone"       VARCHAR2(50),
   "Contact"            VARCHAR2(50),
   "Fax"                VARCHAR2(50),
   "Email"              VARCHAR2(50),
   "StoragePolicy"      NUMBER(9)                       not null,
   "Remark"             VARCHAR2(200),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "RowVersion"         TIMESTAMP,
   constraint PK_USEDPARTSWAREHOUSE primary key ("Id")
)
/

/*==============================================================*/
/* Table: "UsedPartsWarehouseArea"                              */
/*==============================================================*/
create table "UsedPartsWarehouseArea"  (
   "Id"                 NUMBER(9)                       not null,
   "UsedPartsWarehouseId" NUMBER(9)                       not null,
   "ParentId"           NUMBER(9),
   "TopLevelUsedPartsWhseAreaId" NUMBER(9),
   "Code"               VARCHAR2(50)                    not null,
   "IfDefaultStoragePosition" NUMBER(1),
   "StorageCategory"    NUMBER(9),
   "Status"             NUMBER(9)                       not null,
   "Remark"             VARCHAR2(200),
   "StorageAreaType"    NUMBER(9)                       not null,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "RowVersion"         TIMESTAMP,
   constraint PK_USEDPARTSWAREHOUSEAREA primary key ("Id")
)
/

/*==============================================================*/
/* Table: "UsedPartsWarehouseManager"                           */
/*==============================================================*/
create table "UsedPartsWarehouseManager"  (
   "Id"                 NUMBER(9)                       not null,
   "UsedPartsWarehouseAreaId" NUMBER(9)                       not null,
   "PersonnelId"        NUMBER(9)                       not null,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "RowVersion"         TIMESTAMP,
   constraint PK_USEDPARTSWAREHOUSEMANAGER primary key ("Id")
)
/

/*==============================================================*/
/* Table: "UsedPartsWarehouseStaff"                             */
/*==============================================================*/
create table "UsedPartsWarehouseStaff"  (
   "Id"                 NUMBER(9)                       not null,
   "UsedPartsWarehouseId" NUMBER(9)                       not null,
   "PersonnelId"        NUMBER(9)                       not null,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "RowVersion"         TIMESTAMP,
   constraint PK_USEDPARTSWAREHOUSESTAFF primary key ("Id")
)
/

